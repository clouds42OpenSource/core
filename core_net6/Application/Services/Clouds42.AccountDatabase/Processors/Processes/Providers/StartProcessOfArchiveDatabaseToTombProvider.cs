﻿using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfArchiveDatabaseToTomb;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Processors.Processes.Providers
{
    /// <summary>
    /// Провайдер для запуска процесса на архивацию
    /// информационной базы в склеп
    /// </summary>
    internal class StartProcessOfArchiveDatabaseToTombProvider(
        StartProcessOfArchiveDatabaseToTombJobWrapper startProcessOfArchiveDatabaseToTombJobWrapper,
        CheckAccountDatabaseHelper checkAccountDatabaseHelper,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        DatabaseStatusHelper databaseStatusHelper,
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IStartProcessOfArchiveDatabaseToTombProvider
    {
        /// <summary>
        /// Запустить процесс на архивацию
        /// информационной базы в склеп
        /// </summary>
        /// <param name="processParams">Модель параметров для запуска процесса на архивацию
        /// информационной базы в склеп</param>
        public void StartProcessOfArchiveDatabaseToTomb(StartProcessOfArchiveDatabaseToTombParamsDto processParams)
        {
            var accountDatabase = accountDatabaseDataHelper.GetAccountDatabaseOrThrowException(processParams.DatabaseId);

            if (accountDatabase.StateEnum == DatabaseState.DelitingToTomb ||
                accountDatabase.StateEnum == DatabaseState.DetachingToTomb ||
                accountDatabase.StateEnum == DatabaseState.DetachedToTomb)
            {
                logger.Info(
                    $"[{processParams.DatabaseId}] :: Информационная база уже в статусе : {accountDatabase.StateEnum}.");
                return;
            }

            if (accountDatabase.IsDemoDelimiter() && accountDatabase.AccountDatabaseOnDelimiter.Zone == null)
            {
                databaseStatusHelper.SetDbStatus(accountDatabase.Id, DatabaseState.DeletedToTomb);
                return;
            }

            try
            {
                 
                 checkAccountDatabaseHelper.CheckPossibilitiesArchiveDatabasesToTomb(accountDatabase);
                 databaseStatusHelper.SetDbStatus(accountDatabase.Id, DatabaseState.DetachingToTomb);
                 startProcessOfArchiveDatabaseToTombJobWrapper.Start(
                     processParams.MapToProcessOfArchiveDatabaseToTombParamsDc(accessProvider.GetUser().Id));
 
            }
            catch (Exception ex)
            {
                 handlerException.Handle(ex,
                     $"[Ошибка запуска процесса архивации инф. базы] '{processParams.DatabaseId}'");
 
                 LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider,
                     LogActions.MovingDbToArchive,
                     $"Не удалось заархивировать базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}. Описание ошибки: {ex.Message}");
                 throw;
            }
 
        }

        /// <summary>
        ///Run processes for archiving info bases to the crypt
        /// </summary>
        /// <param name="databasesId">List of database ids</param>
        public void StartProcessOfArchiveDatabasesToTomb(List<Guid> databasesId)
        {
            try
            {
                databasesId.ForEach(id => StartProcessOfArchiveDatabaseToTomb(
                    new StartProcessOfArchiveDatabaseToTombParamsDto
                    {
                        DatabaseId = id,
                        Trigger = CreateBackupAccountDatabaseTrigger.MovingDbToArchive,
                        NeedForceFileUpload = true,
                        NeedNotifyHotlineAboutFailure = false
                    }));
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, 
                    "[Ошибка запуска процессов на архивацию информационных баз в склеп]");
                throw;
            }
        }
    }
}
