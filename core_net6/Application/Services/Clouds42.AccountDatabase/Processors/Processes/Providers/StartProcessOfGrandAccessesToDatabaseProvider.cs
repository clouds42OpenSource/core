﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Processors.Processes.Providers
{
    /// <summary>
    /// Провайдер для запуска процесса на выдачу доступа в базу
    /// </summary>
    internal class StartProcessOfGrandAccessesToDatabaseProvider(
        IAccountDatabaseUserAccessManager accountDatabaseUserAccessManager,
        IUnitOfWork dbLayer,
        IAccountDatabaseProvider accountDatabaseProvider)
        : IStartProcessOfGrandAccessesToDatabaseProvider
    {
        /// <summary>
        /// Запустить процессы на выдачу внутренних доступов в инф. базу
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        public IEnumerable<ManageAccessResultDto> StartProcessesOfGrandInternalAccesses(StartProcessesOfManageAccessesToDbDto model)
        {
            var grantResultList = new List<ManageAccessResultDto>();
            var accountDatabase = accountDatabaseProvider.GetAccountDatabaseOrThrowException(model.DatabaseId);

            model.UsersId.ForEach(userId =>
            {
                var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == userId);
                if (accountUser == null)
                    return;

                grantResultList.Add(accountDatabase.AccountId == accountUser.AccountId
                    ? accountDatabaseUserAccessManager.GrandInternalAccessToDb(model.DatabaseId, userId)
                    : accountDatabaseUserAccessManager.GrandExternalAccessToDb(model.DatabaseId, accountUser.Email));
            });
            return grantResultList;
        }

        /// <summary>
        /// Запустить процессы на выдачу доступов из карточки пользователя
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        public IEnumerable<ManageAccessResultDto> StartProcessesOfAccessesForUserCard(StartProcessesAccessesToDbForUserCardDto model)
        {
            var grantResultList = new List<ManageAccessResultDto>();
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == model.UsersId);
            if (accountUser == null)
                return grantResultList; 

            model.DatabaseIds.ForEach(databasId =>
            {
                var accountDatabase = accountDatabaseProvider.GetAccountDatabaseOrThrowException(databasId);

                if (accountDatabase.AccountId == accountUser.AccountId)
                    grantResultList.Add(accountDatabaseUserAccessManager.GrandInternalAccessToDb(databasId, model.UsersId));

                else
                    grantResultList.Add(accountDatabaseUserAccessManager.GrandExternalAccessToDb(databasId, accountUser.Email));
            });
            return grantResultList;
        }
    }
}
