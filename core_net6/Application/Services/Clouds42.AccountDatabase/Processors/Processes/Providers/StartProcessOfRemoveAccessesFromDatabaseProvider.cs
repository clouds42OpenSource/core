﻿using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AcDbAccess;

namespace Clouds42.AccountDatabase.Processors.Processes.Providers
{
    /// <summary>
    /// Провайдер для запуска процесса на удаление доступа из базы
    /// </summary>
    internal class StartProcessOfRemoveAccessesFromDatabaseProvider(
        IAccountDatabaseUserAccessManager accountDatabaseUserAccessManager)
        : IStartProcessOfRemoveAccessesFromDatabaseProvider
    {
        /// <summary>
        /// Запустить процессы на удаление доступов из инф. базы
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        public IEnumerable<ManageAccessResultDto> StartProcessesOfRemoveAccesses(
            StartProcessesOfManageAccessesToDbDto model) => model.UsersId.Select(userId =>
            accountDatabaseUserAccessManager.DeleteAccessFromDb(model.DatabaseId, userId));

        /// <summary>
        /// Запустить процессы на удаление доступов из из карточки пользователей
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        public IEnumerable<ManageAccessResultDto> StartProcessesOfRemoveAccessesForUserCard(
            StartProcessesAccessesToDbForUserCardDto model) => model.DatabaseIds.Select(databaseId =>
            accountDatabaseUserAccessManager.DeleteAccessFromDb(databaseId, model.UsersId));
    }
}
