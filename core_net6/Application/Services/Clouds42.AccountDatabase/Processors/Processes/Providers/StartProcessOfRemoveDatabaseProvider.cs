﻿using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfRemoveDatabase;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Processors.Processes.Providers
{
    /// <summary>
    /// Провайдер для запуска процесса на удаление инф. базы
    /// </summary>
    internal class StartProcessOfRemoveDatabaseProvider(
        StartProcessOfRemoveDatabaseJobWrapper startProcessOfRemoveDatabaseJobWrapper,
        DatabaseStatusHelper databaseStatusHelper,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        AccountDatabasePathHelper accountDatabasePathHelper,
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IStartProcessOfRemoveDatabaseProvider
    {
        /// <summary>
        /// Запустить процесс удаления инф. базы
        /// </summary>
        /// <param name="databaseId">Id базы</param>
        public void StartProcessOfRemoveDatabase(Guid databaseId)
        {
            var database = accountDatabaseDataHelper.GetAccountDatabaseOrThrowException(databaseId);

            if(database.StateEnum == DatabaseState.DetachedToTomb && !database.IsDelimiter() && !database.IsDemoDelimiter())
            {
                databaseStatusHelper.SetDbStatus(database, DatabaseState.DeletedToTomb);
                LogEventHelper.LogEvent(dbLayer, database.AccountId, accessProvider,
                   LogActions.DeleteAccountDatabase,
                   $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} успешно удалена.");

                return;
            }
            if (database is { StateEnum: DatabaseState.ErrorCreate, IsFile: true } && accountDatabasePathHelper.GetPath(database).ToDirectoryInfo().GetSize() == 0)
            {
                databaseStatusHelper.SetDbStatus(database, DatabaseState.DeletedToTomb);
                LogEventHelper.LogEvent(dbLayer, database.AccountId, accessProvider,
                   LogActions.DeleteAccountDatabase,
                   $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} успешно удалена, так как каталог был пуст.");

                return;
            }

            try
            {
                startProcessOfRemoveDatabaseJobWrapper.Start(new ProcessOfRemoveDatabaseParamsDto
                {
                    DatabaseId = databaseId,
                    InitiatorId = accessProvider.GetUser().Id,
                    FinalDeletionDatabaseStatus = DefineDatabaseStateForRemoving(database)
                });

                databaseStatusHelper.SetDbStatus(database, DatabaseState.DelitingToTomb);

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, 
                    $"[Ошибка запуска процесса удаления инф. базы] '{databaseId}'");

                LogEventHelper.LogEvent(dbLayer, database.AccountId, accessProvider,
                    LogActions.DeleteAccountDatabase,
                    $"Не удалось удалить базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}. Описание ошибки: {ex.Message}");


                throw;
            }
            
        }

        /// <summary>
        /// Запустить процессов удаления инф. баз
        /// </summary>
        /// <param name="databasesId">Id баз</param>
        public void StartProcessOfRemoveDatabases(List<Guid> databasesId)
        {
            try
            {
                databasesId.ForEach(StartProcessOfRemoveDatabase);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка запуска процессов удаления инф. баз] {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Определить статус базы для удаления
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <returns>Статус базы для удаления</returns>
        private DatabaseState DefineDatabaseStateForRemoving(Domain.DataModels.AccountDatabase database) =>
            database.IsDelimiter() && database.State == DatabaseState.ErrorCreate.ToString()
                ? DatabaseState.DeletedFromCloud
                : DatabaseState.DeletedToTomb;
    }
}
