﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.AccountDatabase.Jobs.TerminateSessionsInDatabase;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.AccountDatabase.Processors.Processes.Providers
{
    /// <summary>
    /// Провайдер для запуска процесса
    /// по завершению сеансов в информационной базе
    /// </summary>
    internal class StartProcessOfTerminateSessionsInDatabaseProvider(
        ITerminateSessionsInDatabaseJobWrapper terminateSessionsInDatabaseJobWrapper,
        ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider,
        ICreateTerminationSessionsInDatabaseProvider createTerminationSessionsInDatabaseProvider,
        IAccountDatabaseProvider accountDatabaseProvider,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        :
            IStartProcessOfTerminateSessionsInDatabaseProvider
    {
        /// <summary>
        /// Попытаться запустить процесс по завершению
        /// сеансов в информационной базе
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по завершению сеансов в информационной базе</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат успеха запуска процесса</returns>
        public bool TryStartProcess(StartProcessOfTerminateSessionsInDatabaseDto model, out string errorMessage, bool waitExecute = false)
        {
            return TryExecuteActionToStartProcessWithChecks(database =>
            {
                try
                {
                    var terminateSessionsInDatabaseModel = new TerminateSessionsInDatabaseJobParamsDto
                    {
                        TerminationSessionsInDatabaseId = createTerminationSessionsInDatabaseProvider.Create(
                        new CreateTerminationSessionsInDatabaseDto { DatabaseId = database.Id }),
                        InitiatorId = accessProvider.GetUser().Id
                    };

                    if (waitExecute)
                        terminateSessionsInDatabaseJobWrapper.Start(terminateSessionsInDatabaseModel).Wait();
                    else
                        terminateSessionsInDatabaseJobWrapper.Start(terminateSessionsInDatabaseModel);
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex,
                        $"[Ошибка запуска процесса по завершению сеансов в информационной базе] '{database.V82Name}'");
                    throw;
                }
            }, model, out errorMessage);
        }

        /// <summary>
        /// Попытаться выполнить действие по запуску
        /// процесса(завершение сеансов в базе) с необходимыми проверками  
        /// </summary>
        /// <param name="startProcessAction">Действие по запуску процесса</param>
        /// <param name="model">Модель для запуска процесса
        /// по завершению сеансов в информационной базе</param>
        /// <param name="errorMessage">Сообщение об ошибке(по результату проверок)</param>
        /// <returns>Результат выполнения действия</returns>
        private bool TryExecuteActionToStartProcessWithChecks(Action<Domain.DataModels.AccountDatabase> startProcessAction,
            StartProcessOfTerminateSessionsInDatabaseDto model, out string errorMessage)
        {
            var database = accountDatabaseProvider.GetAccountDatabaseOrThrowException(model.DatabaseId);

            if (!CheckAbilityTerminateSessionsInDatabase(database, out errorMessage))
                return false;

            startProcessAction(database);

            return true;
        }

        /// <summary>
        /// Проверить возможность завершить сеансы в инф. базе
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Возможность завершить сеансы в инф. базе</returns>
        private bool CheckAbilityTerminateSessionsInDatabase(Domain.DataModels.AccountDatabase database, out string errorMessage)
        {
            errorMessage = null;

            if (!IsDatabaseSuitableForTerminatingSessions(database))
            {
                errorMessage =
                    $"Завершение сеансов доступно только для файловой базы со статусом '{DatabaseState.Ready.Description()}'.";
                return false;
            }

            if (terminationSessionsInDatabaseDataProvider.IsExistTerminatingSessionsProcessInDatabase(database.Id))
            {
                errorMessage = $"Для инф. базы'{database.V82Name}' уже запущен процесс по завершению сеансов";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверить подходит ли инф. база для завершения сеансов
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <returns>Признак-подходит ли инф. база для завершения сеансов</returns>
        private static bool IsDatabaseSuitableForTerminatingSessions(Domain.DataModels.AccountDatabase database) =>
             database.StateEnum == DatabaseState.Ready || database.StateEnum == DatabaseState.DelitingToTomb 
            || database.StateEnum == DatabaseState.ProcessingSupport ;
    }
}
