﻿using Clouds42.AccountDatabase.Providers;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Processors.SupportProcessors
{
    /// <summary>
    ///  Фабрика создания процессоров обработки задачь потдержки.
    /// </summary>
    /// <typeparam name="TProcessor">Тип процессора обработки.</typeparam>
    public class SupportProcessorFactory<TProcessor>(IServiceProvider serviceProvider)
        where TProcessor : SupportProcessorBase
    {
        readonly AccountDatabaseSupportModelCreator _accountDatabaseSupportModelCreator = serviceProvider.GetRequiredService<AccountDatabaseSupportModelCreator>();

        /// <summary>
        /// Создать процессор обработки задачи поддержки.
        /// </summary>        
        public TProcessor CreateProcessor(Guid acDbSupportId, Starter1CProvider.CanContinue canContinueAction)
        {
            var accountDatabaseSupportModel = _accountDatabaseSupportModelCreator.CreateModel(acDbSupportId);

            var starter1C = CreateStarter1C(accountDatabaseSupportModel);
            var starter1CManager = CreateStarter1CManager(starter1C, accountDatabaseSupportModel, canContinueAction);

            return (TProcessor)serviceProvider.GetRequiredService<TProcessor>()
                .InitializationParameters(accountDatabaseSupportModel, starter1CManager, starter1C);
        }

        /// <summary>
        /// Создать стартер 1С.
        /// </summary>        
        private IStarter1C CreateStarter1C(AccountDatabaseSupportModelDto databaseSupportModel)
        {
            return serviceProvider.GetRequiredService<IStarter1C>().SetUpdateDatabase(databaseSupportModel);
        }

        /// <summary>
        /// Создать менеджер для стартера 1С.
        /// </summary>        
        private IStarter1CProvider CreateStarter1CManager(IStarter1C starter,
            IUpdateDatabaseDto updateDatabase,
            Starter1CProvider.CanContinue canContinueAction)
        {
            return serviceProvider.GetRequiredService<IStarter1CProvider>()
                .SetStarter1C(starter)
                .SetUpdateDatabase(updateDatabase)
                .SetCanContinueAction(canContinueAction);
        }
    }
}