﻿using System.Collections.Concurrent;
using Clouds42.AccountDatabase.Processors.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Processors.SupportProcessors
{
    /// <summary>
    /// Базовый провайдер поддержки
    /// </summary>
    /// <typeparam name="TProcessor">Тип процессора для обработки базы</typeparam>
    public abstract class SupportAccountDatabasesProviderBase<TProcessor>
        where TProcessor : SupportProcessorBase
    {
        /// <summary>
        /// Число работающих процессоров.
        /// </summary>
        private int _busyProcessorsCount;

        private readonly int _maxTehSupportParallelsThreads;
        private readonly bool _needEnqueueNotFatalErrorToQueue;

        private readonly ConcurrentDictionary<Guid, TProcessor> _processorsInProcess;
        private readonly ConcurrentQueue<TProcessor> _processorsQueue;
        private readonly IHandlerException _handlerException;
        protected readonly List<TProcessor> Results;

        private readonly IAccessProvider _accessProvider;
        private readonly SupportProcessorFactory<TProcessor> _supportProcessorFactory;
        protected readonly ILogger42 _logger;
        protected readonly IUnitOfWork _unitOfWork;

        private readonly Lazy<string> _routeForOpenAccountDatabasesPage;

        private readonly IDictionary<AccountDatabaseSupportProcessorState, Action<TProcessor>>
            _mapSupportProcessorStateToFinalizeProcessAction;

        protected SupportAccountDatabasesProviderBase(IAccessProvider accessProvider,
            SupportProcessorFactory<TProcessor> supportProcessorFactory,
            IHandlerException handlerException, ILogger42 logger, IUnitOfWork unitOfWork)
        {
            _accessProvider = accessProvider;
            _supportProcessorFactory = supportProcessorFactory;
            _processorsQueue = new ConcurrentQueue<TProcessor>();
            Results = [];
            _logger = logger;
            _unitOfWork = unitOfWork;
            _handlerException = handlerException;
            _processorsInProcess = new ConcurrentDictionary<Guid, TProcessor>();

            _maxTehSupportParallelsThreads =
                CloudConfigurationProvider.AccountDatabase.Support.GetMaxTehSupportParallelsThreads();
            _needEnqueueNotFatalErrorToQueue =
                CloudConfigurationProvider.AccountDatabase.Support.GetNeedEnqueueNotFatalErrorToQueue();

            _routeForOpenAccountDatabasesPage =
                new Lazy<string>(CloudConfigurationProvider.Cp.GetRouteForOpenAccountDatabasesPage);

            _mapSupportProcessorStateToFinalizeProcessAction =
                new Dictionary<AccountDatabaseSupportProcessorState, Action<TProcessor>>
                {
                    {AccountDatabaseSupportProcessorState.Success, FinalizeProcessWithFinalStatus},
                    {AccountDatabaseSupportProcessorState.FatalError, FinalizeProcessWithFinalStatus},
                    {AccountDatabaseSupportProcessorState.FatalValidationError, FinalizeProcessWithFinalStatus},
                    {AccountDatabaseSupportProcessorState.DbHasModifications, FinalizeProcessWithFinalStatus},
                    {AccountDatabaseSupportProcessorState.Error, FinalizeProcessWithNotFinalStatus},
                    {AccountDatabaseSupportProcessorState.Skip, FinalizeProcess},
                    {AccountDatabaseSupportProcessorState.HasActiveSessions, FinalizeProcess},
                };
        }

        /// <summary>
        /// Провести операцию по обслуживанию информационных баз.
        /// </summary>
        /// <param name="accountDatabasesSupports">Список информационных баз</param>
        /// <returns>Признак успешности</returns>
        protected bool ProcessAccountDatabases(List<AcDbSupport> accountDatabasesSupports)
        {
            foreach (var accountDatabasesSupport in accountDatabasesSupports)
            {
                _processorsQueue.Enqueue(
                    _supportProcessorFactory.CreateProcessor(accountDatabasesSupport.AccountDatabasesID,
                        CanProcessing));
            }

            while (NeedProcessing() && CanProcessing())
            {
                while (NeedProcessing() && CanProcessing() &&
                       _busyProcessorsCount < _maxTehSupportParallelsThreads)
                {
                    ExecuteSupportOperation();
                }

                Task.Delay(2000).Wait();
                _logger.Trace(
                    $"Ждем завершение цикла... _busyProcessorsCount = '{_busyProcessorsCount}' NeedProcessing = '{NeedProcessing()}' CanProcessing = '{CanProcessing()}'");
            }

            _logger.Trace("Начинаем регистрацию результатов.");
            NotifyCompleteOperation();

            return true;
        }

        /// <summary>
        /// Получить модель письма о результате проведения ТиИ
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="resultsOfAccount">Результаты операций по базам аккаунта.</param>
        /// <param name="siteAuthorityUrl">Урл сайта ЛК</param>
        /// <returns>Модель письма о результате проведения ТиИ</returns>
        protected TehSupportResultLetterModelDto GetTehSupportResultLetterModel(Guid accountId,
            List<TProcessor> resultsOfAccount, string siteAuthorityUrl) => new()
        {
                AccountId = accountId,
                UrlForAccountDatabasesPage =
                    new Uri($"{siteAuthorityUrl}/{_routeForOpenAccountDatabasesPage.Value}").AbsoluteUri,
                TehSupportFailureDatabases = resultsOfAccount
                    .Where(result =>
                        result.State == AccountDatabaseSupportProcessorState.Error ||
                        result.State == AccountDatabaseSupportProcessorState.FatalError).Select(result =>
                        new TehSupportDatabaseFailureDto
                        {
                            AccountDatabaseCaption = result.Caption,
                            Message = result.Message,
                            Description = result.Description.LineEndingToHtmlStyle()
                        }),
                TehSupportSuccessesDatabaseNames = resultsOfAccount
                    .Where(result =>
                        result.State != AccountDatabaseSupportProcessorState.Error &&
                        result.State != AccountDatabaseSupportProcessorState.FatalError)
                    .Select(result => result.Caption)
            };

        /// <summary>
        /// Зарегистрировать результат выполнения обслуживания
        /// </summary>
        /// <param name="processor">Обработчик обслуживания</param>
        protected void RegisterSupportHistory(TProcessor processor)
        {
          
            var cloudChangesProvider = new CloudChangesProvider(_unitOfWork, _accessProvider, _handlerException);

            var history = CreateSupportHistory(processor);
            _unitOfWork.AcDbSupportHistoryRepository.Insert(history);
            _unitOfWork.Save();
            cloudChangesProvider.LogEvent(processor.AccountId,
                history.State == SupportHistoryState.Error ? LogErrorAction : LogSuccessAction,
                processor.Message);
        }

        /// <summary>
        /// Операция обработчика.
        /// </summary>
        protected abstract DatabaseSupportOperation Operation { get; }

        /// <summary>
        /// Обозначение успешного выполнения в логе.
        /// </summary>
        protected abstract LogActions LogSuccessAction { get; }

        /// <summary>
        /// Обозначение ошибки в логе.
        /// </summary>
        protected abstract LogActions LogErrorAction { get; }

        /// <summary>
        /// Завершить обработку процессора.
        /// </summary>
        protected abstract void FinalizeProcess(TProcessor processor);

        /// <summary>
        /// Уведомить о проведние операции.
        /// </summary>
        protected abstract void NotifyCompleteOperation();

        /// <summary>
        /// Создать историческую запись по результату операции
        /// </summary>        
        protected abstract AcDbSupportHistory CreateSupportHistory(TProcessor supportResult);

        /// <summary>
        /// Признак возможности продолжать операцию
        /// </summary>
        /// <returns>Признак возможности продолжать операцию</returns>
        private bool CanProcessing()
            => DateTime.Now < StopProcessDateTime;

        private DateTime? _stopProcessDateTime;

        /// <summary>
        /// Время до которого разрешено проводить операции по обслуживанию.
        /// </summary>
        private DateTime StopProcessDateTime
        {
            get
            {
                if (_stopProcessDateTime.HasValue)
                    return _stopProcessDateTime.Value;

                _stopProcessDateTime = SupportProcessDataHelper.GetStopProcessDateTime();
                return _stopProcessDateTime.Value;
            }
        }

        /// <summary>
        /// Продолжить обработку процессоров операций по обслуживанию.
        /// </summary>                        
        private bool NeedProcessing()
            => _processorsQueue.Any(p =>
                   p.State == AccountDatabaseSupportProcessorState.New ||
                   p.State == AccountDatabaseSupportProcessorState.Error ||
                   p.State == AccountDatabaseSupportProcessorState.Updated ||
                   p.State == AccountDatabaseSupportProcessorState.Processing) ||
               _processorsInProcess.Any();

        /// <summary>
        /// Выполнить операцию поддержки
        /// </summary>
        private void ExecuteSupportOperation()
        {
            if (!_processorsQueue.TryDequeue(out var processor))
            {
                Task.Delay(200).Wait();
                return;
            }

            Interlocked.Increment(ref _busyProcessorsCount);
            _processorsInProcess.TryAdd(processor.AccountDatabasesID, processor);

            Task.Factory.StartNew(() =>
            {
                try
                {
                    processor.Process();
                    if (!_mapSupportProcessorStateToFinalizeProcessAction.TryGetValue(processor.State, out var value))
                        throw new InvalidOperationException(
                            $"Не удалось определить операцию завершения процесса для инф. базы {processor.AccountDatabasesID}");

                    value(processor);
                }
                catch (Exception ex)
                {
                    _handlerException.Handle(ex, "[Необработанная ошибка задачи поддержки]");
                }
                finally
                {
                    _processorsInProcess.TryRemove(processor.AccountDatabasesID, out processor);
                    Interlocked.Decrement(ref _busyProcessorsCount);
                }
            }).ContinueWith(t => { _logger.Warn($"Ошибка в выполнение дочернего потока :: {t.Exception}"); },
                TaskContinuationOptions.OnlyOnFaulted);
        }

        /// <summary>
        /// Завершить процесс с финальным статусом
        /// </summary>
        /// <param name="processor">Процессор поддержки</param>
        private void FinalizeProcessWithFinalStatus(TProcessor processor)
        {
            Results.Add(processor);
            FinalizeProcess(processor);
        }

        /// <summary>
        /// Завершить процесс с нефинальным статусом
        /// </summary>
        /// <param name="processor">Процессор поддержки</param>
        private void FinalizeProcessWithNotFinalStatus(TProcessor processor)
        {
            if (_needEnqueueNotFatalErrorToQueue && CanProcessing())
            {
                _processorsQueue.Enqueue(
                    _supportProcessorFactory.CreateProcessor(processor.AccountDatabasesID,
                        CanProcessing));
            }
            else
            {
                Results.Add(processor);
                FinalizeProcess(processor);
            }
        }
    }
}
