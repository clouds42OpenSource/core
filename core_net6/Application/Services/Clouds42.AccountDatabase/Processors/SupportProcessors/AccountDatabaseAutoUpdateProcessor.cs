﻿using System.Collections.Concurrent;
using Clouds42.AccountDatabase.Processors.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.AccountDatabaseSupport;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Processors.SupportProcessors
{
    /// <summary>
    /// Процессор автообновления инф. баз
    /// </summary>
    public class AccountDatabaseAutoUpdateProcessor
    {
        private readonly ConcurrentQueue<AccountDatabaseAutoUpdateJobWrapper> _jobsQueue;
        private readonly ConcurrentDictionary<Guid, AccountDatabaseSupportProcessorState> _processingAutoUpdateJobs;

        private readonly int _maxTehSupportParallelsThreads;
        private int _busyProcessorsCount;

        private readonly Dictionary<AccountDatabaseSupportProcessorState, Action<AccountDatabaseAutoUpdateResultDto>>
            _mapProcessStateToFinalizeAction;
        private readonly IHandlerException _handlerException;
        private readonly List<AccountDatabaseAutoUpdateResultDto> _supportResults;
        private readonly bool _needEnqueueNotFatalErrorToQueue;

        private readonly ILogger42 _logger;
        private readonly AccountDatabaseAutoUpdateJobsHelper _accountDatabaseAutoUpdateJobsHelper;
        private readonly FinalizeAutoUpdateHelper _finalizeAutoUpdateHelper;

        public AccountDatabaseAutoUpdateProcessor(
            AccountDatabaseAutoUpdateJobsHelper accountDatabaseAutoUpdateJobsHelper,
            FinalizeAutoUpdateHelper finalizeAutoUpdateHelper,
            ILogger42 logger, IHandlerException handlerException)
        {
            _jobsQueue = new ConcurrentQueue<AccountDatabaseAutoUpdateJobWrapper>();
            _supportResults = [];
            _processingAutoUpdateJobs = new ConcurrentDictionary<Guid, AccountDatabaseSupportProcessorState>();

            _needEnqueueNotFatalErrorToQueue =
                CloudConfigurationProvider.AccountDatabase.Support.GetNeedEnqueueNotFatalErrorToQueue();
            _maxTehSupportParallelsThreads =
                CloudConfigurationProvider.AccountDatabase.Support.GetMaxTehSupportParallelsThreads();
            _handlerException = handlerException;
            _accountDatabaseAutoUpdateJobsHelper = accountDatabaseAutoUpdateJobsHelper;
            _finalizeAutoUpdateHelper = finalizeAutoUpdateHelper;

            _mapProcessStateToFinalizeAction =
                _accountDatabaseAutoUpdateJobsHelper.CreateMapProcessStateToFinalizeActionDictionary(
                    FinalizeProcessWithDecideStatus, _finalizeAutoUpdateHelper.FinalizeAccountDatabaseAutoUpdateProcess,
                    FinalizeErrorProcess);
            _logger = logger;
        }

        /// <summary>
        /// Выполнить автообновление
        /// </summary>
        /// <param name="acDbSupportList">Список моделей поддержки инф. баз</param>
        public void ProcessAutoUpdate(List<AcDbSupport> acDbSupportList)
        {
            CreateAutoUpdateQueue(acDbSupportList);

            while (NeedProcessingJobs() && _accountDatabaseAutoUpdateJobsHelper.CanProcessTasks())
            {
                while (NeedProcessingJobs() && _accountDatabaseAutoUpdateJobsHelper.CanProcessTasks() &&
                       _busyProcessorsCount < _maxTehSupportParallelsThreads)
                {
                    if (!_jobsQueue.TryDequeue(out var jobWrapper))
                    {
                        Task.Delay(200).Wait();
                        continue;
                    }

                    Interlocked.Increment(ref _busyProcessorsCount);
                    _processingAutoUpdateJobs.TryAdd(jobWrapper.AccountDatabaseId, AccountDatabaseSupportProcessorState.Processing);

                    StartAutoUpdateTask(jobWrapper, FinalizeAutoUpdateProcess);

                    _logger.Trace($"Запущена задача для инф. базы {jobWrapper.AccountDatabaseId}");
                }

                _logger.Trace(
                    $"Ждем завершение цикла. _busyProcessorsCount = '{_busyProcessorsCount}' NeedProcessing = '{NeedProcessingJobs()}' CanProcessing = '{_accountDatabaseAutoUpdateJobsHelper.CanProcessTasks()}'");
               Task.Delay(2000).Wait();
            }

            _finalizeAutoUpdateHelper.NotifyAutoUpdateCompleteOperation(_supportResults);
        }

        /// <summary>
        /// Создать очередь задач на выполнение
        /// </summary>
        /// <param name="acDbSupportList">Список моделей поддержки инф. баз</param>
        private void CreateAutoUpdateQueue(List<AcDbSupport> acDbSupportList)
        {
            acDbSupportList.ForEach(acDbSupport =>
            {
                var jobWrapper = _accountDatabaseAutoUpdateJobsHelper.CreateJobWrapper(acDbSupport.AccountDatabasesID);
                _jobsQueue.Enqueue(jobWrapper);
            });
        }

        /// <summary>
        /// Запустить задачу автообновления
        /// </summary>
        /// <param name="jobWrapper">Обработчик задачи</param>
        /// <param name="finalizeAction">Действие по заврешению выполнения задачи</param>
        private void StartAutoUpdateTask(AccountDatabaseAutoUpdateJobWrapper jobWrapper,
            Action<AccountDatabaseAutoUpdateJobWrapper, AccountDatabaseSupportProcessorState> finalizeAction)
        {
            Task.Factory.StartNew(() =>
            {
                var processorState = AccountDatabaseSupportProcessorState.Processing;

                try
                {
                    var auTaskQueueId = StartTask(jobWrapper);
                    var result = jobWrapper.WaitResult();
                    _accountDatabaseAutoUpdateJobsHelper.RegisterAutoUpdateTechnicalResult(result, auTaskQueueId);

                    _mapProcessStateToFinalizeAction[result.SupportProcessorState](result);
                    _logger.Trace($"Задача для инф. базы {jobWrapper.AccountDatabaseId} завершена");
                    processorState = result.SupportProcessorState;
                }
                catch (Exception ex)
                {
                    _handlerException.Handle(ex,"[Необработаная ошибка задачи поддержки] lInfo()");
                }
                finally
                {
                    finalizeAction(jobWrapper, processorState);
                }
            }).ContinueWith(t => { _logger.Warn($"Ошибка в выполнение дочернего потока :: {t.Exception}"); },
                TaskContinuationOptions.OnlyOnFaulted);
        }

        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="jobWrapper">Обработчик задачи</param>
        /// <returns>ID задачи в очереди</returns>
        private static Guid StartTask(AccountDatabaseAutoUpdateJobWrapper jobWrapper)
        {
            jobWrapper.StartTask();
            if (jobWrapper.AuTaskQueueId == null || jobWrapper.AuTaskQueueId == Guid.Empty)
                throw new InvalidOperationException($"Не удалось запустить задачу автообновления для инф. базы {jobWrapper.AccountDatabaseId}");

            return jobWrapper.AuTaskQueueId.Value;
        }

        /// <summary>
        /// Завершить процесс автообновления
        /// </summary>
        /// <param name="jobWrapper">Обработчик задачи</param>
        /// <param name="processorState">Состояние процессора АО</param>
        private void FinalizeAutoUpdateProcess(AccountDatabaseAutoUpdateJobWrapper jobWrapper, AccountDatabaseSupportProcessorState processorState)
        {
            UpdateItemInProcessingJobsDictionary(jobWrapper.AccountDatabaseId, processorState);
            Interlocked.Decrement(ref _busyProcessorsCount);
        }

        /// <summary>
        /// Признак необходимости обрабатывать очередь задач
        /// </summary>
        /// <returns></returns>
        private bool NeedProcessingJobs()
            => _jobsQueue.Any()
               || _processingAutoUpdateJobs.Any(proc =>
                   _needEnqueueNotFatalErrorToQueue && _accountDatabaseAutoUpdateJobsHelper.CanProcessTasks()
                       ? proc.Value == AccountDatabaseSupportProcessorState.Error || proc.Value == AccountDatabaseSupportProcessorState.Processing
                       : proc.Value == AccountDatabaseSupportProcessorState.Processing);

        /// <summary>
        /// Завершить процесс с окончательным статусом
        /// </summary>
        /// <param name="autoUpdateResult">Результат выполнения джобы АО</param>
        private void FinalizeProcessWithDecideStatus(AccountDatabaseAutoUpdateResultDto autoUpdateResult)
        {
            _supportResults.Add(autoUpdateResult);
            _finalizeAutoUpdateHelper.FinalizeAccountDatabaseAutoUpdateProcess(autoUpdateResult);
        }

        /// <summary>
        /// Завершить процесс с ошибкой
        /// </summary>
        /// <param name="autoUpdateResult">Результат выполнения джобы АО</param>
        private void FinalizeErrorProcess(AccountDatabaseAutoUpdateResultDto autoUpdateResult)
        {
            if (_needEnqueueNotFatalErrorToQueue && _accountDatabaseAutoUpdateJobsHelper.CanProcessTasks())
            {
                _jobsQueue.Enqueue(_accountDatabaseAutoUpdateJobsHelper.CreateJobWrapper(autoUpdateResult.AccountDatabasesId));
                return;
            }

            FinalizeProcessWithDecideStatus(autoUpdateResult);
        }

        /// <summary>
        /// Обновить элемент в списке задач на выполнение
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="processorState">Состояние процессора АО</param>
        private void UpdateItemInProcessingJobsDictionary(Guid accountDatabaseId,
            AccountDatabaseSupportProcessorState processorState)
        {
            _processingAutoUpdateJobs.TryUpdate(accountDatabaseId, processorState,
                AccountDatabaseSupportProcessorState.Processing);
        }
    }
}
