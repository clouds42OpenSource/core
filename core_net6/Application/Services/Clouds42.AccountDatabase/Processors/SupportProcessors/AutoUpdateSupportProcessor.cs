﻿using Clouds42.AccountDatabase._1C.Providers;
using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Cluster1CProviders;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.RemoveDirectory;
using Clouds42.CoreWorker.JobWrappers.RestoreAccountDatabase;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Helpers;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.Logger;
using Clouds42.PowerShellClient;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Processors.SupportProcessors
{
    /// <summary>
    /// Процессор проведения автообновления инфомационной базы.
    /// </summary>
    public class AutoUpdateSupportProcessor : SupportProcessorBase, IAutoApdateSupportResultDto
    {
        public const string PROVIDER_1C_CONSOLE_APP_NAME = "Provider1C.ConsoleApp.exe";

        /// <summary>
        /// Версия информационной базы до обновления.
        /// </summary>
        public string OldVersion { get; private set; }

        /// <summary>
        /// Версия информационной базы после обновления.
        /// </summary>
        public string NewVersion { get; private set; }

        /// <summary>
        /// Выбор метода для пременения обновлений инфобазы 1С,
        /// <para>ключ <c>true</c> - означает что запустить обновление из текущего приложения, подключаясь к COM</para>
        /// <para>ключ <c>false</c> - означает что запустить обновление из консольного приложения Provider1C.ConsoleApp</para>
        /// </summary>
        private readonly Dictionary<bool, Action> _applyUpdateMethodChoice;

        /// <summary>
        /// Выбор метода для получения метаданных инфобазы 1С,
        /// <para>ключ <c>true</c> - означает что получить метаданные из текущего приложения, подключаясь к COM</para>
        /// <para>ключ <c>false</c> - означает что получить метаданные из консольного приложения Provider1C.ConsoleApp</para>
        /// </summary>
        private readonly Dictionary<bool, Func<ConnectorResultDto<MetadataResultDto>>> _getDatabaseMetadataMethodChoice;
        private readonly IAccountConfigurationDataProvider _accountConfProvider;
        private readonly IConfiguration _configuration;
        private readonly ICfuProvider cfuProvider;
        public AutoUpdateSupportProcessor(AccountDatabaseSupportModelDto accountDatabaseSupportModel,
            IUnitOfWork unitOfWork,
            IAccessProvider accessProvider,
            IStarter1C starter1C,
            IStarter1CProvider starter1CManager,
            IConnector1CProvider connector1CProvider,
            IRemoveDirectoryJobWrapper removeDirectoryJobWrapper,
            IHandlerException handlerException,
            IConfiguration configuration,
            ICluster1CProvider cluster1CProvider,
            TombTaskCreatorManager tombTaskCreatorManager,
            AccountDatabasePathHelper accountDatabasePathHelper,
            AccountDatabaseLocalBackupProvider accountDatabaseLocalBackupProvider,
            IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
            IAccountConfigurationDataProvider accountConfProvider,
            IRestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper accountDatabaseAfterFailedAutoUpdateJobWrapper,
            MailNotificationFactory mailNotificationFactory,
            ILogger42 logger42,
            AcDbSupportHelper acDbSupportHelper,
            PowerShellClientWrapper powerShellClientWrapper,
            IServiceProvider serviceProvider) :
                base(
                    accountDatabaseSupportModel,
                    unitOfWork,
                    accessProvider,
                    starter1C,
                    starter1CManager,
                    connector1CProvider,
                    removeDirectoryJobWrapper,
                    handlerException,
                    cluster1CProvider,
                    tombTaskCreatorManager,
                    accountDatabasePathHelper,
                    accountDatabaseLocalBackupProvider,
                    accountDatabaseChangeStateProvider,
                    accountDatabaseAfterFailedAutoUpdateJobWrapper,
                    mailNotificationFactory,
                    logger42,
                    powerShellClientWrapper,
                    acDbSupportHelper )
        {
            _applyUpdateMethodChoice = new Dictionary<bool, Action>
            {
                {true, ApplyUpdatesViaProvider},
                {false, ApplyUpdatesViaConsoleApp }
            };
            _configuration = configuration;
            _getDatabaseMetadataMethodChoice = new Dictionary<bool, Func<ConnectorResultDto<MetadataResultDto>>>
            {
                {true, DatabaseGetMetadataFromHere},
                {false, DatabaseGetMetadataFromConsoleApp}
            };
            cfuProvider = serviceProvider.GetRequiredService<ICfuProvider>();
            _accountConfProvider = accountConfProvider;
        }

        /// <summary>
        /// Начать проведение автообновления информационной базы.
        /// </summary>
        public override void Process()
        {
            try
            {
                InternalProcess();
                if (State == AccountDatabaseSupportProcessorState.Updated)
                    ValidateResult();
            }
            catch (CfuDamagedException ex)
            {
                RegisterState(AccountDatabaseSupportProcessorState.FatalError,
                    $"Ошибка при обновлении для базы  {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки:  \"{ex.Message}\"", debugInformation: ex.GetFullInfo(false));
            }
            catch (DbHasModificationException ex)
            {
                RegisterState(AccountDatabaseSupportProcessorState.DbHasModifications,
                    $"{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки:  \"{ex.Message}\"",
                    debugInformation: ex.GetFullInfo(false));
            }
            catch (AcDbHasActiveSessionsException ex)
            {
                RegisterState(AccountDatabaseSupportProcessorState.Error, $"Ошибка при автообновлении для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки: \"Наличие активных сессий в базе\"",
                    "Автообновление будет проведено в следующие сутки при отсутствии активных сессий",
                    ex.GetFullInfo(false));
            }
            catch (Exception ex)
            {
                RegisterState(AccountDatabaseSupportProcessorState.FatalError,
                    $"{DateTime.Now:f} - Ошибка при автообновлении для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки: \"{ex.Message}\"",
                    $"Ваша база {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)} автоматически восстановлена из архива. В ближайшее время с вами свяжется  техническая поддержка для устранения возможных неполадок",
                    ex.GetFullInfo(false));

                PrepareForProcess();

                RestoreBackupAccountDatabaseIfExist();
            }

            FinalizeProcess();
        }

        /// <summary>
        /// Завершить процесс обновления базы.
        /// </summary>
        private void FinalizeProcess()
        {
            if (!TryMoveDbToCloudStorage(() => State == AccountDatabaseSupportProcessorState.Updated && NeedCopyDbToTempStorage, out var errorMessage))
                RegisterState(AccountDatabaseSupportProcessorState.Error, "Не удалось скопировать обновленный файл базы.", errorMessage);

            MoveBackupToTombIfExist();

            if (State == AccountDatabaseSupportProcessorState.Updated)
            {
                RegisterState(AccountDatabaseSupportProcessorState.Success,
                    $"{DateTime.Now:f} - Обновлен релиз базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)} с \"{AccountDatabaseSupportModel.Support.CurrentVersion}\" на \"{NewVersion}\". ");
            }

            AccountDatabaseSetState(DatabaseState.Ready);
        }

        /// <summary>
        /// Принять обновления через провайдер
        /// </summary>
        private void ApplyUpdatesViaProvider()
        {
            Connector1CProvider.ApplyUpdates(AccountDatabaseSupportModel);
            CloseSession();
        }


        /// <summary>
        /// Принять обновления через консольное приложение
        /// </summary>
        private void ApplyUpdatesViaConsoleApp()
        {
            Connector1CViaConsoleApp.Execute(AccountDatabaseSupportModel,
                CloudConfigurationProvider.ConsoleApp1C.GetComConnectionTimeoutInSeconds(),
                CloudConfigurationProvider.ConsoleApp1C.GetComConnectionAttempts(),
                connector1C => connector1C.ApplyUpdates(
                    CloudConfigurationProvider.ConsoleApp1C.GetApplyUpdatesTimeoutInMinutes(),
                    CloudConfigurationProvider.ConsoleApp1C.GetApplyUpdatesAttempts()),
                _configuration[$"AttachProcess_{PROVIDER_1C_CONSOLE_APP_NAME}"]);
        }

        /// <summary>
        /// Применить полученные обновления.
        /// </summary>
        private void ApplyUpdate(string updateVersion)
        {
            try
            {
                PrepareForProcess();

                var useComConnectionForApplyUpdates = AccountDatabaseSupportModel.ConfigurationInfo?.UseComConnectionForApplyUpdates ?? true;
                _applyUpdateMethodChoice[useComConnectionForApplyUpdates]();

                PrepareForProcess();

                var connectResult = _getDatabaseMetadataMethodChoice[useComConnectionForApplyUpdates]();
                if (connectResult.Result?.Version != updateVersion)
                    throw new InvalidOperationException($"Версия метаданных инф. базы \"{connectResult.Result?.Version}\" не совпадает с версией \"{updateVersion}\"");

                PrepareForProcess();
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка при автообновлении для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки: \"Не удалось принять обновления версии \"{updateVersion}\"\".";
                TraceLog($"{errorMessage} :: {ex.Message}");
                CloseSession();
                throw new InvalidOperationException(errorMessage);
            }
        }

        /// <summary>
        /// Проверить результат выполнения обновлений.
        /// </summary>
        private void ValidateResult()
        {
            if (!string.IsNullOrEmpty(NewVersion))
                return;

            throw new InvalidOperationException($"Ошибка при автообновлении для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки: \"База не обновилась, пакет не содержит доступных обновлений.\"");
        }

        /// <summary>
        /// Получить метаданные инф. базы напрямую
        /// </summary>
        /// <returns>Метаданные инф. базы</returns>
        private ConnectorResultDto<MetadataResultDto> DatabaseGetMetadataFromHere()
            => GetMetadataAccountDatabase();

        /// <summary>
        /// Получить метаданные инф. базы через консольное приложение
        /// </summary>
        /// <returns>Метаданные инф. базы</returns>
        private ConnectorResultDto<MetadataResultDto> DatabaseGetMetadataFromConsoleApp()
        {
            try
            {
                var metadataInfo = Starter1CManager.TryGetMetadata();
                if (metadataInfo.ConnectCode != ConnectCodeDto.Success)
                    throw new InvalidOperationException(metadataInfo.Message);

                TraceLog("Успешно получены метаданные через 1С Предприятие.");
                return metadataInfo;
            }
            catch (Exception ex)
            {
                TraceLog($"Ошибка при автообновлении для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки: \"Не удалось получить метаданные через 1С Предприятие. Причина: {ex.GetFullInfo(false)}\"");
            }

            return Connector1CViaConsoleApp.Execute(AccountDatabaseSupportModel,
                CloudConfigurationProvider.ConsoleApp1C.GetComConnectionTimeoutInSeconds(),
                CloudConfigurationProvider.ConsoleApp1C.GetComConnectionAttempts(),
                connector1C =>
                {
                    return connector1C.GetMetadataInfo(
                        CloudConfigurationProvider.ConsoleApp1C.GetRetrieveDatabaseMetadataTimeoutInMinutes(),
                        CloudConfigurationProvider.ConsoleApp1C.GetRetrieveDatabaseMetadataAttempts());
                }, _configuration[$"AttachProcess_{PROVIDER_1C_CONSOLE_APP_NAME}"]);
        }

        /// <summary>
        /// Провести автообновление.
        /// </summary>
        private void InternalProcess()
        {
            var currentVersion = AccountDatabaseSupportModel.Support.CurrentVersion;
            OldVersion = currentVersion;

            State = AccountDatabaseSupportProcessorState.Processing;

            if (AccountDatabaseUsed())
            {
                RegisterState(AccountDatabaseSupportProcessorState.Error, $"Ошибка при автообновлении для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки: \"Наличие активных сессий в базе\"", "Автообновление будет проведено в следующие сутки при отсутствии активных сессий");
                return;
            }

            AccountDatabaseSetState(DatabaseState.ProcessingSupport);

            if (AccountDatabaseSupportModel.IsFile)
            {
                var getMetadataFromHere = AccountDatabaseSupportModel.ConfigurationInfo?.UseComConnectionForApplyUpdates ?? true;

                var connectResult = _getDatabaseMetadataMethodChoice[getMetadataFromHere]();

                if (connectResult.ConnectCode != ConnectCodeDto.Success)
                {
                    RegisterFailConnect(connectResult);
                    return;
                }

                currentVersion = connectResult.Result.Version;
                OldVersion = currentVersion;
                UpdateAcDbConfigurationVersion(currentVersion);
            }

            TraceLog($"Текущая версия релиза инф. базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}: {currentVersion}");

            if (!HasAvailableUpdate(currentVersion))
            {
                RegisterState(AccountDatabaseSupportProcessorState.Skip, $"Нет доступных обновлений. База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)} актуальна.");
                return;
            }

            if (!HasPlatformVersionCompatibility(currentVersion, out var errorMsg))
            {
                RegisterState(AccountDatabaseSupportProcessorState.FatalValidationError, errorMsg);
                return;
            }

            PrepareForProcess();

            if (!CreateBackupAccountDatabase())
            {
                RegisterState(AccountDatabaseSupportProcessorState.Error, $"Не удалось создать архивную копию базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}");
                return;
            }

            CopyDatabaseToUpdateStorage(() => NeedCopyDbToTempStorage);

            if (Starter1CManager.TryUpdateToLastVersion(currentVersion, AccountDatabaseSupportModel.ConfigurationInfo,
                ApplyUpdate,
                out var targetVersion))
            {
                RegisterState(AccountDatabaseSupportProcessorState.Updated,
                    $"{DateTime.Now:f} - Обновлен релиз базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)} с \"{AccountDatabaseSupportModel.Support.CurrentVersion}\" до  \"{targetVersion}\" ");

                NewVersion = targetVersion;
            }
            else
            {
                RegisterState(AccountDatabaseSupportProcessorState.Skip, $"Нет доступных обновлений. База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)} актуальна.");
            }
        }

        /// <summary>
        /// Уведомить тех поддержку о проблеме автообновления.
        /// </summary>
        protected override void NotifySupportFatalError()
        {
            var message =
                 $"При Автообновлении информационной базы “{Caption}” произошла ошибка.<br/>" +
                 $"{Message}<br/>" +
                 $"Номер базы: {V82Name} <br/>" +
                 (!string.IsNullOrEmpty(BackupPath) ? $"Ссылка на созданный бекап: {BackupPath}" : string.Empty);

            var content = new EmailContentBuilder(_accountConfProvider,message).Build();

            new Support42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(CloudConfigurationProvider.AccountDatabase.Support.GetSupportNotificationEmail())
                .Body(content)
                .Subject("Ядро сбой. Оповещение о неуспешном АО")
                .SendViaNewThread();
        }

        /// <summary>
        /// Тип триггера бекапа.
        /// </summary>
        protected override CreateBackupAccountDatabaseTrigger BackUptrigger =>
            CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase;

        /// <summary>
        /// Проверить обновляемую базу на наличие доступных новых версий обновления.
        /// </summary>
        /// <param name="version">Версия платформы</param>
        /// <returns></returns>
        private bool HasAvailableUpdate(string version)
        {
            if (string.IsNullOrEmpty(version))
                throw new InvalidOperationException(
                    $"Невозможно проверить наличие доступных обновлений для инф. базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}" +
                        " так как текущая версия конфигурации пуста.");

            var lastAvailableVersion = cfuProvider.GetLastUpdateVersion(
                AccountDatabaseSupportModel.ConfigurationInfo, version,
                AccountDatabaseSupportModel.PlatformVersion);
            return lastAvailableVersion != null && lastAvailableVersion.Platform1CVersion != version;
        }

        /// <summary>
        /// Проверить обновляемую базу на совместимость версии платформы 
        /// </summary>
        /// <param name="version">Версия платформы</param>
        /// <param name="errormsg">Сообщение об ошибке</param>
        /// <returns>trie - если версии плтформы совместимы</returns>
        private bool HasPlatformVersionCompatibility(string version, out string errormsg)
        {
            var lastAvailableVersion = cfuProvider.GetLastUpdateVersion(AccountDatabaseSupportModel.ConfigurationInfo,
                version, AccountDatabaseSupportModel.PlatformVersion);

            if (lastAvailableVersion == null)
            {
                errormsg = "Нет доступной версии для обновления.";
                return false;
            }

            if (!string.IsNullOrEmpty(lastAvailableVersion.Platform1CVersion))
            {
                var resultCompare = new VersionComparer().Compare(lastAvailableVersion.Platform1CVersion, AccountDatabaseSupportModel.PlatformVersion);
                if (resultCompare == 1)
                {
                    errormsg = $"Текущая версия платформы базы {AccountDatabaseSupportModel.PlatformVersion}," +
                                   $"меньше версии платформы с которой можно обновить базу {lastAvailableVersion.Platform1CVersion}.";
                    return false;
                }
            }
            errormsg = "";
            return true;
        }
    }
}
