﻿using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Cluster1CProviders;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.RemoveDirectory;
using Clouds42.CoreWorker.JobWrappers.RestoreAccountDatabase;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Helpers;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.Logger;
using Clouds42.PowerShellClient;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers.Interfaces;
using Clouds42.Tools1C.Helpers;

namespace Clouds42.AccountDatabase.Processors.SupportProcessors
{

    /// <summary>
    /// Процессор проведения ТиИ инфомационной базы.
    /// </summary>
    public class TehSupportProcessor(
        AccountDatabaseSupportModelDto accountDatabaseSupportModel,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IStarter1C starter1C,
        IStarter1CProvider starter1CManager,
        IConnector1CProvider connector1CProvider,
        IRemoveDirectoryJobWrapper removeDirectoryJobWrapper,
        IHandlerException handlerException,
        ICluster1CProvider cluster1CProvider,
        TombTaskCreatorManager tombTaskCreatorManager,
        AccountDatabasePathHelper accountDatabasePathHelper,
        AccountDatabaseLocalBackupProvider accountDatabaseLocalBackupProvider,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        IRestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper accountDatabaseAfterFailedAutoUpdateJobWrapper,
        MailNotificationFactory mailNotificationFactory,
        IAccountConfigurationDataProvider accountConfProvider,
        ILogger42 logger42,
        PowerShellClientWrapper powerShellClientWrapper,
        AcDbSupportHelper acDbSupportHelper)
        : SupportProcessorBase(accountDatabaseSupportModel,
            unitOfWork,
            accessProvider,
            starter1C,
            starter1CManager,
            connector1CProvider,
            removeDirectoryJobWrapper,
            handlerException,
            cluster1CProvider,
            tombTaskCreatorManager,
            accountDatabasePathHelper,
            accountDatabaseLocalBackupProvider,
            accountDatabaseChangeStateProvider,
            accountDatabaseAfterFailedAutoUpdateJobWrapper,
            mailNotificationFactory,
            logger42,
            powerShellClientWrapper,
            acDbSupportHelper)
    {        
        private const string CommondLineParams = "/IBCheckAndRepair";

        /// <summary>
        /// Начать проведение ТиИ.
        /// </summary>
        public override void Process()
        {
            try
            {
                InternalProcess();
            }
            catch (Exception ex)
            {
                RegisterState(AccountDatabaseSupportProcessorState.FatalError,
                    $"{DateTime.Now:f} - ошибка проведения ТиИ в базе “{Caption}”. {ex.Message} ",
                    "Ваша база автоматически восстановлена из архива. В ближайшее время с вами свяжется  техническая поддержка для устранения возможных неполадок");
                _handlerException.Handle(ex, "[Ошибка проведения ТиИ.]");

                PrepareForProcess();

                RestoreBackupAccountDatabaseIfExist();
            }

            FinalizeProcess();
        }

        /// <summary>
        /// Завершить процесс ТиИ базы.
        /// </summary>
        private void FinalizeProcess()
        {
            if (!TryMoveDbToCloudStorage(() => State == AccountDatabaseSupportProcessorState.Success && NeedCopyDbToTempStorage, out var errorMessage))
                RegisterState(AccountDatabaseSupportProcessorState.Error, "Неудалось скопировать обновленный файл базы.", errorMessage);

            MoveBackupToTombIfExist();

            if (State == AccountDatabaseSupportProcessorState.Success)
            {
                RegisterState(AccountDatabaseSupportProcessorState.Success, $"{DateTime.Now:f} - успешно выполнено тестирование и исправление ошибок в базе  “{Caption}”. ");
            }

            AccountDatabaseSetState(DatabaseState.Ready);
        }

        /// <summary>
        /// Провести ТиИ.
        /// </summary>
        private void InternalProcess()
        {
            
            State = AccountDatabaseSupportProcessorState.Processing;

            if (AccountDatabaseUsed())
            {
                RegisterState(AccountDatabaseSupportProcessorState.HasActiveSessions, $"Наличие активных сессий в базе “{Caption}”", "ТиИ будет проведено в следующие сутки при отсутствии активных сессий");
                return;
            }
            AccountDatabaseSetState(DatabaseState.ProcessingSupport);
            
            if (!CreateBackupAccountDatabase())
            {
                RegisterState(AccountDatabaseSupportProcessorState.Error, $"Не удалось создать архивную копию базы “{Caption}”");
                return;
            }

            CopyDatabaseToUpdateStorage(() => NeedCopyDbToTempStorage);

            ProcessTehSupport();
        }
       
        /// <summary>
        /// Выполнить ТиИ.
        /// </summary>
        private void ProcessTehSupport()
        {
            TraceLog("Начало ТиИ");

            var result = Starter1C.StartApp(Get1CCommandLineParams(), false, true);

            if (result)
            {                
                RegisterState(AccountDatabaseSupportProcessorState.Success, $"{DateTime.Now:f} - успешно выполнено ТиИ в базе “{Caption}”");             
            }
            else
            {                                
                throw new InvalidOperationException("Процессс проведения ТиИ был прерван.");               
            }
        }
    
        /// <summary>
        /// Уведомить тех поддержку о проблеме ТиИ.
        /// </summary>
        protected override void NotifySupportFatalError()
        {
            var message =
                 $"При проведении регламентной операции Тестирование и Исправление в информационной базе “{Caption}” произошла неизвестная ошибка.<br/>"+
                  "Необходимо выяснить причину и провести ТиИ информационной базы.<br/>" +
                 $"Номер базы: {V82Name} <br/>" +
                 (!string.IsNullOrEmpty(BackupPath) ? $"Ссылка на созданный бекап: {BackupPath}" : string.Empty);

            var content = new EmailContentBuilder(accountConfProvider,message).Build();

            new Support42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(CloudConfigurationProvider.AccountDatabase.Support.GetSupportNotificationEmail())
                .Body(content)
                .Subject("Оповещение о неуспешном ТиИ")
                .SendViaNewThread();
        }

        /// <summary>
        /// Тип триггера бекапа.
        /// </summary>
        protected override CreateBackupAccountDatabaseTrigger BackUptrigger =>
            CreateBackupAccountDatabaseTrigger.TehSupportAccountDatabase;

        /// <summary>
        /// Получить строку запуска 1С
        /// </summary>
        /// <returns>Строка запуска 1С</returns>
        private static string Get1CCommandLineParams()
            => $@"{CommondLineParams} 
                    {TestingParamsValue.ReIndex.Description()} 
                    {TestingParamsValue.IBCompression.Description()} 
                    {TestingParamsValue.Rebuild.Description()}  
                    {TestingParamsValue.RecalcTotals.Description()} 
                    {Start1CCommandLineConstants.DisableStartupDialogs} 
                    {Start1CCommandLineConstants.DisableStartupMessages} 
                    {Start1CCommandLineConstants.UsePrivilegedMode}";
    }
}
