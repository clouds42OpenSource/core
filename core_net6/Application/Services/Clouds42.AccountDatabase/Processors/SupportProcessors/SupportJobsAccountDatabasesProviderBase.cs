﻿using Clouds42.AccountDatabase.Processors.Helpers;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Processors.SupportProcessors
{
    /// <summary>
    /// Базовый провайдер поддержки (использует джобы вместо процессоров)
    /// </summary>
    /// <typeparam name="TProcessor">Тип процессора для обработки базы</typeparam>
    public abstract class SupportJobsAccountDatabasesProviderBase<TProcessor>(
        SupportProcessorFactory<TProcessor> supportProcessorFactory)
        where TProcessor : SupportProcessorBase
    {
        /// <summary>
        /// Обработать инф. базу
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <returns>Результат обработки</returns>
        protected AccountDatabaseAutoUpdateResultDto ProcessSingleAccountDatabase(AcDbSupport acDbSupport)
        {
            var processor = supportProcessorFactory.CreateProcessor(acDbSupport.AccountDatabasesID, CanProcessing);
            processor.Process();

            return (AccountDatabaseAutoUpdateResultDto)CreateSupportResult(processor);
        }

        /// <summary>
        /// Создать результат выполнения поддержки
        /// </summary>
        /// <param name="processor">Процессор поддержки</param>
        /// <returns>Результат выполнения поддержки</returns>
        protected abstract AccountDatabaseSupportResultDto CreateSupportResult(TProcessor processor);

        /// <summary>
        /// Признак возможности продолжать операцию
        /// </summary>
        /// <returns>Признак возможности продолжать операцию</returns>
        private bool CanProcessing()
            => DateTime.Now < StopProcessDateTime;

        private DateTime? _stopProcessDateTime;

        /// <summary>
        /// Время до которого разрешено проводить операции по обслуживанию.
        /// </summary>
        private DateTime StopProcessDateTime
        {
            get
            {
                if (_stopProcessDateTime.HasValue)
                    return _stopProcessDateTime.Value;

                _stopProcessDateTime = SupportProcessDataHelper.GetStopProcessDateTime();
                return _stopProcessDateTime.Value;
            }
        }
    }
}
