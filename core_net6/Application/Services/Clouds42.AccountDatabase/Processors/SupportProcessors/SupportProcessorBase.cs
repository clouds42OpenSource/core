﻿using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Cluster1CProviders;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.RemoveDirectory;
using Clouds42.CoreWorker.JobWrappers.RestoreAccountDatabase;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseRestoreFromTomb;
using Clouds42.Logger;
using Clouds42.PowerShellClient;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers.Interfaces;

namespace Clouds42.AccountDatabase.Processors.SupportProcessors
{
    /// <summary>
    /// Процессор поддержки инфомационной базы.
    /// </summary>
    public abstract class SupportProcessorBase : ISupportResultDto
    {
        protected readonly IAccessProvider AccessProvider;        
        protected readonly IUnitOfWork _unitOfWork;        
        protected AccountDatabaseSupportModelDto AccountDatabaseSupportModel;
        protected readonly IConnector1CProvider Connector1CProvider;
        private readonly IRemoveDirectoryJobWrapper _removeDirectoryJobWrapper;
        protected IStarter1CProvider Starter1CManager;
        protected IStarter1C Starter1C;
        protected ICluster1CProvider Cluster1CProvider;
        protected readonly IHandlerException _handlerException;
        private readonly object _thisLock = new();                                                
        private readonly ILogger42 _logger;
        private readonly AcDbSupportHelper _acDbSupportHelper;
        private readonly Lazy<bool> _isVipAccount;
        private readonly TombTaskCreatorManager _tombTaskCreatorManager;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;
        private readonly AccountDatabaseLocalBackupProvider _accountDatabaseLocalBackupProvider;
        private readonly IAccountDatabaseChangeStateProvider _accountDatabaseChangeStateProvider;
        private readonly IRestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper _accountDatabaseAfterFailedAutoUpdateJobWrapper;
        private readonly MailNotificationFactory _mailNotificationFactory;
        private readonly PowerShellClientWrapper _powerShellClientWrapper;

        protected SupportProcessorBase(
            AccountDatabaseSupportModelDto accountDatabaseSupportModel, 
            IUnitOfWork unitOfWork,
            IAccessProvider accessProvider,
            IStarter1C starter1C,
            IStarter1CProvider starter1CManager,
            IConnector1CProvider connector1CProvider,
            IRemoveDirectoryJobWrapper removeDirectoryJobWrapper, 
            IHandlerException handlerException,
            ICluster1CProvider cluster1CProvider,
            TombTaskCreatorManager tombTaskCreatorManager,
            AccountDatabasePathHelper accountDatabasePathHelper,
            AccountDatabaseLocalBackupProvider accountDatabaseLocalBackupProvider,
            IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
            IRestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper accountDatabaseAfterFailedAutoUpdateJobWrapper,
            MailNotificationFactory mailNotificationFactory,
            ILogger42 logger42,
            PowerShellClientWrapper powerShellClientWrapper,
            AcDbSupportHelper acDbSupportHelper)
        {
            _unitOfWork = unitOfWork;
            Starter1C = starter1C;
            Connector1CProvider = connector1CProvider;
            _removeDirectoryJobWrapper = removeDirectoryJobWrapper;
            AccessProvider = accessProvider;
            Starter1CManager = starter1CManager;
            State = AccountDatabaseSupportProcessorState.New;
            AccountDatabaseSupportModel = accountDatabaseSupportModel;
            _handlerException = handlerException;
            _acDbSupportHelper = acDbSupportHelper;
            _isVipAccount = new Lazy<bool>(IsVipAccount);
            _logger = logger42;
            Cluster1CProvider = cluster1CProvider;
            _tombTaskCreatorManager = tombTaskCreatorManager;
            _accountDatabasePathHelper = accountDatabasePathHelper;
            _accountDatabaseLocalBackupProvider = accountDatabaseLocalBackupProvider;
            _accountDatabaseChangeStateProvider = accountDatabaseChangeStateProvider;
            _accountDatabaseAfterFailedAutoUpdateJobWrapper = accountDatabaseAfterFailedAutoUpdateJobWrapper;
            _mailNotificationFactory = mailNotificationFactory;
            _powerShellClientWrapper = powerShellClientWrapper;
        }

        /// <summary>
        /// Номер объект AcDbSupport
        /// </summary>
        public Guid AccountDatabasesID => AccountDatabaseSupportModel.Support.AccountDatabasesID;

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public Guid AccountId => AccountDatabaseSupportModel.AccountDatabase.AccountId;

        /// <summary>
        /// Номер информационной базы.
        /// </summary>
        public string V82Name => AccountDatabaseSupportModel.AccountDatabase.V82Name;

        /// <summary>
        /// Стостояние процесса.
        /// </summary>
        public AccountDatabaseSupportProcessorState State { get; protected set; }

        /// <summary>
        /// Сообщение.
        /// </summary>
        public string Message { get; protected set; }

        /// <summary>
        /// Детальная информация.
        /// </summary>
        public string Description { get; protected set; }

        /// <summary>
        /// Информация для отладки
        /// </summary>
        public string DebugInformation { get; protected set; }

        /// <summary>
        /// Заголов информационной базы.
        /// </summary>
        public string Caption => AccountDatabaseSupportModel.AccountDatabase.Caption;

        /// <summary>
        /// Путь к бекапу.
        /// </summary>
        public string BackupPath { get; protected set; }        

        /// <summary>
        /// Номер бэкапа информационной базы.
        /// </summary>
        protected Guid? AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// Проверка необходимости копировать базу на временно хранилище.
        /// </summary>
        protected bool NeedCopyDbToTempStorage => AccountDatabaseSupportModel.IsFile && _isVipAccount.Value;
        
        /// <summary>
        /// Начать процесс. 
        /// </summary>
        public abstract void Process();

        /// <summary>
        /// Уведомить тех поддержку о проблеме ТиИ.
        /// </summary>
        protected abstract void NotifySupportFatalError();

        /// <summary>
        /// Тип триггера бекапа.
        /// </summary>
        protected abstract CreateBackupAccountDatabaseTrigger BackUptrigger { get; }
        
        /// <summary>
        /// Подключиться к информационной базе.
        /// </summary>        
        protected ConnectorResultDto<MetadataResultDto> GetMetadataAccountDatabase()
        {
            var connectResult = Connector1CProvider.GetMetadataInfo(AccountDatabaseSupportModel);
            return connectResult;
        }

        /// <summary>
        /// Инициализация параметров процесса
        /// </summary>
        /// <param name="accountDatabaseSupportModel">модель обновления базы</param>
        /// <param name="starter1CManager">менеджер 1с</param>
        /// <param name="starter1C">запуск 1С</param>
        internal SupportProcessorBase InitializationParameters(AccountDatabaseSupportModelDto accountDatabaseSupportModel, IStarter1CProvider starter1CManager, IStarter1C starter1C)
        {
            AccountDatabaseSupportModel = accountDatabaseSupportModel;
            Starter1C = starter1C;
            Starter1CManager = starter1CManager;
            return this;
        }

        /// <summary>
        /// Зарегистрировать не успешное подключение к базе.
        /// </summary>        
        protected void RegisterFailConnect(ConnectorResultDto<MetadataResultDto> connectResult)
        {
            if (connectResult.ConnectCode == ConnectCodeDto.PasswordOrLoginIncorrect)
                RegisterInvalidLoginPassword();

            if (connectResult.ConnectCode == ConnectCodeDto.DatabaseDamaged)
                RegisterState(AccountDatabaseSupportProcessorState.FatalError, $"Файл базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)} поврежден", connectResult.Message);

            if (connectResult.ConnectCode == ConnectCodeDto.ConnectError)
                RegisterState(AccountDatabaseSupportProcessorState.Error, $"Ошибка подключения к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}", connectResult.Message);
        }        

        /// <summary>
        /// Подготовить базу к проведению тех. работ.
        /// </summary>
        protected void PrepareForProcess()
        {
            if (AccountDatabaseSupportModel.AccountDatabase.IsFile == true)
            {
              var cloudServiceFileStorageServer =
              _unitOfWork.CloudServicesFileStorageServerRepository.FirstOrDefault(w => w.ID == AccountDatabaseSupportModel.AccountDatabase.FileStorageID);
              AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(AccountDatabaseSupportModel, AccountDatabaseSupportModel.UpdateDatabaseName, cloudServiceFileStorageServer.DnsName);
            }
        }

        /// <summary>
        /// Записать трассировку в лог.
        /// </summary>
        /// <param name="message">Сообщение трассировки.</param>
        protected void TraceLog(string message)
        {
            _logger.Trace($"{AccountDatabaseSupportModel.AccountDatabase.V82Name}:: {message}");
        }

        /// <summary>
        /// Завершение все активных сеансов
        /// </summary>
        protected void CloseSession()
        {
            _logger.Info($"{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Завершаем все открытые сессии");
            var cloudServiceFileStorageServer =
            _unitOfWork.CloudServicesFileStorageServerRepository.FirstOrDefault(w => w.ID == AccountDatabaseSupportModel.AccountDatabase.FileStorageID);

            _powerShellClientWrapper.ExecuteScript(
                    $"Get-SmbOpenFile | where {{$_.Path –like \"*{AccountDatabaseSupportModel.AccountDatabase.V82Name}*\"}} | Close-SmbOpenFile -Force",
                    cloudServiceFileStorageServer.DnsName);
            _logger.Info($"{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Сессии завершены");
        }

        /// <summary>
        /// Записать ошибку в лог.
        /// </summary>
        /// <param name="message">Сообщение об ошибке.</param>
        protected void ErrorLog(Exception ex, string message)
        {
            _logger.Warn(ex,$"{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}. Описание ошибки: \"{message}\"");
        }

        /// <summary>
        /// Проверить информационную базу на наличие открытых соединений.
        /// </summary>        
        protected bool AccountDatabaseUsed()
        {
            if(AccountDatabaseSupportModel.AccountDatabase.IsFile == true)
            {
                if (AccountDatabaseFileUseChecker.Check(AccountDatabaseSupportModel.AccountDatabasePath))
                    CloseSession();
                return AccountDatabaseFileUseChecker.Check(AccountDatabaseSupportModel.AccountDatabasePath);
            }
            return  Cluster1CProvider.AnyDbSessions(AccountDatabaseSupportModel);
        }

        /// <summary>
        /// Попытаться перенести бэкап в склеп.
        /// </summary>
        protected void MoveBackupToTombIfExist()
        {            
            if (!AccountDatabaseBackupId.HasValue)
                return;

            _tombTaskCreatorManager
                .CreateTaskForSendBackupAccountDatabaseToTomb(new SendBackupAccountDatabaseToTombJobParamsDto
                {
                    AccountDatabaseBackupId = AccountDatabaseBackupId.Value,
                    AccountDatabaseId = AccountDatabasesID,
                    ForceUpload = true
                });
        }

        /// <summary>
        /// Восстановить информационную базу из бекапа.
        /// </summary>
        protected void RestoreBackupAccountDatabaseIfExist()
        {
            if (!AccountDatabaseBackupId.HasValue)
            {
                TraceLog("Нет бэкапа для восстановления информационной базы.");
                return;
            }
            try
            {
                
                var accountDatabaseBackup =
                    _unitOfWork.AccountDatabaseBackupRepository.FirstOrDefault(b => b.Id == AccountDatabaseBackupId.Value);

                if (accountDatabaseBackup == null)
                    throw new NotFoundException("Код ошибки 040920181155.Ошибка восстановления базы из бекапа, отсутствует запись о бэкапе");

                
                var restoreResult = _accountDatabaseAfterFailedAutoUpdateJobWrapper.Start(new RestoreAccountDatabaseFromTombWorkerTaskParam
                {
                    AccountDatabaseBackupId = accountDatabaseBackup.Id,
                    RestoreType = AccountDatabaseBackupRestoreType.ToCurrent
                }).WaitResult();

                if (!restoreResult.Success)
                    throw new InvalidOperationException(restoreResult.Message);
            }
            catch (Exception ex)
            {
                var message = $"Внимание! Не удалось восстановить информационную базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(Caption, V82Name)}, {ex.Message}";
                _mailNotificationFactory.SendMail<MailNotifyRestoreBackupAccountDbFromTombAfterTS, string>(message);
                ErrorLog(ex, $"[Ошибка восстановления ИБ после АО] {message}");
            }
        }

        /// <summary>
        /// Установить статус информационной базе
        /// </summary>
        /// <param name="state">статус</param>
        protected void AccountDatabaseSetState(DatabaseState state)
        {
            var accountDatabase = _unitOfWork.DatabasesRepository.FirstOrDefault(s =>
                    s.Id == AccountDatabaseSupportModel.AccountDatabase.Id);
            _accountDatabaseChangeStateProvider.ChangeState(accountDatabase, state, null);
        }

        /// <summary>
        /// Зарегистрировать состояние процесса ТиИ.
        /// </summary>
        /// <param name="state">Состояние.</param>
        /// <param name="message">Сообщение.</param>
        /// <param name="description">Детальная информация.</param>
        /// <param name="debugInformation">Информация для отладки</param>
        protected void RegisterState(AccountDatabaseSupportProcessorState state, string message, string description = null, string debugInformation = null)
        {
            lock (_thisLock)
            {
                State = state;
                Message = message;
                Description = description;
                DebugInformation = debugInformation;

                TraceLog(message);

                if (state == AccountDatabaseSupportProcessorState.FatalError)
                {
                    NotifySupportFatalError();
                }
            }
        }

        /// <summary>
        /// Создать резервную копию информационной базы.
        /// </summary>
        protected bool CreateBackupAccountDatabase()
        {
           
            try
            {
                AccountDatabaseBackupId = _accountDatabaseLocalBackupProvider
                    .CreateBackupAccountDatabase(AccountDatabaseSupportModel.AccountDatabase.Id, BackUptrigger,
                        AccessProvider.GetUser().Id);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Warn(ex, "[Ошибка создания резервной копии ИБ]");
                return false;
            }

        }

        /// <summary>
        /// Копировать базу в вреенное хранилище обновления.
        /// </summary>
        protected void CopyDatabaseToUpdateStorage(Func<bool> needCopyConditionFunc)
        {
            if (!needCopyConditionFunc())
                return;

            AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(AccountDatabaseSupportModel, AccountDatabaseSupportModel.UpdateDatabaseName, CloudConfigurationProvider.Enterprise1C.GetDnsTemporaryStorage());

            var accountDatabaseCopyPath = Path.Combine(CloudConfigurationProvider.Enterprise1C.GetTemporaryStoragePath(), "Updates", $"{AccountDatabaseSupportModel.UpdateDatabaseName}_{DateTime.Now:yy-mm-dd_hh-mm-ss}");

            if (!Directory.Exists(accountDatabaseCopyPath))
                Directory.CreateDirectory(accountDatabaseCopyPath);

            DirectoryHelper.DirectoryCopy(AccountDatabaseSupportModel.AccountDatabasePath, accountDatabaseCopyPath, true);

            AccountDatabaseSupportModel.AccountDatabasePath = accountDatabaseCopyPath;

            TraceLog($"Файловая база скопирована '{accountDatabaseCopyPath}'");

        }

        /// <summary>
        /// Перенести обновленую файловую базу на облачное хранилище.
        /// </summary>
        protected bool TryMoveDbToCloudStorage(Func<bool> copyConditionFunc, out string errorMessage)
        {

            errorMessage = null;
            if (!copyConditionFunc())
            {
                return true;
            }

            try
            {
                TraceLog("Начинаем ожидать пока база данных используется.");
                var cloudServiceFileStorageServer =
              _unitOfWork.CloudServicesFileStorageServerRepository.FirstOrDefault(w => w.ID == AccountDatabaseSupportModel.AccountDatabase.FileStorageID);
                AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(AccountDatabaseSupportModel, AccountDatabaseSupportModel.UpdateDatabaseName, cloudServiceFileStorageServer.DnsName);

                var accountDatabasePath = _accountDatabasePathHelper.GetPath(
                        AccountDatabaseSupportModel.AccountDatabase.Id);

                TraceLog($"Начинаем удалять директорию {accountDatabasePath}.");

                DirectoryHelper.DeleteDirectory(accountDatabasePath, onlyDirectoryContent: true);

                TraceLog($"Начинаем копировать директорию {AccountDatabaseSupportModel.AccountDatabasePath}.");

                DirectoryHelper.DirectoryCopy(AccountDatabaseSupportModel.AccountDatabasePath, accountDatabasePath, true);

                TraceLog($"Обновленная база скопирована c '{AccountDatabaseSupportModel.AccountDatabasePath}' в облачное хранилище '{accountDatabasePath}'");

                _removeDirectoryJobWrapper.Start(new RemoveDirectoryJobParams
                {
                    DerectoryPath = AccountDatabaseSupportModel.AccountDatabasePath
                });

                return true;
            }

            catch (Exception ex)
            {
                errorMessage = $"[Ошибка копирования базы в хранилище] '{ex.Message}'";
                ErrorLog(ex,errorMessage);
                return false;
            }

        }

        /// <summary>
        /// Обновить версию конфигурации инф. базы
        /// </summary>
        /// <param name="acDbConfigVersionFromMetadata">Версия конфигурации из метаданных</param>
        protected void UpdateAcDbConfigurationVersion(string acDbConfigVersionFromMetadata)
        {
            if (AccountDatabaseSupportModel.Support.CurrentVersion.Equals(acDbConfigVersionFromMetadata,
                StringComparison.InvariantCultureIgnoreCase))
                return;
            
            var accountDatabaseSupport =
                _unitOfWork.AcDbSupportRepository.FirstOrDefault(s =>
                    s.AccountDatabasesID == AccountDatabaseSupportModel.Support.AccountDatabasesID);

            accountDatabaseSupport.CurrentVersion = acDbConfigVersionFromMetadata;
            _unitOfWork.AcDbSupportRepository.Update(accountDatabaseSupport);
            _unitOfWork.Save();
        }

        /// <summary>
        /// Зарегистрировать не валидный логин/пароль администратора базы.
        /// </summary>
        private void RegisterInvalidLoginPassword()
        {
            RegisterState(AccountDatabaseSupportProcessorState.FatalError, GetInvalidLoginOrPasswordMessage());

            var accountDatabaseSupport =
                _unitOfWork.AcDbSupportRepository.FirstOrDefault(s =>
                    s.AccountDatabasesID == AccountDatabaseSupportModel.Support.AccountDatabasesID);

            accountDatabaseSupport.HasSupport = false;
            accountDatabaseSupport.HasAutoUpdate = false;
            accountDatabaseSupport.State = (int)SupportState.NotAutorized;
            _unitOfWork.AcDbSupportRepository.Update(accountDatabaseSupport);
            _unitOfWork.Save();
        }

        /// <summary>
        /// Получить сообщение о не успешной авторизации в информационной базе
        /// </summary>
        /// <returns>Сообщение о не успешной авторизации в информационной базе</returns>
        private string GetInvalidLoginOrPasswordMessage() =>
            _acDbSupportHelper.GetInvalidAuthorizationMessage(AccountDatabaseSupportModel.Support.AccountDatabasesID);

        /// <summary>
        /// Проверить что аккаунт является VIP
        /// </summary>
        /// <returns>Признак указывающий что аккаунт VIP</returns>
        private bool IsVipAccount() => _unitOfWork.GetGenericRepository<AccountConfiguration>()
            .FirstOrDefault(accountConfig => accountConfig.AccountId == AccountId).IsVip;
    }
}
