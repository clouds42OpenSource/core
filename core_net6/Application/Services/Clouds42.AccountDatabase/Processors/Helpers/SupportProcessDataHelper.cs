﻿using Clouds42.AccountDatabase.Processors.SupportProcessors;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.AccountDatabase.Processors.Helpers
{
    /// <summary>
    /// Хэлпер для процессов поддержки инф. баз
    /// </summary>
    public static class SupportProcessDataHelper
    {
        /// <summary>
        /// Получить дату и время завершения процесса
        /// </summary>
        /// <returns>Дата и время завершения процесса</returns>
        public static DateTime GetStopProcessDateTime()
        {
            var timeOfLastUpdateOrTehSupportOperation = CloudConfigurationProvider.AccountDatabase.Support
                .GetTimeOfLastUpdateOrTehSupportOperation();
            var stopProcessDate = DateTime.Now.TimeOfDay > timeOfLastUpdateOrTehSupportOperation
                ? DateTime.Now.AddDays(1)
                : DateTime.Now;

            return new DateTime(stopProcessDate.Year, stopProcessDate.Month, stopProcessDate.Day,
                timeOfLastUpdateOrTehSupportOperation.Hours, timeOfLastUpdateOrTehSupportOperation.Minutes,
                timeOfLastUpdateOrTehSupportOperation.Seconds);
        }

        /// <summary>
        /// Создать результат автообновления
        /// </summary>
        /// <param name="processor">Процессор АО</param>
        /// <returns>Результат автообновления</returns>
        public static AccountDatabaseSupportResultDto CreateSupportResult(AutoUpdateSupportProcessor processor)
            => new AccountDatabaseAutoUpdateResultDto
            {
                SupportProcessorState = processor.State,
                Message = processor.Message,
                Description = processor.Description,
                AccountDatabasesId = processor.AccountDatabasesID,
                NewVersion = processor.NewVersion,
                OldVersion = processor.OldVersion,
                AccountId = processor.AccountId,
                Caption = processor.Caption,
                DebugInformation = processor.DebugInformation
            };

        /// <summary>
        /// Создать историческую запись для автообновления
        /// </summary>
        /// <param name="autoUpdateResult">Результат автообновления</param>
        /// <param name="isError">Признак что операция завершена успешно</param>
        /// <returns>Историческая запись для автообновления</returns>
        public static AcDbSupportHistory CreateAutoUpdateHistoryRecord(AccountDatabaseAutoUpdateResultDto autoUpdateResult, bool isError)
            => new()
            {
                ID = Guid.NewGuid(),
                AccountDatabaseId = autoUpdateResult.AccountDatabasesId,
                EditDate = DateTime.Now,
                Description = autoUpdateResult.Message ?? autoUpdateResult.SupportProcessorState.Description(),
                Operation = DatabaseSupportOperation.AutoUpdate,
                State = isError ? SupportHistoryState.Error : SupportHistoryState.Success,
                OldVersionCfu = autoUpdateResult.OldVersion,
                CurrentVersionCfu = autoUpdateResult.NewVersion
            };
    }
}
