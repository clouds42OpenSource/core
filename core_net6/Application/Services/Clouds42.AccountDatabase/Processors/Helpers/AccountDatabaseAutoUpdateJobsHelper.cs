﻿using Clouds42.CoreWorker.JobWrappers.AccountDatabaseSupport;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.CoreWorkerModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Processors.Helpers
{
    /// <summary>
    /// Хэлпер для работы с задачами автообновления инф. баз
    /// </summary>
    public class AccountDatabaseAutoUpdateJobsHelper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
    {
        /// <summary>
        /// Признак возможности продолжать операцию
        /// </summary>
        /// <returns>Признак возможности продолжать операцию</returns>
        public bool CanProcessTasks()
            => DateTime.Now < StopProcessDateTime;

        /// <summary>
        /// Создать обработчик задачи
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Обработчик задачи</returns>
        public AccountDatabaseAutoUpdateJobWrapper CreateJobWrapper(Guid accountDatabaseId)
            => new(registerTaskInQueueProvider, dbLayer)
            {
                AccountDatabaseId = accountDatabaseId
            };

        /// <summary>
        /// Создать справочник сопоставления статуса АО с действием завершения процесса АО
        /// </summary>
        /// <param name="finalizeProcessWithDecideStatusAction">Действие для завершения процесса с окончательным статусом</param>
        /// <param name="finalizeProcessAction">Действие для завершения процесса</param>
        /// <param name="finalizeErrorProcessAction">Действие для завершения процесса с ошибкой</param>
        /// <returns>Справочник сопоставления статуса АО с действием завершения процесса АО</returns>
        public Dictionary<AccountDatabaseSupportProcessorState, Action<AccountDatabaseAutoUpdateResultDto>> CreateMapProcessStateToFinalizeActionDictionary(
            Action<AccountDatabaseAutoUpdateResultDto> finalizeProcessWithDecideStatusAction,
            Action<AccountDatabaseAutoUpdateResultDto> finalizeProcessAction,
            Action<AccountDatabaseAutoUpdateResultDto> finalizeErrorProcessAction)
        {
            return new Dictionary<AccountDatabaseSupportProcessorState, Action<AccountDatabaseAutoUpdateResultDto>>
            {
                { AccountDatabaseSupportProcessorState.Success, finalizeProcessWithDecideStatusAction },
                { AccountDatabaseSupportProcessorState.FatalError, finalizeProcessWithDecideStatusAction },
                { AccountDatabaseSupportProcessorState.FatalValidationError, finalizeProcessWithDecideStatusAction },
                { AccountDatabaseSupportProcessorState.DbHasModifications, finalizeProcessWithDecideStatusAction },
                { AccountDatabaseSupportProcessorState.Skip, finalizeProcessAction },
                { AccountDatabaseSupportProcessorState.HasActiveSessions, finalizeProcessAction },
                { AccountDatabaseSupportProcessorState.Error, finalizeErrorProcessAction }
            };
        }

        /// <summary>
        /// Зарегистрировать служебный результат автообновления 
        /// </summary>
        /// <param name="result">Результат автообновления</param>
        /// <param name="autoUpdateTaskQueueId">ID задачи автообновления</param>
        public void RegisterAutoUpdateTechnicalResult(AccountDatabaseAutoUpdateResultDto result, Guid autoUpdateTaskQueueId)
        {
            try
            {
                var taskQueueItem =
                    dbLayer.CoreWorkerTasksQueueRepository.FirstOrThrowException(
                        queue => queue.Id == autoUpdateTaskQueueId);

                var autoUpdateResult =
                    CreateAccountDatabaseAutoUpdateResult(result, taskQueueItem);

                dbLayer.GetGenericRepository<AccountDatabaseAutoUpdateResult>().Insert(autoUpdateResult);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"При сохранении служебного результата автообновлениея произошла ошибка:: {ex.Message}");
            }
        }

        /// <summary>
        /// Время до которого разрешено проводить операции по обслуживанию.
        /// </summary>
        private DateTime? _stopProcessDateTime;

        /// <summary>
        /// Время до которого разрешено проводить операции по обслуживанию.
        /// </summary>
        private DateTime StopProcessDateTime
        {
            get
            {
                if (_stopProcessDateTime.HasValue)
                    return _stopProcessDateTime.Value;

                _stopProcessDateTime = SupportProcessDataHelper.GetStopProcessDateTime();
                return _stopProcessDateTime.Value;
            }
        }

        /// <summary>
        /// Создать результат АО для инф. базы
        /// </summary>
        /// <param name="result">Результат автообновления</param>
        /// <param name="taskQueueItem">Задача из очереди воркера</param>
        /// <returns>Результат АО для инф. базы</returns>
        private static AccountDatabaseAutoUpdateResult CreateAccountDatabaseAutoUpdateResult(
            AccountDatabaseAutoUpdateResultDto result,
            CoreWorkerTasksQueue taskQueueItem)
        {
            if (taskQueueItem.CapturedWorkerId == null)
                throw new InvalidOperationException(
                    $"Не верно заполнена модель задачи {taskQueueItem.Id} в очереди");

            return new AccountDatabaseAutoUpdateResult
            {
                Id = Guid.NewGuid(),
                DebugInformation = result.GetDebugInformation(),
                AccountDatabaseId = result.AccountDatabasesId,
                CoreWorkerTasksQueueId = taskQueueItem.Id
            };
        }
    }
}
