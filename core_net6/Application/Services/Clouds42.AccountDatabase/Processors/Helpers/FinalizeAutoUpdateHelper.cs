﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Processors.Helpers
{
    /// <summary>
    /// Хэлпер для завршения процесса АО
    /// </summary>
    public class FinalizeAutoUpdateHelper(
        IUnitOfWork unitOfWork,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        ILogger42 logger,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ICloudChangesProvider cloudChangesProvider)
    {
        /// <summary>
        /// Уведомить о проведние автообновления.
        /// </summary>
        /// <param name="autoUpdateResultList">Результаты обновления инф. баз</param>
        public void NotifyAutoUpdateCompleteOperation(List<AccountDatabaseAutoUpdateResultDto> autoUpdateResultList)
        {
                foreach (var supportGroup in autoUpdateResultList.GroupBy(g => g.AccountId))
                {
                    var list = supportGroup.Where(l =>
                        l.SupportProcessorState == AccountDatabaseSupportProcessorState.DbHasModifications ||
                        l.SupportProcessorState == AccountDatabaseSupportProcessorState.Success ||
                        l.SupportProcessorState == AccountDatabaseSupportProcessorState.Updated ||
                        l.SupportProcessorState == AccountDatabaseSupportProcessorState.Skip).ToList();

                    if (!list.Any())
                        continue;

                    var siteAuthorityUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(supportGroup.Key);

                letterNotificationProcessor
                    .TryNotify<AutoUpdateResultLetterNotification, AutoUpdateResultLetterModelDto>(
                            GetAutoUpdateResultLetterModel(supportGroup.Key, list, siteAuthorityUrl));
                }
        }

        /// <summary>
        /// Завершить обработку процесса АО.
        /// </summary>
        /// <param name="autoUpdateResult">Результат обновления</param>
        public void FinalizeAccountDatabaseAutoUpdateProcess(AccountDatabaseAutoUpdateResultDto autoUpdateResult)
        {
            using (var dbScope = unitOfWork.SmartTransaction.Get())
            {
                try
                {
                    var accountDatabaseSupport =
                        unitOfWork.AcDbSupportRepository.FirstOrDefault(s =>
                            s.AccountDatabasesID == autoUpdateResult.AccountDatabasesId);

                    accountDatabaseChangeStateProvider.ChangeState(accountDatabaseSupport.AccountDatabasesID, DatabaseState.Ready);

                    UpdateAcDbSupport(accountDatabaseSupport, autoUpdateResult);

                    unitOfWork.AcDbSupportRepository.Update(accountDatabaseSupport);
                    unitOfWork.Save();
                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    logger.Warn(ex,
                        $"[Ошибка завершения обработки АО] {autoUpdateResult.AccountDatabasesId}");
                    dbScope.Rollback();
                }
            }

            RegisterSupportHistory(autoUpdateResult);
        }

        /// <summary>
        /// Получить модель письма о результате проведения АО      
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="resultsOfAccount">Результаты операций по базам аккаунта.</param>
        /// <param name="siteAuthorityUrl">Урл сайта ЛК</param>
        /// <returns>Модель письма о результате проведения АО</returns>
        private static AutoUpdateResultLetterModelDto GetAutoUpdateResultLetterModel(Guid accountId,
            List<AccountDatabaseAutoUpdateResultDto> resultsOfAccount, string siteAuthorityUrl)
        {
            return new AutoUpdateResultLetterModelDto
            {
                AccountId = accountId,
                UrlForAccountDatabasesPage =
                    new Uri(
                            $"{siteAuthorityUrl}/{CloudConfigurationProvider.Cp.GetRouteForOpenAccountDatabasesPage()}")
                        .AbsoluteUri,
                AutoUpdateFailureDatabases = resultsOfAccount
                    .Where(result =>
                        result.SupportProcessorState == AccountDatabaseSupportProcessorState.Error ||
                        result.SupportProcessorState == AccountDatabaseSupportProcessorState.FatalError).Select(
                        result => new AutoUpdateDatabaseFailureDto
                        {
                            AccountDatabaseCaption = result.Caption,
                            Message = result.Message,
                            Description = result.Description.LineEndingToHtmlStyle()
                        }),
                AutoUpdateSuccessesDatabases = resultsOfAccount
                    .Where(result =>
                        result.SupportProcessorState != AccountDatabaseSupportProcessorState.Error &&
                        result.SupportProcessorState != AccountDatabaseSupportProcessorState.FatalError).Select(
                        result => new AutoUpdateDatabaseSuccessesDto
                        {
                            AccountDatabaseCaption = result.Caption, 
                            NewVersion = result.NewVersion
                        })
            };
        }

        /// <summary>
        /// Обновить модель поддержки инф. базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <param name="autoUpdateResult">Результат АО</param>
        private static void UpdateAcDbSupport(AcDbSupport acDbSupport,
            AccountDatabaseAutoUpdateResultDto autoUpdateResult)
        {
            acDbSupport.PrepearedForUpdate = false;

            if (autoUpdateResult.SupportProcessorState == AccountDatabaseSupportProcessorState.Success)
                acDbSupport.CurrentVersion = autoUpdateResult.NewVersion;

            if (autoUpdateResult.SupportProcessorState != AccountDatabaseSupportProcessorState.DbHasModifications)
                return;

            acDbSupport.HasAutoUpdate = false;
            acDbSupport.DatabaseHasModifications = true;
        }

        /// <summary>
        /// Зарегистрировать результат выполнения обслуживания
        /// </summary>
        /// <param name="databaseSupportResult">Результат обслуживания</param>
        private void RegisterSupportHistory(AccountDatabaseAutoUpdateResultDto databaseSupportResult)
        {
            using var transaction = unitOfWork.SmartTransaction.Get();
            try
            {
                var history = CreateSupportHistory(databaseSupportResult);
                unitOfWork.AcDbSupportHistoryRepository.Insert(history);
                unitOfWork.Save();

                cloudChangesProvider.LogEvent(databaseSupportResult.AccountId,
                    history.State == SupportHistoryState.Error
                        ? LogActions.ErrorAutoUpdate
                        : LogActions.SuccessAutoUpdate,
                    databaseSupportResult.Message);

                RegisterAcDbSupportHistoryTasksQueueRelation(unitOfWork, databaseSupportResult.CoreWorkerTaskQueueId,
                    history.ID);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                logger.Warn(ex,
                    $"[Ошибка регистрации результатов АО] {databaseSupportResult.AccountDatabasesId}");
                transaction.Rollback();
            }
        }

        /// <summary>
        /// Зарегистрировать связь исторической записи АО и задачи,которая обрабатывала АО
        /// </summary>
        /// <param name="dbLayer">Экземпляр IUnitOfWork</param>
        /// <param name="coreWorkerTaskQueueId">ID задачи,которая обрабатывала АО</param>
        /// <param name="supportHistoryId">ID исторической записи АО</param>
        private static void RegisterAcDbSupportHistoryTasksQueueRelation(IUnitOfWork dbLayer,
            Guid coreWorkerTaskQueueId, Guid supportHistoryId)
        {
            var acDbSupportHistoryTasksQueueRelation = new AcDbSupportHistoryTasksQueueRelation
            {
                AcDbSupportHistoryId = supportHistoryId,
                TasksQueueId = coreWorkerTaskQueueId
            };

            dbLayer.GetGenericRepository<AcDbSupportHistoryTasksQueueRelation>()
                .Insert(acDbSupportHistoryTasksQueueRelation);
            dbLayer.Save();
        }

        /// <summary>
        /// Создать историческую запись по результате операции АО.
        /// </summary>
        /// <param name="databaseSupportResult">Результат обслуживания</param>
        private AcDbSupportHistory CreateSupportHistory(AccountDatabaseAutoUpdateResultDto databaseSupportResult)
        {
            var errorOperation =
                databaseSupportResult.SupportProcessorState == AccountDatabaseSupportProcessorState.Error ||
                databaseSupportResult.SupportProcessorState == AccountDatabaseSupportProcessorState.FatalError ||
                databaseSupportResult.SupportProcessorState ==
                AccountDatabaseSupportProcessorState.FatalValidationError ||
                databaseSupportResult.SupportProcessorState == AccountDatabaseSupportProcessorState.DbHasModifications;

            return SupportProcessDataHelper.CreateAutoUpdateHistoryRecord(databaseSupportResult, errorOperation);
        }
    }
}
