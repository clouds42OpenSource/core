﻿using Clouds42.AccountDatabase.CfuPackages.Helpers;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.CfuPackages.Managers
{
    /// <summary>
    /// Менеджер пакетов CFU
    /// </summary>
    public class CfuManager(
        IAccessProvider accessProvider,
        IUnitOfWork uow,
        DownloadCfuProcess downloadCfuProcess,
        IHandlerException handlerException)
        : BaseManager(accessProvider, uow, handlerException), ICfuManager
    {
        private readonly IUnitOfWork _uow = uow;

        /// <summary>
        /// Скачать доступные Cfu
        /// </summary>
        public void DownloadAvailableCfus()
        {
            AccessProvider.HasAccess(ObjectAction.Cfu_DownloadAvailable, () => AccessProvider.ContextAccountId);

            var configurations = _uow.Configurations1CRepository.Where().ToList();

            foreach (var configurations1C in configurations)
            {
                downloadCfuProcess.DownloadCfus(configurations1C);
            }
        }
    }
}
