﻿using System.IO.Compression;
using System.Net.Http.Headers;
using System.Text;
using Clouds42.Logger;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace Clouds42.AccountDatabase.CfuPackages.Helpers
{
    /// <summary>
    /// Класс операций с файлами
    /// </summary>
    public static class FileOperation
    {
        private static readonly ILogger42 Logger = Logger42.GetLogger();

        /// <summary>
        /// Скачать файл
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <param name="platform">Платформа</param>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        /// <param name="fileName">Название файла</param>
        /// <returns>Название файла</returns>
        public static string DownloadFile(string path, string platform, string login, string password, string fileName = null)
        {
            if (string.IsNullOrEmpty(fileName))
                fileName = Path.GetTempFileName();

            var fi = Path.GetDirectoryName(fileName);
            if (!Directory.Exists(fi))
                Directory.CreateDirectory(fi!);

            using var client = new HttpClient();
            client.DefaultRequestHeaders.UserAgent.ParseAdd($"1C+Enterprise/{platform}");

            var authToken = System.Convert.ToBase64String(Encoding.ASCII.GetBytes($"{login}:{password}"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authToken);

            using var response = client.GetAsync(path, HttpCompletionOption.ResponseHeadersRead).Result;
            response.EnsureSuccessStatusCode();
            using Stream contentStream = response.Content.ReadAsStreamAsync().Result;
            using var  fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None, 8192, true);
            contentStream.CopyToAsync(fileStream).GetAwaiter().GetResult();
            
            Logger.Trace($"Скачан пакет обновлений {path}");

            return fileName;
        }

        /// <summary>
        /// Открыть ZIP файл
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <returns>Содержимое файла</returns>
        public static string OpenZipFile(string path)
        {
            var result = string.Empty;

            using var file = File.OpenRead(path);
            using var zipArchive = new ZipArchive(file, ZipArchiveMode.Read);
            foreach (var zipEntry in zipArchive.Entries)
            {
                result = GetZipEntryContent(zipEntry);
            }

            return result;
        }

        /// <summary>
        /// Распаковать файл во временную директорию
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <param name="newPath">Новый путь</param>
        /// <param name="fileName">Название файла</param>
        public static void UnzipFileToTmp(string path, string newPath, string fileName)
        {
            ICSharpCode.SharpZipLib.Zip.ZipFile? zip = null;
            try
            {
                var file = File.OpenRead(path);
                zip = new ICSharpCode.SharpZipLib.Zip.ZipFile(file);

                foreach (ZipEntry entry in zip)
                {
                    if (!entry.Name.Equals(fileName, StringComparison.InvariantCultureIgnoreCase))
                        continue;

                    var buffer = new byte[4096];
                    var zipStream = zip.GetInputStream(entry);

                    var directoryName = Path.GetDirectoryName(newPath);
                    if (!Directory.Exists(directoryName))
                    {
                        Directory.CreateDirectory(directoryName);
                    }

                    using var streamWriter = File.Create(newPath);
                    StreamUtils.Copy(zipStream, streamWriter, buffer);
                }
            }
            finally
            {
                if (zip != null)
                {
                    zip.IsStreamOwner = true;
                    zip.Close();
                }
            }
        }

        /// <summary>
        /// Получит содержимое вхождения ZIP архива
        /// </summary>
        /// <param name="zipEntry">Вхождение ZIP архива</param>
        /// <returns>Содержимое вхождения ZIP архива</returns>
        private static string GetZipEntryContent(ZipArchiveEntry zipEntry)
        {
            Stream stream = null;
            try
            {
                stream = zipEntry.Open();
                using TextReader textReader = new StreamReader(stream);
                stream = null;
                return textReader.ReadToEnd();
            }
            finally
            {
                stream?.Dispose();
            }
        }
    }
}
