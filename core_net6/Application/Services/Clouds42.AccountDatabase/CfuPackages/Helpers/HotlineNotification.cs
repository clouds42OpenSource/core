﻿using System.Text;
using Clouds42.Configurations.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;

namespace Clouds42.AccountDatabase.CfuPackages.Helpers
{
    /// <summary>
    /// Хэлпер для уведомления хотлайна
    /// </summary>
    public static class HotlineNotification
    {
        public static List<string> NotificationList { get; } = [];

        /// <summary>
        /// Отправить уведомление
        /// </summary>
        public static void Send()
        {
            if(!NotificationList.Any())
                return;
            var sb = new StringBuilder();
            foreach (var element in NotificationList)
            {
                sb.AppendLine(element + "</br>");
            }
            NotificationList.Clear();
            var hotlineMail = CloudConfigurationProvider.Emails.GetHotlineEmail();
            
            new Support42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(hotlineMail)
                .Subject("Загружены обновления конфигураций баз")
                .Body(sb.ToString())
                .SendViaNewThread();
        }

    }

}
