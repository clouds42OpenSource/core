﻿using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Configurations;
using Clouds42.DataContracts.Configurations1c.CfuPackage;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.CfuPackages.Helpers
{
    /// <summary>
    /// Хэлпер для скачивания CFU пакетов
    /// </summary>
    public class DownloadCfuProcess(
        IUnitOfWork uow,
        ICfuProvider cfuProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : IDownloadCfuProcess
    {
        private readonly Lazy<string> _user1CLogin = new(()=>ConfigurationHelper.GetConfigurationValue("CfuUpdateUser1CLogin"));
        private readonly Lazy<string> _user1CPassword = new(()=>ConfigurationHelper.GetConfigurationValue("CfuUpdateUser1CPassword"));

        /// <summary>
        /// Скачать пакеты новые доступные пакеты обновлений.
        /// </summary>
        /// <param name="configurations1C">Конфигурация.</param>
        public void DownloadCfus(Configurations1C configurations1C)
        {
            try
            {
                DownloadCfuMapping(configurations1C);
                DownloadCfuPackages(configurations1C);
            }
            catch (Exception ex)
            {
                var message = $"Произошла ошибка при скачивании обновления для конфигурации {configurations1C.Name}. Ошибка - {ex.Message}";
                handlerException.Handle(ex, $"[Ошибка скачивания обновления для конфигурации] {message}");
                HotlineNotification.NotificationList.Add(message);
            }
        }

        /// <summary>
        /// Получить новые версии CFU пакетов для конфигурации
        /// </summary>
        /// <param name="configurations1C">Конфигурация 1С</param>
        /// <returns>Новые версии CFU пакетов для конфигурации</returns>
        public List<CfuConfigurations> GetNewCfuReleasesForConfiguration(Configurations1C configurations1C)
        {
            try
            {
                var listXmlFile = LoadListXmlFile(configurations1C).List;
                logger.Info($"Успешно скачан xml файл с инфой для конфы {configurations1C.Name} ");

                var cfuConfigurations = listXmlFile.OrderBy(x => x.Version, new VersionComparer()).ToList();
                var lastDownloadedCfu = GetLastDownloadedCfu(configurations1C);

                var newCfuConfigurations = new List<CfuConfigurations>();

                cfuConfigurations.ForEach(cfuConfiguration =>
                {
                    var versionComparer = new VersionComparer();
                    if (lastDownloadedCfu != null && versionComparer.Compare(cfuConfiguration.Version, lastDownloadedCfu.Version) < 1)
                        return;

                    newCfuConfigurations.Add(cfuConfiguration);
                });

                return newCfuConfigurations;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка скачивания карты обновлений для конфигурации]. Конфигурация: {configurations1C.Name}.");
                throw;
            }
        }

        /// <summary>
        /// Скачать карту обновлений конфигурации в базу.
        /// </summary>
        /// <param name="configurations1C">Конфигурация.</param>
        private void DownloadCfuMapping(Configurations1C configurations1C)
        {
            try
            {
                var listXmlFile = LoadListXmlFile(configurations1C).List;
                logger.Info($"Успешно скачан xml файл с инфой для конфы {configurations1C.Name} ");

                var cfuConfigurations = listXmlFile.OrderBy(x => x.Version, new VersionComparer()).ToList();

                var lastDownloadedCfu = GetLastDownloadedCfu(configurations1C);

                cfuConfigurations.ForEach(cfuConfiguration =>
                {
                    var versionComparer = new VersionComparer();
                    if (lastDownloadedCfu != null && versionComparer.Compare(cfuConfiguration.Version, lastDownloadedCfu.Version) < 1)
                        return;

                    SaveResultToDb(cfuConfiguration, configurations1C);
                    logger.Info($"Успешно скачена карта обновлений в БД cfu {configurations1C.Name} {cfuConfiguration.Version}");
                });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при скачивании карты обновлений для конфигурации] {configurations1C.Name}.");
                throw;
            }
        }

        /// <summary>
        /// Сохранить результат в базу
        /// </summary>
        /// <param name="actualVersion">Актуальная версия конфигурации</param>
        /// <param name="configurations1C">Конфигурация 1С</param>
        private void SaveResultToDb(CfuConfigurations actualVersion, Configurations1C configurations1C)
        {
            using var dbScope = uow.SmartTransaction.Get();
            try
            {
                var cfuId = Guid.NewGuid();
                uow.ConfigurationsCfuRepository.Insert(CreateConfigurationsCfuObject(configurations1C, cfuId, actualVersion));
                uow.Save();

                SaveUpdateVersionMapping(configurations1C, cfuId, actualVersion);

                uow.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка сохранения актуальной версии конфигурации в базу]");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Загрузить список версий из файла конфигурации
        /// </summary>
        /// <param name="configurations1C">Конфигурация 1С</param>
        /// <returns>Список версий из файла конфигурации</returns>
        private Configurations1CList LoadListXmlFile(Configurations1C configurations1C)
        {
            var urls = cfuProvider.GetConfigurationRedactionInfo(configurations1C);
            var result = new Configurations1CList();
            foreach (var url in urls)
            {
                var zipFilePath = FileOperation.DownloadFile(url.UrlOfMapping, configurations1C.PlatformCatalog, _user1CLogin.Value, _user1CPassword.Value);
                var xml = FileOperation.OpenZipFile(zipFilePath);
                File.Delete(zipFilePath);
                var mappingRedaction = xml.Deserialize<Configurations1CList>();

                result.List.AddRange(mappingRedaction.List);
            }

            return result;
        }

        /// <summary>
        /// Скачать доступные версии пакетов CFU
        /// </summary>
        /// <param name="configurations1C">Конфигурация 1С</param>
        private void DownloadCfuPackages(IConfigurations1C configurations1C)
        {
            var configurationCfuPackages = GetConfigurationCfuPackages(configurations1C);

            configurationCfuPackages.ForEach(cfuPackage =>
            {
                try
                {
                    if (cfuProvider.CfuExist(cfuPackage, configurations1C))
                        return;

                    cfuProvider.DownloadCfu(cfuPackage, configurations1C);
                    HotlineNotification.NotificationList.Add($"Успешно загружено обновление {configurations1C.Name} {cfuPackage.Version}");
                }
                catch (Exception ex)
                {
                    var errorMessage =
                        $"При скачивании пакета {cfuPackage.ConfigurationName} {cfuPackage.Version} произошла ошибка. Причина: {ex.GetFullInfo(false)}";

                    handlerException.Handle(ex, $"[Ошибка скачивания пакета] {errorMessage}");
                    HotlineNotification.NotificationList.Add(errorMessage);
                }
            });
        }

        /// <summary>
        /// Получить список пакетов конфигурации 1С
        /// </summary>
        /// <param name="configurations1C">Конфигурация 1С</param>
        /// <returns>Список пакетов конфигурации 1С</returns>
        private List<ConfigurationsCfu> GetConfigurationCfuPackages(IConfigurations1C configurations1C)
            => uow.ConfigurationsCfuRepository.Where(c => c.ConfigurationName == configurations1C.Name).ToArray()
                .OrderBy(x => x.Version, new VersionComparer()).ToList();

        /// <summary>
        /// Получить последний скачанный пакет CFU
        /// </summary>
        /// <param name="configurations1C">Конфигурация 1С</param>
        /// <returns>Последний скачанный пакет CFU</returns>
        private ConfigurationsCfu GetLastDownloadedCfu(IConfigurations1C configurations1C)
            => uow.ConfigurationsCfuRepository.WhereLazy(c => c.ConfigurationName == configurations1C.Name).ToArray()
                .OrderBy(x => x.Version, new VersionComparer())
                .LastOrDefault();

        /// <summary>
        /// Создать объект пакета CFU
        /// </summary>
        /// <param name="configurations1C">Конфигурация 1С</param>
        /// <param name="cfuId">ID пакета</param>
        /// <param name="actualVersion">Актуальная версия пакета</param>
        /// <returns>Объект пакета CFU</returns>
        private ConfigurationsCfu CreateConfigurationsCfuObject(IConfigurations1C configurations1C, Guid cfuId, CfuConfigurations actualVersion)
            => new()
            {
                Id = cfuId,
                ConfigurationName = configurations1C.Name,
                DownloadDate = DateTime.Now,
                Version = actualVersion.Version,
                Platform1CVersion = actualVersion.VersionCfu.Platform1C,
                FilePath = actualVersion.FilePath,
                ValidateState = true
            };

        /// <summary>
        /// Сохранить карту обновления
        /// </summary>
        /// <param name="configurations1C">Конфигурация 1С</param>
        /// <param name="cfuId">ID пакета</param>
        /// <param name="actualVersion">Актуальная версия пакета</param>
        private void SaveUpdateVersionMapping(IConfigurations1C configurations1C, Guid cfuId, CfuConfigurations actualVersion)
        {
            actualVersion.TargetList.ForEach(targetVersion =>
            {
                uow.AccountDatabaseUpdateVersionMappingRepository.Insert(new AccountDatabaseUpdateVersionMapping
                {
                    CfuId = cfuId,
                    ConfigurationName = configurations1C.Name,
                    TargetVersion = targetVersion
                });
                uow.Save();
            });
        }
    }
}
