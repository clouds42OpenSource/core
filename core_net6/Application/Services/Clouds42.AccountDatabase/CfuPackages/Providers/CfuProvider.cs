﻿using Clouds42.AccountDatabase.CfuPackages.Helpers;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.AccountDatabase.Contracts.ITS.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.CfuPackages.Providers
{
    /// <summary>
    /// Провайдер пакетов обновления 1С.
    /// </summary>
    public class CfuProvider(IUnitOfWork dbLayer, IItsAuthorizationDataProvider itsAuthorizationDataProvider)
        : ICfuProvider
    {
        /// <summary>
        /// Получить путь до cfu пакета обновлений 1С.
        /// </summary>                
        public string GetCfuFullPath(string configurationCode, string release)
        {
            var configuration1C =
                dbLayer.Configurations1CRepository.FirstOrDefault(c => c.ShortCode == configurationCode);

            if (configuration1C == null)
                throw new NotFoundException($"Не найдена конфигурация по коду '{configurationCode}'");

            var cfu = dbLayer.ConfigurationsCfuRepository.FirstOrDefault(c =>
                c.Version == release && c.ConfigurationName == configuration1C.Name);


            if (cfu == null)
                throw new NotFoundException($"Не найдена запись пакет обновлений версии: '{release}' для конфигурации '{configuration1C.Name}' ");

            if (!CfuExist(cfu, configuration1C))
                throw new NotFoundException($"Пакет обновлений версии: '{release}' для конфигурации '{configuration1C.Name}' не найден. Требуется скачать с ИТС.");

            return GetCfuFullPath(cfu, configuration1C);
        }

        /// <summary>
        /// Скачать пакет обновления.
        /// </summary>        
        public void DownloadCfu(IConfigurationsCfu cfu)
        {
            var configuration =
                   dbLayer.Configurations1CRepository.FirstOrDefault(c => c.Name == cfu.ConfigurationName);

            if (string.IsNullOrEmpty(cfu.ConfigurationName) || configuration == null)
                throw new InvalidOperationException($"Не удалось получить данные о конфигурации '{cfu.ConfigurationName}' у пакета обновлений '{cfu.Id}'");

            DownloadCfu(cfu, configuration);
        }

        /// <summary>
        /// Скачать пакет обновления.
        /// </summary>        
        public void DownloadCfu(ICfuSource cfu, IConfigurations1C configuration)
        {
            if (CfuExist(cfu, configuration)) return;

            var downloadUrl = UriExtension.Combine(configuration.UpdateCatalogUrl, cfu.FilePath).AbsoluteUri;
            var itsAuthorizationData =
                itsAuthorizationDataProvider.GetItsAuthorizationDataDtoById(configuration
                    .ItsAuthorizationDataId);
            try
            {
                var updatePath = GetConfigurationCfuStoragePath(configuration.Name);

                if (!Directory.Exists(updatePath))
                    Directory.CreateDirectory(updatePath);

                var zipPath = FileOperation.DownloadFile(downloadUrl, configuration.PlatformCatalog,
                    itsAuthorizationData.Login, itsAuthorizationData.PasswordHash);
                FileOperation.UnzipFileToTmp(zipPath, GetCfuFullPath(cfu, configuration), "1cv8.cfu");
                File.Delete(zipPath);
            }
            catch (Exception ex)
            {
                var message = $"Произошла ошибка при скачивании файла cfu по ссылке {downloadUrl}. Причина: {ex.Message}";
                throw new CfuDamagedException(message);
            }
        }

        /// <summary>
        /// Пакет обновлений уже скачан.
        /// </summary>        
        public bool CfuExist(ICfuSource cfu, IConfigurations1C configuration)
            => File.Exists(GetCfuFullPath(cfu, configuration));

        /// <summary>
        /// Получить максимально возможную версию обновления пакета.
        /// </summary>
        /// <param name="configuration">Конфигурация инф. базы</param>
        /// <param name="version">Версия релиза</param>
        /// <param name="segmentPlatformVersion">Версия платформы в сегменте</param>
        /// <returns>Максимально возможная версия обновления пакета</returns>
        public IConfigurationsCfu? GetLastUpdateVersion(IConfigurations1C configuration, string version, string segmentPlatformVersion)
        {

            var nextVersion = version;
            IConfigurationsCfu? nextCfu, cfu = null;
            do
            {
                nextCfu = GetNextUpdateVersion(configuration, nextVersion, segmentPlatformVersion);
                if (nextCfu != null)
                {
                    nextVersion = nextCfu.Version;
                    cfu = nextCfu;
                }

            } while (nextCfu != null);

            if (nextVersion != version)
                return cfu;

            return null;
        }

        /// <summary>
        /// Получить следующую версию пакета обновления.
        /// </summary>
        /// <param name="configuration">Конфигурация инф. базы</param>
        /// <param name="version">Версия релиза</param>
        /// <param name="segmentPlatformVersion">Версия платформы в сегменте</param>
        /// <returns>Следующая версия пакета обновления</returns>
        public IConfigurationsCfu? GetNextUpdateVersion(IConfigurations1C configuration, string version, string segmentPlatformVersion)
        {
            if (configuration == null)
                return null;

            var query = (
                from mapping in dbLayer.AccountDatabaseUpdateVersionMappingRepository.WhereLazy()
                join cfu in dbLayer.ConfigurationsCfuRepository.WhereLazy(w => w.ValidateState == true) 
                    on mapping.CfuId equals cfu.Id
                    where mapping.ConfigurationName == configuration.Name && mapping.TargetVersion == version
                select cfu
            ).ToList();

            var availableCfuList = query.OrderBy(v => v.Version, new VersionComparer()).ToList();

            var maxAvailableCfuVersionForUpdate = 
                GetMaxAvailableCfuVersionForUpdate(availableCfuList, segmentPlatformVersion);

            if (maxAvailableCfuVersionForUpdate == null)
                return null;

            return !TryCompareVersionsCfu(version, maxAvailableCfuVersionForUpdate.Version) 
                ? null 
                : maxAvailableCfuVersionForUpdate;
        }

        /// <summary>
        /// Получить полный путь до пакета обновлений на диске.
        /// </summary>
        /// <param name="cfu">Пакет обновлений</param>                    
        /// <param name="configuration">Конфигурация 1С.</param>                    
        /// <returns>Полный путь до пакета обновлений.</returns>
        public string GetCfuFullPath(ICfuSource cfu, IConfigurations1C configuration)
            => Path.Combine(GetConfigurationCfuStoragePath(configuration.Name), cfu.Version + ".cfu");
        
        /// <summary>
        /// Получить полный путь до пакета обновлений на диске.
        /// </summary>
        /// <param name="cfu">Пакет обновлений</param>                            
        /// <returns>Полный путь до пакета обновлений.</returns>
        public string GetCfuFullPath(IConfigurationsCfu cfu)
            => Path.Combine(GetConfigurationCfuStoragePath(cfu.ConfigurationName), cfu.Version + ".cfu");

        /// <summary>
        /// Получить пути к архивам с информацией по обновлениям конфигурации всех редакций.
        /// </summary>
        /// <param name="configuration">Конфигурация 1С.</param>
        /// <returns>Список путей  по редакциям.</returns>
        public List<ConfigurationRedactionModelDto> GetConfigurationRedactionInfo(IConfigurations1C configuration)
        {
            var rootCatalog = CloudConfigurationProvider.Enterprise1C.GetCfuDownloadRootCatalogUrl();

            return configuration.RedactionCatalogs.Split(';').Select(redaction => new ConfigurationRedactionModelDto { RedactionCatalog = redaction, UrlOfMapping = $"{rootCatalog}/{configuration.ConfigurationCatalog}/{redaction.Replace(".", "")}/{configuration.PlatformCatalog.Replace(".", "")}/v8upd11.zip" }).ToList();
        }

        /// <summary>
        /// Попытаться сравнить 2 версии. 
        /// </summary>
        /// <param name="version1">Версия 1.</param>
        /// <param name="version2">Версия 2.</param>
        /// <returns>true - если версия 2 больше 1.</returns>
        private bool TryCompareVersionsCfu(string version1, string version2)
        {
            try
            {
                return new VersionComparer().Compare(version1, version2) == -1;
            }
            catch (Exception)
            {               
                return false;
            }
        }

        /// <summary>
        /// Сравнить версии платформы
        /// </summary>
        /// <param name="cfuVersion">Версия платформы в пакете CFU</param>
        /// <param name="currentVersion">Текущая версия платформы в сегменте</param>
        /// <returns>true - если версия в сегменте больше или равна версии в пакете cfu</returns>
        private bool TryComparePlatform1CVersion(string cfuVersion, string currentVersion)
        {
            try
            {
                return new VersionComparer().Compare(cfuVersion, currentVersion) <= 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Получить путь до хранилища пакетов обновлений конфигурации.
        /// </summary>        
        /// <param name="configurationName">Конфигурация 1С.</param>
        /// <returns>Полный путь до пакетов обновлений.</returns>
        private string GetConfigurationCfuStoragePath(string configurationName)
            => Path.Combine(CloudConfigurationProvider.Enterprise1C.GetCfuStoragePath(), configurationName.Replace(":", "_"));


        /// <summary>
        /// Получить максимально возможную версию релиза для обновления
        /// </summary>
        /// <param name="availableCfuList">Список доступных версий</param>
        /// <param name="segmentPlatformVersion">Версия платформы в сегменте</param>
        /// <returns>Максимально возможная версия релиза для обновления</returns>
        private IConfigurationsCfu? GetMaxAvailableCfuVersionForUpdate(List<ConfigurationsCfu> availableCfuList, string segmentPlatformVersion)
        {
            IConfigurationsCfu? maxAvailableCfuVersionForUpdate = null;

            foreach(ConfigurationsCfu cfu in availableCfuList)
            {
                if (!TryComparePlatform1CVersion(cfu.Platform1CVersion, segmentPlatformVersion))
                {
                    maxAvailableCfuVersionForUpdate = new ConfigurationsCfu 
                    {
                        ConfigurationName = cfu.ConfigurationName,
                        Version = cfu.Version,
                        Platform1CVersion = cfu.Platform1CVersion
                    };
                    continue;
                }
                    

                maxAvailableCfuVersionForUpdate = cfu;
            }

            return maxAvailableCfuVersionForUpdate;
        }
        
    }
}
