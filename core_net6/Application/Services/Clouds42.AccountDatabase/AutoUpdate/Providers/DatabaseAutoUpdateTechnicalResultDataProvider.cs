﻿using Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels.CoreWorkerModels;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;

namespace Clouds42.AccountDatabase.AutoUpdate.Providers
{
    /// <summary>
    /// Провайдер для работы с данными
    /// технического результата АО инф. базы
    /// </summary>
    internal class DatabaseAutoUpdateTechnicalResultDataProvider(IUnitOfWork dbLayer)
        : IDatabaseAutoUpdateTechnicalResultDataProvider
    {
        /// <summary>
        /// Получить данные технических результатов АО инф. базы 
        /// </summary>
        /// <returns>Список технических результатов АО инф. базы</returns>
        public PagedDto<DatabaseAutoUpdateTechnicalResultDataDto> GetData(GetTechnicalResultsQuery query)
        {
          return dbLayer
                .GetGenericRepository<AccountDatabaseAutoUpdateResult>()
                .AsQueryableNoTracking()
                .Select(x => new DatabaseAutoUpdateTechnicalResultDataDto
                {
                    AutoUpdateEndDate = x.CoreWorkerTasksQueue.EditDate,
                    AutoUpdateStartDate = x.CoreWorkerTasksQueue.StartDate,
                    DebugInformation = x.DebugInformation,
                    AccountDatabaseId = x.AccountDatabaseId,
                    CoreWorkerTasksQueueId = x.CoreWorkerTasksQueueId
                })
                .AutoFilter(query.Filter)
                .AutoSort(query)
                .ToPagedList(query.PageNumber ?? 1, query.PageSize ?? 50)
                .ToPagedDto();
        }
    }
}
