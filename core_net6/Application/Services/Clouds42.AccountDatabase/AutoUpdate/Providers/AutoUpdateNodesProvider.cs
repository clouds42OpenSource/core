﻿using Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.AutoUpdate.Providers
{
    public class AutoUpdateNodeProvider(IUnitOfWork unitOfWork) : IAutoUpdateNodeProvider
    {
        /// <inheritdoc />
        public async Task CreateAsync(AutoUpdateNode node)
        {
            unitOfWork.AutoUpdateNodeRepository.Insert(node);
            await unitOfWork.SaveAsync();
        }
        public void Create(AutoUpdateNode node)
        {
            unitOfWork.AutoUpdateNodeRepository.Insert(node);
            unitOfWork.Save();
        }

        /// <inheritdoc />
        public async Task<AutoUpdateNode> GetByIdAsync(Guid id)
        {
            return await unitOfWork.AutoUpdateNodeRepository.GetByIdAsync(id);
        }
        public AutoUpdateNode GetById(Guid id)
        {
            return unitOfWork.AutoUpdateNodeRepository.GetById(id);
        }

        /// <inheritdoc />
        public async Task<List<AutoUpdateNode>> GetAllAsync()
        {
            return await unitOfWork.AutoUpdateNodeRepository.AsQueryable().ToListAsync();
        }
        public List<AutoUpdateNode> GetAll()
        {
            return  unitOfWork.AutoUpdateNodeRepository.AsQueryable().ToList();
        }

        /// <inheritdoc />
        public async Task UpdateAsync(AutoUpdateNode autoUpdateNode)
        {

            unitOfWork.AutoUpdateNodeRepository.Update(autoUpdateNode);
            await unitOfWork.SaveAsync();
        }
        public void Update(AutoUpdateNode autoUpdateNode)
        {

            unitOfWork.AutoUpdateNodeRepository.Update(autoUpdateNode);
            unitOfWork.Save();
        }

        /// <inheritdoc />
        public async Task DeleteAsync(Guid id)
        {
            unitOfWork.AutoUpdateNodeRepository.Delete(id);
            await unitOfWork.SaveAsync();
        }
        public void Delete(Guid id)
        {
            unitOfWork.AutoUpdateNodeRepository.Delete(id);
            unitOfWork.Save();
        }
    }
}
