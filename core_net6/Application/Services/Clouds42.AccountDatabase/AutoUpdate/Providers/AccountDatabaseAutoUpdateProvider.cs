﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.AccountDatabase.Contracts.AutoUpdate.Models;
using Newtonsoft.Json;
using Clouds42.Logger;
using Clouds42.BLL.Common;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Processors.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.AutoUpdate.Enum;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.AutoUpdate.Providers
{
    /// <summary>
    /// Провайдер автообновления инф. баз
    /// </summary>
    internal class AccountDatabaseAutoUpdateProvider(
        IUnitOfWork dbLayer,
        IProcessAutoUpdateAccountDatabasesProvider autoUpdateAccountDatabasesProvider,
        ICloudChangesProvider cloudChangesProvider,
        IHandlerException handler,
        ILogger42 logger,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        FinalizeAutoUpdateHelper finalizeAutoUpdateHelper,
        IChangeDatabasesStateProvider changeDatabasesStateProvider,
        IAccessProvider accessProvider,
        IAccountDatabaseSupportProvider accountDatabaseSupportProvider)
        : IAccountDatabaseAutoUpdateProvider
    {
        readonly int[] TimeToClearObnovlyatorAndUpdateDBStatus = [5, 17, 20];

        readonly List<AccountDatabaseAutoUpdateResultDto> _supportResults = [];
        readonly Lazy<string> _cloudServicesEmail = new(CloudConfigurationProvider.Emails.GetCloudServicesEmail);
        readonly Lazy<string> _hotlineEmail = new(CloudConfigurationProvider.Emails.GetHotlineEmail);

        /// <summary>
        /// Выполнить автообновление
        /// </summary>
        /// <param name="accountDatabaseAutoUpdateParams">Параметры автообновления инф. базы</param>
        /// <returns>Результат выполнения</returns>
        public AccountDatabaseAutoUpdateResultDto ProcessAutoUpdate(AccountDatabaseAutoUpdateParamsDto accountDatabaseAutoUpdateParams)
        {
            var acDbSupport = GetAcDbSupport(accountDatabaseAutoUpdateParams.AccountDatabaseId);
            var result = autoUpdateAccountDatabasesProvider.ProcessAccountDatabase(acDbSupport);

            return result;
        }

        /// <summary>
        /// Получить модель поддержки инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Модель поддержки инф. базы</returns>
        private AcDbSupport GetAcDbSupport(Guid accountDatabaseId)
            => dbLayer.AcDbSupportRepository
                .FirstOrDefault(support => support.HasAutoUpdate && support.PrepearedForUpdate &&
                                           support.AccountDatabase.State == DatabaseState.Ready.ToString() &&
                                           support.AccountDatabasesID == accountDatabaseId);
        /// <summary>
        /// Обработать результаты проведения обслуживания
        /// </summary>
        public void HandleSupportResults()
        {
            var tiIDateList = CloudConfigurationProvider.AccountDatabase.Support.GetTiIDateList().Split(',').Select(int.Parse).ToArray();

            try
            {
                var nodes = dbLayer.AutoUpdateNodeRepository.GetAll().ToList();

                nodes.ForEach(node =>
                {
                    var dirInfo = new DirectoryInfo(node.AutoUpdateObnovlyatorPath + ObnovlyatorConsts.ReportFolderPath).GetDirectories().ToList() ??
                            throw new NullReferenceException($"На ноде {node.NodeAddress} Не найдено ни 1 отчёта");
                    logger.Trace($"На ноде {node.NodeAddress} найдено {dirInfo.Count} папок с отчетами");

                    dirInfo.ForEach(dir =>
                     {
                         if (dir.Name == ObnovlyatorConsts.ReportsCopyDirName || dir.Name == ObnovlyatorConsts.AllReportDirName 
                            || dir.Name.Contains(ObnovlyatorConsts.CleanTempFilesDirName))
                             return;
                         try
                         {
                             var reportObj = GetEncodeReport(dir);

                             if (tiIDateList.Contains(DateTime.Now.AddHours(-7).Day))
                                 HandleTestAndRepairResult(reportObj);
                             else
                                 HandleAutoUpdateResult(reportObj);

                             logger.Debug($"Закончил обрабатывать результаты обслуживания базы {reportObj.NameOfBase}");
                         }
                         catch (ArgumentOutOfRangeException ex)
                         {
                             logger.Error(ex, $"[Ошибка обработки отчёта Поддержки. в отчете не найден base64 отчет] в папке {dir.FullName} ");
                         }
                         catch (Exception ex)
                         {
                             handler.Handle(ex, $"[Ошибка обработки отчёта Поддержки] в папке {dir.FullName}");
                         } 
                     });

                    var updateNodeDto = new UpdateNodeDto
                    {
                        Id = node.Id,
                        ImportFolderPath = node.AutoUpdateObnovlyatorPath + ObnovlyatorConsts.ImportFolderPath,
                        ReportFolderPath = node.AutoUpdateObnovlyatorPath + ObnovlyatorConsts.ReportFolderPath,
                        NodeAddress = node.NodeAddress
                    };

                    DeleteAllReports(updateNodeDto);
                    if (TimeToClearObnovlyatorAndUpdateDBStatus.Contains(DateTime.Now.Hour))
                        DeleteAllBasesFromObnovlyator(updateNodeDto);
                });

                if (tiIDateList.Contains(DateTime.Now.AddHours(-7).Day))
                {
                    logger.Debug("Отправляю письма об обновлении баз");
                    finalizeAutoUpdateHelper.NotifyAutoUpdateCompleteOperation(_supportResults);
                }

                var accountDatabasesSupports = dbLayer.AcDbSupportRepository
                    .Where(s => s.AccountDatabase.State == DatabaseState.ProcessingSupport.ToString())
                    .ToList(); 
                if (TimeToClearObnovlyatorAndUpdateDBStatus.Contains(DateTime.Now.Hour))
                {
                    changeDatabasesStateProvider.UpdateAccountDatabasesState(accountDatabasesSupports);
                }
            }
            catch (Exception ex)
            {
                handler.Handle(ex, "[Ошибка обработки результатов Поддержки]");
            }
        }

        /// <summary>
        /// Получить раскодированный отчёт из папки
        /// </summary>
        /// <returns></returns>
        public  Obnovlyator1CReportDto GetEncodeReport(DirectoryInfo dir)
        {

            string findStringStart = CloudConfigurationProvider.AutoUpdateDatabase.GetObnovlyatorBase64StartString();
            string findStringEnd = CloudConfigurationProvider.AutoUpdateDatabase.GetObnovlyatorBase64EndString();

            var reportHttp = GetReport(dir)
                ?? throw new InvalidOperationException($"В папке {dir.FullName} не найден отчет");

            logger.Debug($"Получен отчёт {reportHttp.FullName}");
            var base64ReportString =  File.ReadAllText(reportHttp.FullName);
            base64ReportString = base64ReportString[base64ReportString.IndexOf(findStringStart, StringComparison.OrdinalIgnoreCase)..];
            base64ReportString = base64ReportString[..base64ReportString.IndexOf(findStringEnd, StringComparison.OrdinalIgnoreCase)].Replace(findStringStart, "");
            var jsonReportString = Encoding.UTF8.GetString(System.Convert.FromBase64String(base64ReportString));
            logger.Debug($"Отчёт раскодирован {jsonReportString}");

            return JsonConvert.DeserializeObject<Obnovlyator1CReportDto>(jsonReportString) ??
                throw new NullReferenceException("не удалось десериализовать отчёт");
        }

        /// <summary>
        /// Достать отчёт из папок
        /// </summary>
        private FileInfo? GetReport(DirectoryInfo dirInfo)
        {
            var dirWithReport = dirInfo.GetDirectories().OrderByDescending(d => d.LastWriteTime).FirstOrDefault();

            if (dirWithReport == null)
                return null;

            if (!dirWithReport.GetFiles().Any())
            {
                return GetReport(dirWithReport);
            }
            return dirWithReport.GetFiles().OrderByDescending(f => f.LastWriteTime).FirstOrDefault();
        }

        /// <summary>
        /// Зафиксировать результаты обновления в базе
        /// </summary>
        public void HandleAutoUpdateResult(Obnovlyator1CReportDto reportObj, bool manualUpdateFlag = false)
        {
            var updateType = manualUpdateFlag ? "ручного" : "авто";
            logger.Debug($"Начинаю Обрабатывать результаты {updateType} обновления для базы {reportObj.NameOfBase}");
            var acDbSupport = dbLayer.AcDbSupportRepository.FirstOrDefault(s => s.AccountDatabase.V82Name == reportObj.NameOfBase) ??
                throw new NullReferenceException($"База {reportObj.NameOfBase} не найдена");
            var autoUpdateResult = CreateAccountDatabaseAutoUpdateResultDto(reportObj, acDbSupport);
            logger.Debug($"Результат выполнения: {JsonConvert.SerializeObject(autoUpdateResult)}");

            accountDatabaseChangeStateProvider.ChangeState(acDbSupport.AccountDatabasesID, DatabaseState.Ready);

            acDbSupport.PrepearedForUpdate = false;
            var supportHistory = CreateAutoUpdateHistoryRecord(autoUpdateResult);

            logger.Debug("Создана запись истории обслуживания");
            if (!reportObj.WereErrors)
            {
                if (acDbSupport.CurrentVersion == autoUpdateResult.NewVersion && reportObj.WereWarnings)
                    HandleUpdateError(reportObj, acDbSupport, SupportTypeEnum.AutoUpdate);

                acDbSupport.CurrentVersion = autoUpdateResult.NewVersion;
                acDbSupport.OldVersion = autoUpdateResult.OldVersion;
                HandleBackups(reportObj, acDbSupport);
            }
            else
                HandleUpdateError(reportObj, acDbSupport, SupportTypeEnum.AutoUpdate);

            cloudChangesProvider.LogEvent(acDbSupport.AccountDatabase.AccountId,
                   supportHistory.State == SupportHistoryState.Error
                       ? manualUpdateFlag ? LogActions.ErrorManualUpdate : LogActions.ErrorAutoUpdate
                       : manualUpdateFlag ? LogActions.SuccessManualUpdate : LogActions.SuccessAutoUpdate,
                   supportHistory.Description);

            _supportResults.Add(autoUpdateResult);


            logger.Debug("Обновляю данные в базе");
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                dbLayer.AcDbSupportRepository.Update(acDbSupport);
                dbLayer.AcDbSupportHistoryRepository.Insert(supportHistory);
                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                logger.Warn(ex,
                    $"[Ошибка записи в базу результатов АО] {acDbSupport.AccountDatabasesID}");
                dbScope.Rollback();
                throw;
            }

        }

        /// <summary>
        /// Зафиксировать результат проведения ТиИ в базе
        /// </summary>
        private void HandleTestAndRepairResult(Obnovlyator1CReportDto reportObj)
        {
            logger.Debug($"Начинаю Обрабатывать результаты ТиИ для базы {reportObj.NameOfBase}");
            var acDbSupport = dbLayer.AcDbSupportRepository.FirstOrDefault(s => s.AccountDatabase.V82Name == reportObj.NameOfBase) ??
                throw new NullReferenceException($"База {reportObj.NameOfBase} не найдена");

            accountDatabaseChangeStateProvider.ChangeState(acDbSupport.AccountDatabasesID, DatabaseState.Ready);

            acDbSupport.PreparedForTehSupport = false;
            var supportHistory = CreateSupportHistoryRecord(reportObj, acDbSupport);
            logger.Debug("Создана запись истории обслуживания");

            if (!reportObj.WereErrors)
            {
                acDbSupport.TehSupportDate = DateTime.Now;

                var pathOfBackup = reportObj.CreatedBackups.ElementAtOrDefault(reportObj.CreatedBackups.Count - 1);

                var backupAll = dbLayer.AccountDatabaseBackupRepository.Where(b=> b.AccountDatabaseId == acDbSupport.AccountDatabasesID &&
                b.EventTrigger == CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase && b.SourceType == AccountDatabaseBackupSourceType.Local).ToList();

                foreach ( var backup in backupAll )
                {
                    if(!File.Exists(backup.FilePath))
                    {
                        dbLayer.AccountDatabaseBackupRepository.Delete(backup);
                        logger.Debug($"Удаление записи о бекапе {backup.FilePath} которого нет");
                    }
                }

                if (pathOfBackup is not null)
                {
                    var backupRecord = new AccountDatabaseBackup
                    {
                        Id = Guid.NewGuid(),
                        AccountDatabaseId = acDbSupport.AccountDatabasesID,
                        CreationBackupDateTime = DateTime.Now,
                        InitiatorId = accessProvider.GetUser().Id,
                        BackupPath = pathOfBackup.PathOfBackup,
                        EventTrigger = CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase,
                        SourceType = AccountDatabaseBackupSourceType.Local
                    };

                    dbLayer.AccountDatabaseBackupRepository.Insert(backupRecord);
                    logger.Debug("Создана запись в базе о бэкапе");
                }

            }
            else
                HandleUpdateError(reportObj, acDbSupport, SupportTypeEnum.TestAndRepair);

            cloudChangesProvider.LogEvent(acDbSupport.AccountDatabase.AccountId,
                  supportHistory.State == SupportHistoryState.Error
                      ? LogActions.ErrorTehSupport
                      : LogActions.SuccessTehSupport,
                  $"База {acDbSupport.AccountDatabase.V82Name} результат: {supportHistory.Description}");


            logger.Debug("Обновляю данные о ТиИ в базе");
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                dbLayer.AcDbSupportRepository.Update(acDbSupport);
                dbLayer.AcDbSupportHistoryRepository.Insert(supportHistory);
                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                logger.Warn(ex,
                    $"[Ошибка записи в базу результатов АО] {acDbSupport.AccountDatabasesID}");
                dbScope.Rollback();
                throw;
            }
        }


        /// <summary>
        /// Метод для мапинга результатов отчёта во внутреннюю модель, так как отправка писем работает только с ней 
        /// </summary>
        private static AccountDatabaseAutoUpdateResultDto CreateAccountDatabaseAutoUpdateResultDto(Obnovlyator1CReportDto reportObj, AcDbSupport acDbSupport)
        {
            var newVersion = acDbSupport.CurrentVersion;
            var installedUpdates = reportObj.InstalledUpdates.ElementAtOrDefault(reportObj.InstalledUpdates.Count - 1);

            if (installedUpdates is not null)
                newVersion = installedUpdates.ConfigVersion;

            if (reportObj.PlainStatusOfOperation.Contains("нет обновлений"))
                newVersion = reportObj.PlainStatusOfOperation.Replace("нет обновлений для ", "");

            return new()
            {
                AccountId = acDbSupport.AccountDatabase.AccountId,
                AccountDatabasesId = acDbSupport.AccountDatabasesID,
                SupportProcessorState = reportObj.WereErrors ? AccountDatabaseSupportProcessorState.Error : AccountDatabaseSupportProcessorState.Success,
                Message = string.IsNullOrEmpty(reportObj.ErrorMessage) ? reportObj.NameOfOperation + " " + reportObj.PlainStatusOfOperation : reportObj.ErrorMessage,
                Description = reportObj.NameOfOperation + " " + reportObj.PlainStatusOfOperation,
                Caption = reportObj.NameOfBase,
                OldVersion = acDbSupport.CurrentVersion,
                NewVersion = newVersion
            };
        }

        /// <summary>
        /// Создать объект истории обслуживания
        /// </summary>
        private static AcDbSupportHistory CreateAutoUpdateHistoryRecord(AccountDatabaseAutoUpdateResultDto AUResult)
        => new()
        {
            ID = Guid.NewGuid(),
            AccountDatabaseId = AUResult.AccountDatabasesId,
            EditDate = DateTime.Now,
            Description = $"База {AUResult.Caption}, результат: {AUResult.Message}" ,
            Operation = DatabaseSupportOperation.AutoUpdate,
            State = AUResult.SupportProcessorState == AccountDatabaseSupportProcessorState.Success ? SupportHistoryState.Success : SupportHistoryState.Error,
            OldVersionCfu = AUResult.OldVersion,
            CurrentVersionCfu = AUResult.NewVersion
        };

        private static AcDbSupportHistory CreateSupportHistoryRecord(Obnovlyator1CReportDto reportObj, AcDbSupport acDbSupport)
        => new()
        {
            ID = Guid.NewGuid(),
            AccountDatabaseId = acDbSupport.AccountDatabasesID,
            EditDate = DateTime.Now,
            Description = string.IsNullOrEmpty(reportObj.ErrorMessage) ? reportObj.PlainStatusOfOperation : reportObj.ErrorMessage,
            Operation = DatabaseSupportOperation.TechSupport,
            State = !reportObj.WereErrors ? SupportHistoryState.Success : SupportHistoryState.Error,
        };

        public void DeleteAllReports(UpdateNodeDto node)
        {
            var reportDir = new DirectoryInfo(node.ReportFolderPath);
            var targetToCopyPath = Path.Combine(reportDir.FullName, ObnovlyatorConsts.ReportsCopyDirName);

            if (DateTime.Now.Hour == 6 && Directory.Exists(targetToCopyPath))
                Directory.Delete(targetToCopyPath, true);

            foreach (string dirPath in Directory.GetDirectories(node.ReportFolderPath, "*", SearchOption.AllDirectories))
            {
                if (!dirPath.Contains(ObnovlyatorConsts.ReportsCopyDirName))
                    Directory.CreateDirectory(dirPath.Replace(node.ReportFolderPath, targetToCopyPath)); //создание копий всех отчетов 
            }

            foreach (string filePath in Directory.GetFiles(node.ReportFolderPath, "*.*", SearchOption.AllDirectories))
            {
                if (!filePath.Contains(ObnovlyatorConsts.ReportsCopyDirName))
                    File.Copy(filePath, filePath.Replace(node.ReportFolderPath, targetToCopyPath), true); // создание копий всех файлов 
            }

            foreach (DirectoryInfo dir in reportDir.GetDirectories())
            {
                if (dir.FullName != targetToCopyPath)
                    dir.Delete(true);
            }
        }
        
        public void DeleteAllBasesFromObnovlyator(UpdateNodeDto node)
        {
            var obnovlyator1CRemoveAllBasesCommand = JsonConvert.SerializeObject(new Obnovlyator1CDeleteDto { Operation = "RemoveAllBases" });
            var obnovlyator1CRemoveEmptyGroupsCommand = JsonConvert.SerializeObject(new Obnovlyator1CDeleteDto { Operation = "RemoveEmptyGroups" });

            if (!Directory.Exists(node.ImportFolderPath))
                Directory.CreateDirectory(node.ImportFolderPath);

            var pathUpdateCommandDeleteBase = Path.Combine(node.ImportFolderPath, "deleteAllDatabases.json");  
            var pathUpdateFolderCommandDeleteGroups = Path.Combine(node.ImportFolderPath, "deleteEmptyGroups.json");

            File.WriteAllText(pathUpdateCommandDeleteBase, obnovlyator1CRemoveAllBasesCommand);
            File.WriteAllText(pathUpdateFolderCommandDeleteGroups, obnovlyator1CRemoveEmptyGroupsCommand);

            logger.Info($"Файл удаления всех баз отправлен по путям {pathUpdateCommandDeleteBase} и {pathUpdateFolderCommandDeleteGroups}");
        }


        /// <summary>
        /// Отправить письмо с результатами автообновления
        /// </summary>
        /// <param name="emailContent">Содержимое письма</param>
        private void SendAutoUpdateErrorLetter(string emailContent, string databaseName, Guid accountId)
        {
            var accountAdminId = accessProvider.GetAccountAdmins(accountId).FirstOrDefault();
            var accountAdminEmail = dbLayer.AccountUsersRepository.FirstOrDefault(x => x.Id == accountAdminId).Email;

            logger.Trace($"Отправляем письмо об ошибке на {_cloudServicesEmail.Value}");
            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(_cloudServicesEmail.Value)
                .Copy(_hotlineEmail.Value)
                .Copy(accountAdminEmail)
                .Body(emailContent)
                .Subject($"Ошибка проведения АО/ТиИ базы {databaseName}")
                .SendViaNewThread();

            LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.MailNotification,
                $"Письмо \"Ошибка проведения АО/ТиИ базы {databaseName}\" отправлено на  {accountAdminEmail}");

        }

        private void HandleUpdateError(Obnovlyator1CReportDto reportObj, AcDbSupport acDbSupport, SupportTypeEnum supportTypeEnum)
        {
            var letterErrorMessage = reportObj.ErrorMessage;

            if (reportObj.ErrorMessage.Contains(ObnovlyatorResultErrorMessageConst.Authorization))
            {
                acDbSupport.State = (int)SupportState.NotValidAuthData;
                OffSupport(acDbSupport, supportTypeEnum);
                logger.Warn($"База {acDbSupport.AccountDatabase.V82Name} отключена от АО/ТиИ, по причине неверных авторизационных данных"); 
                letterErrorMessage = "Поскольку вы обновили данные доступа в базу 1С, автоматическое обновление вашей базы стало невозможным." +
                    " Для восстановления подключения к услуге автоматического обновления " +
                    "необходимо пройти повторную процедуру авторизации в разделе \"Настройки АО и ТиИ\" в Личном кабинете";
            }

            else if (reportObj.ErrorMessage.Contains(ObnovlyatorResultErrorMessageConst.HasModifications))
            {
                acDbSupport.DatabaseHasModifications = true;
                OffSupport(acDbSupport, supportTypeEnum);
                logger.Warn($"База {acDbSupport.AccountDatabase.V82Name} отключена от АО, по причине 'база содержит модификации'");
                letterErrorMessage = "Внимание! Ваша база данных 1С не может быть автоматически обновлена из-за присутствия доработок. " +
                                     "Автоматическое обновление доступно исключительно для типовых баз без доработок.";
            }

            else if (reportObj.ErrorMessage.Contains(ObnovlyatorResultErrorMessageConst.ActiveSession1)
                     || reportObj.ErrorMessage.Contains(ObnovlyatorResultErrorMessageConst.ActiveSession2)
                     || reportObj.ErrorMessage.Contains(ObnovlyatorResultErrorMessageConst.ActiveSession3))
            {
                logger.Warn($@"База {acDbSupport.AccountDatabase.V82Name} не обновлена по причине активных сеансов");
                letterErrorMessage =
                    "Внимание! Автоматическое обновление вашей базы данных не было выполнено из-за активных сеансов или потому, " +
                    "что база осталась открытой по завершении рабочего дня. Для гарантированного успешного обновления важно закрывать базу " +
                    "в конце каждого рабочего дня. Также вы можете включить опцию \"Принудительное завершение сеансов\" в разделе " +
                    "\"Настройки ТиИ и АО\" вашего личного кабинета на странице информационной базы, чтобы обеспечить обновление без " +
                    "необходимости вручную закрывать сеансы.";
            }

            SendAutoUpdateErrorLetter(letterErrorMessage, reportObj.NameOfBase, acDbSupport.AccountDatabase.AccountId);
        }

        private void OffSupport(AcDbSupport acDbSupport, SupportTypeEnum supportTypeEnum)
        {
            switch (supportTypeEnum)
            {
                case SupportTypeEnum.AutoUpdate:
                    accountDatabaseSupportProvider.ChangeHasAutoUpdateFlag(acDbSupport, false);
                    break;
                case SupportTypeEnum.TestAndRepair:
                    accountDatabaseSupportProvider.ChangeHasSupportFlag(acDbSupport, false);
                    break;
                default:
                    break;
            }
        }

        private void HandleBackups(Obnovlyator1CReportDto reportObj, AcDbSupport acDbSupport)
        {
            try
            {
                var backupAll = dbLayer.AccountDatabaseBackupRepository.Where(b => b.AccountDatabaseId == acDbSupport.AccountDatabasesID &&
                b.EventTrigger == CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase && b.SourceType == AccountDatabaseBackupSourceType.Local).ToList();

                foreach (var backup in backupAll)
                {
                    if (!File.Exists(backup.FilePath))
                    {
                        dbLayer.AccountDatabaseBackupRepository.Delete(backup);
                        logger.Debug($"Удаление записи о бекапе {backup.FilePath} которого нет");
                    }
                }

                var pathOfBackup = reportObj.CreatedBackups.ElementAtOrDefault(reportObj.CreatedBackups.Count - 1);

                if (pathOfBackup is not null)
                {
                    var backupRecord = new AccountDatabaseBackup
                    {
                        Id = Guid.NewGuid(),
                        AccountDatabaseId = acDbSupport.AccountDatabasesID,
                        CreationBackupDateTime = DateTime.Now,
                        InitiatorId = accessProvider.GetUser().Id,
                        BackupPath = pathOfBackup.PathOfBackup,
                        EventTrigger = CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase,
                        SourceType = AccountDatabaseBackupSourceType.Local
                    };

                    dbLayer.AccountDatabaseBackupRepository.Insert(backupRecord);
                    logger.Debug("Создана запись в базе о бэкапе");
                }
            }
            catch (Exception ex)
            {
                handler.Handle(ex, $"[Ошибка обработки бэкапов при проведении АО] базы {acDbSupport.AccountDatabasesID}");
            }
        }
    }
}
