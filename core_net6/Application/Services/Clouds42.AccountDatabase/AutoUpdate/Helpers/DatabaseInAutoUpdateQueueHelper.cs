﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;

namespace Clouds42.AccountDatabase.AutoUpdate.Helpers
{
    /// <summary>
    /// Хелпер для работы с данными инф. баз, которые в очереди на АО
    /// </summary>
    public static class DatabaseInAutoUpdateQueueHelper
    {
        /// <summary>
        /// Обновить флаг "В процессе авто обновления"
        /// для модели данных базы которая в очереди на АО
        /// </summary>
        /// <param name="model">Модель данных базы которая в очереди на АО</param>
        /// <param name="databasesWhichAreAutoUpdate">Инф. базы у которых идет авто обновление</param>
        public static void UpdateIsProcessingAutoUpdateFlag(this AutoUpdateAccountDatabaseDto model,
            List<Guid> databasesWhichAreAutoUpdate) =>
            model.IsProcessingAutoUpdate = databasesWhichAreAutoUpdate.Contains(model.Id);
    }
}
