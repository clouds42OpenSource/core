﻿using Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries;
using PagedListExtensionsNetFramework;

namespace Clouds42.AccountDatabase.AutoUpdate.Managers
{
    /// <summary>
    /// Менеджер для работы с данными АО инф. баз
    /// </summary>
    public class AutoUpdateAccountDatabaseDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IDatabaseAutoUpdateTechnicalResultDataProvider databaseAutoUpdateTechnicalResultDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

       

        /// <summary>
        /// Получить данные технических результатов АО инф. базы 
        /// </summary>
        /// <param name="filter">Модель фильтра</param>
        /// <returns>Список технических результатов АО инф. базы</returns>
        public ManagerResult<PagedDto<DatabaseAutoUpdateTechnicalResultDataDto>> GetDatabaseAutoUpdateTechnicalResults(GetTechnicalResultsQuery filter)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_GetAutoUpdateData);
                var data = databaseAutoUpdateTechnicalResultDataProvider.GetData(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения технических результатов АО инф. базы]");
                return PreconditionFailed<PagedDto<DatabaseAutoUpdateTechnicalResultDataDto>>(ex.Message);
            }
        }
    }
}
