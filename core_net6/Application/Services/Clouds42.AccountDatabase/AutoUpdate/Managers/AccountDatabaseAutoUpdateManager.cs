﻿using Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.AutoUpdate.Managers
{
    /// <summary>
    /// Менеджер автообновления инф. баз
    /// </summary>
    public class AccountDatabaseAutoUpdateManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountDatabaseAutoUpdateProvider accountDatabaseAutoUpdateProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Выполнить автообновление
        /// </summary>
        /// <param name="accountDatabaseAutoUpdateParams">Параметры автообновления инф. базы</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult<AccountDatabaseAutoUpdateResultDto> ProcessAutoUpdate(
            AccountDatabaseAutoUpdateParamsDto accountDatabaseAutoUpdateParams)
        {
            try
            {
                var result = accountDatabaseAutoUpdateProvider.ProcessAutoUpdate(accountDatabaseAutoUpdateParams);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<AccountDatabaseAutoUpdateResultDto>(ex.Message);
            }
        }
    }
}
