﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Edit.Providers
{
    /// <summary>
    /// Провайдер изменения шаблона для инф. базы
    /// </summary>
    internal class ChangeAccountDatabaseTemplateProvider(
        IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        IUnitOfWork dbLayer,
        IOnChangeDatabaseTemplateTrigger onChangeDatabaseTemplateTrigger,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        ILogger42 logger)
        : IChangeAccountDatabaseTemplateProvider
    {
        /// <summary>
        /// Изменить шаблон у информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="templateId">Новый шаблон</param>
        public void Change(Guid databaseId, Guid? templateId)
        {
            var database = accountDatabaseDataHelper.GetAccountDatabaseOrThrowException(databaseId);
            var acDbSupport =
                dbLayer.AcDbSupportRepository.FirstOrDefault(sup => sup.AccountDatabasesID == databaseId);
            var dbTemplateRelation = dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                .AsQueryableNoTracking()
                .FirstOrDefault(confRel => confRel.DbTemplateId == templateId);
            
            using (var dbScope = dbLayer.SmartTransaction.Get())
            {
                try
                {
                    database.TemplateId = templateId;
                    //if (acDbSupport != null)
                    //{
                    //    acDbSupport.ConfigurationName = dbTemplateRelation?.Configuration1CName;
                    //    acDbSupport.SynonymConfiguration = dbTemplateRelation?.Configuration1CName;
                    //    _dbLayer.AcDbSupportRepository.Update(acDbSupport);
                    //}
                    dbLayer.DatabasesRepository.Update(database);
                    dbLayer.Save();

                    onChangeDatabaseTemplateTrigger.Execute(new OnChangeDatabaseTemplateTriggerDto
                    {
                        DatabaseId = database.Id
                    });

                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    logger.Trace(
                        $"Изменение шаблона у информационной базы '{database.Id}' завершилось с ошибкой. : {ex.GetFullInfo()}");
                    dbScope.Rollback();
                    throw;
                }
            }

            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(database.Id);
        }
    }
}
