﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Edit.Providers
{
    /// <summary>
    /// Провайдер для смены статуса публикации инф. баз
    /// </summary>
    internal class AccountDatabaseChangePublishStateProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : IAccountDatabaseChangePublishStateProvider
    {
        /// <summary>
        /// Сменить статус публикации
        /// </summary>
        /// <param name="publishState">Статус публикации</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        public void ChangePublishState(PublishState publishState, Guid accountDatabaseId)
        {
            try
            {
                dbLayer.DatabasesRepository
                    .AsQueryable()
                    .Where(x => x.Id == accountDatabaseId)
                    .ExecuteUpdate(x => x.SetProperty(y => y.PublishState, publishState.ToString()));
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка обновления статуса публикации] статус: {publishState} инф. база: {accountDatabaseId}");

                throw;
            }
        }
    }
}
