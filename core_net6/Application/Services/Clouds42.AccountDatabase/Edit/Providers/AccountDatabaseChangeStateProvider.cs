﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Edit.Providers
{
    /// <summary>
    /// Провайдер для смены статуса иформацонной базы
    /// </summary>
    public class AccountDatabaseChangeStateProvider(
        IUnitOfWork dbLayer,
        IOnChangeDatabaseStateTrigger onChangeDatabaseStateTrigger,
        ILogger42 logger)
        : IAccountDatabaseChangeStateProvider
    {
        /// <summary>
        /// Изменить статус информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        public void ChangeState(Guid databaseId, DatabaseState newState) =>
            SetNewDatabaseState(databaseId, newState);

        /// <summary>
        /// Изменить статус информационной базы
        /// </summary>
        /// <param name="database">Модель информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        /// <param name="createAccountDatabaseComment"></param>
        public void ChangeState(Domain.DataModels.AccountDatabase database, DatabaseState newState, string? createAccountDatabaseComment = null) =>
            SetNewDatabaseState(database, newState, createAccountDatabaseComment:createAccountDatabaseComment);

        /// <summary>
        /// Изменить статус информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        /// <param name="lastActivityDate">Дата последней активности в базе</param>
        public void ChangeState(Guid databaseId, DatabaseState newState, DateTime lastActivityDate) =>
            SetNewDatabaseState(databaseId, newState, lastActivityDate);


        /// <summary>
        /// Изменить статус информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        /// <param name="createAccountDatabaseComment">Комментарий по созданию базы</param>
        public void ChangeState(Guid databaseId, DatabaseState newState, string? createAccountDatabaseComment) =>
            SetNewDatabaseState(databaseId, newState, createAccountDatabaseComment: createAccountDatabaseComment);

        /// <summary>
        /// Установить новый статус информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        /// <param name="lastActivityDate">Дата последней активности в базе</param>
        /// <param name="createAccountDatabaseComment">Комментарий по созданию базы</param>
        private void SetNewDatabaseState(Guid databaseId, DatabaseState newState, DateTime? lastActivityDate = null,
            string? createAccountDatabaseComment = null) => SetNewDatabaseState(
            GetAccountDatabaseOrThrowException(databaseId), newState, lastActivityDate, createAccountDatabaseComment);

        /// <summary>
        /// Установить новый статус информационной базы
        /// </summary>
        /// <param name="database">Модель информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        /// <param name="lastActivityDate">Дата последней активности в базе</param>
        /// <param name="createAccountDatabaseComment">Комментарий по созданию базы</param>
        private void SetNewDatabaseState(Domain.DataModels.AccountDatabase database, DatabaseState newState, DateTime? lastActivityDate = null,
            string? createAccountDatabaseComment = null)
        {
            try
            {
                logger.Info($"Для базы {database.V82Name} сменен статус с {database.StateEnum} на {newState}");
                database.StateEnum = newState;
                database.LastActivityDate = lastActivityDate ?? database.LastActivityDate;
                database.CreateAccountDatabaseComment =
                    createAccountDatabaseComment ?? database.CreateAccountDatabaseComment;
                dbLayer.DatabasesRepository.Update(database);
                dbLayer.Save();

                onChangeDatabaseStateTrigger.Execute(database.Id);
            }
            catch (Exception ex)
            {
                logger.Warn($"Ошибка смена статуса для базы {database.V82Name} сменен статус с {database.StateEnum} на {newState} :: {ex.GetMessage()}");
                throw new InvalidOperationException(
                    $"При смене статуса для информационной базы '{database.Id}' произошла ошибка: {ex.GetMessage()}");
            }
        }

        /// <summary>
        /// Получить информационную базу по Id 
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <returns>Модель информационной базы</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid databaseId) =>
            dbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == databaseId) ??
            throw new NotFoundException($"Не удалось найти базу по Id '{databaseId}'");
    }
}
