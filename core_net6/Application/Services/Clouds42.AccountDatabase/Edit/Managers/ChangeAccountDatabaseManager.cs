﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Edit.Managers
{
    /// <summary>
    /// Менеджер для изменения инф. базы
    /// </summary>
    public class ChangeAccountDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IDbTemplateReferenceProvider dbTemplateReferenceProvider,
        IChangeAccountDatabaseTemplateProvider changeAccountDatabaseTemplateProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Изменить шаблон у информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="templateId">Новый шаблон</param>
        public ManagerResult<string> ChangeAccountDatabaseTemplate(Guid databaseId, Guid? templateId)
        {
            var accountDatabase = GetAccountDatabase(databaseId);

            if (accountDatabase == null)
                return PreconditionFailed<string>($"Информационная база '{databaseId}' не найдена!");

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditFull, () => accountDatabase.AccountId);

                var templateChanged = dbTemplateReferenceProvider.GetDbTemplateById(templateId);

                var message =
                    $"Смена шаблона для базы \"{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}\" " +
                    $"с \"{accountDatabase.DbTemplate.DefaultCaption}\" " +
                    $"на \"{templateChanged.DefaultCaption}\".";

                changeAccountDatabaseTemplateProvider.Change(databaseId, templateId);

                LogEvent(() => accountDatabase.AccountId, LogActions.EditInfoBase, message);

                return Ok(templateChanged.DefaultCaption);
            }
            catch (AccessDeniedException ex)
            {
                return Forbidden<string>(ex.Message);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка смены шаблона для информационной базы.]");
                return PreconditionFailed<string>(ex.Message);
            }
        }
    }
}
