﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Migration.Helpers
{
    /// <summary>
    /// Валидатор инф. базы перед миграцией
    /// </summary>
    public class BeforeMigrateAccountDatabaseValidator(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        AccountDatabasePathHelper accountDatabasePathHelper,
        DatabaseMigrationHelper databaseMigrationHelper,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Проверить инф. базу перед миграцией
        /// </summary>
        /// <param name="migrateAccountDatabaseParameters">Модель параметров миграции инф. базы</param>
        /// <returns>Список результатов валидации</returns>
        public List<AccountDatabaseValidationResultDto> ValidateAccountDatabaseBeforeMigrate(MigrateAccountDatabaseParametersDto migrateAccountDatabaseParameters)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(
                        migrateAccountDatabaseParameters.AccountDatabaseId);

                logger.Trace($"Начало процесса проверки инф. базы {accountDatabase.V82Name} перед миграцией.");

                var targetFileStorage =
                    databaseMigrationHelper.GetFileStorageServerOrThrowException(migrateAccountDatabaseParameters.DestinationFileStorageId);

                var validationResults = new List<AccountDatabaseValidationResultDto>();

                ValidateNewFileStorage(validationResults, accountDatabase, targetFileStorage);
                ValidateAccountDatabase(validationResults, accountDatabase);

                logger.Trace(
                    $"Процесс проверки инф. базы {accountDatabase.V82Name} перед миграцией завершен. Количество ошибок: {validationResults.Count}");

                return validationResults;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка проверки информационной базы перед миграцией] {migrateAccountDatabaseParameters.AccountDatabaseId}");
                throw;
            }
        }

        /// <summary>
        /// Проверить новое файловое хранилище
        /// </summary>
        /// <param name="validationResults">Список результатов проверки</param>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetFileStorage">Новое файловое хранилище</param>
        private void ValidateNewFileStorage(List<AccountDatabaseValidationResultDto> validationResults, 
            Domain.DataModels.AccountDatabase accountDatabase, CloudServicesFileStorageServer targetFileStorage)
        {
            ValidateTargetFileStorage(validationResults, targetFileStorage.ID, accountDatabase.FileStorageID);
            ValidateDirectoryInNewFileStorage(validationResults, accountDatabase, targetFileStorage);
        }

        /// <summary>
        /// Проверить новое файловое хранилище
        /// </summary>
        /// <param name="validationResults">Список результатов проверки</param>
        /// <param name="targetFileStorageId">ID нового файлового хранилища</param>
        /// <param name="sourceFileStorageId">ID старого файлового хранилища</param>
        private void ValidateTargetFileStorage(List<AccountDatabaseValidationResultDto> validationResults,
            Guid targetFileStorageId, Guid? sourceFileStorageId)
        {
            if (targetFileStorageId == sourceFileStorageId)
            {
                var message = $"Смена файлового хранилища не требуется, так как новое хранилище {targetFileStorageId} соответствует старому {sourceFileStorageId}.";
                logger.Warn(message);
                validationResults.Add(CreateAccountDatabaseValidationResult(message));
            }
        }

        /// <summary>
        /// Проверить директорию в новом файловом хранилище
        /// </summary>
        /// <param name="validationResults">Список результатов проверки</param>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetFileStorage">Новое файловое хранилище</param>
        private void ValidateDirectoryInNewFileStorage(List<AccountDatabaseValidationResultDto> validationResults,
            Domain.DataModels.AccountDatabase accountDatabase, CloudServicesFileStorageServer targetFileStorage)
        {
            if (CheckAccountDatabaseFilesInNewPlace(accountDatabase, targetFileStorage))
            {
                var message =
                    $"В папке по новому пути ИБ {accountDatabase.V82Name} уже есть файлы. Возможна перезапись базы некорректными данными.";
                logger.Warn(message);
                validationResults.Add(CreateAccountDatabaseValidationResult(message));
            }
        }

        /// <summary>
        /// Проверить инф. базу на соответсвие требованиям
        /// </summary>
        /// <param name="validationResults">Список результатов проверки</param>
        /// <param name="accountDatabase">Инф. база</param>
        private void ValidateAccountDatabase(List<AccountDatabaseValidationResultDto> validationResults,
            Domain.DataModels.AccountDatabase accountDatabase)
        {
            ValidateAccountDatabaseExistence(validationResults, accountDatabase);

            ValidateAccountDatabaseState(validationResults, accountDatabase);

            ValidateAccountDatabaseType(validationResults, accountDatabase);

            ValidateAccountDatabaseUsage(validationResults, accountDatabase);
        }

        /// <summary>
        /// Проверить статус инф. базы
        /// </summary>
        /// <param name="validationResults">Список результатов </param>
        /// <param name="accountDatabase">Инф. база</param>
        private void ValidateAccountDatabaseState(List<AccountDatabaseValidationResultDto> validationResults, Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.State != DatabaseState.Ready.ToString())
            {
                var message =
                    $"Невозможно перенести инф. базу, так как ее статус не Ready. Статус базы: '{accountDatabase.State}'";
                logger.Warn(message);
                validationResults.Add(CreateAccountDatabaseValidationResult(message));
            }
        }

        /// <summary>
        /// Проверить тип инф. базы
        /// </summary>
        /// <param name="validationResults">Список результатов </param>
        /// <param name="accountDatabase">Инф. база</param>
        private void ValidateAccountDatabaseType(List<AccountDatabaseValidationResultDto> validationResults, Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (!accountDatabase.IsFile.HasValue || !accountDatabase.IsFile.Value)
            {
                var message = $"Невозможно перенести инф. базу {accountDatabase.V82Name}, так как она серверная.";
                logger.Warn(message);
                validationResults.Add(CreateAccountDatabaseValidationResult(message));
            }
        }

        /// <summary>
        /// Проверить существование инф. базы
        /// </summary>
        /// <param name="validationResults">Список результатов </param>
        /// <param name="accountDatabase">Инф. база</param>
        private void ValidateAccountDatabaseExistence(List<AccountDatabaseValidationResultDto> validationResults, Domain.DataModels.AccountDatabase accountDatabase)
        {
            var dbPath = accountDatabasePathHelper.GetPath(accountDatabase);

            var databaseFiles = Directory.GetFiles(dbPath);

            if (!databaseFiles.Any())
            {
                var message = $"Невозможно перенести инф.базу {accountDatabase.V82Name}, так как она не найдена по пути {dbPath}.";
                logger.Warn(message);
                validationResults.Add(CreateAccountDatabaseValidationResult(message));
            }
        }

        /// <summary>
        /// Проверить использование инф. базы
        /// </summary>
        /// <param name="validationResults">Список результатов </param>
        /// <param name="accountDatabase">Инф. база</param>
        private void ValidateAccountDatabaseUsage(List<AccountDatabaseValidationResultDto> validationResults, Domain.DataModels.AccountDatabase accountDatabase)
        {
            var dbPath = accountDatabasePathHelper.GetPath(accountDatabase);

            if (AccountDatabaseFileUseChecker.Check(dbPath))
            {
                var message = $"Невозможно перенести инф.базу {accountDatabase.V82Name}, так как она используется.";
                logger.Warn(message);
                validationResults.Add(CreateAccountDatabaseValidationResult(message));
            }
        }

        /// <summary>
        /// Создать объект результата валидации
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns>Объект результата валидации</returns>
        private AccountDatabaseValidationResultDto CreateAccountDatabaseValidationResult(string message)
            => new()
            {
                Result = false,
                ValidationMessage = message
            };

        /// <summary>
        /// Проверить наличие файлов по пути нового расположения ИБ после миграции
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetStorage">Новое файловое хранилище</param>
        /// <returns>true - если папка существует и в ней есть файлы</returns>
        private bool CheckAccountDatabaseFilesInNewPlace(Domain.DataModels.AccountDatabase accountDatabase, CloudServicesFileStorageServer targetStorage)
        {
            var newPath = accountDatabasePathHelper.GetPath(accountDatabase, targetStorage);
            if (Directory.Exists(newPath))
            {
                return Directory.EnumerateFileSystemEntries(newPath).Any();
            }
            return false;
        }
    }
}
