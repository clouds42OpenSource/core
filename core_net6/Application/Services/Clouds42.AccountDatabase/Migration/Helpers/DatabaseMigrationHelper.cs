﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.AccountDatabase.Migration.Helpers
{
    /// <summary>
    /// Хэлпер для миграции инф. баз
    /// </summary>
    public class DatabaseMigrationHelper(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        ISegmentHistoryProvider segmentHistoryProvider,
        IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Создать модель параметров миграции инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="fileStorageServer">Файловое хранилище</param>
        /// <param name="accountUserInitiatorId">ID пользователя, который иницировал миграцию</param>
        /// <returns>Модель параметров миграции инф. базы</returns>
        public MigrateAccountDatabaseParametersDto CreateAccDbMigrationParameters(Guid accountDatabaseId,
            CloudServicesFileStorageServer fileStorageServer, Guid accountUserInitiatorId)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);

            if (accountDatabase.FileStorageID == null)
                throw new InvalidOperationException($"Для базы {accountDatabase.V82Name} не указан ID хранилища");

            return new MigrateAccountDatabaseParametersDto
            {
                AccountDatabaseId = accountDatabaseId,
                DestinationFileStorageId = fileStorageServer.ID,
                AccountUserInitiatorId = accountUserInitiatorId,
                OperationStartDate = DateTime.Now,
                SourceFileStorageId = accountDatabase.FileStorageID.Value
            };
        }

        /// <summary>
        /// Получить сообщения об ошибках из списка результатов проверки
        /// </summary>
        /// <param name="validationResults">Список результатов проверки</param>
        /// <returns>Сообщения об ошибках</returns>
        public string GetErrorsFromValidationResultList(List<AccountDatabaseValidationResultDto> validationResults)
        {
            var errors = string.Empty;

            validationResults.ForEach(w => { errors += $"\r\n{w.ValidationMessage}"; });

            return errors;
        }

        /// <summary>
        /// Зарегистрировать миграцию инф. базы
        /// </summary>
        /// <param name="migrateAccountDatabaseParameters">Модель параметров миграции инф. базы</param>
        /// <param name="result">Результат миграции</param>
        public void RegisterMigrationHistory(MigrateAccountDatabaseParametersDto migrateAccountDatabaseParameters, bool result)
        {
            segmentHistoryProvider.RegisterMigrationAccountDatabaseHistory(new MigrationAccountDatabaseHistory
            {
                AccountDatabaseId = migrateAccountDatabaseParameters.AccountDatabaseId,
                SourceFileStorageId = migrateAccountDatabaseParameters.SourceFileStorageId,
                TargetFileStorageId = migrateAccountDatabaseParameters.DestinationFileStorageId,
                MigrationHistory = segmentHistoryProvider.
                    CreateMigrationHistoryObject(migrateAccountDatabaseParameters.AccountUserInitiatorId, 
                        migrateAccountDatabaseParameters.OperationStartDate, result)
            });
        }

        /// <summary>
        /// Создать ошибочный результат миграции (если во время выполнения процесса упало исключение)
        /// </summary>
        /// <param name="migrateAcDbParams">Модель параметров миграции инф. базы</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Ошибочный результат миграции</returns>
        public DatabaseMigrationResultDto CreateErrorMigrationResult(MigrateAccountDatabaseParametersDto migrateAcDbParams, string errorMessage)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(migrateAcDbParams.AccountDatabaseId);

            return new DatabaseMigrationResultDto
            {
                Result = false,
                DatabaseId = migrateAcDbParams.AccountDatabaseId,
                AccountDatabaseOwnerId = accountDatabase.AccountId,
                AccountUserInitiatorId = migrateAcDbParams.AccountUserInitiatorId,
                ErrorMessage = $"Миграция инф. базы {accountDatabase.V82Name} не выполнена. Причина: {errorMessage}"
            };
        }

        /// <summary>
        /// Обработать результат выполнения процесса миграции
        /// </summary>
        /// <param name="stateMachineResult">Результат выполнения процесса миграции</param>
        /// <param name="migrateAcDbParams">Модель параметров миграции инф. базы</param>
        /// <returns>Результат миграции инф. базы</returns>
        public DatabaseMigrationResultDto ProcessStateMachineResult(StateMachineResult<DatabaseMigrationResultDto> stateMachineResult,
            MigrateAccountDatabaseParametersDto migrateAcDbParams)
        {
            if (stateMachineResult.Finish)
                return stateMachineResult.ResultModel;

            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(migrateAcDbParams.AccountDatabaseId);

            var linkForOpenProcessFlow = GetLinkForOpenProcessFlow(stateMachineResult.ProcessFlowId);

            var errorMessage =
                $"Миграция инф. базы {accountDatabase.V82Name} не выполнена. "+
                $"Чтобы завершить процесс миграции перейдите по <a href='{linkForOpenProcessFlow}' target='_blank'>ссылке</a>. "+
                $"Причина: {stateMachineResult.Message}";

            return new DatabaseMigrationResultDto
            {
                Result = false,
                DatabaseId = migrateAcDbParams.AccountDatabaseId,
                AccountUserInitiatorId = migrateAcDbParams.AccountUserInitiatorId,
                ErrorMessage = errorMessage,
                AccountDatabaseOwnerId = accountDatabase.AccountId
            };
        }
        
        /// <summary>
        /// Получить файловое хранилище или инициировать исключение если оно не найдено
        /// </summary>
        /// <param name="fileStorageServerId">ID файлового хранилища</param>
        /// <returns>Файловое хранилище</returns>
        public CloudServicesFileStorageServer GetFileStorageServerOrThrowException(Guid? fileStorageServerId)
            => dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(w => w.ID == fileStorageServerId)
               ?? throw new NotFoundException($"Файловое хранилище по ID {fileStorageServerId} не найдено.");

        /// <summary>
        /// Получить ссылку для просмотра деталей рабочего процесса
        /// </summary>
        /// <param name="processFlowId"></param>
        /// <returns></returns>
        private string GetLinkForOpenProcessFlow(Guid processFlowId)
        {
            var siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
            var routeValue = CloudConfigurationProvider.Cp.GetRouteValueForOpenProcessFlowDetails();

            return $"{siteUrl}/{routeValue}{processFlowId}";
        }
    }
}
