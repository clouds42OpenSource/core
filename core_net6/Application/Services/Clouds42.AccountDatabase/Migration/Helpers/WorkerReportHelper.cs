﻿using System.Text;
using Clouds42.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Migration.Helpers
{
    /// <summary>
    /// Хэлпер для отчета воркера по результатам миграции
    /// </summary>
    public class WorkerReportHelper(
        IMessagesManager messagesManager,
        IUnitOfWork dbLayer,
        ILogger42 logger)
    {
        /// <summary>
        /// Отправить отчет воркера
        /// </summary>
        /// <param name="migrationsResult">Список результатов миграции инф. баз</param>
        /// <param name="accountDatabaseIds">Список ID инф. баз</param>
        /// <param name="fileStorage">Файловое хранилище</param>
        public void SendWorkerReport(List<DatabaseMigrationResultDto> migrationsResult,
            List<Guid> accountDatabaseIds,
            CloudServicesFileStorageServer fileStorage)
        {
            var databases = dbLayer.DatabasesRepository.Where(d => accountDatabaseIds.Contains(d.Id)).ToDictionary(k => k.Id, v => v);

            var messageBuilder = new StringBuilder();

            messageBuilder.Append($"<p>Перенос баз в файловое хранилище {fileStorage.Name}:</p>");

            CreateMigrationErrorMessagesList(migrationsResult, databases, messageBuilder);

            CreateMigrationSuccessMessagesList(migrationsResult, databases, messageBuilder);

            var topik = "Перенос баз завершен";

            var email = ConfigurationHelper.GetConfigurationValue("SegmentMigrationReportMail");

            logger.Debug(messageBuilder.ToString());

            messagesManager.SendWorkerReport(
                messageBuilder.ToString(),
                topik,
                email);
        }

        /// <summary>
        /// Создать список сообщений об ошибках миграции
        /// </summary>
        /// <param name="migrationsResult">Список результатов миграции инф. баз</param>
        /// <param name="databases">Список баз</param>
        /// <param name="messageBuilder">Сборщик сообщений</param>
        private void CreateMigrationErrorMessagesList(List<DatabaseMigrationResultDto> migrationsResult, 
            Dictionary<Guid, Domain.DataModels.AccountDatabase> databases, StringBuilder messageBuilder)
        {
            if (migrationsResult.Any(p => !p.Result))
            {
                messageBuilder.Append("<p>Ошибки при переносе баз:</p>");
                foreach (var mRes in migrationsResult.Where(p => !p.Result))
                    messageBuilder.Append($"<p>{databases[mRes.DatabaseId]?.V82Name ?? mRes.DatabaseId.ToString()}. Ошибка: {mRes.ErrorMessage}</p>");
            }
        }

        /// <summary>
        /// Создать список сообщений об успешных миграциях
        /// </summary>
        /// <param name="migrationsResult">Список результатов миграции инф. баз</param>
        /// <param name="databases">Список баз</param>
        /// <param name="messageBuilder">Сборщик сообщений</param>
        private void CreateMigrationSuccessMessagesList(List<DatabaseMigrationResultDto> migrationsResult,
            Dictionary<Guid, Domain.DataModels.AccountDatabase> databases, StringBuilder messageBuilder)
        {
            if (migrationsResult.Any(p => p.Result))
            {
                messageBuilder.Append("<p>Перенос баз без ошибок:</p>");
                foreach (var mRes in migrationsResult.Where(p => p.Result))
                    messageBuilder.Append($"<p>{databases[mRes.DatabaseId]?.V82Name ?? mRes.DatabaseId.ToString()}</p>");
            }
        }
    }
}
