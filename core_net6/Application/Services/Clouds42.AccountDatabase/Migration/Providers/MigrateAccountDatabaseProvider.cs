﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Migration.Interfaces;
using Clouds42.AccountDatabase.Migration.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.MigrateAccountDatabaseProcessFlow;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Migration.Providers
{
    /// <summary>
    /// Провайдер миграции инф. баз
    /// </summary>
    internal class MigrateAccountDatabaseProvider(
        WorkerReportHelper workerReportHelper,
        BeforeMigrateAccountDatabaseValidator beforeMigrateAccountDatabaseValidator,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        DatabaseMigrationHelper databaseMigrationHelper,
        IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        IServiceProvider container,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IMigrateAccountDatabaseProvider
    {
        /// <summary>
        /// Перенести инф. базы в новое файловое хранилище
        /// </summary>
        /// <param name="accountDatabaseIds">Список ID инф. баз</param>
        /// <param name="targetFileStorageId">ID файлового хранилища</param>
        /// <param name="accountUserInitiatorId">ID пользователя, инициировавшего миграцию</param>
        /// <returns>Список результатов миграции инф. баз</returns>
        public List<DatabaseMigrationResultDto> MigrateAccountDatabases(List<Guid> accountDatabaseIds, Guid targetFileStorageId,
            Guid accountUserInitiatorId)
        {
            var fileStorage = databaseMigrationHelper.GetFileStorageServerOrThrowException(targetFileStorageId);
            logger.Trace($"Хранилище для переноса найдено. Название: {fileStorage.Name}");

            return MigrateDatabases(accountDatabaseIds, fileStorage, accountUserInitiatorId);
        }

        /// <summary>
        /// Перенести инф. базы в новое файловое хранилище
        /// </summary>
        /// <param name="accountDatabaseIds">Список ID инф. баз</param>
        /// <param name="fileStorageServer">Новое файловое хранилище</param>
        /// <param name="accountUserInitiatorId">ID пользователя, инициировавшего миграцию</param>
        /// <returns>Список результатов миграции инф. баз</returns>
        private List<DatabaseMigrationResultDto> MigrateDatabases(List<Guid> accountDatabaseIds, CloudServicesFileStorageServer fileStorageServer, Guid accountUserInitiatorId)
        {
            var migrationsResultList = new List<DatabaseMigrationResultDto>();

            foreach (var accountDatabaseId in accountDatabaseIds)
            {
                var migrationParams =
                    databaseMigrationHelper.CreateAccDbMigrationParameters(accountDatabaseId, fileStorageServer,
                        accountUserInitiatorId);

                var result = true;
                try
                {
                    var migrationResult = MigrateAccountDatabase(migrationParams);

                    migrationsResultList.Add(migrationResult);
                    logger.Debug($"Результат миграции инф. базы {migrationResult.DatabaseId} : {migrationResult.ErrorMessage}");
                }
                catch (Exception ex)
                {
                    result = false;
                    handlerException.Handle(ex, $"[Ошибка миграции инф. базы] {accountDatabaseId}");
                }
                finally
                {
                    databaseMigrationHelper.RegisterMigrationHistory(migrationParams, result);
                }
            }

            workerReportHelper.SendWorkerReport(migrationsResultList, accountDatabaseIds, fileStorageServer);

            return migrationsResultList;
        }

        /// <summary>
        /// Перенести инф. базу
        /// </summary>
        /// <param name="migrateAccountDatabaseParameters">Модель параметров миграции инф. базы</param>
        /// <returns>Результат миграции инф. базы</returns>
        private DatabaseMigrationResultDto MigrateAccountDatabase(MigrateAccountDatabaseParametersDto migrateAccountDatabaseParameters)
        {
            try
            {
                ValidateAccountDatabaseBeforeMigrate(migrateAccountDatabaseParameters);

                var result = container.GetRequiredService<IMigrateAccountDatabaseProcessFlow>()
                    .Run(migrateAccountDatabaseParameters);

                return databaseMigrationHelper.ProcessStateMachineResult(result, migrateAccountDatabaseParameters);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка миграции инф. базы] {migrateAccountDatabaseParameters.AccountDatabaseId}");

                return databaseMigrationHelper.CreateErrorMigrationResult(migrateAccountDatabaseParameters,
                    ex.Message);
            }
            finally
            {
                ChangeAccountDatabaseState(migrateAccountDatabaseParameters.AccountDatabaseId, DatabaseState.Ready);
            }
        }

        /// <summary>
        /// Сменить состояние базы
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <param name="databaseState">Состояние</param>
        private void ChangeAccountDatabaseState(Guid accountDatabaseId, DatabaseState databaseState)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);
            logger.Trace($"Смена статуса для базы {accountDatabase.V82Name} с {accountDatabase.State} на {databaseState.ToString()}");

            accountDatabaseChangeStateProvider.ChangeState(accountDatabase, databaseState, null);
            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(accountDatabaseId);
        }

        /// <summary>
        /// Проверить инф. базу перед миграцией
        /// </summary>
        /// <param name="migrateAcDbParams">Модель параметров миграции инф. базы</param>
        private void ValidateAccountDatabaseBeforeMigrate(MigrateAccountDatabaseParametersDto migrateAcDbParams)
        {
            var validationResult =
                beforeMigrateAccountDatabaseValidator.ValidateAccountDatabaseBeforeMigrate(migrateAcDbParams);

            if (!validationResult.Any()) 
                return;

            var errorMessage = databaseMigrationHelper.GetErrorsFromValidationResultList(validationResult);
            throw new InvalidOperationException(errorMessage);
        }
    }
}
