﻿using Clouds42.AccountDatabase.Contracts.Migration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Migration.Managers
{
    /// <summary>
    /// Менеджер миграции инф. баз
    /// </summary>
    public class AccountDatabaseMigrationManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IMigrateAccountDatabaseProvider migrateAccountDatabaseProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountDatabaseMigrationManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Перенести инф. базы в новое файловое хранилище
        /// </summary>
        /// <param name="accountDatabaseIds">Список ID инф. баз</param>
        /// <param name="targetFileStorageId">ID файлового хранилища</param>
        /// <param name="accountUserInitiatorId">ID пользователя, инициировавшего миграцию</param>
        /// <returns>Результат выполнения миграции</returns>
        public ManagerResult MigrateAccountDatabases(List<Guid> accountDatabaseIds, Guid targetFileStorageId, Guid accountUserInitiatorId)
        {
            try
            {
                AccessProvider.HasAccessForMultiAccounts(ObjectAction.AccountDatabaseMigration);

                var result = migrateAccountDatabaseProvider.MigrateAccountDatabases(accountDatabaseIds, targetFileStorageId, accountUserInitiatorId);

                WriteMigrationResultsToLog(result);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка миграции информационных баз в хранилище] в количестве '{accountDatabaseIds.Count}' хранилище: '{targetFileStorageId}'");
                throw;
            }
        }

        /// <summary>
        /// Записать результаты миграции в лог
        /// </summary>
        /// <param name="migrationResults">Список результатов миграции</param>
        private void WriteMigrationResultsToLog(List<DatabaseMigrationResultDto> migrationResults)
        {
            migrationResults.ForEach(res =>
                {
                    logger.Trace($"Миграция инф. базы '{res.DatabaseId}' завершена. Результат: {res.Result} - {res.ErrorMessage}");

                    LogEvent(() => res.AccountDatabaseOwnerId, LogActions.MovingBaseBetweenStorage, res.ErrorMessage,
                        res.AccountUserInitiatorId);
                });
        }
    }
}
