﻿using Clouds42.AccountDatabase.Contracts.Migration.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Segment;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Migration
{
    /// <summary>
    /// агрегатор для задач миграции
    /// </summary>
    public class MigrationTasksAggregator(
        AccountDatabasePathHelper accountDatabasesPathHelper,
        IHandlerException handlerException,
        ILogger42 logger)
        : IMigrationTasksAggregator
    {
        /// <summary>
        /// Создание тасок на удаление директорий 
        /// </summary>
        /// <param name="migrateDatabases">список баз которые мигрировали</param>
        /// <param name="databaseToTransfer">список баз для миграции</param>
        /// <param name="oldClientFileFolder">старая клиентская директория</param>
        /// <param name="needMoveClientFiles">нужно ли удалять клиентскую директорию</param>
        /// <returns></returns>
        public List<Task> CreateDeletionOfDirectoryTasks(List<RollbackDataDto> migrateDatabases,
            List<Domain.DataModels.AccountDatabase> databaseToTransfer, string oldClientFileFolder, bool needMoveClientFiles)
        {
            var tasks = new List<Task>();

            //Удаление директорий старых ИБ
            foreach (var item in migrateDatabases)
            {
                try
                {
                    if (Directory.Exists(
                        accountDatabasesPathHelper.GetPath(databaseToTransfer.First(d => d.Id == item.DbId))))
                        tasks.Add(Task.Factory.StartNew(() =>DirectoryHelper.TryDeleteDirectoryOrThrowException(item.OldDbPath, 200)));
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Ошибка удаления старой файловой базы при миграции]. Иб: {item.DbId}");
                }
            }

            //Удаление старых клиентских папок
            try
            {
                if (needMoveClientFiles)
                {
                    logger.Info($"Стартуем таску по удалению директории при миграции: {oldClientFileFolder}");
                    tasks.Add(Task.Factory.StartNew(() => DirectoryHelper.TryDeleteDirectoryOrThrowException(oldClientFileFolder, 200)));
                    
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка удаления старых файлов при миграции]");
            }

            return tasks;
        }

        /// <summary>
        /// Ожидание выполнения тасок
        /// </summary>
        /// <param name="tasks">список тасок</param>
        /// <param name="messageText">результат удаления директории</param>
        public void WaitForCompletionOfTasks(List<Task> tasks, out string messageText)
        {
            messageText = null;
            try
            {
                // Wait for all the tasks to finish.
                Task.WaitAll(tasks.ToArray());

                logger.Info("Таски на удаление директорий при миграции отработали успешно.");

            }
            catch (AggregateException ex)
            {
                handlerException.Handle(ex, "[Ошибка удаления директорий через таску]");
            }
        }

    }
}
