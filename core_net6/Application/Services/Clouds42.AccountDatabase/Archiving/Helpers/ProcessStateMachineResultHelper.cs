﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.AccountDatabase.Archiving.Helpers
{
    /// <summary>
    /// Хэлпер для обработки результатов механизма рабочих процессов
    /// </summary>
    public class ProcessStateMachineResultHelper(IAccountDatabaseDataProvider accountDatabaseDataProvider)
    {
        /// <summary>
        /// Обработать результат переноса в склеп
        /// </summary>
        /// <param name="stateMachineResult">>Результат выполнения процесса переноса</param>
        /// <param name="taskParams"></param>
        /// <returns>Сообщение об ошибке</returns>
        public string ProcessMovingToTombResult(
            StateMachineResult<CopyAccountDatabaseBackupToTombParamsDto> stateMachineResult,
            DeleteAccountDatabaseToTombJobParamsDto taskParams)
        {
            if (stateMachineResult.Finish)
                return null;

            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(taskParams.AccountDatabaseId);

            var linkForOpenProcessFlow = GetLinkForOpenProcessFlow(stateMachineResult.ProcessFlowId);

            var errorMessage =
                $"Перенос инф. базы {accountDatabase.V82Name} в склеп не выполнен. " +
                $"Чтобы завершить процесс переноса перейдите по ссылке {linkForOpenProcessFlow}. " +
                $"Причина: {stateMachineResult.Message}";

            return errorMessage;
        }

        /// <summary>
        /// Получить ссылку для просмотра деталей рабочего процесса
        /// </summary>
        /// <param name="processFlowId"></param>
        /// <returns></returns>
        private string GetLinkForOpenProcessFlow(Guid processFlowId)
        {
            var siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
            var routeValue = CloudConfigurationProvider.Cp.GetRouteValueForOpenProcessFlowDetails();

            return $"{siteUrl}/{routeValue}{processFlowId}";
        }
    }
}
