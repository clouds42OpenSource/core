﻿using Clouds42.AccountDatabase.Archiving.Helpers;
using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.ArchiveAccountDatabaseToTombProcessFlow;

namespace Clouds42.AccountDatabase.Archiving.Providers
{
    /// <summary>
    /// Провайдер архивации инф. баз в склеп
    /// </summary>
    internal class ArchiveAccountDatabaseToTombProvider(
        IArchiveAccountDatabaseToTombProcessFlow archiveAccountDatabaseToTombProcessFlow,
        ProcessStateMachineResultHelper processStateMachineResultHelper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        AccountDatabasePathHelper accountDatabasePathHelper,
        ILetterNotificationProcessor letterNotificationProcessor,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IArchiveAccountDatabaseToTombProvider
    {
        /// <summary>
        /// Перенести инф. базу в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>Результат переноса</returns>
        public void DetachAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParams)
        {
            if (!NeedToDetachAccountDatabase(taskParams.AccountDatabaseId))
                return;

            if (!CanDetachAccountDatabaseToTomb(taskParams))
                return;

            try
            {
                var result = archiveAccountDatabaseToTombProcessFlow.Run(new CopyAccountDatabaseBackupToTombParamsDto
                {
                    AccountDatabaseId = taskParams.AccountDatabaseId,
                    BackupAccountDatabaseTrigger = taskParams.Trigger,
                    AccountUserInitiatorId = taskParams.InitiatorId
                });
                
                var errorMessage = processStateMachineResultHelper.ProcessMovingToTombResult(result, taskParams);

                if (!string.IsNullOrEmpty(errorMessage))
                    throw new InvalidOperationException(errorMessage);

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка переноса инф. базы в склеп]. При переносе инф. базы {taskParams.AccountDatabaseId} в склеп возникла ошибка. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Признак необходимости переноса инф. базы в склеп
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>true - если база существует и она не на разделителях</returns>
        private bool NeedToDetachAccountDatabase(Guid accountDatabaseId)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);

            if (!accountDatabase.IsDelimiter())
                return true;

            logger.Info($"[{accountDatabaseId}] :: Информационная база на разделителях");
            return false;
        }

        /// <summary>
        /// Проверить возмодность перемещения инф. базы в архив
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>Результат проверки</returns>
        private bool CanDetachAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParams)
        {
            if (taskParams.Trigger != CreateBackupAccountDatabaseTrigger.MovingDbToArchive)
                return true;

            if (!IsDatabaseUsed(taskParams.AccountDatabaseId)) 
                return true;

            NotifyClientAbountActiveSessions(taskParams.AccountDatabaseId);
            accountDatabaseChangeStateProvider.ChangeState(taskParams.AccountDatabaseId,
                DatabaseState.Ready);

            return false;
        }

        /// <summary>
        /// Признак что инф. база используется
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Результат проверки</returns>
        private bool IsDatabaseUsed(Guid accountDatabaseId)
        {
            try
            {
                var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);

                if (accountDatabase.IsFile == null || accountDatabase.IsFile == false)
                    return false;

                var accountDatabasePath = accountDatabasePathHelper.GetPath(accountDatabase);
                AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(accountDatabasePath);

                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// Уведомить клиента о наличии активных сессий
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        private void NotifyClientAbountActiveSessions(Guid accountDatabaseId)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);

            letterNotificationProcessor
                .TryNotify<AccountDatabaseUsedLetterNotification, AccountDatabaseUsedLetterModelDto>(
                    new AccountDatabaseUsedLetterModelDto
                    {
                        AccountId = accountDatabase.AccountId,
                        AccountDatabaseId = accountDatabaseId,
                        DatabaseCaption = accountDatabase.Caption
                    });
        }
    }
}
