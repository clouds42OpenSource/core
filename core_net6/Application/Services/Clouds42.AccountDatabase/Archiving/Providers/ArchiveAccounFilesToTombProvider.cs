﻿using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Clouds42.StateMachine.Contracts.ArchiveAccountFilesToTombProcessFlow;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.AccountDatabase.Archiving.Providers
{
    /// <summary>
    /// Провайдер архивации файлов аккаунта в склеп
    /// </summary>
    internal class ArchiveAccounFilesToTombProvider(
        IAccountFilesAnalyzeProvider accountFilesAnalyzeProvider,
        IArchiveAccountFilesToTombProcessFlow archiveAccountFilesToTombProcessFlow,
        IHandlerException handlerException)
        : IArchiveAccounFilesToTombProvider
    {
        /// <summary>
        /// Перенести файлы аккаунта в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>Результат переноса</returns>
        public void DetachAccounFilesToTomb(DeleteAccountFilesToTombJobParamsDto taskParams)
        {
            try
            {
                if (!accountFilesAnalyzeProvider.NeedArchiveAccountFilesToTomb(taskParams.AccountId))
                    return;

                var result = archiveAccountFilesToTombProcessFlow.Run(new CopyAccountFilesBackupToTombParamsDto
                {
                    AccountId = taskParams.AccountId
                });

                var errorMessage = ProcessMovingToTombResult(result, taskParams);

                if (!string.IsNullOrEmpty(errorMessage))
                    throw new InvalidOperationException(errorMessage);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка переноса файлов аккаунта в склеп].  При переносе файлов аккаунта {taskParams.AccountId} в склеп возникла ошибка.");
                throw;
            }
        }

        /// <summary>
        /// Обработать результат переноса в склеп
        /// </summary>
        /// <param name="stateMachineResult">>Результат выполнения процесса переноса</param>
        /// <param name="taskParams"></param>
        /// <returns>Сообщение об ошибке</returns>
        private string ProcessMovingToTombResult(
            StateMachineResult<CopyAccountFilesBackupToTombParamsDto> stateMachineResult,
            DeleteAccountFilesToTombJobParamsDto taskParams)
        {
            if (stateMachineResult.Finish)
                return null;

            var errorMessage =
                $"Перенос файлов аккаунта {taskParams.AccountId} в склеп не выполнен. " +
                $"Причина: {stateMachineResult.Message}";

            return errorMessage;
        }

    }
}
