﻿using Clouds42.AccountDatabase.Archiving.Helpers;
using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.DeleteAccountDatabaseToTombProcessFlow;

namespace Clouds42.AccountDatabase.Archiving.Providers
{
    /// <summary>
    /// Провайдер удаления инф. баз в склеп
    /// </summary>
    internal class DeleteAccountDatabaseToTombProvider(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IDeleteAccountDatabaseToTombProcessFlow deleteAccountDatabaseToTombProcessFlow,
        ProcessStateMachineResultHelper processStateMachineResultHelper,
        ILogger42 logger)
        : IDeleteAccountDatabaseToTombProvider
    {
        /// <summary>
        /// Удалить инф. базу в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>Результат переноса</returns>
        public void DeleteAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParams)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(taskParams.AccountDatabaseId);

                var result = deleteAccountDatabaseToTombProcessFlow.Run(new CopyAccountDatabaseBackupToTombParamsDto
                {
                    AccountDatabaseId = accountDatabase.Id,
                    BackupAccountDatabaseTrigger = taskParams.Trigger,
                    AccountUserInitiatorId = taskParams.InitiatorId,
                    ActionsForSkip = taskParams.ActionsForSkip
                });

                var errorMessage = processStateMachineResultHelper.ProcessMovingToTombResult(result, taskParams);

                if (!string.IsNullOrEmpty(errorMessage))
                    throw new InvalidOperationException(errorMessage);
            }
            catch (Exception ex)
            {
                logger.Warn($"При удалении инф. базы {taskParams.AccountDatabaseId} в склеп возникла ошибка. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }
    }
}
