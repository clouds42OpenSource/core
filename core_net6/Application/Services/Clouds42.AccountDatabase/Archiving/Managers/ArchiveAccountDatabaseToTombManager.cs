﻿using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Archiving.Managers
{
    /// <summary>
    /// Менеджер архивации инф. баз в склеп
    /// </summary>
    public class ArchiveAccountDatabaseToTombManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IArchiveAccountDatabaseToTombProvider archiveAccountDatabaseToTombProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), IArchiveAccountDatabaseToTombManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        ///     Перенос информационной базы в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        public ManagerResult DetachAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParams)
        {
            var accountId = AccountIdByAccountDatabase(taskParams.AccountDatabaseId);
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => accountId);

            try
            {
                logger.Info($"[{taskParams.AccountDatabaseId}] :: Подготовка базы перед архивацией в склеп");

                archiveAccountDatabaseToTombProvider.DetachAccountDatabaseToTomb(taskParams);

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка при отправке в склеп информационной базы]{taskParams.AccountDatabaseId}";
                _handlerException.Handle(ex, message);
                return PreconditionFailed($"{message}: {ex.GetFullInfo(false)}.");
            }
        }
    }
}
