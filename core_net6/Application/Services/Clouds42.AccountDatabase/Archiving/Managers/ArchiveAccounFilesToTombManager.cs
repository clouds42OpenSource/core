﻿using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Archiving.Managers
{
    /// <summary>
    /// Менеджер архивации файлов аккаунта в склеп
    /// </summary>
    public class ArchiveAccounFilesToTombManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IArchiveAccounFilesToTombProvider archiveAccounFilesToTombProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        ///     Перенести файлы аккаунта в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        public ManagerResult DetachAccounFilesToTomb(DeleteAccountFilesToTombJobParamsDto taskParams)
        {
            AccessProvider.HasAccess(ObjectAction.AccountFilesToTomb, () => taskParams.AccountId);

            try
            {
                logger.Info($"[{taskParams.AccountId}] :: Подготовка файлов аккаунта перед архивацией в склеп");
                archiveAccounFilesToTombProvider.DetachAccounFilesToTomb(taskParams);

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка при отправке в склеп файлов аккаунта] {taskParams.AccountId}";
                _handlerException.Handle(ex, message);
                return PreconditionFailed($"{message}: {ex.GetFullInfo(false)}.");
            }
        }

    }
}
