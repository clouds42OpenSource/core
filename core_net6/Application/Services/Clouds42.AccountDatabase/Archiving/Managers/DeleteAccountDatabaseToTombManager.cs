﻿using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Archiving.Managers
{
    /// <summary>
    /// Менеджер удаления инф. баз в склеп
    /// </summary>
    public class DeleteAccountDatabaseToTombManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IDeleteAccountDatabaseToTombProvider deleteAccountDatabaseToTombProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;


        /// <summary>
        ///     Перенос информационной базы в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        public ManagerResult DeleteAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParams)
        {
            var accountId = AccountIdByAccountDatabase(taskParams.AccountDatabaseId);
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_DeleteToTomb, () => accountId);

            try
            {
                logger.Info($"[{taskParams.AccountDatabaseId}] :: Подготовка базы перед удалением в склеп");

                deleteAccountDatabaseToTombProvider.DeleteAccountDatabaseToTomb(taskParams);

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка удаления информационной базы] {taskParams.AccountDatabaseId} в склеп ";
                _handlerException.Handle(ex, message);
                return PreconditionFailed($"{message}: {ex.GetFullInfo(false)}.");
            }
        }
    }
}
