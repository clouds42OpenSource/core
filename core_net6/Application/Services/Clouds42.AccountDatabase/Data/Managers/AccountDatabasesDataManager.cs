﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Data.Managers
{
    public interface IAccountDatabasesDataManager
    {
        /// <summary>
        /// Получить данные информационных баз
        /// по фильтру
        /// </summary>
        /// <param name="args">Модель фильтра для выбора баз</param>
        /// <returns>Данные информационных баз</returns>
        ManagerResult<AccountDatabaseDataDto> GetDatabasesDataByFilter(AccountDatabasesFilterParamsDto args);
    }

    /// <summary>
    /// Менеджер для работы с данными информационных баз
    /// </summary>
    public class AccountDatabasesDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IGetAccountDatabasesDataProcessor? getAccountDatabasesDataProcessor,
        ILogger42 logger,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountDatabasesDataManager
    {
        private readonly IGetAccountDatabasesDataProcessor _getAccountDatabasesDataProcessor = getAccountDatabasesDataProcessor;

        /// <summary>
        /// Получить данные информационных баз
        /// по фильтру
        /// </summary>
        /// <param name="args">Модель фильтра для выбора баз</param>
        /// <returns>Данные информационных баз</returns>
        public ManagerResult<AccountDatabaseDataDto> GetDatabasesDataByFilter(AccountDatabasesFilterParamsDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => args?.Filter?.AccountId);
                var data = _getAccountDatabasesDataProcessor.GetDatabasesDataByFilter(args);
                return Ok(data);
            }
            catch (Exception ex)
            {
                var errorMessage = $"При получении данных информационных баз по фильтру произошла ошибка. {ex.GetFullInfo()}";
                logger.Warn(ex, $"{errorMessage}. Id пользователя '{args.Filter.AccountUserId}'.");
                return PreconditionFailed<AccountDatabaseDataDto>(errorMessage);
            }
        }
    }
}
