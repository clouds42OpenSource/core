﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Data.Managers
{
    /// <summary>
    /// Менеджер данных для создания инф. баз
    /// </summary>
    public class CreateAccountDatabaseDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateAccountDatabaseDataProvider createAccountDatabaseDataProvider,
        IGetDatabasesStatesProvider acDbAccessesStatesProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить данные для создания инф. базы из шаблона
        /// </summary>
        /// <returns>Данные для создания инф. базы из шаблона</returns>
        public async Task<ManagerResult<CreateAcDbFromTemplateDataDto>> GetDataForCreateAcDbFromTemplate(Guid accountId, bool onlyTemplatesOnDelimiters = false)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);

                var model = await createAccountDatabaseDataProvider.GetDataForCreateAcDbFromTemplate(accountId, onlyTemplatesOnDelimiters);

                return Ok(model);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<CreateAcDbFromTemplateDataDto>(
                    $"При получении данных для создания инф. базы произошла ошибка. Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Получить список пользователей для предоставления доступа
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список пользователей для предоставления доступа</returns>
        public ManagerResult<List<UserInfoDataDto>> GetAvailableUsersForGrantAccess(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);
                var model = createAccountDatabaseDataProvider.GetAvailableUsersForGrantAccess(accountId);
                return Ok(model);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<List<UserInfoDataDto>>(
                    $"При получении списка пользователей для предоставления доступа произошла ошибка. Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Получить список пользователей для предоставления доступа в базу, восстанавливаемую из бекапа
        /// </summary>
        /// <param name="backupId">Id бекапа информационной базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список пользователей для предоставления доступа</returns>
        public ManagerResult<RestoreAcDbFromBackupDataDto> GetAvailableUsersForGrantAccessToRestoringDb(Guid backupId, Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);
                var model = createAccountDatabaseDataProvider.GetAvailableUsersForGrantAccessToRestoringDb(backupId);
                return Ok(model);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<RestoreAcDbFromBackupDataDto>(
                    $"При получении списка пользователей для предоставления доступа произошла ошибка. Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Получить статусы по запрашиваемым базам
        /// </summary>
        /// <param name="databasesIds">Массив Id баз</param>
        /// <returns>Статусы по запрашиваемым базам</returns>
        public ManagerResult<List<AccountDatabaseDc>> GetAccountDatabaseStates(Guid[] databasesIds)
        {

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, optionalCheck: () => true);
                return Ok(acDbAccessesStatesProvider.GetAccountDatabaseStates(databasesIds));
            }
            catch (AccessDeniedException ex)
            {
                return Forbidden<List<AccountDatabaseDc>>(ex.Message);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<List<AccountDatabaseDc>>(
                    $"При получении доступов по запрашиваемым базам произошла ошибка. Причина: {ex.Message}");
            }
        }
    }
}
