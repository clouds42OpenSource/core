﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Data.Managers
{
    /// <summary>
    /// Менеджер для работы с общими/базовыми
    /// данными нформационных баз
    /// </summary>
    public class AccountDatabasesCommonDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountDatabasesCommonDataProvider accountDatabasesCommonDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить общие/базовые данные
        /// для работы с информационными базами
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Общие/базовые данные для работы с информационными базами</returns>
        public ManagerResult<CommonDataForWorkingWithDatabasesDto> GetCommonDataForWorkingWithDatabases(Guid accountId,
            Guid accountUserId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => accountId);
                var accountDatabasesListInfo = accountDatabasesCommonDataProvider.GetCommonDataForWorkingWithDatabases(accountId, accountUserId);
                return Ok(accountDatabasesListInfo);
            }
            catch (Exception ex)
            {
                var errorMessage = "При получении общих/базовых данных для информационных баз произошла ошибка";
                Logger.Warn(ex, $"{errorMessage}. Id пользователя '{accountUserId}'.");
                return PreconditionFailed<CommonDataForWorkingWithDatabasesDto>(errorMessage);
            }
        }
    }
}
