﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Data.Providers
{
    /// <summary>
    /// Провайдер активации сервиса Аренда 1С при создании инф. базы
    /// </summary>
    internal class ActivateRent1CAtCreationDatabaeProvider(
        IServiceProvider container,
        ILogger42 logger) : IActivateRent1CAtCreationDatabaeProvider
    {
        /// <summary>
        /// Активировать если необходимо
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void ActivateIfNeeded(Guid accountId)
        {
            var resConfig = container.GetRequiredService<IResourcesService>().GetResourceConfig(accountId, Clouds42Service.MyEnterprise);
            if (resConfig != null)
                return;

            logger.Trace($"Начало активации ресурса {Clouds42Service.MyEnterprise}");
            var myEnterprise42CloudService = container.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountId);
            myEnterprise42CloudService.ActivateService();
        }
    }
}
