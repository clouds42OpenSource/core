﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.AccountDatabase.Data.Constants;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountDatabase.Data.Providers
{
    /// <summary>
    /// Провайдер данных для создания инф. баз
    /// </summary>
    internal class CreateAccountDatabaseDataProvider(
        ICloud42ServiceHelper cloud42ServiceHelper,
        IAccessProvider accessProvider,
        AccountDatabaseHelper accountDatabaseHelper,
        IUnitOfWork dbLayer,
        AccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IConfiguration configuration)
        : ICreateAccountDatabaseDataProvider
    {
        /// <summary>
        /// Получить данные для создания инф. базы из шаблона
        /// </summary>
        /// <returns>Данные для создания инф. базы из шаблона</returns>
        public async Task<CreateAcDbFromTemplateDataDto> GetDataForCreateAcDbFromTemplate(Guid accountId, bool onlyTemplatesOnDelimiters)
        {
            var currentUser = await accessProvider.GetUserAsync();
            var currentUserHasControlPermissions = currentUser.Groups.Contains(AccountUserGroup.CloudAdmin) ||
                                                   currentUser.Groups.Contains(AccountUserGroup.Hotline);
            var dbTemplatesList = (await GetDbTemplates(accountId, onlyTemplatesOnDelimiters))
                .OrderByDescending(x => x.IsDbTemplateDelimiters)
                .ToList();

            var myDiskInfos = cloud42ServiceHelper.GetMyDiskInfo(accountId);
            var serviceIsAllow = myDiskInfos.FreeSizeOnDisk > 0 || currentUserHasControlPermissions;
            var myDatabasesServiceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId();

            return new CreateAcDbFromTemplateDataDto
            {
                AvailableTemplates = dbTemplatesList,
                IsMainServiceAllow = serviceIsAllow,
                MyDatabasesServiceId = myDatabasesServiceId,
                AvailableUsersForGrantAccess = GetAvailableUsersForAddAcDbAccess(accountId)
                    .OrderByDescending(a => a.HasAccess).ToList()
            };
        }

        /// <summary>
        /// Получить список пользователей для предоставления доступа
        /// </summary>
        /// /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список пользователей для предоставления доступа</returns>
        public List<UserInfoDataDto> GetAvailableUsersForGrantAccess(Guid accountId)
            => GetAvailableUsersForAddAcDbAccess(accountId).OrderByDescending(a => a.HasAccess).ToList();

        /// <summary>
        /// Получить список пользователей для предоставления доступа в базу, восстанавливаемую из бекапа
        /// </summary>
        /// /// <param name="backupId">Id бекапа информационной базы</param>
        /// <returns>Список пользователей для предоставления доступа</returns>
        public RestoreAcDbFromBackupDataDto GetAvailableUsersForGrantAccessToRestoringDb(Guid backupId)
        {
            var accountDatabaseBackup =
                dbLayer.AccountDatabaseBackupRepository.FirstOrThrowException(back => back.Id == backupId,
                    $"Бекап инф. базы по ID {backupId} не найден");

            var myDatabasesServiceTypeId =
                myDatabasesServiceTypeDataProvider.GetServiceTypeIdByDatabaseId(accountDatabaseBackup.AccountDatabase
                    .Id);

            return new RestoreAcDbFromBackupDataDto
            {
                MyDatabasesServiceTypeId = myDatabasesServiceTypeId,
                AvailableUsersForGrantAccess = GetAvailableUsersForAddAccessToRestoringDb(backupId)
                    .OrderByDescending(a => a.HasAccess).ToList()
            };
        }

        /// <summary>
        /// Получить список доступных для создания шаблонов
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="onlyTemplatesOnDelimiters">Признак необходимости выбирать только шаблоны баз на разделителях</param>
        /// <returns>Список доступных для создания шаблонов</returns>
        private async Task<List<AvailableForCreateDbTemplateDto>> GetDbTemplates(Guid accountId, bool onlyTemplatesOnDelimiters)
        {
            _ = bool.TryParse(configuration["UsedLocale"], out bool needSearchByLocale);

            return await GetAvailableForCreateDbTemplates(accountId, needSearchByLocale, onlyTemplatesOnDelimiters);
        }


        /// <summary>
        /// Получить список доступных для создания базы шаблонов
        /// </summary>
        /// <param name="accountId">ID локали аккаунта</param>
        /// <param name="needSearchByLocale">Признак необходимости искать шаблоны по локали</param>
        /// <param name="needOnlyTemplateOnDelimiters">Признак необходимости выбирать только шаблоны баз на разделителях</param>
        /// <returns>Список доступных для создания базы шаблонов</returns>
        private async Task<List<AvailableForCreateDbTemplateDto>> GetAvailableForCreateDbTemplates(Guid accountId,
            bool needSearchByLocale, bool needOnlyTemplateOnDelimiters)
        {
            var isVipAccount = accountConfigurationDataProvider.IsVipAccount(accountId);
            bool needAutoCreateClusterDatabase = accountConfigurationDataProvider.NeedAutoCreateClusteredDb(accountId);
            var accountLocaleId = accountConfigurationDataProvider.GetAccountLocaleId(accountId);
            var needAttachDbTemplateOnDelimitersData = needOnlyTemplateOnDelimiters || !isVipAccount;
            bool showOnlyClearTemplates = isVipAccount && needAutoCreateClusterDatabase;
            
            var availableForCreateDbTemplates = await
                GenerateListOfCreateDbTemplates(accountLocaleId,
                    needSearchByLocale,
                    needOnlyTemplateOnDelimiters,
                    needAttachDbTemplateOnDelimitersData,
                    showOnlyClearTemplates);

            if (needAttachDbTemplateOnDelimitersData)
                availableForCreateDbTemplates.ForEach(x =>
                    x.DbTemplateOnDelimitersDemoData =
                        CreateDbTemplateOnDelimitersDemoDataDto(x.Id, accountId));

            return availableForCreateDbTemplates;
        }

        /// <summary>
        /// Сформировать список доступных для создания шаблонов инф.баз
        /// </summary>
        /// <param name="accountLocaleId">Id локали аккаунта</param>
        /// <param name="needSearchByLocale">Признак необходимости искать шаблоны по локали</param>
        /// <param name="needOnlyTemplateOnDelimiters">Признак необходимости выбирать только шаблоны баз на разделителях</param>
        /// <param name="needAttachDbTemplateOnDelimitersData">Признак необходимости добавлять в выборку шаблоны баз на разделителях</param>
        /// <param name="showOnlyClearTemplates">Признак того, что нужно отдаваь только чистые шаблоны</param>
        /// <returns>Список доступных для создания базы шаблонов</returns>
        private async Task<List<AvailableForCreateDbTemplateDto>> GenerateListOfCreateDbTemplates(Guid accountLocaleId, 
            bool needSearchByLocale, 
            bool needOnlyTemplateOnDelimiters,
            bool needAttachDbTemplateOnDelimitersData,
            bool showOnlyClearTemplates)
        {
            return await dbLayer.DbTemplateRepository
                .AsQueryableNoTracking()
                .Include(x => x.ConfigurationDbTemplateRelations)
                .ThenInclude(x => x.Configuration1C)
                .ThenInclude(x => x.ConfigurationServiceTypeRelation)
                .Include(x => x.Delimiters)
                .Where(x => !x.Name.Contains(AccountDatabaseDataProviderConstants.DbTemplateDemoFlagSubstring) &&
                            (!needSearchByLocale || x.LocaleId == accountLocaleId) &&
                            (!needOnlyTemplateOnDelimiters || x.Delimiters.Any()))
                .Select(x => new AvailableForCreateDbTemplateDto
                {
                    Caption = x.DefaultCaption,
                    CaptionOnDelimiters = x.Delimiters.Any() ? x.Delimiters.FirstOrDefault()!.Name : x.DefaultCaption,
                    Id = x.Id,
                    IsDemoDataAvailable = x.DemoTemplateId.HasValue,
                    CanWebPublish = x.CanWebPublish,
                    TemplateImgUrl = x.ImageUrl,
                    IsDbTemplateDelimiters =
                        x.Delimiters.Any() && needAttachDbTemplateOnDelimitersData,
                    VersionTemplate = needAttachDbTemplateOnDelimitersData
                        ? x.Delimiters.Any() ? x.Delimiters.FirstOrDefault()!.ConfigurationReleaseVersion : null
                        : null,
                    MyDatabasesServiceTypeId = x.ConfigurationDbTemplateRelations.Any()
                        ? x.ConfigurationDbTemplateRelations.FirstOrDefault()!.Configuration1C
                            .ConfigurationServiceTypeRelation.ServiceTypeId : null
                })
                .ToListAsync();
        }

        /// <summary>
        /// Создать модель демо данных для шаблона базы на разделителях
        /// </summary>
        /// <param name="templateId">Id шаблона информационной базы</param>
        /// <param name="accountId">Id аккаунта</param>
        private DbTemplateOnDelimitersDemoDataDto CreateDbTemplateOnDelimitersDemoDataDto(Guid templateId, Guid accountId)
        {
            var dbTemplateOnDelimitersDemoDataDto = new DbTemplateOnDelimitersDemoDataDto();

            var dbOnDelimitersWithDemoData = accountDatabaseHelper.FindDatabaseDelimiterDemo(accountId, templateId);

            if (dbOnDelimitersWithDemoData == null)
                return dbTemplateOnDelimitersDemoDataDto;

            dbTemplateOnDelimitersDemoDataDto.HasDbOnDelimitersWithDemoData = true;
            dbTemplateOnDelimitersDemoDataDto.WebPublishPath =
                accountDatabaseWebPublishPathHelper.GetWebPublishPath(dbOnDelimitersWithDemoData.AccountDatabase);

            return dbTemplateOnDelimitersDemoDataDto;
        }

        /// <summary>
        /// Выбрать данные пользователей аккаунта для предоставления доступа
        /// </summary>
        /// <returns>Данные пользователей аккаунта для предоставления доступа</returns>
        private IQueryable<UserInfoDataDto> GetAvailableUsersForAddAcDbAccess(Guid accountId)
        {
            return dbLayer.AccountUsersRepository
                .AsQueryableNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.BillingAccount)
                .ThenInclude(x => x.Resources)
                .Select(x => new UserInfoDataDto
                {
                    UserId = x.Id,
                    UserLogin = x.Login,
                    UserEmail = x.Email,
                    UserLastName = x.LastName,
                    UserFirstName = x.FirstName,
                    UserMiddleName = x.MiddleName,
                    AccountIndexNumber = x.Account.IndexNumber,
                    AccountCaption = x.Account.AccountCaption,
                    HasAccess = x.AccountUserRoles.Any(
                        role => role.AccountUserGroup == AccountUserGroup.AccountAdmin),
                    AccountId = x.AccountId,
                    IsDeleted = x.CorpUserSyncStatus == "Deleted" || x.CorpUserSyncStatus == "SyncDeleted",
                    HasRent = x.Account.BillingAccount.Resources.Any(z =>
                        z.Subject == x.Id && (z.BillingServiceType.SystemServiceType == ResourceType.MyEntUser ||
                                              z.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb))
                })
                .Where(x => x.AccountId == accountId && x.HasRent && !x.IsDeleted);
        }

        /// <summary>
        /// Получить список пользователей для предоставления доступа в базу, восстанавливаемую из бекапа
        /// </summary>
        /// <param name="backupId">Id бекапа информационной базы</param>
        /// <returns></returns>
        private IQueryable<UserInfoDataDto> GetAvailableUsersForAddAccessToRestoringDb(Guid backupId)
        {
            return dbLayer.AccountDatabaseBackupRepository
                .AsQueryableNoTracking()
                .Include(x => x.AccountDatabase)
                .ThenInclude(x => x.Account)
                .ThenInclude(x => x.AccountUsers)
                .Include(x => x.AccountDatabase)
                .ThenInclude(x => x.AcDbAccesses)
                .ThenInclude(x => x.ManageDbOnDelimitersAccess)
                .Include(x => x.AccountDatabase)
                .ThenInclude(x => x.Account)
                .ThenInclude(x => x.BillingAccount)
                .ThenInclude(x => x.Resources)
                .SelectMany(x => x.AccountDatabase.Account.AccountUsers.Select(user => new UserInfoDataDto
                {
                    BackupId = x.Id,
                    UserId = user.Id,
                    UserLogin = user.Login,
                    UserEmail = user.Email,
                    UserLastName = user.LastName,
                    UserFirstName = user.FirstName,
                    UserMiddleName = user.MiddleName,
                    AccountIndexNumber = x.AccountDatabase.Account.IndexNumber,
                    AccountCaption = x.AccountDatabase.Account.AccountCaption,
                    HasAccess = x.AccountDatabase.AcDbAccesses.Any(z => z.AccountID == x.AccountDatabase.AccountId && z.AccountUserID  == user.Id),
                    HasRent = x.AccountDatabase.Account.BillingAccount.Resources.Any(y => (y.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb ||
                        y.BillingServiceType.SystemServiceType == ResourceType.MyEntUser) && y.Subject == user.Id),
                    IsDeleted = user.CorpUserSyncStatus == "Deleted" || user.CorpUserSyncStatus == "SyncDeleted"
                }))
                .Where(x => (x.HasRent || x.HasAccess) && !x.IsDeleted && x.BackupId == backupId);
        }
    }
}
