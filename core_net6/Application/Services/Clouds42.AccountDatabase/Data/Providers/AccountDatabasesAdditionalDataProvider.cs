﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.AccountDatabase.Contracts.DbTemplates.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Data.Providers
{
    /// <summary>
    /// Провайдер для работы с дополнительными
    /// данными информационных баз
    /// </summary>
    internal class AccountDatabasesAdditionalDataProvider(IDbTemplatesProvider dbTemplatesProvider)
        : IAccountDatabasesAdditionalDataProvider
    {
        /// <summary>
        /// Получить дополнительные данные для работы с информационными базами
        /// </summary>
        /// <returns>Дополнительные данные для работы с информационными базами</returns>
        public AdditionalDataForWorkingWithDatabasesDto GetAdditionalDataForWorkingWithDatabases() =>
            new()
            {
                AvailableDatabaseStatuses = GetAvailableDatabaseStatuses(),
                AvailablePlatformTypes = GetAvailablePlatformTypes(),
                AvailableAccountDatabaseRestoreModelTypes = GetAvailableAccountDatabaseRestoreModelTypes(),
                AvailableDbTemplates = GetAvailableDbTemplates(),
                ServiceExtensionsForDatabaseTypes = GetServiceExtensionsForDatabaseTypes(),
            };

        /// <summary>
        /// Получить доступные для выбора статусы инф. базы
        /// </summary>
        /// <returns>Доступные для выбора статусы инф. базы</returns>
        private List<KeyValueDto<int>> GetAvailableDatabaseStatuses() =>
            typeof(DatabaseState).GenerateKeyValueDtoList(
                DatabaseState.Undefined, DatabaseState.Attaching,
                DatabaseState.Detached, DatabaseState.TransferArchive);

        /// <summary>
        /// Получить доступные для выбора типы платформы
        /// </summary>
        /// <returns>Доступные для выбора типы платформы</returns>
        private List<KeyValueDto<int>> GetAvailablePlatformTypes() =>
            typeof(PlatformType).GenerateKeyValueDtoList(PlatformType.Undefined);

        /// <summary>
        /// Получить доступные для выбора модели восстановления инф. базы
        /// </summary>
        /// <returns>Доступные для выбора модели восстановления инф. базы</returns>
        private List<KeyValueDto<int>> GetAvailableAccountDatabaseRestoreModelTypes() =>
            typeof(AccountDatabaseRestoreModelTypeEnum).GenerateKeyValueDtoList(
                AccountDatabaseRestoreModelTypeEnum.Mixed);

        /// <summary>
        /// Получить типы расшений для инф. базы
        /// </summary>
        /// <returns>Типы расшений для инф. базы</returns>
        private List<KeyValueDto<int>> GetServiceExtensionsForDatabaseTypes() =>
            typeof(ServiceExtensionsForDatabaseTypeEnum).GenerateKeyValueDtoList();

        /// <summary>
        /// Получить доступные шаблоны инф. баз
        /// </summary>
        /// <returns>Доступные шаблоны инф. баз</returns>
        private List<KeyValueDto<Guid>> GetAvailableDbTemplates() =>
            dbTemplatesProvider.GetAllDbTemplates().ToList()
                .Select(t => new KeyValueDto<Guid>(t.Id, t.DefaultCaption)).ToList();
    }
}
