﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;

namespace Clouds42.AccountDatabase.Data.Providers
{
    /// <summary>
    /// Провайдер для работы с общими/базовыми
    /// данными нформационных баз
    /// </summary>
    internal class AccountDatabasesCommonDataProvider(
        IAccountDataProvider accountDataProvider,
        IServiceStatusHelper serviceStatusHelper,
        IAccessProvider accessProvider,
        IRent1CClientDataProvider rent1CClientDataProvider,
        IAccountDatabasesAdditionalDataProvider accountDatabasesAdditionalDataProvider,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IAccountDatabasesCommonDataProvider
    {
        /// <summary>
        /// Получить общие/базовые данные
        /// для работы с информационными базами
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Общие/базовые данные для работы с информационными базами</returns>
        public CommonDataForWorkingWithDatabasesDto GetCommonDataForWorkingWithDatabases(Guid accountId,
            Guid accountUserId)
        {
            var serviceStatusInfo = serviceStatusHelper.CreateModelForRentAndMyDisk(accountId);
            var accountUserGroups = accessProvider.GetGroups(accountUserId);

            return new CommonDataForWorkingWithDatabasesDto
            {
                AccountId = accountId,
                AccountUserId = accountUserId,
                IsVipAccount = accountConfigurationDataProvider.IsVipAccount(accountId),
                AccountLocaleName = accountConfigurationDataProvider.GetAccountLocale(accountId).Name,
                MyDatabaseServiceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId(),
                AccessToServerDatabaseServiceTypeId = myDatabasesServiceTypeDataProvider.GetAccessToServerDatabaseServiceTypeId(),
                AccountAdminInfo = accountDataProvider.GetAccountAdminDescription(accountId),
                AccountAdminPhoneNumber = accountDataProvider.GetAccountAdminPhoneNumber(accountId),
                PermissionsForWorkingWithDatabase = GetPermissionsForWorkingWithDatabase(accountId, accountUserId, accountUserGroups),
                MainServiceStatusInfo = serviceStatusInfo,
                AllowAddingDatabaseInfo = GetAllowAddingDatabaseInfo(),
                IsMainServiceAllowed = IsMainServiceAllowed(serviceStatusInfo.ServiceIsLocked, accountUserGroups),
                AdditionalDataForWorkingWithDatabases = accountDatabasesAdditionalDataProvider.GetAdditionalDataForWorkingWithDatabases()
            };
        }

        /// <summary>
        /// Определить доступен ли главный сервис Аренда 1С
        /// </summary>
        /// <param name="isLockedMainService">Главный сервис заблокирован</param>
        /// <param name="accountUserGroups">Список ролей пользователя</param>
        /// <returns>Главный сервис Аренда 1С доступен</returns>
        private bool IsMainServiceAllowed(bool isLockedMainService,
            List<AccountUserGroup> accountUserGroups) =>
            accountUserGroups.Any(r => r == AccountUserGroup.CloudAdmin || r == AccountUserGroup.Hotline) ||
            !isLockedMainService;

        /// <summary>
        /// Получить модель разрешений для работы с базой данных
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="accountUserGroups">Список ролей пользователя</param>
        /// <returns>Модель разрешений для работы с базой данных</returns>
        private PermissionsForWorkingWithDatabaseDto GetPermissionsForWorkingWithDatabase(Guid accountId,
            Guid accountUserId, List<AccountUserGroup> accountUserGroups)
        {
            var rent1CPermissions = rent1CClientDataProvider.GetRent1CClientPermissions(accountId, accountUserId);

            return new PermissionsForWorkingWithDatabaseDto
            {
                HasPermissionForRdp = rent1CPermissions.HasPermissionForRdp,
                HasPermissionForWeb = rent1CPermissions.HasPermissionForWeb,
                HasPermissionForDisplaySupportData =
                    accessProvider.HasAccessBool(ObjectAction.AccountDatabases_DisplaySupportDb, () => accountId),
                HasPermissionForCreateAccountDatabase =
                    accessProvider.HasAccessBool(ObjectAction.AccountDatabases_Add, () => accountId),
                HasPermissionForDisplayPayments =
                    accessProvider.HasAccessBool(ObjectAction.Payments_View, () => accountId),
                HasPermissionForMultipleActionsWithDb = HasPermissionForMultipleActionsWithDb(accountId),
                HasPermissionForDisplayAutoUpdateButton = HasPermissionForDisplayAutoUpdateButton(accountUserGroups)
            };
        }

        /// <summary>
        /// Определить имеет ли разрешение пользователь на отображение кнопки АО
        /// </summary>
        /// <param name="accountUserGroups">Список ролей пользователя</param>
        /// <returns>Имеет разрешение на отображение кнопки АО</returns>
        private bool HasPermissionForDisplayAutoUpdateButton(List<AccountUserGroup> accountUserGroups) =>
            accountUserGroups.Any(g =>
                g == AccountUserGroup.CloudAdmin || g == AccountUserGroup.Hotline ||
                g == AccountUserGroup.AccountSaleManager || g == AccountUserGroup.AccountAdmin);

        /// <summary>
        /// определить имеет ли разрешение пользователь
        /// на множественные действия с базой
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Имеет разрешение на множественные действия с базой</returns>
        private bool HasPermissionForMultipleActionsWithDb(Guid accountId) =>
            accessProvider.HasAccessBool(ObjectAction.AccountDatabase_DeleteToTomb, () => accountId) ||
            accessProvider.HasAccessBool(ObjectAction.AccountDatabase_OfArchiveToTomb, () => accountId);

        /// <summary>
        /// Получить информацию о разрешении добавления информационной базы
        /// </summary>
        /// <returns>Информация о разрешении добавления информационной базы</returns>
        private AllowAddingDatabaseInfoDto GetAllowAddingDatabaseInfo()
            => new()
            {
                IsAllowedCreateDb = true,
                ForbiddeningReasonMessage = string.Empty
            };
    }
}
