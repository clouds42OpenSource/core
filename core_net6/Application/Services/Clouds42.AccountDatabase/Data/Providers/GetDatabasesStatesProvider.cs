﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Data.Providers
{
    /// <summary>
    /// Провайдер для получения статусов баз
    /// </summary>
    internal class GetDatabasesStatesProvider(
        IUnitOfWork dbLayer,
        AccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        IAccessProvider accessProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IGetDatabasesStatesProvider
    {
        private readonly Lazy<IUserPrincipalDto> _currentUser = new Lazy<IUserPrincipalDto>(accessProvider.GetUser);

        /// <summary>
        ///     Получить статусы по запрашиваемым базам
        /// </summary>
        /// <param name="databasesIds">Массив Id баз</param>
        /// <returns>Статусы по запрашиваемым базам</returns>
        public List<AccountDatabaseDc> GetAccountDatabaseStates(Guid[] databasesIds)
        {
            if (databasesIds == null || !databasesIds.Any())
                return [];

            var databases = dbLayer.DatabasesRepository.WhereLazy(w => databasesIds.Contains(w.Id)).ToList();

            var models = new List<AccountDatabaseDc>();
            databases.ForEach(database =>
            {
                var model = InitAccountDatabaseDc(database);
                models.Add(model);
            });
            return models;
        }

        /// <summary>
        /// Инициализировать модель отображения
        /// информационной базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Модель отображения информационной базы</returns>
        private AccountDatabaseDc InitAccountDatabaseDc(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var locale = accountConfigurationDataProvider.GetAccountLocale(accountDatabase.AccountId);

            return new AccountDatabaseDc
            {
                Id = accountDatabase.Id,
                V82Name = accountDatabase.V82Name,
                Caption = accountDatabase.Caption,
                TemplateName = accountDatabase.GetTemplateName(),
                LastActivityDate =
                    TimezoneHelper.DateTimeToTimezone(locale.Name, accountDatabase.LastActivityDate),
                SizeInMB = accountDatabase.SizeInMB,
                State = accountDatabase.StateEnum,
                CreateAccountDatabaseComment = accountDatabase.CreateAccountDatabaseComment,
                TemplateImgUrl = accountDatabase.GetTemplateImgUrl(),
                PublishState = accountDatabase.PublishStateEnum,
                WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(accountDatabase),
                NeedShowWebLink = CheckUserAccess(accountDatabase.Id),
                TimezoneName = TimezoneHelper.GetTimezone(locale.Name),
                IsDeleted = accountDatabase.IsDeleted(),
                IsDbOnDelimiters = accountDatabase.IsDelimiter(),
                IsDemoDelimiters = accountDatabase.IsDemoDelimiter(),
                DatabaseConnectionPath = accountDatabaseDataHelper.GetDatabaseConnectionPath(accountDatabase)
            };
        }

        /// <summary>
        /// Проверить доступ пользователя
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>true - если у пользователя есть доступ к базе</returns>
        private bool CheckUserAccess(Guid accountDatabaseId)
        {
            var currentUser = _currentUser.Value;
            if (currentUser == null)
                throw new InvalidOperationException("Не удалось получить текущего пользователя");

            if (currentUser.Groups.Any(g =>
                g == AccountUserGroup.CloudAdmin ||
                g == AccountUserGroup.Hotline ||
                g == AccountUserGroup.CloudSE))
                return true;

            var acDbAccess = dbLayer.AcDbAccessesRepository.FirstOrDefault(w =>
                w.AccountUserID == currentUser.Id && w.AccountDatabaseID == accountDatabaseId);

            return acDbAccess != null;
        }
    }
}
