﻿namespace Clouds42.AccountDatabase.Data.Constants
{
    /// <summary>
    /// Содержит константы к шаблонам инф.баз
    /// </summary>
    public static class AccountDatabaseDataProviderConstants
    {
        /// <summary>
        /// Подстрока, указывающая на признак демо шаблона базы
        /// </summary>
        public const string DbTemplateDemoFlagSubstring = "demo";

        /// <summary>
        /// Имя отображения конфигурации для чистого шаблона
        /// </summary>
        public const string ClearTemplateConfigurationName = "Своя конфигурация";

        /// <summary>
        /// Имя шаблона чистый шаблон
        /// </summary>
        public const string ClearTemplateName = "Чистый шаблон";

    }
}
