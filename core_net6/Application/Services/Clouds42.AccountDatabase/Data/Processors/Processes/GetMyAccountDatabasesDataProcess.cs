﻿using Clouds42.AccountDatabase.Data.Processors.Models;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Data.Processors.Processes
{
    /// <summary>
    /// Процесс для получения данных информационных баз
    /// с типом принадлежности "Мои информационные базы"
    /// </summary>
    public class GetMyAccountDatabasesDataProcess(IServiceProvider serviceProvider)
        : GetAccountDatabasesDataBaseProcess(serviceProvider)
    {
        /// <summary>
        /// Получить выбранные информационные базы
        /// </summary>
        /// <param name="filter">Модель фильтра информационных баз</param>
        /// <returns>Выбранные информационные базы</returns>
        protected override IQueryable<Domain.DataModels.AccountDatabase> GetSelectedDatabases(AccountDatabasesFilterDto filter)
        {
            var databases = GetNotDeletedDatabases(filter.AccountId);
            return DiscardDatabasesByUserGroups(filter.AccountUserId, databases);
        }

        /// <summary>
        /// Получить название инф. базы
        /// </summary>
        /// <param name="model">Модель данных об информационной базе для выборки</param>
        /// <returns>Название инф. базы</returns>
        protected override string GetAccountDatabaseCaption(DatabaseDataForSampleModel model)
            => $"{model.Database.Caption} ({model.Database.V82Name})";

        /// <summary>
        /// Получить не удаленные информационные базы аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Не удаленные информационные базы аккаунта</returns>
        private IQueryable<Domain.DataModels.AccountDatabase> GetNotDeletedDatabases(Guid accountId) =>
            DbLayer.DatabasesRepository.WhereLazy(acDb =>
                acDb.AccountId == accountId && acDb.State != DatabaseState.Undefined.ToString() &&
                acDb.State != DatabaseState.DeletedFromCloud.ToString());

        /// <summary>
        /// Отбросить/убрать информационные базы
        /// по ролям пользователя
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="databases">Информационные базы</param>
        /// <returns>Новый список информационных баз</returns>
        private IQueryable<Domain.DataModels.AccountDatabase> DiscardDatabasesByUserGroups(Guid accountUserId,
            IQueryable<Domain.DataModels.AccountDatabase> databases)
        {
            var groups = GetAccountUserGroups(accountUserId);

            if (groups.Any(r => r == AccountUserGroup.CloudAdmin || r == AccountUserGroup.Hotline))
                return databases;

            databases = databases.Where(w =>
                w.State != DatabaseState.DelitingToTomb.ToString());

            if (groups.Any(r => r != AccountUserGroup.AccountUser))
                return databases;

            return from db in databases
                join ac in DbLayer.AcDbAccessesRepository.WhereLazy() on db.Id equals ac.AccountDatabaseID
                where accountUserId == ac.AccountUserID
                select db;
        }
    }
}
