﻿using Clouds42.AccountDatabase.Data.Processors.Models;
using Clouds42.AccountDatabase.Data.Processors.Selectors;
using Clouds42.AccountDatabase.Data.Processors.Utilities;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;
using PagedList.Core;

namespace Clouds42.AccountDatabase.Data.Processors.Processes
{
    /// <summary>
    /// Базовый процесс для получения данных информационных баз
    /// </summary>
    public abstract class GetAccountDatabasesDataBaseProcess
    {
        protected readonly IUnitOfWork DbLayer;
        private readonly IAccessProvider _accessProvider;
        private readonly AccountDatabaseWebPublishPathHelper _accountDatabaseWebPublishPathHelper;
        private readonly DatabaseDataForSampleModelSelector _databaseDataForSampleModelSelector;
        private readonly Lazy<IUserPrincipalDto> _currentUser;

        protected GetAccountDatabasesDataBaseProcess(IServiceProvider serviceProvider)
        {
            DbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
            _accessProvider = serviceProvider.GetRequiredService<IAccessProvider>();
            _accountDatabaseWebPublishPathHelper = serviceProvider.GetRequiredService<AccountDatabaseWebPublishPathHelper>();
            _databaseDataForSampleModelSelector = serviceProvider.GetRequiredService<DatabaseDataForSampleModelSelector>();
            _currentUser = new Lazy<IUserPrincipalDto>(_accessProvider.GetUser);
        }

        /// <summary>
        /// Получить выбранные информационные базы
        /// </summary>
        /// <param name="filter">Модель фильтра информационных баз</param>
        /// <returns>Выбранные информационные базы</returns>
        protected abstract IQueryable<Domain.DataModels.AccountDatabase> GetSelectedDatabases(AccountDatabasesFilterDto filter);

        /// <summary>
        /// Получить название инф. базы
        /// </summary>
        /// <param name="model">Модель данных об информационной базе для выборки</param>
        /// <returns>Название инф. базы</returns>
        protected abstract string GetAccountDatabaseCaption(DatabaseDataForSampleModel model);

        /// <summary>
        /// Получить список ролей пользователя
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Список ролей пользователя</returns>
        protected List<AccountUserGroup> GetAccountUserGroups(Guid accountUserId) =>
            _accessProvider.GetGroups(accountUserId);

        /// <summary>
        /// Признак что у текущего пользователя есть права на администрирование облака
        /// </summary>
        /// <returns>true - если роль пользователя выше "Хотлайн"</returns>
        protected bool IsCurrentUserHasAdminRoles()
        {
            var currentUserGroups = GetCurrentUser().Groups;
            return currentUserGroups.Any(gr =>
                gr is AccountUserGroup.CloudAdmin or AccountUserGroup.Hotline or AccountUserGroup.CloudSE);
        }

        /// <summary>
        /// Признак что текущий пользователь является сейл менеджером
        /// </summary>
        /// <returns>true - если у пользователя есть роль сейл менеджера</returns>
        protected bool IsCurrentUserSaleManager()
            => GetCurrentUser().Groups.Any(gr => gr == AccountUserGroup.AccountSaleManager);

        /// <summary>
        /// Получить данные информационных баз
        /// по фильтру
        /// </summary>
        /// <param name="args">Модель фильтра для выбора баз</param>
        /// <returns>Данные информационных баз</returns>
        public AccountDatabaseDataDto GetDatabasesDataByFilter(AccountDatabasesFilterParamsDto args)
        {
            var selectedDatabases = GetDatabaseDataForSampleModels(args.Filter);
            var databasesData = selectedDatabases.ApplyFilterBySearchString(args.Filter.SearchString);
            
            CountDatabasesFromSelection(databasesData, out var archivedDatabasesCount,
                out var serverDatabasesCount, out var allDatabasesCount, out int fileDatabasesCount, out int delimiterDatabasesCount,
                out int deletedDatabases);

            if (args.Filter.isNewPage)
                databasesData = databasesData
                    .ApplyFilterByCondition(args.Filter.AccountDatabaseTypeEnum)
                    .MakeSorting(args.SortingData);
            else
                databasesData = databasesData
                    .ApplyFilterByCondition(args.Filter.IsServerDatabaseOnly, args.Filter.IsArchievedDatabaseOnly)
                    .MakeSorting(args.SortingData);

            var databasesDataCount = databasesData.Count();
            var pageNumber = args?.PageNumber is null or <= 0 ? 1 : args.PageNumber.Value;

            return new AccountDatabaseDataDto
            {
                Pagination = new PaginationBaseDto(pageNumber, databasesDataCount, args?.Filter?.PageSize ?? 50),
                ArchievedDatabasesCount = archivedDatabasesCount,
                ServerDatabasesCount = serverDatabasesCount,
                FileDatabasesCount = fileDatabasesCount,
                DelimeterDatabasesCount = delimiterDatabasesCount,
                DeletedDatabases = deletedDatabases, 
                AllDatabasesCount = allDatabasesCount,
                Records = databasesData
                    .ToPagedList(pageNumber, args?.Filter?.PageSize ?? 10)
                    .Select(TransferToAccountDatabaseDataItemDto)
                    .ToArray()
            };
        }

        /// <summary>
        /// Посчитать количество инф. баз в выборке
        /// </summary>
        /// <param name="databasesData">Список моделей инф. баз</param>
        /// <param name="archivedDatabasesCount">Кол-во инф.баз в архиве</param>
        /// <param name="serverDatabasesCount">Кол-во серверных баз</param>
        /// <param name="allDatabasesCount">Кол-во всех инф. баз</param>
        /// <param name="fileDatabasesCount"></param>
        /// <param name="delimiterDatabasesCount"></param>
        /// <param name="deletedDatabasesCount"></param>
        private void CountDatabasesFromSelection(IQueryable<DatabaseDataForSampleModel> databasesData, out int archivedDatabasesCount,
            out int serverDatabasesCount, out int allDatabasesCount, out int fileDatabasesCount, out int delimiterDatabasesCount,
            out int deletedDatabasesCount)
        {
            allDatabasesCount = databasesData.Count(acDb => acDb.Database.State != DatabaseState.DeletedToTomb.ToString() &&
            acDb.Database.State != DatabaseState.DetachedToTomb.ToString());

            archivedDatabasesCount =
               databasesData.Count(acDb => acDb.Database.State == DatabaseState.DetachedToTomb.ToString());

            serverDatabasesCount = databasesData.Count(x =>
                (x.Database.IsFile == null || x.Database.IsFile == false) &&
                x.Database.AccountDatabaseOnDelimiter == null && x.Database.State != DatabaseState.DeletedToTomb.ToString()
                && x.Database.State != DatabaseState.DetachedToTomb.ToString());

            fileDatabasesCount = databasesData.Count(x => x.Database.IsFile == true && x.Database.State != DatabaseState.DeletedToTomb.ToString()
            && x.Database.State != DatabaseState.DetachedToTomb.ToString());

            delimiterDatabasesCount = databasesData.Count(x => x.Database.AccountDatabaseOnDelimiter != null && x.Database.State != DatabaseState.DeletedToTomb.ToString()
            && x.Database.State != DatabaseState.DetachedToTomb.ToString());

            deletedDatabasesCount = databasesData.Count(x => x.Database.State == DatabaseState.DeletedToTomb.ToString());
        }

        /// <summary>
        /// Получить модели данных информационных баз для выборки
        /// </summary>
        /// <param name="filter">Модель фильтра</param>
        /// <returns>Модели данных информационных баз</returns>
        private IQueryable<DatabaseDataForSampleModel> GetDatabaseDataForSampleModels(AccountDatabasesFilterDto filter)
        {
            return _databaseDataForSampleModelSelector.SelectModels(GetSelectedDatabases(filter), filter.AccountUserId);
        }
        /// <summary>
        /// Переложить в модель описания информационной базы
        /// </summary>
        /// <param name="model">Модель данных об информационной базе для выборки</param>
        /// <returns>Модель описания информационной базы</returns>
        private AccountDatabaseDataItemDto TransferToAccountDatabaseDataItemDto(DatabaseDataForSampleModel model)
        {
            var needShowWebLink = (IsCurrentUserHasAdminRoles() || model.HasAccessToDatabase) && !model.Database.IsDeleted();
            var accountLocale = string.IsNullOrEmpty(model.AccountLocale) ? LocaleConstantsDto.Rus : model.AccountLocale;

            var templateCaption = string.IsNullOrEmpty(model.TemplateCaption)
                ? ""
                : model.TemplateCaption;

            return new AccountDatabaseDataItemDto
            {
                Id = model.Database.Id,
                Name = GetAccountDatabaseCaption(model),
                SizeInMb = model.Database.SizeInMB,
                State = model.Database.StateEnum,
                IsFile = model.Database?.IsFile ?? true,
                LastActivityDate = TimezoneHelper.DateTimeToTimezone(accountLocale, model.Database.LastActivityDate),
                DatabaseLaunchLink = _accountDatabaseWebPublishPathHelper.GetWebPublishPath(model.Database.Id),
                IsDbOnDelimiters = model.IsDbOnDelimiters,
                IsDemo = model.IsDemo,
                TemplateCaption = templateCaption,
                TemplateImageName = string.IsNullOrEmpty(model.TemplateImageName) ? "" : model.TemplateImageName,
                PublishState = model.Database.PublishStateEnum,
                HasSupport = model.HasSupport,
                HasAutoUpdate = model.HasAutoUpdate,
                CreateAccountDatabaseComment = model.Database.CreateAccountDatabaseComment,
                NeedShowWebLink = needShowWebLink,
                HasAcDbSupport = model.HasAcDbSupport,
                IsExistSessionTerminationProcess = model.IsExistSessionTerminationProcess,
                ConfigurationName = model.ConfigurationName, 
                V82Name = model.Database.V82Name
            };
        }

        /// <summary>
        /// Получить текущего пользователя
        /// </summary>
        /// <returns>Текущий пользователь</returns>
        private IUserPrincipalDto GetCurrentUser()
        {
            var currentUser = _currentUser.Value;

            return currentUser ?? throw new InvalidOperationException("Не удалось получить текущего пользователя");
        }
    }
}
