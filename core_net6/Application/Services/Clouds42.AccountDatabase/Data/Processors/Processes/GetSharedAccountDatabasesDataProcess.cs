﻿using Clouds42.AccountDatabase.Data.Processors.Models;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Data.Processors.Processes
{
    /// <summary>
    /// Процесс для получения данных информационных баз
    /// с типом принадлежности "Доступные информационные базы других аккаунтов"
    /// </summary>
    public class GetSharedAccountDatabasesDataProcess(IServiceProvider serviceProvider)
        : GetAccountDatabasesDataBaseProcess(serviceProvider)
    {
        /// <summary>
        /// Получить выбранные информационные базы
        /// </summary>
        /// <param name="filter">Модель фильтра информационных баз</param>
        /// <returns>Выбранные информационные базы</returns>
        protected override IQueryable<Domain.DataModels.AccountDatabase> GetSelectedDatabases(AccountDatabasesFilterDto filter)
        {
            var acDbAccesses = GetAcDbAccessesByUserGroups(filter);
            return GetSharedDatabases(filter.AccountId, acDbAccesses);
        }

        /// <summary>
        /// Получить название инф. базы
        /// </summary>
        /// <param name="model">Модель данных об информационной базе для выборки</param>
        /// <returns>Название инф. базы</returns>
        protected override string GetAccountDatabaseCaption(DatabaseDataForSampleModel model)
        {
            var commonDatabaseName = $"{model.Database.Caption} ({model.Database.Account.AccountCaption})";

            return $"{commonDatabaseName} ({model.Database.V82Name})";
        }

        /// <summary>
        /// Получить расшаренные информационные базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="acDbAccesses">Доступы к информационным базам</param>
        /// <returns>Расшаренные информационные базы</returns>
        private IQueryable<Domain.DataModels.AccountDatabase> GetSharedDatabases(Guid accountId, IQueryable<AcDbAccess> acDbAccesses) =>
            (from access in acDbAccesses
                join database in DbLayer.DatabasesRepository.WhereLazy(w =>
                    w.State == DatabaseState.Ready.ToString()) on access.AccountDatabaseID equals database.Id
                where database.AccountId != accountId
                select database).Distinct();

        /// <summary>
        /// Получить доступы к информационным базам
        /// по ролям пользователя
        /// </summary>
        /// <param name="filter">Модель фильтра информационных баз</param>
        /// <returns>Доступы к информационным базам</returns>
        private IQueryable<AcDbAccess> GetAcDbAccessesByUserGroups(AccountDatabasesFilterDto filter)
        {
            var groups = GetAccountUserGroups(filter.AccountUserId);

            if (groups.All(g => g == AccountUserGroup.AccountUser))
                return DbLayer.AcDbAccessesRepository.WhereLazy(acDbAccess =>
                    acDbAccess.AccountUserID == filter.AccountUserId && acDbAccess.AccountID == filter.AccountId);

            return DbLayer.AcDbAccessesRepository.WhereLazy(acDbAccess => acDbAccess.AccountID == filter.AccountId);
        }
    }
}
