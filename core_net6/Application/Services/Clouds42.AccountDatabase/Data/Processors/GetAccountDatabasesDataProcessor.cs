﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.AccountDatabase.Data.Processors.Processes;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Enums.AccountDatabase;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Data.Processors
{
    /// <summary>
    /// Процессор для получения данных информационных баз
    /// </summary>
    internal class GetAccountDatabasesDataProcessor : IGetAccountDatabasesDataProcessor
    {
        /// <summary>
        /// Создатели процессов для получения данных информационных баз
        /// </summary>
        private readonly
            List<(Func<AccountDatabaseAffiliationTypeEnum, bool> CompareAffiliationDatabase,
                Func<GetAccountDatabasesDataBaseProcess> CreateProcess)> _processCreators;

        public GetAccountDatabasesDataProcessor(IServiceProvider serviceProvider)
        {
            _processCreators =
            [
                (affiliationType => affiliationType == AccountDatabaseAffiliationTypeEnum.My,
                    () => serviceProvider.GetRequiredService<GetMyAccountDatabasesDataProcess>()),


                (affiliationType => affiliationType == AccountDatabaseAffiliationTypeEnum.Shared,
                    () => serviceProvider.GetRequiredService<GetSharedAccountDatabasesDataProcess>())
            ];
        }

        /// <summary>
        /// Получить данные информационных баз
        /// по фильтру
        /// </summary>
        /// <param name="args">Модель фильтра для выбора баз</param>
        /// <returns>Данные информационных баз</returns>
        public AccountDatabaseDataDto GetDatabasesDataByFilter(AccountDatabasesFilterParamsDto args) =>
            CreateProcessForGetDatabasesData(args.Filter.AccountDatabaseAffiliationType).GetDatabasesDataByFilter(args);

        /// <summary>
        /// Создать процесс для получения данных информационных баз
        /// </summary>
        /// <param name="affiliationType">Тип принадлежности базы</param>
        /// <returns>Процесс для получения данных информационных баз</returns>
        private GetAccountDatabasesDataBaseProcess CreateProcessForGetDatabasesData(
            AccountDatabaseAffiliationTypeEnum affiliationType)
        {
            var foundMatch = _processCreators
                .FirstOrDefault(creator => creator.CompareAffiliationDatabase(affiliationType));

            if (foundMatch == default)
                throw new InvalidOperationException(
                    $"Не удалось получить процесс для получения данных информационных баз по типу '{affiliationType}'.");

            return foundMatch.CreateProcess();
        }
    }
}