﻿using Clouds42.AccountDatabase.Data.Constants;
using Clouds42.AccountDatabase.Data.Processors.Models;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Data.Processors.Selectors
{
    /// <summary>
    /// Класс для выбора моделей данных информационных баз
    /// </summary>
    public class DatabaseDataForSampleModelSelector(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        /// Выбрать модели данных информационных баз
        /// </summary>
        /// <param name="databases"></param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Модели данных информационных баз</returns>
        public IQueryable<DatabaseDataForSampleModel> SelectModels(
            IQueryable<Domain.DataModels.AccountDatabase> databases, Guid accountUserId) =>
            from database in databases
            join accountConfigByLocaleData in accountConfigurationDataProvider.GetAccountConfigurationsDataByLocale()
                on database.AccountId equals accountConfigByLocaleData.Account.Id
            join dbSupport in dbLayer.AcDbSupportRepository.WhereLazy()
                on database.Id equals dbSupport.AccountDatabasesID
                into dbSupports
            from support in dbSupports.DefaultIfEmpty()
            join dbTemplate in dbLayer.DbTemplateRepository.WhereLazy() on database.TemplateId equals dbTemplate.Id
                into dbTemplates
            from template in dbTemplates.DefaultIfEmpty()
            join dbTemplateDel in dbLayer.DbTemplateDelimitersReferencesRepository.WhereLazy() on database.TemplateId equals dbTemplateDel.TemplateId
                into dbTemplatesDel
            from templateDel in dbTemplatesDel.DefaultIfEmpty()
            join dbOnDelimiter in dbLayer.AccountDatabaseDelimitersRepository.WhereLazy() on database.Id equals
                dbOnDelimiter.AccountDatabaseId into dbOnDelimiters
            from databaseOnDelimiter in dbOnDelimiters.DefaultIfEmpty()
            join access in dbLayer.AcDbAccessesRepository.WhereLazy(access => access.AccountUserID == accountUserId) on
                database.Id equals access.AccountDatabaseID into accesses
            from dbAccess in accesses.DefaultIfEmpty()
            join sessionTerminationProcess in dbLayer.GetGenericRepository<TerminationSessionsInDatabase>()
                .WhereLazy(ts => ts.TerminateSessionsInDbStatus == TerminateSessionsInDbStatusEnum.InProcess) on 
                database.Id equals sessionTerminationProcess.AccountDatabaseId into sessionsTerminationProcesses
            from sessionTerminationProcess in sessionsTerminationProcesses.DefaultIfEmpty()
            select new DatabaseDataForSampleModel
            {
                Database = database,
                TemplateCaption = databaseOnDelimiter != null ? templateDel.Name : template.DefaultCaption,
                TemplateImageName = template.ImageUrl,
                ConfigurationName = databaseOnDelimiter != null
                    ? templateDel.Name
                    : template.DefaultCaption == AccountDatabaseDataProviderConstants.ClearTemplateName
                        ? AccountDatabaseDataProviderConstants.ClearTemplateConfigurationName 
                        : support.ConfigurationName ?? template.DefaultCaption,
                HasSupport = support != null && support.HasSupport,
                IsDbOnDelimiters = databaseOnDelimiter != null,
                IsDemo = databaseOnDelimiter != null ? databaseOnDelimiter.IsDemo : template.DemoTemplate != null,
                AccountLocale = accountConfigByLocaleData.Locale.Name,
                HasAutoUpdate = support != null && support.HasAutoUpdate &&
                                support.State == (int) SupportState.AutorizationSuccess,
                HasAccessToDatabase = dbAccess != null,
                HasAcDbSupport = support != null && support.State == (int) SupportState.AutorizationSuccess,
                IsExistSessionTerminationProcess = sessionTerminationProcess != null
            };
    }
}
