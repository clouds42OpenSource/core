﻿namespace Clouds42.AccountDatabase.Data.Processors.Models
{
    /// <summary>
    /// Модель данных об информационной базе для выборки
    /// </summary>
    public class DatabaseDataForSampleModel
    {
        /// <summary>
        /// Информационная база
        /// </summary>
        public Domain.DataModels.AccountDatabase Database { get; set; }

        /// <summary>
        /// Локаль аккаунта
        /// </summary>
        public string AccountLocale { get; set; }

        /// <summary>
        /// Название шаблона информационной базы
        /// </summary>
        public string TemplateCaption { get; set; }

        /// <summary>
        /// Название изображения шаблона
        /// </summary>
        public string TemplateImageName { get; set; }

        /// <summary>
        /// База на разделителях
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// Is the database demo
        /// </summary>
        public bool IsDemo { get; set; }

        /// <summary>
        /// Наличие поддержки
        /// </summary>
        public bool HasSupport { get; set; }

        /// <summary>
        /// Наличие авто обновления
        /// </summary>
        public bool HasAutoUpdate { get; set; }

        /// <summary>
        /// Наличие доступа к базе
        /// </summary>
        public bool HasAccessToDatabase { get; set; }

        /// <summary>
        /// Наличие поддержки у инф. базы
        /// </summary>
        public bool HasAcDbSupport { get; set; }

        /// <summary>
        /// Признак наличия процесса завершения сеансов в инф. базе
        /// </summary>
        public bool IsExistSessionTerminationProcess { get; set; }

        /// <summary>
        /// Имя конфигурации
        /// </summary>
        public string ConfigurationName {get; set; }
    }
}
