﻿using Clouds42.AccountDatabase.Data.Processors.Models;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Data.Processors.Utilities
{
    /// <summary>
    /// Утилита для фильтра моделей данных информационных баз
    /// </summary>
    public static class DatabaseDataForSampleModelFilterUtility
    {
        /// <summary>
        /// Применить фильтр для моделей данных информационных баз по условиям
        /// </summary>
        /// <param name="databasesData">Модели данных информационных баз</param>
        /// <param name="isServerDatabaseOnly">Признак "Только серверные базы"</param>
        /// <param name="isArchievedDatabaseOnly">Признак "Только базы в архиве"</param>
        /// <returns>Моделей данных информационных баз после фильтрации</returns>
        public static IQueryable<DatabaseDataForSampleModel> ApplyFilterByCondition(
            this IQueryable<DatabaseDataForSampleModel> databasesData,
            bool isServerDatabaseOnly, bool isArchievedDatabaseOnly) =>
            from databaseData in databasesData
            where
                (!isServerDatabaseOnly || (databaseData.Database.IsFile == null || databaseData.Database.IsFile == false)
                      && databaseData.Database.AccountDatabaseOnDelimiter == null) &&
                (!isArchievedDatabaseOnly || databaseData.Database.State == DatabaseState.DetachedToTomb.ToString())
            select databaseData;

        public static IQueryable<DatabaseDataForSampleModel> ApplyFilterByCondition
            (this IQueryable<DatabaseDataForSampleModel> databasesData, AccountDatabaseTypeEnum accountDatabaseTypeEnum)
        {
            return accountDatabaseTypeEnum switch
            {
                AccountDatabaseTypeEnum.All => databasesData.Where(databasesData => databasesData.Database.State != DatabaseState.DeletedToTomb.ToString() && 
                databasesData.Database.State != DatabaseState.DetachedToTomb.ToString()),
                AccountDatabaseTypeEnum.ServerDatabase =>
                    databasesData.Where(databasesData => databasesData.Database.IsFile != true && databasesData.Database.AccountDatabaseOnDelimiter == null 
                    && databasesData.Database.State != DatabaseState.DeletedToTomb.ToString() && databasesData.Database.State != DatabaseState.DetachedToTomb.ToString()),
                AccountDatabaseTypeEnum.ArchievedDatabase =>
                    databasesData.Where(databasesData => databasesData.Database.State == DatabaseState.DetachedToTomb.ToString()),
                AccountDatabaseTypeEnum.FileDatabases =>
                    databasesData.Where(databasesData => databasesData.Database.IsFile == true && databasesData.Database.State != DatabaseState.DeletedToTomb.ToString() && 
                    databasesData.Database.State != DatabaseState.DetachedToTomb.ToString()),
                AccountDatabaseTypeEnum.DelimeterDatabases =>
                    databasesData.Where(databasesData => databasesData.Database.AccountDatabaseOnDelimiter != null && databasesData.Database.State != DatabaseState.DeletedToTomb.ToString() &&
                    databasesData.Database.State != DatabaseState.DetachedToTomb.ToString()),
                AccountDatabaseTypeEnum.DeletedDatabases =>
                    databasesData.Where(databasesData =>  (databasesData.Database.LastEditedDateTime - DateTime.Now).Days <= 30 &&
                    databasesData.Database.State == DatabaseState.DeletedToTomb.ToString()),
                _ => databasesData,
            };
        }

        /// <summary>
        /// Применить фильтр для моделей данных информационных баз по строке поиска
        /// </summary>
        /// <param name="databasesData">Модели данных информационных баз</param>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>Моделей данных информационных баз после фильтрации</returns>
        public static IQueryable<DatabaseDataForSampleModel> ApplyFilterBySearchString
            (this IQueryable<DatabaseDataForSampleModel> databasesData, string searchString) => from databaseData in databasesData
            where
                string.IsNullOrEmpty(searchString) ||
                 databaseData.Database.Caption.ToLower().Contains(searchString.ToLower()) ||
                 databaseData.Database.V82Name.ToLower().Contains(searchString.ToLower()) ||
                 databaseData.Database.DbTemplate.DefaultCaption.ToLower().Contains(searchString.ToLower())
            select databaseData;
    }
}
