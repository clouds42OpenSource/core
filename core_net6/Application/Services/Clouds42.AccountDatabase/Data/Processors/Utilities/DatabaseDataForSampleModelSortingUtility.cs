﻿using System.Linq.Expressions;
using Clouds42.AccountDatabase.Data.Processors.Models;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Data.Processors.Utilities
{
    /// <summary>
    /// Утилита для сортировки моделей данных информационных баз
    /// </summary>
    public static class DatabaseDataForSampleModelSortingUtility
    {
        /// <summary>
        /// Сопоставление названия поля и функции для применения сортировки
        /// </summary>
        private static readonly
            IDictionary<string, Func<IOrderedQueryable<DatabaseDataForSampleModel>, SortType,
                IOrderedQueryable<DatabaseDataForSampleModel>>> MapOrderFieldToMakeSortingFunc
                = new Dictionary<string, Func<IOrderedQueryable<DatabaseDataForSampleModel>, SortType,
                    IOrderedQueryable<DatabaseDataForSampleModel>>>
                  {
                        {
                            nameof(AccountDatabaseDataItemDto.Name),
                            (rows, sortType) => MakeSorting(rows, model => model.Database.Caption, sortType)
                        },
                        {
                            nameof(AccountDatabaseDataItemDto.TemplateCaption),
                            (rows, sortType) => MakeSorting(rows, model => model.TemplateCaption, sortType)
                        },
                        {
                            nameof(AccountDatabaseDataItemDto.SizeInMb),
                            (rows, sortType) => MakeSorting(rows, model => model.Database.SizeInMB, sortType)
                        },
                        {
                            nameof(AccountDatabaseDataItemDto.LastActivityDate),
                            (rows, sortType) => MakeSorting(rows, model => model.Database.LastActivityDate, sortType)
                        },
                        {
                            nameof(AccountDatabaseDataItemDto.HasSupport),
                            (rows, sortType) => MakeSorting(rows, model => model.HasSupport, sortType)
                        },
                        {
                            nameof(AccountDatabaseDataItemDto.HasAutoUpdate),
                            (rows, sortType) => MakeSorting(rows, model => model.HasAutoUpdate, sortType)
                        },
                        {
                            nameof(AccountDatabaseDataItemDto.DatabaseLaunchLink),
                            (rows, sortType) => MakeSorting(rows, model => model.Database.PublishState, sortType)
                        }
                  };


        /// <summary>
        /// Выполнить сортировку моделей данных информационных баз
        /// </summary>
        /// <param name="databasesData">Модели данных информационных баз</param>
        /// <param name="sortingData">Информация о сортировке</param>
        /// <returns>Отсортированные модели данных информационных баз</returns>
        public static IOrderedQueryable<DatabaseDataForSampleModel> MakeSorting(
            this IQueryable<DatabaseDataForSampleModel> databasesData, SortingDataDto sortingData)
        {
            var sortedData = MakeBaseSorting(databasesData);

            if (sortingData == null || string.IsNullOrEmpty(sortingData.FieldName))
                return sortedData;

            if (!MapOrderFieldToMakeSortingFunc.TryGetValue(sortingData.FieldName, out var makeSortingFunc))
                return sortedData;

            return makeSortingFunc(sortedData, sortingData.SortKind);
        }


        /// <summary>
        /// Выполнить сортировку моделей данных информационных баз
        /// </summary>
        /// <param name="databasesData">Модели данных информационных баз</param>
        /// <param name="keySelector">Выражение для сортировки</param>
        /// <param name="sortType">Тип сортировки</param>
        /// <returns>Отсортированные модели данных информационных баз</returns>
        private static IOrderedQueryable<DatabaseDataForSampleModel> MakeSorting<T>(
            IOrderedQueryable<DatabaseDataForSampleModel> databasesData,
            Expression<Func<DatabaseDataForSampleModel, T>> keySelector, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => databasesData.OrderBy(keySelector),
                SortType.Desc => databasesData.OrderByDescending(keySelector),
                _ => databasesData
            };
        }

        /// <summary>
        /// Применить базовую сортировку
        /// </summary>
        /// <param name="databasesData">Модели данных информационных баз</param>
        /// <returns>Отсортированные модели данных информационных баз</returns>
        private static IOrderedQueryable<DatabaseDataForSampleModel> MakeBaseSorting(
            IQueryable<DatabaseDataForSampleModel> databasesData) =>
            databasesData.OrderByDescending(db => db.Database.State == DatabaseState.NewItem.ToString()
                                                  || db.Database.State == DatabaseState.RestoringFromTomb.ToString() || 
                                                  db.Database.State == DatabaseState.DetachingToTomb.ToString())
                .ThenByDescending(db => db.Database.State == DatabaseState.ErrorCreate.ToString()
                                        || db.Database.State == DatabaseState.ErrorDtFormat.ToString()
                                        || db.Database.State == DatabaseState.ErrorNotEnoughSpace.ToString())
                .ThenByDescending(db => db.Database.State == DatabaseState.Ready.ToString())
                .ThenByDescending(db => db.Database.State == DatabaseState.DetachedToTomb.ToString())
                .ThenByDescending(db => db.Database.CreationDate);
    }
}