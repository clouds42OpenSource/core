﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.AccountDatabase.Restore.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.CoreWorker.JobWrappers.CreateDatabase;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Restore.Providers
{
    /// <summary>
    /// Провайдер для восстановления серверной инф. базы
    /// </summary>
    public class RestoreServerAccountDatabaseProvider(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        ICreateDatabaseClusterJobWrapper createDatabaseClusterJobWrapper,
        RestoreServerAccountDatabaseHelper restoreServerAccountDatabaseHelper,
        ILogger42 logger)
        : IRestoreServerAccountDatabaseProvider
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        public void Restore(RestoreAccountDatabaseParamsDto model)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);
            var accountDatabaseBackup =
                accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(model.AccountDatabaseBackupId);

            if (!File.Exists(model.AccountDatabaseBackupPath))
                throw new NotFoundException($"Восстановление информационной базы {accountDatabase.V82Name} из бекапа, не найден путь '{model.AccountDatabaseBackupPath}'");

            var tempBackupDirectoryPath = restoreServerAccountDatabaseHelper.GetTempBackupDirectory(accountDatabase.AccountId);

            logger.Info($"[{accountDatabase.Id}] :: Путь к временной директории {tempBackupDirectoryPath} и путь к бекапу {model.AccountDatabaseBackupPath}");
            var tempBackupFilePath = model.AccountDatabaseBackupPath;

            logger.Info($"Файл с расширением {Path.GetExtension(model.AccountDatabaseBackupPath)}");
            if (Path.GetExtension(model.AccountDatabaseBackupPath) == ".zip")
            {
                tempBackupFilePath = restoreServerAccountDatabaseHelper.ExtractBackupToTempDirectory(tempBackupDirectoryPath,
                    model.AccountDatabaseBackupPath); 
            }

            var sourceDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseBackup
                    .AccountDatabaseId);

            restoreServerAccountDatabaseHelper.RestoreAccountDatabaseInSqlServer(accountDatabase, tempBackupFilePath, sourceDatabase);
            StartTaskForCreateAcDbOnCluster(accountDatabase);

            DirectoryHelper.DeleteDirectory(tempBackupDirectoryPath);
        }

        /// <summary>
        /// Запустить задачу по созданию инф. базы на кластере
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        private void StartTaskForCreateAcDbOnCluster(IAccountDatabase accountDatabase)
        {
            try
            {
                createDatabaseClusterJobWrapper.Start(new CreateDatabaseClusterJobParams
                {
                    AccountDatabaseId = accountDatabase.Id
                }).Wait();

                logger.Info($"[{accountDatabase.Id}] :: Установка базы на SQL сервер выполнена");
            }
            catch (Exception ex)
            {
                logger.Info($"[{accountDatabase.Id}] :: Ошибка при установке базы на SQL сервер: {ex.GetFullInfo()}");
                throw;
            }
        }
    }
}
