﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Restore.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseRestoreFromTomb;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.AccountDatabase.Restore.Providers
{
    /// <summary>
    /// Провайдер восстановления ИБ из бэкапа.
    /// </summary>
    internal class RestoreAccountDatabaseBackupProvider(
        IRestoreAccountDatabaseFromBackupProcessFlow restoreAccountDatabaseFromBackupProcessFlow,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        NotifyHotlineAboutRestoreErrorHelper notifyHotlineAboutRestoreErrorHelper,
        IRestoreAccountDatabaseAfterFailedAutoUpdateProcessFlow restoreAccountDatabaseAfterFailedAutoUpdateProcessFlow,
        IUnitOfWork dbLayer)
        : IRestoreAccountDatabaseBackupProvider
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>
        /// <param name="restoreAccountDatabaseParams">Параметры восстановления инф. базы из бэкапа</param>
        public void Restore(RestoreAccountDatabaseFromTombWorkerTaskParam restoreAccountDatabaseParams)
        {
            var uploadedFile =
                dbLayer.UploadedFileRepository.FirstOrThrowException(file =>
                    file.Id == restoreAccountDatabaseParams.UploadedFileId);

            CheckFileExistence(uploadedFile.FullFilePath, restoreAccountDatabaseParams.AccountDatabaseName);

            var restoreResult = restoreAccountDatabaseFromBackupProcessFlow.Run(new RestoreAccountDatabaseParamsDto
            {
                AccountDatabaseBackupId = restoreAccountDatabaseParams.AccountDatabaseBackupId,
                NeedChangeState = restoreAccountDatabaseParams.NeedChangeState,
                RestoreType = restoreAccountDatabaseParams.RestoreType,
                NeedSendEmail = restoreAccountDatabaseParams.SendMailAboutError,
                ProcessFlowKey = SynchronizeAccountDatabaseProcessFlowsHelper.CreateParentProcessFlowId(restoreAccountDatabaseParams.AccountDatabaseBackupId),
                UploadedFileId = restoreAccountDatabaseParams.UploadedFileId,
                UsersIdForAddAccess = restoreAccountDatabaseParams.UsersIdForAddAccess,
                AccountDatabaseBackupPath = uploadedFile.FullFilePath,
                AccountDatabaseName = restoreAccountDatabaseParams.AccountDatabaseName
            }, false);

            var errorMessage = ProcessRestoreResult(restoreResult,
                restoreAccountDatabaseParams.AccountDatabaseBackupId,
                restoreAccountDatabaseParams.SendMailAboutError);

            if (!string.IsNullOrEmpty(errorMessage))
                throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Восстановить инф. базу из бэкапа после не успешной попытки АО
        /// </summary>
        /// <param name="restoreAccountDatabaseParams">Параметры восстановления инф. базы из бэкапа</param>
        public void RestoreAfterFailedAutoUpdate(RestoreAccountDatabaseFromTombWorkerTaskParam restoreAccountDatabaseParams)
        {
            ValidateAccountDatabaseBackup(restoreAccountDatabaseParams.AccountDatabaseBackupId);

            var restoreResult = restoreAccountDatabaseAfterFailedAutoUpdateProcessFlow.Run(new RestoreAccountDatabaseParamsDto
            {
                AccountDatabaseBackupId = restoreAccountDatabaseParams.AccountDatabaseBackupId,
                RestoreType = AccountDatabaseBackupRestoreType.ToCurrent
            }, false);

            var errorMessage = ProcessRestoreResult(restoreResult,
                restoreAccountDatabaseParams.AccountDatabaseBackupId,
                restoreAccountDatabaseParams.SendMailAboutError);

            if (!string.IsNullOrEmpty(errorMessage))
                throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Обработать результат восстановления инф. базы
        /// </summary>
        /// <param name="result">Результат восстановления</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <param name="needSendEmail">Признак необходимости отправлять письмо</param>
        /// <returns>Сообщение об ошибке</returns>
        private string ProcessRestoreResult(StateMachineResult<RestoreAccountDatabaseParamsDto> result, Guid accountDatabaseBackupId, bool needSendEmail)
        {
            var accountDatabaseBackup =
                accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(accountDatabaseBackupId);

            if (result.Finish)
                return null;

            return !needSendEmail
                ? notifyHotlineAboutRestoreErrorHelper.GetErrorMessage(result, accountDatabaseBackup) 
                : notifyHotlineAboutRestoreErrorHelper.Notify<MailNotifyHotlineRestoreAccountDatabaseFromTomb>(result, accountDatabaseBackup);
        }

        /// <summary>
        /// Проверить бэкап инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        private void ValidateAccountDatabaseBackup(Guid accountDatabaseBackupId)
        {
            var accountDatabaseBackup =
                accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(accountDatabaseBackupId);

            if (accountDatabaseBackup.SourceType == AccountDatabaseBackupSourceType.GoogleCloud)
                return;

            CheckFileExistence(accountDatabaseBackup.BackupPath, accountDatabaseBackup.AccountDatabase.V82Name);
        }

        /// <summary>
        /// Проверить существование файла
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        /// <param name="acDbV82Name">Номер инф. базы</param>
        private void CheckFileExistence(string filePath, string acDbV82Name)
        {
            if (!File.Exists(filePath))
                throw new NotFoundException(
                    $"Восстановление информационной базы {acDbV82Name} из бекапа, не найден путь '{filePath}'");

            if (filePath.ToFileInfo().Length == 0)
                throw new NotFoundException($"Бэкап инф. базы пуст '{filePath}'.");
        }
    }
}
