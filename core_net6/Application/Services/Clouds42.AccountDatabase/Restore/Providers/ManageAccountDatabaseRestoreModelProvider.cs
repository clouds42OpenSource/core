﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.AccountDatabase.Restore.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Restore.Providers
{
    /// <summary>
    /// Провайдер для управления моделью восстановления инф. баз
    /// </summary>
    internal class ManageAccountDatabaseRestoreModelProvider(
        IAccountDatabaseRestoreModelHelper accountDatabaseRestoreModelHelper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IUnitOfWork dbLayer,
        AcDbRestoreModelSupportNotificationHelper acDbRestoreModelSupportNotificationHelper,
        ILogger42 logger,
        IHandlerException handlerException)
        : IManageAccountDatabaseRestoreModelProvider
    {
        /// <summary>
        /// Выполнить аудит и корректировку моделей восстановления инф. баз
        /// </summary>
        public void ManageAccountDatabasesRestoreModel()
        {
            var errorMessageBuilder = new StringBuilder();
            var supportMessageBuilder = new StringBuilder();

            var accountDatabasesForManageRestoreModel =
                accountDatabaseRestoreModelHelper.GetAccountDatabasesForManageRestoreModel();

            accountDatabasesForManageRestoreModel.ForEach(manageAcDbRestoreModel =>
            {
                try
                {
                    ManageAcDbRestoreModel(manageAcDbRestoreModel, supportMessageBuilder);
                }
                catch (Exception ex)
                {
                    var errorMessage =
                        "<br/> - Операция аудита и корректировки модели восстановления " +
                        $"для инф. базы {manageAcDbRestoreModel.AccountDatabaseV82Name} завершена с ошибкой. " +
                        $"Причина: {ex.GetFullInfo(false)}";
                    
                    handlerException.Handle(ex, $"[Ошибка аудита и корректировки моделей восстановления] ИБ: {manageAcDbRestoreModel.AccountDatabaseV82Name}");
                    errorMessageBuilder.AppendLine(errorMessage);
                }
            });

            var operationErrorMessage = errorMessageBuilder.ToString();

            if (string.IsNullOrEmpty(supportMessageBuilder.ToString()) && string.IsNullOrEmpty(operationErrorMessage))
            {
                logger.Info("Тело пустое для отправки письма");
                return;
            }
                

            acDbRestoreModelSupportNotificationHelper.CreateEmailAndNotifySupport(supportMessageBuilder.ToString(),
                operationErrorMessage);

            logger.Info("Письмо отправлено!!!");
        }

        /// <summary>
        /// Выполнить аудит и корректировку модели восстановления инф. базы
        /// </summary>
        /// <param name="manageAcDbRestoreModel">Данные модели восстановления инф. базы</param>
        /// <param name="supportMessageBuilder">Билдер сообщений для тех. поддержки</param>
        private void ManageAcDbRestoreModel(ManageAcDbRestoreModelDto manageAcDbRestoreModel,
            StringBuilder supportMessageBuilder)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(
                    manageAcDbRestoreModel.AccountDatabaseId);

            if (IsAcDbRestoreModelActual(manageAcDbRestoreModel, accountDatabase))
                return ;

            var needAcDbRestoreModel =
                manageAcDbRestoreModel.SqlServerRestoreModel == AccountDatabaseRestoreModelTypeEnum.Mixed
                    ? manageAcDbRestoreModel.AccountDatabaseRestoreModel
                    : manageAcDbRestoreModel.SqlServerRestoreModel;

            if (needAcDbRestoreModel == null)
            {   
                logger.Warn($"Невозможно проверить/изменить модель восстановления, так как, для инф. базы {manageAcDbRestoreModel.AccountDatabaseV82Name} модель восстановления не настроена.");
                return;
            }
            
            UpdateAcDbRestoreModel(accountDatabase, needAcDbRestoreModel.Value);

            logger.Trace($"Для инф. базы {manageAcDbRestoreModel.AccountDatabaseV82Name} изменена модель восстановления " +
                          $"с '{manageAcDbRestoreModel.AccountDatabaseRestoreModel.Description()}' " +
                          $"на '{manageAcDbRestoreModel.SqlServerRestoreModel.Description()}'");

            AppendMessageForSupport(manageAcDbRestoreModel, supportMessageBuilder);
        }

        /// <summary>
        /// Признак что модель восстановления инф. базы актуальная
        /// </summary>
        /// <param name="manageAcDbRestoreModel">Данные модели восстановления инф. базы</param>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Признак что модель восстановления инф. базы актуальная</returns>
        private bool IsAcDbRestoreModelActual(ManageAcDbRestoreModelDto manageAcDbRestoreModel, Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (!DirectoryHelper.PingServerFromUNC(accountDatabase.Account.AccountConfiguration.Segment.CloudServicesSQLServer.ConnectionAddress))
                return true;

            var realAcDbRestoreModelInSqlServer = GetAcDbRestoreModelInSqlServer(accountDatabase);

            var isActualRecoveryModelInTheCard =
                manageAcDbRestoreModel.SqlServerRestoreModel == AccountDatabaseRestoreModelTypeEnum.Mixed ||
                manageAcDbRestoreModel.SqlServerRestoreModel == manageAcDbRestoreModel.AccountDatabaseRestoreModel;


            var isAcDbRestoreModelActual = isActualRecoveryModelInTheCard &&
                                           realAcDbRestoreModelInSqlServer == manageAcDbRestoreModel.AccountDatabaseRestoreModel;

            if (!isAcDbRestoreModelActual) 
                return false;

            logger.Info(
                $"Модель восстановления, для инф. базы {manageAcDbRestoreModel.AccountDatabaseV82Name} актуальна: {manageAcDbRestoreModel.AccountDatabaseRestoreModel}.");
            return true;
        }

        /// <summary>
        /// Добавить сообщение для тех. поддержки
        /// </summary>
        /// <param name="manageAcDbRestoreModel">Данные модели восстановления инф. базы</param>
        /// <param name="supportMessageBuilder">Билдер сообщений для тех. поддержки</param>
        private void AppendMessageForSupport(ManageAcDbRestoreModelDto manageAcDbRestoreModel, StringBuilder supportMessageBuilder)
        {
            supportMessageBuilder.AppendLine(
                "<tr>" +
                $"<td style='padding: 3px;'>{manageAcDbRestoreModel.AccountDatabaseV82Name}</td>" +
                $"<td style='padding: 3px;'>{manageAcDbRestoreModel.AccountDatabaseRestoreModel.Description()}</td>" +
                $"<td style='padding: 3px;'>{manageAcDbRestoreModel.SqlServerRestoreModel.Description()}</td>" +
                "</tr>");
        }

        /// <summary>
        /// Получает тип модели восстановления инф. базы на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Тип модели восстановления</returns>
        private AccountDatabaseRestoreModelTypeEnum GetAcDbRestoreModelInSqlServer(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var recoveryModel = accountDatabaseRestoreModelHelper.GetAcDbRestoreModelInSqlServer(accountDatabase);

            return recoveryModel?.ToUpper() switch
            {
                "FULL" => AccountDatabaseRestoreModelTypeEnum.Full,
                "SIMPLE" => AccountDatabaseRestoreModelTypeEnum.Simple,
                _ => throw new NotFoundException(
                    $"Невозможно определить модель восстановления на SQL SERVER для инф. базы: {accountDatabase.SqlName}. Не удалось установить соединение или база отсутствует ")
            };
        }

        /// <summary>
        /// Обновить модель восстановления инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        private void UpdateAcDbRestoreModel(Domain.DataModels.AccountDatabase accountDatabase, AccountDatabaseRestoreModelTypeEnum restoreModelType)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                accountDatabase.RestoreModelType = restoreModelType;

                dbLayer.DatabasesRepository.Update(accountDatabase);
                dbLayer.Save();

                accountDatabaseRestoreModelHelper.UpdateAcDbRestoreModelInSqlServer(
                    accountDatabase, restoreModelType);

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
    }
}
