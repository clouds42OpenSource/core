﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.AccountDatabase.Restore.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Restore.Providers
{
    /// <summary>
    /// Провайдер для восстановления серверной инф. базы после не успешной попытки АО
    /// </summary>
    public class RestoreServerAcDbAfterFailedAutoUpdateProvider(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        RestoreServerAccountDatabaseHelper restoreServerAccountDatabaseHelper)
        : IRestoreAccountDatabaseProvider
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        public void Restore(RestoreAccountDatabaseParamsDto model)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            if (!File.Exists(model.AccountDatabaseBackupPath))
                throw new NotFoundException($"Восстановление информационной базы {accountDatabase.V82Name} из бекапа, не найден путь '{model.AccountDatabaseBackupPath}'");

            var tempBackupDirectoryPath = restoreServerAccountDatabaseHelper.GetTempBackupDirectory(accountDatabase.AccountId);

            var tempBackupFilePath =
                restoreServerAccountDatabaseHelper.ExtractBackupToTempDirectory(tempBackupDirectoryPath,
                    model.AccountDatabaseBackupPath);

            restoreServerAccountDatabaseHelper.RestoreAccountDatabaseInSqlServer(accountDatabase, tempBackupFilePath);
            DirectoryHelper.DeleteDirectory(tempBackupDirectoryPath);
        }
    }
}
