﻿using System.IO.Compression;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Restore.Providers
{
    /// <summary>
    /// Провайдер для восстановления файловой инф. базы
    /// </summary>
    public class RestoreFileAccountDatabaseProvider(
        IUnitOfWork dbLayer,
        IServiceProvider serviceProvider,
        AccountDatabasePathHelper accountDatabasePathHelper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountDataProvider accountDataProvider,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        ILogger42 logger)
        : IRestoreAccountDatabaseProvider
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        public void Restore(RestoreAccountDatabaseParamsDto model)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);
            var fileStorage = GetFileStorage(accountDatabase, model.RestoreType);
            RestoreAccountDatabaseBackupFile(accountDatabase, model.AccountDatabaseBackupPath, fileStorage);
        }

        /// <summary>
        /// Получить файловое хранилище для инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="restoreType">Тип восстановления</param>
        /// <returns>Файловое хранилище для инф. базы</returns>
        private CloudServicesFileStorageServer GetFileStorage(IAccountDatabase accountDatabase, AccountDatabaseBackupRestoreType restoreType)
        {
            var account = accountDataProvider.GetAccountOrThrowException(accountDatabase.AccountId);
            var accountDatabasePath = accountDatabasePathHelper.GetPath(accountDatabase);

            CloudServicesFileStorageServer fileStorage;

            if (restoreType == AccountDatabaseBackupRestoreType.ToCurrent &&
                DirectoryHelper.DirectoryHas1CDatabaseFile(accountDatabasePath))
            {
                fileStorage = dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(f => f.ID == accountDatabase.FileStorageID);

                if (fileStorage == null)
                    throw new NotFoundException($"Не удалось получить объект CloudServicesFileStorageServer по номеру '{accountDatabase.FileStorageID}' ");
            }
            else
            {
                fileStorage = serviceProvider.GetRequiredService<ISegmentHelper>().GetFileStorageServer(account);
            }

            return fileStorage;
        }

        /// <summary>
        /// Восстановить файловую информационную базу из архива.
        /// </summary>
        /// <param name="accountDatabase">Текущая инфомрационная база</param>
        /// <param name="backupPath">Путь к бэкапу на хранилке</param>
        /// <param name="storage">Целевоей файловое хранилище (место куда восстановится база)</param>
        private void RestoreAccountDatabaseBackupFile(Domain.DataModels.AccountDatabase accountDatabase, string backupPath, CloudServicesFileStorageServer storage)
        {
            var account = accountDataProvider.GetAccountOrThrowException(accountDatabase.AccountId);

            var companyName = DomainCoreGroups.CompanyGroupBuild(account.IndexNumber);
            var companyPath = Path.Combine(storage.ConnectionAddress, companyName);

            if (!Directory.Exists(companyPath))
                Directory.CreateDirectory(companyPath);

            activeDirectoryTaskProcessor.TryDoImmediately(provider => provider.SetDirectoryAcl(companyName, companyPath));

            var databaseDirectory = accountDatabasePathHelper.GetPath(accountDatabase, storage);

            if (!File.Exists(backupPath))
                throw new NotFoundException($"Восстановление информационной базы {accountDatabase.V82Name} из бекапа, не найден путь '{backupPath}'");

            if (!Directory.Exists(databaseDirectory))
            {
                Directory.CreateDirectory(databaseDirectory);
                ExtractToDirectory(backupPath, databaseDirectory);
            }
            else
            {
                DirectoryHelper.DeleteDirectory(databaseDirectory, onlyDirectoryContent: true);
                ExtractToDirectory(backupPath, databaseDirectory);
            }

            logger.Info($"[{accountDatabase.Id}] :: смена файлового хранилища для базы на дефолтный после восстановления из склепа ");

            UpdateDatabaseStorage(accountDatabase, storage);
        }

        /// <summary>
        ///  Разархивирование данных
        /// </summary>
        /// <param name="databaseDirectory">Путь к папке куда разархивировать</param>
        /// <param name="backupPath">Путь к бэкапу на хранилке</param>
        private static void ExtractToDirectory(string backupPath, string databaseDirectory)
        {
            ZipFile.ExtractToDirectory(backupPath, databaseDirectory);
            if (databaseDirectory.ToDirectoryInfo().GetSize() == 0)
                throw new InvalidOperationException($"Директория {databaseDirectory} пуста. Проверьте архив на наличие файлов.");
        }

        /// <summary>
        ///     Изменение файлового хранилища для ИБ на дефолтный после восстановления из архива
        /// </summary>
        /// <param name="database">Текущая инфомрационная база</param>
        /// <param name="storage">Файловое хранилище.</param>
        private void UpdateDatabaseStorage(IAccountDatabase database, ICloudServicesFileStorageServer storage)
        {
            var accountDatabase =
                dbLayer.DatabasesRepository.FirstOrDefault(d =>
                    d.Id == database.Id);
            accountDatabase.FileStorageID = storage.ID;
            dbLayer.DatabasesRepository.Update(accountDatabase);
            dbLayer.Save();
        }
    }
}
