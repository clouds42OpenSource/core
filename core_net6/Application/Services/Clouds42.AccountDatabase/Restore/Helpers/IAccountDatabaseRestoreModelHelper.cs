﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Restore.Helpers
{
    /// <summary>
    /// Хэлпер для моделей восстановления инф. баз
    /// </summary>
    public interface IAccountDatabaseRestoreModelHelper
    {
        /// <summary>
        /// Получить список инф. баз для аудита и корректировки моделей восстановления
        /// </summary>
        /// <returns>Cписок инф. баз для аудита и корректировки моделей восстановления</returns>
        List<ManageAcDbRestoreModelDto> GetAccountDatabasesForManageRestoreModel();

        /// <summary>
        /// Обновить тип модели восстановления инф. базы на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        void UpdateAcDbRestoreModelInSqlServer(Domain.DataModels.AccountDatabase accountDatabase, AccountDatabaseRestoreModelTypeEnum restoreModelType);

        /// <summary>
        /// Получить тип модели восстановления инф. базы на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        string GetAcDbRestoreModelInSqlServer(Domain.DataModels.AccountDatabase accountDatabase);
    }
}
