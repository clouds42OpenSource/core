﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.AccountDatabase.Restore.Helpers
{
    /// <summary>
    /// Хэлпер для уведомления хотлайна об ошибках восстановления базы
    /// </summary>
    public class NotifyHotlineAboutRestoreErrorHelper(MailNotificationFactory mailNotificationFactory)
    {
        /// <summary>
        /// Отправить уведомление на хотлайн
        /// </summary>
        /// <typeparam name="TTemplateMailNotification">Шаблон письма</typeparam>
        /// <param name="result">Результат восстановления</param>
        /// <param name="accountDatabaseBackup">Бэкап инф. базы</param>
        /// <returns>Сообщение об ошибке</returns>
        public string Notify<TTemplateMailNotification>(StateMachineResult<RestoreAccountDatabaseParamsDto> result, AccountDatabaseBackup accountDatabaseBackup) 
            where TTemplateMailNotification : IMailNotificationTemplate<string>, new()
        {
            var message = $"Восстановление инф. базы {accountDatabaseBackup.AccountDatabase.V82Name} из бэкапа {accountDatabaseBackup.BackupPath} не выполнено. ";
            message = AddFooterWithLinkToRetryProcessFlow(message, result);

            mailNotificationFactory.SendMail<TTemplateMailNotification, string>(message);

            return message;
        }

        /// <summary>
        /// Получить сообщение об ошибке
        /// </summary>
        /// <param name="result">Результат восстановления</param>
        /// <param name="accountDatabaseBackup">Бэкап инф. базы</param>
        /// <returns>Сообщение об ошибке</returns>
        public string GetErrorMessage(StateMachineResult<RestoreAccountDatabaseParamsDto> result, AccountDatabaseBackup accountDatabaseBackup)
        {
            var message = $"Восстановление инф. базы {accountDatabaseBackup.AccountDatabase.V82Name} из бэкапа {accountDatabaseBackup.BackupPath} не выполнено. ";
            message = AddFooterWithLinkToRetryProcessFlow(message, result);
            return message;
        }

        /// <summary>
        /// Добавить ссылку на перезапуск рабочего процесса
        /// </summary>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <param name="result">Результат восстановления</param>
        /// <returns>Сообщение с ссылкой на перезапуск рабочего процесса</returns>
        private static string AddFooterWithLinkToRetryProcessFlow(string errorMessage, StateMachineResult<RestoreAccountDatabaseParamsDto> result)
        {
            var linkForOpenProcessFlow = GetLinkForOpenProcessFlow(result.ProcessFlowId);

            errorMessage +=
                $"Чтобы завершить процесс восстановления перейдите по ссылке {linkForOpenProcessFlow}. " +
                $"Причина: {result.Message}";
            return errorMessage;
        }

        /// <summary>
        /// Получить ссылку для просмотра деталей рабочего процесса
        /// </summary>
        /// <param name="processFlowId"></param>
        /// <returns>Ссылка для просмотра деталей рабочего процесса</returns>
        private static string GetLinkForOpenProcessFlow(Guid processFlowId)
        {
            var siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
            var routeValue = CloudConfigurationProvider.Cp.GetRouteValueForOpenProcessFlowDetails();

            return $"{siteUrl}/{routeValue}{processFlowId}";
        }
    }
}
