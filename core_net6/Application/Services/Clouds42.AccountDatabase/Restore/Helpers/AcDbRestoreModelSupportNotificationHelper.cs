﻿using Clouds42.Configurations.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;

namespace Clouds42.AccountDatabase.Restore.Helpers
{
    /// <summary>
    /// Хэлпер для уведомления тех. поддержки об изменениях модели восстановления
    /// </summary>
    public class AcDbRestoreModelSupportNotificationHelper
    {
        /// <summary>
        /// Создать письмо и уведомить тех. поддержку
        /// </summary>
        /// <param name="messageBody">Тело письма</param>
        /// <param name="operationErrorMessage">Сообщение об ошибках</param>
        public void CreateEmailAndNotifySupport(string messageBody, string operationErrorMessage)
        {
            var email = CreateEmail(messageBody, operationErrorMessage);

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(CloudConfigurationProvider.AccountDatabase.Support.GetSupportNotificationEmail())
                .Body(email)
                .Subject("Отчет об изменениях модели восстановления инф. баз.")
                .SendViaNewThread();
        }

        /// <summary>
        /// Создать письмо
        /// </summary>
        /// <param name="messageBody">Тело письма</param>
        /// <param name="operationErrorMessage">Сообщение об ошибках</param>
        /// <returns>Письмо</returns>
        private string CreateEmail(string messageBody, string operationErrorMessage)
            => @"<table 
                    border='1'
                    cellpadding='0'
                    cellspacing='0'
                    style=""                        
                            border: medium #000;
                            border-collapse: collapse;
                            font-family: 'arial';
                            font-size: 10pt;
                            width: 0px;""
                >
                <thead style='background-color: #efefef'>
                    <tr>
                        <td style='padding: 3px;'>Номер базы</td>
                        <td style='padding: 3px;'>Старая модель восстановления</td>
                        <td style='padding: 3px;'>Новая модель восстановления</td>
                    </tr>
                </thead>" +
                $"<tbody>{messageBody}</tbody>" +
                "</table>" +
                $"<br/> Ошибки выполнения: <br/> {operationErrorMessage}";
    }
}
