﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Restore.Helpers
{
    /// <summary>
    /// Хэлпер для моделей восстановления инф. баз
    /// </summary>
    public class AccountDatabaseRestoreModelHelper(
        IUnitOfWork dbLayer,
        IAccountDataProvider accountDataProvider,
        IServiceProvider serviceProvider)
        : IAccountDatabaseRestoreModelHelper
    {
        /// <summary>
        /// Получить список инф. баз для аудита и корректировки моделей восстановления
        /// </summary>
        /// <returns>Cписок инф. баз для аудита и корректировки моделей восстановления</returns>
        public List<ManageAcDbRestoreModelDto> GetAccountDatabasesForManageRestoreModel()
            => dbLayer.DatabasesRepository
                .AsQueryableNoTracking()
                .Where(x => (x.IsFile == null || !x.IsFile.Value) &&
                            x.AccountDatabaseOnDelimiter == null &&
                            x.Account.ServiceAccount == null &&
                            (x.State == DatabaseState.Ready.ToString() ||
                             x.State == DatabaseState.ProcessingSupport.ToString()))
                .Select(x => new ManageAcDbRestoreModelDto
                {
                    AccountDatabaseId = x.Id,
                    AccountDatabaseRestoreModel = x.RestoreModelType,
                    SqlServerRestoreModel = x.Account.AccountConfiguration.Segment.CloudServicesSQLServer.RestoreModelType,
                    AccountDatabaseV82Name = x.V82Name
                })
                .ToList();

        /// <summary>
        /// Обновить тип модели восстановления инф. базы на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        public void UpdateAcDbRestoreModelInSqlServer(Domain.DataModels.AccountDatabase accountDatabase, AccountDatabaseRestoreModelTypeEnum restoreModelType)
        {
            var account = accountDataProvider.GetAccountOrThrowException(accountDatabase.AccountId);
            var sqlServer = serviceProvider.GetRequiredService<ISegmentHelper>().GetSQLServer(account);
            
            dbLayer.Execute1CSqlQuery(string.Format(SqlQueries.DatabaseCore.SetRecovery, accountDatabase.SqlName, restoreModelType), sqlServer);
        }

        /// <summary>
        /// Получает тип модели восстановления инф. базы на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        public string GetAcDbRestoreModelInSqlServer(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var account = accountDataProvider.GetAccountOrThrowException(accountDatabase.AccountId);
            var sqlServer = serviceProvider.GetRequiredService<ISegmentHelper>().GetSQLServer(account);

            var recoveryModel =dbLayer
                .Execute1CSqlReader(string.Format(SqlQueries.DatabaseCore.GetRecovery, accountDatabase.SqlName), sqlServer)
                .Cast<string>()
                .FirstOrDefault();

            return recoveryModel;
        }
    }
}
