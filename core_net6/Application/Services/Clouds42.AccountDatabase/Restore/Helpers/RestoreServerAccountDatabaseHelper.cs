﻿using System.IO.Compression;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Restore.Helpers
{
    /// <summary>
    /// Хэлпер для восстановления серверных инф. баз
    /// </summary>
    public class RestoreServerAccountDatabaseHelper(
        IServiceProvider container,
        IUnitOfWork dbLayer,
        IAccountDataProvider accountDataProvider,
        ILogger42 logger)
    {
        /// <summary>
        ///     Восстановить информационную базу на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="backupPath">Путь к бэкапу на хранилке</param>
        /// <param name="sourceDatabase">ИБ на основе которой делается восстановление.</param>        
        public void RestoreAccountDatabaseInSqlServer(IAccountDatabase accountDatabase, string backupPath, IAccountDatabase sourceDatabase)
        {
            var commandTimeout =
                CloudConfigurationProvider.AccountDatabase.Restore.GetRestoreDatabaseQueryExecutionTimeOut() * 60;

            logger.Info($"[{accountDatabase.Id}] :: Начинаем установку базы на SQL сервер: Name={accountDatabase.V82Name}; SqlName={accountDatabase.SqlName};");

            var account = accountDataProvider.GetAccountOrThrowException(accountDatabase.AccountId);
            var sqlServer = container.GetRequiredService<ISegmentHelper>().GetSQLServer(account);

            dbLayer.SetCommandTimeout(commandTimeout);
            dbLayer.Execute1CSqlQuery($"{GetKillConnectionsQuery(accountDatabase.SqlName)}", sqlServer);

            logger.Info($"[{accountDatabase.Id}] :: удалены активные сессис базы");

            dbLayer.Execute1CSqlQuery(string.Format(SqlQueries.DatabaseCore.DropDatabase, accountDatabase.SqlName), sqlServer);

            if (accountDatabase.V82Name == sourceDatabase.V82Name)
            {
                logger.Info($"[{accountDatabase.Id}] :: начинается восстановление базы {accountDatabase.V82Name} ");
                dbLayer.Execute1CSqlQuery(string.Format(SqlQueries.DatabaseCore.RestoreDatabase, accountDatabase.SqlName, backupPath), sqlServer, commandTimeout);
            }
            else
            {
                logger.Info($"[{accountDatabase.Id}] :: начинается восстановление базы {accountDatabase.V82Name} на основание бэкапа {sourceDatabase.V82Name}");
                logger.Info($"[{accountDatabase.Id}] :: скрипт восстановления {GetQueryForRestoreAccountDatabase(accountDatabase, sourceDatabase, backupPath)}");
                dbLayer.Execute1CSqlQuery(GetQueryForRestoreAccountDatabase(accountDatabase, sourceDatabase, backupPath), sqlServer, commandTimeout);
            }
        }

        /// <summary>
        ///     Восстановить информационную базу на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="backupPath">Путь к бэкапу на хранилке</param>
        public void RestoreAccountDatabaseInSqlServer(IAccountDatabase accountDatabase, string backupPath)
        {
            var commandTimeout =
                CloudConfigurationProvider.AccountDatabase.Restore.GetRestoreDatabaseQueryExecutionTimeOut() * 60;

            logger.Trace($"[{accountDatabase.Id}] :: Начинаем установку базы на SQL сервер: Name={accountDatabase.V82Name}; SqlName={accountDatabase.SqlName};");

            var account = accountDataProvider.GetAccountOrThrowException(accountDatabase.AccountId);
            var sqlServer = container.GetRequiredService<ISegmentHelper>().GetSQLServer(account);

            dbLayer.SetCommandTimeout(commandTimeout);
            dbLayer.Execute1CSqlQuery($"{GetKillConnectionsQuery(accountDatabase.SqlName)}", sqlServer);

            logger.Trace($"[{accountDatabase.Id}] :: удалены активные сессис базы");

            if (!File.Exists(backupPath))
                throw new NotFoundException($"По пути {backupPath} не удалось найти бекап");

            dbLayer.Execute1CSqlQuery(string.Format(SqlQueries.DatabaseCore.DropDatabase, accountDatabase.SqlName), sqlServer);

            logger.Trace($"[{accountDatabase.Id}] :: начинается восстановление базы {accountDatabase.V82Name} ");

            dbLayer.Execute1CSqlQuery(string.Format(SqlQueries.DatabaseCore.RestoreDatabase, accountDatabase.SqlName, backupPath), sqlServer, commandTimeout);
        }

        /// <summary>
        /// Извлеч архив с бэкапом во временную директорию
        /// </summary>
        /// <param name="tempBackupDirectoryPath">Путь к временной директории для файла бэкапа</param>
        /// <param name="accountDatabaseBackupPath">Путь к бэкапу</param>
        /// <returns>Путь к файлу бэкапа</returns>
        public string ExtractBackupToTempDirectory(string tempBackupDirectoryPath, string accountDatabaseBackupPath)
        {
            ZipFile.ExtractToDirectory(accountDatabaseBackupPath, tempBackupDirectoryPath);

            var tempBackupFilePath = Directory.GetFiles(tempBackupDirectoryPath, "*.bak").FirstOrDefault();
            ValidateAccountDatabaseBackup(tempBackupDirectoryPath, tempBackupFilePath);

            return tempBackupFilePath;
        }

        /// <summary>
        /// Получить путь к временной директории для файла бэкапа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Путь к временной директории для файла бэкапа</returns>
        public string GetTempBackupDirectory(Guid accountId)
        {
            var account = accountDataProvider.GetAccountOrThrowException(accountId);

            var tempBackupDirectoryPath = Path.Combine(
                container.GetRequiredService<ISegmentHelper>().GetBackupStorage(account),
                "tomb",
                $"company_{account.IndexNumber}", "baks", DateTime.Now.ToString("yyyy-MM-dd-HH-mm"));

            if (Directory.Exists(tempBackupDirectoryPath))
                DirectoryHelper.DeleteDirectory(tempBackupDirectoryPath);

            Directory.CreateDirectory(tempBackupDirectoryPath);

            return tempBackupDirectoryPath;
        }

        /// <summary>
        /// Проверить бэкап инф. базы
        /// </summary>
        /// <param name="tempBackupDirectoryPath">Путь к временной директории для файла бэкапа</param>
        /// <param name="tempBackupFilePath">Путь к файлу бэкапа</param>
        private static void ValidateAccountDatabaseBackup(string tempBackupDirectoryPath, string tempBackupFilePath)
        {
            if (string.IsNullOrEmpty(tempBackupFilePath))
                throw new InvalidOperationException($"Невозможно получить имя файла бэкапа из пути {tempBackupDirectoryPath}");

            if (tempBackupDirectoryPath.ToDirectoryInfo().GetSize() == 0)
                throw new NotFoundException($"Директория '{tempBackupDirectoryPath}' пуста. Проверьте архив на наличие файлов.");
        }

        /// <summary>
        /// Получить запрос для закрытия всех соединений к инф. базе
        /// </summary>
        /// <param name="accountDatabaseSqlName">Название базы на SQL сервере</param>
        /// <returns>Запрос для закрытия всех соединений к инф. базе</returns>
        private static string GetKillConnectionsQuery(string accountDatabaseSqlName)
            => string.Format(SqlQueries.DatabaseCore.KillDatabaseSession, accountDatabaseSqlName);


        /// <summary>
        /// Получить запрос для восстановления инф. базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="sourceDatabase">ИБ на основе которой делается восстановление.</param>
        /// <param name="backupPath">Путь к бэкапу на хранилке</param>
        /// <returns>Запрос для восстановления инф. базы</returns>
        private static string GetQueryForRestoreAccountDatabase(IAccountDatabase accountDatabase, IAccountDatabase sourceDatabase, string backupPath)
            => string.Format(SqlQueries.DatabaseCore.ExtendedRestoreDatabase, accountDatabase.SqlName, backupPath, sourceDatabase.SqlName);
    }

        
}
