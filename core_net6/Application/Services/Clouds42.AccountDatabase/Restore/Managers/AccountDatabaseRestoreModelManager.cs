﻿using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Restore.Managers
{
    /// <summary>
    /// Менеджер для управления моделью восстановления инф. баз
    /// </summary>
    public class AccountDatabaseRestoreModelManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IManageAccountDatabaseRestoreModelProvider manageAccountDatabaseRestoreModelProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Выполнить аудит и корректировку моделей восстановления инф. баз
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ManageAccountDatabaseRestoreModel()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_ManageRestoreModel);

                manageAccountDatabaseRestoreModelProvider.ManageAccountDatabasesRestoreModel();
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка выполнении аудита и корректировки моделей восстановления инф. баз.]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
