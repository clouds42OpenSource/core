﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Restore.Managers
{
    /// <summary>
    /// Менеджер восстановления ИБ из бэкапа.
    /// </summary>
    public class RestoreAccountDatabaseBackupManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IRestoreAccountDatabaseBackupProvider restoreAccountDatabaseBackupProvider,
        IHandlerException handlerException,
        IAccountDatabaseDataProvider accountDatabaseDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IRestoreAccountDatabaseBackupManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>
        /// <param name="restoreAccountDatabaseParams">Параметры восстановления инф. базы из бэкапа</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult Restore(RestoreAccountDatabaseFromTombWorkerTaskParam restoreAccountDatabaseParams)
        {
            try
            {
                var accountDatabaseBackup = accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(restoreAccountDatabaseParams.AccountDatabaseBackupId);

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Restore, () => accountDatabaseBackup.AccountDatabase.AccountId);

                if (accountDatabaseBackup.AccountDatabase.IsDelimiter())
                    throw new NotImplementedException("Функция восстановления базы на разделителях не поддерживается.");

                restoreAccountDatabaseBackupProvider.Restore(restoreAccountDatabaseParams);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка восстановления ИБ из бэкапа] {restoreAccountDatabaseParams.AccountDatabaseBackupId} и тип восстановления {restoreAccountDatabaseParams.RestoreType}.");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Восстановить инф. базу из бэкапа после не успешной попытки АО
        /// </summary>
        /// <param name="restoreAccountDatabaseParams">Параметры восстановления инф. базы из бэкапа</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult RestoreAfterFailedAutoUpdate(RestoreAccountDatabaseFromTombWorkerTaskParam restoreAccountDatabaseParams)
        {
            try
            {
                var accountDatabaseBackup = accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(restoreAccountDatabaseParams.AccountDatabaseBackupId);

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Restore, () => accountDatabaseBackup.AccountDatabase.AccountId);

                if (accountDatabaseBackup.AccountDatabase.IsDelimiter())
                    throw new NotImplementedException("Функция восстановления базы на разделителях не поддерживается.");

                restoreAccountDatabaseBackupProvider.RestoreAfterFailedAutoUpdate(restoreAccountDatabaseParams);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка восстановления ИБ после не успешной попытки АО из бэкапа] {restoreAccountDatabaseParams.AccountDatabaseBackupId} " +
                    $"и тип восстановления {restoreAccountDatabaseParams.RestoreType}.");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
