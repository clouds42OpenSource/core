﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.TerminateSessions.Providers
{
    /// <summary>
    /// Провайдер для работы с данными
    /// завершения сеансов в информационной базе
    /// </summary>
    internal class TerminationSessionsInDatabaseDataProvider(IUnitOfWork dbLayer)
        : ITerminationSessionsInDatabaseDataProvider
    {
        /// <summary>
        /// Получить запись о завершении сеансов
        /// в информационной базе или выкинуть исключение
        /// </summary>
        /// <param name="id">Id записи/процесса завершения</param>
        /// <returns>Запись о завершении сеансов
        /// в информационной базе</returns>
        public TerminationSessionsInDatabase GetTerminationSessionsInDatabaseOrThrowException(Guid id) =>
            dbLayer.GetGenericRepository<TerminationSessionsInDatabase>().FirstOrDefault(ts => ts.Id == id) ??
            throw new NotFoundException(
                $"Не удалось получить запись о завершении сеансов в информационной базе по Id= '{id}'");

        /// <summary>
        /// Получить информационную базу по Id процессу завершения сеансов
        /// </summary>
        /// <param name="id">Id записи/процесса завершения</param>
        /// <returns>Информационная база</returns>
        public Domain.DataModels.AccountDatabase GetDatabaseByTerminationSessionsIdOrThrowException(Guid id) =>
            (from terminationSessions in dbLayer.GetGenericRepository<TerminationSessionsInDatabase>().WhereLazy()
                join database in dbLayer.DatabasesRepository.WhereLazy() on terminationSessions.AccountDatabaseId
                    equals database.Id
                where terminationSessions.Id == id
                select database).FirstOrDefault() ??
            throw new NotFoundException($"Не удалось получить базу по Id процессу завершения сеансов '{id}'");

        /// <summary>
        /// Проверить существует ли процесс завершения сеансов в базе данных
        /// Запись со статусом "В процессе завершения сеансов"
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Признак указывающий что существует
        /// процесс завершения сеансов в базе данных</returns>
        public bool IsExistTerminatingSessionsProcessInDatabase(Guid databaseId) =>
            dbLayer.GetGenericRepository<TerminationSessionsInDatabase>().FirstOrDefault(ts =>
                ts.AccountDatabaseId == databaseId &&
                ts.TerminateSessionsInDbStatus == TerminateSessionsInDbStatusEnum.InProcess) != null;
    }
}
