﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.TerminateSessionsInDatabaseProcessFlow;

namespace Clouds42.AccountDatabase.TerminateSessions.Providers
{
    /// <summary>
    /// Провайдер для завершения сеансов в информационной базе
    /// </summary>
    internal class TerminateSessionsInDatabaseProvider(
        IRegisterTerminationSessionsInDatabaseResultProvider registerTerminationSessionsInDatabaseResultProvider,
        ITerminateSessionsInDatabaseProcessFlow terminateSessionsInDatabaseProcessFlow,
        ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider,
        ILogger42 logger)
        : ITerminateSessionsInDatabaseProvider
    {
        /// <summary>
        /// Попытаться завершить сеансы в информационной базе
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат завершения сеансов в информационной базе</returns>
        public bool TryTerminate(TerminateSessionsInDatabaseJobParamsDto model, out string errorMessage)
        {
            errorMessage = null;

            var terminateResult = terminateSessionsInDatabaseProcessFlow.Run(model);

            if (terminateResult.Finish)
                return true;

            errorMessage = terminateResult.Message;

            registerTerminationSessionsInDatabaseResultProvider.Register(
                new RegisterTerminationSessionsInDatabaseResultDto
                {
                    TerminationSessionsInDatabaseId = model.TerminationSessionsInDatabaseId,
                    TerminateSessionsInDbStatus = TerminateSessionsInDbStatusEnum.Error
                });

            return false;
        }


        /// <summary>
        /// Завершить сеансы в информационной базе
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Результат завершения сеансов в информационной базе</returns>
        public void Terminate(TerminateSessionsInDatabaseJobParamsDto model)
        {
            var accountDatabase =
                terminationSessionsInDatabaseDataProvider.GetDatabaseByTerminationSessionsIdOrThrowException(
                    model.TerminationSessionsInDatabaseId);

            var message = $"Завершение активных сеансов в базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}";

            try
            {
                if (!TryTerminate(model, out var errorMessage))
                    throw new InvalidOperationException(errorMessage);
            }
            catch (Exception ex)
            {
                var errorMessage = $@"[{message} выполнено с ошибкой]";
                logger.Warn(ex, errorMessage);
            }
        }
    }
}
