﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.TerminateSessions.Providers
{
    /// <summary>
    /// Провайдер для создания записи завершения сеансов в информационной базе 
    /// </summary>
    internal class CreateTerminationSessionsInDatabaseProvider(IUnitOfWork dbLayer)
        : ICreateTerminationSessionsInDatabaseProvider
    {
        /// <summary>
        /// Создать запись завершения сеансов в информационной базе
        /// </summary>
        /// <param name="model">Модель создания записи завершения сеансов в информационной базе</param>
        /// <returns>Id созданной записи</returns>
        public Guid Create(CreateTerminationSessionsInDatabaseDto model)
        {
            var terminationSessionsInDatabase = new TerminationSessionsInDatabase
            {
                Id = Guid.NewGuid(),
                TerminateSessionsInDbStatus = model.TerminateSessionsInDbStatus,
                AccountDatabaseId = model.DatabaseId
            };

            if (model.TerminateSessionsInDbStatus == TerminateSessionsInDbStatusEnum.Success)
                terminationSessionsInDatabase.TerminatedSessionsDate = DateTime.Now;

            dbLayer.GetGenericRepository<TerminationSessionsInDatabase>().Insert(terminationSessionsInDatabase);
            dbLayer.Save();

            return terminationSessionsInDatabase.Id;
        }
    }
}
