﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.TerminateSessions.Providers
{
    /// <summary>
    /// Провайдер для регистрации результата завершения
    /// сеансов в информационной базе
    /// </summary>
    internal class
        RegisterTerminationSessionsInDatabaseResultProvider(
            ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider,
            IUnitOfWork dbLayer)
        : IRegisterTerminationSessionsInDatabaseResultProvider
    {
        /// <summary>
        /// Зарегестрировать результат завершения
        /// сеансов в информационной базе
        /// </summary>
        /// <param name="model">Модель регистрации результата завершения
        /// сеансов в информационной базе</param>
        public void Register(RegisterTerminationSessionsInDatabaseResultDto model)
        {
            var terminationSessionsInDatabase =
                terminationSessionsInDatabaseDataProvider.GetTerminationSessionsInDatabaseOrThrowException(
                    model.TerminationSessionsInDatabaseId);

            terminationSessionsInDatabase.TerminateSessionsInDbStatus = model.TerminateSessionsInDbStatus;

            if (model.TerminateSessionsInDbStatus == TerminateSessionsInDbStatusEnum.Success)
                terminationSessionsInDatabase.TerminatedSessionsDate = DateTime.Now;

            dbLayer.GetGenericRepository<TerminationSessionsInDatabase>().Update(terminationSessionsInDatabase);
            dbLayer.Save();
        }
    }
}
