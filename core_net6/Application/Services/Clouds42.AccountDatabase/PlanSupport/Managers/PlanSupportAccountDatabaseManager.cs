﻿using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.PlanSupport.Managers
{
    /// <summary>
    /// Менеджер планирования тех работ в инф. базах
    /// </summary>
    public class PlanSupportAccountDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IPlanSupportAccountDatabaseProvider planSupportAccountDatabaseProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Провести планирование тех работ в информационных базах.
        /// </summary>
        public ManagerResult PlanTehSupport()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AcDbSupport_PlanTehSupport);

                planSupportAccountDatabaseProvider.RunPlaning();

                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
