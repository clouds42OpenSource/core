﻿using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.PlanSupport.Providers
{
    /// <summary>
    /// Провайдер планирования обновления версии информационной базы.
    /// </summary>
    internal class PlanUpdateVersionAccountDatabaseProvider(
        IUnitOfWork dbLayer,
        ICfuProvider cfuProvider,
        AccountDatabaseSupportModelCreator accountDatabaseSupportModelCreator,
        AcDbSupportHelper acDbSupportHelper,
        ILogger42 logger,
        IHandlerException handlerException,
        ISupportAuthorizationProvider supportAuthorizationProvider,
        IAccessProvider accessProvider)
        : PlanSupportBaseProvider(dbLayer, acDbSupportHelper), IPlanUpdateVersionAccountDatabaseProvider
    {
        readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Запланировать обновление версии ИБ
        /// </summary>
        /// <param name="support">Модель поддержки базы</param>
        public bool PlanUpdateVersionAccountDatabase(AcDbSupport support)
        {
            logger.Info($"Начинаем планирование обновления для базы {support.AccountDatabase.V82Name}");

            var connectResult = GetAccountDatabaseSupportVersion(support);

            UpdateAcDbSupportVersion(support, connectResult.Result.Version);

            logger.Info($"текущая версия релиза базы {support.AccountDatabase.V82Name} - {connectResult.Result.Version}");

            var configuration1C = GetReferenceConfigurations1C(connectResult.Result?.SynonymConfiguration);

            if (configuration1C == null)
            {
                var history = CreateSupportHistoryRecord(support, DatabaseSupportOperation.PlanAutoUpdate, $"Для конфигурации «{connectResult.Result.SynonymConfiguration}» не найдена схема обновлений.", SupportHistoryState.Error);
                DbLayer.AcDbSupportHistoryRepository.Insert(history);
                DbLayer.Save();
                return false;
            }

            var databaseSupportModel = accountDatabaseSupportModelCreator.CreateModel(support.AccountDatabasesID);

            var lastUpdateVersion = cfuProvider.GetLastUpdateVersion(configuration1C, connectResult.Result.Version, databaseSupportModel.PlatformVersion);

            if (lastUpdateVersion == null)
            {
                logger.Info($"База {support.AccountDatabase.V82Name}. Не найден новый релиз для обновления");
                return false;
            }

            if (!string.IsNullOrEmpty(lastUpdateVersion.Platform1CVersion) &&
                new VersionComparer().Compare(lastUpdateVersion.Platform1CVersion, databaseSupportModel.PlatformVersion) == 1)
            {
                if (support.AccountDatabase.Account.AccountConfiguration.IsVip)
                    SendLowPlatformLetter(support.AccountDatabase.V82Name, support.AccountDatabase.AccountId);

                var history = CreateSupportHistoryRecord(support, DatabaseSupportOperation.PlanAutoUpdate,
                     $"Текущая версия платформы базы {databaseSupportModel.PlatformVersion}," +
                               $"меньше версии платформы с которой можно обновить базу {lastUpdateVersion.Platform1CVersion}."
                    , SupportHistoryState.Error);
                DbLayer.AcDbSupportHistoryRepository.Insert(history);
                DbLayer.Save();
                return false;
            }

            logger.Info($"Версия базы на которую можем обновиться {support.AccountDatabase.V82Name} - {lastUpdateVersion.Version}");

            CreateUpdatePlan(support, configuration1C, connectResult.Result.Version, lastUpdateVersion.Version);

            return true;
        }

        /// <summary>
        /// Получить справочные данные конфигурации 1С.
        /// </summary>
        /// <param name="synonymConfiguration">Синоним конфигурации 1С.</param>
        /// <returns>Конфигурация 1С справочные данные.</returns>
        private Configurations1C? GetReferenceConfigurations1C(string synonymConfiguration)
        {
            if (string.IsNullOrEmpty(synonymConfiguration))
                return null;

            var configuration1C = (from conf1C in DbLayer.Configurations1CRepository.WhereLazy()
                                   join variation in DbLayer.ConfigurationNameVariationRepository.WhereLazy() on conf1C.Name equals
                                       variation.Configuration1CName
                                   where variation.VariationName == synonymConfiguration
                                   select conf1C).FirstOrDefault();
            return configuration1C;
        }

        /// <summary>
        /// Обновить текущую версию релиза конфигурации инф. базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки базы</param>
        /// <param name="actualVersion">Актуальная версия релиза конфигурации инф. базы</param>
        private void UpdateAcDbSupportVersion(AcDbSupport acDbSupport, string actualVersion)
        {
            if (acDbSupport.CurrentVersion == actualVersion)
                return;

            acDbSupport.CurrentVersion = actualVersion;
            DbLayer.AcDbSupportRepository.Update(acDbSupport);
            DbLayer.Save();
        }

        /// <summary>
        /// Получить метаданные из инф. базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки базы</param>
        /// <returns>Модель метаданных инф. базы</returns>
        private ConnectorResultDto<MetadataResultDto> GetAcDbMetadataInfo(AcDbSupport acDbSupport)
        {
            var connectResult = supportAuthorizationProvider.GetMetadataInfoAccountDatabase(acDbSupport);
            if (connectResult.ConnectCode == ConnectCodeDto.Success)
                return connectResult;

            if (connectResult.ConnectCode == ConnectCodeDto.PasswordOrLoginIncorrect)
                RegisterNotValidLoginPassword(acDbSupport);

            logger.Warn($"Метаданные не получены по причине {connectResult.Message}");
            return connectResult;   
        }

        /// <summary>
        /// Получить версию поддержки базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки базы</param>
        /// <returns>Модель метаданных инф. базы</returns>
        private ConnectorResultDto<MetadataResultDto> GetAccountDatabaseSupportVersion(AcDbSupport acDbSupport)
        {
            if (NeedUseComConnectionForGetSupportVersion(acDbSupport))
                return GetAcDbMetadataInfo(acDbSupport);

            return new ConnectorResultDto<MetadataResultDto>
            {
                ConnectCode = ConnectCodeDto.Success,
                Result = new MetadataResultDto(acDbSupport.CurrentVersion, acDbSupport.SynonymConfiguration ?? acDbSupport.ConfigurationName)
            };
        }

        /// <summary>
        /// Признак необходимости использовать COM соединение для получения версии конфигурации инф. базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки базы</param>
        /// <returns>true - если в модели поддержки базы не указаны версия или название конфигурации</returns>
        private bool NeedUseComConnectionForGetSupportVersion(AcDbSupport acDbSupport)
            => (string.IsNullOrEmpty(acDbSupport.SynonymConfiguration) && string.IsNullOrEmpty(acDbSupport.ConfigurationName)) ||
               string.IsNullOrEmpty(acDbSupport.CurrentVersion);

        /// <summary>
        /// Построить план на обновления для базы
        /// </summary>
        private void CreateUpdatePlan(AcDbSupport support, Configurations1C configuration1C, string currentVersionDb, string updateVersion)
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {

                DbLayer.AcDbSupportRepository.SetConfiguration(support, configuration1C);
                DbLayer.Save();

                var autoUpdateHistory = CreateSupportHistoryRecord(support,
                    DatabaseSupportOperation.PlanAutoUpdate,
                    $"Запланировано обновление базы на {(int)support.TimeOfUpdate} часов '{DateTime.Now.ToString(Globalization.ShortDateFormat)}'", SupportHistoryState.Success);

                autoUpdateHistory.OldVersionCfu = currentVersionDb;
                autoUpdateHistory.CurrentVersionCfu = updateVersion;

                DbLayer.AcDbSupportHistoryRepository.Insert(autoUpdateHistory);
                logger.Info($"База {support.AccountDatabase.V82Name} поставлена в очередь на АО");

                support.PrepearedForUpdate = true;
                support.CurrentVersion = currentVersionDb;
                support.UpdateVersion = updateVersion;

                DbLayer.AcDbSupportRepository.Update(support);
                DbLayer.Save();

                logger.Info($"База {support.AccountDatabase.V82Name} :: успешно создан план на обновление");

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания плана для базы] {support.AccountDatabase.V82Name}");
                dbScope.Rollback();
                throw;
            }
        }

        public bool CanPlugAutoUpdate(AcDbSupport support)
        {
            var configuration1C = GetReferenceConfigurations1C(support.SynonymConfiguration ?? support.ConfigurationName);
            return configuration1C != null;
        }

        /// <summary>
        /// Отправить письмо клиенту о низкой версии платформы
        /// </summary>
        private void SendLowPlatformLetter(string databaseName, Guid accountId)
        {
            var accountAdminId = accessProvider.GetAccountAdmins(accountId).FirstOrDefault();
            var accountAdminEmail = _dbLayer.AccountUsersRepository.FirstOrDefault(x => x.Id == accountAdminId);
            var message = "Дальнейшее автообновление баз невозможно, поскольку ваша текущая версия платформы больше не соответствует " +
                "минимальным требованиям для получения новых релизов конфигураций. Мы рекомендуем вам отправить запрос " +
                "<b>с адреса электронной почты администратора аккаунта</b> на vip.support@42clouds.com с просьбой о выполнении обновления версии платформы. " +
                "Пожалуйста, не забудьте указать удобное для вас <b>время и дату проведения работ</b> на вашем сервере.";


            logger.Trace($"Отправляем письмо об пониженой версии платформы на {accountAdminEmail.Email}");
            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(accountAdminEmail.Email)
                .Body(message)
                .Subject($"Ошибка планирования Автообновления АО базы {databaseName}")
                .SendViaNewThread();
        }
    }
}
