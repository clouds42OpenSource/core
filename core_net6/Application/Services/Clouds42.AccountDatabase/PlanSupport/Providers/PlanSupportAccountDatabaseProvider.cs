﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.AccountDatabase.PlanSupport.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.PlanSupport.Providers
{
    /// <summary>
    /// Провайдер планировния поддержки инф. баз
    /// </summary>
    internal class PlanSupportAccountDatabaseProvider(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IPlanTehSupportAccountDatabaseProvider planTehSupportAccountDatabaseProvider,
        IPlanUpdateVersionAccountDatabaseProvider planUpdateVersionAccountDatabaseProvider,
        IPlanSupportAccountDatabaseDataProvider planSupportAccountDatabaseDataProvider,
        PlanSupportAccountDatabaseHelper planSupportAccountDatabaseHelper,
        ILogger42 logger)
        : IPlanSupportAccountDatabaseProvider
    {
        private readonly Lazy<int> _accountDatabasesTehSupportMaxCountPerNight = new(CloudConfigurationProvider.AccountDatabase
            .Support.GetAccountDatabasesTehSupportMaxCountPerNigth);
        private readonly Lazy<int> _tehSupportPeriodicDaysCount = new(CloudConfigurationProvider.AccountDatabase.Support.GetTehSupportPeriodicDaysCount);
        private int _autoUpdateCount;
        private int _tehSupportCount;

        /// <summary>
        /// Запустить планирование
        /// </summary>
        public void RunPlaning()
        {
            var tiIDateList = CloudConfigurationProvider.AccountDatabase.Support.GetTiIDateList().Split(',').Select(int.Parse).ToArray();
            PlanSupportAccountDatabasesResultDto planTestAndRepairResult;
            var supportOperation = DatabaseSupportOperation.PlanAutoUpdate;

            if (tiIDateList.Contains(DateTime.Now.Day))
            {
                planTestAndRepairResult = PlanTestAndRepair();
                supportOperation = DatabaseSupportOperation.PlanTehSupport;
            }
            else
                planTestAndRepairResult = PlanAutoUpdate();

            planSupportAccountDatabaseHelper.ProcessPlanSupportAccountDatabasesResult(planTestAndRepairResult, supportOperation);
        }

        /// <summary>
        /// Запустить планирование автообновления в базах.
        /// </summary>
        /// <returns>Результат планирования поддержки инф. баз</returns>
        private PlanSupportAccountDatabasesResultDto PlanAutoUpdate()
        {
            logger.Info("Начинаю планирование Автообновления");
            var currentPage = 1;
            var databasesForSupport = new List<BeforeSupportAccountDatabaseModelDto.DbItem>();
            var databasesIdsWithErrors = new List<KeyValuePair<Guid, string>>();
            var connectedAccountDatabases = planSupportAccountDatabaseDataProvider.GetAcDatabasesForPlanAuWithPagination(currentPage);

            while (connectedAccountDatabases.Records.Any() && !AuQueueIsFull())
            {
                foreach (var connectedDbOnAuData in connectedAccountDatabases.Records)
                {
                    try
                    {
                        var planAccountDatabase = PlanAutoUpdateOperationWithDatabase(connectedDbOnAuData.Id);
                        if (planAccountDatabase != null)
                            databasesForSupport.Add(planAccountDatabase);
                    }
                    catch (Exception ex)
                    {
                        databasesIdsWithErrors.Add(new KeyValuePair<Guid, string>(connectedDbOnAuData.Id, ex.Message));
                    }
                }

                currentPage++;
                connectedAccountDatabases =
                    planSupportAccountDatabaseDataProvider.GetAcDatabasesForPlanAuWithPagination(currentPage);
            }

            return PlanSupportAccountDatabaseHelper.CreatePlanSupportAccountDatabasesResult(databasesForSupport,
                databasesIdsWithErrors);
        }

        /// <summary>
        /// Запустить планирование ТиИ в базах.
        /// </summary>
        /// <returns>Результат планирования поддержки инф. баз</returns>
        private PlanSupportAccountDatabasesResultDto PlanTestAndRepair()
        {
            logger.Info("Начинаю планирование ТиИ");
            var currentPage = 1;
            var databasesIdsWithErrors = new List<KeyValuePair<Guid, string>>();
            var databasesForSupport = new List<BeforeSupportAccountDatabaseModelDto.DbItem>();
            var accountDatabasesPage = planSupportAccountDatabaseDataProvider.GetAcDatabasesForPlanTarWithPagination(currentPage);

            while (accountDatabasesPage.Any() && !TarQueueIsFull())
            {
                using (var accountDatabasePageEnumerator = accountDatabasesPage.GetEnumerator())
                {
                    AccountDatabasesOfAccountModelDto accountDatabasesOfAccount;
                    while (accountDatabasePageEnumerator.MoveNext() &&
                           (accountDatabasesOfAccount = accountDatabasePageEnumerator.Current) != null &&
                           !TarQueueIsFull())
                    {
                        accountDatabasesOfAccount.AccountDatabases.ForEach(acDbId =>
                        {
                            try
                            {
                                var planAccountDatabase = PlanSupportOperationWithAccountDatabase(acDbId);

                                if (planAccountDatabase != null)
                                    databasesForSupport.Add(planAccountDatabase);
                            }
                            catch (Exception ex)
                            {
                                databasesIdsWithErrors.Add(new KeyValuePair<Guid, string>(acDbId, ex.Message));
                            }
                        });
                    }
                }

                currentPage++;
                accountDatabasesPage =
                    planSupportAccountDatabaseDataProvider.GetAcDatabasesForPlanTarWithPagination(currentPage);
            }

            return PlanSupportAccountDatabaseHelper.CreatePlanSupportAccountDatabasesResult(databasesForSupport,
                databasesIdsWithErrors);
        }

        /// <summary>
        /// Запланировать тех работы в информационной базе.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор инфо базы.</param>
        private BeforeSupportAccountDatabaseModelDto.DbItem PlanSupportOperationWithAccountDatabase(Guid accountDatabaseId)
        {
            
          var accountDatabase = GetAccountDatabaseOrThrowException(accountDatabaseId);
          if (!CanPlanTehSupport(accountDatabase.AcDbSupport))
              return null;

          if (!planTehSupportAccountDatabaseProvider
              .PlanTehSupportAccountDatabase(accountDatabase.AcDbSupport))
              return null;

          _tehSupportCount++;
          return PlanSupportAccountDatabaseHelper.CreateNotificationDbItem(accountDatabase, DatabaseSupportOperation.PlanTehSupport);
            
        }

        /// <summary>
        /// Запланировать проведение АО для инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Запланированная инф. база</returns>
        private BeforeSupportAccountDatabaseModelDto.DbItem PlanAutoUpdateOperationWithDatabase(Guid accountDatabaseId)
        {
            
            var accountDatabase = GetAccountDatabaseOrThrowException(accountDatabaseId);
            if (!CanPlanAutoUpdate(accountDatabase.AcDbSupport))
                return null;

            if (!planUpdateVersionAccountDatabaseProvider.PlanUpdateVersionAccountDatabase(accountDatabase.AcDbSupport))
                return null;

            _autoUpdateCount++;
            return PlanSupportAccountDatabaseHelper.CreateNotificationDbItem(accountDatabase, DatabaseSupportOperation.PlanAutoUpdate);
            
        }

        /// <summary>
        /// Получить инф. базу или выкинуть исключение
        /// </summary>
        /// <param name="container">Экземпляр DI контейнера</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Инф. база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid accountDatabaseId)
            => accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);

        /// <summary>
        /// Признак что очередь инф. баз на АО заполнена
        /// </summary>
        /// <returns>true-если очередь полностью заполнена.</returns>
        private bool AuQueueIsFull()
            => _autoUpdateCount >= _accountDatabasesTehSupportMaxCountPerNight.Value;

        /// <summary>
        /// Признак что очередь инф. баз на ТиИ заполнена
        /// </summary>
        /// <returns>true-если очередь полностью заполнена.</returns>
        private bool TarQueueIsFull()
            => _tehSupportCount >= _accountDatabasesTehSupportMaxCountPerNight.Value;

        /// <summary>
        /// Проверить возможность запланировать АО базы.
        /// </summary>
        /// <param name="support">Обновляемвя база.</param>
        private bool CanPlanAutoUpdate(AcDbSupport support)
            => support is { HasAutoUpdate: true, DatabaseHasModifications: false } 
            && _autoUpdateCount < _accountDatabasesTehSupportMaxCountPerNight.Value
            && support.State == (int)SupportState.AutorizationSuccess;

        /// <summary>
        /// Проверить возможность запланировать ТиИ базы.
        /// </summary>
        /// <param name="support">Обновляемвя база.</param>
        private bool CanPlanTehSupport(AcDbSupport support)
        {
            var tehSupportPeriodicDate = DateTime.Now.AddDays(-_tehSupportPeriodicDaysCount.Value);
            return (!support.TehSupportDate.HasValue ||
                    support.TehSupportDate <= tehSupportPeriodicDate) &&
                   support.HasSupport &&
                   _tehSupportCount < _accountDatabasesTehSupportMaxCountPerNight.Value
                   && support.State == (int)SupportState.AutorizationSuccess;
        }
    }
}
