﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.PlanSupport.Providers
{
    /// <summary>
    /// Провадйер планирования ТиИ информационной базы
    /// </summary>
    internal class PlanTehSupportAccountDatabaseProvider(
        IUnitOfWork dbLayer,
        IAccountDatabaseManager accountDatabaseManager,
        IHandlerException handlerException,
        AcDbSupportHelper acDbSupportHelper,
        ILogger42 logger)
        : PlanSupportBaseProvider(dbLayer, acDbSupportHelper), IPlanTehSupportAccountDatabaseProvider
    {
        /// <summary>
        /// Запланировать проведение ТиИ для информационной базы.
        /// </summary>
        /// <param name="support">Информационная база.</param>
        /// <returns>Признак успешности.</returns>
        public bool PlanTehSupportAccountDatabase(AcDbSupport support)
        {

            logger.Info($"Начинаем планирование ТиИ для базы {support.AccountDatabase.V82Name}");

            var connectResult = accountDatabaseManager.GetMetadataInfoAccountDatabase(support.AccountDatabasesID);

            if (connectResult.Result == null)
            {
                logger.Warn($"База {support.AccountDatabase.V82Name} не запланирована на ТиИ, так как не удалось прочитать метаданные");
                return false;
            }

            if (connectResult.Error ||
                connectResult.Result.ConnectCode == ConnectCodeDto.DatabaseDamaged ||
                connectResult.Result.ConnectCode == ConnectCodeDto.ConnectError)
            {
                logger.Warn($"База не запланирована на ТиИ по причине : {connectResult.Result.Message}");
                return false;
            }

            if (connectResult.Result.ConnectCode == ConnectCodeDto.PasswordOrLoginIncorrect)
            {
                logger.Warn($"База не запланирована на ТиИ по причине : {connectResult.Result.Message}");
                RegisterNotValidLoginPassword(support);
                return false;
            }           

            CreateTehSupportPlan(support);

            return true;
        }        

        private void CreateTehSupportPlan(AcDbSupport support)
        {
            var logKey = support.AccountDatabasesID;

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                support.PreparedForTehSupport = true;

                DbLayer.AcDbSupportRepository.Update(support);

                var history = CreateSupportHistoryRecord(support, DatabaseSupportOperation.PlanTehSupport, $"Запланировано проведение ТиИ на вечер {DateTime.Now:D}", SupportHistoryState.Success);
                DbLayer.AcDbSupportHistoryRepository.Insert(history);

                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка планирования ТиИ для базы] {logKey}");
                dbScope.Rollback();
                throw;
            }
        }
       
    }
}
