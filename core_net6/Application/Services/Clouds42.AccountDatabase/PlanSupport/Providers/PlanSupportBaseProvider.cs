﻿using Clouds42.AccountDatabase.Helpers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.PlanSupport.Providers
{
    /// <summary>
    /// Базовый класс планирования поддержки инф. баз
    /// </summary>
    internal abstract class PlanSupportBaseProvider(IUnitOfWork dbLayer, AcDbSupportHelper acDbSupportHelper)
    {
        protected readonly IUnitOfWork DbLayer = dbLayer;

        /// <summary>
        /// Зарегистрировать не верные авторизационные данные
        /// </summary>
        /// <param name="support">Модель поддержки инф. базы</param>
        protected void RegisterNotValidLoginPassword(AcDbSupport support)
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var errorMessage = acDbSupportHelper.GetInvalidAuthorizationMessage(support.AccountDatabasesID);

                var autoUpdateHistory = CreateSupportHistoryRecord(support, DatabaseSupportOperation.PlanAutoUpdate, errorMessage,
                    SupportHistoryState.Error);
                DbLayer.AcDbSupportHistoryRepository.Insert(autoUpdateHistory);

                support.PrepearedForUpdate = false;
                support.PreparedForTehSupport = false;
                support.HasAutoUpdate = false;
                support.HasSupport = false;
                support.State = (int)SupportState.NotValidAuthData;

                DbLayer.AcDbSupportRepository.Update(support);
                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать историческую запись поддержки
        /// </summary>
        /// <param name="support">Модель поддержки инф. базы</param>
        /// <param name="operation">Тип операции</param>
        /// <param name="description">Описание</param>
        /// <param name="state">Статус выполнения</param>
        /// <returns>Историческая запись поддержки</returns>
        protected AcDbSupportHistory CreateSupportHistoryRecord(AcDbSupport support, DatabaseSupportOperation operation, string description, SupportHistoryState state)
        {
            return new AcDbSupportHistory
            {
                AccountDatabaseId = support.AccountDatabasesID,
                State = state,
                Description = description,
                Operation = operation,
                ID = Guid.NewGuid()
            };
        }
    }
}