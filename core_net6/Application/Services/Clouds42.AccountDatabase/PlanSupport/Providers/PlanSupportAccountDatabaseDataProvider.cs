﻿using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries;
using MediatR;
using PagedList.Core;
using PagedListExtensionsNetFramework;

namespace Clouds42.AccountDatabase.PlanSupport.Providers
{
    /// <summary>
    /// Провайдер данных для планирования поддержки инф. баз
    /// </summary>
    internal class PlanSupportAccountDatabaseDataProvider(
        IUnitOfWork dbLayer,
        IBillingServiceDataProvider billingServiceDataProvider,
        IConfigurations1CDataProvider configurations1CDataProvider,
        ISender sender)
        : IPlanSupportAccountDatabaseDataProvider
    {
        private readonly Lazy<int> _accountDatabasesTehSupportMaxCountPerNight = new(CloudConfigurationProvider.AccountDatabase
            .Support.GetAccountDatabasesTehSupportMaxCountPerNigth);

        /// <summary>
        /// Получить постраничный список информационых баз, для которых нужно спланировать ТиИ.
        /// </summary>
        /// <param name="pageNumber">Номер запрашиваемой страницы.</param>
        /// <returns>Список инф. баз</returns>
        public List<AccountDatabasesOfAccountModelDto> GetAcDatabasesForPlanTarWithPagination(int pageNumber)
        {
            var billingService = billingServiceDataProvider
                 .GetSystemService(Clouds42Service.MyEnterprise);

            var query = (from account in dbLayer.AccountsRepository.WhereLazy()
                         join db in dbLayer.DatabasesRepository.WhereLazy() on account.Id equals db.AccountId
                         join res in dbLayer.ResourceConfigurationRepository.WhereLazy() on new
                         {
                             db.AccountId,
                             BillingServiceId = billingService.Id
                         } equals new { res.AccountId, res.BillingServiceId }
                         join acDbSupport in dbLayer.AcDbSupportRepository.WhereLazy()
                             on db.Id equals acDbSupport.AccountDatabasesID
                         where
                             db.State == DatabaseState.Ready.ToString() &&
                             acDbSupport.State == (int)SupportState.AutorizationSuccess &&
                             !acDbSupport.PrepearedForUpdate &&
                             !acDbSupport.PreparedForTehSupport &&
                             res.Frozen == false &&
                             (acDbSupport.HasSupport || acDbSupport.HasAutoUpdate)
                         orderby db.AccountId
                         select db)
                .ToPagedList(pageNumber, _accountDatabasesTehSupportMaxCountPerNight.Value)
                .GroupBy(d => d.AccountId)
                .Select(gr => new AccountDatabasesOfAccountModelDto
                { AccountId = gr.Key, AccountDatabases = gr.Select(db => db.Id).ToList() });

            return query.ToList();
        }

        /// <summary>
        /// Получить постраничный список информационых баз, для которых нужно спланировать АО.
        /// </summary>
        /// <param name="pageNumber">Номер запрашиваемой страницы.</param>
        /// <returns>Список инф. баз</returns>
        public PagedDto<AutoUpdateAccountDatabaseDto> GetAcDatabasesForPlanAuWithPagination(int pageNumber)
        {
          return sender.Send(new DatabaseInAutoUpdateQueueQuery
          {
              Filter = new DatabaseInAutoUpdateQueryFilter
              {
                  IsActiveRent1COnly = true,
                  Configurations = configurations1CDataProvider.GetConfigurationNames()
              },
              PageNumber = pageNumber,
              AutoUpdateState = AutoUpdateState.Connected,
              PageSize = _accountDatabasesTehSupportMaxCountPerNight.Value,
              OrderBy = $"{nameof(AutoUpdateAccountDatabaseDto.ConnectDate)}.desc"
          }).Result.Result;
        }
    }
}
