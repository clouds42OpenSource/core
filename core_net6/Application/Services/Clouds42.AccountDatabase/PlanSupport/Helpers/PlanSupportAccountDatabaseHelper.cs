﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.PlanSupport.Helpers
{
    /// <summary>
    /// Хэлпер для планирования поддержки инф. баз
    /// </summary>
    public class PlanSupportAccountDatabaseHelper(
        ILetterNotificationProcessor letterNotificationProcessor,
        IHandlerException handlerException,
        ILogger42 logger)
    {
        /// <summary>
        /// Создать модель инф. базы с данными для уведомления.
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="supportOperation">Тип операции поддержки</param>
        /// <returns>Модель уведомления</returns>
        public static BeforeSupportAccountDatabaseModelDto.DbItem CreateNotificationDbItem(Domain.DataModels.AccountDatabase accountDatabase,
            DatabaseSupportOperation supportOperation)
            => new()
            {
                Caption = accountDatabase.Caption,
                V82Name = accountDatabase.V82Name,
                CurrentVersion = accountDatabase.AcDbSupport.CurrentVersion,
                UpdateVersion = accountDatabase.AcDbSupport.UpdateVersion,
                Operation = supportOperation,
                AccountId = accountDatabase.AccountId,
                TimeOfOperation = accountDatabase.AcDbSupport.TimeOfUpdate
            };

        /// <summary>
        /// Создать результат планирования поддержки инф. баз
        /// </summary>
        /// <param name="databasesForSupport">Список инф. баз для выполнения поддержки</param>
        /// <param name="databasesIdsWithErrors">Список инф. баз с ошибкой планирования</param>
        /// <returns>Результат планирования поддержки инф. баз</returns>
        public static PlanSupportAccountDatabasesResultDto CreatePlanSupportAccountDatabasesResult(
            List<BeforeSupportAccountDatabaseModelDto.DbItem> databasesForSupport,
            List<KeyValuePair<Guid, string>> databasesIdsWithErrors)
            => new()
            {
                AccountDatabasesWithErrors = databasesIdsWithErrors,
                AccountDatabasesForPerformingSupport = databasesForSupport
            };

        /// <summary>
        /// Обработать результат планирования поддержки
        /// </summary>
        /// <param name="planSupportResult">Результат планирования</param>
        /// <param name="supportOperation">Тип операции</param>
        public void ProcessPlanSupportAccountDatabasesResult(PlanSupportAccountDatabasesResultDto planSupportResult, DatabaseSupportOperation supportOperation)
        {
            HandleDatabasesWithError(planSupportResult.AccountDatabasesWithErrors, supportOperation);

            var notificationModels =  AddToModelsListForSendNotification(planSupportResult.AccountDatabasesForPerformingSupport);

            SendNotifications(notificationModels, supportOperation);
        }

        /// <summary>
        /// Получить список моделей для отправки уведомлений клиентам
        /// </summary>
        /// <returns></returns>
        private static List<NotificationAboutPlanningSupportOfAcDbsDto> AddToModelsListForSendNotification(
            IEnumerable<BeforeSupportAccountDatabaseModelDto.DbItem> acDbsForPerformingSupport)
        {
            var notificationModels = new List<NotificationAboutPlanningSupportOfAcDbsDto>();

            var groupedAcDbsForPerformingSupport = acDbsForPerformingSupport.GroupBy(acDb => acDb.AccountId).ToList();

            groupedAcDbsForPerformingSupport.ForEach(accountDatabases =>
            {
                notificationModels.Add(new NotificationAboutPlanningSupportOfAcDbsDto
                {
                    AccountId = accountDatabases.Key,
                    AccountDatabasesForPerformingSupport = accountDatabases.ToList()
                });
            });
            return notificationModels;
        }

        /// <summary>
        /// Отправить уведомления
        /// </summary>
        /// <param name="notificationModels">Список моделей для уведомления</param>
        /// <param name="supportOperation">тип поддержки</param>
        private void SendNotifications(List<NotificationAboutPlanningSupportOfAcDbsDto> notificationModels, DatabaseSupportOperation supportOperation)
        {
            if (!notificationModels.Any())
            {
                logger.Warn("Нет ни одной операции для уведомления");
                return;
            }
            notificationModels.ForEach(notificationModel =>
            {
                Notify(notificationModel.AccountId, notificationModel.AccountDatabasesForPerformingSupport, supportOperation);
            });
        }

        /// <summary>
        /// Уведомить владельцев информационных баз о планируемых работах.
        /// </summary>
        /// <param name="accountId">ID аккаунта для отправки уведомления.</param>
        /// <param name="databasesForSupport">Список баз на ТиИ и АО.</param>
        /// <param name="supportOperation">тип поддержки</param>
        private void Notify(Guid accountId, ICollection<BeforeSupportAccountDatabaseModelDto.DbItem> databasesForSupport, DatabaseSupportOperation supportOperation)
        {
            if (!databasesForSupport.Any())
                return;

            letterNotificationProcessor.TryNotify<BeforeSupportDatabaseLetterNotification, BeforeSupportDatabaseLetterModelDto>(
                new BeforeSupportDatabaseLetterModelDto
                {
                    AccountId = accountId,
                    AutoUpdateDatabases = GenerateListAutoUpdateDatabasesForNotification(databasesForSupport),
                    TehSupportDatabaseNames = GenerateListTehSupportDatabaseNamesForNotification(databasesForSupport),
                    SupportOperation = supportOperation
                });
            
        }

        /// <summary>
        /// Сформировать список баз, в которых будет проведено AO,
        /// для уведомления
        /// </summary>
        /// <param name="databasesForSupport">Список баз на ТиИ и АО</param>
        /// <returns>Список баз, в которых будет проведено AO</returns>
        private IEnumerable<DatabaseSupportInfoDto> GenerateListAutoUpdateDatabasesForNotification(
            ICollection<BeforeSupportAccountDatabaseModelDto.DbItem> databasesForSupport) => databasesForSupport
            .Where(database => database.Operation == DatabaseSupportOperation.PlanAutoUpdate)
            .Select(database => new DatabaseSupportInfoDto
            {
                Caption = database.Caption,
                CurrentVersion = database.CurrentVersion,
                UpdateVersion = database.UpdateVersion,
                TimeOfOperation = database.TimeOfOperation
            });

        /// <summary>
        /// Сформировать список названий баз, в которых будет проведено ТиИ,
        /// для уведомления
        /// </summary>
        /// <param name="databasesForSupport">Список баз на ТиИ и АО</param>
        /// <returns>Список названий баз, в которых будет проведено ТиИ</returns>
        private IEnumerable<string> GenerateListTehSupportDatabaseNamesForNotification(
            ICollection<BeforeSupportAccountDatabaseModelDto.DbItem> databasesForSupport) => databasesForSupport
            .Where(database => database.Operation == DatabaseSupportOperation.PlanTehSupport)
            .Select(database => database.Caption);

        /// <summary>
        /// Обработать инф. базы с ошибкой
        /// </summary>
        /// <param name="databasesWithError">Список инф. баз с ошибкой</param>
        /// <param name="supportOperation">Тип операции</param>
        private void HandleDatabasesWithError(List<KeyValuePair<Guid, string>> databasesWithError,
            DatabaseSupportOperation supportOperation)
        {
            if (!databasesWithError.Any())
                return;
             
            var errorSubject = supportOperation == DatabaseSupportOperation.PlanAutoUpdate
                ? "[Ошибки при планировании проведения АО информационных баз]"
                : "[Ошибки при планировании проведения ТиИ информационных баз]";
            
            var errorMessage = "Список баз с ошибками: ";
            databasesWithError.ForEach(acDbWithError =>
            {
                errorMessage += $" -- [{acDbWithError.Key}] {acDbWithError.Value} \r\n";
            });
            
            var commonException = new InvalidOperationException(errorMessage);
            handlerException.Handle(commonException, errorSubject);
           
        }
    }
}
