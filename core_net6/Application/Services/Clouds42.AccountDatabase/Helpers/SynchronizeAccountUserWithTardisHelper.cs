﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер синхронизации пользователя с Тардис
    /// </summary>
    public class SynchronizeAccountUserWithTardisHelper(
        IUnitOfWork dbLayer,
        ITardisSynchronizerHelper tardisSynchronizerHelper,
        IHandlerException handlerException)
        : BaseTardisSynchronizeHelper(dbLayer)
    {

        /// <summary>
        /// Синхронизировать обновление пользователя в новом потоке
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        public void SynchronizeAccountUserUpdateViaNewThread(Guid accountUserId)
        {
            Execute(accountUserId);
        }

        public void Execute(Guid accountUserId)
        {
            if (!NeedSynchronizeAccountUser(accountUserId))
                return;

            RunSynchronizeTask<UpdateAccountUserTrigger>(new AccountUserIdDto
            {
                AccountUserId = accountUserId
            });
        }

        /// <summary>
        /// Запустить задачу синхронизации
        /// </summary>
        /// <typeparam name="TTrigger">Тип триггера</typeparam>
        /// <param name="accountUserIdDto">Модель ID пользователя</param>
        private void RunSynchronizeTask<TTrigger>(AccountUserIdDto accountUserIdDto)
            where TTrigger : SynchTriggerBase<AccountUserIdDto>
        {
            try
            {
                tardisSynchronizerHelper.SynchronizeAccountUser<TTrigger, AccountUserIdDto>(accountUserIdDto);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка уведомления Тардис о редактирование пользователя.]");
            }
        }
    }
}
