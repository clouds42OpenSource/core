﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер для уведомления пользователей об изменениях доступа к инф. базам
    /// </summary>
    public class AcDbAccessNotificationHelper(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        private readonly Lazy<string> _routeForOpenAccountDatabasesPage = new(CloudConfigurationProvider.Cp.GetRouteForOpenAccountDatabasesPage);

        /// <summary>
        /// Уведомить о предоставлении доступа
        /// </summary>
        /// <param name="accessEntity">Доступ к инф. базе</param>
        public void NotifyAbountGrantAccess(AcDbAccess accessEntity)
        {
            var mailTo = string.Empty;
            try
            {
                if (!accessEntity.AccountUserID.HasValue)
                    return;

                if (!TryGetAccount(accessEntity.AccountID, out var accountFor))
                    return;

                if (!TryGetAccountDatabase(accessEntity.AccountDatabaseID, out var accountDatabase))
                    return;

                if (!TryGetAccount(accountDatabase.AccountId, out var accountFrom))
                    return;

                if (accountFor.Id == accountFrom.Id)
                    return;

                var userFor = dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == accessEntity.AccountUserID);
                mailTo = userFor.Email;
                TryNotifyAndWriteLogEvent(accountDatabase, userFor, accountFrom.AccountCaption);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Error sending notification]: toEmail: {mailTo}");
            }
        }

        /// <summary>
        /// Попытаться получить аккаунт
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Результат попытки</returns>
        private bool TryGetAccount(Guid accountId, out Account account)
        {
            account = dbLayer.AccountsRepository.FirstOrDefault(ac => ac.Id == accountId);
            if (account != null) 
                return true;

            logger.Warn($"Аккаунт по ID {accountId} не найден!");
            return false;
        }

        /// <summary>
        /// Попытаться получить инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Результат попытки</returns>
        private bool TryGetAccountDatabase(Guid accountDatabaseId, out IAccountDatabase accountDatabase)
        {
            accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == accountDatabaseId);
            if (accountDatabase != null)
                return true;

            logger.Warn($"Инф. база по ID {accountDatabaseId} не найдена!");
            return false;
        }

        /// <summary>
        /// Попытаться отправить письмо и записать лог ивент
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="userFor">Пользователь для которого отправляется уведомление</param>
        /// <param name="accountCaption">Название аккаунта</param>
        private void TryNotifyAndWriteLogEvent(IAccountDatabase accountDatabase, IAccountUser userFor, string accountCaption)
        {
            var cpSiteUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(userFor.AccountId);
            letterNotificationProcessor
                .TryNotify<ProvidingAccessToDatabaseLetterNotification, ProvidingAccessToDatabaseLetterModelDto>(
                    new ProvidingAccessToDatabaseLetterModelDto
                    {
                        AccountCaption = accountCaption,
                        DatabaseCaption = accountDatabase.Caption,
                        AccountUserId = userFor.Id,
                        AccountId = userFor.AccountId,
                        AccountDatabaseId = accountDatabase.Id,
                        UrlForAccountDatabasesPage = new Uri($"{cpSiteUrl}/{_routeForOpenAccountDatabasesPage.Value}").AbsoluteUri
                    });

            LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.MailNotification,
                $"Письмо \"Вам предоставили доступ к базе\" отправлено на адрес {userFor.Email}");
        }
    }
}
