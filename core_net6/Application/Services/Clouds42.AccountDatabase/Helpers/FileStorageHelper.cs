﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер для работы с файловым хранилищем
    /// </summary>
    public class FileStorageHelper(
        AccountDatabasePathHelper accountDatabasePathHelper,
        ILogger42 logger)
        : IFileStorageHelper
    {
        /// <summary>
        /// Метод получения последней даты активности
        /// </summary>
        /// <param name="db">Объект AccountDatabase</param>
        /// <returns>Дата последней активности или ошибка</returns>
        public DateTime GetLastActiveDateFileBase(Domain.DataModels.AccountDatabase db)
        {
            logger.Info($"Получение даты последнего доступа к файлам базы {db.V82Name}");
            var filepath = accountDatabasePathHelper.GetPath(db);
            var directoryInfo = new DirectoryInfo(filepath);

            if (!directoryInfo.Exists)
                throw new DirectoryNotFoundException($"Папка базы {db.V82Name} не найдена.");

            var files = directoryInfo.GetFiles(@"*.*", SearchOption.AllDirectories);

            if (!files.Any())
                throw new FileNotFoundException($"Папка базы {db.V82Name} пустая.");

            var lastActive = files.Max(f => f.LastAccessTime);
            return lastActive;
        }
    }
}
