﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.Access;
using CommonLib.Enums;

namespace Clouds42.AccountDatabase.Helpers
{
    public class CheckRental1CActivateHelper(
        IAccessProvider accessProvider,
        IServiceStatusHelper serviceStatusHelper)
    {
        /// <summary>
        /// Проверить активность аренды 1С
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public CreateCloud42ServiceModelDto CheckRental1CActivate(Guid accountId)
        {
            var currentUser = accessProvider.GetUser();
            if (currentUser != null && (currentUser.Groups.Contains(AccountUserGroup.CloudAdmin) || currentUser.Groups.Contains(AccountUserGroup.Hotline)))
                return new CreateCloud42ServiceModelDto { Complete = true };

            var createCloud42ServiceModel = new CreateCloud42ServiceModelDto { Complete = true };

            var serviceStatusModel = serviceStatusHelper.CreateModelForRentAndMyDisk(accountId);
            createCloud42ServiceModel.Complete = !serviceStatusModel.ServiceIsLocked;
            createCloud42ServiceModel.Comment = serviceStatusModel.ServiceLockReason;
            createCloud42ServiceModel.ResourceForRedirect = serviceStatusModel.ServiceIsLocked ? ResourceType.MyEntUser : null;

            return createCloud42ServiceModel;
        }
    }
}
