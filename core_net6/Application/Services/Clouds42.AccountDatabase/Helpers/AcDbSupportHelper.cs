﻿using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хелпер для работы с поддержкой (АО и ТиИ) информационных баз
    /// </summary>
    public class AcDbSupportHelper(AccountDatabaseHelper accountDatabaseHelper, ICloudLocalizer cloudLocalizer)
    {
        /// <summary>
        /// Получить сообщение о не успешной авторизации в информационной базе
        /// </summary>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        /// <returns>Сообщение о не успешной авторизации в информационной базе</returns>
        public string GetInvalidAuthorizationMessage(Guid accountDatabaseId)
        {
            var accountId = accountDatabaseHelper.GetAccountByDatabaseId(accountDatabaseId).Id;

            return "Не удалось авторизоваться в информационной базе\n" +
                   "Убедитесь, что:\n" +
                   "1. Введены корректные регистрационные данные\n" +
                   $"2. {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.UserIsAnAdministratorInThe1CDatabase,accountId)}\n";
        }
    }
}
