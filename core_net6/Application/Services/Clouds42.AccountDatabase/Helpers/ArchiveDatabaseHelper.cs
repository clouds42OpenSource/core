﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер для получения списка баз для архивации
    /// </summary>
    public class ArchiveDatabaseHelper(IUnitOfWork dbLayer, ILogger42 logger)
    {
        private readonly Lazy<int> _archiveDays = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired.GetArchiveDays);
        private readonly Lazy<int> _availableCountOfDbOnDelimitersForArchive = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired.GetAvailableCountOfDbOnDelimitersForArchive);

        /// <summary>
        /// Получить список баз которые надо заархивировать
        /// </summary>
        public List<ArchiveDatabaseModelDto> ArchiveDatabaseList()
        {
            var clearingDate = DateTime.Now.AddDays(-_archiveDays.Value);

            var resourcesConfigurations = GetExpiredResourcesConfigurations(clearingDate);
            logger.Debug("Список просроченных ресурсов для блокировки получен успешно.");
            
            var accountDatabasesOfRealAccounts = GetAccountDatabasesOfRealAccounts(resourcesConfigurations);
            logger.Debug($"Список баз аккаунтов для архивации получен. Количество: {accountDatabasesOfRealAccounts.Count()}");

            var sourceDatabasesList = GetSourceDatabasesList();
            logger.Debug($"Список материнских баз получен успешно. Количество: {sourceDatabasesList.Count}");

            var databasesForArchive = accountDatabasesOfRealAccounts.Distinct().ToList();
            logger.Debug($"Общее количество баз для архивации {databasesForArchive.Count}");

            var accountDatabasesForProcessing = databasesForArchive
                .Where(p => !sourceDatabasesList.Any(p2 => p2.DataBaseId == p.DataBaseId)).ToList();

            logger.Debug($"Количество баз без учета материнских {accountDatabasesForProcessing.Count}");

            return GetListOfDbForProcessing(accountDatabasesForProcessing);
        }

        /// <summary>
        /// Получить список просроченных ресурс конфигураций
        /// </summary>
        /// <param name="clearingDate">Период, после которого сервис считается просроченным</param>
        /// <returns>Список просроченных ресурс конфигураций</returns>
        private IQueryable<ResourcesConfiguration> GetExpiredResourcesConfigurations(DateTime clearingDate)
            => dbLayer.ResourceConfigurationRepository.WhereLazy(
                r =>
                    r.BillingService.SystemService == Clouds42Service.MyEnterprise && r.ExpireDate != null &&
                    r.ExpireDate < clearingDate);

        /// <summary>
        /// Получить список баз реальных аккаунтов которые надо заархивировать
        /// </summary>
        /// <param name="resourcesConfigurations">Список ресурс конфигураций</param>
        /// <returns>Список баз реальных аккаунтов которые надо заархивировать</returns>
        private IQueryable<ArchiveDatabaseModelDto> GetAccountDatabasesOfRealAccounts(
            IQueryable<ResourcesConfiguration> resourcesConfigurations)
            =>
                from resConf in resourcesConfigurations
                join account in dbLayer.AccountsRepository.WhereLazy()
                    on resConf.AccountId equals account.Id
                join db in dbLayer.DatabasesRepository.WhereLazy(d => d.State == "Ready") on account.Id equals
                    db.AccountId
                join payment in dbLayer.PaymentRepository.WhereLazy() on account.Id equals payment.AccountId 
                select new ArchiveDatabaseModelDto
                {
                    DataBaseId = db.Id, 
                    V82Name = db.V82Name, 
                    AccountId = account.Id, 
                    IsDemo = false,
                    IsDelimiter = db.AccountDatabaseOnDelimiter != null
                };

        /// <summary>
        /// Получить список материнских баз на раздлелителях
        /// </summary>
        /// <returns>Список материнских баз на раздлелителях</returns>
        private List<ArchiveDatabaseModelDto> GetSourceDatabasesList()
            =>
                (from serviceAccount in dbLayer.ServiceAccountRepository.All() 
                join acDb in dbLayer.DatabasesRepository.WhereLazy(d => d.State == "Ready") on serviceAccount.Id equals acDb.AccountId
                select new ArchiveDatabaseModelDto { DataBaseId = acDb.Id, V82Name = acDb.V82Name, IsDemo = true }).ToList();

        /// <summary>
        /// Получить список баз для обработки
        /// </summary>
        /// <param name="accountDatabasesForProcessing">Исходный список баз для обработки</param>
        /// <returns>Отсортированный список баз</returns>
        private List<ArchiveDatabaseModelDto> GetListOfDbForProcessing(List<ArchiveDatabaseModelDto> accountDatabasesForProcessing)
        {
            var dbOnDelimiters = accountDatabasesForProcessing.Where(acDb => acDb.IsDelimiter);
            accountDatabasesForProcessing = accountDatabasesForProcessing
                .Where(acDb => dbOnDelimiters.All(db => db.DataBaseId != acDb.DataBaseId)).ToList();

            var dbOnDelimitersForProcessing = dbOnDelimiters.Take(_availableCountOfDbOnDelimitersForArchive.Value);
            accountDatabasesForProcessing.AddRange(dbOnDelimitersForProcessing);

            return accountDatabasesForProcessing.ToList();
        }
    }
}
