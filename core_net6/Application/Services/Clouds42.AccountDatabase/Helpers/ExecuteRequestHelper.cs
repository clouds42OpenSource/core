﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер для выполнения запросов
    /// </summary>
    public class ExecuteRequestHelper(
        IAccessToDbOnDelimiterCommand accessToDbOnDelimiterCommand,
        IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Выполнить запрос для управления доступом
        /// </summary>
        /// <param name="acDbAccess">Доступ</param>
        /// <param name="url">Адрес для отправки запроса</param>
        /// <param name="isInstall">Операция установки доступов</param>
        /// <returns>Результат выполнения</returns>
        public ManageAcDbAccessResultDto ExecuteRequestForManageAcDbAccess(AcDbAccess acDbAccess, string url, bool isInstall = true)
        {
            var accountDatabase = GetAccountDatabase(acDbAccess.AccountDatabaseID);

            if (acDbAccess.AccountUserID == null)
                throw new InvalidOperationException($"Для доступа {acDbAccess.ID} не указан ID пользователя");

            return accessToDbOnDelimiterCommand.Execute(accountDatabase, acDbAccess.AccountUserID.Value, url, "", isInstall);
        }

        /// <summary>
        /// Получить инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Инф. база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId)
            => dbLayer.DatabasesRepository.GetAccountDatabase(accountDatabaseId)
               ?? throw new NotFoundException($"Инф. база по ID {accountDatabaseId} не найдена");
    }
}
