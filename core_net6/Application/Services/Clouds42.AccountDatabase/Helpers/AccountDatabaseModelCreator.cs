﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Класс создания модели данных информационной базы и тех поддерки.
    /// </summary>
    public class AccountDatabaseModelCreator(
        IUnitOfWork dbLayer,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IAccountDatabaseModelCreator
    {
        private readonly CloudServicesEnterpriseServerProvider _cloudServicesEnterpriseServerProvider = new(dbLayer);

        /// <summary>
        /// Создать модель.
        /// </summary>        
        public AccountDatabaseEnterpriseModelDto CreateModel(Guid accountDatabaseId)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);
            return CreateModel(accountDatabase);
        }

        /// <summary>
        /// Создать модель.
        /// </summary>        
        public AccountDatabaseEnterpriseModelDto CreateModel(string v82Name)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(v82Name);
            return CreateModel(accountDatabase);
        }

        /// <summary>
        /// Создать модель серверной инф. базы.
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Модель серверной инф. базы</returns>
        public AccountDatabaseEnterpriseModelDto CreateModel(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var segment = accountConfigurationDataProvider.GetAccountSegment(accountDatabase.AccountId);

            return new AccountDatabaseEnterpriseModelDto
            {
                AccountDatabase = accountDatabase,
                EnterpriseServer = _cloudServicesEnterpriseServerProvider.GetEnterpriseServerData(accountDatabase),
                SqlServer = segment.CloudServicesSQLServer,
                Segment = segment,
                PlatformPathX64 = GetPathToPlatformX64(accountDatabase, segment)
            };
        }

        /// <summary>
        /// Получить путь к платформе х64
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="accountDatabaseSegment">Сегмент по инф. базе</param>
        /// <returns>Путь к платформе х64</returns>
        private static string GetPathToPlatformX64(Domain.DataModels.AccountDatabase accountDatabase,
            CloudServicesSegment accountDatabaseSegment)
        {
            if (accountDatabase.PlatformType == PlatformType.V82)
            {
                if (string.IsNullOrEmpty(accountDatabaseSegment.Stable82PlatformVersionReference.PathToPlatfromX64))
                    throw new InvalidOperationException(
                        $"У сегмента '{accountDatabaseSegment.Name}' :: '{accountDatabaseSegment.Description}' не задан путь до 1С 8.2 х64");

                return accountDatabaseSegment.Stable82PlatformVersionReference.PathToPlatfromX64;
            }

            if (accountDatabase.PlatformType != PlatformType.V83)
                throw new InvalidOperationException(
                    $"Для типа платформы {accountDatabase.PlatformType} не определено откуда брать путь до 1С.");

            if (accountDatabase.DistributionType == DistributionType.Alpha.ToString())
            {
                if (string.IsNullOrEmpty(accountDatabaseSegment.Alpha83PlatformVersionReference.PathToPlatfromX64))
                    throw new InvalidOperationException(
                        $"У сегмента '{accountDatabaseSegment.Name}':: '{accountDatabaseSegment.Description}' не задан путь до альфа 1С 8.3 х64");

                return accountDatabaseSegment.Alpha83PlatformVersionReference.PathToPlatfromX64;
            }

            if (string.IsNullOrEmpty(accountDatabaseSegment.Stable83PlatformVersionReference.PathToPlatfromX64))
                throw new InvalidOperationException(
                    $"У сегмента '{accountDatabaseSegment.Name}':: '{accountDatabaseSegment.Description}' не задан путь до стабильной 1С 8.3 х64");

            return accountDatabaseSegment.Stable83PlatformVersionReference.PathToPlatfromX64;
        }
    }
}
