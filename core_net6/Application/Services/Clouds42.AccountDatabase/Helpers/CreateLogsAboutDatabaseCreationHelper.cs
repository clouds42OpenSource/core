﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хелпер для создания логов
    /// о создании инф. баз
    /// </summary>
    public class CreateLogsAboutDatabaseCreationHelper(IUnitOfWork dbLayer, IAccessProvider accessProvider)
    {
        /// <summary>
        /// Создать логи
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="result">Модель результата создания инф. баз</param>
        /// <param name="isLoadedFromFile">Загружена из файла</param>
        public void Create(Guid accountId, CreateCloud42ServiceModelDto result, bool isLoadedFromFile = false)
        {
            var databasesData = GetShortDatabasesData(result.Ids);
            foreach (var data in databasesData)
            {
                result.CreatedDatabases.TryGetValue(data.Id, out var isDemo);
                var operationDescription = GetAddingDatabaseDescription(data.AccountDatabaseName,
                    data.AccountDatabaseNumber, result, data.LoadedFromFile || isLoadedFromFile, isDemo);

                WriteLogEventAboutAddingDatabase(accountId, operationDescription);
            }
        }


        /// <summary>
        /// Получить краткую информацию по информационным базам
        /// </summary>
        /// <param name="databasesId">Список Id баз</param>
        /// <returns>Краткая информация по информационным базам</returns>
        private List<(Guid Id,string AccountDatabaseName, string AccountDatabaseNumber, bool LoadedFromFile)> GetShortDatabasesData(List<Guid> databasesId)
            =>
            (
                from accountDatabase in dbLayer.DatabasesRepository.WhereLazy()
                join databaseOnDelimiter in dbLayer.AccountDatabaseDelimitersRepository.WhereLazy() on
                    accountDatabase.Id equals databaseOnDelimiter.AccountDatabaseId
                    into databaseOnDelimiters
                from databaseOnDelimiter in databaseOnDelimiters.Take(1).DefaultIfEmpty()
                where databasesId.Contains(accountDatabase.Id)
                select new
                {
                    accountDatabase.Id,
                    AccountDatabaseName = accountDatabase.Caption,
                    AccountDatabaseNumber = accountDatabase.V82Name,
                    databaseOnDelimiter.UploadedFileId
                }
            )
            .AsEnumerable()
            .Select(x => (x.Id, x.AccountDatabaseName, x.AccountDatabaseNumber, x.UploadedFileId.HasValue))
            .ToList();


        /// <summary>
        /// Записать в логи результат добавления инф. базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="operationDescription">Описание операции</param>
        private void WriteLogEventAboutAddingDatabase(Guid accountId, string operationDescription)
        {
            LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.AddInfoBase,
                operationDescription);
        }


        /// <summary>
        /// Возвращение описания операции добавления инф. баз
        /// </summary>
        /// <param name="databaseCaption">Название инф.базы</param>
        /// <param name="databaseV82Name">Номер инф.базы</param>
        /// <param name="result">Модель результата создания инф. баз</param>
        /// <param name="isLoadedFromFile">Признак что инф. база загружена из файла</param>
        /// <param name="isDemo">Демо база или нет</param>
        /// <returns>Описание операции добавления инф. базы</returns>
        private string GetAddingDatabaseDescription(string databaseCaption, string databaseV82Name, CreateCloud42ServiceModelDto result,
            bool isLoadedFromFile, bool isDemo)
        {
            var databaseFullName =
                AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(databaseCaption, databaseV82Name);

            if (!result.Complete)
                return $"Ошибка создания базы {databaseFullName}. Описание ошибки: {result.Comment}";

            var operationDescription = isLoadedFromFile
                ? "Загружена"
                : "Создана новая";

            return $"{operationDescription} {(isDemo ?  "демо" : "")} база {databaseFullName}";
        }

    }

}
