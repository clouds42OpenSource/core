﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Общий валидатор информационных баз на создание
    /// </summary>
    public class DatabaseToCreateGeneralValidator(IServiceProvider serviceProvider)
    {
        private readonly IUnitOfWork _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();

        /// <summary>
        /// Выполнить валидацию
        /// </summary>
        /// <param name="databases">Информационные базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns></returns>
        public CreateCloud42ServiceModelDto Validate(List<InfoDatabaseDomainModelDto> databases, Guid accountId)
        {
            databases = databases.Where(w => w.IsChecked).ToList();
            var dbsOnDelimiters = databases.Where(w => w is { DbTemplateDelimiters: true, DemoData: true }).ToList(); 
            var databasesOnDelimitersWithDemoData = serviceProvider.GetRequiredService<AccountDatabaseHelper>().GetListTemplatesWhichHaveDatabaseWithDemoData(dbsOnDelimiters, accountId);
            databases = databases.Where(w => databasesOnDelimitersWithDemoData.All(r => r.TemplateId != w.TemplateId)).ToList();

            var validateResult = CheckAbilityToCreateDatabasesByLimit(accountId, databases.Count);
            return validateResult;
        }

        /// <summary>
        /// Проверить по количеству доступность создать базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="dbCount">Количество баз на создание</param>
        /// <returns></returns>
        private CreateCloud42ServiceModelDto CheckAbilityToCreateDatabasesByLimit(Guid accountId, int dbCount)
        {
            var account = _dbLayer.AccountsRepository.FirstOrDefault(w => w.Id == accountId);
            var checkAbilityToCreateDatabasesByLimit = serviceProvider.GetRequiredService<ICheckAccountDataProvider>()
                .CheckAbilityToCreateDatabasesByLimit(account, dbCount);

            return checkAbilityToCreateDatabasesByLimit;
        }
    }
}
