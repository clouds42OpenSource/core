﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хелпер для работы с информационными базами
    /// </summary>
    public class AccountDatabaseHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить список шаблонов по которым уже есть базы с демо данными
        /// </summary>
        /// <param name="databases">Информационные базы</param>
        /// <param name="accountId">Id аккаунта текущего пользователя</param>
        /// <returns></returns>
        public List<InfoDatabaseDomainModelDto> GetListTemplatesWhichHaveDatabaseWithDemoData(List<InfoDatabaseDomainModelDto> databases, Guid accountId)
        {
            var databasesOnDelimitersWithDemoData = new List<InfoDatabaseDomainModelDto>();
            foreach (var database in databases)
            {
                var dbOnDelimitersWithDemoData = FindDatabaseDelimiterDemo(accountId, database.TemplateId);

                if (dbOnDelimitersWithDemoData != null)
                    databasesOnDelimitersWithDemoData.Add(database);
            }
            return databasesOnDelimitersWithDemoData;
        }

        /// <summary>
        /// Найти демо базу на разделителях.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        /// <param name="templateId">Идентификатор шаблона.</param>
        /// <returns>Найденая запись демо базы на разделителях.</returns>
        public AccountDatabaseOnDelimiters FindDatabaseDelimiterDemo(Guid accountId, Guid templateId)
        {
            var dbOnDelimitersWithDemoData = dbLayer.AccountDatabaseDelimitersRepository
                .All().
                Include(w => w.AccountDatabase)
                .FirstOrDefault(del =>
                    del.IsDemo && del.AccountDatabase.TemplateId == templateId &&
                    del.AccountDatabase.AccountId == accountId &&
                    del.AccountDatabase.State == DatabaseState.Ready.ToString());

            return dbOnDelimitersWithDemoData;
        }

        /// <summary>
        /// Получить инф. базу по ID бэкапа
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа</param>
        /// <returns>Инф. база</returns>
        public Domain.DataModels.AccountDatabase GetAccountDatabaseByBackupId(Guid accountDatabaseBackupId)
        {
            var accountDatabaseBackup =
                dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(w => w.Id == accountDatabaseBackupId)
                ?? throw new NotFoundException($"Бэкап инф. базы по ID {accountDatabaseBackupId} не найден");

            return accountDatabaseBackup.AccountDatabase;
        }

        /// <summary>
        /// Получить аккаунт по Id информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <returns>Модель аккаунта</returns>
        public Account GetAccountByDatabaseId(Guid databaseId) =>
            (from database in dbLayer.DatabasesRepository.WhereLazy(db => db.Id == databaseId)
                join account in dbLayer.AccountsRepository.WhereLazy() on database.AccountId equals account.Id
                select account
            ).FirstOrDefault() ??
            throw new NotFoundException($"Не удалось найти аккаунт по Id базы {databaseId}");

        /// <summary>
        /// Map arguments to AccountDatabasesFilterParamsDto model
        /// </summary>
        /// <returns></returns>
        public AccountDatabasesFilterParamsDto MapToAccountDatabasesFilterParams(
            Guid accountId, Guid accountUserId, AccountDatabaseAffiliationTypeEnum affiliation, string field, int page, 
            string search, int size, SortType sort, AccountDatabaseTypeEnum type, bool isNewPage)
        {
            var accountDatabasesFilter = new AccountDatabasesFilterDto
            {
                AccountId = accountId,
                AccountDatabaseAffiliationType = affiliation,
                AccountUserId = accountUserId,
                SearchString = search,
                IsServerDatabaseOnly = type == AccountDatabaseTypeEnum.ServerDatabase,
                IsArchievedDatabaseOnly = type == AccountDatabaseTypeEnum.ArchievedDatabase,
                AccountDatabaseTypeEnum = type,
                PageSize = size,
                isNewPage = isNewPage
            };
            var sortingData = new SortingDataDto
            {
                SortKind = sort,
                FieldName = field
            };
            return new AccountDatabasesFilterParamsDto
            {
                Filter = accountDatabasesFilter,
                PageNumber = page,
                SortingData = sortingData
            };
        }
    }
}
