﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер для генерации названия компании
    /// </summary>
    public static class GenerateCompanyNameHelper
    {
        /// <summary>
        /// Генерирует имя папки, в которой хранятся папки с файлами ИБ 
        /// </summary>
        /// <param name="accountDatabase"></param>
        /// <returns></returns>
        public static string GenerateCompanyName(Domain.DataModels.AccountDatabase accountDatabase)
        {
            return $"company_{accountDatabase.Account.IndexNumber}"; 
        }

        /// <summary>
        /// Получить название компании
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Название компании</returns>
        public static string GetCompanyName(Account account)
        {
            return $"company_{account.IndexNumber}";
        }
    }
}
