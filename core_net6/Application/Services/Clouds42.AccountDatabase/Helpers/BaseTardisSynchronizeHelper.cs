﻿using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Базовый хэлпер синхронизации с Тардис
    /// </summary>
    public abstract class BaseTardisSynchronizeHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Признак необходимости синхронизировать инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>true - если есть лицензии сервиса Тардис</returns>
        protected bool NeedSynchronizeAccountDatabase(Guid accountDatabaseId)
        {
            var accountDatabase = GetAccountDatabaseOrThrowException(accountDatabaseId);

            return  dbLayer.ResourceRepository.Any(resource =>
                    resource.AccountId == accountDatabase.AccountId
                    && resource.BillingServiceType.Service.InternalCloudService == InternalCloudServiceEnum.Tardis
                    && resource.Subject != null);
        }

        /// <summary>
        /// Признак необходимости синхронизировать пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>true - если есть лицензии сервиса Тардис</returns>
        protected bool NeedSynchronizeAccountUser(Guid accountUserId)
        {
            return dbLayer.ResourceRepository.Any(resource =>
                resource.Subject == accountUserId && resource.BillingServiceType.Service.InternalCloudService ==
                InternalCloudServiceEnum.Tardis);
        }

        /// <summary>
        /// Получить инф. базу или выбросить исключение
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Инф. база</returns>
        protected Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid accountDatabaseId)
            => dbLayer.DatabasesRepository.GetAccountDatabase(accountDatabaseId)
               ?? throw new NotFoundException($"Инф. база по ID {accountDatabaseId} не найдена");
    }
}
