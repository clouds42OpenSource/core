﻿using Clouds42.AccountDatabase.DataHelpers;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хелпер валидации изменений в базе
    /// </summary>
    public static class ValidateAccountDatabaseChangingHelper
    {
        /// <summary>
        /// Провалидировать базу на возможность мирации в другое хранилище
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="fileStorageId">Id фалового хранилища</param>
        /// <param name="errorMessage">Сообщени об ошибке</param>
        /// <returns>Результат валидации</returns>
        public static bool TryValidateChangeAccountDatabaseFileStorage(this Domain.DataModels.AccountDatabase accountDatabase, Guid fileStorageId, out string errorMessage)
        {
            errorMessage = null;

            if (accountDatabase == null)
            {
                errorMessage = "Информационная база не найдена!";
                return false;
            }

            if (accountDatabase.FileStorageID == fileStorageId)
            {
                errorMessage = $"Информационная база {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} уже находится на выбранном хранилище!";
                return false;
            }

            if (accountDatabase.IsFile == false)
            {
                errorMessage =
                    $"Информационная база {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} серверная, процесс смены хранилища невозможен!";

                return false;
            }

            return true;
        }
    }
}
