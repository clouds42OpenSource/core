﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using System.Text;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    public class RdpHelper(string terminalServer, string gateway, ILogger42 logger, IHandlerException handlerException)
    {
        public string BuildRemoteAppLinkString(string userLogin, string dbid = default, string lMode = default)
        {
            var param = dbid == default ? "\r\n" : $" /L {dbid.ToString()} {lMode}\r\n";
            var res = new StringBuilder();
            res.AppendLine("redirectclipboard:i:1");
            res.AppendLine("redirectposdevices:i:0");
            res.AppendLine("redirectprinters:i:1");
            res.AppendLine("redirectcomports:i:1");
            res.AppendLine("redirectsmartcards:i:1");
            res.AppendLine("devicestoredirect:s:*");
            res.AppendLine("drivestoredirect:s:*");
            res.AppendLine("redirectdrives:i:1");
            res.AppendLine("session bpp:i:32");
            res.AppendLine("prompt for credentials:i:0");
            res.AppendLine("span monitors:i:1");
            res.AppendLine("use multimon:i:1");
            res.AppendLine("remoteapplicationmode:i:1");
            res.AppendLine("server port:i:3389");
            res.AppendLine("allow font smoothing:i:1");
            res.AppendLine("promptcredentialonce:i:1");
            res.AppendLine("authentication level:i:0");
            if (!string.IsNullOrEmpty(gateway))
            {
                res.AppendLine("gatewayusagemethod:i:1");
                res.AppendLine("gatewaycredentialssource:i:0");
                res.AppendLine("gatewayprofileusagemethod:i:1");
            }
            else
            {
                res.AppendLine("gatewayusagemethod:i:0");
                res.AppendLine("gatewaycredentialssource:i:0");
                res.AppendLine("gatewayprofileusagemethod:i:0");
            }
            res.AppendLine($"full address:s:{terminalServer}");
            res.AppendLine("alternate shell:s:||Link42");
            res.AppendLine("remoteapplicationprogram:s:||Link42");
            if (!string.IsNullOrEmpty(gateway))
            {
                res.AppendLine($"gatewayhostname:s:{gateway}");
            }
            res.AppendLine("remoteapplicationname:s:Link42.exe");
            res.AppendFormat("remoteapplicationcmdline:s:{0}", param);
            res.AppendLine($"full address:s:{terminalServer}");
            res.AppendLine("signscope:s:Full Address,Alternate Full Address,Server Port,GatewayHostname,GatewayUsageMethod,GatewayProfileUsageMethod,GatewayCredentialsSource,PromptCredentialOnce,Alternate Shell,RemoteApplicationProgram,RemoteApplicationMode,RemoteApplicationName,RemoteApplicationCmdLine,Authentication Level,RedirectDrives,RedirectPrinters,RedirectCOMPorts,RedirectSmartCards,RedirectPOSDevices,RedirectClipboard,DevicesToRedirect,DrivesToRedirect");
            res.AppendFormat(@"username:s: AD\{0}", userLogin);
            return res.ToString();
        }

        public string BuildRemoteAppLinkStringDev(string userLogin, string dbid = default)
        {
            var param = dbid == default ? "\r\n" : $" /L {dbid.ToString()} \r\n";
            var res = new StringBuilder();
            res.AppendLine("redirectclipboard:i:1");
            res.AppendLine("redirectposdevices:i:0");
            res.AppendLine("redirectprinters:i:1");
            res.AppendLine("redirectcomports:i:1");
            res.AppendLine("redirectsmartcards:i:1");
            res.AppendLine("devicestoredirect:s:*");
            res.AppendLine("drivestoredirect:s:*");
            res.AppendLine("redirectdrives:i:1");
            res.AppendLine("session bpp:i:32");
            res.AppendLine("prompt for credentials:i:0");
            res.AppendLine("span monitors:i:1");
            res.AppendLine("use multimon:i:1");
            res.AppendLine("remoteapplicationmode:i:1");
            res.AppendLine("server port:i:3389");
            res.AppendLine("allow font smoothing:i:1");
            res.AppendLine("promptcredentialonce:i:1");
            res.AppendLine("authentication level:i:0");
            res.AppendLine("gatewayusagemethod:i:2");
            res.AppendLine("gatewayprofileusagemethod:i:1");
            res.AppendLine("gatewaycredentialssource:i:0");
            res.AppendLine($"full address:s:{terminalServer}");
            res.AppendLine("alternate shell:s:||Link42");
            res.AppendLine("remoteapplicationprogram:s:||Link42");
            res.AppendLine($"gatewayhostname:s:{gateway}");
            res.AppendLine("remoteapplicationname:s:Link42.exe");
            res.AppendFormat("remoteapplicationcmdline:s:{0}", param);
            res.AppendLine($"full address:s:{terminalServer}");
            res.AppendFormat(@"username:s: AD\{0}", userLogin);
            return res.ToString();
        }

        public string BuildRemoteDesktopLinkString(string userLogin)
        {
            var res = new StringBuilder();
            res.AppendLine("screen mode id:i:2");
            res.AppendLine("use multimon:i:0");
            res.AppendLine("session bpp:i:16");
            res.AppendLine("compression:i:1");
            res.AppendLine("keyboardhook:i:2");
            res.AppendLine("audiocapturemode:i:0");
            res.AppendLine("videoplaybackmode:i:1");
            res.AppendLine("connection type:i:7");
            res.AppendLine("networkautodetect:i:1");
            res.AppendLine("bandwidthautodetect:i:1");
            res.AppendLine("displayconnectionbar:i:1");
            res.AppendLine("enableworkspacereconnect:i:0");
            res.AppendLine("disable wallpaper:i:0");
            res.AppendLine("allow font smoothing:i:0");
            res.AppendLine("allow desktop composition:i:0");
            res.AppendLine("disable full window drag:i:1");
            res.AppendLine("disable menu anims:i:0");
            res.AppendLine("disable themes:i:0");
            res.AppendLine("disable cursor setting:i:0");
            res.AppendLine("bitmapcachepersistenable:i:1");
            res.AppendLine($"full address:s:{terminalServer}");
            res.AppendLine("audiomode:i:0");
            res.AppendLine("redirectprinters:i:1");
            res.AppendLine("redirectcomports:i:0");
            res.AppendLine("redirectsmartcards:i:0");
            res.AppendLine("redirectclipboard:i:1");
            res.AppendLine("redirectposdevices:i:0");
            res.AppendLine("autoreconnection enabled:i:1");
            res.AppendLine("authentication level:i:2");
            res.AppendLine("prompt for credentials:i:0");
            res.AppendLine("negotiate security layer:i:1");
            res.AppendLine("remoteapplicationmode:i:0");
            res.AppendLine("alternate shell:s:");
            res.AppendLine("shell working directory:s:");
            res.AppendLine($"gatewayhostname:s:{gateway}");
            res.AppendLine("gatewayusagemethod:i:1");
            res.AppendLine("gatewaycredentialssource:i:4");
            res.AppendLine("gatewayprofileusagemethod:i:1");
            res.AppendLine("promptcredentialonce:i:1");
            res.AppendLine("gatewaybrokeringtype:i:0");
            res.AppendLine("use redirection server name:i:0");
            res.AppendLine("rdgiskdcproxy:i:0");
            res.AppendLine("kdcproxyname:s:");
            res.AppendLine("drivestoredirect:s:*");
            res.AppendLine("devicestoredirect:s:*");
            res.AppendFormat(@"username:s:AD\{0}", userLogin);
            return res.ToString();
        }

        public string GetRdpPath()
        {
            string appData = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appData, "EFSOL");
            path = Path.Combine(path, "RemoteApp.rdp");
            logger.Debug("[GetRdpPath] RDP temp path is {0}", path);
            return path;
        }

        public void SaveRdpFile(string content)
        {
            var path = GetRdpPath();
            try
            {
                var p = Path.GetDirectoryName(path);
                if (!Directory.Exists(p)) Directory.CreateDirectory(p);
                File.WriteAllText(path, content, Encoding.Default);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[SaveRdpFile Method failed cause]");
            }

        }

        public string GetDesktopPath()
        {
            string appData = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appData, "EFSOL");
            path = Path.Combine(path, "RemoteDesktop.rdp");
            logger.Debug("[GetDesktopPath] RDP temp path is {0}", path);
            return path;
        }

        public void SaveDesktopFile(string content)
        {
            var path = GetDesktopPath();
            try
            {
                File.WriteAllText(path, content, Encoding.Default);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[SaveDesktopFile Method failed]");
            }

        }
    }

    public class RdpHelperFactory(
        IUnitOfWork dbLayer,
        ISegmentHelper segmentHelper,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        public RdpHelper CreateForAccountUser(Guid accountUserId)
        {
            var accUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountUserId) ?? throw new InvalidOperationException($"Пользователь не найден по id: {accountUserId}");
            var acc = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accUser.AccountId) ?? throw new InvalidOperationException($"Аккаунт не найден по ИД: {accUser.AccountId}");
            var terminalServer = segmentHelper.GetTerminalFarm(acc);
            var gateway = segmentHelper.GetGatewayTerminals(acc);

            return new RdpHelper(terminalServer, gateway, logger, handlerException);
        }

    }
}
