﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Помошник синхронизации информационной базы с Тардис.
    /// </summary>
    public class SynchronizeAccountDatabaseWithTardisHelper(
        ITardisSynchronizerHelper tardisSynchronizerHelper,
        IHandlerException handlerException,
        IUnitOfWork dbLayer)
        : BaseTardisSynchronizeHelper(dbLayer)
    {
        /// <summary>
        /// Выполнить синхронизацию в отдельном потоке.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        public void SynchronizeAccountDatabaseViaNewThread(Guid accountDatabaseId)
        {
            try
            {
                if (!NeedSynchronizeAccountDatabase(accountDatabaseId))
                    return;

                tardisSynchronizerHelper
                    .SynchronizeAccountDatabase<UpdateAccountDatabaseTrigger, AccountDatabaseIdDto>(
                        new AccountDatabaseIdDto
                        {
                            AccountDatabaseId = accountDatabaseId
                        });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка уведомления Тардис о изменении информационной базы.]");
            }
        }

        /// <summary>
        /// Синхронизировать добавление базы на разделителях с тардис.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        public void SynchronizeAcDbOnDelimitersCreation(Guid accountDatabaseId)
        {
            try
            {
                var accountDatabase = GetAccountDatabaseOrThrowException(accountDatabaseId);

                if (!NeedSynchronizeAccountDatabase(accountDatabaseId))
                    return;

                tardisSynchronizerHelper
                    .SynchronizeAccountDatabase<CreateDatabaseOnDelimitersTrigger, CreateDatabaseOnDelimitersDto>(
                        new CreateDatabaseOnDelimitersDto
                        {
                            AccountDatabaseId = accountDatabaseId,
                            ConfigurationCode = accountDatabase.AccountDatabaseOnDelimiter.DbTemplateDelimiterCode
                        });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка уведомления Тардис о добавлении информационной базы.]");
            }
        }
    }
}
