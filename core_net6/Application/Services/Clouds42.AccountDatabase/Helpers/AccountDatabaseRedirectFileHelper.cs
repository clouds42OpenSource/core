﻿using Clouds42.Configurations.Configurations;
using Clouds42.Locales.Contracts.Interfaces;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер для редиректа опубликованных инф. баз
    /// </summary>
    public class AccountDatabaseRedirectFileHelper(ILocalesConfigurationDataProvider localesConfigurationDataProvider)
    {
        private readonly Lazy<string> _webConfigFileName = new(
            CloudConfigurationProvider.AccountDatabase.Publish.GetWebConfigFileName);
        private readonly Lazy<string> _lockWebConfigFileName = new(
            CloudConfigurationProvider.AccountDatabase.Publish.GetLockWebConfigFileName);
        private readonly Lazy<string> _routeValueForHttpRedirect = new(CloudConfigurationProvider.AccountDatabase.Publish.GetRouteValueForHttpRedirect);

        /// <summary>
        /// Получить полный путь для web конфига с блокировкой
        /// </summary>
        /// <param name="publishedDatabasePhysicalPath">Физический путь к опубликованной инф. базе</param>
        /// <returns>Полный путь для web конфига с блокировкой</returns>
        public string GetFullPathForLockWebConfigFile(string publishedDatabasePhysicalPath)
            => Path.Combine(publishedDatabasePhysicalPath, _lockWebConfigFileName.Value);

        /// <summary>
        /// Получить полный путь для web конфига
        /// </summary>
        /// <param name="publishedDatabasePhysicalPath">Физический путь к опубликованной инф. базе</param>
        /// <returns>Полный путь для web конфига</returns>
        public string GetFullPathForWebConfigFile(string publishedDatabasePhysicalPath)
            => Path.Combine(publishedDatabasePhysicalPath, _webConfigFileName.Value);

        /// <summary>
        /// Создание web-config-а базы с необходимыми параметрами
        /// </summary>
        /// <param name="companyGroupName">Название группы компании</param>
        /// <param name="publishedDatabasePhysicalPath">Физический путь опубликованного приложения (куда писать файл)</param>
        /// <param name="pathTo1CModule">Путь к модулю публикации</param>
        public void CreateWebConfigFile(string companyGroupName, string publishedDatabasePhysicalPath,
            string pathTo1CModule)
        {
            var text = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                            <configuration>
                                                <system.webServer>
                                                    <handlers>
                                                          <add name=""1C Web-service Extension"" path=""*"" verb=""*"" modules=""IsapiModule"" scriptProcessor=""{pathTo1CModule}"" resourceType=""Unspecified"" requireAccess=""None"" />
                                                    </handlers>
                                                    <security>
                                                        <authorization>
                                                            <remove users=""*"" roles="""" verbs="""" />
                                                            <add accessType=""Allow"" roles=""ad\{companyGroupName}"" />
                                                            <add accessType=""Allow"" roles=""ad\Администраторы домена"" />
                                                        </authorization>
                                                    </security>
                                                </system.webServer>
                                            </configuration>";

            var filePath = Path.Combine(publishedDatabasePhysicalPath, _webConfigFileName.Value);

            if (File.Exists(filePath))
                File.Delete(filePath);

            File.WriteAllText(filePath, text);
        }

        /// <summary>
        /// Лок файл содержит конфигурации приложения
        /// </summary>
        /// <param name="publishedDatabaseWebConfigPath">Физический путь к конфиг файлу опубликованной инф. базе</param>
        public bool LockFileContainsConfigurations(string publishedDatabaseWebConfigPath)
        {
            if (!File.Exists(publishedDatabaseWebConfigPath))
                return false;

            var lockConfigText = File.ReadAllText(publishedDatabaseWebConfigPath);

            return !lockConfigText.Contains("httpRedirect");
        }

        /// <summary>
        /// Удалить файл web конфига
        /// </summary>
        /// <param name="webConfigFilePath">Путь к web конфигу</param>
        public void DeleteWebConfigFile(string webConfigFilePath)
        {
            if (File.Exists(webConfigFilePath))
                File.Delete(webConfigFilePath);
        }

        /// <summary>
        /// Получить xml тэг блокировки для файла web конфига
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Xml тэг блокировки для файла web конфига</returns>
        public string GetLockXmlForWebConfig(Guid accountId)
            => $@"<?xml version=""1.0"" encoding=""UTF-8""?>
						<configuration>
							<system.webServer>
								<httpRedirect enabled=""true"" destination=""{GetFullPathRouteValueForHttpRedirect(accountId)}"" httpResponseStatus=""Permanent""/>
							</system.webServer>
				  </configuration>";


        /// <summary>
        /// Получить полный путь маршрута для редиректа
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Полный путь маршрута для редиректа</returns>
        private string GetFullPathRouteValueForHttpRedirect(Guid accountId) =>
            localesConfigurationDataProvider.GetFullPathToAction(accountId, _routeValueForHttpRedirect.Value);
    }
}