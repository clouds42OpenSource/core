﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Помошник синхронизации с Тардис доступов пользователя к информационной базы.
    /// </summary>
    public class SynchronizeAccountDatabaseAccessWithTardisHelper(
        ITardisSynchronizerHelper tardisSynchronizerHelper,
        IHandlerException handlerException,
        IUnitOfWork dbLayer)
        : BaseTardisSynchronizeHelper(dbLayer)
    {
        /// <summary>
        /// Выполнить в отдельном потоке синхронизацию с Тардис удаления доступа в базе у пользователя.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        /// <param name="accountUserId">Идентификатор пользователя облака.</param>
        public void SynchronizeRemoveAccessAccountDatabaseViaNewThread(Guid accountDatabaseId, Guid accountUserId)
        {
            if (!NeedSynchronizeAccountUser(accountUserId))
                return;
            try
            {
                SynchronizeAccessAccountDatabase(accountDatabaseId, accountUserId, AccountDatabaseAccessDto.StatusEnum.Disable, tardisSynchronizerHelper);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка уведомления Тардис о изменение информационной базы.]");
            }
        }

        /// <summary>
        /// Выполнить в отдельном потоке синхронизацию с Тардис предоставления доступа пользователю в базу.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        /// <param name="accountUserId">Идентификатор пользователя облака.</param>
        public void SynchronizeGrandAccessAccountDatabaseViaNewThread(Guid accountDatabaseId, Guid accountUserId)
        {
            if (!NeedSynchronizeAccountUser(accountUserId))
                return;
            try
            {
                SynchronizeAccessAccountDatabase(accountDatabaseId, accountUserId, AccountDatabaseAccessDto.StatusEnum.Enable, tardisSynchronizerHelper);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка уведомления Тардис о изменение информационной базы.]");
            }
        }

        /// <summary>
        /// Выполнить синхронизаю с Тардис о изменение доступа пользоваля в информационной базе.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        /// <param name="accountUserId">Идентификатор пользователя облака.</param>
        /// <param name="status">Статус устанавливаемого доступа <see cref="AccountDatabaseAccessDto.StatusEnum"/></param>
        /// <param name="tardisSynchronizerHelper">Помошник синхронизации.</param>
        private void SynchronizeAccessAccountDatabase(
            Guid accountDatabaseId,
            Guid accountUserId,
            AccountDatabaseAccessDto.StatusEnum status,
            ITardisSynchronizerHelper tardisSynchronizerHelper)
        {
            tardisSynchronizerHelper.SynchronizeAccountDatabaseAccess<UpdateAccountDatabaseAccessTrigger, AccountDatabaseAccessDto>(
                    new AccountDatabaseAccessDto
                    {
                        AccountDatabaseId = accountDatabaseId,
                        AccountUserId = accountUserId,
                        Status = status,
                        AccessType = AccountDatabaseAccessDto.AccessTypeEnum.Администратор
                    });
        }
    }
}
