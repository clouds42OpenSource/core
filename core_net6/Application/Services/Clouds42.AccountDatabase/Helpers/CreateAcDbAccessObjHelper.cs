﻿using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер для создания объектов доступа к инф. базам
    /// </summary>
    public static class CreateAcDbAccessObjHelper
    {
        /// <summary>
        /// Создать объект доступа к инф. базе
        /// </summary>
        /// <param name="model">Параметры предоставления доступа</param>
        /// <returns>Объект доступа к инф. базе</returns>
        public static AcDbAccess CreateAcDbAccessObj(AcDbAccessPostAddModelDto model)
            => CreateAcDbAccessObj(model.AccountDatabaseID, model.AccountID, model.LocalUserID, model.AccountUserID);

        /// <summary>
        /// Создать объект доступа к инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">Параметры предоставления доступа</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="localUserId">ID локального пользователя</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Объект доступа к инф. базе</returns>
        public static AcDbAccess CreateAcDbAccessObj(Guid accountDatabaseId, Guid accountId, Guid localUserId, Guid accountUserId)
            => new()
            {
                AccountDatabaseID = accountDatabaseId,
                AccountID = accountId,
                LocalUserID = localUserId,
                AccountUserID = accountUserId,
                ID = Guid.NewGuid()
            };
    }
}
