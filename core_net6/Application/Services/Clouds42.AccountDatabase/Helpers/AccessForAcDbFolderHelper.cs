﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Хэлпер управления доступом к папке инф. базы
    /// </summary>
    public class AccessForAcDbFolderHelper(
        IUnitOfWork dbLayer,
        AccountDatabasePathHelper accountDatabasesPathHelper,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Изменить доступ к папке
        /// </summary>
        /// <param name="action">Тип действия управления доступом</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        public void ChangeAccessForDbFolder(Guid accountDatabaseId, Guid accountUserId, AclAction action)
        {
            var accountDatabasePath = string.Empty;
            try
            {
                var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == accountDatabaseId);
                accountDatabasePath = accountDatabasesPathHelper.GetPath(accountDatabase);

                var user = dbLayer.AccountUsersRepository.FirstOrThrowException(u => u.Id == accountUserId,
                    $"Пользователь по ID {accountUserId} не найден");

                if (accountDatabase.AccountId == user.AccountId)
                    return;

                ChangeAccessForDbFolder(action, accountDatabasePath, user.Login);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, 
                    $"[Ошибка установки прав на папку] с типом {action}. Папка: {accountDatabasePath}");
            }
        }

        /// <summary>
        /// Изменить доступ к папке
        /// </summary>
        /// <param name="action">Тип действия управления доступом</param>
        /// <param name="acDbFolderPath">Путь к папке инф. базы</param>
        /// <param name="accountUserLogin">Логин пользователя</param>
        public void ChangeAccessForDbFolder(AclAction action, string acDbFolderPath, string accountUserLogin)
        {
            if (string.IsNullOrWhiteSpace(acDbFolderPath) || !Directory.Exists(acDbFolderPath))
            {
                logger.Warn($"Директория инф. базы по пути {acDbFolderPath} не найдена");
                return;
            }
                
            if (action == AclAction.SetAccessControl)
            {
                activeDirectoryTaskProcessor.RunTask(provider =>
                    provider.SetDirectoryAclForUser(accountUserLogin, acDbFolderPath));
                return;
            }

            activeDirectoryTaskProcessor.RunTask(provider =>
                provider.RemoveDirectoryAclForUser(accountUserLogin, acDbFolderPath));
        }
    }
}
