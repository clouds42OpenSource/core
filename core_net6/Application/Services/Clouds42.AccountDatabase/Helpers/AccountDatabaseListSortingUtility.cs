﻿using Clouds42.AccountDatabase.Managers;
using Clouds42.Common;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Утилита для сортиовки записей списка информационных баз
    /// </summary>
    public static class AccountDatabaseListSortingUtility
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateQueryable<Domain.DataModels.AccountDatabase>> SortingActions = new()
        {
            {AccountDatabaseListSortFieldNames.CreationDate, SortByCreationDate},
            {AccountDatabaseListSortFieldNames.LastActivityDate, SortByLastActivityDate}
        };

        /// <summary>
        /// Сортитровать записи списка баз данных
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи списка баз данных</returns>
        private static IOrderedQueryable<Domain.DataModels.AccountDatabase> SortByDefault(IQueryable<Domain.DataModels.AccountDatabase> records) 
            => records.OrderByDescending(row => row.CreationDate);


        /// <summary>
        /// Сортитровать выбранные записи списка баз данных
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей списка баз данных по полю CreationDate</returns>
        private static IOrderedQueryable<Domain.DataModels.AccountDatabase> SortByCreationDate(IQueryable<Domain.DataModels.AccountDatabase> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.CreationDate),
                SortType.Desc => records.OrderByDescending(row => row.CreationDate),
                _ => SortByDefault(records)
            };
        }


        /// <summary>
        /// Сортитровать выбранные записи списка баз данных
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей списка баз данных по полю CreationDate</returns>
        private static IOrderedQueryable<Domain.DataModels.AccountDatabase> SortByLastActivityDate(IQueryable<Domain.DataModels.AccountDatabase> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.LastActivityDate),
                SortType.Desc => records.OrderByDescending(row => row.LastActivityDate),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Производит сортировку выбранных записей списка баз данных
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи  списка баз данных</returns>
        public static IOrderedQueryable<Domain.DataModels.AccountDatabase> MakeSorting(IQueryable<Domain.DataModels.AccountDatabase> records, SortingDataDto sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();
            
            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate) 
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }

    }
}