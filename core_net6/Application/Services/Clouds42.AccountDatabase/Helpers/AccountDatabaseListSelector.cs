﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Helpers
{
    /// <summary>
    /// Класс для выбора списка информационных баз
    /// </summary>
    public sealed class AccountDatabaseListSelector(IUnitOfWork dbLayer, AccountDatabaseItemsFilterParamsDto filter)
    {
        /// <summary>
        /// Параметры фильтрации записей списка информационных баз
        /// </summary>
        private readonly AccountDatabaseItemsFilterParamsDto _filter = filter;

        /// <summary>
        /// Дата контекст
        /// </summary>
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Получить валидные записи информационных баз
        /// </summary>
        /// <returns>Валидные записи информационных баз</returns>
        private IQueryable<Domain.DataModels.AccountDatabase> GetRecords()
            => _dbLayer.DatabasesRepository.WhereLazy(item => item.Account != null && item.State != DatabaseState.DeletedFromCloud.ToString());


        /// <summary>
        /// Отфильтровать записи
        /// </summary>
        /// <param name="records">Записи для фильтрации</param>
        /// <returns>Отфильтрованные записи</returns>
        private IQueryable<Domain.DataModels.AccountDatabase> FilterByFields(
            IQueryable<Domain.DataModels.AccountDatabase> records)
        {
            var zone = _filter.SearchLine.ToInt();
            return string.IsNullOrEmpty(_filter.SearchLine)
                ? records
                : records.Where(
                    rec => rec.Caption != null && rec.Caption.Contains(_filter.SearchLine) ||
                           rec.V82Name != null && rec.V82Name.Contains(_filter.SearchLine) ||
                           rec.Account.AccountCaption != null &&
                           rec.Account.AccountCaption.Contains(_filter.SearchLine) ||
                           rec.Account.IndexNumber.ToString().Contains(_filter.SearchLine) ||
                           rec.DbNumber.ToString().Contains(_filter.SearchLine) ||
                           (rec.AccountDatabaseOnDelimiter != null &&
                            rec.AccountDatabaseOnDelimiter.Zone == zone)
                );
        }


        /// <summary>
        /// Получить отфильтрованный список информационных баз
        /// </summary>
        /// <returns>Отфильтрованный список информационных баз</returns>
        public IQueryable<Domain.DataModels.AccountDatabase> SelectWithFilters()
        {
            return _filter == null
            ? GetRecords()
            : GetRecords().ComposeFilters(FilterByFields);
        }
    }
}
