﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Managers
{
    /// <summary>
    /// Менеджер для регистрации/созднаия инф. базы по шаблону
    /// </summary>
    public class RegisterAccountDatabaseByTemplateManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IRegisterAccountDatabaseByTemplateProvider registerAccountDatabaseByTemplateProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider,
            dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Зарегистрировать/создать инф. базу по шаблону
        /// при активации сервиса биллинга
        /// </summary>
        /// <param name="model">Модель регистрации инф. базы по шаблону
        /// при активации сервиса биллинга</param>
        public ManagerResult RegisterDatabaseUponServiceActivation(RegisterDatabaseUponServiceActivationDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabse_RegisterUponServiceActivation,
                    () => model.AccountId);

                registerAccountDatabaseByTemplateProvider.RegisterDatabaseUponServiceActivation(model);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"Ошибка регистрации инф. базу по шалону '{model.TemplateId}' при активации сервиса биллинга '{model.ServiceId}'");

                return PreconditionFailed(ex.Message);
            }
        }
    }
}
