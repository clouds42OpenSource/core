﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Managers
{
    /// <summary>
    /// Менеджер создания материнской базы разделителей
    /// </summary>
    public class CreateDelimiterSourceAccountDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICreateDelimiterSourceAccountDatabaseProvider createDelimiterSourceAccountDatabaseProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabase">Модель материнской базы разделителей</param>
        /// <returns>Результат создания</returns>
        public ManagerResult Create(DelimiterSourceAccountDatabaseDto delimiterSourceAccountDatabase)
        {
            AccessProvider.HasAccess(ObjectAction.DelimiterSourceAccountDatabase_Create);
            var message =
                $"Создание материнской базы разделителей. dbId = {delimiterSourceAccountDatabase.AccountDatabaseId}; configCode = {delimiterSourceAccountDatabase.DbTemplateDelimiterCode}";
            try
            {
                logger.Info(message);
                createDelimiterSourceAccountDatabaseProvider.Create(delimiterSourceAccountDatabase);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка создания материнской базы на разделителях] {message} завершилось ошибкой");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Создать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabase">Модель материнской базы разделителей</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<Guid> CreateMotherBase(MotherAccountDatabaseDto delimiterSourceAccountDatabase)
        {
            AccessProvider.HasAccess(ObjectAction.DelimiterSourceAccountDatabase_Create);
            var message =
                $"Создание материнской базы разделителей. acId = {delimiterSourceAccountDatabase.AccountId}; configCode = {delimiterSourceAccountDatabase.DbTemplateDelimiterCode}";
            try
            {
                logger.Info(message);
                var accountDatabaseId = createDelimiterSourceAccountDatabaseProvider.CreateMotherBase(delimiterSourceAccountDatabase);
                return Ok(accountDatabaseId);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка создания материнской базы на разделителях] {message} завершилось ошибкой");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }
    }
}
