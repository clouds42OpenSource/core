﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Managers
{
    /// <summary>
    ///     Точка входа для управления базами на разделителях
    /// </summary>
    public class DbTemplateDelimitersManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IDbTemplateDelimitersDataProvider dbTemplateDelimitersDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        ///  Провайдер для работы с данными шаблонов на разделителях
        /// </summary>
        private readonly IDbTemplateDelimitersDataProvider _dbTemplateDelimitersDataProvider = dbTemplateDelimitersDataProvider;
	    
        /// <summary>
        /// Хендлер ошибок
        /// </summary>
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить все конфигурации разделителей
        /// </summary>
        /// <param name="args">параметры для выбора шаблонов баз на разделителях</param>
        /// <returns>Все конфигурации разделителей</returns>
        public ManagerResult<SelectDataResultCommonDto<DbTemplateDelimeterItemDto>> GetDbTemplateDelimetersItems(GetDbTemplateDelimitersParamsDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ViewDbTemplateDelimitersReferences, () => AccessProvider.ContextAccountId);

                var result = _dbTemplateDelimitersDataProvider.GetDbTemplateDelimetersItems(args);

                return Ok(result);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка получения шаблонов баз на разделителях]");
                return PreconditionFailed<SelectDataResultCommonDto<DbTemplateDelimeterItemDto>>(exception.Message);
            }
        }

        /// <summary>
        ///     Метод редактирования базы на разделителях
        /// </summary>
        /// <param name="dbTemplateDelimiters">Обновленная база на разделителях</param>
        /// <returns>Возвращает <c>true</c> если редактирование прошло успешно</returns>
        public ManagerResult<bool> UpdateDbTemplateDelimiterItem(IDbTemplateDelimiters dbTemplateDelimiters)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.EditDbTemplateDelimitersReferences, () => AccessProvider.ContextAccountId);

                _dbTemplateDelimitersDataProvider.UpdateDbTemplateDelimiterItem(dbTemplateDelimiters);

                return Ok(true);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, $"[Ошибка редактирования базы на разделителях] {dbTemplateDelimiters.ConfigurationId}.");
                return PreconditionFailed<bool>(exception.Message);
            }
        }

        /// <summary>
        ///     Метод добавления новой базы на разделителях
        /// </summary>
        /// <param name="newDbTemplateDelimiter">База на разделителях для добавления</param>
        /// <returns>Возвращает <c>true</c> если добавление прошло успешно</returns>
        public ManagerResult<bool> AddDbTemplateDelimiterItem(IDbTemplateDelimiters newDbTemplateDelimiter)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewDbTemplateDelimitersReferences, () => AccessProvider.ContextAccountId);

                _dbTemplateDelimitersDataProvider.AddDbTemplateDelimiterItem(newDbTemplateDelimiter);

                return Ok(true);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, $"[Ошибка добавления новай базы на разделителях] {newDbTemplateDelimiter.ConfigurationId}.");
				return PreconditionFailed<bool>(exception.Message);
            }
        }

        /// <summary>
        ///     Удалить существующую базу на разделителях
        /// </summary>
        /// <param name="args">Параметры для удаления базы на разделителях</param>
        /// <returns>Возвращает <c>true</c> если удаление прошло успешно</returns>
		public ManagerResult<bool> DeleteDbTemplateDelimiterItem(DeleteDbTemplateDelimiterDto args)
	    {
		    try
		    {
			    AccessProvider.HasAccess(ObjectAction.DeleteDbTemplateDelimitersReferences, () => AccessProvider.ContextAccountId);

                _dbTemplateDelimitersDataProvider.DeleteDbTemplateDelimiterItem(args);

                return Ok(true);
		    }
		    catch (Exception exception)
		    {
                _handlerException.Handle(exception, $"[Ошибка удаления базы на разделителях] ({args.ConfigurationId}).");
				return PreconditionFailed<bool>(exception.Message);
		    }
	    }

        /// <summary>
        ///     Получить базу на разделителях
        /// </summary>
        /// <param name="dbTemplateDelimitersId">ID конфигурации для которой получить базу на разделителях.</param>
        /// <returns>Базу на разделителях</returns>
		public ManagerResult<DbTemplateDelimeterItemDto> GetDbTemplateDelimiter(string dbTemplateDelimitersId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ViewDbTemplateDelimitersReferences, () => AccessProvider.ContextAccountId);

               var database = _dbTemplateDelimitersDataProvider.GetDbTemplateDelimiter(dbTemplateDelimitersId);

                return Ok(database);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, $"[Ошибка получить базу на разделителях] ({dbTemplateDelimitersId}).");
                return PreconditionFailed<DbTemplateDelimeterItemDto>(exception.Message);
            }
        }
    }
}
