﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Managers
{
    /// <summary>
    /// Менеджер редактирования материнской базы разделителей
    /// </summary>
    public class EditDelimiterSourceAccountDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IEditDelimiterSourceAccountDatabaseProvider editDelimiterSourceAccountDatabaseProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Редактировать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabase">Модель материнской базы разделителей</param>
        /// <returns>Результат редактирования</returns>
        public ManagerResult Edit(DelimiterSourceAccountDatabaseDto delimiterSourceAccountDatabase)
        {
            AccessProvider.HasAccess(ObjectAction.DelimiterSourceAccountDatabase_Edit);
            var message =
                $"Редактирование материнской базы разделителей. dbId = {delimiterSourceAccountDatabase.AccountDatabaseId}; configCode = {delimiterSourceAccountDatabase.DbTemplateDelimiterCode}";
            try
            {
                logger.Info(message);
                editDelimiterSourceAccountDatabaseProvider.Edit(delimiterSourceAccountDatabase);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилось ошибкой]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
