﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Managers
{
    /// <summary>
    /// Менеджер для удаления материнской базы разделителей
    /// </summary>
    public class RemoveDelimiterSourceAccountDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IRemoveDelimiterSourceAccountDatabaseProvider removeDelimiterSourceAccountDatabaseProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Удалить материнскую базу разделителей
        /// </summary>
        /// <param name="accountDatabaseId">Id базы источника на разделителях</param>
        /// <param name="dbTemplateDelimiterCode">Код шаблона на разделителях</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult Remove(Guid accountDatabaseId, string dbTemplateDelimiterCode)
        {
            AccessProvider.HasAccess(ObjectAction.DelimiterSourceAccountDatabase_Remove);
            var message = $"Удаление материнской базы разделителей. dbId = {accountDatabaseId}; configCode = {dbTemplateDelimiterCode}";
            try
            {
                logger.Info(message);
                removeDelimiterSourceAccountDatabaseProvider.Remove(accountDatabaseId, dbTemplateDelimiterCode);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка удаления материнской базы]{message} завершилось ошибкой");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
