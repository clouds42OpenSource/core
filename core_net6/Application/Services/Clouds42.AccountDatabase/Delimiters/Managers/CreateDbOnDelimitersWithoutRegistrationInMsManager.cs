﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Managers
{
    /// <summary>
    /// Менеджер для создания базы на разделителях(без регистрации в МС)
    /// </summary>
    public class CreateDbOnDelimitersWithoutRegistrationInMsManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICreateDbOnDelimitersWithoutRegistrationInMsProvider createDbOnDelimitersWithoutRegistrationInMsProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать базу на разделителях(без регистрации в МС)
        /// </summary>
        /// <param name="model">Модель создания базы на разделителях</param>
        /// <returns>Id созданной базы</returns>
        public ManagerResult<Guid> Create(CreateDbOnDelimitersWithoutRegistrationInMsDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DatabaseOnDelimiters_CreateWithoutRegistrationInMs,
                    () => model.AccountId);

                var dbId = createDbOnDelimitersWithoutRegistrationInMsProvider.Create(model);

                return Ok(dbId);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка создания базы на разделителях] для аккаунта '{model.AccountId}' и конфигурации '{model.ConfigurationId}'");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }
    }
}
