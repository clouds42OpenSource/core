﻿namespace Clouds42.AccountDatabase.Delimiters
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки материнских баз разделителей
    /// </summary>
    public static class DelimiterSourceAccountDatabaseSortFieldNames
    {
        /// <summary>
        /// Наименование поля номер базы
        /// </summary>
        public const string AccountDatabaseV82Name = "accountdatabasev82name";

        /// <summary>
        /// Наименование поля адрес публикации базы на разделителях
        /// </summary>
        public const string DatabaseOnDelimitersPublicationAddress = "databaseondelimiterspublicationaddress";

        /// <summary>
        /// Наименование поля код конфигурации базы на разделителях
        /// </summary>
        public const string DbTemplateDelimiterCode = "dbtemplatedelimitercode";

    }
}