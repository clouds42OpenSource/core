﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Internal;
using Clouds42.CoreWorker.AccountUserJobs.ManageLockState;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseRestoreFromTomb;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.AccountDatabase.Delimiters.Helpers
{
    /// <summary>
    /// Провайдер по завершению регистрации базы на разделителях.
    /// </summary>
    public class ReleaseRegistrationAccountDatabaseDelimiterProvider(
        IUnitOfWork dbLayer,
        DatabaseStatusHelper databaseStatusHelper,
        DatabaseUploadedFileHelper databaseUploadedFileHelper,
        MailNotificationFactory mailNotificationFactory,
        ManageAccountUserLockStateJobWrapper manageAccountUserLockStateJobWrapper,
        SynchronizeAccountDatabaseWithTardisHelper synchronizeAccountDatabaseWithTardisHelper,
        SynchronizeAccountDatabaseAccessWithTardisHelper synchronizeAccountDatabaseAccessWithTardisHelper,
        AccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        ILetterNotificationProcessor letterNotificationProcessor,
        IEmailAddressesDataProvider emailAddressesDataProvider,
        IOnChangeDatabaseStateTrigger onChangeDatabaseStateTrigger,
        ILogger42 logger)
    {
        /// <summary>
        /// Проверить модель на валидность и завершить регистрацию базы на разделителях.
        /// </summary>
        /// <param name="model">Модель базы на разделителях</param>
        public void ValidateModelAndRelease(AccountDatabaseViaDelimiterDto model)
        {
            var database = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == model.AccountDatabaseId) ??
                           throw new InvalidOperationException($"Не удалось получить базу данных по Id {model.AccountDatabaseId}");

            var initialDatabaseState = database.State;

            if (model.DatabaseSourceId == null && model.Zone == null)
            {
                databaseStatusHelper.SetDbStatus(database.V82Name, DatabaseState.ErrorCreate, model.Comment);
                logger.Warn($"Ответ для базы {model.AccountDatabaseId} :: {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} вернулся с незаполнеными полями");
                return;
            }

            if (model.DatabaseState == DatabaseState.ErrorCreate.ToString())
            {
                databaseUploadedFileHelper.DeleteDbOnDelimitersUploadedFileWithDelay(model.AccountDatabaseId, DateTime.Now.AddDays(1));
            }

            if (model.DatabaseState != DatabaseState.Ready.ToString())
            {
                databaseStatusHelper.SetDbStatus(model.AccountDatabaseId, model.DatabaseState.GetDatabaseState(), model.Comment);
                return;
            }

            logger.Debug($"Начинаем регистрацию данных для базы на разделителях '{model.AccountDatabaseId}'");
            RegistrationAcDbOnDelimiter(model, initialDatabaseState);
        }


        /// <summary>
        /// Запись в таблицу данных о зоне для информационной базы
        /// Предоставление доступа к базе для администратора
        /// Выставления статуса Redy и  статус Publish для информационной базы
        /// </summary>
        /// <param name="model">Модель базы на разделителях</param>
        /// <param name="initialDatabaseState">Исходный статус базы</param>
        public void RegistrationAcDbOnDelimiter(AccountDatabaseViaDelimiterDto model, string initialDatabaseState)
        {
            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == model.AccountDatabaseId);
            if (accountDatabase == null)
            {
                var errorMessage = $"Не найдена база {model.AccountDatabaseId}";
                logger.Warn(errorMessage);
                throw new InvalidOperationException(errorMessage);
            }

            var accountDatabaseOnDelimiter =
                dbLayer.AccountDatabaseDelimitersRepository.FirstOrDefault(t =>
                    t.AccountDatabaseId == model.AccountDatabaseId);

            if (accountDatabaseOnDelimiter == null)
            {
                var errorMessage = $"Не удалось зарегистрировать создание базы, не найдена запись в таблице AccountDatabaseDelimiters по номеру базы '{model.AccountDatabaseId}'";
                logger.Warn(errorMessage);
                throw new InvalidOperationException(errorMessage);
            }

            if (accountDatabaseOnDelimiter.DatabaseSourceId.HasValue)
            {
                DeactivateNotActiveUsers(accountDatabaseOnDelimiter);
            }

            try
            {
                accountDatabaseOnDelimiter.Zone = model.Zone;
                accountDatabaseOnDelimiter.DatabaseSourceId = model.DatabaseSourceId;
                accountDatabase.PublishStateEnum = PublishState.Published;
                accountDatabase.StateEnum = DatabaseState.Ready;

                dbLayer.AccountDatabaseDelimitersRepository.Update(accountDatabaseOnDelimiter);
                dbLayer.DatabasesRepository.Update(accountDatabase);

                onChangeDatabaseStateTrigger.Execute(accountDatabase.Id);

                databaseUploadedFileHelper.DeleteDbOnDelimitersUploadedFileWithDelay(
                accountDatabaseOnDelimiter, DateTime.Now.AddDays(1));

                dbLayer.Save();


                logger.Trace($"Создание базы на разделителях базы '{model.AccountDatabaseId}-{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}' завершено успешно");
            }
            catch (Exception ex)
            {
                logger.Warn(ex, "[Ошибка регистрации результата создания базы на разделителях]");
                throw;
            }

            SendNotification(model.AccountDatabaseId, accountDatabaseOnDelimiter.LoadTypeEnum, initialDatabaseState);


            synchronizeAccountDatabaseWithTardisHelper.SynchronizeAcDbOnDelimitersCreation(model.AccountDatabaseId);
            SynchronizeAccountUsersAccessesToDatabaseWithTardis(model.AccountDatabaseId);
        }

        /// <summary>
        /// Отключить пользователей без аренды 1С от базы.
        /// </summary>            
        private void DeactivateNotActiveUsers(AccountDatabaseOnDelimiters accountDatabaseOnDelimiter)
        {
            var accountUsersQuery =
                from access in dbLayer.AcDbAccessesRepository.WhereLazy(a => a.AccountUserID != null)
                join r in dbLayer.ResourceRepository.WhereLazy(r =>
                        r.Subject != null && (r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb ||
                                              r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser)) on access.AccountUserID
            equals r.Subject
            into resources
                from resource in resources.DefaultIfEmpty()
                where resource == null && access.AccountDatabaseID == accountDatabaseOnDelimiter.AccountDatabaseId
                select new { access.AccountUser.Id, access.AccountUser.Login };

            var accountUsers = accountUsersQuery.ToList();

            foreach (var accountUser in accountUsers)
            {
                logger.Trace($"Отключаем аренду пользователю '{accountUser.Login}' в базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabaseOnDelimiter.AccountDatabase)}");
                manageAccountUserLockStateJobWrapper.Start(new ManageAccountUserLockStateParamsDto
                {
                    AccountUserId = accountUser.Id,
                    IsAvailable = false
                });
            }
        }

        /// <summary>
        /// Отправка сообщения клиенту, о завершении создания базы
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        /// <param name="loadType">триггер по которому была создана база.</param>
        /// <param name="initialDatabaseState">Исходный статус базы</param>
        private void SendNotification(Guid accountDatabaseId, DelimiterLoadType loadType, string initialDatabaseState)
        {
            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);
            var databaseInfo = $"[{accountDatabase.Id}]-{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}";

            if (loadType == DelimiterLoadType.Restored)
            {
                logger.Info($"Начало отправки письма клиенту о восстановлении ИБ {databaseInfo} из бекапа");
                mailNotificationFactory.SendMail<MailNotifyRestoreAccountDatabaseOnDelimiterFromTombClient, Guid>(accountDatabaseId);
                logger.Info($"Сообщение отправлено клиенту о восстановлении ИБ {accountDatabaseId} из бекапа");
                return;
            }

            if (initialDatabaseState == DatabaseState.Ready.ToString())
            {
                logger.Info(
                    $"Статус информационной базы {databaseInfo}: {initialDatabaseState}."
                + " Письмо отправлять не нужно.");
                return;
            }

            logger.Info($"Начало отправки письма клиенту о создании ИБ {databaseInfo}");
            SendNotificationToClient(accountDatabase);
            logger.Info($"Сообщение отправлено клиенту о создании ИБ {databaseInfo}");
        }

        /// <summary>
        /// Отправить уведомление клиенту о создании инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        private void SendNotificationToClient(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var acDbPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(accountDatabase);
            var emailsToCopy = emailAddressesDataProvider.GetAllAccountEmailsToSend(accountDatabase.AccountId);

            letterNotificationProcessor
                 .TryNotify<CreateAccountDatabaseLetterNotification, CreateAccountDatabaseLetterModelDto>(
                     new CreateAccountDatabaseLetterModelDto
                     {
                         AccountId = accountDatabase.AccountId,
                         AccountDatabaseCaption = accountDatabase.Caption,
                         AccountDatabasePublishPath = acDbPublishPath,
                         EmailsToCopy = emailsToCopy
                     });
        }

        /// <summary>
        /// Синхронизировать наличие прав доступа пользователей к базе после создания базы с Тардис
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        private void SynchronizeAccountUsersAccessesToDatabaseWithTardis(Guid accountDatabaseId)
        {
            var acDbAccesses =
                dbLayer.AcDbAccessesRepository.Where(access => access.AccountDatabaseID == accountDatabaseId).ToList();

            acDbAccesses.ForEach(acDbAccess =>
            {
                logger.Trace($"Устанавливаем доступ пользователю '{acDbAccess.AccountUser.Login}' к базе '{accountDatabaseId}'");
                synchronizeAccountDatabaseAccessWithTardisHelper.SynchronizeGrandAccessAccountDatabaseViaNewThread(accountDatabaseId, acDbAccess.AccountID);
            });
        }
    }
}
