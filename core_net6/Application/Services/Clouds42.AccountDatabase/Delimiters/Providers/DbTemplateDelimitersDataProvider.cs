﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер для работы с данными шаблонов на разделителях
    /// </summary>
    internal class DbTemplateDelimitersDataProvider(IUnitOfWork dbLayer) : IDbTemplateDelimitersDataProvider
    {
        /// <summary>
        /// Получить шаблон базы на разделителях по Id конфигурации
        /// </summary>
        /// <param name="configurationId">Id конфигурации</param>
        /// <returns>Шаблон базы на разделителях</returns>
        public DbTemplateDelimiters GetDbTemplateDelimitersByConfigurationId(string configurationId) =>
            dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(
                template => template.ConfigurationId == configurationId);

        /// <summary>
        /// Получить все конфигурации разделителей
        /// </summary>
        /// <returns>Все конфигурации разделителей, где key=ID шаблона, value=название шаблона</returns>
        public KeyValuePair<string, string>[] GetDbTemplateDelimiters()
        {
            return dbLayer.DbTemplateDelimitersReferencesRepository.WhereLazy().AsEnumerable().Select(item =>
                new KeyValuePair<string, string>(item.ConfigurationId, item.Name)).ToArray();
        }

        /// <summary>
        /// Получить все конфигурации разделителей
        /// </summary>
        /// <param name="args">параметры для выбора шаблонов баз на разделителях</param>
        /// <returns>Все конфигурации разделителей</returns>
        public SelectDataResultCommonDto<DbTemplateDelimeterItemDto> GetDbTemplateDelimetersItems(GetDbTemplateDelimitersParamsDto args)
        {
            var items = dbLayer.DbTemplateDelimitersReferencesRepository.WhereLazy().Select(item =>
                new DbTemplateDelimeterItemDto
                {
                    ConfigurationId = item.ConfigurationId,
                    TemplateId = item.TemplateId,
                    ConfigurationReleaseVersion = item.ConfigurationReleaseVersion,
                    DemoDatabaseOnDelimitersPublicationAddress = item.DemoDatabaseOnDelimitersPublicationAddress,
                    MinReleaseVersion = item.MinReleaseVersion,
                    ShortName = item.ShortName,
                    Name = item.Name
                }).ToArray();

            return new SelectDataResultCommonDto<DbTemplateDelimeterItemDto>
            {
                Records = items
            };
        }


        /// <summary>
        ///     Получить базу на разделителях
        /// </summary>
        /// <param name="dbTemplateDelimitersId">ID конфигурации для которой получить базу на разделителях.</param>
        /// <returns>Базу на разделителях</returns>
        public DbTemplateDelimeterItemDto GetDbTemplateDelimiter(string dbTemplateDelimitersId)
        {
            var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.ConfigurationId == dbTemplateDelimitersId);

            if (database == null)
                throw new NotFoundException($"Не найдена база на разделителях({dbTemplateDelimitersId})");

            return new DbTemplateDelimeterItemDto
            {
                ConfigurationId = database.ConfigurationId,
                TemplateId = database.TemplateId,
                ConfigurationReleaseVersion = database.ConfigurationReleaseVersion,
                DemoDatabaseOnDelimitersPublicationAddress = database.DemoDatabaseOnDelimitersPublicationAddress,
                MinReleaseVersion = database.MinReleaseVersion,
                ShortName = database.ShortName,
                Name = database.Name
            };
        }


        /// <summary>
        ///     Обновить существующую базу на разделителях
        /// </summary>
        /// <param name="dbTemplateDelimitersToUpdate">Обновленная база на разделителях</param>
        public void UpdateDbTemplateDelimiterItem(IDbTemplateDelimiters dbTemplateDelimitersToUpdate)
        {
            var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.ConfigurationId == dbTemplateDelimitersToUpdate.ConfigurationId);

            if (database == null)
                throw new NotFoundException($"Для редактирования не найден шаблон базы на разделителях({dbTemplateDelimitersToUpdate.ConfigurationId})");

            try
            {
                database.Name = dbTemplateDelimitersToUpdate.Name;
                database.TemplateId = dbTemplateDelimitersToUpdate.TemplateId;
                database.ShortName = dbTemplateDelimitersToUpdate.ShortName;
                database.DemoDatabaseOnDelimitersPublicationAddress =
                    dbTemplateDelimitersToUpdate.DemoDatabaseOnDelimitersPublicationAddress;

                dbLayer.DbTemplateDelimitersReferencesRepository.Update(database);
                dbLayer.Save();
            }
            catch (Exception e)
            {
                var message = $"Во время редактирования шаблона базы на разделителях ({dbTemplateDelimitersToUpdate.ConfigurationId}) произошла ошибка : {e.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }


        /// <summary>
        ///     Добавить новую базу на разделителях
        /// </summary>
        /// <param name="newDbTemplateDelimiter">База на разделителях для добавления</param>
        public void AddDbTemplateDelimiterItem(IDbTemplateDelimiters newDbTemplateDelimiter)
        {
            var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(b =>
                b.Name == newDbTemplateDelimiter.Name && b.ConfigurationId == newDbTemplateDelimiter.ConfigurationId);

            if (database != null)
                throw new InvalidOperationException($"База на разделителях {newDbTemplateDelimiter.ConfigurationId} уже существует.");

            try
            {
                var newDatabase = new DbTemplateDelimiters
                {
                    Name = newDbTemplateDelimiter.Name,
                    ConfigurationId = newDbTemplateDelimiter.ConfigurationId,
                    TemplateId = newDbTemplateDelimiter.TemplateId,
                    ShortName = newDbTemplateDelimiter.ShortName,
                    DemoDatabaseOnDelimitersPublicationAddress = newDbTemplateDelimiter.DemoDatabaseOnDelimitersPublicationAddress,
                    ConfigurationReleaseVersion = newDbTemplateDelimiter.ConfigurationReleaseVersion,
                    MinReleaseVersion = newDbTemplateDelimiter.MinReleaseVersion
                };

                dbLayer.DbTemplateDelimitersReferencesRepository.Insert(newDatabase);
                dbLayer.Save();
            }
            catch (Exception e)
            {
                var message = $"Ошибка создания новой базы в справочнике с именем' ({newDbTemplateDelimiter.ConfigurationId}) произошла ошибка : {e.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }


        /// <summary>
        ///     Удалить существующую базу на разделителях
        /// </summary>
        /// <param name="args">Параметры для удаления базы на разделителях</param>
        public void DeleteDbTemplateDelimiterItem(DeleteDbTemplateDelimiterDto args)
        {
            var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.ConfigurationId == args.ConfigurationId);

            if (database == null)
                throw new NotFoundException($"Базу на разделителях ({args.ConfigurationId}) не найдена.");

            try
            {
                dbLayer.DbTemplateDelimitersReferencesRepository.Delete(database);
                dbLayer.Save();
            }
            catch (Exception e)
            {
                var message =
                    $"Ошибка удаления базы на разделителях с именем'{database.ConfigurationId}' : {e.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }

    }
}
