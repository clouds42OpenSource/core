﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер для проверки возможности создания базы на разделителях
    /// </summary>
    internal class CheckCreationDbOnDelimitersProvider(
        IAccessProvider accessProvider,
        CheckRental1CActivateHelper checkRental1CActivateHelper)
        : ICheckCreationDbOnDelimitersProvider
    {
        /// <summary>
        /// Проверить возможность создать базу на разделителях(без регистрации в МС)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void CheckCreationWithoutRegistrationInMs(Guid accountId)
        {
            if (!accessProvider.GetAccountAdmins(accountId).Any())
                throw new ValidateException("У аккаунта нету ни одного аккаунт-админа. Невозможно создать базу.");

            var checkRental1CActivate = checkRental1CActivateHelper.CheckRental1CActivate(accountId);
            if (!checkRental1CActivate.Complete)
                throw new ValidateException(checkRental1CActivate.Comment);
        }
    }
}