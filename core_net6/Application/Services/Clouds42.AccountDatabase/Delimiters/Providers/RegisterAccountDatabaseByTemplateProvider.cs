﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер для регистрации/созднаия инф. базы по шаблону
    /// </summary>
    internal class RegisterAccountDatabaseByTemplateProvider(
        IRegisterAccountDatabaseProvider registerAccountDatabaseProvider,
        IBillingServiceDataProvider billingServiceDataProvider,
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IInstallServiceExtensionDatabaseProvider installServiceExtensionDatabaseProvider)
        : IRegisterAccountDatabaseByTemplateProvider
    {
        /// <summary>
        /// Зарегистрировать/создать инф. базу по шаблону
        /// при активации сервиса биллинга
        /// </summary>
        /// <param name="model">Модель регистрации инф. базы по шаблону
        /// при активации сервиса биллинга</param>
        public void RegisterDatabaseUponServiceActivation(RegisterDatabaseUponServiceActivationDto model)
        {
            if(!NeedRegisterDatabase(model.ServiceId))
                return;

            var accountDatabaseId = registerAccountDatabaseProvider.RegisterByTemplate(GetAccountUserForRegistrationDatabase(model.AccountId),
                GeDbTemplateOrThrowNewException(model.TemplateId));
            installServiceExtensionDatabaseProvider.TryInstalExtensionForCreatedDatabase(model.ServiceId,
                accountDatabaseId);
        }

        /// <summary>
        /// Признак необходимости регистрировать инф. базу
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <returns>true - если сервис не является Тардисом</returns>
        private bool NeedRegisterDatabase(Guid serviceId)
        {
            var service = billingServiceDataProvider.GetBillingServiceOrThrowNewException(serviceId);
            return service.InternalCloudService != InternalCloudServiceEnum.Tardis;
        }

        /// <summary>
        /// Получить пользователя для регистрации информационной базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Пользователь для регистрации информационной базы</returns>
        private AccountUser GetAccountUserForRegistrationDatabase(Guid accountId)
        {
            var accountAdmins = accessProvider.GetAccountAdmins(accountId);

            if (!accountAdmins.Any())
                throw new NotFoundException(
                    "Невозможно зарегестрировать базу. У аккаунта нет ни одного администратора.");

            var accountAdminId = accountAdmins.First();

            return dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountAdminId);
        }

        /// <summary>
        /// Получить шаблон или выкинуть исключение
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона инф. базы</param>
        /// <returns>Шаблон ифн. базы</returns>
        private DbTemplate GeDbTemplateOrThrowNewException(Guid dbTemplateId) =>
            dbLayer.DbTemplateRepository.FirstOrThrowException(t => t.Id == dbTemplateId,
                $"Не удалось получить шаблон по Id '{dbTemplateId}'");
    }
}
