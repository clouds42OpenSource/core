﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.DelemiterSourceAccountDatabase.Validators;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер редактирования материнской базы разделителей
    /// </summary>
    internal class EditDelimiterSourceAccountDatabaseProvider : IEditDelimiterSourceAccountDatabaseProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly DelimiterSourceAccountDatabaseDtoValidator _validator;

        public EditDelimiterSourceAccountDatabaseProvider(IUnitOfWork dbLayer,
            DelimiterSourceAccountDatabaseDtoValidator validator)
        {
            dbLayer.CheckForNull();
            validator.CheckForNull();
            _dbLayer = dbLayer;
            _validator = validator;
        }


        /// <summary>
        /// Редактировать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabaseDto">Модель материнской базы разделителей</param>
        public void Edit(DelimiterSourceAccountDatabaseDto delimiterSourceAccountDatabaseDto)
        {
            _validator.Validate(delimiterSourceAccountDatabaseDto, true);
            var delimiterSourceAccountDatabase =
                _dbLayer.DelimiterSourceAccountDatabaseRepository.FirstOrThrowException(x =>
                    x.AccountDatabaseId == delimiterSourceAccountDatabaseDto.AccountDatabaseId &&
                    x.DbTemplateDelimiterCode == delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode);

            delimiterSourceAccountDatabase.DatabaseOnDelimitersPublicationAddress =
                delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress;

            _dbLayer.DelimiterSourceAccountDatabaseRepository.Update(delimiterSourceAccountDatabase);
            _dbLayer.Save();
        }
    }
}
