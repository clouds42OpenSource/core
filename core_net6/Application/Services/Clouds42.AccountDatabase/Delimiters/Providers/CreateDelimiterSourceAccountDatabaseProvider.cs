﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.AccountDatabase.Delimiters.Mappers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.DelemiterSourceAccountDatabase.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер создания материнской базы разделителей
    /// </summary>
    internal class CreateDelimiterSourceAccountDatabaseProvider : ICreateDelimiterSourceAccountDatabaseProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly DelimiterSourceAccountDatabaseDtoValidator _validator;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger42 _logger;
        public CreateDelimiterSourceAccountDatabaseProvider(IUnitOfWork dbLayer,
            IServiceProvider serviceProvider,
            DelimiterSourceAccountDatabaseDtoValidator validator,
            ILogger42 logger)
        {
            dbLayer.CheckForNull();
            validator.CheckForNull();
            _dbLayer = dbLayer;
            _validator = validator;
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        /// <summary>
        /// Создать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabase">Модель материнской базы разделителей</param>
        public void Create(DelimiterSourceAccountDatabaseDto delimiterSourceAccountDatabase)
        {
            _validator.Validate(delimiterSourceAccountDatabase);
            var agentTransferBalanseRequest = delimiterSourceAccountDatabase.MapToDelimiterSourceAccountDatabase();
            _dbLayer.DelimiterSourceAccountDatabaseRepository.Insert(agentTransferBalanseRequest);
            _dbLayer.Save();
        }


        /// <summary>
        /// Создать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabase">Модель материнской базы разделителей</param>
        public Guid CreateMotherBase(MotherAccountDatabaseDto delimiterSourceAccountDatabase)
        {
            try
            {
                var template = _dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(
                                       tmpl => tmpl.ConfigurationId == delimiterSourceAccountDatabase.DbTemplateDelimiterCode);

                _logger.Trace(
                        $"Начало добавления материнской базы {delimiterSourceAccountDatabase.DatabaseCaption} по шаблону {template.Name} для аккаунта {delimiterSourceAccountDatabase.AccountId}");

                var accountDatabase = CreateDataBase(delimiterSourceAccountDatabase.AccountId,
                    delimiterSourceAccountDatabase.DatabaseCaption, template.Template);

                _logger.Trace(
                       $"База успешно создана с именем {accountDatabase.Caption} номером {accountDatabase.V82Name} идентификатором {accountDatabase.Id}");

                Create(new DelimiterSourceAccountDatabaseDto
                {
                    AccountDatabaseId = accountDatabase.Id,
                    DatabaseOnDelimitersPublicationAddress = delimiterSourceAccountDatabase.DatabaseOnDelimitersPublicationAddress,
                    DbTemplateDelimiterCode = delimiterSourceAccountDatabase.DbTemplateDelimiterCode,
                });
                return accountDatabase.Id;
            }
            catch (Exception ex)
            {
                var message = $"Во время создания материнской базы {delimiterSourceAccountDatabase.DatabaseCaption} для аккаунта {delimiterSourceAccountDatabase.AccountId} " +
                    $"произошла ошибка : {ex.Message}";
                _logger.Trace(message);
                throw new InvalidOperationException(message);
               
            }

            
        }
        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="databaseDomainModel">Параметры создания инф. базы.</param>
        /// <param name="template">Шаблон инф. базы</param>
        private Domain.DataModels.AccountDatabase CreateDataBase(Guid accountId, string caption, DbTemplate template)
        {
            var account = _dbLayer.AccountsRepository.GetById(accountId);
            var segmentHelper = _serviceProvider.GetRequiredService<ISegmentHelper>();
            var fileStorage = segmentHelper.GetFileStorageServer(account);
            var accountDatabase = _dbLayer.DatabasesRepository.CreateNewRecord(template, account, GetPlatformTypeForTemplate(template),
                caption, fileStorage, false);

            accountDatabase.StateEnum = DatabaseState.Ready;
            _dbLayer.DatabasesRepository.Update(accountDatabase);
            _dbLayer.Save();

            return accountDatabase;
        }

        /// <summary>
        /// Получить версию платформы у шаблона.
        /// </summary>	
        private PlatformType GetPlatformTypeForTemplate(DbTemplate dbTemplate)
        {
            return dbTemplate.PlatformType == PlatformType.Undefined ? PlatformType.V83 : dbTemplate.PlatformType;
        }
    }
}
