﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер для создания базы на разделителях(без регистрации в МС)
    /// </summary>
    internal class
        CreateDbOnDelimitersWithoutRegistrationInMsProvider(
            IUnitOfWork dbLayer,
            ICheckCreationDbOnDelimitersProvider checkCreationDbOnDelimitersProvider,
            IGetDataForCreationDbOnDelimitersProvider getDataForCreationDbOnDelimitersProvider,
            IHandlerException handlerException)
        : ICreateDbOnDelimitersWithoutRegistrationInMsProvider
    {
        /// <summary>
        /// Создать базу на разделителях(без регистрации в МС)
        /// </summary>
        /// <param name="model">Модель создания базы на разделителях</param>
        /// <returns>Id созданной базы</returns>
        public Guid Create(CreateDbOnDelimitersWithoutRegistrationInMsDto model)
        {
            checkCreationDbOnDelimitersProvider.CheckCreationWithoutRegistrationInMs(model.AccountId);
            try
            {
                var accountDatabaseId = CreateDatabase(model);
                CreateAccessesToDatabase(model.AccountId, accountDatabaseId, model.UsersToProvideAccess?.List);

                return accountDatabaseId;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка создания базы на разделителях]");
                throw;
            }

        }

        /// <summary>
        /// Создать информационную базу
        /// </summary>
        /// <param name="model">Модель создания базы на разделителях</param>
        /// <returns>Id созданной базы</returns>
        private Guid CreateDatabase(CreateDbOnDelimitersWithoutRegistrationInMsDto model)
        {
            var template = getDataForCreationDbOnDelimitersProvider.GetDbTemplate(model.ConfigurationId);
            var account = dbLayer.AccountsRepository.GetById(model.AccountId);
            var databaseCaption = string.IsNullOrEmpty(model.ApplicationName) ? template.DefaultCaption : model.ApplicationName;

            var accountDatabase =
                dbLayer.DatabasesRepository.CreateNewRecord(template, account,
                    getDataForCreationDbOnDelimitersProvider.GetPlatformTypeForTemplate(template),
                    databaseCaption);

            var dbOnDelimiters = new AccountDatabaseOnDelimiters
            {
                AccountDatabaseId = accountDatabase.Id,
                LoadTypeEnum = DelimiterLoadType.UserCreateNew,
                DbTemplateDelimiterCode = model.ConfigurationId
            };

            dbLayer.AccountDatabaseDelimitersRepository.Insert(dbOnDelimiters);
            dbLayer.Save();

            return accountDatabase.Id;
        }

        /// <summary>
        /// Создать доспупы для пользователей к информационной базе
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="usersToProvideAccess">Список Id пользователей
        /// которым нужно предоставить доступ к созданной базе</param>
        private void CreateAccessesToDatabase(Guid accountId, Guid accountDatabaseId, List<Guid>? usersToProvideAccess)
        {
            var usersWhoCanBeGrantedAccess = getDataForCreationDbOnDelimitersProvider
                .SelectUsersWhoCanBeGrantedAccess(usersToProvideAccess);

            if (!usersWhoCanBeGrantedAccess.Any())
                return;

            usersWhoCanBeGrantedAccess.ForEach(userId => CreateAccessToDatabase(accountDatabaseId, accountId, userId));
        }

        /// <summary>
        /// Создать доспуп для пользователя к информационной базе
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="userId">Id пользователя</param>
        private void CreateAccessToDatabase(Guid accountDatabaseId, Guid accountId, Guid userId)
        {
            var accessEntity = new AcDbAccess
            {
                AccountDatabaseID = accountDatabaseId,
                AccountID = accountId,
                LocalUserID = Guid.Empty,
                IsLock = false,
                AccountUserID = userId,
                ID = Guid.NewGuid()
            };

            dbLayer.AcDbAccessesRepository.Insert(accessEntity);
            dbLayer.Save();
        }
    }
}
