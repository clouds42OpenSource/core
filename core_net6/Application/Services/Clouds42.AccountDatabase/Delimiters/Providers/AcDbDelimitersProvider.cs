﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Delimiters.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер инф. баз на разделителях
    /// </summary>
    public class AcDbDelimitersProvider(
        IUnitOfWork dbLayer,
        ReleaseRegistrationAccountDatabaseDelimiterProvider releaseRegistrationAccountDatabaseDelimiterProvider,
        ICreateAccountDatabaseProvider createAccountDatabaseProvider,
        IAcDbOnDelimitersDataProvider acDbOnDelimitersDataProvider,
        IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        IGrantAcDbAccessForAccountUserProvider grantAcDbAccessForAccountUserProvider,
        IDeleteAcDbAccessForAccountUserProvider deleteAcDbAccessForAccountUserProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IAcDbDelimitersProvider
    {
        /// <summary>
        /// Получить номер информационной базы на разделителях по номеру материнской базы и номеру области.
        /// </summary>
        /// <param name="databaseSourceId">ID материнской базы</param>
        /// <param name="zone">Номер зоны</param>
        /// <returns>Номер информационной базы</returns>
        public Guid GetAccountDatabaseIdByZone(Guid databaseSourceId, int zone)
        {
            try
            {
                var accountDatabaseOnDelimiters =
                    dbLayer.AccountDatabaseDelimitersRepository.FirstOrDefault(acDb =>
                        acDb.DatabaseSourceId == databaseSourceId && acDb.Zone == zone &&
                        acDb.AccountDatabase.State != DatabaseState.DeletedToTomb.ToString() &&
                        acDb.AccountDatabase.State != DatabaseState.RestoringFromTomb.ToString() &&
                        acDb.AccountDatabase.State != DatabaseState.DelitingToTomb.ToString() &&
                        acDb.AccountDatabase.State != DatabaseState.DetachingToTomb.ToString() &&
                        acDb.AccountDatabase.State != DatabaseState.DeletedFromCloud.ToString()
                    );

                return accountDatabaseOnDelimiters?.AccountDatabaseId ?? Guid.Empty;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения номер инф. базы]");
                throw;
            }
        }

        /// <summary>
        /// Выставление статуса информационной базы и добавление информации о базе на разделителях
        /// </summary>
        /// <param name="model">Модель базы на разделителях</param>
        public void SetStatusAndInsertDataOnDelimiter(AccountDatabaseViaDelimiterDto model)
        {
            if (model == null)
                throw new InvalidOperationException(
                    "Модель для выставления статуса информационной базы и добавление информации о базе на разделителях");

            releaseRegistrationAccountDatabaseDelimiterProvider.ValidateModelAndRelease(model);
            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(model.AccountDatabaseId);

        }

        /// <summary>
        /// Создать базу на разделителях и выдать права доступа пользователю.
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="configurationCode">Код конфигурации</param>
        /// <returns>Результат создания</returns>
        public CreateCloud42ServiceModelDto CreateAccountDatabaseAndAddAccessForUser(Guid accountUserId,
            string configurationCode)
        {
            try
            {
                var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountUserId) ?? throw new NotFoundException($"По номеру '{accountUserId}' не найден пользователь.");
                var configuration =
                    dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(t =>
                        t.ConfigurationId == configurationCode) ?? throw new NotFoundException($"По ключу '{configurationCode}' не найдена конфигурация.");
                var result = createAccountDatabaseProvider.Process([
                    new()
                    {
                        IsChecked = true,
                        Publish = true,
                        DataBaseName = "Моя информационная база.",
                        UsersToGrantAccess = [accountUserId],
                        DbTemplateDelimiters = true,
                        TemplateId = configuration.TemplateId
                    }
                ], accountUser.AccountId);

                return result;
            }
            catch (Exception ex)
            {
                logger.Warn($"Ошибка создания базы на разделителях: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Метод предоставления доступа одному пользователю
        /// </summary>
        /// <param name="model">Модель предоставления доступа</param>
        /// <param name="database">Инф. база</param>
        public void AddAccessToDbOnDelimiters(AcDbAccessPostAddModelDto model, Domain.DataModels.AccountDatabase database)
        {
            CheckAccountUserAccess(model);

            logger.Info($"Начало отправки запроса на добавление прав пользователю {model.AccountUserID}");
            InsertAcDbAccess(model);

            var result = grantAcDbAccessForAccountUserProvider.GrantAcDbAccess(new ManageAcDbAccessParamsDto
            {
                ActionType = UpdateAcDbAccessActionType.Grant,
                AccountDatabaseId = database.Id,
                AccountUserId = model.AccountUserID
            });

            if (result.AccessState == AccountDatabaseAccessState.Error)
            {
                logger.Info($"Запрос на добавление прав пользователю '{model.AccountUserID}' завершился ошибкой {result.ErrorMessage}");
                var accessEntity = acDbOnDelimitersDataProvider.GetAcDbAccessOrThrowException(database.Id, model.AccountUserID);
                acDbOnDelimitersDataProvider.DeleteAcDbAccess(accessEntity);
                throw new InvalidOperationException(result.ErrorMessage);
            }

            logger.Info($"Запрос на добавление прав пользователю '{model.AccountUserID}' выполнен успешно");
        }

        /// <summary>
        /// Удаление доступа для одного пользователя
        /// </summary>
        /// <param name="database">Информационная база.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        public void DeleteAccessFromDelimiterDatabase(Domain.DataModels.AccountDatabase database, Guid accountUserId)
        {
            try
            {
                var accessEntity = acDbOnDelimitersDataProvider.GetAcDbAccessOrThrowException(database.Id, accountUserId);

                logger.Info($"Начало отправки запроса на удаление прав пользователю '{accountUserId}'");

                var result = deleteAcDbAccessForAccountUserProvider.DeleteAcDbAccess(new ManageAcDbAccessParamsDto
                {
                    ActionType = UpdateAcDbAccessActionType.Delete,
                    AccountDatabaseId = database.Id,
                    AccountUserId = accountUserId
                });

                if (result.AccessState == AccountDatabaseAccessState.Error)
                    acDbOnDelimitersDataProvider.DeleteAcDbAccessForError(accessEntity);

                logger.Info($"Запрос на удаление прав пользователю '{accountUserId}' выполнен успешно");
            }
            catch (Exception ex)
            {
                logger.Warn($"Ошибка удаления доступа пользователю {accountUserId} к инф. базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}. Описание ошибки: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Проверить доступ пользователя
        /// </summary>
        /// <param name="model">Модель предоставления доступа</param>
        private void CheckAccountUserAccess(AcDbAccessPostAddModelDto model)
        {
            var accessExist = dbLayer.AcDbAccessesRepository
                .Any(x =>
                    x.AccountDatabaseID == model.AccountDatabaseID &&
                    x.AccountID == model.AccountID &&
                    x.AccountUserID == model.AccountUserID);

            if (accessExist)
                throw new InvalidOperationException("Доступ с такими параметрами уже есть");
        }

        /// <summary>
        /// Вставить запись о доступе к инф. базе
        /// </summary>
        /// <param name="model">Модель предоставления доступа</param>
        private void InsertAcDbAccess(AcDbAccessPostAddModelDto model)
        {
            var accessEntity = CreateAcDbAccess(model);
           try
           {
               logger.Info($"Начало добавления данных в таблицу AcDbAccesses для пользователя '{model.AccountUserID}'");

               dbLayer.AcDbAccessesRepository.Insert(accessEntity);
               dbLayer.Save();
 
               logger.Info($"Данные успешно добавлены в таблицу AcDbAccesses для пользователя '{model.AccountUserID}'");
           }
           catch (Exception ex)
           {
               handlerException.Handle(ex, $"[Ошибка добавления досутпа к базе на разделителях]");
               throw new InvalidOperationException($"Ошибка предоставления доступа для пользователя. {ex.Message}");
           }

        }

        /// <summary>
        /// Создать модель доступа к инф. базе
        /// </summary>
        /// <param name="model">Модель предоставления доступа</param>
        /// <returns>Модель доступа к инф. базе</returns>
        private AcDbAccess CreateAcDbAccess(AcDbAccessPostAddModelDto model)
            => new()
            {
                AccountDatabaseID = model.AccountDatabaseID,
                AccountID = model.AccountID,
                LocalUserID = model.LocalUserID,
                AccountUserID = model.AccountUserID,
                ID = Guid.NewGuid()
            };
    }
}
