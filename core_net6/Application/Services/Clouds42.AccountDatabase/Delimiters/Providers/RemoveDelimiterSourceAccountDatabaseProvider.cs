﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер для удаления материнской базы разделителей
    /// </summary>
    internal class RemoveDelimiterSourceAccountDatabaseProvider : IRemoveDelimiterSourceAccountDatabaseProvider
    {
        private readonly IUnitOfWork _dbLayer;

        public RemoveDelimiterSourceAccountDatabaseProvider(IUnitOfWork dbLayer)
        {
            dbLayer.CheckForNull();
            _dbLayer = dbLayer;
        }

        /// <summary>
        /// Удалить материнскую базу разделителей
        /// </summary>
        /// <param name="accountDatabaseId">Id базы источника на разделителях</param>
        /// <param name="dbTemplateDelimiterCode">Код шаблона на разделителях</param>
        public void Remove(Guid accountDatabaseId, string dbTemplateDelimiterCode)
        {
            if (accountDatabaseId.IsNullOrEmpty())
                throw new ArgumentNullException(nameof(accountDatabaseId));

            dbTemplateDelimiterCode.CheckForNull();

            var delimiterSourceAccountDatabase =
                _dbLayer.DelimiterSourceAccountDatabaseRepository.FirstOrThrowException(x =>
                    x.AccountDatabaseId == accountDatabaseId && x.DbTemplateDelimiterCode == dbTemplateDelimiterCode);

            _dbLayer.DelimiterSourceAccountDatabaseRepository.Delete(delimiterSourceAccountDatabase);
            _dbLayer.Save();
        }
    }
}
