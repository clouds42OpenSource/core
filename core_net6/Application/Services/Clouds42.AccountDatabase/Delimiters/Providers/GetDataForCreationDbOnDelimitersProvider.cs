﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.AccountDatabase.Delimiters.Providers
{
    /// <summary>
    /// Провайдер получения данных для создания базы на разделителях
    /// </summary>
    public class GetDataForCreationDbOnDelimitersProvider(IUnitOfWork dbLayer)
        : IGetDataForCreationDbOnDelimitersProvider
    {
        /// <summary>
        /// Получить версию платформы у шаблона
        /// </summary>
        /// <param name="dbTemplate">Шаблон базы</param>
        /// <returns>Версия платформы</returns>
        public PlatformType GetPlatformTypeForTemplate(DbTemplate dbTemplate) =>
            dbTemplate.PlatformType == PlatformType.Undefined ? PlatformType.V83 : dbTemplate.PlatformType;

        /// <summary>
        /// Получить шаблон базы
        /// </summary>
        /// <param name="configurationId">Код конфигурации шаблона на разделителях</param>
        /// <returns>Шаблон базы</returns>
        public DbTemplate GetDbTemplate(string configurationId) =>
            dbLayer.DbTemplateDelimitersReferencesRepository
                .FirstOrDefault(td =>
                    td.ConfigurationId == configurationId)
                ?.Template ??
            throw new NotFoundException(
                $"Не удалось получить шаблон информационной базы по коду конфигурации {configurationId}");

        /// <summary>
        /// Выбрать пользователей которым можно предоставить доступ
        /// к информационной базе
        /// </summary>
        /// <param name="usersToProvideAccess">Список Id пользователей
        /// которым нужно предоставить доступ к созданной базе</param>
        /// <returns>Список Id пользователей
        /// которым можно предоставить доступ к созданной базе</returns>
        public List<Guid> SelectUsersWhoCanBeGrantedAccess(List<Guid>? usersToProvideAccess)
        {
            if (usersToProvideAccess == null || !usersToProvideAccess.Any())
                return [];

            return dbLayer.ResourceRepository
                .AsQueryableNoTracking()
                .Where(x => x.Subject.HasValue && usersToProvideAccess.Contains(x.Subject.Value) &&
                            (x.BillingServiceType.SystemServiceType == ResourceType.MyEntUser ||
                             x.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb))
                .Select(x => x.Subject!.Value)
                .Distinct()
                .ToList();
        }
    }
}
