﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Delimiters.Mappers
{
    /// <summary>
    /// Маппер моделей материнской базы разделителей
    /// </summary>
    public static class DelimiterSourceAccountDatabaseMapper
    {
        /// <summary>
        /// Выполнить маппинг модели 
        /// </summary>
        /// <param name="model">Модель материнской базы разделителей</param>
        /// <returns>Материнская база разделителей</returns>
        public static DelimiterSourceAccountDatabase MapToDelimiterSourceAccountDatabase(
            this DelimiterSourceAccountDatabaseDto model) =>
            new()
            {
                AccountDatabaseId = model.AccountDatabaseId,
                DbTemplateDelimiterCode = model.DbTemplateDelimiterCode,
                DatabaseOnDelimitersPublicationAddress = model.DatabaseOnDelimitersPublicationAddress,
            };

        /// <summary>
        /// Выполнить маппинг модели 
        /// </summary>
        /// <param name="model">Материнская база разделителей</param>
        /// <returns>Модель материнской базы разделителей</returns>
        public static DelimiterSourceAccountDatabaseDto MapToDelimiterSourceAccountDatabaseDto(
            this DelimiterSourceAccountDatabase model) =>
            new()
            {
                AccountDatabaseId = model.AccountDatabaseId,
                DbTemplateDelimiterCode = model.DbTemplateDelimiterCode,
                DatabaseOnDelimitersPublicationAddress = model.DatabaseOnDelimitersPublicationAddress,
            };

        /// <summary>
        /// Выполнить маппинг модели 
        /// </summary>
        /// <param name="model">Материнская база разделителей</param>
        /// <returns>Модель данных материнской базы разделителей</returns>
        public static DelimiterSourceAccountDatabaseDataDto MapToDelimiterSourceAccountDatabaseDataDto(
            this DelimiterSourceAccountDatabase model) =>
            new()
            {
                AccountDatabaseId = model.AccountDatabaseId,
                DbTemplateDelimiterCode = model.DbTemplateDelimiterCode,
                DatabaseOnDelimitersPublicationAddress = model.DatabaseOnDelimitersPublicationAddress,
                AccountDatabaseName = model.AccountDatabase.V82Name
            };
    }
}
