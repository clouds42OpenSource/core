﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using PagedList.Core;

namespace Clouds42.AccountDatabase.Delimiters.DbTemplateDelimitersReferences
{
    /// <summary>
    ///     Точка входа для управления базами на разделителях
    /// </summary>
    public class DbTemplateDelimitersReferencesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IDbTemplateDelimitersReferencesProvider dbTemplateDelimitersReferencesProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        ///     Получить пагинированный список баз на разделителях
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ManagerResult<IPagedList<IDbTemplateDelimiters>> GetPagedListOfDbTemplateDelimiters(PagedListFilter filter = null)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ViewDbTemplateDelimitersReferences, () => AccessProvider.ContextAccountId);
                var collection = dbTemplateDelimitersReferencesProvider.GetListOfDbTemplateDelimiters();

                if (filter == null)
                    filter = new PagedListFilter();

                var pagedList = dbTemplateDelimitersReferencesProvider.ToPagedList(collection, filter);

                return Ok(pagedList);
            }
            catch (Exception exception)
            {
                return PreconditionFailed<IPagedList<IDbTemplateDelimiters>>(exception.Message);
            }
        }

        /// <summary>
        ///     Поиск информационных баз для выпадающего списка
        /// </summary>
        /// <param name="searchString">слово поиска</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns></returns>
        public List<DemoAccountDatabaseInfoDto> FindAccountDatabase(string searchString, Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.ViewDbTemplateDelimitersReferences, () => AccessProvider.ContextAccountId);

            if (string.IsNullOrWhiteSpace(searchString))
                return [];

            var listAccountDatabases = DbLayer.DatabasesRepository.Where(database => database.AccountId == accountId &&
                                                                                       (database.DbNumber.ToString().StartsWith(searchString))).Take(20).ToList();

            var result = new List<DemoAccountDatabaseInfoDto>();
            foreach (var accountDatabase in listAccountDatabases)
            {
                result.Add(new DemoAccountDatabaseInfoDto
                {
                    DemoAccountDatabaseId = accountDatabase.Id,
                    DemoAccountDatabaseCaption = $"{accountDatabase.DbNumber} ( {accountDatabase.Caption} )"
                });
            }

            return result;
        }

        /// <summary>
        ///  Обновить версию конфигурации для шаблона баз на разделителях.
        /// </summary>
        /// <param name="configurationCode">Id базы на разделителях</param>
        /// <param name="releaseVersion">версия релиза конфигурации</param>
        /// <returns></returns>
        public ManagerResult UpdateConfigurationReleaseVersion(string configurationCode, string releaseVersion)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdateConfigurationReleaseVersions);
                dbTemplateDelimitersReferencesProvider.UpdateConfigurationReleaseVersion(configurationCode, releaseVersion);
                return Ok();
            }
            catch (Exception exception)
            {
                return PreconditionFailed<IDbTemplateDelimiters>(exception.Message);
            }
        }

        /// <summary>
        ///  Обновить минимальную версию конфигурации для шаблона баз на разделителях.
        /// </summary>
        /// <param name="configurationCode">Id базы на разделителях</param>
        /// <param name="minVersion">минимальная версия релиза конфигурации</param>
        /// <returns></returns>
        public ManagerResult UpdateConfigurationMinVersion(string configurationCode, string minVersion)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdateConfigurationReleaseVersions);
                dbTemplateDelimitersReferencesProvider.UpdateConfigurationMinVersion(configurationCode, minVersion);
                return Ok();
            }
            catch (Exception exception)
            {
                return PreconditionFailed<IDbTemplateDelimiters>(exception.Message);
            }
        }

    }
}
