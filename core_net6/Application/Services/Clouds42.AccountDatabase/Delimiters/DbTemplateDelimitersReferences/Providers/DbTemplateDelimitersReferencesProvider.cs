﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using PagedList.Core;

namespace Clouds42.AccountDatabase.Delimiters.DbTemplateDelimitersReferences.Providers
{
    /// <summary>
    ///     Класс управления справочником баз на разделителях, основная реализация интерфейса
    /// </summary>
    internal class DbTemplateDelimitersReferencesProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : IDbTemplateDelimitersReferencesProvider
    {
        /// <inheritdoc />
        /// <summary>
        ///     Получить коллекцию баз на разделителях
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IDbTemplateDelimiters> GetListOfDbTemplateDelimiters()
        {
            var listOfDbTemplateDelimiters =(IEnumerable<IDbTemplateDelimiters>)dbLayer.DbTemplateDelimitersReferencesRepository.GetAll();

            return listOfDbTemplateDelimiters;
        }

		/// <summary>
		///     Получить базу на разделителях
		/// </summary>
		/// <returns></returns>
		public IDbTemplateDelimiters GetDbTemplateDelimiters(string dbTemplateDelimitersId)
		{
			var dbTemplateDelimiters = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d=>d.ConfigurationId == dbTemplateDelimitersId);

			return dbTemplateDelimiters;
		}

		/// <inheritdoc />
		/// <summary>
		///     Добавить новую базу на разделителях
		/// </summary>
		/// <param name="newDbTemplateDelimiter">Обьект новой конфигурации</param>
		public void AddNewDbTemplateDelimiters(IDbTemplateDelimiters newDbTemplateDelimiter)
		{
		    var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(b =>
		        b.Name == newDbTemplateDelimiter.Name && b.ConfigurationId == newDbTemplateDelimiter.ConfigurationId);

            if (database != null)
                throw new InvalidOperationException("Найден дубликат");

            try
            {
                var newDatabase = new DbTemplateDelimiters
                {
                    Name = newDbTemplateDelimiter.Name,
                    ConfigurationId = newDbTemplateDelimiter.ConfigurationId,
                    TemplateId = newDbTemplateDelimiter.TemplateId,
                    ShortName = newDbTemplateDelimiter.ShortName,
                    DemoDatabaseOnDelimitersPublicationAddress = newDbTemplateDelimiter.DemoDatabaseOnDelimitersPublicationAddress,
                    ConfigurationReleaseVersion = newDbTemplateDelimiter.ConfigurationReleaseVersion,
                    MinReleaseVersion = newDbTemplateDelimiter.MinReleaseVersion
                };

                dbLayer.DbTemplateDelimitersReferencesRepository.Insert(newDatabase);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания новой базы в справочнике] с именем '{newDbTemplateDelimiter.Name}'");
                throw;
            }
	        

        }

		/// <inheritdoc />
		/// <summary>
		///     Удалить существующую базу на разделителях
		/// </summary>
		/// <param name="dbTemplateDelimitersId">Id базы на разделителях</param>
		public void DeleteDbTemplateDelimiters(string dbTemplateDelimitersId)
        {
            var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.ConfigurationId == dbTemplateDelimitersId);

            if (database == null)
                throw new InvalidOperationException("Не найден обьект в базе");
		    try
		    {
			    dbLayer.DbTemplateDelimitersReferencesRepository.Delete(database);
			    dbLayer.Save();
			    logger.Info($"Успешно удалена база из справочника с именем'{database.Name}'");
		    }
		    catch (Exception ex)
		    {
			    handlerException.Handle(ex,$"[Ошибка удаления базы в справочнике] с именем '{database.Name}'");
			    throw;
		    }

        }

		/// <inheritdoc />
		/// <summary>
		///     Обновить существующую базу на разделителях
		/// </summary>
		/// <param name="dbTemplateDelimitersToUpdate">Обновленная база на разделителях</param>
		public void EditExistingDbTemplateDelimiters(IDbTemplateDelimiters dbTemplateDelimitersToUpdate)
        {
            var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.ConfigurationId == dbTemplateDelimitersToUpdate.ConfigurationId);

            if (database == null)
                throw new InvalidOperationException("Для редактирования не найден обьект в базе");

		    try
		    {
			    database.Name = dbTemplateDelimitersToUpdate.Name;
			    database.TemplateId = dbTemplateDelimitersToUpdate.TemplateId;
			    database.ShortName = dbTemplateDelimitersToUpdate.ShortName;
		        database.DemoDatabaseOnDelimitersPublicationAddress =
		            dbTemplateDelimitersToUpdate.DemoDatabaseOnDelimitersPublicationAddress;

			    dbLayer.DbTemplateDelimitersReferencesRepository.Update(database);
			    dbLayer.Save();
			    logger.Trace($"Успешно отредактирована база в справочнике с именем'{dbTemplateDelimitersToUpdate.Name}'");
            }
		    catch (Exception ex)
		    {
			    handlerException.Handle(ex, $"[Ошибка редактирования базы в справочнике] с именем '{dbTemplateDelimitersToUpdate.Name}'");
			    throw;
            }


        }

		/// <inheritdoc />
		/// <summary>
		///     Метод перевода коллекции баз на разделителях в нумерованый список
		/// </summary>
		/// <param name="collection">Входящая коллекция с объектами
		/// которые имплементируют IConfigurations1C</param>
		/// <param name="filter">Параметры фильтрации</param>
		/// <returns></returns>
		public IPagedList<IDbTemplateDelimiters> ToPagedList(IEnumerable<IDbTemplateDelimiters> collection, PagedListFilter filter)
        {
			return collection.OrderBy(x => x.Name).AsQueryable().ToPagedList(filter.CurrentPage, filter.PageSize);
		}

        /// <summary>
        ///  Обновить версию конфигурации для шаблона баз на разделителях.
        /// </summary>
        /// <param name="dbTemplateDelimitersId">Id базы на разделителях</param>
        /// <param name="releaseVersion">версия релиза конфигурации</param>
        public void UpdateConfigurationReleaseVersion(string dbTemplateDelimitersId, string releaseVersion)
        {
            var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.ConfigurationId == dbTemplateDelimitersId);

            if (database == null)
                throw new InvalidOperationException("Для редактирования не найден обьект в базе.");


            if (string.IsNullOrEmpty(releaseVersion))
	            throw new InvalidOperationException("Версия релиза конфигурации не указана.");

            try
            {
                database.ConfigurationReleaseVersion = releaseVersion;

                dbLayer.DbTemplateDelimitersReferencesRepository.Update(database);
                dbLayer.Save();
                logger.Trace($"Успешно отредактирована база в справочнике с именем'{database.Name}'");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования базы в справочнике] с именем '{database.Name}'");
                throw;
            }

        }

        /// <summary>
        ///  Обновить минимальную версию конфигурации для шаблона баз на разделителях.
        /// </summary>
        /// <param name="dbTemplateDelimitersId">Id базы на разделителях</param>
        /// <param name="minVersion">версия релиза конфигурации</param>
        public void UpdateConfigurationMinVersion(string dbTemplateDelimitersId, string minVersion)
        {
	        var database = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.ConfigurationId == dbTemplateDelimitersId);

	        if (database == null)
		        throw new InvalidOperationException("Для редактирования не найден обьект в базе.");


	        if (string.IsNullOrEmpty(minVersion))
		        throw new InvalidOperationException("Минимальная версия релиза конфигурации не указана.");

		    try
		    {
			    database.MinReleaseVersion = minVersion;

			    dbLayer.DbTemplateDelimitersReferencesRepository.Update(database);
			    dbLayer.Save();
			    logger.Trace($"Успешно отредактирована минимальная версия конфигурации базы в справочнике с именем'{database.Name}'");
		    }
		    catch (Exception ex)
		    {
			    handlerException.Handle(ex, $"[Ошибка редактирования минимальной версия конфигурации базы в справочнике] с именем '{database.Name}'");
			    throw;
		    }


        }
    }
}
