﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.DbTemplates
{
    public class DbTemplateManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить список шаблонов.
        /// </summary>
        /// <returns></returns>
        public ManagerResult<List<IDbTemplate>> GetDbTemplatesList()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_View, () => AccessProvider.ContextAccountId);
                var result = DbLayer.DbTemplateRepository.GetAll().ToList().Cast<IDbTemplate>().ToList();
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения списка шаблонов]");
                return PreconditionFailed<List<IDbTemplate>>(ex.Message);
            }
        }
    }
}
