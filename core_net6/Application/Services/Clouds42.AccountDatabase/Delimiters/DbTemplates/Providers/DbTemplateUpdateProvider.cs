﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.DbTemplates.Providers
{
    /// <summary>
    /// Обработчик обновления шаблонов.
    /// </summary>
    internal class DbTemplateUpdateProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger) : IDbTemplateUpdateProvider
    {
        /// <summary>
        /// Обновление шаблонов
        /// </summary>
        public void UpdateTemplates()
        {
            var obnovlyatorTemplateUpdateFolderPath = Path.Combine(CloudConfigurationProvider.Enterprise1C.GetTemporaryStoragePath(), nameof(UpdateTemplates));
            var templateFolderPath = CloudConfigurationProvider.Template.GetTemplatesDirectory();
            var directories = new DirectoryInfo(obnovlyatorTemplateUpdateFolderPath).GetDirectories();

            foreach (var directory in directories)
            {
                try
                {
                    var template = dbLayer.DbTemplateRepository.FirstOrDefault(t => t.DefaultCaption == directory.Name.Replace("_", ":"))
                        ?? throw new InvalidOperationException("Не найден шаблон по имени дирректории");

                    var updatedTemplateFilePath = Path.Combine(directory.FullName, "1Cv8.1CD");
                    var templatePath = Path.Combine(templateFolderPath, template.Name + ".1CD");

                    File.Copy(updatedTemplateFilePath, templatePath, true);
                    logger.Info($"шаблон {template.Name} скопирован из временной папки в шару с шаблонами");
                }
                catch (Exception ex )
                {
                    logger.Warn(ex, $"Ошибка обновления шаблона в папке {directory.Name.Replace("_", ":")}");
                }
                
            }
        }
    }
}
