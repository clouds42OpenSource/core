﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.Domain.IDataModels;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Delimiters.DbTemplates.Providers
{
    /// <summary>
    /// Класс по созданию обработчика обновления шаблонов.
    /// </summary>
    internal class TemplateUpdateProcessorCreator(IServiceProvider serviceProvider)
    {
        /// <summary>
        /// Создать обработчик
        /// </summary>  
        public TemplateUpdateProcessor Create(IUpdateDatabaseDto database, IConfigurations1C configuration, Starter1CProvider.CanContinue canContinueAction)
        {
            var starter1C = CreateStarter1C(database);
            var starter1CManager = CreateStarter1CManager(starter1C, database, canContinueAction);

            return serviceProvider.GetRequiredService<TemplateUpdateProcessor>().InitializationParameters(database, configuration, starter1CManager);
        }

        /// <summary>
        /// Создать стартер 1С.
        /// </summary>        
        public IStarter1C CreateStarter1C(IUpdateDatabaseDto updateDatabase)
        {
            return serviceProvider.GetRequiredService<IStarter1C>().SetUpdateDatabase(updateDatabase);
        }

        /// <summary>
        /// Создать менеджер для стартера 1С.
        /// </summary>        
        public IStarter1CProvider CreateStarter1CManager(IStarter1C starter, 
            IUpdateDatabaseDto updateDatabase,
            Starter1CProvider.CanContinue canContinueAction)
        {
            return serviceProvider.GetRequiredService<IStarter1CProvider>()
                .SetStarter1C(starter)
                .SetUpdateDatabase(updateDatabase)
                .SetCanContinueAction(canContinueAction);
        }
    }
}