﻿using Clouds42.AccountDatabase._1C.Providers;
using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountDatabase.Delimiters.DbTemplates.Providers
{
    /// <summary>
    /// Процессор проведения автообновления инфомационной базы.
    /// </summary>
    internal class TemplateUpdateProcessor : IAutoApdateSupportResultDto
    {
        private IUpdateDatabaseDto _updateDatabase;
        private IConfigurations1C _configuration;
        private readonly IConnector1CProvider _connector1CProvider;
        private IStarter1CProvider _starter1CManager;
        private readonly ILogger42 _logger;
        private readonly IConfiguration _configurationSettings;
        public const string PROVIDER_1C_CONSOLE_APP_NAME = "Provider1C.ConsoleApp.exe";
        /// <summary>
        /// Выбор метода для пременения обновлений инфобазы 1С,
        /// <para>ключ <c>true</c> - означает что запустить обновление из текущего приложения, подключаясь к COM</para>
        /// <para>ключ <c>false</c> - означает что запустить обновление из консольного приложения Provider1C.ConsoleApp</para>
        /// </summary>
        private readonly Dictionary<bool, Action> _applyUpdateMethodChoice;


        /// <summary>
        /// Выбор метода для получения метаданных инфобазы 1С,
        /// <para>ключ <c>true</c> - означает что получить метаданные из текущего приложения, подключаясь к COM</para>
        /// <para>ключ <c>false</c> - означает что получить метаданные из консольного приложения Provider1C.ConsoleApp</para>
        /// </summary>
        private readonly Dictionary<bool, Func<ConnectorResultDto<MetadataResultDto>>> _getDatabaseMetadataMethodChoice;
        
        /// <summary>
        /// Версия информационной базы до обновления.
        /// </summary>
        public string OldVersion { get; private set; }

        /// <summary>
        /// Версия информационной базы после обновления.
        /// </summary>
        public string NewVersion { get; private set; }

        public TemplateUpdateProcessor(IConnector1CProvider connector1CProvider,
            ILogger42 logger, IConfiguration configurationSettings)
        {
            _connector1CProvider = connector1CProvider;
            _logger = logger;
            _configurationSettings = configurationSettings;
            _applyUpdateMethodChoice = new Dictionary<bool, Action>
            {
                {true, ApplyUpdatesFromHere},
                {false, ApplyUpdatesFromConsoleApp }
            };

            _getDatabaseMetadataMethodChoice = new Dictionary<bool, Func<ConnectorResultDto<MetadataResultDto>>>
            {
                {true, DatabaseGetMetadataFromHere},
                {false, DatabaseGetMetadataFromConsoleApp}
            };
        }

        /// <summary>
        /// Начать проведение автообновления информационной базы.
        /// </summary>
        public void Process()
        {
            try
            {
                InternalProcess();                
                ValidateResult();
            }
            catch (Exception ex)
            {
                _logger.Warn($"Ошибка обновления шаблона {_updateDatabase.UpdateDatabaseName} :: {ex.Message}");
                throw;
            }            
        }

        /// <summary>
        /// Провести инициализацию параметров
        /// </summary>
        public TemplateUpdateProcessor InitializationParameters(IUpdateDatabaseDto updateDatabase, IConfigurations1C configuration, IStarter1CProvider starter1CManager)
        {

            _updateDatabase = updateDatabase;
            _configuration = configuration;
            _starter1CManager = starter1CManager;
            return this;
        }

        private void ApplyUpdatesFromHere()
        {
            _connector1CProvider.ApplyUpdates(_updateDatabase);
        }

        private void ApplyUpdatesFromConsoleApp()
        {
            Connector1CViaConsoleApp.Execute(_updateDatabase,
                CloudConfigurationProvider.ConsoleApp1C.GetComConnectionTimeoutInSeconds(),
                CloudConfigurationProvider.ConsoleApp1C.GetComConnectionAttempts(),
                connector1C =>
                {
                    return connector1C.ApplyUpdates(
                        CloudConfigurationProvider.ConsoleApp1C.GetApplyUpdatesTimeoutInMinutes(),
                        CloudConfigurationProvider.ConsoleApp1C.GetApplyUpdatesAttempts());
                }, _configurationSettings[$"AttachProcess_{PROVIDER_1C_CONSOLE_APP_NAME}"]);
        }


        /// <summary>
        /// Применить полученные обновления.
        /// </summary>
        private void ApplyUpdate(string updateVersion)
        {
            try
            {
                AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(_updateDatabase, _updateDatabase.UpdateDatabaseName.Replace("_", "."), CloudConfigurationProvider.Enterprise1C.GetDnsTemporaryStorage());

                var applyUpdatesFromHere = _configuration.UseComConnectionForApplyUpdates;
                _applyUpdateMethodChoice[applyUpdatesFromHere]();

                AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(_updateDatabase, _updateDatabase.UpdateDatabaseName.Replace("_", "."), CloudConfigurationProvider.Enterprise1C.GetDnsTemporaryStorage());
            }
            catch (Exception ex)
            {
                var errorMessage = $"Не удалось принять обновления версии '{updateVersion}' автоматически. :: {ex}";                
                throw new InvalidOperationException(errorMessage);
            }
        }

        /// <summary>
        /// Валидировать результат выполнения обновлений.
        /// </summary>
        private void ValidateResult()
        {

            var connectResult = _connector1CProvider.GetMetadataInfo(_updateDatabase);

            if (connectResult.ConnectCode != ConnectCodeDto.Success)
            {
                
                NewVersion = null;
                throw new InvalidOperationException($"Шаблон {_updateDatabase.UpdateDatabaseName} не обновился, {connectResult.Message}.");                
            }

            if (connectResult.Result.Version != NewVersion)
            {
                var message = GetLog1CMessage();
                
                NewVersion = null;            
                throw new InvalidOperationException($"Шаблон {_updateDatabase.UpdateDatabaseName} не обновился. {message}");
            }
        }

        private ConnectorResultDto<MetadataResultDto> DatabaseGetMetadataFromHere()
        {
            return _connector1CProvider.GetMetadataInfo(_updateDatabase);
        }

        private ConnectorResultDto<MetadataResultDto> DatabaseGetMetadataFromConsoleApp()
        {
            return Connector1CViaConsoleApp.Execute(_updateDatabase,
                CloudConfigurationProvider.ConsoleApp1C.GetComConnectionTimeoutInSeconds(),
                CloudConfigurationProvider.ConsoleApp1C.GetComConnectionAttempts(),
                connector1C =>
                {
                    return connector1C.GetMetadataInfo(
                        CloudConfigurationProvider.ConsoleApp1C.GetRetrieveDatabaseMetadataTimeoutInMinutes(),
                        CloudConfigurationProvider.ConsoleApp1C.GetRetrieveDatabaseMetadataAttempts());
                }, _configurationSettings[$"AttachProcess_{PROVIDER_1C_CONSOLE_APP_NAME}"]);
        }



        /// <summary>
        /// Провести автообновление.
        /// </summary>
        private void InternalProcess()
        {

            var getMetadataFromHere = _configuration.UseComConnectionForApplyUpdates;
            _logger.Info($"{getMetadataFromHere}. Начало проведения автообновления");
            var connectResult = _getDatabaseMetadataMethodChoice[getMetadataFromHere]();

            AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(_updateDatabase, _updateDatabase.UpdateDatabaseName.Replace("_", "."), CloudConfigurationProvider.Enterprise1C.GetDnsTemporaryStorage());

            if (connectResult.ConnectCode != ConnectCodeDto.Success)
            {

                NewVersion = null;
                throw new InvalidOperationException($"Шаблон {_updateDatabase.UpdateDatabaseName} нельзя обновить, {connectResult.Message}.");
            }
            _logger.Info($"{connectResult.Message}. Начало применения обновления");
            if (_starter1CManager.TryUpdateToLastVersion(connectResult.Result.Version, _configuration,
                    ApplyUpdate,
                    out var targetVersion))
            {                
                OldVersion = connectResult.Result.Version;
                NewVersion = targetVersion;
            }
        }

        /// <summary>                                                                                                                                                                                       
        /// Получить сообщение из логов 1С                                                                                                                                                         
        /// </summary>                                                                                                                                                                             
        /// <returns>Сообщение из лога 1С</returns>                                                                                                                                                
        private string GetLog1CMessage()
        {
            var path = ConfigurationHelper.GetConfigurationValue("TaSLogsSharePath");

            var logFileName = NewVersion.Replace(".", "_");

            var pathToVersionLogFile = Path.Combine(path, logFileName + ".txt");

            if (!File.Exists(pathToVersionLogFile))
                return string.Empty;

            return File.ReadAllText(pathToVersionLogFile);
        }

    }
}
