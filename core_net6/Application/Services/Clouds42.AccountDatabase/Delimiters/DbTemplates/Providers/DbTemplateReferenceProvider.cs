﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delimiters.DbTemplates.Providers
{
    /// <summary>
    /// Класс справочными данными шаблонов.
    /// </summary>
    internal class DbTemplateReferenceProvider(IUnitOfWork dbLayer) : IDbTemplateReferenceProvider
    {
        /// <summary>
        /// Получить шаблон по Id
        /// </summary>
        /// <param name="templateId">Id шаблона</param>
        /// <returns>Шаблон</returns>
        public DbTemplate GetDbTemplateById(Guid? templateId)
            => dbLayer.DbTemplateRepository.FirstOrDefault(dbtamplate => dbtamplate.Id == templateId) ??
               throw new NotFoundException($"Не найден шаблон по Id {templateId}");
    }
}
