﻿using Clouds42.Common;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Delimiters.Utility
{
    /// <summary>
    /// Утилита для сортиовки записей материнских баз разделителей
    /// </summary>
    public static class DelimiterSourceAccountDatabaseSortingUtility
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateQueryable<DelimiterSourceAccountDatabase>> SortingActions = new()
        {
            { DelimiterSourceAccountDatabaseSortFieldNames.AccountDatabaseV82Name, SortByAccountDatabaseV82Name },
            { DelimiterSourceAccountDatabaseSortFieldNames.DbTemplateDelimiterCode, SortByDbTemplateDelimiterCode },
            { DelimiterSourceAccountDatabaseSortFieldNames.DatabaseOnDelimitersPublicationAddress, SortByDatabaseOnDelimitersPublicationAddress }
        };

        /// <summary>
        /// Сортитровать записи материнских баз разделителей по коду конфигурации базы 
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи материнских баз разделителей по полю DatabaseOnDelimitersPublicationAddress</returns>
        private static IOrderedQueryable<DelimiterSourceAccountDatabase> SortByDefault(IQueryable<DelimiterSourceAccountDatabase> records)
            => records.OrderBy(row => row.DbTemplateDelimiterCode);


        /// <summary>
        /// Сортитровать записи материнских баз разделителей по полю AccountDatabase.V82Name
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи логирования запуска баз и RDP по полю AccountDatabase.V82Name</returns>
        private static IOrderedQueryable<DelimiterSourceAccountDatabase> SortByAccountDatabaseV82Name(IQueryable<DelimiterSourceAccountDatabase> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.AccountDatabase.V82Name),
                SortType.Desc => records.OrderByDescending(row => row.AccountDatabase.V82Name),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать записи материнских баз разделителей  по полю DbTemplateDelimiterCode
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи логирования запуска баз и RDP по полю DbTemplateDelimiterCode</returns>
        private static IOrderedQueryable<DelimiterSourceAccountDatabase> SortByDbTemplateDelimiterCode(IQueryable<DelimiterSourceAccountDatabase> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.DbTemplateDelimiterCode),
                SortType.Desc => records.OrderByDescending(row => row.DbTemplateDelimiterCode),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать записи материнских баз разделителей  по полю DatabaseOnDelimitersPublicationAddress
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи логирования запуска баз и RDP по полю DatabaseOnDelimitersPublicationAddress</returns>
        private static IOrderedQueryable<DelimiterSourceAccountDatabase> SortByDatabaseOnDelimitersPublicationAddress(IQueryable<DelimiterSourceAccountDatabase> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.DatabaseOnDelimitersPublicationAddress),
                SortType.Desc => records.OrderByDescending(row => row.DatabaseOnDelimitersPublicationAddress),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Производит сортировку выбранных записей материнских баз разделителей
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи материнских баз разделителей</returns>
        public static IOrderedQueryable<DelimiterSourceAccountDatabase> MakeSorting(IQueryable<DelimiterSourceAccountDatabase> records, SortingDataDto sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();

            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate)
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }
    }
}