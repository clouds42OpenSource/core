﻿using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Create.Managers
{
    /// <summary>
    /// Менеджер для работы с файлами инф. базы
    /// </summary>
    public class AccountDatabaseFilesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountDatabaseParseZipFileProvider accountDatabaseParseZipFileProvider,
        IUploadedFileProvider uploadedFileProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить информацию из zip файла информационной базы.
        /// Список пользователей, информация о конфигурации, тип шаблона.
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        public ManagerResult<AccountDatabaseInfoDto> GetDatabaseInfoFromZip(Guid uploadedFileId)
        {
            try
            {
                return Ok(accountDatabaseParseZipFileProvider.GetDatabaseInfo(uploadedFileId));
            }
            catch (Exception ex)
            {
                var errorMessage = "Ошибка при распозновании XML файла из ZIP архива";
                if (!uploadedFileProvider.TryGetUploadedFileName(uploadedFileId, out var uploadedFileName))
                    errorMessage = $"{errorMessage} {uploadedFileName}";
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddInfoBase,
                    $"{errorMessage}. {ex.Message}");
                return PreconditionFailedError<AccountDatabaseInfoDto>(null, ex.Message);
            }
        }

        /// <summary>
        /// Проверка совместимости релиза шаблона и релиза из zip файла информационной базы
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Результат проверки</returns>
        public ManagerResult ValidateVersionXmlInZipFile(Guid uploadedFileId)
        {
            try
            {
                accountDatabaseParseZipFileProvider.ValidateVersionXmlInZipFile(uploadedFileId);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "Ошибка при валидировании релиза из ZIP архива";
                if (!uploadedFileProvider.TryGetUploadedFileName(uploadedFileId, out var uploadedFileName))
                    errorMessage = $"{errorMessage} {uploadedFileName}";
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddInfoBase,
                    $"{errorMessage}. {ex.Message}");
                return PreconditionFailedError(ex.Message);
            }
        }
    }
}
