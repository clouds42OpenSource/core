﻿using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Common.Http;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Create.Managers
{
    /// <summary>
    /// Менеджер загружаемых файлов
    /// </summary>
    public class UploadFilesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IUploadFilesProvider uploadFilesProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Инициировать процесс загрузки файла
        /// </summary>
        /// <returns>ID объекта загружаемого файла</returns>
        public ManagerResult<Guid> InitUploadProcess(InitUploadFileRequestDto initUploadFileRequestDc)
        {
            try
            {
                var result = uploadFilesProvider.InitUploadProcess(initUploadFileRequestDc);
                logger.Trace(
                    $"[{initUploadFileRequestDc.AccountId}]::Инициализация процесса загрузки файла {initUploadFileRequestDc.FileName} завершена успешно");
                return Ok(result);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка при инициализации загрузки файла]::При инициализации процесса загрузки файла {initUploadFileRequestDc.FileName} произошла ошибка.";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<Guid>(ex.Message);
            }
        }


        /// <summary>
        /// Загрузить часть файла информационной базы
        /// </summary>
        /// <param name="httpRequest">Контекст запроса на загрузку части файла</param>
        /// <returns>Ok - если часть файла загружена успешно</returns>
        public ManagerResult<bool> UploadChunkOfDbFile(UploadChunkDto uploadChunkDto)
        {
            var uploadedFileId = uploadChunkDto.FileId;
            try
            {
                var result = uploadFilesProvider.UploadChunkOfDbFile(uploadChunkDto);
                logger.Trace($"{uploadedFileId}::Часть файла загружена успешно");
                return Ok(result);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка загрузки чанка] {uploadedFileId}::При загрузке части файла произошла ошибка.";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        public ManagerResult<bool> UploadChunkOfDbFile(ThinHttpRequest httpRequest)
        {
            var uploadedFileId = httpRequest.GetUploadedFileId();
            try
            {
                var result = uploadFilesProvider.UploadChunkOfDbFile(httpRequest);
                logger.Trace($"[{uploadedFileId}]::Часть файла загружена успешно");
                return Ok(result);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка загрузки чанка] {uploadedFileId}::При загрузке части файла произошла ошибка.";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Проверить загруженный файл на соответствие требованиям
        /// </summary>
        /// <param name="httpRequest">Контекст запроса на проверку файла</param>
        /// <returns>Ok - если файл соответсвует требованиям</returns>
        public ManagerResult<string> CheckLoadFileValidity(ThinHttpRequest httpRequest)
        {
            var uploadedFileId = httpRequest.GetUploadedFileId();
            try
            {
                var result = uploadFilesProvider.CheckLoadFileValidity(httpRequest);

                if (!string.IsNullOrEmpty(result))
                {
                    logger.Trace($"Загруженный файл {uploadedFileId} не соответсвует требованиям. Причина: {result}");

                    return ValidationError<string>(result);
                }

                logger.Trace($"Загруженный файл {uploadedFileId} соответсвует требованиям");

                return Ok(result);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка при проверке файла на соответствиетребованиям] При проверке файла {uploadedFileId} на соответсвие требованиям возникла ошибка. Причина: {ex.GetFullInfo()}.";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<string>(ex.Message);
            }
        }

        /// <summary>
        /// Запустить процесс склейки частей в исходный файл
        /// </summary>
        /// <param name="httpRequest">Контекст запроса на запуск процесса склейки</param>
        /// <returns>Ok - если процесс запущен успешно</returns>
        public ManagerResult InitMergeChunksToInitialFileProcess(Guid uploadFileId, int countOfChunks)
        {
            try
            {
                uploadFilesProvider.InitMergeChunksToInitialFileProcess(uploadFileId, countOfChunks);

                logger.Trace($"Процесс склейки частей в исходный файл {uploadFileId} запущен успешно");

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка запуска процесса склейки частей в исходный файл] {uploadFileId}. Причина: {ex.GetFullInfo()}.";
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }
        public ManagerResult InitMergeChunksToInitialFileProcess(ThinHttpRequest httpRequest)
        {
            var uploadedFileId = httpRequest.GetUploadedFileId();
            try
            {
                uploadFilesProvider.InitMergeChunksToInitialFileProcess(httpRequest);

                logger.Trace($"Процесс склейки частей в исходный файл {uploadedFileId} запущен успешно");

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка при склейки частей] При запуске процесса склейки частей в исходный файл {uploadedFileId} возникла ошибка. Причина: {ex.GetFullInfo()}.";
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Проверить статус загрузки файла
        /// </summary>
        /// <param name="httpRequest">Контекст запроса на проверку статуса загрузки</param>
        /// <returns>Cтатус загрузки файла</returns>
        public ManagerResult<bool> CheckFileUploadStatus(Guid uploadFileId)
        {
            try
            {
                var result = uploadFilesProvider.CheckFileUploadStatus(uploadFileId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка проверки статуса загрузки файла] Файл: {uploadFileId} ");
                return PreconditionFailed<bool>(ex.Message);
            }
        }
        public ManagerResult<bool> CheckFileUploadStatus(ThinHttpRequest httpRequest)
        {
            var uploadedFileId = httpRequest.GetUploadedFileId();
            try
            {
                var result = uploadFilesProvider.CheckFileUploadStatus(httpRequest);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка проверки статуса загрузки файла] Файл: {uploadedFileId} ");
                return PreconditionFailed<bool>(ex.Message);
            }
        }
    }
}
