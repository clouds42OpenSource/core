﻿using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Create.Managers
{
    /// <summary>
    /// Менеджер создания инф. баз на основании бэкапа
    /// </summary>
    public class CreateAccountDatabaseFromBackupManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateAccountDatabaseFromBackupProvider createAccountDatabaseFromBackupProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать новую информационную базу на основе бэкапа
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        /// <param name="accountDatabaseName">Название инф. базы</param>
        /// <param name="parentProcessFlowKey">Ключ родительского рабочего процесса</param>
        public ManagerResult<IAccountDatabase> Create(Guid accountDatabaseBackupId, string accountDatabaseName, string parentProcessFlowKey)
        {
            var accountDatabaseBackup = accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(accountDatabaseBackupId);
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Restore, () => accountDatabaseBackup.AccountDatabase.AccountId);

            try
            {
                var newAccountDatabaseFromBackup =
                    createAccountDatabaseFromBackupProvider.Create(accountDatabaseBackup, accountDatabaseName,
                        parentProcessFlowKey);
                return Ok(newAccountDatabaseFromBackup);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка создания новой информационной базы на основе бэкапа]");
                return PreconditionFailed<IAccountDatabase>(ex.GetFullInfo(false));
            }
        }
    }
}
