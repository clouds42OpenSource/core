﻿using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.UploadedFiles.Helpers;

namespace Clouds42.AccountDatabase.Create.Helpers
{
    /// <summary>
    /// Хэлпер для загруженных файлов
    /// </summary>
    public class UploadedFileHelper(IUploadedFileProvider uploadedFileProvider)
    {
        /// <summary>
        /// Удалить загруженный файл
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        public void DeleteUploadedFile(Guid uploadedFileId)
        {
            var uploadedFile = uploadedFileProvider.GetUploadedFile(uploadedFileId);

            if (!File.Exists(uploadedFile.FullFilePath))
                return;

            FileUseChecker.WaitWhileFileUsing(uploadedFile.FullFilePath, waitTimeInSeconds: 120);
            File.Delete(uploadedFile.FullFilePath);
        }
    }
}
