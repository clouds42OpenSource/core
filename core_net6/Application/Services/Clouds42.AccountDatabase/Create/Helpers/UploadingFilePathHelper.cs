﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Create.Helpers
{
    /// <summary>
    /// Хэлпер формирования путей для загружаемых файлов
    /// </summary>
    public class UploadingFilePathHelper(IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        /// Получить путь для частей загружаемого файла
        /// </summary>
        /// <param name="uploadedFile">Загружаемый файл</param>
        /// <returns>Путь для частей загружаемого файла</returns>
        public string GetPathForUploadingFileChunks(UploadedFile uploadedFile)
        {
            var folderForChunks =
                CloudConfigurationProvider.AccountDatabase.UploadFiles.GetDirectoryNameForChunksOfFile();

            var filePath = Path.GetDirectoryName(uploadedFile.FullFilePath) ??
                           throw new InvalidOperationException(
                               $"Не удалось получить путь для файла {uploadedFile.FileName}");

            return Path.Combine(filePath, folderForChunks);
        }

        /// <summary>
        /// Получить полный путь для загруженного файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="fileName">Имя файла</param>
        /// <returns>Полный путь для загруженного файла</returns>
        public string GetFullUploadedFilePath(Guid accountId, string fileName)
        {
            var accountConfiguration = accountConfigurationDataProvider.GetAccountConfigurationBySegmentData(accountId);

            var uploadedFilePath = GetPathForUploadingFile(accountConfiguration.Account.IndexNumber,
                accountConfiguration.Segment.CoreHosting.UploadFilesPath);

            if (!Directory.Exists(uploadedFilePath))
            {
                Directory.CreateDirectory(uploadedFilePath);
            }

            return Path.Combine(uploadedFilePath, fileName);
        }

        /// <summary>
        /// Получить имя папки для аккаунта
        /// </summary>
        /// <param name="accountIndexNumber">Номер аккаунта</param>
        /// <returns>Имя папки для аккаунта</returns>
        private static string GetAccountFolderName(int accountIndexNumber)
            => $"company_{accountIndexNumber}";

        /// <summary>
        /// Получить путь для загружаемого файла
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        /// <param name="uploadFilesPath">Путь для загрузки файлов из сегмента</param>
        /// <returns>Путь для загружаемого файла</returns>
        private static string GetPathForUploadingFile(int accountNumber, string uploadFilesPath) =>
            Path.Combine(uploadFilesPath, GetAccountFolderName(accountNumber));
    }
}
