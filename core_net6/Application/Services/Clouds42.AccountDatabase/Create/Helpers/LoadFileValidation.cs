﻿using Clouds42.Common.Constants;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.UploadedFiles.Helpers;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountDatabase.Create.Helpers
{
    /// <summary>
    /// Класс для проверки валидности загружаемого файла
    /// </summary>
    public class LoadFileValidator(
        IUnitOfWork dbLayer,
        IConfiguration configuration,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Валидация файла
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Сообщение об ошибке</returns>
        public string Validation(string filePath, Guid accountId)
        {
            var fileExtension = Path.GetExtension(filePath);

            var locale = dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(configuration["DefaultLocale"] ?? ""));
            if (locale.Name != LocaleConst.Russia)
            {
                if (fileExtension == ".dt")
                {
                    return null;
                }

                File.Delete(filePath);
                return $"Не поддерживаемый формат {fileExtension}. Загрузите, пожалуйста, файл в формате .dt";
            }

            if (fileExtension == ".zip" || fileExtension == ".dt")
            {
                return fileExtension == ".dt" ? null : CheckNecessaryFilesInZip(filePath);
            }

            File.Delete(filePath);
            return $"Не поддерживаемый формат {fileExtension}. Загрузите, пожалуйста, файл в формате .dt или .zip";

        }

        /// <summary>
        /// Проверить наличие необходимых файлов в архиве
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        /// <returns>Сообщение о результате проверки</returns>
        private string CheckNecessaryFilesInZip(string filePath)
        {
            try
            {
                var dataObject = ExtractZipFileHelper.ExtractDumpInfoFromZipFile(filePath);
                if (dataObject?.DumpInfo == null)
                {
                    handlerException.HandleWarning($"По пути {filePath} не найден файл DumpInfo.xml","Ошибка поиска файла DumpInfo.xml" );
                    FileUseChecker.WaitWhileFileUsing(filePath, waitTimeInSeconds: 120);
                    File.Delete(filePath);
                    return "В загруженном файле отсутствует DumpInfo.xml, обратитесь в тех.поддержку";
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка разбора файла]. При разборе файла {filePath} произошла ошибка");
                FileUseChecker.WaitWhileFileUsing(filePath, waitTimeInSeconds: 120);
                File.Delete(filePath);
                return "Загруженный файл не корректный, обратитесь в тех.поддержку";
            }

            return null;
        }
    }
}
