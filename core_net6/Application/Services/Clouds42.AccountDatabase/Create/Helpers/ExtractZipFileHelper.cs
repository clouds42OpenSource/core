﻿using System.IO.Compression;
using System.Xml.Serialization;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Create.Helpers
{
    /// <summary>
    /// Хелпер для извлечения файла DumpInfo.xml из zip пакета и десериализация его в объект
    /// </summary>
    public static class ExtractZipFileHelper
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Метод извлечения файла DumpInfo.xml из zip пакета и десериализация его в объект
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        /// <returns></returns>
        public static DumpInfoMetadataDto ExtractDumpInfoFromZipFile(string filePath)
        {
            var directoryName = Path.GetDirectoryName(filePath) ??
                       throw new NotFoundException($"Не удалось получить имя директории из пути файла {filePath}");

            var extractFolderPath = Path.Combine(directoryName, Guid.NewGuid().ToString("N"));

            var sourceFileName = Path.Combine(extractFolderPath, "DumpInfo.xml");

            if (!Directory.Exists(extractFolderPath))
                Directory.CreateDirectory(extractFolderPath);

            var serializer = new XmlSerializer(typeof(DumpInfoMetadataDto));
            DumpInfoMetadataDto dataObject = null;
            using (var zip = ZipFile.Open(filePath, ZipArchiveMode.Read))
                foreach (var entry in zip.Entries)
                    if (entry.Name == "DumpInfo.xml")
                    {
                        entry.ExtractToFile(sourceFileName);
                        _logger.Debug($"Распаковали файл по пути {sourceFileName}");
                        var reader = new StreamReader(sourceFileName);
                        _logger.Debug($"Начало дессериализации файла {sourceFileName}");
                        dataObject = (DumpInfoMetadataDto)serializer.Deserialize(reader);
                        reader.Close();
                        _logger.Debug($"Удаление файла {sourceFileName}");
                        File.Delete(sourceFileName);
                        break;
                    }

            Directory.Delete(extractFolderPath);

            if (dataObject == null)
                throw new ValidateException("В архиве не найден файл 'DumpInfo.xml'");

            return dataObject;
        }

    }
}
