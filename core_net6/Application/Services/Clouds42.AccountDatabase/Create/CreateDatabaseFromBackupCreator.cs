﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;

namespace Clouds42.AccountDatabase.Create
{
    internal class CreateDatabaseFromBackupCreator(
        IServiceProvider serviceProvider,
        IRegisterDatabaseFromBackupProvider registerDatabaseFromBackupProvider)
        : AccountDatabaseCreatorBase(serviceProvider), ICreateDatabaseFromBackupCreator
    {
        protected override void RegisterDatabaseOnServer(CreateDatabaseResult createdDatabase)
        {
            registerDatabaseFromBackupProvider.RegisterDatabaseOnServer(new RegisterDatabaseModelDto{AccountDatabaseId = createdDatabase.AccountDatabase.Id});
        }
    }

}
