﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Create
{
    /// <summary>
    /// Провайдер по созданию информационных баз.
    /// </summary>
    internal class CreateAccountDatabaseProvider : ICreateAccountDatabaseProvider
    {
		/// <summary>
		/// пары объектов, тип создаваемого адаптера в зависимости от условия отбора элемента списка
		/// </summary>
		private readonly Dictionary<Func<InfoDatabaseDomainModelDto, bool>, Func<IDatabaseCreator>> _dictionary;

        private readonly IServiceProvider _serviceProvider;
        private readonly AccountDatabaseHelper _accountDatabaseHelper;
        private readonly IAccountDatabaseChangeStateProvider _accountDatabaseChangeStateProvider;
        private readonly IHandlerException _handlerException;

        public CreateAccountDatabaseProvider(IServiceProvider serviceProvider, 
            AccountDatabaseHelper accountDatabaseHelper, 
            IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
            IHandlerException handlerException)
        {
            _serviceProvider = serviceProvider;
            _accountDatabaseHelper = accountDatabaseHelper;
            _accountDatabaseChangeStateProvider = accountDatabaseChangeStateProvider;
            _handlerException = handlerException;
            _dictionary = new Dictionary<Func<InfoDatabaseDomainModelDto, bool>, Func<IDatabaseCreator>>
            {
                {
                    d => d is { DbTemplateDelimiters: false, IsChecked: true }, serviceProvider.GetRequiredService<CreateDatabaseFromTemplateCreator>
                },
                {
                    d => d.DbTemplateDelimiters && d is { IsChecked: true, UploadedFileId: null, DemoData: false }, serviceProvider.GetRequiredService<CreateDatabaseOnDelimitersCreator>
                },
	            {
		            d => d.DbTemplateDelimiters && d is { IsChecked: true, UploadedFileId: not null } and { DemoData: false, PasswordAdmin: null }, serviceProvider.GetRequiredService<CreateDatabaseOnDelimitersFromZipCreator>
	            },
                {
                    d => d.DbTemplateDelimiters && d is { IsChecked: true, UploadedFileId: not null } and { DemoData: false, PasswordAdmin: not null }, serviceProvider.GetRequiredService<CreateDatabaseOnDelimitersFromDtToZipCreator>
                },
                {
		            d => d.DbTemplateDelimiters && d is { IsChecked: true, DemoData: true }, serviceProvider.GetRequiredService<CreateDatabaseOnDelimitersCreator>
	            }
            };
        }

		/// <summary>
		/// Создать базы.
		/// </summary>        
		public CreateCloud42ServiceModelDto Process(List<InfoDatabaseDomainModelDto> databases, Guid accountId)
		{
			var result = new CreateCloud42ServiceModelDto();
		    var validateResult = _serviceProvider.GetRequiredService<DatabaseToCreateGeneralValidator>().Validate(databases, accountId);
		    if (!validateResult.Complete)
		        return validateResult;

            foreach (var item in _dictionary)
			{
				var list = databases.Where(item.Key).ToList();

				if (!list.Any())
					continue;
				
				result += item.Value()?.CreateDatabases(accountId, list);

				if (!result.Complete)
					break;
			}

			return result;

		}

        /// <summary>
        /// Создать информационную базу из бэкапа
        /// </summary>
        /// <param name="infoDatabaseModel">Модель инф. базы</param>
        public CreateCloud42ServiceModelDto CreateDbOnDelimitersFromBackup(InfoDatabaseDomainModelDto infoDatabaseModel)
        {
            try
            {
                var databases = new List<InfoDatabaseDomainModelDto>
                {
                    infoDatabaseModel
                };

                var result = Process(databases, infoDatabaseModel.AccountId);
            
                if (infoDatabaseModel.AccountDatabaseBackupId.IsNullOrEmpty() || !infoDatabaseModel.AccountDatabaseBackupId.HasValue)
                    throw new InvalidOperationException(
                        $"Для создаваемой базы {infoDatabaseModel.AccountId}:{infoDatabaseModel.AccountDatabaseBackupId} не указан ID бэкапа");
            
                var accountDatabase =
                    _accountDatabaseHelper.GetAccountDatabaseByBackupId(infoDatabaseModel.AccountDatabaseBackupId.Value);
            
                if (accountDatabase.StateEnum == DatabaseState.DetachedToTomb)
                    _accountDatabaseChangeStateProvider.ChangeState(accountDatabase.Id, DatabaseState.DeletedFromCloud);
            
            
                return result;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка создания инф. базы из бэкапа]. Ошибка создания инф. базы '{infoDatabaseModel.DataBaseName}' из бэкапа аккаунту '{infoDatabaseModel.AccountId}");
            
                return new CreateCloud42ServiceModelDto
                {
                    Complete = false,
                    Comment = $"Загрузка zip файла завершилось с ошибкой :: {ex.Message}"
                };
            }
            
        }
    }
}
