﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Create
{
    public class CreateDemoDatabaseOnDelimiterCreator(
        IServiceProvider serviceProvider,
        CheckRental1CActivateHelper checkRental1CActivateHelper,
        DatabaseStatusHelper databaseStatusHelper,
        IAcDbAccessProvider acDbAccessProvider,
        AccountDatabaseHelper accountDatabaseHelper)
        : AccountDatabaseCreatorBase(serviceProvider)
    {
        /// <summary>
        /// Создать информационные базы. 
        /// </summary>		
        public override CreateCloud42ServiceModelDto CreateDatabases(Guid accountId, List<InfoDatabaseDomainModelDto> databases)
        {
            var checkRental1CActivate = checkRental1CActivateHelper.CheckRental1CActivate(accountId);
            if (!checkRental1CActivate.Complete)
                return checkRental1CActivate;

            var databasesOnDelimitersWithDemoData = accountDatabaseHelper.GetListTemplatesWhichHaveDatabaseWithDemoData(databases, accountId);
            databases = databases.Where(w => databasesOnDelimitersWithDemoData.All(r => r.TemplateId != w.TemplateId)).ToList();
            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);            
            return CreateFromTemplateWithoutChecks(databases, account);
        }

        protected override void RegisterDatabaseOnServer(CreateDatabaseResult createdDatabase)
        {

            databaseStatusHelper.SetDbStatus(createdDatabase.AccountDatabase.V82Name, DatabaseState.Ready);

            var accountAdmin = DbLayer.AccountUsersRepository.FirstOrDefault(a =>
                a.AccountId == createdDatabase.AccountDatabase.AccountId &&
                a.AccountUserRoles.Any(r => r.AccountUserGroup == AccountUserGroup.AccountAdmin));

            if (accountAdmin == null)
                return;

            acDbAccessProvider.GetOrCreateAccess(new AcDbAccessPostAddModelDto
            {
                AccountDatabaseID = createdDatabase.AccountDatabase.Id,
                AccountID = createdDatabase.AccountDatabase.AccountId,
                LocalUserID = Guid.Empty,
                AccountUserID = accountAdmin.Id,
                SendNotification = true
            });
        }

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="platformType">Тип платформы.</param>
        /// <param name="caption">Название.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="uploadData">Информация о зип пакете.</param>
        /// <param name="isFile">Файловая база.</param>
        protected override Domain.DataModels.AccountDatabase CreateNewDataBase(DbTemplate template, PlatformType platformType,
            string caption,
            Guid accountId,
            IUploadDataDto uploadData,
            bool isFile = true,
            bool isDemo = false,
            bool hasAutoUpdate = false,
            bool hasSupport = false)
        {
            var account = DbLayer.AccountsRepository.GetById(accountId);

            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                var accountDatabase = DbLayer.DatabasesRepository.CreateNewRecord(template, account, platformType, caption);

                var dbTemplateDelimiter = DbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.TemplateId == template.Id);

                if (dbTemplateDelimiter == null)
                    throw new NotFoundException($"По шаблону {template.DefaultCaption}-> {template.Id} не найдено соответсвие шаблона разделителей.");

                var model = new AccountDatabaseOnDelimiters
                {
                    AccountDatabaseId = accountDatabase.Id,
                    LoadTypeEnum = DelimiterLoadType.UserCreateNew,
                    IsDemo = isDemo,
                    DbTemplateDelimiterCode = dbTemplateDelimiter.ConfigurationId
                };
                accountDatabase.PublishStateEnum = PublishState.Published;                    

                DbLayer.AccountDatabaseDelimitersRepository.Insert(model);
                DbLayer.DatabasesRepository.Update(accountDatabase);
                DbLayer.Save();
                transaction.Commit();

                return accountDatabase;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }

        }

    }
}
