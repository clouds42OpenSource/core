﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Internal;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Create
{
    /// <summary>
    /// Класс создания информационных баз
    /// </summary>
    public abstract class AccountDatabaseCreatorBase(IServiceProvider serviceProvider) : IDatabaseCreator
    {
        protected readonly ILogger42 Logger = serviceProvider.GetRequiredService<ILogger42>();
        private readonly IAccessProvider _accessProvider = serviceProvider.GetRequiredService<IAccessProvider>();
		protected readonly IUnitOfWork DbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly IResourcesService _resourcesService = serviceProvider.GetRequiredService<IResourcesService>();
        protected readonly IHandlerException HandlerException = serviceProvider.GetRequiredService<IHandlerException>();
        protected readonly IAccountConfigurationDataProvider AccountConfigurationDataProvider = serviceProvider.GetRequiredService<IAccountConfigurationDataProvider>();
        const string ClearTemplateName = "Чистый шаблон";

        /// <summary>
        /// Создать информационные базы. 
        /// </summary>		
        public virtual CreateCloud42ServiceModelDto CreateDatabases(Guid accountId, List<InfoDatabaseDomainModelDto> databases)
        {
            Logger.Debug($"Добавление демо данных если они выбраны в список баз для аккаунта {accountId}");
			databases = AddDemoDatabases(databases);

            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);

			Logger.Debug($"Проверка создания баз данных из шаблона для аккаунта {accountId}");
			var canCreate = CanCreateDatabases(account, databases);

            if (canCreate.Complete)
                return CreateFromTemplateWithoutChecks(databases, account);
				

			Logger.Debug($"Ошибка создания баз данных из шаблона для аккаунта {accountId}");
			return canCreate;
		}

        /// <summary>
        /// Выполнить регистрацию базы на сервере в отдельном потоке.
        /// </summary>
        protected abstract void RegisterDatabaseOnServer(CreateDatabaseResult createdDatabase);


        /// <summary>
        /// Метод добавление в список с выбранными базами баз с демо данными
        /// </summary>		
        protected virtual List<InfoDatabaseDomainModelDto> AddDemoDatabases(List<InfoDatabaseDomainModelDto> databases)
        {
            var demoDatabases = new List<InfoDatabaseDomainModelDto>();
            foreach (var infoDatabaseDomainModel in databases)
            {
                if (!infoDatabaseDomainModel.DemoData)
                    continue;

                var demoTemplate = DbLayer.DbTemplateRepository.GetFirst(f => f.Id == infoDatabaseDomainModel.TemplateId).DemoTemplate;
                demoDatabases.Add(new InfoDatabaseDomainModelDto
                {
                    TemplateId = demoTemplate.Id,
                    DataBaseName = infoDatabaseDomainModel.DataBaseName,
                    IsChecked = true,
                    DemoData = false,
                    Publish = infoDatabaseDomainModel.Publish,
                    IsFile = infoDatabaseDomainModel.IsFile,
                    UsersToGrantAccess = infoDatabaseDomainModel.UsersToGrantAccess
                });
            }

            databases = databases.Where(d => !d.DemoData).Union(demoDatabases).ToList();

            return databases;
        }

        /// <summary>
        /// Добавление ИБ, платежей и публикация баз
        /// </summary>
        protected CreateCloud42ServiceModelDto CreateFromTemplateWithoutChecks(List<InfoDatabaseDomainModelDto> databases, Account account)
		{
			try
			{
				Logger.Trace($"Начало добавления {databases.Count} баз данных у аккаунта ");
				var accountDatabases = InsertDatabasesRecords(databases, account);
                var createdDatabases = new Dictionary<Guid, bool>();
				foreach (var database in accountDatabases)
				{					
                    RegisterDatabaseOnServer(database);
                    createdDatabases[database.AccountDatabase.Id] = database.InfoDatabase.DemoData;
                }                               

                return new CreateCloud42ServiceModelDto
                {
                    Complete = true,
                    Ids = accountDatabases.Select(d => d.AccountDatabase.Id).ToList(),
                    CreatedDatabases = createdDatabases
                };

			}
			catch (Exception ex)
			{
                HandlerException.Handle(ex, $"[Ошибка добавление ИБ, платежей и публикации]. Аккаунт: {account.Id}");
				return new CreateCloud42ServiceModelDto
				{
					Complete = false,
					Comment = ex.Message
				};
			}
		}

        /// <summary>
        /// Создать объект AcDbSupport
        /// </summary>
        /// <param name="accountDatabase">База данных</param>
        /// <param name="template">Шаблон</param>
        /// <param name="hasAutoupdate">Включить автообновление или нет</param>
        /// <param name="hasSupport">Включить поддержка или нет</param>	
        private void CreateAcDbSupport(Domain.DataModels.AccountDatabase accountDatabase,
            DbTemplate template,
            bool hasAutoupdate ,
            bool hasSupport)
        {
            var configurationName = DbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().FirstOrDefault(x => x.DbTemplateId == template.Id).Configuration1CName;
            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = accountDatabase.Id,
                CurrentVersion = template.CurrentVersion,
                State = (int)SupportState.AutorizationSuccess,
                HasSupport = hasSupport,
                HasAutoUpdate = hasAutoupdate,
                Login = template.AdminLogin,
                Password = template.AdminPassword,
                ConfigurationName = configurationName,
            };
            
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();
            Logger.Info($"Создана запись обслуживания для базы {accountDatabase.Id}");
        }

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="platformType">Тип платформы.</param>
        /// <param name="caption">Название.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="uploadData">Информация о зип пакете.</param>
        /// <param name="isFile">Файловая база.</param>
        /// <param name="isDemo">Демо база.</param>
        /// <param name="hasAutoupdate">Включить автообновление или нет</param>
        /// <param name="hasSupport">Включить поддержка или нет</param>	 
        protected virtual Domain.DataModels.AccountDatabase CreateNewDataBase(DbTemplate template, PlatformType platformType,
            string caption, 
            Guid accountId, 
            IUploadDataDto uploadData,
            bool isFile = true,
            bool isDemo = false,
            bool hasAutoupdate = false,
            bool hasSupport = false)
        {
            var account = DbLayer.AccountsRepository.GetById(accountId);
            var segmentHelper = serviceProvider.GetRequiredService<ISegmentHelper>();
            var fileStorage = segmentHelper.GetFileStorageServer(account);
            var accountDatabase = DbLayer.DatabasesRepository.CreateNewRecord(template, account, platformType, caption, fileStorage, isFile);

            if (template.DefaultCaption != ClearTemplateName)
                CreateAcDbSupport(accountDatabase, template, hasAutoupdate, hasSupport );

            return accountDatabase;
        }

		/// <summary>
		///  Проверяет возможность создания бд 
		/// </summary>
        private CreateCloud42ServiceModelDto CheckForDatabasesCreationAvailability(List<InfoDatabaseDomainModelDto> databases, out decimal totalCost)
		{			
            
			totalCost = 0;					
            
            foreach (var infoDatabaseDomainModel in databases)
			{
				var template = DbLayer.DbTemplateRepository.FirstOrDefault(dbTemplate => dbTemplate.Id == infoDatabaseDomainModel.TemplateId);
                if (template != null)
                    continue;

                Logger.Debug($"Не найден шаблон {infoDatabaseDomainModel.TemplateId} для базы {infoDatabaseDomainModel.DataBaseName}");

                return new CreateCloud42ServiceModelDto
                {
                    Comment = $"Не найден шаблон {infoDatabaseDomainModel.TemplateId} для базы {infoDatabaseDomainModel.DataBaseName}",
                    Complete = false,
                    NeedMoney = totalCost
                };

            }

		    return new CreateCloud42ServiceModelDto
		    {
		        Comment = "Успех",
		        Complete = true,
		        NeedMoney = totalCost
		    };
        }

        /// <summary>
		/// Получить версию платформы у шаблона.
		/// </summary>	
		private PlatformType GetPlatformTypeForTemplate(DbTemplate dbTemplate)
        {
            return dbTemplate.PlatformType == PlatformType.Undefined ? PlatformType.V83 : dbTemplate.PlatformType;
        }

		/// <summary>
		/// Записываем записи информационных баз.
		/// </summary>	
		private List<CreateDatabaseResult> InsertDatabasesRecords(IReadOnlyCollection<InfoDatabaseDomainModelDto> databases, Account account)
        {
            using var transaction = DbLayer.SmartTransaction.Get();
            var createdDatabases = new List<CreateDatabaseResult>();
            try
            {									
                var resConfig = _resourcesService.GetResourceConfig(account.Id, Clouds42Service.MyEnterprise);

                if (resConfig == null)
                {
                    Logger.Trace($"Начало активации ресурса {Clouds42Service.MyEnterprise}");
                    var myEnterprise42CloudService = serviceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(account.Id);
                    myEnterprise42CloudService.ActivateService();
                }

                foreach (var infoDatabaseDomainModel in databases)
                {
                    var template =
                        DbLayer.DbTemplateRepository.GetFirst(
                            x => x.Id == infoDatabaseDomainModel.TemplateId);
                    
                    bool isFile = !AccountConfigurationDataProvider.NeedAutoCreateClusteredDb(account.Id);
                    
                    Logger.Trace($"Начало добавления {(infoDatabaseDomainModel.DemoData ? "демо" : "")} базы {infoDatabaseDomainModel.DataBaseName} по шаблону {template.Name} для аккаунта {account.Id}");
                    var database = CreateNewDataBase(template, GetPlatformTypeForTemplate(template),
                        infoDatabaseDomainModel.DataBaseName,
                        account.Id,
                        infoDatabaseDomainModel,
                        isFile,
                        infoDatabaseDomainModel.DemoData,
                        infoDatabaseDomainModel.HasAutoupdate,                   
                        infoDatabaseDomainModel.HasSupport);                        

                    createdDatabases.Add(new CreateDatabaseResult
                    {
                        AccountDatabase = database,
                        InfoDatabase = infoDatabaseDomainModel,
                        Template = template,
                    });
                    Logger.Trace($"База {infoDatabaseDomainModel.DataBaseName} - {database.Id} создана");
                }

                DbLayer.Save();
                transaction.Commit();
                return createdDatabases;
            }
            catch (Exception ex)
            {					
                Logger.Trace($"Ошибка добавления {databases.Count} баз данных у аккаунта {account.Id} и создания платежа: {ex.GetFullInfo()} \n ");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Проверяет возможность создания базы
        /// </summary>
        private CreateCloud42ServiceModelDto CanCreateDatabases(Account account, List<InfoDatabaseDomainModelDto> databases)
        {
            try
            {
                var dbDomainModelValidator = new InfoDatabaseDomainModelValidator();
                dbDomainModelValidator.CreateDbFromTemplateValidation(databases);
                dbDomainModelValidator.CheckCreatingAccountDatabaseOnDelimiter(databases, _accessProvider, account);

                var checkRental1CActivate = serviceProvider.GetRequiredService<CheckRental1CActivateHelper>().CheckRental1CActivate(account.Id);
                if (!checkRental1CActivate.Complete)
                    return checkRental1CActivate;

                var problemInstance = CheckForDatabasesCreationAvailability(databases, out _);
                return problemInstance;
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, $"[Ошибка проверки создания базы из шаблона] Account: {account.Id}");
                return new CreateCloud42ServiceModelDto
                {
                    Complete = false,
                    Comment = ex.Message
                };
            }
        }

	}
}
