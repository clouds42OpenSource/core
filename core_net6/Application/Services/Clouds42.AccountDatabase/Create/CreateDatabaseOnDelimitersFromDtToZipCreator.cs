﻿using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.Files;

namespace Clouds42.AccountDatabase.Create
{

    /// <summary>
    /// Класс создания баз на разделителях.
    /// </summary>
	internal class CreateDatabaseOnDelimitersFromDtToZipCreator(
        IServiceProvider serviceProvider,
        IRegisterDatabaseFromDtToZipProvider registerDatabaseFromZipProvider)
        : AccountDatabaseCreatorBase(serviceProvider)
    {
        protected override void RegisterDatabaseOnServer(CreateDatabaseResult createdDatabase)
        {
            if (!createdDatabase.InfoDatabase.UploadedFileId.HasValue)
                throw new InvalidOperationException("Не указан ID загруженного файла");

            registerDatabaseFromZipProvider.RegisterDatabaseOnServer(new RegisterDatabaseFromDTToZipModelDto
            {
                AccountDatabaseId = createdDatabase.AccountDatabase.Id,
                UploadedFileId = createdDatabase.InfoDatabase.UploadedFileId.Value,
                ListInfoAboutUserFromZipPackage = createdDatabase.InfoDatabase.ListInfoAboutUserFromZipPackage,
                LoginAdmin = createdDatabase.InfoDatabase.LoginAdmin,
                PasswordAdmin = createdDatabase.InfoDatabase.PasswordAdmin
            });
        }

        /// <summary>
		/// Метод добавление в список с выбранными базами баз с демо данными
		/// </summary>		
        protected override List<InfoDatabaseDomainModelDto> AddDemoDatabases(List<InfoDatabaseDomainModelDto> databases)
		{
			return databases;
		}

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="platformType">Тип платформы.</param>
        /// <param name="caption">Название.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="uploadData">Информация о зип пакете.</param>
        /// <param name="isFile">Файловая база.</param>
        /// <param name="hasAutoupdate">Включить автообновление или нет</param>
        /// <param name="hasSupport">Включить поддержка или нет</param>	    
        protected override Domain.DataModels.AccountDatabase CreateNewDataBase(DbTemplate template, PlatformType platformType,
	        string caption,
            Guid accountId,
            IUploadDataDto uploadData,
            bool isFile = true,
            bool isDemo = false,
            bool hasAutoupdate = false,
            bool hasSupport = false)
	    {
	        var account = DbLayer.AccountsRepository.GetById(accountId);

            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                var accountDatabase = DbLayer.DatabasesRepository.CreateNewRecord(template, account, platformType, caption);

                var dbTemplateDelimiter = DbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.TemplateId == template.Id);

                if (dbTemplateDelimiter == null)
                    throw new NotFoundException($"По шаблону {template.DefaultCaption}-> {template.Id} не найдено соответсвие шаблона разделителей.");
                var uploadedFile =
                    DbLayer.UploadedFileRepository.FirstOrDefault(f => f.Id == uploadData.UploadedFileId);
                if (uploadedFile == null && uploadData.UploadedFileId != null && uploadData.UploadedFileId != Guid.Empty)
                {
                    var uploadModel = new UploadedFile
                    {
                        AccountId = accountId,
                        Id = (Guid)uploadData.UploadedFileId,
                        FileName = "data_dump.zip",
                        Status = UploadedFileStatus.UploadSuccess,
                        UploadDateTime = DateTime.Now
                    };
                    DbLayer.UploadedFileRepository.Insert(uploadModel);
                    DbLayer.Save();
                }

                var model = new AccountDatabaseOnDelimiters
                {
                    AccountDatabaseId = accountDatabase.Id,
                    UploadedFileId = uploadData.UploadedFileId,
                    LoadTypeEnum = uploadData.UploadedFileId.HasValue ? DelimiterLoadType.UserUploadZip : DelimiterLoadType.UserCreateNew,
                    DbTemplateDelimiterCode = dbTemplateDelimiter.ConfigurationId
                };
                Logger.Info($"Подготовлена модель для записи в таблицу для информационной базы '{accountDatabase.Id}'");

                DbLayer.AccountDatabaseDelimitersRepository.Insert(model);

                DbLayer.Save();
                transaction.Commit();

                return accountDatabase;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }

        }

    }
}
