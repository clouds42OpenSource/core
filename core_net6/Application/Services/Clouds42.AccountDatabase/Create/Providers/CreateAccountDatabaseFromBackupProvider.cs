﻿using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.Enums.StateMachine;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;
using Clouds42.StateMachine.Contracts.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Create.Providers
{
    /// <summary>
    /// Провайдер создания инф. базы из бэкапа
    /// </summary>
    internal class CreateAccountDatabaseFromBackupProvider : ICreateAccountDatabaseFromBackupProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly IServiceProvider _serviceProvider;
        private readonly IAccountDatabaseDataProvider _accountDatabaseDataProvider;

        private readonly Dictionary<bool, Func<InfoDatabaseDomainModelDto, string, Guid>> _mapAcDbTypeToCreationMethod;

        public CreateAccountDatabaseFromBackupProvider(IUnitOfWork dbLayer,
            IServiceProvider serviceProvider,
            IAccountDatabaseDataProvider accountDatabaseDataProvider)
        {
            _dbLayer = dbLayer;
            _serviceProvider = serviceProvider;
            _accountDatabaseDataProvider = accountDatabaseDataProvider;

            _mapAcDbTypeToCreationMethod = new Dictionary<bool, Func<InfoDatabaseDomainModelDto, string, Guid>>
            {
                {true, CreateFileAccountDatabase},
                {false, CreateServerAccountDatabase}
            };
        }

        /// <summary>
        /// Создать новую информационную базу на основе бэкапа
        /// </summary>
        /// <param name="accountDatabaseBackup">Бэкап информационной базы</param>
        /// <param name="accountDatabaseName">Название инф. базы</param>
        /// <param name="parentProcessFlowKey">Ключ родительского рабочего процесса</param>
        /// <returns>ID новой базы</returns>
        public IAccountDatabase Create(AccountDatabaseBackup accountDatabaseBackup, string accountDatabaseName,
            string parentProcessFlowKey)
        {
            var infoDatabaseDomainModel = CreateInfoDatabaseDomainModel(accountDatabaseBackup, accountDatabaseName);
            return CreateAccountDatabase(accountDatabaseBackup, infoDatabaseDomainModel, parentProcessFlowKey);
        }

        /// <summary>
        /// Создать инф. базу
        /// </summary>
        /// <param name="accountDatabaseBackup">Бекап инф. базы</param>
        /// <param name="infoDatabaseDomainModel">Модель параметров создания инф. базы</param>
        /// <param name="parentProcessFlowKey">Ключ родительского рабочего процесса</param>
        /// <returns></returns>
        private IAccountDatabase CreateAccountDatabase(AccountDatabaseBackup accountDatabaseBackup,
            InfoDatabaseDomainModelDto infoDatabaseDomainModel,
            string parentProcessFlowKey)
        {
            var failedProcessFlow = GetFailedProcessFlow(infoDatabaseDomainModel, parentProcessFlowKey);
            if (failedProcessFlow != null)
                return RetryFailedProcessFlow(failedProcessFlow);

            if (!accountDatabaseBackup.AccountDatabase.IsFile.HasValue)
                throw new InvalidOperationException(
                    $"Для инф. базы {accountDatabaseBackup.AccountDatabase.V82Name} не указан тип (серверная/файловая)");

            var accountDatabaseId =
                _mapAcDbTypeToCreationMethod[accountDatabaseBackup.AccountDatabase.IsFile.Value](
                    infoDatabaseDomainModel, parentProcessFlowKey);

            var accountDatabase = _accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId)
                                  ?? throw new NotFoundException($"Инф. база по ID {accountDatabaseId} не найдена");

            return accountDatabase;
        }

        /// <summary>
        /// Создать модель параметров создания инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackup">Бэкап инф. базы</param>
        /// <param name="accountDatabaseName">Название инф. базы</param>
        /// <returns>Модель параметров создания инф. базы</returns>
        private InfoDatabaseDomainModelDto CreateInfoDatabaseDomainModel(AccountDatabaseBackup accountDatabaseBackup,
            string accountDatabaseName)
        {
            var accountId = accountDatabaseBackup.AccountDatabase.AccountId;
            return GetInfoDatabaseDomainModel(accountDatabaseBackup.AccountDatabase.DbTemplate.Id, accountDatabaseName,
                accountId, accountDatabaseBackup.AccountDatabase.IsFile == true);
        }

        /// <summary>
        /// Получить модель параметров создания инф. базы
        /// </summary>
        /// <param name="templateId">ID шаблона</param>
        /// <param name="databaseName">Название инф. базы</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isFile">Признак что база файловая</param>
        /// <returns>Модель параметров создания инф. базы</returns>
        private InfoDatabaseDomainModelDto GetInfoDatabaseDomainModel(Guid templateId, string databaseName, Guid accountId, bool isFile)
            => new()
            {
                TemplateId = templateId,
                DataBaseName = databaseName,
                DemoData = false,
                IsChecked = true,
                Publish = false,
                IsFile = isFile,
                AccountId = accountId
            };

        /// <summary>
        /// Создать файловую инф. базу
        /// </summary>
        /// <param name="infoDatabaseDomainModel">Модель параметров создания инф. базы</param>
        /// <param name="parentProcessFlowKey">Ключ родительского рабочего процесса</param>
        /// <returns>ID созданной базы</returns>
        private Guid CreateFileAccountDatabase(InfoDatabaseDomainModelDto infoDatabaseDomainModel,
            string parentProcessFlowKey)
        {
            var creationResult = _serviceProvider.GetRequiredService<ICreateFileAccountDatabaseFromBackupProcessFlow>().Run(
                new CreateAccountDatabaseFromBackupParamsDto
                {
                    AccountDatabaseCreationParams = infoDatabaseDomainModel,
                    ParentProcessFlowKey = parentProcessFlowKey
                });

            if (creationResult.Finish)
                return creationResult.ResultModel.CreatedAccountDatabaseId;

            throw new InvalidOperationException(
                $"При создании файловой инф. базы возникла ошибка. Причина: {creationResult.Message}");
        }

        /// <summary>
        /// Создать серверную инф. базу
        /// </summary>
        /// <param name="infoDatabaseDomainModel">Модель параметров создания инф. базы</param>
        /// <param name="parentProcessFlowKey">Ключ родительского рабочего процесса</param>
        /// <returns>ID созданной базы</returns>
        private Guid CreateServerAccountDatabase(InfoDatabaseDomainModelDto infoDatabaseDomainModel,
            string parentProcessFlowKey)
        {
            var creationResult = _serviceProvider.GetRequiredService<ICreateServerAccountDatabaseFromBackupProcessFlow>().Run(
                new CreateAccountDatabaseFromBackupParamsDto
                {
                    AccountDatabaseCreationParams = infoDatabaseDomainModel,
                    ParentProcessFlowKey = parentProcessFlowKey
                });

            if (creationResult.Finish)
                return creationResult.ResultModel;

            throw new InvalidOperationException(
                $"При создании серверной инф. базы возникла ошибка. Причина: {creationResult.Message}");
        }

        /// <summary>
        /// Получить рабочий процесс с ошибкой
        /// </summary>
        /// <param name="infoDatabaseDomainModel">Модель параметров создания инф. базы</param>
        /// <param name="parentProcessFlowKey">ID родительского рабочего процесса</param>
        /// <returns>Рабочий процесс с ошибкой</returns>
        private ProcessFlow GetFailedProcessFlow(InfoDatabaseDomainModelDto infoDatabaseDomainModel,
            string parentProcessFlowKey)
        {
            var processedObjectName =
                SynchronizeAccountDatabaseProcessFlowsHelper.GetProcessedObjectNameForCreateAccountDatabaseFromBackup(
                    infoDatabaseDomainModel,
                    parentProcessFlowKey);

            var failedProcessFlow =
                _dbLayer.ProcessFlowRepository.FirstOrDefault(flow =>
                    flow.ProcessedObjectName == processedObjectName &&
                    flow.Status == StateMachineComponentStatus.Error);

            return failedProcessFlow;
        }

        /// <summary>
        /// Перезапустить упавший рабочий процесс
        /// </summary>
        /// <param name="failedProcessFlow">Рабочий процесс с ошибкой</param>
        /// <returns>Инф. база</returns>
        private IAccountDatabase RetryFailedProcessFlow(ProcessFlow failedProcessFlow)
        {
            _serviceProvider.GetRequiredService<IProcessFlowRetryProcessor>().Retry(failedProcessFlow.Id);

            var processFlowParams =
                _dbLayer.ActionFlowRepository.FirstOrDefault(actionFlow =>
                    actionFlow.ProcessFlowId == failedProcessFlow.Id &&
                    actionFlow.StateRefKey == failedProcessFlow.State)
                ?? throw new NotFoundException($"Параметры рабочего процесса {failedProcessFlow.Id} не найдены");

            var accountDatabaseId = processFlowParams.ResultDataInJson.DeserializeFromJson<Guid>();

            if (accountDatabaseId.IsNullOrEmpty())
                throw new InvalidOperationException(
                    $"Невозможно определить ID инф. базы из результата выполнения рабочего процесса {failedProcessFlow.Id}");

            return _accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId)
                   ?? throw new NotFoundException($"Инф. база по ID {accountDatabaseId} не найдена");
        }
    }
}
