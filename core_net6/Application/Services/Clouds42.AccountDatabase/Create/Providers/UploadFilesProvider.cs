﻿using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Logger;
using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Create.Helpers;
using Clouds42.Common.Helpers;
using Clouds42.Common.Http;
using Clouds42.CoreWorker.JobWrappers.UploadFile;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.AccountDatabase.Create.Providers
{
    /// <summary>
    /// Провайдер загружаемых файлов
    /// </summary>
    public class UploadFilesProvider(
        UploadingFilePathHelper uploadingFilePathHelper,
        IUploadedFileProvider uploadedFileProvider,
        LoadFileValidator loadFileValidator,
        IMergeChunksToInitialFileJobWrapper mergeChunksToInitialFileJobWrapper,
        ILogger42 logger,
        IHandlerException handlerException)
        : IUploadFilesProvider
    {
        private static readonly object Locker = new();


        /// <summary>
        /// Инициировать процесс загрузки файла
        /// </summary>
        /// <param name="initUploadFileRequestDc">Модель запроса инициализации загрузки файла</param>
        /// <returns>ID объекта загружаемого файла</returns>
        public Guid InitUploadProcess(InitUploadFileRequestDto initUploadFileRequestDc)
        {
            try
            {
                var uploadedFile = uploadedFileProvider.CreateUploadedFile(initUploadFileRequestDc);

                logger.Trace($"Инициализация процесса загрузки файла {uploadedFile.FileName} " +
                             $"для аккаунта {initUploadFileRequestDc.AccountId} завершена успешно.");

                return uploadedFile.Id;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка инициализации процесса загрузки файла] файл: {initUploadFileRequestDc.FileName} " +
                    $"Аккаунт: {initUploadFileRequestDc.AccountId}");
                throw;
            }
        }

        #region загрузка файла
        /// <summary>
        /// Загрузить часть файла
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        /// <returns>true - если часть файла загружена успешно</returns>
        public bool UploadChunkOfDbFile(UploadChunkDto uploadChunkDto)
        {
            try
            {
                using var inputStream = uploadChunkDto.ChunkFile.OpenReadStream();
                var uploadedFile = uploadedFileProvider.GetUploadedFile(uploadChunkDto.FileId);

                var file = new UploadedFileItem
                {
                    FileName = uploadChunkDto.ChunkFile.FileName,
                    ContentLength = uploadChunkDto.ChunkFile.Length,
                    InputStream = inputStream,
                };

                return UploadChunkOfDbFile(file, uploadedFile);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка загрузки части файла]");
                throw;
            }
        }
        public bool UploadChunkOfDbFile(ThinHttpRequest httpRequest)
        {
            try
            {
                var uploadedFile = uploadedFileProvider.GetUploadedFile(httpRequest);

                var files = httpRequest.Files;

                return UploadChunkOfDbFile(files, uploadedFile);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка загрузки части файла]");
                throw;
            }
        }

        /// <summary>
        /// Загрузить часть файла
        /// </summary>
        /// <param name="chunkOfUploadedFile">Часть файла</param>
        /// <param name="uploadedFile">Загружаемый файл</param>
        /// <returns>true если часть файла загружена успешно</returns>
        private bool UploadChunkOfDbFile(UploadedFileItem chunkOfUploadedFile, UploadedFile uploadedFile)
        {
            var folderPath = uploadingFilePathHelper.GetPathForUploadingFileChunks(uploadedFile);

            if (chunkOfUploadedFile == null || chunkOfUploadedFile.ContentLength <= 0)
                return false;

            var fileName = Path.GetFileName(chunkOfUploadedFile.FileName);

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            var path = Path.Combine(folderPath, fileName);

            CreateChunkOfFileInUploadDirectory(chunkOfUploadedFile.InputStream, path);


            return true;
        }
        private bool UploadChunkOfDbFile(UploadedFilesCollection chunkOfUploadedFile, UploadedFile uploadedFile)
        {
            var folderPath = uploadingFilePathHelper.GetPathForUploadingFileChunks(uploadedFile);

            foreach (var fileDataContent in chunkOfUploadedFile)
            {
                if (fileDataContent == null || fileDataContent.ContentLength <= 0)
                    continue;

                var stream = fileDataContent.InputStream;
                var fileName = Path.GetFileName(fileDataContent.FileName);

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                var path = Path.Combine(folderPath, fileName);

                CreateChunkOfFileInUploadDirectory(stream, path);
            }

            return true;
        }
        #endregion

        /// <summary>
        /// Проверить загруженный файл на соответствие требованиям
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        /// <returns>Сообщение с результатом проверки</returns>
        public string CheckLoadFileValidity(ThinHttpRequest httpRequest)
        {
            try
            {
                var uploadedFile = uploadedFileProvider.GetUploadedFile(httpRequest);

                var result = loadFileValidator.Validation(uploadedFile.FullFilePath, uploadedFile.AccountId);
                ProcessFileValidationResult(uploadedFile.Id, result);

                return result;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка проверки загруженного файла на соответствие требованиям]");
                throw;
            }
        }

        #region склейка файла
        /// <summary>
        /// Запустить процесс склейки частей в исходный файл
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        public void InitMergeChunksToInitialFileProcess(Guid uploadFileId, int countOfChunks)
        {
            try
            {
                var uploadedFile = uploadedFileProvider.GetUploadedFile(uploadFileId);
                if (countOfChunks == 0 || uploadedFile == null) throw new InvalidOperationException("Количество чанков равно нулю"); 

                var pathForFileChunks = uploadingFilePathHelper.GetPathForUploadingFileChunks(uploadedFile);

                mergeChunksToInitialFileJobWrapper.Start(new MergeChunksToInitialFIleParamsDto
                {
                    UploadedFileId = uploadedFile.Id,
                    CountOfFileChunks = countOfChunks,
                    PathForFileChunks = pathForFileChunks
                });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка запуска процесса склейки частей в исходный файл]");
                throw;
            }
        }
        public void InitMergeChunksToInitialFileProcess(ThinHttpRequest httpRequest)
        {
            try
            {
                var uploadedFile = uploadedFileProvider.GetUploadedFile(httpRequest);
                var countOfFileParts = httpRequest.GetCountOfLoadedParts();

                var pathForFileChunks = uploadingFilePathHelper.GetPathForUploadingFileChunks(uploadedFile);

                mergeChunksToInitialFileJobWrapper.Start(new MergeChunksToInitialFIleParamsDto
                {
                    UploadedFileId = uploadedFile.Id,
                    CountOfFileChunks = countOfFileParts,
                    PathForFileChunks = pathForFileChunks
                });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка запуска процесса склейки частей в исходный файл]");
                throw;
            }
        }
        #endregion

        #region проверка загрузки
        /// <summary>
        /// Проверить статус загрузки файла
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        /// <returns>true - если файл загружен успешно</returns>
        public bool CheckFileUploadStatus(Guid uploadFileId)
        {
            try
            {
                var uploadedFile = uploadedFileProvider.GetUploadedFile(uploadFileId);

                if (uploadedFile.Status == UploadedFileStatus.UploadFailed)
                    throw new InvalidOperationException(
                        "Файл не загружен. Обратитесь пожалуйста в нашу <a href=\"js:;\" onclick=\"openSupportWindow()\">службу поддержки</a>.");

                return uploadedFile.Status == UploadedFileStatus.UploadSuccess;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка проверки статуса загрузки файла]");
                throw;
            }
        }
        public bool CheckFileUploadStatus(ThinHttpRequest httpRequest)
        {
            try
            {
                var uploadedFile = uploadedFileProvider.GetUploadedFile(httpRequest);

                if (uploadedFile.Status == UploadedFileStatus.UploadFailed)
                    throw new InvalidOperationException(
                        "Файл не загружен. Обратитесь пожалуйста в нашу <a href=\"js:;\" onclick=\"openSupportWindow()\">службу поддержки</a>.");

                return uploadedFile.Status == UploadedFileStatus.UploadSuccess;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка проверки статуса загрузки файла]");
                throw;
            }
        }
        #endregion

        /// <summary>
        /// Создать часть файла в директории для загрузки
        /// </summary>
        /// <param name="uploadPath">Путь для создания файла</param>
        /// <param name="stream">Поток для записи файла</param>
        private void CreateChunkOfFileInUploadDirectory(Stream stream, string uploadPath)
        {
            try
            {
                if (File.Exists(uploadPath))
                    File.Delete(uploadPath);

                lock (Locker)
                {
                    using var fileHandle = File.Create(uploadPath, (int)stream.Length);
                    byte[] bytesInStream = new byte[stream.Length];
                    stream.Read(bytesInStream, 0, bytesInStream.Length);
                    fileHandle.Write(bytesInStream, 0, bytesInStream.Length);
                }
                stream.Close();
                logger.Trace($"Часть файла по пути '{uploadPath}' сохранена успешно.");
            }
            catch (IOException ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка сохранения части файла] При сохранении части файла по пути '{uploadPath}' произошла ошибка");
                throw;
            }
        }

        /// <summary>
        /// Обработать результат валидации файла
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <param name="result">Результат валидации файла</param>
        private void ProcessFileValidationResult(Guid uploadedFileId, string result)
        {
            if (string.IsNullOrEmpty(result))
                return;

            logger.Warn($"Загруженный файл {uploadedFileId} удален. Причина: {result}");

            uploadedFileProvider.UpdateUploadedFileStatus(uploadedFileId, UploadedFileStatus.Deleted, result);
        }
    }
}
