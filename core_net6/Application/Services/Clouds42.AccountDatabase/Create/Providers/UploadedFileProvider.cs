﻿using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Logger;
using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Create.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.Common.Http;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Create.Providers
{
    /// <summary>
    /// Провайдер загруженных файлов
    /// </summary>
    public class UploadedFileProvider(
        IUnitOfWork dbLayer,
        UploadingFilePathHelper uploadFilePathHelper,
        ILogger42 logger,
        IHandlerException handlerException)
        : IUploadedFileProvider
    {
        /// <summary>
        /// Получить загруженный файл
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Загруженный файл</returns>
        public UploadedFile GetUploadedFile(Guid uploadedFileId)
            => dbLayer.UploadedFileRepository.FirstOrDefault(w => w.Id == uploadedFileId)
               ?? throw new NotFoundException($"Загружаемый файл по ID {uploadedFileId} не найден");
        public UploadedFile GetUploadedFile(ThinHttpRequest httpRequest)
        {
            var uploadedFileId = httpRequest.GetUploadedFileId();

            return GetUploadedFile(uploadedFileId);
        }

        /// <summary>
        /// Попытаться получить имя загруженного файла
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <param name="uploadedFileName">Имя загруженного файла</param>
        /// <returns>Имя загруженного файла</returns>
        public bool TryGetUploadedFileName(Guid uploadedFileId, out string uploadedFileName)
        {
            uploadedFileName = string.Empty;
            var uploadedFile = dbLayer.UploadedFileRepository.FirstOrDefault(file => file.Id == uploadedFileId);
            if (uploadedFile == null)
                return false;

            uploadedFileName = uploadedFile.FileName;
            return true;
        }


        /// <summary>
        /// Создать загруженный файл
        /// </summary>
        /// <param name="initUploadFileRequestDc">Модель запроса инициализации загрузки файла</param>
        /// <returns>загруженный файл</returns>
        public UploadedFile CreateUploadedFile(InitUploadFileRequestDto initUploadFileRequestDc)
        {
            var fullFilePath = GetFullFilePath(initUploadFileRequestDc.AccountId, initUploadFileRequestDc.FileName);

            var uploadedFile = new UploadedFile
            {
                Id = Guid.NewGuid(),
                AccountId = initUploadFileRequestDc.AccountId,
                FileName = initUploadFileRequestDc.FileName,
                FullFilePath = fullFilePath,
                Status = UploadedFileStatus.UploadingInProcess
            };

            dbLayer.UploadedFileRepository.Insert(uploadedFile);
            dbLayer.Save();

            logger.Trace($"Успешно создан объект загруженного файла. ID: {uploadedFile.Id} AccountId: {uploadedFile.AccountId}");

            return uploadedFile;
        }
        
        private string GetFullFilePath(Guid accountId, string fileName) =>  
            uploadFilePathHelper.GetFullUploadedFilePath(accountId, fileName);

        /// <summary>
        /// Обновить статус загруженного файла
        /// </summary>
        /// <param name="uploadedFileId"></param>
        /// <param name="status"></param>
        /// <param name="comment">Комментарий</param>
        public void UpdateUploadedFileStatus(Guid uploadedFileId, UploadedFileStatus status, string comment)
        {
           try
           {
               var uploadedFile = dbLayer.UploadedFileRepository.FirstOrDefault(w => w.Id == uploadedFileId);
               uploadedFile.Status = status;
               uploadedFile.Comment = comment;

               dbLayer.UploadedFileRepository.Update(uploadedFile);
               dbLayer.Save();

               logger.Trace(
                   $"Успешно обновлен объект загруженного файла. ID: {uploadedFile.Id} AccountId: {uploadedFile.AccountId}" +
                   $"Статус изменен на {uploadedFile.Status}. Комментарий: {comment}");
           }
           catch (Exception ex)
           {
               handlerException.Handle(ex, $"[Ошибка обновления загруженного файла] Файл: {uploadedFileId}.");
               throw;
           }

        }
    }
}
