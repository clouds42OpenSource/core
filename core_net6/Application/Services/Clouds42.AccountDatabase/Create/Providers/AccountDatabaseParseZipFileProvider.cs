﻿using System.IO.Compression;
using System.Xml;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Create.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Domain.Enums.Files;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Create.Providers
{
    /// <summary>
    /// Парсинг zip архива информационной базы
    /// </summary>
    public class AccountDatabaseParseZipFileProvider(
        IUnitOfWork dbLayer,
        UploadedFileHelper uploadedFileHelper,
        IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider,
        ILogger42 logger)
        : IAccountDatabaseParseZipFileProvider
    {
        /// <summary>
        /// Получить информацию об информационной базе из загруженного архива.
        /// </summary>
        /// <param name="uploadedFileId">Идентификатор загруженного архива.</param>
        /// <returns>Информация о базе.</returns>
        public AccountDatabaseInfoDto GetDatabaseInfo(Guid uploadedFileId)
        {
            var filePath = GetUploadFileFullPath(uploadedFileId);
            var dataObject = ExtractZipFileHelper.ExtractDumpInfoFromZipFile(filePath);
            var templateId = SearchTemplateIdByConfigName(dataObject);

            return new AccountDatabaseInfoDto
            {
                TemplateId = templateId,
                ConfigurationId = dataObject.DumpInfo.Configuration.Name,
                Users = ParseUsersXmlInZipFile(uploadedFileId),
                MyDatabasesServiceTypeId = myDatabasesServiceTypeDataProvider.GetServiceTypeIdByDbTemplateId(templateId)
            };
        }

        /// <summary>
        /// Поиск id шаблона по имени конфигурации полученного из файла DumpInfo.xml
        /// </summary>
        private Guid SearchTemplateIdByConfigName(DumpInfoMetadataDto dataObject)
        {
            var confName = dataObject.DumpInfo.Configuration.Name;

            var templateDelimiters = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(s => s.ShortName == confName) ??
                                     throw new NotFoundException($"Не найден шаблон  конфигурации по коду '{confName}'.");

            return templateDelimiters.TemplateId;
        }

        /// <summary>
        /// Получить данные о пользователях из ZIP файла загруженной базы
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        public List<UserAccountDatabaseZipDto> ParseUsersXmlInZipFile(Guid uploadedFileId)
        {
            try
            {
                logger.Info($"Получен файл на распаковку: {uploadedFileId}");

                var xml = GetUsersListXml(uploadedFileId);

                logger.Info($"Получен Users.xml: {xml}");

                var result = ParseXmlDocument(xml);

                logger.Info($"XML успешно был опознан: {result.ToJson()}");

                return result;
            }
            catch (Exception ex)
            {
                logger.Info($"Неожиданная ошибка: {ex.GetFullInfo()}");
                uploadedFileHelper.DeleteUploadedFile(uploadedFileId);
                throw;
            }
        }

        /// <summary>
        /// Проверка совместимости релиза шаблона и релиза из zip файла информационной базы
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        public void ValidateVersionXmlInZipFile(Guid uploadedFileId)
        {
            try
            {
                var filePath = GetUploadFileFullPath(uploadedFileId);

                var dataObject = ExtractZipFileHelper.ExtractDumpInfoFromZipFile(filePath);
                if (dataObject?.DumpInfo == null)
                {
                    logger.Warn("Файл DumpInfo.xml не найден");
                    throw new InvalidOperationException("Не найден файл (DumpInfo.xml) с данными о конфигурации");
                }

                ValidateVersion(dataObject.DumpInfo.Configuration.Name, dataObject.DumpInfo.Configuration.Version);
                logger.Info("XML успешно был опознан");
            }
            catch (Exception ex)
            {
                logger.Info($"Неожиданная ошибка: {ex.GetFullInfo()}");
                uploadedFileHelper.DeleteUploadedFile(uploadedFileId);
                throw;
            }
        }

        /// <summary>
        /// Получить путь к загруженному файлу
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Путь к загруженному файлу</returns>
        private string GetUploadFileFullPath(Guid uploadedFileId)
        {
            var uploadedFile = dbLayer.UploadedFileRepository.FirstOrDefault(w =>
                                   w.Id == uploadedFileId && w.Status == UploadedFileStatus.UploadSuccess) ??
                               throw new NotFoundException($"Загруженный файл по ID {uploadedFileId} не найден");

            return uploadedFile.FullFilePath;
        }

        /// <summary>
        /// Получить список пользователей xml из zip архива. 
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        private string GetUsersListXml(Guid uploadedFileId)
        {
            var filePath = GetUploadFileFullPath(uploadedFileId);

            var zipArchive = ZipFile.OpenRead(filePath);
            var archiveFile = zipArchive.Entries.FirstOrDefault(w => w.FullName.Contains("Users.xml"));
            if (archiveFile == null)
                throw new NotFoundException("Не найден файл (Users.xml) с пользователями базы");

            var stream = archiveFile.Open();
            var reader = new StreamReader(stream);
            var xmlDocument = reader.ReadToEnd();

            return xmlDocument;

        }

        /// <summary>
        /// Распарсить XML документ Users.xml
        /// </summary>
        /// <param name="xml">Содержание xml</param>
        private List<UserAccountDatabaseZipDto> ParseXmlDocument(string xml)
        {
            var users = new List<UserAccountDatabaseZipDto>();

            if (string.IsNullOrEmpty(xml))
                return users;

            var xmlDocument = xml.GetXmlDocument();
            var infoBaseUsers = xmlDocument[nameof(Data)]?.SelectNodes("*");
            if (infoBaseUsers == null || infoBaseUsers.Count == 0)
                return users;

            foreach (XmlNode infoBaseUser in infoBaseUsers)
            {
                var fullName = infoBaseUser["FullName"]?.InnerText;
                var id = infoBaseUser["UUID"]?.InnerText;
                var roles = new List<string>();

                var rolesNode = infoBaseUser["Roles"]?.SelectNodes("*");
                if (rolesNode != null)
                    foreach (XmlNode role in rolesNode)
                        roles.Add(role?.InnerText);

                if (!string.IsNullOrEmpty(fullName) && !string.IsNullOrEmpty(id))
                    users.Add(new UserAccountDatabaseZipDto
                    {
                        FullName = fullName,
                        Uid = Guid.Parse(id),
                        IsAdmin = roles.Contains("ПолныеПрава")
                    });
            }

            return users;
        }


        /// <summary>
        /// Проверка совместимости релиза шаблона и релиза из zip файла информационной базы
        /// </summary>
        /// <param name="confName">Короткое имя конфигурации</param>
        /// <param name="version">Версия загружаемой конфигурации</param>
        private void ValidateVersion(string confName, string version)
        {
            var templateDelimiters = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(s =>
                s.ShortName == confName);
            var versionComparer = new VersionComparer();

            if (templateDelimiters == null)
                throw new InvalidOperationException($"Конфигурация '{confName}' не поддерживается. " +
                                    "Для загрузки базы обратитесь, пожалуйста, в");

            var resultCompare = versionComparer.Compare(version, templateDelimiters.ConfigurationReleaseVersion);
            if (resultCompare == 0)
                return;

            if (resultCompare == 1)
                throw new InvalidOperationException($"Релиз конфигурации загружаемой базы - {version}. Система поддерживает загрузку базы не выше {templateDelimiters.ConfigurationReleaseVersion} релиза. " +
                        "Для дополнительной информации обратитесь, пожалуйста, в");

            resultCompare = versionComparer.Compare(version, templateDelimiters.MinReleaseVersion);
            if (resultCompare == -1)
                throw new InvalidOperationException($"Релиз конфигурации загружаемой базы устаревший ({version}). Система поддерживает загрузку от {templateDelimiters.MinReleaseVersion} и выше. " +
                       "Вы можете обновить релиз конфигурации базы самостоятельно и загрузить базу повторно или обратиться в");
        }
    }
}
