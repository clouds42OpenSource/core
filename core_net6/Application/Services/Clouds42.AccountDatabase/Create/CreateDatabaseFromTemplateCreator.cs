﻿using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromTemplate;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;

namespace Clouds42.AccountDatabase.Create
{
    /// <summary>
    /// Класс создания баз из шаблонов.
    /// </summary>
	internal class CreateDatabaseFromTemplateCreator(
        IServiceProvider serviceProvider,
        CreateDatabaseFromTemplateJobWrapper createDatabaseFromTemplateJobWrapper)
        : AccountDatabaseCreatorBase(serviceProvider)
    {
        /// <summary>
        /// Зарегистрировать инф. базу
        /// </summary>
        /// <param name="createdDatabase">Модель созданной инф. базы</param>
        protected override void RegisterDatabaseOnServer(CreateDatabaseResult createdDatabase)
        {
            createDatabaseFromTemplateJobWrapper.Start(new RegisterFileDatabaseFromTemplateModelDto
            {
                AccountDatabaseId = createdDatabase.AccountDatabase.Id,
                NeedPublish = createdDatabase.InfoDatabase?.Publish ?? false,
                UsersIdsForAddAccess = createdDatabase.InfoDatabase?.UsersToGrantAccess ?? []
            });
        }
    }
}
