﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Validators;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.AccountUsers.ServiceManagerConnector.Commands;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices.Contracts;
using Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromDt;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Create
{
    /// <summary>
    /// Создатель информационной базы из Dt
    /// </summary>
    internal class CreateDatabaseFromDtCreator(
        IServiceProvider serviceProvider,
        IUnitOfWork dbLayer,
        IResourcesService resourcesService,
        CreateDatabaseFromDtJobWrapper createDatabaseFromDtJobWrapper,
        IHandlerException handlerException,
        ICloud42ServiceHelper cloud42ServiceHelper,
        ICheckAccountDataProvider checkAccountProvider,
        CheckRental1CActivateHelper checkRental1CActivateHelper,
        CreateFromZipNewDelimiterCommand createFromZipNewDelimiterCommand,
        ILogger42 logger,
        DatabaseStatusHelper databaseStatusHelper,
        IAcDbAccessProvider acDbAccessProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : ICreateDatabaseFromDtCreator
    {
        /// <summary>
        /// Создать инф. базу из DT файла
        /// </summary>
        /// <param name="accountId">Id аккаунта </param>
        /// <param name="createAcDbFromDtDto">модель данных</param>
        /// <returns>Результат создания</returns>
        public CreateCloud42ServiceModelDto CreateFromDt(Guid accountId, CreateAcDbFromDtDto createAcDbFromDtDto)
        {
            var account = dbLayer.AccountsRepository.GetById(accountId);

            var validateResult = CheckAbilityToCreateDatabase(createAcDbFromDtDto, account);
            if (!validateResult.Complete)
                return validateResult;

            try
            {
                ActivateRent1CIfNeeded(accountId);

                var template = dbLayer.DbTemplateRepository.FirstOrDefault(t => t.Name == "db_1c_my_shablon");

                logger.Info($"При запросе на создание базы пришло название конфигурации {createAcDbFromDtDto.ConfigurationName}");
                bool isFile = !accountConfigurationDataProvider.NeedAutoCreateClusteredDb(accountId);
                
                var accountDatabase = CreateNewDataBase(template, createAcDbFromDtDto.DbCaption, account, isFile);
                

                logger.Info($"Тип загружаемого файла {createAcDbFromDtDto.TypeFile}");

                if (string.IsNullOrEmpty(createAcDbFromDtDto.ConfigurationName))
                {
                    logger.Info($"Загружаемый файл dt так как конфигурация не найдена, создаем задачу на загрузку в чистый шаблон {accountDatabase.Id}");
                    createDatabaseFromDtJobWrapper.Start(new RegisterDatabaseFromUploadFileModelDto
                    {
                        UploadedFileId = createAcDbFromDtDto.UploadedFileId,
                        AccountDatabaseId = accountDatabase.Id,
                        UsersIdListForAddAccess = createAcDbFromDtDto.UsersIdForAddAccess ?? []
                    });
                    return CreateResultObj(true, databaseId: accountDatabase.Id);
                }

                var configuration = dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                  .AsQueryableNoTracking()
                  .Include(t => t.DbTemplate)
                  .Include(c => c.Configuration1C)
                  .ThenInclude(x => x.ConfigurationNameVariations)
                  .FirstOrDefault(d => d.Configuration1C.ConfigurationNameVariations.FirstOrDefault(r => r.VariationName == createAcDbFromDtDto.ConfigurationName) != null  && d.DbTemplate.Remark != "demo");

                logger.Info($"Найдена конфигурация первым способом {configuration?.Configuration1CName}");

                if (configuration == null)
                {
                    var nametemplate = !createAcDbFromDtDto.ConfigurationName.Contains(",")
                        ? createAcDbFromDtDto.ConfigurationName
                        : createAcDbFromDtDto.ConfigurationName.Remove(createAcDbFromDtDto.ConfigurationName.LastIndexOf(",") + 1);
                    configuration = dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                        .AsQueryableNoTracking()
                        .Include(t => t.DbTemplate)
                        .Include(c => c.Configuration1C)
                        .FirstOrDefault(d => d.Configuration1CName.Contains(nametemplate)
                     && d.DbTemplate.Remark != "demo");

                    logger.Info($"По данной конфигурации {createAcDbFromDtDto.ConfigurationName}, после обработки вторым способом {nametemplate} найден шаблон {configuration?.DbTemplate.Name}");
                }

                if (configuration != null)
                {
                    logger.Info($"Создали запись в сапорт с данными: Login {createAcDbFromDtDto.Login} " +
                    $"Password {createAcDbFromDtDto.Password} ConfigurationName {configuration.Configuration1CName} CurrentVersion {createAcDbFromDtDto.CurrentVersion}");
                    var acDbSupport = new AcDbSupport
                    {
                        AccountDatabasesID = accountDatabase.Id,
                        Password = createAcDbFromDtDto.Password,
                        Login = createAcDbFromDtDto.Login,
                        ConfigurationName = configuration.Configuration1CName,
                        State = createAcDbFromDtDto.Login is not null && createAcDbFromDtDto.Password is not null
                            ? (int)SupportState.AutorizationSuccess
                            : (int)SupportState.NotAutorized,
                        CurrentVersion = createAcDbFromDtDto.CurrentVersion,
                        HasSupport = createAcDbFromDtDto.HasSupport,
                        HasAutoUpdate = createAcDbFromDtDto.HasAutoupdate
                    };
                    
                    dbLayer.AcDbSupportRepository.Insert(acDbSupport);
                    dbLayer.Save();
                }

                if (createAcDbFromDtDto.TypeFile == UploadedFileEnumDto.ZipFile)
                {
                    var templateDel = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(t => t.ConfigurationId == createAcDbFromDtDto.ConfigurationId);

                    logger.Info($"Загружаемый файл zip,переданная конфигурация {createAcDbFromDtDto.ConfigurationName}," +
                        $" найденный шаблон {templateDel?.Name}, отправляем запрос в мс для конвертации базы {accountDatabase.Id}");
                    var result = createFromZipNewDelimiterCommand.Execute(new CreateDatabaseFromZipInMsParamsDto
                    {
                        AccountDatabaseCaption = createAcDbFromDtDto.DbCaption,
                        AccountDatabaseId = accountDatabase.Id,
                        AccountId = account.Id,
                        UploadedFileId = createAcDbFromDtDto.UploadedFileId,
                        TemplateId = templateDel != null? templateDel.TemplateId: template.Id,
                        ExtensionsList = [],
                        UserFromZipPackage = []
                    });
                    if (!string.IsNullOrEmpty(result))
                    {
                        handlerException.Handle(new InvalidOperationException("result"),
                            $"[Ошибка загрузки zip для конвертации в DT] Загруженный файл '{createAcDbFromDtDto.UploadedFileId}', ошибка {result}.");
                        return CreateResultObj(false, result);
                    }
                    createAcDbFromDtDto.UsersIdForAddAccess.ForEach(userId =>
                    {
                        AddAccess(userId, accountDatabase.AccountId, accountDatabase.Id);
                    });
                    return CreateResultObj(true, databaseId: accountDatabase.Id);
                }

                logger.Info($"Загружаемый файл dt для конфигурации {configuration?.DbTemplate.DefaultCaption}, создаем задачу на загрузку в чистый шаблон {accountDatabase.Id}");
                createDatabaseFromDtJobWrapper.Start(new RegisterDatabaseFromUploadFileModelDto
                {
                    UploadedFileId = createAcDbFromDtDto.UploadedFileId,
                    AccountDatabaseId = accountDatabase.Id,
                    UsersIdListForAddAccess = createAcDbFromDtDto.UsersIdForAddAccess ?? [],
                    TemplateId = configuration?.DbTemplateId
                });

                dbLayer.Save();

                return CreateResultObj(true, databaseId: accountDatabase.Id);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания ИБ из DT] Загруженный файл '{createAcDbFromDtDto.UploadedFileId}'.");
                return CreateResultObj(false, ex.Message);
            }
        }

        /// <summary>
        ///     Метод добавления доступа к новосозданной базе 
        /// администратору аккаунта
        /// </summary>
        /// <param name="accountAdminId">Идентификатор админ. аккаунта</param>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="createdDatabaseId">Идентификатор базы</param>
        private void AddAccess(Guid accountAdminId, Guid accountId, Guid createdDatabaseId)
        {
            if (accountAdminId == Guid.Empty)
                return;

            logger.Debug($"Добавление доступа к базе {createdDatabaseId} для админа {accountAdminId}");

            acDbAccessProvider.GetOrCreateAccess(new AcDbAccessPostAddModelDto
            {
                AccountDatabaseID = createdDatabaseId,
                AccountID = accountId,
                LocalUserID = Guid.Empty,
                AccountUserID = accountAdminId,
                SendNotification = true
            });
        }

        /// <summary>
        /// Загрузить инф. базу из DT файла после конвертации
        /// </summary>
        /// <param name="createAcDbFromDtDto">модель данных</param>
        /// <returns>Результат создания</returns>
        public CreateCloud42ServiceModelDto LoadFromDtАfterСonversion(CreateAcDbDtDto createAcDbFromDtDto)
        {

            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(ad => ad.Id == createAcDbFromDtDto.DatabaseId)?? 
                throw new FileNotFoundException ($"База с таким идентификатором не найдена {createAcDbFromDtDto.DatabaseId}");

            if(createAcDbFromDtDto.UploadedFileId == null)
            {
                logger.Info($"В запросе отсутствует идентификатор файла, база {createAcDbFromDtDto.DatabaseId} переводится в статус ошибки по причине {createAcDbFromDtDto.Description}");
                databaseStatusHelper.SetDbStatus(accountDatabase.V82Name, DatabaseState.ErrorCreate, createAcDbFromDtDto.Description);
                return CreateResultObj(true, databaseId: accountDatabase.Id);
            }

            try
            {
                var configuration = dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                    .AsQueryableNoTracking()
                    .Include(t => t.DbTemplate)
                    .Include(c => c.Configuration1C)
                    .FirstOrDefault(d => d.Configuration1C.ConfigurationNameVariations.FirstOrDefault(r => r.VariationName == createAcDbFromDtDto.ConfigurationName) != null
                     && d.DbTemplate.Remark != "demo");

                logger.Info($"Найдена конфигурация первым способом {configuration?.Configuration1CName}");

                if (configuration == null)
                {
                    var configurationName = !createAcDbFromDtDto.ConfigurationName.Contains(",")
                        ? createAcDbFromDtDto.ConfigurationName
                        : createAcDbFromDtDto.ConfigurationName.Remove(createAcDbFromDtDto.ConfigurationName.LastIndexOf(",") + 1);

                    configuration = dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                        .AsQueryableNoTracking()
                        .Include(t => t.DbTemplate)
                        .Include(c => c.Configuration1C)
                        .FirstOrDefault(d => d.Configuration1CName.Contains(configurationName)  && d.DbTemplate.Remark != "demo");

                    logger.Info($"По данной конфигурации {createAcDbFromDtDto.ConfigurationName}, после обработки вторым способом {configurationName} найден шаблон {configuration?.DbTemplate.Name}");
                }
                
                createDatabaseFromDtJobWrapper.Start(new RegisterDatabaseFromUploadFileModelDto
                {
                    UploadedFileId = createAcDbFromDtDto.UploadedFileId.Value,
                    AccountDatabaseId = accountDatabase.Id,
                    UsersIdListForAddAccess = [],
                    TemplateId = configuration?.DbTemplateId
                });

                return CreateResultObj(true, databaseId: accountDatabase.Id);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка загрузки ИБ из DT после конвертации] Загруженный файл '{createAcDbFromDtDto.UploadedFileId}'.");
                return CreateResultObj(false, ex.Message);
            }
        }

        /// <summary>
        /// Активировать Аренду 1С если необходимо
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void ActivateRent1CIfNeeded(Guid accountId)
        {
            var resConfig = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);
            if (resConfig != null) 
                return;

            var myEnterprise42CloudService = serviceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountId);
            myEnterprise42CloudService.ActivateService();
        }

        /// <summary>
        /// Создать результат выполнения
        /// </summary>
        /// <returns>Результат выполнения</returns>
        private CreateCloud42ServiceModelDto CreateResultObj(bool isComplete, string comment = null, Guid? databaseId = null)
        {
            var result = new CreateCloud42ServiceModelDto
            {
                Complete = isComplete,
                Comment = comment ?? string.Empty
            };

            if (databaseId.HasValue)
                result.Ids = [databaseId.Value];

            return result;
        }

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="caption">Название.</param>
        /// <param name="account">Аккаунт.</param>
        /// <param name="isFile">флаг файловая база или нет</param>
        private Domain.DataModels.AccountDatabase CreateNewDataBase(DbTemplate template, string caption, Account account, bool isFile = true) => dbLayer.DatabasesRepository.CreateNewRecord(
                template, 
                account, 
                PlatformType.V83, 
                caption,
                serviceProvider.GetRequiredService<ISegmentHelper>().GetFileStorageServer(account), 
                isFile);

        /// <summary>
        /// Проверить возможность создания инф. базы
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <param name="caption">Название инф. базы</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Результат проверки</returns>
        private CreateCloud42ServiceModelDto CheckAbilityToCreateDatabase(CreateAcDbFromDtDto createAcDbFromDtDto, Account account)
        {
            var uploadedFile = dbLayer.UploadedFileRepository.FirstOrDefault(f => f.Id == createAcDbFromDtDto.UploadedFileId);

            if (uploadedFile != null && (uploadedFile.FileSizeInMegabytes ?? 0) * 1.5 >
                cloud42ServiceHelper.GetMyDiskInfo(account.Id).FreeSizeOnDisk)
            {
                return new CreateCloud42ServiceModelDto
                {
                    Complete = false,
                    Comment = "Недостаточно свободного места на диске. Освободите свободное место или увеличьте размер диска.",
                    ResourceForRedirect = ResourceType.DiskSpace
                };
            }

            var checkAbilityToCreateDatabasesByLimit = checkAccountProvider.CheckAbilityToCreateDatabasesByLimit(account, 1);
            if (!checkAbilityToCreateDatabasesByLimit.Complete)
            {
                logger.Debug(checkAbilityToCreateDatabasesByLimit.Comment);
                return checkAbilityToCreateDatabasesByLimit;
            }

            var accountDbCaptionValidateResult = AccountDbCaptionValidator.Validate(createAcDbFromDtDto.DbCaption);
            if (!accountDbCaptionValidateResult.Success)
            {
                logger.Debug(accountDbCaptionValidateResult.Message);
                return new CreateCloud42ServiceModelDto
                {
                    Complete = false,
                    Comment = accountDbCaptionValidateResult.Message
                };
            }

            var checkRental1CActivate = checkRental1CActivateHelper.CheckRental1CActivate(account.Id);
            return checkRental1CActivate.Complete ? new CreateCloud42ServiceModelDto {Complete = true} : checkRental1CActivate;
        }
    }
}
