﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Access;

public interface IAcDbAccessesManager
{
    ManagerResult<List<Guid>> GetIDs(Guid accountDatabaseId);
    ManagerResult<int> Count(Guid accountDatabaseId);
    ManagerResult SetLocalUserId(AcDbAccessPostSetLocalUserIdDto model);
    ManagerResult<Guid> GetLocalUserId(Guid accessId);
    ManagerResult<Guid> GetAccountId(Guid accessId);
    ManagerResult<Guid> GetAccountUserId(Guid accessId);
    ManagerResult<AcDbAccess> GetProperties(Guid accessId);
    ManagerResult<List<Guid>> FindIDsByAccessAccountId(Guid accountId, Guid? accountUserId = null);

    /// <summary>
    /// Get accesses ids list 
    /// </summary>
    /// <param name="accountId">Account id</param>
    /// <param name="accountUserId">(optional) Account user id for filtering (default - return all)</param>
    /// <param name="includeMine">(optional) Exclude/include accesses to my databases (default - exclude)</param>
    /// <returns></returns>        
    Task<ManagerResult<List<Guid>>> FindAccessIDsByAccountId(Guid accountId,
        Guid? accountUserId = null, bool? includeMine = null);

    ManagerResult<List<Guid>> FindAllIDsByAccessAccountId(Guid accountId, Guid? accountUserId = null);
    ManagerResult<bool> IsLocked(AcDbAccessCheckLockModelDto model);
    ManagerResult<List<AcDbAccessItem>> GetAccessList(Guid accountID, Guid? accountUserId = null);
    ManagerResult LockAccess(AcDbAccessesLockModelDto model);
    ManagerResult UnLockAccess(AcDbAccessesLockModelDto model);
    ManagerResult LockAllDbAccess(AcDbAccessesLockAllDbModelDto model);
    ManagerResult UnLockAllDbAccess(AcDbAccessesLockAllDbModelDto model);
}
