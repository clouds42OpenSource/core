﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Access
{
    public class AcDbAccessesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IAcDbAccessesManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        #region public

        public ManagerResult<List<Guid>> GetIDs(Guid accountDatabaseId)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View,
                () => AccountIdByAccountDatabase(accountDatabaseId));

            var idList = DbLayer.AcDbAccessesRepository
                .Where(x => x.AccountDatabaseID == accountDatabaseId)
                .Select(y => y.ID)
                .ToList();
            return Ok(idList);
        }

        public ManagerResult<int> Count(Guid accountDatabaseId)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View,
                () => AccountIdByAccountDatabase(accountDatabaseId));

            var result = DbLayer.AcDbAccessesRepository
                .Where(x => x.AccountDatabaseID == accountDatabaseId)
                .Count();
            return Ok(result);
        }

        public ManagerResult SetLocalUserId(AcDbAccessPostSetLocalUserIdDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Edit, () => AccountIdByAcDbAccess(model.AccessID));

            if (!model.IsValid) return PreconditionFailed("Model is invalid");
            try
            {
                var entity = DbLayer.AcDbAccessesRepository.FirstOrDefault(x => x.ID == model.AccessID);
                if (entity == null)
                    throw new InvalidOperationException($"Access with id {model.AccessID} was not found");

                if (entity.LocalUserID == model.LocalUserID)
                    throw new InvalidOperationException("Access with such params already exists");

                entity.LocalUserID = model.LocalUserID;
                try
                {
                    DbLayer.AcDbAccessesRepository.Update(entity);
                    DbLayer.Save();
                }
                catch (Exception ex)
                {
                    _handlerException.Handle(ex, "[Ошибка установки локального пользователя]");
                    throw;
                }

                return Ok(new EmptyResultDto());
            }
            catch (Exception e)
            {
                ErrorStructuredDto errorStruct = new ErrorStructuredDto
                {
                    Code = 409,
                    Description = "Setting property failed",
                    DebugInfo = $"Error description: {e.GetMessage()}"
                };
                return ConflictError(errorStruct);
            }
        }


        public ManagerResult<Guid> GetLocalUserId(Guid accessId)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => AccountIdByAcDbAccess(accessId));

            var result = DbLayer.AcDbAccessesRepository.FirstOrDefault(x => x.ID == accessId);
            if (result == null)
                throw new InvalidOperationException($"Access with id {accessId} was not found");
            return Ok(result.LocalUserID);
        }

        public ManagerResult<Guid> GetAccountId(Guid accessId)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => AccountIdByAcDbAccess(accessId));
            var result = DbLayer.AcDbAccessesRepository.FirstOrDefault(x => x.ID == accessId);
            if (result == null)
                throw new InvalidOperationException($"Access with id {accessId} was not found");
            return Ok(result.AccountID);
        }

        public ManagerResult<Guid> GetAccountUserId(Guid accessId)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => AccountIdByAcDbAccess(accessId));
            var result = DbLayer.AcDbAccessesRepository.FirstOrDefault(x => x.ID == accessId);
            if (result == null) return NotFound<Guid>($"Access with id {accessId} was not found");
            return result.AccountUserID.HasValue
                ? Ok(result.AccountUserID.Value)
                : Conflict<Guid>("AccountUserID is null");
        }

        public ManagerResult<AcDbAccess> GetProperties(Guid accessId)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => AccountIdByAcDbAccess(accessId));
            var entity = DbLayer.AcDbAccessesRepository.FirstOrDefault(x => x.ID == accessId);
            if (entity == null)
                return NotFound<AcDbAccess>($"Access with id {accessId} was not found");
            return Ok(entity);
        }

        public ManagerResult<List<Guid>> FindIDsByAccessAccountId(Guid accountId, Guid? accountUserId = null)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => accountId);
            var accDbList = DbLayer.DatabasesRepository.Where(x => x.AccountId == accountId).Select(y => y.Id);
            var accessList = DbLayer.AcDbAccessesRepository
                .Where(x => x.AccountID == accountId);
            if (accountUserId.HasValue && accountUserId.Value != Guid.Empty)
                accessList = accessList
                    .Where(x => x.AccountUserID == accountUserId.Value)
                    .Where(z => !accDbList.Contains(z.AccountDatabaseID));
            var idList = accessList
                .Select(y => y.ID)
                .ToList();
            return Ok(idList);
        }

        /// <summary>
        /// Get accesses ids list 
        /// </summary>
        /// <param name="accountId">Account id</param>
        /// <param name="accountUserId">(optional) Account user id for filtering (default - return all)</param>
        /// <param name="includeMine">(optional) Exclude/include accesses to my databases (default - exclude)</param>
        /// <returns></returns>        
        public async Task<ManagerResult<List<Guid>>> FindAccessIDsByAccountId(Guid accountId,
            Guid? accountUserId = null, bool? includeMine = null)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => accountId);
            return await Task.FromResult(Ok(FindAccessIDs(accountId, accountUserId, includeMine)));
        }

        public ManagerResult<List<Guid>> FindAllIDsByAccessAccountId(Guid accountId, Guid? accountUserId = null)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => accountId);
            var accessList = DbLayer.AcDbAccessesRepository
                .Where(x => x.AccountID == accountId);
            if (accountUserId.HasValue && accountUserId.Value != Guid.Empty)
                accessList = accessList
                    .Where(x => x.AccountUserID == accountUserId.Value);
            var idList = accessList
                .Select(y => y.ID)
                .ToList();
            return Ok(idList);
        }

        public ManagerResult<bool> IsLocked(AcDbAccessCheckLockModelDto model)
        {
            var accountId = unitOfWork.AccountUsersRepository.FirstOrDefault(a => a.Id == model.AccountUserId)?.AccountId;
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => accountId);

            var access = DbLayer.AcDbAccessesRepository
                .Where(x => x.AccountUserID == model.AccountUserId && x.AccountDatabaseID == model.DatabaseId)
                .FirstOrDefault();

            return access == null ? PreconditionFailed<bool>("Доступ не найден") : Ok(access.IsLock);
        }

        public ManagerResult<List<AcDbAccessItem>> GetAccessList(Guid accountID, Guid? accountUserId = null)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_View, () => accountID);
            var answer = new List<AcDbAccessItem>();

            var accessList = DbLayer.AcDbAccessesRepository
                .AsQueryable()
                .Where(x => x.AccountID == accountID);

            if (accountUserId.HasValue && accountUserId.Value != Guid.Empty)
                accessList = accessList
                    .Where(a => a.AccountUserID == accountUserId);

            foreach (var item in accessList)
                answer.Add(new AcDbAccessItem
                {
                    AccountID = item.AccountID,
                    AccountDatabaseID = item.AccountDatabaseID,
                    LocalUserID = item.LocalUserID,
                    AccountUserID = item.AccountUserID ?? Guid.Empty,
                    ID = item.ID
                });

            return Ok(answer);
        }

        public ManagerResult LockAccess(AcDbAccessesLockModelDto model)
        {
            var accountId = unitOfWork.DatabasesRepository.FirstOrDefault(d => d.V82Name == model.DbName)?.AccountId;
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Lock, () => accountId);
            var user = DbLayer.AccountUsersRepository.FirstOrDefault(x => x.Login.Equals(model.Login));
            if (user == null)
                return NotFound("Пользователь не найден");
            var db = DbLayer.DatabasesRepository.FirstOrDefault(x => x.V82Name == model.DbName);
            if (db == null)
                return NotFound("Инфо база не найдена");

            var access =
                DbLayer.AcDbAccessesRepository.FirstOrDefault(
                    x => x.AccountDatabaseID == db.Id && x.AccountUserID.HasValue && x.AccountUserID.Value == user.Id);
            if (access == null)
                return NotFound("Доступ не найден");

            SetLockDbStateAccess(user.Id, db.Id, true);

            return Ok("Доступ заблокирован");
        }

        public ManagerResult UnLockAccess(AcDbAccessesLockModelDto model)
        {
            var accountId = unitOfWork.DatabasesRepository.FirstOrDefault(d => d.V82Name == model.DbName)?.AccountId;
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Lock, () => accountId);
            var user = DbLayer.AccountUsersRepository.FirstOrDefault(x => x.Login.Equals(model.Login));
            if (user == null)
                return NotFound("Пользователь не найден");
            var db = DbLayer.DatabasesRepository.FirstOrDefault(x => x.V82Name == model.DbName);
            if (db == null)
                return NotFound("Инфо база не найдена");
            var access =
                DbLayer.AcDbAccessesRepository.FirstOrDefault(
                    x => x.AccountDatabaseID == db.Id && x.AccountUserID.HasValue && x.AccountUserID.Value == user.Id);
            if (access == null)
                return NotFound("Доступ не найден");

            SetLockDbStateAccess(user.Id, db.Id, false);

            return Ok("Доступ разблокирован");
        }

        public ManagerResult LockAllDbAccess(AcDbAccessesLockAllDbModelDto model)
        {
            var accountId = unitOfWork.DatabasesRepository.FirstOrDefault(d => d.V82Name == model.DbName)?.AccountId;
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Lock, () => accountId);
            var db = DbLayer.DatabasesRepository.FirstOrDefault(x => x.V82Name == model.DbName);
            if (db == null)
                return NotFound("Инфо база не найдена");

            var accesses = db.AcDbAccesses.ToList();

            foreach (var acDbAccess in accesses)
            {
                if (acDbAccess.AccountUserID == null)
                    continue;

                SetLockDbStateAccess(acDbAccess.AccountUserID.Value, db.Id, true);
            }

            return Ok("Доступ заблокирован");
        }

        public ManagerResult UnLockAllDbAccess(AcDbAccessesLockAllDbModelDto model)
        {
            var accountId = unitOfWork.DatabasesRepository.FirstOrDefault(d => d.V82Name == model.DbName)?.AccountId;
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Lock, () => accountId);
            var db = DbLayer.DatabasesRepository.FirstOrDefault(x => x.V82Name == model.DbName);
            if (db == null)
                return NotFound("Инфо база не найдена");

            var accesses = db.AcDbAccesses.ToList();

            foreach (var acDbAccess in accesses)
            {
                if (acDbAccess.AccountUserID == null)
                    continue;

                SetLockDbStateAccess(acDbAccess.AccountUserID.Value, db.Id, false);
            }

            return Ok("Доступ заблокирован");
        }

        #endregion

        private void SetLockDbStateAccess(Guid accountUserId, Guid databaseId, bool setState)
        {
            var access =
                DbLayer.AcDbAccessesRepository.FirstOrDefault(
                    a => a.AccountDatabaseID == databaseId && a.AccountUserID == accountUserId);

            if (access == null)
                return;

            access.IsLock = setState;
            DbLayer.AcDbAccessesRepository.Update(access);
            DbLayer.Save();
        }

        private List<Guid> FindAccessIDs(Guid accountId, Guid? accountUserId = null, bool? includeMine = null)
        {
            // get my databases ids
            var myDbList = DbLayer.DatabasesRepository.Where(x => x.AccountId == accountId).Select(db => db.Id);

            // get all accesses for current account
            var accessList = DbLayer.AcDbAccessesRepository
                .AsQueryable()
                .Where(x => x.AccountID == accountId);

            // filtered by account user id
            if (accountUserId.HasValue && accountUserId.Value != Guid.Empty)
                accessList = accessList
                    .Where(a => a.AccountUserID == accountUserId);

            // exclude accesses for my databases (default)
            if (!includeMine.HasValue || !includeMine.Value)
                accessList = accessList
                    .Where(ac => !myDbList.Contains(ac.AccountDatabaseID));

            return accessList.Select(al => al.ID).ToList();
        }
    }
}
