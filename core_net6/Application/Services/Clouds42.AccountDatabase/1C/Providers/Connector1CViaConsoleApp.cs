﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.Enums._1C;
using Clouds42.Tools1C.Helpers;
using Provider1C.Client;
using Provider1C.Client.Models;
using Provider1C.Common.Models.Commands.Args;
using Provider1C.Common.Models.Commands.Results;

namespace Clouds42.AccountDatabase._1C.Providers
{
    /// <summary>
    /// Класс для выполнения комманд к 1С через консольное приложение
    /// </summary>
    public sealed class Connector1CViaConsoleApp : IDisposable
    {
        private static readonly object Lock = new();
        private readonly Provider1CClient _provider1CClient;

        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="Connector1CViaConsoleApp"/>.
        /// </summary>
        /// <param name="database">Параметры информационной базы</param>
        /// <param name="connectTo1CTimeoutInSeconds">Время ожидания коннеста к 1С по COM</param>
        /// <param name="connectTo1CAttempts">Количество попыток подключения к 1С</param>
        private Connector1CViaConsoleApp(IUpdateDatabaseDto database, int connectTo1CTimeoutInSeconds, byte connectTo1CAttempts, string attachToProcessSettings)
        {
            _provider1CClient = new Provider1CClient(
                CreateDatabaseConnectCommandArgumentsModel(database),
                TimeSpan.FromSeconds(connectTo1CTimeoutInSeconds),
                connectTo1CAttempts, attachToProcessSettings);
        }

        /// <summary>
        /// Выполняет комманды к 1С через консольное приложение
        /// </summary>
        /// <typeparam name="TResult">Тип результата</typeparam>
        /// <param name="database">Параметры информационной базы</param>
        /// <param name="connectTo1CTimeoutInSeconds">Время ожидания коннеста к 1С по COM</param>
        /// <param name="connectTo1CAttempts">Количество попыток подключения к 1С</param>
        /// <param name="syncAction">Метод выполнения комманд.</param>
        /// <param name="attachToProcessSettings"></param>
        /// <returns></returns>
        public static TResult Execute<TResult>(
            IUpdateDatabaseDto database, int connectTo1CTimeoutInSeconds, byte connectTo1CAttempts,
            Func<Connector1CViaConsoleApp, TResult> syncAction, string attachToProcessSettings)
        {
            lock (Lock)
            {
                using var connector1C = new Connector1CViaConsoleApp(database, connectTo1CTimeoutInSeconds, connectTo1CAttempts, attachToProcessSettings);
                return syncAction(connector1C);
            }
        }

        /// <summary>
        /// Создать модель параметров подключения для Provider1C.ConsoleApp.exe
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <returns>Модель параметров подключения к Provider1C.ConsoleApp.exe</returns>
        private static ConnectTo1CDatabaseCommandArgumentsModel CreateDatabaseConnectCommandArgumentsModel(
            IUpdateDatabaseDto database)
            => new()
            {
                IsFile = database.IsFile,
                DatabaseName = database.UpdateDatabaseName,
                DbAdminLogin = database.AdminLogin,
                DbAdminPassword = database.AdminPassword,
                Engine1CVersion = database.PlatformVersion,
                Engine1CType = database.PlatformType,
                PathToDatabaseFolder = database.IsFile ? database.ConnectionAddress : string.Empty,
                ConnectionAddress = database.IsFile ? string.Empty : database.ConnectionAddress,
                Engine1CFolderBinPath = database.PlatformType == PlatformType.V82 ? Configurations.Configurations.CloudConfigurationProvider.Enterprise1C.Get1C82ExecutionPath() : database.FolderPath1C
            };
        
        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>
        /// <param name="timeToWaitInMinutes">Время ожидания выполнения команды в минутах</param> 
        /// <param name="numberOfAttempts">Количество попыток</param> 
        public ConnectorResultDto<MetadataResultDto> GetMetadataInfo(int timeToWaitInMinutes, byte numberOfAttempts)
        {
            var metadataResult = _provider1CClient.GetDatabaseMetadata(TimeSpan.FromMinutes(timeToWaitInMinutes), 
                numberOfAttempts,
                new CommandOptions
                {
                    KillProcessAfterExecution = true
                });

            return metadataResult.IsSuccess
                ? new ConnectorResultDto<MetadataResultDto>
                {
                    Result = new MetadataResultDto(
                        metadataResult.Result.ConfigurationVersion,
                        metadataResult.Result.ConfigurationSynonym),
                    ConnectCode = metadataResult.Result.ConnectCode,
                }
                : GetErrorResultFromMetadataResult(metadataResult);
        }

        /// <summary>
        /// Принять полученные обновления информационной базы.
        /// <para>В случаи ошибки, делает throw этой ошибки</para>
        /// </summary>        
        /// <param name="timeToWaitInMinutes">Время ожидания выполнения команды в минутах</param>
        /// <param name="numberOfAttempts">Количество попыток</param> 
        public bool ApplyUpdates(int timeToWaitInMinutes, byte numberOfAttempts)
        {
            var applyUpdatesResult = _provider1CClient.ApplyDatabaseUpdates(TimeSpan.FromMinutes(timeToWaitInMinutes),
                numberOfAttempts, new CommandOptions
                {
                    KillProcessAfterExecution = true
                });

            if (applyUpdatesResult.IsSuccess)
            {
                return true;
            }

            throw applyUpdatesResult.Error;
        }

        public void Dispose()
        {
            _provider1CClient.Dispose();
        }

        /// <summary>
        /// Получить модель ошибки из метаданных инф. базы
        /// </summary>
        /// <param name="metadataResult">Метаданные инф. базы</param>
        /// <returns>Распознанный результат подключения к инф. базе</returns>
        private static ConnectorResultDto<MetadataResultDto> GetErrorResultFromMetadataResult(CommandExecuteResultModel<DatabaseMetadataCommandResultModel> metadataResult)
        {
            var errorModel = Connector1CErrorParser.Parse(metadataResult.Error?.Message);
            return new ConnectorResultDto<MetadataResultDto>
            {
                ConnectCode = errorModel.ConnectCode,
                Message = errorModel.ErorMessage
            };
        }
    }
}
