﻿using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.Its;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase._1C.Providers
{
    /// <summary>
    /// Провайдер для работы с данными конфигураций 1С
    /// </summary>
    internal class Configurations1CDataProvider(
        IUnitOfWork dbLayer,
        ICfuProvider cfuProvider,
        ICreateServiceTypeForConfiguration1CProvider createServiceTypeForConfiguration1CProvider)
        : IConfigurations1CDataProvider
    {
        /// <summary>
        /// Дата контекст
        /// </summary>
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Провайдер пакетов обновления 1С.
        /// </summary>
        private readonly ICfuProvider _cfuProvider = cfuProvider;

        /// <summary>
        ///  Получить конфигурацию
        /// </summary>
        /// <param name="name">Название</param>
        /// <returns>Конфигурация</returns>
        public ConfigurationDetailsModelDto GetConfiguration(string name)
        {
            var configuration = _dbLayer.Configurations1CRepository.GetById(name);

            if (configuration == null)
                throw new NotFoundException($"Не найдена конфигурация по имени {name}");

            return new ConfigurationDetailsModelDto
            {
                Configuration = configuration,
                Redactions = _cfuProvider.GetConfigurationRedactionInfo(configuration),
                ConfigurationNameVariations =
                    configuration.ConfigurationNameVariations.Select(w => w.VariationName).ToList()
            };
        }

        /// <summary>
        /// Получить названия конфигураций 1С
        /// </summary>
        /// <returns>Список названий</returns>
        public List<string> GetConfigurationNames() => _dbLayer.Configurations1CRepository.Select(x => x.Name).ToList();
        

        /// <summary>
        ///     Добавить конфигурацию 1С
        /// </summary>
        /// <param name="configurationToAdd">Модель свойств элемента конфигурации 1С для обновления или добавления</param>
        /// <returns>Данные после добавления</returns>
        public AddOrUpdateConfiguration1CResultDto AddConfiguration1C(
            AddOrUpdateConfiguration1CDataItemDto configurationToAdd)
        {
            var configuration = _dbLayer.Configurations1CRepository.GetById(configurationToAdd.Name);

            if (configuration != null)
            {
                var message = $"Найден дубликат конфигурации {configurationToAdd.Name}";
                throw new InvalidOperationException(message);
            }

            using var transaction = _dbLayer.SmartTransaction.Get();
            try
            {
                var newConfig = CreateConfigurations1CDomainModelFrom(configurationToAdd);
                createServiceTypeForConfiguration1CProvider.Create(new PerformOperationOnServiceTypeByConfig1CDto
                {
                    ConfigurationName = configurationToAdd.Name,
                    ConfigurationCost = configurationToAdd.ConfigurationCost
                });

                _dbLayer.Save();
                transaction.Commit();

                return new AddOrUpdateConfiguration1CResultDto
                {
                    Reductions = _cfuProvider.GetConfigurationRedactionInfo(newConfig).ToArray()
                };
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                var message = $"Во время добавления конфигурации произошла ошибка : {ex.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Создать новую конфигурацию
        /// </summary>
        /// <param name="configurationToAdd">Модель свойств элемента конфигурации 1С для обновления или добавления</param>
        /// <returns>Новую дабавленую конфигурацию в репазиторий</returns>
        private Configurations1C CreateConfigurations1CDomainModelFrom(
            AddOrUpdateConfiguration1CDataItemDto configurationToAdd)
        {
            var newConfig = new Configurations1C
            {
                Name = configurationToAdd.Name,
                ConfigurationCatalog = configurationToAdd.ConfigurationCatalog,
                RedactionCatalogs = configurationToAdd.RedactionCatalogs,
                PlatformCatalog = configurationToAdd.PlatformCatalog,
                UpdateCatalogUrl = configurationToAdd.UpdateCatalogUrl,
                ShortCode = configurationToAdd.ShortCode,
                UseComConnectionForApplyUpdates = configurationToAdd.UseComConnectionForApplyUpdates,
                NeedCheckUpdates = configurationToAdd.NeedCheckUpdates,
                ItsAuthorizationDataId = configurationToAdd.ItsAuthorizationDataId
            };
            _dbLayer.Configurations1CRepository.Insert(newConfig);


            if (!configurationToAdd.ConfigurationVariations.Any())
            {
                return newConfig;
            }

            var configurationNameVariations = configurationToAdd.ConfigurationVariations.Select(w =>
                new ConfigurationNameVariation
                {
                    Id = Guid.NewGuid(),
                    Configuration1CName = configurationToAdd.Name,
                    VariationName = w
                });

            _dbLayer.ConfigurationNameVariationRepository.InsertRange(configurationNameVariations);

            return newConfig;
        }

        /// <summary>
        ///     Удалить существующую конфигурацию
        /// </summary>
        /// <param name="args">Аргументы для удаления конфигурации</param>
        public void DeleteConfiguration1C(DeleteConfiguration1CDataItemDto args)
        {
            var configuration = _dbLayer.Configurations1CRepository.GetById(args.ConfigurationName);
            var configurationServiceType = _dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>()
                .FirstOrDefault(relation => relation.Configuration1CName == configuration.Name);
            var conCfu = _dbLayer.ConfigurationsCfuRepository.Where(cfu => cfu.ConfigurationName == configuration.Name).ToList();
            if (configuration == null)
            {
                var message = $"Для удаления конфигурации {args.ConfigurationName} не найден обьект в базе";
                throw new NotFoundException(message);
            }

            using var transaction = _dbLayer.SmartTransaction.Get();
            try
            {
                var configurationNameVariations = configuration.ConfigurationNameVariations.ToList();
                if (configurationNameVariations.Any())
                {
                    _dbLayer.ConfigurationNameVariationRepository.DeleteRange(configurationNameVariations);
                    _dbLayer.Save();
                }

                if (configurationServiceType != null)
                {
                    _dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>().Delete(configurationServiceType);
                    _dbLayer.Save();
                }
                if (conCfu.Any())
                {
                    foreach(var cfu in conCfu)
                    {
                        var accDbUpdateVersionMappings = _dbLayer.AccountDatabaseUpdateVersionMappingRepository.
                            Where(x => x.CfuId == cfu.Id).ToList();
                        if(accDbUpdateVersionMappings.Any())
                        {
                            _dbLayer.AccountDatabaseUpdateVersionMappingRepository.DeleteRange(accDbUpdateVersionMappings);
                            _dbLayer.Save();
                        }
                        _dbLayer.ConfigurationsCfuRepository.Delete(cfu);
                        _dbLayer.Save();
                    }
                }
                var acDbsup = _dbLayer.AcDbSupportRepository.Where(x => x.ConfigurationName == configuration.Name).ToList();
                foreach(var sup in acDbsup)
                {
                    sup.ConfigurationName = null;
                    _dbLayer.AcDbSupportRepository.Update(sup);
                }

                _dbLayer.Configurations1CRepository.Delete(configuration);
                _dbLayer.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                var message = $"Во время удаления конфигурации произошла ошибка : {ex.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }
    }
}
