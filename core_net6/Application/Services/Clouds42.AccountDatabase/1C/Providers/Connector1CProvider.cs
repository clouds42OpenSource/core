﻿using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Tools1C.Connectors1C.DbConnector;
using Clouds42.Tools1C.Helpers;

namespace Clouds42.AccountDatabase._1C.Providers
{
    /// <summary>
    /// Провайдер COM коннектора 1С
    /// </summary>
    public class Connector1CProvider : IConnector1CProvider
    {
        private static readonly object Lock = new();



        /// <summary>
        /// Фабрика создания объекта подключения к информационной базе 1С
        /// </summary>
        private static class Connector1CFactory
        {
            /// <summary>
            /// Класс подключения через COM объект к файловой базе.
            /// </summary>                
            public static DbConnector1C CreateComConnector(IUpdateDatabaseDto database)
            {
                var paramsForFileDb = Connector1CParamsCreator.CreateParamsForConnectToFileDatabase(database);
                var paramsForServerDb = Connector1CParamsCreator.CreateParamsForConnectToServerDatabase(database);

                return database.IsFile ? new DbConnector1C(paramsForFileDb) : new DbConnector1C(paramsForServerDb);
            }
        }

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>
        /// <param name="database">Модель данных обновления базы</param>
        /// <returns>Метаданные информационной базы</returns>
        public ConnectorResultDto<MetadataResultDto> GetMetadataInfo(IUpdateDatabaseDto database)
        {
            lock (Lock)
            {
                using var comConnector = Connector1CFactory.CreateComConnector(database);
                return comConnector.GetMetadataInfo();
            }
        }

        /// <summary>
        /// Принять полученные обновления информационной базы.
        /// </summary>
        /// <param name="database">Модель данных обновления базы</param>  
        public void ApplyUpdates(IUpdateDatabaseDto database)
        {
            lock (Lock)
            {
                using var comConnector = Connector1CFactory.CreateComConnector(database);
                comConnector.ApplyUpdates();
            }
        }
    }
}
