﻿namespace Clouds42.AccountDatabase._1C
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки конфигураций 1С
    /// </summary>
    public static class Configurations1CSortFieldNames
    {
        /// <summary>
        ///  Название конфигурации
        /// </summary>
        public const string ConfigurationName = "configurationname";

        /// <summary>
        /// Наименование поля каталог конфигурации на ИТС
        /// </summary>
        public const string ConfigurationCatalog = "configurationcatalog";
    }
}