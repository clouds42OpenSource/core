﻿using System.Net;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase._1C.Validators
{
    /// <summary>
    ///     Класс валидации для Каталога конфигураций 1С
    /// </summary>
    public class CatalogOfConfigurationValidator(
        IUnitOfWork dbLayer,
        ICfuProvider cfuProvider,
        IHttpClientFactory httpClientFactory)
    {
        /// <summary>
        /// Дата контекст
        /// </summary>
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Провайдер пакетов обновления 1С.
        /// </summary>
        private readonly ICfuProvider _cfuProvider = cfuProvider;

        /// <summary>
        ///     Проверка на существование конфигурации в базе
        /// </summary>
        /// <param name="name">Название конфигурации</param>
        /// <returns>Возвращает <c>true</c> если конфигурация с именем <paramref name="name"/>не существует, иначе <c>false</c>.</returns>
        public bool CheckName(string name)
        {
            var configuration = _dbLayer.Configurations1CRepository.FirstOrDefault(conf => conf.Name == name);

            if (configuration == null)
                return true;
            return false;
        }

        /// <summary>
        ///     Метод валидации конфигурации
        /// </summary>
        /// <param name="configuration">Конфигурация 1С.</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        public void ValidateConfiguration(IConfigurations1C configuration, out string errorMessage)
        {
            try
            {
                var redaction = _cfuProvider.GetConfigurationRedactionInfo(configuration);

                foreach (var red in redaction)
                {
                    if (!TryDetectCatalogAvailability(configuration, red, out errorMessage))
                        return;
                }

                errorMessage = null;
            }
            catch (Exception exception)
            {
                errorMessage = $"Не верно составлена конфигурация, ошибка - {exception.Message}";
            }
        }

        /// <summary>
        ///     Метод отправки и получения запроса для 
        /// валидации url конфигурации
        /// </summary>
        /// <param name="configuration">Конфигурация 1С.</param>
        /// <param name="redaction">Редакция конфигурации 1С.</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Статус ответа на проверку существования url конфигурации</returns>
        private bool TryDetectCatalogAvailability(IConfigurations1C configuration, ConfigurationRedactionModelDto redaction, out string errorMessage)
        {
            errorMessage = null;
            var errorDescription =
                $"В каталоге обновлений по адресу \"{configuration.UpdateCatalogUrl}\" не удалось найти конфигурацию с названием \"{configuration.Name}\". " +
                "Проверьте корректность введенных данных.";
            try
            {
                var httpClient = httpClientFactory.CreateClient();

                var responseMessage = httpClient.Send(new HttpRequestMessage(HttpMethod.Get, redaction.UrlOfMapping));

                if (responseMessage.StatusCode is HttpStatusCode.OK or HttpStatusCode.Forbidden)
                    return true;

                errorMessage = errorDescription;
                return false;
            }
            catch (Exception)
            {
                errorMessage = errorDescription;
                return false;
            }
        }
    }
}
