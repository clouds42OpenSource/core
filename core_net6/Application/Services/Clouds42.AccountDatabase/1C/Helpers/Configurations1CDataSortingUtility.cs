﻿using Clouds42.AccountDatabase._1C.Models;
using Clouds42.Common;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase._1C.Helpers
{
    /// <summary>
    /// Утилита для сортиовки записей конфигураций 1С
    /// </summary>
    public static class Configurations1CDataSortingUtility
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateQueryable<Configuration1CDataModel>> SortingActions =
            new()
            {
                {Configurations1CSortFieldNames.ConfigurationName, SortByConfigurationName},
                {Configurations1CSortFieldNames.ConfigurationCatalog, SortByConfigurationCatalog}
            };

        /// <summary>
        /// Производит сортировку выбранных записей конфигураций 1С
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи конфигураций 1С</returns>
        public static IOrderedQueryable<Configuration1CDataModel> MakeSorting(
            IQueryable<Configuration1CDataModel> records, SortingDataDto sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();

            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate)
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }

        /// <summary>
        /// Сортитровать записи записей конфигураций 1С
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи конфигураций 1С по названию конфигурации</returns>
        private static IOrderedQueryable<Configuration1CDataModel> SortByDefault(
            IQueryable<Configuration1CDataModel> records)
            => records.OrderByDescending(row => row.Configuration1C.Name);


        /// <summary>
        /// Сортитровать выбранные записи конфигураций 1С
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей конфигураций 1С по полю Name</returns>
        private static IOrderedQueryable<Configuration1CDataModel> SortByConfigurationName(
            IQueryable<Configuration1CDataModel> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Configuration1C.Name),
                SortType.Desc => records.OrderByDescending(row => row.Configuration1C.Name),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать выбранные записи конфигураций 1С
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи конфигураций 1С действий приложения по полю ConfigurationCatalog</returns>
        private static IOrderedQueryable<Configuration1CDataModel> SortByConfigurationCatalog(
            IQueryable<Configuration1CDataModel> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Configuration1C.ConfigurationCatalog),
                SortType.Desc => records.OrderByDescending(row => row.Configuration1C.ConfigurationCatalog),
                _ => SortByDefault(records)
            };
        }
    }
}