﻿using Clouds42.AccountDatabase._1C.Models;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase._1C.Helpers
{
    /// <summary>
    /// Класс для выбора данных концигураций 1С
    /// </summary>
    internal sealed class Configurations1CDataSelector(IUnitOfWork dbLayer, Configurations1CFilterParamsDto filter)
    {
        /// <summary>
        /// Параметры фильтрации записей концигураций 1С
        /// </summary>
        private readonly Configurations1CFilterParamsDto _filter = filter;

        /// <summary>
        /// Дата контекст
        /// </summary>
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Получить отфильтрованный список концигураций 1С
        /// </summary>
        /// <returns>Отфильтрованный список концигураций 1С</returns>
        public IQueryable<Configuration1CDataModel> SelectWithFilters()
        {
            return _filter == null
                ? SelectConfigurations1CData()
                : SelectConfigurations1CData().ComposeFilters(
                    FilterByConfigurationName,
                    FilterByConfigurationCatalog);
        }

        /// <summary>
        /// Выбрать данные конфигураций 1С
        /// </summary>
        /// <returns>Данные конфигураций 1С</returns>
        private IQueryable<Configuration1CDataModel> SelectConfigurations1CData() =>
            from configurations1C in _dbLayer.Configurations1CRepository.WhereLazy()
            join relation in _dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>().WhereLazy() on
                configurations1C.Name equals relation.Configuration1CName
            join serviceTypeRate in _dbLayer.RateRepository.WhereLazy() on relation.ServiceTypeId equals
                serviceTypeRate.BillingServiceTypeId
            select new Configuration1CDataModel
            {
                Configuration1C = configurations1C,
                ConfigurationVariations = configurations1C.ConfigurationNameVariations.Select(w => w.VariationName)
                    .ToList(),
                ConfigurationCost = serviceTypeRate.Cost
            };

        /// <summary>
        /// Отфильтровать записи по названию конфигурации
        /// </summary>
        /// <param name="records">Записи для фильтрации</param>
        /// <returns>Отфильтрованные записи</returns>
        private IQueryable<Configuration1CDataModel> FilterByConfigurationName(
            IQueryable<Configuration1CDataModel> records)
            => records.Where(rec =>
                _filter.ConfigurationName == null ||
                rec.Configuration1C.Name != null && rec.Configuration1C.Name.Contains(_filter.ConfigurationName));


        /// <summary>
        /// Отфильтровать записи по каталогу конфигурации на ИТС
        /// </summary>
        /// <param name="records">Записи для фильтрации</param>
        /// <returns>Отфильтрованные записи</returns>
        private IQueryable<Configuration1CDataModel> FilterByConfigurationCatalog(
            IQueryable<Configuration1CDataModel> records)
            => records.Where(rec =>
                _filter.ConfigurationCatalog == null ||
                rec.Configuration1C.ConfigurationCatalog != null &&
                rec.Configuration1C.ConfigurationCatalog.Contains(_filter.ConfigurationCatalog));
    }
}