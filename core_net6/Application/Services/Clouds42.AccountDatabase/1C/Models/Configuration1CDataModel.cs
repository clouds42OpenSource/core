﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase._1C.Models
{
    /// <summary>
    /// Модель данных конфигурации 1С
    /// </summary>
    public class Configuration1CDataModel
    {
        /// <summary>
        /// Конфигурация 1С
        /// </summary>
        public Configurations1C Configuration1C { get; set; }

        /// <summary>
        /// Стоимость конфигурации
        /// </summary>
        public decimal ConfigurationCost { get; set; }

        /// <summary>
        /// Вариации имени конфигурации 1С
        /// </summary>
        public List<string> ConfigurationVariations { get; set; } = [];
    }
}
