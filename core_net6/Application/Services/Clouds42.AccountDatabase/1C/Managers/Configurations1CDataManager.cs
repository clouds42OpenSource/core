﻿using Clouds42.AccountDatabase._1C.Validators;
using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase._1C.Managers
{
    /// <summary>
    /// Менеджер для работы с данными конфигураций 1С
    /// </summary>
    public class Configurations1CDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IConfigurations1CDataProvider configurations1CDataProvider,
        CatalogOfConfigurationValidator configurationValidator)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        ///  Провайдер для работы с данными конфигураций 1С
        /// </summary>
        private readonly IConfigurations1CDataProvider _configurations1CDataProvider = configurations1CDataProvider;

        /// <summary>
        /// Валидации для конфигураций 1С
        /// </summary>
        private readonly CatalogOfConfigurationValidator _configurationValidator = configurationValidator;

        /// <summary>
        ///     Получить конфигурацию 1С
        /// </summary>
        /// <param name="name">Название конфигурации</param>
        /// <returns></returns>
        public ManagerResult<ConfigurationDetailsModelDto> GetConfiguration(string name)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ViewConfiguration1C, () => AccessProvider.ContextAccountId);
                var configuration = _configurations1CDataProvider.GetConfiguration(name);
                return Ok(configuration);
            }
            catch (Exception exception)
            {
                return PreconditionFailed<ConfigurationDetailsModelDto>(exception.Message);
            }
        }

        /// <summary>
        ///     Валидация названия конф., не должно быть повторений в базе
        /// </summary>
        /// <param name="name">Название конфигурации для проверки</param>
        /// <returns>Возвращает <c>true</c> если конфигурация с именем <paramref name="name"/>не существует, иначе <c>false</c>.</returns>
        public ManagerResult<bool> CheckConfigurationNameNotExist(string name)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewConfiguration1C, () => AccessProvider.ContextAccountId);
                return Ok(_configurationValidator.CheckName(name));
            }
            catch (Exception exception)
            {
                return ValidationError<bool>(exception.Message);
            }
        }

        /// <summary>
        ///   Метод добавления конфигурации 1С
        /// </summary>
        /// <param name="configuration1C">Модель свойств элемента конфигурации 1С для обновления или добавления</param>
        /// <returns>Результат добавления</returns>
        public ManagerResult<AddOrUpdateConfiguration1CResultDto> AddConfiguration1C(AddOrUpdateConfiguration1CDataItemDto configuration1C)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewConfiguration1C, () => AccessProvider.ContextAccountId);

                _configurationValidator.ValidateConfiguration(configuration1C, out var errorMessage);

                if (!string.IsNullOrEmpty(errorMessage))
                    return ValidationError<AddOrUpdateConfiguration1CResultDto>(errorMessage);

                var result = _configurations1CDataProvider.AddConfiguration1C(configuration1C);
                return Ok(result);
            }
            catch (Exception exception)
            {
                return PreconditionFailed<AddOrUpdateConfiguration1CResultDto>(exception.Message);
            }
        }

        /// <summary>
        ///     Удаление конфигурации с базы
        /// </summary>
        /// <param name="args">Аргументы для удаления</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult<bool> DeleteConfiguration(DeleteConfiguration1CDataItemDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeleteConfiguration1C, () => AccessProvider.ContextAccountId);
                _configurations1CDataProvider.DeleteConfiguration1C(args);
                return Ok(true);
            }
            catch (Exception exception)
            {
                return PreconditionFailed<bool>(exception.Message);
            }
        }

    }
}
