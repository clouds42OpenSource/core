﻿using Clouds42.AccountDatabase.Contracts.Delete.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delete.Managers
{
    /// <summary>
    /// Менеджер уведомления пользователей о скором удалении данных
    /// </summary>
    public class BeforeArchiveDataNotificationManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IBeforeArchiveDataNotificationProvider beforeArchiveDataNotificationProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Отправить уведомления
        /// </summary>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult SendNotifications()
        {
            try
            {
                beforeArchiveDataNotificationProvider.SendNotifications();
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка отправки уведомлений о скором удалении данных неактивноых аккаунтов]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
