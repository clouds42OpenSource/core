﻿using Clouds42.AccountDatabase.Contracts.Delete.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delete.Managers
{
    /// <summary>
    /// Менеджер для удаления данных неактивных аккаунтов
    /// </summary>
    public class DeleteInactiveAccountDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IDeleteInactiveAccountDataProvider deleteInactiveAccountDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Удалить данные неактивных аккаунтов
        /// </summary>
        /// <returns>Ok - если операция завершилась успешно</returns>
        public ManagerResult DeleteInactiveAccountData()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Accounts_DeleteData);

                deleteInactiveAccountDataProvider.DeleteInactiveAccountData();
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка удаления данные неактивных аккаунтов]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
