﻿using Clouds42.AccountDatabase.Contracts.Delete.Interfaces;
using Clouds42.AccountDatabase.Delete.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.StateMachine.Contracts.DeleteInactiveAccountDataProcessFlow;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delete.Providers
{
    /// <summary>
    /// Провайдер для удаления данных неактивных аккаунтов
    /// </summary>
    internal class DeleteInactiveAccountDataProvider : IDeleteInactiveAccountDataProvider
    {
        private readonly InactiveAccountDataHelper _inactiveAccountDataHelper;
        private readonly IUnitOfWork _dbLayer;
        private readonly IAccessProvider _accessProvider;
        private readonly IServiceProvider _serviceProvider;

        public DeleteInactiveAccountDataProvider(InactiveAccountDataHelper inactiveAccountDataHelper,
            IAccountDatabaseDataProvider accountDatabaseDataProvider,
            ILogger42 logger,
            IUnitOfWork dbLayer, IAccessProvider accessProvider, IServiceProvider servcieProvider)
        {
            _inactiveAccountDataHelper = inactiveAccountDataHelper;
            _dbLayer = dbLayer;
            _accessProvider = accessProvider;
            _serviceProvider = servcieProvider;
        }

        /// <summary>
        /// Удалить данные неактивных аккаунтов
        /// </summary>
        public void DeleteInactiveAccountData()
        {
            var resultList = new List<DeleteInactiveAccountDataResultDto>();

            var accountsForDeleteData = _inactiveAccountDataHelper.GetAccountsForDeleteData();

            accountsForDeleteData.ForEach(accountId =>
            {
                var result = _serviceProvider.GetRequiredService<IDeleteInactiveAccountDataProcessFlow>()
                    .Run(new DeleteInactiveAccountDataParamsDto
                    {
                        AccountId = accountId
                    });

                if (result.Finish)
                {
                    resultList.Add(result.ResultModel);
                    return;
                }

                resultList.Add(new DeleteInactiveAccountDataResultDto
                {
                    AccountId = accountId,
                    Success = result.Finish,
                    ErrorMessage = result.Message
                });
            });

            ProcessDeletionResult(resultList);
        }

        /// <summary>
        /// Обработать результат удаления
        /// </summary>
        /// <param name="result">Список результатов</param>
        private void ProcessDeletionResult(List<DeleteInactiveAccountDataResultDto> result)
        {
            result.ForEach(res =>
            {
                var message = !res.Success
                    ? res.ErrorMessage
                    : $"Для аккаунта удалены архивные данные по пути: {res.AccountFolderPath}";
                LogEventHelper.LogEvent(_dbLayer, res.AccountId, _accessProvider, LogActions.DeleteInactiveAccountData, message);
            });
        }
    }
}
