﻿using Clouds42.AccountDatabase.Contracts.Delete.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delete.Providers
{
    /// <summary>
    /// Провайдер для удаления бекапов после удаления баз данных
    /// </summary>
    internal class BeforeDeleteAccountDatabaseProvider : IBeforeDeleteAccountDatabaseProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly IAccessProvider _accessProvider;
        private readonly IAccountDatabaseChangeStateProvider _accountDatabaseChangeStateProvider;
        private readonly ILogger42 _logger;
        private readonly IAccountDatabaseDataProvider _accountDatabaseDataProvider;
        private readonly AccountDatabaseLocalBackupProvider _accountDatabaseLocalBackupProvider;
        private readonly TombCleanerProvider _tombCleanerProvider;
        private readonly IDictionary<AccountDatabaseBackupSourceType, Action<Guid>> _mapBackupSourceTypeToDeleteAction;

        public BeforeDeleteAccountDatabaseProvider(
            IAccountDatabaseDataProvider accountDatabaseDataProvider, AccountDatabaseLocalBackupProvider accountDatabaseLocalBackupProvider,
            ILogger42 logger, TombCleanerProvider tombCleanerProvider,
            IUnitOfWork dbLayer, IAccessProvider accessProvider, IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider)
        {
            _dbLayer = dbLayer;
            _accessProvider = accessProvider;
            _accountDatabaseChangeStateProvider = accountDatabaseChangeStateProvider;
            _logger = logger;
            _accountDatabaseLocalBackupProvider = accountDatabaseLocalBackupProvider;
            _tombCleanerProvider = tombCleanerProvider;
            _accountDatabaseDataProvider = accountDatabaseDataProvider;
            _mapBackupSourceTypeToDeleteAction = new Dictionary<AccountDatabaseBackupSourceType, Action<Guid>>
            {
                { AccountDatabaseBackupSourceType.Local, backupId => _accountDatabaseLocalBackupProvider.DeleteBackupAccountDatabase(backupId, true) },
                { AccountDatabaseBackupSourceType.GoogleCloud, backupId => _tombCleanerProvider.DeleteCloudAccountDatabaseBackup(backupId, true) }
            };
        }

        /// <summary>
        /// Удалить бекапы после удаления баз данных
        /// </summary>
        public void DeleteBackupAccountDatabase()
        {

            var deleteDatabasesFromCloud = _dbLayer.DatabasesRepository.Where(databasesData => (databasesData.LastEditedDateTime - DateTime.Now).Days > 30 &&
                    databasesData.State == DatabaseState.DeletedToTomb.ToString());

            _logger.Info($"Для удаления бекапов найдено {deleteDatabasesFromCloud.Count()} инф. баз");

            foreach (var databaseData in deleteDatabasesFromCloud)
            {
                _logger.Info($"Начинаем удаление бэкапа для инф. базы {databaseData.V82Name}");
                if (databaseData.AccountDatabaseOnDelimiter == null)
                    DeleteAccountDatabaseBackups(databaseData.Id);

                _accountDatabaseChangeStateProvider.ChangeState(databaseData, DatabaseState.DeletedFromCloud, null);
                LogEventHelper.LogEvent(_dbLayer, _accessProvider.ContextAccountId, _accessProvider, LogActions.DeleteAccountDatabase,
                    $"База {databaseData.V82Name} полностью удалена с облака по истечению 30 дней с момента удаления");
            }
        }

        /// <summary>
        /// Удалить бэкапы инф базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        private void DeleteAccountDatabaseBackups(Guid accountDatabaseId)
        {
            var accountDatabaseBackups = _accountDatabaseDataProvider.GetAccountDatabaseBackups(accountDatabaseId).ToList();

            accountDatabaseBackups.ForEach(backup =>
            {
                _logger.Info($"Удаление бэкапа {backup.BackupPath} инф. базы {backup.AccountDatabase.V82Name}");
                _mapBackupSourceTypeToDeleteAction[backup.SourceType](backup.Id);
            });
        }

    }
}
