﻿using Clouds42.AccountDatabase.Contracts.Delete.Interfaces;
using Clouds42.AccountDatabase.Delete.Helpers;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Account;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Delete.Providers
{
    /// <summary>
    /// Провайдер уведомления пользователей о скором удалении данных
    /// </summary>
    internal class BeforeArchiveDataNotificationProvider(
        InactiveAccountNotificationDataHelper inactiveAccountNotificationDataHelper,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILogger42 logger,
        IHandlerException handlerException)
        : IBeforeArchiveDataNotificationProvider
    {
        /// <summary>
        /// Отправить уведомления
        /// </summary>
        public void SendNotifications()
        {
            try
            {
                var accountsForSendNotifications =
                    inactiveAccountNotificationDataHelper.GetAccountForSendNotificationBeforeDeleteData();
                var demoAccountsForSendNotifications =
                    inactiveAccountNotificationDataHelper.GetDemoAccountsForSendNotificationBeforeDeleteData();

                SendNotifications(accountsForSendNotifications, false);
                SendNotifications(demoAccountsForSendNotifications, true);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка отправки уведомлений о скором удалении данных]");
                throw;
            }
        }

        /// <summary>
        /// Отправить уведомления
        /// </summary>
        /// <param name="beforeArchiveDbModels">Список аккаунтов для отправки</param>
        /// <param name="isDemo">Признак что аккаунты демо</param>
        private void SendNotifications(List<BeforeArchiveDbModelDto> beforeArchiveDbModels, bool isDemo)
        {
            logger.Trace(
                $"Количество {(isDemo ? "демо" : "")} аккаунтов для отправки уведомления о скорой архивации данных: {beforeArchiveDbModels.Count}");

            beforeArchiveDbModels.ForEach(model =>
            {
                var daysToArchive = inactiveAccountNotificationDataHelper.GetDaysToArchiveCount(model, isDemo);

                if (daysToArchive < 1)
                    return;

                model.DaysToArchive = daysToArchive;

                if (isDemo)
                    letterNotificationProcessor
                        .TryNotify<BeforeDeleteAccountDataLetterNotification, BeforeDeleteAccountDataLetterModelDto>(
                            new BeforeDeleteAccountDataLetterModelDto
                            {
                                AccountId = model.AccountId,
                                DaysToDelete = model.DaysToArchive
                            });
                else
                    letterNotificationProcessor
                        .TryNotify<BeforeArchiveAccountDataLetterNotification, BeforeArchiveAccountDataLetterModelDto>(
                            new BeforeArchiveAccountDataLetterModelDto
                            {
                                AccountId = model.AccountId,
                                DaysToArchive = model.DaysToArchive
                            });
            });
        }
    }
}
