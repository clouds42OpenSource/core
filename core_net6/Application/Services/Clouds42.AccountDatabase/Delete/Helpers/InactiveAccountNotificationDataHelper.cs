﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delete.Helpers
{
    /// <summary>
    /// Хэлпер для получения данных неактивных аккаунтов для отправки уведомлений
    /// </summary>
    public class InactiveAccountNotificationDataHelper(IUnitOfWork dbLayer)
    {
        private readonly Lazy<int> _beforeArchiveDataDemoDaysCount = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired.GetBeforeArchiveDataDemoDaysCount);
        private readonly Lazy<int> _beforeArchiveDataDaysCount = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired.GetBeforeArchiveDataDaysCount);
        private readonly Lazy<int> _archiveDemoDays = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired.GetArchiveDemoDays);
        private readonly Lazy<int> _archiveDays = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired.GetArchiveDays);

        /// <summary>
        /// Получить список аккаунтов для отправки уведомлений перед удалением данных
        /// </summary>
        /// <returns>Список аккаунтов для отправки уведомлений перед удалением данных</returns>
        public List<BeforeArchiveDbModelDto> GetAccountForSendNotificationBeforeDeleteData()
        {
            var clearingDateTime = DateTime.Now.AddDays(-_beforeArchiveDataDaysCount.Value);

            return GetAccountsWithExpiredRent1C(clearingDateTime);
        }

        /// <summary>
        /// Получить список демо аккаунтов для отправки уведомлений перед удалением данных
        /// </summary>
        /// <returns>Список демо аккаунтов для отправки уведомлений перед удалением данных</returns>
        public List<BeforeArchiveDbModelDto> GetDemoAccountsForSendNotificationBeforeDeleteData()
        {
            var clearingDemoDateTime = DateTime.Now.AddDays(-_beforeArchiveDataDemoDaysCount.Value);

            return GetDemoAccountsWithExpiredRent1C(clearingDemoDateTime);
        }

        /// <summary>
        /// Получить количество дней до архивации файлов
        /// </summary>
        /// <param name="model">Модель данных перед архивацией некативного аккаунта</param>
        /// <param name="isDemo">Признак что аккаунт демо</param>
        /// <returns>Количество дней до архивации файлов</returns>
        public int GetDaysToArchiveCount(BeforeArchiveDbModelDto model, bool isDemo)
        {
            var archiveDaysCount = isDemo
                ? _archiveDemoDays.Value
                : _archiveDays.Value;

            var dateOfDataArchive = model.Rent1CExpireDate.AddDays(archiveDaysCount);

            var dateDiff = (dateOfDataArchive.Date - DateTime.Now.Date).TotalDays;
            dateDiff = Math.Round(dateDiff, MidpointRounding.AwayFromZero);

            return (int) dateDiff;
        }

        /// <summary>
        /// Получить список демо аккаунтов с просроченной арендой
        /// </summary>
        /// <returns>Список демо аккаунтов с просроченной арендой</returns>
        private List<BeforeArchiveDbModelDto> GetDemoAccountsWithExpiredRent1C(DateTime clearingDateDemo)
        {
            return dbLayer.AccountsRepository
                .AsQueryableNoTracking()
                .Where(x => x.BillingAccount.ResourcesConfigurations.Any(y => y.ExpireDate < clearingDateDemo && y.BillingService.SystemService == Clouds42Service.MyEnterprise) &&
                            x.AccountDatabases.Any(y => y.State != DatabaseState.DeletedFromCloud.ToString()) &&
                            !x.Payments.Any() &&
                            x.ServiceAccount == null)
                .Select(x => new BeforeArchiveDbModelDto
                {
                    AccountId = x.Id,
                    Rent1CExpireDate = x.BillingAccount.ResourcesConfigurations.FirstOrDefault(y => y.BillingService.SystemService == Clouds42Service.MyEnterprise && y.ExpireDate < clearingDateDemo)!.ExpireDate!.Value
                })
                .Distinct()
                .ToList();
        }


        /// <summary>
        /// Получить список аккаунтов с просроченной арендой
        /// </summary>
        /// <returns>Список аккаунтов с просроченной арендой</returns>
        private List<BeforeArchiveDbModelDto> GetAccountsWithExpiredRent1C(DateTime clearingDate)
        {
            return dbLayer.AccountsRepository
                .AsQueryableNoTracking()
                .Where(x => x.BillingAccount.ResourcesConfigurations.Any(y => y.ExpireDate < clearingDate && y.BillingService.SystemService == Clouds42Service.MyEnterprise) &&
                            x.AccountDatabases.Any(y => y.State != DatabaseState.DeletedFromCloud.ToString()) &&
                            x.Payments.Any(y => y.OperationType == PaymentType.Inflow.ToString()) &&
                            x.ServiceAccount == null)
                .Select(x => new BeforeArchiveDbModelDto
                {
                    AccountId = x.Id,
                    Rent1CExpireDate = x.BillingAccount.ResourcesConfigurations.FirstOrDefault(y => y.BillingService.SystemService == Clouds42Service.MyEnterprise && y.ExpireDate < clearingDate)!.ExpireDate!.Value
                })
                .Distinct()
                .ToList();
        }
            
    }
}
