﻿using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Delete.Helpers
{
    /// <summary>
    /// Хэлпер для получения данных для неактивных аккаунтов
    /// </summary>
    public class InactiveAccountDataHelper(IUnitOfWork dbLayer)
    {
        private readonly Lazy<int> _daysCountAfterRent1CExpiredForDemoAccount = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired.GetArchiveDemoDays);
        private readonly Lazy<int> _daysCountAfterRent1CExpiredForAccount = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired
            .GetDaysCountAfterRent1CExpiredForAccountBeforeDeleteData);
        private readonly Lazy<int> _accountsCountForDeleteData = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired
            .GetAccountsCountForDeleteData);

        /// <summary>
        /// Получить список аккаунтов для удаления данных
        /// </summary>
        /// <returns>Список аккаунтов для удаления данных</returns>
        public List<Guid> GetAccountsForDeleteData()
        {
            var nowDate = DateTime.Now;

            var accountsWithExpiredRent1C = GetAccountsAndDemoWithExpiredRent1C(nowDate.AddDays(-_daysCountAfterRent1CExpiredForAccount.Value), nowDate.AddDays(-_daysCountAfterRent1CExpiredForDemoAccount.Value));
          
            return accountsWithExpiredRent1C.Take(_accountsCountForDeleteData.Value).ToList();
        }

        /// <summary>
        /// Получить список аккаунтов с просроченной арендой
        /// </summary>
        /// <returns>Список аккаунтов с просроченной арендой</returns>
        private List<Guid> GetAccountsAndDemoWithExpiredRent1C(DateTime clearingDate, DateTime clearingDateDemo)
        {
            return dbLayer.AccountsRepository
                .AsQueryableNoTracking()
                .Where(x => x.BillingAccount.ResourcesConfigurations.Any(y => (y.ExpireDate < clearingDate || y.ExpireDate < clearingDateDemo) && y.BillingService.SystemService == Clouds42Service.MyEnterprise) && 
                            x.AccountDatabases.Any(y => y.State != DatabaseState.DeletedFromCloud.ToString()) &&
                            x.ServiceAccount == null)
                .Select(x => x.Id)
                .Distinct()
                .ToList();
        }
    }
}
