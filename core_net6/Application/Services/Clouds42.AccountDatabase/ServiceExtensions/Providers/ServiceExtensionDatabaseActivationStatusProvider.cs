﻿using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.CoreWorker.AccountDatabase.Jobs.ManageServiceExtensionDatabaseInMs;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.ServiceExtensions.Providers
{
    /// <summary>
    /// Провайдер для работы со статусом активации
    /// расширения сервиса для инф. базы
    /// </summary>
    internal class ServiceExtensionDatabaseActivationStatusProvider(
        SetServiceExtensionDatabaseActivationStatusJobWrapper setServiceExtensionDatabaseActivationStatusJobWrapper,
        ILogger42 logger,
        IHandlerException handlerException)
        : IServiceExtensionDatabaseActivationStatusProvider
    {
        /// <summary>
        /// Установить статус активации расширения сервиса для инф. базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="isActive">Признак активности</param>
        ///  <param name="isDemo">Признак демо периода</param>
        public void SetActivationStatus(Guid accountId, bool isActive = false, Guid? serviceId = null, bool isDemo = false)
        {
            var msg = "Установка статусов активации расширений сервисов для инф. баз";
            logger.Info(msg);
            try
            {
                var message =
                    $"[{serviceId}]-[{accountId}]-[{isActive}]-Запуск задачи на установку статуса активации расширения сервиса для инф. базы";
                logger.Info(message);
                setServiceExtensionDatabaseActivationStatusJobWrapper.Start(
                    new SetServiceExtensionDatabaseActivationStatusDto
                    {
                        AccountId = accountId,
                        IsActive = isActive,
                        IsDemo = isDemo,
                        ServiceId = serviceId ?? Guid.Empty
                    });

                logger.Info($"{msg} завершилась успешно");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка установки статусов активации расширений сервисов для инф. баз]");
                throw;
            }
        }

    }
}
