﻿using Clouds42.AccountDatabase.Contracts.ManageService.Interfaces;
using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.ServiceExtensions.Providers
{
    /// <summary>
    /// Провайдер для удаления расширения сервиса из информационной базы
    /// </summary>
    internal class DeleteServiceExtensionDatabaseProvider(
        IServiceExtensionDatabaseStateProvider serviceExtensionDatabaseStateProvider,
        IServiceExtensionDatabaseDataProvider serviceExtensionDatabaseDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IDeleteServiceExtensionDatabaseProvider
    {
        /// <summary>
        /// Удалить расширение сервиса из информационной базы
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabase">Модель удаления расширения сервиса из информационной базы</param>
        public void DeleteServiceExtensionFromDatabase(
            DeleteServiceExtensionFromDatabaseDto deleteServiceExtensionFromDatabase)
        {
            var prefix =
                $"[{deleteServiceExtensionFromDatabase.ServiceId}]-[{deleteServiceExtensionFromDatabase.DatabaseId}]-";
             try
             {
                 logger.Info($"{prefix} Удаление расширения сервиса из информационной базы");
             
                 logger.Info($"{prefix} Отправить запрос на удаление расширения сервиса из информационной базы в МС");
             
                 serviceExtensionDatabaseStateProvider.DeleteServiceExtensionDatabase(new ManageServiceExtensionDatabaseStateDto
                 {
                     ServiceId = deleteServiceExtensionFromDatabase.ServiceId,
                     AccountDatabaseId = deleteServiceExtensionFromDatabase.DatabaseId
                 });
             
                 logger.Info($"{prefix} Удаление расширения сервиса из информационной базы завершилось успешно");
             }
             catch (Exception ex)
             {
                 handlerException.Handle(ex,
                     $"[Ошибка удаления расширения сервиса из информационной базы] serviceId-Databaseid: {prefix} ");
                 throw;
             }
        }

        /// <summary>
        /// Удалить расширение сервиса из области информационной базы
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabaseZone">Модель удаления расширения сервиса
        /// из области информационной базы</param>
        public void DeleteServiceExtensionFromDatabaseZone(
            DeleteServiceExtensionFromDatabaseZoneDto deleteServiceExtensionFromDatabaseZone) =>
            DeleteServiceExtensionFromDatabase(new DeleteServiceExtensionFromDatabaseDto
            {
                ServiceId = deleteServiceExtensionFromDatabaseZone.ServiceId,
                DatabaseId =
                    serviceExtensionDatabaseDataProvider
                        .GetDatabaseIdForManageServiceExtensionByZone(deleteServiceExtensionFromDatabaseZone
                            .DatabaseZone)
            });
    }
}
