﻿using Clouds42.AccountDatabase.Contracts.ManageService.Interfaces;
using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.DataModels.billing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ServiceExtensions.Providers
{
    /// <summary>
    /// Провайдер для установки расширения сервиса для информационной базы
    /// </summary>
    internal class InstallServiceExtensionDatabaseProvider(
        IUnitOfWork dbLayer,
        IServiceExtensionDatabaseStateProvider serviceExtensionDatabaseStateProvider,
        IServiceExtensionDatabaseDataProvider serviceExtensionDatabaseDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IInstallServiceExtensionDatabaseProvider
    {
        /// <summary>
        /// Попытаться установить сервис для созданной инф. базы
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        public void TryInstalExtensionForCreatedDatabase(Guid serviceId, Guid accountDatabaseId)
        {
            try
            {
                var billingService = GetBillingServiceOrThrowNewException(serviceId);
                if (!billingService.CanInstallServiceExtension())
                    return;
                var accountDatabase = GetAccountDatabase(accountDatabaseId);
                if (accountDatabase == null)
                    return;

                InstallServiceExtensionForDatabase(serviceId, accountDatabaseId, true);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка установки сервиса] {serviceId} для созданной по шаблону базы {accountDatabaseId}");
            }
        }

        /// <summary>
        /// Установить расширение сервиса для информационных баз
        /// </summary>
        /// <param name="installServiceExtensionModel">Модель установки расширения сервиса
        /// для информационных баз</param>
        public void InstallServiceExtensionForDatabases(
            InstallServiceExtensionForDatabasesDto installServiceExtensionModel) =>
            installServiceExtensionModel.DatabaseIds.ForEach(dbId =>
                InstallServiceExtensionForDatabase(installServiceExtensionModel.ServiceId, dbId));

        /// <summary>
        /// Установить расширение сервиса для информационной базы
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="needDelay">Признак необходимости создавать задачу с задержкой выполнения</param>
        public void InstallServiceExtensionForDatabase(Guid serviceId, Guid databaseId, bool needDelay = false)
        {
            var prefix = $"[{serviceId}]-[{databaseId}]-";

            try
            {
                logger.Info($"{prefix} Установка расширения сервиса для информационной базы");
                logger.Info($"{prefix}  Отправить запрос на установку расширения сервиса для информационной базы в МС");
          
                serviceExtensionDatabaseStateProvider.InstallServiceExtensionDatabase(CreateParams(serviceId, databaseId));
          
                logger.Info($"{prefix} Установка расширения сервиса для информационной базы завершилась успешна");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка установки расширения сервиса] serviceId-DatabaseId{prefix} ");
                throw;
            }

        }

        /// <summary>
        /// Установить расширение сервиса для области информационной базы
        /// </summary>
        /// <param name="installServiceExtensionModel">Модель данных для установки расширения сервиса
        /// для области инф. базы</param>
        public void InstallServiceExtensionForDatabaseZone(
            InstallServiceExtensionForDatabaseZoneDto installServiceExtensionModel) =>
            InstallServiceExtensionForDatabase(installServiceExtensionModel.ServiceId,
                serviceExtensionDatabaseDataProvider.GetDatabaseIdForManageServiceExtensionByZone(installServiceExtensionModel.DatabaseZone));


        /// <summary>
        /// Получить сервис биллинга
        /// или выкинуть ошибку
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        private BillingService GetBillingServiceOrThrowNewException(Guid billingServiceId) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(x => x.Id == billingServiceId) ??
            throw new NotFoundException($"Сервис '{billingServiceId}' не найден!");

        /// <summary>
        /// Получить информационную базу.
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <returns>Модель инф. базы</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId) =>
            dbLayer.DatabasesRepository.FirstOrDefault(acDb => acDb.Id == accountDatabaseId);


        /// <summary>
        /// Создать параметры запроса на установку расширения
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="databaseId">ID базы</param>
        /// <returns>Параметры запроса на установку расширения</returns>
        private static ManageServiceExtensionDatabaseStateDto CreateParams(Guid serviceId, Guid databaseId)
            => new()
            {
                ServiceId = serviceId,
                AccountDatabaseId = databaseId
            };
    }
}
