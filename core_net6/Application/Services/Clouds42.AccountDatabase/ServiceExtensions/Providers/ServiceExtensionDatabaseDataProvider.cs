﻿using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.ServiceExtensions.Providers
{
    /// <summary>
    /// Провайдер для работы с данными расширения сервиса
    /// для информационной базы
    /// </summary>
    internal class ServiceExtensionDatabaseDataProvider(
        IUnitOfWork dbLayer,
        IGetServiceExtensionDatabaseStatusCommand getServiceExtensionDatabaseStatusCommand,
        IGetServicesExtensionForDatabaseCommand getServicesExtensionForDatabaseCommand,
        IGetDatabasesForServicesExtensionCommand getDatabasesForServicesExtensionCommand,
        ILogger42 logger)
        : IServiceExtensionDatabaseDataProvider
    {
        /// <summary>
        /// Получить список баз аккаунта, в которые установлено расширение сервиса
        /// </summary>
        /// <param name="accountId">Id аккауна</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список баз аккаунта, в которые установлено расширение сервиса</returns>
        public IQueryable<AvailableDatabaseForService> GetAccountServiceExtensionDatabases(Guid accountId, Guid serviceId)
        {
            var databasesForServices = getDatabasesForServicesExtensionCommand.Execute(accountId, serviceId);
            if (databasesForServices == null || !databasesForServices.Any())
                return new List<AvailableDatabaseForService>().AsQueryable();

            var databaseIDs = databasesForServices.Select(c => c.AccountDatabaseID).ToList();
            var databaseAccount = dbLayer.DatabasesRepository.Where(db =>
                    (db.State == DatabaseState.Ready.ToString() || db.State == DatabaseState.NewItem.ToString()) &&
                    db.AccountId == serviceId &&
                    db.AccountDatabaseOnDelimiter != null &&
                    databaseIDs.Contains(
                        db.Id)).ToList();

            var model = (from db in databaseAccount
                         join temp in dbLayer.DbTemplateRepository.WhereLazy() on db.TemplateId equals temp.Id
                         join dbSer in databasesForServices on db.Id equals dbSer.AccountDatabaseID
                         select new AvailableDatabaseForService
                         {
                             Caption = db.Caption,
                             ServiceId = serviceId,
                             Id = db.Id,
                             DatabaseStateString = db.State,
                             TemplateName = temp.Name,
                             DatabaseImageCssClass = temp.ImageUrl,
                             ExtensionLastActivityDate = DateTime.Now,
                             ExtensionState = dbSer.AccountDatabaseServiceState,
                             IsInstalled = dbSer.AccountDatabaseServiceState is ServiceExtensionDatabaseStatusEnum.DoneInstall or ServiceExtensionDatabaseStatusEnum.ProcessingInstall or ServiceExtensionDatabaseStatusEnum.ProcessingDelete,
                             Error = dbSer.Error
                         }).AsQueryable();

            return model;
        }

        /// <summary>
        /// Получить информационные базы
        /// для сервиса 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="configurations"></param>
        /// <returns>Список инф. баз</returns>
        public List<Domain.DataModels.AccountDatabase> GetDatabasesForServiceExtension(Guid accountId, List<string> configurations)
        {
            return dbLayer.DatabasesRepository.Where(db =>
                    (db.State == DatabaseState.Ready.ToString() || db.State == DatabaseState.NewItem.ToString()) &&
                    db.AccountId == accountId &&
                    db.AccountDatabaseOnDelimiter != null &&
                    configurations.Contains(
                        db.AccountDatabaseOnDelimiter.DbTemplateDelimiter.ConfigurationId)).ToList();

        }

        /// <summary>
        /// Получить Id информационной базы по номеру области
        /// для управления расширением
        /// </summary>
        /// <param name="zone">Номер области базы</param>
        /// <returns>Id информационной базы</returns>
        public Guid GetDatabaseIdForManageServiceExtensionByZone(int zone) =>
            dbLayer.AccountDatabaseDelimitersRepository
                .FirstOrDefault(db =>
                    db.AccountDatabase.State == DatabaseState.Ready.ToString() && db.Zone == zone)
                ?.AccountDatabaseId ??
            throw new NotFoundException($"не удалось найти базу на разделителях по номеру зоны '{zone}'");

        /// <summary>
        /// Получить список расширений сервиса,
        /// совместимых с инф. базой по фильтру
        /// </summary>
        /// <param name="filter">Модель фильтра на получение
        /// подходящих расширений сервиса для инф. базы</param>
        /// <returns>Список расширений сервиса,
        /// совместимых с инф. базой</returns>
        public async Task<List<AvailableServiceExtensionForDatabaseDto>> GetCompatibleWithDatabaseServiceExtensions(AvailableServiceExtensionsForDatabaseFilterDto filter) 
        {
            List<AvailableServiceExtensionForDatabaseDto> result = [];

            var model = await getServicesExtensionForDatabaseCommand.Execute(filter.AccountDatabaseId);

            model = GetDatabaseServiceExtensions(model, filter.AccountId);

            result = filter.ServiceExtensionsForDatabaseType switch
            {
                ServiceExtensionsForDatabaseTypeEnum.Installed => model.Where(s => s is { IsInstalled: true, ExtensionState: ServiceExtensionDatabaseStatusEnum.DoneInstall or ServiceExtensionDatabaseStatusEnum.ProcessingInstall }).ToList(),
                ServiceExtensionsForDatabaseTypeEnum.Deleted => model.Where(s => s is { IsInstalled: true, ExtensionState: ServiceExtensionDatabaseStatusEnum.DoneDelete or ServiceExtensionDatabaseStatusEnum.ProcessingDelete or ServiceExtensionDatabaseStatusEnum.NotInstalled }).ToList(),
                ServiceExtensionsForDatabaseTypeEnum.Available => model.Where(s => !s.IsInstalled).ToList(),
                _ => model,
            };
            return string.IsNullOrEmpty(filter.ServiceName) ? result : result.Where(s=> s.Name.ToLower().Contains(filter.ServiceName.ToLower())).ToList();
        }

        /// <summary>
        /// Получить список активности сервиса,
        /// совместимых с инф. базой
        /// </summary>
        /// <param name="models">Модель список расширений сервиса</param>
        /// <param name="accountId">идентификатор аккаунта</param>
        /// <returns>Список расширений сервиса,
        /// совместимых с инф. базой</returns>
        public List<AvailableServiceExtensionForDatabaseDto> GetDatabaseServiceExtensions(List<AvailableServiceExtensionForDatabaseDto> models, Guid accountId)
        {
            logger.Trace($"Начинаем получение информации о сервисах для аккаунта {accountId}");

            var resources = dbLayer.ResourceRepository
                .AsQueryableNoTracking()
                .Include(x => x.BillingServiceType)
                .Where(r => r.AccountId == accountId)
                .ToList();

            logger.Trace($"тут получили ресурсы по аккаунту {accountId}");

            var resourcesConfig = dbLayer.ResourceConfigurationRepository
                .Where(r =>  r.AccountId == accountId)
                .ToList();

            logger.Trace($"тут получили конфигурации по аккаунту {accountId}");

            foreach (var service in models)
            {
                var resourceConfig = resourcesConfig.FirstOrDefault(s => s.BillingServiceId == service.Id);
                service.IsFrozenService = resourceConfig != null && (resourceConfig.FrozenValue || (resourceConfig.ExpireDate < DateTime.Now && resourceConfig.IsDemoPeriod)) ;
                service.IsActiveService = !(resourceConfig == null || resources.All(x => x.BillingServiceType.ServiceId != service.Id));
            }

            return models;
        }


        /// <summary>
        /// Получить статус расширения сервиса для информационной базы
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        /// <returns>Статус расширения</returns>
        public ServiceExtensionDatabaseStatusDto GetServiceExtensionDatabaseStatus(Guid serviceId,
            Guid accountDatabaseId) => getServiceExtensionDatabaseStatusCommand.Execute(serviceId, accountDatabaseId);
    }
}
