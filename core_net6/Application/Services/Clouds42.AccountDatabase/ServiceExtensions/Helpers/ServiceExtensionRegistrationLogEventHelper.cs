﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ServiceExtensions.Helpers
{
    /// <summary>
    /// Хэлпер для записи результата активации сервиса в инф. базе в лог
    /// </summary>
    public class ServiceExtensionRegistrationLogEventHelper(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IBillingServiceDataProvider billingServiceDataProvider)
    {
        /// <summary>
        /// Записать результат активации в лог
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="serviceActivationResult">Результат активации</param>
        public void WriteActivationToLog(Guid accountId, Guid accountDatabaseId, Guid serviceId)
        {
            var accountDatabase = GetAccountDatabase(accountDatabaseId);
            var service = billingServiceDataProvider.GetBillingServiceOrThrowException(serviceId);
            var message =
                $"Cервис \"{service.Name}\" " +
                $"установлен в базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}";

            LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.ActivateServiceInDatabase, message);
        }

        /// <summary>
        /// Получить инф. базу по ID
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Инф. база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId)
            => dbLayer.DatabasesRepository.GetAccountDatabase(accountDatabaseId) ??
               throw new NotFoundException($"Инф. база по ID {accountDatabaseId} не найдена");
    }
}
