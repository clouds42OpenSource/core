﻿using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ServiceExtensions.Managers
{
    /// <summary>
    /// Менеджер для работы с данными расширения сервиса
    /// для информационной базы
    /// </summary>
    public class ServiceExtensionDatabaseDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IServiceExtensionDatabaseDataProvider serviceExtensionDatabaseDataProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить статус расширения сервиса для информационной базы
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        /// <returns>Статус расширения</returns>
        public ManagerResult<ServiceExtensionDatabaseStatusDto> GetServiceExtensionDatabaseStatus(Guid serviceId,
            Guid accountDatabaseId)
        {
            var message = $"Получение статуса расширения сервиса '{serviceId}' для инф. базы {accountDatabaseId}";
            try
            {
                AccessProvider.HasAccess(ObjectAction.GetServiceExtensionDatabase,
                    () => AccountIdByAccountDatabase(accountDatabaseId));

                var status =
                    serviceExtensionDatabaseDataProvider.GetServiceExtensionDatabaseStatus(serviceId, accountDatabaseId);

                logger.Info($"{message} завершилось успешно");
                return Ok(status);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилось с ошибкой]");
                return PreconditionFailed<ServiceExtensionDatabaseStatusDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список расширений сервиса,
        /// совместимых с инф. базой по фильтру
        /// </summary>
        /// <param name="filter">Модель фильтра на получение
        /// подходящих расширений сервиса для инф. базы</param>
        /// <returns>Список расширений сервиса,
        /// совместимых с инф. базой</returns>
        public async Task<ManagerResult<List<AvailableServiceExtensionForDatabaseDto>>> GetCompatibleWithDatabaseServiceExtensions(AvailableServiceExtensionsForDatabaseFilterDto filter)
        {
            var message =
                $"Получение списка расширений сервисов, совместимых с инф. базой {filter.AccountDatabaseId} для аккаунта {filter.AccountId}";

            try
            {
                AccessProvider.HasAccess(ObjectAction.GetServiceExtensionDatabase,
                    () => filter.AccountId);

                var data = await serviceExtensionDatabaseDataProvider.GetCompatibleWithDatabaseServiceExtensions(filter);
                logger.Info($"{message} завершилось успешо");

                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилось с ошибкой]");
                return PreconditionFailed<List<AvailableServiceExtensionForDatabaseDto>>(ex.Message);
            }
        }
    }
}
