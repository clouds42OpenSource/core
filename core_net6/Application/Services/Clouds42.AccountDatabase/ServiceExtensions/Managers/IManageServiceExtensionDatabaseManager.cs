﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountDatabase.ServiceExtensions.Managers;

public interface IManageServiceExtensionDatabaseManager
{
    /// <summary>
    /// Установить расширение сервиса для информационных баз
    /// </summary>
    /// <param name="args">Модель установки расширения сервиса для информационных баз</param>
    ManagerResult InstallServiceToDatabase(InstallServiceExtensionToDatabaseDto args);

    /// <summary>
    /// Установить расширение сервиса для информационных баз
    /// </summary>
    /// <param name="installServiceExtensionModel">Модель установки расширения сервиса
    /// для информационных баз</param>
    ManagerResult InstallServiceExtensionForDatabases(
        InstallServiceExtensionForDatabasesDto installServiceExtensionModel);

    /// <summary>
    /// Установить расширение сервиса для информационной базы
    /// </summary>
    /// <param name="installServiceExtensionModel">Модель установки расширения сервиса
    /// для информационной базы</param>
    ManagerResult InstallServiceExtensionForDatabase(
        InstallServiceExtensionForDatabaseDto installServiceExtensionModel);

    /// <summary>
    /// Установить расширение сервиса для области информационной базы
    /// </summary>
    /// <param name="installServiceExtensionModel">Модель данных для установки расширения сервиса
    /// для области инф. базы</param>
    ManagerResult InstallServiceExtensionForDatabaseZone(
        InstallServiceExtensionForDatabaseZoneDto installServiceExtensionModel);

    /// <summary>
    /// Удалить расширение сервиса из информационной базы
    /// </summary>
    /// <param name="serviceId">ID сервиса для удаления из базы</param>
    /// <param name="accountDatabaseId">ID базы у которой удалить сервис</param>
    ManagerResult DeleteServiceFromDatabase(Guid serviceId, Guid accountDatabaseId);

    /// <summary>
    /// Удалить расширение сервиса из информационной базы
    /// </summary>
    /// <param name="deleteServiceExtensionFromDatabase">Модель удаления расширения сервиса
    /// из информационной базы</param>
    ManagerResult DeleteServiceExtensionFromDatabase(
        DeleteServiceExtensionFromDatabaseDto deleteServiceExtensionFromDatabase);

    /// <summary>
    /// Удалить расширение сервиса из области информационной базы
    /// </summary>
    /// <param name="deleteServiceExtensionFromDatabaseZone">Модель удаления расширения сервиса
    /// из области информационной базы</param>
    ManagerResult DeleteServiceExtensionFromDatabaseZone(
        DeleteServiceExtensionFromDatabaseZoneDto deleteServiceExtensionFromDatabaseZone);

    /// <summary>
    /// Записать результат активации расширения в лог
    /// </summary>
    /// <param name="accountDatabaseId">ID инф. базы</param>
    /// <param name="serviceActivationResult">Результат активации</param>
    ManagerResult WriteServiceExtensionDatabaseActivationToLog(Guid accountDatabaseId, Guid serviceId);
}
