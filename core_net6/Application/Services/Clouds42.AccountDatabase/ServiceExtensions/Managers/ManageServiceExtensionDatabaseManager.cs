﻿using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.AccountDatabase.ServiceExtensions.Helpers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ServiceExtensions.Managers
{
    /// <summary>
    /// Менеджер для управления расширением сервиса для информационной базы
    /// </summary>
    public class ManageServiceExtensionDatabaseManager : BaseManager, IManageServiceExtensionDatabaseManager
    {
        private readonly IDeleteServiceExtensionDatabaseProvider _deleteServiceExtensionDatabaseProvider;
        private readonly IInstallServiceExtensionDatabaseProvider _installServiceExtensionDatabaseProvider;
        private readonly IHandlerException _handlerException;
        private readonly ServiceExtensionRegistrationLogEventHelper _serviceExtensionRegistrationLogEventHelper;
        private readonly ILogger42 _logger;

        public ManageServiceExtensionDatabaseManager(IAccessProvider accessProvider, 
            IUnitOfWork dbLayer,
            IDeleteServiceExtensionDatabaseProvider deleteServiceExtensionDatabaseProvider,
            IInstallServiceExtensionDatabaseProvider installServiceExtensionDatabaseProvider,
            IHandlerException handlerException, 
            ServiceExtensionRegistrationLogEventHelper serviceExtensionRegistrationLogEventHelper,
            IBillingServiceDataProvider billingServiceDataProvider,
            ILogger42 logger) : base(accessProvider, dbLayer, handlerException)
        {
            _deleteServiceExtensionDatabaseProvider = deleteServiceExtensionDatabaseProvider;
            _installServiceExtensionDatabaseProvider = installServiceExtensionDatabaseProvider;
            _handlerException = handlerException;
            _serviceExtensionRegistrationLogEventHelper = serviceExtensionRegistrationLogEventHelper;
            _logger = logger;
        }

        /// <summary>
        /// Установить расширение сервиса для информационных баз
        /// </summary>
        /// <param name="args">Модель установки расширения сервиса для информационных баз</param>
        public ManagerResult InstallServiceToDatabase(InstallServiceExtensionToDatabaseDto args)
            => InstallServiceExtensionForDatabases(new InstallServiceExtensionForDatabasesDto
            {
                ServiceId = args.ServiceId,
                DatabaseIds = [args.DatabaseId]
            });


        /// <summary>
        /// Установить расширение сервиса для информационных баз
        /// </summary>
        /// <param name="installServiceExtensionModel">Модель установки расширения сервиса
        /// для информационных баз</param>
        public ManagerResult InstallServiceExtensionForDatabases(
            InstallServiceExtensionForDatabasesDto installServiceExtensionModel)
        {
            var message = $"Установка расширения сервиса '{installServiceExtensionModel.ServiceId}' для информационных баз";
            try
            {
                AccessProvider.HasAccess(ObjectAction.ManageServiceExtensionDatabase,
                    () => AccessProvider.ContextAccountId);

                _installServiceExtensionDatabaseProvider.InstallServiceExtensionForDatabases(
                    installServiceExtensionModel);
                _logger.Trace($"{message} завершилась успешно");

                WriteServiceExtensionDatabaseActivationToLog(installServiceExtensionModel.DatabaseIds.FirstOrDefault(), installServiceExtensionModel.ServiceId);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилась с ошибкой]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Установить расширение сервиса для информационной базы
        /// </summary>
        /// <param name="installServiceExtensionModel">Модель установки расширения сервиса
        /// для информационной базы</param>
        public ManagerResult InstallServiceExtensionForDatabase(
            InstallServiceExtensionForDatabaseDto installServiceExtensionModel)
        {
            var message =
                $"Установка расширения сервиса '{installServiceExtensionModel.ServiceId}' для информационной базы '{installServiceExtensionModel.DatabaseId}'";
            try
            {
                AccessProvider.HasAccess(ObjectAction.ManageServiceExtensionDatabase,
                    () => AccessProvider.ContextAccountId);

                _installServiceExtensionDatabaseProvider.InstallServiceExtensionForDatabase(
                    installServiceExtensionModel.ServiceId, installServiceExtensionModel.DatabaseId);

                _logger.Trace($"{message} завершилась успешно");
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилась с ошибкой]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Установить расширение сервиса для области информационной базы
        /// </summary>
        /// <param name="installServiceExtensionModel">Модель данных для установки расширения сервиса
        /// для области инф. базы</param>
        public ManagerResult InstallServiceExtensionForDatabaseZone(
            InstallServiceExtensionForDatabaseZoneDto installServiceExtensionModel)
        {
            var message =
                $@"Установка расширения сервиса '{installServiceExtensionModel.ServiceId}' 
                для области информационной базы '{installServiceExtensionModel.DatabaseZone}'";
            try
            {
                AccessProvider.HasAccess(ObjectAction.ManageServiceExtensionDatabase,
                    () => AccessProvider.ContextAccountId);

                _installServiceExtensionDatabaseProvider.InstallServiceExtensionForDatabaseZone(
                    installServiceExtensionModel);

                _logger.Trace($"{message} завершилась успешно");
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилась с ошибкой]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить расширение сервиса из информационной базы
        /// </summary>
        /// <param name="serviceId">ID сервиса для удаления из базы</param>
        /// <param name="accountDatabaseId">ID базы у которой удалить сервис</param>
        public ManagerResult DeleteServiceFromDatabase(Guid serviceId, Guid accountDatabaseId)
            => DeleteServiceExtensionFromDatabase(new DeleteServiceExtensionFromDatabaseDto
            {
                ServiceId = serviceId,
                DatabaseId = accountDatabaseId
            });

        /// <summary>
        /// Удалить расширение сервиса из информационной базы
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabase">Модель удаления расширения сервиса
        /// из информационной базы</param>
        public ManagerResult DeleteServiceExtensionFromDatabase(
            DeleteServiceExtensionFromDatabaseDto deleteServiceExtensionFromDatabase)
        {
            var message =
                $"Удаление расширения сервиса '{deleteServiceExtensionFromDatabase.ServiceId}' из информационной базы '{deleteServiceExtensionFromDatabase.DatabaseId}'";
            try
            {
                AccessProvider.HasAccess(ObjectAction.ManageServiceExtensionDatabase,
                    () => AccessProvider.ContextAccountId);

                _deleteServiceExtensionDatabaseProvider.DeleteServiceExtensionFromDatabase(
                    deleteServiceExtensionFromDatabase);

                _logger.Trace($"{message} завершилось успешно");
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилось с ошибкой]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить расширение сервиса из области информационной базы
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabaseZone">Модель удаления расширения сервиса
        /// из области информационной базы</param>
        public ManagerResult DeleteServiceExtensionFromDatabaseZone(
            DeleteServiceExtensionFromDatabaseZoneDto deleteServiceExtensionFromDatabaseZone)
        {
            var message =
                $@"Удаление расширения сервиса '{deleteServiceExtensionFromDatabaseZone.ServiceId}' 
                из области информационной базы '{deleteServiceExtensionFromDatabaseZone.DatabaseZone}'";
            try
            {
                AccessProvider.HasAccess(ObjectAction.ManageServiceExtensionDatabase,
                    () => AccessProvider.ContextAccountId);

                _deleteServiceExtensionDatabaseProvider.DeleteServiceExtensionFromDatabaseZone(
                    deleteServiceExtensionFromDatabaseZone);

                _logger.Trace($"{message} завершилось успешно");
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилось с ошибкой]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Записать результат активации расширения в лог
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="serviceId">ID сервиса</param>
        public ManagerResult WriteServiceExtensionDatabaseActivationToLog(Guid accountDatabaseId, Guid serviceId)
        {
            try
            {
                var accountId = AccessProvider.ContextAccountId;
                _serviceExtensionRegistrationLogEventHelper.WriteActivationToLog(accountId, accountDatabaseId, serviceId);
                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
