﻿using Clouds42.AccountDatabase.Contracts.Merge.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.UploadedFiles.Helpers;

namespace Clouds42.AccountDatabase.Merge.Providers
{
    /// <summary>
    /// Провайдер для склейки файлов
    /// </summary>
    internal class MergeFileProvider(
        IUnitOfWork dbLayer,
        FileSizeCalculator fileSizeCalculator,
        ILogger42 logger,
        IHandlerException handlerException)
        : IMergeFileProvider
    {
        private readonly Lazy<string> _partToken = new(
            CloudConfigurationProvider.AccountDatabase.UploadFiles.GetPartTokenNameForChunkOfFile);

        /// <summary>
        /// Склеить части в исходный файл
        /// </summary>
        /// <param name="mergeChunksToInitialFileParams">Параметры склейки</param>
        public void MergeChunksToInitialFile(MergeChunksToInitialFIleParamsDto mergeChunksToInitialFileParams)
        {
            var uploadedFile = GetUploadedFile(mergeChunksToInitialFileParams.UploadedFileId);
            var folderForChunksPath = mergeChunksToInitialFileParams.PathForFileChunks;

            var uploadedFileName = uploadedFile.FileName;
            var mergedFilePath = uploadedFile.FullFilePath;

            var filesList = GetFileChunksList(folderForChunksPath, uploadedFileName);
            logger.Trace($"Путь до папки с чанком: {folderForChunksPath}\n" +
                $"UploadFileName: {uploadedFileName}\n" +
                $"mergerdFilePath: {mergedFilePath}\n" +
                $"filesList.Count: {filesList.Count},  количество отправленных чанков: {mergeChunksToInitialFileParams.CountOfFileChunks} ");

            if (filesList.Count != mergeChunksToInitialFileParams.CountOfFileChunks)
            {
                var message = $"Получены не все части файла {uploadedFile.FileName} для склейки";
                UpdateUploadedFileStatus(mergeChunksToInitialFileParams.UploadedFileId, UploadedFileStatus.UploadFailed, message);
                throw new InvalidOperationException(message);
            }

            if (File.Exists(mergedFilePath))
                File.Delete(mergedFilePath);

            var mergeList = GetFileMergeList(filesList);

            MergeChunksToInitialFile(mergeList, mergeChunksToInitialFileParams.UploadedFileId);
        }

        /// <summary>
        /// Получить список частей для склейки
        /// </summary>
        /// <param name="filesList">Список файлов в директории</param>
        /// <returns>Список частей для склейки</returns>
        private List<AccountDatabaseSortedFileDto> GetFileMergeList(List<string> filesList)
        {
            var mergeList = new List<AccountDatabaseSortedFileDto>();

            foreach (var file in filesList)
            {
                var sortedFile = new AccountDatabaseSortedFileDto
                {
                    FileName = file
                };

                var trailingTokens = GetTrailingTokens(file);

                int.TryParse(trailingTokens[..trailingTokens.IndexOf(".", StringComparison.Ordinal)], out var fileIndex);
                sortedFile.FileOrder = fileIndex;
                mergeList.Add(sortedFile);
            }

            return mergeList;
        }

        /// <summary>
        /// Получить название конченого токена
        /// </summary>
        /// <param name="initialName">Исходное имя</param>
        /// <returns>Название конченого токена</returns>
        private string GetTrailingTokens(string initialName)
            => initialName[(initialName.IndexOf(_partToken.Value, StringComparison.Ordinal) + _partToken.Value.Length)..];

        /// <summary>
        /// Получить список частей файла
        /// </summary>
        /// <param name="folderForChunksPath">Путь к частям файла</param>
        /// <param name="uploadedFileName">Имя загруженного файла</param>
        /// <returns>Список частей файла</returns>
        private List<string> GetFileChunksList(string folderForChunksPath, string uploadedFileName)
        {
            var searchPattern = uploadedFileName + _partToken.Value + "*";

            return Directory.GetFiles(folderForChunksPath, searchPattern).ToList();
        }

        /// <summary>
        /// Склеить части в исходный файл
        /// </summary>
        /// <param name="mergeList">Список частей для склейки</param>
        /// <param name="uploadedFileId">ID загружаемого файла</param>
        private void MergeChunksToInitialFile(List<AccountDatabaseSortedFileDto> mergeList, Guid uploadedFileId)
        {
            var mergeOrder = mergeList.OrderBy(file => file.FileOrder).ToList();
            var uploadedFile = GetUploadedFile(uploadedFileId);

            if (!mergeOrder.Any())
            {
                var message = $"Не найдено ни одной части файла {uploadedFile.FileName} для склейки";
                UpdateUploadedFileStatus(uploadedFileId, UploadedFileStatus.UploadFailed, message);
                throw new InvalidOperationException(message);
            }

            using (var fileStream = File.Create(uploadedFile.FullFilePath))
            {
                foreach (var chunk in mergeOrder)
                {
                    try
                    {
                        FileUseChecker.WaitWhileFileUsing(chunk.FileName, waitTimeInSeconds: 120);

                        using var fileChunk = new FileStream(chunk.FileName, FileMode.Open);
                        fileChunk.CopyTo(fileStream);
                    }
                    catch (Exception ex)
                    {
                        handlerException.Handle(ex, $"[Ошибка при добавлении части к исходному файлу]  Часть: {chunk.FileName} ");
                        fileStream.Close();

                        UpdateUploadedFileStatus(uploadedFileId, UploadedFileStatus.UploadFailed, ex.Message);
                        throw;
                    }
                }
            }
            UpdateUploadedFileStatus(uploadedFileId, UploadedFileStatus.UploadSuccess, "");
        }

        /// <summary>
        /// Получить загруженный файл
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Загруженный файл</returns>
        private UploadedFile GetUploadedFile(Guid uploadedFileId)
            => dbLayer.UploadedFileRepository.FirstOrDefault(w => w.Id == uploadedFileId)
                    ?? throw new NotFoundException($"Загружаемый файл по ID {uploadedFileId} не найден");

        /// <summary>
        /// Обновить статус загруженного файла
        /// </summary>
        /// <param name="uploadedFileId"></param>
        /// <param name="status"></param>
        /// <param name="comment">Комментарий</param>
        private void UpdateUploadedFileStatus(Guid uploadedFileId, UploadedFileStatus status, string comment)
        {
            try
            {
                var uploadedFile = dbLayer.UploadedFileRepository.FirstOrDefault(w => w.Id == uploadedFileId);
                uploadedFile.Status = status;
                uploadedFile.Comment = comment;
                uploadedFile.FileSizeInMegabytes = fileSizeCalculator.GetFileSizeInMegabytes(uploadedFile.FullFilePath);

                dbLayer.UploadedFileRepository.Update(uploadedFile);
                dbLayer.Save();

                logger.Trace(
                    $"Успешно обновлен объект загруженного файла. ID: {uploadedFile.Id} AccountId: {uploadedFile.AccountId}" +
                    $"Статус изменен на {uploadedFile.Status}. Комментарий: {comment}");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка обновления загруженного файла] {uploadedFileId}");
                throw;
            }
        }
    }
}
