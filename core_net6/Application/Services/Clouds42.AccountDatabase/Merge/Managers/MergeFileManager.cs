﻿using Clouds42.AccountDatabase.Contracts.Merge.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Merge.Managers
{
    public class MergeFileManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IMergeFileProvider mergeFileProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Склеить части в исходный файл
        /// </summary>
        /// <param name="mergeChunksToInitialFileParams">Параметры склейки</param>
        /// <returns>Ok - если склейка завершена успешно</returns>
        public ManagerResult MergeChunksToInitialFile(MergeChunksToInitialFIleParamsDto mergeChunksToInitialFileParams)
        {
            try
            {
                var accountId = AccountIdByUploadedFile(mergeChunksToInitialFileParams.UploadedFileId);
                AccessProvider.HasAccess(ObjectAction.MergeChunksToInitialFile, () => accountId);

                mergeFileProvider.MergeChunksToInitialFile(mergeChunksToInitialFileParams);

                logger.Trace($"Склейка частей в исходный файл {mergeChunksToInitialFileParams.UploadedFileId} завершена успешно.");
                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    $"[При склейке частей в исходный файл {mergeChunksToInitialFileParams.UploadedFileId} возникла ошибка. Причина: {ex.GetFullInfo()}.]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
