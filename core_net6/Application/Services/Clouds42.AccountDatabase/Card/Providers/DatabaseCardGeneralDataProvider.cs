﻿using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.AccountDatabase.Data.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums.AccountDatabase;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Queries;
using MediatR;

namespace Clouds42.AccountDatabase.Card.Providers
{
    /// <summary>
    /// Провайдер для работы с общими/основными данными
    /// карточки информационной базы
    /// </summary>
    internal class DatabaseCardGeneralDataProvider(ISender sender) : IDatabaseCardGeneralDataProvider
    {

        /// <summary>
        /// Получить модель общих/основных данных
        /// карточки информационной базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель общих/основных данных
        /// карточки информационной базы</returns>
        public DataContracts.AccountDatabase.AccountDatabaseCard.DatabaseCardGeneralDataDto GetData(Guid databaseId)
        {
            var result = sender.Send(new GetAccountDatabaseQuery { AccountDatabaseId = databaseId }).Result;
            if (result.Error)
                throw new NotFoundException(result.Message);

            return TransferToDatabaseCardGeneralDataDto(result.Result);
        }

        /// <summary>
        /// Переложить в модель общих/основных данных
        /// карточки информационной базы
        /// </summary>
        /// <param name="model">Временная модель общих/основных данных
        /// карточки информационной базы</param>
        /// <returns>Модель общих/основных данных
        /// карточки информационной базы</returns>
        private static DataContracts.AccountDatabase.AccountDatabaseCard.DatabaseCardGeneralDataDto TransferToDatabaseCardGeneralDataDto(AccountDatabaseCardResultDto model)
            => new()
            {
                
                TemplatePlatform = model.Database.TemplatePlatform,
                V82Name = model.Database.V82Name,
                IsFile = model.Database?.IsFile == true,
                PlatformType = model.Database.PlatformType,
                DbNumber = model.Database.DbNumber,
                V82Server =model.Database.V82Server,
                SqlServer = model.Database.SqlServer,
                DbTemplate = model.Database.DbTemplate ?? "",
                CanWebPublish = model.Database.CanWebPublish,
                DatabaseState = model.Database.State,
                UsedWebServices = model.Database.UsedWebServices,
                FileStorageId = model.Database.FileStorageId,
                IsDbOnDelimiters = model.Database.IsDbOnDelimiters,
                IsDemoDelimiters = model.Database.IsDemoDelimiters,
                DistributionType = model.Database.DistributionType,
                Alpha83Version = model.Database.Alpha83Version,
                Stable83Version = model.Database.Stable83Version,
                Stable82Version = model.Database.Stable82Version,
                ZoneNumber = model.Database.ZoneNumber,
                RestoreModelType = model.Database.RestoreModelType,
                CanChangeRestoreModel = model.Database.RestoreModelType == AccountDatabaseRestoreModelTypeEnum.Mixed,
                PublishState = model.Database.PublishState,
                Id = model.Database.Id,
                Caption = model.Database.Caption,
                Version = model.Database.Version,
                ConfigurationName = model.Database.DefaultTemplateCaption == AccountDatabaseDataProviderConstants.ClearTemplateName
                    ? AccountDatabaseDataProviderConstants.ClearTemplateConfigurationName
                    : model.Database.ConfigurationName ?? model.Database.DefaultTemplateCaption,
                ConfigurationCode = model.Database.ConfigurationCode,
                HasModifications = model.Database.HasModifications,
                TemplateName = model.Database.DbTemplateDelimiterName ?? model.Database.TemplateName,
                FileStorageName = model.Database.FileStorageName,
                DatabaseConnectionPath = model.Database.DatabaseConnectionPath,
                FilePath = model.Database.FilePath,
                CanEditDatabase = model.Database.CanEditDatabase,
                AvailableFileStorages = model.AccountCardEditModeDictionaries.AvailableFileStorages.Select(x => new KeyValueDto<Guid>(x.Key, x.Value)).ToList(),
                CreationDate = model.Database.CreationDate,
                MyDatabasesServiceTypeId = model.Database.MyDatabasesServiceTypeId,
                ActualDatabaseBackupId = model.Database.ActualDatabaseBackupId,
                IsExistTerminatingSessionsProcessInDatabase = model.Database.IsExistTerminatingSessionsProcessInDatabase,
                IsStatusErrorCreated = model.Database.IsStatusErrorCreated,
                CreateDatabaseComment = model.Database.CreateAccountDatabaseComment,
                SizeInMb = model.Database.SizeInMB,
                CloudStorageWebLink = model.Database.CloudStorageWebLink,
                WebPublishPath = model.Database.WebPublishPath,
                LastEditedDateTime = model.Database.LastEditedDateTime,
                AccountCaption = model.Database.AccountCaption,
                BackupDate = model.Database.BackupDate,
                CalculateSizeDateTime = model.Database.CalculateSizeDateTime,
                ArchivePath = model.Database.ArchivePath,
                CommonDataForWorkingWithDB = model.Database.CommonDataForWorkingWithDB,
                HasAutoUpdate = model.Database.HasAutoUpdate,
            };
    }
}
