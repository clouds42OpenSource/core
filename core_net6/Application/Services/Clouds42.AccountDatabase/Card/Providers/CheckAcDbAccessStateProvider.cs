﻿using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Card.Providers
{
    /// <summary>
    /// Провайдер для проверки статусов доступов к базам
    /// </summary>
    internal class CheckAcDbAccessStateProvider(IUnitOfWork dbLayer) : ICheckAcDbAccessStateProvider
    {
        /// <summary>
        /// Получить статусы доступов
        /// </summary>
        /// <param name="acDbAccessesForCheck">Доступы для проверки</param>
        /// <returns>Статусы доступов</returns>
        public List<CheckAcDbAccessDto> GetAcDbAccessesStates(List<CheckAcDbAccessDto> acDbAccessesForCheck)
        {
            var checkedAccess = new List<CheckAcDbAccessDto>();

            acDbAccessesForCheck.ForEach(access => { checkedAccess.Add(CheckAccessItem(access)); });

            return checkedAccess;
        }

        /// <summary>
        /// Проверить запрашиваемый доступ
        /// </summary>
        /// <param name="accessItem">Доступ для проверки</param>
        /// <returns>Проверенный доступ</returns>
        private CheckAcDbAccessDto CheckAccessItem(CheckAcDbAccessDto accessItem)
        {
            var acDbAccess = dbLayer.AcDbAccessesRepository.FirstOrDefault(w =>
                w.AccountDatabaseID == accessItem.AccountDatabaseId && w.AccountUserID == accessItem.AccountUserId);

            if (accessItem.Operation == UpdateAcDbAccessActionType.Delete && acDbAccess == null)
            {
                accessItem.AccessState = AccountDatabaseAccessState.Done;
                return accessItem;
            }

            if (acDbAccess == null)
            {
                accessItem.AccessState = AccountDatabaseAccessState.Done;
                return accessItem;
            }
            var accessOperation =
                dbLayer.ManageDbOnDelimitersAccessRepository.FirstOrDefault(w => w.Id == acDbAccess.ID);

            accessItem.AccessState = accessOperation?.AccessState ?? AccountDatabaseAccessState.Done;
            accessItem.DelayReason = accessOperation?.ProcessingDelayReason;
            return accessItem;
        }
    }
}
