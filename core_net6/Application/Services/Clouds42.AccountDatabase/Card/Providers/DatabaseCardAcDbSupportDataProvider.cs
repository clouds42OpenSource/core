﻿using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.AccountDatabase.Contracts.History.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Card.Providers
{
    /// <summary>
    /// Провайдер для работы с данными технической поддержки инф. базы
    /// (карточка информационной базы)
    /// </summary>
    internal class DatabaseCardAcDbSupportDataProvider(
        IAcDbSupportHistoryDataProvider acDbSupportHistoryDataProvider,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        AcDbSupportHelper acDbSupportHelper,
        IUnitOfWork unitOfWork)
        : IDatabaseCardAcDbSupportDataProvider
    {
        /// <summary>
        /// Сопоставление состояния авторизации с описанием состояния
        /// </summary>
        private readonly IDictionary<SupportState, string> _mapSupportStateToStateDescription = new Dictionary<SupportState, string>
        {
            {
                SupportState.NotAutorized,
                "Все регламентные операции выполняются в установленное вами время по МСК и только при отсутствии активных сессий пользователя."
            },
            {
                SupportState.AutorizationFail,
                "Ошибка подключения к базе, попробуйте еще раз"
            },
            {
                SupportState.NotValidAuthData,
                SupportMessagesConstsDto.InvalidLoginPasswordFull.LineEndingToHtmlStyle()
            },
            {
                SupportState.AutorizationProcessing,
                "Уважаемый пользователь!\r\n" +
                "Проводится авторизация в информационной базе." +
                "\r\nПроцесс может занять до 30 минут.\r\n" +
                "По завершении процесса авторизации вам будет доступна возможность подключить Автоматическое обновление (АО) и Тестирование и Исправление ошибок (ТиИ)" +
                "\r\nПожалуйста, подождите."
            },
            {
                SupportState.AutorizationSuccess,
                "Авторизация успешно завершена!"
            }
        };

        public AcDbSupport UpdateAuthorizationData(Guid accountDatabaseId, string login, string password)
        {
            var acDbSupport = unitOfWork.AcDbSupportRepository.FirstOrDefault(acdb => acdb.AccountDatabasesID == accountDatabaseId);

            if (acDbSupport == null)
            {
                acDbSupport = new AcDbSupport
                {
                    Login = login,
                    Password = password,
                    State = (int)SupportState.AutorizationProcessing,
                    AccountDatabasesID = accountDatabaseId
                };
                unitOfWork.AcDbSupportRepository.Insert(acDbSupport);
                unitOfWork.Save();
                return acDbSupport;
            }

            acDbSupport.Login = login;
            acDbSupport.Password = login;
            acDbSupport.State = (int)SupportState.AutorizationProcessing;
            unitOfWork.AcDbSupportRepository.Update(acDbSupport);
            unitOfWork.Save();
            return acDbSupport;
        }

        /// <summary>
        /// Получить данные технической поддержки инф. базы
        /// для карточки информационной базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель данных технической поддержки инф. базы</returns>
        public DatabaseCardAcDbSupportDataDto GetData(Guid databaseId)
        {
            var acDbSupport = accountDatabaseDataHelper.GetFirstAcDbSupport(databaseId);

            return acDbSupport is null 
                ? GenetateSupportDataModelWithAbsenceSupport(databaseId) 
                : GenerateDatabaseCardAcDbSupportDataDto(acDbSupport);
        }

        /// <summary>
        /// Сформировать модель данных технической поддержки инф. базы
        /// </summary>
        /// <param name="acDbSupport">Данные технической поддержки</param>
        /// <returns>Модель данных технической поддержки инф. базы</returns>
        private DatabaseCardAcDbSupportDataDto GenerateDatabaseCardAcDbSupportDataDto(AcDbSupport acDbSupport)
        {
            var supportState = (SupportState)acDbSupport.State;

            return new DatabaseCardAcDbSupportDataDto
            {
                Login = acDbSupport.Login,
                Password = acDbSupport.Password,
                DatabaseId = acDbSupport.AccountDatabasesID,
                IsAuthorized = supportState == SupportState.AutorizationSuccess,
                IsConnects = supportState is SupportState.AutorizationSuccess or SupportState.AutorizationProcessing,
                SupportState = supportState,
                LastTehSupportDate = acDbSupport.TehSupportDate,
                SupportStateDescription = acDbSupport.State == (int)SupportState.NotValidAuthData
                    ? acDbSupportHelper.GetInvalidAuthorizationMessage(acDbSupport.AccountDatabasesID).LineEndingToHtmlStyle()
                    : GetSupportStateDescription(supportState),
                AcDbSupportHistories = GetAcDbSupportHistories(acDbSupport.AccountDatabasesID),
                TimeOfUpdate = acDbSupport.TimeOfUpdate,
                CompleteSessioin = acDbSupport.CompleteSessioin,
                HasAcDbSupport = true,
                HasAutoUpdate = acDbSupport.HasAutoUpdate,
                HasSupport = acDbSupport.HasSupport,
                Version = acDbSupport.CurrentVersion,
                HasModifications = acDbSupport.DatabaseHasModifications,
                ConfigurationName = string.IsNullOrEmpty(acDbSupport.ConfigurationName)
                    ? acDbSupport.SynonymConfiguration
                    : acDbSupport.ConfigurationName
            };
        }

        /// <summary>
        /// Сформировать модель данных технической поддержки инф. базы
        /// при отсутвии записи о технической поддержки
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель данных технической поддержки инф. базы</returns>
        private DatabaseCardAcDbSupportDataDto GenetateSupportDataModelWithAbsenceSupport(Guid databaseId) =>
            new()
            {
                DatabaseId = databaseId,
                SupportState = SupportState.NotAutorized,
                SupportStateDescription = GetSupportStateDescription(SupportState.NotAutorized),
                TimeOfUpdate = AutoUpdateTimeEnum.night,
                CompleteSessioin = false,
                HasAcDbSupport = false
            };

        /// <summary>
        /// Получить историю технической поддержки инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>История технической поддержки инф. базы</returns>
        private List<AcDbSupportHistoryInfoDto> GetAcDbSupportHistories(Guid databaseId) =>
            acDbSupportHistoryDataProvider.GetActualAcDbSupportHistories(databaseId)
                .ToList()
                .Select(h => new AcDbSupportHistoryInfoDto
                {
                    Operation = EnumExtensions.GetEnumDescription(h.Operation),
                    Description = h.Description.LineEndingToHtmlStyle(),
                    Date = h.EditDate
                }).ToList();

        /// <summary>
        /// Получить описание состояния авторизации
        /// </summary>
        /// <param name="supportState">Состояния авторизации</param>
        /// <returns>Описание состояния авторизации</returns>
        private string GetSupportStateDescription(SupportState supportState)
        {
            if (!_mapSupportStateToStateDescription.TryGetValue(supportState, out var supportStateDescription))
                return supportState.Description();

            return supportStateDescription;
        }
    }
}
