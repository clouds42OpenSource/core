﻿using Clouds42.AccountDatabase.Card.Utilities;
using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Card.Providers
{
    /// <summary>
    /// Провайдер для работы с данными бэкапов (базы)
    /// карточки инф. базы
    /// </summary>
    internal class DatabaseCardDbBackupsDataProvider(IUnitOfWork dbLayer, IAccessProvider accessProvider)
        : IDatabaseCardDbBackupsDataProvider
    {
        /// <summary>
        /// Получить данные бэкапов (базы)
        /// карточки инф. базы по фильтру
        /// </summary>
        /// <param name="filter">Модель фильтра</param>
        /// <returns>Модель данных бэкапов
        /// карточки инф. базы</returns>
        public DatabaseCardDbBackupsDataDto GetDataByFilter(DatabaseCardDbBackupsFilterDto filter)
        {
            var databaseBackups = GetDatabaseBackupsByFilter(filter);

            return new DatabaseCardDbBackupsDataDto
            {
                DbBackupsCount = databaseBackups.Count(),
                DatabaseBackups = databaseBackups.ToList()
            };
        }

        /// <summary>
        /// Получить бэкапы инф. баз по фильтру
        /// </summary>
        /// <param name="filter">Модель фильтра</param>
        /// <returns></returns>
        private IQueryable<DatabaseBackupDataDto> GetDatabaseBackupsByFilter(DatabaseCardDbBackupsFilterDto filter) =>
            GetDatabaseBackups(filter.DatabaseId).ApplyFilter(filter)
                .FilterByUserGroups(accessProvider.GetUser().Groups);

        /// <summary>
        /// Получить бэкапы инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Бэкапы инф. базы</returns>
        private IQueryable<DatabaseBackupDataDto> GetDatabaseBackups(Guid databaseId) =>
            from database in dbLayer.DatabasesRepository.WhereLazy()
            join databaseBackup in dbLayer.AccountDatabaseBackupRepository.WhereLazy() on database.Id equals
                databaseBackup.AccountDatabaseId
            join initiator in dbLayer.AccountUsersRepository.WhereLazy() on databaseBackup.InitiatorId equals
                initiator.Id
            where database.Id == databaseId
            orderby databaseBackup.CreateDateTime descending, databaseBackup.CreationBackupDateTime descending
            select new DatabaseBackupDataDto
            {
                Id = databaseBackup.Id,
                CreationDate = databaseBackup.CreationBackupDateTime,
                BackupPath = databaseBackup.BackupPath,
                EventTrigger = databaseBackup.EventTrigger,
                Initiator = initiator.Login,
                IsDbOnDelimiters = database.AccountDatabaseOnDelimiter != null,
                SourceType = databaseBackup.SourceType
            };
    }
}