﻿using Clouds42.AccountDatabase.Card.Mappers;
using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;

namespace Clouds42.AccountDatabase.Card.Providers
{
    /// <summary>
    /// Провайдер получения данных информационной базы для админ панели
    /// </summary>
    internal class AccountDatabaseAdminPanelDataProvider(
        IAccessProvider accessProvider,
        IDatabaseCardGeneralDataProvider databaseCardGeneralDataProvider,
        IDatabaseCardDbBackupsDataProvider databaseCardDbBackupsDataProvider)
        : IAccountDatabaseAdminPanelDataProvider
    {
        /// <summary>
        /// Получить данные
        /// </summary>
        /// <param name="database">Модель информационной базы</param>
        /// <returns>Информация об информационной базы</returns>
        public AccountDatabaseInfoDc GetData(Domain.DataModels.AccountDatabase database)
        {
            var generalData = GetGeneralData(database.Id);

            var backups = GetBackups(database.Id);

            return new AccountDatabaseInfoDc
            {
                Database = generalData.MapToAccountDatabaseAdditionalDataDc(),
                AvailableFileStorages = generalData.AvailableFileStorages.MapToAccountDatabaseFileStorageList(),
                DbTemplateList = generalData.CommonDataForWorkingWithDB.AvailableDbTemplates.MapToAccountDatabaseTemplate(),
                AccountDatabaseBackups = backups, 
                CanEditDatabase = CheckAccountDatabaseHelper.CanEditDatabase(accessProvider.GetUser(), database),
                DbBackupsCount = backups.Count
            };
        }

        /// <summary>
        /// Получить обшие данные информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <returns>Модель общих/основных данных карточки информационной базы</returns>
        private DatabaseCardGeneralDataDto GetGeneralData(Guid databaseId) =>
            databaseCardGeneralDataProvider.GetData(databaseId);

        /// <summary>
        /// Получить список бекапов информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <returns>Список бекапов информационной базы</returns>
        private List<AccountDatabaseBackupsDc> GetBackups(Guid databaseId) => databaseCardDbBackupsDataProvider
            .GetDataByFilter(new DatabaseCardDbBackupsFilterDto {DatabaseId = databaseId})
            .MapToAccountDatabaseBackupsDcList();
       
    }
}
