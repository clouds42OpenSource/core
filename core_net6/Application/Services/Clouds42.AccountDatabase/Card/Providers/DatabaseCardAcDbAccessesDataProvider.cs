﻿using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.AccountDatabase.Data.Constants;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountDatabase.Card.Providers
{

    /// <summary>
    /// Провайдер для работы с данными доступов (к инф. базе)
    /// карточки инф. базы
    /// </summary>
    internal class DatabaseCardAcDbAccessesDataProvider(
        IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider,
        IUnitOfWork unitOfWork,
        IConfiguration configuration)
        : IDatabaseCardAcDbAccessesDataProvider
    {


        /// <summary>
        /// Выбрать инф. базы c возможными доступами
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="accountId"></param>
        /// <returns>Внутренние доступы инф. базы</returns>
        public List<AcDbAccessUserDto> SelectDatabaseExternalAccessesForUser(Guid userId, Guid accountId)
        {
            return unitOfWork.AccountUsersRepository
                .AsQueryableNoTracking()
                .Join(unitOfWork.DatabasesRepository.AsQueryable(), z => z.AccountId, x => accountId,
                    (user, database) => new { user, database })
                .Where(x => x.database.State == DatabaseState.Ready.ToString() &&
                            x.database.AcDbAccesses.Any(z => z.AccountID == x.user.AccountId))
                .Select(x => new AcDbAccessUserDto
                {
                    Caption = x.database.Caption,
                    ConfigurationName =
                        x.database.DbTemplate.ConfigurationDbTemplateRelations.FirstOrDefault()!.Configuration1CName ==
                        AccountDatabaseDataProviderConstants.ClearTemplateName
                            ? AccountDatabaseDataProviderConstants.ClearTemplateConfigurationName
                            : x.database.DbTemplate.ConfigurationDbTemplateRelations.FirstOrDefault()!
                                .Configuration1CName,
                    IsDbOnDelimiters = x.database.AccountDatabaseOnDelimiter != null,
                    IsFile = x.database.IsFile.HasValue && x.database.IsFile.Value,
                    DatabaseId = x.database.Id,
                    MyDatabasesServiceTypeId = x.database.DbTemplate.ConfigurationDbTemplateRelations.FirstOrDefault()!.Configuration1C
                            .ConfigurationServiceTypeRelation.ServiceType.Id,
                    TemplateImageName =
                        string.IsNullOrEmpty(x.database.DbTemplate.ImageUrl) ? "" : x.database.DbTemplate.ImageUrl,
                    V82Name = x.database.V82Name,
                    HasAccess = x.database.AcDbAccesses.Any(z => z.AccountUserID == x.user.Id),
                    State = x.database.AcDbAccesses.FirstOrDefault(z => z.AccountUserID == userId) != null
                        ? x.database.AcDbAccesses.FirstOrDefault(z => z.AccountUserID == userId)!
                            .ManageDbOnDelimitersAccess != null
                            ?
                            x.database.AcDbAccesses.FirstOrDefault(z => z.AccountUserID == userId)!
                                .ManageDbOnDelimitersAccess.AccessState
                            :
                            x.database.AcDbAccesses.Any(z => z.AccountUserID == userId)
                                ? AccountDatabaseAccessState.Done
                                : AccountDatabaseAccessState.Undefined
                        : AccountDatabaseAccessState.Undefined,
                    ConfigurationCode = x.database.AccountDatabaseOnDelimiter != null
                        ? x.database.AccountDatabaseOnDelimiter.DbTemplateDelimiterCode
                        : null,
                })
                .OrderByDescending(x => x.HasAccess)
                .ToList();
        }

        /// <summary>
        /// Получить данные доступов (к инф. базе)
        /// карточки пользователей
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="accountId"></param>
        /// <returns>Модель данных доступов (к базе)
        /// карточки пользователей</returns>
        public UserCardAcDbAccessesDataDto GetDataForExternalUserCard(Guid userId, Guid accountId)
        {
            var accessList = SelectDatabaseExternalAccessesForUser(userId, accountId);
            return new UserCardAcDbAccessesDataDto
            {
                DatabaseAccesses = accessList,
                AccessToServerDatabaseServiceTypeId = myDatabasesServiceTypeDataProvider.GetAccessToServerDatabaseServiceTypeId(),
                Currency = unitOfWork.AccountsRepository.GetLocale(unitOfWork.AccountsRepository.GetAccountByUserId(userId).Id, Guid.Parse(configuration["DefaultLocale"]!)).Currency,
                MyDatabaseBillingServiceId = unitOfWork.BillingServiceRepository.FirstOrDefault(bs => bs.SystemService == Clouds42Service.MyDatabases).Id,
            };
        }
    }
}
