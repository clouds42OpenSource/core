﻿using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.AccountDatabase.Card.Providers
{
    /// <summary>
    /// Провайдер поиска внешнего пользователя для предоставления доступа
    /// </summary>
    internal class SearchExternalAccountUserForGrandAccessProvider(
        IUnitOfWork dbLayer,
        IResourceDataProvider resourceDataProvider,
        ICloudLocalizer cloudLocalizer,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : ISearchExternalAccountUserForGrandAccessProvider
    {
        /// <summary>
        /// Найти внешнего пользователя для предоставления доступа
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserEmail">Почта пользователя</param>
        /// <returns>Модель пользователя для предоставления доступа</returns>
        public ExternalAccountUserDataForGrandAccessDto Search(Guid databaseId, string accountUserEmail)
        {
            var accountUser = GetAccountUserByEmailOrThrowException(accountUserEmail);
            var accountDatabase = GetAccountDatabaseOrThrowException(databaseId);

            CheckAccountUserRent1CResources(accountDatabase.AccountId, accountUser);

            if (accountDatabase.AccountId == accountUser.AccountId)
                throw new InvalidOperationException(
                    $"Пользователь \"{accountUserEmail}\" является пользователем вашего аккаунта \"{accountUser.Account.AccountCaption}\".");

            var dbAccess = dbLayer.AcDbAccessesRepository.FirstOrDefault(access =>
                access.AccountDatabaseID == accountDatabase.Id && access.AccountUserID == accountUser.Id);
            if (dbAccess != null)
                throw new InvalidOperationException(
                    $"Доступ для пользователя \"{accountUserEmail}\" уже предоставлен.");

            return CreateUserModel(accountUser);
        }

        /// <summary>
        /// Проверить ресурсы пользователя по Аренде 1С
        /// </summary>
        /// <param name="accountOwnerId">Id аккаунта владельца базы</param>
        /// <param name="accountUser">Пользователь</param>
        private void CheckAccountUserRent1CResources(Guid accountOwnerId, AccountUser accountUser)
        {
            var isAnyResources = resourceDataProvider.GetResourcesLazy(Clouds42Service.MyEnterprise,
                res => res.Subject == accountUser.Id,
                ResourceType.MyEntUserWeb,
                ResourceType.MyEntUser).Any();

            if (isAnyResources)
            {
                var resourseConfiguration = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == accountUser.AccountId
                && rc.BillingService.SystemService == Clouds42Service.MyEnterprise);
                if (resourseConfiguration.FrozenValue)
                {
                    throw new InvalidOperationException($"Для внешнего пользователя \"({accountUser.Email})\" сервис 1С заблокирован  " +
                        $"\n Оплатите сервис!"); 
                }
                return;

            }

            var rent1CName = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C,accountOwnerId);
            var cpSiteUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountUser.AccountId);
            var url = new Uri(
                    $"{cpSiteUrl}/{CloudConfigurationProvider.Cp.GetRouteForOpenRent1CPage()}")
                .AbsoluteUri;

            var rent1CHtmlLink = $"<a href='{url}' target='_blank'>\"{rent1CName}\"</a>";
            throw new InvalidOperationException(
                $"Для внешнего пользователя \"({accountUser.Email})\" сервис \"{rent1CName}\" неактивен. Вы можете проспонсировать его на вкладке {rent1CHtmlLink}");
        }

        /// <summary>
        /// Создать модель пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <returns>Модель пользователя</returns>
        private ExternalAccountUserDataForGrandAccessDto CreateUserModel(AccountUser accountUser)
            => new()
            {
                UserId = accountUser.Id,
                UserLogin = accountUser.Login,
                UserEmail = accountUser.Email,
                UserLastName = accountUser.LastName,
                UserFirstName = accountUser.FirstName,
                UserMiddleName = accountUser.MiddleName,
                AccountIndexNumber = accountUser.Account.IndexNumber,
                AccountCaption = accountUser.Account.AccountCaption,
                HasLicense = false
            };

        /// <summary>
        /// Получить инф. базу по ID или выкинуть исключение
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Инф. база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid accountDatabaseId)
            => dbLayer.DatabasesRepository.GetAccountDatabase(accountDatabaseId) ??
               throw new NotFoundException($"Инф. база по ID {accountDatabaseId} не найдена.");

        /// <summary>
        /// Получить пользователя по эл. почте или выкинуть исключение
        /// </summary>
        /// <param name="accountUserEmail">Почта пользователя</param>
        /// <returns>Пользователь</returns>
        private AccountUser GetAccountUserByEmailOrThrowException(string accountUserEmail)
            => dbLayer.AccountUsersRepository.GetAccountUserByEmail(accountUserEmail) ??
               throw new NotFoundException($"Пользователь по эл. почте {accountUserEmail} не найден");
    }
}
