﻿using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Card.Manager
{
    /// <summary>
    /// Менеджер для работы с карточкой инф. базы
    /// </summary>
    public class AccountDatabaseCardManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IDatabaseCardGeneralDataProvider databaseCardGeneralDataProvider,
        IHandlerException handlerException,
        IDatabaseCardAcDbSupportDataProvider databaseCardAcDbSupportDataProvider,
        IDatabaseCardDbBackupsDataProvider databaseCardDbBackupsDataProvider,
        IDatabaseCardAcDbAccessesDataProvider databaseCardAcDbAccessesDataProvider,
        ISearchExternalAccountUserForGrandAccessProvider searchExternalAccountUserForGrandAccessProvider,
        IAccountDatabaseAdminPanelDataProvider accountDatabaseAdminPanelData,
        IAccountDatabaseDataProvider accountDatabaseDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить модель общих/основных данных
        /// карточки информационной базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель общих/основных данных
        /// карточки информационной базы</returns>
        public ManagerResult<DatabaseCardGeneralDataDto> GetDatabaseCardGeneralData(Guid databaseId, Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_GetCardGeneralData,
                    () => GetAccountIdByDatabase(databaseId),
                    optionalCheck: () => AccountHasAccessToDataBase(accountId, databaseId));

                var data = databaseCardGeneralDataProvider.GetData(databaseId);

                var localeId = DbLayer.AccountConfigurationRepository.FirstOrDefault(x => x.AccountId == accountId)?
                    .LocaleId;
                
                var archiveAndBackupServices = DbLayer.BillingServiceRepository
                    .AsQueryable()
                    .Join(DbLayer.BillingServiceTypeRepository.AsQueryable(), x => x.Id, z => z.ServiceId,
                        (service, type) => new { service, type })
                    .Join(DbLayer.RateRepository.AsQueryable(), x => x.type.Id, z => z.BillingServiceTypeId,
                        (serviceType, rate) => new { serviceType, rate })
                    .Join(DbLayer.LocaleRepository.AsQueryable(), x => x.rate.LocaleId, z => z.ID,
                        (serviceTypeRate, locale) => new { serviceTypeRate, locale })
                    .Where(x => (x.serviceTypeRate.serviceType.service.Name == "Создание бекапа базы" ||
                                 x.serviceTypeRate.serviceType.service.Name == "Создание архива базы") &&
                                x.locale.ID == localeId)
                    .Select(x => new DatabaseOperation
                    {
                        Cost = x.serviceTypeRate.rate.Cost,
                        Currency = x.locale.Currency,
                        Id = x.serviceTypeRate.serviceType.service.Id,
                        Name = x.serviceTypeRate.serviceType.service.Name
                    })
                    .ToList();

                data.DatabaseOperations = archiveAndBackupServices;

                return Ok(data);
            }
            catch (AccessDeniedException ex)
            {
                return Forbidden<DatabaseCardGeneralDataDto>(ex.Message);
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения общих/основных данных карточки информационной базы. {ex.GetFullInfo()}";
                Logger.Warn(ex, $"{errorMessage}  {databaseId}.");
                return PreconditionFailed<DatabaseCardGeneralDataDto>(errorMessage);
            }
        }

        /// <summary>
        /// Получить данные технической поддержки инф. базы
        /// для карточки информационной базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель данных технической поддержки инф. базы</returns>
        public ManagerResult<DatabaseCardAcDbSupportDataDto> GetDatabaseCardAcDbSupportData(Guid databaseId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_GetAcDbSupportData,
                    () => GetAccountIdByDatabase(databaseId));

                var data = databaseCardAcDbSupportDataProvider.GetData(databaseId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка получения данных технической поддержки информационной базы] {databaseId}.");
                return PreconditionFailed<DatabaseCardAcDbSupportDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные бэкапов (базы)
        /// карточки инф. базы по фильтру
        /// </summary>
        /// <param name="filter">Модель фильтра бэкапов карточки инф. базы</param>
        /// <returns>Модель данных бэкапов
        /// карточки инф. базы</returns>
        public ManagerResult<DatabaseCardDbBackupsDataDto> GetDatabaseCardDbBackupsDataByFilter(
            DatabaseCardDbBackupsFilterDto filter)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_GetBackupsData,
                    () => GetAccountIdByDatabase(filter.DatabaseId),
                    optionalCheck: () => AccountHasAccessToDataBase(AccessProvider.ContextAccountId, filter.DatabaseId));

                var data = databaseCardDbBackupsDataProvider.GetDataByFilter(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка получения данных бэкапов информационной базы]  '{filter.DatabaseId}'.");
                return PreconditionFailed<DatabaseCardDbBackupsDataDto>(ex.GetFullInfo());
            }
        }

        /// <summary>
        /// Получить данные доступов (к инф. базе)
        /// карточки пользователи
        /// </summary>
        /// <param name="userId">идентификатор пользователя</param>
        /// <param name="accountId"></param>
        /// <returns>Модель данных доступов (к базе)
        /// карточки пользователей</returns>
        public ManagerResult<UserCardAcDbAccessesDataDto> GetUserCardAcDbExternalAccessesDatabases(
            Guid userId, Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_GetAcDbAccesses,
                    () => AccessProvider.ContextAccountId);

                var data = databaseCardAcDbAccessesDataProvider.GetDataForExternalUserCard(userId, accountId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                Logger.Warn(ex,
                    $"[Ошибка получения данных доступов карточки пользователей]  '{userId}'.");
                return PreconditionFailed<UserCardAcDbAccessesDataDto>(ex.GetFullInfo());
            }
        }

        /// <summary>
        /// Найти внешнего пользователя для предоставления доступа
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserEmail">Почта пользователя</param>
        /// <returns>Модель пользователя для предоставления доступа</returns>
        public ManagerResult<ExternalAccountUserDataForGrandAccessDto> SearchExternalAccountUserForGrandAccess(Guid databaseId, string accountUserEmail)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_GrantAccessToUser,
                    () => AccountIdByAccountDatabase(databaseId));

                return Ok(searchExternalAccountUserForGrandAccessProvider.Search(databaseId, accountUserEmail));
            }
            catch (Exception ex)
            {
                Logger.Warn(ex,
                    $"Ошибка поиска внешнего пользователя '{accountUserEmail}' для предоставления доступа к инф. базе {databaseId}.");
                return PreconditionFailed<ExternalAccountUserDataForGrandAccessDto>(ex.Message); 
            }
        }

        /// <summary>
        /// Получить информацию об информационной базы
        /// </summary>
        /// <param name="databaseNumber">Номер инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информация об информационной базе</returns>
        public ManagerResult<AccountDatabaseInfoDc> GetAccountDatabaseInfo(string databaseNumber, Guid accountId) =>
            GetAccountDatabaseInfo(
                () => accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(databaseNumber), accountId);

        /// <summary>
        /// Получить информацию об информационной базы
        /// </summary>
        /// <param name="getAccountDatabaseFunc">Функция для получения инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информация об информационной базе</returns>
        public ManagerResult<AccountDatabaseInfoDc> GetAccountDatabaseInfo(Func<Domain.DataModels.AccountDatabase> getAccountDatabaseFunc, Guid accountId)
        {
            try
            {
                var accountDatabase = getAccountDatabaseFunc();

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => accountDatabase.AccountId, null,
                    () => AccountHasAccessToDataBase(accountId, accountDatabase.Id));

                return Ok(accountDatabaseAdminPanelData.GetData(accountDatabase));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    "[Ошибка получения информации об информационной базе]");
                return PreconditionFailed<AccountDatabaseInfoDc>(ex.Message);
            }
        }
    }
}
