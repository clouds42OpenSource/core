﻿using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.AccountDatabase.Card.Mappers
{
    /// <summary>
    /// Маппер моделей информационных баз
    /// </summary>
    public static class AccountDatabaseDataMapper
    {
        /// <summary>
        /// Выполнить маппинг к модели дополнительных данных
        /// по информационной базе
        /// </summary>
        /// <param name="cardGeneralDataDto">Модель общих/основных данных карточки информационной базы</param>
        /// <returns>Дополнительные данные по информационной базе</returns>
        public static AccountDatabaseAdditionalDataDc MapToAccountDatabaseAdditionalDataDc(this DatabaseCardGeneralDataDto cardGeneralDataDto)
        {
            return new AccountDatabaseAdditionalDataDc
            {
                Id = cardGeneralDataDto.Id,
                Alpha83Version = cardGeneralDataDto.Alpha83Version,
                Caption = cardGeneralDataDto.Caption,
                CreationDate = cardGeneralDataDto.CreationDate,
                CanWebPublish = cardGeneralDataDto.CanWebPublish,
                CanChangeRestoreModel = cardGeneralDataDto.CanChangeRestoreModel,
                FilePath = cardGeneralDataDto.FilePath,
                FileStorageName = cardGeneralDataDto.FileStorageName,
                FileStorage = cardGeneralDataDto.FileStorageId,
                DatabaseConnectionPath = cardGeneralDataDto.DatabaseConnectionPath,
                DatabaseState = cardGeneralDataDto.DatabaseState,
                DbNumber = cardGeneralDataDto.DbNumber,
                DbTemplate = cardGeneralDataDto.DbTemplate,
                DistributionType = cardGeneralDataDto.DistributionType,
                IsFile = cardGeneralDataDto.IsFile,
                IsDbOnDelimiters = cardGeneralDataDto.IsDbOnDelimiters,
                IsDemoDelimiters = cardGeneralDataDto.IsDemoDelimiters,
                SizeInMB = cardGeneralDataDto.SizeInMb,
                Stable82Version = cardGeneralDataDto.Stable82Version,
                Stable83Version = cardGeneralDataDto.Stable83Version,
                PublishState = cardGeneralDataDto.PublishState,
                PlatformType = cardGeneralDataDto.PlatformType,
                TemplateName = cardGeneralDataDto.TemplateName,
                TemplatePlatform = cardGeneralDataDto.TemplatePlatform,
                WebPublishPath = cardGeneralDataDto.WebPublishPath,
                UsedWebServices = cardGeneralDataDto.UsedWebServices,
                V82Name = cardGeneralDataDto.V82Name,
                CloudStorageWebLink = cardGeneralDataDto.CloudStorageWebLink,
                SqlServer = cardGeneralDataDto.SqlServer,
                ZoneNumber = cardGeneralDataDto.ZoneNumber,
                RestoreModelType = cardGeneralDataDto.RestoreModelType,
                State = cardGeneralDataDto.DatabaseState,
                AccountCaption = cardGeneralDataDto.AccountCaption,
                LastActivityDate = cardGeneralDataDto.LastEditedDateTime,
                ActualDatabaseBackupId = cardGeneralDataDto.ActualDatabaseBackupId,
                BackupDate = cardGeneralDataDto.BackupDate,
                CalculateSizeDateTime = cardGeneralDataDto.CalculateSizeDateTime,
                V82Server = cardGeneralDataDto.V82Server,
                ArchivePath = cardGeneralDataDto.ArchivePath
            };
        }

        /// <summary>
        /// Выполнить маппинг к списку моделей
        /// файлового хранилища информационной базы
        /// </summary>
        /// <param name="list">Список моделей типа пара ключ-значение хранилищ файловых баз</param>
        /// <returns>Список файловых хранилищ информационной базы</returns>
        public static List<AccountDatabaseFileStorage> MapToAccountDatabaseFileStorageList(this List<KeyValueDto<Guid>> list) =>
            list.Select(l => new AccountDatabaseFileStorage { Id = l.Key, Name = l.Value }).ToList();

        /// <summary>
        /// Выполнить маппинг к списку моделей шаблонов базы
        /// </summary>
        /// <param name="list">Список моделей типа пара ключ-значение шаблонов информационных баз</param>
        /// <returns>Список шаблонов баз</returns>
        public static List<AccountDatabaseTemplate> MapToAccountDatabaseTemplate(this List<KeyValueDto<Guid>> list) =>
            list.Select(l => new AccountDatabaseTemplate {Id = l.Key, DefaultCaption = l.Value}).ToList();

        /// <summary>
        /// Выполнить маппинг к списку моделей бекапов информационных баз
        /// </summary>
        /// <param name="backupsDataDto">Модель данных бэкапов (базы) карточки инф. базы</param>
        /// <returns>Список бекапов информационной базы</returns>
        public static List<AccountDatabaseBackupsDc> MapToAccountDatabaseBackupsDcList(
            this DatabaseCardDbBackupsDataDto backupsDataDto) =>
            backupsDataDto.DatabaseBackups.Select(bak => new AccountDatabaseBackupsDc
            {
                Id = bak.Id,
                BackupPath = bak.BackupPath,
                IsDbOnDelimiters = bak.IsDbOnDelimiters,
                CreateDateTime = bak.CreationDate.ToString("s"),
                EventTrigger = bak.EventTriggerDescription,
                Initiator = bak.Initiator
            }).ToList();
    }
}
