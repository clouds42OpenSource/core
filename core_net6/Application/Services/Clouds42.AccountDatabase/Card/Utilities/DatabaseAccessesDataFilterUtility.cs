﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Card.Utilities
{
    /// <summary>
    /// Утилита для фильтра моделей данных доступов инф. базы
    /// </summary>
    public static class DatabaseAccessesDataFilterUtility
    {
        /// <summary>
        /// Применить фильтр для моделей данных
        /// доступов инф. базы
        /// </summary>
        /// <param name="dbAccesses">Модели данных доступов инф. базы</param>
        /// <param name="filter">Модель фильтра доступов информационной к инф. базе</param>
        /// <returns>Отфильтрованные данные доступов инф. базы</returns>
        public static IQueryable<AcDbAccessDataDto> ApplyFilter(this IQueryable<AcDbAccessDataDto> dbAccesses,
            DatabaseCardAcDbAccessesFilterDto filter) =>
            from dbAccess in dbAccesses
            where (filter.UsersByAccessFilterType == UsersByAccessFilterTypeEnum.All ||
                   filter.UsersByAccessFilterType == UsersByAccessFilterTypeEnum.ExternalUsers &&
                   dbAccess.IsExternalAccess ||
                   filter.UsersByAccessFilterType == UsersByAccessFilterTypeEnum.WithAccess &&
                   dbAccess.HasAccess && dbAccess.State == AccountDatabaseAccessState.Done ||
                   filter.UsersByAccessFilterType == UsersByAccessFilterTypeEnum.WithoutAccess &&
                   !dbAccess.HasAccess && dbAccess.State != AccountDatabaseAccessState.Done)
                  && (string.IsNullOrEmpty(filter.SearchString)
                      || dbAccess.UserLastName.ToLower().Contains(filter.SearchString.ToLower())
                      || dbAccess.UserFirstName.ToLower().Contains(filter.SearchString.ToLower())
                      || dbAccess.UserMiddleName.ToLower().Contains(filter.SearchString.ToLower())
                      || dbAccess.UserLogin.ToLower().Contains(filter.SearchString.ToLower())
                      || !string.IsNullOrEmpty(dbAccess.UserEmail) &&
                      dbAccess.UserEmail.ToLower().Contains(filter.SearchString.ToLower())
                  )
            select dbAccess;
    }
}