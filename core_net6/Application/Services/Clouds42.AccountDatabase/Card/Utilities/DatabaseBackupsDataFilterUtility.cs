﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Card.Utilities
{
    /// <summary>
    /// Утилита для фильтра моделей данных бэкапов инф. базы
    /// </summary>
    public static class DatabaseBackupsDataFilterUtility
    {
        /// <summary>
        /// Список групп пользователя у которых есть доступ к бэкапам инф. баз 
        /// </summary>
        private static readonly IList<AccountUserGroup> GroupsWithAccessToBackups = new List<AccountUserGroup>
        {
            AccountUserGroup.CloudAdmin,
            AccountUserGroup.Hotline,
            AccountUserGroup.AccountSaleManager,
            AccountUserGroup.CloudSE,
            AccountUserGroup.AccountAdmin
        };

        /// <summary>
        /// Триггеры создания бэкапа для отображения бэкапов аккаунт админу
        /// </summary>
        private static readonly IList<CreateBackupAccountDatabaseTrigger> CreateBackupDbTriggersToDisplayAccountAdmin =
            new List<CreateBackupAccountDatabaseTrigger>
            {
                CreateBackupAccountDatabaseTrigger.OverdueRent,
                CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase,
                CreateBackupAccountDatabaseTrigger.TehSupportAccountDatabase,
                CreateBackupAccountDatabaseTrigger.MovingDbToArchive,
                CreateBackupAccountDatabaseTrigger.ManualRemoval,
                CreateBackupAccountDatabaseTrigger.ManualArchive,
                CreateBackupAccountDatabaseTrigger.RegulationBackup,
                CreateBackupAccountDatabaseTrigger.TechnicalBackup
            };

        /// <summary>
        /// Применить фильтр для моделей данных
        /// бэкапов инф. базы
        /// </summary>
        /// <param name="backups">Бэкапы инф. базы</param>
        /// <param name="filter">Модель фильтра</param>
        /// <returns>Отфильтрованные данные бэкапов инф. базы</returns>
        public static IQueryable<DatabaseBackupDataDto> ApplyFilter(this IQueryable<DatabaseBackupDataDto> backups,
            DatabaseCardDbBackupsFilterDto filter)
        {
            var creationDateFrom = filter.CreationDateFrom?.Date;
            var creationDateTo = filter.CreationDateTo?.Date.AddTicks(-1).AddDays(1);

            return from backup in backups
                where (creationDateFrom == null || creationDateFrom.Value <= backup.CreationDate) &&
                      (creationDateTo == null ||
                       creationDateTo.Value >= backup.CreationDate)
                select backup;
        }
            

        /// <summary>
        /// Применить фильтр для моделей данных
        /// бэкапов инф. базы по группам пользователя
        /// </summary>
        /// <param name="backups">Бэкапы инф. базы</param>
        /// <param name="userGroups">Группы пользователя</param>
        /// <returns>Отфильтрованные данные бэкапов инф. базы</returns>
        public static IQueryable<DatabaseBackupDataDto> FilterByUserGroups(
            this IQueryable<DatabaseBackupDataDto> backups, List<AccountUserGroup> userGroups)
        {
            var groupsWithAccess = userGroups.Intersect(GroupsWithAccessToBackups).ToList();

            if (!groupsWithAccess.Any())
                return Enumerable.Empty<DatabaseBackupDataDto>().AsQueryable();

            if (groupsWithAccess.Any(g => g != AccountUserGroup.AccountAdmin))
                return backups;

            return backups.Where(b =>
                CreateBackupDbTriggersToDisplayAccountAdmin.Any(trigger => trigger == b.EventTrigger));
        }
    }
}
