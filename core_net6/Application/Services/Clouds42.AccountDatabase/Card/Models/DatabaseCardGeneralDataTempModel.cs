﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Card.Models
{
    /// <summary>
    /// Временная модель общих/основных данных
    /// карточки информационной базы
    /// </summary>
    public class DatabaseCardGeneralDataTempModel
    {
        /// <summary>
        /// Информационная база
        /// </summary>
        public Domain.DataModels.AccountDatabase Database { get; set; }

        /// <summary>
        /// Аккаунт владельца базы
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Версия базы
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// База на разделителях
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// Признак что база демо на разделителях
        /// </summary>
        public bool IsDemoDelimiters { get; set; }

        /// <summary>
        /// Номер области (если база на разделителях)
        /// </summary>
        public int? ZoneNumber { get; set; }

        /// <summary>
        /// Признак, что в базе есть доработки
        /// </summary>
        public bool HasModifications { get; set; }

        /// <summary>
        /// Подключено ли автообновление
        /// </summary>
        public bool? HasAutoUpdate { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }


        /// <summary>
        /// Код конфигурации
        /// </summary>
        public string ConfigurationCode { get; set; }

        /// <summary>
        /// Синоним конфигурации
        /// </summary>
        public string SynonymConfiguration { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        public string TemplatePlatform { get; set; }

        /// <summary>
        /// Id шаблона базы
        /// </summary>
        public Guid? DbTemplateId { get; set; }

        /// <summary>
        /// Возможность вэб публикации
        /// </summary>
        public bool CanWebPublish { get; set; }

        /// <summary>
        /// Дефолтное описание шаблона базы
        /// </summary>
        public string DefaultTemplateCaption { get; set; }

        /// <summary>
        /// Наименование файлового хранилища
        /// </summary>
        public string FileStorageName { get; set; }

        /// <summary>
        /// Sql сервер
        /// </summary>
        public string SqlServer { get; set; }

        /// <summary>
        /// Тип модели восстановления
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum? RestoreModelType { get; set; }

        /// <summary>
        /// Id актуального бэкапа инф. базы
        /// </summary>
        public Guid? ActualDatabaseBackupId { get; set; }

        /// <summary>
        /// Cуществует ли процесс завершения сеансов в базе данных
        /// Запись со статусом "В процессе завершения сеансов"
        /// </summary>
        public bool IsExistTerminatingSessionsProcessInDatabase { get; set; }

        /// <summary>
        /// Доступные хранилища файлов
        /// </summary>
        public List<CloudServicesFileStorageServer> AvailableFileStorages { get; set; }

        /// <summary>
        /// Дата создания бекапа
        /// </summary>
        public DateTime? BackupDate { get; set; }

        /// <summary>
        /// Путь до бекапа
        /// </summary>
        public string ArchivePath { get; set; }
    }
}
