﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.DataModels.Link;

namespace Clouds42.AccountDatabase.Activity.Helpers
{
    /// <summary>
    /// Маппер моделей фиксации активности инф. баз
    /// </summary>
    public static class FixationAcDbActivityMapper
    {
        /// <summary>
        /// Создать объект регистрации истории запуска баз через Линк
        /// </summary>
        /// <param name="model">Модель для регистрации истории запуска базы или RDP</param>
        /// <param name="v82Name">Номер информационной базы</param>
        /// <param name="accountIndexNumber">Номер аккаунта</param>
        /// <param name="clientIpAddress">IP адрес клиента</param>
        /// <returns>Объект регистрации истории запуска баз через Линк</returns>
        public static DatabaseLaunchRdpStartHistory CreateDatabaseLaunchRdpStartHistoryObj(
            LinkLaunchActionInfoDto model, string v82Name, int accountIndexNumber, string clientIpAddress)
            => new()
            {
                AccountNumber = accountIndexNumber,
                LinkAppType = model.LinkAppType,
                Action = model.Action,
                LinkAppVersion = model.LinkAppVersion,
                LaunchType = model.LaunchType,
                ActionCreated = DateTime.Now,
                Login = model.Login,
                V82Name = v82Name,
                ExternalIpAddress = clientIpAddress,
                InternalIpAddress = model.InternalIpAddress
            };
    }
}
