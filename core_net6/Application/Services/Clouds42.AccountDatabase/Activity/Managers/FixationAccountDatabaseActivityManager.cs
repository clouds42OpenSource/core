﻿using Clouds42.AccountDatabase.Contracts.Activity.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums.Link42;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Activity.Managers
{
    /// <summary>
    /// Менеджер фиксации активности инф. баз
    /// </summary>
    public class FixationAccountDatabaseActivityManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IFixactionAccountDatabaseActivityProvider fixactionAccountDatabaseActivityProvider,
        IRent1CClientDataManager rent1CClientDataManager,
        ILogger42 logger,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IFixationAccountDatabaseActivityManager
    {
        /// <summary>
        /// Зафиксировать дату активности в инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Результат регистрации</returns>
        public ManagerResult FixLastActivity(Guid accountDatabaseId)
        {

            if(!AccessProvider.HasAccessBool(ObjectAction.AccountDatabase_Change,
                optionalCheck: () =>
                    AccountHasAccessToDataBase(AccessProvider.GetUser().RequestAccountId, accountDatabaseId)))
                return Unauthorized($"Отказано в доступе к объекту AccountDatabaseID='{accountDatabaseId}'");

            try
            {
                fixactionAccountDatabaseActivityProvider.FixDatabaseLastActivity(accountDatabaseId);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"Ошибка в процессе обновления информации о дате последнего доступа к базе данных c ID {accountDatabaseId}";
                logger.Warn(ex, errorMessage);
                return PreconditionFailed(errorMessage);
            }
        }

        /// <summary>
        /// Зафиксировать последнюю активность запуска инф. базы или RDP
        /// </summary>
        /// <param name="model">Модель фиксации последней активности запуска инф. базы или RDP</param>
        /// <param name="clientIpAddress">IP адрес клиента</param>
        public ManagerResult FixLaunchActivity(PostRequestSetActivityDateDto model, string clientIpAddress)
        {
            if (!CanFixateLaunchActivity(model, out var checkResult))
                return checkResult;

            try
            {
                fixactionAccountDatabaseActivityProvider.FixLaunchActivity(model, clientIpAddress);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"Ошибка в процессе обновления информации о дате последнего запуска инф. базы c ID {model.AccountDatabaseID} или RDP";
                logger.Warn(ex, errorMessage);
                return PreconditionFailed(errorMessage);
            }
        }

        /// <summary>
        /// Проверить возможность фиксации активности
        /// </summary>
        /// <param name="model">Модель фиксации последней активности запуска инф. базы или RDP</param>
        /// <param name="result">Результат работы менеджера</param>
        /// <returns>true - если есть доступ к данной операции</returns>
        private bool CanFixateLaunchActivity(PostRequestSetActivityDateDto model, out ManagerResult result)
        {
            result = Ok();

            if (model.LinkLaunchActionInfo == null)
            {
                result = PreconditionFailed("Модель фиксации последней активности запуска инф. базы или RDP пуста!");
                return false;
            }

            if (model.LinkLaunchActionInfo.Action != LinkActionType.OpenRdp)
                return true;

            var hasRent1CRdpPermission = HasRent1CRdpPermissionForCurrentUser();
            if (hasRent1CRdpPermission is { Error: false, Result: true }) 
                return true;

            result = Unauthorized("Отказано в доступе к запуску RDP");
            return false;
        }

        /// <summary>
        /// Получить признак доступности к Аренда 1С RDP для текущего пользователя
        /// </summary>
        /// <returns>Признак доступности к Аренда 1С RDP для текущего пользователя</returns>
        private ManagerResult<bool> HasRent1CRdpPermissionForCurrentUser()
        {
            var user = AccessProvider.GetUser();
            var accountId = user.ContextAccountId;
            var accountUserId = user.Id;

            var rent1CPermissions = rent1CClientDataManager.GetRent1CClientPermissions(accountId, accountUserId);
            if (rent1CPermissions.Error)
                return PreconditionFailed<bool>(rent1CPermissions.Message);

            return rent1CPermissions.Result.HasPermissionForRdp
                ? Ok(true)
                : Unauthorized<bool>("Отказано в доступе к запуску RDP");
        }
    }
}
