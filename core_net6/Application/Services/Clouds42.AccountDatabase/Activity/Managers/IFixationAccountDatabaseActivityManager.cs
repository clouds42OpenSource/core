﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;

namespace Clouds42.AccountDatabase.Activity.Managers;

public interface IFixationAccountDatabaseActivityManager
{
    /// <summary>
    /// Зафиксировать дату активности в инф. базе
    /// </summary>
    /// <param name="accountDatabaseId">ID инф. базы</param>
    /// <returns>Результат регистрации</returns>
    ManagerResult FixLastActivity(Guid accountDatabaseId);

    /// <summary>
    /// Зафиксировать последнюю активность запуска инф. базы или RDP
    /// </summary>
    /// <param name="model">Модель фиксации последней активности запуска инф. базы или RDP</param>
    /// <param name="clientIpAddress">IP адрес клиента</param>
    ManagerResult FixLaunchActivity(PostRequestSetActivityDateDto model, string clientIpAddress);
}
