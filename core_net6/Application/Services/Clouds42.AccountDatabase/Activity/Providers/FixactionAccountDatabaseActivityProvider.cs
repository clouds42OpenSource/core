﻿using Clouds42.AccountDatabase.Activity.Helpers;
using Clouds42.AccountDatabase.Contracts.Activity.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.DataModels.Link;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Activity.Providers
{
    /// <summary>
    /// Провайдер фиксации активности инф. баз
    /// </summary>
    internal class FixactionAccountDatabaseActivityProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : IFixactionAccountDatabaseActivityProvider
    {
        /// <summary>
        /// Зафиксировать последнюю активность запуска инф. базы или RDP
        /// </summary>
        /// <param name="model">Модель для фиксации последней активности запуска инф. базы или RDP</param>
        /// <param name="clientIpAddress">IP адрес клиента</param>
        /// <param name="needThrowExceptionOnError">Признак необходимости выбрасывать исключение при ошибке</param>
        public void FixLaunchActivity(PostRequestSetActivityDateDto model, string clientIpAddress, bool needThrowExceptionOnError = true)
        {
            var accountDatabaseId = model.AccountDatabaseID;
            var accountDatabase = accountDatabaseId.Equals(Guid.Empty)
                ? null
                : dbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);

            var databaseV82Name = accountDatabaseId.Equals(Guid.Empty)
                ? null
                : accountDatabase?.V82Name ?? accountDatabaseId.ToString("D");

            RegisterDatabaseLaunchRdpStartHistory(model.LinkLaunchActionInfo, databaseV82Name, clientIpAddress, needThrowExceptionOnError);
        }

        /// <summary>
        /// Зафиксировать дату активности в инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        public void FixDatabaseLastActivity(Guid accountDatabaseId)
        {
            try
            {
                var accountDatabase = dbLayer.DatabasesRepository.FirstOrThrowException(
                    acDb => acDb.Id == accountDatabaseId, $"Инф. база по ID {accountDatabaseId} не найдена");

                accountDatabase.LastActivityDate = DateTime.Now;
                dbLayer.DatabasesRepository.Update(accountDatabase);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                var message = $"Во время фиксации активности в базе {accountDatabaseId} произошла ошибка : {ex.Message}";
                throw new InvalidOperationException(message);
            }
            
        }

        /// <summary>
        /// Регистрация истории запуска базы или RDP
        /// Если <paramref name="model"/> равняется <c>null</c>, то регистрации не будет, метод завершается
        /// </summary>
        /// <param name="model">Модель для регистрации истории запуска базы или RDP</param>
        /// <param name="v82Name">Номер информационной базы</param>
        /// <param name="clientIpAddress">IP адрес клиента</param>
        /// <param name="needThrowExceptionOnError">Признак необходимости выбрасывать исключение при ошибке</param>
        private void RegisterDatabaseLaunchRdpStartHistory(LinkLaunchActionInfoDto? model, string v82Name, string clientIpAddress, bool needThrowExceptionOnError)
        {
            if (model == null)
                return;

            try
            {
                var account = dbLayer.AccountsRepository.GetAccount(model.AccountId);
                var accountIndexNumber = account?.IndexNumber ?? 0;

                var databaseLaunchRdpStartHistory = dbLayer.GetGenericRepository<DatabaseLaunchRdpStartHistory>();
                databaseLaunchRdpStartHistory.Insert(
                    FixationAcDbActivityMapper.CreateDatabaseLaunchRdpStartHistoryObj(model, v82Name,
                        accountIndexNumber, clientIpAddress));

                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка в процессе сохранения логирования о запуске базы или RDP]");
                if(needThrowExceptionOnError)
                    throw;
            }
        }
    }
}
