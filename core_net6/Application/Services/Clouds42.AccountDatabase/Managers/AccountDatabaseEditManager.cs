﻿using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Validators;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountDatabase.DatabasesPlacement;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Access;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.BuyDatabasePlacement.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    ///     Прослойка редактирования объектов AccountDatabase
    /// </summary>
    public class AccountDatabaseEditManager(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IDatabaseEditHelper databaseEditHelper,
        IHandlerException handlerException,
        SynchronizeAccountDatabaseWithTardisHelper synchronizeAccountDatabaseWithTardisHelper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IBuyDatabasesPlacementProvider buyDatabasesPlacementProvider,
        IDatabaseCardAcDbSupportDataProvider databaseCardAcDbSupportDataProvider,
        IPlanUpdateVersionAccountDatabaseProvider planUpdateVersionAccountDatabaseProvider,
        ICheckAccountDataProvider checkAccountDataProvider,
        ICloudLocalizer cloudLocalizer,
        IAutoUpdateAccountDatabasesProvider autoUpdateAccountDatabasesProvider)
        : BaseManager(accessProvider, unitOfWork, handlerException)
    {
        readonly IHandlerException _handlerException = handlerException;
        readonly IUnitOfWork _unitOfWork = unitOfWork;

        /// <summary>
        ///     Редактирование базы через админ-панель
        /// </summary>
        /// <param name="dataBase">Модель данных для карточки базы</param>
        /// <param name="contextAccountId">ID аккаунта</param>
        public ManagerResult EditAccountDatabase(AccountDatabaseCartDomainModel dataBase, Guid contextAccountId)
        {

            var fullDatabaseName =
                AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(dataBase.Infos.Caption,
                    dataBase.Infos.V82Name);

            try
            {
                var accountId = AccountIdByAccountDatabase(dataBase.Infos.Id);
                
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => accountId, null,
                () => AccountHasAccessToDataBase(contextAccountId, dataBase.Infos.Id));

                var validateResult = AccountDbCaptionValidator.Validate(dataBase.Infos.Caption);
                if (!validateResult.Success)
                    throw new ValidateException(validateResult.Message);

                var validateV82NameResult = AccountDbV82NameValidator.Validate(dataBase.Infos.V82Name);
                if (!validateV82NameResult.Success)
                    throw new ValidateException(validateV82NameResult.Message);

                logger.Debug($"[{dataBase.Infos.Id}]:Начинаем сохранять результат внеченных изменений {fullDatabaseName}");
                
                databaseEditHelper.EditAccountDatabase(dataBase, accountId ?? contextAccountId);

                logger.Debug($"[{dataBase.Infos.Id}]:Успешно выполнено обновление базы :{fullDatabaseName}");
                LogEvent(() => accountId, LogActions.EditInfoBase, $"Успешно выполнено обновление базы {fullDatabaseName}");

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка при редактировании базы {fullDatabaseName} через админ-панель]");
                logger.Debug($"Ошибка при редактировании базы {fullDatabaseName} через админ-панель. Описание ошибки: {ex.GetFullInfo()}");
                LogEvent(() => contextAccountId, LogActions.EditInfoBase, $"Ошибка при редактировании базы {fullDatabaseName}. Причина : {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        ///     Изменить платформу у информационной базы
        /// </summary>
        /// <param name="databaseId">ID информационной базы</param>
        /// <param name="platformType">Выбранная платформа</param>
        /// <param name="distributionType">Тип платформы (Альфа или Стабильная)</param>
        public ManagerResult ChangeAccountDatabasePlatformType(Guid databaseId, PlatformType platformType,
            DistributionType distributionType = DistributionType.Stable)
        {
            return DoLogicFuncWithNullCheck(accountDatabaseDataProvider.GetAccountDatabase, databaseId,
                accountDatabase =>
                {
                    try
                    {
                        AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => accountDatabase.AccountId);

                        var resultLogMessage =
                            databaseEditHelper.ChangeDbPlatformAndGetLogMessage(accountDatabase, platformType,
                                distributionType);

                        LogEvent(() => accountDatabase.AccountId, LogActions.EditInfoBase, resultLogMessage);

                        return Ok();
                    }
                    catch (ArgumentNullException exception)
                    {
                        _handlerException.Handle(exception, $"[Ошибка при смене платформы] {platformType} или дистрибутива {distributionType} для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                        return PreconditionFailed(exception.Message);
                    }
                    catch (InvalidOperationException exception)
                    {
                        _handlerException.Handle(exception, $"[Ошибка при смене платформы] {platformType} или дистрибутива {distributionType} для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                        return PreconditionFailed(exception.Message);
                    }
                });
        }

        /// <summary>
        ///     Изменить тип у информационной базы
        /// </summary>
        /// <param name="databaseId">ID информационной базы</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        public ManagerResult<bool> ChangeAccountDatabaseType(ChangeAccountDatabaseTypeDto changeAccountDatabaseTypeDto)
        {
            return DoLogicFuncWithNullCheck(accountDatabaseDataProvider.GetAccountDatabase, changeAccountDatabaseTypeDto.DatabaseId,
                accountDatabase =>
                {
                    try
                    {
                        AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => accountDatabase.AccountId);

                        var result = databaseEditHelper.ChangeAccountDatabaseType(accountDatabase, changeAccountDatabaseTypeDto);
                        var type = result ? "файловую" : "серверную";
                        LogEvent(() => accountDatabase.AccountId, LogActions.EditInfoBase,
                            $"Смена типа для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} на {type}.");

                        return Ok(result);
                    }
                    catch (ArgumentNullException exception)
                    {
                        _handlerException.Handle(exception, $"[Ошибка при смене типа ИБ] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                        return PreconditionFailed<bool>(exception.Message);
                    }
                    catch (InvalidOperationException exception)
                    {
                        _handlerException.Handle(exception, $"[Ошибка при смене типа ИБ] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                        return PreconditionFailed<bool>(exception.Message);
                    }
                });
        }

        /// <summary>
        /// Сменить название инф. базы
        /// </summary>
        /// <param name="model">Модель изменения названия</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult SetCaption(PostRequestAccDbCaptionDto model)
        {
            return DoLogicFuncWithNullCheck(accountDatabaseDataProvider.GetAccountDatabase, model.AccountDatabaseID,
                accountDatabase =>
                {
                    try
                    {
                        AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => AccountIdByAccountDatabase(model.AccountDatabaseID));

                        var oldDatabaseFullName = AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase);

                        databaseEditHelper.ChangeAccountDatabaseCaption(model);

                        LogEvent(() => accountDatabase.AccountId, LogActions.EditInfoBase,
                            $"Смена названия для базы {oldDatabaseFullName} на \"{model.Caption}\".");

                        return Ok();
                    }
                    catch (AccessDeniedException ex)
                    {
                        return Forbidden(ex.Message);
                    }
                    catch (ValidateException ex)
                    {
                        return PreconditionFailed(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        _handlerException.Handle(ex, $"[Ошибка смены названия инф. базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                        return PreconditionFailed(ex.Message);
                    }
                });
        }

        /// <summary>
        /// Изменить стьатус информационной базы
        /// </summary>
        /// <param name="databaseId">Идентификатор информационной базы.</param>
        /// <param name="databaseState">Статус, который нужно утсановить.</param>
        public ManagerResult ChangeDatabaseState(Guid databaseId, DatabaseState databaseState)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditStatus, () => AccountIdByAccountDatabase(databaseId));

            return DoLogicFuncWithNullCheck(accountDatabaseDataProvider.GetAccountDatabase, databaseId,
                accountDatabase =>
                {
                    try
                    {
                        accountDatabaseChangeStateProvider.ChangeState(accountDatabase.Id, databaseState);
                        synchronizeAccountDatabaseWithTardisHelper.SynchronizeAccountDatabaseViaNewThread(databaseId);
                        fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(accountDatabase.Id);

                        return Ok();
                    }
                    catch (AccessDeniedException ex)
                    {
                        return Forbidden(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        _handlerException.Handle(ex, $"[Ошибка смены статуса у базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                        return PreconditionFailed(ex.Message);
                    }
                });
        }

        /// <summary>
        /// Изменить номер ИБ
        /// </summary>
        /// <param name="databaseId">идентификатор ИБ</param>
        /// <param name="v82Name">номер ИБ</param>
        /// <returns></returns>
        public ManagerResult SetV82Name(Guid databaseId, string v82Name)
        {
            return DoLogicFuncWithNullCheck(accountDatabaseDataProvider.GetAccountDatabase, databaseId,
                accountDatabase =>
                {
                    try
                    {
                        AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditV82Name,
                            () => AccountIdByAccountDatabase(databaseId));

                        var message =
                            $"Смена номера для базы \"{accountDatabase.Caption}\" с \"{accountDatabase.V82Name}\" на \"{v82Name}\".";

                        databaseEditHelper.ChangeDbV82Name(accountDatabase, v82Name);

                        LogEvent(() => accountDatabase.AccountId, LogActions.EditInfoBase, message);

                        return Ok();
                    }
                    catch (AccessDeniedException ex)
                    {
                        return Forbidden(ex.Message);
                    }
                    catch (ArgumentNullException exception)
                    {
                        _handlerException.Handle(exception,
                            $"[Ошибка при смене номера ИБ] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                        return PreconditionFailed<bool>(exception.Message);
                    }
                    catch (InvalidOperationException exception)
                    {
                        _handlerException.Handle(exception,
                            $"[Ошибка при смене номера ИБ] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                        return PreconditionFailed<bool>(exception.Message);
                    }
                });
        }

        /// <summary>
        /// Сменить тип модели восстановления для инф. базы
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ChangeDatabaseRestoreModelType(Guid databaseId, AccountDatabaseRestoreModelTypeEnum restoreModelType)
        {
            return DoLogicFuncWithNullCheck(accountDatabaseDataProvider.GetAccountDatabase, databaseId,
                accountDatabase =>
                {
                    try
                    {
                        AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => accountDatabase.AccountId);

                        databaseEditHelper.ChangeDatabaseRestoreModelType(accountDatabase, restoreModelType);
                        LogEvent(() => accountDatabase.AccountId, LogActions.EditInfoBase,
                            $"Смена типа модели восстановления для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase.DbTemplate.DefaultCaption, accountDatabase.V82Name)} " +
                            $"на '{restoreModelType.Description()}'.");

                        return Ok();
                    }
                    catch (AccessDeniedException ex)
                    {
                        return Forbidden(ex.Message);
                    }
                    catch (Exception exception)
                    {
                        _handlerException.Handle(exception,
                            $"[Ошибка при смене типа модели восстановления ИБ] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                        return PreconditionFailed(exception.Message);
                    }
                });
        }

        /// <summary>
        /// Сменить признак наличия доработок для инф. базы
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="hasModifications">Признак наличия доработок</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ChangeDatabaseHasModificationsFlag(Guid databaseId, bool hasModifications)
        {
            return DoLogicFuncWithNullCheck(accountDatabaseDataProvider.GetAccountDatabase, databaseId, accountDatabase =>
            {
                try
                {
                    AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => accountDatabase.AccountId);

                    databaseEditHelper.ChangeDatabaseHasModificationsFlag(accountDatabase, hasModifications);
                    LogEvent(() => accountDatabase.AccountId, LogActions.EditInfoBase,
                        $"Признак наличия доработок для инф. базы " +
                        $"{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase.DbTemplate.DefaultCaption, accountDatabase.V82Name)} " +
                        $"изменен на '{hasModifications}'.");

                    return Ok();
                }
                catch (AccessDeniedException ex)
                {
                    return Forbidden(ex.Message);
                }
                catch (Exception exception)
                {
                    _handlerException.Handle(exception,
                        $"[Ошибка при смене признака наличия доработок в базе] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                    return PreconditionFailed(exception.Message);
                }
            });
        }

        /// <summary>
        /// Выполнить функцию бизнес логики с проверкой сущности на NULL
        /// </summary>
        /// <typeparam name="TModel">Тип модели</typeparam>
        /// <param name="searchElementFunc">Функция поиска сущности</param>
        /// <param name="entityId">ID сущности</param>
        /// <param name="mainLogicFunc">Основная функция бизнес логики</param>
        /// <returns>Результат выполнения</returns>
        private ManagerResult DoLogicFuncWithNullCheck<TModel>(Func<Guid, TModel> searchElementFunc, Guid entityId, Func<TModel, ManagerResult> mainLogicFunc)
        {
            var entity = searchElementFunc(entityId);
            return entity == null 
                ? PreconditionFailed($"Не удалось найти запись \"{nameof(TModel)}\" по Id {entityId}!") 
                : mainLogicFunc(entity);
        }

        /// <summary>
        /// Выполнить функцию бизнес логики с проверкой сущности на NULL
        /// </summary>
        /// <typeparam name="TModel">Тип модели</typeparam>
        /// <typeparam name="TResult">Тип ожидаемого результата</typeparam>
        /// <param name="searchElementFunc">Функция поиска сущности</param>
        /// <param name="entityId">ID сущности</param>
        /// <param name="mainLogicFunc">Основная функция бизнес логики</param>
        /// <returns>Результат выполнения</returns>
        private ManagerResult<TResult> DoLogicFuncWithNullCheck<TModel, TResult>(Func<Guid, TModel> searchElementFunc, Guid entityId, Func<TModel, ManagerResult<TResult>> mainLogicFunc)
        {
            var entity = searchElementFunc(entityId);
            return entity == null
                ? PreconditionFailed<TResult>($"Не удалось найти запись \"{nameof(TModel)}\" по Id {entityId}!")
                : mainLogicFunc(entity);
        }

        /// <summary>
        /// Принять заявку на изменение режима работы инф базы
        /// </summary>
        public ManagerResult ChangeAccDbTypeRequest(RequestToChangeAccDbTypeDto requestToChangeAccDbTypeDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => AccessProvider.ContextAccountId);
                if(requestToChangeAccDbTypeDto == null)
                    return PreconditionFailed("Не все обязательные поля заполнены");

                if (requestToChangeAccDbTypeDto.ChangeTypeDate == null)
                    return PreconditionFailed("Не указана дата подходящего дня для перевода");

                if (requestToChangeAccDbTypeDto.ChangeTypeDate.Value > DateTime.UtcNow.AddHours(3).AddDays(2))
                    return PreconditionFailed("Дата оплаты сервиса может быть выбрана только в течение двух дней");

                if (DateTime.UtcNow.AddHours(3) > requestToChangeAccDbTypeDto.ChangeTypeDate)
                    return PreconditionFailed("Дата оплаты сервиса может быть выбрана только больше сегодняшней");

                databaseCardAcDbSupportDataProvider.UpdateAuthorizationData
                    (requestToChangeAccDbTypeDto.AccountDatabseId, requestToChangeAccDbTypeDto.DatabaseAdminLogin, requestToChangeAccDbTypeDto.DatabaseAdminPassword);
                string type = "файловый";

                var database = accountDatabaseDataProvider.GetAccountDatabase(requestToChangeAccDbTypeDto.AccountDatabseId);

                if (database.IsFile != null && database.IsFile.Value)
                {
                    type = "серверный";
                    var databasesPlacementResult = buyDatabasesPlacementProvider.BuyDatabasesPlacement(new BuyDatabasesPlacementDto
                    {
                        AccountId = database.Account.Id,
                        SystemServiceType = ResourceType.ServerDatabasePlacement,
                        IsPromisePayment = false,
                        DatabasesForPlacementCount = 1,
                        PaidDate = requestToChangeAccDbTypeDto.ChangeTypeDate
                    });

                    if (!databasesPlacementResult.IsComplete)
                        throw new PreconditionException("Ошибка оплаты размещения серверной базы");
                }
                

                registerTaskInQueueProvider.RegisterTask(
                taskType: CoreWorkerTaskType.HandleRequestToChangeAccDbTypeJob,
                comment: $"Обработка заявки на смену режима платформы для базы {requestToChangeAccDbTypeDto.AccountDatabseId}",
                parametrizationModel: new ParametrizationModelDto(requestToChangeAccDbTypeDto.ToJson()));

                LogEventHelper.LogEvent(_unitOfWork, database.AccountId, AccessProvider, LogActions.EditInfoBase,
                    $"Задача на обработку заявки смены режима базы {database.V82Name} в {type} режим создана");
                logger.Info($"Задача на обработку заявки смены режима базы {requestToChangeAccDbTypeDto.AccountDatabseId} создана" +
                    $"\nданные в заявке: {requestToChangeAccDbTypeDto.ToJson()}");
                return Ok();
            }
            catch (PreconditionException ex)
            {
                logger.Warn(ex, $"Ошибка принятия заявки на смену режима базы {requestToChangeAccDbTypeDto.AccountDatabseId} из за оплаты");
                return PreconditionFailed($"Не удалось принять заявку по причине: {ex.Message}");
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка принятия заявки на смену режима базы] {requestToChangeAccDbTypeDto.AccountDatabseId}");
                return PreconditionFailed($"Не удалось принять заявку по причине: {ex.Message}");
            }
        }

        public async Task<ManagerResult<bool>> UpdateDatabase(Guid AccountDatabaseId)
        {
            try
            {
                var accountId = AccountIdByAccountDatabase(AccountDatabaseId)
                    ?? throw new InvalidOperationException("Аккаунт базы не найден");

                AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditSupport, () => accountId);
                
                var acDbSupport = await _unitOfWork.AcDbSupportRepository.FirstOrDefaultAsync(x => x.AccountDatabasesID == AccountDatabaseId)
                    ?? throw new InvalidOperationException($"Поддержка инф базы {AccountDatabaseId} не найдена");
                CheckUpdateDatabaseAbility(acDbSupport);

                var freeNode = autoUpdateAccountDatabasesProvider.GetCorrespondenceFreeNode(acDbSupport);

                if (_unitOfWork.UpdateNodeQueueRepository.Any(x => x.AccountDatabaseId == acDbSupport.AccountDatabasesID))
                {
                    var numberInQueue =
                        (await _unitOfWork.UpdateNodeQueueRepository.OrderByDescending(x => x.AddDate).ToListAsync())
                        .FindIndex(x => x.AccountDatabaseId == acDbSupport.AccountDatabasesID);
                    return Ok(true, $"Данная база уже поставлена в очередь на обновление, её место {numberInQueue}");
                }

                if (freeNode is not null)
                {
                    registerTaskInQueueProvider.RegisterTask(
                        CoreWorkerTasksCatalog.AccountDatabaseUpdateJob, 
                        new ParametrizationModelDto(new AccountDatabaseUpdateJobParamsDto {AccountDatabaseId = AccountDatabaseId }.ToJson()),
                        $"Обновление базы {AccountDatabaseId}");

                    Logger.Info($"Свободная нода найдена, джоба по обновлению базы {AccountDatabaseId} создана");
                    return Ok(true, $"Обновление успешно запущено");
                }
                
                var updateQueue = new UpdateNodeQueue { Id = Guid.NewGuid(), AccountDatabaseId = AccountDatabaseId, AddDate = DateTime.Now };
                _unitOfWork.UpdateNodeQueueRepository.Insert(updateQueue);
                await _unitOfWork.SaveAsync();
                logger.Info($"Свободная нода не найдена, база {AccountDatabaseId} поставлена в очередь на обновление");
                LogEventHelper.LogEvent(_unitOfWork, accountId, AccessProvider, LogActions.EditInfoBase,
                    $"База {acDbSupport.AccountDatabase.V82Name} поставлена в очередь на обновление");
                var countOfDatabaseInQueue = _unitOfWork.UpdateNodeQueueRepository.Count();

                return Ok(true, $"В данный момент все обновляторы заняты, Ваше место в очереди {countOfDatabaseInQueue}");
            }
            catch (AccessDeniedException ex)
            {
                return Forbidden<bool>(ex.Message);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка выполнения обновления инф базы] {AccountDatabaseId}");
                return PreconditionFailed<bool>($"Не удалось выполнить обновление инф. базы, Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Проверить возможность проведения обновления
        /// </summary>
        /// <returns>Результат проверки</returns>
        private void CheckUpdateDatabaseAbility(AcDbSupport support)
        {
            if (support.DatabaseHasModifications)
                throw new InvalidOperationException($"Инф. база {support.AccountDatabase.Caption} содержит доработки!");

            if (!planUpdateVersionAccountDatabaseProvider.CanPlugAutoUpdate(support))
                throw new InvalidOperationException($"Не совместимая конфигурация: {support.SynonymConfiguration}");

            if (!checkAccountDataProvider.CheckAvailabilityUseSupportOperation(support.AccountDatabase.Account))
                throw new InvalidOperationException("Услуга Автообновление будет доступна после оплаты сервиса " +
                                                    $"“{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, support.AccountDatabase.AccountId)}”");

            logger.Debug($"Проведение АО для базы {support.AccountDatabasesID} возможно");
        }
    }
}
