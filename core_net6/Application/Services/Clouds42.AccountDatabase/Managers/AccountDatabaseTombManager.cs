﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Managers
{

    /// <summary>
    /// Менеджер по переносу информационной базы в склеп.
    /// </summary>
    public class AccountDatabaseTombManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAccountDatabaseTombProvider accountDatabaseTombProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountDatabaseAnalyzeProvider accountDatabaseAnalyzeProvider,
        AccountDatabaseLogEventMessageBuilder accountDatabaseLogEventMessageBuilder)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Удалить информационную базу в склеп
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <param name="sendEmail">В случае ошибки, отправить письмо хотлайну</param>
        /// <param name="forceUpload">Принудительная загрузка файла в склеп</param>
        /// <param name="trigger">Триггер отправки базы в склеп</param>
        public ManagerResult<bool> DeleteAccountDatabaseToTomb(Guid accountDatabaseId, bool sendEmail, bool forceUpload = false,
            CreateBackupAccountDatabaseTrigger trigger = CreateBackupAccountDatabaseTrigger.ManualRemoval)
        {
            if (IsSourceDatabase(accountDatabaseId))
            {
                string message = "База является материнской";
                Logger.Warn(message);
                return PreconditionFailed<bool>(message);
            }
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_DeleteToTomb, () => accountDatabase.AccountId);

            try
            {
                if (accountDatabaseAnalyzeProvider.NeedMoveDbToTomb(accountDatabase, DatabaseState.DeletedToTomb))
                {
                    accountDatabaseTombProvider.DeleteAccountDatabaseToTomb(accountDatabase, sendEmail, forceUpload, trigger);
                    LogEvent(() => accountDatabase.AccountId, LogActions.DeleteAccountDatabase,
                        accountDatabaseLogEventMessageBuilder.BuildMessageBeforeDeletion(accountDatabase));
                    return Ok(true);
                }

                LogEvent(() => accountDatabase.AccountId, LogActions.DeleteAccountDatabase,
                    accountDatabaseLogEventMessageBuilder.BuildMessageAboutDeletion(accountDatabase));
                return Ok(true);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка перенесения ИБ {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} в склеп.]");
                LogEvent(() => accountDatabase?.AccountId, LogActions.DeleteAccountDatabase, $"Не удалось перенести базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} в склеп. Описание ошибки: {ex.Message}");
                return PreconditionFailed<bool>($"Не удалось удалить базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}. Описание ошибки: {ex.Message}");
            }
        }
        private bool IsSourceDatabase(Guid dbId) => 
            DbLayer.DelimiterSourceAccountDatabaseRepository.Any(ds => ds.AccountDatabaseId == dbId);
    }
}
