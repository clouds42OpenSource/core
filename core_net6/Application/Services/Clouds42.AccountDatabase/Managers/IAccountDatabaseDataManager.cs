﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.AccountDatabase.Managers;

public interface IAccountDatabaseDataManager
{
    /// <summary>
    /// Получить список баз, доступных пользователю.
    /// </summary>
    /// <param name="accountUserId">Номер пользователя.</param>
    Task<ManagerResult<List<AccountDatabasePropertiesDto>>> GetAccessDatabaseListAsync(Guid accountUserId);

    /// <summary>
    ///     Получить список баз на разделителях, доступных пользователю,
    /// с возможностью указания кода конфигурации.
    /// </summary>
    /// <param name="accountUserId">Идентификатор пользователя</param>
    /// <param name="configurationCode">Код конфигурации</param>
    Task<ManagerResult<List<DelimitersDbsPropertiesDto>>> GetDelimitersDbsByAccUserAsync(Guid accountUserId,
        string configurationCode = null);

    /// <summary>
    ///     Получить статус базы по Id
    /// </summary>
    /// <param name="accountDatabaseId">Идентификатор базы</param>
    Task<ManagerResult<DatabaseStatusDto>> GetDatabaseStatusInfo(Guid accountDatabaseId);

    /// <summary>
    /// Получить данные информационной базы по номеру.
    /// </summary>
    /// <param name="accountDatabaseId">Номер информационной базы.</param>
    Task<ManagerResult<AccountDatabasePropertiesDto>> GetAccountDatabasesByIdAsync(Guid accountDatabaseId);

    /// <summary>
    /// Получить данные информационной базы по номеру.
    /// </summary>
    /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
    /// <param name="accountUserId">Идентификатор пользователя который запрашивает сведения о базе.</param>
    Task<ManagerResult<AccountDatabasePropertiesDto>> GetAccountDatabasesForUserById(Guid accountDatabaseId, Guid accountUserId);

    /// <summary>
    /// Получить данные информационной базы по имени базы.
    /// </summary>
    /// <param name="v82Name">Имя информационной базы.</param>
    Task<ManagerResult<AccountDatabasePropertiesDto>> GetAccountDatabasesByNameAsync(string v82Name);

    /// <summary>
    /// Получить путь к бэкап хранилищу информационной базы.
    /// </summary>
    /// <param name="accountDatabaseId">Номер информационной базы.</param>
    Task<ManagerResult<SimpleResultModelDto<string>>> GetDatabaseBackupStoragePathAsync(Guid accountDatabaseId);

    /// <summary>
    /// Получить список пользователей, которые имеют доступ к базе
    /// </summary>
    /// <param name="accountDatabaseId">ID инф. базы</param>
    /// <returns>Список пользователей, которые имеют доступ к базе</returns>
    ManagerResult<AccountDatabaseUserAccessListDto> GetAccountDatabaseUserAccessList(Guid accountDatabaseId);

    /// <summary>
    /// Получить список пользователей, которые имеют доступ к базе
    /// </summary>
    /// <param name="accountDatabaseNumber">Номер инф. базы</param>
    /// <returns>Список пользователей, которые имеют доступ к базе</returns>
    ManagerResult<AccountDatabaseUserAccessListDto> GetAccountDatabaseUserAccessList(string accountDatabaseNumber);

    /// <summary>
    /// Удалить кэш 
    /// </summary>
    /// <param name="deleteAcDbCacheDto">модель очистки кэша</param>
    ManagerResult DeleteAccountDatabaseCache(DeleteAcDbCacheDto deleteAcDbCacheDto);
}
