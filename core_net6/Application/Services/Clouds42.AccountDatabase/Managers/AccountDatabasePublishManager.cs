﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal.ModelProcessors;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    ///     Отдельный менеджер для управлением публикаций баз
    /// </summary>
    public class AccountDatabasePublishManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        DbControlOptionsDomainModelProcessor dbControlOptionsDomainModelProcessor,
        IExecuteDatabasePublishTasksProvider executeDatabasePublishTasksProvider,
        AccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;


        /// <summary>
        ///     Отмена публикации базы
        /// </summary>
        public ManagerResult<bool> CancelPublishDatabase(PostRequestAccDbIdDto model)
        {
            if (model == null) return PreconditionFailed<bool>("Model is null");
            if (!model.IsValid) return PreconditionFailed<bool>("Model is not valid");

            var accountId = AccountIdByAccountDatabase(model.AccountDatabaseId);
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => accountId);
            try
            {
                var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == model.AccountDatabaseId);
                if (database == null)
                    return NotFound<bool>($"База по ID={model.AccountDatabaseId} не найдена.");

                executeDatabasePublishTasksProvider.CreateAndStartCancelPublishTask(database);

                LogEvent(() => accountId,
                    LogActions.UnpublishInfoBase,
                    $"Запущена таска на отмену публикации для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}");
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка отмены публикации] для базы {model.AccountDatabaseId}.");
                return Ok(false);
            }
            return Ok(true);
        }

        /// <summary>
        ///     Отмена публикации базы с ожиданием выполнения таски
        /// </summary>
        /// <param name="databaseId">Идентификатор базы</param>
        public ManagerResult<bool> CancelPublishDatabaseWithWaiting(Guid databaseId)
        {
            var accountId = AccountIdByAccountDatabase(databaseId);

            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => accountId);

            try
            {
                var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == databaseId);
                if (database == null)
                    return NotFound<bool>($"База по ID={databaseId} не найдена.");

                executeDatabasePublishTasksProvider.CreateAndStartCancelPublishTask(database).Wait();

                LogEvent(() => accountId,
                    LogActions.UnpublishInfoBase,
                    $"Запущена таска на отмену публикации для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}");
                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка отмены публикации базы] {databaseId}.");
                return Ok(false);
            }
        }

        /// <summary>
        ///     Публикация базы
        /// </summary>
        public ManagerResult<string> PublishDatabase(PublishDto model)
        {
            if (model == null) return PreconditionFailed<string>("Model is null");
            if (!model.IsValid) return PreconditionFailed<string>("Model is not valid");


            var accountId = AccountIdByAccountDatabase(model.AccountDatabaseId);
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => accountId);
            try
            {
                var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == model.AccountDatabaseId);
                if (database == null)
                    return NotFound<string>($"База по ID={model.AccountDatabaseId} не найдена.");

                executeDatabasePublishTasksProvider.CreateAndStartPublishTask(database);

                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"Для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} запущена задача публикации");

                return Ok(accountDatabaseWebPublishPathHelper.GetPublishPathWithoutChecks(database));
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка публикации базы] {model.AccountDatabaseId}");
                
                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"При публикации базы {model.AccountDatabaseId} произошла ошибка. Описание ошибки: {ex.GetFullInfo()}");
            }

            return GetWebPublishPath(model.AccountDatabaseId);
        }

        /// <summary>
        /// Перезапуск пула приложений аккаунта на нодах публикаций
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        /// <param name="accountUsersId">Идентификатор пользователя</param>
        public ManagerResult<DbControlOptionsDomainModel> RestartApplicationPoolOnIis(Guid accountDatabaseId)
        {
            var accountId = AccountIdByAccountDatabase(accountDatabaseId);
            var accountDatabase = DbLayer.DatabasesRepository.GetAccountDatabase(accountDatabaseId);
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_RestartIisAppPool, () => accountId);
                executeDatabasePublishTasksProvider.CreateAndStartRestartApplicationPoolTask(accountDatabaseId);

                LogEvent(() => accountId,
                    LogActions.RestartApplicationPoolOnIis,
                    $"Создана задача на перезапуск пула для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
            }
            catch (Exception ex)
            {
                var message = $"При перезапуске пула для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} на нодах публикаций произошла ошибка. Описание ошибки: {ex.GetFullInfo()}";
                _handlerException.Handle(ex, $"[Ошибка перезапуска пула для базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                return PreconditionFailed<DbControlOptionsDomainModel>(message);
            }

            var result = dbControlOptionsDomainModelProcessor.GetModel(accountId.Value, accountDatabaseId);

            return Ok(result);
        }

        /// <summary>
        ///     Публикация базы
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public ManagerResult<DbControlOptionsDomainModel> PublishDatabase(Guid accountDatabaseId)
        {
            var accountId = AccountIdByAccountDatabase(accountDatabaseId);

            logger.Trace($"Начинаем публикацию базы: {accountDatabaseId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => accountId);

                var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);
                if (database == null)
                    throw new InvalidOperationException($"База с ID={accountDatabaseId} не найдена");
                
                executeDatabasePublishTasksProvider.CreateAndStartPublishTask(database);

                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"Для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} запущена задача публикации.");
                var model = dbControlOptionsDomainModelProcessor.GetModel(accountId.Value,
                accountDatabaseId);
                return Ok(model);
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка публикации базы] {accountDatabaseId} ");
                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"При публикации базы {accountDatabaseId} произошла ошибка. Описание ошибки: {ex.GetFullInfo()}");
                return PreconditionFailed<DbControlOptionsDomainModel>(ex.Message);
            }

        }

        /// <summary>
        ///     Публикация базы с ожиданием выполнения таски
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public ManagerResult PublishDatabaseWithWaiting(Guid accountDatabaseId)
        {
            var accountId = AccountIdByAccountDatabase(accountDatabaseId);

            logger.Trace($"Начинаем публикацию базы: {accountDatabaseId}");

            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => accountId);

            try
            {
                var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);
                if (database == null)
                    throw new InvalidOperationException($"База с ID={accountDatabaseId} не найдена");

                executeDatabasePublishTasksProvider.CreateAndStartPublishTask(database).Wait();

                DbLayer.DatabasesRepository.Reload(database);

                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} опубликована.");

                return Ok();
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка публикации базы] {accountDatabaseId}");

                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"При публикации базы {accountDatabaseId} произошла ошибка. Описание ошибки: {ex.GetFullInfo()}");
            }

            return GetWebPublishPath(accountDatabaseId);
        }

        /// <summary>
        ///     Переопубликовать базу с ожиданием выполнения таски
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public ManagerResult RepublishDatabaseWithWaiting(Guid accountDatabaseId)
        {
            var accountId = AccountIdByAccountDatabase(accountDatabaseId);

            logger.Trace($"Начинаем переопубликацию базы: {accountDatabaseId}");

            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => accountId);

            try
            {
                var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);
                if (database == null)
                    throw new InvalidOperationException($"База с ID={accountDatabaseId} не найдена");

                executeDatabasePublishTasksProvider.CreateAndStartRepublishTask(database).Wait();

                DbLayer.DatabasesRepository.Reload(database);

                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"Для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} запущена задача на переопубликацию.");

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка переопубликации базы] {accountDatabaseId}");
                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"При переопубликации базы {accountDatabaseId} произошла ошибка. Описание ошибки: {ex.GetFullInfo()}");
            }

            return GetWebPublishPath(accountDatabaseId);
        }
        /// <summary>
        ///     Переопубликовать базу без ожидания выполнения таски
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public ManagerResult RepublishDatabaseWithoutWaiting(Guid accountDatabaseId)
        {
            var accountId = AccountIdByAccountDatabase(accountDatabaseId);

            logger.Trace($"Начинаем переопубликацию базы: {accountDatabaseId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => accountId);

                var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);
                if (database == null)
                    throw new InvalidOperationException($"База с ID={accountDatabaseId} не найдена");

                executeDatabasePublishTasksProvider.CreateAndStartRepublishTask(database);

                return Ok();
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка переопубликации базы] {accountDatabaseId}");
            }

            return GetWebPublishPath(accountDatabaseId);
        }

        /// <summary>
        ///     Переопубликовать базу с изменением файла web.config
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public ManagerResult RepublishWithChangingConfig(Guid accountDatabaseId)
        {
            var accountId = AccountIdByAccountDatabase(accountDatabaseId);

            logger.Trace($"Начинаем переопубликацию базы: {accountDatabaseId}");

            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => accountId);

            try
            {
                var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);
                if (database == null)
                    throw new InvalidOperationException($"База с ID={accountDatabaseId} не найдена");

                executeDatabasePublishTasksProvider.CreateAndStartRepublishWebConfigTask(database);

                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"Для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} запущена задача переопубликации со сменой web.config.");

                return Ok();
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка переопубликации базы] {accountDatabaseId}");
                LogEvent(() => accountId,
                    LogActions.PublishInfoBase,
                    $"При переопубликации базы {accountDatabaseId} произошла ошибка. Описание ошибки: {ex.GetFullInfo()}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        ///     Получить путь к опубликованной базе по ID
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public ManagerResult<string> GetWebPublishPath(Guid accountDatabaseId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Publish, () => AccountIdByAccountDatabase(accountDatabaseId));

            try
            {
                var accountDatabase = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);
                if (accountDatabase == null)
                    return NotFound<string>($"База с ID={accountDatabaseId} не найдена");

                var result = accountDatabaseWebPublishPathHelper.GetWebPublishPath(accountDatabase);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения пути для базы {accountDatabaseId}");
                return PreconditionFailed<string>(ex.Message);
            }
        }
    }
}
