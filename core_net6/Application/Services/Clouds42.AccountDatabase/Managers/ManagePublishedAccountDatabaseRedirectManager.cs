﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    /// Менеджер для управления редиректом для опубликованной базы
    /// </summary>
    public class ManagePublishedAccountDatabaseRedirectManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IManagePublishedAccountDatabaseRedirectProvider managePublishedAccountDatabaseRedirectProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Обработать операцию управления редиректом для инф. базы
        /// </summary>
        /// <param name="manageAcDbRedirectParams">Параметры управления редиректом</param>
        /// <returns>Ok - если операция завершена успешно</returns>
        public ManagerResult ManageAcDbRedirect(ManagePublishedAcDbRedirectParamsDto manageAcDbRedirectParams)
        {
            try
            {
                managePublishedAccountDatabaseRedirectProvider.ManageAcDbRedirect(manageAcDbRedirectParams);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка управления редиректом для инф. базы {manageAcDbRedirectParams.AccountDatabaseV82Name}. ]" +
                                $"Тип операции: {manageAcDbRedirectParams.OperationType.Description()}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
