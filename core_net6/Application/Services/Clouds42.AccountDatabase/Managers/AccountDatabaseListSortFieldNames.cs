﻿namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки списка информационных баз
    /// </summary>
    public static class AccountDatabaseListSortFieldNames
    {
        /// <summary>
        ///  Дата создания
        /// </summary>
        public const string CreationDate = "creationdate";

        /// <summary>
        /// Дата последней активности
        /// </summary>
        public const string LastActivityDate = "lastactivitydate";
    }
}