﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Internal;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Domain.Enums.Link42;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    /// Менеджеры для работы с ифн. базами
    /// </summary>
    public partial class AccountDatabaseManager(
        IAcDbSupportHistoryRegistrator supportHistoryRegistrator,
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountDatabaseInfoDataProvider accountDatabaseInfoDataProvider,
        Run1CEpfMcobCommandHandler run1CEpfMcobCommandHandler,
        IHandlerException handlerException,
        ISupportAuthorizationProvider supportAuthorizationProvider,
        IAccountDatabaseSupportProvider accountDatabaseSupportProvider,
        IAccountDatabaseProvider accountDatabaseProvider,
        DatabaseSupportSettingHelper databaseSupportSettingHelper,
        IAccountDatabaseAnalyzeProvider accountDatabaseAnalyzeProvider,
        RdpHelperFactory rdpHelperFactory,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountDatabaseManager
    {
        readonly IHandlerException _handlerException = handlerException;
        readonly IAccessProvider _accessProvider = accessProvider;
        readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        ///     Пересчитать размер информационной базы
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        public void RecalculateSizeOfAccountDatabase(Guid accountDatabaseId)
        {
            var database = GetAccountDatabase(accountDatabaseId);
            if (database == null)
                return;

            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => database.AccountId);
            accountDatabaseAnalyzeProvider.RecalculateSizeOfAccountDatabase(database);
        }

        /// <summary>
        ///  Получить список баз которые надо заархивировать
        /// </summary>
        public ManagerResult<List<ArchiveDatabaseModelDto>> GetArchiveDatabaseList()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => AccessProvider.ContextAccountId);
                logger.Debug("Получить список баз которые надо заархивировать");
                var archiveDatabaseList = new ArchiveDatabaseHelper(DbLayer, logger).ArchiveDatabaseList();
                logger.Debug($"Получено {archiveDatabaseList.Count} баз, которые надо заархивировать: {string.Join(",", archiveDatabaseList.Select(b => b.V82Name))}");
                return Ok(archiveDatabaseList);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения списка баз для архивации]");
                return PreconditionFailed<List<ArchiveDatabaseModelDto>>($"Не удалось получить список баз для архивации. Детали: {ex.Message}");
            }
        }

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>                
        public ManagerResult<ConnectorResultDto<MetadataResultDto>> GetMetadataInfoAccountDatabase(Guid accountDatabaseId)
        {

            var support = accountDatabaseSupportProvider.GetAcDbSupport(accountDatabaseId);
            if (support == null)
            {
                var errorMessage = $"Не удалось найти данные о поддержке базы по номеру - {accountDatabaseId}";
                logger.Warn(errorMessage);
                return PreconditionFailed<ConnectorResultDto<MetadataResultDto>>(errorMessage);
            }

            AccessProvider.HasAccess(ObjectAction.AcDbSupport_AutorizationConnect, () => support.AccountDatabase.AccountId);

            var accountDatabase = GetAccountDatabase(accountDatabaseId);
            if (accountDatabase == null)
                return PreconditionFailed<ConnectorResultDto<MetadataResultDto>>(
                    $"Не удалось найти информационную базу по Id {accountDatabaseId}");

            try
            {
                var result = supportAuthorizationProvider.GetMetadataInfoAccountDatabase(support);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения метаданных ИБ] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");
                return PreconditionFailed<ConnectorResultDto<MetadataResultDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Returns list of database IDs in scope of specific account
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public ManagerResult<IEnumerable<Guid>> GetIDs(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => accountId);
            var result = GetIds(accountId);
            return Ok(result);
        }

        //#endregion

        #region POST methods


        /// <summary>
        /// Sets last client activity date
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ManagerResult SetLastActivityDate(PostRequestAccDbLastActivityDateDto model)
        {
            if (model == null) return PreconditionFailed("Model is null");

            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => AccountIdByAccountDatabase(model.AccountDatabaseID));

            if (!model.IsValid) return PreconditionFailed("Model is not valid");
            Domain.DataModels.AccountDatabase accountDatabase =
                DbLayer.DatabasesRepository.GetAccountDatabase(model.AccountDatabaseID);
            if (accountDatabase == null)
                return NotFound($"Account database with Id: {model.AccountDatabaseID} not found");

            accountDatabase.LastActivityDate = model.LastActivityDate;
            DbLayer.DatabasesRepository.InsertOrUpdateAccountDatabaseValue(accountDatabase);
            return Ok();
        }

        public ManagerResult ChangeDbFlagUsedWebServices(Guid databaseId, bool? usedWebServices)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_ChangeDbFlagUsedWebServices, () => AccountIdByAccountDatabase(databaseId));

                var accountDatabase = DbLayer.DatabasesRepository.GetAccountDatabase(databaseId);
                if (accountDatabase == null || accountDatabase.UsedWebServices == usedWebServices)
                    return PreconditionFailed("База не найдена, или флаг остался не изменным");

                accountDatabase.UsedWebServices = usedWebServices;
                DbLayer.DatabasesRepository.Update(accountDatabase);
                DbLayer.Save();
                return Ok();
            }
            catch (AccessDeniedException ex)
            {
                return Forbidden(ex.Message);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка смены флага 'опубликованы веб сервисы']");
                return PreconditionFailed(ex.Message);
            }
            
        }

        
        /// <summary>
        /// Sets 1C application name
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ManagerResult SetApplicationName(PostRequestAppNameDto model)
        {
            if (model == null) return PreconditionFailed("Model is null");
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => AccountIdByAccountDatabase(model.AccountDatabaseID));
            if (!model.IsValid) return PreconditionFailed("Model is not valid");
            var uow = DbLayer;
            var accountDatabase = uow.DatabasesRepository.FirstOrDefault(x => x.Id == model.AccountDatabaseID);
            if (accountDatabase == null)
                return NotFound($"Account database with Id: {model.AccountDatabaseID} not found");

            try
            {
                accountDatabase.ApplicationName = model.ApplicationName.Trim();
                accountDatabase.DistributionTypeEnum = DistributionType.Stable;
                uow.DatabasesRepository.Update(accountDatabase);
                uow.Save();
                return Ok();
            }
            catch (Exception e)
            {
                var errModel = new ErrorStructuredDto
                {
                    Code = 409,
                    Description = "Application name setting failed",
                    DebugInfo =
                        $"Error description: {(e.InnerException == null ? e.Message : e.InnerException.Message)}"
                };
                return ConflictError(errModel);
            }
        }

        #endregion

        public ManagerResult<AccountDatabaseRemoteParamsDto> GetRemoteAppParams(Guid accountDatabaseId, Guid accountUserId, string launchMode = default)
        {
            if (accountDatabaseId == Guid.Empty)
                return BadRequest<AccountDatabaseRemoteParamsDto>("Параметр accountDatabaseId не задан");

            if (accountUserId == Guid.Empty)
                return BadRequest<AccountDatabaseRemoteParamsDto>("Параметр accountUserId не задан");

            var accUser = DbLayer.AccountUsersRepository.FirstOrDefault(x => x.Id == accountUserId);
            if (accUser == null)
                return NotFound<AccountDatabaseRemoteParamsDto>($"Account user with Id: {accountUserId} not found");

            if (!AccessProvider.HasAccessBool(ObjectAction.AccountDatabase_Detail,
                    () => AccountIdByAccountDatabase(accountDatabaseId), null,
                    () => AccountHasAccessToDataBase(accUser.AccountId, accountDatabaseId)))
                return Unauthorized<AccountDatabaseRemoteParamsDto>("доступ запрещен");

            logger.Debug("[GetRemoteAppParams] AccDbId is {0}", accountDatabaseId);

            var accountDatabase = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);
            if (accountDatabase == null)
                return NotFound<AccountDatabaseRemoteParamsDto>($"Account database with Id: {accountDatabaseId} not found");

            var login = accUser.Login;

            var remoteAppLink = rdpHelperFactory.CreateForAccountUser(accountUserId).BuildRemoteAppLinkString(login, accountDatabaseId.ToString(), launchMode);

            var farm = _dbLayer.AccountUsersRepository
                .AsQueryableNoTracking()
                .Include(x => x.Account.AccountConfiguration.Segment.CloudServicesTerminalFarm)
                .FirstOrDefault(x => x.Id == accountUserId);

            return Ok(new AccountDatabaseRemoteParamsDto { AccountDatabaseRemoteParams = remoteAppLink, UsingOutdatedWindows = farm?.Account?.AccountConfiguration?.Segment?.CloudServicesTerminalFarm?.UsingOutdatedWindows ?? true});
        }

        
        /// <summary>
        /// Добавляет базам аккаунта запись, что пользователь уведомлен о функционале АО и ТИИ
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>Добавилась запись или нет</returns>
        public async Task<ManagerResult<bool>> AddUserNotifiedState(Guid accountId)
        {
            try
            {
                var accountDatabase = await DbLayer.AccountsRepository
                    .AsQueryable()
                    .Where(a => a.Id == accountId)
                    .SelectMany(a => a.AccountDatabases.Where(ad => 
                        ad.AccountDatabaseOnDelimiter == null && ad.State == "Ready"))
                    .FirstOrDefaultAsync();
                
                if (accountDatabase == null)
                {
                    return false.ToOkManagerResult();
                }
            
                await supportHistoryRegistrator.RegisterNotificationRead(accountDatabase);
                return true.ToOkManagerResult();
            }
            catch (Exception e)
            {
                string message = "Ошибка при попытке проставить базам статус, что пользователь уведомлен о АО и ТИИ " +
                                 e.Message;
                logger.Error(message);
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(message);
            }
           
          
        }

        public ManagerResult<string> BuildRemoteDesktopLinkString(Guid accountUserId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => AccountIdByAccountUser(accountUserId));
            var uow = DbLayer;
            var accUser = uow.AccountUsersRepository.FirstOrDefault(x => x.Id == accountUserId);
            if (accUser == null)
                return NotFound<string>($"Account user with Id: {accountUserId} not found");

            var login = accUser.Login;

            var result = rdpHelperFactory.CreateForAccountUser(accountUserId).BuildRemoteDesktopLinkString(login);
            return Ok(result);
        }
        public ManagerResult SetLaunchType(PostRequestLaunchTypeDto model)
        {
            if (model == null)
                return PreconditionFailed("Model is null");

            AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditLaunchType, () => AccountIdByAccountDatabase(model.AccountDatabaseID), () => AccessProvider.GetUser().Id);

            if (!model.IsValid)
                return PreconditionFailed("Model is not valid");


            if (model.AccountUserId == Guid.Empty)
                model.AccountUserId = AccessProvider.GetUser().Id;

            try
            {
                new UpdateDatabaseLaunchSettingProvider(DbLayer, model, logger).UpdateLaunchType();
                return Ok();
            }
            catch (Exception e)
            {
                var errModel = new ErrorStructuredDto
                {
                    Code = 409,
                    Description = "Launch type setting failed",
                    DebugInfo =
                        $"Error description: {(e.InnerException == null ? e.Message : e.InnerException.Message)}"
                };
                return ConflictError(errModel);
            }
        }

        public ManagerResult<double> GetAccountBallanse(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => accountId);

            var bacc = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Id == accountId);
            if (bacc == null)
                return NotFound<double>($"Billing account with Id: {accountId} not found");
            return Ok((double)bacc.Balance);

        }

        /// <summary>
        /// Получить список информационных баз
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="searchQuery">Фильр поиска</param>
        /// <returns>Список информационных баз</returns>
        public ManagerResult<AccountDatabasesListDataDto> GetAccountDatabasesViewModel(int page, string searchQuery)
        {
            try
            {
                AccessProvider.HasAccessForMultiAccounts(ObjectAction.AccountDatabase_Detail);
                var data = accountDatabaseInfoDataProvider.GetAccountDatabasesList(page, searchQuery);
                logger.Trace("Получение списка информационных баз завершено успешно.");

                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получении списка информационных баз] ");
                return PreconditionFailed<AccountDatabasesListDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Сохранить локальную базу аккаунта
        /// </summary>
        /// <param name="model">База аккаунта</param>
        /// <returns>ID измененной базы</returns>
        public ManagerResult<Guid> SaveLocalAccountDatabases(SaveLocalAccountDatabasesDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => model.AccountID);

            var account = DbLayer.DatabasesRepository.FirstOrDefault(x => x.AccountId == model.AccountID);
            if (account == null)
                return NotFound<Guid>($"Account with Id: {model.AccountID} not found");

            var db = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == model.ID);
            if (db == null)
                return NotFound<Guid>($"Database with Id '{model.ID}' not found");

            db.AccountId = model.AccountID;
            db.Caption = model.Caption;
            db.LaunchType = model.Launch;
            db.ApplicationName = model.Platform.ToString();
            db.DistributionTypeEnum = DistributionType.Stable;
            db.PublishStateEnum = PublishState.Unpublished;
            db.V82Server = model.ServerName;
            db.V82Name = model.SqlName;
            db.IsFile = model.IsFile;

            db.StateEnum = DatabaseState.Undefined;

            DbLayer.Save();

            return Ok(db.Id);
        }

        public ManagerResult<List<LocalAccountDatabasesDto>> GetLocalAccountDatabases(Guid accountID)
        {
            if (accountID == Guid.Empty)
                return BadRequest<List<LocalAccountDatabasesDto>>("Не задан параметр accountID");

            if (!AccessProvider.HasAccessBool(ObjectAction.AccountDatabases_Add, () => accountID))
                return Ok(new List<LocalAccountDatabasesDto>());

            var account = DbLayer.DatabasesRepository.FirstOrDefault(x => x.AccountId == accountID);
            if (account == null)
                return NotFound<List<LocalAccountDatabasesDto>>($"Account with Id: {accountID} not found");

            var accountLocalDB = DbLayer.DatabasesRepository
                .Where(z => z.AccountId == accountID)
                .Where(x => string.IsNullOrEmpty(x.ServiceName));

            var list = new List<LocalAccountDatabasesDto>();
            accountLocalDB.ToList().ForEach(db => list.Add(new LocalAccountDatabasesDto
            {
                ID = db.Id,
                AccountID = db.AccountId,
                Caption = db.Caption,
                Launch = (LaunchType)(db.LaunchType ?? 0),
                Platform = db.PlatformType,
                ServerName = db.V82Server,
                SqlName = db.V82Name,
                IsFile = db.IsFile ?? false,
            }));
            return Ok(list);
        }

        /// <summary></summary>
        public void Run1CEpfMcob(string v82Name, bool isFile, string filePath, string parameters, PlatformType platform)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_McobEditRolesOfDatabase);
            run1CEpfMcobCommandHandler.Handle(new Run1CEpfMcobCommand(v82Name, isFile, filePath, parameters, platform));
        }
        public AccountDatabaseSupportSettingDto GetDatabaseSupportSettings(Guid databaseId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditSupport, () => AccountIdByAccountDatabase(databaseId));
            return databaseSupportSettingHelper.GetDatabaseSupportSettings(databaseId);
        }

        public ManagerResult<bool> SaveSupportDataAndAuthorize(SupportDataDto authorizeInDatabaseDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditSupport, () => AccountIdByAccountDatabase(authorizeInDatabaseDto.DatabaseId));
                databaseSupportSettingHelper.SaveSupportDataAndAuthorize(authorizeInDatabaseDto);
                return Ok(true);
            }
            catch (AccessDeniedException ex)
            {
                return Forbidden<bool>(ex.Message);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка сохранения данных поддержки] {authorizeInDatabaseDto.DatabaseId}");
                return PreconditionFailed<bool>($"[Ошибка сохранения данных поддержки] {authorizeInDatabaseDto.DatabaseId}: {ex.Message}");
            }
        }

        /// <summary>
        /// Отключить авторизацию в инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        public ManagerResult DisableSupportAuthorization(Guid accountDatabaseId)
        {
            var accountDatabase = GetAccountDatabase(accountDatabaseId);
            if (accountDatabase == null)
                return PreconditionFailed($"Информационная база по Id {accountDatabaseId} не найдена");

            var logEventMessage =
                $"Отмена авторизации в базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}";

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditSupport, () => accountDatabase.AccountId);
                accountDatabaseSupportProvider.DisableSupportAuthorization(accountDatabaseId);

                LogEvent(() => accountDatabase.AccountId, LogActions.DisableConnectInfoBase, logEventMessage);
                return Ok();
            }
            catch (Exception ex)
            {
                LogEvent(() => accountDatabase.AccountId, LogActions.ErrorConnectInfoBase,
                    $"{logEventMessage} завершена с ошибкой. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Попытка авторизации в информационную базу под администратором.
        /// </summary>
        /// <param name="accountDatabaseId">Номер объекта поддержки ИБ</param>
        /// <returns>Признак успешности.</returns>
        public ManagerResult TryAdminAuthorizeToAccountDatabase(Guid accountDatabaseId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditSupport, () => AccountIdByAccountDatabase(accountDatabaseId));

                var support = DbLayer.AcDbSupportRepository.FirstOrDefault(x => x.AccountDatabasesID == accountDatabaseId);
                if (support == null)
                {
                    var errorMessage = $"Не удалось найти данные о поддержке базы по номеру - {accountDatabaseId}";
                    logger.Warn(errorMessage);
                    return PreconditionFailed(errorMessage);
                }

                var res = supportAuthorizationProvider.TryAdminAuthorize(support);
                var textResult = res ? "успешно" : "неудачно";
                SendEndAuthorizeLetter($"Авторизация в базу {support.AccountDatabase.V82Name} прошла {textResult}", support.AccountDatabase.V82Name, support.AccountDatabase.AccountId);

                if (!res)
                {
                    var messageError =
                        $"Ошибка авторизации в базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(support.AccountDatabase)}.";

                    LogEvent(() => support.AccountDatabase.AccountId, LogActions.ErrorConnectInfoBase, messageError);
                    return PreconditionFailed(messageError);
                }

                LogEvent(() => AccountIdByAccountDatabase(support.AccountDatabasesID),
                    LogActions.SuccessConnectInfoBase,
                    $"Выполнена авторизация в базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(support.AccountDatabase)}.");

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"Произошла ошибка при попытке авторизации в базу {accountDatabaseId}: {ex.Message}";
                _handlerException.Handle(ex, message);
                return PreconditionFailed($"Произошла ошибка при авторизации: {ex.Message}");
            }
        }



        public ManagerResult SaveLaunchParameters(Guid accountDatabaseID, string launchParameter)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => AccountIdByAccountDatabase(accountDatabaseID));

                var db = DbLayer.DatabasesRepository.GetAccountDatabase(accountDatabaseID);
                if (db == null)
                    return NotFound("Databases not found ");


                db.LaunchParameters = string.IsNullOrEmpty(launchParameter) ? null : launchParameter;
                DbLayer.DatabasesRepository.Update(db);
                DbLayer.Save();

                return Ok();
            }
            catch(AccessDeniedException ex)
            {
                logger.Warn(ex,$"Ошибка доступа к сохранению параметров в базу {accountDatabaseID} :: {ex.Message}");
                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка сохранения LaunchParameters] база: {accountDatabaseID}";
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }

        #region internal


        internal IEnumerable<Guid> GetIds(Guid accountId)
        {
            var dbIds = DbLayer.DatabasesRepository
                .Where(owned => owned.AccountId == accountId)
                .Select(i => i.Id);
            return dbIds;
        }

        #endregion

        public ManagerResult SetDistributiveType(PostRequestSetDistributiveTypeDto model)
        {
            if (model == null) return PreconditionFailed("Model is null");

            // Права требуются те же, что и для установки версии платформы.
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => AccountIdByAccountDatabase(model.AccountDatabaseID));

            var accountDatabase = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == model.AccountDatabaseID);

            if (accountDatabase == null)
                return NotFound($"Account database with Id: {model.AccountDatabaseID} not found");

            try
            {
                accountDatabase.DistributionType = model.DistributiveType;

                DbLayer.DatabasesRepository.Update(accountDatabase);
                DbLayer.Save();
                return Ok();
            }
            catch (Exception e)
            {
                var errModel = new ErrorStructuredDto
                {
                    Code = 409,
                    Description = "Application name setting failed",
                    DebugInfo =
                        $"Error description: {(e.InnerException == null ? e.Message : e.InnerException.Message)}"
                };
                return ConflictError(errModel);
            }
        }

        /// <summary>
        /// Получить информацию  о сервисе в информационной базе
        /// </summary>
        /// <param name="accountDatabaseId">Для какой базы получить информацию о сервисе в ней</param>
        /// <returns>Информацию  о сервисе в информационной базе</returns>
        public async Task<ManagerResult<ServiceExtensionDatabaseInfoDto>> GetServiceExtensionStatusesForAccountDatabase(
            Guid accountDatabaseId)
        {
            AccessProvider.HasAccess(ObjectAction.ManageServiceExtensionDatabase, () => AccessProvider.ContextAccountId);

            try
            {
                return Ok(await accountDatabaseProvider.GetServiceExtensionStatusesForAccountDatabase(accountDatabaseId));
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка получения информации о сервисах в информационной базе] база: {accountDatabaseId}";
                _handlerException.Handle(ex, message);
                return Conflict<ServiceExtensionDatabaseInfoDto>(message + ex.Message);
            }
        }

        public ManagerResult<IEnumerable<DatabaseAccessRightsDto>> GetAccountDatabaseAccess(List<ObjectAction> objectActions, Guid databaseId)
        {
            var ownerDatabaseId = _accessProvider.AccountIdByAccountDatabase(databaseId);

            var props = objectActions.Distinct().ToDictionary<ObjectAction, string, object>(
                objectAction => objectAction.ToString(),
                objectAction => _accessProvider.HasAccessBool(objectAction, () => ownerDatabaseId));

            var result = props.Select(s => new DatabaseAccessRightsDto { ObjectAction = s.Key, HasAccess = (bool)s.Value });

            return Ok(result);
        }

        /// <summary>
        /// Отправить письмо об окончании авторизации
        /// </summary>
        /// <param name="emailContent">Содержимое письма</param>
        /// <param name="databaseName">имя базы</param>
        /// <param name="accountId">аккаунт айди</param>
        private void SendEndAuthorizeLetter(string emailContent, string databaseName, Guid accountId)
        {
            var accountAdminId = _accessProvider.GetAccountAdmins(accountId).FirstOrDefault();
            var accountAdminEmail = _dbLayer.AccountUsersRepository.FirstOrDefault(x => x.Id == accountAdminId).Email;

            logger.Debug($"Отправляю письмо об окончании авторизации на {accountAdminEmail}");
            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(accountAdminEmail)
                .Body(emailContent)
                .Subject($"Авторизация в базу {databaseName}")
                .SendViaNewThread();
        }
    }
}
