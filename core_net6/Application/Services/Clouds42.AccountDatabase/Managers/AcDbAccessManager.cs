﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    /// Менеджер для работы с доступами к инф. базам
    /// </summary>
    public class AcDbAccessManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAcDbAccessProvider acDbAccessProvider,
        AccessForAcDbFolderHelper accessForAcDbFolderHelper,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Добавить доступ к инф. базе
        /// </summary>
        /// <param name="model">Параметры предоставления доступа</param>
        /// <returns>Доступ к инф. базе</returns>
        public ManagerResult<Guid> Add(AcDbAccessPostAddModelDto model)
        {
            if (!model.IsValid)
                return PreconditionFailed<Guid>("Model is invalid");

            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Add, () => model.AccountID);
            
            try
            {
                var acDbAccess = acDbAccessProvider.GetOrCreateAccess(model);
                return Ok(acDbAccess.ID);
            }
            catch (Exception ex)
            {
                return ConflictError<Guid>(CreateErrorResultObj("Adding new access failed", $"Error description: {ex.GetMessage()}"));
            }
        }

        /// <summary>
        /// Удалить доступ к инф. базе
        /// </summary>
        /// <param name="model">Параметры удаления доступа</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult Delete(AcDbAccessPostIDDto model)
        {
            if (model is null || !model.IsValid())
                return PreconditionFailed("Model is invalid");

            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Delete, () => AccountIdByAcDbAccess(model.AccessID));

            try
            {
                acDbAccessProvider.DeleteAccess(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return ConflictError(CreateErrorResultObj("Deleting access failed", $"Error description: {ex.GetMessage()}"));
            }
        }

        /// <summary>
        /// Удалить доступы аккаунта к инф. базе
        /// </summary>
        /// <param name="model">Параметры удаления доступов</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult DeleteAccountAccesses(DeleteAccessDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Delete, () => model.AccountId);

            try
            {
                acDbAccessProvider.DeleteAccountAccesses(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return ConflictError(CreateErrorResultObj("Deleting access failed", $"Error description: {ex.GetMessage()}"));
            }
        }

        /// <summary>
        /// Установить ID аккаунта для доступа
        /// </summary>
        /// <param name="model">Параметры установки ID аккаунта для доступа</param>
        /// <returns>Результат установки</returns>
        public ManagerResult SetAccountId(AcDbAccessPostSetAccIdDto model)
        {
            if (!model.IsValid)
                return PreconditionFailed("Model is invalid");

            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Edit, () => model.AccountID);

            try
            {

                acDbAccessProvider.SetAccountId(model);
                return Ok(new EmptyResultDto());
            }
            catch (Exception ex)
            {
                return ConflictError(CreateErrorResultObj("Setting property failed", $"Error description: {ex.GetMessage()}"));
            }
        }

        /// <summary>
        /// Добавить доступ для всех пользователей
        /// </summary>
        /// <param name="model">Параметры добавления доступов</param>
        /// <returns>Список добавленных доступов</returns>
        public ManagerResult<List<Guid>> AddAccessesForAllAccountsUsers(AcDbAccessPostAddAllUsersModelDto model)
        {
            if (model == null)
                return PreconditionFailed<List<Guid>>("Model is null");

            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_AddAccessesForAllAccountsUsers, () => model.AccountID);

            try
            {
                return Ok(acDbAccessProvider.AddAccessesForAllAccountsUsers(model));
            }
            catch (Exception ex)
            {
                return ConflictError<List<Guid>>(CreateErrorResultObj("Adding new accesses failed", $"Error description: {ex.GetMessage()}"));
            }
        }

        /// <summary>
        /// Изменить доступ к папке
        /// </summary>
        /// <param name="action">Тип действия управления доступом</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        public ManagerResult ChangeAccessForDbFolder(Guid accountDatabaseId, Guid accountUserId, AclAction action)
        {
            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Edit);

            try
            {
                accessForAcDbFolderHelper.ChangeAccessForDbFolder(accountDatabaseId, accountUserId, action);
                return Ok();
            }
            catch (Exception ex)
            {
                return ConflictError(CreateErrorResultObj("Adding access to folder failed", $"Error description: {ex.GetMessage()}"));
            }
        }

        /// <summary>
        /// Установить ID пользователя для доступа
        /// </summary>
        /// <param name="model">Параметры установки ID пользователя для доступа</param>
        /// <returns>Результат установки</returns>
        public ManagerResult SetAccountUserId(AcDbAccessAccountUserIDPostDto model)
        {
            if (!model.IsValid)
                return PreconditionFailed("Model is invalid");

            AccessProvider.HasAccess(ObjectAction.AcDbAccesses_Edit, () => AccountIdByAcDbAccess(model.AccessID));

            try
            {

                acDbAccessProvider.SetAccountUserId(model);
                return Ok(new EmptyResultDto());
            }
            catch (Exception ex)
            {
                return ConflictError(CreateErrorResultObj("Setting property failed", $"Error description: {ex.GetMessage()}"));
            }
        }

        /// <summary>
        /// Создать результат с ошибкой
        /// </summary>
        /// <param name="description">Описание</param>
        /// <param name="debugInfo">Информация для отладки</param>
        /// <returns>Результат с ошибкой</returns>
        private static ErrorStructuredDto CreateErrorResultObj(string description, string debugInfo)
            => new()
            {
                Code = 409,
                Description = description,
                DebugInfo = debugInfo
            };
    }
}
