﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    /// Менеджер для работы с данными инф. базы для отчета
    /// </summary>
    public class AccountDatabaseReportDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAccountDatabaseReportDataProvider accountDatabaseReportDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить список моделей данных по инф. базам для отчета
        /// </summary>
        /// <returns>Список моделей данных по инф. базам для отчета</returns>
        public ManagerResult<List<AccountDatabaseReportDataDto>> GetAccountDatabaseReportDataDcs()
        {
            try
            {
                logger.Info("Получение списка моделей данных по инф. базам для отчета");
                AccessProvider.HasAccess(ObjectAction.GetAccountDatabaseReportDataDcs);
                var data = accountDatabaseReportDataProvider.GetAccountDatabaseReportDataDcs();
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения списка моделей данных по инф. базам]");
                return PreconditionFailed<List<AccountDatabaseReportDataDto>>(ex.Message);
            }
        }
    }
}
