﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    /// Менеджер для удаления инф. баз
    /// </summary>
    public class DeleteAccountDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IDeleteAccountDatabaseProvider deleteAccountDatabaseProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Удалить инф. базу
        /// </summary>
        /// <param name="deleteAccountDatabaseParams">Параметры удаления инф. базы</param>
        /// <returns>Ok - если база удалена успешно</returns>
        public ManagerResult DeleteAccountDatabase(DeleteAccountDatabaseParamsDto deleteAccountDatabaseParams)
        {
            try
            {
                deleteAccountDatabaseProvider.DeleteAccountDatabase(deleteAccountDatabaseParams);
                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка удаления информационной базы {deleteAccountDatabaseParams.AccountDatabaseId}]";
                _handlerException.Handle(ex, message, () => deleteAccountDatabaseParams.AccountDatabaseId, () => deleteAccountDatabaseParams.InitiatorId);
                return PreconditionFailed($"{message}: {ex.GetFullInfo(false)}.");
            }
        }
    }
}
