﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Managers
{

    /// <summary>
    /// Менеджер управления доступами в базах МЦОБ.
    /// </summary>
    public class AccountDatabaseMcobManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountDatabaseMcobProvider accountDatabaseMcobProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Предоставить доступ пользователю к базе с роллями.
        /// </summary>
        /// <param name="dataBaseName">Имя базы.</param>
        /// <param name="userLogin">Логин пользователя.</param>
        /// <param name="userName">Имя пользователя в базе 1С.</param>
        /// <param name="roles">Список профилей, через ;</param>
        /// <param name="jhoRoles">Список ролей ЖХО.</param>        
        public ManagerResult<string> McobAddUserToDatabase(string dataBaseName, string userLogin, string userName, string roles, string jhoRoles)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_McobEditRolesOfDatabase);

            try
            {
                if (IsSourceDatabase(dataBaseName))
                {
                    string message = "База является материнской";
                    Logger.Warn(message);
                    return PreconditionFailed<string>(message);
                }
                var result = accountDatabaseMcobProvider.McobAddUser(dataBaseName, userLogin, userName, roles, jhoRoles);
                return result.OperationSuccess ? Ok(nameof(Ok)) : PreconditionFailed<string>(result.OperationMessage);
            }
            catch (Exception ex)
            {
                Logger.Warn("[Ошибка предоставления доступа пользователю МЦОБ]", ()=> dataBaseName, ()=> userLogin, ()=> userName, ()=> roles, ()=> jhoRoles);
                return PreconditionFailed<string>(ex.Message); 
            }
        }


        private bool IsSourceDatabase(string dbName)
        {
            var database = DbLayer.DatabasesRepository.FirstOrDefault(d => d.V82Name == dbName);
            if (database == null)
                return false;

            return  DbLayer.DelimiterSourceAccountDatabaseRepository.Any(ds => ds.AccountDatabaseId == database.Id);
        }

        /// <summary>
        /// Забрать доступ пользоватля к базе.
        /// </summary>
        /// <param name="dataBaseName">Имя базы.</param>
        /// <param name="userLogin">Логин пользователя.</param>        
        /// <param name="roles">Список профилей, через ;</param>                
        public ManagerResult<string> McobRemoveUserRolesToDatabase(string dataBaseName, string userLogin, string roles)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_McobRemoveUserRoles);
            try
            {
                var result = accountDatabaseMcobProvider.McobRemoveUserRoles(dataBaseName, userLogin, roles);
                return result.OperationSuccess ? Ok(nameof(Ok)) : PreconditionFailed<string>(result.OperationMessage);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка закрытия доступа пользователю '{userLogin}' к базе '{dataBaseName}' с роллями '{roles}'.]");
                return PreconditionFailed<string>(ex.Message);
            }
        }
       
    }
}
