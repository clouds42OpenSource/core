﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Internal.ModelProcessors;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Managers
{
    /// <summary>
    /// Менеджер доступа данных информационной базы.
    /// </summary>
    public class AccountDatabaseDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountDatabaseBackupStorageDataProvider accountDatabaseBackupStorageDataProvider,
        IHandlerException handlerException,
        AccountDatabaseAccessDataProvider accountDatabaseAccessDataProvider,
        ILogger42 logger,
        IRecalculateMyDiskUsedSizeProvider recalculateMyDiskUsedSizeProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountDatabaseDataManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить список баз, доступных пользователю.
        /// </summary>
        /// <param name="accountUserId">Номер пользователя.</param>
        public async Task<ManagerResult<List<AccountDatabasePropertiesDto>>> GetAccessDatabaseListAsync(Guid accountUserId)
        {
            try
            {
                if (accountUserId == Guid.Empty)
                    return BadRequest<List<AccountDatabasePropertiesDto>>("Параметр accountUserId не может быть незаполнен.");

                AccessProvider.HasAccess(ObjectAction.AccountDatabases_ViewAccessDatabaseList, null, () => accountUserId);
                var result = await accountDatabaseDataProvider.GetAccessDatabaseListAsync(accountUserId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения списка баз, доступных пользователю {accountUserId}.]");
                return PreconditionFailed<List<AccountDatabasePropertiesDto>>(ex.Message);
            }
        }

        /// <summary>
        ///     Получить список баз на разделителях, доступных пользователю,
        /// с возможностью указания кода конфигурации.
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <param name="configurationCode">Код конфигурации</param>
        public async Task<ManagerResult<List<DelimitersDbsPropertiesDto>>> GetDelimitersDbsByAccUserAsync(Guid accountUserId,
            string configurationCode = null)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_ViewDelimitersInfo, null, () => accountUserId);

                var result = await accountDatabaseDataProvider.GetDelimitersDbsByUserAsync(accountUserId, configurationCode);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения списка баз на разделителях с конфигурацией] {configurationCode}, доступных пользователю {accountUserId}.");
                return PreconditionFailed<List<DelimitersDbsPropertiesDto>>(ex.Message);
            }
        }

        /// <summary>
        ///     Получить статус базы по Id
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public async Task<ManagerResult<DatabaseStatusDto>> GetDatabaseStatusInfo(Guid accountDatabaseId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail);

                var result = await DbLayer.DatabasesRepository.GetByIdAsync(accountDatabaseId);

                if (result == null)
                    return NotFound<DatabaseStatusDto>($"Не найдена база по Id {accountDatabaseId}");

                var resultModel = new DatabaseStatusDto
                {
                    Status = result.State,
                    Description = result.StateEnum.Description()
                };

                return Ok(resultModel);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения статуса ИБ {accountDatabaseId} по Id.]");
                return PreconditionFailed<DatabaseStatusDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        public async Task<ManagerResult<AccountDatabasePropertiesDto>> GetAccountDatabasesByIdAsync(Guid accountDatabaseId)
        {
            try
            {

                var result = await accountDatabaseDataProvider.GetAccountDatabasesByIdAsync(accountDatabaseId);

                if (result == null)
                    return NotFound<AccountDatabasePropertiesDto>($"База по идентификатору '{accountDatabaseId}' не найдена");

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => result.AccountID);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения данных ИБ {accountDatabaseId} по номеру.]");
                return PreconditionFailed<AccountDatabasePropertiesDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        /// <param name="accountUserId">Идентификатор пользователя который запрашивает сведения о базе.</param>
        public async Task<ManagerResult<AccountDatabasePropertiesDto>> GetAccountDatabasesForUserById(Guid accountDatabaseId, Guid accountUserId)
        {
            try
            {

                var result = await accountDatabaseDataProvider.GetAccountDatabasesForUserById(accountDatabaseId, accountUserId);

                if (result == null)
                {
                    var message = $"База по идентификатору '{accountDatabaseId}' не найдена для пользователя '{accountUserId}'";
                    logger.Warn(message);
                    return NotFound<AccountDatabasePropertiesDto>(message);
                }

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => result.AccountID);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения данных ИБ {accountDatabaseId} по номеру для пользователя {accountUserId}.]");
                logger.Warn($"Не удалось получить базу '{accountDatabaseId}' для пользователя '{accountUserId}' :: '{ex.GetFullInfo()}'");
                return PreconditionFailed<AccountDatabasePropertiesDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные информационной базы по имени базы.
        /// </summary>
        /// <param name="v82Name">Имя информационной базы.</param>
        public async Task<ManagerResult<AccountDatabasePropertiesDto>> GetAccountDatabasesByNameAsync(string v82Name)
        {
            try
            {

                var result = await accountDatabaseDataProvider.GetAccountDatabasesByNameAsync(v82Name);

                if (result == null)
                    return NotFound<AccountDatabasePropertiesDto>($"База по номеру '{v82Name}' не найдена");

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => result.AccountID);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения данных ИБ {v82Name} по имени.]");
                return PreconditionFailed<AccountDatabasePropertiesDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить путь к бэкап хранилищу информационной базы.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        public async Task<ManagerResult<SimpleResultModelDto<string>>> GetDatabaseBackupStoragePathAsync(Guid accountDatabaseId)
        {
            try
            {

                AccessProvider.HasAccess(ObjectAction.AccountDatabases_EditFull);

                var result = await accountDatabaseBackupStorageDataProvider.GetDatabaseBackupStoragePathAsync(accountDatabaseId);

                if (result == null)
                    return NotFound<SimpleResultModelDto<string>>($"База по идентификатору базы '{accountDatabaseId}' не найдено бэкап хранилище");

                return Ok(result);

            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения пути к бэкап хранилищу информационной базы {accountDatabaseId}.]");
                return PreconditionFailed<SimpleResultModelDto<string>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список пользователей, которые имеют доступ к базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Список пользователей, которые имеют доступ к базе</returns>
        public ManagerResult<AccountDatabaseUserAccessListDto> GetAccountDatabaseUserAccessList(Guid accountDatabaseId)
        {
            try
            {
                var accountId = AccountIdByAccountDatabase(accountDatabaseId);

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_GetUserAccessList, () => accountId);

                var accountDatabaseUserAccessList = accountDatabaseAccessDataProvider.GetAccountDatabaseUserAccessList(accountDatabaseId);

                return Ok(accountDatabaseUserAccessList);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения списка пользователей, которые имеют доступ к базе] {accountDatabaseId}");
                return PreconditionFailed<AccountDatabaseUserAccessListDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список пользователей, которые имеют доступ к базе
        /// </summary>
        /// <param name="accountDatabaseNumber">Номер инф. базы</param>
        /// <returns>Список пользователей, которые имеют доступ к базе</returns>
        public ManagerResult<AccountDatabaseUserAccessListDto> GetAccountDatabaseUserAccessList(string accountDatabaseNumber)
        {
            try
            {
                var accountId = AccountIdByAccountDatabase(accountDatabaseNumber);

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_GetUserAccessList, () => accountId);

                var accountDatabaseUserAccessList = accountDatabaseAccessDataProvider.GetAccountDatabaseUserAccessList(accountDatabaseNumber);

                return Ok(accountDatabaseUserAccessList);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения списка пользователей, которые имеют доступ к базе] {accountDatabaseNumber}");
                return PreconditionFailed<AccountDatabaseUserAccessListDto>(ex.Message);
            }
        }


        /// <summary>
        /// Удалить кэш 
        /// </summary>
        /// <param name="deleteAcDbCacheDto">модель очистки кэша</param>
        public ManagerResult DeleteAccountDatabaseCache(DeleteAcDbCacheDto deleteAcDbCacheDto)
        {
            var accountId = AccountIdByAccountDatabase(deleteAcDbCacheDto.AccountDatabaseId);
            if (accountId == null || accountId == Guid.Empty)
                return PreconditionFailed($"по Id базы: {deleteAcDbCacheDto.AccountDatabaseId} не найден аккаунт");
            var database = GetAccountDatabase(deleteAcDbCacheDto.AccountDatabaseId);

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_GetCardGeneralData, () => accountId.Value);

                accountDatabaseDataProvider.DeleteAccountDatabaseCache(deleteAcDbCacheDto);
                recalculateMyDiskUsedSizeProvider.RunTaskToRecalculateAccountSizeAndTryProlong(accountId.Value);

                LogEvent(() => accountId, LogActions.EditInfoBase,
                    $"Удаление кэша в базе {deleteAcDbCacheDto.AccountDatabaseId} :: {database.V82Name} выполнено успешно");

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка удаления кэша инф базы]: {ex.Message}";
                logger.Warn(ex, message);
                LogEvent(() => accountId, LogActions.EditInfoBase,
                    $"Ошибка очистки кэша в базе {deleteAcDbCacheDto.AccountDatabaseId}  :: {database.V82Name} :: {ex.Message}");
                return PreconditionFailed($"{message}");
            }
        }
    }
}
