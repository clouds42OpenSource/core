﻿using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.GoogleCloud;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Tomb.Internal.Providers
{
    /// <summary>
    /// Провайдер для удаления данных в склепе
    /// </summary>
    public class TombCleanerProvider(
        IServiceProvider serviceProvider,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IGoogleCloudProvider cloudProvider)
    {
        /// <summary>
        /// Удалить данные аккаунта из склепа
        /// </summary>
        public void DeleteAccountDataFromTomb(string accountFolderId)
        {
            cloudProvider.DeleteAllInFolder(accountFolderId);
        }

        /// <summary>
        /// Удалить бэкап инф. базы в склепе
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <param name="deleteInDb">Признак что необходимо удалить запись о бэкапе в бд</param>
        public void DeleteCloudAccountDatabaseBackup(Guid accountDatabaseBackupId, bool deleteInDb = false)
        {
            var accountDatabaseBackup =
                dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup => backup.Id == accountDatabaseBackupId)
                ?? throw new NotFoundException($"Бэкап инф. базы по ID {accountDatabaseBackupId} не найден");

            if (accountDatabaseBackup.SourceType == AccountDatabaseBackupSourceType.Local)
                return;

            if (accountDatabaseBackup.AccountDatabase.StateEnum == DatabaseState.DeletedToTomb 
                && accountDatabaseBackup.CreateDateTime > DateTime.Now.AddMonths(-1))
                return;

            var backupFileId = accountDatabaseBackup.FilePath;

            if (!string.IsNullOrEmpty(backupFileId))
            {
                DeleteAccountDatabaseBackupFile(backupFileId);
            }

            if (deleteInDb)
                DeleteAccountDatabaseBackupInDb(accountDatabaseBackup);
        }

        /// <summary>
        ///     Очистка данных информационной базы после архивации в склепе
        /// </summary>
        /// <param name="accountDatabase">Текущая информационная база</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        public void ClearAccountDatabaseDataAfterArchiveToTomb(Domain.DataModels.AccountDatabase accountDatabase, Guid accountDatabaseBackupId)
        {
            try
            {
                logger.Info($"[{accountDatabase.Id}] :: Удаление временного бэкапа с хранилки: {accountDatabaseBackupId}");

                serviceProvider
                    .GetRequiredService<AccountDatabaseLocalBackupProvider>()
                    .DeleteBackupAccountDatabase(accountDatabaseBackupId);

                logger.Info($"[{accountDatabase.Id}] :: Удаление временного бэкапа завершено");
            }

            catch (Exception ex)
            {
                throw new InvalidOperationException($"Не удалось удалить временный бэкап с хранилки: {accountDatabaseBackupId}. Exception: {ex.GetFullInfo()}");
            }
        }

        /// <summary>
        /// Удалить файл бэкапа инф. базы в склепе
        /// </summary>
        /// <param name="backupFileId">ID файла бэкапа</param>
        private void DeleteAccountDatabaseBackupFile(string backupFileId)
        {
            try
            {
                var backupFile = cloudProvider.GetFileById(backupFileId);
                if (backupFile == null)
                    return;
            }
            catch (Exception ex)
            {
                logger.Warn($"[{backupFileId}] :: файл бекапа не найден, ошибка при поиске на хранилище {ex.Message}");
                return;
            }

            var result = cloudProvider.DeleteFile(backupFileId);
            if (!result)
                throw new InvalidOperationException($"Не удалось удалить файл {backupFileId} из склепа");
        }

        /// <summary>
        /// Удалить запись о бэкапе инф. базы в базе данных
        /// </summary>
        /// <param name="databaseBackup">Бэкап инф. базы</param>
        private void DeleteAccountDatabaseBackupInDb(AccountDatabaseBackup databaseBackup)
        {
            logger.Info($"[{databaseBackup.AccountDatabaseId}] :: Удаление записи о бэкапе из базы данных");

            var backupHistories = dbLayer.AccountDatabaseBackupHistoryRepository.Where(history =>
                history.AccountDatabaseBackupId == databaseBackup.Id);

            var serviceManagerBackups = dbLayer.GetGenericRepository<ServiceManagerAcDbBackup>()
                .Where(smb => smb.AccountDatabaseBackupId == databaseBackup.Id);

            dbLayer.AccountDatabaseBackupHistoryRepository.DeleteRange(backupHistories);
            dbLayer.Save();

            dbLayer.GetGenericRepository<ServiceManagerAcDbBackup>().DeleteRange(serviceManagerBackups);
            dbLayer.Save();

            dbLayer.AccountDatabaseBackupRepository.Delete(databaseBackup);
            dbLayer.Save();

            logger.Info($"[{databaseBackup.AccountDatabaseId}] :: Удаление бэкапа выполнено");
        }
    }
}
