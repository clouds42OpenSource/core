﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Tomb.Internal.Providers
{
    /// <summary>
    /// Провайдер для синхронизации данных со склепом
    /// </summary>
    public class TombActualizeProvider(
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider)
    {
        /// <summary>
        ///     Актуализация данных после удаления в склеп
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        /// <param name="fileId">ID бэкапа на GoogleDrive</param>
        /// <param name="state">Статус базы данных</param>
        public void ActualizeAccountDatabaseAfterDeletedToTomb(Guid accountDatabaseId, Guid accountDatabaseBackupId, string fileId, DatabaseState state)
        {
            var shareLink = CloudConfigurationProvider.Tomb.GetCloudLink();
            var bucketName = CloudConfigurationProvider.Tomb.GetBucketName();

            using var transaction = unitOfWork.SmartTransaction.Get();
            try
            {
                accountDatabaseChangeStateProvider.ChangeState(accountDatabaseId, state, DateTime.Now);

                var backup =
                    unitOfWork.AccountDatabaseBackupRepository.FirstOrDefault(w =>
                        w.Id == accountDatabaseBackupId);
                backup.BackupPath = string.Format(shareLink,bucketName, fileId);
                backup.SourceType = AccountDatabaseBackupSourceType.GoogleCloud;
                backup.FilePath = fileId;

                unitOfWork.AccountDatabaseBackupRepository.Update(backup);

                unitOfWork.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                logger.Info($"[{accountDatabaseId}] :: Ошибка актуализации данных: {ex.GetFullInfo()}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        ///     Актуализация данных после архивации в склепе
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        /// <param name="fileId">ID бэкапа информационной базы на GoogleDrive</param>
        public void ActualizeAccountDatabaseAfterArchiveToTomb(Guid accountDatabaseId, Guid accountDatabaseBackupId, string fileId)
        {
            using var transaction = unitOfWork.SmartTransaction.Get();
            try
            {
                var database = unitOfWork.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);
                database.LastActivityDate = DateTime.Now;

                unitOfWork.DatabasesRepository.Update(database);

                var backup = unitOfWork.AccountDatabaseBackupRepository.FirstOrDefault(w => w.Id == accountDatabaseBackupId);
                backup.BackupPath = string.Format(CloudConfigurationProvider.Tomb.GetCloudLink(), CloudConfigurationProvider.Tomb.GetBucketName(),  fileId);
                backup.FilePath = fileId;
                backup.SourceType = AccountDatabaseBackupSourceType.GoogleCloud;
                unitOfWork.AccountDatabaseBackupRepository.Update(backup);

                unitOfWork.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                logger.Info($"[{accountDatabaseId}] :: Ошибка актуализации данных: {ex.GetFullInfo()}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        ///     Обновить карточку базы (Указать в ней ссылку на директорию гугл диска)
        /// </summary>
        public void SetCloudStorageWebLinkForAccountDatabase(Guid accountDatabaseId, string filePath)
        {
            var cloudLink = CloudConfigurationProvider.Tomb.GetCloudLink();
            var bucketName = CloudConfigurationProvider.Tomb.GetBucketName();
            var database = unitOfWork.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);

            database.CloudStorageWebLink = string.Format(cloudLink, bucketName, filePath);

            unitOfWork.DatabasesRepository.Update(database);
            unitOfWork.Save();
        }
    }
}
