﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.Domain;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Tomb.Internal.Providers
{
    /// <summary>
    /// Провайдер для подготовки информационной базы к действиям в склепе
    /// </summary>
    public class TombPreparationProvider(
        AccountDatabasePublishManager accountDatabasePublishManager,
        IServiceProvider serviceProvider,
        IDropSessionsFromClusterJobWrapper dropSessionsFromClusterJobWrapper,
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        ISegmentHelper segmentHelper)
    {
        /// <summary>
        /// Проверка возможности удалить информационную базу в склеп.
        /// </summary>
        /// <param name="database">Информационная база.</param>
        /// <param name="errorMessage">Сообщения об ошибке.</param>
        /// <returns></returns>
        public bool CanDelete(Domain.DataModels.AccountDatabase database, out string errorMessage)
        {
            if (database.IsFile == true)
            {
                var path = serviceProvider.GetRequiredService<AccountDatabasePathHelper>()
                    .GetPath(database);

                if (!Directory.Exists(path))
                {
                    errorMessage = $"Отсутствует директория информационной базы по пути {path}";
                    return false;
                }

                if (!DirectoryHelper.DirectoryHas1CDatabaseFile(path))
                {
                    errorMessage =
                        $"В директории информационной базы по пути {path} не найден файл инфорамционной базы.";
                    return false;
                }
            }
            else
            {   
                var sqlServer = segmentHelper.GetSQLServer(database.Account);
              
                try
                {
                    unitOfWork.Execute1CSqlQuery(string.Format(SqlQueries.DatabaseCore.CheckDatabaseExists, database.SqlName), sqlServer);
                }

                catch (Exception)
                {
                    errorMessage = $"База {database.SqlName} не найдена на сервере.";
                    return false;
                }
            }

            errorMessage = null;
            return true;
        }

        /// <summary>
        ///     Подготовка информационной базы к удалению в склеп
        /// </summary>
        /// <param name="database">Текущая инфомрационная база</param>
        /// <param name="trigger">Триггер создания бэкапа базы</param>
        /// <param name="initiatorId">Инициатор создания бэкапа базы</param>
        public Guid PreparationAccountDatabaseBeforeDeleteToTomb(Domain.DataModels.AccountDatabase database, CreateBackupAccountDatabaseTrigger trigger, Guid initiatorId)
        {
            var backupId = serviceProvider
                .GetRequiredService<AccountDatabaseLocalBackupProvider>()
                .CreateBackupAccountDatabase(database, trigger, initiatorId);

            logger.Info($"[{database.Id}] :: Создание бэкапа. Done!");

            try
            {                
                if (database.IsFile != true)
                {
                    dropSessionsFromClusterJobWrapper.Start(new DropSessionsFromClusterJobParams
                    {
                        AccountDatabaseId = database.Id
                    }).Wait();
                }

                logger.Info($"[{database.Id}] :: Удаление активных сессий. Done!");
            }
            catch (Exception ex)
            {
                logger.Info($"[{database.Id}] :: Ошибка при удалении активных сессий в базе: {ex.GetFullInfo()}");
            }

            if (database.PublishStateEnum != PublishState.Published)
            {
                return backupId;
            }

            accountDatabasePublishManager.CancelPublishDatabaseWithWaiting(database.Id);
            logger.Info($"[{database.Id}] :: Снятие базы с публикации. Done!");

            return backupId;
        }
        
    }
}
