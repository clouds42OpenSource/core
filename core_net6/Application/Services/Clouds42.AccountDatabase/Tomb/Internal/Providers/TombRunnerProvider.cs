﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.GoogleCloud;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;

namespace Clouds42.AccountDatabase.Tomb.Internal.Providers
{
    /// <summary>
    /// Провадер для работы со склепом
    /// </summary>
    public class TombRunnerProvider(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IMoveAccountDatabaseBackupToTombProcessFlow moveAccountDatabaseBackupToTombProcessFlow,
        TombActualizeProvider tombActualizeProvider,
        IUpdateAccountConfigurationProvider updateAccountConfigurationProvider,
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        ISegmentHelper segmentHelper,
        IGoogleCloudProvider cloudProvider,
        IGoogleDriveProvider driveProvider)
    {
        /// <summary>
        ///     Перенести бэкап информационной базы в склеп
        /// </summary>
        /// <param name="accountDatabaseId">Текущая информационная база</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        /// <param name="forceUpload">Принудительная загрузка файла</param>
        /// <returns>Результат переноса</returns>
        public bool MoveBackupToTomb(Guid accountDatabaseId, Guid accountDatabaseBackupId, bool forceUpload)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);
            if (accountDatabase.StateEnum == DatabaseState.DeletedFromCloud)
                return true;

            var movingResult =
                moveAccountDatabaseBackupToTombProcessFlow.Run(new CopyAccountDatabaseBackupToTombParamsDto
                {
                    AccountDatabaseId = accountDatabase.Id,
                    AccountDatabaseBackupId = accountDatabaseBackupId,
                    ForceUpload = forceUpload
                });

            if (!movingResult.Finish)
                throw new InvalidOperationException($"Ошибка переноса бэкапа инф. базы в склеп. Причина: {movingResult.Message}");

            return true;
        }

        /// <summary>
        /// Получить ID папки аккаунта в склепе
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>ID папки аккаунта в склепе</returns>
        public string GetAccountFolderId(Account account)
        {
            return $"company_{account.IndexNumber}";
        }

        /// <summary>
        ///     Отправить архив базы в склеп.
        /// </summary>
        /// <param name="accountDatabase">Текущая информационная база</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        /// <param name="forceUpload">Принудительная загрузка файла</param>
        public string UploadBackupToTomb(Domain.DataModels.AccountDatabase accountDatabase, Guid accountDatabaseBackupId, bool forceUpload)
        {
            logger.Info($"[{accountDatabase.Id}] :: Поиск подходящей директории в склепе");
            var accountFolderPrefix = $"company_{accountDatabase.Account.IndexNumber}";
            var folderPrefix = $"{accountFolderPrefix}/{accountDatabase.V82Name}";

            logger.Info($"[{accountDatabase.Id}] :: Директория найдена: {folderPrefix}");

            logger.Info($"[{accountDatabase.Id}] :: Принудительная загрузка: {forceUpload}");

            var backup = unitOfWork.AccountDatabaseBackupRepository.FirstOrDefault(w => w.Id == accountDatabaseBackupId);
            if (backup != null)
            {
                logger.Info($"[{accountDatabase.Id}] :: Загрузка файла в склеп: {backup.BackupPath}");

                var fileId = cloudProvider.UploadFile(backup.BackupPath, folderPrefix, forceUpload,
                    accountDatabase.V82Name);
                if (string.IsNullOrEmpty(fileId))
                    throw new InvalidOperationException(
                        $"Не удалось загрузить/получить архив {backup.BackupPath} в/из склеп(а).");

                updateAccountConfigurationProvider.UpdateCloudStorageWebLink(accountDatabase.AccountId, accountFolderPrefix);
                tombActualizeProvider.SetCloudStorageWebLinkForAccountDatabase(accountDatabase.Id, fileId);

                return fileId;
            }

            throw new InvalidOperationException($"Бэкап не найден: {accountDatabaseBackupId}");
        }

        /// <summary>
        /// Загрузить бэкап файлов аккаунта в склеп
        /// </summary>
        /// <param name="backupPath">Путь до бэкапа</param>
        /// <param name="account">Аккаунт</param>
        /// <param name="forceUpload">Принудительная загрузка файла</param>
        /// <returns>Id загруженного файла на гугл диск</returns>
        public string UploadAccountFilesBackupToTomb(string backupPath, Account account, bool forceUpload)
        {
            logger.Info($"[{account.Id}] :: Принудительная загрузка: {forceUpload}");
            logger.Info($"[{account.Id}] :: Загрузка файла в склеп: {backupPath}");

            var accountFolderPrefix = $"company_{account.IndexNumber}";
            var fileId = cloudProvider.UploadFile(backupPath, accountFolderPrefix, forceUpload, string.Empty);
            if (string.IsNullOrEmpty(fileId))
                throw new InvalidOperationException($"Не удалось загрузить/получить архив {backupPath} в/из склеп(а).");

            return fileId;
        }

        /// <summary>
        ///     Скачивание информационной базы из склепа
        /// </summary>
        /// <param name="backup">Запись бэкапа информационной базы</param>
        public string DownloadAccountDatabaseFromTomb(AccountDatabaseBackup backup)
        {
          
            var cloudFile = cloudProvider.GetFileById(backup.FilePath);
            if (cloudFile == null)
            {
                throw new InvalidOperationException(
                    $"Не удалось найти файл на Google storage по пути - {backup.FilePath}");
            }

            var fileExtension = MimeTypes.GetMimeTypeExtensions(cloudFile.ContentType);

            var backupStorage = segmentHelper.GetBackupStorage(backup.AccountDatabase.Account);
            var backupDirectory = Path.Combine(backupStorage, "tomb",
                $"company_{backup.AccountDatabase.Account.IndexNumber}", "zips");

            if (!Directory.Exists(backupDirectory))
                Directory.CreateDirectory(backupDirectory);

            var backupPath = Path.Combine(backupDirectory,
                $"{backup.AccountDatabase.V82Name}_{DateTime.Now:yyyy-MM-dd-HH-mm}{fileExtension}");

            logger.Info($"[{backup.AccountDatabaseId}] :: Поиск файла в склепе");

            if (backup.BackupPath?.Contains("drive.google.com") ?? false)
            {
                logger.Info($"[{backup.AccountDatabaseId}] :: Загрузка файла {backup.BackupPath} с гугл диска");

                driveProvider.PartialDownloadFile(backup.BackupPath, backupPath);

                logger.Info($"[{backup.AccountDatabaseId}] :: Файл сохранен по пути: {backupPath}");

                return backupPath;
            }

            var result = cloudProvider.DownloadFile(backup.FilePath, backupPath, true);
            if (!result)
                throw new InvalidOperationException($"Ошибка скачивания файла {backup.FilePath} из склепа");

            logger.Info($"[{backup.AccountDatabaseId}] :: Файл сохранен по пути: {backupPath}");

            return backupPath;

        }
    }
}
