﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels;
using Clouds42.GoogleCloud;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Tomb.Internal.Providers
{
    /// <summary>
    /// Провайдер для проверки данных в склепе
    /// </summary>
    public class TombAnalysisProvider(
        IGoogleCloudProvider googleCloudProvider,
        IUnitOfWork unitOfWork,
        ILogger42 logger)
    {
        /// <summary>
        ///     Проверить загруженную информационную базу на облаке
        /// </summary>
        /// <param name="accountDatabase">Текущая информационная база</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        /// <param name="fileId">ID полученный после загрузки на облако</param>
        /// <returns>true - если файл загружен и его размер равен файлу в хранилище</returns>
        public bool ValidateUploadedAccountDatabase(Domain.DataModels.AccountDatabase accountDatabase, Guid accountDatabaseBackupId, string fileId)
        {
            logger.Info($"[{accountDatabase.Id}] :: Поиск файла {fileId} в склепе");
            var file = googleCloudProvider.GetFileById(fileId);
            if (file == null)
            {
                logger.Info($"[{accountDatabase.Id}] :: Файл {fileId} не найден на GoogleDrive");
                return false;
            }

            var backup = unitOfWork.AccountDatabaseBackupRepository.FirstOrDefault(w => w.Id == accountDatabaseBackupId);
            if (backup == null)
            {
                logger.Info($"[{accountDatabase.Id}] :: Файл {accountDatabaseBackupId} не найден на хранилке");
                return false;
            }

            if (file.Size.HasValue)
                return (ulong)backup.BackupPath.ToFileInfo().GetSize() == file.Size.Value;

            logger.Info($"[{accountDatabase.Id}] :: Неудается распознать размер файла на облаке. Файл: {fileId}");

            return false;
        }

        /// <summary>
        /// Проверить бэкап инф. базы в склепе
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>true - если файл есть и его размер больше 0</returns>
        public AuditAccountDatabaseBackupResultDto AuditAccountDatabaseBackup(Guid accountDatabaseBackupId)
        {
            var backup = unitOfWork.AccountDatabaseBackupRepository.FirstOrDefault(w => w.Id == accountDatabaseBackupId);
            if (backup == null)
                return CreateAuditResult(null,false, null, $"Файл бэкапа {accountDatabaseBackupId} не найден");

            var backupFileId = backup.FilePath;

            if (string.IsNullOrEmpty(backupFileId))
                return CreateAuditResult(backup.Id, false, backup.BackupPath, $"Не удалось получить ID файла бэкапа {accountDatabaseBackupId} из пути");

            logger.Info($"[{backup.AccountDatabase.V82Name}] :: Поиск файла {backupFileId} в склепе");
            var file = googleCloudProvider.GetFileById(backupFileId);
            
            if (file == null)
                return CreateAuditResult(backup.Id, false, backup.BackupPath, $"[{backup.AccountDatabase.V82Name}] :: Файл {backupFileId} не найден на GoogleDrive");

            return file.Size is > 0
                ? CreateAuditResult(backup.Id, true, backup.BackupPath)
                : CreateAuditResult(backup.Id, false, backup.BackupPath,
                    $"[{backup.AccountDatabase.V82Name}] :: Не удается распознать размер файла на облаке.");
        }

        /// <summary>
        /// Проверить загруженный бэкап файлов аккаунта в склепе
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="accountFilesBackupFileId">ID бэкапа в склепе</param>
        /// <param name="accountFilesBackupFilePath">Путь к бекапу файлов аккаунта</param>
        /// <returns>Успешность загрузки файла</returns>
        public bool ValidateUploadedAccountFilesBackupToTomb(Account account, string accountFilesBackupFileId, string accountFilesBackupFilePath)
        {
          
            logger.Info($"[{account.Id}] :: Поиск файла {accountFilesBackupFileId} в склепе");

            var file = googleCloudProvider.GetFileById(accountFilesBackupFileId);

            if (file == null)
            {
                logger.Info($"[{account.Id}] :: Файл {accountFilesBackupFileId} не найден на GoogleDrive");
                return false;
            }
            
            if (file.Size.HasValue)
                return (ulong)accountFilesBackupFilePath.ToFileInfo().GetSize() == file.Size.Value;

            logger.Info($"[{account.Id}] :: Неудается распознать размер файла на облаке. Файл: {accountFilesBackupFileId}");

            return false;
        }

        /// <summary>
        /// Создать результат аудита
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа</param>
        /// <param name="success">Аудит выполнен успешно</param>
        /// <param name="backupPath">Путь к файлу бэкапа</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат аудита</returns>
        private AuditAccountDatabaseBackupResultDto CreateAuditResult(Guid? accountDatabaseBackupId, bool success, string backupPath, string errorMessage = null)
        {
            if (!string.IsNullOrEmpty(errorMessage))
                logger.Info(errorMessage);

            return new AuditAccountDatabaseBackupResultDto
            {
                AccountDatabaseBackupId = accountDatabaseBackupId,
                Success = success,
                BackupPath = backupPath,
                ErrorMessage = errorMessage
            };
        }
    }
}
