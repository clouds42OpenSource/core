﻿using Clouds42.AccountDatabase.Contracts.Tomb.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Tomb
{
    /// <summary>
    ///     Склеп информационных баз
    /// </summary>
    public class TombManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        TombRunnerProvider tombRunnerProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), ITombManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Скачать архив со склепа.
        /// </summary>
        /// <param name="accountDatabaseBackup">Данные бэкапа.</param>
        public ManagerResult<string> DownloadBackup(AccountDatabaseBackup accountDatabaseBackup)
        {
            var accountDatabase = DbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == accountDatabaseBackup.AccountDatabaseId);
            if (accountDatabase == null)
            {
                logger.Info($"[{accountDatabaseBackup.AccountDatabaseId}] :: Информационная база не найдена");
                return NotFound<string>($"Информационная база {accountDatabaseBackup.AccountDatabaseId} не найдена.");
            }

            AccessProvider.HasAccess(ObjectAction.AccountDatabase_DownloadFromTomb, () => accountDatabase.AccountId);

            try
            {
                return Ok(tombRunnerProvider.DownloadAccountDatabaseFromTomb(accountDatabaseBackup));
            }
            catch (Exception ex)
            {
                var message = $"Ошибка при восстановлении информационной базы из склепа {accountDatabase.V82Name}: {ex.GetFullInfo(false)}.";
                _handlerException.Handle(ex, $"[Ошибка при восстановлении информационной базы из склепа] {accountDatabase.V82Name}");
                return PreconditionFailed<string>(message);
            }
        }

        /// <summary>
        ///     Перенести бэкап информационной базы в склеп
        /// </summary>
        /// <param name="accountDatabaseId">Текущая информационная база</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        /// <param name="forceUpload">Принудительная загрузка файла</param>
        public ManagerResult<bool> MoveBackupToTomb(Guid accountDatabaseId, Guid accountDatabaseBackupId, bool forceUpload)
        {
            var accountDatabase = DbLayer.DatabasesRepository.FirstOrDefault(acDb =>
                acDb.Id == accountDatabaseId ||
                acDb.AccountDatabaseBackups.Any(back => back.Id == accountDatabaseBackupId));

            AccessProvider.HasAccess(ObjectAction.AccountDatabaseBackup_MoveToTomp, () => accountDatabase.AccountId);

            try
            {
                var result = tombRunnerProvider.MoveBackupToTomb(accountDatabase.Id, accountDatabaseBackupId, forceUpload);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка переноса бэкапа '{accountDatabaseBackupId}' информационной базы '{accountDatabase.Id}' в склеп.]");
                return PreconditionFailed<bool>($"Ошибка при отправке в склеп информационной базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}. Описание ошибки: {ex.GetFullInfo(false)}.");
            }
        }
    }
}
