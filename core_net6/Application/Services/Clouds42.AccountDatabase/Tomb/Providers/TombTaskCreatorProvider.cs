﻿using Clouds42.AccountDatabase.Contracts.Tomb.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.AccountDatabase.Tomb.Providers
{
    /// <summary>
    /// Провайдер для создания задач по управлению инф. базой в склепе
    /// </summary>
    public class TombTaskCreatorProvider(IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : ITombTaskCreatorProvider
    {
        /// <summary>
        /// Создать новую задачу воркера на удаление в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForDeleteAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParam,
            DateTime? delayDateTime = null) =>
            CreateTaskForDeleteAccountDatabaseToTombForBackup(taskParam, delayDateTime);

        /// <summary>
        /// Создать новую задачу воркера на удаление в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public Guid CreateTaskForDeleteAccountDatabaseToTombForBackup(DeleteAccountDatabaseToTombJobParamsDto taskParam,
            DateTime? delayDateTime = null) =>
           registerTaskInQueueProvider.RegisterTask(
                taskName: "DeleteAccountDatabaseToTombJob",
                parametrizationModel: new ParametrizationModelDto(taskParam.ToJson()),
                dateTimeDelayOperation: delayDateTime,
                comment: "Удаление базы в склеп");

        /// <summary>
        /// Создать новую задачу воркера на архивацию в склепе
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForArchivateAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParam)
        {
            registerTaskInQueueProvider.RegisterTask(
                    taskName: "ArchivateAccountDatabaseToTombJob",
                    comment: "Архивация базы в склепе",
                    parametrizationModel: new ParametrizationModelDto(taskParam.ToJson())
                );
        }

        /// <summary>
        /// Создать новую задачу воркера на восстановление информационной базы из склепа
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForRestoreAccountDatabaseFromTomb(RestoreAccountDatabaseFromTombWorkerTaskParam taskParam)
        {
            registerTaskInQueueProvider.RegisterTask(
                    taskType: CoreWorkerTaskType.RestoreAccountDatabaseFromTombJob,
                    comment: "Восстановление базы из склепа",
                    parametrizationModel: new ParametrizationModelDto(taskParam.ToJson())
                );
        }

        /// <summary>
        /// Создать новую задачу воркера на отправку бэкапа информационной базы в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForSendBackupAccountDatabaseToTomb(SendBackupAccountDatabaseToTombJobParamsDto taskParam)
        {
            registerTaskInQueueProvider.RegisterTask(
                    taskName: "SendBackupAccountDatabaseToTombJob",
                    comment: "Перенос бэкапа информационной базы в склеп",
                    parametrizationModel: new ParametrizationModelDto(taskParam.ToJson()),
                    dateTimeDelayOperation: DateTime.Now.AddMinutes(5)
                );
        }

        /// <summary>
        /// Создать новую задачу воркера на архивацию файлов аккаунта в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForArchivateAccountFilesToTomb(DeleteAccountFilesToTombJobParamsDto taskParam)
        {
            registerTaskInQueueProvider.RegisterTask(
                   taskName: "ArchivateAccountFilesToTombJob",
                   comment: "Архивация файлов аккаунта в склеп",
                   parametrizationModel: new ParametrizationModelDto(taskParam.ToJson()),
                   dateTimeDelayOperation: DateTime.Now.AddMinutes(5)
               );
        }

    }
}
