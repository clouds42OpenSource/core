﻿using Clouds42.AccountDatabase.Contracts.Tomb.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Tomb.Managers
{
    /// <summary>
    /// Менеджер для создания задач по управлению инф. базой в склепе
    /// </summary>
    public class TombTaskCreatorManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ITombTaskCreatorProvider tombTaskCreatorProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Создать новую задачу воркера на удаление в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForDeleteAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParam)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_DeleteToTomb, () => AccountIdByAccountDatabase(taskParam.AccountDatabaseId));
            tombTaskCreatorProvider.CreateTaskForDeleteAccountDatabaseToTomb(taskParam, OptimalTimeForSendBackupToTombCalculator.Calculate());
        }

        /// <summary>
        /// Создать новую задачу воркера на архивацию в склепе
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForArchivateAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParam)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_DeleteToTomb, () => AccountIdByAccountDatabase(taskParam.AccountDatabaseId));
            tombTaskCreatorProvider.CreateTaskForArchivateAccountDatabaseToTomb(taskParam);
        }

        /// <summary>
        /// Создать новую задачу воркера на архивацию файлов аккаунта в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForArchivateAccountFilesToTomb(DeleteAccountFilesToTombJobParamsDto taskParam)
        {
            AccessProvider.HasAccess(ObjectAction.AccountFilesToTomb, () => taskParam.AccountId);
            tombTaskCreatorProvider.CreateTaskForArchivateAccountFilesToTomb(taskParam);
        }

        /// <summary>
        /// Создать новую задачу воркера на отправку бэкапа информационной базы в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForSendBackupAccountDatabaseToTomb(SendBackupAccountDatabaseToTombJobParamsDto taskParam)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_DeleteToTomb, () => AccountIdByAccountDatabaseBackup(taskParam.AccountDatabaseBackupId));
            tombTaskCreatorProvider.CreateTaskForSendBackupAccountDatabaseToTomb(taskParam);
        }

        /// <summary>
        /// Создать новую задачу воркера на восстановление информационной базы из склепа
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        public void CreateTaskForRestoreAccountDatabaseFromTomb(RestoreAccountDatabaseFromTombWorkerTaskParam taskParam)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabase_DeleteToTomb, () => AccountIdByAccountDatabaseBackup(taskParam.AccountDatabaseBackupId));
            tombTaskCreatorProvider.CreateTaskForRestoreAccountDatabaseFromTomb(taskParam);
        }
    }
}
