﻿namespace Clouds42.AccountDatabase.Internal.Exceptions;

public class NotEnoughSpaceException : Exception
{


    public NotEnoughSpaceException(string message) : base(message)
    {
        
    }


    public NotEnoughSpaceException(string message ,Exception inner) : base(message, inner)
    {
        
    }
}
