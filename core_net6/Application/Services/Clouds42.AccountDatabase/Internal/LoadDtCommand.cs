﻿using System.Diagnostics;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal.Exceptions;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Tools1C.Helpers;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Команда загрузки Dt файла
    /// </summary>
    public class LoadDtCommand
    {
        private readonly Launch1CHelper _launch1CHelper;
        private readonly DatabaseStatusHelper _databaseStatusHelper;
        private readonly ILogger42 _logger;
        private readonly Lazy<int> _restoreIbFromDtCommandTimeout;
        private readonly LogUpdate1CParser _logUpdate1CParser;
        private readonly IHandlerException _handlerException;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccessProvider _accessProvider;

        private readonly Dictionary<LogUpdate1CParser.LogState, Action<DatabaseInfoDto, LogUpdate1CParser.Result>>
            _mapLogParseResultToFinalizeAction;

        public LoadDtCommand(DatabaseStatusHelper databaseStatusHelper, 
            Launch1CHelper launch1CHelper, 
            LogUpdate1CParser logUpdate1CParser,
            ILogger42 logger,
            IHandlerException handlerException,
            IUnitOfWork unitOfWork,
            IAccessProvider accessProvider)
        {
            _databaseStatusHelper = databaseStatusHelper;
            _launch1CHelper = launch1CHelper;
            _restoreIbFromDtCommandTimeout = new Lazy<int>(CloudConfigurationProvider.Enterprise1C.GetRestoreIbFromDtCommandTimeout);

            _mapLogParseResultToFinalizeAction =
                new Dictionary<LogUpdate1CParser.LogState, Action<DatabaseInfoDto, LogUpdate1CParser.Result>>
                {
                    {LogUpdate1CParser.LogState.Success, (databaseInfo, result) => 
                        {
                            SetDbStatus(databaseInfo, result, DatabaseState.Ready);
                        }
                    },
                    {LogUpdate1CParser.LogState.ErrorNotEnoughSpace, (databaseInfo, result) => 
                        {
                            SetDbStatus(databaseInfo, result, DatabaseState.ErrorNotEnoughSpace);
                            throw new NotEnoughSpaceException("Статус был установлен в ErrorNotEnoughSpace.");
                        }
                    },
                    {LogUpdate1CParser.LogState.ErrorNoLisenseFound, (databaseInfo, result) => 
                        {
                            SetDbStatus(databaseInfo, result, DatabaseState.ErrorCreate);
                            throw new InvalidOperationException("Статус был установлен в ErrorCreate.");
                        }
                    }
                };
            _logUpdate1CParser = logUpdate1CParser;
            _logger = logger;
            _handlerException = handlerException;
            _unitOfWork = unitOfWork;
            _accessProvider = accessProvider;
        }

        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="databaseInfo">Модель информации об инф. базе</param>
        /// <param name="uploadedDtFilePath">Путь к загруженному файлу Dt</param>
        public void Execute(DatabaseInfoDto databaseInfo, string uploadedDtFilePath)
        {
            _logger.Trace($"Начало загрузки Dt в {databaseInfo.FilePath} из {uploadedDtFilePath}");

            try
            {
                _databaseStatusHelper.SetDbStatus(databaseInfo.V82Name, DatabaseState.NewItem);
                var fileIbPath = databaseInfo.FilePath;
                var loadDtFileResult = LoadDtFile(fileIbPath, uploadedDtFilePath);
                ProccessLoadDtResult(databaseInfo, loadDtFileResult, uploadedDtFilePath);
            }
            catch (NotEnoughSpaceException)
            {
                _databaseStatusHelper.SetDbStatus(databaseInfo.V82Name, DatabaseState.ErrorNotEnoughSpace);
                DeleteUploadedDtFile(uploadedDtFilePath);
                throw; 
            }
            catch (Exception)
            {
                _databaseStatusHelper.SetDbStatus(databaseInfo.V82Name, DatabaseState.ErrorDtFormat);
                DeleteUploadedDtFile(uploadedDtFilePath);
                throw;
            }
        }

        /// <summary>
        /// Обработать результат разбора лога после загрузки ДТ 
        /// </summary>
        /// <param name="databaseInfo">Модель информации об инф. базе</param>
        /// <param name="uploadedDtFilePath">Путь к загруженному файлу Dt</param>
        /// <param name="result">Результат парсинга лога</param>
        private void ProccessLoadDtResult(DatabaseInfoDto databaseInfo, LogUpdate1CParser.Result result, string uploadedDtFilePath)
        {
            try
            {
                if (!_mapLogParseResultToFinalizeAction.ContainsKey(result.LogState))
                    throw new InvalidOperationException(
                        $"Действие для указанного состояния {result.LogState} не реализовано!");

                _mapLogParseResultToFinalizeAction[result.LogState](databaseInfo, result);
                
                if (result.LogState == LogUpdate1CParser.LogState.Success)
                    DeleteUploadedDtFile(uploadedDtFilePath);
            }
            catch (Exception ex)
            {
                LogEventHelper.LogEvent(
                    _unitOfWork,
                    databaseInfo.AccountId,
                    _accessProvider,
                    LogActions.DatabaseLoadFailed, 
                    $"Не удалось загрузить базу {databaseInfo.V82Name}. Статус : {result.LogState}");
                _handlerException.Handle(ex, ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Установить статус базы по данным логов
        /// </summary>
        /// <param name="databaseInfo">Модель информации об инф. базе</param>
        /// <param name="result">Результат парсинга логов</param>
        /// <param name="databaseState">Статус базы, который нужно установить</param>
        private void SetDbStatus(DatabaseInfoDto databaseInfo, LogUpdate1CParser.Result result, DatabaseState databaseState)
        {
            _logger.Trace($"Результат загрузки DT - {result.LogState}. Дополнительная информация: {string.Join(" ", result.Lines)}");
            _databaseStatusHelper.SetDbStatus(databaseInfo.V82Name, databaseState);
        }

        /// <summary>
        /// Удалить загруженный файл Dt
        /// </summary>
        /// <param name="uploadedDtFilePath">Путь к загруженному файлу Dt</param>
        private void DeleteUploadedDtFile(string uploadedDtFilePath)
        {
            File.Delete(uploadedDtFilePath);
        }

        /// <summary>
        /// Загрузить DT файл
        /// </summary>
        /// <param name="accountDatabaseDestinationPath">Путь для создаваемой базы</param>
        /// <param name="uploadedDtFilePath">Путь к загруженному файлу Dt</param>
        /// <returns>Обработанный результат лог файла</returns>
        private LogUpdate1CParser.Result LoadDtFile( string accountDatabaseDestinationPath, string uploadedDtFilePath )
        {
            var launch1CParams =
                _launch1CHelper.GetLaunch1CParamsForConfigMode(accountDatabaseDestinationPath, uploadedDtFilePath);

            _logger.Info($"Строка запуска: {launch1CParams.ExeParameters}");

            var process = new Process
            {
                StartInfo =
                {
                    FileName = launch1CParams.Client1CPath,
                    Arguments = launch1CParams.ExeParameters
                }
            };

            _logger.Info($"Путь к исполняемому файлу платформы: {launch1CParams.Client1CPath}");

            process.Start();
            process.WaitForExit(_restoreIbFromDtCommandTimeout.Value);

            if (process.HasExited)
                return _logUpdate1CParser.Parse(launch1CParams.LogFilePath, process.ExitCode);

            var message = $"{uploadedDtFilePath} :: принудительная отмета процесса создания dt.";
            _logger.Trace(message);
            process.Kill();
            throw new InvalidOperationException(message);
        }
    }
}
