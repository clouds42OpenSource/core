﻿using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Хэлпер для управлени доступами к инф. базе
    /// </summary>
	public class MangeDbAccessHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить список внешних доступов к инф. базе
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="databaseOwnerId">ID владельца инф. базы</param>
        /// <returns>Список внешних доступов к инф. базе</returns>
        public List<AccessUserInfoDc> GetExternalAccessList(Guid databaseId, Guid databaseOwnerId)
        {
            var res = new List<AccessUserInfoDc>();
            var accesses = dbLayer.AcDbAccessesRepository.WhereLazy(a =>
                a.AccountDatabaseID == databaseId && a.AccountID != databaseOwnerId && a.AccountUserID != null &&
                a.AccountUserID != Guid.Empty).Include(i => i.AccountUser);
            
            foreach (var access in accesses.ToList())
            {
                var tUser = new AccessUserInfoDc { UserId = access.AccountUserID.Value };

                var accessOperation =
                    dbLayer.ManageDbOnDelimitersAccessRepository.FirstOrDefault(w => w.Id == access.ID);
                tUser.UserName = $"{access.AccountUser.LastName} {access.AccountUser.FirstName} {access.AccountUser.MiddleName}";
                tUser.UserEmail = access.AccountUser.Email;
                tUser.State = accessOperation?.AccessState ?? AccountDatabaseAccessState.Done;
                tUser.DelayReason = accessOperation?.ProcessingDelayReason;
                res.Add(tUser);
            }
            return res;
        }

        /// <summary>
        /// Получить список внутренних доступов к инф. базе
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="databaseOwnerId">ID владельца инф. базы</param>
        /// <returns>Список внутренних доступов к инф. базе</returns>
        public List<AccessUserInfoDc> GetInternalAccessList(Guid databaseId, Guid databaseOwnerId)
        {
            var res = new List<AccessUserInfoDc>();
            var accesses = dbLayer.AcDbAccessesRepository.WhereLazy(a =>
                a.AccountDatabaseID == databaseId && a.AccountID == databaseOwnerId && a.AccountUserID != null &&
                a.AccountUserID != Guid.Empty).Include(i => i.AccountUser);

            foreach (var access in accesses.ToList())
            {
                var tUser = new AccessUserInfoDc { UserId = access.AccountUserID.Value };
                var accessOperation =
                    dbLayer.ManageDbOnDelimitersAccessRepository.FirstOrDefault(w => w.Id == access.ID);
                tUser.UserName = $"{access.AccountUser.LastName} {access.AccountUser.FirstName} {access.AccountUser.MiddleName}";
                tUser.UserEmail = access.AccountUser.Email;
                tUser.State = accessOperation?.AccessState ?? AccountDatabaseAccessState.Done;
                tUser.DelayReason = accessOperation?.ProcessingDelayReason;
                res.Add(tUser);
            }
            return res;
        }
                
    }
}
