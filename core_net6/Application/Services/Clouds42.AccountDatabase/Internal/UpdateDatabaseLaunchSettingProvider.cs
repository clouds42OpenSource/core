﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Провайдер обновления настройки "Тип запуска базы" у информационной базы.
    /// </summary>
    internal class UpdateDatabaseLaunchSettingProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly PostRequestLaunchTypeDto _launchInfo;
        private readonly Lazy<AccountUser> _updateInitiator;
        private readonly ILogger42 _logger;
        internal UpdateDatabaseLaunchSettingProvider(IUnitOfWork dbLayer,
                                                     PostRequestLaunchTypeDto launchInfo,
                                                     ILogger42 logger)
        {
            _dbLayer = dbLayer;
            _launchInfo = launchInfo;
            _logger = logger;
            _updateInitiator = new Lazy<AccountUser>(() =>
            {
                var accountUser = _dbLayer.AccountUsersRepository.AsQueryableNoTracking().Include(i=>i.AccountUserRoles).FirstOrDefault(au => au.Id == _launchInfo.AccountUserId);
                if (accountUser == null)
                    throw new InvalidOperationException($"Не удалось получить AccountUser по ID {_launchInfo.AccountUserId}");

                return accountUser;
            });
        }

        /// <summary>
        /// Обновить настройку "Тип запуска базы"
        /// </summary>
        public void UpdateLaunchType()
        {
            var accessesListOfDatabase = GetAccessForUpdate();
            try
            {
                foreach (var access in accessesListOfDatabase)
                {
                    access.LaunchType = _launchInfo.LaunchType;
                    _dbLayer.AcDbAccessesRepository.Update(access);
                }

                _dbLayer.Save();
            }
            catch (Exception ex)
            {
                _logger.Error($"При обновлении настройки \"Тип запуска базы\" произошла ошибка {ex.Message}");
            }
        }

        /// <summary>
        /// Инициатор обновления Администратор аккаунта
        /// </summary>
        private bool UpdateInitiatorIsAccountAdmin
            =>  _updateInitiator.Value.AccountUserRoles.Any(a => a.AccountUserGroup == AccountUserGroup.AccountAdmin);

        /// <summary>
        /// Получить список доступов которые нужно обновить.
        /// </summary>
        private List<AcDbAccess> GetAccessForUpdate()
        {
            var databaseAccessQuery = _dbLayer.AcDbAccessesRepository.WhereLazy(x => x.AccountDatabaseID == _launchInfo.AccountDatabaseID);

            //Администратор аккаунта может сменить настройку себе и всем пользователям своего аккаунта, если у них настройка не выставлена.
            if (!UpdateInitiatorIsAccountAdmin)
            {
                return databaseAccessQuery.Where(a => a.AccountUserID == _updateInitiator.Value.Id).ToList();
            }

            {
                var accessesOfAccountUsersSameAccount = from access in databaseAccessQuery
                    join accountUser in _dbLayer.AccountUsersRepository.WhereLazy() on access.AccountUserID equals
                        accountUser.Id
                    where accountUser.AccountId == _updateInitiator.Value.AccountId
                    select access;

                return accessesOfAccountUsersSameAccount
                    .Where(a => !a.LaunchType.HasValue || a.AccountUserID == _updateInitiator.Value.Id).ToList();
            }

        }
    }
}
