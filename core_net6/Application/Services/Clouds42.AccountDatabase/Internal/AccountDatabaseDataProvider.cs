﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Провайдер данных ифнормационной базы.
    /// </summary>
    internal class AccountDatabaseDataProvider(
        IUnitOfWork dbLayer,
        IAccountDatabasePathHelper accountDatabasesPathHelper,
        IStartProcessOfTerminateSessionsInDatabaseProvider startProcessOfTerminateSessionsInDatabaseProvider)
        : IAccountDatabaseDataProvider
    {
        /// <summary>
        /// Получить информационную базу.
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        public Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid accountDatabaseId) =>
            dbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId) ??
            throw new NotFoundException(
                $"По идентификатору '{accountDatabaseId}' не удалось найти информационную базу.");

        /// <summary>
        /// Получить информационную базу.
        /// </summary>
        /// <param name="v82Name">Номер информационной базы.</param>
        public Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(string v82Name) =>
            dbLayer.DatabasesRepository.AsQueryable().FirstOrDefault(d => d.V82Name.ToLower() == v82Name.ToLower()) ??
            throw new NotFoundException(
                $"По номеру '{v82Name}' не удалось найти информационную базу.");

        /// <summary>
        /// Получить бэкап инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>Бэкап инф. базы</returns>
        public AccountDatabaseBackup GetAccountDatabaseBackupOrThrowException(Guid accountDatabaseBackupId)
            => dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup =>
                   backup.Id == accountDatabaseBackupId) ??
               throw new NotFoundException($"Бэкап инф. базы по ID {accountDatabaseBackupId} не найден");

        /// <summary>
        /// Получить инф. базу по Id бэкапа или выкинуть исключение
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>Инф. база</returns>
        public Domain.DataModels.AccountDatabase GetAccountDatabaseByBackupIdOrThrowException(Guid accountDatabaseBackupId)
            => (from backup in dbLayer.AccountDatabaseBackupRepository.WhereLazy()
                join database in dbLayer.DatabasesRepository.WhereLazy() on backup.AccountDatabaseId equals database.Id
                where backup.Id == accountDatabaseBackupId
                select database).FirstOrDefault() ?? throw new NotFoundException(
                   $"По идентификатору бэкапа '{accountDatabaseBackupId}' не удалось найти информационную базу.");

        /// <summary>
        /// Получить бэкап инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>Бэкап инф. базы</returns>
        public AccountDatabaseBackup GetAccountDatabaseBackupById(Guid accountDatabaseBackupId)
            => dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup =>
                backup.Id == accountDatabaseBackupId);

        /// <summary>
        /// Получить список бекапов инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Список бекапов инф. базы</returns>
        public IQueryable<AccountDatabaseBackup> GetAccountDatabaseBackups(Guid accountDatabaseId)
            => dbLayer.AccountDatabaseBackupRepository.WhereLazy(back => back.AccountDatabaseId == accountDatabaseId);

        /// <summary>
        /// Получить информационную базу.
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <returns>Модель инф. базы</returns>
        public Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId) =>
            dbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);

        /// <inheritdoc />
        /// <summary>
        /// Получить список баз, доступных пользователю.
        /// </summary>        
        /// <param name="accountUserId">Номер пользователя.</param>        
        public async Task<List<AccountDatabasePropertiesDto>> GetAccessDatabaseListAsync(Guid accountUserId)
        {
            var serviceAccountId = CloudConfigurationProvider.DbTemplateDelimiters
                .GetServiceAccountIdForDemoDelimiterDatabases();

            var resultList = await dbLayer.DatabasesRepository.GetAccountDatabasesForAccountUserAsync(accountUserId, serviceAccountId);

            
            var bitDepthList = await dbLayer.AccountUserBitDepthRepository
                .AsQueryableNoTracking()
                .Where(d => d.AccountUserId == accountUserId)
                .ToListAsync();
            
            var bitDepthLookup = bitDepthList.ToDictionary(d => d.AccountDatabaseId, d => d.BitDepth);
            var list = resultList.Select(accessDatabaseListQueryDataItem =>
            {
                var accountDatabaseItem = accessDatabaseListQueryDataItem.ConvertToDto();
                accountDatabaseItem.FilePath = accountDatabasesPathHelper.GetPath(accessDatabaseListQueryDataItem.Id);
                if (bitDepthLookup.TryGetValue(accountDatabaseItem.ID, out var bitDepth))
                {
                    accountDatabaseItem.Application1CBitDepth = bitDepth;
                }
                return accountDatabaseItem;
            }).ToList();
            
            return list;

        }

        /// <inheritdoc />
        /// <summary>
        ///     Получить список баз на разделителях, доступных пользователю.
        /// </summary>        
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <param name="configurationCode">Код конфигурации</param>        
        public async Task<List<DelimitersDbsPropertiesDto>> GetDelimitersDbsByUserAsync(Guid accountUserId, string configurationCode)
        {
            List<DelimitersDatabaseQueryDataDto> listOfQueryData;

            if (string.IsNullOrWhiteSpace(configurationCode))
                listOfQueryData = await dbLayer.DatabasesRepository.GetDelimitersDbsForAccUserAsync(accountUserId);
            else
            {
                listOfQueryData = await dbLayer.DatabasesRepository.GetDelimitersDbsForAccUserAsync(accountUserId, configurationCode);
            }

            return listOfQueryData.Select(CreateDelimitersDto).ToList();
        }

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>        
        /// <param name="accountDatabaseId">Номер информационной базы.</param>        
        public async Task<AccountDatabasePropertiesDto> GetAccountDatabasesByIdAsync(Guid accountDatabaseId)
        {
            var accountDatabaseData = await dbLayer.DatabasesRepository.GetAccountDatabasesById(accountDatabaseId);

            return accountDatabaseData?.ConvertToDto();
        }


        /// <summary>
        /// Получить данные информационной базы по имени базы.
        /// </summary>        
        /// <param name="v82Name">Имя информационной базы.</param>        
        public async Task<AccountDatabasePropertiesDto> GetAccountDatabasesByNameAsync(string v82Name)
        {
            var accountDatabaseData = await dbLayer.DatabasesRepository.GetAccountDatabasesByNameAsync(v82Name);

            return accountDatabaseData?.ConvertToDto();
        }

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        /// <param name="accountUserId">Идентификатор пользователя который запрашивает сведения о базе.</param>        
        public async Task<AccountDatabasePropertiesDto> GetAccountDatabasesForUserById(Guid accountDatabaseId, Guid accountUserId)
        {
            var accountDatabaseData = await dbLayer.DatabasesRepository.GetAccountDatabasesForUserById(accountDatabaseId, accountUserId);

            return accountDatabaseData?.ConvertToDto();
        }

        /// <summary>
        ///     Смапить модель в DTO
        /// </summary>
        /// <param name="data"></param>
        private static DelimitersDbsPropertiesDto CreateDelimitersDto(DelimitersDatabaseQueryDataDto data)
        {
            var resultModel = new DelimitersDbsPropertiesDto
            {
                Id = data.Id,
                Caption = data.Caption,
                DbName = data.DbName,
                WebLink = AccountDatabaseWebPublishPath.GetWebPublishPath(data),
                Zone = data.Zone,
                SourceDatabaseId = data.DbSourceId
            };
            return resultModel;
        }


        public void DeleteAccountDatabaseCache(DeleteAcDbCacheDto deleteAcDbCacheDto)
        {
            var database = GetAccountDatabase(deleteAcDbCacheDto.AccountDatabaseId);
            if (database.IsFile != null && !database.IsFile.Value)
                throw new InvalidOperationException($"База по id {deleteAcDbCacheDto.AccountDatabaseId} не явлется файловой");

            var databasePath = accountDatabasesPathHelper.GetPath(database);

            var endSessionModel = new StartProcessOfTerminateSessionsInDatabaseDto
            { DatabaseId = deleteAcDbCacheDto.AccountDatabaseId };

            startProcessOfTerminateSessionsInDatabaseProvider.TryStartProcess(endSessionModel, out var errorMessage, true);

            DirectoryInfo directoryInfo = new DirectoryInfo(databasePath);

            foreach (var file in directoryInfo.GetFiles())
            {
                if (!string.Equals(file.Name, "1Cv8.1CD", StringComparison.CurrentCultureIgnoreCase))
                    file.Delete();
            }
            foreach (var folder in directoryInfo.GetDirectories())
            {
                if (deleteAcDbCacheDto.ClearDatabaseLogs || folder.Name != "1Cv8Log")
                    folder.Delete(true);
            }
        }


    }
}
