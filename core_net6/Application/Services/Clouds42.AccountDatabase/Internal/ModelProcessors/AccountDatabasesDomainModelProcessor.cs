﻿using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Text;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Internal.ModelProcessors
{
    /// <summary>
    /// Процессор для работы с моделями информационной базы
    /// </summary>
    public class AccountDatabasesDomainModelProcessor(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        AccountDatabaseHelper accountDatabaseHelper,
        ICloud42ServiceHelper cloud42ServiceHelper,
        AccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IConfiguration configuration)
    {
        /// <summary>
        /// Получить модель инф. базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель инф. базы</returns>
        internal AccountDatabasesDomainModel GetModel(Guid accountId)
        {
            var currentUser = accessProvider.GetUser();
            var currentUserHasControlPermissions = currentUser.Groups.Contains(AccountUserGroup.CloudAdmin) ||
                                                   currentUser.Groups.Contains(AccountUserGroup.Hotline);
            var dbTemplatesList = GetDbTemplates(accountId);

            var myDiskInfos = cloud42ServiceHelper.GetMyDiskInfo(accountId);

            var serviceIsAllow = myDiskInfos.FreeSizeOnDisk > 0 || currentUserHasControlPermissions;

            var errorMessage = new StringBuilder();
            if (!serviceIsAllow)
                errorMessage.AppendLine("У вас закончилось свободное дисковое пространство.");

            var templateDelimiters = GetDbTemplatesDelimiter();

            return new AccountDatabasesDomainModel
            {
                AvailableTemplates = dbTemplatesList,
                ServiceIsAllow = serviceIsAllow,
                ErrorMessage = errorMessage.ToString(),
                DbTemplateDelimiters = templateDelimiters
            };
        }


        /// <summary>
        ///  Получить список всех доступных баз на разделителях из справочника
        /// </summary>
        private List<DbTemplateDelimiters> GetDbTemplatesDelimiter()
        {
            return dbLayer.DbTemplateDelimitersReferencesRepository.GetAll().ToList();
        }

        /// <summary>
        /// Получить шаблоны инф. баз
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Шаблоны инф. баз</returns>
        private List<DbTemplateDomainModel> GetDbTemplates(Guid accountId)
        {
            var templates = dbLayer.DbTemplateRepository.Where(tmpl => !tmpl.Name.Contains("demo"));

            var usedLocale = bool.Parse(configuration["UsedLocale"]);
            if (usedLocale)
            {
                var accLocale = dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(configuration["DefaultLocale"]));
                templates = templates.Where(tmpl => tmpl.LocaleId == accLocale.ID);
            }

            var templatesList = templates.OrderBy(t => t.Order).ToList();

            var dbTemplateDelimiters = dbLayer.DbTemplateDelimitersReferencesRepository.GetAll().ToList();
            var dbTemplatesList = new List<DbTemplateDomainModel>();

            var isVipAccount = accountConfigurationDataProvider.IsVipAccount(accountId);

            foreach (var template in templatesList)
            {
                var isDbTemplateDelimiters =
                    !isVipAccount && dbTemplateDelimiters.Any(d => d.TemplateId == template.Id);
                var versionTemplateDelimiters = !isVipAccount
                    ? dbTemplateDelimiters.FirstOrDefault(d => d.TemplateId == template.Id)?.ConfigurationReleaseVersion
                    : null;
                var tagsDemoDataInDatabaseOnDelimiters = new TagsDemoDataInDatabaseOnDelimiters();
                if (isDbTemplateDelimiters)
                    InitTagsDemoDataInDatabaseOnDelimiters(tagsDemoDataInDatabaseOnDelimiters, template.Id, accountId);

                dbTemplatesList.Add(new DbTemplateDomainModel
                {
                    Caption = template.DefaultCaption,
                    Id = template.Id,
                    DemoIsAvailable = template.DemoTemplateId.HasValue,
                    CanWebPublish = template.CanWebPublish,
                    TemplateImgUrl = template.ImageUrl,
                    DbTemplateDelimiters = isDbTemplateDelimiters,
                    TagsDemoDataInDatabaseOnDelimiters = tagsDemoDataInDatabaseOnDelimiters,
                    VersionTemplate = versionTemplateDelimiters
                });
            }

            return dbTemplatesList;
        }

        /// <summary>
        /// Инициализация модели с признаками демо данных в базе на разделителях
        /// </summary>
        /// <param name="tagsDemoDataInDatabaseOnDelimiters"></param>
        /// <param name="templateId">Id шаблона информационной базы</param>
        /// <param name="accountId">Id аккаунта</param>
        private void InitTagsDemoDataInDatabaseOnDelimiters(
            TagsDemoDataInDatabaseOnDelimiters tagsDemoDataInDatabaseOnDelimiters, Guid templateId, Guid accountId)
        {
            var dbOnDelimitersWithDemoData = accountDatabaseHelper.FindDatabaseDelimiterDemo(accountId, templateId);

            if (dbOnDelimitersWithDemoData != null)
            {
                tagsDemoDataInDatabaseOnDelimiters.HasDbOnDelimitersWithDemoData = true;
                tagsDemoDataInDatabaseOnDelimiters.WebPublishPath =
                    accountDatabaseWebPublishPathHelper.GetWebPublishPath(dbOnDelimitersWithDemoData.AccountDatabase);
            }
        }
    }
}