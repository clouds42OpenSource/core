﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.AccountDatabase.Internal.ModelProcessors
{
    /// <summary>
    /// Провайдер для получения информации о доступах к инф. базе
    /// </summary>
    public class AccountDatabaseAccessDataProvider(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        MangeDbAccessHelper mangeDbAccessHelper,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Получить список пользователей, которые имеют доступ к базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Список пользователей, которые имеют доступ к базе</returns>
        public AccountDatabaseUserAccessListDto GetAccountDatabaseUserAccessList(Guid accountDatabaseId)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseId);

                logger.Trace($"Список доступов к инф. базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} получен успешно.");

                return GetAccessList(accountDatabase.Id, accountDatabase.AccountId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, 
                    $"[Ошибка получения информации о доступах к инф. базе] {accountDatabaseId}");
                throw;
            }
        }

        /// <summary>
        /// Получить список пользователей, которые имеют доступ к базе
        /// </summary>
        /// <param name="accountDatabaseNumber">Номер инф. базы</param>
        /// <returns>Список пользователей, которые имеют доступ к базе</returns>
        public AccountDatabaseUserAccessListDto GetAccountDatabaseUserAccessList(string accountDatabaseNumber)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(accountDatabaseNumber);

                logger.Trace($"Список доступов к инф. базе {accountDatabaseNumber} получен успешно.");

                return GetAccessList(accountDatabase.Id, accountDatabase.AccountId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения информации о доступах к инф. базе] {accountDatabaseNumber}");
                throw;
            }
        }

        /// <summary>
        /// Получить список пользователей, которые имеют доступ к базе
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="databaseOwnerId">ID владельца инф. базы</param>
        /// <returns>Cписок пользователей, которые имеют доступ к базе</returns>
        private AccountDatabaseUserAccessListDto GetAccessList(Guid databaseId, Guid databaseOwnerId)
        {
            var acDbUserAccessList = new AccountDatabaseUserAccessListDto();

            var internalAccessList = mangeDbAccessHelper.GetInternalAccessList(databaseId, databaseOwnerId);
            acDbUserAccessList.AccessList.AddRange(internalAccessList.Select(w =>
                MapToAccountDatabaseUserAccess(w, AccountDatabaseAccessType.InternalAccess)));

            logger.Trace($"Кол-во внутренних доступов к базе {databaseId} {internalAccessList.Count}");

            var externalAccessList = mangeDbAccessHelper.GetExternalAccessList(databaseId, databaseOwnerId);
            acDbUserAccessList.AccessList.AddRange(externalAccessList.Select(w =>
                MapToAccountDatabaseUserAccess(w, AccountDatabaseAccessType.ExternalAccess)));

            logger.Trace($"Кол-во внешних доступов к базе {databaseId} {externalAccessList.Count}");

            return acDbUserAccessList;
        }

        /// <summary>
        /// Сконвертировать модель информации о доступе к модели доступа пользователя
        /// </summary>
        /// <param name="accessUserInfoDc">Модель информации о доступе</param>
        /// <param name="accessType">Модель доступа пользователя</param>
        /// <returns>Модель доступа пользователя</returns>
        private AccountDatabaseUserAccessDto MapToAccountDatabaseUserAccess(AccessUserInfoDc accessUserInfoDc, AccountDatabaseAccessType accessType)
        {
            var acDbUserAccess = new AccountDatabaseUserAccessDto
            {
                AccountUserId = accessUserInfoDc.UserId,
                AccessType = accessType,
                Rent1CLicense = GetUserRent1CLicenseType(accessUserInfoDc.UserId)
            };

            logger.Trace(
                $"Доступ пользователя {acDbUserAccess.AccountUserId} {acDbUserAccess.AccessType.Description()}. Тип лицензии {acDbUserAccess.Rent1CLicense}");

            return acDbUserAccess;
        }

        /// <summary>
        /// Получить тип лицензии Аренды 1С для пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Тип лицензии Аренды 1С или null если аренда не подключена</returns>
        private ResourceType? GetUserRent1CLicenseType(Guid accountUserId)
        {
            var accountUserResources = dbLayer.ResourceRepository.Where(w =>
                w.Subject == accountUserId &&
                w.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise).ToList();

            if (CheckResourceExistenceForAccountUser(accountUserResources, ResourceType.MyEntUser))
                return ResourceType.MyEntUser;

            if (CheckResourceExistenceForAccountUser(accountUserResources, ResourceType.MyEntUserWeb))
                return ResourceType.MyEntUserWeb;

            return null;
        }

        /// <summary>
        /// Проверить существование ресурса для пользователя
        /// </summary>
        /// <param name="resources">Список ресурсов</param>
        /// <param name="resourceType">Тип ресурса</param>
        /// <returns>true - если ресурс существует</returns>
        private bool CheckResourceExistenceForAccountUser(List<Resource> resources, ResourceType resourceType)
        {
            var resource =
                resources.FirstOrDefault(w =>
                    w.BillingServiceType.SystemServiceType == resourceType);

            if (resource != null)
                return true;

            return false;
        }
    }
}
