﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.AccountDatabase.Internal.ModelProcessors
{
    /// <summary>
    /// Процессор для работы с сервисом инф. базам 
    /// </summary>
    public class ServiceAccountDatabasesDomainModelProcessor(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IResourceDataProvider resourceDataProvider)
    {
        /// <summary>
        /// Получить виды разрешений на аренду 1С 
        /// </summary>
        /// <param name="accountId">индификатор аккаунта</param>
        /// <param name="requestedUserId">индификатор пользователя который запрашивает информацию</param>
        /// <returns>Разрешения на аренду 1С</returns>
        public Rent1CPermissionDto GetRent1CPermissionFor(Guid accountId, Guid requestedUserId)
        {
            var requestedGroups = accessProvider.GetGroups(requestedUserId);

            var resourcesLazy = resourceDataProvider.GetResourcesLazy(Clouds42Service.MyEnterprise,
                r => r.Subject == requestedUserId && r.AccountId == accountId,
                ResourceType.MyEntUserWeb,
                ResourceType.MyEntUser);

            var rent1CResData =
                (from res in resourcesLazy.Where(r => r.AccountSponsorId == null)
                    join resConf in dbLayer.ResourceConfigurationRepository.WhereLazy(r => r.AccountId == accountId)
                        on res.BillingServiceType.ServiceId equals resConf.BillingServiceId
                    select new {res, resConf}).Concat(
                    from res in resourcesLazy.Where(r => r.AccountSponsorId != null)
                    join resConf in dbLayer.ResourceConfigurationRepository.WhereLazy()
                        on new {acs = (Guid) res.AccountSponsorId, service = res.BillingServiceType.ServiceId} equals
                        new {acs = resConf.AccountId, service = resConf.BillingServiceId}
                    select new {res, resConf}
                );

            var rent1CResDataList = rent1CResData.ToList().Select(s => new
            {
                ServiceIsActive = !s.resConf.FrozenValue,
                HasWebAccess = s.res.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb ||
                               s.res.BillingServiceType.SystemServiceType == ResourceType.MyEntUser,
                HasRdpAccess = s.res.BillingServiceType.SystemServiceType == ResourceType.MyEntUser
            }).ToList();

            var requestedUserHasEfsolPermission = HasEfsolPermissionFor(requestedGroups);
            var requestedUserHasSaleManagerPermission = HasSaleManagerPermissionFor(requestedGroups);

            var hasPermissionForRdp = rent1CResDataList.Any(r => r.HasRdpAccess && r.ServiceIsActive) ||
                                      requestedUserHasEfsolPermission;
            var hasPermissionForWeb = rent1CResDataList.Any(r => r.HasWebAccess && r.ServiceIsActive) ||
                                      requestedUserHasEfsolPermission || requestedUserHasSaleManagerPermission;

            return new Rent1CPermissionDto
            {
                HasPermissionForRdp = hasPermissionForRdp,
                HasPermissionForWeb = hasPermissionForWeb
            };
        }

        /// <summary>
        /// Получить есть ли разрешение efsol для переданных групп
        /// </summary>
        /// <param name="requestedGroups">Группы по которым смотреть разрешение Efsol</param>
        /// <returns>Признак того есть или нет разрешение Efsol для переданных групп</returns>
        private static bool HasEfsolPermissionFor(List<AccountUserGroup> requestedGroups)
            => requestedGroups.Any(r => r == AccountUserGroup.CloudAdmin || r == AccountUserGroup.Hotline);

        /// <summary>
        /// Получить есть ли разрешение SaleManager для переданных групп
        /// </summary>
        /// <param name="requestedGroups">Группы по которым смотреть разрешение SaleManager</param>
        /// <returns>Признак того есть или нет разрешение SaleManager для переданных групп</returns>
        private static bool HasSaleManagerPermissionFor(List<AccountUserGroup> requestedGroups)
            => requestedGroups.Any(r => r == AccountUserGroup.AccountSaleManager);
    }
}