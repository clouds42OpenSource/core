﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.AccountDatabase.Internal.ModelProcessors
{
    /// <summary>
    /// Процессор управления опциями инф. базы
    /// </summary>
    public class DbControlOptionsDomainModelProcessor(
        IUnitOfWork dbLayer,
        IResourcesService resourcesService,
        ICloud42ServiceHelper cloud42ServiceHelper,
        AccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper)
    {
        /// <summary>
        /// Получить набор опций информационной базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountDatabaseId">Id инф. базы</param>
        /// <param name="accountUsersId">Id пользователя</param>
        /// <returns>Набор опций информационной базы</returns>
        public DbControlOptionsDomainModel GetModel(Guid accountId, Guid accountDatabaseId)
        {
            var account = dbLayer.AccountsRepository.GetById(accountId);            
            var resConfig = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);
            var isServiceEnabled = resConfig is { FrozenValue: false };

            var enableUsers = cloud42ServiceHelper.GetEnabledAccountUserLogins(account, Clouds42Service.MyEnterprise, ResourceType.MyEntUser);
            var enabledUserCount = enableUsers?.Count ?? 0;

            var db = dbLayer.DatabasesRepository.GetById(accountDatabaseId);
            var options = new DbControlOptionsDomainModel
            {
                IsEnable = isServiceEnabled && enabledUserCount != 0,
                Id = db.Id,
                PublishState = db.PublishStateEnum,
                WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(db),
                Detached = db.State.GetDatabaseState() == DatabaseState.Detached,
                State = db.StateEnum,
                TemplatePlatform = db.DbTemplate?.PlatformType ?? PlatformType.Undefined,
                PlatformType = db.PlatformType,
                DbNumber = db.DbNumber
            };
            return options;
        }
      
    }
}
