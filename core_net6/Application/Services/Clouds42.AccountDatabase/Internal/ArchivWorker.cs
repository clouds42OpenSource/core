//using System.Security.AccessControl;
//using NLog;
//using Repositories;

//namespace Clouds42.AccountDatabase.Internal {
//    // Author: A. Mudritskiy
//    internal class ArchivWorker
//    {
//        readonly IUnitOfWork _dbLoyer;
//        private readonly NLog.Logger _log = LogManager.GetLogger("AppCommon.DatabaseOperations.ArchivWorker");
//        public ArchivWorker( IUnitOfWork dbLoyer ) {
//            _dbLoyer = dbLoyer;
//        }

//        private void ClearDirectory(string path)
//        {
//            if (Directory.Exists(path))
//            {
//                try
//                {
//                    var filePaths = Directory.GetFiles(path);
//                    foreach (var filePath in filePaths)
//                    {
//                        File.Delete(filePath);
//                    }

//                    filePaths = Directory.GetDirectories(path);
//                    foreach (var filePath in filePaths)
//                    {
//                        Directory.Delete(filePath, true);
//                    }

//                }
//                catch (Exception e)
//                {
//                    _log.Error("�� ������� ������� ������� {0} �� ������� {1}", path, e.Message);
//                }
//            }
//            else
//            {
//                _log.Error("������� �� ������ {0}", path);
//            }
//        }

//        private void CreateDirectory(string newPath)
//        {
//            if (!Directory.Exists(newPath))
//            {
//                Directory.CreateDirectory(newPath);
//            }
//        }

//        public bool MoveCompanyInfobase(Guid companiId, Guid cloudFileStorageId)
//        {
//            //var NewPath = ConfigurationManager.AppSettings["Path"];
//            var fileStoragePath = string.Empty;

//            var cloudFileStorageServers = _dbLoyer.CloudFileStorageServerRepository.FirstOrDefault(z => z.ID == cloudFileStorageId);

//            if (cloudFileStorageServers != null)
//                fileStoragePath = cloudFileStorageServers.FileStoragePath;

//            if (string.IsNullOrEmpty(fileStoragePath))
//                throw new InvalidOperationException(string.Format("�� ������� ����� Storage ��: {0}", cloudFileStorageId.ToString()));


//            var companies = _dbLoyer.AccountsRepository.FirstOrDefault(x => x.Id == companiId);   //_UOW.CompaniesRepository.FirstOrDefault(x => x.IndexNumber == IdexNumber);
//            if (companies == null)
//                throw new InvalidOperationException(string.Format("�� ������� ����� �������� � �� {0}", companiId.ToString()));

//            // new path => '\\cloud-prod-fs-1\base1c\company_331'
//            var newPath = Path.Combine(fileStoragePath, string.Format("company_{0}", companies.IndexNumber));
//            var group = string.Format("company_{0}", companies.IndexNumber);
//            _log.Trace("Try to change access rights for folder: {0}, group: {1}", newPath, group);
//            // ���� ����� �� �����
//            SetDirectoryAcl(group, newPath);

//            // new path => '\\cloud-prod-fs-1\base1c\company_331\fileinfobases'
//            newPath = Path.Combine(newPath, "fileinfobases");

//            _log.Trace("Try to create dir: {0}", newPath);

//            CreateDirectory(newPath);

//            _log.Trace("Dir created. Start searching for IB");

//            // get all users infobases
//            var infobaseList = _dbLoyer.DatabasesRepository
//                .Where(z => z.AccountId == companies.Id && !string.IsNullOrEmpty(z.FilePath))
//                .Where(u => !u.IsPublished.HasValue || !u.IsPublished.Value)
//                .Where(x => Directory.Exists(x.FilePath))
//                .ToList();

//            _log.Trace("Total find {0} infobases", infobaseList.Count);

//            if (!infobaseList.Any())
//                return true;

//            // '\\cloud-prod-fs-1\base1c\company_331\fileinfobases\1c_my_4_12'
//            var oldRootPath = Directory.GetParent(infobaseList.First().FilePath);

//            // '\\cloud-prod-fs-1\base1c\company_331\fileinfobases'
//            _log.Trace("Old infobases path: " + oldRootPath.FullName);

//            foreach (var infobase in infobaseList)
//            {
//                // ���� ��� ���� � ����� ���������.
//                if (infobase.FilePath.Contains(fileStoragePath.Trim()))
//                    continue;

//                // ����� �� ��������.
//                if (!AccessGuaranted(infobase.FilePath))
//                    continue;


//                var infobaseNewPath = Path.Combine(newPath, infobase.V82Name);
//                Moveinfobase(infobase.V82Name, infobaseNewPath);
//            }

//            _log.Trace("Start moving other files");


//            MoveOtherFiles(oldRootPath.FullName, newPath);

//            return true;
//        }

//        private bool AccessGuaranted(string path)
//        {
//            var newName = path + "__";

//            try
//            {
//                Directory.Move(path, newName);
//                Directory.Move(newName, path);
//                return true;
//            }
//            catch (Exception ex)
//            {
//                _log.Warn("Access to folder is denied: {0}", ex.Message);
//                return false;
//            }
//        }

//        private void SetDirectoryAcl(string groupName, string strPath)
//        {
//            var tmpDirInfo = new DirectoryInfo(strPath);

//            if (!tmpDirInfo.Exists) Directory.CreateDirectory(strPath);

//            var dirInfo = new DirectoryInfo(strPath);
//            dirInfo.Refresh();

//            var ds = dirInfo.GetAccessControl();
//            var rule = new FileSystemAccessRule(
//                @"AD\" + groupName,
//                FileSystemRights.Read | FileSystemRights.Write | FileSystemRights.CreateDirectories |
//                FileSystemRights.CreateFiles | FileSystemRights.Delete | FileSystemRights.ExecuteFile |
//                FileSystemRights.Traverse,
//                InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
//                PropagationFlags.None, AccessControlType.Allow
//                );

//            ds.AddAccessRule(rule);
//            dirInfo.SetAccessControl(ds);
//            _log.Info("����������� ����� �� ����� {0} ��� ������ {1}", strPath, groupName);
//        }

//        private void Moveinfobase(string name, string newPath)
//        {
//            var dBase = _dbLoyer.DatabasesRepository.FirstOrDefault(x => x.V82Name == name);

//            if (dBase == null)
//                throw new InvalidOperationException(string.Format("�� ������� ���� � ������ {0}", name));

//            if (dBase.FilePath.Equals(newPath))
//            {
//                _log.Warn("���� {0} ��� ���� ����������", dBase.FilePath);
//                return;
//            }

//            if (!Directory.Exists(dBase.FilePath))
//            {
//                _log.Warn("�� ������� ����� ������� ���� {0}", dBase.FilePath);
//                return;
//            }

//            if (!Directory.Exists(newPath))
//                Directory.CreateDirectory(newPath);

//            DirectoryCopy(dBase.FilePath, newPath);

//            ClearDirectory(dBase.FilePath);
//            Directory.Delete(dBase.FilePath);

//            dBase.FilePath = newPath;
//            _dbLoyer.Save();

//            _log.Info("!! ��������� ���� {0}, ����� ����� {1}", dBase.V82Name, newPath);
//        }

//        private void MoveOtherFiles(string rootPath, string newDirName)
//        {
//            _log.Info("RootPath: " + rootPath);
//            _log.Info("Destination: " + newDirName);
//            DirectoryInfo dir = new DirectoryInfo(rootPath);
//            FileInfo[] files = dir.GetFiles();
//            foreach (FileInfo file in files)
//            {
//                _log.Trace("Move file: " + file.Name);
//                string temppath = Path.Combine(newDirName, file.Name);
//                file.CopyTo(temppath, false);
//                File.Delete(file.FullName);
//            }

//            DirectoryInfo[] dirs = dir.GetDirectories();

//            foreach (DirectoryInfo subdir in dirs)
//            {
//                if (!subdir.Name.Contains("1c_my_"))
//                {
//                    string temppath = Path.Combine(newDirName, subdir.Name);
//                    DirectoryCopy(subdir.FullName, temppath);
//                    ClearDirectory(subdir.FullName);
//                    Directory.Delete(subdir.FullName);
//                }
//            }
//        }

//        private void DirectoryCopy(string sourceDirName, string newDirName)
//        {
//            _log.Trace("Copy directory: {0} to {1}", sourceDirName, newDirName);
//            var dir = new DirectoryInfo(sourceDirName);
//            var dirs = dir.GetDirectories();


//            FileInfo[] files = dir.GetFiles();
//            foreach (FileInfo file in files)
//            {
//                string temppath = Path.Combine(newDirName, file.Name);
//                _log.Trace("Copy file to {0}", temppath);
//                CreateDirectory(newDirName);
//                file.CopyTo(temppath, false);
//            }

//            foreach (DirectoryInfo subdir in dirs)
//            {
//                string temppath = Path.Combine(newDirName, subdir.Name);

//                if (!Directory.Exists(temppath))
//                {
//                    Directory.CreateDirectory(temppath);
//                }

//                DirectoryCopy(subdir.FullName, temppath);
//            }
//        }
//    }
//}