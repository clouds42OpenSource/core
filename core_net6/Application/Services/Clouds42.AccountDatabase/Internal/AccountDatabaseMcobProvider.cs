﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Internal
{
    internal class AccountDatabaseMcobProvider(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IAccessToDbOnDelimiterCommand accessToDbOnDelimiterCommand,
        IAcDbDelimitersManager acDbDelimitersManager,
        IAddWebAccessToDatabaseJobWrapper addWebAccessToDatabaseJobWrapper,
        IRemoveWebAccessToDatabaseJobWrapper removeWebAccessToDatabaseJobWrapper,
        AccountDatabasePathHelper accountDatabasesPathHelper,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        ILogger42 logger)
        : IAccountDatabaseMcobProvider
    {
        /// <summary>
        /// Добавить доступ пользователю к базе с роллями.
        /// </summary>
        /// <param name="dbName">Имя базы.</param>
        /// <param name="userLogin">Логин пользователя.</param>
        /// <param name="userName">Имя пользователя в базе 1С.</param>
        /// <param name="roles">Список профилей, через ;</param>
        /// <param name="jhoRoles">Список ролей ЖХО.</param>
        public OperationResultDto McobAddUser(string dbName, string userLogin, string userName, string roles, string jhoRoles)
        {
            var corpParameters = $"{McobUserAction.AddUser};{userLogin};{userName};{roles};";

            logger.Info($"Добавление пользователя {userLogin} к базе {roles}.");

            var dataBase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.V82Name == dbName);
            if (dataBase == null)
            {
                var errorMessage = $"Не найдена база по имени : '{dbName}'";
                logger.Warn(errorMessage);
                return new OperationResultDto(false, errorMessage);
            }

            var user = dbLayer.AccountUsersRepository.GetAccountUserByLogin(userLogin);
            if (user == null)
            {
                var errorMessage = $"Не найден пользоватль с логином {userLogin}";
                logger.Warn(errorMessage);
                return new OperationResultDto(false, errorMessage);
            }

            logger.Info($"Cписок параметров: название - {dbName}; файловый - {dataBase.IsFile};" +
                        $"корп-параметры - {corpParameters}");

            if (dataBase.AccountDatabaseOnDelimiter != null)
            {
                AddAccessRoleToDbOnDelimiters(dataBase, user, jhoRoles, roles);
            }
            else
            {
                registerTaskInQueueProvider.RegisterTask(CoreWorkerTaskType.McobEditUserRolesOfDatabase,
                    new ParametrizationModelDto(new RunJobOfMsobModelDto { DbName = dataBase.V82Name, Parametrs = corpParameters }));
                if (dataBase.PublishStateEnum == PublishState.Published)
                    addWebAccessToDatabaseJobWrapper.Start(new WebAccessToDatabaseJobParams
                    {
                        AccountDatabaseId = dataBase.Id,
                        AccountUserId = user.Id
                    });

                AccessControl(dataBase, user, jhoRoles);
                SetAccessForDatabaseFolder(dataBase, user);
            }

            return new OperationResultDto(true);
        }

        /// <summary>
        /// Забрать доступ пользоватля к базе.
        /// </summary>
        /// <param name="dbName">Имя базы.</param>
        /// <param name="userLogin">Логин пользователя.</param>
        /// <param name="roles">Список профилей, через ;</param>
        public OperationResultDto McobRemoveUserRoles(string dbName, string userLogin, string roles)
        {
            var dataBase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.V82Name == dbName);

            if (dataBase == null)
            {
                var errorMessage = $"Не найдена база по имени : '{dbName}'";
                logger.Warn(errorMessage);
                return new OperationResultDto(false, errorMessage);
            }

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == userLogin);

            if (accountUser == null)
                return new OperationResultDto(false, $"Не найден пользователь {userLogin}");

            if (dataBase.AccountDatabaseOnDelimiter != null)
            {
                if (dbLayer.AcDbAccessesRepository.FirstOrDefault(x =>
                        x.AccountDatabaseID == dataBase.Id && x.AccountUserID == accountUser.Id) == null)
                {
                    var errorMessage = $"Для пользователя '{accountUser.Login}' доступ к базе '{dataBase.Id}' не найден.";
                    logger.Warn(errorMessage);
                    return new OperationResultDto(false, errorMessage);
                }

                var result = acDbDelimitersManager.DeleteAccessFromDelimiterDatabase(dataBase, accountUser.Id);
                if (result.Error)
                    throw new InvalidOperationException(result.Message);
            }
            else
            {
                var corpParameters = $"{McobUserAction.RemoveUserRoles};{userLogin};{"empty"};{roles}";

                logger.Info("Удаление роли(-ей) {0} пользователя {1} в базе {2}", roles, userLogin, dbName);

                registerTaskInQueueProvider.RegisterTask(CoreWorkerTaskType.McobEditUserRolesOfDatabase,
                  new ParametrizationModelDto(new RunJobOfMsobModelDto { DbName = dataBase.V82Name, Parametrs = corpParameters }));

                if (dataBase.PublishStateEnum == PublishState.Published)
                    removeWebAccessToDatabaseJobWrapper.Start(new WebAccessToDatabaseJobParams
                    {
                        AccountDatabaseId = dataBase.Id,
                        AccountUserId = accountUser.Id
                    });

                RemoveAccessForDatabaseFolder(dataBase, accountUser);
            }

            return new OperationResultDto(true);
        }

        /// <summary>
        /// Предоставление прав пользователю МЦОБ если база на разделителях
        /// </summary>
        /// <param name="database">Иб аккаунта </param>
        /// <param name="user">пользователь</param>
        /// <param name="jhoRoles">роли жхо</param>
        /// <param name="roles">роли пользователя</param>
        /// <returns></returns>
        private void AddAccessRoleToDbOnDelimiters(Domain.DataModels.AccountDatabase database, AccountUser user, string jhoRoles, string roles)
        {
            if (database.AccountDatabaseOnDelimiter.Zone == null)
            {
                var errorMessage = $"Номер зоны для базы '{database.V82Name}' не найден.";
                logger.Warn(errorMessage);
                throw new NotFoundException(errorMessage);
            }

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                logger.Info($"Начало добавления ролей JHO для пользователя {user.Id}");
                AccessControl(database, user, jhoRoles);
                logger.Info($"Закончено добавление ролей JHO для пользователя {user.Id}");

                logger.Info($"Начало отправки запроса на добавление прав пользователю {user.Id}");
                var url = CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForAccessToDb();
                var result = accessToDbOnDelimiterCommand.Execute(database, user.Id, url, roles);

                if (result.AccessState == AccountDatabaseAccessState.Error)
                    throw new InvalidOperationException(result.ErrorMessage);

                logger.Info($"Запроса на добавление прав пользователю {user.Id} выполнен успешно");

                transaction.Commit();
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка предоставления доступа к базе на разделителях пользователю МЦОБ] {user.Id}  база: {database.Id}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Добавление ролей жхо в таблицу AcDbAccRolesJHO
        /// </summary>
        /// <param name="jhoRoles">роли жхо</param>
        /// <param name="accessId">индификатор доступа пользователя</param>
        private void AddJhoRoles(string jhoRoles, Guid accessId)
        {
            if (string.IsNullOrEmpty(jhoRoles))
                return;
            string[] arrayStrings = jhoRoles.Split(new char[';']); // созданим роли
            foreach (var itemRoles in arrayStrings)
            {
                dbLayer.AcDbAccRolesJhoRepository.Insert(new AcDbAccRolesJho
                {
                    Id = Guid.NewGuid(),
                    AcDbAccessesID = accessId,
                    RoleJHO = itemRoles
                });
            }
            dbLayer.Save();
        }

        /// <summary>
        /// Управление доступами
        /// </summary>
        /// <param name="dataBase">ИБ аккаунта</param>
        /// <param name="user">пользователь</param>
        /// <param name="jhoRoles">роли жхо</param>
        /// <returns></returns>
        private void AccessControl(Domain.DataModels.AccountDatabase dataBase, AccountUser user, string jhoRoles)
        {
            var access = dbLayer.AcDbAccessesRepository.FirstOrDefault(x => x.AccountUserID == user.Id
                                                                             && x.AccountDatabaseID == dataBase.Id);
            // созданим доступ к БД
            if (access == null)
            {
                access = new AcDbAccess
                {
                    AccountDatabaseID = dataBase.Id,
                    AccountID = user.AccountId,
                    AccountUserID = user.Id,
                    ID = Guid.NewGuid()
                };
                dbLayer.AcDbAccessesRepository.Insert(access);
                dbLayer.Save();
            }
            else // удалим старые роли
            {
                var list = dbLayer.AcDbAccRolesJhoRepository.Where(acDbAccRolesJho => acDbAccRolesJho.AcDbAccessesID == access.ID);
                list.ToList().ForEach(z => dbLayer.AcDbAccRolesJhoRepository.Delete(z));
                dbLayer.Save();
            }

            AddJhoRoles(jhoRoles, access.ID);
        }

        /// <summary>
        /// Выдать права на папку
        /// </summary>
        /// <param name="dataBase">ИБ аккаунта</param>
        /// <param name="user">пользователь</param>
        /// <returns></returns>
        private void SetAccessForDatabaseFolder(Domain.DataModels.AccountDatabase dataBase, AccountUser user)
        {
            if (dataBase.IsFile == true)
            {
                var dbFolder = accountDatabasesPathHelper.GetPath(dataBase);
                logger.Info($"Начало выдачи прав на папку {dbFolder} для пользователя {user.Login}");

                if (string.IsNullOrWhiteSpace(dbFolder) || !Directory.Exists(dbFolder))
                {
                    logger.Warn($"Папка {dbFolder} пуста");
                    throw new InvalidDataException("Path to db is invalid. Path: " + dbFolder);
                }

                activeDirectoryTaskProcessor.RunTask(provider => provider.SetDirectoryAclForUser(user.Login, dbFolder));

            }
        }

        /// <summary>
        /// Забрать права на папку
        /// </summary>
        /// <param name="dataBase">ИБ аккаунта</param>
        /// <param name="user">пользователь</param>
        /// <returns></returns>
        private void RemoveAccessForDatabaseFolder(Domain.DataModels.AccountDatabase dataBase, AccountUser user)
        {
            if (dataBase.IsFile == true)
            {
                var dbFolder = accountDatabasesPathHelper.GetPath(dataBase);
                logger.Info($"Начало удаления прав на папку {dbFolder} для пользователя {user.Login}");

                if (string.IsNullOrWhiteSpace(dbFolder) || !Directory.Exists(dbFolder))
                {
                    logger.Warn($"Папка {dbFolder} пуста");
                    throw new InvalidDataException("Path to db is invalid. Path: " + dbFolder);
                }

                activeDirectoryTaskProcessor.RunTask(provider => provider.RemoveDirectoryAclForUser(user.Login, dbFolder));
            }
        }


    }
}
