﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Провайдер данных бэкап хранилища информационной базы
    /// </summary>
    internal class AccountDatabaseBackupStorageDataProvider(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IAccountDatabaseBackupStorageDataProvider
    {
        /// <summary>
        /// Получить путь к бэкап хранилищу информационной базы.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        public async Task<SimpleResultModelDto<string>> GetDatabaseBackupStoragePathAsync(Guid accountDatabaseId)
        {
            var database = await dbLayer.DatabasesRepository.AsQueryableNoTracking().Include(i => i.Account)
                .FirstOrDefaultAsync(d => d.Id == accountDatabaseId);

            if (database == null)
                return null;

            var backupStorage =
                await accountConfigurationDataProvider.GetBackupStorageByAccountSegment(database.AccountId);

            var path = Path.Combine(backupStorage.ConnectionAddress,
                database.Account.GetAccountName());

            return new SimpleResultModelDto<string>
            {
                Value = path
            };
        }
    }
}
