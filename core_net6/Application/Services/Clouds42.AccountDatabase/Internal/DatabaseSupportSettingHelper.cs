﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Хелпер для работы с настройками тех. поддержки инф. баз
    /// </summary>
    public class DatabaseSupportSettingHelper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        ICloudLocalizer cloudLocalizer,
        AcDbSupportHelper acDbSupportHelper,
        IAccessProvider accessProvider,
        IAccountDatabaseSupportProvider accountDatabaseSupportProvider,
        IHandlerException handlerException,
        ILogger42 logger)
    {
        public AccountDatabaseSupportSettingDto GetDatabaseSupportSettings(Guid databaseId)
        {
            var support = dbLayer.AcDbSupportRepository.FirstOrDefault(x => x.AccountDatabasesID == databaseId);
            SupportState state;
            string login = null;
            string password = null;
            DateTime? lastHistoryDate = null;
            List<AcDbSupportHistory> histories = null;
            if (support != null)
            {
                state = (SupportState) support.State;
                login = support.Login;
                password = support.Password;

                lastHistoryDate = support.TehSupportDate;

                histories = dbLayer.AcDbSupportHistoryRepository
                    .Where(x => x.AccountDatabaseId == support.AccountDatabasesID)
                    .GroupBy(x => x.Operation)
                    .Select(g => g.OrderByDescending(x => x.EditDate).FirstOrDefault())
                    .ToList();
            }
            else
            {
                state = SupportState.NotAutorized;
            }

            return new AccountDatabaseSupportSettingDto
            {
                DatabaseId = databaseId,
                IsConnects = state == SupportState.AutorizationSuccess || state == SupportState.AutorizationProcessing,
                IsError = state == SupportState.NotValidAuthData || state == SupportState.AutorizationFail,
                LastHistoryDate = lastHistoryDate,
                Login = login,
                Password = password,
                SupportState = state,
                SupportStateDescription = state == SupportState.AutorizationFail
                    ? acDbSupportHelper.GetInvalidAuthorizationMessage(databaseId)
                    : GetSupportStateDescription(state),
                SupportLogs = histories?.OrderByDescending(w => w.EditDate).Select(x => new SupportDatabasLogDc
                {
                    Date = x.EditDate.ToString("dd.MM.yyyy HH:mm"),
                    Operation = EnumExtensions.GetEnumDescription(x.Operation),
                    Description = x.Description.LineEndingToHtmlStyle()
                }).ToList()
            };
        }

        public void SaveSupportDataAndAuthorize(SupportDataDto supportDataDto)
        {
            var accountDatabase = dbLayer.DatabasesRepository.WhereLazy().Include(i => i.Account).FirstOrDefault(d => d.Id == supportDataDto.DatabaseId)
                ?? throw new InvalidOperationException($"Не удалось получить запись о информационной базе по номеру {supportDataDto.DatabaseId}");
            logger.Info($"Начинаю редактировать данные поддержки базы {accountDatabase.Id}");

            var acDbSupport = dbLayer.AcDbSupportRepository.FirstOrDefault(x => x.AccountDatabasesID == supportDataDto.DatabaseId);

            if (acDbSupport == null)
            {
                acDbSupport = new AcDbSupport { AccountDatabasesID = supportDataDto.DatabaseId };
                dbLayer.AcDbSupportRepository.Insert(acDbSupport);
            }

            if (acDbSupport.State == (int)SupportState.AutorizationSuccess)
            {
                acDbSupport.TimeOfUpdate = supportDataDto.TimeOfUpdate;
                acDbSupport.CompleteSessioin = supportDataDto.CompleteSessioin;

                LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.EditInfoBase,
                    $"Время обновления инф базы {acDbSupport.AccountDatabase.V82Name} изменено на {(int)supportDataDto.TimeOfUpdate}");
                LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.EditInfoBase,
                    $"Флаг завершения сеансов базы {acDbSupport.AccountDatabase.V82Name} изменен на {supportDataDto.CompleteSessioin}");
            }
            
            if (supportDataDto.HasSupport != null && acDbSupport.HasSupport != supportDataDto.HasSupport)
                ChangeHasTIIFlag(acDbSupport, supportDataDto.HasSupport.Value);

            if (supportDataDto.HasAutoUpdate != null && supportDataDto.HasAutoUpdate.Value && acDbSupport.DatabaseHasModifications)
            {
                throw new InvalidOperationException($"База имеет доработки и ее нельзя подключить на АО");
            }

            if (supportDataDto.HasAutoUpdate != null && acDbSupport.HasAutoUpdate != supportDataDto.HasAutoUpdate)
                ChangeHasAutoUpdateFlag(acDbSupport, supportDataDto.HasAutoUpdate.Value);

            if (acDbSupport.State != (int)SupportState.AutorizationSuccess && supportDataDto.Login is not null && supportDataDto.Password is not null)
            {
                acDbSupport.Login = supportDataDto.Login;
                acDbSupport.Password = supportDataDto.Password;
                acDbSupport.State = (int)SupportState.AutorizationProcessing;
                registerTaskInQueueProvider.RegisterTask(
                taskType: CoreWorkerTaskType.SupportAuthVerification,
                comment: cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.CheckingLoginAndPasswordOf1CDatabase, accountDatabase.AccountId),
                parametrizationModel: new ParametrizationModelDto(acDbSupport.AccountDatabasesID.ToString()));
            }

            dbLayer.AcDbSupportRepository.Update(acDbSupport);
            dbLayer.Save();
        }

        private string GetSupportStateDescription(SupportState state)
        {
            return state switch
            {
                SupportState.NotAutorized =>
                    "Все регламентные операции выполняются в установленное вами время по МСК и только при отсутствии активных сессий пользователя.",
                SupportState.AutorizationFail => "Ошибка подключения к базе, попробуйте еще раз",
                SupportState.NotValidAuthData =>
                    SupportMessagesConstsDto.InvalidLoginPasswordFull.LineEndingToHtmlStyle(),
                SupportState.AutorizationProcessing =>
                    "Уважаемый пользователь!\r\nПроводится авторизация в информационной базе.\r\nПроцесс может занять до 30 минут.\r\nПо завершении процесса авторизации вам будет доступна возможность подключить Автоматическое обновление (АО) и Тестирование и Исправление ошибок (ТиИ)\r\nПожалуйста, подождите.",
                SupportState.AutorizationSuccess => "Авторизация успешно завершена!",
                _ => state.Description()
            };
        }

        public void ChangeHasTIIFlag(AcDbSupport acDbSupport, bool hasSupport)
        {
            try
            {
                accountDatabaseSupportProvider.ChangeHasSupportFlag(acDbSupport, hasSupport);

                if (hasSupport) 
                    LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider, LogActions.ConnectInfoBaseTaC, 
                        $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(acDbSupport.AccountDatabase)} подключена к ТиИ");
                else
                    LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider, LogActions.DisConnectInfoBaseTaC, 
                        $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(acDbSupport.AccountDatabase)} отключена от ТиИ");
            }
            catch (Exception ex)
            {
                LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider, LogActions.ConnectInfoBaseTaC, 
                    $"При попытке подключения базы  {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(acDbSupport.AccountDatabase)} к ТИИ произошла ошибка: {ex.Message}");
                handlerException.Handle(ex, $"[Ошибка переключения флага ТиИ]");
                throw;
            }
        }

        public void ChangeHasAutoUpdateFlag(AcDbSupport acDbSupport, bool hasAutoUpdate)
        {
            try
            {
                accountDatabaseSupportProvider.ChangeHasAutoUpdateFlag(acDbSupport, hasAutoUpdate);

                var AUState = hasAutoUpdate ? "подклюено" : "отключено";
                LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider,
                    hasAutoUpdate ? LogActions.ConnectInfoBaseToAU : LogActions.DisConnectInfoBaseToAU,
                    $"У базы {acDbSupport.AccountDatabase.V82Name} успешно {AUState} автообновление");
            }
            catch (Exception ex)
            {
                LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider,
                        LogActions.DisConnectInfoBaseToAU,
                        $"Не удачная попытка подключения/отключения АО для базы {acDbSupport.AccountDatabase.V82Name} Причина: '{ex.Message}'");
                handlerException.Handle(ex, $"[Ошибка переключения флага Автообновления]");
                throw;
            }
        }


    }
}
