﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Internal
{
    public class CreatePathForNewFileStorageHelper(
        IUnitOfWork dbLayer,
        AccountDatabasePathHelper accountDatabasePathHelper,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        ILogger42 logger)
    {
        /// <summary>
        /// Создать путь для нового файлового хранилища
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="newFileStorageId">ID нового хранилища</param>
        /// <returns>Путь для нового хранилища</returns>
        public string Create(Domain.DataModels.AccountDatabase accountDatabase, Guid newFileStorageId)
        {
            var fileStorageServer = dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(w => w.ID == newFileStorageId)
                                    ?? throw new NotFoundException($"Файловое хранилище по ID {newFileStorageId} не найдено");

            var companyName = GenerateCompanyNameHelper.GenerateCompanyName(accountDatabase);
            var companyPath = Path.Combine(fileStorageServer.ConnectionAddress, companyName);

            var newPath = accountDatabasePathHelper.GetPath(accountDatabase, fileStorageServer);

            if (!Directory.Exists(companyPath))
            {
                Directory.CreateDirectory(companyPath);
            }

            activeDirectoryTaskProcessor.TryDoImmediately(provider=> provider.SetDirectoryAcl(companyName, companyPath));
            
            logger.Info($"[{accountDatabase.Id}] :: Права на папку {companyPath} успешно добавлены");

            if (!Directory.Exists(newPath))
            {
                return newPath;
            }

            var waitAfterDbDelete = int.Parse(CloudConfigurationProvider.CoreWorker.GetSecondToWaitAfterDbDelete());

            Directory.Delete(newPath, true);
            Task.Delay(waitAfterDbDelete).Wait();

            logger.Info($"[{accountDatabase.Id}] :: Папка очищена для переноса базы. {newPath}");

            return newPath;
        }
    }
}
