﻿using System.Diagnostics;
using Clouds42.Configurations.Configurations;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Tools1C.Helpers;

namespace Clouds42.AccountDatabase.Internal;
/// <summary>
/// команда для конвертации базы с 1сd файла в dt
/// </summary>
public class Convert1CDToDtCommand
{
    private readonly Launch1CHelper _launch1CHelper;
    private readonly ILogger42 _logger;
    private readonly Lazy<int> _restoreIbFromDtCommandTimeout;
    private readonly IHandlerException _handlerException;
    private readonly LogUpdate1CParser _logUpdate1CParser;


    public Convert1CDToDtCommand(Launch1CHelper launch1CHelper,
        ILogger42 logger,
        IHandlerException handlerException,
        LogUpdate1CParser logUpdate1CParser)
    {
        _launch1CHelper = launch1CHelper;
        _logger = logger;
        _restoreIbFromDtCommandTimeout = new Lazy<int>(CloudConfigurationProvider.Enterprise1C.GetRestoreIbFromDtCommandTimeout);
        _handlerException = handlerException;
        _logUpdate1CParser = logUpdate1CParser;
    }
    
    
    public LogUpdate1CParser.Result Execute(Guid accountDatabaseId, string pathToDtFile, string login, string password)
    {
        try
        {
            var launch1CParams =
                _launch1CHelper.GetLaunch1CParamsForDesignMode(accountDatabaseId, pathToDtFile, login, password);
            
            _logger.Info($"Строка запуска: {launch1CParams.ExeParameters}");

            var process = new Process
            {
                StartInfo =
                {
                    FileName = launch1CParams.Client1CPath,
                    Arguments = launch1CParams.ExeParameters
                }
            };

            _logger.Info($"Путь к исполняемому файлу платформы: {launch1CParams.Client1CPath}");

            process.Start();
            process.WaitForExit(_restoreIbFromDtCommandTimeout.Value);

            if (process.HasExited)
                return _logUpdate1CParser.Parse(launch1CParams.LogFilePath, process.ExitCode);

            var cancelMessage = "Принудительная отмена конвертирования 1cd в dt ";
            _logger.Trace(cancelMessage);
            process.Kill();
            throw new InvalidOperationException(cancelMessage);
        }
        catch (Exception ex)
        {
            _handlerException.Handle(ex, $"Ошибка при выполнении процесса: {ex.Message}");
            throw;
        }
    }
}
