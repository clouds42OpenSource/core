﻿using System.Diagnostics;
using Clouds42.Configurations.Configurations;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Tools1C.Helpers;
using Microsoft.Data.SqlClient;

namespace Clouds42.AccountDatabase.Internal;

public class CreateClusteredDatabaseCommand
{
    private readonly Launch1CHelper _launch1CHelper;
    private readonly ILogger42 _logger;
    private readonly Lazy<int> _restoreIbFromDtCommandTimeout;
    private readonly IHandlerException _handlerException;
    
    public CreateClusteredDatabaseCommand(Launch1CHelper launch1CHelper, ILogger42 logger, IHandlerException handlerException)
    {
        _launch1CHelper = launch1CHelper;
        _logger = logger;
        _handlerException = handlerException;
        _restoreIbFromDtCommandTimeout = new Lazy<int>(CloudConfigurationProvider.Enterprise1C.GetRestoreIbFromDtCommandTimeout);

    }
    
    public void Execute(Guid accountDatabaseId, string? uploadedDtFilePath = null, bool isCopy = false)
    {
        try
        {
            var launch1CParams =
                _launch1CHelper.GetLaunch1CParamsForCreateMode(accountDatabaseId, uploadedDtFilePath);
            
            var parameters = launch1CParams.ExeParameters;
            var dbServerName = parameters.Split(new[] { "DBSrvr=" }, StringSplitOptions.None)[1].Split(';')[0].Trim('"');
            var dbUserId = parameters.Split(new[] { "DBUID=" }, StringSplitOptions.None)[1].Split(';')[0].Trim('"');
            var dbPassword = parameters.Split(new[] { "DBPwd=" }, StringSplitOptions.None)[1].Split(';')[0].Trim('"');
            
            if (!PingDatabase(dbServerName, dbUserId, dbPassword))
            {
                var message = $"Не удалось подключиться к серверу {dbServerName}";
                _logger.Error(message);
                throw new InvalidOperationException(message);
            }
            _logger.Info($"Строка запуска: {launch1CParams.ExeParameters}");

            var process = new Process
            {
                StartInfo =
                {
                    FileName = launch1CParams.Client1CPath,
                    Arguments = launch1CParams.ExeParameters
                }
            };

            _logger.Info($"Путь к исполняемому файлу платформы: {launch1CParams.Client1CPath}");

            process.Start();
            process.WaitForExit(_restoreIbFromDtCommandTimeout.Value);

            if (process.HasExited)
                return;

            var cancelMessage = $"Принудительная отмена создания серверной базы ";
            _logger.Trace(cancelMessage);
            process.Kill();
            throw new InvalidOperationException(cancelMessage);
        }
        catch (Exception ex)
        {
            
            _handlerException.Handle(ex, $"Ошибка при выполнении процесса: {ex.Message}");
            throw;
        }
    }
    
    
    private bool PingDatabase(string serverName, string userId, string password)
    {
        try
        {
            using var connection = new SqlConnection(
                $"Server={serverName};User Id={userId};Password={password};Connection Timeout=5;Encrypt=True;TrustServerCertificate=True;");
            connection.Open();
            return true;
        }
        catch (Exception ex)
        {
            _logger.Warn($"Не удалось подключиться к серверу {serverName}: {ex.Message}");
            return false;
        }
    }
}
