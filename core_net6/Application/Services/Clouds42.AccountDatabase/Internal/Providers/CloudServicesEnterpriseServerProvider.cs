﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Internal.Providers
{
    /// <summary>
    /// Провайдер доступа к данным по серверу предприятия.
    /// </summary>
    public class CloudServicesEnterpriseServerProvider(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить данные сервера 1С по инф. базе
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Данные по серверу 1С</returns>
        public CloudServicesEnterpriseServer GetEnterpriseServerData(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var segmentOfAccount =
                (from accountConfiguration in dbLayer.GetGenericRepository<AccountConfiguration>().WhereLazy()
                    join segment in dbLayer.CloudServicesSegmentRepository.WhereLazy() on accountConfiguration
                        .SegmentId equals segment.ID
                    where accountConfiguration.AccountId == accountDatabase.AccountId
                    select segment).FirstOrDefault();

            if (segmentOfAccount == null)
                return null;

            return accountDatabase.ApplicationName.Contains("8.2")
                ? segmentOfAccount.CloudServicesEnterpriseServer82
                : segmentOfAccount.CloudServicesEnterpriseServer83;
        }
    }
}
