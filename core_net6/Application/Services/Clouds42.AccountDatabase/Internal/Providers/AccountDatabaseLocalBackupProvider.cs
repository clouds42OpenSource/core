﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.BLL.Common.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Domain;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Internal.Providers
{
    /// <summary>
    /// Провайдер для локальных бэкапов инф. баз
    /// </summary>
    public class AccountDatabaseLocalBackupProvider(
        IUnitOfWork dbLayer,
        ISegmentHelper segmentHelper,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        private readonly AccountDatabasePathHelper _accountDatabasesPathHelper = new(dbLayer, logger, segmentHelper, handlerException);

        /// <summary>
        ///     Сделать бэкап информационной базы
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <param name="trigger">Триггер создания бэкапа</param>
        /// <param name="initiatorId">Инициатор удаления инфомрационной базы</param>
        /// <returns>ID бэкапа</returns>
        public Guid CreateBackupAccountDatabase(Domain.DataModels.AccountDatabase database, CreateBackupAccountDatabaseTrigger trigger, Guid initiatorId)
        {
            logger.Info($"[{database.Id}] :: Создание бэкапа для информационной базы {database.V82Name}");

            var backupPath = database.IsFile == true
                ? BackupAccountDatabaseFile(_accountDatabasesPathHelper.GetPath(database), database.Account.IndexNumber, database.V82Name)
                : BackupAccountDatabaseServer(database.SqlName, database.Account.IndexNumber, database.V82Name);

            logger.Info($"[{database.Id}] :: Бэкап создан: {backupPath}");

            return SaveBackupAccountDatabaseInDb(database, trigger, initiatorId, backupPath, AccountDatabaseBackupSourceType.Local);
        }

        /// <summary>
        ///     Сделать бэкап информационной базы
        /// </summary>
        /// <param name="accountDatabaseId">Информационная база</param>
        /// <param name="trigger">Триггер создания бэкапа</param>
        /// <param name="initiatorId">Инициатор удаления инфомрационной базы</param>
        /// <returns>ID бэкапа</returns>
        public Guid CreateBackupAccountDatabase(Guid accountDatabaseId, CreateBackupAccountDatabaseTrigger trigger, Guid initiatorId)
        {
            var database = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == accountDatabaseId) 
                           ?? throw new NotFoundException($"Не удалось найти информационную базу по номеру {accountDatabaseId}");

            return CreateBackupAccountDatabase(database, trigger, initiatorId);
        }

        /// <summary>
        ///     Удалить бэкап информационной базы
        /// </summary>
        /// <param name="backupId">Бэкап на удаление</param>
        /// <param name="deleteInDb">Удалить запись о бэкапе в базе данных</param>
        public void DeleteBackupAccountDatabase(Guid backupId, bool deleteInDb = false)
        {
            var backup = dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(w => w.Id == backupId)
                         ?? throw new NotFoundException(
                             $"Объект бэкапа {backupId} информационной базы не найден");

            if (File.Exists(backup.BackupPath))
            {
                File.Delete(backup.BackupPath);
                logger.Info($"[{backup.AccountDatabaseId}] :: Удаление архива выполнено");
            }
            if (!deleteInDb)
                return;
            DeleteAccountDatabaseBackupInDatabase(backup);

        }

        /// <summary>
        ///     Сохранить запись о бэкапе в базе
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <param name="trigger">Триггер создания бэкапа</param>
        /// <param name="initiatorId">Инициатор удаления инфомрационной базы</param>
        /// <param name="backupPath">Путь к файлу бэкапа</param>
        /// <param name="sourceType">Тип хранилища</param>
        /// <returns>ID бэкапа</returns>
        private Guid SaveBackupAccountDatabaseInDb(Domain.DataModels.AccountDatabase database, CreateBackupAccountDatabaseTrigger trigger,
            Guid initiatorId, string backupPath, AccountDatabaseBackupSourceType sourceType)
        {
            try
            {
                var backup = dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(w =>
                    w.AccountDatabaseId == database.Id && w.BackupPath == backupPath);

                if (backup != null)
                {
                    logger.Info($"[{database.Id}] :: Схожая запись уже была добавлена в базу, актуализируем ее");

                    backup.InitiatorId = initiatorId;
                    backup.EventTrigger = trigger;
                    backup.SourceType = sourceType;

                    dbLayer.AccountDatabaseBackupRepository.Update(backup);
                    dbLayer.Save();

                    logger.Info($"[{database.Id}] :: Создание бэкапа и актуализация данных завершена");
                    return backup.Id;
                }

                logger.Info($"[{database.Id}] :: Создание новой записи о бэкапе в базе");

                var newBackup = new AccountDatabaseBackup
                {
                    AccountDatabaseId = database.Id,
                    BackupPath = backupPath,
                    CreationBackupDateTime = DateTime.Now,
                    EventTrigger = trigger,
                    SourceType = sourceType,
                    InitiatorId = initiatorId,
                    Id = Guid.NewGuid()
                };

                dbLayer.AccountDatabaseBackupRepository.Insert(newBackup);
                dbLayer.Save();

                logger.Info($"[{database.Id}] :: Создание бэкапа и актуализация данных завершена");
                return newBackup.Id;
            }
            catch (Exception ex)
            {
                logger.Info($"[{database.Id}] :: Непредвиденная ошибка: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Удалить запись о бэкапе инф. базы в базе данных
        /// </summary>
        /// <param name="databaseBackup">Бэкап инф. базы</param>
        private void DeleteAccountDatabaseBackupInDatabase(AccountDatabaseBackup databaseBackup)
        {
            logger.Info($"[{databaseBackup.AccountDatabaseId}] :: Удаление записи о бэкапе из базы данных");

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                DeleteAccountDatabaseBackupHistories(databaseBackup.Id);
                DeleteServiceManagerAcDbBackup(databaseBackup.Id);
                dbLayer.AccountDatabaseBackupRepository.Delete(databaseBackup);
                dbLayer.Save();

                dbScope.Commit();

                logger.Info($"[{databaseBackup.AccountDatabaseId}] :: Удаление бэкапа выполнено");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаление бэкапа] ИБ: {databaseBackup.AccountDatabaseId}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Удалить записи истории бэкапа
        /// </summary>
        /// <param name="backupId">Id бэкапа</param>
        private void DeleteAccountDatabaseBackupHistories(Guid backupId)
        {
            var backupHistories = dbLayer.AccountDatabaseBackupHistoryRepository.Where(history =>
                history.AccountDatabaseBackupId == backupId).ToList();

            if (!backupHistories.Any())
                return;

            logger.Info($"[{backupId}] :: Удаление историй бэкапа");

            dbLayer.AccountDatabaseBackupHistoryRepository.DeleteRange(backupHistories);
            dbLayer.Save();
        }

        /// <summary>
        /// Удалить связь бэкапа инф. базы с бэкапом МС
        /// </summary>
        /// <param name="backupId"></param>
        private void DeleteServiceManagerAcDbBackup(Guid backupId)
        {
            var serviceManagerAcDbBackup = dbLayer.GetGenericRepository<ServiceManagerAcDbBackup>()
                .FirstOrDefault(smb => smb.AccountDatabaseBackupId == backupId);

            if (serviceManagerAcDbBackup == null)
                return;

            logger.Info($"[{backupId}] :: Удаление связи бэкапа инф. базы с бэкапом МС");

            dbLayer.GetGenericRepository<ServiceManagerAcDbBackup>().Delete(serviceManagerAcDbBackup);
            dbLayer.Save();
        }

        /// <summary>
        ///     Бэкап файловой информационной базы
        /// </summary>
        /// <param name="filePath">Путь к базе</param>
        /// <param name="accountIndexNumber">Номер аккаунта</param>
        /// <param name="v82Name">Имя базы</param>
        /// <returns>Путь к файлу бэкапа</returns>
        private string BackupAccountDatabaseFile(string filePath, int accountIndexNumber, string v82Name)
        {
            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.IndexNumber == accountIndexNumber)
                          ?? throw new NotFoundException($"Аккаунт не найден по номеру: {accountIndexNumber}");

            var backupStorage = segmentHelper.GetBackupStorage(account);
            var backupDirectory = Path.Combine(backupStorage, "tomb", $"company_{accountIndexNumber}");
            var backupPath = Path.Combine(backupDirectory, $"{v82Name}_{DateTime.Now:yyyy-MM-dd-HH-mm}.zip");

            if (!Directory.Exists(backupDirectory))
                Directory.CreateDirectory(backupDirectory);

            if (File.Exists(backupPath) && filePath.ToDirectoryInfo().GetSize() != 0)
                File.Delete(backupPath);

            if (filePath.ToDirectoryInfo().GetSize() == 0 && File.Exists(backupPath))
            {
                logger.Info($"Каталог {filePath} пуст, но есть бекап");
                return backupPath;
            }

            if (filePath.ToDirectoryInfo().GetSize() == 0)
                throw new InvalidOperationException($"Каталог {filePath} пуcт");

            ZipHelper.SafelyCreateZipFromDirectory(filePath, backupPath);

            return backupPath;
        }

        /// <summary>
        ///     Бэкап серверной информационной базы
        /// </summary>
        /// <param name="sqlName">Название в SQL сервере</param>
        /// <param name="accountIndexNumber"></param>
        /// <param name="v82Name">Имя базы</param>
        /// <returns>Путь к файлу бэкапа</returns>
        private string BackupAccountDatabaseServer(string sqlName, int accountIndexNumber, string v82Name)
        {
            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.IndexNumber == accountIndexNumber) 
                          ?? throw new NotFoundException($"Аккаунт не найден по номеру: {accountIndexNumber}");

            var backupDirectory = Path.Combine(segmentHelper.GetBackupStorage(account), "tomb", $"company_{accountIndexNumber}");
            var bakDirectory = Path.Combine(backupDirectory, "baks", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-sss"));
            var zipDirectory = Path.Combine(backupDirectory, "zips");

            var blankFileName = $"{v82Name}_{DateTime.Now:yyyy-MM-dd-HH-mm-sss}";

            var bakFilePath = Path.Combine(bakDirectory, $"{blankFileName}.bak");
            var zipFilePath = Path.Combine(zipDirectory, $"{blankFileName}.zip");

            if (!Directory.Exists(zipDirectory))
                Directory.CreateDirectory(zipDirectory);

            if (!Directory.Exists(bakDirectory))
                Directory.CreateDirectory(bakDirectory);

            if (File.Exists(bakFilePath))
                File.Delete(bakFilePath);

            if (File.Exists(zipFilePath))
                File.Delete(zipFilePath);

            var sqlServer = segmentHelper.GetSQLServer(account);
            logger.Info($"Создание бекапа для серверной базы: Sql запрос BACKUP DATABASE [{sqlName}] TO DISK='{bakFilePath}', на SQL сервер {sqlServer}");

            const int timeout = 60 * 45;

            dbLayer.Execute1CSqlQuery(string.Format(SqlQueries.DatabaseCore.BackupDatabase, sqlName, bakFilePath), sqlServer, timeout);

            Task.Delay(60000).Wait();

            ZipHelper.SafelyCreateZipFromDirectory(bakDirectory, zipFilePath);

            DirectoryHelper.DeleteDirectory(bakDirectory);

            return zipFilePath;
        }
    }
}
