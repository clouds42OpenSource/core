﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.CoreWorker.JobWrappers.DropDatabase;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.PowerShellClient;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Internal.Providers
{
    /// <summary>
    /// Провайдер удаления инф. баз
    /// </summary>
    public class AccountDatabaseDeleteProvider(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        ISegmentHelper segmentHelper,
        TombTaskCreatorManager tombTaskCreatorManager,
        AccountDatabasePathHelper accountDatabasesPathHelper,
        IDeleteDelimiterCommand deleteDelimiterCommand,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        PowerShellClientWrapper powerShellClientWrapper,
        IDropDatabaseFromClusterJobWrapper dropDatabaseFromClusterJobWrapper,
        ILogger42 logger)
    {
        /// <summary>
        ///     Создать задачу на удаление базы в склеп
        /// </summary>
        /// <param name="databaseId">ID информационной базы</param>
        /// <param name="sendEmail">В случае ошибки, отправить письмо хотлайну</param>
        /// <param name="forceUpload">Принудительная загрузка файла в склеп</param>
        /// <param name="trigger">Триггер отправки базы в склеп</param>
        public void CreateTaskForDeleteAccountDatabaseToTomb(Guid databaseId, bool sendEmail, bool forceUpload, CreateBackupAccountDatabaseTrigger trigger)
        {
            logger.Info($"[{databaseId}] :: Информационная база поставлена на отправку в склеп");

            var database = unitOfWork.DatabasesRepository.FirstOrDefault(d => d.Id == databaseId);
            if (database == null)
            {
                logger.Info($"[{databaseId}] :: Информационная база не найдена");
                return;
            }

            logger.Info($"[{databaseId}] :: Информационная база найдена: {database.V82Name}");

            var databaseState = database.State.ToEnum<DatabaseState>();
            if (databaseState is DatabaseState.DelitingToTomb or DatabaseState.DetachingToTomb or DatabaseState.DeletedToTomb)
            {
                logger.Info($"[{databaseId}] :: Информационная база уже в статусе : {database.State}.");
                return;
            }

            accountDatabaseChangeStateProvider.ChangeState(database.Id, DatabaseState.DelitingToTomb);

            if (database.IsDelimiter())
            {
                var locale = database.Account.AccountConfiguration.Locale.Name;
                logger.Info($"[{databaseId}] :: Запрос на удаление базы на разделителях отправлен в МС.");
                var result = deleteDelimiterCommand.Execute(database.Id, locale);
                accountDatabaseChangeStateProvider.ChangeState(database.Id, DatabaseState.DeletedFromCloud, result);
                logger.Info($"[{databaseId}] :: Запрос на удаление базы на разделителях успешно отправлен в МС.");
                return;
            }

            tombTaskCreatorManager.CreateTaskForDeleteAccountDatabaseToTomb(new DeleteAccountDatabaseToTombJobParamsDto
            {
                AccountDatabaseId = databaseId,
                SendMail = sendEmail,
                ForceUpload = forceUpload,
                Trigger = trigger,
                InitiatorId = accessProvider.GetUser().Id
            });
        }

        /// <summary>
        ///     Удалить локальные файлы информационной базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns></returns>
        public bool DeleteLocalAccountDatabase(Domain.DataModels.AccountDatabase accountDatabase)
        {
            return accountDatabase.IsFile == true
                ? DeleteFileBase(accountDatabase)
                : DeleteServerBase(accountDatabase);
        }

        /// <summary>
        ///     Удалить сессии на Sql
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns></returns>
        public void DropSessionOnSql(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var sqlScript = string.Format(SqlQueries.DatabaseCore.KillDatabaseSession, accountDatabase.SqlName);

            logger.Info($"[{accountDatabase.Id}] :: Скрипт завершения сеансов: {sqlScript}");

            unitOfWork.Execute1CSqlQuery(sqlScript, segmentHelper.GetSQLServer(accountDatabase.Account));
        }

        /// <summary>
        /// Удалить серверную базу
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Результат удаления</returns>
        private bool DeleteServerBase(Domain.DataModels.AccountDatabase accountDatabase)
        {
            logger.Info($"[{accountDatabase.Id}] :: Удаление серверной информационной базы");
            try
            {
                logger.Info($"[{accountDatabase.Id}] :: Завершение всех сеансов в серверной информационной базы на SQL сервера");

                DropSessionOnSql(accountDatabase);

                logger.Info($"[{accountDatabase.Id}] :: Удаление серверной информационной базы c SQL сервера");

                var sqlScript = string.Format(SqlQueries.DatabaseCore.DropDatabase, accountDatabase.SqlName);

                logger.Info($"[{accountDatabase.Id}] :: Скрипт удаления: {sqlScript}");

                unitOfWork.Execute1CSqlQuery(sqlScript, segmentHelper.GetSQLServer(accountDatabase.Account));

                logger.Info($"[{accountDatabase.Id}] :: Удаление серверной информационной базы c SQL сервера выполнено");
            }
            catch (Exception ex)
            {
                logger.Info($"[{accountDatabase.Id}] :: Ошибка удаления серверной информационной базы c SQL сервера. {ex.GetFullInfo()}");
                throw;
            }

            try
            {
                logger.Info($"[{accountDatabase.Id}] :: Удаление серверной информационной базы c кластера");

                dropDatabaseFromClusterJobWrapper
                    .Start(new DropDatabaseFromClusterJobParams { AccountDatabaseId = accountDatabase.Id })
                    .Wait();

                logger.Info($"[{accountDatabase.Id}] :: Удаление серверной информационной базы c кластера выполнено");
            }
            catch (Exception ex)
            {
                logger.Info($"[{accountDatabase.Id}] :: Ошибка удаления серверной информационной базы c кластера. {ex.GetFullInfo()}");
                throw;
            }


            return true;
        }

        /// <summary>
        /// Удалить файловую базу
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Результат удаления</returns>
        private bool DeleteFileBase(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var path = accountDatabasesPathHelper.GetPath(accountDatabase);
            logger.Info($"[{accountDatabase.Id}] :: Удаление файловой информационной базы: {path}");
            var cloudServiceFileStorageServer =
                    unitOfWork.CloudServicesFileStorageServerRepository.FirstOrDefault(w => w.ID == accountDatabase.FileStorageID);
            try
            {
                if (!DirectoryHelper.Exists(path))
                {
                    logger.Info($"[{accountDatabase.Id}] :: Директория не найдена: {path}");
                    return true;
                }
                DirectoryHelper.SetAttributesNormal(path);

                powerShellClientWrapper.ExecuteScript(
                    $"Get-SmbOpenFile | where {{$_.Path –like \"*{accountDatabase.V82Name}*\"}} | Close-SmbOpenFile -Force",
                    cloudServiceFileStorageServer.DnsName);

                logger.Info($"[{accountDatabase.Id}] :: Стартуем таску по удалению директории: {path}");
                DirectoryHelper.TryDeleteDirectoryOrThrowException(path, 200);

                logger.Info($"[{accountDatabase.Id}] ::Файловая информационная база удалена: {path}");

                return true;
            }
            catch (Exception ex)
            {
                logger.Info($"[{accountDatabase.Id}] :: Ошибка удаления файловой информационной базы: {ex.GetFullInfo()}");
                return false;
            }
        }
    }
}
