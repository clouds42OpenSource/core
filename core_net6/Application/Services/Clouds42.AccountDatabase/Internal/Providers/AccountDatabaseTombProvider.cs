﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Internal.Providers
{
    /// <summary>
    /// Провайдер по переносу информационной базы в склеп.
    /// </summary>
    internal class AccountDatabaseTombProvider(
        AccountDatabaseDeleteProvider accountDatabaseDeleteProvider,
        SynchronizeAccountDatabaseWithTardisHelper synchronizeAccountDatabaseWithTardisHelper,
        CheckAccountDatabaseHelper checkAccountDatabaseHelper)
        : IAccountDatabaseTombProvider
    {
        /// <summary>
        /// Удалить информационную базу в склеп
        /// </summary>
        /// <param name="accountDatabase">Информационная база.</param>
        /// <param name="sendEmail">В случае ошибки, отправить письмо хотлайну</param>
        /// <param name="forceUpload">Принудительная загрузка файла в склеп</param>
        /// <param name="trigger">Триггер отправки базы в склеп</param>
        public void DeleteAccountDatabaseToTomb(
            Domain.DataModels.AccountDatabase accountDatabase, 
            bool sendEmail, 
            bool forceUpload = false,
            CreateBackupAccountDatabaseTrigger trigger = CreateBackupAccountDatabaseTrigger.ManualRemoval)
        {

            checkAccountDatabaseHelper.CheckPossibilitiesArchiveDatabasesToTomb(accountDatabase);
            accountDatabaseDeleteProvider.CreateTaskForDeleteAccountDatabaseToTomb(accountDatabase.Id, sendEmail, forceUpload, trigger);
            synchronizeAccountDatabaseWithTardisHelper.SynchronizeAccountDatabaseViaNewThread(accountDatabase.Id);
        }
    }
}