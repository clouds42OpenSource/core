﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Internal.Mappers;
using Clouds42.AccountDatabase.Specification;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using PagedList.Core;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;

namespace Clouds42.AccountDatabase.Internal.Providers
{
    /// <summary>
    /// Провайдер получения данных об информационных базах
    /// </summary>
    internal class AccountDatabaseInfoDataProvider(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer)
        : IAccountDatabaseInfoDataProvider
    {
        /// <summary>
        /// Получить список информационных баз
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="searchQuery">Фильр поиска</param>
        /// <returns>Список информационных баз</returns>
        public AccountDatabasesListDataDto GetAccountDatabasesList(int page, string searchQuery)
        {
            const int itemsPerPage = 50;

            var accountDatabases = GetAccountDatabases();

            accountDatabases = SearchDatabasesByFilter(accountDatabases, searchQuery);

            var databases = accountDatabases.OrderByDescending(a => a.CreationDate);

            var databaseCount = accountDatabases.Count();

            var userPrincipal = accessProvider.GetUser();

            var pagedList = databases.ToPagedList(page, itemsPerPage).ToList()
                .Select(w => w.MapToAccountDatabaseInfoDc(userPrincipal)).ToList();

            var accountDatabasesOnCurrentPage = new AccountDatabasesListDataDto
            {
                Databases = pagedList,
                Pagination = new PaginationBaseDto(page, databaseCount, itemsPerPage)
            };

            return accountDatabasesOnCurrentPage;

        }

        /// <summary>
        /// Получить список информационных баз
        /// </summary>
        /// <returns>Список информационных баз</returns>
        private IQueryable<Domain.DataModels.AccountDatabase> GetAccountDatabases() => dbLayer.DatabasesRepository.WhereLazy(d =>
            d.Account != null && d.State != DatabaseState.DeletedFromCloud.ToString());

        /// <summary>
        /// Найти информационные базы по фильтру поиска
        /// </summary>
        /// <returns>Найденные инф базы</returns>
        private IQueryable<Domain.DataModels.AccountDatabase> SearchDatabasesByFilter(IQueryable<Domain.DataModels.AccountDatabase> accountDatabases, string searchQuery)
        {
            var zoneNumber = searchQuery.ToInt();

            if (!string.IsNullOrEmpty(searchQuery))
            {
                accountDatabases = accountDatabases.Where(d =>
                        (d.Caption != null && d.Caption.ToLower().Contains(searchQuery.ToLower())) ||
                        (d.V82Name != null && d.V82Name.ToLower().Contains(searchQuery.ToLower())) ||
                        d.DbNumber.ToString().Contains(searchQuery) ||
                        (d.Account.AccountCaption.ToLower().Contains(searchQuery.ToLower())) ||
                        (d.Account.IndexNumber.ToString().Contains(searchQuery)) ||
                        (d.AccountDatabaseOnDelimiter != null
                         && d.AccountDatabaseOnDelimiter.Zone == zoneNumber));
            }

            return accountDatabases;
        }

        /// <summary>
        /// Получить список информационных баз
        /// </summary>
        /// <param name="args">Фильр поиска</param>
        /// <returns>Список информационных баз</returns>
        public async Task<PagedDto<AccountDatabaseItemDto>> GetAccountDatabaseItemsAsync(GetAccountDatabaseListParamsDto args)
        {
            return (await dbLayer
                .DatabasesRepository
                .AsQueryable()
                .Where(DatabaseSpecification.ByAccountNotNull() 
                        && DatabaseSpecification.ByStateNotEquals(DatabaseState.DeletedFromCloud) 
                        && DatabaseSpecification.BySearchLine(args.Filter?.SearchLine))
                .AutoSort(args, StringComparison.OrdinalIgnoreCase)
                .Select(x => new AccountDatabaseItemDto
                {
                    SizeInMB = x.SizeInMB,
                    State = x.State,
                    LockedState = x.LockedState,
                    IsFile = x.IsFile,
                    AccountCaption = x.Account.AccountCaption,
                    AccountId = x.AccountId,
                    V82Name = x.V82Name,
                    AccountNumber = x.Account.IndexNumber,
                    ApplicationName = x.ApplicationName,
                    Caption = x.Caption,
                    LastActivityDate = x.LastActivityDate,
                    Id = x.Id,
                    CreationDate = x.CreationDate

                })
                .ToPagedListAsync(args?.PageNumber ?? 1, args?.PageSize ?? 50))
                .ToPagedDto();
        }
    }
}
