﻿using Clouds42.Common.Exceptions;
using Clouds42.CoreWorker.JobWrappers.RemoveFile;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Хэлпер для работы с загруженными файлами инф. баз
    /// </summary>
    public class DatabaseUploadedFileHelper(
        IRemoveFileJobWrapper removeFileJobWrapper,
        IUnitOfWork dbLayer,
        ILogger42 logger)
    {
        /// <summary>
        /// Удалить загруженный файл инф. базы на разделителях
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        public void DeleteDbOnDelimitersUploadedFile(Guid accountDatabaseId)
        {
            var accountDatabaseOnDelimiters = GetAccountDatabaseOnDelimiters(accountDatabaseId);

            if (!ValidateAcDbUploadedFile(accountDatabaseOnDelimiters))
                return;

            StartRemoveFileJob(accountDatabaseOnDelimiters.UploadedFile.FullFilePath);

            TraceRemoveFileJobCreation(accountDatabaseOnDelimiters);
        }

        /// <summary>
        /// Удалить загруженный файл инф. базы на разделителях
        /// </summary>
        /// <param name="accountDatabaseOnDelimiters">Инф. база на разделителях</param>
        public void DeleteDbOnDelimitersUploadedFile(AccountDatabaseOnDelimiters accountDatabaseOnDelimiters)
        {
            if (!ValidateAcDbUploadedFile(accountDatabaseOnDelimiters))
                return;

            StartRemoveFileJob(accountDatabaseOnDelimiters.UploadedFile.FullFilePath);

            TraceRemoveFileJobCreation(accountDatabaseOnDelimiters);
        }

        /// <summary>
        /// Удалить загруженный файл инф. базы на разделителях с задержкой
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="startTaskDelay">Задержка выполнения задачи</param>
        public void DeleteDbOnDelimitersUploadedFileWithDelay(Guid accountDatabaseId, DateTime startTaskDelay)
        {
            var accountDatabaseOnDelimiters = GetAccountDatabaseOnDelimiters(accountDatabaseId);

            if (!ValidateAcDbUploadedFile(accountDatabaseOnDelimiters))
                return;

            StartRemoveFileJob(accountDatabaseOnDelimiters.UploadedFile.FullFilePath, startTaskDelay);

            TraceRemoveFileJobCreation(accountDatabaseOnDelimiters, startTaskDelay);
        }

        /// <summary>
        /// Удалить загруженный файл инф. базы на разделителях с задержкой
        /// </summary>
        /// <param name="accountDatabaseOnDelimiters">Инф. база на разделителях</param>
        /// <param name="startTaskDelay">Задержка выполнения задачи</param>
        public void DeleteDbOnDelimitersUploadedFileWithDelay(AccountDatabaseOnDelimiters accountDatabaseOnDelimiters, DateTime startTaskDelay)
        {
            if(!ValidateAcDbUploadedFile(accountDatabaseOnDelimiters))
                return;

            StartRemoveFileJob(accountDatabaseOnDelimiters.UploadedFile.FullFilePath, startTaskDelay);

            TraceRemoveFileJobCreation(accountDatabaseOnDelimiters, startTaskDelay);
        }

        /// <summary>
        /// Запустить задачу по удалению загруженного файла
        /// </summary>
        /// <param name="filePath">Путь к загруженному файлу</param>
        /// <param name="startTaskDelay">Задержка выполнения задачи</param>
        private void StartRemoveFileJob(string filePath, DateTime? startTaskDelay = null)
        {
            var removeFileJobParams = new RemoveFileJobParams
            {
                FilePath = filePath
            };

            if (startTaskDelay != null)
            {
                removeFileJobWrapper.Start(removeFileJobParams, startTaskDelay.Value);
                return;
            }

            removeFileJobWrapper.Start(removeFileJobParams);
        }

        /// <summary>
        /// Получить базу на разделителях
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>База на разделителях</returns>
        private AccountDatabaseOnDelimiters GetAccountDatabaseOnDelimiters(Guid accountDatabaseId)
            => dbLayer.AccountDatabaseDelimitersRepository.FirstOrDefault(t =>
                   t.AccountDatabaseId == accountDatabaseId) ??
               throw new NotFoundException($"База на разделителях по ID {accountDatabaseId} не найдена");

        /// <summary>
        /// Записать в лог создание задачи на удаление файла
        /// </summary>
        /// <param name="accountDatabaseOnDelimiters">Инф. база на разделителях</param>
        /// <param name="startTaskDelay">Задержка выполнения задачи</param>
        private void TraceRemoveFileJobCreation(AccountDatabaseOnDelimiters accountDatabaseOnDelimiters, DateTime? startTaskDelay = null)
        {
            if (startTaskDelay == null)
                startTaskDelay = DateTime.Now;

            logger.Trace(
                $"Создана задача по удалению файла {accountDatabaseOnDelimiters.UploadedFile.FileName}. " +
                $"Задача будет запущена {startTaskDelay:dd.MM.yyyy HH:mm}");
        }

        /// <summary>
        /// Проверить загруженный файл инф. базы
        /// </summary>
        /// <param name="accountDatabaseOnDelimiters">Инф. база на разделителях</param>
        /// <returns>true - если объект файла есть и у него заполнен путь</returns>
        private bool ValidateAcDbUploadedFile(AccountDatabaseOnDelimiters accountDatabaseOnDelimiters)
        {
            if (accountDatabaseOnDelimiters.UploadedFile == null ||
                string.IsNullOrEmpty(accountDatabaseOnDelimiters.UploadedFile.FullFilePath))
                return false;

            return true;
        }
    }
}
