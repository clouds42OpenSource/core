﻿using System.Diagnostics;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Tools1C.Helpers;

namespace Clouds42.AccountDatabase.Internal
{
    public class Run1CEpfMcobCommand(
        string v82Name,
        bool isFile,
        string filePath,
        string parameters,
        PlatformType platform)
    {
        public string V82Name { get; set; } = v82Name;
        public bool IsFile { get; set; } = isFile;
        public string FilePath { get; set; } = filePath;
        public string Parameters { get; set; } = parameters;
        public PlatformType Platform { get; set; } = platform;
    }

    public class Run1CEpfMcobCommandHandler(IUnitOfWork dbLayer, ISegmentHelper segmentHelper, ILogger42 logger)
    {
        public void Handle(Run1CEpfMcobCommand command)
        {
            var mcobLogin = CloudConfigurationProvider.Mcob.GetLoginMcob();
            var mcobPassword = CloudConfigurationProvider.Mcob.GetPasswordMcob();
            var externalProcessorPath = CloudConfigurationProvider.Mcob.GetProcessorMcob();

            var path1CExe = GetPath1CExe(command.V82Name, command.Platform);
            var server = Get1CServer(command.V82Name, command.Platform);
            var connectString1C = $" ENTERPRISE {GetConnectionString(server, command.V82Name, command.IsFile, command.FilePath)} /N\"{mcobLogin}\" /P\"{mcobPassword}\" ";

            var args = $"/RunModeOrdinaryApplication /Execute \"{externalProcessorPath}\" /C\"{command.Parameters}\" " +
                       $"{Start1CCommandLineConstants.DisableStartupMessages} {Start1CCommandLineConstants.DisableStartupDialogs} {Start1CCommandLineConstants.UsePrivilegedMode}";

            var script = $"\"{path1CExe}\" {connectString1C + args}";
            logger.Info("Arguments: {0}", script);

            var process = new Process { StartInfo = { FileName = path1CExe, Arguments = script } };
            process.Start();

            var secondsToWait = 60 * 10;   // 5 минут

            var wasStopped = false;
            // ждем 5 минут
            while (!process.HasExited)
            {
                Task.Delay(1000).Wait();
                if (--secondsToWait > 0) continue;
                try
                {
                    wasStopped = true;
                    process.Kill();
                }
                catch (Exception e)
                {
                    logger.Trace($"В процессе выполнения произошла ошибка: {e.GetFullInfo()}");
                }
            }

            if (!wasStopped)
            {
                logger.Info("Процесс завершил обработку ");
            }
            else
            {
                logger.Warn("Процесс был остановлен принудительно");
            }
        }

        private string GetPath1CExe(string v82Name, PlatformType platform)
        {
            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.V82Name == v82Name);

            if (accountDatabase == null)
                throw new InvalidOperationException($"По номеру базы {v82Name} не удалось получить информацию");

            return accountDatabase.DistributionTypeEnum switch
            {
                DistributionType.Alpha => segmentHelper.GetPath1CExeAlpha83Platform(accountDatabase.Account) +
                                          "1cv8.exe",
                DistributionType.Stable when platform == PlatformType.V83 => segmentHelper.GetPath1CExeStable83Platform(
                    accountDatabase.Account) + "1cv8.exe",
                _ => segmentHelper.GetPath1CExeStable82Platform(accountDatabase.Account) + "1cv8.exe"
            };
        }
        public string GetConnectionString(string server1C, string v82Name, bool isFile, string filePath)
        {
            return isFile ? $"/F{filePath}" : $"/S\"{server1C}\\{v82Name}\"";
        }
        private string Get1CServer(string v82Name, PlatformType platform)
        {
            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.V82Name == v82Name);

            if (accountDatabase == null)
                throw new InvalidOperationException($"По номеру базы {v82Name} не удалось получить информацию");

            // ReSharper disable once SwitchStatementMissingSomeCases
            return platform switch
            {
                PlatformType.V82 => segmentHelper.GetEnterpriseServer82(accountDatabase.Account),
                PlatformType.V83 => segmentHelper.GetEnterpriseServer83(accountDatabase.Account),
                _ => throw new InvalidOperationException(
                    $"По версии платформы {platform} не удалось получить информацию")
            };
        }
    }
}
