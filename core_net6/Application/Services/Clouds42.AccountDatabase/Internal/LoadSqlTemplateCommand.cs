﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Команда загрзки шаблона инф. базы
    /// </summary>
    public class LoadSqlTemplateCommand(
        DatabaseStatusHelper databaseStatusHelper,
        ILogger42 logger)
    {
        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="databaseInfo">Модель информации об инф. базе</param>
        /// <param name="template">Шаблон</param>
        public void Execute(DatabaseInfoDto databaseInfo, IDbTemplate template)
        {
            var fileTemplatePath = CloudConfigurationProvider.Template.GetTemplatesDirectory();

            var templatePath = Path.Combine(fileTemplatePath, template.Name + ".1CD");

            logger.Trace($"Начало загрузки шаблона {template.Name} в базу {databaseInfo.FilePath}");

            var destination = Path.Combine(databaseInfo.FilePath, "1Cv8.1CD");

            try
            {
                if (!Directory.Exists(databaseInfo.FilePath))
                    Directory.CreateDirectory(databaseInfo.FilePath);

                File.Copy(templatePath, destination, true);

                logger.Info($"Шаблон {template.Name} успешно загружен в базу {databaseInfo.FilePath}");
            }
            catch (Exception ex)
            {
                if (ex.IsDiskFullException())
                {
                    databaseStatusHelper.SetDbStatus(databaseInfo.V82Name, DatabaseState.ErrorNotEnoughSpace);
                }
                else
                {
                    logger.Warn($"Ошибка копированни шаблона в файловую базу {databaseInfo.FilePath}. Причина: {ex.GetFullInfo()}");
                    databaseStatusHelper.SetDbStatus(databaseInfo.V82Name, DatabaseState.ErrorCreate);
                    throw;
                }
            }
        }
    }
}
