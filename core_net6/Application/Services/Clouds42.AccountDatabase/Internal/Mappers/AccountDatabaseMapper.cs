﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.AccountDatabase.Internal.Mappers
{
    /// <summary>
    /// Маппер моделей информационных баз
    /// </summary>
    public static class AccountDatabaseMapper
    {
        /// <summary>
        /// Смапить доменную модель инф. базы к модели информации об инф. базе
        /// </summary>
        /// <param name="accountDatabase">Доменная модель инф. базы</param>
        /// <param name="userPrincipal">Авторизованный пользователь</param>
        /// <returns>Модель информации об инф. базе</returns>
        public static AccountDatabaseInfoDataDto MapToAccountDatabaseInfoDc(this Domain.DataModels.AccountDatabase accountDatabase, IUserPrincipalDto userPrincipal)
        {
            return new AccountDatabaseInfoDataDto
            {
                Id = accountDatabase.Id,
                V82Name = accountDatabase.V82Name,
                Caption = accountDatabase.Caption,
                Account = accountDatabase.Account,
                CreationDate = accountDatabase.CreationDate,
                LastActivityDate = accountDatabase.LastActivityDate,
                State = accountDatabase.State,
                IsFile = accountDatabase.IsFile,
                ApplicationName = accountDatabase.ApplicationName,
                SizeInMB = accountDatabase.SizeInMB,
                LockedState = accountDatabase.LockedState,
                DbNumber = accountDatabase.DbNumber,
                ZoneNumber = accountDatabase.AccountDatabaseOnDelimiter?.Zone,
                AbilityToEdit = CheckAccountDatabaseHelper.CanEditDatabase(userPrincipal, accountDatabase)
            };
        }
    }
}
