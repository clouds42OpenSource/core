﻿using Clouds42.AccountDatabase.Validators;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Internal
{
    public class InfoDatabaseDomainModelValidator
    {
        /// <summary>
        ///     проверка имён БД на корректность
        /// </summary>
        public void CreateDbFromTemplateValidation(IEnumerable<InfoDatabaseDomainModelDto> databases)
        {
            foreach (var database in databases)
            {
                var validateAccountDbCaptionResult = AccountDbCaptionValidator.Validate(database.DataBaseName);
                if (!validateAccountDbCaptionResult.Success)
                    throw new ValidateException(validateAccountDbCaptionResult.Message);
            }
        }

        /// <summary>
        ///     Проверить возможность создания базы на разделителе
        /// </summary>
        public void CheckCreatingAccountDatabaseOnDelimiter(IEnumerable<InfoDatabaseDomainModelDto> databases,
            IAccessProvider accessProvider, Account account)
        {
            var anyAccountUserAdmin = accessProvider.GetAccountAdmins(account.Id).Any();
            var anyAccountDatabaseOnDelimiters = databases.Any(w => w.DbTemplateDelimiters);

            if (!anyAccountUserAdmin && anyAccountDatabaseOnDelimiters)
                throw new ValidateException("У аккаунта нету ни одного аккаунт-админа. Невозможно создать базу.");
        }

    }
}