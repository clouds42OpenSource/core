﻿using Clouds42.Domain.Enums.Link42;

namespace Clouds42.AccountDatabase.Internal.Extensions
{
    public static class EnumsExtension
    {
        private static readonly Dictionary<TaskActionTypes, string> _actionCaptions = new()
        {
            {TaskActionTypes.DoNothing, ""},
            {TaskActionTypes.Create, nameof(Create)},
            {TaskActionTypes.Delete, nameof(Delete)},
            {TaskActionTypes.Edit, nameof(Edit)},
            {TaskActionTypes.Find, "Find"},
            {TaskActionTypes.Get, "Get"},
            {TaskActionTypes.Post, "Post"},
            {TaskActionTypes.Query, "Query"},
            {TaskActionTypes.GetID, "GetID"},
            {TaskActionTypes.Command, "Command"}
        };

        private static readonly Dictionary<OneSObjects, string> _objTypeCaptions = new()
        {
            {OneSObjects.String, "Строка"},
            {OneSObjects.Boolean, "Булево"},
            {OneSObjects.Catalog, "Справочник"},
            {OneSObjects.Date, "Дата"},
            {OneSObjects.Document, "Документ"},
            {OneSObjects.Enum, "Перечисление"},
            {OneSObjects.Number, "Число"},
            {OneSObjects.Undefined, "Неопределено"}
        };

        public static string GetObjCaption(OneSObjects obj)
        {
            return obj.ToString();
        }

        public static string GetActionCaption(TaskActionTypes obj)
        {
            return obj.ToString();
        }

        public static OneSObjects GetObjTypeByCaption(string caption)
        {
            return _objTypeCaptions.FirstOrDefault(c => c.Value == caption).Key;
        }

        public static TaskActionTypes GetActionTypeByCaption(string caption)
        {
            var result = _actionCaptions.FirstOrDefault(c => c.Value == caption).Key;
            return result;
        }
    }
}