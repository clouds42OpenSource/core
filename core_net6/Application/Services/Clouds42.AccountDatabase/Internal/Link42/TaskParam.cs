﻿using System.Xml.Linq;
using Clouds42.AccountDatabase.Internal.Extensions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums.Link42;

namespace Clouds42.AccountDatabase.Internal.Link42
{
    public class TaskParam
    {
        private const string ConstAction = "action";
        private const string ConstRoot = "Params";
        public TaskActionTypes ObjActionType { get; set; } = TaskActionTypes.DoNothing;
        public List<ParamDescription> InnerParams { get; } = [];

        public void AddInnerParam(ParamDescription paramDescr)
        {
            if (paramDescr == null) return;
            InnerParams.Add(paramDescr);
        }

        public static TaskParam CreateFromXml(XElement xml)
        {
            var result = new TaskParam { ObjActionType = EnumsExtension.GetActionTypeByCaption(xml.GetAttributeValue(ConstAction)) };
            xml.Elements().ToList().ForEach(x => result.InnerParams.Add(ParamDescription.CreateFromXml(x)));
            return result;
        }

        public void LoadFromXml(XElement xml)
        {
            InnerParams.Clear();
            ObjActionType = EnumsExtension.GetActionTypeByCaption(xml.GetAttributeValue(ConstAction));
            xml.Elements().ToList().ForEach(x => InnerParams.Add(ParamDescription.CreateFromXml(x)));
        }

        public XElement GetXml()
        {
            var result = new XElement(ConstRoot);
            result.Add(new XAttribute(ConstAction, EnumsExtension.GetActionCaption(ObjActionType)));
            InnerParams.ForEach(x => result.Add(x.GetXml()));
            return result;
        }
    }

    public class ParamDescription
    {
        private const string ConstObjType = "objType";
        private const string ConstRef = "ref";

        private static readonly Dictionary<OneSObjects, string> _objCaptions = new()
        {
            {OneSObjects.String, "Строка"},
            {OneSObjects.Boolean, "Булево"},
            {OneSObjects.Catalog, "Справочник"},
            {OneSObjects.Date, "Дата"},
            {OneSObjects.Document, "Документ"},
            {OneSObjects.Enum, "Перечисление"},
            {OneSObjects.Number, "Число"},
            {OneSObjects.Undefined, "Неопределено"}
        };

        public OneSObjects ObjType { get; set; } = OneSObjects.Undefined;
        public string Ref { get; set; } = string.Empty;
        public string Value { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public List<ParamDescription> InnerParams { get; } = [];

        public void AddInnerParam(ParamDescription paramDescr)
        {
            if (paramDescr == null) return;
            InnerParams.Add(paramDescr);
        }

        public bool HasChildren()
        {
            return InnerParams.Count > 0;
        }

        private static string GetAttributeValue(XElement node, string attrName)
        {
            return node.Attributes().Where(a => a.Name.ToString() == attrName).Select(v => v.Value).FirstOrDefault();
        }

        public XElement GetXml()
        {
            var result = new XElement(Name);
            if (ObjType != OneSObjects.Undefined)
            {
                result.Add(new XAttribute(ConstObjType, GetObjTypeName(ObjType)));
            }
            if (!string.IsNullOrEmpty(Ref))
            {
                result.Add(new XAttribute(ConstRef, Ref));
            }
            if (!string.IsNullOrEmpty(Value))
            {
                result.Value = CoverEscapes(Value);
            }
            if (HasChildren())
            {
                InnerParams.ForEach(x => result.Add(x.GetXml()));
            }
            return result;
        }

        public static ParamDescription CreateFromXml(XElement xml)
        {
            var result = new ParamDescription
            {
                ObjType = GetObjTypeValue(GetAttributeValue(xml, ConstObjType)),
                Ref = GetAttributeValue(xml, ConstRef),
                Name = xml.Name.ToString(),
                Value = !xml.HasElements ? RestoreEscapes(xml.Value) : string.Empty
            };

            if (xml.HasElements)
                xml.Elements().ToList().ForEach(x => result.InnerParams.Add(CreateFromXml(x)));
            return result;
        }

        private static string GetObjTypeName(OneSObjects objType)
        {
            var caption = _objCaptions.Where(x => x.Key == objType).Select(n => n.Value).FirstOrDefault();
            return caption ?? string.Empty;
        }

        private static OneSObjects GetObjTypeValue(string objType)
        {
            return _objCaptions.FirstOrDefault(c => c.Value == objType).Key;
        }

        private static string RestoreEscapes(string input)
        {
            return input
                .Replace("&quot", "\"")
                .Replace("&apos;", "'")
                .Replace("&lt;", "<")
                .Replace("&gt;", ">")
                .Replace("&amp;", "&")
                .Replace("&amp;amp;", "&");
        }

        private static string CoverEscapes(string input)
        {
            return input
                .Replace("\"", "&quot")
                .Replace("'", "&apos;")
                .Replace("<", "&lt;")
                .Replace(">", "&gt;")
                .Replace("&", "&amp;");
        }
    }
}
