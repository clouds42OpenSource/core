﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Validators;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts;
using Clouds42.StateMachine.Contracts.ServiceManagerProcessFlows;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Internal
{
    /// <summary>
    /// Хелпер для редактирования инф. базы
    /// </summary>
    internal class DatabaseEditHelper(
        IUnitOfWork dbLayer,
        AccountDatabasePublishManager accountDatabasePublishManager,
        IAccountDatabaseUserAccessManager accountDatabaseUserAccessManager,
        IUpdateAccountDatabaseCaptionProcessFlow updateAccountDatabaseCaptionProcessFlow,
        IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        ICreateMigrationAccountDatabaseTaskHelper createMigrationAccountDatabaseTaskHelper,
        SynchronizeAccountDatabaseWithTardisHelper synchronizeAccountDatabaseWithTardisHelper,
        IOnChangeDatabaseTypeTrigger onChangeDatabaseTypeTrigger,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        IOnChangeDatabaseTemplateTrigger onChangeDatabaseTemplateTrigger,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IDatabaseEditHelper
    {
        /// <summary>
        ///     Метод смены платформы и дистрибутива,
        /// возвращает строку с записью что именно изменено
        /// </summary>
        /// <param name="accountDatabase">База</param>
        /// <param name="platformType">Платформа (8.2 или 8.3)</param>
        /// <param name="distributionType">Дистрибутив (Stable или Alpha)</param>
        public string ChangeDbPlatformAndGetLogMessage(Domain.DataModels.AccountDatabase accountDatabase, PlatformType platformType,
            DistributionType distributionType)
        {
            if (accountDatabase.IsFile == false)
                throw new InvalidOperationException(
                    $"Информационная база {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не файловая, процесс смены платформы невозможен!");

            if (accountDatabase.StateEnum == DatabaseState.DetachedToTomb)
                throw new InvalidOperationException(
                    $"Информационная база {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} находится в архиве, процесс смены платформы невозможен!");

            var logMessage = new StringBuilder();
            var needToRepublish = NeedToRepublishDb(accountDatabase, platformType, distributionType);

            if (accountDatabase.PlatformType != platformType)
                logMessage.AppendLine(
                    $"Смена версии платформы для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} с \"{accountDatabase.PlatformType.GetDisplayName()}\" на \"{platformType.GetDisplayName()}\". ");

            if (accountDatabase.DistributionTypeEnum != distributionType)
                logMessage.AppendLine(
                    $"Смена дистрибутива платформы для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} с \"{accountDatabase.DistributionTypeEnum.GetDisplayName()}\" на \"{distributionType.GetDisplayName()}\"");

            accountDatabase.ApplicationName = platformType.GetDisplayName();
            accountDatabase.DistributionTypeEnum = distributionType;

            try
            {
                dbLayer.DatabasesRepository.Update(accountDatabase);
                dbLayer.Save();
            }
            catch (DbUpdateException exception)
            {
                throw new InvalidOperationException("Произошла ошибка при сохранении Информ. базы", exception);
            }

            if (needToRepublish)
                accountDatabasePublishManager.RepublishWithChangingConfig(accountDatabase.Id);

            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(accountDatabase.Id);

            return logMessage.ToString();
        }

        /// <summary>
        /// Метод смены типа информационной базы,для серверных баз проставляет платформу Stable,
        /// опубликованные базы переопубликовываются, возвращает значение типа ИБ
        /// </summary>
        /// <param name="accountDatabase">База</param>
        /// <param name="changeAccountDatabaseTypeDto"></param>
        public bool ChangeAccountDatabaseType(Domain.DataModels.AccountDatabase accountDatabase,
            ChangeAccountDatabaseTypeDto changeAccountDatabaseTypeDto)
        {
            var isFileAcDbType = accountDatabase.IsFile ?? false;
            var invertedAcDbType = !isFileAcDbType;

            using (var transaction = dbLayer.SmartTransaction.Get())
            {
                try
                {
                    if (isFileAcDbType && accountDatabase.DistributionTypeEnum != DistributionType.Stable)
                        accountDatabase.DistributionTypeEnum = DistributionType.Stable;

                    if (accountDatabase.PublishStateEnum == PublishState.Published)
                        accountDatabasePublishManager.RepublishDatabaseWithoutWaiting(accountDatabase.Id);

                    accountDatabase.IsFile = invertedAcDbType;

                    dbLayer.DatabasesRepository.Update(accountDatabase);
                    dbLayer.Save();

                    if (!invertedAcDbType)
                        UpdateAccountDatabaseRestoreType(accountDatabase, changeAccountDatabaseTypeDto.RestoreModelType);

                    onChangeDatabaseTypeTrigger.Execute(accountDatabase.Id);

                    transaction.Commit();
                }
                catch (DbUpdateException ex)
                {
                    var errorMessage =
                        $"[Ошибка изменения типа инф. базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}";

                    handlerException.Handle(ex, errorMessage);
                    transaction.Rollback();
                    throw new InvalidOperationException(errorMessage, ex);
                }
            }

            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(accountDatabase.Id);
            return invertedAcDbType;
        }

        /// <summary>
        /// Редактирование карточки аккаунта с админ-панели
        /// </summary>
        /// <param name="accountDatabaseDm">Модель данных для карточки базы</param>
        /// <param name="accountId">ID аккаунта</param>
        public void EditAccountDatabase(AccountDatabaseCartDomainModel accountDatabaseDm, Guid accountId)
        {
            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == accountDatabaseDm.Infos.Id)
                                  ?? throw new NotFoundException(
                                      $"Не удалось получить инфо базу по индентификатору: '{accountDatabaseDm.Infos.Id}'");

            ChangeAccountDatabaseFileStorage(accountDatabaseDm, accountDatabase);

            var needSyncronizeWithTardis = NeedSyncronizeWithTardis(accountDatabaseDm, accountDatabase);

            accountDatabase.Caption = string.IsNullOrWhiteSpace(accountDatabaseDm.Infos.Caption)
                ? $"Моя база №{accountDatabase.DbNumber}"
                : accountDatabaseDm.Infos.Caption;

            accountDatabaseChangeStateProvider.ChangeState(accountDatabase.Id, accountDatabaseDm.Infos.DatabaseState);
            logger.Trace(
                $"Изменили статус базы с {accountDatabase.StateEnum} на {accountDatabaseDm.Infos.DatabaseState}");

            accountDatabase.V82Name = accountDatabaseDm.Infos.V82Name;

            var needToRepublish = NeedToRepublishDb(accountDatabase, accountDatabaseDm.Infos.PlatformType,
                accountDatabaseDm.Infos.DistributionType);

            accountDatabase.ApplicationName = accountDatabaseDm.Infos.PlatformType.GetDisplayName();
            accountDatabase.DistributionTypeEnum = accountDatabaseDm.Infos.DistributionType;
            accountDatabase.UsedWebServices = accountDatabaseDm.Infos.UsedWebServices;

            var isTemplateChanged = TryChangeTemplateIdForDatabase(accountDatabase, accountDatabaseDm.Infos.DbTemplate);

            dbLayer.DatabasesRepository.Update(accountDatabase);
            dbLayer.Save();

            if (needSyncronizeWithTardis)
                synchronizeAccountDatabaseWithTardisHelper.SynchronizeAccountDatabaseViaNewThread(accountDatabase.Id);

            if (needToRepublish)
                accountDatabasePublishManager.RepublishWithChangingConfig(accountDatabaseDm.Infos.Id);

            if (isTemplateChanged)
                onChangeDatabaseTemplateTrigger.Execute(new OnChangeDatabaseTemplateTriggerDto
                {
                    DatabaseId = accountDatabase.Id
                });
        }

        /// <summary>
        /// Предоставление доступов внутренним пользователям
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        public void GrantInternalAccess(Guid databaseId, Guid? accountUserId)
        {
            if (!accountUserId.HasValue)
                return;
            try
            {
                accountDatabaseUserAccessManager.GrandInternalAccessToDb(databaseId, accountUserId.Value);
            }
            catch (Exception ex)
            {
                logger.Trace(
                    $"Error while grant access to my user {accountUserId} for db {databaseId},. Exception message: {ex.Message} ");
                throw;
            }
        }

        /// <summary>
        /// Предоставления доступов внешним пользовователям
        /// </summary>
        /// <param name="result"></param>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        public void GrantExternalAccess(SaveResult result, Guid databaseId, Guid accountUserId)
        {
            try
            {
                var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(a => a.Id == accountUserId);
                if (accountUser == null)
                    return;

                accountDatabaseUserAccessManager.GrandExternalAccessToDb(databaseId, accountUser.Email);
                result.GrandAccessUserIds.Add(accountUserId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка предоставления доступа внешнему пользователю]");
                throw;
            }
        }

        /// <summary>
        /// Проверка на необходимость переопубликации информационной базы
        /// </summary>
        /// <param name="database"> информационная база</param>
        /// <param name="platform">платформа</param>
        /// <param name="distributionType">релиз платформы</param>
        public bool NeedToRepublishDb(Domain.DataModels.AccountDatabase database, PlatformType platform,
            DistributionType distributionType)
        {
            if (database.PublishStateEnum == PublishState.Unpublished)
                return false;

            if (database.PlatformType == platform &&
                database.DistributionTypeEnum == distributionType)
                return false;

            if (database.AccountDatabaseOnDelimiter != null)
                return false;

            logger.Trace(
                $"Необходима переопубликация базы: {database.PublishStateEnum}, платформа с {database.PlatformType} на {platform}, релиз с {database.DistributionTypeEnum} на {distributionType}");
            return true;
        }

        /// <summary>
        /// Сменить название инф. базы
        /// </summary>
        /// <param name="model">Модель изменения названия</param>
        public void ChangeAccountDatabaseCaption(PostRequestAccDbCaptionDto model)
        {
            try
            {
                ValidateAccountDatabaseModel(model);

                updateAccountDatabaseCaptionProcessFlow.Run(new UpdateAccountDatabaseCaptionParamsDto
                {
                    AccountDatabaseId = model.AccountDatabaseID,
                    Caption = model.Caption.Trim()
                });
            }
            catch(ValidateException ex)
            {
                logger.Warn(ex,
                    $"Ошибка валидации имени ИБ {model.Caption}");
                throw;
            }
            catch (Exception ex)
            {
                logger.Warn(ex, 
                    $"Ошибка смены названия инф. базы {model.AccountDatabaseID} на {model.Caption}");
                throw;
            }
        }

        /// <summary>
        /// Изменить номер базы
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <param name="v82Name">Номер информационной базы</param>
        /// <returns></returns>
        public void ChangeDbV82Name(Domain.DataModels.AccountDatabase database, string v82Name)
        {
            var validateResult = AccountDbV82NameValidator.Validate(v82Name);
            if (!validateResult.Success)
                throw new InvalidOperationException(validateResult.Message);

            database.V82Name = v82Name.Trim();
            dbLayer.DatabasesRepository.InsertOrUpdateAccountDatabaseValue(database);

            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(database.Id);
        }

        /// <summary>
        /// Сменить тип модели восстановления для инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        public void ChangeDatabaseRestoreModelType(Domain.DataModels.AccountDatabase accountDatabase,
            AccountDatabaseRestoreModelTypeEnum restoreModelType)
        {
            UpdateAccountDatabaseRestoreType(accountDatabase, restoreModelType);
        }

        /// <summary>
        /// Сменить признак наличия доработок для инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="hasModifications">Признак наличия доработок</param>
        public void ChangeDatabaseHasModificationsFlag(Domain.DataModels.AccountDatabase accountDatabase, bool hasModifications)
        {
            var acDbSupport = accountDatabase.AcDbSupport;

            acDbSupport.DatabaseHasModifications = hasModifications;
            if (hasModifications)
            {
                acDbSupport.HasAutoUpdate = false;
                acDbSupport.PrepearedForUpdate = false;
            }

            dbLayer.AcDbSupportRepository.Update(acDbSupport);
            dbLayer.Save();
        }

        /// <summary>
        /// Проверить модель инф. базы
        /// </summary>
        /// <param name="model">Модель изменения названия</param>
        private void ValidateAccountDatabaseModel(PostRequestAccDbCaptionDto model)
        {
            if (model == null)
                throw new ValidateException("Модель изменения названия инф. базы пуста");

            if (!model.IsValid)
                throw new ValidateException("Модель изменения названия инф. базы не валидна");

            var accountDatabase = dbLayer.DatabasesRepository.GetAccountDatabase(model.AccountDatabaseID);

            if (accountDatabase == null)
                throw new NotFoundException($"Инф. база по ID {model.AccountDatabaseID} не найдена");

            var validateResult = AccountDbCaptionValidator.Validate(model.Caption);
            if (!validateResult.Success)
                throw new ValidateException(validateResult.Message);
        }

        /// <summary>
        /// Сменить файловое хранилище для инф. базы
        /// </summary>
        /// <param name="accountDatabaseDm">Модель данных для карточки базы</param>
        /// <param name="accountDatabase">Модель инф. базы</param>
        private void ChangeAccountDatabaseFileStorage(AccountDatabaseCartDomainModel accountDatabaseDm,
            Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.FileStorageID == accountDatabaseDm.Infos.FileStorage || accountDatabase.IsFile != true)
                return;

            if (!accountDatabaseDm.Infos.FileStorage.HasValue)
                throw new InvalidOperationException(
                    $"Не выбрано хранилище для информационной базы: '{accountDatabaseDm.Infos.Id}'");

            createMigrationAccountDatabaseTaskHelper.CreateTask(accountDatabase.Id,
                accountDatabaseDm.Infos.FileStorage.Value);
        }

        /// <summary>
        /// Изменить модель восстановления инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        private void UpdateAccountDatabaseRestoreType(Domain.DataModels.AccountDatabase accountDatabase,
            AccountDatabaseRestoreModelTypeEnum? restoreModelType)
        {
            var cloudServicesSqlServer = accountConfigurationDataProvider.GetAccountSegment(accountDatabase.AccountId)
                .CloudServicesSQLServer;

            if (!CanChangeRestoreModelType(cloudServicesSqlServer) || restoreModelType == null)
                restoreModelType = cloudServicesSqlServer.RestoreModelType;

            accountDatabase.RestoreModelType = restoreModelType;

            dbLayer.DatabasesRepository.Update(accountDatabase);
            dbLayer.Save();
        }

        /// <summary>
        /// Признак что можно изменить модель восстановления
        /// </summary>
        /// <param name="servicesSqlServer">Sql сервер облака</param>
        /// <returns>true - если на SQL сервере установлен смешанный режим восстановления</returns>
        private static bool CanChangeRestoreModelType(CloudServicesSqlServer servicesSqlServer)
            => servicesSqlServer.RestoreModelType == AccountDatabaseRestoreModelTypeEnum.Mixed;

        /// <summary>
        /// Признак необходимости синхронизировать инф. базу с Тардис
        /// </summary>
        /// <param name="accountDatabaseDm">Модель данных инф. базы</param>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>true - если изменено название или статус инф. базы</returns>
        private bool NeedSyncronizeWithTardis(AccountDatabaseCartDomainModel accountDatabaseDm,
            Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (!accountDatabaseDm.Infos.Caption.Equals(accountDatabase.Caption))
                return true;

            return accountDatabaseDm.Infos.DatabaseState != accountDatabase.StateEnum;
        }

        /// <summary>
        /// Попытаться изменить Id шаблона для инф. базы
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <param name="dbTemplateId">Id шаблона базы</param>
        /// <returns>Признак, указывающий на изменение шаблона</returns>
        private bool TryChangeTemplateIdForDatabase(Domain.DataModels.AccountDatabase database, string dbTemplateId)
        {
            if (string.IsNullOrEmpty(dbTemplateId) || !Guid.TryParse(dbTemplateId, out var dbTemplateGuid))
                return false;

            var dbTemplate = dbLayer.DbTemplateRepository.FirstOrDefault(db => db.Id == dbTemplateGuid);
            if (dbTemplate == null)
                return false;

            database.TemplateId = dbTemplate.Id;

            return true;
        }
    }
}
