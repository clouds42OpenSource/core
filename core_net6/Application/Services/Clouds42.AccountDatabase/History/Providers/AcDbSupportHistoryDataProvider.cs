﻿using Clouds42.AccountDatabase.Contracts.History.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.History.Providers
{
    /// <summary>
    /// Провайдер для работы с данными
    /// истории технической поддержки инф. базы
    /// </summary>
    internal class AcDbSupportHistoryDataProvider(IUnitOfWork dbLayer) : IAcDbSupportHistoryDataProvider
    {
        /// <summary>
        /// Получить актуальную историю технической
        /// поддержки инф. базы по каждой операции
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <returns>Актуальная история технической
        /// поддержки инф. базы по каждой операции</returns>
        public IQueryable<AcDbSupportHistory> GetActualAcDbSupportHistories(Guid databaseId)
        {
            return dbLayer.AcDbSupportHistoryRepository
                .AsQueryable()
                .Where(x => x.AccountDatabaseId == databaseId && x.Operation.HasValue)
                .GroupBy(x => x.Operation.Value)
                .Select(groupHistories => groupHistories.OrderByDescending(x => x.EditDate).FirstOrDefault());
        }
    }
}
