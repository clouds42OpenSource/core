﻿using Clouds42.AccountDatabase.Contracts.UploadZipAccount.Interfaces;
using Clouds42.AccountDatabase.Create.Helpers;
using Clouds42.AccountDatabase.UploadZipAccount.Helpers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.UploadZipAccount.Providers
{
    /// <summary>
    /// Провайдер для загрузки ZIP файлов инф. базы
    /// </summary>
    public class UploadAccountDatabaseZipFileProvider(
        IUnitOfWork dbLayer,
        UploadingFilePathHelper uploadingFilePathHelper,
        ZipFileChunksHelper zipFileChunksHelper,
        IUploadFileCommand uploadFileCommand,
        IHandlerException handlerException)
        : IUploadAccountDatabaseZipFileProvider
    {
        /// <summary>
        /// Загрузить ZIP файл по частям
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>ID загруженного файла от МС</returns>
        public Guid UploadZipFileByChunks(Guid uploadedFileId)
        {
            var uploadedFile = GetUploadedFile(uploadedFileId);
            var folderForChunksPath = uploadingFilePathHelper.GetPathForUploadingFileChunks(uploadedFile);

            try
            {
                var uploadedFileData =
                    zipFileChunksHelper.GetUploadedFileDataDc(folderForChunksPath, uploadedFile.FileName);
                if (!uploadedFileData.SortedFilesDc.Any())
                    throw new InvalidOperationException(
                        $"Не удалось найти части файла загружаемого файла {uploadedFile.Id}-{uploadedFile.FileName}");

                return UploadZipFileChunksThroughHttp(uploadedFileData, uploadedFile.FileName);
            }
            finally
            {
                zipFileChunksHelper.RemoveFileChunks(folderForChunksPath, uploadedFile.FileName);
            }
        }

        /// <summary>
        /// Загрузить части ZIP файла
        /// </summary>
        /// <param name="uploadedFileData">Модель загружаемого файла</param>
        /// <param name="uploadedFileName">Имя загруженного файла</param>
        /// <returns>ID загружаемого файла</returns>
        private Guid UploadZipFileChunksThroughHttp(UploadedFileDataDto uploadedFileData, string uploadedFileName)
        {
            var uploadUrl = uploadFileCommand.GetUploadedFileId(uploadedFileName);
            long startPoint = 0;
            var uploadedFileId = Guid.Empty;

            uploadedFileData.SortedFilesDc.ForEach(sortedFile =>
            {
                try
                {
                    var fileContent = File.ReadAllBytes(sortedFile.FileName);
                    var contentLength = fileContent.Length;

                    uploadedFileId = uploadFileCommand.UploadChunkFileBytes(
                        FileHelpers.GetRequestHeadersForFile(startPoint, contentLength,uploadedFileData.ContentLength), 
                        fileContent, uploadUrl);

                    startPoint += contentLength;
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex,
                        $"[Ошибка загрузки части файла в МС] №{sortedFile.FileOrder} {sortedFile.FileName}");
                    throw;
                }
            });
            uploadFileCommand.StopUploadedFile(uploadedFileName);
            if (uploadedFileId.IsNullOrEmpty())
                throw new InvalidOperationException(
                    $"Не удалось получить ID загруженного файла от МС для {uploadedFileName}");

            return uploadedFileId;
        }

        /// <summary>
        /// Получить загруженный файл
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Загруженный файл</returns>
        private UploadedFile GetUploadedFile(Guid uploadedFileId)
            => dbLayer.UploadedFileRepository.FirstOrThrowException(w => w.Id == uploadedFileId,
                $"Загружаемый файл по ID {uploadedFileId} не найден");
    }
}
