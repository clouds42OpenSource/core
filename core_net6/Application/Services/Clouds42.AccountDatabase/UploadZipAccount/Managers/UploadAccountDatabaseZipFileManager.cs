﻿using Clouds42.AccountDatabase.Contracts.UploadZipAccount.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.UploadZipAccount.Managers
{
    /// <summary>
    /// Менеджер для загрузки ZIP файлов инф. базы
    /// </summary>
    public class UploadAccountDatabaseZipFileManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IUploadAccountDatabaseZipFileProvider uploadAccountDatabaseZipFileProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IUploadAccountDatabaseZipFileManager
    {
        /// <summary>
        /// Загрузить ZIP файл по частям
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>ID загруженного файла от МС</returns>
        public ManagerResult<Guid> UploadZipFileByChunks(Guid uploadedFileId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_UploadZipFileToSm);

                var smUploadedFileId = uploadAccountDatabaseZipFileProvider.UploadZipFileByChunks(uploadedFileId);

                return Ok(smUploadedFileId);
            }
            catch (Exception ex)
            {
                Logger.Warn(ex, $"[Ошибка загрузки файла {uploadedFileId} инф. базы в МС]");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }
    }
}
