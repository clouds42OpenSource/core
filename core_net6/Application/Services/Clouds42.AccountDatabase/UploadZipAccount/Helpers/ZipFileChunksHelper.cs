﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.AccountDatabase.UploadZipAccount.Helpers
{
    /// <summary>
    /// Хэлпер для работы с частями ZIP файла
    /// </summary>
    public class ZipFileChunksHelper
    {
        private readonly Lazy<string> _partToken = new(
            CloudConfigurationProvider.AccountDatabase.UploadFiles.GetPartTokenNameForChunkOfFile);

        /// <summary>
        /// Получить модель загружаемого файла
        /// </summary>
        /// <param name="folderForChunksPath">Путь к частям файла</param>
        /// <param name="uploadedFileName">Имя загруженного файла</param>
        /// <returns>Модель загружаемого файла</returns>
        public UploadedFileDataDto GetUploadedFileDataDc(string folderForChunksPath, string uploadedFileName)
        {
            var fileChunksOrderedList = GetFileChunksOrderedList(folderForChunksPath, uploadedFileName);
            return new UploadedFileDataDto
            {
                SortedFilesDc = fileChunksOrderedList,
                ContentLength = GetZipFileChunksContentLength(fileChunksOrderedList)
            };
        }

        /// <summary>
        /// Удалить части файла
        /// </summary>
        /// <param name="folderForChunksPath">Путь к частям файла</param>
        /// <param name="uploadedFileName">Имя загруженного файла</param>
        public void RemoveFileChunks(string folderForChunksPath, string uploadedFileName)
        {
            var searchPattern = Path.GetFileName(uploadedFileName) + _partToken.Value + "*";
            RemoveFilesFromFolder(folderForChunksPath, searchPattern);
        }

        /// <summary>
        /// Удалить файлы из директории
        /// </summary>
        /// <param name="folderPath">Путь к директории</param>
        /// <param name="searchPattern">Маска поиска файлов</param>
        private static void RemoveFilesFromFolder(string folderPath, string searchPattern)
        {
            var fileChunks = Directory.GetFiles(folderPath, searchPattern);

            foreach (var fileChunk in fileChunks)
            {
                if (File.Exists(fileChunk))
                    File.Delete(fileChunk);
            }
        }

        /// <summary>
        /// Получить отсортированный список частей
        /// </summary>
        /// <param name="folderForChunksPath">Путь к частям файла</param>
        /// <param name="uploadedFileName">Имя загруженного файла</param>
        /// <returns>Отсортированный список частей</returns>
        private List<AccountDatabaseSortedFileDto> GetFileChunksOrderedList(string folderForChunksPath, string uploadedFileName)
        {
            var filesList = GetFileChunksList(folderForChunksPath, uploadedFileName);
            var mergeList = new List<AccountDatabaseSortedFileDto>();

            foreach (var file in filesList)
            {
                var sortedFile = new AccountDatabaseSortedFileDto
                {
                    FileName = file
                };

                var trailingTokens = GetTrailingTokens(file);

                int.TryParse(trailingTokens[..trailingTokens.IndexOf(".", StringComparison.Ordinal)], out var fileIndex);
                sortedFile.FileOrder = fileIndex;
                mergeList.Add(sortedFile);
            }

            return mergeList.OrderBy(file => file.FileOrder).ToList();
        }

        /// <summary>
        /// Получить длину содержимого частей файла
        /// </summary>
        /// <param name="zipFileChunksOrderedList">Список частей файла</param>
        /// <returns>Длина содержимого частей файла</returns>
        private static long GetZipFileChunksContentLength(List<AccountDatabaseSortedFileDto> zipFileChunksOrderedList)
        {
            long contentLegth = 0;

            zipFileChunksOrderedList.ForEach(sortedFile =>
            {
                var content = File.ReadAllBytes(sortedFile.FileName);
                contentLegth += content.Length;
            });

            return contentLegth;
        }

        /// <summary>
        /// Получить список частей файла
        /// </summary>
        /// <param name="folderForChunksPath">Путь к частям файла</param>
        /// <param name="uploadedFileName">Имя загруженного файла</param>
        /// <returns>Список частей файла</returns>
        private IEnumerable<string> GetFileChunksList(string folderForChunksPath, string uploadedFileName)
        {
            var searchPattern = uploadedFileName + _partToken.Value + "*";

            return Directory.GetFiles(folderForChunksPath, searchPattern).ToList();
        }

        /// <summary>
        /// Получить название конченого токена
        /// </summary>
        /// <param name="initialName">Исходное имя</param>
        /// <returns>Название конченого токена</returns>
        private string GetTrailingTokens(string initialName)
            => initialName[(initialName.IndexOf(_partToken.Value, StringComparison.Ordinal) + _partToken.Value.Length)..];
    }
}
