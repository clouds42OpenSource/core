﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;


namespace Clouds42.AccountDatabase.DbTemplates.Helpers
{
    /// <summary>
    /// Класс для выбора шаблонов
    /// </summary>
    internal sealed class DbTemplatesSelector(IUnitOfWork dbLayer, DbTemplatesFilterParamsDto filter)
    {
        /// <summary>
        /// Параметры фильтрации записей шаблонов
        /// </summary>
        private readonly DbTemplatesFilterParamsDto _filter = filter;

        /// <summary>
        /// Дата контекст
        /// </summary>
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Отфильтровать записи по названию шаблона
        /// </summary>
        /// <param name="records">Записи для фильтрации</param>
        /// <returns>Отфильтрованные записи</returns>
        private IQueryable<DbTemplate> FilterByDbTemplateName(IQueryable<DbTemplate> records)
            => records.Where(rec =>
                _filter.DbTemplateName == null ||
                rec.Name != null && rec.Name.Contains(_filter.DbTemplateName));


        /// <summary>
        /// Отфильтровать записи по названию шаблона
        /// </summary>
        /// <param name="records">Записи для фильтрации</param>
        /// <returns>Отфильтрованные записи</returns>
        private IQueryable<DbTemplate> FilterByDbTemplateDescription(IQueryable<DbTemplate> records)
            => records.Where(rec =>
                _filter.DbTemplateDescription == null ||
                rec.DefaultCaption != null && rec.DefaultCaption.Contains(_filter.DbTemplateDescription));


        /// <summary>
        /// Получить отфильтрованный список шаблонов
        /// </summary>
        /// <returns>Отфильтрованный список шаблонов</returns>
        public IQueryable<DbTemplate> SelectWithFilters()
        {
            return _filter == null
            ? _dbLayer.DbTemplateRepository.WhereLazy()
            : _dbLayer.DbTemplateRepository.WhereLazy().ComposeFilters(
                FilterByDbTemplateName,
                FilterByDbTemplateDescription);
        }
    }
}