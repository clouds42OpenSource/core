﻿using Clouds42.Common;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.DbTemplates.Helpers
{
    /// <summary>
    /// Утилита для сортиовки записей шаблонов
    /// </summary>
    public static class DbTemplatesSortingUtility
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateQueryable<DbTemplate>> SortingActions = new()
        {
            { DbTemplatesSortFieldNames.DbTemplateName, SortByDbTemplateName },
            { DbTemplatesSortFieldNames.DbTemplateDescription, SortByDbTemplateDefaultCaption }
        };

        /// <summary>
        /// Сортитровать записи шаблонов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи шаблонов по названию</returns>
        private static IOrderedQueryable<DbTemplate> SortByDefault(IQueryable<DbTemplate> records)
            => records.OrderByDescending(row => row.Name);


        /// <summary>
        /// Сортитровать выбранные записи шаблонов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей шаблонов по полю Name</returns>
        private static IOrderedQueryable<DbTemplate> SortByDbTemplateName(IQueryable<DbTemplate> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Name),
                SortType.Desc => records.OrderByDescending(row => row.Name),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать выбранные записи шаблонов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей шаблонов по полю DefaultCaption</returns>
        private static IOrderedQueryable<DbTemplate> SortByDbTemplateDefaultCaption(IQueryable<DbTemplate> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.DefaultCaption),
                SortType.Desc => records.OrderByDescending(row => row.DefaultCaption),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Производит сортировку выбранных записей шаблонов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи шаблонов</returns>
        public static IOrderedQueryable<DbTemplate> MakeSorting(IQueryable<DbTemplate> records, SortingDataDto sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();

            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate)
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }
    }
}