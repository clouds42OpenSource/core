﻿namespace Clouds42.AccountDatabase.DbTemplates.Helpers
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки шаблонов
    /// </summary>
    public static class DbTemplatesSortFieldNames
    {
        /// <summary>
        ///  Название шаблона
        /// </summary>
        public const string DbTemplateName = "dbtemplatename";

        /// <summary>
        ///  Описание шаблона
        /// </summary>
        public const string DbTemplateDescription = "dbtemplatedescription";
    }
}