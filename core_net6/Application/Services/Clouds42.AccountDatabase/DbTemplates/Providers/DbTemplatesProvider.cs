﻿using Clouds42.AccountDatabase.Contracts.DbTemplates.Interfaces;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.DataContracts.BaseModel;
using PagedList.Core;
using Clouds42.AccountDatabase.DbTemplates.Helpers;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.DbTemplates.Providers
{
    /// <summary>
    /// Провайдер для работы с шаблонами
    /// </summary>
    public sealed class DbTemplatesProvider(IUnitOfWork dbLayer) : IDbTemplatesProvider
    {
        /// <summary>
        /// DB контекст
        /// </summary>
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Получить все шаблоны ввиде key - ID шаблона, value - описание шаблона
        /// </summary>
        /// <returns>Все шаблоны ввиде key - ID шаблона, value - описание шаблона</returns>
        public KeyValuePair<Guid, string>[] GetAllDbTemplatesAsKeyValue() => GetAllDbTemplates()
            .ToDictionary(t => t.Id, t => t.DefaultCaption)
            .ToArray();

        /// <summary>
        /// Получить все шаблоны инф. базы
        /// </summary>
        /// <returns>Все шаблоны инф. базы</returns>
        public IOrderedQueryable<DbTemplate> GetAllDbTemplates() => _dbLayer.DbTemplateRepository
            .WhereLazy()
            .OrderBy(item => item.DefaultCaption);

        /// <summary>
        /// Получить данные для редактирования шаблона
        /// </summary>
        /// <param name="args">Аргументы поиска шаблона для редактирования</param>
        /// <returns>Данные для редактирования шаблона</returns>
        public DbTemplateToEditDataDto GetDbTemplateDataToEdit(DbTemplateEditDto args)
        {
            var dbTemplateToEdit = GetDbTemplateItemToEdit(args.DbTemplateId);
            return new DbTemplateToEditDataDto
            {
                DbTemplateToEdit = dbTemplateToEdit
            };
        }

        /// <summary>
        /// Получить элемент шаблона для редактирования
        /// </summary>
        /// <param name="dbTemplateId">Для какого шаблона получить данные</param>
        /// <returns>Данные шаблона для редактирования</returns>
        private DbTemplateItemDto GetDbTemplateItemToEdit(Guid? dbTemplateId)
        {
            if (dbTemplateId == null || Guid.Empty.Equals(dbTemplateId))
            {
                return new DbTemplateItemDto
                {
                    Id = Guid.Empty,
                    Name = "",
                    LocaleId = null,
                    Platform = PlatformType.Undefined,
                    Order = 0,
                    DefaultCaption = "",
                    DemoTemplateId = null,
                    CanWebPublish = false,
                    NeedUpdate = false,
                    AdminLogin = "",
                    AdminPassword = ""
                };
            }

            var foundTemplate = _dbLayer.DbTemplateRepository.GetById(dbTemplateId);
            if (foundTemplate == null)
            {
                throw new NotFoundException($"Шаблон с ID:{dbTemplateId} не найден.");
            }

            return CreateDbTemplateItemFrom(foundTemplate, GetConfiguration1CName(foundTemplate.Id));
        }

        /// <summary>
        /// Удалить шаблон
        /// </summary>
        /// <param name="args">Данные для удаления шаблона</param>
        public void DeleteDbTemplateItem(DeleteDbTemplateDataItemDto args)
        {
            var dbTemplate = _dbLayer.DbTemplateRepository.FirstOrDefault(t => t.Id == args.DbTemplateId);
            if (dbTemplate == null)
                throw new NotFoundException(
                    $"Не удалось получить значение справочника шаблонов по номеру {args.DbTemplateId} для удаления.");

            try
            {
                DeleteConfigurationDbTemplateRelations(dbTemplate.Id);
                DeleteUpdateFolder(dbTemplate.DefaultCaption.Replace(":", "_"));

                _dbLayer.DbTemplateRepository.Delete(dbTemplate);
                _dbLayer.Save();

            }
            catch (Exception ex)
            {
                var message = $"Во время удаления шаблона произошла ошибка : {ex.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Получить список названий конфигураций
        /// </summary>
        /// <returns>список названий конфигураций</returns>
        public List<string> GetConfiguration1CName()=> _dbLayer.Configurations1CRepository.Select(c=>c.Name).ToList();


        /// <summary>
        /// Добавить шаблон
        /// </summary>
        /// <param name="itemToAdd">Данные для добавления шаблона</param>
        /// <returns>Данные после добавления</returns>
        public AddDbTemplateResultDto AddDbTemplateItem(DbTemplateItemDto itemToAdd)
        {
            var dbTemplate = new DbTemplate
            {
                Id = Guid.NewGuid(),
                Name = itemToAdd.Name,
                AdminLogin = itemToAdd.AdminLogin,
                AdminPassword = itemToAdd.AdminPassword,
                CanWebPublish = itemToAdd.CanWebPublish,
                DefaultCaption = itemToAdd.DefaultCaption,
                DemoTemplateId = Guid.Empty.Equals(itemToAdd.DemoTemplateId) ? null : itemToAdd.DemoTemplateId,
                LocaleId = Guid.Empty.Equals(itemToAdd.LocaleId) ? null : itemToAdd.LocaleId,
                NeedUpdate = itemToAdd.NeedUpdate,
                Order = itemToAdd.Order,
                Platform = itemToAdd.Platform == PlatformType.Undefined ? "" : itemToAdd.Platform.ToString()
            };

            try
            {
                _dbLayer.DbTemplateRepository.Insert(dbTemplate);
                _dbLayer.Save();

                CreateConfigurationDbTemplateRelations(dbTemplate.Id, itemToAdd.Configuration1CName);
                CreateOrUpdateFolderForUpdateTemplate(itemToAdd.DefaultCaption.Replace(":", "_"));

                return new AddDbTemplateResultDto
                {
                    DbTemplateId = dbTemplate.Id
                };
            }
            catch (Exception ex)
            {
                var message = $"Во время добавления шаблона произошла ошибка : {ex.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Создать зависимость шаблона и конфигурации 1С
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона</param>
        private void CreateConfigurationDbTemplateRelations(Guid dbTemplateId, string configuration1CName)
        {
            var configuration = _dbLayer.Configurations1CRepository.FirstOrDefault(c=>c.Name == configuration1CName) ??
                                throw new NotFoundException("Не удалось найти конфигурацию 1С");

            var newConfigurationDbTemplateRelation = new ConfigurationDbTemplateRelation
            {
                DbTemplateId = dbTemplateId,
                Configuration1CName = configuration.Name
            };

            _dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().Insert(newConfigurationDbTemplateRelation);
            _dbLayer.Save();

        }

        /// <summary>
        /// Обновить шаблон
        /// </summary>
        /// <param name="itemToUpdate">Данные для обновления шаблона</param>
        public void UpdateDbTemplateItem(DbTemplateItemDto itemToUpdate)
        {
            var dbTemplate = _dbLayer.DbTemplateRepository.FirstOrDefault(t => t.Id == itemToUpdate.Id);
            if (dbTemplate == null)
                throw new NotFoundException(
                    $"Не удалось получить значение справочника шаблонов по номеру {itemToUpdate.Id}");
            var oldDbTemplateName = dbTemplate.DefaultCaption.Replace(":", "_");

            dbTemplate.Name = itemToUpdate.Name;
            dbTemplate.AdminLogin = itemToUpdate.AdminLogin;
            dbTemplate.AdminPassword = itemToUpdate.AdminPassword;
            dbTemplate.CanWebPublish = itemToUpdate.CanWebPublish;
            dbTemplate.DefaultCaption = itemToUpdate.DefaultCaption;
            dbTemplate.DemoTemplateId = Guid.Empty.Equals(itemToUpdate.DemoTemplateId) ? null : itemToUpdate.DemoTemplateId;
            dbTemplate.LocaleId = Guid.Empty.Equals(itemToUpdate.LocaleId) ? null : itemToUpdate.LocaleId;
            dbTemplate.NeedUpdate = itemToUpdate.NeedUpdate;
            dbTemplate.Order = itemToUpdate.Order;
            dbTemplate.Platform =
                itemToUpdate.Platform == PlatformType.Undefined ? "" : itemToUpdate.Platform.ToString();

            try
            {
                _dbLayer.DbTemplateRepository.Update(dbTemplate);
                _dbLayer.Save();
                UpdateConfigurationDbTemplateRelations(dbTemplate.Id, itemToUpdate.Configuration1CName);
                if (dbTemplate.DefaultCaption.Replace(":", "_") != oldDbTemplateName)
                    CreateOrUpdateFolderForUpdateTemplate(dbTemplate.DefaultCaption.Replace(":", "_"), oldDbTemplateName);
            }
            catch (Exception ex)
            {
                var message = $"Во время редактирования шаблона произошла ошибка : {ex.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Изменить зависимость шаблона и конфигурации 1С
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона</param>
        private void UpdateConfigurationDbTemplateRelations(Guid dbTemplateId, string configuration1CName)
        {
            var dbTemplateRelation = _dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                .FirstOrDefault(confRel => confRel.DbTemplateId == dbTemplateId);

            if (dbTemplateRelation == null)
            {
                CreateConfigurationDbTemplateRelations(dbTemplateId, configuration1CName);
                return;
            }

            var configuration = _dbLayer.Configurations1CRepository.FirstOrDefault(conf=> conf.Name == configuration1CName) ??
                                throw new NotFoundException("Не удалось найти конфигурацию 1С");

            dbTemplateRelation.Configuration1CName = configuration.Name;

            _dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().Update(dbTemplateRelation);
            _dbLayer.Save();

        }

        /// <summary>
        /// Удалить зависимость шаблона и конфигурации 1С
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона</param>
        private void DeleteConfigurationDbTemplateRelations(Guid dbTemplateId)
        {
            var dbTemplateRelation = _dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                .FirstOrDefault(confRel => confRel.DbTemplateId == dbTemplateId);

            if (dbTemplateRelation == null)
                return;

            _dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().Delete(dbTemplateRelation);
            _dbLayer.Save();

        }

        /// <summary>
        /// Получить коллекцию шаблонов
        /// </summary>
        /// <param name="args">Параметры поиска получения коллекции шаблонов</param>
        /// <returns>Список шаблонов с пагинацией</returns>
        public DbTemplatesDataResultDto GetDbTemplatesItems(GetDbTemplatesParamsDto args)
        {
            const int itemsPerPage = GlobalSettingsConstants.ReferenceDataPages.DefaultDataGridRecordsCount;
            var records = new DbTemplatesSelector(_dbLayer, args?.Filter).SelectWithFilters();
            var recordsCount = records.Count();
            var pageNumber = PaginationHelper.GetFilterPageNumberBasedOnItemsCount(recordsCount, itemsPerPage,
                args?.PageNumber ?? 1);
            var pageRecords = DbTemplatesSortingUtility.MakeSorting(records, args?.SortingData)
                .ToPagedList(pageNumber, itemsPerPage);

            var resultRecords = pageRecords.Select(item =>
                new DbTemplateSelectDataItemDto
                {
                    Id = item.Id,
                    Name = item.Name,
                    DefaultCaption = item.DefaultCaption,
                    Platform = item.Platform
                }).ToArray();

            return new DbTemplatesDataResultDto
            {
                Records = resultRecords,
                Pagination = new PaginationBaseDto(pageNumber, recordsCount, itemsPerPage)
            };
        }

        /// <summary>
        /// Найти шаблон по имени
        /// </summary>
        /// <param name="dbTemplateName">Название шаблона</param>
        /// <returns>Возвращает найденный шаблон</returns>
        public DbTemplateItemDto GetDbTemplateByName(string dbTemplateName)
        {
            var foundDbTemplate = _dbLayer.DbTemplateRepository.FirstOrDefault(item => item.Name == dbTemplateName);

            if (foundDbTemplate == null)
            {
                throw new NotFoundException($"Шаблон с названием {dbTemplateName} не найден");
            }

            return CreateDbTemplateItemFrom(foundDbTemplate, GetConfiguration1CName(foundDbTemplate.Id));
        }

        /// <summary>
        /// Найти шаблон по ID
        /// </summary>
        /// <param name="dbTemplateId">ID шаблона</param>
        /// <returns>Возвращает true при успешном удалении</returns>
        public DbTemplateItemDto GetDbTemplateById(Guid dbTemplateId)
        {
            var foundDbTemplate = _dbLayer.DbTemplateRepository.GetById(dbTemplateId);

            if (foundDbTemplate == null)
            {
                throw new NotFoundException($"Шаблон с ID {dbTemplateId} не найден");
            }

            return CreateDbTemplateItemFrom(foundDbTemplate, GetConfiguration1CName(dbTemplateId));
        }

        /// <summary>
        /// Получить имя конфигурации 1С
        /// </summary>
        /// <param name="dbTemplateId">ID шаблона</param>
        /// <returns>Возвращает имя конфигурации или пустое значение</returns>
        private string GetConfiguration1CName(Guid dbTemplateId)
        {
            var ConfigurationDbTemplate = _dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                .FirstOrDefault(confRel => confRel.DbTemplateId == dbTemplateId);
            return ConfigurationDbTemplate != null
                ? ConfigurationDbTemplate.Configuration1CName
                : String.Empty;
            
        }

        /// <summary>
        /// Создать <see cref="DbTemplateItemDto"/> по <see cref="DbTemplate"/>
        /// </summary>
        /// <param name="dbTemplate">Шаблон, по которому создать <see cref="DbTemplateItemDto"/></param>
        /// <param name="configuration1CName">имя конфигурации></param>
        /// <returns>Возвращает созданный <see cref="DbTemplateItemDto"/> объект</returns>
        private static DbTemplateItemDto CreateDbTemplateItemFrom(DbTemplate dbTemplate, string configuration1CName)
        {
            
            return new DbTemplateItemDto
            {
                Id = dbTemplate.Id,
                Name = dbTemplate.Name,
                LocaleId = dbTemplate.LocaleId,
                Platform = dbTemplate.PlatformType,
                Order = dbTemplate.Order,
                DefaultCaption = dbTemplate.DefaultCaption,
                DemoTemplateId = dbTemplate.DemoTemplateId,
                CanWebPublish = dbTemplate.CanWebPublish,
                NeedUpdate = dbTemplate.NeedUpdate,
                AdminLogin = dbTemplate.AdminLogin,
                AdminPassword = dbTemplate.AdminPassword,
               Configuration1CName = configuration1CName
            };
        }

        public static void CreateOrUpdateFolderForUpdateTemplate(string templateName, string oldTemplateName = "")
        {
            var templateFolderPath = Path.Combine(CloudConfigurationProvider.Enterprise1C.GetTemporaryStoragePath(), "UpdateTemplates", templateName);

            if (!string.IsNullOrEmpty(oldTemplateName))
            {
                var oldTemplateFolderPath = Path.Combine(CloudConfigurationProvider.Enterprise1C.GetTemporaryStoragePath(), "UpdateTemplates", oldTemplateName);
                Directory.Move(oldTemplateFolderPath, templateFolderPath);
                return;
            }

            if (!Directory.Exists(templateFolderPath))
                Directory.CreateDirectory(templateFolderPath);
        }
        public static void DeleteUpdateFolder(string templateName)
        {
            var templateFolderPath = Path.Combine(CloudConfigurationProvider.Enterprise1C.GetTemporaryStoragePath(), "UpdateTemplates", templateName);

            if (Directory.Exists(templateFolderPath))
                Directory.Delete(templateFolderPath, true);
        }
    }
}
