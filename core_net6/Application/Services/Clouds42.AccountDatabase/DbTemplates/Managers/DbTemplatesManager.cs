﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.AccountDatabase.Contracts.DbTemplates.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.DbTemplates.Managers
{
    /// <summary>
    /// Менеджер для работы с шаблонами
    /// </summary>
    public sealed class DbTemplatesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IDbTemplatesProvider dbTemplatesProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Провайдер для работы с шаблонами 
        /// </summary>
        private readonly IDbTemplatesProvider _dbTemplatesProvider = dbTemplatesProvider;

        /// <summary>
        /// Обработчик ошибок
        /// </summary>
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить все шаблоны ввиде key - ID шаблона, value - описание шаблона
        /// </summary>
        /// <returns>Все шаблоны ввиде key - ID шаблона, value - описание шаблона</returns>
        public ManagerResult<KeyValuePair<Guid, string>[]> GetAllDbTemplatesAsKeyValue()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_View, () => AccessProvider.ContextAccountId);

                var result = _dbTemplatesProvider.GetAllDbTemplatesAsKeyValue();

                return Ok(result);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка получения списка шаблонов.]");
                return PreconditionFailed<KeyValuePair<Guid, string>[]>(exception.Message);
            }
        }

        /// <summary>
        /// Получить данные для редактирования шаблона
        /// </summary>
        /// <param name="args">Аргументы поиска шаблона для редактирования</param>
        /// <returns>Данные для редактирования шаблона</returns>
        public ManagerResult<DbTemplateToEditDataDto> GetDbTemplateDataToEdit(DbTemplateEditDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_View, () => AccessProvider.ContextAccountId);

                var result = _dbTemplatesProvider.GetDbTemplateDataToEdit(args);

                return Ok(result);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка получения данных о шаблоне для редактирования.]");
                return PreconditionFailed<DbTemplateToEditDataDto>(exception.Message);
            }
        }

        /// <summary>
        /// Получить список названий конфигураций
        /// </summary>
        /// <returns>список названий конфигураций</returns>
        public ManagerResult<List<string>> GetConfiguration1CName()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_View, () => AccessProvider.ContextAccountId);
                return Ok(_dbTemplatesProvider.GetConfiguration1CName());
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка получения списка названий конфигураций.]");
                return PreconditionFailed<List<string>>(exception.Message);
            }
        }

        /// <summary>
        /// Получить пагинированный список шаблонов.
        /// </summary>
        /// <param name="args">Фильтр</param>
        /// <returns>Пагинированный список шаблонов</returns>
        public ManagerResult<DbTemplatesDataResultDto> GetDbTemplatesItems(GetDbTemplatesParamsDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_View, () => AccessProvider.ContextAccountId);

                var pagedList = _dbTemplatesProvider.GetDbTemplatesItems(args);

                return Ok(pagedList);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка получения пагинированного списка шаблонов.]");
                return PreconditionFailed<DbTemplatesDataResultDto>(exception.Message);
            }
        }

        /// <summary>
        /// Найти шаблон по имени
        /// </summary>
        /// <param name="dbTemplateName">Название шаблона</param>
        /// <returns>Возвращает найденный шаблон</returns>
        public ManagerResult<DbTemplateItemDto> GetDbTemplateByName(string dbTemplateName)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_View, () => AccessProvider.ContextAccountId);

                var result = _dbTemplatesProvider.GetDbTemplateByName(dbTemplateName);

                return Ok(result);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка получения шаблона по имени.]");
                return PreconditionFailed<DbTemplateItemDto>(exception.Message);
            }
        }

        /// <summary>
        /// Найти шаблон по ID
        /// </summary>
        /// <param name="dbTemplateId">ID шаблона</param>
        /// <returns>Возвращает true при успешном удалении</returns>
        public ManagerResult<DbTemplateItemDto> GetDbTemplateById(Guid dbTemplateId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_View, () => AccessProvider.ContextAccountId);

                var result = _dbTemplatesProvider.GetDbTemplateById(dbTemplateId);

                return Ok(result);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка получения шаблона по ID.]");
                return PreconditionFailed<DbTemplateItemDto>(exception.Message);
            }
        }

        /// <summary>
        /// Добавить шаблон
        /// </summary>
        /// <param name="itemToAdd">Данные для добавления шаблона</param>
        /// <returns>Возвращает данные после успешного добавления</returns>
        public ManagerResult<AddDbTemplateResultDto> AddDbTemplateItem(DbTemplateItemDto itemToAdd)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_Add, () => AccessProvider.ContextAccountId);

                var result = _dbTemplatesProvider.AddDbTemplateItem(itemToAdd);

                return Ok(result);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка добавления шаблона.]");
                return PreconditionFailed<AddDbTemplateResultDto>(exception.Message);
            }
        }

        /// <summary>
        /// Обновить шаблон
        /// </summary>
        /// <param name="itemToUpdate">Данные для обновления шаблона</param>
        /// <returns>Возвращает <c>true</c> если обновление прошло успешно</returns>
        public ManagerResult<bool> UpdateDbTemplateItem(DbTemplateItemDto itemToUpdate)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_Edit, () => AccessProvider.ContextAccountId);

                _dbTemplatesProvider.UpdateDbTemplateItem(itemToUpdate);

                return Ok(true);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка редактирования шаблона.]");
                return PreconditionFailed<bool>(exception.Message);
            }

        }

        /// <summary>
        /// Удалить шаблон
        /// </summary>
        /// <param name="args">Данные для удаления шаблона</param>
        /// <returns>Возвращает <c>true</c> если удаление прошло успешно</returns>
        public ManagerResult<bool> DeleteDbTemplateItem(DeleteDbTemplateDataItemDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DbTemplates_Delete, () => AccessProvider.ContextAccountId);

                _dbTemplatesProvider.DeleteDbTemplateItem(args);

                return Ok(true);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка удаления шаблона.]");
                return PreconditionFailed<bool>(exception.Message);
            }
        }

        public ManagerResult<bool> DeleteDbTemplateItem(Guid dbTemplateId) =>
            DeleteDbTemplateItem(new DeleteDbTemplateDataItemDto { DbTemplateId = dbTemplateId });
    }
}
