﻿using Clouds42.Configurations.Configurations;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.DbTemplates.Extensions
{
    /// <summary>
    /// Расширение для DbTemplates
    /// </summary>
    public static class DbTemplateExtensions
    {
        /// <summary>
        /// получить полный путь до файла шаблона.
        /// </summary>
        /// <param name="dbTemplate">Шаблон файловой базы.</param>
        /// <returns>Полный путь дошаблона на диске.</returns>
        public static string GetTemplateFullPath(this IDbTemplate dbTemplate)
        {
            var fileTemplatePath = CloudConfigurationProvider.Template.GetTemplatesDirectory();
            return Path.Combine(fileTemplatePath, dbTemplate.Name + ".1CD");
        }
    }
}