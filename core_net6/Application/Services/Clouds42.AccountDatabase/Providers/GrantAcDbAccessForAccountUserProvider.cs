﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер предоставления доступа для пользователя к инф. базе 
    /// </summary>
    internal class GrantAcDbAccessForAccountUserProvider(
        IAcDbOnDelimitersDataProvider acDbOnDelimitersDataProvider,
        ExecuteRequestHelper executeRequestHelper,
        IHandlerException handlerException)
        : IGrantAcDbAccessForAccountUserProvider
    {
        private readonly Lazy<string> _urlForForAddAccessToDb = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForAccessToDb);

        /// <summary>
        /// Выдать доступ для пользователя
        /// </summary>
        /// <param name="manageAcDbAccessParams">Параметры управления доступом</param>
        public ManageAcDbAccessResultDto GrantAcDbAccess(ManageAcDbAccessParamsDto manageAcDbAccessParams)
        {
            try
            {
                var acDbAccess =
                    acDbOnDelimitersDataProvider.GetAcDbAccess(
                        manageAcDbAccessParams.AccountDatabaseId, manageAcDbAccessParams.AccountUserId);

                if (acDbAccess == null)
                    return new ManageAcDbAccessResultDto
                    {
                        AccessState = AccountDatabaseAccessState.Error
                    };

                return executeRequestHelper.ExecuteRequestForManageAcDbAccess(acDbAccess, _urlForForAddAccessToDb.Value);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка выдачи доступа к инф. базе] пользователю {manageAcDbAccessParams.AccountUserId} " +
                              $"к инф. базе {manageAcDbAccessParams.AccountDatabaseId} ");
                throw;
            }
        }
    }
}
