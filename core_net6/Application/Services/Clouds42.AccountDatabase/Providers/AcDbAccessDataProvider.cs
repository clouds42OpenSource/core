﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер для получения данных по доступам к инф. базам
    /// </summary>
    internal class AcDbAccessDataProvider(IUnitOfWork dbLayer) : IAcDbAccessDataProvider
    {
        /// <summary>
        /// Получить доступ к инф. базе
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Доступ к инф. базе</returns>
        public AcDbAccess GetAcDbAccess(Guid accountUserId, Guid accountDatabaseId)
            => dbLayer.AcDbAccessesRepository
                .FirstOrDefault(access =>
                    access.AccountDatabaseID == accountDatabaseId &&
                    access.AccountUserID == accountUserId);

        /// <summary>
        /// Получить доступ к инф. базе или выбросить исключение
        /// </summary>
        /// <param name="acDbAccessId">ID доступа</param>
        /// <returns>Доступ к инф. базе</returns>
        public AcDbAccess GetAcDbAccessOrThrowException(Guid acDbAccessId)
            => dbLayer.AcDbAccessesRepository.FirstOrThrowException(access => access.ID == acDbAccessId,
                $"Доступ по ID {acDbAccessId} не найден");

        /// <summary>
        /// Получить доступы аккаунта к инф. базе
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Доступы аккаунта к инф. базе</returns>
        public List<AcDbAccess> GetAccountAccessesForDatabase(Guid accountId, Guid accountDatabaseId)
            => dbLayer.AcDbAccessesRepository.WhereLazy(
                access => access.AccountDatabaseID == accountDatabaseId && access.AccountID == accountId).ToList();
    }
}
