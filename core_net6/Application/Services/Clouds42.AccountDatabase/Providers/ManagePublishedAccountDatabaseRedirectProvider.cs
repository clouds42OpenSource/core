﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.StateMachine.Contracts.IisApplicationPoolProcessFlow;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер для управления редиректом для опубликованной базы
    /// </summary>
    internal class ManagePublishedAccountDatabaseRedirectProvider(
        IInstallPublishedAcDbRedirectProcessFlow installPublishedAcDbRedirectProcessFlow,
        IRemovePublishedAcDbRedirectProcessFlow removePublishedAcDbRedirectProcessFlow)
        : IManagePublishedAccountDatabaseRedirectProvider
    {
        /// <summary>
        /// Обработать операцию управления редиректом для инф. базы
        /// </summary>
        /// <param name="manageAcDbRedirectParams">Параметры управления редиректом</param>
        /// <returns>Ok - если операция завершена успешно</returns>
        public void ManageAcDbRedirect(ManagePublishedAcDbRedirectParamsDto manageAcDbRedirectParams)
        {
            if (manageAcDbRedirectParams.OperationType == ManageAcDbRedirectOperationType.Install)
            {
                installPublishedAcDbRedirectProcessFlow.Run(manageAcDbRedirectParams);
                return;
            }

            removePublishedAcDbRedirectProcessFlow.Run(manageAcDbRedirectParams);
        }
    }
}
