﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Регистратор событий потдержки.
    /// </summary>
    internal class AcDbSupportHistoryRegister(
        IUnitOfWork dbLayer,
        AcDbSupportHelper acDbSupportHelper,
        IHandlerException handlerException)
        : IAcDbSupportHistoryRegistrator
    {
        /// <summary>
        /// Зарегистрировать неверные авторазационные данные.
        /// </summary>        
        public void RegisterAuthIncorrectData(AcDbSupport support)
        {
            var errorMessage = acDbSupportHelper.GetInvalidAuthorizationMessage(support.AccountDatabasesID);
            RegisterAuthError(support, errorMessage);
        }
        
        /// <summary>
        /// Зарегистрировать ошибку авторизации.
        /// </summary>        
        public void RegisterAuthFail(AcDbSupport support, string errorMessage)
        {
            RegisterAuthError(support, errorMessage);
        }        

        /// <summary>
        /// Зарегистрировать успешную авторизация.
        /// </summary>        
        public void RegisterSuccessAuth(AcDbSupport support)
        {
            dbLayer.AcDbSupportHistoryRepository.Insert(new AcDbSupportHistory
            {
                AccountDatabaseId = support.AccountDatabasesID,
                Operation = DatabaseSupportOperation.AuthToDb,
                State = SupportHistoryState.Success,
                Description = $"Авторизация в информационную базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(support.AccountDatabase)} прошла успешно.",
                ID = Guid.NewGuid()
            });
            dbLayer.Save();
        }

        public async Task RegisterNotificationRead(Domain.DataModels.AccountDatabase database)
        {
            var newRecord = new AcDbSupportHistory
            {
                AccountDatabaseId = database.Id,
                Operation = DatabaseSupportOperation.UserNotified,
                State = SupportHistoryState.Success,
                Description = $"Пользователь уведомлен, что {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} можно поставить на АО и ТИИ",
                ID = Guid.NewGuid()
            };
            if (database.AcDbSupport == null)
            {
                dbLayer.AcDbSupportRepository.Insert(new AcDbSupport
                {
                    AcDbSupportHistories = new List<AcDbSupportHistory>{newRecord},
                    AccountDatabasesID = database.Id,
                });
            }
            else
            {
                if (database.AcDbSupport.AcDbSupportHistories == null || database.AcDbSupport.AcDbSupportHistories.Count == 0)
                {
                    dbLayer.AcDbSupportHistoryRepository.Insert(newRecord);
                }
            }
            await dbLayer.SaveAsync();        
        }

        /// <summary>
        /// Зарегистрировать успешное подключение к АО.
        /// </summary>        
        public void RegisterSuccessConnectToAutoUpdate(AcDbSupport support)
        {
            var history = new AcDbSupportHistory
            {
                AccountDatabaseId = support.AccountDatabasesID,
                State = SupportHistoryState.Success,
                Description = $"Информационная база «{support.AccountDatabase.Caption}» успешно подключена к Автообновлению.",
                Operation = DatabaseSupportOperation.AutoUpdateRequest,
                ID = Guid.NewGuid()
            };

            dbLayer.AcDbSupportHistoryRepository.Insert(history);

            dbLayer.Save();

        }

        /// <summary>
        /// Зарегистрировать неизвестную конфигурацию.
        /// Примечание: конфигурация может быть не известная если не заполнить справочник dbo.Configurations1C
        /// </summary>        
        public void RegisterUndefinedConfiguration(AcDbSupport support, string configurationName)
        {
            var history = new AcDbSupportHistory
            {
                AccountDatabaseId = support.AccountDatabasesID,
                Operation = DatabaseSupportOperation.AuthToDb,
                State = SupportHistoryState.Error,
                Description =
                    $"Не известная конфигурация «{configurationName}».\n" +
                    "Обратитесь в потдержку 42Clouds за помощью",
                ID = Guid.NewGuid()
            };

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                support.HasAutoUpdate = false;                    

                dbLayer.AcDbSupportRepository.Update(support);
                dbLayer.AcDbSupportHistoryRepository.Insert(history);

                dbLayer.Save();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при регистрации неизвестной конфигурации {configurationName}.]");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Зарегистрировать ошибку авторизации в базу.
        /// </summary>
        /// <param name="support">Данные о базе.</param>
        /// <param name="errorMessage">Сообщение об ошибке авторизации.</param>
        private void RegisterAuthError(AcDbSupport support, string errorMessage)
        {
            dbLayer.AcDbSupportHistoryRepository.Insert(new AcDbSupportHistory
            {
                AccountDatabaseId = support.AccountDatabasesID,
                Operation = DatabaseSupportOperation.AuthToDb,
                State = SupportHistoryState.Error,
                Description = errorMessage,
                ID = Guid.NewGuid()
            });

            dbLayer.Save();
        }

    }
}
