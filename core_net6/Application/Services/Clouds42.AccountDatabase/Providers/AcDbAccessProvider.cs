﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер для работы с доступами к инф. базам
    /// </summary>
    internal class AcDbAccessProvider(
        IUnitOfWork dbLayer,
        AccountDatabasePathHelper accountDatabasesPathHelper,
        IAcDbAccessDataProvider acDbAccessDataProvider,
        IAccountDatabaseProvider accountDatabaseProvider,
        AcDbAccessNotificationHelper acDbAccessNotificationHelper,
        AccessForAcDbFolderHelper accessForAcDbFolderHelper,
        ILogger42 logger,
        IHandlerException handlerException)
        : IAcDbAccessProvider
    {
        /// <summary>
        /// Получить или добавить доступ к инф. базе
        /// </summary>
        /// <param name="model">Параметры предоставления доступа</param>
        /// <returns>Доступ к инф. базе</returns>
        public AcDbAccess GetOrCreateAccess(AcDbAccessPostAddModelDto model)
        {
            var existAccess = acDbAccessDataProvider.GetAcDbAccess(model.AccountUserID, model.AccountDatabaseID);
            if (existAccess != null)
                return existAccess;

            var accountDatabase = accountDatabaseProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseID);
            var accountUser = GetAccountUserOrThrowException(model.AccountUserID);

            if (accountDatabase.IsFile == true)
            {
                var dbFolder = accountDatabasesPathHelper.GetPath(accountDatabase);
                accessForAcDbFolderHelper.ChangeAccessForDbFolder(AclAction.SetAccessControl, dbFolder, accountUser.Login);
            }

            var accessEntity = CreateAcDbAccessObjHelper.CreateAcDbAccessObj(model);
            dbLayer.AcDbAccessesRepository.Insert(accessEntity);
            dbLayer.Save();

            if (accountDatabase.IsFile != true)
                return accessEntity;

            if (model.SendNotification.HasValue && model.SendNotification.Value)
                acDbAccessNotificationHelper.NotifyAbountGrantAccess(accessEntity);

            return accessEntity;
        }

        /// <summary>
        /// Удалить доступ если он существует
        /// </summary>
        /// <param name="accounDatabaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        public void DeleteAccessIfExist(Guid accounDatabaseId, Guid accountUserId)
        {
            var acDbAccess = acDbAccessDataProvider.GetAcDbAccess(accountUserId, accounDatabaseId);
            if (acDbAccess == null)
                return;

            DeleteAccess(acDbAccess);
        }

        /// <summary>
        /// Удалить доступ
        /// </summary>
        /// <param name="model">Параметры удаления доступа</param>
        public void DeleteAccess(AcDbAccessPostIDDto model)
        {
            var acDbAccess = acDbAccessDataProvider.GetAcDbAccessOrThrowException(model.AccessID);
            DeleteAccess(acDbAccess);
        }

        /// <summary>
        /// Удалить доступы аккаунта к инф. базе
        /// </summary>
        /// <param name="model">Параметры удаления доступов</param>
        public void DeleteAccountAccesses(DeleteAccessDto model)
        {
            if (!model.AccountId.HasValue)
                throw new InvalidOperationException("В модели не указан ID аккаунта");

            if (!model.AccountDatabaseID.HasValue)
                throw new InvalidOperationException("В модели не указан ID инф. базы");

            var acDbAccesses = acDbAccessDataProvider.GetAccountAccessesForDatabase(model.AccountId.Value, model.AccountDatabaseID.Value);
            if (!acDbAccesses.Any())
                throw new InvalidOperationException(
                    $"Доступы аккаунта {model.AccountId} к инф. базе {model.AccountDatabaseID} не найдены");
            acDbAccesses.ForEach(DeleteAccess);
        }

        /// <summary>
        /// Установить ID аккаунта для доступа
        /// </summary>
        /// <param name="model">Параметры установки ID аккаунта для доступа</param>
        public void SetAccountId(AcDbAccessPostSetAccIdDto model)
        {
            var acDbAccess = acDbAccessDataProvider.GetAcDbAccessOrThrowException(model.AccessID);
            if (acDbAccess.AccountID == model.AccountID)
                throw new InvalidOperationException("Access with such params already exists");
            
            acDbAccess.AccountID = model.AccountID;
            dbLayer.AcDbAccessesRepository.Update(acDbAccess);
            dbLayer.Save();

            if (model.SendNotification.HasValue && model.SendNotification.Value)
                acDbAccessNotificationHelper.NotifyAbountGrantAccess(acDbAccess);
        }

        /// <summary>
        /// Добавить доступ для всех пользователей
        /// </summary>
        /// <param name="model">Параметры добавления доступов</param>
        /// <returns>Список добавленных доступов</returns>
        public List<Guid> AddAccessesForAllAccountsUsers(AcDbAccessPostAddAllUsersModelDto model)
        {
            if (!model.AccountID.HasValue)
                throw new InvalidOperationException("В модели не указан ID аккаунта");
            var accountUsers = dbLayer.AccountUsersRepository.GetAccountUsers(model.AccountID.Value);
            logger.Trace($"Account has {accountUsers.Count} users");

            if (!accountUsers.Any())
                return [];

            if (!model.AccountDatabaseID.HasValue)
                throw new InvalidOperationException("В модели не указан ID инф. базы");
            var accountDatabase = accountDatabaseProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseID.Value);
            var existingAccesses =
                acDbAccessDataProvider.GetAccountAccessesForDatabase(model.AccountID.Value, accountDatabase.Id);

            try
            {
                var newAccessesList = CreateAcDbAccessesForAccountUsers(accountUsers, existingAccesses,
                    model.AccountDatabaseID.Value, model.AccountID.Value, model.LocalUserID);
                if (!newAccessesList.Any())
                    return [];

                foreach (var access in newAccessesList)
                {
                    if (!access.AccountUserID.HasValue)
                        continue;
                    logger.Trace($"Устанавливаем права на папку для пользователя {access.AccountUserID}");
                    accessForAcDbFolderHelper.ChangeAccessForDbFolder(access.AccountDatabaseID, access.AccountUserID.Value,
                        AclAction.SetAccessControl);
                }

                dbLayer.AcDbAccessesRepository.InsertRange(newAccessesList);
                dbLayer.Save();
                return newAccessesList.Select(access => access.ID).ToList();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка добавления доступа всем пользователям]");
                throw;
            }
        }

        /// <summary>
        /// Установить ID пользователя для доступа
        /// </summary>
        /// <param name="model">Параметры установки ID пользователя для доступа</param>
        /// <returns>Результат установки</returns>
        public void SetAccountUserId(AcDbAccessAccountUserIDPostDto model)
        {
            var acDbAccess = acDbAccessDataProvider.GetAcDbAccessOrThrowException(model.AccessID);
            if (acDbAccess.AccountUserID == model.AccountUserID)
                throw new InvalidOperationException("Access with such params already exists");
            
            if (acDbAccess.AccountUserID.HasValue)
                accessForAcDbFolderHelper.ChangeAccessForDbFolder(acDbAccess.AccountDatabaseID,
                    acDbAccess.AccountUserID.Value, AclAction.RemoveAccessRule);

            accessForAcDbFolderHelper.ChangeAccessForDbFolder(acDbAccess.AccountDatabaseID,
                model.AccountUserID, AclAction.SetAccessControl);

            acDbAccess.AccountUserID = model.AccountUserID;
            dbLayer.AcDbAccessesRepository.Update(acDbAccess);
            dbLayer.Save();
        }

        /// <summary>
        /// Создать доступы для пользователей
        /// </summary>
        /// <param name="accountUsers">Список пользователей</param>
        /// <param name="existingAccesses">Существующие доступы</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="localUserId">ID локального пользователя</param>
        /// <returns>Список доступов</returns>
        private List<AcDbAccess> CreateAcDbAccessesForAccountUsers(IEnumerable<AccountUser> accountUsers, List<AcDbAccess> existingAccesses, 
            Guid accountDatabaseId, Guid accountId, Guid? localUserId)
        {
            var newAccessesList = new List<AcDbAccess>();
            foreach (var accountUser in accountUsers)
            {
                if (existingAccesses.Any(ac => ac.AccountUserID == accountUser.Id))
                {
                    logger.Trace($"Доступ для пользователя {accountUser.Login} уже существует. Пропускаем его");
                    continue;
                }

                var acDbAccess = CreateAcDbAccessObjHelper.CreateAcDbAccessObj(accountDatabaseId, accountId,
                    localUserId ?? Guid.Empty, accountUser.Id);
                newAccessesList.Add(acDbAccess);
            }

            return newAccessesList;
        }

        /// <summary>
        /// Получить пользователя или выбросить исключение
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Пользователь</returns>
        private AccountUser GetAccountUserOrThrowException(Guid accountUserId)
            => dbLayer.AccountUsersRepository.FirstOrThrowException(acUs => acUs.Id == accountUserId,
                $"Пользователь не найден по номеру '{accountUserId}'");

        /// <summary>
        /// Удалить доступ к инф. базе
        /// </summary>
        /// <param name="acDbAccess">Доступ к инф. базе</param>
        private void DeleteAccess(AcDbAccess acDbAccess)
        {
            try
            {
                var rolesJho = dbLayer.AcDbAccRolesJhoRepository.Where(x => x.AcDbAccessesID == acDbAccess.ID);
                if (rolesJho != null)
                    foreach (var role in rolesJho)
                        dbLayer.AcDbAccRolesJhoRepository.Delete(role);

                if (acDbAccess.AccountUserID.HasValue)
                    accessForAcDbFolderHelper.ChangeAccessForDbFolder(acDbAccess.AccountDatabaseID, acDbAccess.AccountUserID.Value,
                        AclAction.RemoveAccessRule);
                dbLayer.AcDbAccessesRepository.Delete(acDbAccess);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка удаления доступа]");
                throw;
            }
        }
    }
}
