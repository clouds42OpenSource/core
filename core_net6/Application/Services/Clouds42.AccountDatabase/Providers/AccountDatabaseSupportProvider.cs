﻿using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер инфо базы по управлению ТиИ и АО
    /// </summary>
    internal class AccountDatabaseSupportProvider(
        IUnitOfWork dbLayer,
        ICfuProvider cfuProvider,
        IDatabasePlatformVersionHelper databasePlatformVersionHelper,
        ICheckAccountDataProvider checkAccountDataProvider,
        IHandlerException handlerException,
        IPlanUpdateVersionAccountDatabaseProvider planUpdateVersionAccountDatabaseProvider)
        : IAccountDatabaseSupportProvider
    {
        readonly IDictionary<DatabaseSupportOperation, string> _mapSupportOperationToHistoryDescription = new Dictionary<DatabaseSupportOperation, string>
        {
            {DatabaseSupportOperation.PlanAutoUpdate, $"Запланировано Автообновление базы '{DateTime.Now:d}'"},
            {DatabaseSupportOperation.PlanTehSupport, $"Запланировано проведение ТиИ на вечер {DateTime.Now:D}"}
        };
        readonly int[] TiIDateList = Configurations.Configurations.CloudConfigurationProvider.AccountDatabase.Support.GetTiIDateList().Split(',').Select(int.Parse).ToArray();

        /// <summary>
        /// Переключить тумлер ТиИ у информационной базы
        /// </summary>
        /// <param name="accountDatabaseId">Номер базы</param>
        /// <param name="hasSupport">Положение тумблера</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Успешность операции</returns>
        public void ChangeHasSupportFlag(AcDbSupport acDbSupport, bool hasSupport)
        {
            var preparedForTehSupport = TiIDateList.Contains(DateTime.Now.Day) && DateTime.Now.Hour < 21;
            acDbSupport.PreparedForTehSupport = preparedForTehSupport;
            acDbSupport.HasSupport = hasSupport;

            UpdateAcDbSupportAndInsertHistoryRecord(acDbSupport, DatabaseSupportOperation.PlanTehSupport, preparedForTehSupport);
        }

        /// <summary>
        /// Переключить тумлер Автообновление у информационной базы
        /// </summary>
        /// <param name="accountDatabaseId">Номер базы</param>
        /// <param name="hasAutoUpdate">Положение тумблера</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Успешность операции</returns>
        public void ChangeHasAutoUpdateFlag(AcDbSupport acDbSupport, bool hasAutoUpdate)
        {
            CheckAbilityToChangeAuFlag(acDbSupport, hasAutoUpdate);

            var prepearedForUpdate = hasAutoUpdate && NeedAddDatabaseToAutoUpdatePlan(acDbSupport) 
                && !TiIDateList.Contains(DateTime.Now.Day) && DateTime.Now.Hour < (int)acDbSupport.TimeOfUpdate; 

            acDbSupport.HasAutoUpdate = hasAutoUpdate;
            if (!acDbSupport.ConnectDate.HasValue)
                acDbSupport.ConnectDate = DateTime.Now;

            acDbSupport.PrepearedForUpdate = prepearedForUpdate;

            UpdateAcDbSupportAndInsertHistoryRecord(acDbSupport, DatabaseSupportOperation.PlanAutoUpdate, prepearedForUpdate);
        }

        /// <summary>
        /// Отключить авторизацию в инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        public void DisableSupportAuthorization(Guid accountDatabaseId)
        {
            try
            {
                var acDbSupport = GetAcDbSupport(accountDatabaseId)
                                  ?? throw new NotFoundException($"Модель поддержки для инф. базы {accountDatabaseId} не найдена.");

                NullifyAuthorizationDataAndDisableSupportFlags(acDbSupport);

                dbLayer.AcDbSupportHistoryRepository.Insert(new AcDbSupportHistory
                {
                    AccountDatabaseId = acDbSupport.AccountDatabasesID,
                    Operation = DatabaseSupportOperation.DisableAuthToDb,
                    State = SupportHistoryState.Success,
                    Description = "Произведена отмена авторизации в ИБ",
                });

                dbLayer.AcDbSupportRepository.Update(acDbSupport);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка отключении авторизации в инф. базу] {accountDatabaseId}");
                throw;
            }
        }

        /// <summary>
        /// Обнулить авторизационные данные и отключить флаги тех поддержки
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        private static void NullifyAuthorizationDataAndDisableSupportFlags(AcDbSupport acDbSupport)
        {
            acDbSupport.HasAutoUpdate = false;
            acDbSupport.PrepearedForUpdate = false;
            acDbSupport.HasSupport = false;
            acDbSupport.PreparedForTehSupport = false;
            acDbSupport.State = (int) SupportState.NotAutorized;
            acDbSupport.Login = string.Empty;
            acDbSupport.Password = string.Empty;
        }

        /// <summary>
        /// Получить модель поддержки инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Модель поддержки инф. базы</returns>
        public AcDbSupport GetAcDbSupport(Guid accountDatabaseId)
            => dbLayer.AcDbSupportRepository.FirstOrDefault(s => s.AccountDatabasesID == accountDatabaseId);

        /// <summary>
        /// Проверить возможность переключить тумлер Автообновление у информационной базы
        /// </summary>
        /// <param name="support">Модель поддержки инф. базы</param>
        /// <param name="hasAutoUpdate">Положение тумблера</param>
        /// <returns>Результат проверки</returns>
        private void CheckAbilityToChangeAuFlag(AcDbSupport support, bool hasAutoUpdate)
        {
            if (!hasAutoUpdate)
                return;

            if (support.DatabaseHasModifications && hasAutoUpdate)
                throw new InvalidOperationException($"Не удалось подключить услугу Автообновление, так так инф. база {support.AccountDatabase.Caption} содержит доработки!");

            if (!planUpdateVersionAccountDatabaseProvider.CanPlugAutoUpdate(support))
                throw new InvalidOperationException($"Невозможно подключить автообновление к данной конфигурации: {support.SynonymConfiguration}");
        }

        /// <summary>
        /// Обновить модель поддержки инф. базы и вставить запись об успешном планировании
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <param name="supportOperation">Тип операции поддержки</param>
        /// <param name="needRegisterSupportHistory">Признак что необходимо регистрировать историческую запись</param>
        private void UpdateAcDbSupportAndInsertHistoryRecord(AcDbSupport acDbSupport,
            DatabaseSupportOperation supportOperation, bool needRegisterSupportHistory)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                dbLayer.AcDbSupportRepository.Update(acDbSupport);
                dbLayer.Save();

                if (needRegisterSupportHistory)
                    CreateAcDbSupportHistory(acDbSupport.AccountDatabasesID, supportOperation);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка обновления записи о поддержке инф. базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(acDbSupport.AccountDatabase)}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать запись о планировании АО для инф. базы
        /// </summary>
        /// <param name="accountDatabasesId">ID инф. базы</param>
        /// <param name="supportOperation">Тип операции поддержки</param>
        private void CreateAcDbSupportHistory(Guid accountDatabasesId, DatabaseSupportOperation supportOperation)
        {
            if (!_mapSupportOperationToHistoryDescription.ContainsKey(supportOperation))
                throw new InvalidOperationException(
                    $"Создание исторической записи для операции {supportOperation} не поддерживается");

            var acDbSupportHistory = new AcDbSupportHistory
            {
                AccountDatabaseId = accountDatabasesId,
                State = SupportHistoryState.Success,
                Description = _mapSupportOperationToHistoryDescription[supportOperation],
                Operation = supportOperation,
                ID = Guid.NewGuid()
            };

            dbLayer.AcDbSupportHistoryRepository.Insert(acDbSupportHistory);
            dbLayer.Save();
        }

        /// <summary>
        /// Признак необходимости добавлять инф. базу в план
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <returns>true - если есть доступная версия для обновления</returns>
        private bool NeedAddDatabaseToAutoUpdatePlan(AcDbSupport acDbSupport)
        {
            if (string.IsNullOrEmpty(acDbSupport.CurrentVersion))
                return true;

            var acDbPlatformVersion = databasePlatformVersionHelper.GetPlatformVersion(acDbSupport.AccountDatabase);

            var lastUpdateVersion = cfuProvider.GetLastUpdateVersion(acDbSupport.Configurations1C,
                acDbSupport.CurrentVersion, acDbPlatformVersion.Version);

            return lastUpdateVersion != null;
        }
    }
}
