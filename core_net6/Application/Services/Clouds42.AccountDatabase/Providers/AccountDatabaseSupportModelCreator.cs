﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Класс создания модели данных информационной базы и тех поддерки.
    /// </summary>
    public class AccountDatabaseSupportModelCreator(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IAccountConfigurationDataProvider configurationDataProvider,
        ISegmentHelper segmentHelper,
        IHandlerException handlerException)
    {
        private readonly CloudServicesEnterpriseServerProvider _cloudServicesEnterpriseServerProvider = new(dbLayer);

        /// <summary>
        /// Создать модель поддержки инф. базы.
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Модель поддержки инф. базы</returns>
        public AccountDatabaseSupportModelDto CreateModel(Guid accountDatabaseId)
        {
            var acDbSupport =
                dbLayer.AcDbSupportRepository.FirstOrDefault(s => s.AccountDatabasesID == accountDatabaseId)
                ?? throw new InvalidOperationException(
                    $"Не удалось получить AcDbSupport по номеру {accountDatabaseId}");

            var accountDatabasePath = new AccountDatabasePathHelper(dbLayer, logger, segmentHelper, handlerException).GetPath(acDbSupport.AccountDatabase);

            var accountSegment =
                configurationDataProvider.GetAccountSegment(acDbSupport.AccountDatabase.AccountId);

            return new AccountDatabaseSupportModelDto
            {
                AccountDatabase = acDbSupport.AccountDatabase,
                Support = acDbSupport,
                EnterpriseServer =
                    _cloudServicesEnterpriseServerProvider.GetEnterpriseServerData(acDbSupport.AccountDatabase),
                ConfigurationInfo = GetConfigurations1C(acDbSupport),
                Template = acDbSupport.AccountDatabase.DbTemplate,
                AccountDatabasePath = accountDatabasePath,
                Segment = accountSegment,
                SqlServer = accountSegment.CloudServicesSQLServer,
                CloudPlatforms = dbLayer.PlatformVersionReferencesRepository.GetAll().Cast<IPlatformVersionReference>()
                    .ToList()
            };
        }

        /// <summary>
        /// Получить конфигурацию 1С для инф. базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <returns>Конфигурация 1С для инф. базы</returns>
        private IConfigurations1C GetConfigurations1C(AcDbSupport acDbSupport)
        {
            if (acDbSupport.Configurations1C != null)
                return acDbSupport.Configurations1C;

            var configuration1C = (from conf1C in dbLayer.Configurations1CRepository.WhereLazy()
                join variation in dbLayer.ConfigurationNameVariationRepository.WhereLazy() on conf1C.Name equals
                    variation.Configuration1CName
                where variation.VariationName == acDbSupport.SynonymConfiguration ||
                      conf1C.Name == acDbSupport.SynonymConfiguration
                select conf1C).FirstOrDefault();

            return configuration1C;
        }
    }
}
