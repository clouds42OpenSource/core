﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер для удаления инф. баз
    /// </summary>
    internal class DeleteAccountDatabaseProvider(
        IDeleteAccountDatabaseProcessFlow deleteAccountDatabaseProcessFlow,
        IStartProcessOfTerminateSessionsInDatabaseProvider startProcessOfTerminateSessionsInDatabaseProvider,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IAccessProvider accessProvider)
        : IDeleteAccountDatabaseProvider
    {
        /// <summary>
        /// Удалить инф. базу
        /// </summary>
        /// <param name="deleteAccountDatabaseParams">Параметры удаления инф. базы</param>
        public void DeleteAccountDatabase(DeleteAccountDatabaseParamsDto deleteAccountDatabaseParams)
        {
            try
            {
                if (!startProcessOfTerminateSessionsInDatabaseProvider.TryStartProcess(new StartProcessOfTerminateSessionsInDatabaseDto
                { DatabaseId = deleteAccountDatabaseParams.AccountDatabaseId }, out var errorMessage))
                    throw new InvalidOperationException(errorMessage);

                var result = deleteAccountDatabaseProcessFlow.Run(deleteAccountDatabaseParams);

                errorMessage = ProcessDeletionResult(result, deleteAccountDatabaseParams);

                if (!string.IsNullOrEmpty(errorMessage))
                    throw new InvalidOperationException(errorMessage);

            }
            catch (Exception ex)
            {
                logger.Warn($"При удалении инф. базы {deleteAccountDatabaseParams.AccountDatabaseId} возникла ошибка. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Обработать результат удаления инф. базы
        /// </summary>
        /// <param name="result">Результат удаления</param>
        /// <param name="deleteAccountDatabaseParams">Параметры удаления инф. базы</param>
        /// <returns>Сообщение об ошибке</returns>
        private string ProcessDeletionResult(StateMachineResult<bool> result, DeleteAccountDatabaseParamsDto deleteAccountDatabaseParams)
        {
            var accountDatabase =
                dbLayer.DatabasesRepository.GetAccountDatabase(deleteAccountDatabaseParams.AccountDatabaseId);

            if (result.Finish)
            {
               LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider,
               LogActions.DeleteAccountDatabase,
               $"Удаление инф. базы {accountDatabase.V82Name} успешно выполнено.");
                return null;
            }

            LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider,
            LogActions.DeleteAccountDatabase,
            $"Удаление инф. базы {accountDatabase.V82Name} не выполнено. Причина: {result.Message}");

            var linkForOpenProcessFlow = GetLinkForOpenProcessFlow(result.ProcessFlowId);

            var errorMessage =
                $"Удаление инф. базы {accountDatabase.V82Name} не выполнено. " +
                $"Чтобы завершить процесс переноса перейдите по ссылке {linkForOpenProcessFlow}. " +
                $"Причина: {result.Message}";

            return errorMessage;
        }

        /// <summary>
        /// Получить ссылку для просмотра деталей рабочего процесса
        /// </summary>
        /// <param name="processFlowId"></param>
        /// <returns>Ссылка для просмотра деталей рабочего процесса</returns>
        private string GetLinkForOpenProcessFlow(Guid processFlowId)
        {
            var siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
            var routeValue = CloudConfigurationProvider.Cp.GetRouteValueForOpenProcessFlowDetails();

            return $"{siteUrl}/{routeValue}{processFlowId}";
        }
    }
}
