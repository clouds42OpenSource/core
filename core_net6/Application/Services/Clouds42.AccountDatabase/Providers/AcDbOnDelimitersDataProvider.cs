﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провадер данных для доступа к инф. базе на разделителях
    /// </summary>
    internal class AcDbOnDelimitersDataProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : IAcDbOnDelimitersDataProvider
    {
        /// <summary>
        /// Получить модель доступа к инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Модель доступа к инф. базе</returns>
        public AcDbAccess GetAcDbAccess(Guid accountDatabaseId, Guid accountUserId)
            => dbLayer.AcDbAccessesRepository.FirstOrDefault(x =>
                x.AccountDatabaseID == accountDatabaseId && x.AccountUserID == accountUserId);

        /// <summary>
        /// Получить модель доступа к инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Модель доступа к инф. базе</returns>
        public AcDbAccess GetAcDbAccessOrThrowException(Guid accountDatabaseId, Guid accountUserId)
            => dbLayer.AcDbAccessesRepository.FirstOrThrowException(x =>
                   x.AccountDatabaseID == accountDatabaseId && x.AccountUserID == accountUserId);

        /// <summary>
        /// Удалить запись о доступе
        /// </summary>
        /// <param name="accessEntity">Модель доступа</param>
        public void DeleteAcDbAccess(AcDbAccess accessEntity)
        {
            var acDbAccessRolesJho =
                dbLayer.AcDbAccRolesJhoRepository.Where(role => role.AcDbAccessesID == accessEntity.ID).ToList();

            try
            {
                logger.Info($"Начало удаления данных из таблицы AcDbAccesses для пользователя '{accessEntity.AccountUserID}'");

                acDbAccessRolesJho.ForEach(role =>
                {
                    dbLayer.AcDbAccRolesJhoRepository.Delete(role);
                });

                dbLayer.AcDbAccessesRepository.Delete(accessEntity);
                dbLayer.Save();
                logger.Info($"Данные успешно удалены в таблице AcDbAccesses для пользователя '{accessEntity.AccountUserID}'");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,$"[Ошибка удаления доступа к базе на разделителях]");
                throw new InvalidOperationException("Ошибка удаления доступа для пользователя");
            }

        }

        /// <summary>
        /// Удалить запись о доступе при ошибке
        /// </summary>
        /// <param name="accessEntity">Модель доступа</param>
        public void DeleteAcDbAccessForError(AcDbAccess accessEntity)
        {

            var acDbAccessRolesJho =
                dbLayer.AcDbAccRolesJhoRepository.Where(role => role.AcDbAccessesID == accessEntity.ID).ToList();

            using (var transaction = dbLayer.SmartTransaction.Get())
            {
                try
                {
                    logger.Info($"Начало удаления данных из таблицы AcDbAccesses для пользователя '{accessEntity.AccountUserID}'");

                    acDbAccessRolesJho.ForEach(role =>
                    {
                        dbLayer.AcDbAccRolesJhoRepository.Delete(role);
                    });

                    dbLayer.AcDbAccessesRepository.Delete(accessEntity);
                    dbLayer.Save();
                    transaction.Commit();
                    logger.Info($"Данные успешно удалены в таблице AcDbAccesses для пользователя '{accessEntity.AccountUserID}'");
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex,$"[Ошибка удаления доступа к базе на разделителях]");
                    transaction.Rollback();
                    throw new InvalidOperationException("Ошибка удаления доступа для пользователя");
                }
            }
        }
    }
}
