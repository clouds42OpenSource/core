﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер удаления доступа для пользователя к инф. базе 
    /// </summary>
    internal class DeleteAcDbAccessForAccountUserProvider(
        IAcDbOnDelimitersDataProvider acDbOnDelimitersDataProvider,
        ILogger42 logger,
        ExecuteRequestHelper executeRequestHelper)
        : IDeleteAcDbAccessForAccountUserProvider
    {
        private readonly Lazy<string> _urlForDeleteAccessToDb = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForAccessToDb);

        /// <summary>
        /// Удалить доступ для пользователя
        /// </summary>
        /// <param name="manageAcDbAccessParams">Параметры управления доступом</param>
        public ManageAcDbAccessResultDto DeleteAcDbAccess(ManageAcDbAccessParamsDto manageAcDbAccessParams)
        {
            try
            {
                var acDbAccess =
                    acDbOnDelimitersDataProvider.GetAcDbAccess(
                        manageAcDbAccessParams.AccountDatabaseId, manageAcDbAccessParams.AccountUserId);

                if (acDbAccess == null)
                {
                    return new ManageAcDbAccessResultDto
                    {
                        AccessState = AccountDatabaseAccessState.Done
                    };
                }

                return executeRequestHelper.ExecuteRequestForManageAcDbAccess(acDbAccess, _urlForDeleteAccessToDb.Value, false);
            }
            catch (Exception ex)
            {
                logger.Warn($"При удалении доступа пользователю {manageAcDbAccessParams.AccountUserId} " +
                              $"к инф. базе {manageAcDbAccessParams.AccountDatabaseId} возникла ошибка. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }
    }
}
