﻿using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Starter1C.Helpers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер тех обслуживания (ТиИ) информационной базы
    /// </summary>
    internal class SupportAuthorizationProvider(
        IUnitOfWork dbLayer,
        IConnector1CProvider connector1CProvider,
        IAcDbSupportHistoryRegistrator acDbSupportHistoryRegistrator,
        AccountDatabaseSupportModelCreator accountDatabaseSupportModelCreator,
        IServiceProvider serviceProvider,
        ILogger42 logger,
        IChangeAccountDatabaseTemplateProvider changeAccountDatabaseTemplateProvider,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : ISupportAuthorizationProvider
    {
        readonly ILetterNotificationProcessor _letterNotificationProcessor = serviceProvider.GetRequiredService<ILetterNotificationProcessor>();
        readonly Lazy<string> _hotlineEmail = new(Configurations.Configurations.CloudConfigurationProvider.Emails.GetHotlineEmail);

        /// <summary>
        /// Попытыться авторизоваться в информационную базу под администратором.
        /// </summary>
        /// <param name="support">Данные поддержки.</param>
        /// <returns>Признак успешности авторизации</returns>
        public bool TryAdminAuthorize(AcDbSupport support)
        {
            try
            {
                var connectorResult = GetMetadataInfoAccountDatabase(support);

                if (connectorResult.ConnectCode == ConnectCodeDto.PasswordOrLoginIncorrect)
                {
                    support.State = (int)SupportState.NotValidAuthData;
                    dbLayer.AcDbSupportRepository.Update(support);
                    acDbSupportHistoryRegistrator.RegisterAuthIncorrectData(support);
                    return false;
                }

                if (connectorResult.ConnectCode != ConnectCodeDto.Success)
                {
                    support.State = (int)SupportState.AutorizationFail;
                    dbLayer.AcDbSupportRepository.Update(support);
                    dbLayer.Save();
                    acDbSupportHistoryRegistrator.RegisterAuthFail(support, connectorResult.Message);
                    _letterNotificationProcessor
                        .TryNotify<AuthorizationToDatabaseFailedLetterNotification,
                            AuthorizationToDatabaseFailedLetterModelDto
                        >(new AuthorizationToDatabaseFailedLetterModelDto
                        {
                            AccountId = support.AccountDatabase.AccountId,
                            AccountDatabaseCaption = support.AccountDatabase.Caption
                        });

                    return false;
                }

                support.SynonymConfiguration = connectorResult.Result.SynonymConfiguration;
                support.State = (int)SupportState.AutorizationSuccess;
                support.CurrentVersion = connectorResult.Result.Version;
                dbLayer.AcDbSupportRepository.Update(support);
                acDbSupportHistoryRegistrator.RegisterSuccessAuth(support);

                UpdateDatabaseConfiguration(support, connectorResult);

                return true;
            }
            catch (Exception ex)
            {
                var message =
                    $"Произошла ошибка при авторизации для базы данных {support.AccountDatabase.Caption}: {ex.Message}";
                support.State = (int)SupportState.AutorizationFail;
                handlerException.Handle(ex, message);
                dbLayer.Save();
                return false;
            }
        }        

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>        
        public ConnectorResultDto<MetadataResultDto> GetMetadataInfoAccountDatabase(AcDbSupport support)
        {
            logger.Info($"Начинаем получать метаданные для  информационной базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(support.AccountDatabase)}");
            var databaseSupportModel = accountDatabaseSupportModelCreator.CreateModel(support.AccountDatabasesID);

            try
            {
                var metadataInfo = CreateStarter1C(databaseSupportModel).GetMetadata();
                if (metadataInfo.ConnectCode != ConnectCodeDto.Success)
                    throw new InvalidOperationException(metadataInfo.Message);

                logger.Trace("Успешно получены метаданные через 1С Предприятие.");
                return metadataInfo;
            }
            catch (Exception ex)
            {
                logger.Warn($"Не удалось получить метаданные через 1С Предприятие. Причина: {ex.Message}");
            }

            var connectorResult = connector1CProvider.GetMetadataInfo(databaseSupportModel);
            logger.Info($"Получили метаданные {connectorResult.ConnectCode} для  информационной базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(support.AccountDatabase)}");
            return connectorResult;
        }

        /// <summary>
        /// Обновить конфигурацию базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <param name="connectorResult">Метаданные информационной базы</param>
        private void UpdateDatabaseConfiguration(AcDbSupport acDbSupport, ConnectorResultDto<MetadataResultDto> connectorResult)
        {
            try
            {
                var configuration1C = (from conf1C in dbLayer.Configurations1CRepository.WhereLazy()
                                       join variation in dbLayer.ConfigurationNameVariationRepository.WhereLazy() on conf1C.Name equals
                                           variation.Configuration1CName
                                       where variation.VariationName == connectorResult.Result.SynonymConfiguration
                                       select conf1C).FirstOrDefault();

                if (configuration1C == null)
                {
                    logger.Warn($"Для конфигурации '{connectorResult.Result.SynonymConfiguration}' не найдена схема обновлений.");
                    acDbSupport.HasAutoUpdate = false;
                    dbLayer.AcDbSupportRepository.Update(acDbSupport);
                    dbLayer.Save();
                    LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider, LogActions.EditInfoBase,
                    $"Для конфигурации '{connectorResult.Result.SynonymConfiguration}' не найдена схема обновлений. Отключено АО и ТИИ");

                    var acDbSupportHistory = new AcDbSupportHistory
                    {
                        AccountDatabaseId = acDbSupport.AccountDatabasesID,
                        State = SupportHistoryState.Error,
                        Description = $"Автоматическое обновление конфигурации {configuration1C} невозможно, так как она не входит в перечень конфигураций, " +
                                      $"предоставляемых в аренду. Текущую конфигурацию вы можете обновить самостоятельно, воспользовавшись следующей " +
                                      $"<a href=\"https://42clouds.com/ru-ru/manuals/obnovlenie-konfiguracii-bazy-1s/\">Инструкцией</a>.",
                        Operation = DatabaseSupportOperation.AuthToDb,
                        ID = Guid.NewGuid()
                    };
                    dbLayer.AcDbSupportHistoryRepository.Insert(acDbSupportHistory);

                    return;
                }
                logger.Trace($"Получена конфигурация {configuration1C.Name}");

                var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == acDbSupport.AccountDatabasesID);
                var accConf = dbLayer.AccountConfigurationRepository.FirstOrDefault(c=>c.AccountId == accountDatabase.AccountId);
                var dbTemplateConfig = dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                    .FirstOrDefault(x => x.Configuration1CName == configuration1C.Name && x.DbTemplate.Remark != "demo");
                logger.Trace($"Найден тариф ");

                if (dbTemplateConfig != null && accountDatabase.TemplateId != dbTemplateConfig.DbTemplateId)
                {
                    logger.Trace($"Конфигурация Базы {accountDatabase.V82Name} не соответствует указанному шаблону, начинаю менять " +
                        $"\t конфигурация шаблона базы: {dbTemplateConfig.Configuration1CName}");
                    var billingServiceTypeId = dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>()
                        .FirstOrDefault(x => x.Configuration1CName == dbTemplateConfig.Configuration1CName).ServiceTypeId;

                    var rate = dbLayer.RateRepository.FirstOrDefault(x => x.BillingServiceTypeId == billingServiceTypeId).Cost;

                    if (rate != 0 && !accConf.IsVip)
                        SendChangeTemplateLetter(accountDatabase.V82Name, dbTemplateConfig.Configuration1CName);

                    changeAccountDatabaseTemplateProvider.Change(accountDatabase.Id, dbTemplateConfig.DbTemplateId);
                }
                else if (dbTemplateConfig == null)
                {
                    logger.Trace($"Шаблон по конфигурации {configuration1C.Name} не найден");
                    LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider, LogActions.EditInfoBase,
                        $"Для конфигурации '{connectorResult.Result.SynonymConfiguration}' не найден шаблон");
                }

                dbLayer.AcDbSupportRepository.SetConfiguration(acDbSupport, configuration1C);
                dbLayer.Save();

                logger.Trace($"Конфигурация базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(acDbSupport.AccountDatabase)} обновлена. Текущая конфигурация: {configuration1C.Name}");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения обновления конфигурации] базы {acDbSupport.AccountDatabasesID}");
            }
        }

        /// <summary>
        /// Создать класс IStarter1C
        /// </summary>
        /// <returns>Класс IStarter1C</returns>
        private IStarter1C CreateStarter1C(IUpdateDatabaseDto updateDatabase)
            => serviceProvider.GetRequiredService<IStarter1C>().SetUpdateDatabase(updateDatabase);

        // <summary>
        /// Отправить письмо клиенту о низкой версии платформы
        /// </summary>
        private void SendChangeTemplateLetter(string v82Name, string configurationName)
        {
            var message = $"Задача для ведущего менеджера.\r\nКлиент, базу {v82Name}, которая была загружена в \"Чистый шаблон\", " +
                $"подключил к Автоматическому обновлению. Данная конфигурация, а именно {configurationName}, " +
                $"является платной и с клиента начиная со следующего месяца будет взиматься оплата. Необходимо дополнительно " +
                $"уведомить клиента об оплате за использование данной конфигурации.\r\n";

            logger.Trace($"Отправляем письмо об изменении версии платформы на {_hotlineEmail.Value}");
            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(_hotlineEmail.Value)
                .Body(message)
                .Subject($"Изменение конфигурации базы клиента {v82Name}")
                .SendViaNewThread();
        }
    }
}
