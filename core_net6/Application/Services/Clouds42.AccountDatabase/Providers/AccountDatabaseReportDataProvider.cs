﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер для работы с данными инф. базы для отчета
    /// </summary>
    internal class AccountDatabaseReportDataProvider(IUnitOfWork dbLayer) : IAccountDatabaseReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по инф. базам для отчета
        /// </summary>
        /// <returns>Список моделей данных по инф. базам для отчета</returns>
        public List<AccountDatabaseReportDataDto> GetAccountDatabaseReportDataDcs()
        {
            return dbLayer.DatabasesRepository
                .AsQueryableNoTracking()
                .Select(x => new AccountDatabaseReportDataDto
                {
                    AccountNumber = x.Account.IndexNumber, 
                    Caption = x.Caption,
                    DatabaseNumber = x.V82Name,
                    Template = $"{x.DbTemplate.DefaultCaption}({x.DbTemplate.Name})",
                    CreationDate = x.CreationDate,
                    LastActivityDate = x.LastActivityDate,
                    State = x.State,
                    DatabaseType = x.IsFile.HasValue && x.IsFile.Value ? "Файловая база" : "Серверная база",
                    Platform = x.DbTemplate.Platform,
                    PlatformRelease = x.DbTemplate.Platform == "8.3" && x.DistributionType == DistributionType.Stable.ToString() ? x.Account.AccountConfiguration.Segment.Stable83Version :
                        x.DbTemplate.Platform == "8.2" && x.DistributionType == DistributionType.Stable.ToString() ? x.Account.AccountConfiguration.Segment.Stable82Version :
                        x.DbTemplate.Platform == "8.3" && x.DistributionType == DistributionType.Alpha.ToString() ? x.Account.AccountConfiguration.Segment.Alpha83Version:
                        x.DbTemplate.Platform == null && x.DistributionType == DistributionType.Alpha.ToString() ? x.Account.AccountConfiguration.Segment.Alpha83Version : 
                        x.Account.AccountConfiguration.Segment.Stable83Version,
                    DatabaseSize = x.SizeInMB,
                    IsPublished = x.PublishState == PublishState.Published.ToString(),
                    ExternalAccessesToDatabaseCount = x.AcDbAccesses.Count(y => y.AccountUser.AccountId != x.AccountId),
                    InternalAccessesToDatabaseCount = x.AcDbAccesses.Count(y => y.AccountUser.AccountId == x.AccountId),
                    HasSupport = x.AcDbSupport.State == (int)SupportState.AutorizationSuccess && x.AcDbSupport.HasSupport,
                    HasAutoUpdate = x.AcDbSupport.State == (int)SupportState.AutorizationSuccess && x.AcDbSupport.HasAutoUpdate,
                    ZoneNumber = x.AccountDatabaseOnDelimiter.Zone,
                    SourceDbName = x.AccountDatabaseOnDelimiter.AccountDatabase.V82Name
                })
                .OrderByDescending(x => x.CreationDate)
                .ToList();
        }
    }
}
