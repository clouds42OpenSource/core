﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер для работы с информационными базами
    /// </summary>
    internal class AccountDatabaseProvider(
        IUnitOfWork dbLayer,
        IGetServicesExtensionForDatabaseCommand getServicesExtensionForDatabaseCommand)
        : IAccountDatabaseProvider
    {
        /// <summary>
        /// Получить статус информационной базы
        /// </summary>
        /// <param name="id">Id базы</param>
        /// <returns>Статус информационной базы</returns>
        public DatabaseState GetAccountDatabaseStatus(Guid id) =>
            dbLayer.DatabasesRepository.GetAccountDatabase(id)?.StateEnum ??
            throw new NotFoundException($"Не удалось получить статус для информационной базы '{id}'");

        /// <summary>
        /// Получить информационную базу или выкинуть исключение
        /// </summary>
        /// <param name="id">Id информационной базы</param>
        /// <returns>Информационная база</returns>
        public Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid id) =>
            dbLayer.DatabasesRepository.GetAccountDatabase(id) ??
            throw new NotFoundException($"Не удалось получить информационную базу по Id = '{id}'");

        /// <summary>
        /// Получает модель инф. базы по Id
        /// </summary>
        /// <param name="databaseId">Id базы</param>
        /// <param name="accountDatabase">out параметр Модель базы</param>
        /// <returns>Признак нашлась ли база по Id</returns>
        public bool TryGetAccountDatabaseById(Guid databaseId, out Domain.DataModels.AccountDatabase accountDatabase)
        {
            accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == databaseId);

            return accountDatabase != null;
        }

        /// <summary>
        /// Получить информацию  о сервисе в информационной базе
        /// </summary>
        /// <param name="accountDatabaseId">Для какой базы получить информацию о сервисе в ней</param>
        /// <returns>Информацию  о сервисе в информационной базе</returns>
        public async Task<ServiceExtensionDatabaseInfoDto> GetServiceExtensionStatusesForAccountDatabase(
            Guid accountDatabaseId)
        {
            var serviceExtensionDatabase = await getServicesExtensionForDatabaseCommand.Execute(accountDatabaseId);
            
            var result = new ServiceExtensionDatabaseInfoDto { Services = { List = serviceExtensionDatabase.Select(item => new ServiceExtensionDatabaseInfoItemDto
                {
                    ServiceId = item.Id,
                    ServiceExtensionDatabaseStatus = item.ExtensionState.Value,
                    CreationDate = item.ExtensionLastActivityDate.Value,
                    StateDate =  DateTime.MinValue,
                    Error = item.Error
                }).ToList()
            } };

            return result;
        }
    }
}
