﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Providers
{
    /// <summary>
    /// Провайдер по анализу информационных баз.
    /// </summary>
    internal class AccountDatabaseAnalyzeProvider(
        AccountDatabasePathHelper accountDatabasePathHelper,
        IUnitOfWork dbLayer,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        IMemorySizeCalculator memorySizeCalculator,
        ILogger42 logger)
        : IAccountDatabaseAnalyzeProvider
    {
        /// <summary>
        ///     Пересчитать размер информационной базы
        /// </summary>
        /// <param name="database">Информационная база</param>
        public void RecalculateSizeOfAccountDatabase(Domain.DataModels.AccountDatabase database)
        {
            try
            {
                if (database.IsDelimiter())
                    return;

                if (database.StateEnum != DatabaseState.Ready)
                    return;

                var size = database.IsFile == true
                    ? memorySizeCalculator.CalculateFile1CDataBase(database)
                    : memorySizeCalculator.CalculateSqlDataBase(database.SqlName);

                var lastActive = database.IsFile == true ? memorySizeCalculator.CalculateLastActiveDataBase(database) : database.LastActivityDate;
                logger.Info($"[{database.Id}] :: Рассчитанный размер равен: {size} MB");

                database.LastActivityDate = lastActive;
                database.SizeInMB = size;
                database.CalculateSizeDateTime = DateTime.Now;
                dbLayer.DatabasesRepository.Update(database);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                logger.Info($"[{database.Id}] :: Ошибка: {ex.GetFullInfo()}");
            }
        }

        /// <summary>
        ///  Проверка необходимости запуска таски
        /// на удаление или архивацию информационной базы в склеп.
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <param name="stateToChange">статус базы на который надо изменить если базу не надо удалять</param>
        public bool NeedMoveDbToTomb(Domain.DataModels.AccountDatabase database, DatabaseState stateToChange)
        {
            try
            {
                if (CanDeleteToTomb(database)) return true;

                logger.Info($"[{database.Id}] :: Для базы запуск таски не нужен, меняем статус на: {database.State}.");
                accountDatabaseChangeStateProvider.ChangeState(database.Id, stateToChange);
            }
            catch (Exception ex)
            {
                logger.Info($"[{database.Id}] :: Ошибка: {ex.GetFullInfo()}");
            }
            return false;
        }


        /// <summary>
        /// Проверка необходимости запуска таски
        /// на удаление или архивацию информационной базы в склеп.
        /// </summary>
        /// <param name="database">Информационная база.</param>
        /// <returns></returns>
        private bool CanDeleteToTomb(Domain.DataModels.AccountDatabase database)
        {

            if (database.StateEnum != DatabaseState.Ready)
            {
                logger.Info($"[{database.Id}] ::Информационная база не в статусе реди {database.StateEnum.ToString()}");
                return false;
            }
            if (database.IsFile == true)
            {
                var path = accountDatabasePathHelper.GetPath(database);
                if (!Directory.Exists(path))
                {
                    logger.Info($"[{database.Id}] ::Отсутствует директория информационной базы по пути {path}");
                    return false;
                }

                if (!DirectoryHelper.DirectoryHas1CDatabaseFile(path))
                {
                    logger.Info($"[{database.Id}] ::В директории информационной базы по пути {path} не найден файл инфорамционной базы.");
                    return false;
                }
            }
            if (database.IsDelimiter() && database.AccountDatabaseOnDelimiter.Zone == null)
            {
                logger.Info($"[{database.Id}] :: В базе на разделителях нет номера зоны");
                return false;
            }
            return true;
        }
    }
}
