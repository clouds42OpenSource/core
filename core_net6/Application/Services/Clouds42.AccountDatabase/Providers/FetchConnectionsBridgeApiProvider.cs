﻿using System.Net;
using System.Net.Mime;
using System.Text;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.AccountDatabase.Providers
{
    public class FetchConnectionsBridgeApiProvider(
        IHttpClientFactory httpClientFactory,
        IHandlerException handlerException)
        : IFetchConnectionsBridgeApiProvider
    {
        private bool _backgroundMode;
        private readonly Lazy<string> _bridgeUrl = new(CloudConfigurationProvider.WebSocketBridge.GetServiceApiUri);

        /// <summary>
        /// Выполнить обработку ассинхронно.
        /// </summary>
        public IFetchConnectionsBridgeApiProvider ExecuteInBackgroundThread()
        {
            _backgroundMode = true;
            return this;
        }

        /// <summary>
        /// Обаботать команду обновления базы в облаке.
        /// </summary>
        public void UpdateDatabaseHandle(Guid accountDatabaseId)
        {
            ProcessRequest($"{_bridgeUrl.Value}/ChangesAccountDatabase/UpdateDatabaseHandle", new SimpleRequestModelDto<Guid> { Value = accountDatabaseId });
        }

        /// <summary>
        /// Обаботать команду содание новой базы в облаке.
        /// </summary>
        public void CreateDatabaseHandle(Guid accountDatabaseId)
        {
            ProcessRequest($"{_bridgeUrl.Value}/ChangesAccountDatabase/CreateDatabaseHandle", new SimpleRequestModelDto<Guid> { Value = accountDatabaseId });
        }

        /// <summary>
        /// Обаботать команду удаления базы с облака.
        /// </summary>
        public void DeleteDatabaseHandle(Guid accountDatabaseId)
        {
            ProcessRequest($"{_bridgeUrl.Value}/ChangesAccountDatabase/DeleteDatabaseHandle", new SimpleRequestModelDto<Guid> { Value = accountDatabaseId });
        }

        /// <summary>
        /// Обаботать команду предоставления доступа к базе.
        /// </summary>
        public void GrandAccessDatabaseHandle(Guid accountDatabaseId)
        {
            ProcessRequest($"{_bridgeUrl.Value}/ChangesAccountDatabase/GrandAccessDatabaseHandle", new SimpleRequestModelDto<Guid> { Value = accountDatabaseId });
        }

        /// <summary>
        /// Обаботать команду удаления пользовательского доступа у базы.
        /// </summary>
        public void DeleteAccessDatabaseHandle(Guid accountDatabaseId, Guid accountUserId)
        {
            ProcessRequest($"{_bridgeUrl.Value}/ChangesAccountDatabase/DeleteAccessDatabaseHandle", new DeleteAccessDatabaseRequestDto { AccountDatabaseId = accountDatabaseId, AccountUserId = accountUserId });
        }

        /// <summary>
        /// Выполнить запрос.
        /// </summary>
        private void ProcessRequest(string fullUrl, object body)
        {
            if (_backgroundMode)
            {
                try
                {
                    SendRequest(fullUrl, body);
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, "[Ошибка уведомления WebSocketBridge]", () => fullUrl, () => body);
                }
            }
            else
            {
                SendRequest(fullUrl, body);
            }
        }

        /// <summary>
        /// Отправить REST запрос по адресу с объектов в теле.
        /// </summary>
        /// <param name="fullUrl">Адрес получателя запроса.</param>
        /// <param name="body">Объект тела запроса.</param>
        private void SendRequest(string fullUrl, object body)
        {
            try
            {
                var client = httpClientFactory.CreateClient();

                var request = new HttpRequestMessage(HttpMethod.Post, fullUrl)
                {
                    Content = new StringContent(body.ToJson(), Encoding.UTF8, MediaTypeNames.Application.Json)
                };

                var response = client.Send(request);

                if(response.StatusCode == HttpStatusCode.OK)
                    return;

                var error = response.Content.ReadAsStringAsync().Result;
                throw new InvalidOperationException($"Запрос вернул ошибку: {response.StatusCode} :: {error}");

            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    $"Ошибка уведомления сервиса BridgeWebSocket {ex.GetFullInfo(false)} URI:{fullUrl} :: body {body.ToJson()}");
            }
        }
    }
}
