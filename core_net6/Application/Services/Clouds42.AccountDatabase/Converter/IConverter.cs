﻿namespace Clouds42.AccountDatabase.Converter;

public interface IConverter
{
    /// <summary>
    /// Метод конвертации файловой базы в серверную
    /// </summary>
    /// <param name="databaseId"></param>
    /// <param name="accountId"></param>
    /// <param name="accountUserId"></param>
    /// <param name="login"></param>
    /// <param name="password"></param>
    /// <param name="phone"></param>
    /// <param name="isCopy"></param>
    /// <returns></returns>
    public Task ConvertAsync(Guid databaseId, Guid accountId,Guid accountUserId, string login, string password, string phone, bool isCopy);
}
