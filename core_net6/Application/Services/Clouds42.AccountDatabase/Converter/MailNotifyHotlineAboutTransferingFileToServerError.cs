﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Converter;

public class MailNotifyHotlineAboutTransferingFileToServerError : IMailNotificationTemplate<string>
{
    public void SendMail(IUnitOfWork unitOfWork, IAccessProvider accessProvider, string param,
        IAccountConfigurationDataProvider provider)
    {
        var cloudServices = ConfigurationHelper.GetConfigurationValue<string>("cloud-services");

        new Manager42CloudsMail()
            .DisplayName("Команда 42Clouds")
            .To(cloudServices)
            .Subject("Произошла ошибка перевода файловой базы в серверную")
            .Body(param)
            .SendViaNewThread();
    }
}
