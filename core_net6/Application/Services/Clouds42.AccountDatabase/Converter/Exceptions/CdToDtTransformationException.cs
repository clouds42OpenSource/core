﻿namespace Clouds42.AccountDatabase.Convert.Exceptions;

public class CdToDtTransformationException : Exception
{


    public CdToDtTransformationException(string message) : base(message)
    {
        
    }


    public CdToDtTransformationException(string message, Exception innerException) : base(message, innerException)
    {
        
    }
}
