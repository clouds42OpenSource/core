﻿namespace Clouds42.AccountDatabase.Convert.Exceptions;

public class SessionStoppingException : Exception
{
    public SessionStoppingException(string message) : base(message)
    {
        
    }

    public SessionStoppingException(string message, Exception innerException) : base(message, innerException)
    {
        
    }
}
