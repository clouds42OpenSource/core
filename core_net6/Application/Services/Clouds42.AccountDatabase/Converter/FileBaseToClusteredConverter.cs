﻿using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.AccountDatabase.Convert.Exceptions;
using Clouds42.AccountDatabase.Internal;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.Files;
using Clouds42.GoogleCloud;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Tools1C.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Converter;

public class FileBaseToClusteredConverter(
    IUnitOfWork unitOfWork,
    Convert1CDToDtCommand convert1CdToDtCommand,
    MailNotificationFactory mailNotificationFactory,
    IAccountConfigurationDataProvider accountConfigurationDataProvider,
    IRegisterDbOnServerFromDtProvider createDbOnServerFromDtProvider,
    IUploadedFileProvider uploadedFileProvider,
    IGoogleCloudProvider googleCloudProvider,
    ILogger42 logger,
    IAccountDatabasePathHelper accountDatabasePathHelper,
    IArchiveAccountDatabaseToTombProvider archiveAccountDatabaseToTombProvider,
    IAccessProvider accessProvider,
    IStartProcessOfTerminateSessionsInDatabaseProvider startProcessOfTerminateSessionsInDatabaseProvider
)
    : IConverter
{

    /// <summary>
    /// Метод конвертации файловой базы в серверную
    /// </summary>
    /// <param name="databaseId"></param>
    /// <param name="accountId"></param>
    /// <param name="accountUserId"></param>
    /// <param name="login"></param>
    /// <param name="password"></param>
    /// <param name="phone">номер телефона</param>
    /// <param name="isCopy">Является ли копией существующей баызы</param>
    /// <exception cref="InvalidDataException">Ошибка переданных данных</exception>
    /// <exception cref="SessionStoppingException">Ошибка во время остановки сессий</exception>
    /// <exception cref="CdToDtTransformationException">Ошибка в моменте трансформации</exception>
    public async Task ConvertAsync(Guid databaseId,
        Guid accountId,
        Guid accountUserId ,
        string login, 
        string password,
        string phone,
        bool isCopy)
    {
        
        var accountDatabase = await unitOfWork
            .DatabasesRepository
            .AsQueryable()
            .FirstOrDefaultAsync(d => d.Id == databaseId);
        
        if (accountDatabase == null)
        {
            throw new InvalidDataException($"База данных с Id {databaseId} не найдена.");
        }
        
        LogEventHelper.LogEvent(
            unitOfWork,
            accountDatabase.AccountId,
            accessProvider,
            LogActions.FileToClusterTransformationStart, 
            $"Начался перевод файловой базы {accountDatabase.V82Name} в серверную");
        
        try
        {
            logger.Info($"Перевод файловой базы {databaseId}  в серверную : получаю модель базы");
            if (accountDatabase == null)
            {
                var message = $"Перевод файловой базы  в серверную : база данных с Id {databaseId} не найдена.";
                throw new InvalidDataException(message); 
            }
            logger.Info($"Перевод файловой базы {databaseId} в серверную : отключение сессий ");
            startProcessOfTerminateSessionsInDatabaseProvider.TryStartProcess(
                new StartProcessOfTerminateSessionsInDatabaseDto { DatabaseId = databaseId }, out var errorMessage, true);
            logger.Info($"Перевод файловой базы в серверную: закончено отключения сессий базы {databaseId}");

            if (!string.IsNullOrEmpty(errorMessage))
            {
                throw new SessionStoppingException(errorMessage);
            }
            var accountSegment = accountConfigurationDataProvider.GetAccountSegment(accountId);
            bool pathCondition = accountSegment.Name.ToLower().Contains("htznr".ToLower());
            
            var fileId = await CreateUploadedFile(accountId, accountDatabase.V82Name, pathCondition);
            
            var uploadedFile = await unitOfWork.UploadedFileRepository
                .AsQueryable()
                .FirstOrDefaultAsync(up => up.Id == fileId);
            
            if (uploadedFile == null)
            {
                throw new FileNotFoundException($"Не удалось создать найти файл с айди {fileId}");
            }
            
            CreateDirectory(uploadedFile.FullFilePath);
            logger.Info($"Перевод файловой базы в серверную: начинаю создание файла dt из cd базы {databaseId} по пути {uploadedFile.FullFilePath}");
            var result = convert1CdToDtCommand.Execute(databaseId, uploadedFile.FullFilePath, login, password);
            logger.Info($"Перевод файловой базы {databaseId} в серверную: закончено создание файла dt из cd : {result}");
            
            if (result.LogState == LogUpdate1CParser.LogState.Success)
            {
                uploadedFile.Status = UploadedFileStatus.UploadSuccess;
                unitOfWork.UploadedFileRepository.Update(uploadedFile);
                await unitOfWork.SaveAsync();
                var accountFolderPrefix = $"company_{accountDatabase.Account.IndexNumber}";
                var folderPrefix = $"{accountFolderPrefix}/{accountDatabase.V82Name}";
                logger.Info($"Перевод файловой базы {databaseId} в серверную: загрузка dt в гугл клауд");
                string backupPath = googleCloudProvider.UploadFile(uploadedFile.FullFilePath, folderPrefix, true, accountDatabase.V82Name);
                
                await CreateBackupRecord(databaseId, accountUserId, backupPath, folderPrefix);
                
                logger.Info($"Перевод файловой базы в серверную: создание серверной базы {databaseId}");
                createDbOnServerFromDtProvider.CreateClusteredWithDt(accountDatabase, fileId, isCopy);
                logger.Info($"Перевод файловой базы в серверную: закончено создание серверной базы {databaseId}");
            }
            else
            {
                var message = $"Ошибка перевода базы '{accountDatabase.V82Name}' в dt по пути {uploadedFile.FullFilePath}: " + result.LogState;
                throw new CdToDtTransformationException(message);
            }
            
            logger.Info($"Перевод файловой базы в серверную: начало архивации базы {databaseId}");
            archiveAccountDatabaseToTombProvider.DetachAccountDatabaseToTomb(new DeleteAccountDatabaseToTombJobParamsDto()
            {
                AccountDatabaseId = databaseId,
                InitiatorId = accountUserId,
                SendMail = false,
            });
            logger.Info($"Перевод файловой базы в серверную: закончена архивации базы {databaseId}");
            
            LogEventHelper.LogEvent(
                unitOfWork,
                accountDatabase.AccountId,
                accessProvider,
                LogActions.DeleteAccountDatabase, 
                $"Файловая база удалена по пути {accountDatabasePathHelper.GetPath(accountDatabase)} и перенесена в архив");
            
            accountDatabase.IsFile = false;
            accountDatabase.StateEnum = DatabaseState.Ready;
            unitOfWork.DatabasesRepository.Update(accountDatabase);
            await unitOfWork.SaveAsync();

            LogEventHelper.LogEvent(
                unitOfWork,
                accountDatabase.AccountId,
                accessProvider,
                LogActions.FileToClusterTransformationSuccesses, 
                $"Перевод файловой базы {accountDatabase.V82Name} в серверную был успешно завершен");
            
        }
        catch (Exception e)
        {
            if (accountDatabase != null)
            {
                accountDatabase.StateEnum = DatabaseState.Ready;
                unitOfWork.DatabasesRepository.Update(accountDatabase);
                await unitOfWork.SaveAsync();
            }
            var message =
                    $"Перевод в файловую базу не был завершен или был завершен с ошибкой : {e.Message} \n " +
                    $"Номер телефона для связи с пользователем: {phone} Логин : {login} \n Пароль {password} \n Название базы: {accountDatabase?.V82Name} ";
                mailNotificationFactory.SendMail<MailNotifyHotlineAboutTransferingFileToServerError, string>(message);
            
            throw;
        }
    }
    
    /// <summary>
    /// Получить путь для сохранения dt
    /// </summary>
    /// <param name="accountId">айди аккаунта</param>
    /// <param name="v82Name"> название базы</param>
    /// <param name="pathCondition"> условие выбора пути</param>
    /// <returns></returns>
    private async Task<Guid> CreateUploadedFile(Guid accountId, string v82Name, bool pathCondition)
    {
        Guid fileId = pathCondition
            ? await CreateUploadedFileRecord(
                accountId, 
                v82Name,
                CloudConfigurationProvider.AccountDatabase.HtznrSharedStoragePath())
            : uploadedFileProvider
                .CreateUploadedFile(new InitUploadFileRequestDto 
                { 
                    FileName = $"{v82Name}.dt", 
                    AccountId = accountId 
                })
                .Id;
        await unitOfWork.SaveAsync();
        return fileId;
    }
    
    
    /// <summary>
    /// Создать директорию если не было
    /// </summary>
    /// <param name="fullPath"></param>
    /// <exception cref="IOException"></exception>
    private void CreateDirectory(string fullPath)
    {
        string? directoryPath = Path.GetDirectoryName(fullPath);
        if (directoryPath == null)
        {
            throw new IOException($"Неправильная структура пути {fullPath}");
        }

        if (Directory.Exists(directoryPath))
        {
            return;
        }

        logger.Info($"Некоторых папок в {directoryPath} нет, начинаю создание");
        Directory.CreateDirectory(directoryPath);
    }

    /// <summary>
    /// Создать запись о загруженном файле
    /// </summary>
    /// <param name="accountId"></param>
    /// <param name="v82Name"></param>
    /// <param name="fullFilePath"></param>
    /// <returns></returns>
    private async Task<Guid> CreateUploadedFileRecord(Guid accountId, string v82Name, string fullFilePath)
    {
        var id = Guid.NewGuid();
        unitOfWork.UploadedFileRepository.Insert(new UploadedFile()
        {
            Id = id,
            AccountId = accountId,
            FileName = v82Name + ".dt",
            FullFilePath = fullFilePath,
            UploadDateTime = DateTime.Now,
            Comment = $"Файл был создан после конвертации 1cd базы {v82Name}",
            Status = UploadedFileStatus.UploadSuccess,
        });
       await unitOfWork.SaveAsync();
        return id;
    }

    /// <summary>
    /// сделать запись в таблице бекапов
    /// </summary>
    /// <param name="accountDatabaseId"></param>
    /// <param name="accountUserId"></param>
    /// <param name="path"></param>
    /// <param name="folderPrefix"></param>
    private async Task CreateBackupRecord(Guid accountDatabaseId, Guid accountUserId, string path, string folderPrefix)
    {
        unitOfWork.AccountDatabaseBackupRepository.Insert(new AccountDatabaseBackup
        {
            Id = Guid.NewGuid(),
            CreateDateTime = DateTime.Now,
            CreationBackupDateTime = DateTime.Now,
            BackupPath = path,
            FilePath = folderPrefix,
            EventTrigger = CreateBackupAccountDatabaseTrigger.TechnicalBackup,
            SourceType = AccountDatabaseBackupSourceType.GoogleCloud,
            AccountDatabaseId = accountDatabaseId,
            InitiatorId = accountUserId,
        });
        await unitOfWork.SaveAsync();
    }
    
}
