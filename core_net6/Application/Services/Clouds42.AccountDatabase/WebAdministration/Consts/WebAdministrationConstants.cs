﻿namespace Clouds42.AccountDatabase.WebAdministration.Consts
{
    /// <summary>
    /// Константы вею администрирования
    /// </summary>
    public static class WebAdministrationConstants
    {
        /// <summary>
        /// Название поля для пула приложения в объекте PowerShell
        /// </summary>
        public const string PsObjectApplicationPoolFieldName = "applicationPool";
    }
}
