﻿using System.Diagnostics;
using Clouds42.AccountDatabase.Contracts.WebAdministration.Interfaces;
using Clouds42.DataContracts.Service.WebAdministration;
using Clouds42.Common.Models;
using Clouds42.Domain.Enums;
using Clouds42.PowerShellClient;

namespace Clouds42.AccountDatabase.WebAdministration.WebAdministrationCommands
{
    /// <summary>
    /// Базовая команда веб администрирования PowerShell
    /// </summary>
    public abstract class BaseWebAdministrationPowerShellCommand<TParam, TResult>(
        PowerShellClientWrapper powerShellClientWrapper) : IWebAdministrationPowerShellCommand<TParam, TResult>
    {
        protected readonly PowerShellClientWrapper PowerShellClientWrapper = powerShellClientWrapper;

        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <param name="contentServerNodeAddress">Адрес ноды публикации</param>
        /// <returns>Результат выполнения</returns>
        protected abstract CommandExecutionResultDto<TResult> Execute(TParam commandParams, string contentServerNodeAddress);

        /// <summary>
        /// Получить тип операции для измерения времени выполнения
        /// </summary>
        /// <returns>Тип операции для измерения времени выполнения</returns>
        protected abstract ExecutionTimeOperationsEnum GetExecutionTimeOperationType();

        /// <summary>
        /// Выполнить команду и зафиксировать время ее выполнения
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <param name="contentServerNodeAddress">Адрес ноды публикации</param>
        /// <returns>Результат выполнения</returns>
        public CommandExecutionResultDto<TResult> ExecuteWithCountingExecutionTime(TParam commandParams, string contentServerNodeAddress)
        {
            var stopWatch = Stopwatch.StartNew();
            var commandResult = Execute(commandParams, contentServerNodeAddress);
            stopWatch.Stop();

            commandResult.TraceRuntimeData = new TraceRuntimeDataDc
            {
                ElapsedSeconds = stopWatch.Elapsed.Seconds,
                ExecutionTimeOperation = GetExecutionTimeOperationType()
            };

            return commandResult;
        }

        /// <summary>
        /// Вернуть объект успешного результата выполнения команды
        /// </summary>
        /// <param name="contentServerNodeAddress">Адрес ноды публикации</param>
        /// <param name="result">Результат выполнения</param>
        /// <returns>Объект успешного результата выполнения команды</returns>
        protected CommandExecutionResultDto<TResult> ExecutionSuccess(string contentServerNodeAddress, TResult result)
            => new()
            {
                Result = result,
                ServerName = contentServerNodeAddress,
                IsSuccess = true
            };

        /// <summary>
        /// Вернуть объект не успешного результата выполнения команды
        /// </summary>
        /// <param name="contentServerNodeAddress">Адрес ноды публикации</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Объект не успешного результата выполнения команды</returns>
        protected CommandExecutionResultDto<TResult> ExecutionFailed(string contentServerNodeAddress, string errorMessage)
            => new()
            {
                ServerName = contentServerNodeAddress,
                IsSuccess = false,
                ErrorMessage = errorMessage
            };
    }
}
