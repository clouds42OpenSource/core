﻿using Clouds42.AccountDatabase.WebAdministration.Consts;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.WebAdministration;
using Clouds42.Domain.Enums;
using Clouds42.PowerShellClient;

namespace Clouds42.AccountDatabase.WebAdministration.WebAdministrationCommands
{
    /// <summary>
    /// Команда для получения названия пула
    /// </summary>
    public class GetAppPoolNameCommand(PowerShellClientWrapper powerShellClientWrapper)
        : BaseWebAdministrationPowerShellCommand<GetAppPoolNameCommandParamsDto, string>(powerShellClientWrapper)
    {
        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <param name="contentServerNodeAddress">Адрес ноды публикации</param>
        protected override CommandExecutionResultDto<string> Execute(GetAppPoolNameCommandParamsDto commandParams,
            string contentServerNodeAddress)
        {
            try
            {
                var executionResult = PowerShellClientWrapper.ExecuteScript(
                    $@"Import-Module WebAdministration; Get-WebApplication -Site ""{commandParams.PublishSiteName}"" -Name ""{commandParams.AcDbPublishPath}""",
                    contentServerNodeAddress);

                var publishSiteProperties = executionResult.FirstOrDefault()?.Properties
                                            ?? throw new InvalidOperationException(
                                                $"Приложение инф. базы {commandParams} на сервере публикации {commandParams.PublishSiteName} не найдено");

                var applicationPoolName =
                    (string)publishSiteProperties[WebAdministrationConstants.PsObjectApplicationPoolFieldName]?.Value;

                if (string.IsNullOrEmpty(applicationPoolName))
                    throw new InvalidOperationException(
                        $"Не удалось определить пул приложения инф. базы {commandParams} на сервере публикации {commandParams.PublishSiteName}");

                return ExecutionSuccess(contentServerNodeAddress, applicationPoolName);
            }
            catch (Exception ex)
            {
                return ExecutionFailed(contentServerNodeAddress, ex.GetFullInfo(false));
            }
        }

        /// <summary>
        /// Получить тип операции для измерения времени выполнения
        /// </summary>
        /// <returns>Тип операции для измерения времени выполнения</returns>
        protected override ExecutionTimeOperationsEnum GetExecutionTimeOperationType() =>
            ExecutionTimeOperationsEnum.GetApplicationPool;
    }
}