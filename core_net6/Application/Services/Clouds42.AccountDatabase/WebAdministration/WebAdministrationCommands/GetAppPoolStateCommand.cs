﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.WebAdministration;
using Clouds42.Domain.Enums;
using Clouds42.PowerShellClient;
using Clouds42.PowerShellClient.Extensions;

namespace Clouds42.AccountDatabase.WebAdministration.WebAdministrationCommands
{
    /// <summary>
    /// Команда для получения состояния пулов
    /// </summary>
    public class GetAppPoolStateCommand(PowerShellClientWrapper powerShellClientWrapper)
        : BaseWebAdministrationPowerShellCommand<string, List<AppPoolStateDto>>(powerShellClientWrapper)
    {
        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <param name="contentServerNodeAddress">Адрес ноды публикации</param>
        /// <returns>Список моделей состояния пула на нодах публикации</returns>
        protected override CommandExecutionResultDto<List<AppPoolStateDto>> Execute(string commandParams,
            string contentServerNodeAddress)
        {
            try
            {
                var executionResult = PowerShellClientWrapper.ExecuteScript($@"
                    Import-Module WebAdministration;

                    $appPool = Get-IISAppPool ""{commandParams}""; 
                    [PSCustomObject]@{{                        
                        AppPoolName = $appPool.Name; 
                        PoolState = $appPool.State.ToString();
                        WorkerProcessesCount = $appPool.WorkerProcesses.Count
                }}", contentServerNodeAddress);

                return ExecutionSuccess(contentServerNodeAddress, executionResult
                    .Select(psObject => psObject.Properties.DeserializeFromPowerShell<AppPoolStateDto>())
                    .ToList());
            }
            catch (Exception ex)
            {
                return ExecutionFailed(contentServerNodeAddress, ex.GetFullInfo(false));
            }
        }

        /// <summary>
        /// Получить тип операции для измерения времени выполнения
        /// </summary>
        /// <returns>Тип операции для измерения времени выполнения</returns>
        protected override ExecutionTimeOperationsEnum GetExecutionTimeOperationType() =>
            ExecutionTimeOperationsEnum.GetApplicationPoolState;
    }
}