﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.WebAdministration;
using Clouds42.Domain.Enums;
using Clouds42.PowerShellClient;

namespace Clouds42.AccountDatabase.WebAdministration.WebAdministrationCommands
{
    /// <summary>
    /// Запусить пул приложения на IIS
    /// </summary>
    public class StartAppPoolCommand(PowerShellClientWrapper powerShellClientWrapper)
        : BaseWebAdministrationPowerShellCommand<string, bool>(powerShellClientWrapper)
    {
        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <param name="contentServerNodeAddress">Адрес ноды публикации</param>
        protected override CommandExecutionResultDto<bool> Execute(string commandParams, string contentServerNodeAddress)
        {
            try
            {
                PowerShellClientWrapper.ExecuteScript($@"
                    Import-Module WebAdministration;

                    $appPoolName = ""{commandParams}"";                                       

                    while((Get-WebAppPoolState -Name $appPoolName).Value -eq 'Stopping'){{
                        Start-Sleep -s 1;
                    }}

                    if((Get-WebAppPoolState -Name $appPoolName).Value -ne 'Started'){{
                        Start-WebAppPool -Name $appPoolName
                    }}

                    while((Get-WebAppPoolState -Name $appPoolName).Value -ne 'Started'){{
                        Start-Sleep -s 1;
                    }}

                ", contentServerNodeAddress);

                return ExecutionSuccess(contentServerNodeAddress, true);
            }
            catch (Exception ex)
            {
                return ExecutionFailed(contentServerNodeAddress, ex.GetFullInfo(false));
            }
        }

        /// <summary>
        /// Получить тип операции для измерения времени выполнения
        /// </summary>
        /// <returns>Тип операции для измерения времени выполнения</returns>
        protected override ExecutionTimeOperationsEnum GetExecutionTimeOperationType() =>
            ExecutionTimeOperationsEnum.StartApplicationPool;
    }
}
