﻿using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.WebAdministration.Models
{
    /// <summary>
    /// Модель операции информационной базы
    /// </summary>
    public class DatabaseOperationModel
    {
        /// <summary>
        /// Операция
        /// </summary>
        public ExecutionTimeOperationsEnum OperationsEnum { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid DatabaseId { get; set; }
    }
}
