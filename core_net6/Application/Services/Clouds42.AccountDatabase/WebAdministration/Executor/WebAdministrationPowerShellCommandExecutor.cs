﻿using Clouds42.AccountDatabase.Contracts.WebAdministration.Interfaces;
using Clouds42.DataContracts.Service.WebAdministration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.WebAdministration.Executor
{
    /// <summary>
    /// Исполнитель команд веб администрирования через PowerShell
    /// </summary>
    internal class WebAdministrationPowerShellCommandExecutor(IServiceProvider container)
        : IWebAdministrationPowerShellCommandExecutor
    {
        /// <summary>
        /// Выполнить команду в нескольких потоках
        /// </summary>
        /// <typeparam name="TParam">Тип параметров команды</typeparam>
        /// <typeparam name="TResult">Тип результата</typeparam>
        /// <typeparam name="TCommand">Тип команды</typeparam>
        /// <param name="contentServerNodes">Ноды сервера публикаций</param>
        /// <param name="commandParam">Параметры команды</param>
        /// <returns>Результат выполнения</returns>
        public List<CommandExecutionResultDto<TResult>> ExcecuteCommandInMultipleThreads<TCommand, TParam, TResult>(IEnumerable<string> contentServerNodes,
            TParam commandParam)
            where TCommand : IWebAdministrationPowerShellCommand<TParam, TResult>
        {
            var tasksList = contentServerNodes
                .Select(serverNode => Task.Factory.StartNew(() =>
                {
                    using var scope = container.CreateScope();
                    return scope.ServiceProvider.GetRequiredService<TCommand>()
                        .ExecuteWithCountingExecutionTime(commandParam, serverNode);
                }))
                .ToList();

            return Task.WhenAll(tasksList).GetAwaiter().GetResult().ToList();
        }
    }
}