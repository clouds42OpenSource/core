﻿using System.Text.RegularExpressions;
using Clouds42.Configurations.Configurations;
using Clouds42.Validators.Models;

namespace Clouds42.AccountDatabase.Validators
{
    /// <summary>
    /// Валидатор номера информационной базы
    /// </summary>
    public static class AccountDbV82NameValidator
    {
        //Используются только английские маленькие буквы, цифры и знак _
        private const string RegxString = @"^[a-z0-9_]+$";

        private static readonly int LengthV82Name = CloudConfigurationProvider.AccountDatabase.LengthV82NameAccountDatabase();

        /// <summary>
        /// Регулярное выражение для валидации номера базы
        /// </summary>
        public static Regex DbV82NameRegex { get; } = new(RegxString);

        /// <summary>
        /// Провалидировать номер ИБ
        /// </summary>
        /// <param name="v82Name">Номер базы</param>
        /// <returns></returns>
        public static ValidateResultModel Validate(string v82Name)
        {
            if (string.IsNullOrEmpty(v82Name))
                return new ValidateResultModel { Success = false, Message = "Номер базы не может быть пустым" };

            if (v82Name.Length > LengthV82Name)
                return new ValidateResultModel { Success = false, Message = "Длина номера превышает 20 символов" };

            if (DbV82NameRegex.IsMatch(v82Name))
                return new ValidateResultModel { Success = true, Message = "Валидация прошла успешна" };

            var erroneousCharacters = v82Name.Where(c => !DbV82NameRegex.IsMatch(c.ToString())).ToArray();
            return new ValidateResultModel { Success = false, Message = "В номере БД указаны недопустимые символы " + string.Join(" ", erroneousCharacters) };
        }
    }
}
