﻿using System.Text.RegularExpressions;
using Clouds42.Validators.Models;

namespace Clouds42.AccountDatabase.Validators
{
    /// <summary>
    /// Валидатор названия информационной базы
    /// </summary>
    public static class AccountDbCaptionValidator
    {
        //Используются буквы Украинской локали ІіЇїЄєҐґ
        private const string RegxString = @"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z0-9\-_.\(\):, ]+$";

        /// <summary>
        /// Регулярное выражение для валидации названия базы
        /// </summary>
        public static Regex DbCaptionRegex { get; } = new(RegxString);

        /// <summary>
        /// Максимальная длина названия базы
        /// </summary>
        public static int MaxLengthDbCaption => 100;

        /// <summary>
        /// Провалидировать название ИБ
        /// </summary>
        /// <param name="caption">Название базы</param>
        /// <returns></returns>
        public static ValidateResultModel Validate(string caption)
        {
            if (string.IsNullOrEmpty(caption))
                return new ValidateResultModel {Success = false, Message = "Поле \"Название\" должно быть заполнено"};

            if (caption.Length > MaxLengthDbCaption)
                return new ValidateResultModel
                    {Success = false, Message = $"Поле \"Название\" должно содержать до {MaxLengthDbCaption} символов"};

            if (DbCaptionRegex.IsMatch(caption))
                return new ValidateResultModel {Success = true, Message = "Валидация прошла успешна"};

            var erroneousCharacters = caption.Where(c => !DbCaptionRegex.IsMatch(c.ToString())).ToArray();
            return new ValidateResultModel { Success = false, Message = "В названии БД указаны недопустимые символы " + string.Join(" ", erroneousCharacters) };
        }
    }
}
