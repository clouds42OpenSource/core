﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.StateMachine.Contracts.ServiceManagerProcessFlows;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Register.CreateDatabaseOnDelimiterFromZip
{
    /// <summary>
    /// Класс регистрации баз данных аккаунта на сервере разделителей.
    /// </summary>
    internal class RegisterDatabaseFromZipProvider(
        IServiceProvider serviceProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        ICreateDatabaseFromZipInMsProcessFlow createDatabaseFromZipInMsProcessFlow,
        IHandlerException handlerException)
        : RegisterDatabaseOnServerBase<RegisterDatabaseFromZipModelDto>(serviceProvider),
            IRegisterDatabaseFromZipProvider
    {
        /// <summary>
        /// Зарегистрировать базу на сервере
        /// </summary>
        /// <param name="model">Модель регистрации инф. базы из zip</param>
        protected override void Register(RegisterDatabaseFromZipModelDto model)
        {
            try
            {
                var database = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                RegisterInServiceManager(model, database);
            }
            catch (Exception e)
            {
                HandlerException.Handle(e, $"Ошибка регистрации информационной базы из zip {model.AccountDatabaseId} ");
            }
        }

        /// <summary>
        /// Зарегистрировать инф. базу в МС
        /// </summary>
        /// <param name="model">Список пользователей инф. базы</param>
        /// <param name="accountDatabase">инф. база</param>
        private void RegisterInServiceManager(RegisterDatabaseFromZipModelDto model,
            Domain.DataModels.AccountDatabase accountDatabase)
        {
            Logger.Info($"Добавляем новую область для базы {accountDatabase.Id} созданную из zip");

            try
            {

                CreateAcDbAccessesForDatabase(model.ListInfoAboutUserFromZipPackage.Select(u => u.AccountUserId).Distinct().ToList(),
                    accountDatabase);

                var uploadedFile =
                    DbLayer.UploadedFileRepository.FirstOrDefault(f => f.Id == model.UploadedFileId) ?? throw new InvalidOperationException(
                        $"Не найден загруженный файл zip архива по идентификатору '{model.UploadedFileId}'");
                Logger.Info(
                    $"Начало отправки запроса на создание области для базы {accountDatabase.Id} созданную из zip");

                createDatabaseFromZipInMsProcessFlow.Run(new CreateDatabaseFromZipInMsParamsDto
                {
                    AccountDatabaseId = accountDatabase.Id,
                    AccountId = accountDatabase.AccountId,
                    AccountDatabaseCaption = accountDatabase.Caption,
                    TemplateId = accountDatabase.DbTemplate.Id,
                    FullFilePath = uploadedFile.FullFilePath,
                    UserFromZipPackage = model.ListInfoAboutUserFromZipPackage,
                    UploadedFileId = uploadedFile.Id,
                    ExtensionsList = model.ExtensionsList
                });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка регистрации инф. базы в МС] {accountDatabase.Id}");
                throw;
            }
        }

        /// <summary>
        /// Выбрать список Id пользователей, для создания доступов в базу
        /// </summary>
        /// <param name="accountUserIds">Список Id пользователей, для выбора</param>
        /// <param name="databaseId">Id базы</param>
        /// <returns>Список Id пользователей, для создания доступов в базу</returns>
        private List<Guid> SelectUsersIdsForCreateAcDbAccess(List<Guid> accountUserIds, Guid databaseId)
        {
            return DbLayer.AccountUsersRepository
                .AsQueryableNoTracking()
                .Include(x => x.AcDbAccesses)
                .Where(x => accountUserIds.Contains(x.Id) && x.AcDbAccesses.All(z => z.AccountDatabaseID != databaseId))
                .Select(x => x.Id)
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// Создать доступы в информационную базу
        /// </summary>
        /// <param name="accountUserIds">Список Id пользователей</param>
        /// <param name="accountDatabase">Информационная база</param>
        private void CreateAcDbAccessesForDatabase(List<Guid> accountUserIds, IAccountDatabase accountDatabase)
        {
           var acDbAccess = SelectUsersIdsForCreateAcDbAccess(accountUserIds,
                    accountDatabase.Id).Select(userId => CreateAcDbAccessRow(userId, accountDatabase)).ToList();

           DbLayer.BulkInsert(acDbAccess);
           DbLayer.Save();
        }

        /// <summary>
        /// Создать объект AcDbAccess
        /// </summary>
        /// <param name="accountUserId">Id пользователя с правами админа</param>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Объект AcDbAccess</returns>
        private static AcDbAccess CreateAcDbAccessRow(Guid accountUserId, IAccountDatabase accountDatabase) =>
            new()
            {
                AccountDatabaseID = accountDatabase.Id,
                AccountID = accountDatabase.AccountId,
                LocalUserID = Guid.Empty,
                AccountUserID = accountUserId,
                ID = Guid.NewGuid()
            };
    }
}
