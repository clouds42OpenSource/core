﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.AccountDatabase.Internal;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal.Exceptions;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Logger;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.Register.CreateDatabaseFromDt;

/// <summary>
/// Класс регистрации базы созданной из dt
/// </summary>
internal class RegisterDatabaseFromDtProvider(
    IServiceProvider serviceProvider,
    IRegisterTaskInQueueProvider coreWorkerTasksQueueManager,
    IAccountDatabaseDataProvider accountDatabaseDataProvider,
    IChangeAccountDatabaseTemplateProvider changeAccountDatabaseTemplateProvider,
    DatabaseStatusHelper databaseStatusHelper,
    IGetFullPathForDtFileCommand getFullPathForDtFileCommand,
    IAccountConfigurationDataProvider accountConfigurationDataProvider,
    ILogger42 logger)
    : RegisterDatabaseOnServerBase<RegisterDatabaseFromUploadFileModelDto>(serviceProvider),
        IRegisterDbOnServerFromDtProvider
{
    /// <summary>
    /// Класс регистрации базы созданной из dt
    /// </summary>
    protected override void Register(RegisterDatabaseFromUploadFileModelDto model)
    {
        var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);
        var accountDatabasePath = AccountDatabasePathHelper.GetPath(accountDatabase);
        try
        {

            if (!accountConfigurationDataProvider.NeedAutoCreateClusteredDb(accountDatabase.AccountId) && !model.CreateClusterForce)
            {
                logger.Info($"Начинаю создание и регистрацию файловой базы {accountDatabase.Id} из DT" );
                LoadSqlTemplate(accountDatabase, accountDatabasePath);
                LoadDt(accountDatabase, accountDatabasePath, model.UploadedFileId); 
                SetAdAccessForAccountFolder(accountDatabase.AccountId);
            }
            else
            {
                logger.Info($"Начинаю создание и регистрацию серверной базы {accountDatabase.Id} из DT");
                CreateClusteredWithDt(accountDatabase, model.UploadedFileId);
            }
                
            if (model.TemplateId.HasValue)
            {
                changeAccountDatabaseTemplateProvider.Change(accountDatabase.Id, model.TemplateId.Value);
            }
            FinalizeRegister(accountDatabase, true, model.UsersIdListForAddAccess);
            coreWorkerTasksQueueManager.RegisterTask(taskType: CoreWorkerTaskType.DatabaseRegularOperations,
                comment: "RecalculateSingleDbSize",
                parametrizationModel: new ParametrizationModelDto(model.AccountDatabaseId));
        }
        catch (NotEnoughSpaceException ex)
        {
            Logger.Error($"Ошибка загрузки базы из dt файла {accountDatabase.Id}:: {ex.Message}");
            databaseStatusHelper.SetDbStatus(accountDatabase.V82Name, DatabaseState.ErrorNotEnoughSpace);
            throw; 
        }
        catch (Exception ex)
        {
            Logger.Error($"Ошибка загрузки базы из dt файла {accountDatabase.Id}:: {ex.Message}");
            databaseStatusHelper.SetDbStatus(accountDatabase.V82Name, DatabaseState.ErrorDtFormat);
            throw;
        }
            
    }

    /// <summary>
    /// Загрузка дт 
    /// </summary>
    private void LoadDt(IAccountDatabase accountDatabase, string accountDatabasePath, Guid uploadedFileId)
    {
        var uploadedFile = DbLayer.UploadedFileRepository.FirstOrDefault(f => f.Id == uploadedFileId);
        var loadDtCommand = ServiceProvider.GetRequiredService<LoadDtCommand>();

        Logger.Info($"Начало загрузки дт для базы {accountDatabase.Id}");

        loadDtCommand.Execute(new DatabaseInfoDto
        {
            Name = accountDatabase.Caption,
            V82Name = accountDatabase.V82Name,
            Platform = accountDatabase.PlatformType,
            IsFile = accountDatabase.IsFile == true,
            FilePath = accountDatabasePath,
            AccountId = accountDatabase.AccountId
        }, uploadedFile == null? getFullPathForDtFileCommand.Execute(uploadedFileId, accountDatabase.AccountId) : uploadedFile.FullFilePath);
    }

    /// <summary>
    /// Создание серверной базы с dt
    /// </summary>
    /// <param name="accountDatabase"></param>
    /// <param name="uploadedFileId"></param>
    /// <param name="isCopy"></param>
    public void CreateClusteredWithDt(IAccountDatabase accountDatabase, Guid uploadedFileId, bool isCopy = false)
    {
        var uploadedFile = DbLayer.UploadedFileRepository.FirstOrDefault(f => f.Id == uploadedFileId);
        Logger.Info($"Начало создания кластера и загрузки дт для базы {accountDatabase.Id}");
        var loadDtCommand = ServiceProvider.GetRequiredService<CreateClusteredDatabaseCommand>();
        var uploadedFilePath = uploadedFile == null ? getFullPathForDtFileCommand.Execute(uploadedFileId, accountDatabase.AccountId) : uploadedFile.FullFilePath;

        try
        {
            loadDtCommand.Execute(
                accountDatabase.Id,
                uploadedFilePath);

            DeleteUploadedDtFile(uploadedFilePath);
        }
        catch (Exception ex)
        {
            
            Logger.Error($"Ошибка загрузки базы из dt файла {accountDatabase.Id}:: {ex.Message}");
            databaseStatusHelper.SetDbStatus(accountDatabase.V82Name, DatabaseState.ErrorDtFormat);
            DeleteUploadedDtFile(uploadedFilePath);
            throw;
        }
        
    }

    /// <summary>
    /// Удалить загруженный файл Dt
    /// </summary>
    /// <param name="uploadedDtFilePath">Путь к загруженному файлу Dt</param>
    private void DeleteUploadedDtFile(string uploadedDtFilePath)
    {
        File.Delete(uploadedDtFilePath);
    }
}
