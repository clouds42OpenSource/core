﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Register.CreateDatabaseFromTemplate
{

    /// <summary>
    /// Класс регистрации базы созданной из шаблона.
    /// </summary>
    internal class RegisterDatabaseFromTemplateProvider(
        IServiceProvider serviceProvider,
        IRegisterTaskInQueueProvider coreWorkerTasksQueueManager,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccessProvider accessProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider, 
        ILogger42 logger): RegisterDatabaseOnServerBase<RegisterFileDatabaseFromTemplateModelDto>(serviceProvider), IRegisterDatabaseFromTemplateProvider
    {
        /// <summary>
        /// Зарегистрировать файловую базу данных на сервере из шаблона.
        /// </summary>
        protected override void Register(RegisterFileDatabaseFromTemplateModelDto model)
        {
            var accountDatabase =
            accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);
            var databaseFullName =
            AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase.Caption, accountDatabase.V82Name);
            
            try
            {
                var accountDatabasePath = AccountDatabasePathHelper.GetPath(accountDatabase);

                if (!accountConfigurationDataProvider.NeedAutoCreateClusteredDb(accountDatabase.AccountId))
                {
                    logger.Info($"Начинаю создание и регистрацию файловой базы {accountDatabase.Id} из шаблона");
                    LoadSqlTemplate(accountDatabase, accountDatabasePath);
                    SetAdAccessForAccountFolder(accountDatabase.AccountId);
                }
                else
                {
                    logger.Info($"Начинаю создание и регистрацию серверной базы {accountDatabase.Id} из шаблона");
                    CreateClusteredDatabase(accountDatabase); 
                }

                FinalizeRegister(accountDatabase, model.NeedPublish, model.UsersIdsForAddAccess);

                coreWorkerTasksQueueManager.RegisterTask(
                    taskType: CoreWorkerTaskType.DatabaseRegularOperations,
                    comment: "RecalculateSingleDbSize",
                    parametrizationModel: new ParametrizationModelDto(model.AccountDatabaseId));
            }
            catch (Exception ex)
            {
                LogEventHelper.LogEvent(DbLayer, accountDatabase.AccountId, accessProvider, LogActions.AddInfoBase,
                $"Ошибка создания новой базы {databaseFullName}:: {ex.Message}");
                throw;
            }
        }
        
        private void CreateClusteredDatabase(IAccountDatabase accountDatabase)
        {
            Logger.Info($"Начало создания кластера для базы {accountDatabase.Id}");
            var loadDtCommand = ServiceProvider.GetRequiredService<CreateClusteredDatabaseCommand>();

            loadDtCommand.Execute(accountDatabase.Id);
        }
    }
}
