﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Register
{

    /// <summary>
    /// Класс регистрации баз данных аккаунта на сервере
    /// </summary>
    internal abstract class RegisterDatabaseOnServerBase<TParamsModel>(IServiceProvider serviceProvider)
        where TParamsModel : RegisterDatabaseModelDto
    {
        protected readonly IServiceProvider ServiceProvider = serviceProvider;
        protected readonly ILogger42 Logger = serviceProvider.GetRequiredService<ILogger42>();
		protected readonly IUnitOfWork DbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
		protected IHandlerException HandlerException = serviceProvider.GetRequiredService<IHandlerException>();
		protected AccountDatabasePathHelper AccountDatabasePathHelper = serviceProvider.GetRequiredService<AccountDatabasePathHelper>();
        protected readonly DatabaseStatusHelper DatabaseStatusHelper = serviceProvider.GetRequiredService<DatabaseStatusHelper>();

        private readonly ILetterNotificationProcessor _letterNotificationProcessor = serviceProvider.GetRequiredService<ILetterNotificationProcessor>();
        private readonly IAcDbAccessProvider _acDbAccessProvider = serviceProvider.GetRequiredService<IAcDbAccessProvider>();
        private readonly AccountDatabasePublishManager _databasePublishManager = serviceProvider.GetRequiredService<AccountDatabasePublishManager>();
        private readonly IServiceStatusHelper _serviceStatusHelper = serviceProvider.GetRequiredService<IServiceStatusHelper>();
        private readonly IFetchConnectionsBridgeApiProvider _fetchConnectionsBridgeApiProvider = serviceProvider.GetRequiredService<IFetchConnectionsBridgeApiProvider>();
        private readonly IEmailAddressesDataProvider _emailAddressesDataProvider = serviceProvider.GetRequiredService<IEmailAddressesDataProvider>();

        /// <summary>
        /// Зарегистрировать информационную базу.
        /// </summary>
        public void RegisterDatabaseOnServer(TParamsModel model)
        {
            Register(model);
            _fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().CreateDatabaseHandle(model.AccountDatabaseId);
        }

        /// <summary>
        /// Зарегистрировать информационную базу.
        /// </summary>
        protected abstract void Register(TParamsModel model);

        /// <summary>
        /// Заврешить процесс регистрации
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="needPublishDatabase">Признак необходимости публиковать базу</param>
        protected void FinalizeRegister(IAccountDatabase accountDatabase, bool needPublishDatabase)
        {
            var accountAdminId = DbLayer.AccountsRepository.GetAccountAdminIds(accountDatabase.AccountId).FirstOrDefault();

            AddAccess(accountAdminId, accountDatabase.AccountId, accountDatabase.Id);

            FinalizeRegisterProcess(accountDatabase, needPublishDatabase);
        }

        /// <summary>
        /// Заврешить процесс регистрации
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="needPublishDatabase">Признак необходимости публиковать базу</param>
        /// <param name="usersIdsForAddAccess">Список ID пользователей для предоставления доступа</param>
        protected void FinalizeRegister(IAccountDatabase accountDatabase, bool needPublishDatabase, List<Guid> usersIdsForAddAccess)
        {
            usersIdsForAddAccess.ForEach(userId =>
            {
                AddAccess(userId, accountDatabase.AccountId, accountDatabase.Id);
            });
            FinalizeRegisterProcess(accountDatabase, needPublishDatabase);
        }

        /// <summary>
		/// Загрузка шаблона 
		/// </summary>
        protected void LoadSqlTemplate(IAccountDatabase accountDatabase, string accountDatabasePath)
        {
            var dbTemplate = DbLayer.DbTemplateRepository.FirstOrThrowException(t => t.Id == accountDatabase.TemplateId,
                $"По номеру '{accountDatabase.TemplateId}' не найден шаблон");

            Logger.Info($"Начало загрузки шаблона {dbTemplate.Name} для базы {accountDatabase.Id}");

            var loadSqlTemplateCommand = ServiceProvider.GetRequiredService<LoadSqlTemplateCommand>();

            loadSqlTemplateCommand.Execute(new DatabaseInfoDto
            {
                Name = accountDatabase.Caption,
                V82Name = accountDatabase.V82Name,
                IsFile = accountDatabase.IsFile == true,
                Platform = accountDatabase.PlatformType,
                FilePath = accountDatabasePath
            }, dbTemplate);
	    }

        /// <summary>
        /// Заврешить процесс регистрации
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="needPublishDatabase">Признак необходимости публиковать базу</param>
        private void FinalizeRegisterProcess(IAccountDatabase accountDatabase, bool needPublishDatabase)
        {
            DatabaseStatusHelper.SetDbStatus(accountDatabase.V82Name, DatabaseState.Ready);

            if (needPublishDatabase)
                _databasePublishManager.PublishDatabaseWithWaiting(accountDatabase.Id);

            SendNotificationToClient(accountDatabase.Id);
        }

        /// <summary>
        /// Отправка сообщения клиенту, о завершении создания базы
        /// </summary>
        /// <param name="accountDatabaseId">информационная база</param>
        private void SendNotificationToClient(Guid accountDatabaseId)
        {
            Logger.Info($"Начало отправки письма клиенту о создании ИБ {accountDatabaseId}");

            var accountDatabase = ServiceProvider.GetRequiredService<IAccountDatabaseDataProvider>()
                .GetAccountDatabaseOrThrowException(accountDatabaseId);

            var acDbPublishPath = ServiceProvider.GetRequiredService<AccountDatabaseWebPublishPathHelper>()
                .GetWebPublishPath(accountDatabase);

            var emailsToCopy = _emailAddressesDataProvider.GetAllAccountEmailsToSend(accountDatabase.AccountId);

            _letterNotificationProcessor
                .TryNotify<CreateAccountDatabaseLetterNotification, CreateAccountDatabaseLetterModelDto>(
                    new CreateAccountDatabaseLetterModelDto
                    {
                        AccountId = accountDatabase.AccountId,
                        AccountDatabaseCaption = accountDatabase.Caption,
                        AccountDatabasePublishPath = acDbPublishPath,
                        EmailsToCopy = emailsToCopy
                    });

            Logger.Info($"Сообщение отправлено клиенту о создании ИБ {accountDatabaseId}");
        }

        /// <summary>
        /// Возвращает состояние Аренды 1С.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>true если Аренда 1С заблокированна и false если активна.</returns>
        protected bool CheckRent1CIsLocked(Guid accountId)
        {
            var resConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            if (resConfig == null)
            {
                return true;
            }

            return _serviceStatusHelper.CreateModelForRentAndMyDisk(accountId).ServiceIsLocked;
        }

        /// <summary>
        /// Установить доступ к папке в AD
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        protected void SetAdAccessForAccountFolder(Guid accountId)
        {
            var account = ServiceProvider.GetRequiredService<IAccountDataProvider>().GetAccountOrThrowException(accountId);

            var fileStorage = ServiceProvider.GetRequiredService<ISegmentHelper>().GetFileStorageServer(account);

            var groupName = DomainCoreGroups.CompanyGroupBuild(account.IndexNumber);
            var companyPath = Path.Combine(fileStorage.ConnectionAddress, groupName);

            if (!Directory.Exists(companyPath))
            {
                Directory.CreateDirectory(companyPath);
                Logger.Info($"Создана директория {companyPath}");
            }

            ServiceProvider.GetRequiredService<IActiveDirectoryTaskProcessor>().TryDoImmediately(provider => provider.SetDirectoryAcl(groupName, companyPath));
        }

        /// <summary>
        ///     Метод добавления доступа к новосозданной базе 
        /// администратору аккаунта
        /// </summary>
        /// <param name="accountAdminId">Идентификатор админ. аккаунта</param>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="createdDatabaseId">Идентификатор базы</param>
        private void AddAccess(Guid accountAdminId, Guid accountId, Guid createdDatabaseId)
        {
            if (accountAdminId == Guid.Empty)
                return;

            Logger.Debug($"Добавление доступа к базе {createdDatabaseId} для админа {accountAdminId}");

            _acDbAccessProvider.GetOrCreateAccess(new AcDbAccessPostAddModelDto
            {
                AccountDatabaseID = createdDatabaseId,
                AccountID = accountId,
                LocalUserID = Guid.Empty,
                AccountUserID = accountAdminId,
                SendNotification = true
            });
        }
    }
}
