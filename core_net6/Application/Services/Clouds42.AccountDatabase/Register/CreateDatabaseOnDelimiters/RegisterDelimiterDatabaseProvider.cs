﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Register.CreateDatabaseOnDelimiters
{

    /// <summary>
    /// Класс регистрации баз данных аккаунта на сервере разделителей.
    /// </summary>
    internal class RegisterDelimiterDatabaseProvider(
        IServiceProvider serviceProvider,
        ICreateNewDelimiterCommand createNewDelimiterCommand,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        ILogger42 logger)
        : RegisterDatabaseOnServerBase<RegisterDatabaseOnDelimiterModelDto>(serviceProvider),
            IRegisterDelimiterDatabaseProvider
    {
        protected override void Register(RegisterDatabaseOnDelimiterModelDto model)
		{
			try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                RegisterInServiceManager(model.AccountUserListToBeAddAccess, accountDatabase, model.IsDemoDatabase);

			}
			catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка регистрации информационной базы на разделителях] '{model.AccountDatabaseId}'");
            }
		}

        /// <summary>
        /// Зарегестрировать базу на разделителях в МС
        /// </summary>
        /// <param name="ids">Список Id пользователей</param>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="isdemodatabase">признак демо базы</param>
		private void RegisterInServiceManager(List<Guid> ids, IAccountDatabase accountDatabase, bool isdemodatabase)
		{

            if (!accountDatabase.TemplateId.HasValue)
                throw new InvalidOperationException($"У информационной базы '{accountDatabase.Id}' не указан шаблон ");

            Logger.Info($"Добавляем новую область для базы {accountDatabase.Id} ");
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var accountUserListToBeAddAccess = GetAccountUserListToBeAddAccess(ids, accountDatabase);

                foreach (var accountUser in accountUserListToBeAddAccess)
                {
                    CreateAcDbAccessRow(accountUser.Id, accountDatabase);
                }

                Logger.Info($"Начало отправки запроса на создание области для базы {accountDatabase.Id}");

                createNewDelimiterCommand.Execute(accountUserListToBeAddAccess,
                    accountDatabase.Caption, accountDatabase.TemplateId.Value, accountDatabase.AccountId,
                    accountDatabase.Id, isdemodatabase);

                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                DatabaseStatusHelper.SetDbStatus(accountDatabase.V82Name, DatabaseState.ErrorCreate);
                throw;
            }
        }

	    private List<IAccountUser> GetAccountUserListToBeAddAccess(List<Guid> ids, IAccountDatabase accountDatabase)
	    {
	        if (CheckRent1CIsLocked(accountDatabase.AccountId))
	        {
	            return [];
	        }

	        if (ids.Any())
	        {
	            var accountUserList = DbLayer.AccountUsersRepository.Where(au =>
                    ids.Contains(au.Id)).ToList();

	            return accountUserList.Cast<IAccountUser>().ToList();
	        }

            var accountUserListToBeAddAccess =
	            DbLayer.AccountsRepository.GetAccountAdminsWithActivatedRent1C(accountDatabase.AccountId)
	                .Cast<IAccountUser>().ToList();

	        return accountUserListToBeAddAccess;
	    }
	    

        /// <summary>
        /// Управление доступом для админа
        /// </summary>
        /// <param name="accountUserId"> Id пользователя с правами админа</param>
        /// <param name="accountDatabase"> ИБ </param>
        /// <returns></returns>
        private void CreateAcDbAccessRow(Guid accountUserId, IAccountDatabase accountDatabase)
        {
            var accessEntity = new AcDbAccess
            {
                AccountDatabaseID = accountDatabase.Id,
                AccountID = accountDatabase.AccountId,
                LocalUserID = Guid.Empty,
                AccountUserID = accountUserId,
                ID = Guid.NewGuid()
            };

            DbLayer.AcDbAccessesRepository.Insert(accessEntity);                        
        }      
  
    }

}
