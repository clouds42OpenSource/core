﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;

namespace Clouds42.AccountDatabase.Register.CreateDatabaseFromBackup
{

    /// <summary>
    /// Класс регистратор созданной базы из бэкапа.
    /// </summary>
    internal class RegisterDatabaseFromBackupProvider(
        IServiceProvider serviceProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        AccountDatabasePathHelper accountDatabasePathHelper)
        : RegisterDatabaseOnServerBase<RegisterDatabaseModelDto>(serviceProvider), IRegisterDatabaseFromBackupProvider
    {
        protected override void Register(RegisterDatabaseModelDto model)
        {
            var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            if (accountDatabase.IsFile == true)
            {
                var dbPath = accountDatabasePathHelper.GetPath(accountDatabase);
                if (!Directory.Exists(dbPath))
                    Directory.CreateDirectory(dbPath);
            }

            FinalizeRegister(accountDatabase, false);
        }

    }

}