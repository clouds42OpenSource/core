﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.CoreWorker.JobWrappers.RemoveFile;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.Files;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Backup.Providers
{
    /// <summary>
    /// Провайдер загрузки файлов бэкапов
    /// </summary>
    internal class BackupFileProvider : IBackupFileProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly Dictionary<AccountDatabaseBackupSourceType, IUploadedFileFetcher> _mapBackupSourceTypeToLoadFileProvider;
        private readonly IGetBackupFullPathBySmBackupIdCommand _getBackupFullPathBySmBackupIdCommand;
        private readonly IRemoveFileJobWrapper _removeFileJobWrapper;
        private readonly IHandlerException _handlerException;
        public BackupFileProvider(IUnitOfWork dbLayer,
            ILocalBackupFileProvider localBackupFileProvider,
            IGoogleDriveBackupFileProvider googleDriveBackupFileProvider,
            IGetBackupFullPathBySmBackupIdCommand getBackupFullPathBySmBackupIdCommand,
            ILogger42 logger, 
            IHandlerException handlerException, 
            IRemoveFileJobWrapper removeFileJobWrapper)
        {
            _dbLayer = dbLayer;
            _handlerException = handlerException;
            _removeFileJobWrapper = removeFileJobWrapper;
            _getBackupFullPathBySmBackupIdCommand = getBackupFullPathBySmBackupIdCommand;
            _mapBackupSourceTypeToLoadFileProvider = new Dictionary<AccountDatabaseBackupSourceType, IUploadedFileFetcher>
            {
                { AccountDatabaseBackupSourceType.Local, localBackupFileProvider },
                { AccountDatabaseBackupSourceType.GoogleCloud, googleDriveBackupFileProvider }
            };
        }

        /// <summary>
        /// Загрузить файл бэкапа
        /// </summary>
        /// <param name="backupFileFetchingParams">Параметры задачи по загрузке файла бэкапа</param>
        public void FetchBackupFile(BackupFileFetchingJobParamsDto backupFileFetchingParams)
        {
            try
            {
                var backup = _dbLayer.AccountDatabaseBackupRepository
                    .FirstOrThrowException(back => back.Id == backupFileFetchingParams.BackupId);
                var uploadedFile = _dbLayer.UploadedFileRepository
                    .FirstOrThrowException(file => file.Id == backupFileFetchingParams.UploadedFileId);

                CheckBackupPathAndUpdateIfNeed(backupFileFetchingParams.BackupId);

                _mapBackupSourceTypeToLoadFileProvider[backup.SourceType].Fetch(backup, uploadedFile);
                UpdateUploadedFile(uploadedFile.Id, UploadedFileStatus.UploadSuccess);
                StartDeletingTask(uploadedFile.FullFilePath);
            }
            catch (Exception ex)
            {
                UpdateUploadedFile(backupFileFetchingParams.UploadedFileId, UploadedFileStatus.UploadFailed, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Обновить загруженный файл
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <param name="status">Статус загрузки</param>
        /// <param name="comment">Комментарий</param>
        private void UpdateUploadedFile(Guid uploadedFileId, UploadedFileStatus status, string comment = null)
        {
            var uploadedFile = _dbLayer.UploadedFileRepository
                .FirstOrThrowException(x => x.Id == uploadedFileId);

            uploadedFile.Status = status;
            uploadedFile.Comment = comment;

            _dbLayer.UploadedFileRepository.Update(uploadedFile);
            _dbLayer.Save();
        }

        private void StartDeletingTask(string filePath) =>
            _removeFileJobWrapper.Start(new RemoveFileJobParams
            {
                FilePath = filePath
            }, DateTime.Now.AddHours(2));
        

        /// <summary>
        /// Проверить путь к бэкапу и обновить его при необходимости
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        public void CheckBackupPathAndUpdateIfNeed(Guid accountDatabaseBackupId)
        {
            try
            {
                var accountDatabaseBackup = _dbLayer.AccountDatabaseBackupRepository
                    .FirstOrThrowException(back => back.Id == accountDatabaseBackupId);

                if (accountDatabaseBackup.SourceType == AccountDatabaseBackupSourceType.GoogleCloud)
                    return;

                if (File.Exists(accountDatabaseBackup.BackupPath))
                    return;

                if (accountDatabaseBackup.ServiceManagerAcDbBackup == null)
                    return;

                accountDatabaseBackup.BackupPath = _getBackupFullPathBySmBackupIdCommand.Execute(accountDatabaseBackupId);

                _dbLayer.AccountDatabaseBackupRepository.Update(accountDatabaseBackup);
                _dbLayer.Save();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, 
                    $"[Ошибка проверка пути к бэкапу ]. Бэкап: {accountDatabaseBackupId}");
            }
        }
    }
}
