﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.UploadedFiles.Helpers;

namespace Clouds42.AccountDatabase.Backup.Providers
{
    /// <summary>
    /// Провайдер загрузки локальных бэкапов инф. баз
    /// </summary>
    internal class LocalBackupFileProvider(ILogger42 logger) : ILocalBackupFileProvider
    {
        /// <summary>
        /// Загрузить локальный бэкап
        /// </summary>
        /// <param name="backup">Id бэкапа</param>
        /// <param name="uploadedFile">Загружаемый файл</param>
        public void Fetch(AccountDatabaseBackup backup, UploadedFile uploadedFile)
        {
            try
            {
                CopyAccountDatabaseBackup(backup, uploadedFile);
            }
            catch (Exception ex)
            {
                logger.Warn(
                    $"При загрузке локального бэкапа {backup.Id} возникла ошибка. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Скопировать файл бэкапа
        /// </summary>
        /// <param name="backup">Бэкап инф. базы</param>
        /// <param name="backupFile">Загружаемый файл бэкапа</param>
        private void CopyAccountDatabaseBackup(AccountDatabaseBackup backup, UploadedFile backupFile)
        {
            try
            {
                if (!File.Exists(backup.BackupPath))
                    throw new NotFoundException($"[{backup.AccountDatabaseId}] :: Архив {backup.BackupPath} не найден");

                if (File.Exists(backupFile.FullFilePath))
                    File.Delete(backupFile.FullFilePath);

                FileUseChecker.WaitWhileFileUsing(backup.BackupPath, waitTimeInSeconds: 120);

                var folderPath = Path.GetDirectoryName(backupFile.FullFilePath) ??
                                 throw new NotFoundException(
                                     $"Для загруженного файла {backupFile.FileName} не указан путь.");

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                File.Copy(backup.BackupPath, backupFile.FullFilePath);

                if (!File.Exists(backupFile.FullFilePath))
                    throw new InvalidOperationException($"Копирование файла {backupFile.FileName} завершено с ошибкой");

                logger.Trace($"Копирование файла {backupFile.FileName} завершено успешно");
            }
            catch (NotFoundException ex)
            {
                logger.Warn(ex, $"При копировании бэкапа {backup.Id} инф. базы возникла ошибка. Причина: {ex.GetFullInfo()}");
            }
            
        }
    }
}
