﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.GoogleCloud;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Backup.Providers
{
    /// <summary>
    /// Провайдер загрузки бэкапов инф. баз с гугл диска
    /// </summary>
    internal class GoogleDriveBackupFileProvider(
        IGoogleCloudProvider cloudProvider,
        ILogger42 logger,
        IGoogleDriveProvider driveProvider)
        : IGoogleDriveBackupFileProvider
    {
        /// <summary>
        /// Загрузить бэкап с гугл диска
        /// </summary>
        /// <param name="backup">Id бэкапа</param>
        /// <param name="uploadedFile">Загружаемый файл</param>
        public void Fetch(AccountDatabaseBackup backup, UploadedFile uploadedFile)
        {
            try
            {
                var destinationFilePath = GetDestinationFilePath(uploadedFile);
                var fileId = backup.FilePath;
                
                if (backup.BackupPath?.Contains("drive.google.com") ?? false)
                {
                    logger.Info($"[{backup.AccountDatabaseId}] :: Загрузка файла {backup.BackupPath} с гугл диска");

                    driveProvider.PartialDownloadFile(backup.BackupPath, destinationFilePath);

                    logger.Info($"[{backup.AccountDatabaseId}] :: Файл сохранен по пути: {destinationFilePath}");

                    return;
                }

                logger.Info($"[{backup.AccountDatabaseId}] :: Загрузка файла {fileId} с гугл облака");

                var result = cloudProvider.DownloadFile(fileId, destinationFilePath, true);
                if (!result)
                    throw new InvalidOperationException($"Ошибка загрузки файла {fileId} из склепа");

                logger.Info($"[{backup.AccountDatabaseId}] :: Файл сохранен по пути: {destinationFilePath}");
            }

            catch (Exception ex)
            {
                logger.Warn($"При загрузке файла бэкапа {uploadedFile.Id} с гугл диска произошла ошибка. Причина: {ex.GetFullInfo()}");

                throw;
            }
        }

        /// <summary>
        /// Получить путь для сохранения файла
        /// </summary>
        /// <param name="uploadedFile">Загружаемый файл</param>
        /// <returns>Путь для сохранения файла</returns>
        private string GetDestinationFilePath(UploadedFile uploadedFile)
        {
            logger.Trace($"Получен путь для загружаемого бэкапа {uploadedFile.FileName} : {uploadedFile.FullFilePath}");

            var folderPath = Path.GetDirectoryName(uploadedFile.FullFilePath) ??
                             throw new NotFoundException(
                                 $"Для загруженного файла {uploadedFile.FileName} не указан путь.");

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            return uploadedFile.FullFilePath;
        }
    }
}
