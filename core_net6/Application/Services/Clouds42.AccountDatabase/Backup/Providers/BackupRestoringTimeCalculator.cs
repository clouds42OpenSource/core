﻿using Clouds42.AccountDatabase.Backup.Helpers;
using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Backup.Models;

namespace Clouds42.AccountDatabase.Backup.Providers
{
    /// <summary>
    /// Калькулятор времени восстановления базы
    /// </summary>
    public class BackupRestoringTimeCalculator : IBackupRestoringTimeCalculator
    {
        private readonly List<(Func<int, bool> DetermineCompliance, int RestoringTimeInSeconds)>
            _restoreBackupTimeFinder;

        public BackupRestoringTimeCalculator()
        {
            _restoreBackupTimeFinder =
            [
                CreateFindConditionForRestoreBackupTime(RestoreBackupTimeConst.FirstGapStart,
                    RestoreBackupTimeConst.FirstGapFinish, RestoreBackupTimeConst.FirstGapResult),

                CreateFindConditionForRestoreBackupTime(RestoreBackupTimeConst.SecondGapStart,
                    RestoreBackupTimeConst.SecondGapFinish, RestoreBackupTimeConst.SecondGapResult),

                CreateFindConditionForRestoreBackupTime(RestoreBackupTimeConst.ThirdGapStart,
                    RestoreBackupTimeConst.ThirdGapFinish, RestoreBackupTimeConst.ThirdGapResult),

                CreateFindConditionForRestoreBackupTime(RestoreBackupTimeConst.FourthGapStart,
                    RestoreBackupTimeConst.FourthGapFinish, RestoreBackupTimeConst.FourthGapResult),

                (size => size > RestoreBackupTimeConst.MaxGapValue, RestoreBackupTimeConst.MaxGapValueResult)
            ];
        }

        /// <summary>
        /// Выполнить рассчет
        /// </summary>
        /// <param name="dbSizeInMb">Размер базы в Мб</param>
        /// <returns>Время для восстановления базы</returns>
        public int Calculate(int dbSizeInMb)
        {
            var sizeInGb = CalculateBackupRestoringTimeHelper.GetDbSizeInGb(dbSizeInMb);

            var foundCompliance =
                _restoreBackupTimeFinder.FirstOrDefault(determinant => determinant.DetermineCompliance(sizeInGb));

            if (foundCompliance == default)
                return 0;

            return foundCompliance.RestoringTimeInSeconds.IncreaseValueToThirtyPercents();
        }

        /// <summary>
        /// Создать условия поиска времени восстановления бекапа 
        /// </summary>
        /// <param name="startGap">Начальное значение промежутка</param>
        /// <param name="finishGap">Конечное значение промежутка</param>
        /// <param name="restoringTimeInSeconds">Время восстановления в секундах</param>
        /// <returns>Условие поиска времени восстановления бекапа</returns>
        private static (Func<int, bool> DetermineCompliance, int RestoringTimeInSeconds)
            CreateFindConditionForRestoreBackupTime(int startGap, int finishGap, int restoringTimeInSeconds) => 
            (size => size.IsValueEntryIntoGap(startGap, finishGap), restoringTimeInSeconds);
    }
}