﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Backup.Providers
{
    /// <summary>
    /// Провайдер удаления бэкапов инф. баз
    /// </summary>
    internal class DeleteAccountDatabaseBackupProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : IDeleteAccountDatabaseBackupProvider
    {
        /// <summary>
        /// Удалить бэкап инф. базы
        /// </summary>
        /// <param name="model">Параметры удаления бэкапа инф. базы</param>
        public void Delete(DeleteAccountDatabaseBackupRequestDto model)
        {
            var accountDatabaseBackup = dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup =>
                backup.ServiceManagerAcDbBackup.ServiceManagerAcDbBackupId == model.ServiceManagerAcDbBackupId);

            if (accountDatabaseBackup == null)
                return;

            if (accountDatabaseBackup.AccountDatabase.StateEnum == DatabaseState.DetachedToTomb)
                return;

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                logger.Info(
                    $"[{accountDatabaseBackup.AccountDatabaseId}] :: Удаление записи о бэкапе из базы данных");

                var backupHistories = dbLayer.AccountDatabaseBackupHistoryRepository.Where(history =>
                    history.AccountDatabaseBackupId == accountDatabaseBackup.Id);

                dbLayer.AccountDatabaseBackupHistoryRepository.DeleteRange(backupHistories);
                dbLayer.Save();

                dbLayer.GetGenericRepository<ServiceManagerAcDbBackup>()
                    .Delete(accountDatabaseBackup.ServiceManagerAcDbBackup);
                dbLayer.Save();

                dbLayer.AccountDatabaseBackupRepository.Delete(accountDatabaseBackup);
                dbLayer.Save();

                transaction.Commit();

                logger.Info($"[{accountDatabaseBackup.AccountDatabaseId}] :: Удаление бэкапа выполнено");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка удаления записи о бэкапе]. Бэкап: {accountDatabaseBackup.Id}");
                transaction.Rollback();
                throw;
            }
        }
    }
}
