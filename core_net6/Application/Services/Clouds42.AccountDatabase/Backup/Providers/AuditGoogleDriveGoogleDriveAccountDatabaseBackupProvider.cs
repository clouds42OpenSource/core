﻿using Clouds42.AccountDatabase.Backup.Resources;
using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseAudit;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Backup.Providers
{
    /// <summary>
    /// Провайдер для аудита облачных бэкапов инф. баз
    /// </summary>
    internal class AuditGoogleDriveGoogleDriveAccountDatabaseBackupProvider(
        IUnitOfWork dbLayer,
        TombAnalysisProvider tombAnalysisProvider,
        MailNotificationFactory mailNotificationFactory)
        : IAuditGoogleDriveAccountDatabaseBackupProvider
    {
        /// <summary>
        /// Провести аудит облачных бэкапов инф. баз
        /// </summary>
        public void ProcessAudit()
        {
            var accountDatabasesBackups = GetCloudAccountDatabasesBackups();

            var auditResultList = new List<AuditAccountDatabaseBackupResultDto>();

            accountDatabasesBackups.ForEach(accountDatabasesBackup =>
            {
                auditResultList.Add(tombAnalysisProvider.AuditAccountDatabaseBackup(accountDatabasesBackup.Id));
            });

            ProcessAuditResult(auditResultList);
        }

        /// <summary>
        /// Получить облачные бэкапы инф. баз
        /// </summary>
        /// <returns>Список облачных бэкапов инф. баз</returns>
        private List<AccountDatabaseBackup> GetCloudAccountDatabasesBackups()
            => dbLayer.AccountDatabaseBackupRepository.Where(w =>
                w.SourceType == AccountDatabaseBackupSourceType.GoogleCloud).ToList();

        /// <summary>
        /// Обработать результаты аудита бэкапов
        /// </summary>
        /// <param name="auditResultList">Результаты аудита бэкапов</param>
        private void ProcessAuditResult(List<AuditAccountDatabaseBackupResultDto> auditResultList)
        {
            var messageBody = GetMessageBody(auditResultList);
            mailNotificationFactory.SendMail<MailNotifyAuditAccountDatabasesBackups, string>(messageBody);
        }

        /// <summary>
        /// Получить тело письма
        /// </summary>
        /// <param name="auditResultList">Результаты аудита бэкапов</param>
        /// <returns>Тело письма</returns>
        private string GetMessageBody(List<AuditAccountDatabaseBackupResultDto> auditResultList)
        {
            var totalCount = auditResultList.Count;

            var failedResults = auditResultList.Where(res => !res.Success).ToList();
            var successResults = auditResultList.Where(res => res.Success).ToList();

            return string.Format(EmailTemplates.AuditAccountDatabasesBackupsNotificationTemplate,
                DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"),
                totalCount,
                successResults.Count,
                GetAuditResultsList(successResults),
                failedResults.Count,
                GetAuditResultsList(failedResults));
        }

        /// <summary>
        /// Получить html список проверенных бэкапов
        /// </summary>
        /// <param name="auditResults">Результаты аудита бэкапов</param>
        /// <returns>html список проверенных бэкапов</returns>
        private string GetAuditResultsList(List<AuditAccountDatabaseBackupResultDto> auditResults)
        {
            var htmlResult = string.Empty;

            if (!auditResults.Any())
                return "<li>Список пуст</li>";
            
            auditResults.ForEach(res =>
            {
                var errorMessage = !string.IsNullOrEmpty(res.ErrorMessage)
                    ? $"Причина: {res.ErrorMessage}"
                    : null;
                htmlResult +=
                    $@"<li>
                            ID бэкапа - {res.AccountDatabaseBackupId}. Результат проверки: {(res.Success ? "Успешно" : "Ошибка")}. Путь к файлу бэкапа - {res.BackupPath}. {errorMessage}
                       </li>";
            });

            return htmlResult;
        }
    }
}
