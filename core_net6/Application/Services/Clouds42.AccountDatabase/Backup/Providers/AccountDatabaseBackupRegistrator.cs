﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.JobWrappers.SendBackupAccountDatabaseToTomb;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Backup.Providers
{
    /// <summary>
    /// Регистратор бэкапов инфо баз.
    /// </summary>
    internal class AccountDatabaseBackupRegistrator(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ISendBackupAccountDatabaseToTombJobWrapper sendBackupAccountDatabaseToTombJobWrapper,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IAccountDatabaseBackupRegistrator
    {
        private readonly IDictionary<DatabaseState, DatabaseState> _mapDeletionDatabaseState = new Dictionary<DatabaseState, DatabaseState>
        {
            { DatabaseState.DelitingToTomb, DatabaseState.DeletedToTomb },
            { DatabaseState.DetachingToTomb, DatabaseState.DetachedToTomb },
            { DatabaseState.DeletedToTomb, DatabaseState.DeletedToTomb },
            { DatabaseState.DeletedFromCloud, DatabaseState.DeletedFromCloud },
            { DatabaseState.DetachedToTomb, DatabaseState.DetachedToTomb }
        };

        /// <summary>
        /// Зарегистрировать копию информационной базы.
        /// </summary>
        /// <param name="model">Модель регистарации бэкапа инфомационной базы</param> 	
        public void RegisterBackup(RegisterAccountDatabaseBackupRequestDto model)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                if (!string.IsNullOrEmpty(model.BackupFullPath))
                    RegisterSuccess(model);
                else
                    CreateBackupHistoryRecord(model);

                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка регистрации бэкапа ИБ]. При регистрации бэкапа для инф. базы {model.AccountDatabaseId} возникла ошибка.");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Зарегистрировать успешно созданный бэкап.
        /// </summary>
        /// <param name="model">Модель регистрации бэкапа инфомационной базы</param> 
        private void RegisterSuccess(RegisterAccountDatabaseBackupRequestDto model)
        {
            if(model.ServiceManagerAcDbBackupId == null || model.ServiceManagerAcDbBackupId.IsNullOrEmpty())
                throw new InvalidOperationException("Отсутствует значение параметра ApplicationBackupID.");

            var accountDatabaseBackup = GetAccountDatabaseBackupBySmId(model.ServiceManagerAcDbBackupId.Value);
            if (accountDatabaseBackup != null)
            {
                UpdateExistingBackup(accountDatabaseBackup, model);
                return;
            }

            logger.Trace($"{model.AccountDatabaseId} :: создана запись бэкапа");

            var accountDatabaseBackupId = CreteBackupRecord(model);
            CreateSmBackupRecord(accountDatabaseBackupId, model.ServiceManagerAcDbBackupId.Value);

            FinalizeBackupRegistration(model, accountDatabaseBackupId);
        }

        /// <summary>
        /// Завершить регистрацию бэкапа
        /// </summary>
        /// <param name="model">Модель регистрации бэкапа инфомационной базы</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        private void FinalizeBackupRegistration(RegisterAccountDatabaseBackupRequestDto model,
            Guid accountDatabaseBackupId)
        {
            var database = dbLayer.DatabasesRepository.GetAccountDatabase(model.AccountDatabaseId);

            if (model.Trigger != CreateBackupAccountDatabaseTrigger.RegulationBackup)
            {
                UpdateDbStateOnSentForDeletion(model.AccountDatabaseId);
                LogResultOfDatabaseArchiving(database, model.BackupFullPath);
            }

            CreateBackupHistoryRecord(model, accountDatabaseBackupId);
            CreateTaskForSendBackupToTomb(model, accountDatabaseBackupId);
        }

        /// <summary>
        /// Создать задачу на отправку бэкапа в склеп
        /// </summary>
        /// <param name="model">Модель регистрации бэкапа инф. базы.</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        private void CreateTaskForSendBackupToTomb(RegisterAccountDatabaseBackupRequestDto model, Guid accountDatabaseBackupId)
        {
            if (model.Trigger == CreateBackupAccountDatabaseTrigger.RegulationBackup)
                return;

            sendBackupAccountDatabaseToTombJobWrapper.Start(new SendBackupAccountDatabaseToTombJobParamsDto
            {
                AccountDatabaseId = model.AccountDatabaseId,
                AccountDatabaseBackupId = accountDatabaseBackupId
            }, OptimalTimeForSendBackupToTombCalculator.Calculate().AddDays(7));
        }

        /// <summary>
        /// Создать запись бэкапа.
        /// </summary>
        /// <param name="model">Модель регистрации бэкапа инф. базы.</param>
        private Guid CreteBackupRecord(RegisterAccountDatabaseBackupRequestDto model)
        {
            var eventTrigger = AdjustTriggerStatus(model);

            var backupRecord = new AccountDatabaseBackup
            {
                Id = Guid.NewGuid(),
                AccountDatabaseId = model.AccountDatabaseId,
                CreationBackupDateTime = model.CreationBackupDateTime ?? DateTime.Now,
                InitiatorId = accessProvider.GetUser().Id,
                BackupPath = model.BackupFullPath,
                EventTrigger = eventTrigger,
                SourceType = AccountDatabaseBackupSourceType.Local
            };

            dbLayer.AccountDatabaseBackupRepository.Insert(backupRecord);
            return backupRecord.Id;
        }

        /// <summary>
        /// Создать запись о связи бэкапа инф. базы с бэкапом МС
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <param name="serviceManagerAcDbBackupId">ID бэкапа инф. базы от МС</param>
        private void CreateSmBackupRecord(Guid accountDatabaseBackupId, Guid serviceManagerAcDbBackupId)
        {
            var smBackupRecord = new ServiceManagerAcDbBackup
            {
                AccountDatabaseBackupId = accountDatabaseBackupId,
                ServiceManagerAcDbBackupId = serviceManagerAcDbBackupId
            };

            dbLayer.GetGenericRepository<ServiceManagerAcDbBackup>().Insert(smBackupRecord);
            dbLayer.Save();
        }

        /// <summary>
        /// Обновить статус ИБ на разделителях оправленую на удаление
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param> 
        private void UpdateDbStateOnSentForDeletion(Guid databaseId)
        {
            var database = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == databaseId);
            if (database == null)
            {
                logger.Info($"[{databaseId}] :: Информационная база не найдена");
                return ;
            }
            if(database.AccountDatabaseOnDelimiter == null)
            {
                logger.Info($"[{databaseId}] :: Информационная база не на разделителях");
                return ;
            }
            logger.Info($"[{databaseId}] :: Информационная база найдена: {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}, меняем статус");

            accountDatabaseChangeStateProvider.ChangeState(databaseId, GetAccountDatabaseState(database));
        }

        /// <summary>
        /// Скорректировать статус тригера
        /// </summary>
        /// <param name="model">Модель регистрации бэкапа инф. базы.</param>
        /// <returns>Значение триггера</returns>
        private CreateBackupAccountDatabaseTrigger AdjustTriggerStatus(RegisterAccountDatabaseBackupRequestDto model)
        {
            var eventTrigger = model.Trigger;

            if (model.Trigger == CreateBackupAccountDatabaseTrigger.RegulationBackup)
                return eventTrigger;

            var database = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == model.AccountDatabaseId);
            if (database == null)
            {
                logger.Info($"[{model.AccountDatabaseId}] :: Информационная база не найдена");
                return eventTrigger;
            }
            if (database.AccountDatabaseOnDelimiter == null)
            {
                logger.Info($"[{model.AccountDatabaseId}] :: Информационная база не на разделителях");
                return eventTrigger;
            }
            logger.Info($"[{model.AccountDatabaseId}] :: Информационная база найдена: {database.V82Name}, меняем статуc тригерра {model.Trigger}");

            eventTrigger = 
                database.StateEnum == DatabaseState.DetachedToTomb || database.StateEnum == DatabaseState.DetachingToTomb
                    ? CreateBackupAccountDatabaseTrigger.MovingDbToArchive 
                    : CreateBackupAccountDatabaseTrigger.ManualRemoval;
            
            logger.Info($"[{model.AccountDatabaseId}] :: Cтатуc тригерра изменен на {eventTrigger}");

            return eventTrigger;
        }

        /// <summary>
        /// Создать запись истории архивации.
        /// </summary>
        /// <param name="model">Модель регистрации бэкапа инф. базы.</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <param name="message">Сообщение</param> 
        private void CreateBackupHistoryRecord(RegisterAccountDatabaseBackupRequestDto model, 
            Guid? accountDatabaseBackupId = null,
            string message = null)
        {
            var history = new AccountDatabaseBackupHistory
            {
                Id = Guid.NewGuid(),
                Message = message ?? model.Comment,
                AccountDatabaseId = model.AccountDatabaseId,
                AccountDatabaseBackupId = accountDatabaseBackupId,
                State = model.State
            };

            dbLayer.AccountDatabaseBackupHistoryRepository.Insert(history);
        }

        /// <summary>
        /// Получить статус инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Статус инф. базы</returns>
        private DatabaseState GetAccountDatabaseState(Domain.DataModels.AccountDatabase accountDatabase)
        => !_mapDeletionDatabaseState.ContainsKey(accountDatabase.StateEnum)
                ? DatabaseState.DeletedToTomb 
                : _mapDeletionDatabaseState[accountDatabase.StateEnum];

        /// <summary>
        /// Обновить существующий бэкап
        /// </summary>
        /// <param name="accountDatabaseBackup">Бэкап инф. базы</param>
        /// <param name="model">Модель регистрации бэкапа инф. базы</param>
        private void UpdateExistingBackup(AccountDatabaseBackup accountDatabaseBackup,
            RegisterAccountDatabaseBackupRequestDto model)
        {
            if (accountDatabaseBackup.SourceType == AccountDatabaseBackupSourceType.GoogleCloud)
                return;

            accountDatabaseBackup.BackupPath = model.BackupFullPath;
            dbLayer.AccountDatabaseBackupRepository.Update(accountDatabaseBackup);
            dbLayer.Save();

            FinalizeBackupRegistration(model, accountDatabaseBackup.Id);
        }

        /// <summary>
        /// Получить запись о бэкапе по ID МС
        /// </summary>
        /// <param name="serviceManagerAcDbBackupId">ID бэкапа от МС</param>
        /// <returns>Запись о бэкапе</returns>
        private AccountDatabaseBackup GetAccountDatabaseBackupBySmId(Guid serviceManagerAcDbBackupId)
            => dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup =>
                backup.ServiceManagerAcDbBackup.ServiceManagerAcDbBackupId == serviceManagerAcDbBackupId);


        /// <summary>
        /// Создать запись в логировании о перемещении базы в архив
        /// </summary>
        /// <param name="accountDatabase">Модель информационной базы</param>
        /// <param name="backupPath">Путь до бекапа</param>
        private void LogResultOfDatabaseArchiving(Domain.DataModels.AccountDatabase accountDatabase, string backupPath)
        {
            if (accountDatabase.StateEnum != DatabaseState.DetachedToTomb)
                return;

            LogEventHelper.LogEvent(dbLayer, accessProvider.ContextAccountId, accessProvider,
                LogActions.MovingDbToArchive,
                $"База “{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}” перемещена в архив. Резервная копия доступна по ссылке: {backupPath}");
        }
    }
}
