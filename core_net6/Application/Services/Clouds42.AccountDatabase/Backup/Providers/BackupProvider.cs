﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Backup.Models;
using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.CoreWorker.JobWrappers.BackupFileFetching;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.Files;
using Clouds42.GoogleCloud;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Backup.Providers
{
    /// <summary>
    /// Провайдер для работы с бэкапами
    /// </summary>
    public class BackupProvider(
        IUnitOfWork dbLayer,
        IUploadedFileProvider uploadedFileProvider,
        IBackupFileFetchingJobWrapper backupFileFetchingJobWrapper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAbilityToPayForMyDatabasesServiceProvider abilityToPayForMyDatabasesServiceProvider,
        IApplyOrPayForAllMyDatabasesServiceChanges applyOrPayForAllMyDatabasesServiceChanges,
        TombTaskCreatorManager tombTaskCreatorManager,
        IEmailAddressesDataProvider emailAddressesDataProvider,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        IHandlerException handlerException,
        IGoogleCloudProvider googleCloudProvider)
        : IBackupProvider
    {
        /// <summary>
        /// Получить данные для восстановления инф. базы из бекапа
        /// </summary>
        /// <param name="backupId">ID бекапа</param>
        /// <returns>Данные для восстановления инф. базы из бекапа</returns>
        public BackupRestoringDto GetDataForRestoreAccountDatabase(Guid backupId)
        {
            var accountDatabaseBackup = accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(backupId);
            var uploadedFileId = CreateUploadedFile(accountDatabaseBackup);
            var accountDatabaseName = GetAccountDatabaseName(accountDatabaseBackup);

            return new BackupRestoringDto
            {
                AccountDatabaseBackupId = backupId,
                AccountDatabaseName = accountDatabaseName,
                UploadedFileId = uploadedFileId,
                IsDbOnDelimiters = accountDatabaseBackup.AccountDatabase.IsDelimiter(),
                EmailAddresses =
                    emailAddressesDataProvider.GetAllAccountEmailsToSend(accountDatabaseBackup.AccountDatabase
                        .AccountId)
            };
        }

        /// <summary>
        /// Начать процесс загрузки файла бэкапа
        /// </summary>
        /// <param name="backupId">Id бэкапа</param>
        /// <param name="uploadedFileId">Id файла бэкапа</param>
        public void StartBackupFileFetching(Guid backupId, Guid uploadedFileId)
        {
            var accountDatabaseBackup = accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(backupId);
            var uploadedFile = dbLayer.UploadedFileRepository
                .FirstOrThrowException(w => w.Id == uploadedFileId);

            if (uploadedFile.Status != UploadedFileStatus.UploadingInProcess)
                throw new InvalidOperationException(
                    $"Загрузка файла \"{uploadedFile.FileName}\" прервалась. Необходимо выполнить восстановление повторно");

            backupFileFetchingJobWrapper.Start(new BackupFileFetchingJobParamsDto
            {
                BackupId = accountDatabaseBackup.Id,
                UploadedFileId = uploadedFileId
            });
        }

        /// <summary>
        /// Запустить задачу восстановления инф. базы из бекапа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="restoreAcDbFromBackupModel">Модель параметров восстановления инф. базы из бекапа</param>
        /// <returns>Результат выполнения</returns>
        public CreateAccountDatabasesResultDto StartTaskForRestoreAccoundDatabase(Guid accountId,
            RestoreAcDbFromBackupDto restoreAcDbFromBackupModel)
        {
            var abilityToPayForDatabasesResult =
                CheckPossibilityToPayForRestorationDb(accountId, restoreAcDbFromBackupModel);

            if (!abilityToPayForDatabasesResult.IsComplete)
                return abilityToPayForDatabasesResult;
            try
            {
                applyOrPayForAllMyDatabasesServiceChanges.ApplyOrPay(accountId,
                    restoreAcDbFromBackupModel.UsersIdForAddAccess,
                    restoreAcDbFromBackupModel.MyDatabasesServiceTypeId);

                CreateTaskForRestoreAccountDatabaseFromTomb(restoreAcDbFromBackupModel);

                if (restoreAcDbFromBackupModel.RestoreType == AccountDatabaseBackupRestoreType.ToCurrent)
                    ChangeDbState(restoreAcDbFromBackupModel.AccountDatabaseBackupId,
                        DatabaseState.RestoringFromTomb);

                return new CreateAccountDatabasesResultDto
                {
                    IsComplete = true
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка восстановления ИБ из бэкапа]. Восстановление инф. базы из бэкапа '{restoreAcDbFromBackupModel.AccountDatabaseBackupId}' завершилось с ошибкой");
                throw;
            }
        }

        public void DeleteOldBackups(DeleteOldBackupsDto deleteOldBackupsDto)
        {
            var dateWithDelay = DateTime.Now.AddDays(-deleteOldBackupsDto.Delay);

            var backups = dbLayer.AccountDatabaseBackupRepository
                .AsQueryable()
                .Where(x => x.EventTrigger == deleteOldBackupsDto.Trigger && x.CreateDateTime <= dateWithDelay)
                .ToList();

            foreach (var backup in backups)
            {
                googleCloudProvider.DeleteFile(backup.FilePath);
            }
        }

        /// <summary>
        /// Получить информацию о статусе загрузки файла
        /// </summary>
        /// <param name="uploadedFileId">Идентификатор загружаемого файла</param>
        /// <returns>Модель информации о статусе загрузки файла</returns>
        public UploadedFileInfoDto GetUploadedFileStatusInfo(Guid uploadedFileId)
        {

            var uploadedFileStatus = dbLayer.UploadedFileRepository.GetUploadedFileStatus(uploadedFileId);

            return new UploadedFileInfoDto
            {
                UploadedFileStatus = uploadedFileStatus,
                StatusComment = uploadedFileStatus == UploadedFileStatus.UploadSuccess
                    ? "Загрузка архивной копии из хранилища успешно завершена"
                    : "При загрузке архивной копии из хранилище возникла ошибка. Не удалось восстановить информационную базу из архивной копии. " +
                      "Для дополнительной информации обратитесь, пожалуйста, в нашу <a href=\"js:;\" onclick=\"openSupportWindow()\">службу поддержки</a>."
            };
        }

        /// <summary>
        /// Сгенерировать имя загружаемого файла бэкапа
        /// </summary>
        /// <param name="backup">Бэкап</param>
        /// <returns>Имя загружаемого файла бэкапа</returns>
        private string CreateUploadedFileName(AccountDatabaseBackup backup)
        {
            if (backup.SourceType == AccountDatabaseBackupSourceType.GoogleCloud)
                return $"{backup.AccountDatabase.V82Name}_{DateTime.Now:yyyy-MM-dd-HH-mm}.zip";

            var fileExtension = Path.GetExtension(backup.BackupPath);
            var fileName = Path.GetFileNameWithoutExtension(backup.BackupPath);

            return $"{fileName}_{DateTime.Now:yyyy-MM-dd-HH-mm}{fileExtension}";
        }

        /// <summary>
        /// Получить имя базы данных
        /// </summary>
        /// <param name="backup">Бэкап инф. базы</param>
        /// <returns>Имя базы данных</returns>
        private string GetAccountDatabaseName(AccountDatabaseBackup backup)
            => backup.AccountDatabase.StateEnum == DatabaseState.DetachedToTomb
                ? $"{backup.AccountDatabase.Caption}"
                : $"{backup.AccountDatabase.Caption} копия от {backup.CreationBackupDateTime:dd-MM-yyyy}";

        /// <summary>
        /// Начать загрузку файла бэкапа
        /// </summary>
        /// <param name="backup">Бэкап инф. базы</param>
        /// <returns>Id загружаемого файла бэкапа</returns>
        private Guid CreateUploadedFile(AccountDatabaseBackup backup)
        {
            var fileName = CreateUploadedFileName(backup);

            var uploadedFile = uploadedFileProvider.CreateUploadedFile(new InitUploadFileRequestDto
            {
                AccountId = backup.AccountDatabase.AccountId,
                FileName = fileName
            });

            return uploadedFile.Id;
        }

        /// <summary>
        /// Проверить возможность оплатить восстановление инф. базы
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="restoreAcDbFromBackupModel">Модель параметров восстановления инф. базы из бекапа</param>
        /// <returns>Модель результата проверки возможности оплаты</returns>
        private CreateAccountDatabasesResultDto CheckPossibilityToPayForRestorationDb(Guid accountId,
            RestoreAcDbFromBackupDto restoreAcDbFromBackupModel) =>
            abilityToPayForMyDatabasesServiceProvider.CheckAbility(accountId,
                restoreAcDbFromBackupModel.NeedUsePromisePayment,
                restoreAcDbFromBackupModel.UsersIdForAddAccess,
                restoreAcDbFromBackupModel.MyDatabasesServiceTypeId);

        /// <summary>
        /// Сменить состояние информационной базы
        /// </summary>
        /// <param name="accountDatabaseBackupId">Id бэкапа инф. базы</param>
        /// <param name="databaseState">Состояние</param>
        private void ChangeDbState(Guid accountDatabaseBackupId, DatabaseState databaseState)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseByBackupIdOrThrowException(accountDatabaseBackupId);
            accountDatabaseChangeStateProvider.ChangeState(accountDatabase, databaseState, createAccountDatabaseComment:null);
        }

        /// <summary>
        /// Создать новую задачу воркера на восстановление информационной базы из бэкапа
        /// </summary>
        /// <param name="restoreAcDbFromBackupModel">Модель параметров восстановления инф. базы из бекапа</param>
        private void CreateTaskForRestoreAccountDatabaseFromTomb(RestoreAcDbFromBackupDto restoreAcDbFromBackupModel) =>
            tombTaskCreatorManager.CreateTaskForRestoreAccountDatabaseFromTomb(
                new RestoreAccountDatabaseFromTombWorkerTaskParam
                {
                    AccountDatabaseBackupId = restoreAcDbFromBackupModel.AccountDatabaseBackupId,
                    UploadedFileId = restoreAcDbFromBackupModel.UploadedFileId,
                    UsersIdForAddAccess = restoreAcDbFromBackupModel.UsersIdForAddAccess,
                    RestoreType = restoreAcDbFromBackupModel.RestoreType,
                    SendMailAboutError = true,
                    NeedChangeState = true,
                    AccountDatabaseName = restoreAcDbFromBackupModel.AccountDatabaseName
                });
    }
}
