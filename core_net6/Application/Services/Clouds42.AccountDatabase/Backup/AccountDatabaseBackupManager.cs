﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Backup
{
    /// <summary>
    /// Менеджер бекапа информационной базы.
    /// Помечен как obsolete(устаревший), так как этот менеджер будет снесен вместе с AppCommon.
    /// Был добавлен новый менеджер с таким же именем в другой сборке Clouds42.BLL.AccountDatabaseBackup.
    /// </summary>
    [Obsolete("Устаревший класс менеджера, новый находится в другой сборке Clouds42.BLL.AccountDatabaseBackup")]
    public class AccountDatabaseBackupManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountDatabaseBackupRegistrator accountDatabaseBackupRegistrator,
        IHandlerException handlerException,
        RegisterAccountDatabaseBackupValidator registerAccountDatabaseBackupValidator,
        IDeleteAccountDatabaseBackupProvider deleteAccountDatabaseBackupProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Зарегистрировать копию информационной базы.
        /// </summary>	
        public ManagerResult RegisterBackup(RegisterAccountDatabaseBackupRequestDto model)
        {
            try
            {
                var accountId = AccountIdByAccountDatabase(model.AccountDatabaseId);
                if (accountId == null)
                {
                    logger.Warn($"Аккаунт по айди БД {model.AccountDatabaseId} не найден");
                    return PreconditionFailed($"Аккаунт по айди БД {model.AccountDatabaseId} не найден");
                }

                AccessProvider.HasAccess(ObjectAction.AccountDatabaseBackup_Add, () => accountId);
                var validateResult = registerAccountDatabaseBackupValidator.Validate(model);
                if (!validateResult.Success)
                {
                    logger.Warn(validateResult.Message);
                    return PreconditionFailed(validateResult.Message);
                }

                accountDatabaseBackupRegistrator.RegisterBackup(model);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка регистрации копии ИБ]. Ошибка регистрации копии ИБ {model.AccountDatabaseId} по пути {model.BackupFullPath}.");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить бэкап инф. базы
        /// </summary>
        /// <param name="model">Параметры удаления бэкапа инф. базы</param>
        /// <returns>Результат выполения</returns>
        public ManagerResult DeleteBackup(DeleteAccountDatabaseBackupRequestDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabaseBackup_Delete, () => AccountIdBySmAcDbBackupId(model.ServiceManagerAcDbBackupId));
                deleteAccountDatabaseBackupProvider.Delete(model);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка удаления бэкапа по ID от МС] {model.ServiceManagerAcDbBackupId}.");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
