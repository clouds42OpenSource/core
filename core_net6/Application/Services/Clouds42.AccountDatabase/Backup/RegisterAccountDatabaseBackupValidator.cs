﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Models;

namespace Clouds42.AccountDatabase.Backup
{
    /// <summary>
    /// Валидатор регистрации бекапа информационной базы
    /// </summary>
    public class RegisterAccountDatabaseBackupValidator(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Провалидировать модель регистрации бекапа
        /// </summary>
        /// <param name="model">Модель регистарации
        /// бэкапа инфомационной базы</param>
        /// <returns>Результат валидации</returns>
        public ValidateResultModel Validate(RegisterAccountDatabaseBackupRequestDto model)
        {
            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == model.AccountDatabaseId);

            if (accountDatabase == null)
                return new ValidateResultModel
                {
                    Message = $"Информационная база по Id= {model.AccountDatabaseId} не найдена."
                };

            return new ValidateResultModel{Success = true};
        }
    }
}
