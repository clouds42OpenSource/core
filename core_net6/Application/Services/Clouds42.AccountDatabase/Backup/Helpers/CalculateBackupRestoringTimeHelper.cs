﻿using Clouds42.AccountDatabase.Contracts.Backup.Models;

namespace Clouds42.AccountDatabase.Backup.Helpers
{
    /// <summary>
    /// Хелпер для рассчета времени восстановления бекапа 
    /// </summary>
    public static class CalculateBackupRestoringTimeHelper
    {
        /// <summary>
        /// Получить размер базы в Гб
        /// </summary>
        /// <param name="sizeInMb">Размер базы в Мб</param>
        /// <returns>Размер базы в Гб</returns>
        public static int GetDbSizeInGb(int sizeInMb) => (int) Math.Ceiling((decimal) sizeInMb / 1024);

        /// <summary>
        /// Находится ли значение в промежутке
        /// </summary>
        /// <param name="value">Проверяемое значение</param>
        /// <param name="start">Начальное значение промежутка</param>
        /// <param name="finish">Конечное значение промежутка</param>
        /// <returns>Результат проверки</returns>
        public static bool IsValueEntryIntoGap(this int value, int start, int finish) =>
            value > start && value < finish;

        /// <summary>
        /// Увеличить значение на коэфициент запаса времени для восстановления
        /// </summary>
        /// <param name="value">Значение для увелчение</param>
        /// <returns>Увеличенное значение</returns>
        public static int IncreaseValueToThirtyPercents(this int value) =>
            (int) Math.Ceiling(value + (decimal) (value * RestoreBackupTimeConst.RecoveryTimeFactor));
    }
}
