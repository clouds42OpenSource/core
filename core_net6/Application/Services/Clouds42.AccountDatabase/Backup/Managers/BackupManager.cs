﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Backup.Models;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Tomb.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.Billing.BillingServices.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Guid = System.Guid;

namespace Clouds42.AccountDatabase.Backup.Managers
{
    /// <summary>
    /// Менеджер по работе с бэкапами инф. баз
    /// </summary>
    public class BackupManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IBackupProvider backupProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        BillingServiceTypeAccessProvider billingServiceTypeAccessProvider,
        StartProcessToWorkDatabaseManager startProcessToWorkDatabaseManager,
        ITombTaskCreatorProvider tombTaskCreatorProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Get data for recovery infobases from backup
        /// </summary>
        /// <param name="backupId">Backup identifier</param>
        /// <returns></returns>
        public ManagerResult<BackupRestoringDto> GetDataForRestoreAccountDatabase(Guid backupId)
        {
            try
            {
                var accountId = AccountIdByAccountDatabaseBackup(backupId);
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Restore, () => accountId);

                var data = backupProvider.GetDataForRestoreAccountDatabase(backupId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                var message = "[При получении данных для восстановления инф. базы из бекапа произошла ошибка]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<BackupRestoringDto>(ex.Message);
            }
        }

        /// <summary>
        /// Start the process of downloading the backup file
        /// </summary>
        /// <param name="backupId">Backup identifier</param>
        /// <param name="uploadedFileId">Uploaded file identifier</param>
        /// <returns></returns>
        public ManagerResult StartBackupFileFetching(Guid backupId, Guid uploadedFileId)
        {
            try
            {
                var accountId = AccountIdByAccountDatabaseBackup(backupId);
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Restore, () => accountId);

                backupProvider.StartBackupFileFetching(backupId, uploadedFileId);

                var message = $"Инициализация процесса загрузки файла бэкапа (Id={backupId}) завершена успешно";
                Logger.Trace(message);
                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[При инициализации процесса загрузки файла бэкапа произошла ошибка] (Id={backupId})";
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить информацию о статусе загрузки файла
        /// </summary>
        /// <param name="uploadedFileId">Идентификатор загружаемого файла</param>
        /// <returns>Модель информации о статусе загрузки файла</returns>
        public ManagerResult<UploadedFileInfoDto> GetUploadedFileStatusInfo(Guid uploadedFileId)
        {
            try
            {
                var accountId = AccountIdByUploadedFile(uploadedFileId);
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Restore, () => accountId);

                var result = backupProvider.GetUploadedFileStatusInfo(uploadedFileId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка проверки статуса загрузки бэкапа ИБ]. При проверке статуса загрузки файла (Id={uploadedFileId}) бэкапа инф. базы произошла ошибка.");
                return PreconditionFailed<UploadedFileInfoDto>(ex.Message);
            }
        }

        /// <summary>
        ///     Восстановить информационную базу из бекапа
        /// </summary>
        /// <param name="restoreAcDbFromBackupModel">Модель параметров восстановления инф. базы из бекапа</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult<CreateAccountDatabasesResultDto> RestoreAccountDatabase(
            RestoreAcDbFromBackupDto restoreAcDbFromBackupModel)
        {
            var accountDatabaseBackup =
                accountDatabaseDataProvider.GetAccountDatabaseBackupById(restoreAcDbFromBackupModel
                    .AccountDatabaseBackupId);
            
            if (accountDatabaseBackup == null)
                return PreconditionFailed<CreateAccountDatabasesResultDto>("Указанный бекап инф. базы не найден");
            
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Restore,
                    () => accountDatabaseBackup.AccountDatabase.AccountId);
                if (restoreAcDbFromBackupModel.RestoreType == AccountDatabaseBackupRestoreType.ToCurrent 
                    && restoreAcDbFromBackupModel.UsersIdForAddAccess.Count == 0 )
                {
                    var accountDatabaseByBackupId =
                        accountDatabaseDataProvider.GetAccountDatabaseByBackupIdOrThrowException(accountDatabaseBackup.Id);
                    restoreAcDbFromBackupModel.UsersIdForAddAccess =
                        accountDatabaseByBackupId.AcDbAccesses.Select(ac => ac.AccountUser.Id).ToList();
                }
                var result =
                    backupProvider.StartTaskForRestoreAccoundDatabase(accountDatabaseBackup.AccountDatabase.AccountId,
                        restoreAcDbFromBackupModel);

                if (!result.IsComplete)
                    return Ok(result);

                LogEvent(() => accountDatabaseBackup.AccountDatabase.AccountId, LogActions.RestoreAccountDatabase,
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabaseBackup.AccountDatabase)} поставлена в очередь на восстановление в " +
                    $"{(restoreAcDbFromBackupModel.RestoreType == AccountDatabaseBackupRestoreType.ToNew ? $"новую базу с именем \"{restoreAcDbFromBackupModel.AccountDatabaseName}\"" : "текущую базу")}.");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка восстановления базы из архива] {accountDatabaseBackup.AccountDatabase?.V82Name}.");

                LogEvent(() => accountDatabaseBackup.AccountDatabase?.AccountId, LogActions.RestoreAccountDatabase,
                    $"Не удалось восстановить базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabaseBackup.AccountDatabase)}. Описание ошибки: {ex.Message}");

                return PreconditionFailed<CreateAccountDatabasesResultDto>(ex.Message);
            }
        }

        public ManagerResult<CreateAccountDatabasesResultDto> BackupAccountDatabase(SendDatabaseToArchiveOrBackupDto dto)
        {

            var databaseV82Name = DbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == dto.DatabaseId)?.V82Name;

            switch (dto.Operation)
            {
                case DatabaseOperation.Archive:
                    var result = startProcessToWorkDatabaseManager.StartProcessOfArchiveDatabasesToTomb(
                        [dto.DatabaseId]);

                    if (result.Error)
                        return PreconditionFailedError(new CreateAccountDatabasesResultDto { ErrorMessage = result.Message }, result.Message);

                    LogEvent(() => dto.AccountId, LogActions.MovingDbToArchive,
                        $"Запуск процесса пользователем переноса в архив базы {databaseV82Name ?? dto.DatabaseId.ToString()}",
                        AccessProvider.GetUser()?.Id ?? AccessProvider.ContextAccountId);
                    break;


                case DatabaseOperation.Backup:
                case DatabaseOperation.TechnicalBackup:
                    tombTaskCreatorProvider.CreateTaskForDeleteAccountDatabaseToTombForBackup(new DeleteAccountDatabaseToTombJobParamsDto
                    {
                        AccountDatabaseId = dto.DatabaseId,
                        SendMail = false,
                        ForceUpload = true,
                        Trigger = dto.Operation == DatabaseOperation.Backup ? CreateBackupAccountDatabaseTrigger.ManualArchive : CreateBackupAccountDatabaseTrigger.TechnicalBackup,
                        InitiatorId = AccessProvider.GetUser()?.Id ?? DbLayer.AccountUsersRepository.FirstOrDefault(x => x.AccountId == dto.AccountId).Id,
                        ActionsForSkip =
                        [
                            "DropSessionsFromClusterAction",
                            "UnpublishAccountDatabaseAction",
                            "DeleteLocalAccountDatabaseAction",
                            "ActualizeAccountDatabaseAfterDeleteToTombAction"
                        ]
                    });

                    var operation = dto.Operation == DatabaseOperation.Backup ? "создания бэкапа" : "создания технического бэкапа";

                    LogEvent(() => dto.AccountId, LogActions.MovingDbToArchive,
                        $"Запуск процесса пользователем {operation} для базы {databaseV82Name ?? dto.DatabaseId.ToString()}",
                        AccessProvider.GetUser()?.Id ?? AccessProvider.ContextAccountId);
                    break;

                default:
                    return PreconditionFailedError(
                        new CreateAccountDatabasesResultDto
                        {
                            ErrorMessage = "Не удалось распознать тип действия с базой данных"
                        }, "Не удалось распознать тип действия с базой данных");
            }

            if (dto.Operation == DatabaseOperation.TechnicalBackup)
            {
                return Ok(new CreateAccountDatabasesResultDto { IsComplete = true });
            }

            Logger.Info("Начало создания платежа");
            var paymentResult = PayService(new PayBackupModelDto
            {
                AccountId = dto.AccountId,
                ServiceId = dto.ServiceId,
                PaymentSum = dto.Cost,
                UsePromisePayment = dto.UsePromisePayment
            });

            if (!paymentResult.Complete)
                return PreconditionFailedError(new CreateAccountDatabasesResultDto
                {
                    Amount = paymentResult.Amount,
                    Currency = paymentResult.Currency,
                    NotEnoughMoney = paymentResult.NotEnoughMoney,
                    CanUsePromisePayment = paymentResult.CanUsePromisePayment,
                    ErrorMessage = paymentResult.ErrorMessage
                }, paymentResult.ErrorMessage);


            return Ok(new CreateAccountDatabasesResultDto { IsComplete = true });
        }

        /// <summary>
        /// Pay the service backup for account
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public ManagerResult<PaymentServiceResultDto> PayBackup(PayBackupModelDto dto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabaseBackup_Add, () => AccessProvider.ContextAccountId);

                var paymentResult = PayService(dto);

                return paymentResult.Complete ? Ok(paymentResult) : PreconditionFailedError(paymentResult, paymentResult.ErrorMessage);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Ошибка списания во время отправки в архив");

                return PreconditionFailed<PaymentServiceResultDto>("Во время оплаты бэкапа/архива произошла ошибка", ex);
            }
        }

        /// <summary>
        /// Pay the service for the account
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private PaymentServiceResultDto PayService(PayBackupModelDto dto)
        {
            try
            {
                var paymentResult = billingServiceTypeAccessProvider.PaymentService(dto.ServiceId, dto.AccountId,
                    dto.PaymentSum, dto.UsePromisePayment);

                if (paymentResult.Complete)
                    LogEvent(() => dto.AccountId, LogActions.MovingDbToArchive,
                        $"Было списано {dto.PaymentSum} {paymentResult.Currency} за услугу бэкапирования");
                else
                {
                    Logger.Warn($"[Ошибка списания за услугу бэкопирования] {paymentResult.ErrorMessage}.");
                    LogEvent(() => dto.AccountId, LogActions.MovingDbToArchive,
                        $"Не удалось списать денежные средства в размере {dto.PaymentSum} {paymentResult.Currency} за услугу бэкапирования, по причине: {paymentResult.ErrorMessage}");
                }

                return paymentResult;
            }

            catch (Exception e)
            {
                _handlerException.Handle(e,
                    $"[Ошибка списания за услугу бэкопирования] {e.Message}.");
                LogEvent(() => dto.AccountId, LogActions.MovingDbToArchive,
                    $"Не удалось списать денежные средства в размере {dto.PaymentSum} за услугу бэкапирования");

                return new PaymentServiceResultDto { Complete = false, ErrorMessage = e.Message, };
            }
            
        }
    }
}
