﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Backup.Managers
{
    /// <summary>
    /// Провайдер загрузки файлов бэкапов
    /// </summary>
    public class BackupFileManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IBackupFileProvider backupFileProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Загрузить файл бэкапа
        /// </summary>
        /// <param name="fetchingBackupparams">Параметры задачи по загрузке файла бэкапа</param>
        /// <returns>Ok - если файл бэкапа загружен успешно</returns>
        public ManagerResult FetchBackupFile(BackupFileFetchingJobParamsDto fetchingBackupparams)
        {
            try
            {
                var accountDatabaseBackup = DbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup =>
                    backup.Id == fetchingBackupparams.BackupId);
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_DownloadFromTomb, () => accountDatabaseBackup.AccountDatabase.AccountId);

                backupFileProvider.FetchBackupFile(fetchingBackupparams);

                logger.Trace($"Бэкап инф. базы {fetchingBackupparams.BackupId} загружен успешно");
                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    $"{fetchingBackupparams.BackupId}::[При инициализации процесса загрузки бэкапа инф. базы произошла ошибка]";
                _handlerException.Handle(ex, message);
                throw;
            }
        }
    }
}
