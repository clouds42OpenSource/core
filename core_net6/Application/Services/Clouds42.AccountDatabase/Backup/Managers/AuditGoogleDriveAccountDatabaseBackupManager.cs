﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Backup.Managers
{
    /// <summary>
    /// Менеджер для аудита бэкапов инф. баз
    /// </summary>
    public class AuditGoogleDriveAccountDatabaseBackupManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAuditGoogleDriveAccountDatabaseBackupProvider auditGoogleDriveAccountDatabaseBackupProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Провести аудит бэкапов
        /// </summary>
        /// <returns>Ok - если операция завершена успешно</returns>
        public ManagerResult ProcessAudit()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabaseBackup_Audit);

                auditGoogleDriveAccountDatabaseBackupProvider.ProcessAudit();
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка проведения аудита бэкапов инф. баз]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
