﻿using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Clouds42.AccountDatabase.IISApplication
{
    public class IisHttpClient
    {
        private readonly string? _iisApiUrl;
        private const string ContentType = "application/hal+json";
        private readonly HttpClient _httpClient;
        public IisHttpClient(IConfiguration configuration, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.DefaultRequestHeaders.Add("Access-Token", $"Bearer {configuration.GetRequiredSection("IISConfiguration:BearerToken")?.Value}");
            _httpClient.DefaultRequestHeaders.Add("Accept", ContentType);
            _iisApiUrl = configuration.GetRequiredSection("IISConfiguration:ApiUrl")?.Value;
        }

        public T? Get<T>(string host, string link)
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, BuildUri(host, link));
            var response = _httpClient.Send(httpRequestMessage);

            return HandleResponse<T>(response, null, link, host);
        }

        public void Delete(string host, string link)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, BuildUri(host, link));
            var response = _httpClient.Send(requestMessage);
            HandleResponse<string>(response, null, link, host);
        }

        public T? Post<TRequest, T>(string host, TRequest model, string link)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, BuildUri(host, link))
            {
                Content = GetRequestData(model)
            };

            var response = _httpClient.Send(requestMessage);

            return HandleResponse<T?>(response, model, link, host);
        }

        public T? Patch<TRequest, T>(string host, TRequest model, string link)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Patch, BuildUri(host, link))
            {
                Content = GetRequestData(model)
            };

            var response = _httpClient.Send(requestMessage);

            return HandleResponse<T?>(response, model, link, host);
        }

        private static JsonSerializerSettings GetJsonMediaTypeFormatter()
        {
            JsonSerializerSettings jsonFormat = new()
                {
                    NullValueHandling = NullValueHandling.Ignore, ContractResolver = new DefaultContractResolver { NamingStrategy = new SnakeCaseNamingStrategy() }
                };

            return jsonFormat;
        }

        private static StringContent GetRequestData<T>(T model)
        {
            return new StringContent(JsonConvert.SerializeObject(model, GetJsonMediaTypeFormatter()),  Encoding.UTF8, ContentType);
        }

        private static T? HandleResponse<T>(HttpResponseMessage response, object? model, string link, string host)
        {
            var content = response.Content.ReadAsStringAsync().Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Ошибка настройки IIS. " +
                                               $"Код {(int)response.StatusCode}. " +
                                               $"Тело ошибки {content}. " +
                                               $"Тело запроса {(model != null ? JsonConvert.SerializeObject(model) : "отсутствует")}. " +
                                               $"Адрес {link}"+
                                               $"Хост {host}");
            }

            return JsonConvert.DeserializeObject<T>(content, GetJsonMediaTypeFormatter());
        }

        private Uri BuildUri(string host, string link)
        {
            return new Uri(string.Concat(string.Format(_iisApiUrl!, host), link));
        }
    }
}
