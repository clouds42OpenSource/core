﻿using Clouds42.AccountDatabase.Contracts.IISApplication.Interfaces;
using Clouds42.AccountDatabase.Contracts.WebAdministration.Interfaces;
using Clouds42.AccountDatabase.IISApplication.Models;
using Clouds42.AccountDatabase.WebAdministration.WebAdministrationCommands;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Service.WebAdministration;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces;

namespace Clouds42.AccountDatabase.IISApplication.Providers
{
    /// <summary>
    /// Провайдер для работы с данными пула приложения на ноде публикаций (на IIS)
    /// </summary>
    internal class IisApplicationPoolDataProvider(
        IUnitOfWork dbLayer,
        IWebAdministrationPowerShellCommandExecutor webAdministrationPowerShellCommandExecutor,
        ICloudContentServerDataProvider cloudContentServerDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IIisApplicationPoolDataProvider
    {
        /// <summary>
        /// Получить данные для управления пулом приложения инф. базы на нодах публикации
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        /// <returns>Данные для управления пулом приложения инф. базы на нодах публикации</returns>
        public AcDbDataForManageIisAppPoolDto GetDataForManageDatabaseIisAplicationPool(Guid databaseId,
            string databaseNumber)
        {
            var tempDataForWorkinWithDatabaseApplication = GetTempDataForWorkinWithDatabaseApplicationPool(databaseId);

            var applicationPath = GetApplicationPath(tempDataForWorkinWithDatabaseApplication.AcDbV82Name,
                tempDataForWorkinWithDatabaseApplication.AccountId,
                tempDataForWorkinWithDatabaseApplication.ContentServerGroupedByAccount);

            var applicationPoolName = GetApplicationPoolNameOnIis(applicationPath,
                tempDataForWorkinWithDatabaseApplication.ContentServerId,
                tempDataForWorkinWithDatabaseApplication.PublishSiteName);

            return new AcDbDataForManageIisAppPoolDto
            {
                AppPoolName = applicationPoolName,
                ContentServerId = tempDataForWorkinWithDatabaseApplication.ContentServerId,
                PublishSiteName = tempDataForWorkinWithDatabaseApplication.PublishSiteName
            };
        }

        /// <summary>
        /// Получить название пула инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        /// <returns>Название пула инф. базы</returns>
        public string GetAccountDatabaseAppPoolName(Guid accountDatabaseId, string databaseNumber)
            => GetDataForManageDatabaseIisAplicationPool(accountDatabaseId, databaseNumber).AppPoolName;

        /// <summary>
        /// Получить состояние пула инф. базы на всех нодах публикации
        /// </summary>
        /// <param name="contentServerId">ID сервера публикаций</param>
        /// <param name="appPoolName">Название пула инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        /// <returns>Состояние пула инф. базы на всех нодах публикации</returns>
        public List<AppPoolStateDto> GetApplicationPoolStateOnAllPublishNodes(Guid contentServerId, string appPoolName,
            string databaseNumber)
        {
            var getAppPoolStateResultList = webAdministrationPowerShellCommandExecutor
                .ExcecuteCommandInMultipleThreads<GetAppPoolStateCommand, string,
                    List<AppPoolStateDto>>(
                    cloudContentServerDataProvider.GetContentServerPublishNodeAddresses(contentServerId), appPoolName);
            
            if (getAppPoolStateResultList.All(res => res.IsSuccess))
                return getAppPoolStateResultList.SelectMany(res => res.Result).ToList();

            var errorDescription = getAppPoolStateResultList.Where(res => !res.IsSuccess)
                .Select(res => res.ErrorMessage)
                .Aggregate((first, next) => $"{first}; {next}");

            throw new InvalidOperationException(
                $"Не удалось проверить состояние пула на всех нодах сервера публикации {contentServerId}. Причина: {errorDescription}");
        }

        /// <summary>
        /// Получить временные данные для работы с пулом приложения инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Временные данные для работы с пулом приложения инф. базы</returns>
        private TempDataForWorkinWithDatabaseApplicationPoolModel GetTempDataForWorkinWithDatabaseApplicationPool(
            Guid databaseId) =>
            (from database in dbLayer.DatabasesRepository.WhereLazy()
                join accountConfiguration in accountConfigurationDataProvider.GetAccountConfigurationsDataBySegment()
                    on database.AccountId equals accountConfiguration.Account.Id
                join contentServer in dbLayer.CloudServicesContentServerRepository.WhereLazy() on accountConfiguration
                    .Segment
                    .ContentServerID equals contentServer.ID
                where database.Id == databaseId
                select new TempDataForWorkinWithDatabaseApplicationPoolModel
                {
                    AcDbV82Name = database.V82Name,
                    AccountId = accountConfiguration.Account.Id,
                    ContentServerId = contentServer.ID,
                    PublishSiteName = contentServer.PublishSiteName,
                    ContentServerGroupedByAccount = contentServer.GroupByAccount
                })
            .FirstOrDefault() ??
            throw new NotFoundException(
                $"Не удалось получить данные для определения названия пула приложения инф. базы {databaseId}");

        /// <summary>
        /// Получить путь к приложению на IIS
        /// </summary>
        /// <param name="acDbV82Name">Номер инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="contentServerGroupedByAccount">Признак что сервер публикаций сгруппирован по аккаунту</param>
        /// <returns></returns>
        private static string GetApplicationPath(string acDbV82Name, Guid accountId, bool contentServerGroupedByAccount)
            => contentServerGroupedByAccount
                ? $"/{accountId.GetEncodeGuid()}/{acDbV82Name}"
                : $"/{acDbV82Name}";

        /// <summary>
        /// Получить название пула приложения на IIS
        /// </summary>
        /// <param name="appPath">Web путь приложения</param>
        /// <param name="contentServerId">ID сервера публикаций</param>
        /// <param name="publishSiteName">Название сайта публикаций</param>
        /// <returns>Название пула приложения</returns>
        private string GetApplicationPoolNameOnIis(string appPath, Guid contentServerId, string publishSiteName)
        {
            var commandResults = webAdministrationPowerShellCommandExecutor
                .ExcecuteCommandInMultipleThreads<GetAppPoolNameCommand, GetAppPoolNameCommandParamsDto, string>(
                    cloudContentServerDataProvider.GetContentServerPublishNodeAddresses(contentServerId),
                    new GetAppPoolNameCommandParamsDto
                    {
                        AcDbPublishPath = appPath,
                        PublishSiteName = publishSiteName
                    });

            var appPoolName = commandResults.FirstOrDefault(res => res.IsSuccess)?.Result;

            if (string.IsNullOrEmpty(appPoolName))
                throw new NotFoundException($"Не удалось найти название пула приложения по пути {appPath}.");

            return appPoolName;
        }
    }
}
