﻿using Clouds42.AccountDatabase.Contracts.IISApplication.Interfaces;
using Clouds42.AccountDatabase.Contracts.WebAdministration.Interfaces;
using Clouds42.AccountDatabase.WebAdministration.WebAdministrationCommands;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.WebAdministration;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.IISApplication.Providers
{
    /// <summary>
    /// Провайдер для управления пулом приложения на ноде публикаций (на IIS)
    /// </summary>
    internal class ManageIisApplicationPoolProvider(
        IIisApplicationPoolDataProvider iisApplicationPoolDataProvider,
        IWebAdministrationPowerShellCommandExecutor webAdministrationPowerShellCommandExecutor,
        ILogger42 logger)
        : IManageIisApplicationPoolProvider
    {
        /// <summary>
        /// Запустить пул приложения для инф. базы на всех нодах публикации
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        public void StartForDatabase(Guid databaseId, string databaseNumber)
        {
            var acDbDataForRestartIisAppPool = iisApplicationPoolDataProvider
                .GetDataForManageDatabaseIisAplicationPool(databaseId, databaseNumber);

            ProcessOperationWithAppPools<StartAppPoolCommand>(acDbDataForRestartIisAppPool.AppPoolName,
                () => GetPublishNodesOnWhichProcessOperation(acDbDataForRestartIisAppPool.AppPoolName,
                    acDbDataForRestartIisAppPool.ContentServerId, databaseNumber, false));
        }

        /// <summary>
        /// Остановить пул приложения для инф. базы на всех нодах публикации
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        public void StopForDatabase(Guid databaseId, string databaseNumber)
        {
            var acDbDataForRestartIisAppPool = iisApplicationPoolDataProvider
                .GetDataForManageDatabaseIisAplicationPool(databaseId, databaseNumber);

            ProcessOperationWithAppPools<StopAppPoolCommand>(acDbDataForRestartIisAppPool.AppPoolName,
                () => GetPublishNodesOnWhichProcessOperation(acDbDataForRestartIisAppPool.AppPoolName,
                    acDbDataForRestartIisAppPool.ContentServerId, databaseNumber));
        }

        /// <summary>
        /// Перезапустить пул приложения на указанных нодах
        /// </summary>
        /// <param name="appPoolName">Название пула</param>
        /// <param name="contentServerId">ID сервера публикаций</param>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        public void Restart(string appPoolName, Guid contentServerId, Guid databaseId, string databaseNumber)
        {
            var publishNodesOnWhichProcessOperation =
                GetPublishNodesOnWhichProcessOperation(appPoolName, contentServerId, databaseNumber);

            ProcessOperationWithAppPools<StopAppPoolCommand>(appPoolName, () => publishNodesOnWhichProcessOperation);
            
            ProcessOperationWithAppPools<StartAppPoolCommand>(appPoolName, () => publishNodesOnWhichProcessOperation);
        }

        /// <summary>
        /// Получить cписок нод на которых необходимо выполнить операцию
        /// </summary>
        /// <param name="appPoolName">Название пула</param>
        /// <param name="contentServerId">ID сервера публикаций</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        /// <param name="onlyWithWorkerProcesses">Флаг, указывающий что нужны ноды
        /// только с рабочими процессами</param>
        /// <returns>Список нод на которых необходимо выполнить операцию</returns>
        private List<string> GetPublishNodesOnWhichProcessOperation(string appPoolName, Guid contentServerId,
            string databaseNumber,
            bool onlyWithWorkerProcesses = true)
        {
            var operationStartDateTime = DateTime.Now;

            var appPoolStateList = iisApplicationPoolDataProvider
                .GetApplicationPoolStateOnAllPublishNodes(contentServerId, appPoolName, databaseNumber);

            logger.Trace(
                $"Получение состояния пула {appPoolName} на всех нодах публикации завершено. " +
                $"Затрачено {operationStartDateTime.CalculateElapsedTimeInSeconds():F1} сек.");

            return appPoolStateList
                .Where(state => !onlyWithWorkerProcesses || state.WorkerProcessesCount > 0)
                .Select(state => state.ServerName).ToList();
        }

        /// <summary>
        /// Выполнить операцию с пулами приложений
        /// </summary>
        /// <param name="appPoolName">Название пула приложения</param>
        /// <param name="getPublishNodesFunc">Функция для получения списка нод, на которых необходимо выполнить операцию</param>
        private void ProcessOperationWithAppPools<TCommand>(string appPoolName, Func<List<string>> getPublishNodesFunc)
            where TCommand : IWebAdministrationPowerShellCommand<string, bool>
        {
            var publishNodesOnWhichProcessOperation = getPublishNodesFunc();

            if (!publishNodesOnWhichProcessOperation.Any())
            {
                logger.Trace($"Не найдено ни одного пула {appPoolName} с запущенными рабочими процессами");
                return;
            }

            var operationDescription = typeof(TCommand) == typeof(StopAppPoolCommand) ? "остановки" : "запуска";
            var operationStartDateTime = DateTime.Now;

            var stopResultList = webAdministrationPowerShellCommandExecutor
                .ExcecuteCommandInMultipleThreads<TCommand, string, bool>(
                    publishNodesOnWhichProcessOperation, appPoolName);
            
            HandleExecutionResult(appPoolName, stopResultList);
            logger.Trace(
                $"Операция {operationDescription} пула {appPoolName} на всех нодах публикации завершена. " +
                $"Затрачено {operationStartDateTime.CalculateElapsedTimeInSeconds():F1} сек.");
        }

        /// <summary>
        /// Обработать результат выполнения
        /// </summary>
        /// <param name="appPoolName">Название пула</param>
        /// <param name="executionResultList">Список результатов выполнения операции</param>
        private static void HandleExecutionResult(string appPoolName,
            List<CommandExecutionResultDto<bool>> executionResultList)
        {
            if (executionResultList.All(res => res.IsSuccess))
                return;

            throw new InvalidOperationException(
                $"Не удалось перезапустить пул инф. базы {appPoolName} на всех нодах сервера публикации. " +
                $"Причина: {GetErrorDescription(executionResultList)}");
        }

        /// <summary>
        /// Получить описание ошибки
        /// </summary>
        /// <param name="executionResultList">Список результатов выполнения операции</param>
        /// <returns>Описание ошибки</returns>
        private static string GetErrorDescription(IEnumerable<CommandExecutionResultDto<bool>> executionResultList)
            => executionResultList.Where(res => !res.IsSuccess)
                .Select(res => res.ErrorMessage)
                .Aggregate((first, next) => $"{first}; {next}");
    }
}
