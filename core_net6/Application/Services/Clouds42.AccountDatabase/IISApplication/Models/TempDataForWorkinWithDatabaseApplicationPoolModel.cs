﻿namespace Clouds42.AccountDatabase.IISApplication.Models
{
    /// <summary>
    /// Модель временных данных дляработы с пулом приложения инф. базы
    /// </summary>
    public class TempDataForWorkinWithDatabaseApplicationPoolModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Номер информационной базы
        /// </summary>
        public string AcDbV82Name { get; set; }

        /// <summary>
        /// ID сервера публикаций
        /// </summary>
        public Guid ContentServerId { get; set; }

        /// <summary>
        /// Название сайта публикаций
        /// </summary>
        public string PublishSiteName { get; set; }

        /// <summary>
        /// Признак что сервер публикаций сгруппирован по аккаунту
        /// </summary>
        public bool ContentServerGroupedByAccount { get; set; }
    }
}
