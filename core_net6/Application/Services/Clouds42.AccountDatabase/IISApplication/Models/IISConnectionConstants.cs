﻿namespace Clouds42.AccountDatabase.IISApplication.Models
{
    public static class IISConnectionConstants
    {
        public static string IISError =
            "Dear it-engineer, " +
            "when accessing IIS-API {0}, " +
            "I could not find it, " +
            "please check that iis-api is installed on the server and correctly configured " +
            "(access token in the api-keys.json file, determine which user is running service, " +
            "check user read/write rights and user group (IIS API Administration Owners) See details here {1}";
    }
}
