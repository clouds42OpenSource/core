﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.StateMachine;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Internal;

namespace Clouds42.AccountDatabase.Audit.Providers
{
    /// <summary>
    /// Провайдер аудита и изменения статусов инф. баз
    /// </summary>
    internal class AuditAndChangeAccountDatabasesStatesProvider : IAuditAccountDatabasesStatesProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly ILogger42 _logger;
        private readonly Lazy<string> _cloudServicesEmail;
        private readonly Lazy<string> _hotlineEmail;
        private readonly DateTime _periodForProcessAudit = DateTime.Now.AddHours(-3);
        private readonly IAccountDatabaseChangeStateProvider _accountDatabaseChangeStateProvider;
        private readonly IProcessFlowRetryProcessor _processFlowRetryProcessor;
        private readonly IDeleteDelimiterCommand _deleteDelimiterCommand;
        /// <summary>
        /// Список правил маппинга данных инф. базы к признаку необходимости пропускать аудит
        /// </summary>
        private readonly List<(Func<AccountDatabaseDataForAuditStateDto, bool> CheckDatabaseState, bool NeedSkipAudit)>
            _listRulesForMapDatabaseDataToNeedSkipAuditFlag;

        public AuditAndChangeAccountDatabasesStatesProvider(IUnitOfWork dbLayer, 
            IProcessFlowRetryProcessor processFlowRetryProcessor,
            IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
            IDeleteDelimiterCommand deleteDelimiterCommand,
            ILogger42 logger)
        {
            _logger = logger;
            _dbLayer = dbLayer;
            _cloudServicesEmail = new Lazy<string>(CloudConfigurationProvider.Emails.GetCloudServicesEmail);
            _hotlineEmail = new Lazy<string>(CloudConfigurationProvider.Emails.GetHotlineEmail);
            _accountDatabaseChangeStateProvider = accountDatabaseChangeStateProvider;
            _processFlowRetryProcessor = processFlowRetryProcessor;
            _deleteDelimiterCommand = deleteDelimiterCommand;
            _listRulesForMapDatabaseDataToNeedSkipAuditFlag =
            [
                (databaseData => databaseData.State == DatabaseState.NewItem.ToString() &&
                                 (databaseData.AccountDatabaseOnDelimiters?.LoadTypeEnum ==
                                  DelimiterLoadType.Restored ||
                                  databaseData.AccountDatabaseOnDelimiters?.LoadTypeEnum ==
                                  DelimiterLoadType.UserUploadZip) &&
                                 (DateTime.Now - databaseData.StateDateTime).TotalHours <= 12, true)
            ];
        }

        /// <summary>
        /// Провести аудит
        /// </summary>
        public void ProcessAudit()
        {
            var databasesForAudit = GetDatabasesForAudit();
            if (!databasesForAudit.Any())
                return;

            _logger.Info($"Для аудита выбрано {databasesForAudit.Count} инф. баз");

            var messageBuilder = new StringBuilder();
            var databasesWithErrorStateCount = 0;
            databasesForAudit.ForEach(accountDatabase =>
            {
                var auditSuccess = ProcessAccountDatabaseAuditAndChangeStatus(accountDatabase, messageBuilder);
                if (auditSuccess)
                    return;
                databasesWithErrorStateCount++;
            });

            var databasesForManualAutoUpdate = _dbLayer.UpdateNodeQueueRepository
                .AsQueryableNoTracking()
                .ToList();
            if(databasesWithErrorStateCount > 0)
                messageBuilder.Insert(0, $"<p>Кол-во баз с проблемными статусами - {databasesWithErrorStateCount}.</p>");
            if (databasesForManualAutoUpdate.Count > 0)
                messageBuilder.Insert(0, $"<p>Кол-во баз в очереди на ручное обновление - {databasesForManualAutoUpdate.Count}. {string.Join("|", databasesForManualAutoUpdate.Select(x => x.AccountDatabaseId))}</p>");

            if(databasesWithErrorStateCount != 0 && databasesForManualAutoUpdate.Count != 0)
            SendAuditResultLetter(messageBuilder.ToString());
        }

        /// <summary>
        /// Провести аидит и изменить статус базы
        /// </summary>
        /// <param name="databaseData">Данные базы для аудита</param>
        /// <param name="emailContentBuilder">Билдер содержимого письма</param>
        /// <returns>Результат аудита</returns>
        private bool ProcessAccountDatabaseAuditAndChangeStatus(AccountDatabaseDataForAuditStateDto databaseData, StringBuilder emailContentBuilder)
        {
            try
            {
                var needSkipAudit = _listRulesForMapDatabaseDataToNeedSkipAuditFlag
                .FirstOrDefault(data => data.CheckDatabaseState(databaseData)).NeedSkipAudit;
                if (needSkipAudit)
                    return true;
                var stateDatabase = GetAccountDatabaseState(databaseData.State);
                switch (stateDatabase)
                {
                    case (DatabaseState.NewItem):
                        if (databaseData.AccountDatabaseOnDelimiters == null)
                            ChangeAccountDatabaseStateAndWriteResult(databaseData, emailContentBuilder, DatabaseState.ErrorCreate);
                        else
                            emailContentBuilder.Append($"<p>[{databaseData.Id}]::[{databaseData.V82Name}] - {databaseData.State}. Последнее время установки статуса: {databaseData.StateDateTime}</p>");
                        break;
                    case (DatabaseState.ProcessingSupport):
                        ChangeAccountDatabaseStateAndWriteResult(databaseData, emailContentBuilder, DatabaseState.Ready);
                        break;
                    case (DatabaseState.DelitingToTomb):
                    case (DatabaseState.RestoringFromTomb):
                        if (databaseData.AccountDatabaseOnDelimiters == null)
                            emailContentBuilder.Append(
                                $"<p>[{databaseData.Id}]::[{databaseData.V82Name}] - {databaseData.State}. {RetryProcessFlowData(databaseData.Id)}</p>");
                        else
                        {
                            var result = _deleteDelimiterCommand.Execute(databaseData.Id, "Ru-ru");
                            if (!string.IsNullOrEmpty(result))
                                ChangeAccountDatabaseStateAndWriteResult(databaseData, emailContentBuilder,
                                    DatabaseState.DeletedFromCloud, result);
                            else
                                emailContentBuilder.Append(
                                $"<p>[{databaseData.Id}]::[{databaseData.V82Name}] - {databaseData.State}. Запрос на удаление отправлен повторно в МС.</p>");
                        }
                        break;
                    default:
                        emailContentBuilder.Append($"<p>[{databaseData.Id}]::[{databaseData.V82Name}] - {databaseData.State}. Последнее время установки статуса: {databaseData.StateDateTime}</p>");
                        break;
                }
                return false;
            }
            catch (Exception ex)
            {
                emailContentBuilder.Append($"<p>[{databaseData.Id}]::[{databaseData.V82Name}] - {databaseData.State}. При аудите произошла ошибка: {ex.GetFullInfo()}</p>");
                return false;
            }


        }

        /// <summary>
        /// Получить список баз для аудита
        /// </summary>
        /// <returns>Список баз для аудита</returns>
        private List<AccountDatabaseDataForAuditStateDto> GetDatabasesForAudit()
            => (from acDb in _dbLayer.DatabasesRepository.WhereLazy()
                join dbOnDelimiters in _dbLayer.AccountDatabaseDelimitersRepository.WhereLazy() on acDb.Id equals dbOnDelimiters.AccountDatabaseId
                into delemiters
                from acDbOnDelimiters in delemiters.Take(1).DefaultIfEmpty()
                where acDb.State != DatabaseState.Ready.ToString() &&
                      acDb.State != DatabaseState.DetachedToTomb.ToString() &&
                      acDb.State != DatabaseState.DeletedToTomb.ToString() &&
                      acDb.State != DatabaseState.DeletedFromCloud.ToString() &&
                      acDb.State != DatabaseState.ErrorCreate.ToString() &&
                      acDb.State != DatabaseState.ErrorDtFormat.ToString() &&
                      acDb.StateDateTime <= _periodForProcessAudit
                select new AccountDatabaseDataForAuditStateDto
                {
                    Id = acDb.Id,
                    V82Name = acDb.V82Name,
                    State = acDb.State,
                    StateDateTime = acDb.StateDateTime,
                    AccountDatabaseOnDelimiters = acDbOnDelimiters
                }).OrderBy(acDb => acDb.StateDateTime).ToList();

        /// <summary>
        /// Получить статус базы Enum
        /// </summary>
        /// <param name="databaseState">Состояние базы</param>
        /// <returns>enum статус базы</returns>
        private DatabaseState GetAccountDatabaseState(string databaseState) => (DatabaseState)Enum.Parse(typeof(DatabaseState), databaseState);


        /// <summary>
        /// Сменить состояние базы и записать результат
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <param name="databaseState">Состояние</param>
        /// <param name="comment">комментарий</param>
        private void ChangeAccountDatabaseStateAndWriteResult(AccountDatabaseDataForAuditStateDto databaseData, StringBuilder emailContentBuilder,
            DatabaseState databaseState, string comment = null)
        {
            _accountDatabaseChangeStateProvider.ChangeState(databaseData.Id, databaseState, comment);
            emailContentBuilder.Append($"<p>[{databaseData.Id}]::[{databaseData.V82Name}] - {databaseData.State}. Был изменен на { databaseState}</p>");
        }


        /// <summary>
        /// Перезапустить процесс состояние базы
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <returns>Результат перезапуска процесса</returns>
        private string RetryProcessFlowData(Guid accountDatabaseId)
        {
            try
            {
                var processFlowsQuery = _dbLayer.ProcessFlowRepository.WhereLazy(p =>
                    p.Status == StateMachineComponentStatus.Error && p.ProcessedObjectName.ToLower().Contains(accountDatabaseId.ToString().ToLower())).FirstOrDefault();

                if (processFlowsQuery == null)
                    return "Процесс не найден";
                try
                {
                    _processFlowRetryProcessor.Retry(processFlowsQuery.Id);
                    return "Процесс был перезапущен";
                }
                catch (Exception ex)
                {
                    return $"При перезапуске процесса произошла ошибка: {ex.GetFullInfo()}";
                }

            }
            catch (Exception ex)
            {
                return $"Не возможно перезапустить процесс, произошла ошибка:  {ex.GetFullInfo()}";
            }



        }

        /// <summary>
        /// Отправить письмо с результатами аудита и выполненными дествиями
        /// </summary>
        /// <param name="emailContent">Содержимое письма</param>
        private void SendAuditResultLetter(string emailContent)
            => new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(_cloudServicesEmail.Value)
                .Copy(_hotlineEmail.Value)
                .Body(emailContent)
                .Subject("Проблемные статусы баз")
                .SendViaNewThread();
    }
}
