﻿using System.Text.RegularExpressions;
using System.Xml;
using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.IISApplication.Constants;
using Clouds42.AccountDatabase.Contracts.IISApplication.Models;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Audit.Providers
{
    /// <summary>
    /// Провайдер аудита путей публикации информационной базы
    /// </summary>
    public class PublishingPathsDatabaseAuditProvider(
        IUnitOfWork dbLayer,
        AccountDatabasePathHelper accountDatabasePathHelper,
        WebConfigHelper webConfigHelper,
        IDatabasePlatformVersionHelper databasePlatformVersionHelper,
        ILogger42 logger,
        ISegmentHelper segmentHelper,
        IisHttpClient connectionProviderIis)
        : IPublishingPathsDatabaseAuditProvider
    {
        private Domain.DataModels.AccountDatabase _accountDatabase;
        private bool _isFile;

        /// <summary>
        /// Регулярное выражение для получения серверов из VRD файла
        /// </summary>
        private static readonly Regex MaskForServerDb = new(@"Srvr=""(?'Srvr'\S*)"";Ref=""(?'Ref'\S*)"";",
            RegexOptions.None, TimeSpan.FromSeconds(.5));

        /// <summary>
        /// Регулярное выражение для поиска серверов в строке подключения к инф. базе
        /// </summary>
        private static readonly Regex RegExpForSearchServers = new(@"Srvr=(?'Srvr'\S*);Ref=(?'Ref'\S*);",
            RegexOptions.None, TimeSpan.FromSeconds(.5));

        /// <summary>
        /// Регулярное выражение для поиска пути к файловой базе в VRD файле
        /// </summary>
        private static readonly Regex MaskForFileDb = new(@"File=""(?'File'\S+)""");

        private AuditResultDto _AuditResult { get; set; }

        /// <summary>
        /// Выполнить аудит информационной базы
        /// </summary>
        /// <returns>Результат аудита</returns>
        public AuditResultDto AccountDatabaseAudit(Domain.DataModels.AccountDatabase accountDatabase)
        {
            try
            {
                _AuditResult = new AuditResultDto { Success = true };
                var resourseConfiguration = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == accountDatabase.AccountId
                && rc.BillingService.SystemService == Clouds42Service.MyEnterprise);
                if (resourseConfiguration.FrozenValue)
                    return _AuditResult;

                var accountContentServer = segmentHelper.GetContentServer(accountDatabase.Account);
                var publishSiteName = segmentHelper.GetPublishSiteName(accountDatabase.Account);
                _accountDatabase = accountDatabase;
                _isFile = accountDatabase.IsFile ?? false;
                var contentServerNode = dbLayer.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);

                var rootSites = connectionProviderIis.Get<Root>(contentServerNode.PublishNodeReference.Address, IISLinksConstants.WebSites);

                if (rootSites?.Websites == null || !rootSites.Websites.Any())
                {
                    throw new InvalidOperationException("Нода не доступна");
                }
                var connectedNode =
                    $"Ip: {contentServerNode.PublishNodeReference.Address} | Имя: {contentServerNode.PublishNodeReference.Description}";
                var site = rootSites.Websites.FirstOrDefault(x => x.Name == publishSiteName);
                if (site == null)
                    throw new InvalidOperationException($"Не найдено опубликованное приложение {publishSiteName} на ноде {contentServerNode.PublishNodeReference.Address}");
                var host = contentServerNode.PublishNodeReference.Address;
                var siteInfo = connectionProviderIis.Get<SiteModel>(host, site.Links.Self.Href);
                var accountEncodeId = _accountDatabase.Account.GetEncodeAccountId();
                var publishedDatabasePath =
                    segmentHelper.GetPublishedDatabasePhycicalPath(siteInfo?.PhysicalPath, accountEncodeId,
                        _accountDatabase.V82Name, _accountDatabase.Account);

                var vrdFullPath = Path.Combine(publishedDatabasePath, "default.vrd");

                if (!CheckFileOnPath(vrdFullPath, accountContentServer.Name, publishedDatabasePath, connectedNode))
                    return _AuditResult;

                var link = segmentHelper.GetWindowsThinkClientDownloadLink(accountDatabase.Account, accountDatabase);
                var xmlDocument = vrdFullPath.GetXmlDocumentFromPath();
                if (!CheckFileForValidity(xmlDocument, accountContentServer.Name, publishedDatabasePath,
                    connectedNode, link))
                    return _AuditResult;

                CompareAccountDatabaseConnectionString(xmlDocument, accountContentServer, publishedDatabasePath);
                CompareWebConfigPlatformVersionWithSegment(accountContentServer, publishedDatabasePath);

                return _AuditResult;
            }
            catch (Exception ex)
            {
                logger.Warn(
                    $"Во время выполнения аудита базы Id = {_accountDatabase.Id} произошла ошибка:{ex.Message}");
                HandleErrorMessage("Произошла ошибка во время аудита, возможно сервер выключен, для доп инфы см. логи");
                return _AuditResult;
            }
        }

        /// <summary>
        /// Сравнить версию платформы в веб конфиге и по сегменту
        /// </summary>
        /// <param name="accountContentServer">Сервер публикации аккаунта</param>
        /// <param name="publishedDatabasePhysicalPath">Физический путь к опубликованной базе</param>
        private void CompareWebConfigPlatformVersionWithSegment(CloudServicesContentServer accountContentServer,
            string publishedDatabasePhysicalPath)
        {
            var webExtension1CPathFromWebConfig =
                webConfigHelper.Get1CWebExtensionPath(_accountDatabase.AccountId, publishedDatabasePhysicalPath);

            if (string.IsNullOrEmpty(webExtension1CPathFromWebConfig))
                return;

            var webExtensionPathFromSegment = databasePlatformVersionHelper.GetPathToModule1C(_accountDatabase);

            if (webExtension1CPathFromWebConfig.Equals(webExtensionPathFromSegment,
                StringComparison.InvariantCultureIgnoreCase))
                return;

            HandleErrorMessage(
                $@"На сервере {accountContentServer.Name} по пути {publishedDatabasePhysicalPath} в файле web.config не совпадают пути к модулю 1С. 
                    В файле {webExtension1CPathFromWebConfig}, по сегменту {webExtensionPathFromSegment}. ", AuditErrorType.WebConfigPaths);
        }

        /// <summary>
        /// Сравнить строки подключения к инф. базе
        /// </summary>
        /// <param name="vrdFileContent">Содержимое файла vrd</param>
        /// <param name="accountContentServer">Сервер публикаций аккаунта</param>
        /// <param name="publishedDatabasePath">Путь к опубликованной базе</param>
        private void CompareAccountDatabaseConnectionString(XmlDocument vrdFileContent,
            CloudServicesContentServer accountContentServer, string publishedDatabasePath)
        {
            var acDbPathInVrdFile = GetPathOfFile(vrdFileContent, accountContentServer.Name, publishedDatabasePath);

            if (string.IsNullOrEmpty(acDbPathInVrdFile))
                return;

            var acDbPathInSegment = GetPathBySegment();

            if (string.IsNullOrEmpty(acDbPathInSegment))
                return;

            CompareFileDatabasePaths(acDbPathInVrdFile, acDbPathInSegment, accountContentServer,
                publishedDatabasePath);

            CompareServersInAcDbConnectionStrings(acDbPathInVrdFile, acDbPathInSegment, accountContentServer,
                publishedDatabasePath);
        }

        /// <summary>
        /// Сравнить пути к файловой инф. базе
        /// </summary>
        /// <param name="acDbPathInVrdFile">Строка подключения из VRD файла</param>
        /// <param name="acDbPathInSegment">Строка подключения из сегмента</param>
        /// <param name="accountContentServer">Сервер публикаций аккаунта</param>
        /// <param name="publishedDatabasePath">Путь к опубликованной базе</param>
        private void CompareFileDatabasePaths(string acDbPathInVrdFile, string acDbPathInSegment,
            CloudServicesContentServer accountContentServer, string publishedDatabasePath)
        {
            if (!_isFile)
                return;

            if (acDbPathInVrdFile.Equals(acDbPathInSegment, StringComparison.InvariantCultureIgnoreCase))
                return;

            HandleErrorMessage(
                $@"На сервере {accountContentServer.Name} по пути {publishedDatabasePath} в файле default.vrd не совпадают пути публикации. 
                    В файле {acDbPathInVrdFile} , по сегменту {acDbPathInSegment}.", AuditErrorType.VrdConfigPaths);
        }

        /// <summary>
        /// Сравнить сервера в строках подключения к инф. базе
        /// </summary>
        /// <param name="acDbPathInVrdFile">Строка подключения из VRD файла</param>
        /// <param name="acDbPathInSegment">Строка подключения из сегмента</param>
        /// <param name="accountContentServer">Сервер публикаций аккаунта</param>
        /// <param name="publishedDatabasePath">Путь к опубликованной базе</param>
        private void CompareServersInAcDbConnectionStrings(string acDbPathInVrdFile, string acDbPathInSegment,
            CloudServicesContentServer accountContentServer, string publishedDatabasePath)
        {
            if (_isFile)
                return;

            var serversInVrd = GetServersFromConnectionString(acDbPathInVrdFile);
            var serversInSegment = GetServersFromConnectionString(acDbPathInSegment);

            if (serversInSegment.Any(srv => serversInVrd.Contains(srv)))
                return;

            HandleErrorMessage(
                @$"На сервере {accountContentServer.Name} по пути {publishedDatabasePath} в файле default.vrd не совпадают пути публикации. 
                    В файле {acDbPathInVrdFile}, по сегменту {acDbPathInSegment}.", AuditErrorType.VrdConfigPaths);
        }

        /// <summary>
        /// Получить список серверов из строки подключения
        /// </summary>
        /// <param name="connectionString">Строка подключения</param>
        /// <returns>Список серверов</returns>
        private List<string> GetServersFromConnectionString(string connectionString)
        {
            var servers = RegExpForSearchServers.Match(connectionString).Groups["Srvr"]?.Value ?? string.Empty;
            return servers.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        /// <summary>
        /// Проверить файл по пути
        /// </summary>
        /// <param name="vrdFullPath">Путь к файлу</param>
        /// <param name="iisIp">Ip сервера</param>
        /// <param name="basePath">Физический путь к файлу</param>
        /// <param name="connectedNode">Адрес подключенной ноды сервера публикации</param>
        /// <returns>Наличие файла по пути</returns>
        private bool CheckFileOnPath(string vrdFullPath, string iisIp, string basePath, string connectedNode)
        {
            if (File.Exists(vrdFullPath))
                return true;

            HandleErrorMessage(
                $"На сервере {iisIp} (Нода - {connectedNode}) по пути {basePath} нет файла default.vrd.", AuditErrorType.VrdConfigPaths);
            return false;
        }

        /// <summary>
        /// Проверить файл на валидность
        /// </summary>
        /// <param name="xml">Xml документ</param>
        /// <param name="iisIp">Ip сервера</param>
        /// <param name="basePath">Физический путь к файлу</param>
        /// <param name="connectedNode">Нода</param>
        /// <returns>Валидность файла</returns>
        private bool CheckFileForValidity(XmlDocument xml, string iisIp, string basePath, string connectedNode, string? thinkClientPath)
        {
            var point = xml["point"];
            if (point == null)
            {
                HandleErrorMessage(
                    $"На сервере {iisIp} (Нода - {connectedNode}) по пути {basePath} не валидный default.vrd файл.");
                return false;
            }

            var ib = xml[nameof(point)].Attributes["ib"];
            if (ib == null)
            {
                HandleErrorMessage(
                    $"На сервере {iisIp} (Нода - {connectedNode}) по пути {basePath} не валидный default.vrd файл(отсутствует параметр ib).");
                return false;
            }

            var xmlpubdst = point.Attributes["pubdst"];

            if (xmlpubdst == null)
            {
                HandleErrorMessage(
                    $"На сервере {iisIp} (Нода - {connectedNode}) по пути {basePath} не валидный default.vrd файл(отсутствует параметр pubdst).", AuditErrorType.VrdConfigPaths);
                return false;
            }

            if (!string.IsNullOrEmpty(thinkClientPath) && xmlpubdst.Value != thinkClientPath)
            {
                HandleErrorMessage(
                    $"На сервере {iisIp} (Нода - {connectedNode}) по пути {basePath} не валидный default.vrd файл(значение pubdst текущее {xmlpubdst.Value}, а в сегменте указан {thinkClientPath}).", AuditErrorType.VrdConfigPaths);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Получить путь из файла
        /// </summary>
        /// <param name="xml">Xml файл</param>
        /// <param name="iisIp">Ip сервера</param>
        /// <param name="basePath">Физический путь к файлу</param>
        /// <returns>Путь из файла</returns>
        private string GetPathOfFile(XmlDocument xml, string iisIp, string basePath)
        {
            try
            {
                var ib = xml["point"].Attributes["ib"].Value;
                if (_isFile)
                {
                    var path = MaskForFileDb.Match(ib).Groups[nameof(File)]?.Value.TrimEnd(';') ?? string.Empty;

                    if (string.IsNullOrEmpty(path))
                        throw new InvalidOperationException("Не удалось получить значение пути для файловой базы из vrd file.");

                    return path;
                }

                var server = MaskForServerDb.Match(ib).Groups["Srvr"]?.Value ?? string.Empty;
                var v82Name = MaskForServerDb.Match(ib).Groups["Ref"]?.Value ?? string.Empty;

                if (string.IsNullOrEmpty(server) || string.IsNullOrEmpty(v82Name))
                    throw new InvalidOperationException("Не удалось получить значение пути для серверной базы из vrd file.");

                return $"Srvr={server};Ref={v82Name};";
            }
            catch (Exception ex)
            {
                HandleErrorMessage(
                    $"На сервере {iisIp} по пути {basePath} не валидный default.vrd файл. {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Получить путь по сегменту
        /// </summary>
        /// <returns>Путь по сегменту</returns>
        private string GetPathBySegment()
        {
            try
            {
                if (_isFile)
                {
                    var pathForFileDb = accountDatabasePathHelper.GetPath(_accountDatabase.Id);
                    if (string.IsNullOrEmpty(pathForFileDb))
                        throw new NotFoundException("Не удалось получить путь публикации.");

                    return pathForFileDb;
                }

                var curPlatform = string.IsNullOrEmpty(_accountDatabase.ApplicationName)
                    ? "8.2"
                    : _accountDatabase.ApplicationName.Trim();
                var server1C = curPlatform == "8.3"
                    ? segmentHelper.GetEnterpriseServer83(_accountDatabase.Account)
                    : segmentHelper.GetEnterpriseServer82(_accountDatabase.Account);

                var pathForServerDb = $"Srvr={server1C};Ref={_accountDatabase.V82Name};";

                if (string.IsNullOrEmpty(server1C))
                    throw new NotFoundException("Не удалось получить путь публикации для серверной базы.");

                return pathForServerDb;
            }
            catch (Exception ex)
            {
                HandleErrorMessage($"Во время получения пути по сегменту произошла ошибка. {ex}");
                return string.Empty;
            }
        }

        /// <summary>
        /// Обработать ошибку
        /// </summary>
        /// <param name="message">Текст ошибки</param>
        private void HandleErrorMessage(string message, AuditErrorType? errorType = null)
        {
            _AuditResult.Success = false;
            var textError = _accountDatabase.UsedWebServices.HasValue && _accountDatabase.UsedWebServices.Value 
                ? "Веб сервисы опубликованы" 
                : "Запущена таска переопубликации";
            message = errorType == null ? message : $"{textError}. Так как ошибка: {message}";
            _AuditResult.Message =
                $"{message} Для информационной базы ID = {_accountDatabase.Id}; V82Name = {_accountDatabase.V82Name};";
            _AuditResult.ErrorType = errorType;
            logger.Info(message);
        }
    }
}
