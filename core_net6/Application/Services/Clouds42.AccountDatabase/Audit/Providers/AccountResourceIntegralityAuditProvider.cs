﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit;
using Clouds42.Domain.DataModels.billing;
using Clouds42.LetterNotification.Helpers;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Audit.Providers
{
    /// <summary>
    /// Провайдер аудита проверки целостности услуг ресурсов всех аккаунтов
    /// </summary>
    public class AccountResourceIntegralityAuditProvider(
        IUnitOfWork dbLayer,
        IBillingServiceTypeRelationHelper billingServiceTypeRelationHelper,
        ILogger42 logger,
        IAccountConfigurationDataProvider accountConfProvider)
        : IAccountResourceIntegralityAuditProvider
    {
        /// <summary>
        /// Выполнить проверку целостности услуг ресурсов всех аккаунтов
        /// </summary>
        public void AuditAccountResourceIntegrality()
        {
            try
            {
                logger.Info("Начало выполнения аудита.");
                logger.Info("Поиск аккаунтов у которых отсутствуют конфигурации, необходимые для работы сервиса.");

                var accountBillingServiceTypes = GetListOfMissingResources();

                if (!accountBillingServiceTypes.Any())
                {
                    logger.Info("У аккаунтов не было найдено отсутствующих ресурсов.");
                    return;
                }

                logger.Info("Формирование сообщения");
                var message = GenearateMessage(accountBillingServiceTypes);

                logger.Info("Отправка письма");
                SendMail(message);
            }
            catch (Exception ex)
            {
                var message = $"Выполнение аудита завершилось ошибкой : {ex}";
                logger.Info(message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Получить все типы пропущенных услуг ресурсов для аккаунтов
        /// </summary>
        /// <returns>Список аккаунтов с отсутсвующими услугами</returns>
        public List<AccountBillingServiceTypesModelDto> GetListOfMissingResources()
        {
            var accountIds = dbLayer.AccountsRepository.GetAllAccountIds();

            var missingResourcesforAccountsList = GetMissingResourceDependenciesForAccounts(accountIds);

            return missingResourcesforAccountsList;
        }

        /// <summary>
        /// Получить типы пропущенных услуг ресурсов (по зависимостям) для аккаунтов
        /// </summary>
        /// <param name="accountIds">Ids аккаунтов</param>
        /// <returns>Провущенные услуги</returns>
        private List<AccountBillingServiceTypesModelDto> GetMissingResourceDependenciesForAccounts(List<Guid> accountIds)
        {
            return accountIds.Select(FindMissingResourceDependenciesForAccount)
                .Where(accountMissingResources => accountMissingResources != null).ToList();
        }

        /// <summary>
        /// Найти все типы пропущенных услуг ресурсов (по зависимостям) для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Пропущенные услуги аккаунта</returns>
        private AccountBillingServiceTypesModelDto FindMissingResourceDependenciesForAccount(Guid accountId)
        {
            var missingAccountResources = GetMissingResourceDependenciesForAccount(accountId);
            return !missingAccountResources.Any()
                ? null
                : new AccountBillingServiceTypesModelDto
                {
                    AccountId = accountId,
                    BillingServiceTypes = missingAccountResources
                };
        }

        /// <summary>
        /// Получить пропущенные услуги сервисов для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Пропущенные услуги у аккаунта</returns>
        private List<AccountBillingServiceTypeModelDto> GetMissingResourceDependenciesForAccount(Guid accountId)
        {
            var missingBillingServiceTypeModelList = new List<AccountBillingServiceTypeModelDto>();

            var accountBillingServiceTypes = (from resource in dbLayer.ResourceRepository.WhereLazy(r => r.AccountId == accountId)
                                              join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource.BillingServiceTypeId
                                                  equals serviceType.Id
                                              select serviceType).Distinct().ToList();

            var uniqRequiredDependencies = GetAccountChildDependencyRelations(accountBillingServiceTypes, accountId);

            var missingBillingServiceTypes = uniqRequiredDependencies
                .Where(billingServiceType => !accountBillingServiceTypes.Contains(billingServiceType)).ToList();

            missingBillingServiceTypes.ForEach(serviceType =>
            {
                missingBillingServiceTypeModelList.Add(new AccountBillingServiceTypeModelDto
                {
                    BillingServiceTypeId = serviceType.Id,
                    BillingServiceTypeName = serviceType.Name
                });
            });

            return missingBillingServiceTypeModelList;
        }

        /// <summary>
        /// Получить необходимые зависимые услуги для аккаунта
        /// </summary>
        /// <param name="accountBillingServiceTypes">Список услуг аккаунта</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Необходимые услуги</returns>
        private IEnumerable<BillingServiceType> GetAccountChildDependencyRelations(IEnumerable<BillingServiceType> accountBillingServiceTypes, Guid accountId)
        {
            var requiredDependencies = new List<BillingServiceType>();

            foreach (var serviceType in accountBillingServiceTypes)
                requiredDependencies.AddRange(billingServiceTypeRelationHelper.GetChildDependencyRelations(serviceType, accountId));

            var uniqRequiredDependencies = new HashSet<BillingServiceType>(requiredDependencies);

            return uniqRequiredDependencies;
        }

        /// <summary>
        /// Сгенерировать сообщение
        /// </summary>
        /// <param name="accountBillingServiceTypesModels">Отсутсвующие типы сервисов</param>
        /// <returns>Сообщение</returns>
        private string GenearateMessage(List<AccountBillingServiceTypesModelDto> accountBillingServiceTypesModels)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(
                $"Количество аккаунтов, у которых есть пропущенные ресурсы - {accountBillingServiceTypesModels.Count}");

            foreach (var accountBillingServiceTypesModel in accountBillingServiceTypesModels)
            {
                stringBuilder.AppendLine("</br>");
                stringBuilder.AppendLine($"<p>У аккаунта AccountId = \"{accountBillingServiceTypesModel.AccountId}\" было найдено {accountBillingServiceTypesModel.BillingServiceTypes.Count} отсутствующий(их) ресурс(а)</p></br>");
                stringBuilder.AppendLine("<p>Список типов биллинг сервиса, которые отсутствуют:</p></br>");
                accountBillingServiceTypesModel.BillingServiceTypes.ForEach(w =>
                {
                    stringBuilder.AppendLine($"<p> Id: {w.BillingServiceTypeId}</p></br>");
                    stringBuilder.AppendLine($"<p> Наименование: {w.BillingServiceTypeName}</p></br>");
                });
                stringBuilder.AppendLine("</br>");
            }

            var content = new EmailContentBuilder(accountConfProvider, stringBuilder.ToString()).Build();
            return content;
        }

        /// <summary>
        /// Отправить письмо
        /// </summary>
        /// <param name="message">Тело письма</param>
        private void SendMail(string message)
        {
            new Support42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(ConfigurationHelper.GetConfigurationValue("cloud-services"))
                .Body(message)
                .CoreFailSubject("Оповещение о результате проверки ресурсов аккаунтов.")
                .SendViaNewThread();
        }

    }
}
