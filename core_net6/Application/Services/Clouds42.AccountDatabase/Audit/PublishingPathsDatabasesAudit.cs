﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Configurations;
using Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.LetterNotification.Helpers;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;


namespace Clouds42.AccountDatabase.Audit
{
    /// <summary>
    /// Аудит путей публикации информационных баз
    /// </summary>
    public class PublishingPathsDatabasesAudit(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfProvider,
        IPublishingPathsDatabaseAuditProvider publishingPathsDatabaseAuditProvider,
        RepublishWithChangingConfigJobWrapper republishWithChangingConfigJobWrapper,
        RepublishDatabaseJobWrapper republishDatabaseJobWrapper)
        : IPublishingPathsDatabasesAudit
    {
        private List<AuditResultDto> AuditResults { get; } = [];

        /// <summary>
        /// Выполнить аудит путей
        /// публикации информационных баз
        /// </summary>
        public void ProcessAudit()
        {
            var publishedDatabases = dbLayer.DatabasesRepository
                .AsQueryableNoTracking()
                .Include(i => i.Account)
                .Where(d => d.PublishState == PublishState.Published.ToString() &&
                            d.State == DatabaseState.Ready.ToString() &&
                            d.AccountDatabaseOnDelimiter == null).ToList();

            foreach (var database in publishedDatabases)
            {
                //todo костыль для випа, потом удалить
                if(database.V82Name == "base_25931_37")
                    break;

                var auditResult = AccountDatabaseAudit(database);
                if (auditResult != null)
                {
                    if (auditResult is { Success: false, ErrorType: not null } && database.UsedWebServices != true)
                    {
                        var jobParams = new PublishOrCancelPublishDatabaseJobParamsDto { AccountDatabaseId = database.Id };
                        try
                        {
                            switch (auditResult.ErrorType)
                            {
                                case AuditErrorType.WebConfigPaths:
                                    republishWithChangingConfigJobWrapper.Start(jobParams);
                                    break;
                                case AuditErrorType.VrdConfigPaths:
                                    republishDatabaseJobWrapper.Start(jobParams);
                                    break;
                            }
                        }
                        catch (Exception) 
                        {
                            AuditResults.Add(new AuditResultDto
                            {
                                Success = false,
                                Message = $"Ошибка для перезапуска публикации для базы {database.Id}"
                            });
                        }
                        
                    }

                    AuditResults.Add(auditResult);
                }
            }

            ProcessAuditResults();
        }

        /// <summary>
        /// Выполнить аудит информационной базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        private AuditResultDto AccountDatabaseAudit(Domain.DataModels.AccountDatabase accountDatabase)
        {
             return publishingPathsDatabaseAuditProvider.AccountDatabaseAudit(accountDatabase);
        }

        /// <summary>
        /// Обработать результат аудирования
        /// </summary>
        private void ProcessAuditResults()
        {
            var notSuccessfulAuditResults = AuditResults.Where(w => !w.Success).ToList();
            if (!notSuccessfulAuditResults.Any())
                return;

            var builder = new StringBuilder();

            notSuccessfulAuditResults.ForEach(w =>
            {
                builder.AppendLine($"<p>{w.Message}</p></br>");
            });

            var content = new EmailContentBuilder(accountConfProvider, builder.ToString()).Build();
            SendMail(content);
        }

        /// <summary>
        /// Отправить письмо
        /// </summary>
        /// <param name="content">Тело письма</param>
        private void SendMail(string content)
        {
            new Support42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(ConfigurationHelper.GetConfigurationValue("cloud-services"))
                .Copy(ConfigurationHelper.GetConfigurationValue("HotlineEmail"))
                .Body(content)
                .CoreFailSubject("Оповещение о результате проверки путей публикации.")
                .SendViaNewThread();
        }
    }
}
