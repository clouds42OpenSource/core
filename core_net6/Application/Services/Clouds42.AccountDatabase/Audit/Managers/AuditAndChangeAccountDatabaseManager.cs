﻿using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Audit.Managers
{
    /// <summary>
    /// Менеджер аудита и изменения статуса инф. баз
    /// </summary>
    public class AuditAndChangeAccountDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAuditAccountDatabasesStatesProvider auditAccountDatabasesStatesProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Провести аудит и изменения статусов инф. баз
        /// </summary>
        /// <returns>Результат аудита</returns>
        public ManagerResult ProcessAuditAndChangeOfAccountDatabasesStates()
        {
            try
            {
                auditAccountDatabasesStatesProvider.ProcessAudit();
                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
