﻿using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Audit.Managers
{
    /// <summary>
    /// Менеджер аудита целостности зависимостей услуг ресурсов всех аккаунтов
    /// </summary>
    public class AccountResourceIntegralityAuditManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountResourceIntegralityAuditProvider provider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Выполнить целостности зависимостей услуг ресурсов всех аккаунтов
        /// </summary>
        public ManagerResult AuditAccountResourceIntegrality()
        {
            try
            {
                logger.Info("Начать проверку целостности услуг ресурсов всех аккаунтов");
                provider.AuditAccountResourceIntegrality();
                logger.Info("Проверка целостности услуг ресурсов всех аккаунтов завершилась корректно");
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка проверки целостности услуг ресурсов всех аккаунтов]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
