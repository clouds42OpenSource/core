﻿using Clouds42.AccountDatabase.Contracts.ITS.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.DataContracts.Configurations1c.ItsAuthorization;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ITS.Providers
{
    /// <summary>
    /// Провайдер создания модели данных авторизации в ИТС
    /// </summary>
    internal class CreateItsAuthorizationDataProvider(IUnitOfWork dbLayer, CommonCryptoProvider commonCryptoProvider)
        : ItsAuthorizationDataBaseProvider(dbLayer),
            ICreateItsAuthorizationDataProvider
    {
        /// <summary>
        /// Создать новые данные авторизации в ИТС
        /// </summary>
        /// <param name="newItsAuthorization">Новый модель данных авторизации в ИТС</param>
        public void CreateItsAuthorization(ItsAuthorizationDataDto newItsAuthorization)
        {
            ItsAuthorizationDataRepository.Insert(new ItsAuthorizationData
            {
                Id = Guid.NewGuid(),
                Login = newItsAuthorization.Login,
                PasswordHash = commonCryptoProvider.Encrypt(newItsAuthorization.PasswordHash,
                    CryptoDestinationTypeEnum.CommonAes)
            });

            DbLayer.Save();
        }
    }
}
