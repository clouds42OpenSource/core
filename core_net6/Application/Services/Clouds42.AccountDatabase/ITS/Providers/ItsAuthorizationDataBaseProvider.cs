﻿using Clouds42.Domain.DataModels.Security;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ITS.Providers
{
    /// <summary>
    /// Базовый провайдер для справочника "Доступ к ИТС"
    /// </summary>
    public abstract class ItsAuthorizationDataBaseProvider
    {
        protected readonly IUnitOfWork DbLayer;
        protected readonly IGenericRepository<ItsAuthorizationData> ItsAuthorizationDataRepository;

        protected ItsAuthorizationDataBaseProvider(IUnitOfWork dbLayer)
        {
            DbLayer = dbLayer;
            ItsAuthorizationDataRepository = DbLayer.GetGenericRepository<ItsAuthorizationData>();
        }
    }
}