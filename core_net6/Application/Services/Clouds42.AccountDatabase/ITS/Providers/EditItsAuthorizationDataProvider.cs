﻿using Clouds42.AccountDatabase.Contracts.ITS.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.DataContracts.Configurations1c.ItsAuthorization;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ITS.Providers
{
    /// <summary>
    /// Провайдер редактирования данных авторизации в ИТС
    /// </summary>
    internal class EditItsAuthorizationDataProvider(
        IUnitOfWork dbLayer,
        IItsAuthorizationDataProvider availableItsDataProvider,
        CommonCryptoProvider commonCryptoProvider)
        : ItsAuthorizationDataBaseProvider(dbLayer),
            IEditItsAuthorizationDataProvider
    {
        /// <summary>
        /// Редактировать данные авторизации в ИТС
        /// </summary>
        /// <param name="itsAuthorization">Модель данных авторизации в ИТС</param>
        public void EditItsAuthorization(ItsAuthorizationDataDto itsAuthorization)
        {
            var currentItsAuthorizationData =
                availableItsDataProvider.GetItsAuthorizationDataByIdOrThrowException(itsAuthorization.Id);

            currentItsAuthorizationData.Login = itsAuthorization.Login;

            if (!string.Equals(itsAuthorization.PasswordHash, currentItsAuthorizationData.PasswordHash,
                StringComparison.OrdinalIgnoreCase))
                currentItsAuthorizationData.PasswordHash =
                    commonCryptoProvider.Encrypt(itsAuthorization.PasswordHash, CryptoDestinationTypeEnum.CommonAes);

            ItsAuthorizationDataRepository.Update(currentItsAuthorizationData);
            DbLayer.Save();
        }
    }
}