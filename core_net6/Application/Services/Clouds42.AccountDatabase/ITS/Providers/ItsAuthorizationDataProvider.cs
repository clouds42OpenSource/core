﻿using Clouds42.DataContracts.Configurations1c.ItsAuthorization;
using Clouds42.DataContracts.BaseModel;
using PagedList.Core;
using Clouds42.AccountDatabase.Contracts.ITS.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ITS.Providers
{
    /// <summary>
    /// Провайдер получения данных авторизации ИТС
    /// </summary>
    internal class ItsAuthorizationDataProvider(IUnitOfWork dbLayer, CommonCryptoProvider commonCryptoProvider)
        : ItsAuthorizationDataBaseProvider(dbLayer), IItsAuthorizationDataProvider
    {
        /// <summary>
        /// Получить список данных авторизации в ИТС с пагинацией
        /// </summary>
        /// <param name="filterData">Модель фильтра для получения списка данных авторизации ИТС</param>
        /// <returns>Список данных авторизации в ИТС с пагинацией</returns>
        public ItsAuthorizationDataPaginationDto GetItsDataAuthorizations(ItsAuthorizationFilterDto filterData)
        {
            var searchString = filterData.Filter?.SearchString;
            var searchStringIsNullOrEmpty = searchString.IsNullOrEmpty();
            var itsDataAuthorizations =
            (
                from itsAuthorizationData in ItsAuthorizationDataRepository.WhereLazy()
                where searchStringIsNullOrEmpty || itsAuthorizationData.Login.Contains(searchString)
                select new ItsAuthorizationDataDto
                {
                    Id = itsAuthorizationData.Id,
                    Login = itsAuthorizationData.Login,
                    PasswordHash = itsAuthorizationData.PasswordHash
                }
            ).OrderByDescending(i => i.Login);

            return GetPagedItsDataAuthorizations(filterData, itsDataAuthorizations);
        }

        /// <summary>
        /// Получить модель данных авторизации в ИТС по ID
        /// </summary>
        /// <param name="id">ID модель данных авторизации в ИТС</param>
        /// <returns>Модель модель данных авторизации в ИТС</returns>
        public ItsAuthorizationDataDto GetItsAuthorizationDataDtoById(Guid id)
        {
            var itsAuthorizationData = GetItsAuthorizationDataByIdOrThrowException(id);

            return new ItsAuthorizationDataDto
            {
                Id = itsAuthorizationData.Id,
                Login = itsAuthorizationData.Login,
                PasswordHash = !string.IsNullOrEmpty(itsAuthorizationData.PasswordHash)
                    ? commonCryptoProvider.Decrypt(itsAuthorizationData.PasswordHash, CryptoDestinationTypeEnum.CommonAes)
                    : null
            };
        }

        /// <summary>
        /// Получить доменный модель данных авторизации в ИТС по ID
        /// </summary>
        /// <param name="id">ID данных авторизации в ИТС</param>
        /// <returns>Доменный модель данных авторизации в ИТС</returns>
        public ItsAuthorizationData GetItsAuthorizationDataByIdOrThrowException(Guid id) =>
            ItsAuthorizationDataRepository.FirstOrDefault(i => i.Id == id)
            ?? throw new NotFoundException($"Данные авторизации ИТС не найдены по ID: {id}");

        /// <summary>
        /// Получить коллекцию авторизации данных в ИТС
        /// </summary>
        /// <param name="searchValue">Значение поиска</param>
        /// <returns>Коллекцию авторизации данных в ИТС</returns>
        public ICollection<ItsAuthorizationDataDto> GetItsAuthorizationDataBySearchValue(string searchValue)
        {
            return ItsAuthorizationDataRepository
                .WhereLazy(its => string.IsNullOrEmpty(searchValue) || its.Login.Contains(searchValue))
                .Select(its => new ItsAuthorizationDataDto
                {
                    Id = its.Id,
                    Login = its.Login,
                    PasswordHash = its.PasswordHash
                }).ToList();
        }

        /// <summary>
        /// Приватный метод для пагинации
        /// </summary>
        /// <param name="filterData">Модель фильтра для получения списка данных авторизации ИТС</param>
        /// <param name="itsDataAuthorizations">коллекция модели авторизации данных</param>
        /// <returns>Коллекция модели авторизации данных с пагинацией</returns>
        private ItsAuthorizationDataPaginationDto GetPagedItsDataAuthorizations(ItsAuthorizationFilterDto filterData,
            IQueryable<ItsAuthorizationDataDto> itsDataAuthorizations)
        {
            const int itemsPerPage = GlobalSettingsConstants.ReferenceDataPages.DefaultDataGridRecordsCount;
            var tableCount = itsDataAuthorizations.Count();
            var pageNumber = PaginationHelper.GetFilterPageNumberBasedOnItemsCount(tableCount, itemsPerPage, filterData?.PageNumber ?? 1);

            if (filterData?.SortingData != null)
                itsDataAuthorizations = itsDataAuthorizations.MakeSorting(filterData.SortingData);

            var tablePagedList = itsDataAuthorizations
                .ToPagedList(pageNumber, itemsPerPage)
                .ToArray();

            return new ItsAuthorizationDataPaginationDto
            {
                Records = tablePagedList,
                Pagination = new PaginationBaseDto(pageNumber, tableCount, itemsPerPage)
            };
        }
    }
}
