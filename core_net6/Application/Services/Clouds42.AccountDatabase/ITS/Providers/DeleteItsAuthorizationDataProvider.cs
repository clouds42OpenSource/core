﻿using Clouds42.AccountDatabase.Contracts.ITS.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ITS.Providers
{
    /// <summary>
    /// Провайдер для удаления модели данных авторизации в ИТС
    /// </summary>
    internal class DeleteItsAuthorizationDataProvider(
        IUnitOfWork dbLayer,
        IItsAuthorizationDataProvider availableItsDataProvider)
        : ItsAuthorizationDataBaseProvider(dbLayer),
            IDeleteItsAuthorizationDataProvider
    {
        /// <summary>
        /// Удалить данные авторизации в ИТС
        /// </summary>
        /// <param name="id">ID данных авторизации в ИТС</param>
        public void DeleteItsAuthorization(Guid id)
        {
            var currentItsAuthorization =
                availableItsDataProvider.GetItsAuthorizationDataByIdOrThrowException(id);

            ItsAuthorizationDataRepository.Delete(currentItsAuthorization);
            DbLayer.Save();
        }
    }
}