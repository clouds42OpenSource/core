﻿using Clouds42.AccountDatabase.Contracts.ITS.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Configurations1c.ItsAuthorization;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ITS.Managers
{
    /// <summary>
    /// Менеджер для справочника "Доступ к ИТС"
    /// </summary>
    public class ItsAuthorizationDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IItsAuthorizationDataProvider itsAuthorizationDataProvider,
        ICreateItsAuthorizationDataProvider createItsAuthorizationDataProvider,
        IEditItsAuthorizationDataProvider editItsAuthorizationDataProvider,
        IDeleteItsAuthorizationDataProvider deleteItsAuthorizationDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить список данных авторизации в ИТС с пагинацией
        /// </summary>
        /// <param name="filterData">Модель фильтра для получения списка данных авторизации ИТС</param>
        /// <returns>Список данных авторизации в ИТС с пагинацией</returns>
        public ManagerResult<ItsAuthorizationDataPaginationDto> GetItsDataAuthorizations(
            ItsAuthorizationFilterDto filterData)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.GetItsDataAuthorizations, () => AccessProvider.ContextAccountId);
                var itsDataAuthorizations = itsAuthorizationDataProvider.GetItsDataAuthorizations(filterData);
                return Ok(itsDataAuthorizations);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ItsAuthorizationDataPaginationDto>(ex.Message);
            }
        }

        /// <summary>
        /// Создать новые данные авторизации в ИТС
        /// </summary>
        /// <param name="newItsAuthorization">Новый модель данных авторизации в ИТС</param>
        public ManagerResult CreateItsAuthorizationData(ItsAuthorizationDataDto newItsAuthorization)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CreateItsAuthorizationData, () => AccessProvider.ContextAccountId);
                createItsAuthorizationDataProvider.CreateItsAuthorization(newItsAuthorization);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при создании новой модели данных авторизации в ИТС.]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать данные авторизации в ИТС
        /// </summary>
        /// <param name="itsAuthorization">Модель данных авторизации в ИТС</param>
        public ManagerResult EditItsAuthorizationData(ItsAuthorizationDataDto itsAuthorization)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.EditItsAuthorizationData, () => AccessProvider.ContextAccountId);
                editItsAuthorizationDataProvider.EditItsAuthorization(itsAuthorization);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при редактировании модели данных авторизации в ИТС.]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель данных авторизации в ИТС по ID
        /// </summary>
        /// <param name="id">ID модель данных авторизации в ИТС</param>
        /// <returns>Модель модель данных авторизации в ИТС</returns>
        public ManagerResult<ItsAuthorizationDataDto> GetItsAuthorizationDataDtoById(Guid id)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.EditItsAuthorizationData, () => AccessProvider.ContextAccountId);
                var result = itsAuthorizationDataProvider.GetItsAuthorizationDataDtoById(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при получении данных авторизации в ИТС по ID.]");
                return PreconditionFailed<ItsAuthorizationDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Удалить данные авторизации в ИТС
        /// </summary>
        /// <param name="id">ID данных авторизации в ИТС</param>
        public ManagerResult DeleteItsAuthorizationData(Guid id)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeleteItsAuthorizationData, () => AccessProvider.ContextAccountId);
                deleteItsAuthorizationDataProvider.DeleteItsAuthorization(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при удалении модели данных авторизации в ИТС по ID.]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить коллекцию авторизации данных в ИТС
        /// </summary>
        /// <param name="searchValue">Значение поиска</param>
        /// <returns>Коллекцию авторизации данных в ИТС</returns>
        public ManagerResult<ICollection<ItsAuthorizationDataDto>> GetItsAuthorizationDataBySearchValue(string searchValue)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.GetItsAuthorizationDataForSelectList, () => AccessProvider.ContextAccountId);
                var result = itsAuthorizationDataProvider.GetItsAuthorizationDataBySearchValue(searchValue);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ICollection<ItsAuthorizationDataDto>>(ex.Message);
            }
        }
    }
}
