﻿using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using LinqExtensionsNetFramework;

namespace Clouds42.AccountDatabase.Specification
{
    public static class DatabaseSpecification
    {
        public static Spec<Domain.DataModels.AccountDatabase> BySearchLine(string? searchLine)
        {
            var zone = searchLine.ToInt();
            return new (rec => string.IsNullOrEmpty(searchLine) || rec.Caption != null && rec.Caption.Contains(searchLine) ||
                           rec.V82Name != null && rec.V82Name.Contains(searchLine) ||
                           rec.Account.AccountCaption != null &&
                           rec.Account.AccountCaption.Contains(searchLine) ||
                           rec.Account.IndexNumber.ToString().Contains(searchLine) ||
                           rec.DbNumber.ToString().Contains(searchLine) ||
                           (rec.AccountDatabaseOnDelimiter != null &&
                            rec.AccountDatabaseOnDelimiter.Zone == zone));
        }
        public static Spec<Domain.DataModels.AccountDatabase> ByAccountNotNull()
        {
            return new (item => item.Account != null);
        }

        public static Spec<Domain.DataModels.AccountDatabase> ByStateNotEquals(DatabaseState state)
        {
            return new (item => item.State != state.ToString());
        }
    }
}
