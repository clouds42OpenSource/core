﻿using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm
{
    /// <summary>
    /// Провайдер данных печатных форм HTML
    /// </summary>
    public interface IPrintedHtmlFormDataProvider
    {
        /// <summary>
        /// Получить модель печатной формы для создания
        /// </summary>
        /// <returns>Модель печатной формы для создания</returns>
        PrintedHtmlFormDto GetPrintedHtmlFormModelForCreating();

    }
}
