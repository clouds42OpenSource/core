﻿using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm
{
    /// <summary>
    /// Провайдер сохранения/изменения печатной формой HTML
    /// </summary>
    public interface ICreateAndEditPrintedHtmlFormProvider
    {
        /// <summary>
        /// Создать/Изменить печатную форму
        /// </summary>
        /// <param name="printedHtmlFormDc">Модель печатной формы</param>
        /// <returns>Id печатной формы</returns>
        Guid CreateOrChangePrintedHtmlForm(PrintedHtmlFormDto printedHtmlFormDc);
    }
}
