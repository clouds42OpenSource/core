﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm
{
    /// <summary>
    /// Провайдер для работы с печатной формой HTML
    /// </summary>
    public interface IPrintedHtmlFormProvider
    {
        /// <summary>
        /// Получить документ с тестовыми данными
        /// для печатной формы HTML
        /// </summary>
        /// <param name="printedHtmlFormDc">Модель печатной формы</param>
        /// <returns>Документ с тестовыми данными</returns>
        DocumentBuilderResult GetDocumentWithTestDataForPrintedHtmlForm(PrintedHtmlFormDto printedHtmlFormDc);
    }
}
