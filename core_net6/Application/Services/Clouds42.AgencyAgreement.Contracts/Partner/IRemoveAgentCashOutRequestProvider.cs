﻿namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер для удаления заявок на вывод средств агента
    /// </summary>
    public interface IRemoveAgentCashOutRequestProvider
    {
        /// <summary>
        /// Удалить агентскую заявку на вывод средств
        /// </summary>
        /// <param name="cashOutRequestId">ID агентской заявки</param>
        void RemoveAgentCashOutRequest(Guid cashOutRequestId);
    }
}
