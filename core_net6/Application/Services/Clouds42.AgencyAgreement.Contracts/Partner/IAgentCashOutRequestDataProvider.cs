﻿using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер для работы с данными агентских заявок
    /// </summary>
    public interface IAgentCashOutRequestDataProvider
    {
        /// <summary>
        /// Получить заявки на расходование средств в статусе "Новая"
        /// </summary>
        /// <returns>Заявки на расходование средств в статусе "Новая"</returns>
        AgentCashOutRequestsInStatusNewDto GetAgentCashOutRequestsInStatusNew();
    }
}
