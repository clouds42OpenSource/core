﻿using Clouds42.DataContracts.Service.Partner.PartnerClients;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер для работы с клиентами партнера
    /// </summary>
    public interface IPartnerClientsProvider
    {
        /// <summary>
        /// Привязать аккаунт к партнеру
        /// </summary>
        /// <param name="accountsBinding">Модель связи аккаунтов</param>
        void BindAccountToPartner(AccountsBindingDto accountsBinding);

        /// <summary>
        ///     Поиск аккаунтов по строке поиска
        /// </summary>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>Список аккаунтов</returns>
        List<AccountInfoDto> SearchAccounts(string searchString);
    }
}
