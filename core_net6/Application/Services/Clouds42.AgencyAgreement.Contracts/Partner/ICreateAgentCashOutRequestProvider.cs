﻿using System.Text;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер для создания заявок на вывод средств агента
    /// </summary>
    public interface ICreateAgentCashOutRequestProvider
    {
        /// <summary>
        /// Создать заявку на вывод средств агента
        /// </summary>
        /// <param name="createAgentCashOutRequest">Модель создания заявки</param>
        /// <returns>Id созданной заявки</returns>
        Guid CreateAgentCashOutRequest(CreateAgentCashOutRequestDto createAgentCashOutRequest);

        /// <summary>
        /// Получить список активных реквизитов агента
        /// </summary>
        /// <param name="agentId">ID агента</param>
        /// <returns>Список активных реквизитов агента</returns>
        List<AgentRequisitesInfoDto> GetActiveAgentRequisites(Guid agentId);

        /// <summary>
        /// Распечатать отчет агента
        /// </summary>
        /// <param name="editAgentCashOutRequestDto">Модель редактирования заявки на вывод средств</param>
        /// <returns>Pdf файл на печать</returns>
        DocumentBuilderResult PrintAgentReport(EditAgentCashOutRequestDto editAgentCashOutRequestDto);

        /// <summary>
        /// Распечатать отчет агента
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки агента</param>
        /// <returns>Pdf файл на печать</returns>
        DocumentBuilderResult PrintAgentReport(Guid agentCashOutRequestId);

        /// <summary>
        /// Получить доступную сумму для вывода
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Доступная сумма для вывода</returns>
        decimal GetAvailableSumForCashOutRequest(Guid accountId);

        /// <summary>
        /// Проверить возможность создания заявки на вывод средств
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="messageBuilder">Сборщик сообщений о результатах проверки возможности создания заявки</param>
        /// <returns>Результат проверки</returns>
        bool CheckAbilityToCreateCashOutRequest(Guid accountId, StringBuilder messageBuilder = null);

        /// <summary>
        /// Попробовать получить номер заявки на расходование средств
        /// которая находится в обработке
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="cashOutRequestNumber">Номер заявки</param>
        /// <returns>Результат поиска</returns>
        bool TryGetCashOutRequestNumberWhichIsProcessed(Account account, out string cashOutRequestNumber);
    }
}
