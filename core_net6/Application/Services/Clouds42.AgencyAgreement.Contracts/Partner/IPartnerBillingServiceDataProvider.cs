﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.ResourceConfiguration;
using Clouds42.DataContracts.Service.Partner;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{

    /// <summary>
    /// Провайдер данных партнерских сервисов.
    /// </summary>
    public interface IPartnerBillingServiceDataProvider
    {
        /// <summary>
        /// Получить детальную информацию по партнерскому сервису.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <returns>Информация по сервису.</returns>        
        ServiceInfoDto GetServiceInfo(Guid serviceKey);

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <param name="accountUserId">Номер пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        ServiceTypeStateForUserDto GetServiceTypesStateForUserByKey(Guid serviceKey, Guid accountUserId);

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        ServiceTypeStateForUserDto GetServiceTypeStateForUserByKey(Guid serviceKey, string accountUserLogin);

        /// <summary>
        /// Проверить активность конкретной услуги сервиса у клиента.
        /// </summary>
        /// <param name="serviceTypeKey">Ключ услуги сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Признак активности услуги сервиса.</returns>
        ServiceTypeStatusForUserDto CheckServiceTypeStatusForUser(Guid serviceTypeKey, string accountUserLogin);

        /// <summary>
        /// Получить список услуг партнерского сервиса.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <returns>Список услуг сервиса.</returns>   
        GuidListItemDto GetServiceTypesList(Guid serviceKey);

        /// <summary>
        /// Получить список сервисов у аккаунта.
        /// Сервисы принадлежащие партнеру.
        /// </summary>
        /// <param name="accountId">Номер аккаунта партнера.</param>
        /// <returns>Список номеров сервисов.</returns>    
        GuidListItemDto GetAccountServicesList(Guid accountId);

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceId">Id сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        ServiceTypeStateForUserDto GetServiceTypeStateForUserById(Guid serviceId, string accountUserLogin);

        /// <summary>
        /// Получить статус сервиса для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Модель статуса сервиса для аккаунта</returns>
        ServiceStateForAccountDto GetServiceStatusForAccount(Guid accountId, Guid serviceId);

        /// <summary>
        /// Получить информацию о сервисе на аккаунт по пользователю
        /// </summary>
        /// <param name="serviceId">Для какого сервиса</param>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <returns>Информацию о сервисе на аккаунт</returns>
        AccountServiceDto GetServiceData(Guid serviceId, Guid accountId);
    }
}