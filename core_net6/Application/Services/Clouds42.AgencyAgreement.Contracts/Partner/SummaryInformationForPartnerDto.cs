﻿using System.ComponentModel.DataAnnotations;
using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Модель сводной информации для партнера
    /// </summary>
    public class SummaryInformationForPartnerDto
    {
        /// <summary>
        /// Название компании
        /// </summary>
        [Display(Name = "Название компании")]
        public string CompanyName { get; set; }

        /// <summary>
        /// Реферальная ссылка
        /// </summary>
        [Display(Name = "Реф. ссылка")]
        public string ReferrerLink { get; set; }

        /// <summary>
        /// Форум
        /// </summary>
        [Display(Name = "Форум")]
        public string Forum { get; set; }

        /// <summary>
        /// Всего заработано
        /// </summary>
        [Display(Name = "Всего заработано")]
        public decimal TotalEarned { get; set; }

        /// <summary>
        /// Доступно к выплате
        /// </summary>
        [Display(Name = "Доступно к выплате")]
        public decimal AvailableToPay { get; set; }

        /// <summary>
        /// Символ валюты
        /// </summary>
        [Display(Name = "Символ валюты")]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Необходимость принять агентское соглашение
        /// </summary>
        public bool NeedApplyAgencyAgreement { get; set; }

        /// <summary>
        /// Актуальное агентское соглашение
        /// </summary>
        public AgencyAgreementDetailsDto ActualAgencyAgreement { get; set; }

        /// <summary>
        /// Дата окончания агентского соглашения
        /// </summary>
        public DateTime? AgencyAgreementEndDate => NextEffectiveAgencyAgreement?.EffectiveDate.AddDays(-1);

        /// <summary>
        /// Следующее агентское соглашение которое вступит в силу
        /// </summary>
        public AgencyAgreementDetailsDto NextEffectiveAgencyAgreement { get; set; }
    }
}
