﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер файлов агентского документа
    /// </summary>
    public interface IAgentDocumentFileProvider
    {
        /// <summary>
        /// Получить файл документа агента
        /// </summary>
        /// <param name="agentDocumentFileId">ID файла документа агента</param>
        /// <returns>Файл облака</returns>
        CloudFile GetAgentDocumentFile(Guid agentDocumentFileId);

        /// <summary>
        /// Получить файл разработки сервиса
        /// </summary>
        /// <param name="billingService1CFileId">ID файла разработки сервиса</param>
        /// <returns>Файл разработки сервиса</returns>
        CloudFile GetBillingService1CFile(Guid billingService1CFileId);
    }
}
