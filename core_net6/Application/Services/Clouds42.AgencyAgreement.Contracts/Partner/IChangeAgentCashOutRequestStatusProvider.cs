﻿using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер смены статуса заявки на вывод средств
    /// </summary>
    public interface IChangeAgentCashOutRequestStatusProvider
    {
        /// <summary>
        /// Попытаться сменить статус заявки на вывод средств
        /// </summary>
        /// <param name="changeAgentCashOutRequestStatus">Модель смены статуса заявки
        /// на вывод средств</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Успешность смены статуса</returns>
        bool TryChangeAgentCashOutRequestStatus(
            ChangeAgentCashOutRequestStatusDto changeAgentCashOutRequestStatus, out string errorMessage);
    }
}
