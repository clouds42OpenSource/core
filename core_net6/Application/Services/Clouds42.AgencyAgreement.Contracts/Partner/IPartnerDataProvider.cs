﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Интерфейс провайдера данных партнера
    /// </summary>
    public interface IPartnerDataProvider
    {
        /// <summary>
        /// Получить сводную информацию для партнера
        /// </summary>
        /// <param name="accountId">Id аккаунта партнера</param>
        /// <returns>Сводная информация для партнера</returns>
        SummaryInformationForPartnerDto GetSummaryInformationForPartner(Guid accountId);

        Task<ManagerResult<decimal>> GetMonthlyCharge(Guid accountId);
    }
}
