﻿using Clouds42.DataContracts.Service.Partner.PartnerTransactions;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер создания агентских платежей.
    /// </summary>
    public interface ICreateAgentPaymentProvider
    {

        /// <summary>
        /// Создать платеж в пользу агента.
        /// </summary>
        /// <param name="agencyPaymentCreation">Модель создания агентского платежа</param>
        void CreateAgentPayment(AgencyPaymentCreationDto agencyPaymentCreation);
    }
}
