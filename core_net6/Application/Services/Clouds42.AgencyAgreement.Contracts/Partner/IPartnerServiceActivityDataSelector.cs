﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    public interface IPartnerServiceActivityDataSelector
    {
        /// <summary>
        /// Выбрать данные
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        ServiceDataForAccountDto SelectData(Guid accountId, Guid serviceId);
    }
}
