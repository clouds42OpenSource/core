﻿using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер для редактирования заявок на вывод средств агента
    /// </summary>
    public interface IEditAgentCashOutRequestProvider
    {
        /// <summary>
        /// Получить модель заявки для редактирования по ID
        /// </summary>
        /// <param name="cashOutRequestId">ID заявки</param>
        /// <returns>Модель заявки для редактирования</returns>
        EditAgentCashOutRequestDto GetAgentCashOutRequestById(Guid cashOutRequestId);

        /// <summary>
        /// Изменить заявку на вывод средств
        /// </summary>
        /// <param name="editAgentCashOutRequestDto">Модель изменения заявки</param>
        void EditAgentCashOutRequest(EditAgentCashOutRequestDto editAgentCashOutRequestDto);
    }
}
