﻿using Clouds42.DataContracts.Service.Partner;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{

    /// <summary>
    /// Провайдер данных партнерских услуг сервиса.
    /// </summary>
    public interface IPartnerBillingServiceTypeDataProvider
    {
        /// <summary>
        /// Получить детальную информацию о услуге сервиса.
        /// </summary>
        /// <param name="serviceTypeKey">Ключ услуги сервиса.</param>
        /// <returns>Информация по услуге сервиса.</returns>        
        ServiceTypeInfoDto GetServiceTypeInfo(Guid serviceTypeKey);
    }
}