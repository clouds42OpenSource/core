﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels.billing.AgentPayments;

namespace Clouds42.AgencyAgreement.Contracts.Partner
{
    /// <summary>
    /// Провайдер для получения агентских заявок
    /// </summary>
    public interface IAgentCashOutRequestProvider
    {
        /// <summary>
        /// Получить список агентских заявок
        /// </summary>
        /// <param name="filter">Фильтр получения агентских заявок на вывод средств</param>
        /// <returns>Список агентских заявок</returns>
        PaginationDataResultDto<AgentCashOutRequestInfoDto> GetAgentCashOutRequests(AgentCashOutRequestsFilterDto filter);

        /// <summary>
        /// Получить агентскую заявку
        /// </summary>
        /// <param name="cashOutRequestId">Id агентской заявки</param>
        /// <returns>Агентскую заявку</returns>
        AgentCashOutRequest GetAgentCashOutRequest(Guid cashOutRequestId);
    }
}
