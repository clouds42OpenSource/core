﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.Contracts.AgencyAgreement
{
    /// <summary>
    /// Провайдер для создания агентских соглашений
    /// </summary>
    public interface ICreateAgencyAgreementProvider
    {
        /// <summary>
        /// Создать агентское соглашение
        /// </summary>
        /// <param name="args">Модель создания агентского соглашения</param>
        /// <returns>Результат создания, Id агентского соглашения</returns>
        Guid AddAgencyAgreementItem(AddAgencyAgreementItemDto args);
    }
}
