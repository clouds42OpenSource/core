﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.Contracts.AgencyAgreement
{
    /// <summary>
    /// Провайдер для работы с данными агентского соглашения
    /// </summary>
    public interface IAgencyAgreementDataProvider
    {
        /// <summary>
        /// Получить данные агентских соглашений
        /// </summary>
        /// <returns>Данные агентских соглашений</returns>
        AgencyAgreementsDataDto GetAgencyAgreementsData();

        /// <summary>
        /// Получить детальную информацию
        /// об агентском соглашении
        /// </summary>
        /// <param name="agencyAgreementId">Id агентского соглашения</param>
        /// <returns>Детальная информация об агентском соглашении</returns>
        AgencyAgreementDetailsDto GetAgencyAgreementDetails(Guid agencyAgreementId);

        /// <summary>
        /// Получить актуальное агентское соглашение
        /// </summary>
        /// <returns>Актуальное агентское соглашение</returns>
        Domain.DataModels.billing.AgencyAgreement GetActualAgencyAgreement();

        /// <summary>
        /// Получить актуальное агентское соглашение
        /// </summary>
        /// <returns>Актуальное агентское соглашение</returns>
        Task<Domain.DataModels.billing.AgencyAgreement> GetActualAgencyAgreementAsync();

        /// <summary>
        /// Получить следующее агентское соглашение
        /// которое вступит в силу(если такое соглашение существует)
        /// </summary>
        /// <returns>Следующее агентское соглашение</returns>
        AgencyAgreementDetailsDto TryGetNextEffectiveAgencyAgreement();

    }
}
