﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;

namespace Clouds42.AgencyAgreement.Contracts.AgencyAgreement
{
    /// <summary>
    /// Провайдер для печати с агентского соглашения
    /// </summary>
    public interface IPrintAgencyAgreementProvider
    {
        /// <summary>
        /// Печать агентского соглашения
        /// </summary>
        /// <param name="agencyAgreementId">Id агентского соглашения</param>
        /// <returns>Документ</returns>
        DocumentBuilderResult Print(Guid agencyAgreementId);
    }
}
