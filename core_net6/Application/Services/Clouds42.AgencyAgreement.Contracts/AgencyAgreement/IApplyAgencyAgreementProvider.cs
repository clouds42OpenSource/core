﻿namespace Clouds42.AgencyAgreement.Contracts.AgencyAgreement
{
    /// <summary>
    /// Провайдер принятия агентского соглашения
    /// </summary>
    public interface IApplyAgencyAgreementProvider
    {
        /// <summary>
        /// Принять агентское соглашение
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void Apply(Guid accountId);
    }
}
