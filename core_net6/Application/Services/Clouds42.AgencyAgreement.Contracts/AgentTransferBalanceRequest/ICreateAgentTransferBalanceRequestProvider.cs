﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.Contracts.AgentTransferBalanceRequest
{
    /// <summary>
    /// Провайдер для создания заявки на перевод баланса агента
    /// </summary>
    public interface ICreateAgentTransferBalanceRequestProvider
    {
        /// <summary>
        /// Создать заявку на перевод баланса агента
        /// </summary>
        /// <param name="createAgentTransferBalanceRequest">Модель создания заявки на перевод баланса</param>
        void Create(CreateAgentTransferBalanseRequestDto createAgentTransferBalanceRequest);

        /// <summary>
        /// Проверить возможность создания заявки
        /// на перевод баланса агента 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <param name="sum">Сумма перевода</param>
        /// <returns>Результат проверки</returns>
        bool CheckAbilityToCreateAgentTransferBalanceRequest(Guid accountId, out string message, decimal? sum = null);

        /// <summary>
        /// Получить доступную сумму для
        /// перевода баланса агента
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Доступная сумма перевода</returns>
        decimal GetAvailableSumForAgentTransferBalanceRequest(Guid accountId);
    }
}
