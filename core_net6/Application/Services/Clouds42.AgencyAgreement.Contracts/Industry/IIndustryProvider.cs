﻿using Clouds42.DataContracts.Service.Industry;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AgencyAgreement.Contracts.Industry
{
    /// <summary>
    /// Интерфейс для работы с отраслью
    /// </summary>
    public interface IIndustryProvider
    {
        /// <summary>
        ///     Добавить новую отрасль
        /// </summary>
        /// <param name="newIndustry">Обьект новой отрасли</param>
        /// <returns>Модель свойств после добавления новой отрасли</returns>
        AddIndustryResultDto AddNewIndustry(AddIndustryDataItemDto newIndustry);

        /// <summary>
        ///     Удалить существующую отрасль
        /// </summary>
        /// <param name="id">Id отрасли</param>
        void DeleteIndustry(Guid id);

        /// <summary>
        ///     Обновить существующую отрасль
        /// </summary>
        /// <param name="industryToUpdate">Обновленная отрасль</param>
        void EditExistingIndustry(IIndustry industryToUpdate);

        /// <summary>
        /// Получить коллекцию отраслей
        /// </summary>
        /// <param name="args">Параметры поиска получения коллекции отраслей</param>
        /// <returns>Список отраслей с пагинацией</returns>
        IndustryDataResultDto GetIndustryItems(GetIndustryParamsDto args);
    }
}
