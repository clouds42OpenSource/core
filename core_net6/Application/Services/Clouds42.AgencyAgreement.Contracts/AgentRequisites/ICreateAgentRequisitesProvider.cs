﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.Contracts.AgentRequisites
{
    /// <summary>
    /// Провайдер создания реквизитов агента
    /// </summary>
    public interface ICreateAgentRequisitesProvider
    {
        /// <summary>
        /// Создать реквизиты агента
        /// </summary>
        /// <param name="createAgentRequisites">Модель создания реквизитов агента</param>
        /// <returns>Id созданных реквизитов</returns>
        Guid CreateAgentRequisites(CreateAgentRequisitesDto createAgentRequisites);
    }
}
