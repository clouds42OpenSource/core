﻿namespace Clouds42.AgencyAgreement.Contracts.AgentRequisites
{
    /// <summary>
    /// Провайдер удаления реквизитов агента
    /// </summary>
    public interface IRemoveAgentRequisitesProvider
    {

        /// <summary>
        /// Удалить реквизиты агента
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов агента</param>
        void RemoveAgentRequisites(Guid agentRequisitesId);
    }
}
