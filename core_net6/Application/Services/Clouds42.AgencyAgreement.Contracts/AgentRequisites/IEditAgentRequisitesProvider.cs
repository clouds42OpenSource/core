﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.Contracts.AgentRequisites
{
    /// <summary>
    /// Провайдер редактирования реквизитов агента
    /// </summary>
    public interface IEditAgentRequisitesProvider
    {

        /// <summary>
        /// Отредактировать реквизиты агента
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        void EditAgentRequisites(EditAgentRequisitesDto editAgentRequisites);

        /// <summary>
        /// Сменить статус реквизитов агента
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        void ChangeAgentRequisitesStatus(EditAgentRequisitesDto editAgentRequisites);
    }
}
