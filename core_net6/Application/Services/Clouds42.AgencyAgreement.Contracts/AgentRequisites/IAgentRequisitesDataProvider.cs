﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.Contracts.AgentRequisites
{
    /// <summary>
    /// Провайдер данных реквизитов агента
    /// </summary>
    public interface IAgentRequisitesDataProvider
    {
        /// <summary>
        /// Получить реквизиты агента
        /// </summary>
        /// <param name="filter">Значение фильтра</param>
        /// <returns>Реквизиты агента</returns>
        PaginationDataResultDto<AgentRequisitesInfoDto> GetAgentRequisites(PartnersAgentRequisitesFilterDto filter);

        /// <summary>
        /// Получить реквизиты агента
        /// </summary>
        /// <param name="agentRequisitesId">Id агента</param>
        /// <returns>Реквизиты агента</returns>
        Domain.DataModels.billing.AgentRequisites.AgentRequisites GetAgentRequisitesOrThrowException(Guid agentRequisitesId);
    }
}
