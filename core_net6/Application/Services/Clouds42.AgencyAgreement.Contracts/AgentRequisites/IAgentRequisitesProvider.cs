﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.Contracts.AgentRequisites
{
    /// <summary>
    /// Провайдер для работы с реквизитами агента
    /// </summary>
    public interface IAgentRequisitesProvider
    {
        /// <summary>
        /// Получить модель для редактирования реквизитов агента по Id
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов</param>
        /// <returns>Модель для редактирования реквизитов агента</returns>
        EditAgentRequisitesDto GetEditAgentRequisitesDto(Guid agentRequisitesId);

        /// <summary>
        /// Получить реквизиты агента по Id
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов</param>
        /// <returns>Модель реквизитов агента</returns>
        Domain.DataModels.billing.AgentRequisites.AgentRequisites GetAgentRequisites(Guid agentRequisitesId);
    }
}
