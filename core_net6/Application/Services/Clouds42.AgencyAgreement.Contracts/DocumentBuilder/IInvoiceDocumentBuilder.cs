﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.AgencyAgreement.Contracts.DocumentBuilder
{
    /// <summary>
    /// Фабрика генерации счетов на оплату
    /// </summary>
    public interface IInvoiceDocumentBuilder
    {
        /// <summary>
        /// Сгенерировать PDF документ для счета на оплату
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        DocumentBuilderResult Build(InvoiceDc invoice);
    }
}