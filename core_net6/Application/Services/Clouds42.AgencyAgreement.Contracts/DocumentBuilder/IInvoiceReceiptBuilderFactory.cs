﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.AgencyAgreement.Contracts.DocumentBuilder
{
    public interface IInvoiceReceiptBuilderFactory
    {
        DocumentBuilderResult Build(InvoiceReceiptDto receipt);
    }
}