﻿using Clouds42.DataContracts.Billing.Interfaces;

namespace Clouds42.AgencyAgreement.Contracts.DocumentBuilder
{
    /// <summary>
    /// Результат билда документа
    /// </summary>
    public class DocumentBuilderResult(byte[] bytes, string fileName) : IDocumentBuilderResultDto
    {
        /// <summary>
        /// Массив байтов
        /// </summary>
        public byte[] Bytes { get; } = bytes;

        /// <summary>
        /// Название документа
        /// </summary>
        public string FileName { get; } = fileName;
    }
}