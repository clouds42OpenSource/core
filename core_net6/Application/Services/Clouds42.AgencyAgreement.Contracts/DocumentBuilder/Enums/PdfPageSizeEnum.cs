﻿namespace Clouds42.AgencyAgreement.Contracts.DocumentBuilder.Enums
{
    /// <summary>
    /// Размер Pdf страницы
    /// </summary>
    public enum PdfPageSizeEnum
    {
        /// <summary>
        /// Формат A4
        /// </summary>
        A4 = 0,

        /// <summary>
        /// Формат A5
        /// </summary>
        A5 = 1,

        /// <summary>
        /// Формат A6
        /// </summary>
        A6 = 2,

        /// <summary>
        /// Формат A3
        /// </summary>
        A3 = 3,

        /// <summary>
        /// Формат письмо
        /// </summary>
        Letter = 4
    }
}
