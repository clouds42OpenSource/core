﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder.Enums;
using Clouds42.Common.PdfDocuments;

namespace Clouds42.AgencyAgreement.Contracts.DocumentBuilder
{
    /// <summary>
    /// Провайдер для преобразования Html в Pdf
    /// </summary>
    public interface IHtmlToPdfProvider
    {
        /// <summary>
        /// Сгенерировать Pdf документ
        /// </summary>
        /// <param name="htmlContent">Html контент</param>
        /// <param name="uniqTitle">Заголовок</param>
        /// <param name="pdfPageSize">Размер страницы Pdf</param>
        /// <param name="documentParams">Параметры документа</param>
        /// <returns>Pdf документ</returns>
        byte[] GeneratePdf(string htmlContent, string uniqTitle, PdfPageSizeEnum pdfPageSize,
            PdfDocumentParams? documentParams = null);
    }
}
