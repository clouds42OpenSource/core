﻿using Microsoft.AspNetCore.Http;

namespace Clouds42.AgencyAgreement.Contracts.AgentFileStorage
{
    /// <summary>
    /// Провайдер для работы с файловым хранилищем агента
    /// </summary>
    public interface IAgentFileStorageProvider
    {
        /// <summary>
        /// Получить путь к файловому хранилищу агента
        /// </summary>
        /// <param name="accountId">Id агента(аккаунта)</param>
        /// <returns>Путь к файловому хранилищу</returns>
        string GetPathToFileStorageAgent(Guid accountId);

        /// <summary>
        /// Получить путь для файла агента
        /// </summary>
        /// <param name="accountId">Id аккаунта(агента)</param>
        /// <param name="fileName">Название файла</param>
        /// <returns>Путь для файла агента</returns>
        string GetPathForFileAgent(Guid accountId, string fileName);

        /// <summary>
        /// Сохранить файл в файловое хранилище агента
        /// </summary>
        /// <param name="accountId">Id аккаунта(агента)</param>
        /// <param name="file">Файл</param>
        /// <param name="fileName">Название файла</param>
        void SaveFileInFileStorageAgent(Guid accountId, IFormFile file, string fileName);

        /// <summary>
        /// Проверить наличие файла в файловом хранилище агента
        /// </summary>
        /// <param name="accountId">Id агента</param>
        /// <param name="fileName">Название файла</param>
        /// <returns>Наличие файла</returns>
        bool IsExistFileInAgentStorage(Guid accountId, string fileName);

        /// <summary>
        /// Сгенерировать новое имя файлу
        /// </summary>
        /// <param name="oldFileName">Старое имя файла</param>
        /// <returns>Новое имя файла</returns>
        string GenerateNewFileName(string oldFileName);

        /// <summary>
        /// Получить временную папку агентского хранилища
        /// </summary>
        /// <returns>Временная папка агентского хранилища</returns>
        string GetTemporaryFolderInAgentStorage();
    }
}
