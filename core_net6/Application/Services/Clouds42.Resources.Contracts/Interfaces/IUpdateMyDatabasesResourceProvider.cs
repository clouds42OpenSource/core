﻿using CommonLib.Enums;

namespace Clouds42.Resources.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для обновления ресурса сервиса "Мои инф. базы"
    /// </summary>
    public interface IUpdateMyDatabasesResourceProvider
    {
        /// <summary>
        /// Увеличить количество оплаченных информационных баз
        /// для ресурса сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="paidDatabasesCount">Количество оплаченных информационных баз,
        /// на которое необходимо увеличить ресурс</param>
        void IncreasePaidDatabasesCount(Guid accountId, ResourceType systemServiceType, int paidDatabasesCount);

        /// <summary>
        /// Увеличить актуальное количество информационных баз
        /// для ресурса сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="actualDatabasesCount">Актуальное количество инф. баз,
        /// на которое необходимо увеличить ресурс</param>
        void IncreaseActualDatabasesCount(Guid accountId, ResourceType systemServiceType, int actualDatabasesCount);

        /// <summary>
        /// Обновить актуальное количество баз для ресурса сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="actualDatabasesCount">Актуальное количество инф. баз,
        /// которое необходимо обновить в ресурсе</param>
        void UpdateActualDatabasesCount(Guid accountId, ResourceType systemServiceType, int actualDatabasesCount);

        /// <summary>
        /// Установить единое значение на количество информационных баз в ресурсе
        /// (Количество актуальных и оплаченных)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="databasesCount">Количество информационных баз,
        /// которое необходимо записать в ресурс</param>
        void SetSingleValueForDatabasesCount(Guid accountId, ResourceType systemServiceType, int databasesCount);
    }
}
