﻿using System.Linq.Expressions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Resources.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными ресурсов
    /// </summary>
    public interface IResourceDataProvider
    {
        /// <summary>
        /// Получить список ресурсов
        /// </summary>
        /// <param name="expression">Выражение для поиска</param>
        /// <param name="service">Тип системного сервиса</param>
        /// <param name="resourceTypes">Типы ресурсов</param>
        /// <returns>Cписок ресурсов</returns>
        IQueryable<Resource> GetResourcesLazy(Clouds42Service service, Expression<Func<Resource, bool>> expression,
            params ResourceType[] resourceTypes);

        /// <summary>
        /// Получить список ресурсов
        /// </summary>
        /// <param name="expression">Выражение для поиска</param>
        /// <param name="service">Тип системного сервиса</param>
        /// <param name="resourceTypes">Типы ресурсов</param>
        /// <returns>Cписок ресурсов</returns>
        List<Resource> GetResources(Clouds42Service service, Expression<Func<Resource, bool>> expression,
            params ResourceType[] resourceTypes);

        /// <summary>
        /// Получить все ресурсы аккаунта по услуге
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Ресурсы аккаунта по услуге</returns>
        IQueryable<Resource> GetAllAccountResourcesForServiceType(Guid accountId, Guid serviceTypeId);

        /// <summary>
        /// Получить сгрупированные ресурсы услуги по аккаунту
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Сгрупированные ресурсы услуги по аккаунту</returns>
        IQueryable<GroupedResourcesByAccountDto> GetGroupedServiceTypeResourcesByAccount(Guid serviceTypeId);

        /// <summary>
        /// Получить стоимость используемых ресурсов по сервису для аккаунта
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Стоимость используемых ресурсов по сервису для аккаунта</returns>
        decimal GetUsedPaidResourcesCostByService(Guid serviceId, Guid accountId);

        /// <summary>
        /// Получить все ресурсы аккаунта по сервису
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Все ресурсы аккаунта по сервису</returns>
        IQueryable<Resource> GetAllAccountResourcesForService(Guid accountId, Guid serviceId);

        /// <summary>
        /// Получить данные оплаченных/используемых ресурсов по услугам
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные оплаченных/используемых ресурсов по услугам
        /// для аккаунта</returns>
        IQueryable<ResourcesDataByServiceTypeDto> GetUsedPaidResourcesDataByServiceTypes(Guid accountId);

        /// <summary>
        /// Получить свободные ресурсы по услуге для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Свободные ресурсы по услуге для аккаунта</returns>
        IQueryable<Resource> GetFreeResourcesByServiceType(Guid accountId, Guid serviceTypeId);

        /// <summary>
        /// Получить ресурсы пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Список ресурсов пользователя</returns>
        IQueryable<Resource> GetAccountUserResources(Guid accountUserId);

        /// <summary>
        /// Получить ресурсы сервиса для пользователя
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Список ресурсов сервиса для пользователя</returns>
        IQueryable<Resource> GetServiceResourcesForAccountUser(Guid serviceId, Guid accountUserId);

        /// <summary>
        /// Получить ресурсы аккаунта сгруппированные по сервису
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Ресурсы аккаунта сгруппированные по сервису</returns>
        IQueryable<GroupedResourcesByServiceDto> GetGroupedResourcesByService(Guid accountId);
    }
}
