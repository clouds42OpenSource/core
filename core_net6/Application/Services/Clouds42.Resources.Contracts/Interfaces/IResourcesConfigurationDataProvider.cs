﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Resources.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными конфигурации ресурсов
    /// </summary>
    public interface IResourcesConfigurationDataProvider
    {
        /// <summary>
        /// Получить конфигурацию ресурсов сервиса
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Конфигурация ресурсов</returns>
        ResourcesConfiguration GetResourcesConfiguration(Guid accountId, Guid serviceId);

        /// <summary>
        /// Получить конфигурацию ресурсов сервиса
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemService">Тип системного сервиса</param>
        /// <returns>Конфигурация ресурсов</returns>
        ResourcesConfiguration GetResourcesConfiguration(Guid accountId, Clouds42Service systemService);

        /// <summary>
        /// Получить конфигурацию ресурсов сервиса
        /// для аккаунта или выкинуть исключение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Конфигурация ресурсов</returns>
        ResourcesConfiguration GetResourcesConfigurationOrThrowException(Guid accountId, Guid serviceId);

        /// <summary>
        /// Получить дату пролонгации
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <returns>Дата пролонгации</returns>
        DateTime GetExpireDateOrThrowException(ResourcesConfiguration resourcesConfiguration);

    }
}
