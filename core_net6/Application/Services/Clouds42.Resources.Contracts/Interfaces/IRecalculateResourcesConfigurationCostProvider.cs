﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Resources.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для пересчета стоимости конфигурации ресурсов
    /// </summary>
    public interface IRecalculateResourcesConfigurationCostProvider
    {
        /// <summary>
        /// Пересчитать стоимость конфигурации ресурсов по сервису
        /// Учитываются только используемые ресурсы(по описанию ресурсов)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        void Recalculate(Guid accountId, Guid serviceId);

        /// <summary>
        /// Пересчитать стоимость конфигурации ресурсов по сервису
        /// Учитываются только используемые ресурсы(по описанию ресурсов)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Id сервиса</param>
        void Recalculate(Guid accountId, IBillingService service);
    }
}
