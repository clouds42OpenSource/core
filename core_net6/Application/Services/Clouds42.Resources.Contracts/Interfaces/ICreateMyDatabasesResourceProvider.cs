﻿using Clouds42.DataContracts.MyDatabasesService;

namespace Clouds42.Resources.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер создания ресурса для сервиса "Мои информационные базы"
    /// </summary>
    public interface ICreateMyDatabasesResourceProvider
    {
        /// <summary>
        /// Создать ресурс для сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="model">Модель создания ресурса "Мои информационные базы"</param>
        void Create(CreateMyDatabasesResourceDto model);
    }
}
