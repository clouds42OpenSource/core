﻿using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;

namespace Clouds42.Resources.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными ресурса сервиса "Мои информационные базы"
    /// </summary>
    public interface IMyDatabasesResourceDataProvider
    {
        /// <summary>
        /// Получить ресурс сервиса "Мои информационные базы" для аккаунта
        /// или выкинуть исключение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса
        /// "Мои информационные базы"(с типом биллинга по аккаунту)</param>
        /// <returns>Ресурс сервиса "Мои информационные базы"</returns>
        MyDatabasesResource GetMyDatabasesResourceOrThrowException(Guid accountId,
            ResourceType systemServiceType);

        /// <summary>
        /// Получить актуальные значения для ресурсов сервиса "Мои инф. базы"
        /// сгруппированные по аккаунту
        /// </summary>
        /// <returns>Актуальные значения для ресурсов сервиса "Мои инф. базы"
        /// сгруппированные по аккаунту</returns>
        IQueryable<MyDatabasesResourcesActualValuesForAccountDto> GetActualValuesForMyDatabasesResourcesByAccounts();

        /// <summary>
        /// Получить активные базы для биллинга(для учета баз, в ресурсах)
        /// </summary>
        /// <returns>Активные базы для биллинга(для учета баз, в ресурсах)</returns>
        IQueryable<AccountDatabase> GetActiveDatabasesForBilling();

        /// <summary>
        /// Получить актуальные значения ресурсов сервиса "Мои инф. базы" для аккаунта
        /// </summary>
        /// <returns>Получить актуальные значения ресурсов сервиса "Мои инф. базы" для аккаунта</returns>
        MyDatabasesResourcesActualValuesForAccountDto GetActualValuesForMyDatabasesResourcesForAccount(Guid accountId);

        /// <summary>
        /// Получить данные ресурсов сервиса "Мои инф. базы" по аккаунту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные ресурсов сервиса "Мои инф. базы" по аккаунту</returns>
        IQueryable<MyDatabasesResourceDataDto> GetMyDatabasesResourcesDataByAccount(Guid accountId);
    }
}
