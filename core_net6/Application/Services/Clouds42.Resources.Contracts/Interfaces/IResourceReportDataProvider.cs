﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Resources.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными ресурсов для отчета 
    /// </summary>
    public interface IResourceReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных ресурсов для отчета
        /// </summary>
        /// <returns>Список моделей данных ресурсов для отчета</returns>
        List<ResourceReportDataDto> GetResourceReportDataDcs();
    }
}
