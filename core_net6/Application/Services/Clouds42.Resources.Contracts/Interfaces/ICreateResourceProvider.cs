﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Resources.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для создания ресурса услуги
    /// </summary>
    public interface ICreateResourceProvider
    {
        /// <summary>
        /// Создать ресурс услуги
        /// </summary>
        /// <param name="createResourceDc">Модель создания нового ресурса</param>
        /// <returns>Ресурс услуги</returns>
        Guid Create(CreateResourceDto createResourceDc);
    }
}
