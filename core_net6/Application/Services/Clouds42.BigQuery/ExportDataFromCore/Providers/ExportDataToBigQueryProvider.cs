﻿using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.ExportDataFromCore;

namespace Clouds42.BigQuery.ExportDataFromCore.Providers
{
    /// <summary>
    /// Провайдер для экспорта данных в BigQuery
    /// </summary>
    internal class ExportDataToBigQueryProvider(IRefreshDataInBigQueryProcessor refreshDataInBigQueryProcessor)
        : IExportDataToBigQueryProvider
    {
        /// <summary>
        /// Экспортировать данные аккаунта в BigQuery
        /// </summary>
        public void ExportAccountsData() => refreshDataInBigQueryProcessor.Refresh<IAccountsDataRefresher>();

        /// <summary>
        /// Экспортировать данные пользователей в BigQuery
        /// </summary>
        public void ExportAccountUsersData() => refreshDataInBigQueryProcessor.Refresh<IAccountUsersDataRefresher>();

        /// <summary>
        /// Экспортировать данные ресурсов в BigQuery
        /// </summary>
        public void ExportResourcesData() => refreshDataInBigQueryProcessor.Refresh<IResourcesDataRefresher>();

        /// <summary>
        /// Экспортировать данные счетов на оплату в BigQuery
        /// </summary>
        public void ExportInvoicesData() => refreshDataInBigQueryProcessor.Refresh<IInvoicesDataRefresher>();

        /// <summary>
        /// Экспортировать данные информационных баз в BigQuery
        /// </summary>
        public void ExportDatabasesData() => refreshDataInBigQueryProcessor.Refresh<IDatabasesDataRefresher>();

        /// <summary>
        /// Экспортировать данные платежей в BigQuery
        /// </summary>
        public void ExportPaymentsData() => refreshDataInBigQueryProcessor.Refresh<IPaymentsDataRefresher>();
    }
}