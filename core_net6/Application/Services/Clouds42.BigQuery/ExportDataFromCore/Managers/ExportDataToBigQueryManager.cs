﻿using Clouds42.BigQuery.Contracts.ExportDataFromCore;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.BigQuery.ExportDataFromCore.Managers
{
    /// <summary>
    /// Менеджер для экспорта данных в BigQuery
    /// </summary>
    public class ExportDataToBigQueryManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IExportDataToBigQueryProvider exportDataToBigQueryProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IExportDataToBigQueryManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Экспортировать данные аккаунта в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ExportAccountsData() =>
            PerformExportActionWithErrorHandling(exportDataToBigQueryProvider.ExportAccountsData, "аккаунтов");

        /// <summary>
        /// Экспортировать данные пользователей в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ExportAccountUsersData() =>
            PerformExportActionWithErrorHandling(exportDataToBigQueryProvider.ExportAccountUsersData, "пользователей");

        /// <summary>
        /// Экспортировать данные ресурсов в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ExportResourcesData() =>
            PerformExportActionWithErrorHandling(exportDataToBigQueryProvider.ExportResourcesData, "ресурсов");

        /// <summary>
        /// Экспортировать данные счетов на оплату в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ExportInvoicesData() =>
            PerformExportActionWithErrorHandling(exportDataToBigQueryProvider.ExportInvoicesData, "счетов на оплату");

        /// <summary>
        /// Экспортировать данные информационных баз в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ExportDatabasesData() =>
            PerformExportActionWithErrorHandling(exportDataToBigQueryProvider.ExportDatabasesData, "информационных баз");

        /// <summary>
        /// Экспортировать данные платежей в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ExportPaymentsData() =>
            PerformExportActionWithErrorHandling(exportDataToBigQueryProvider.ExportPaymentsData, "платежей");

        /// <summary>
        /// Выполнить действие экспорта данных в BigQuery
        /// с обработкой ошибки
        /// </summary>
        /// <param name="exportDataAction">Действие экспорта данных</param>
        /// <param name="exportDataType">Тип экспортируемых данных</param>
        /// <returns>Результат выполнения</returns>
        private ManagerResult PerformExportActionWithErrorHandling(Action exportDataAction, string exportDataType)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ExportDataToBigQuery);
                exportDataAction();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Экспорт данных аккаунта завершился с ошибкой] данные : {exportDataType} ";
                _handlerException.Handle(ex, errorMessage);
                return PreconditionFailed($"{errorMessage} Причина: {ex.Message}");
            }
        }
    }
}
