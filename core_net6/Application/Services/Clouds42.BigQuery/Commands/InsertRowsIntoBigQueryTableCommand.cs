﻿using Clouds42.BigQuery.Contracts.Commands;
using Clouds42.BigQuery.Extensions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BaseModel.BiqQuery;
using Google.Cloud.BigQuery.V2;

namespace Clouds42.BigQuery.Commands
{
    /// <summary>
    /// Команда для записи/вставки строк
    /// в таблицу BigQuery 
    /// </summary>
    internal class InsertRowsIntoBigQueryTableCommand : ClearBigQueryTableCommand, IInsertRowsIntoBigQueryTableCommand
    {
        private readonly Lazy<int> _defaultDataChunkSizeForInsert = new(CloudConfigurationProvider.BigQuery.GetDefaultDataChunkSizeForInsert);

        /// <summary>
        /// Выполнить команду по записи/вставки строк
        /// в таблицу BigQuery 
        /// </summary>
        /// <param name="model">Модель записи/вставки строк
        /// в таблицу BigQuery</param>
        public void Execute(InsertRowsIntoBigQueryTableDto model) => ExecuteCommandAction(bigQueryClient =>
        {
            foreach (var portionRows in GetPortionedRowsForInsertion(model.Rows))
                bigQueryClient.InsertRows(GetDatabaseName(), model.TableName, portionRows);
        });

        /// <summary>
        /// Получить порции строк для записи/вставки
        /// </summary>
        /// <param name="rows">Модели записи/вставки строки
        /// в таблицу BigQuery</param>
        /// <returns>Порции строк для записи/вставки</returns>
        private IEnumerable<BigQueryInsertRow[]> GetPortionedRowsForInsertion(List<BigQueryInsertRowDto> rows)
        {
            var portionsOfRows = rows.SplitListIntoParts(_defaultDataChunkSizeForInsert.Value);

            foreach (var portionOfRows in portionsOfRows)
                yield return portionOfRows.PrepareRowsForInsertion();
        }
    }
}