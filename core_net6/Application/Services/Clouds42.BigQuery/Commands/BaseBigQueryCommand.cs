﻿using Clouds42.Configurations.Configurations;
using Clouds42.HandlerExeption.Contract;
 
using Clouds42.Logger;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.BigQuery.V2;

namespace Clouds42.BigQuery.Commands
{
    /// <summary>
    /// Базовый класс команды BigQuery
    /// </summary>
    public abstract class BaseBigQueryCommand
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();
        private static readonly IHandlerException _handlerException = HandlerException42.GetHandler();
        private readonly Lazy<string> _pathToCredentialFile = new(CloudConfigurationProvider.BigQuery.GetPathToCredentialFile);
        private readonly Lazy<string> _projectId = new(CloudConfigurationProvider.BigQuery.GetProjectId);
        private readonly Lazy<string> _databaseName = new(CloudConfigurationProvider.BigQuery.GetDatabaseName);

        /// <summary>
        /// Выполнить действие команды BigQuery
        /// </summary>
        /// <param name="commandAction">Действие команды BigQuery</param>
        protected void ExecuteCommandAction(Action<BigQueryClient> commandAction)
        {
            var credentials = GoogleCredential.FromFile(_pathToCredentialFile.Value);
            var message = $"Выполнение команды '{GetCurrentCommandName()}'";

            try
            {
                using var client = BigQueryClient.Create(_projectId.Value, credentials);
                commandAction(client);
                _logger.Trace($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка Выполния действия команды BigQuery]{message}");
                throw;
            }
        }

        /// <summary>
        /// Получить Id проекта в сервисе BigQuery
        /// </summary>
        /// <returns>Id проекта в сервисе BigQuery</returns>
        protected string GetProjectId() => _projectId.Value;

        /// <summary>
        /// Получить название базы в сервисе BigQuery
        /// </summary>
        /// <returns>Название базы в сервисе BigQuery</returns>
        protected string GetDatabaseName() => _databaseName.Value;

        /// <summary>
        /// Получить название текущей команды
        /// </summary>
        /// <returns>Название текущей команды</returns>
        private string GetCurrentCommandName() => GetType().Name;
    }
}
