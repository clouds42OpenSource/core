﻿using Clouds42.BigQuery.Contracts.Commands;
using Clouds42.DataContracts.BaseModel.BiqQuery;

namespace Clouds42.BigQuery.Commands
{
    /// <summary>
    /// Команда для очистки таблицы
    /// в БД BigQuery 
    /// </summary>
    internal class ClearBigQueryTableCommand : BaseBigQueryCommand, IClearBigQueryTableCommand
    {
        /// <summary>
        /// Выполнить команду по очистке таблицы BigQuery
        /// </summary>
        /// <param name="model">Модель для очистки таблицы
        /// в БД BigQuery</param>
        public void Execute(ClearBigQueryTableDto model) => ExecuteCommandAction(bigQueryClient =>
            bigQueryClient.ExecuteQuery(GenerateRequestToClearTable(model.TableName), null));

        /// <summary>
        /// Сформировать запрос для очистки таблицы
        /// в БД BigQuery
        /// </summary>
        /// <param name="tableName">Название таблицы</param>
        /// <returns>Запрос для очистки таблицы
        /// в БД BigQuery</returns>
        private string GenerateRequestToClearTable(string tableName) =>
            $@"DELETE FROM `{GetProjectId()}.{GetDatabaseName()}.{tableName}` where true;";
    }
}
