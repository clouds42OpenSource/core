﻿using Clouds42.DataContracts.BaseModel.BiqQuery;
using Google.Cloud.BigQuery.V2;

namespace Clouds42.BigQuery.Extensions
{
    /// <summary>
    /// Расширение для модели записи/вставки строки
    /// в таблицу BigQuery
    /// </summary>
    public static class BigQueryInsertRowDcExtension
    {
        /// <summary>
        /// Подготовить строки для записи в таблицу BigQuery
        /// </summary>
        /// <param name="rows">Модели записи/вставки строки
        /// в таблицу BigQuery</param>
        /// <returns>Строки для записи в таблицу BigQuery</returns>
        public static BigQueryInsertRow[] PrepareRowsForInsertion(this List<BigQueryInsertRowDto> rows) =>
            rows.Select(row => new BigQueryInsertRow(row.InsertKey) { row.RowElements }).ToArray();
    }
}
