﻿using System.Reflection;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Attributes;
using Google.Cloud.BigQuery.V2;

namespace Clouds42.BigQuery.Extensions
{
    /// <summary>
    /// Расширение модели записи/вставки в таблицу BigQuery
    /// </summary>
    public static class BigQueryInsertModelExtension
    {
        /// <summary>
        /// Преобразователи типа значений для BigQuery
        /// </summary>
        private static readonly
            List<(Func<Type, bool> DetermineCompliance, Func<object, object> PerformTransformationFunc)>
            ValueTypeTransformations
                =
                [
                    (type => type == typeof(Guid) || type == typeof(Guid?), TransformGuidValue),
                    (type => type == typeof(decimal) || type == typeof(decimal?), TransformDecimalValue),
                    (type => type.BaseType == typeof(Enum), TransformEnumValue)
                ];

        /// <summary>
        /// Получить элементы строки для записи/вставки в таблицу BigQuery
        /// </summary>
        /// <typeparam name="TModel">Тип модели записи/вставки</typeparam>
        /// <param name="model">Модель записи/вставки</param>
        /// <param name="bindingAttr">Аттрибуты по которым необходимо выбрать
        /// элементы из модели</param>
        /// <returns>Элементы строки для записи/вставки в таблицу BigQuery</returns>
        public static IDictionary<string, object> SelectRowElements<TModel>(this TModel model,
            BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
            where TModel : class => model.GetType().GetProperties(bindingAttr).Where(
            prop => !Attribute.IsDefined(prop, typeof(BigQueryIgnoreAttribute))).ToDictionary
        (
            propInfo => propInfo.Name,
            propInfo => propInfo.GetPropertyValueByPerformingTransformation(model)
        );

        /// <summary>
        /// Получить значение поля выполнив преобразование(если это необходимо)
        /// </summary>
        /// <typeparam name="TModel">Тип модели записи/вставки</typeparam>
        /// <param name="propertyInfo">Информация о поле</param>
        /// <param name="model">Модель записи/вставки</param>
        /// <returns>Значение поля после преобразования</returns>
        private static object GetPropertyValueByPerformingTransformation<TModel>(this PropertyInfo propertyInfo,
            TModel model) where TModel : class
        {
            var value = propertyInfo.GetValue(model, null);

            var transformation = ValueTypeTransformations.FirstOrDefault(transform =>
                transform.DetermineCompliance(propertyInfo.PropertyType));

            if (transformation == default)
                return value;

            return transformation.PerformTransformationFunc(value);
        }

        /// <summary>
        /// Преобразовать значение Decimal
        /// </summary>
        /// <param name="value">Значение с типом Decimal или Decimal?</param>
        /// <returns>Преобразованное значение для BigQuery</returns>
        private static object TransformDecimalValue(object value)
        {
            if (value == null)
                return BigQueryNumeric.Zero;

            return BigQueryNumeric.FromDecimal((decimal)value, LossOfPrecisionHandling.Throw);
        }

        /// <summary>
        /// Преобразовать значение GUID
        /// </summary>
        /// <param name="value">Значение с типом Guid или Guid?</param>
        /// <returns>Преобразованное значение для BigQuery</returns>
        private static object TransformGuidValue(object value)
        {
            if (value == null || (Guid)value == Guid.Empty)
                return null;

            return value.ToString();
        }

        /// <summary>
        /// Преобразовать значение Enum
        /// </summary>
        /// <param name="value">Значение с типом Enum</param>
        /// <returns>Преобразованное значение для BigQuery</returns>
        private static object TransformEnumValue(object value) => ((Enum)value).Description();
    }
}