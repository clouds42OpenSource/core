﻿using Clouds42.BigQuery.Contracts.DataRefreshers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BigQuery.DataRefreshers.Processors
{
    /// <summary>
    /// Процессор для перезаписи данных в BigQuery
    /// </summary>
    internal class RefreshDataInBigQueryProcessor(IServiceProvider serviceProvider) : IRefreshDataInBigQueryProcessor
    {
        /// <summary>
        /// Выполнить перезапись данных в BigQuery
        /// </summary>
        /// <typeparam name="TRefresher">Тип интерфейса класса
        /// для перезаписи данных</typeparam>
        public void Refresh<TRefresher>() where TRefresher : IBigQueryDataRefresher =>
            serviceProvider.GetRequiredService<TRefresher>().Refresh();
    }
}