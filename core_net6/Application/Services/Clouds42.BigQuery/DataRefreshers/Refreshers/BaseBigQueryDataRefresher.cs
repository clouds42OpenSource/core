﻿using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.Tables;

namespace Clouds42.BigQuery.DataRefreshers.Refreshers
{
    /// <summary>
    /// Базовый класс для рефреша/перезаписи
    /// данных в таблице BigQuery
    /// </summary>
    public abstract class BaseBigQueryDataRefresher<TModel>(IServiceProvider serviceProvider) : IBigQueryDataRefresher
        where TModel : class
    {
        protected readonly IServiceProvider ServiceProvider = serviceProvider;

        /// <summary>
        /// Перезаписать данные в таблице BigQuery
        /// </summary>
        public void Refresh()
        {
            var entityToManipulateTable = GetBigQueryTable();
            entityToManipulateTable.ClearTable();
            entityToManipulateTable.InsertRows(SelectDataForRefresh());
        }

        /// <summary>
        /// Выбрать данные для перезаписи
        /// </summary>
        /// <returns>Данные для перезаписи</returns>
        protected abstract List<TModel> SelectDataForRefresh();

        /// <summary>
        /// Получить таблицу BigQuery
        /// </summary>
        /// <returns>Таблица BigQuery</returns>
        protected abstract IBigQueryTable<TModel> GetBigQueryTable();
    }
}