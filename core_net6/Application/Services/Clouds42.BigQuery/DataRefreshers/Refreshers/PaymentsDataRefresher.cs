﻿using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BigQuery.DataRefreshers.Refreshers
{
    /// <summary>
    /// Класс для рефреша/перезаписи
    /// данных в таблице платежей(BigQuery)
    /// </summary>
    internal class PaymentsDataRefresher(IServiceProvider serviceProvider)
        : BaseBigQueryDataRefresher<PaymentReportDataDto>(serviceProvider), IPaymentsDataRefresher
    {
        /// <summary>
        /// Выбрать данные для перезаписи
        /// </summary>
        /// <returns>Данные для перезаписи</returns>
        protected override List<PaymentReportDataDto> SelectDataForRefresh() =>
            ServiceProvider.GetRequiredService<IPaymentReportDataProvider>().GetPaymentReportDataDcs();

        /// <summary>
        /// Получить таблицу BigQuery
        /// </summary>
        /// <returns>Таблица BigQuery</returns>
        protected override IBigQueryTable<PaymentReportDataDto> GetBigQueryTable() =>
            ServiceProvider.GetRequiredService<IPaymentsBigQueryTable>();
    }
}
