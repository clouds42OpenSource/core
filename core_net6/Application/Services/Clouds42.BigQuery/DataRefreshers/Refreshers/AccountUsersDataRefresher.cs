﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.DataContracts.AccountUser;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BigQuery.DataRefreshers.Refreshers
{
    /// <summary>
    /// Класс для рефреша/перезаписи
    /// данных в таблице пользователей(BigQuery)
    /// </summary>
    internal class AccountUsersDataRefresher(IServiceProvider serviceProvider)
        : BaseBigQueryDataRefresher<AccountUserReportDataDto>(serviceProvider), IAccountUsersDataRefresher
    {
        private readonly IAccountUserReportDataProvider _accountUserReportDataProvider = serviceProvider.GetRequiredService<IAccountUserReportDataProvider>();
        private readonly IAccountUsersBigQueryTable _accountUsersBigQueryTable = serviceProvider.GetRequiredService<IAccountUsersBigQueryTable>();

        /// <summary>
        /// Выбрать данные для перезаписи
        /// </summary>
        /// <returns>Данные для перезаписи</returns>
        protected override List<AccountUserReportDataDto> SelectDataForRefresh() =>
            _accountUserReportDataProvider.GetAccountUserReportDataDcs();

        /// <summary>
        /// Получить таблицу BigQuery
        /// </summary>
        /// <returns>Таблица BigQuery</returns>
        protected override IBigQueryTable<AccountUserReportDataDto> GetBigQueryTable() =>
            _accountUsersBigQueryTable;
    }
}
