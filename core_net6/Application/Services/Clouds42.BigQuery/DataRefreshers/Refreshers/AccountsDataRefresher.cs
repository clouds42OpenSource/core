﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.DataContracts.Account;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BigQuery.DataRefreshers.Refreshers
{
    /// <summary>
    /// Класс для рефреша/перезаписи
    /// данных в таблице аккаунтов(BigQuery)
    /// </summary>
    internal class AccountsDataRefresher(IServiceProvider serviceProvider)
        : BaseBigQueryDataRefresher<AccountReportDataDto>(serviceProvider), IAccountsDataRefresher
    {
        /// <summary>
        /// Выбрать данные для перезаписи
        /// </summary>
        /// <returns>Данные для перезаписи</returns>
        protected override List<AccountReportDataDto> SelectDataForRefresh() =>
            ServiceProvider.GetRequiredService<IAccountReportDataProvider>().GetAccountReportDataDcs();

        /// <summary>
        /// Получить таблицу BigQuery
        /// </summary>
        /// <returns>Таблица BigQuery</returns>
        protected override IBigQueryTable<AccountReportDataDto> GetBigQueryTable() =>
            ServiceProvider.GetRequiredService<IAccountsBigQueryTable>();
    }
}