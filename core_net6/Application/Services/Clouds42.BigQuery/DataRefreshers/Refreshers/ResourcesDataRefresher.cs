﻿using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Resources.Contracts.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BigQuery.DataRefreshers.Refreshers
{
    /// <summary>
    /// Класс для рефреша/перезаписи
    /// данных в таблице ресурсов(BigQuery)
    /// </summary>
    internal class ResourcesDataRefresher(IServiceProvider serviceProvider)
        : BaseBigQueryDataRefresher<ResourceReportDataDto>(serviceProvider), IResourcesDataRefresher
    {
        /// <summary>
        /// Выбрать данные для перезаписи
        /// </summary>
        /// <returns>Данные для перезаписи</returns>
        protected override List<ResourceReportDataDto> SelectDataForRefresh() =>
            ServiceProvider.GetRequiredService<IResourceReportDataProvider>().GetResourceReportDataDcs();

        /// <summary>
        /// Получить таблицу BigQuery
        /// </summary>
        /// <returns>Таблица BigQuery</returns>
        protected override IBigQueryTable<ResourceReportDataDto> GetBigQueryTable() =>
            ServiceProvider.GetRequiredService<IResourcesBigQueryTable>();
    }
}
