﻿using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.DataContracts.Billing.Inovice;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BigQuery.DataRefreshers.Refreshers
{
    /// <summary>
    /// Класс для рефреша/перезаписи
    /// данных в таблице счетов на оплату(BigQuery)
    /// </summary>
    internal class InvoicesDataRefresher(IServiceProvider serviceProvider)
        : BaseBigQueryDataRefresher<InvoiceReportDataDto>(serviceProvider), IInvoicesDataRefresher
    {
        /// <summary>
        /// Выбрать данные для перезаписи
        /// </summary>
        /// <returns>Данные для перезаписи</returns>
        protected override List<InvoiceReportDataDto> SelectDataForRefresh() =>
            ServiceProvider.GetRequiredService<IInvoiceReportDataProvider>().GetInvoiceReportDataDcs();

        /// <summary>
        /// Получить таблицу BigQuery
        /// </summary>
        /// <returns>Таблица BigQuery</returns>
        protected override IBigQueryTable<InvoiceReportDataDto> GetBigQueryTable() =>
            ServiceProvider.GetRequiredService<IInvoicesBigQueryTable>();
    }
}
