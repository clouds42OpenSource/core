﻿using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.AccountDatabase.Contracts.Interfaces;

namespace Clouds42.BigQuery.DataRefreshers.Refreshers
{
    /// <summary>
    /// Класс для рефреша/перезаписи
    /// данных в таблице информационных баз(BigQuery)
    /// </summary>
    internal class DatabasesDataRefresher(IServiceProvider serviceProvider)
        : BaseBigQueryDataRefresher<AccountDatabaseReportDataDto>(serviceProvider), IDatabasesDataRefresher
    {
        /// <summary>
        /// Выбрать данные для перезаписи
        /// </summary>
        /// <returns>Данные для перезаписи</returns>
        protected override List<AccountDatabaseReportDataDto> SelectDataForRefresh() =>
            ServiceProvider.GetRequiredService<IAccountDatabaseReportDataProvider>().GetAccountDatabaseReportDataDcs();


        /// <summary>
        /// Получить таблицу BigQuery
        /// </summary>
        /// <returns>Таблица BigQuery</returns>
        protected override IBigQueryTable<AccountDatabaseReportDataDto> GetBigQueryTable() =>
            ServiceProvider.GetRequiredService<IDatabasesBigQueryTable>();
    }
}
