﻿using Clouds42.BigQuery.Contracts.Commands;
using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.BigQuery.Extensions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BaseModel.BiqQuery;
using Clouds42.Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BigQuery.Tables
{
    /// <summary>
    /// Базовый класс таблицы BigQuery 
    /// </summary>
    /// <typeparam name="TModel">Тип модели для записи/вставки</typeparam>
    public abstract class BaseBigQueryTable<TModel>(
        IServiceProvider serviceProvider,
        ILogger42 logger)
        : IBigQueryTable<TModel>
        where TModel : class
    {
        private readonly IClearBigQueryTableCommand _clearBigQueryTableCommand = serviceProvider.GetRequiredService<IClearBigQueryTableCommand>();
        private readonly IInsertRowsIntoBigQueryTableCommand _insertRowsIntoBigQueryTableCommand = serviceProvider.GetRequiredService<IInsertRowsIntoBigQueryTableCommand>();

        /// <summary>
        /// Очистить таблицу
        /// (удалить все записи в таблице)
        /// </summary>
        public void ClearTable()
        {
            var tableName = GetTableName();
            var message = $"Удаление всех данных в таблице '{tableName}' BigQuery";

            try
            {
                _clearBigQueryTableCommand.Execute(new ClearBigQueryTableDto
                {
                    TableName = tableName
                });

                logger.Trace($"{message} завершилось успешно.");
            }
            catch (Exception ex)
            {
                logger.Trace($"{message} завершилось c ошибкой: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Записать/вставить строки в таблицу
        /// </summary>
        /// <param name="models">Модели для записи/вставки</param>
        public void InsertRows(List<TModel> models)
        {
            var tableName = GetTableName();
            var message = $"Запись данных в таблицу '{tableName}' BigQuery";

            try
            {
                _insertRowsIntoBigQueryTableCommand.Execute(new InsertRowsIntoBigQueryTableDto
                {
                    TableName = tableName,
                    Rows = models.Select(SelectToBigQueryInsertRowDc).ToList()
                });

                logger.Trace($"{message} завершилось успешно. Количество строк: {models.Count}");
            }
            catch (Exception ex)
            {
                logger.Trace($"{message} завершилось c ошибкой: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Переложить в модель записи/вставки строки
        /// в таблицу BigQuery
        /// </summary>
        /// <param name="model">Модели для записи/вставки</param>
        /// <returns>Модель записи/вставки строки
        /// в таблицу BigQuery</returns>
        protected virtual BigQueryInsertRowDto SelectToBigQueryInsertRowDc(TModel model) =>
            new()
            {
                InsertKey = GetInsertKey(model),
                RowElements = model.SelectRowElements()
            };

        /// <summary>
        /// Получить название таблицы
        /// </summary>
        /// <returns>Название таблицы</returns>
        protected abstract string GetTableName();

        /// <summary>
        /// Получить уникальный ключ вставки,
        /// идентифицирующий строку в таблице
        /// </summary>
        /// <param name="model">Модель для записи строки</param>
        /// <returns>Уникальный ключ вставки,
        /// идентифицирующий строку в таблице</returns>
        protected abstract string GetInsertKey(TModel model);
    }
}