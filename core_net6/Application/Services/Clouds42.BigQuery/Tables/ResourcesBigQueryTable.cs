﻿using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Logger;

namespace Clouds42.BigQuery.Tables
{
    /// <summary>
    /// Класс таблицы BigQuery для ресурсов
    /// </summary>
    internal class ResourcesBigQueryTable(IServiceProvider serviceProvider, ILogger42 logger)
        : BaseBigQueryTable<ResourceReportDataDto>(serviceProvider, logger), IResourcesBigQueryTable
    {
        private readonly Lazy<string> _tableName = new(CloudConfigurationProvider.BigQuery.GetResourcesTableName);

        /// <summary>
        /// Получить название таблицы
        /// </summary>
        /// <returns>Название таблицы</returns>
        protected override string GetTableName() => _tableName.Value;

        /// <summary>
        /// Получить уникальный ключ вставки,
        /// идентифицирующий строку в таблице
        /// </summary>
        /// <param name="model">Модель для записи строки</param>
        /// <returns>Уникальный ключ вставки,
        /// идентифицирующий строку в таблице</returns>
        protected override string GetInsertKey(ResourceReportDataDto model) => model.Id.ToString();
    }
}