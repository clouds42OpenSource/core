﻿using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Logger;

namespace Clouds42.BigQuery.Tables
{
    /// <summary>
    /// Класс таблицы BigQuery для счетов на оплату
    /// </summary>
    internal class InvoicesBigQueryTable(IServiceProvider serviceProvider, ILogger42 logger)
        : BaseBigQueryTable<InvoiceReportDataDto>(serviceProvider, logger), IInvoicesBigQueryTable
    {
        private readonly Lazy<string> _tableName = new(CloudConfigurationProvider.BigQuery.GetInvoicesTableName);

        /// <summary>
        /// Получить название таблицы
        /// </summary>
        /// <returns>Название таблицы</returns>
        protected override string GetTableName() => _tableName.Value;

        /// <summary>
        /// Получить уникальный ключ вставки,
        /// идентифицирующий строку в таблице
        /// </summary>
        /// <param name="model">Модель для записи строки</param>
        /// <returns>Уникальный ключ вставки,
        /// идентифицирующий строку в таблице</returns>
        protected override string GetInsertKey(InvoiceReportDataDto model) => model.InvoiceNumber;
    }
}