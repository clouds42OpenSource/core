﻿using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Logger;

namespace Clouds42.BigQuery.Tables
{
    /// <summary>
    /// Класс таблицы BigQuery для информацонных баз
    /// </summary>
    internal class DatabasesBigQueryTable(IServiceProvider serviceProvider, ILogger42 logger)
        : BaseBigQueryTable<AccountDatabaseReportDataDto>(serviceProvider, logger), IDatabasesBigQueryTable
    {
        private readonly Lazy<string> _tableName = new(CloudConfigurationProvider.BigQuery.GetDatabasesTableName);

        /// <summary>
        /// Получить название таблицы
        /// </summary>
        /// <returns>Название таблицы</returns>
        protected override string GetTableName() => _tableName.Value;

        /// <summary>
        /// Получить уникальный ключ вставки,
        /// идентифицирующий строку в таблице
        /// </summary>
        /// <param name="model">Модель для записи строки</param>
        /// <returns>Уникальный ключ вставки,
        /// идентифицирующий строку в таблице</returns>
        protected override string GetInsertKey(AccountDatabaseReportDataDto model) => model.DatabaseNumber;
    }
}
