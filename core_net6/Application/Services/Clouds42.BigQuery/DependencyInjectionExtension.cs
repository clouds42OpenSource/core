﻿using Clouds42.BigQuery.Commands;
using Clouds42.BigQuery.Contracts.Commands;
using Clouds42.BigQuery.Contracts.DataRefreshers;
using Clouds42.BigQuery.Contracts.ExportDataFromCore;
using Clouds42.BigQuery.Contracts.Tables;
using Clouds42.BigQuery.DataRefreshers.Processors;
using Clouds42.BigQuery.DataRefreshers.Refreshers;
using Clouds42.BigQuery.ExportDataFromCore.Managers;
using Clouds42.BigQuery.ExportDataFromCore.Providers;
using Clouds42.BigQuery.Tables;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BigQuery
{
    public static class DependencyInjectionExtension
    {
        public static IServiceCollection AddBigQuery(this IServiceCollection services)
        {
            services.AddTransient<IClearBigQueryTableCommand, ClearBigQueryTableCommand>();
            services.AddTransient<IInsertRowsIntoBigQueryTableCommand, InsertRowsIntoBigQueryTableCommand>();
            services.AddTransient<IRefreshDataInBigQueryProcessor, RefreshDataInBigQueryProcessor>();
            services.AddTransient<IAccountsDataRefresher, AccountsDataRefresher>();
            services.AddTransient<IAccountUsersDataRefresher, AccountUsersDataRefresher>();
            services.AddTransient<IResourcesDataRefresher, ResourcesDataRefresher>();
            services.AddTransient<IInvoicesDataRefresher, InvoicesDataRefresher>();
            services.AddTransient<IDatabasesDataRefresher, DatabasesDataRefresher>();
            services.AddTransient<IPaymentsDataRefresher, PaymentsDataRefresher>();
            services.AddTransient<IAccountsBigQueryTable, AccountsBigQueryTable>();
            services.AddTransient<IAccountUsersBigQueryTable, AccountUsersBigQueryTable>();
            services.AddTransient<IResourcesBigQueryTable, ResourcesBigQueryTable>();
            services.AddTransient<IInvoicesBigQueryTable, InvoicesBigQueryTable>();
            services.AddTransient<IDatabasesBigQueryTable, DatabasesBigQueryTable>();
            services.AddTransient<IPaymentsBigQueryTable, PaymentsBigQueryTable>();
            services.AddTransient<IExportDataToBigQueryProvider, ExportDataToBigQueryProvider>();
            services.AddTransient<IExportDataToBigQueryManager, ExportDataToBigQueryManager>();
            return services;
        }
    }
}
