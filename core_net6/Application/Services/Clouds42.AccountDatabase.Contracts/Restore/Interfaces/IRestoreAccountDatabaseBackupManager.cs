﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;

namespace Clouds42.AccountDatabase.Contracts.Restore.Interfaces
{
    public interface IRestoreAccountDatabaseBackupManager
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>
        /// <param name="restoreAccountDatabaseParams">Параметры восстановления инф. базы из бэкапа</param>
        /// <returns>Результат выполнения</returns>
        ManagerResult Restore(RestoreAccountDatabaseFromTombWorkerTaskParam restoreAccountDatabaseParams);

        /// <summary>
        /// Восстановить инф. базу из бэкапа после не успешной попытки АО
        /// </summary>
        /// <param name="restoreAccountDatabaseParams">Параметры восстановления инф. базы из бэкапа</param>
        /// <returns>Результат выполнения</returns>
        ManagerResult RestoreAfterFailedAutoUpdate(RestoreAccountDatabaseFromTombWorkerTaskParam restoreAccountDatabaseParams);
    }
}