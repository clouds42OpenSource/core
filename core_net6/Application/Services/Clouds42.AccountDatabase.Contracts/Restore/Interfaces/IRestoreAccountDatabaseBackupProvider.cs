﻿using Clouds42.DataContracts.CoreWorkerTasks.Parameters;

namespace Clouds42.AccountDatabase.Contracts.Restore.Interfaces
{
    /// <summary>
    /// Провайдер восстановления ИБ из бэкапа.
    /// </summary>
    public interface IRestoreAccountDatabaseBackupProvider
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>
        /// <param name="restoreAccountDatabaseParams">Параметры восстановления инф. базы из бэкапа</param>
        void Restore(RestoreAccountDatabaseFromTombWorkerTaskParam restoreAccountDatabaseParams);

        /// <summary>
        /// Восстановить инф. базу из бэкапа после не успешной попытки АО
        /// </summary>
        /// <param name="restoreAccountDatabaseParams">Параметры восстановления инф. базы из бэкапа</param>
        void RestoreAfterFailedAutoUpdate(RestoreAccountDatabaseFromTombWorkerTaskParam restoreAccountDatabaseParams);
    }
}