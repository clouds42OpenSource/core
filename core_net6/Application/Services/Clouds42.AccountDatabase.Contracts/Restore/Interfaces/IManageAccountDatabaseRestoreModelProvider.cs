﻿namespace Clouds42.AccountDatabase.Contracts.Restore.Interfaces
{
    /// <summary>
    /// Провайдер для управления моделью восстановления инф. баз
    /// </summary>
    public interface IManageAccountDatabaseRestoreModelProvider
    {
        /// <summary>
        /// Выполнить аудит и корректировку моделей восстановления инф. баз
        /// </summary>
        void ManageAccountDatabasesRestoreModel();
    }
}
