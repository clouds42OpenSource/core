﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Restore.Interfaces
{
    /// <summary>
    /// Провайдер для восстановления инф. базы
    /// </summary>
    public interface IRestoreServerAccountDatabaseProvider
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        void Restore(RestoreAccountDatabaseParamsDto model);
    }
}
