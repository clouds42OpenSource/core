﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Environment.Interfaces
{
    /// <summary>
    /// Провайдер функций проведения автообновление информационных баз.
    /// </summary>
    public interface IAutoUpdateAccountDatabasesProvider
    {
        /// <summary>
        /// Провести автообновление информационных баз.
        /// </summary>
        void ProcessAutoUpdate(List<AcDbSupport> accountDatabasesSupports);

        /// <summary>
        /// Ручное обнволение базы
        /// </summary>
        void UpdateDatabase(Guid accountDatabaseId);

        /// <summary>
        /// Получить соответствующую свободную ноду
        /// </summary>
        public AutoUpdateNode GetCorrespondenceFreeNode(AcDbSupport acDbSupport);


    }
}
