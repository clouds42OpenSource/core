﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Environment.Interfaces
{
    public interface IChangeDatabasesStateProvider
    {
        public void UpdateAccountDatabasesState(List<AcDbSupport> acDbSupports);
    }
}
