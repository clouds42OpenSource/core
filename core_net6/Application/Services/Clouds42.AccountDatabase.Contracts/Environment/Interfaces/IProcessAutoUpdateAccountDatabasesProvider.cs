﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Environment.Interfaces
{
    /// <summary>
    /// Провайдер функций проведения автообновление информационных баз.
    /// </summary>
    public interface IProcessAutoUpdateAccountDatabasesProvider
    {
        /// <summary>
        /// Обработать инф. базу
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <returns>Результат обработки</returns>
        AccountDatabaseAutoUpdateResultDto ProcessAccountDatabase(AcDbSupport acDbSupport);
    }
}