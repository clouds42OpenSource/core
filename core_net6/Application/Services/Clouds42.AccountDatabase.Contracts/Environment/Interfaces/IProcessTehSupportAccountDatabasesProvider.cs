﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Environment.Interfaces
{

    /// <summary>
    /// Провайдер функций проведения ТиИ для информационных баз.
    /// </summary>
    public interface IProcessTehSupportAccountDatabasesProvider
    {
        /// <summary>
        /// Провести ТиИ для информационных баз.
        /// </summary>
        /// <param name="accountDatabasesSupports">Спсок информационных баз</param>
        /// <returns>Признак успешности</returns>
        bool Process(List<AcDbSupport> accountDatabasesSupports);
    }
}