﻿using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountDatabase.Contracts.ManageService.Interfaces
{
    /// <summary>
    /// Провайдер для работы с состоянием расширения сервиса
    /// для информационной базы в МС
    /// </summary>
    public interface IServiceExtensionDatabaseStateProvider
    {
        /// <summary>
        /// Установить расширение сервиса
        /// для информационной базы в МС
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        void InstallServiceExtensionDatabase(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState);

        /// <summary>
        /// Удалить расширение сервиса
        /// из информационной базы в МС
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        void DeleteServiceExtensionDatabase(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState);
    }
}
