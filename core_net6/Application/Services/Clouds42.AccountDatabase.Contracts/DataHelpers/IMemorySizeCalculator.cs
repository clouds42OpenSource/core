﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.DataHelpers
{
    public interface IMemorySizeCalculator
    {
        int CalculateFile1CDataBase(Domain.DataModels.AccountDatabase dbInfo);
        DateTime CalculateLastActiveDataBase(Domain.DataModels.AccountDatabase dbInfo);
        long? CalculateCatalog(string catalogPath);
        long CalculateClintFilesSize(Account account, long?size);
        int CalculateSqlDataBase(string dbName);
    }
}
