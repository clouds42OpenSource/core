﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.Contracts.DataHelpers
{
    public interface IAccountDatabasePathHelper
    {
        /// <summary>
        /// Метод формирует потенциальный путь размещения ИБ после миграции
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetStorage">Хранилище, в которое нужно перенести базу</param>
        /// <returns>Потенциальный путь размещения ИБ</returns>
        string GetPath(Domain.DataModels.AccountDatabase accountDatabase, CloudServicesFileStorageServer targetStorage);

        /// <summary>
        /// Получить путь к ИБ
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Путь к ИБ</returns>
        string GetPath(Guid accountDatabaseId);

        /// <summary>
        /// Получить путь к ИБ
        /// </summary>
        /// <param name="database">ИБ аккаунта</param>
        /// <returns>путь к ИБ</returns>
        string GetPath(IAccountDatabase database);

        /// <summary>
        /// Получить строку подключения к серверной инф. базе
        /// </summary>
        /// <returns>Строка подключения к серверной инф. базе</returns>
        string GetServerDatabaseConnectionPath(Domain.DataModels.AccountDatabase accountDatabase);
    }
}