﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.DataHelpers
{
    public interface IDatabasePlatformVersionHelper
    {
        string? GetPathToModule1C(Domain.DataModels.AccountDatabase accountDatabase);
        string? CreatePathTo1CModuleForServerDb(Guid accountId, PlatformType platformType);
        string? CreatePathTo1CModuleForFileDb(Guid accountId, PlatformType platformType, DistributionType distributionType);
        bool HasAlpha83VersionInSegment(Guid accountId);
        string? GetAlpha83VersionFromSegment(Guid accountId);
        string? GetStable82VersionFromSegment(Guid accountId);
        string? GetStable83VersionFromSegment(Domain.DataModels.AccountDatabase accountDatabase);
        PlatformVersionReference GetPlatformVersion(Domain.DataModels.AccountDatabase accountDatabase, Guid? demoAccountId = null);
    }
}
