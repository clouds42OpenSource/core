﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.DataHelpers
{
    public interface IAccountDatabaseWebPublishPathHelper
    {
        string GetWebPublishPath(Domain.DataModels.AccountDatabase accountDatabase);
        string GetWebPublishPath(Guid databaseId);
        string GetPublishPathWithoutChecks(Domain.DataModels.AccountDatabase accountDatabase);

        DbTemplateDelimiters GetDbTemplateDelimiters(string dbTemplateDelimiterCode);
    }
}
