﻿namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель для информации - можно ли добавлять информационную базу
    /// </summary>
    public class AllowAddingDatabaseModel
    {
        /// <summary>
        /// Разрешено ли создать базу
        /// </summary>
        public bool IsAllowedCreateDb { get; set; }

        /// <summary>
        /// Сообщение о заперете
        /// </summary>
        public string ForbiddeningReasonMessage { get; set; }
    }
}
