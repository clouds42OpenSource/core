﻿namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    ///     Модель для частичной подгрузки списка бекапов
    /// </summary>
    public class BackupsChunkModel
    {
        /// <summary>
        ///     Идентификатор базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        ///     Количество элементов которые нужно пропустить
        /// </summary>
        public int SkipCount { get; set; }

        /// <summary>
        ///     Количество элементов которые нужно взять из БД
        /// </summary>
        public const int TakeCount = 4;
    }
}
