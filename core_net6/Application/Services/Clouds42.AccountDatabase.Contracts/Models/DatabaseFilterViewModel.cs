﻿using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель параметров для фильтрации информационных баз
    /// </summary>
    public class DatabaseFilterViewModel
    {
        /// <summary> 
        /// Индекс элемента с которого начинать выборку в БД
        /// </summary>
        public int Index { get; set; } = 0;

        /// <summary>
        /// Индекс элемента с которого начинать выборку в БД
        /// для доступных внешних баз
        /// </summary>
        public int SharedDbsIndex { get; set; } = 0;

        /// <summary> 
        /// Строка поиска в БД
        /// </summary>
        public string SearchQuery { get; set; }

        /// <summary>
        /// Использовать индекс элемента как количество баз
        /// </summary>
        public bool UseIndexAsCount { get; set; }

        /// <summary> 
        /// Перечисление для указания к каким таблицам применять фильтр 
        /// </summary>
        public FilterInfluenceType InfluenceType { get; set; } = FilterInfluenceType.All;

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Поле, по которому нужно применить сортировку
        /// </summary>
        public string OrderBy { get; set; }

        /// <summary>
        /// Направление сортировки
        /// </summary>
        public string SortDirection { get; set; }
    }
}
