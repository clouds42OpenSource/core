﻿namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Шаблон базы
    /// </summary>
    public class AccountDatabaseTemplate
    {
        /// <summary>
        /// Id шаблона
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дефолтное название
        /// </summary>
        public string DefaultCaption { get; set; }
    }
}
