﻿using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель видимости и справочников для карточки информационной базы в режиме редактирования
    /// </summary>
    public class AccountDatabaseEditModeDictionariesDto
    {
        /// <summary>
        /// Платформы 1С
        /// </summary>
        public PlatformType[] AvailablePlatformTypes { get; set; }

        /// <summary>
        /// Типы распространения базы
        /// </summary>
        public KeyValuePair<DistributionType, string>[] AvailableDestributionTypes { get; set; }

        /// <summary>
        /// Состояния базы
        /// </summary>
        public DatabaseState[] AvailableDatabaseStates { get; set; }

        /// <summary>
        /// Файловые хранилища
        /// </summary>
        public KeyValuePair<Guid, string>[] AvailableFileStorages { get; set; }

        /// <summary>
        /// Шаблоны баз
        /// </summary>
        public KeyValuePair<Guid, string>[] AvailableDatabaseTemplates{ get; set; }
    }
}