﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель информационной базы с информацией об АО и ТИИ
    /// </summary>
    public class DatabaseAndSupportModel
    {
        /// <summary>
        /// Информационная база
        /// </summary>
        public Domain.DataModels.AccountDatabase Database { get; set; }

        /// <summary>
        /// Информация об АО и ТИИ базы
        /// </summary>
        public AcDbSupport Support { get; set; }
    }
}
