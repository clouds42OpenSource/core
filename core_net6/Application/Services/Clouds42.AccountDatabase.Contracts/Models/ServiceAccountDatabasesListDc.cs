﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Сервис со списком информационных баз
    /// </summary>
    public class ServiceAccountDatabasesListDc
    {
        /// <summary>
        /// Информационные базы
        /// </summary>
        public List<AccountDatabaseDc> Databases { get; set; }

        /// <summary>
        /// Доступные информационные базы других аккаунтов
        /// </summary>
        public List<AccountDatabaseDc> SharedDatabases { get; set; }

        /// <summary>
        /// Сервис доступен
        /// </summary>
        public bool IsServiceAllowed { get; set; }

        /// <summary>
        /// Информация заблокирован сервис или нет
        /// </summary>
	    public ServiceStatusModelDto ServiceStatus { get; set; }

        /// <summary>
        /// Имеет разрешение для Rdp подключения
        /// </summary>
		public bool HasPermissionForRdp { get; set; }

        /// <summary>
        /// Имеет разрешение для Web подключения
        /// </summary>
        public bool HasPermissionForWeb { get; set; }

        /// <summary>
        /// Имеет разрешение на редактирование данных поддержки
        /// </summary>
        public bool HasPermissionForEditSupportData { get; set; }

        /// <summary>
        /// Имеет разрешение на просмотр данных поддержки
        /// </summary>
        public bool HasPermissionForDisplaySupportData { get; set; }
        
        /// <summary>
        /// Информация об админе аккаунта
        /// </summary>
	    public string AccountAdminInfo { get; set; }

        /// <summary>
        /// Количество информационных баз
        /// </summary>
        public int DatabasesCount { get; set; }

        /// <summary>
        /// Количество информационных базы других аккаунтов
        /// </summary>
        public int SharedDatabasesCount { get; set; }

        /// <summary>
        /// Разрешение на добавление информационной базы
        /// </summary>
        public AllowAddingDatabaseModel AllowAddingDatabaseModel { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; } = 1;
    }
}