﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    public class CreateDatabaseResult
    {
        public IAccountDatabase AccountDatabase { get; set; }
        public InfoDatabaseDomainModelDto InfoDatabase { get; set; }
        public IDbTemplate Template { get; set; }
    }
}
