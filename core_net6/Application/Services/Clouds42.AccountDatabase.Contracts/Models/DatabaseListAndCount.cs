﻿namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель списка и общего количества информационных баз
    /// </summary>
    public class DatabaseListAndCount
    {
        /// <summary>
        /// Список баз
        /// </summary>
        public List<AccountDatabaseDc> DatabaseList { get; set; }

        /// <summary>
        /// Общее количество баз. Может быть больше чем <see cref="DatabaseList"/>
        /// </summary>
        public int Count { get; set; }
    }
}
