﻿namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Бекап информационной базы
    /// </summary>
    public class AccountDatabaseBackupsDc
    {
        /// <summary>
        /// Id бекапа
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Инициатор
        /// </summary>
        public string Initiator { get; set; }

        /// <summary>
        /// Дата создания бекапа
        /// </summary>
        public string CreateDateTime { get; set; }

        /// <summary>
        /// Путь к бекапу
        /// </summary>
        public string BackupPath { get; set; }

        /// <summary>
        /// Тригер события
        /// </summary>
        public string EventTrigger { get; set; }

        /// <summary>
        /// База на разделителях
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }
    }
}
