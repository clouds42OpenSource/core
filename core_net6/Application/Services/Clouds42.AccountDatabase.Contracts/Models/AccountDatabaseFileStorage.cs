﻿namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Файловое хранилище информационной базы
    /// </summary>
    public class AccountDatabaseFileStorage
    {
        /// <summary>
        /// Id хранилища базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название хранилища
        /// </summary>
        public string Name { get; set; }
    }
}
