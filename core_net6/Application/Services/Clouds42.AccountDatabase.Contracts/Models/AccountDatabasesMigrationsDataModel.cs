﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель данных для миграции информационных баз
    /// </summary>
    public class AccountDatabasesMigrationsDataModel
    {
        /// <summary>
        /// Количество ИБ
        /// </summary>
        public int CountDatabases { get; set; }
        /// <summary>
        /// Список хранилищ с информацией о размерах и количествах баз в них
        /// </summary>
        public List<FileStorageAccountDatabasesSummaryDto> FileStorageAccountDatabasesSummury { get; set; } = [];

        /// <summary>
        /// Список ИБ с информацией о имени, ее месте нахождения, размере и последней дате запуска
        /// </summary>
        public List<AccountDatabaseMigrationInfoModel> AccountDatabases { get; set; } = [];

        /// <summary>
        /// Нумирация страниц
        /// </summary>
        public PaginationBaseDto Pagination { get; set; } = new(0, 0, 0);

        /// <summary>
        /// Список номеров выбранных баз для миграции
        /// </summary>
        public IEnumerable<string> SelectedDatabases { get; set; } = new List<string>();

        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
        /// <summary>
        /// Список содержащий информацию о ID информационной базы и имени хранилища для миграции
        /// </summary>
        public List<SelectAccountDatabasesToMigration> SelectAccountDatabasesToMigration { get; set; } = [];

        /// <summary>
        /// Список содержащий информацию о пути хранилища для abkmnhfwbbи
        /// </summary>
        public List<ListAccountDatabasesPath> ListAccountDatabasesPath { get; set; } = [];

        /// <summary>
        /// Список содержащий информацию о типах хранилища для фильтрации
        /// </summary>
        public TypeStorage TypeStorage { get; set; }
    }
     

    /// <summary>
    /// Список содержащий информацию о ID информационной базы и имени хранилища для миграции
    /// </summary>
    public class SelectAccountDatabasesToMigration
    {
        /// <summary>
        /// ID хранилища
        /// </summary>
        public Guid FileStorageId { get; set; }
        /// <summary>
        /// Имя хранилища
        /// </summary>
        public string FileStorageName { get; set; }
    }


    /// <summary>
    /// Информацию о пути хранилища для фильтра
    /// </summary>
    public class ListAccountDatabasesPath
    {
        /// <summary>
        /// ID хранилища
        /// </summary>
        public string PathStorageId { get; set; }
        /// <summary>
        /// Имя хранилища
        /// </summary>
        public string PathStorageName { get; set; }
    }


    /// <summary>
    /// Список ИБ с информацией о имени, ее месте нахождения, размере и последней дате запуска
    /// </summary>
    public class AccountDatabaseMigrationInfoModel
    {
        /// <summary>
        /// ID базы аккаунта
        /// </summary>
        public Guid AccountDatabaseId { get; set; }
        /// <summary>
        /// Номер информационной базы
        /// </summary>
        public string V82Name { get; set; }
        /// <summary>
        /// Путь по которому располагается ИБ
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Занимаемый размер ИБ
        /// </summary>
        public int Size { get; set; }
        /// <summary>
        /// Статус ИБ
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Web база
        /// </summary>
        public bool IsPublishDatabase { get; set; }
        /// <summary>
        /// Web сервис
        /// </summary>
        public bool IsPublishServices { get; set; }
        /// <summary>
        /// Выставляется тип информационной базы Ф или С
        /// </summary>
        public bool IsFile { get; set; }
        /// <summary>
        /// Крайняя дата запуска
        /// </summary>
        public DateTime LastTimeActivity { get; set; }
    }


    /// <summary>
    /// Содержит список параметров нужных для получения списка ИБ
    /// </summary>
    public class ParametersAccountDatabasesMigrationsData
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }   
        /// <summary>
        /// Номер пагинированной страницы
        /// </summary>
        public int Page { get; set; } = 1;
        /// <summary>
        /// Список номеров выбранных ИБ для миграции
        /// </summary>                              
        public string SelectedDatabases { get; set; } = string.Empty;
        /// <summary>
        /// Действие которое выполнится со списком номеров ИБ для миграции(add, removed)
        /// </summary>
        public string ActionForSelected { get; set; } = string.Empty;
        /// <summary>
        /// //Номер ИБ
        /// </summary>
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// Ограничение количества баз для миграции
        /// </summary>
        public int Limit { get; set; } = 0;
        /// <summary>
        /// Тип сортировки по убыванию или возрастанию
        /// </summary>                   
        public SortType SortType { get; set; }
        /// <summary>
        /// Имя сортируемой колонки
        /// </summary>
        public string SortDir { get; set; } = string.Empty;
        /// <summary>
        /// Критерий поиска
        /// </summary>
        public string Search { get; set; } = string.Empty;
        /// <summary>
        /// Введенное слово для фильтрации по номеру
        /// </summary>
        public string FilterNumber { get; set; } = string.Empty;
        /// <summary>
        /// Введенное слово для фильтрации по пути
        /// </summary>
        public string FilterPath { get; set; } = string.Empty;
        /// <summary>
        /// Выбранное хранилище
        /// </summary>
        public string FilterPathDrop { get; set; } = string.Empty;
        /// <summary>
        /// Введенное слово для фильтрации по типу
        /// </summary>
        public TypeStorage FilterType { get; set; } = 0;
    }

}
