﻿namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель для сохранения результатов с админ панели в карточке ИБ
    /// </summary>
    public class SaveResult
    {
        public List<Guid> DeletedAccessUserIds { get; set; } = [];
        public List<Guid> GrandAccessUserIds { get; set; } = [];
    }
}
