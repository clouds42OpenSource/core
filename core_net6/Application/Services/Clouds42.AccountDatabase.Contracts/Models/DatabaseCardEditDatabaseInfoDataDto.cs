﻿using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель для редактирования данных базы данных
    /// </summary>
    public class DatabaseCardEditDatabaseInfoDataDto
    {
        /// <summary>
        /// Id базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public string V82Name { get; set; }
        
        /// <summary>
        /// Наименование базы
        /// </summary>
        public string DatabaseCaption { get; set; }

        /// <summary>
        /// Шаблон базы
        /// </summary>
        public Guid DatabaseTemplateId { get; set; }
        
        /// <summary>
        /// Тип платформы
        /// </summary>
        public PlatformType PlatformType { get; set; }
        
        /// <summary>
        /// Дистрибуция, версия
        /// </summary>
        public DistributionType DistributionType { get; set; }
        
        /// <summary>
        /// Маркер о том что опубликованы веб сервисы
        /// </summary>
        public bool UsedWebServices { get; set; }

        /// <summary>
        /// Состояние базы
        /// </summary>
        public DatabaseState DatabaseState { get; set; }
        
        /// <summary>
        /// Файловое хранилище
        /// </summary>
        public Guid? FileStorageId { get; set; }
    }
}
