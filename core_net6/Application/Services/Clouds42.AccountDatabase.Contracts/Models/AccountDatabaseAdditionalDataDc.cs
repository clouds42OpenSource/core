﻿using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Дополнительные данные
    /// по информационной базе
    /// </summary>
    public class AccountDatabaseAdditionalDataDc : AccountDatabaseDc
    {
        /// <summary>
        /// База является внешной
        /// </summary>
        public bool IsExternalDb { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата бэкапа
        /// </summary>
        public DateTime? BackupDate { get; set; }

        /// <summary>
        /// Дата бэкапа(строка)
        /// </summary>
        public string BackupDateString { get; set; }

        /// <summary>
        /// Существует путь к бекапу
        /// </summary>
        public bool ExistBackUpPath { get; set; }

        /// <summary>
        /// Дата пересчета размера базы
        /// </summary>
        public DateTime? CalculateSizeDateTime { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        public PlatformType TemplatePlatform { get; set; }

        /// <summary>
        /// Тип распространения
        /// </summary>
        public DistributionType DistributionType { get; set; }

        /// <summary>
        /// Стабильная версия 82
        /// </summary>
        public string Stable82Version { get; set; }

        /// <summary>
        /// Альфа версия 83
        /// </summary>
        public string Alpha83Version { get; set; }

        /// <summary>
        /// Стабильная версия 83
        /// </summary>
        public string Stable83Version { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public int DbNumber { get; set; }

        /// <summary>
        /// База файловая
        /// </summary>
        public bool? IsFile { get; set; }

        /// <summary>
        /// Путь к файлу
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// База готова
        /// </summary>
        public bool IsReady { get; set; }

        /// <summary>
        /// Шаблон базы
        /// </summary>
        public string DbTemplate { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Сервер 82
        /// </summary>
        public string V82Server { get; set; }

        /// <summary>
        /// Sql сервер
        /// </summary>
        public string SqlServer { get; set; }

        /// <summary>
        /// Путь к архиву
        /// </summary>
        public string ArchivePath { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Заблокированное состояние
        /// </summary>
        public string LockedState { get; set; }

        /// <summary>
        /// Тип платформы
        /// </summary>
        public PlatformType PlatformType { get; set; }

        /// <summary>
        /// Возможность вэб публикации
        /// </summary>
        public bool CanWebPublish { get; set; }

        /// <summary>
        /// Статус базы
        /// </summary>
        public DatabaseState DatabaseState { get; set; }

        /// <summary>
        /// Использование вэб сервиса
        /// </summary>
        public bool UsedWebServices { get; set; }

        /// <summary>
        /// Веб-ссылка на облачное хранилище
        /// </summary>
        public string CloudStorageWebLink { get; set; }

        /// <summary>
        /// Id файлового хранилища
        /// </summary>
        public Guid? FileStorage { get; set; }

        /// <summary>
        /// Тип модели восстановления
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum? RestoreModelType { get; set; }

        /// <summary>
        /// Признак что есть возможность сменить модель восстановления
        /// </summary>
        public bool CanChangeRestoreModel { get; set; }

        /// <summary>
        /// Наименование файлового хранилища
        /// </summary>
        public string FileStorageName { get; set; }

        /// <summary>
        /// Id актуального бекапа информационной базы
        /// </summary>
        public Guid? ActualDatabaseBackupId { get; set; }
    }
}
