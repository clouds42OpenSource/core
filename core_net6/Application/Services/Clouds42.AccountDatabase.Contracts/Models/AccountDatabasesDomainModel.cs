﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.Models
{

    public class AccountDatabasesDomainModel
    {
        public List<DbTemplateDomainModel> AvailableTemplates { get; set; }
        public bool ServiceIsAllow { get; set; }                
        public string ErrorMessage { get; set; }
	    public List<DbTemplateDelimiters> DbTemplateDelimiters { get; set; }
	}

    public class DbTemplateDomainModel
    {
        public Guid Id { get; set; }
        public string Caption { get; set; }
        public bool DemoIsAvailable { get; set; }
        public bool CanWebPublish { get; set; }
	    public bool DbTemplateDelimiters { get; set; }
        public TagsDemoDataInDatabaseOnDelimiters TagsDemoDataInDatabaseOnDelimiters { get; set; }
        public string TemplateImgUrl { get; set; }
        public string VersionTemplate { get; set; }
    }

    /// <summary>
    /// Признаки демо данных в базах на разделителях
    /// </summary>
    public class TagsDemoDataInDatabaseOnDelimiters
    {
        /// <summary>
        /// Наличие базы на разделителях с демо данными
        /// </summary>
        public bool HasDbOnDelimitersWithDemoData { get; set; }

        /// <summary>
        /// Путь к опубликованной базе
        /// </summary>
        public string WebPublishPath { get; set; }
    }

    /// <summary>
    /// Карточка Информационной базы
    /// </summary>
    public class AccountDatabaseDomainModel
    {
        /// <summary>
        /// Списиок шаблонов для ИБ
        /// </summary>
        public List<KeyValuePair<string, string>> DbTemplateList { set; get; }
        /// <summary>
        /// ID ИБ
        /// </summary>
        public Guid AccountDatabaseId { get; set; }
        /// <summary>
        /// Номер ИБ
        /// </summary>
        public string V82Name { get; set; }
        /// <summary>
        /// Внутренняя или внешняя ИБ
        /// </summary>
        public bool IsExternalDb { get; set; }
        /// <summary>
        /// Наименование ИБ
        /// </summary>
        [Required(ErrorMessage = "Укажите название базы")]
        public string Caption { get; set; }
        /// <summary>
        /// Дата создания ИБ
        /// </summary>
        public DateTime CreationDate { get; set; }
        /// <summary>
        /// Дата последней активации ИБ
        /// </summary>
        public DateTime LastActivityDate { get; set; }
        /// <summary>
        /// Дата последнего изменения
        /// </summary>
        public string LastActivityDateDescription { get; set; }
        /// <summary>
        /// Дата Бекапа
        /// </summary>
        public DateTime? BackupDate { get; set; }
        /// <summary>
        /// Состояние базы
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Cостояние ИБ
        /// </summary>
        private DatabaseState _databaseState;
        public DatabaseState DatabaseState
        {
            get
            {
                if (_databaseState == DatabaseState.Undefined && Enum.TryParse(State, out DatabaseState res))
                {
                    _databaseState = res;
                }

                return _databaseState;
            }
            set { _databaseState = value; }
        }
        /// <summary>
        /// Размер ИБ
        /// </summary>
        public int SizeInMB { get; set; }
        /// <summary>
        /// Дата последнего подсчета размера ИБ
        /// </summary>
        public DateTime? CalculateSizeDateTime { get; set; }
        /// <summary>
        /// Имя шаблона ИБ
        /// </summary>
        public string TemplateName { get; set; }
        /// <summary>
        /// Тип платформы ИБ
        /// </summary>
        public PlatformType TemplatePlatform { get; set; }
        /// <summary>
        /// Номер базы данных
        /// </summary>
        public int DbNumber { get; set; }
        /// <summary>
        /// URL картинки ИБ
        /// </summary>
        public string TemplateImgUrl { get; set; }
        /// <summary>
        /// Тип ИБ файловая или серверная ИБ
        /// </summary>
        public bool? IsFile { get; set; }
        /// <summary>
        /// Путь для ИБ
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// Публичная база или нет
        /// </summary>
        public PublishState PublishState { get; set; }
        /// <summary>
        /// URL публикации
        /// </summary>
        public string WebPublishPath { get; set; }
        /// <summary>
        /// Состояние базы готова ли она для изменений
        /// </summary>
        public bool IsReady { get; set; }
        /// <summary>
        /// Название шаблона
        /// </summary>
        public string DbTemplate { get; set; }
        /// <summary>
        /// Имя аккаунта
        /// </summary>
        public string AccountCaption { get; set; }
        /// <summary>
        /// Имя сервера для платформы
        /// </summary>
        public string V82Server { get; set; }
        /// <summary>
        /// Путь к Архиву
        /// </summary>
        public string ArchivePath { get; set; }
        /// <summary>
        /// Имя сервера
        /// </summary>
        public string ServiceName { get; set; }
        /// <summary>
        /// Состояние базы например заблокирована
        /// </summary>
        public string LockedState { get; set; }
        /// <summary>
        /// Тип платформы
        /// </summary>
        [Required(ErrorMessage = "Укажите версию платформы")]
        public PlatformType PlatformType { get; set; }
        /// <summary>
        /// Значение чекбокса для меню "Опубликованы веб сервисы" 
        /// </summary>
        public bool UsedWebServices { get; set; }
        /// <summary>
        /// Использовать веб публикацию или нет
        /// </summary>
        public bool CanWebPublish { get; set; }

        /// <summary>
        /// Список содержащий информацию о ID информационной базы и имени хранилища для миграции
        /// </summary>
        public List<SelectAccountDatabasesToMigration> SelectAccountDatabasesToMigration { get; set; }
        /// <summary>
        /// Текущее ID хранилище для выбранной ИБ
        /// </summary>
        public Guid? FileStorageIdAccountDatabases { get; set; }
    }

    /// <summary>
    /// Десереализованный объект с модального окна (карточка Информационной базы)
    /// </summary>
    public class AccountDatabaseCartModel
    {
        /// <summary>
        /// Данные с карточки ИБ
        /// </summary>
        public AccountDatabaseDomainModel Infos { set; get; }
        /// <summary>
        /// Id внутреннего пользователя которому предоставили доступ к ИБ
        /// </summary>
        public List<Guid> InternalAccessUserIds { set; get; }
        /// <summary>
        /// Id внешнего пользователя которому предоставили доступ к ИБ
        /// </summary>
        public List<Guid> ExternalAccesssUserIds { set; get; }
        /// <summary>
        /// Предоставление настроек доступа
        /// </summary>
        public bool AccessNeedSave { set; get; }
    }

}
