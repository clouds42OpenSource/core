﻿namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель данных для карточки базы
    /// </summary>
    public class AccountDatabaseCartDomainModel
    {
        /// <summary>
        /// Дополнительные данные
        /// по информационной базе
        /// </summary>
        public AccountDatabaseAdditionalDataDc Infos { set; get; }

        /// <summary>
        /// Список идентификаторов внутренних пользователей
        /// </summary>
        public List<Guid> InternalAccessUserIds { set; get; }

        /// <summary>
        /// Список идентификаторов внешних пользователей
        /// </summary>
        public List<Guid> ExternalAccesssUserIds { set; get; }

        /// <summary>
        /// Доступ нужно сохранить
        /// </summary>
        public bool AccessNeedSave { set; get; }

        /// <summary>
        /// Создаёт объект <see cref="AccountDatabaseCartDomainModel"/> от <see cref="DatabaseCardEditDatabaseInfoDataDto"/>
        /// </summary>
        /// <param name="model">Объект от которой создать <see cref="AccountDatabaseCartDomainModel"/></param>
        /// <returns>Возвращает объект <see cref="AccountDatabaseCartDomainModel"/></returns>
        public static AccountDatabaseCartDomainModel CreateFrom(DatabaseCardEditDatabaseInfoDataDto model)
        {
            return new AccountDatabaseCartDomainModel
            {
                Infos = new AccountDatabaseAdditionalDataDc
                {
                    Id = model.DatabaseId,
                    V82Name = model.V82Name,
                    DbTemplate = model.DatabaseTemplateId.ToString(),
                    DistributionType = model.DistributionType,
                    UsedWebServices = model.UsedWebServices,
                    FileStorage = model.FileStorageId,
                    DatabaseState = model.DatabaseState,
                    PlatformType = model.PlatformType,
                    Caption = model.DatabaseCaption
                }
            };
        }
    }
}
