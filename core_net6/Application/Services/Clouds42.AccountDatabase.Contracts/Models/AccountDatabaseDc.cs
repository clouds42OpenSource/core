﻿using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Модель отображения информационной базы
    /// </summary>
    public class AccountDatabaseDc
    {

        /// <summary>
        /// Id базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Размер базы (МБ)
        /// </summary>
        public int SizeInMB { get; set; }

        /// <summary>
        /// Статус базы
        /// </summary>
        public DatabaseState State { get; set; }

        /// <summary>
        /// Комментарий по созданию базы
        /// </summary>
        public string CreateAccountDatabaseComment { get; set; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// Ссылка картинки шаблона
        /// </summary>
        public string TemplateImgUrl { get; set; }

        /// <summary>
        /// Статус публикации
        /// </summary>
        public PublishState PublishState { get; set; }

        /// <summary>
        /// Ссылка веб публикации
        /// </summary>
        public string WebPublishPath { get; set; }

        /// <summary>
        /// Признак необходимости показывать ссылку
        /// </summary>
        public bool NeedShowWebLink { get; set; }

        /// <summary>
        /// Дата последней активности
        /// </summary>
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Временная зона
        /// </summary>
        public string TimezoneName { get; set; }

        /// <summary>
        /// База удалена
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// База на разделителях
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// Признак что база демо на разделителях
        /// </summary>
        public bool IsDemoDelimiters { get; set; }

        /// <summary>
        /// Номер области (если база на разделителях)
        /// </summary>
        public int? ZoneNumber { get; set; }

        /// <summary>
        /// Путь подключения к инф. базе
        /// </summary>
        public string DatabaseConnectionPath { get; set; }
    }
}
