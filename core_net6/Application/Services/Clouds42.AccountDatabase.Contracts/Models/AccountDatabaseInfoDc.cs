﻿using Clouds42.DataContracts.AccountUser;

namespace Clouds42.AccountDatabase.Contracts.Models
{
    /// <summary>
    /// Информация информационной базы
    /// </summary>
    public class AccountDatabaseInfoDc
    {
        /// <summary>
        /// Дополнительные данные по информационной базе
        /// </summary>
        public AccountDatabaseAdditionalDataDc Database { get; set; }

        /// <summary>
        /// Возвращает <c>true</c>, если текущий аутентифицирующий пользователь имеет права редактировать информационную базу <see cref="Database"/>, иначе <c>false</c>
        /// </summary>
        public bool CanEditDatabase { get; set; }

        /// <summary>
        /// Пользователи с внешним доступом
        /// </summary>
        public List<AccessUserInfoDc> ExternalAccessUsers { get; set; }

        /// <summary>
        /// Пользователи с внутренним доступом
        /// </summary>
        public List<AccessUserInfoDc> InternalAccessUsers { get; set; }

        /// <summary>
        /// Бэкапы информационной базы
        /// </summary>
        public List<AccountDatabaseBackupsDc> AccountDatabaseBackups { get; set; }

        /// <summary>
        /// Количество бэкапов
        /// </summary>
        public int DbBackupsCount { get; set; }

        /// <summary>
        /// Доступные хранилища файлов
        /// </summary>
        public List<AccountDatabaseFileStorage> AvailableFileStorages { get; set; }

        /// <summary>
        /// Список шаблонов
        /// </summary>
        public List<AccountDatabaseTemplate> DbTemplateList { get; set; }
    }
}
