﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;

namespace Clouds42.AccountDatabase.Contracts._1C.Interfaces
{
    /// <summary>
    /// Провайдер ком коннектора 1С
    /// </summary>
    public interface IConnector1CProvider
    {
        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>
        /// <param name="database">Модель данных обновления базы</param>
        /// <returns>Метаданные информационной базы</returns>     
        ConnectorResultDto<MetadataResultDto> GetMetadataInfo(IUpdateDatabaseDto database);

        /// <summary>
        /// Принять полученные обновления информационной базы.
        /// </summary>
        /// <param name="database">Модель данных обновления базы</param>
        void ApplyUpdates(IUpdateDatabaseDto database);
    }
}
