﻿using Clouds42.DataContracts.Configurations1c;

namespace Clouds42.AccountDatabase.Contracts._1C.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными конфигураций 1С
    /// </summary>
    public interface IConfigurations1CDataProvider
    {
        /// <summary>
        ///  Получить конфигурацию
        /// </summary>
        /// <param name="name">Название</param>
        /// <returns>Конфигурация</returns>
        ConfigurationDetailsModelDto GetConfiguration(string name);

        /// <summary>
        /// Получить названия конфигураций 1С
        /// </summary>
        /// <returns>Список названий</returns>
        List<string> GetConfigurationNames();

        /// <summary>
        ///     Добавить конфигурацию 1С
        /// </summary>
        /// <param name="configurationToAdd">Модель свойств элемента конфигурации 1С для обновления или добавления</param>
        /// <returns>Данные после добавления</returns>
        AddOrUpdateConfiguration1CResultDto AddConfiguration1C(AddOrUpdateConfiguration1CDataItemDto configurationToAdd);


        /// <summary>
        ///     Удалить существующую конфигурацию
        /// </summary>
        /// <param name="args">Аргументы для удаления конфигурации</param>
        void DeleteConfiguration1C(DeleteConfiguration1CDataItemDto args);
    }
}
