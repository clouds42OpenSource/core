﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;

namespace Clouds42.AccountDatabase.Contracts.Card.Interfaces
{
    /// <summary>
    /// Провайдер для работы с общими/основными данными
    /// карточки информационной базы
    /// </summary>
    public interface IDatabaseCardGeneralDataProvider
    {
        /// <summary>
        /// Получить модель общих/основных данных
        /// карточки информационной базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель общих/основных данных
        /// карточки информационной базы</returns>
        DatabaseCardGeneralDataDto GetData(Guid databaseId);
    }
}
