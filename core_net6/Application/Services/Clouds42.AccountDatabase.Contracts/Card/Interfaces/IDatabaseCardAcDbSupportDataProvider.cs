﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Card.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными технической поддержки инф. базы
    /// (карточка информационной базы)
    /// </summary>
    public interface IDatabaseCardAcDbSupportDataProvider
    {
        /// <summary>
        /// Получить данные технической поддержки инф. базы
        /// для карточки информационной базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель данных технической поддержки инф. базы</returns>
        DatabaseCardAcDbSupportDataDto GetData(Guid databaseId);

        /// <summary>
        /// Обновить авторизационные данные в базу
        /// </summary>
        /// <returns></returns>
        public AcDbSupport UpdateAuthorizationData(Guid accountDatabaseId, string login, string password);
    }
}
