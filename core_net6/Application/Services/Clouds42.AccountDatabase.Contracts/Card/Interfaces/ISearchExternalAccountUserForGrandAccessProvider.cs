﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;

namespace Clouds42.AccountDatabase.Contracts.Card.Interfaces
{
    /// <summary>
    /// Провайдер поиска внешнего пользователя для предоставления доступа
    /// </summary>
    public interface ISearchExternalAccountUserForGrandAccessProvider
    {
        /// <summary>
        /// Найти внешнего пользователя для предоставления доступа
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserEmail">Почта пользователя</param>
        /// <returns>Модель пользователя для предоставления доступа</returns>
        ExternalAccountUserDataForGrandAccessDto Search(Guid databaseId, string accountUserEmail);
    }
}
