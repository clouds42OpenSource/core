﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;

namespace Clouds42.AccountDatabase.Contracts.Card.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными доступов (к инф. базе)
    /// карточки инф. базы
    /// </summary>
    public interface IDatabaseCardAcDbAccessesDataProvider
    {
        /// <summary>
        /// Получить данные доступов (к инф. базе)
        /// карточки пользователя
        /// </summary>
        /// <param name="userId">идентификатор пользователя</param>
        /// <returns>Модель данных доступов (к базе)
        /// карточки инф. базы</returns>
        UserCardAcDbAccessesDataDto GetDataForExternalUserCard(Guid userId, Guid accountId);
    }
}
