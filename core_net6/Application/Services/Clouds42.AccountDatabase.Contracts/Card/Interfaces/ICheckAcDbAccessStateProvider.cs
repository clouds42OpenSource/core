﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;

namespace Clouds42.AccountDatabase.Contracts.Card.Interfaces
{
    /// <summary>
    /// Провайдер для проверки статусов доступов к базам
    /// </summary>
    public interface ICheckAcDbAccessStateProvider
    {
        /// <summary>
        /// Получить статусы доступов
        /// </summary>
        /// <param name="acDbAccessesForCheck">Доступы для проверки</param>
        /// <returns>Статусы доступов</returns>
        List<CheckAcDbAccessDto> GetAcDbAccessesStates(List<CheckAcDbAccessDto> acDbAccessesForCheck);
    }
}
