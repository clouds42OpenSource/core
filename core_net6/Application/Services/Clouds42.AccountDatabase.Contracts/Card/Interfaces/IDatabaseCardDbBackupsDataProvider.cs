﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;

namespace Clouds42.AccountDatabase.Contracts.Card.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными бэкапов (базы)
    /// карточки инф. базы
    /// </summary>
    public interface IDatabaseCardDbBackupsDataProvider
    {
        /// <summary>
        /// Получить данные бэкапов (базы)
        /// карточки инф. базы по фильтру
        /// </summary>
        /// <param name="filter">Модель фильтра</param>
        /// <returns>Модель данных бэкапов
        /// карточки инф. базы</returns>
        DatabaseCardDbBackupsDataDto GetDataByFilter(DatabaseCardDbBackupsFilterDto filter);
    }
}
