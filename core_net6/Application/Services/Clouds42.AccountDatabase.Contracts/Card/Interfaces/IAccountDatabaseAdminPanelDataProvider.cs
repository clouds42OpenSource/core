﻿using Clouds42.AccountDatabase.Contracts.Models;

namespace Clouds42.AccountDatabase.Contracts.Card.Interfaces
{
    /// <summary>
    /// Провайдер получения данных информационной базы для админ панели
    /// </summary>
    public interface IAccountDatabaseAdminPanelDataProvider
    {
        /// <summary>
        /// Получить данные
        /// </summary>
        /// <param name="database">Модель информационной базы</param>
        /// <returns>Информация об информационной базы</returns>
        AccountDatabaseInfoDc GetData(Domain.DataModels.AccountDatabase database);
    }
}
