﻿namespace Clouds42.AccountDatabase.Contracts.Delete.Interfaces
{
    /// <summary>
    /// Провайдер для удаления данных неактивных аккаунтов
    /// </summary>
    public interface IDeleteInactiveAccountDataProvider
    {
        /// <summary>
        /// Удалить данные неактивных аккаунтов
        /// </summary>
        void DeleteInactiveAccountData();
    }
}
