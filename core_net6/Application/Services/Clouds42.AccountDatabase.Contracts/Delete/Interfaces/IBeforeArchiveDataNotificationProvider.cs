﻿namespace Clouds42.AccountDatabase.Contracts.Delete.Interfaces
{
    /// <summary>
    /// Провайдер уведомления пользователей о скором удалении данных
    /// </summary>
    public interface IBeforeArchiveDataNotificationProvider
    {
        /// <summary>
        /// Отправить уведомления
        /// </summary>
        void SendNotifications();
    }
}
