﻿namespace Clouds42.AccountDatabase.Contracts.Delete.Interfaces
{
    /// <summary>
    /// Провайдер для удаления бекапов после удаления баз данных
    /// </summary>
    public interface IBeforeDeleteAccountDatabaseProvider
    {
        /// <summary>
        /// Удалить бекапы после удаления баз данных
        /// </summary>
        void DeleteBackupAccountDatabase();
    }
}
