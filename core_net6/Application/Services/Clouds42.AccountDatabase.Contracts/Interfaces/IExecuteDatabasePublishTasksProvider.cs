﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    ///     Провайдер создания и выполнения тасок по публикации базы
    /// </summary>
    public interface IExecuteDatabasePublishTasksProvider
    {
        /// <summary>
        ///     Создать и выполнить таску по переопубликации базы
        /// </summary>
        /// <param name="database">Идентификатор базы</param>
        IWorkerTask CreateAndStartRepublishTask(Domain.DataModels.AccountDatabase database);

        /// <summary>
        ///     Создать и выполнить таску по публикации базы
        /// </summary>
        /// <param name="database">Идентификатор базы</param>
        IWorkerTask CreateAndStartPublishTask(Domain.DataModels.AccountDatabase database);

        /// <summary>
        ///     Создать и выполнить таску по снятию с публикации базы
        /// </summary>
        /// <param name="database">Идентификатор базы</param>
        IWorkerTask CreateAndStartCancelPublishTask(Domain.DataModels.AccountDatabase database);

        /// <summary>
        /// Создать и выполнить задачу удаления старой публикации инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="oldSegmentId">ID старого сегмента</param>
        /// <returns>Обработчик задачи</returns>
        IWorkerTask CreateAndStartRemoveOldPublicationJob(Guid accountDatabaseId, Guid oldSegmentId);

        /// <summary>
        ///     Создать и выполнить таску по переопубликации базы
        /// с изменением файла web.config
        /// </summary>
        /// <param name="database">Идентификатор базы</param>
        IWorkerTask CreateAndStartRepublishWebConfigTask(Domain.DataModels.AccountDatabase database);

        /// <summary>
        ///     Создать и выполнить таску по перезапуску пула приложения
        /// на нодах публикций
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        IWorkerTask CreateAndStartRestartApplicationPoolTask(Guid accountDatabaseId);
    }
}
