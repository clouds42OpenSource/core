﻿using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер по анализу информационных баз.
    /// </summary>
    public interface IAccountDatabaseAnalyzeProvider
    {

        /// <summary>
        /// Пересчитать размер информационной базы
        /// </summary>
        /// <param name="database">Информационная база</param>
        void RecalculateSizeOfAccountDatabase(Domain.DataModels.AccountDatabase database);

        /// <summary>
        ///  Проверка необходимости запуска таски
        /// на удаление или архивацию информационной базы в склеп.
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <param name="stateToChange">статус базы на который надо изменить если базу не надо удалять</param>
        bool NeedMoveDbToTomb(Domain.DataModels.AccountDatabase database, DatabaseState stateToChange);
    }
}