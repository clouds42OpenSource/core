﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер данных бэкап хранилища информационной базы
    /// </summary>
    public interface IAccountDatabaseBackupStorageDataProvider
    {
        /// <summary>
        /// Получить путь к бэкап хранилищу информационной базы.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        Task<SimpleResultModelDto<string>> GetDatabaseBackupStoragePathAsync(Guid accountDatabaseId);

    }
}