﻿using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для управления редиректом для опубликованной базы
    /// </summary>
    public interface IManagePublishedAccountDatabaseRedirectProvider
    {
        /// <summary>
        /// Обработать операцию управления редиректом для инф. базы
        /// </summary>
        /// <param name="manageAcDbRedirectParams">Параметры управления редиректом</param>
        /// <returns>Ok - если операция завершена успешно</returns>
        void ManageAcDbRedirect(ManagePublishedAcDbRedirectParamsDto manageAcDbRedirectParams);
    }
}
