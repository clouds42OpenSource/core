﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер удаления доступа для пользователя к инф. базе 
    /// </summary>
    public interface IDeleteAcDbAccessForAccountUserProvider
    {
        /// <summary>
        /// Удалить доступ для пользователя
        /// </summary>
        /// <param name="manageAcDbAccessParams">Параметры управления доступом</param>
        ManageAcDbAccessResultDto DeleteAcDbAccess(ManageAcDbAccessParamsDto manageAcDbAccessParams);
    }
}
