﻿using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    ///Хелпер изменений данных в ИБ на вкладке админ панели
    /// </summary>
    public interface IDatabaseEditHelper
    {
        /// <summary>
        ///     Метод смены платформы и дистрибутива,
        /// возвращает строку с записью что именно изменено
        /// </summary>
        /// <param name="accountDatabase">База</param>
        /// <param name="platformType">Платформа (8.2 или 8.3)</param>
        /// <param name="distributionType">Дистрибутив (Stable или Alpha)</param>
        string ChangeDbPlatformAndGetLogMessage(Domain.DataModels.AccountDatabase accountDatabase, PlatformType platformType,
            DistributionType distributionType);

        /// <summary>
        /// Метод смены типа информационной базы,
        /// возвращает значение типа ИБ
        /// </summary>
        /// <param name="accountDatabase">База</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        bool ChangeAccountDatabaseType(Domain.DataModels.AccountDatabase accountDatabase, ChangeAccountDatabaseTypeDto changeAccountDatabaseTypeDto);

        /// <summary>
        ///     Редактирование карточки аккаунта с админ-панели
        /// </summary>
        /// <param name="accountDatabaseDm">Модель данных для карточки базы</param>
        /// <param name="accountId">ID инф. базы</param>
        void EditAccountDatabase(AccountDatabaseCartDomainModel accountDatabaseDm, Guid accountId);

        /// <summary>
        /// Предоставление доступов внутренним пользователям
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        void GrantInternalAccess(Guid databaseId, Guid? accountUserId);

        /// <summary>
        /// Предоставления доступов внешним пользовователям
        /// </summary>
        /// <param name="result"></param>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        void GrantExternalAccess(SaveResult result, Guid databaseId, Guid accountUserId);

        /// <summary>
        /// Признак что необходимо переопубликовать базу
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <param name="platform">Версия платформы</param>
        /// <param name="distributionType">Тип распространения</param>
        bool NeedToRepublishDb(Domain.DataModels.AccountDatabase database, PlatformType platform,
            DistributionType distributionType);

        /// <summary>
        /// Сменить название инф. базы
        /// </summary>
        /// <param name="model">Модель изменения названия</param>
        void ChangeAccountDatabaseCaption(PostRequestAccDbCaptionDto model);

        /// <summary>
        /// Изменить номер базы 
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <param name="v82Name">Номер информационной базы</param>
        /// <returns></returns>
        void ChangeDbV82Name(Domain.DataModels.AccountDatabase database, string v82Name);

        /// <summary>
        /// Сменить тип модели восстановления для инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        void ChangeDatabaseRestoreModelType(Domain.DataModels.AccountDatabase accountDatabase, AccountDatabaseRestoreModelTypeEnum restoreModelType);

        /// <summary>
        /// Сменить признак наличия доработок для инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="hasModifications">Признак наличия доработок</param>
        void ChangeDatabaseHasModificationsFlag(Domain.DataModels.AccountDatabase accountDatabase, bool hasModifications);
    }
}
