﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для работы с информационными базами
    /// </summary>
    public interface IAccountDatabaseProvider
    {
        /// <summary>
        /// Получить статус информационной базы
        /// </summary>
        /// <param name="id">Id базы</param>
        /// <returns>Статус информационной базы</returns>
        DatabaseState GetAccountDatabaseStatus(Guid id);

        /// <summary>
        /// Получить информационную базу или выкинуть исключение
        /// </summary>
        /// <param name="id">Id информационной базы</param>
        /// <returns>Информационная база</returns>
        Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid id);

        /// <summary>
        /// Получает модель инф. базы по Id
        /// </summary>
        /// <param name="databaseId">Id базы</param>
        /// <param name="accountDatabase">out параметр Модель базы</param>
        /// <returns>Признак нашлась ли база по Id</returns>
        bool TryGetAccountDatabaseById(Guid databaseId, out Domain.DataModels.AccountDatabase accountDatabase);

        /// <summary>
        /// Получить информацию  о сервисе в информационной базе
        /// </summary>
        /// <param name="accountDatabaseId">Для какой базы получить информацию о сервисе в ней</param>
        /// <returns>Информацию  о сервисе в информационной базе</returns>
        Task<ServiceExtensionDatabaseInfoDto> GetServiceExtensionStatusesForAccountDatabase(Guid accountDatabaseId);
    }
}
