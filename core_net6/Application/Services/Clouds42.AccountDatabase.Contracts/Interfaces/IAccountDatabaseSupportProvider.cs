﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер инфо базы по управлению ТиИ и АО
    /// </summary>
    public interface IAccountDatabaseSupportProvider
    {
        /// <summary>
        /// Переключить тумлер ТиИ у информационной базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="hasSupport">Положение тумблера</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Успешность операции</returns>
        void ChangeHasSupportFlag(AcDbSupport acDbSupport, bool hasSupport);

        /// <summary>
        /// Переключить тумлер Автообновление у информационной базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="hasAutoUpdate">Положение тумблера</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Успешность операции</returns>
        void ChangeHasAutoUpdateFlag(AcDbSupport acDbSupport, bool hasAutoUpdate);

        /// <summary>
        /// Отключить авторизацию в инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        void DisableSupportAuthorization(Guid accountDatabaseId);

        /// <summary>
        /// Получить модель поддержки инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Модель поддержки инф. базы</returns>
        AcDbSupport GetAcDbSupport(Guid accountDatabaseId);
    }
}
