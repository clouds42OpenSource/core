﻿using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для удаления инф. баз
    /// </summary>
    public interface IDeleteAccountDatabaseProvider
    {
        /// <summary>
        /// Удалить инф. базу
        /// </summary>
        /// <param name="deleteAccountDatabaseParams">Параметры удаления инф. базы</param>
        void DeleteAccountDatabase(DeleteAccountDatabaseParamsDto deleteAccountDatabaseParams);
    }
}
