﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер упраления ролями для баз МЦОБ.
    /// </summary>
    public interface IAccountDatabaseMcobProvider
    {

        /// <summary>
        /// Предоставить доступ пользователю к базе с роллями.
        /// </summary>
        /// <param name="dbName">Имя базы.</param>
        /// <param name="userLogin">Логин пользователя.</param>
        /// <param name="userName">Имя пользователя в базе 1С.</param>
        /// <param name="roles">Список профилей, через ;</param>
        /// <param name="jhoRoles">Список ролей ЖХО.</param>        
        OperationResultDto McobAddUser(string dbName, string userLogin, string userName, string roles, string jhoRoles);

        /// <summary>
        /// Забрать доступ пользоватля к базе.
        /// </summary>
        /// <param name="dbName">Имя базы.</param>
        /// <param name="userLogin">Логин пользователя.</param>        
        /// <param name="roles">Список профилей, через ;</param>                
        OperationResultDto McobRemoveUserRoles(string dbName, string userLogin, string roles);
    }
}