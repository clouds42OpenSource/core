﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными инф. базы для отчета
    /// </summary>
    public interface IAccountDatabaseReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по инф. базам для отчета
        /// </summary>
        /// <returns>Список моделей данных по инф. базам для отчета</returns>
        List<AccountDatabaseReportDataDto> GetAccountDatabaseReportDataDcs();
    }
}
