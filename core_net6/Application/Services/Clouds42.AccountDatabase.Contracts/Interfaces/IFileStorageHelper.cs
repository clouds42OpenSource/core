﻿namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    public interface IFileStorageHelper
    {
        /// <summary>
        /// Метод получения последней даты активности
        /// </summary>
        /// <param name="db">Объект AccountDatabase</param>
        /// <returns>Дата последней активности или ошибка</returns>
        DateTime GetLastActiveDateFileBase(Domain.DataModels.AccountDatabase db);
    }
}