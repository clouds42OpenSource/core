﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Регистратор событий потдержки.
    /// </summary>
    public interface IAcDbSupportHistoryRegistrator
    {
        /// <summary>
        /// Зарегистрировать неверные авторазационные данные.
        /// </summary>                
        void RegisterAuthIncorrectData(AcDbSupport support);        

        /// <summary>
        /// Зарегистрировать ошибку авторизации.
        /// </summary>        
        void RegisterAuthFail(AcDbSupport support, string errorMessage);

        /// <summary>
        /// Зарегистрировать успешную авторизация.
        /// </summary>        
        void RegisterSuccessAuth(AcDbSupport support);     
        
        /// <summary>
        /// Зарегистрировать прочитанное уведомление.
        /// </summary>        
        Task RegisterNotificationRead(Domain.DataModels.AccountDatabase database); 

        /// <summary>
        /// Зарегистрировать успешное подключение к АО.
        /// </summary>        
        void RegisterSuccessConnectToAutoUpdate(AcDbSupport support);

        /// <summary>
        /// Зарегистрировать неизвестную конфигурацию.
        /// Примечание: конфигурация может быть не известная если не заполнить справочник dbo.Configurations1C
        /// </summary>        
        void RegisterUndefinedConfiguration(AcDbSupport support, string configurationName);

    }
}
