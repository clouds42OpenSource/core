﻿using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для работы с доступами к инф. базам
    /// </summary>
    public interface IAcDbAccessProvider
    {
        /// <summary>
        /// Получить или добавить доступ к инф. базе
        /// </summary>
        /// <param name="model">Параметры предоставления доступа</param>
        /// <returns>Доступ к инф. базе</returns>
        AcDbAccess GetOrCreateAccess(AcDbAccessPostAddModelDto model);

        /// <summary>
        /// Удалить доступ если он существует
        /// </summary>
        /// <param name="accounDatabaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        void DeleteAccessIfExist(Guid accounDatabaseId, Guid accountUserId);

        /// <summary>
        /// Удалить доступ
        /// </summary>
        /// <param name="model">Параметры удаления доступа</param>
        void DeleteAccess(AcDbAccessPostIDDto model);

        /// <summary>
        /// Удалить доступы аккаунта к инф. базе
        /// </summary>
        /// <param name="model">Параметры удаления доступов</param>
        void DeleteAccountAccesses(DeleteAccessDto model);

        /// <summary>
        /// Установить ID аккаунта для доступа
        /// </summary>
        /// <param name="model">Параметры установки ID аккаунта для доступа</param>
        void SetAccountId(AcDbAccessPostSetAccIdDto model);

        /// <summary>
        /// Добавить доступ для всех пользователей
        /// </summary>
        /// <param name="model">Параметры добавления доступов</param>
        /// <returns>Список добавленных доступов</returns>
        List<Guid> AddAccessesForAllAccountsUsers(AcDbAccessPostAddAllUsersModelDto model);

        /// <summary>
        /// Установить ID пользователя для доступа
        /// </summary>
        /// <param name="model">Параметры установки ID пользователя для доступа</param>
        /// <returns>Результат установки</returns>
        void SetAccountUserId(AcDbAccessAccountUserIDPostDto model);
    }
}
