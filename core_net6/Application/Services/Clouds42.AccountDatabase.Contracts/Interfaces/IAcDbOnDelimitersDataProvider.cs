﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провадер данных для доступа к инф. базе на разделителях
    /// </summary>
    public interface IAcDbOnDelimitersDataProvider
    {
        /// <summary>
        /// Получить модель доступа к инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Модель доступа к инф. базе</returns>
        AcDbAccess GetAcDbAccess(Guid accountDatabaseId, Guid accountUserId);

        /// <summary>
        /// Получить модель доступа к инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Модель доступа к инф. базе</returns>
        AcDbAccess GetAcDbAccessOrThrowException(Guid accountDatabaseId, Guid accountUserId);

        /// <summary>
        /// Удалить запись о доступе
        /// </summary>
        /// <param name="accessEntity">Модель доступа</param>
        void DeleteAcDbAccess(AcDbAccess accessEntity);

        /// <summary>
        /// Удалить запись о доступе при ошибке
        /// </summary>
        /// <param name="accessEntity">Модель доступа</param>
        void DeleteAcDbAccessForError(AcDbAccess accessEntity);
    }
}
