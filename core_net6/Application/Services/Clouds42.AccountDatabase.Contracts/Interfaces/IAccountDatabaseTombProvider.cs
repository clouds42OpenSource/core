﻿using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер по переносу информационной базы в склеп.
    /// </summary>
    public interface IAccountDatabaseTombProvider
    {
        /// <summary>
        /// Удалить информационную базу в склеп
        /// </summary>
        /// <param name="accountDatabase">Информационная база.</param>
        /// <param name="sendEmail">В случае ошибки, отправить письмо хотлайну</param>
        /// <param name="forceUpload">Принудительная загрузка файла в склеп</param>
        /// <param name="trigger">Триггер отправки базы в склеп</param>
        void DeleteAccountDatabaseToTomb(
            Domain.DataModels.AccountDatabase accountDatabase,
            bool sendEmail,
            bool forceUpload = false,
            CreateBackupAccountDatabaseTrigger trigger = CreateBackupAccountDatabaseTrigger.ManualRemoval);
    }
}