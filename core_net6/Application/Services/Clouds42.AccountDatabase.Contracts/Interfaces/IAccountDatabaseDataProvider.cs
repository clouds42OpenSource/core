using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер данных ифнормационной базы.
    /// </summary>
    public interface IAccountDatabaseDataProvider
    {
        /// <summary>
        /// Получить информационную базу.
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid accountDatabaseId);

        /// <summary>
        /// Получить информационную базу.
        /// </summary>
        /// <param name="v82Name">Номер информационной базы.</param>
        Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(string v82Name);

        /// <summary>
        /// Получить бэкап инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>Бэкап инф. базы</returns>
        AccountDatabaseBackup GetAccountDatabaseBackupOrThrowException(Guid accountDatabaseBackupId);

        /// <summary>
        /// Получить список бекапов инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Список бекапов инф. базы</returns>
        IQueryable<AccountDatabaseBackup> GetAccountDatabaseBackups(Guid accountDatabaseId);

        /// <summary>
        /// Получить список баз, доступных пользователю.
        /// </summary>        
        /// <param name="accountUserId">Номер пользователя.</param>        
        Task<List<AccountDatabasePropertiesDto>> GetAccessDatabaseListAsync(Guid accountUserId);

        /// <summary>
        ///     Получить список баз на разделителях, доступных пользователю.
        /// </summary>        
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <param name="configurationCode">Код конфигурации</param>   
        Task<List<DelimitersDbsPropertiesDto>> GetDelimitersDbsByUserAsync(Guid accountUserId, string configurationCode);

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>        
        /// <param name="accountDatabaseId">Номер информационной базы.</param>        
        Task<AccountDatabasePropertiesDto> GetAccountDatabasesByIdAsync(Guid accountDatabaseId);

        /// <summary>
        /// Получить данные информационной базы по имени базы.
        /// </summary>        
        /// <param name="v82Name">Имя информационной базы.</param>        
        Task<AccountDatabasePropertiesDto> GetAccountDatabasesByNameAsync(string v82Name);

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        /// <param name="accountUserId">Идентификатор пользователя который запрашивает сведения о базе.</param>        
        Task<AccountDatabasePropertiesDto> GetAccountDatabasesForUserById(Guid accountDatabaseId,
            Guid accountUserId);
        
        /// <summary>
        /// Получить бэкап инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>Бэкап инф. базы</returns>
        AccountDatabaseBackup GetAccountDatabaseBackupById(Guid accountDatabaseBackupId);

        /// <summary>
        /// Получить информационную базу.
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <returns>Модель инф. базы</returns>
        Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId);

        /// <summary>
        /// Получить инф. базу по Id бэкапа или выкинуть исключение
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>Инф. база</returns>
        Domain.DataModels.AccountDatabase GetAccountDatabaseByBackupIdOrThrowException(Guid accountDatabaseBackupId);

        /// <summary>
        /// удалить кэш бд
        /// </summary>
        /// <param name="accountDatabaseId"></param>
        /// <returns></returns>
        void DeleteAccountDatabaseCache(DeleteAcDbCacheDto deleteAcDbCacheDto);
    }
}
