﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using PagedListExtensionsNetFramework;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер получения данных об информационных базах
    /// </summary>
    public interface IAccountDatabaseInfoDataProvider
    {
        /// <summary>
        /// Получить список информационных баз
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="searchQuery">Фильр поиска</param>
        /// <returns>Список информационных баз</returns>
        AccountDatabasesListDataDto GetAccountDatabasesList(int page, string searchQuery);

        /// <summary>
        /// Получить список информационных баз
        /// </summary>
        /// <param name="args">Фильр поиска</param>
        /// <returns>Список информационных баз</returns>
        Task<PagedDto<AccountDatabaseItemDto>> GetAccountDatabaseItemsAsync(GetAccountDatabaseListParamsDto args);

    }
}
