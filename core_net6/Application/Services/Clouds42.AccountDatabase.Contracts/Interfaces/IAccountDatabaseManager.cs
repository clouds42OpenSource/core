﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    public interface IAccountDatabaseManager
    {
        Task<ManagerResult<bool>> AddUserNotifiedState(Guid accountUserId);
        ManagerResult<string> BuildRemoteDesktopLinkString(Guid accountUserId);
        ManagerResult ChangeDbFlagUsedWebServices(Guid databaseId, bool? usedWebServices);
        ManagerResult DisableSupportAuthorization(Guid accountDatabaseId);
        ManagerResult<double> GetAccountBallanse(Guid accountId);
        ManagerResult<AccountDatabasesListDataDto> GetAccountDatabasesViewModel(int page, string searchQuery);
        ManagerResult<List<ArchiveDatabaseModelDto>> GetArchiveDatabaseList();
        AccountDatabaseSupportSettingDto GetDatabaseSupportSettings(Guid databaseId);
        ManagerResult<IEnumerable<Guid>> GetIDs(Guid accountId);
        ManagerResult<List<LocalAccountDatabasesDto>> GetLocalAccountDatabases(Guid accountID);
        ManagerResult<ConnectorResultDto<MetadataResultDto>> GetMetadataInfoAccountDatabase(Guid accountDatabaseId);
        ManagerResult<AccountDatabaseRemoteParamsDto> GetRemoteAppParams(Guid accountDatabaseId, Guid accountUserId, string launchMode = null);
        Task<ManagerResult<ServiceExtensionDatabaseInfoDto>> GetServiceExtensionStatusesForAccountDatabase(
            Guid accountDatabaseId);
        void RecalculateSizeOfAccountDatabase(Guid accountDatabaseId);
        void Run1CEpfMcob(string v82Name, bool isFile, string filePath, string parameters, PlatformType platform);
        ManagerResult SaveLaunchParameters(Guid accountDatabaseID, string launchParameter);
        ManagerResult<Guid> SaveLocalAccountDatabases(SaveLocalAccountDatabasesDto model);
        ManagerResult SetApplicationName(PostRequestAppNameDto model);
        ManagerResult SetDistributiveType(PostRequestSetDistributiveTypeDto model);
        ManagerResult SetLastActivityDate(PostRequestAccDbLastActivityDateDto model);
        ManagerResult SetLaunchType(PostRequestLaunchTypeDto model);
        ManagerResult<bool> SaveSupportDataAndAuthorize(SupportDataDto authorizeInDatabaseDto);
        ManagerResult TryAdminAuthorizeToAccountDatabase(Guid accountDatabaseId);
        ManagerResult<IEnumerable<DatabaseAccessRightsDto>> GetAccountDatabaseAccess(List<ObjectAction> objectActions, Guid databaseId); 
    }
}
