﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для получения данных по доступам к инф. базам
    /// </summary>
    public interface IAcDbAccessDataProvider
    {
        /// <summary>
        /// Получить доступ к инф. базе
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Доступ к инф. базе</returns>
        AcDbAccess GetAcDbAccess(Guid accountUserId, Guid accountDatabaseId);

        /// <summary>
        /// Получить доступ к инф. базе или выбросить исключение
        /// </summary>
        /// <param name="acDbAccessId">ID доступа</param>
        /// <returns>Доступ к инф. базе</returns>
        AcDbAccess GetAcDbAccessOrThrowException(Guid acDbAccessId);

        /// <summary>
        /// Получить доступы аккаунта к инф. базе
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Доступы аккаунта к инф. базе</returns>
        List<AcDbAccess> GetAccountAccessesForDatabase(Guid accountId, Guid accountDatabaseId);
    }
}
