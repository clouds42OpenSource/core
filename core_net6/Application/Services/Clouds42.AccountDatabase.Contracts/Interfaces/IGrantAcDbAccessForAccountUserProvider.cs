﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер предоставления доступа для пользователя к инф. базе 
    /// </summary>
    public interface IGrantAcDbAccessForAccountUserProvider
    {
        /// <summary>
        /// Выдать доступ для пользователя
        /// </summary>
        /// <param name="manageAcDbAccessParams">Параметры управления доступом</param>
        ManageAcDbAccessResultDto GrantAcDbAccess(ManageAcDbAccessParamsDto manageAcDbAccessParams);
    }
}
