﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    public interface IAccountDatabaseModelCreator
    {
        /// <summary>
        /// Создать модель серверной инф. базы.
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Модель серверной инф. базы</returns>
        AccountDatabaseEnterpriseModelDto CreateModel(Domain.DataModels.AccountDatabase accountDatabase);

        /// <summary>
        /// Создать модель.
        /// </summary>    
        AccountDatabaseEnterpriseModelDto CreateModel(Guid accountDatabaseId);

        /// <summary>
        /// Создать модель.
        /// </summary>     
        AccountDatabaseEnterpriseModelDto CreateModel(string v82Name);
    }
}