﻿namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    public interface IFetchConnectionsBridgeApiProvider
    {
        /// <summary>
        /// Обаботать команду обновления базы в облаке.
        /// </summary>
        void UpdateDatabaseHandle(Guid accountDatabaseId);

        /// <summary>
        /// Обаботать команду содание новой базы в облаке.
        /// </summary>
        void CreateDatabaseHandle(Guid accountDatabaseId);

        /// <summary>
        /// Обаботать команду удаления базы с облака.
        /// </summary>
        void DeleteDatabaseHandle(Guid accountDatabaseId);

        /// <summary>
        /// Обаботать команду предоставления доступа к базе.
        /// </summary>
        void GrandAccessDatabaseHandle(Guid accountDatabaseId);

        /// <summary>
        /// Обаботать команду удаления пользовательского доступа у базы.
        /// </summary>
        void DeleteAccessDatabaseHandle(Guid accountDatabaseId, Guid accountUserId);

        /// <summary>
        /// Выполнить обработку ассинхронно.
        /// </summary>
        IFetchConnectionsBridgeApiProvider ExecuteInBackgroundThread();

    }
}
