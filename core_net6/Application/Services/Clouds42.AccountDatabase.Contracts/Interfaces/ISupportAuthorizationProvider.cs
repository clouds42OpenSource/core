﻿using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер тех обслуживания (ТиИ) информационной базы
    /// </summary>
    public interface ISupportAuthorizationProvider
    {
        /// <summary>
        /// Попытыться авторизоваться в информационную базу под администратором.
        /// </summary>
        /// <param name="support">Данные поддержки.</param>
        /// <returns>Признак успешности авторизации</returns>
        bool TryAdminAuthorize(AcDbSupport support);

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>    
        ConnectorResultDto<MetadataResultDto> GetMetadataInfoAccountDatabase(AcDbSupport support);
    }
}
