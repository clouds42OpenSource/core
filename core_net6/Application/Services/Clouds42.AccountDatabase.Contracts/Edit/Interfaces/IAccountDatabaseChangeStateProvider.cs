﻿using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Contracts.Edit.Interfaces
{
    /// <summary>
    /// Провайдер для смены статуса иформацонной базы
    /// </summary>
    public interface IAccountDatabaseChangeStateProvider
    {
        /// <summary>
        /// Изменить статус информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        void ChangeState(Guid databaseId, DatabaseState newState);

        /// <summary>
        /// Изменить статус информационной базы
        /// </summary>
        /// <param name="database">Id информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        /// <param name="createAccountDatabaseComment"></param>
        void ChangeState(Domain.DataModels.AccountDatabase database, DatabaseState newState, string? createAccountDatabaseComment = null);

        /// <summary>
        /// Изменить статус информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        /// <param name="lastActivityDate">Дата последней активности в базе</param>
        void ChangeState(Guid databaseId, DatabaseState newState, DateTime lastActivityDate);


        /// <summary>
        /// Изменить статус информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="newState">Новый статус информационной базы</param>
        /// <param name="createAccountDatabaseComment">Комментарий по созданию базы</param>
        void ChangeState(Guid databaseId, DatabaseState newState, string? createAccountDatabaseComment);
    }
}
