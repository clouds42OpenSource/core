﻿using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.Edit.Interfaces
{
    /// <summary>
    /// Провайдер для смены статуса публикации инф. баз
    /// </summary>
    public interface IAccountDatabaseChangePublishStateProvider
    {
        /// <summary>
        /// Сменить статус публикации
        /// </summary>
        /// <param name="publishState">Статус публикации</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        void ChangePublishState(PublishState publishState, Guid accountDatabaseId);
    }
}
