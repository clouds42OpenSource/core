﻿namespace Clouds42.AccountDatabase.Contracts.Edit.Interfaces
{
    /// <summary>
    /// Провайдер изменения шаблона для инф. базы
    /// </summary>
    public interface IChangeAccountDatabaseTemplateProvider
    {
        /// <summary>
        /// Изменить шаблон у информационной базы
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="templateId">Новый шаблон</param>
        void Change(Guid databaseId, Guid? templateId);
    }
}
