﻿using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.Contracts.Publishes.Interfaces
{
    public interface IRepublishSegmentDatabasesProvider
    {
        /// <summary>
        /// Переопубликовать базы сегмента
        /// </summary>
        /// <param name="segmentId">Id сегмента</param>
        /// <param name="platformType">Версия платформы 1С</param>
        /// <param name="distributionType">Версия дистрибутива</param>
        /// <returns>Сообщение о результате рестарта пулов</returns>
        void RepublishSegmentDatabases(Guid segmentId, PlatformType platformType, DistributionType distributionType);
    }
}