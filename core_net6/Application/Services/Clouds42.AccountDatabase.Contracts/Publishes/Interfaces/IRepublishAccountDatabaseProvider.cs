﻿namespace Clouds42.AccountDatabase.Contracts.Publishes.Interfaces
{
    /// <summary>
    /// Провайдер для перепубликации инф. баз
    /// </summary>
    public interface IRepublishAccountDatabaseProvider
    {
        /// <summary>
        /// Переопубликовать инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        void Republish(Guid accountDatabaseId);

        /// <summary>
        ///     Переопубликовать базу с изменением файла web.config
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        void RepublishWithChangingWebConfig(Guid accountDatabaseId);
    }
}
