﻿using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;

namespace Clouds42.AccountDatabase.Contracts.Publishes.Interfaces
{
    public interface IWebAccessHelper
    {
        void UpdateLoginForWebAccessDatabase(UpdateLoginForWebAccessDatabaseDto updateLoginForWebAccessDatabase);
        void ManageAccess(List<Domain.DataModels.AccountDatabase> databases,
            List<Guid> usersForGrandAccess, List<Guid> usersForDeleteAccess);

        void ManageAccess(List<Domain.DataModels.AccountDatabase> databases,
            List<string>? usersForGrandAccess, List<string>? usersForDeleteAccess);

    }
}
