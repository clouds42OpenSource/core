﻿namespace Clouds42.AccountDatabase.Contracts.Publishes.Interfaces
{
    /// <summary>
    /// Провайдер рестарта пулов баз на нодах публикаций
    /// </summary>
    public interface IRestartDatabaseIisApplicationPoolProvider
    {
        /// <summary>
        /// Перезапустить пул базы аккаунта на нодах публикаций
        /// </summary>
        /// <param name="accountDatabaseId">Id инф. базы аккаунта</param>
        void RestartIisApplicationPool(Guid accountDatabaseId);
    }
}
