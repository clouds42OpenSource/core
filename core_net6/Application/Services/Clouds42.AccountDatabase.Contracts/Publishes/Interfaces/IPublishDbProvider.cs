﻿using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;

namespace Clouds42.AccountDatabase.Contracts.Publishes.Interfaces
{
    public interface IPublishDbProvider
    {
        void AddWebAccessToDatabase(IServiceProvider serviceProvider, Guid databaseId, Guid accountUserId);
        void CancelPublish(IServiceProvider serviceProvider, Guid databaseId);
        void Publish(IServiceProvider serviceProvider, Guid databaseId);
        void RemoveOldDatabasePublication(IServiceProvider serviceProvider, Guid accountDatabaseId, Guid oldSegmentId);
        void RemoveWebAccessToDatabase(IServiceProvider serviceProvider, Guid databaseId, Guid accountUserId);
        void UpdateLoginForWebAccessDatabase(IServiceProvider serviceProvider, UpdateLoginForWebAccessDatabaseDto updateLoginForWebAccessDatabase);
    }
}
