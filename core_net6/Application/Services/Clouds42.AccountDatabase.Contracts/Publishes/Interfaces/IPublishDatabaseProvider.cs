﻿namespace Clouds42.AccountDatabase.Contracts.Publishes.Interfaces
{
    public interface IPublishDatabaseProvider
    {
        /// <summary>
        /// Отменить публикацию базы
        /// </summary>
        /// <param name="accountDatabaseId">ID базы аккаунта</param>
        /// <returns>Ok - если публикация базы отменена</returns>
        void CancelPublishDatabase(Guid accountDatabaseId);

        /// <summary>
        /// Опубликовать инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID базы аккаунта</param>
        /// <returns>Ok - если база переопубликована</returns> 
        void PublishDatabase(Guid accountDatabaseId);

        /// <summary>
        /// Переопубликовать инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID базы аккаунта</param>
        /// <returns>Ok - если база переопубликована</returns>
        void RepublishDatabase(Guid accountDatabaseId);

        /// <summary>
        /// Переопубликовать базу с изменением файла web.config
        /// </summary>
        /// <param name="accountDatabaseId">ID базы аккаунта</param>
        /// <returns>Ok - если база переопубликована</returns>
        void RepublishWithChangingWebConfig(Guid accountDatabaseId);
    }
}