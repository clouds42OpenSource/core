﻿namespace Clouds42.AccountDatabase.Contracts.Audit.Interfaces
{
    /// <summary>
    /// Выполнить аудит путей
    /// публикации информационных баз
    /// </summary>
    public interface IPublishingPathsDatabasesAudit
    {
        void ProcessAudit();
    }
}