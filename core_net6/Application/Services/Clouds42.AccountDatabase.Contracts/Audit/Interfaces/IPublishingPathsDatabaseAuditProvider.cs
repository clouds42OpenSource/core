﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit;

namespace Clouds42.AccountDatabase.Contracts.Audit.Interfaces
{
    public interface IPublishingPathsDatabaseAuditProvider
    {
        AuditResultDto AccountDatabaseAudit(Domain.DataModels.AccountDatabase accountDatabase);
    }
}