﻿namespace Clouds42.AccountDatabase.Contracts.Audit.Interfaces
{
    /// <summary>
    /// Провайдер аудита статусов инф. баз
    /// </summary>
    public interface IAuditAccountDatabasesStatesProvider
    {
        /// <summary>
        /// Провести аудит
        /// </summary>
        void ProcessAudit();
    }
}
