﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit;

namespace Clouds42.AccountDatabase.Contracts.Audit.Interfaces
{
    public interface IAccountResourceIntegralityAuditProvider
    {
        void AuditAccountResourceIntegrality();
        List<AccountBillingServiceTypesModelDto> GetListOfMissingResources();
    }
}