﻿using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces
{
    /// <summary>
    /// Провайдер пакетов обновления 1С.
    /// </summary>
    public interface ICfuProvider
    {

        /// <summary>
        /// Получить путь до cfu пакета обновлений 1С.
        /// </summary>                
        string GetCfuFullPath(string configurationCode, string release);

        /// <summary>
        /// Скачать пакет обновления.
        /// </summary>        
        void DownloadCfu(IConfigurationsCfu cfu);

        /// <summary>
        /// Скачать пакет обновления.
        /// </summary>        
        void DownloadCfu(ICfuSource cfu, IConfigurations1C configuration);

        /// <summary>
        /// Пакет обновлений уже скачан.
        /// </summary>        
        bool CfuExist(ICfuSource cfu, IConfigurations1C configuration);

        /// <summary>
        /// Получить максимально возможную версию обновления пакета.
        /// </summary>
        /// <param name="configuration">Конфигурация инф. базы</param>
        /// <param name="version">Версия релиза</param>
        /// <param name="segmentPlatformVersion">Версия платформы в сегменте</param>
        /// <returns>Максимально возможная версия обновления пакета</returns>       
        IConfigurationsCfu? GetLastUpdateVersion(IConfigurations1C configuration, string version, string segmentPlatformVersion);

        /// <summary>
        /// Получить следующую версию пакета обновления.
        /// </summary>
        /// <param name="configuration">Конфигурация инф. базы</param>
        /// <param name="version">Версия релиза</param>
        /// <param name="segmentPlatformVersion">Версия платформы в сегменте</param>
        /// <returns>Следующая версия пакета обновления</returns>
        IConfigurationsCfu? GetNextUpdateVersion(IConfigurations1C configuration, string version, string segmentPlatformVersion);

        /// <summary>
        /// Получить полный путь до пакета обновлений на диске.
        /// </summary>
        /// <param name="cfu">Пакет обновлений</param>                    
        /// <param name="configuration">Конфигурация 1С.</param>                    
        /// <returns>Полный путь до пакета обновлений.</returns>
        string GetCfuFullPath(ICfuSource cfu, IConfigurations1C configuration);

        /// <summary>
        /// Получить полный путь до пакета обновлений на диске.
        /// </summary>
        /// <param name="cfu">Пакет обновлений</param>                            
        /// <returns>Полный путь до пакета обновлений.</returns>
        string GetCfuFullPath(IConfigurationsCfu cfu);

        /// <summary>
        /// Получить полный путь к архиву с информацией по обновлениям конфигурации.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        List<ConfigurationRedactionModelDto> GetConfigurationRedactionInfo(IConfigurations1C configuration);

    }
}
