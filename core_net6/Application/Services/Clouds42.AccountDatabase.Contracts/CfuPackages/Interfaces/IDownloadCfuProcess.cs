﻿using Clouds42.DataContracts.Configurations1c.CfuPackage;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces
{
    public interface IDownloadCfuProcess
    {
        void DownloadCfus(Configurations1C configurations1C);
        List<CfuConfigurations> GetNewCfuReleasesForConfiguration(Configurations1C configurations1C);
    }
}
