﻿namespace Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces
{
    public interface ICfuManager
    {
        /// <summary>
        /// Скачать доступные Cfu
        /// </summary>
        void DownloadAvailableCfus();
    }
}