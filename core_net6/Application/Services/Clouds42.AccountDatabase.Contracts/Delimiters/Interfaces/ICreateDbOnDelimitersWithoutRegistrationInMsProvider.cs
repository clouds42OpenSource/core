﻿using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер для создания базы на разделителях(без регистрации в МС)
    /// </summary>
    public interface ICreateDbOnDelimitersWithoutRegistrationInMsProvider
    {
        /// <summary>
        /// Создать базу на разделителях(без регистрации в МС)
        /// </summary>
        /// <param name="model">Модель создания базы на разделителях</param>
        /// <returns>Id созданной базы</returns>
        Guid Create(CreateDbOnDelimitersWithoutRegistrationInMsDto model);
    }
}
