﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер редактирования материнской базы разделителей
    /// </summary>
    public interface IEditDelimiterSourceAccountDatabaseProvider
    {

        /// <summary>
        /// Редактировать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabaseDto">Модель материнской базы разделителей</param>
        void Edit(DelimiterSourceAccountDatabaseDto delimiterSourceAccountDatabaseDto);
    }
}
