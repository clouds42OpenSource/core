﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер создания материнской базы разделителей
    /// </summary>
    public interface ICreateDelimiterSourceAccountDatabaseProvider
    {
        /// <summary>
        /// Создать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabase">Модель материнской базы разделителей</param>
        void Create(DelimiterSourceAccountDatabaseDto delimiterSourceAccountDatabase);

        /// <summary>
        /// Создать материнскую базу разделителей
        /// </summary>
        /// <param name="delimiterSourceAccountDatabase">Модель материнской базы разделителей</param>
        Guid CreateMotherBase(MotherAccountDatabaseDto delimiterSourceAccountDatabase);

    }
}
