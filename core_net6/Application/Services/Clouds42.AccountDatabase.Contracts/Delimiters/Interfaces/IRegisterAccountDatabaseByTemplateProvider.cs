﻿using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер для регистрации/созднаия инф. базы по шаблону
    /// </summary>
    public interface IRegisterAccountDatabaseByTemplateProvider
    {
        /// <summary>
        /// Зарегистрировать/создать инф. базу по шалону
        /// при активации сервиса биллинга
        /// </summary>
        /// <param name="model">Модель регистрации инф. базы по шаблону
        /// при активации сервиса биллинга</param>
        void RegisterDatabaseUponServiceActivation(RegisterDatabaseUponServiceActivationDto model);
    }
}
