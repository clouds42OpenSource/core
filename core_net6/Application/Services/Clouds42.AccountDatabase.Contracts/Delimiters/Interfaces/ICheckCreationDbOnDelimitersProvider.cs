﻿namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер для проверки возможности создания базы на разделителях
    /// </summary>
    public interface ICheckCreationDbOnDelimitersProvider
    {
        /// <summary>
        /// Проверить возможность создать базу на разделителях(без регистрации в МС)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void CheckCreationWithoutRegistrationInMs(Guid accountId);
    }
}
