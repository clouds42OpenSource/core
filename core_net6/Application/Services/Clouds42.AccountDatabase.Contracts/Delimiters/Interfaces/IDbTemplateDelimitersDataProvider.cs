﻿using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными шаблонов на разделителях
    /// </summary>
    public interface IDbTemplateDelimitersDataProvider
    {
        /// <summary>
        /// Получить шаблон базы на разделителях по Id конфигурации
        /// </summary>
        /// <param name="configurationId">Id конфигурации</param>
        /// <returns>Шаблон базы на разделителях</returns>
        DbTemplateDelimiters GetDbTemplateDelimitersByConfigurationId(string configurationId);


        /// <summary>
        /// Получить все конфигурации разделителей
        /// </summary>
        /// <returns>Все конфигурации разделителей, где key=ID шаблона, value=название шаблона</returns>
        KeyValuePair<string, string>[] GetDbTemplateDelimiters();

        /// <summary>
        /// Получить все конфигурации разделителей
        /// </summary>
        /// <param name="args">параметры для выбора шаблонов баз на разделителях</param>
        /// <returns>Все конфигурации разделителей</returns>
        SelectDataResultCommonDto<DbTemplateDelimeterItemDto> GetDbTemplateDelimetersItems(GetDbTemplateDelimitersParamsDto args);


        /// <summary>
        ///     Получить базу на разделителях
        /// </summary>
        /// <param name="dbTemplateDelimitersId">ID конфигурации для которой получить базу на разделителях.</param>
        /// <returns>Базу на разделителях</returns>
        DbTemplateDelimeterItemDto GetDbTemplateDelimiter(string dbTemplateDelimitersId);


        /// <summary>
        ///     Обновить существующую базу на разделителях
        /// </summary>
        /// <param name="dbTemplateDelimitersToUpdate">Обновленная база на разделителях</param>
        void UpdateDbTemplateDelimiterItem(IDbTemplateDelimiters dbTemplateDelimitersToUpdate);

        /// <summary>
        ///     Добавить новую базу на разделителях
        /// </summary>
        /// <param name="newDbTemplateDelimiter">База на разделителях для добавления</param>
        void AddDbTemplateDelimiterItem(IDbTemplateDelimiters newDbTemplateDelimiter);


        /// <summary>
        ///     Удалить существующую базу на разделителях
        /// </summary>
        /// <param name="args">Параметры для удаления базы на разделителях</param>
        void DeleteDbTemplateDelimiterItem(DeleteDbTemplateDelimiterDto args);
    }
}
