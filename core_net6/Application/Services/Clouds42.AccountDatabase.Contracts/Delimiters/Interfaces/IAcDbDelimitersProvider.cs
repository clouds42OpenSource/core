﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.Cloud42Services;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер инф. баз на разделителях
    /// </summary>
    public interface IAcDbDelimitersProvider
    {
        /// <summary>
        /// Получить номер информационной базы на разделителях по номеру материнской базы и номеру области.
        /// </summary>
        /// <param name="databaseSourceId">ID материнской базы</param>
        /// <param name="zone">Номер зоны</param>
        /// <returns>Номер информационной базы</returns>
        Guid GetAccountDatabaseIdByZone(Guid databaseSourceId, int zone);

        /// <summary>
        /// Выставление статуса информационной базы и добавление информации о базе на разделителях
        /// </summary>
        /// <param name="model">Модель базы на разделителях</param>
        void SetStatusAndInsertDataOnDelimiter(AccountDatabaseViaDelimiterDto model);

        /// <summary>
        /// Создать базу на разделителях и выдать права доступа пользователю.
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="configurationCode">Код конфигурации</param>
        /// <returns>Результат создания</returns>
        CreateCloud42ServiceModelDto CreateAccountDatabaseAndAddAccessForUser(Guid accountUserId, string configurationCode);

        /// <summary>
        /// Метод предоставления доступа одному пользователю
        /// </summary>
        /// <param name="model">Модель предоставления доступа</param>
        /// <param name="database">Инф. база</param>
        void AddAccessToDbOnDelimiters(AcDbAccessPostAddModelDto model, Domain.DataModels.AccountDatabase database);

        /// <summary>
        /// Удаление доступа для одного пользователя
        /// </summary>
        /// <param name="database">Информационная база.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        void DeleteAccessFromDelimiterDatabase(Domain.DataModels.AccountDatabase database, Guid accountUserId);

    }
}
