﻿namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{

    /// <summary>
    /// Обработчик обновления шаблонов.
    /// </summary>
    public interface IDbTemplateUpdateProvider
    {

        /// <summary>
        /// Обновить шаблоны.
        /// </summary>

        void UpdateTemplates();
    }
}
