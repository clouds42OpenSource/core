﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Класс справочными данными шаблонов.
    /// </summary>
    public interface IDbTemplateReferenceProvider
    {
        /// <summary>
        /// Получить шаблон по Id
        /// </summary>
        /// <param name="templateId">Id шаблона</param>
        /// <returns>Шаблон</returns>
        DbTemplate GetDbTemplateById(Guid? templateId);
    }
}
