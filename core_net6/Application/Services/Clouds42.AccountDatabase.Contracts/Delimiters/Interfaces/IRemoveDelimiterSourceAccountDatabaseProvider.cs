﻿namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер для удаления материнской базы разделителей
    /// </summary>
    public interface IRemoveDelimiterSourceAccountDatabaseProvider
    {
        /// <summary>
        /// Удалить материнскую базу разделителей
        /// </summary>
        /// <param name="accountDatabaseId">Id базы источника на разделителях</param>
        /// <param name="dbTemplateDelimiterCode">Код шаблона на разделителях</param>
        void Remove(Guid accountDatabaseId, string dbTemplateDelimiterCode);
    }
}
