﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    /// Провайдер получения данных для создания базы на разделителях
    /// </summary>
    public interface IGetDataForCreationDbOnDelimitersProvider
    {
        /// <summary>
        /// Получить версию платформы у шаблона
        /// </summary>
        /// <param name="dbTemplate">Шаблон базы</param>
        /// <returns>Версия платформы</returns>
        PlatformType GetPlatformTypeForTemplate(DbTemplate dbTemplate);

        /// <summary>
        /// Получить шаблон базы
        /// </summary>
        /// <param name="configurationId">Код конфигурации шаблона на разделителях</param>
        /// <returns>Шаблон базы</returns>
        DbTemplate GetDbTemplate(string configurationId);

        /// <summary>
        /// Выбрать пользователей которым можно предоставить доступ
        /// к информационной базе
        /// </summary>
        /// <param name="usersToProvideAccess">Список Id пользователей
        /// которым нужно предоставить доступ к созданной базе</param>
        /// <returns>Список Id пользователей
        /// которым можно предоставить доступ к созданной базе</returns>
        List<Guid> SelectUsersWhoCanBeGrantedAccess(List<Guid>? usersToProvideAccess);
    }
}
