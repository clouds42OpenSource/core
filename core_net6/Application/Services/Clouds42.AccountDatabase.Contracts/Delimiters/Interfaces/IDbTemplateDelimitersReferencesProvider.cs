﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.IDataModels;
using PagedList.Core;

namespace Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces
{
    /// <summary>
    ///     Управление базами на разделителях.
    /// </summary>
    public interface IDbTemplateDelimitersReferencesProvider
	{

		/// <summary>
		///     Получить коллекцию баз на разделителях
		/// </summary>
		/// <returns></returns>
		IEnumerable<IDbTemplateDelimiters> GetListOfDbTemplateDelimiters();

		/// <summary>
		///     Получить базу на разделителях
		/// </summary>
		/// <returns></returns>
		IDbTemplateDelimiters GetDbTemplateDelimiters(string dbTemplateDelimitersId);

		/// <summary>
		///     Добавить базу на разделителях
		/// </summary>
		/// <param name="newDbTemplateDelimiter">Обьект новой базы на разделителях</param>
		void AddNewDbTemplateDelimiters(IDbTemplateDelimiters newDbTemplateDelimiter);

		/// <summary>
		///     Удалить существующую базу на разделителях
		/// </summary>
		/// <param name="dbTemplateDelimitersId">Название базы на разделителях</param>
		void DeleteDbTemplateDelimiters(string dbTemplateDelimitersId);

		/// <summary>
		///     Обновить существующую базу на разделителях
		/// </summary>
		/// <param name="dbTemplateDelimitersToUpdate">Обновленная база на разделителях</param>
		void EditExistingDbTemplateDelimiters(IDbTemplateDelimiters dbTemplateDelimitersToUpdate);

		/// <summary>
		///     Метод перевода коллекции в нумерованый список
		/// </summary>
		/// <param name="collection">Входящая коллекция с объектами
		/// которые имплементируют IDbTemplateDelimiters</param>
		/// <param name="filter">Параметры фильтрации</param>
		/// <returns></returns>
		IPagedList<IDbTemplateDelimiters> ToPagedList(IEnumerable<IDbTemplateDelimiters> collection,
            PagedListFilter filter);

        /// <summary>
        /// Обновить версию конфигурации для шаблона баз на разделителях.
        /// </summary>
        /// <param name="dbTemplateDelimitersId">Id базы на разделителях</param>
        /// <param name="releaseVersion">версия релиза конфигурации</param>
        void UpdateConfigurationReleaseVersion(string dbTemplateDelimitersId, string releaseVersion);

        /// <summary>
        ///  Обновить минимальную версию конфигурации для шаблона баз на разделителях.
        /// </summary>
        /// <param name="dbTemplateDelimitersId">Id базы на разделителях</param>
        /// <param name="minVersion">версия релиза конфигурации</param>
        void UpdateConfigurationMinVersion(string dbTemplateDelimitersId, string minVersion);

	}
}
