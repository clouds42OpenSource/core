﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;

namespace Clouds42.AccountDatabase.Contracts.Migration.Interfaces
{
    /// <summary>
    /// Провайдер миграции инф. баз
    /// </summary>
    public interface IMigrateAccountDatabaseProvider
    {
        /// <summary>
        /// Перенести инф. базы в новое файловое хранилище
        /// </summary>
        /// <param name="accountDatabaseIds">Список ID инф. баз</param>
        /// <param name="targetFileStorageId">ID файлового хранилища</param>
        /// <param name="accountUserInitiatorId">ID пользователя, инициировавшего миграцию</param>
        /// <returns>Список результатов миграции инф. баз</returns>
        List<DatabaseMigrationResultDto> MigrateAccountDatabases(List<Guid> accountDatabaseIds, Guid targetFileStorageId,
            Guid accountUserInitiatorId);
    }
}
