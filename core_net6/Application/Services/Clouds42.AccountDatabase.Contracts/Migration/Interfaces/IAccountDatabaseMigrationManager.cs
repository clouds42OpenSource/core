﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.AccountDatabase.Contracts.Migration.Interfaces
{
    public interface IAccountDatabaseMigrationManager
    {
        ManagerResult MigrateAccountDatabases(List<Guid> accountDatabaseIds, Guid targetFileStorageId, Guid accountUserInitiatorId);
    }
}