﻿using Clouds42.DataContracts.Segment;

namespace Clouds42.AccountDatabase.Contracts.Migration.Interfaces
{
    /// <summary>
    /// агрегатор для задач миграции
    /// </summary>
    public interface IMigrationTasksAggregator
    {
        /// <summary>
        /// Создание тасок на удаление директорий 
        /// </summary>
        /// <param name="migrateDatabases">список баз которые мигрировали</param>
        /// <param name="databaseToTransfer">список баз для миграции</param>
        /// <param name="oldClientFileFolder">старая клиентская директория</param>
        /// <param name="needMoveClientFiles">нужно ли удалять клиентскую директорию</param>
        /// <returns></returns>
        List<Task> CreateDeletionOfDirectoryTasks(List<RollbackDataDto> migrateDatabases,
            List<Domain.DataModels.AccountDatabase> databaseToTransfer, string oldClientFileFolder, bool needMoveClientFiles);

        /// <summary>
        /// Ожидание выполнения тасок
        /// </summary>
        /// <param name="tasks">список тасок</param>
        /// <param name="messageText">результат удаления директории</param>
        void WaitForCompletionOfTasks(List<Task> tasks, out string messageText);
    }
}

