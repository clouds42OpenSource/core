﻿using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;

namespace Clouds42.AccountDatabase.Contracts.Register
{
    public interface IRegisterDatabaseFromBackupProvider
    {
        void RegisterDatabaseOnServer(RegisterDatabaseModelDto model);
    }
}