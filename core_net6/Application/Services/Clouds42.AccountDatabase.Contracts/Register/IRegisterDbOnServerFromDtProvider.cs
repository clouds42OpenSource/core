﻿using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.Contracts.Register
{
    public interface IRegisterDbOnServerFromDtProvider
    {
        /// <summary>
        /// Зарегистрировать базу данных на сервере из dt файла.
        /// </summary>
        void RegisterDatabaseOnServer(RegisterDatabaseFromUploadFileModelDto createRegisterDatabase);
        
        /// <summary>
        /// Создает кластеризованную базу данных из DT файла
        /// </summary>
        /// <param name="accountDatabase">База данных аккаунта</param>
        /// <param name="uploadedFileId">Идентификатор загруженного файла</param>
        /// <param name="isCopy">Признак создания копии</param>
        public void CreateClusteredWithDt(IAccountDatabase accountDatabase, Guid uploadedFileId, bool isCopy);

    }
}
