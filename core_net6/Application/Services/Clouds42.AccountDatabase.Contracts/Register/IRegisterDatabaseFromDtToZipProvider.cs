﻿using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;

namespace Clouds42.AccountDatabase.Contracts.Register
{
    public interface IRegisterDatabaseFromDtToZipProvider
    {
        /// <summary>
        /// Зарегистрировать базу данных на сервере из dt файла.
        /// </summary>
        void RegisterDatabaseOnServer(RegisterDatabaseFromDTToZipModelDto model);
    }
}
