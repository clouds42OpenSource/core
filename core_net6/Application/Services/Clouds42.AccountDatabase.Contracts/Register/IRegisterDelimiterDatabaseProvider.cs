﻿using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;

namespace Clouds42.AccountDatabase.Contracts.Register
{
    public interface IRegisterDelimiterDatabaseProvider
    {
        void RegisterDatabaseOnServer(RegisterDatabaseOnDelimiterModelDto registerModel);
    }
}