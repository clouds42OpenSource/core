﻿using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;

namespace Clouds42.AccountDatabase.Contracts.Register
{
    public interface IRegisterDatabaseFromTemplateProvider
    {
        /// <summary>
        /// Зарегистрировать файловую базу данных на сервере из шаблона.
        /// </summary>
        void RegisterDatabaseOnServer(RegisterFileDatabaseFromTemplateModelDto registerModel);
    }
}