﻿using Clouds42.DataContracts.Configurations1c.ItsAuthorization;

namespace Clouds42.AccountDatabase.Contracts.ITS.Interfaces
{
    /// <summary>
    /// Интерфейс создания модели данных авторизации в ИТС
    /// </summary>
    public interface ICreateItsAuthorizationDataProvider
    {
        /// <summary>
        /// Создать новые данные авторизации в ИТС
        /// </summary>
        /// <param name="newItsAuthorization">Новый модель данных авторизации в ИТС</param>
        void CreateItsAuthorization(ItsAuthorizationDataDto newItsAuthorization);
    }
}