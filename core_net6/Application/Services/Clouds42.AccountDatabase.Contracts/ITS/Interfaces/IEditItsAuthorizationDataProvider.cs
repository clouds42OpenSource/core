﻿using Clouds42.DataContracts.Configurations1c.ItsAuthorization;

namespace Clouds42.AccountDatabase.Contracts.ITS.Interfaces
{
    /// <summary>
    /// Интерфейс редактирования данных авторизации в ИТС
    /// </summary>
    public interface IEditItsAuthorizationDataProvider
    {
        /// <summary>
        /// Редактировать данные авторизации в ИТС
        /// </summary>
        /// <param name="itsAuthorization">Модель данных авторизации в ИТС</param>
        void EditItsAuthorization(ItsAuthorizationDataDto itsAuthorization);
    }
}