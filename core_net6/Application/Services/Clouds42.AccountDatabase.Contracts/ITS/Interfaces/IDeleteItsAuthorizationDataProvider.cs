﻿namespace Clouds42.AccountDatabase.Contracts.ITS.Interfaces
{
    /// <summary>
    /// Интерфейс для удаления данных авторизации в ИТС
    /// </summary>
    public interface IDeleteItsAuthorizationDataProvider
    {
        /// <summary>
        /// Удалить данные авторизации в ИТС
        /// </summary>
        /// <param name="id">ID данных авторизации в ИТС</param>
        void DeleteItsAuthorization(Guid id);
    }
}