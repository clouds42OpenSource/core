﻿using Clouds42.DataContracts.Configurations1c.ItsAuthorization;
using Clouds42.Domain.DataModels.Security;

namespace Clouds42.AccountDatabase.Contracts.ITS.Interfaces
{
    /// <summary>
    /// Интерфейс получения данных авторизации ИТС
    /// </summary>
    public interface IItsAuthorizationDataProvider
    {
        /// <summary>
        /// Получить список данных авторизации в ИТС с пагинацией
        /// </summary>
        /// <param name="filterData">Модель фильтра для получения списка данных авторизации ИТС</param>
        /// <returns>Список данных авторизации в ИТС с пагинацией</returns>
        ItsAuthorizationDataPaginationDto GetItsDataAuthorizations(ItsAuthorizationFilterDto filterData);

        /// <summary>
        /// Получить модель данных авторизации в ИТС по ID
        /// </summary>
        /// <param name="id">ID модель данных авторизации в ИТС</param>
        /// <returns>Модель модель данных авторизации в ИТС</returns>
        ItsAuthorizationDataDto GetItsAuthorizationDataDtoById(Guid id);

        /// <summary>
        /// Получить доменный модель данных авторизации в ИТС по ID
        /// </summary>
        /// <param name="id">ID данных авторизации в ИТС</param>
        /// <returns>Доменный модель данных авторизации в ИТС</returns>
        ItsAuthorizationData GetItsAuthorizationDataByIdOrThrowException(Guid id);

        /// <summary>
        /// Получить коллекцию авторизации данных в ИТС
        /// </summary>
        /// <param name="searchValue">Значение поиска</param>
        /// <returns>Коллекцию авторизации данных в ИТС</returns>
        ICollection<ItsAuthorizationDataDto> GetItsAuthorizationDataBySearchValue(string searchValue);
    }
}