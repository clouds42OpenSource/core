﻿using Clouds42.DataContracts.AccountDatabase.Interface;

namespace Clouds42.AccountDatabase.Contracts.Check.Interfaces
{
    public interface IAccountDatabaseFileUseCheck
    {
        /// <summary>
        /// Выполнить проверку наличия активных сессий в файловой информационной базе.
        /// </summary>
        /// <returns>true - есть активные сессии, false - активных сессий нет.</returns>
        bool Check(string accountDatabasePath);

        /// <summary>
        /// Ожидать пока база данных файловой базы используется.
        /// </summary>        
        void WaitWhileFileDatabaseUsing(IUpdateDatabaseDto database, string databaseName, string dnsName, int minutes = 15);

        /// <summary>
        /// Ожидать пока база используется
        /// </summary>
        /// <param name="accountDatabasePath">Путь к базе</param>
        /// <param name="minutes">Кол-во минут ожидания</param>
        void WaitWhileFileDatabaseUsing(string accountDatabasePath, int minutes = 15);
    }
}
