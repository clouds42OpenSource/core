﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Processors.Managers
{
    public interface IExecuteProcessForWorkingWithDatabaseManager
    {
        ManagerResult ExecuteProcessOfArchiveDatabaseToTomb(ProcessOfArchiveDatabaseToTombParamsDto processParams);
        ManagerResult ExecuteProcessOfRemoveDatabase(ProcessOfRemoveDatabaseParamsDto processParams);
    }
}