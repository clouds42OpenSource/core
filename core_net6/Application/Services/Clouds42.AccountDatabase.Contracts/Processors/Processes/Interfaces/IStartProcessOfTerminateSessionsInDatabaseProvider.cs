﻿using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;

namespace Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces
{
    /// <summary>
    /// Провайдер для запуска процесса
    /// по завершению сеансов в информационной базе
    /// </summary>
    public interface IStartProcessOfTerminateSessionsInDatabaseProvider
    {
        /// <summary>
        /// Попытаться запустить процесс по завершению
        /// сеансов в информационной базе
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по завершению сеансов в информационной базе</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат успеха запуска процесса</returns>
        bool TryStartProcess(StartProcessOfTerminateSessionsInDatabaseDto model, out string errorMessage, bool waitExecute = false);
    }
}
