﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces
{
    /// <summary>
    /// Провайдер для запуска процесса на архивацию
    /// информационной базы в склеп
    /// </summary>
    public interface IStartProcessOfArchiveDatabaseToTombProvider
    {
        /// <summary>
        /// Запустить процесс на архивацию
        /// информационной базы в склеп
        /// </summary>
        /// <param name="processParams">Модель параметров для запуска процесса на архивацию
        /// информационной базы в склеп</param>
        void StartProcessOfArchiveDatabaseToTomb(StartProcessOfArchiveDatabaseToTombParamsDto processParams);

        /// <summary>
        /// Запустить процессы на архивацию
        /// информационных баз в склеп
        /// </summary>
        /// <param name="databasesId">Список Id баз</param>
        void StartProcessOfArchiveDatabasesToTomb(List<Guid> databasesId);
    }
}
