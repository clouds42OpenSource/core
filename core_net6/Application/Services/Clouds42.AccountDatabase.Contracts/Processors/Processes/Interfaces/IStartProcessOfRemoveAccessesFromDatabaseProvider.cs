﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AcDbAccess;

namespace Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces
{
    /// <summary>
    /// Провайдер для запуска процесса на удаление доступа из базы
    /// </summary>
    public interface IStartProcessOfRemoveAccessesFromDatabaseProvider
    {
        /// <summary>
        /// Запустить процессы на удаление доступов из инф. базы
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        IEnumerable<ManageAccessResultDto> StartProcessesOfRemoveAccesses(StartProcessesOfManageAccessesToDbDto model);

        /// <summary>
        /// Запустить процессы на удаление доступов из из карточки пользователей
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        IEnumerable<ManageAccessResultDto> StartProcessesOfRemoveAccessesForUserCard(
            StartProcessesAccessesToDbForUserCardDto model);
    }
}
