﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces
{
    /// <summary>
    /// Процесс архивации информационной базы в склеп
    /// </summary>
    public interface IArchiveAccountDatabaseToTombProcess
    {
        /// <summary>
        /// Выполнить процесс по архивации
        /// информационной базы в склеп
        /// </summary>
        /// <param name="processParams">Модель параметров процесса на архивацию
        /// информационной базы в склеп</param>
        void ExecuteProcessOfArchiveDatabaseToTomb(ProcessOfArchiveDatabaseToTombParamsDto processParams);

        /// <summary>
        /// Удалить базы на разделителях
        /// </summary>
        /// <param name="database"></param>
        public void DeleteDatabaseOnDelimiterInMs(Domain.DataModels.AccountDatabase database);

    }
}
