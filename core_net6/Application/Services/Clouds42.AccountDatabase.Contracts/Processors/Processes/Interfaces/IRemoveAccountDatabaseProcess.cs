﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces
{
    /// <summary>
    /// Процесс удаления информационной базы
    /// </summary>
    public interface IRemoveAccountDatabaseProcess
    {
        /// <summary>
        /// Выполнить процесс по удалению инф. базы
        /// </summary>
        /// <param name="processParams">Модель параметров процесса удаления инф. базы</param>
        void ExecuteProcessOfRemoveDatabase(ProcessOfRemoveDatabaseParamsDto processParams);
    }
}
