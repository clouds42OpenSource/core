﻿namespace Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces
{
    /// <summary>
    /// Провайдер для запуска процесса на удаление инф. базы
    /// </summary>
    public interface IStartProcessOfRemoveDatabaseProvider
    {
        /// <summary>
        /// Запустить процесс удаления инф. базы
        /// </summary>
        /// <param name="databaseId">Id базы</param>
        void StartProcessOfRemoveDatabase(Guid databaseId);

        /// <summary>
        /// Запустить процессов удаления инф. баз
        /// </summary>
        /// <param name="databasesId">Id баз</param>
        void StartProcessOfRemoveDatabases(List<Guid> databasesId);
    }
}
