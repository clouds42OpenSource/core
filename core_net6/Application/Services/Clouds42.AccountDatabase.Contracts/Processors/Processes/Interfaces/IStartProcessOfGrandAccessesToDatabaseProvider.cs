﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AcDbAccess;

namespace Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces
{
    /// <summary>
    /// Провайдер для запуска процесса на выдачу доступа в базу
    /// </summary>
    public interface IStartProcessOfGrandAccessesToDatabaseProvider
    {
        /// <summary>
        /// Запустить процессы на выдачу внутренних доступов в инф. базу
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        IEnumerable<ManageAccessResultDto> StartProcessesOfGrandInternalAccesses(StartProcessesOfManageAccessesToDbDto model);

        /// <summary>
        /// Запустить процессы на выдачу доступов из карточки пользователя
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        IEnumerable<ManageAccessResultDto> StartProcessesOfAccessesForUserCard(StartProcessesAccessesToDbForUserCardDto model);
    }
}
