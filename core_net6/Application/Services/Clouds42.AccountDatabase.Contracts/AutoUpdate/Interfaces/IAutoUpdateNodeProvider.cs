﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces
{
    public interface IAutoUpdateNodeProvider
    {
        /// <summary>
        /// Создать ноду ао
        /// </summary>
        Task CreateAsync(AutoUpdateNode node);
        void Create(AutoUpdateNode node);

        /// <summary>
        /// удалить ноду ао
        /// </summary>
        Task DeleteAsync(Guid id);
        void Delete(Guid id);

        /// <summary>
        /// получить все ноды ао
        /// </summary>
        Task<List<AutoUpdateNode>> GetAllAsync();
        List<AutoUpdateNode> GetAll();

        /// <summary>
        /// получить по айди ноду ао
        /// </summary>
        Task<AutoUpdateNode> GetByIdAsync(Guid id);
        AutoUpdateNode GetById(Guid id);

        /// <summary>
        /// обновить ноду ао
        /// </summary>
        Task UpdateAsync(AutoUpdateNode autoUpdateNode);
        void Update(AutoUpdateNode autoUpdateNode);
    }
}
