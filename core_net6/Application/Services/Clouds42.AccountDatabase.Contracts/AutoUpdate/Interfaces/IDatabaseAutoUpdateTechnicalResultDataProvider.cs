﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries;
using PagedListExtensionsNetFramework;

namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными
    /// технического результата АО инф. базы
    /// </summary>
    public interface IDatabaseAutoUpdateTechnicalResultDataProvider
    {
        /// <summary>
        /// Получить данные технических результатов АО инф. базы 
        /// </summary>
        /// <param name="filter">Модель фильтра</param>
        /// <returns>Список технических результатов АО инф. базы</returns>
        PagedDto<DatabaseAutoUpdateTechnicalResultDataDto> GetData(
            GetTechnicalResultsQuery filter);
    }
}
