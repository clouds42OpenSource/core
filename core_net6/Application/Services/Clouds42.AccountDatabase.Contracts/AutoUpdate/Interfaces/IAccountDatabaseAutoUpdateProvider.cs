﻿using Clouds42.AccountDatabase.Contracts.AutoUpdate.Models;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;

namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces
{
    /// <summary>
    /// Провайдер автообновления инф. баз
    /// </summary>
    public interface IAccountDatabaseAutoUpdateProvider
    {
        /// <summary>
        /// Выполнить автообновление
        /// </summary>
        /// <param name="accountDatabaseAutoUpdateParams">Параметры автообновления инф. базы</param>
        /// <returns>Результат выполнения</returns>
        AccountDatabaseAutoUpdateResultDto ProcessAutoUpdate(AccountDatabaseAutoUpdateParamsDto accountDatabaseAutoUpdateParams);

        /// <summary>
        /// Обработать результаты проведения автообновления
        /// </summary>
        void HandleSupportResults();

        /// <summary>
        /// получить отчёт из папки
        /// </summary>
        Obnovlyator1CReportDto GetEncodeReport(DirectoryInfo dir);

        /// <summary>
        /// обработать результат АО
        /// </summary>
        /// <param name="reportObj"></param>
        /// <param name="manualUpdateFlag"></param>
        void HandleAutoUpdateResult(Obnovlyator1CReportDto reportObj, bool manualUpdateFlag = false);

        /// <summary>
        /// удалить отчёты
        /// </summary>
        void DeleteAllReports(UpdateNodeDto node);

        /// <summary>
        /// 
        /// </summary>
        void DeleteAllBasesFromObnovlyator(UpdateNodeDto node);
    }
}
