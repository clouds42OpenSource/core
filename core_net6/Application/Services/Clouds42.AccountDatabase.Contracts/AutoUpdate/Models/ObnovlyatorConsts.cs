﻿namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Models
{
    public static class ObnovlyatorConsts
    {
        public const string ReportsCopyDirName = "reportsCopy";
        public const string AllReportDirName = "_all_";
        public const string CleanTempFilesDirName = "clean-temp-files";
        public const string ImportFolderPath = @"\Data\Import";
        public const string ReportFolderPath = @"\Data\Reports";
    }
}
