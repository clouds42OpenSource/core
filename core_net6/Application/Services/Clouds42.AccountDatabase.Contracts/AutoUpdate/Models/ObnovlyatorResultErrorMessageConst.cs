﻿namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Models
{
    public static class ObnovlyatorResultErrorMessageConst
    {
        public static string Authorization { get; set; } = "Не удалось подключиться к базе, возможные причины:";

        public static string HasModifications { get; set; } = "Не удалось провести обновление конфигурации на очередную версию (1cv8.cfu)";

        public static string ActiveSession1 { get; set; } = "Не удалось создать резервную копию - Процесс не может получить доступ к файлу";

        public static string ActiveSession2 { get; set; } = "Не удалось установить блокировку базы: 'В базе всё ещё работают пользователи";

        public static string ActiveSession3 { get; set; } = "Не удалось создать резервную копию - получили код ошибки 2";

    }
}
