﻿
namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Models
{
    /// <summary>
    /// Модель отчёта обновлятора
    /// </summary>
    public class Obnovlyator1CReportDto
    {
        public string NameOfBase { get; set; }
        public string PathOfBase { get; set; }
        public string KindOfOperation { get; set; }
        public string StartOfOperation { get; set; }
        public string NameOfOperation { get; set; }
        public string StatusOfOperation { get; set; }
        public bool Skipped { get; set; }
        public bool WereErrors { get; set; }
        public bool WereWarnings { get; set; }
        public string ErrorMessage { get; set; }

        public List<InstalledUpdate> InstalledUpdates { get; set; }
        public List<CreatedBackup> CreatedBackups { get; set; }
        public List<string> InstalledPatches { get; set; }
        public List<string> RevokedPatches { get; set; }


        public string PlainStatusOfOperation { get; set; }
        public string IndexOfKindOfOperation { get; set; }
        public string IndexStartOfOperation { get; set; }
        public string IndexOfStatusOfOperation { get; set; }
        public string IndexOfWereErrors { get; set; }
        public string IndexOfWereWarnings { get; set; }
        public string IndexOfCreatedBackups { get; set; }
    }

    public class InstalledUpdate
    {
        public string Path { get; set; }
        public string ConfigVersion { get; set; }
        public string VendorName { get; set; }
        public string ConfigName { get; set; }
    }
    public class CreatedBackup
    {
        public string PathOfBackup { get; set; }
        public object CloudIdentifiers { get; set; }
    }

}
