﻿
namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Models
{
    /// <summary>
    /// Модель удаления баз в обновлятору
    /// </summary>
    public class Obnovlyator1CDeleteDto
    {
        public string Operation { get; set; }
    }

}
