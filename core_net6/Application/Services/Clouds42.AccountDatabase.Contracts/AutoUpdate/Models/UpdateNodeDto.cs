﻿namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Models
{
    public class UpdateNodeDto
    {

        /// <summary>
        /// id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Адрес ноды
        /// </summary>
        public string NodeAddress { get; set; }

        /// <summary>
        /// Путь до папки с импортом обновлятора
        /// </summary>
        public string ImportFolderPath { get; set; }

        /// <summary>
        /// Путь до папки с импортом обновлятора
        /// </summary>
        public string ReportFolderPath { get; set; }
    }
}
