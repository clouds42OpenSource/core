﻿
using Clouds42.Common.DataModels;

namespace Clouds42.AccountDatabase.Contracts.AutoUpdate.Models
{
    /// <summary>
    /// Модель импорта в обновлятор
    /// </summary>
    public class Obnovlyator1CImportDto
    {
        public string Operation { get; set; }
        public BaseSettings BaseSettings { get; set; }
    }


    public class Obnovlyator1CPlatformDto 
    {
        public string Template { get; set; }
        public string Bitness { get; set; }
        public string Type { get; set; }
    }

    public class Obnovlyator1CCluster
    {
        public CommonLoginModelDto Admin { get; set; }
        public string LstConfigUrl { get; set; }
    }

    public class Obnovlyator1CDatabaseServer
    {
        public string SqlType { get; set; }
        public string AddressForUpdater { get; set; }
        public string AddressForCluster { get; set; }
        public CommonLoginModelDto Admin { get; set; }
        public bool SqlServerIsOtherMachine { get; set; }
    }

    public class Obnovlyator1CSqlBackupSettings
    {
        public bool CompressBackup { get; set; }
        public bool CompressByUpdater { get; set; }
        public bool OptimizeClusterTestForSqlBackup { get; set; }
        public bool IsDtBackupDisabled { get; set; }
    }

    public class BaseSettings
    {
        public string Group { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Version { get; set; }
        public CommonLoginModelDto User { get; set; }
        public Obnovlyator1CPlatformDto Platform { get; set; }
        public string Comment { get; set; }
        public string LaunchParameters { get; set; }
        public string OverrideConfigurationUpdatesDirectory { get; set; }
        public string MaxVersion { get; set; }
        public Obnovlyator1CCluster Cluster { get; set; }
        public Obnovlyator1CDatabaseServer DatabaseServer { get; set; }
        public Obnovlyator1CSqlBackupSettings SqlBackupSettings { get; set; }
        public bool CompleteSession { get; set; }
    }
}
