﻿namespace Clouds42.AccountDatabase.Contracts.IISApplication.Interfaces
{
    /// <summary>
    /// Провайдер для управления пулом приложения на ноде публикаций (на IIS)
    /// </summary>
    public interface IManageIisApplicationPoolProvider
    {
        /// <summary>
        /// Перезапустить пул приложения на указанных нодах
        /// </summary>
        /// <param name="appPoolName">Название пула</param>
        /// <param name="contentServerId">ID сервера публикаций</param>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        void Restart(string appPoolName, Guid contentServerId, Guid databaseId, string databaseNumber);

        /// <summary>
        /// Запустить пул приложения для инф. базы на всех нодах публикации
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        void StartForDatabase(Guid databaseId, string databaseNumber);

        /// <summary>
        /// Остановить пул приложения для инф. базы на всех нодах публикации
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        void StopForDatabase(Guid databaseId, string databaseNumber);
    }
}
