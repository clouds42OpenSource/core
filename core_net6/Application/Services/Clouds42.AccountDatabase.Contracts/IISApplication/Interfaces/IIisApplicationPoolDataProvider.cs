﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Service.WebAdministration;

namespace Clouds42.AccountDatabase.Contracts.IISApplication.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными пула приложения на ноде публикаций (на IIS)
    /// </summary>
    public interface IIisApplicationPoolDataProvider
    {
        /// <summary>
        /// Получить данные для перезапуска пула приложения инф. базы на нодах публикации
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        /// <returns>Данные для для перезапуска пула приложения инф. базы на нодах публикации</returns>
        AcDbDataForManageIisAppPoolDto GetDataForManageDatabaseIisAplicationPool(Guid databaseId, string databaseNumber);

        /// <summary>
        /// Получить название пула инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        /// <returns>Название пула инф. базы</returns>
        string GetAccountDatabaseAppPoolName(Guid accountDatabaseId, string databaseNumber);

        /// <summary>
        /// Получить состояние пула инф. базы на всех нодах публикации
        /// </summary>
        /// <param name="contentServerId">ID сервера публикаций</param>
        /// <param name="appPoolName">Название пула инф. базы</param>
        /// <param name="databaseNumber">Номер инф. базы</param>
        /// <returns>Состояние пула инф. базы на всех нодах публикации</returns>
        List<AppPoolStateDto> GetApplicationPoolStateOnAllPublishNodes(Guid contentServerId, string appPoolName, string databaseNumber);
    }
}
