﻿namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models
{
    public class Root
    {
        public List<SiteModel> Websites { get; set; }
        public List<SiteApplicationModel> Webapps { get; set; }
        public List<ApplicationPool> AppPools { get; set; }
        public List<Rule> Rules { get; set; }
    }
}
