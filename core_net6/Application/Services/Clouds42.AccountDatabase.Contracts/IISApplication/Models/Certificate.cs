﻿using Newtonsoft.Json;

namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models;

public class Certificate
{
    public string Alias { get; set; }
    public string Id { get; set; }
    public string IssuedBy { get; set; }
    public string Subject { get; set; }
    public string Thumbprint { get; set; }
    public DateTime ValidTo { get; set; }

    [JsonProperty("_links")]
    public Links Links { get; set; }
}
