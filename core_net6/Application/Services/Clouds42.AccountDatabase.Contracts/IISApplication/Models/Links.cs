﻿using Newtonsoft.Json;

namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models
{
    public class BaseUrl
    {
        public string Href { get; set; }
    }
    
    public class Links
    {
        public BaseUrl Self { get; set; }
        public BaseUrl Authentication { get; set; }
        public BaseUrl Authorization { get; set; }
        public BaseUrl DefaultDocument { get; set; }
        public BaseUrl Delegation { get; set; }
        public BaseUrl DirectoryBrowsing { get; set; }
        public BaseUrl Files { get; set; }
        public BaseUrl Handlers { get; set; }
        public BaseUrl HttpRedirect { get; set; }
        public BaseUrl IpRestrictions { get; set; }
        public BaseUrl Logging { get; set; }
        public BaseUrl Modules { get; set; }
        public BaseUrl Monitoring { get; set; }
        public BaseUrl RequestFiltering { get; set; }
        public BaseUrl RequestTracing { get; set; }
        public BaseUrl Requests { get; set; }
        public BaseUrl ResponseCompression { get; set; }
        public BaseUrl ResponseHeaders { get; set; }
        public BaseUrl Ssl { get; set; }
        public BaseUrl StaticContent { get; set; }
        public BaseUrl UrlRewrite { get; set; }
        public BaseUrl Vdirs { get; set; }
        public BaseUrl Webapps { get; set; }
        public BaseUrl Websites { get; set; }

        [JsonProperty("anonymous")]
        public BaseUrl AnonymousAuth { get; set; }

        [JsonProperty("digest")]
        public BaseUrl DigestAuth { get; set; }
        [JsonProperty("windows")]
        public BaseUrl WindowsAuth { get; set; }

        [JsonProperty("basic")]
        public BaseUrl BasicAuth { get; set; }
        [JsonProperty("rules")]
        public BaseUrl Rules { get; set; }
    }
}
