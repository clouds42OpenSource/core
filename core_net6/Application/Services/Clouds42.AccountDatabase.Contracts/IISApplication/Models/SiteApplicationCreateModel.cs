﻿namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models
{
    public class SiteApplicationCreateModel
    {
        public string Path { get; set; }
        public string PhysicalPath { get; set; }
        public WebsiteModel Website { get; set; }
        public  ApplicationPool ApplicationPool { get; set; }
    }
}
