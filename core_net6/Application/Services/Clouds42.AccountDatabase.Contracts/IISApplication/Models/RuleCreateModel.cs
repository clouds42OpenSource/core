﻿namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models
{
    public class RuleCreateModel
    {
        public string AccessType { get; set; }
        public Authentication Authorization { get; set; }
        public string Users { get; set; }
    }
}
