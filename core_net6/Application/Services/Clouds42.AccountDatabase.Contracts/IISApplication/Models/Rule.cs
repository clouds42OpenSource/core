﻿using Newtonsoft.Json;

namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models
{
    public class Rule
    {
        public string Id { get; set; }
        public string Users { get; set; }
        public string Roles { get; set; }
        public string Verbs { get; set; }
        public string AccessType { get; set; }
        [JsonProperty("_links")]
        public Links Links { get; set; }
    }
}
