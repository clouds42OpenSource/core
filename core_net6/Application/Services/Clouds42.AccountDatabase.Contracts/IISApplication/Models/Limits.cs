﻿namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models;

public class Limits
{
    public string ConnectionTimeout { get; set; }
    public string MaxBandWidth { get; set; }
    public string MaxConnections { get; set; }
    public string MaxUrlSegments { get; set; }
}
