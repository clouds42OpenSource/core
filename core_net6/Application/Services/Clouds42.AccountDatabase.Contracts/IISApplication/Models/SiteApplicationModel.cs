﻿using Newtonsoft.Json;

namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models
{
    public class SiteApplicationModel
    {
        public string Location { get; set; }
        public string Path { get; set; }
        public string Id { get; set; }
        public ApplicationPool ApplicationPool { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }
}
