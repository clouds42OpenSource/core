﻿namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models;

public class Binding
{
    public string Protocol { get; set; }
    public string BindingInformation { get; set; }
    public string IpAddress { get; set; }
    public string Port { get; set; }
    public string Hostname { get; set; }
    public Certificate Certificate { get; set; }
    public string RequireSni { get; set; }
}
