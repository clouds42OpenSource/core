﻿using Newtonsoft.Json;

namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models
{
    public class SiteModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string PhysicalPath { get; set; }
        public string Key { get; set; }
        public string Status { get; set; }
        public string ServerAutoStart { get; set; }
        public string EnabledProtocols { get; set; }
        public Limits Limits { get; set; }
        public List<Binding> Bindings { get; set; }
        public ApplicationPool ApplicationPool { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }
}
