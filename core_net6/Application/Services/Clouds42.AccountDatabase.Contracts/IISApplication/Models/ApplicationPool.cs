﻿using Newtonsoft.Json;

namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models;

public class ApplicationPool
{
    public string Name { get; set; }
    public string Id { get; set; }
    public string Status { get; set; }

    [JsonProperty("_links")]
    public Links Links { get; set; }
}
