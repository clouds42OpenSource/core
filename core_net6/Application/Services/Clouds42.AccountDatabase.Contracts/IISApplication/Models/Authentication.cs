﻿using Newtonsoft.Json;

namespace Clouds42.AccountDatabase.Contracts.IISApplication.Models
{
    public class Authentication
    {
        public string Id { get; set; }
        public string Scope { get; set; }
        public string Enabled { get; set; }
        public WebsiteModel Website { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }
    }
}
