﻿namespace Clouds42.AccountDatabase.Contracts.IISApplication.Constants
{
    public static class IISLinksConstants
    {
        public const string WebSites = "/api/webserver/websites";
        public const string WebApps = "/api/webserver/webapps";
        public const string AppPools = "/api/webserver/application-pools";
        public const string AnonAuth = "/api/webserver/authentication/anonymous-authentication";
        public const string BasicAuth = "/api/webserver/authentication/basic-authentication";
        public const string Rules = "/api/webserver/authorization/rules";
    }
}
