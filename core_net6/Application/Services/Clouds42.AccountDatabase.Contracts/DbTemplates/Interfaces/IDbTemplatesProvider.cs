﻿using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.DbTemplates.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с шаблонами
    /// </summary>
    public interface IDbTemplatesProvider
    {
        /// <summary>
        /// Получить все шаблоны ввиде key - ID шаблона, value - описание шаблона
        /// </summary>
        /// <returns>Все шаблоны ввиде key - ID шаблона, value - описание шаблона</returns>
        KeyValuePair<Guid, string>[] GetAllDbTemplatesAsKeyValue();

        /// <summary>
        /// Получить данные для редактирования шаблона
        /// </summary>
        /// <param name="args">Аргументы поиска шаблона для редактирования</param>
        /// <returns>Данные для редактирования шаблона</returns>
        DbTemplateToEditDataDto GetDbTemplateDataToEdit(DbTemplateEditDto args);

        /// <summary>
        /// Получить все шаблоны инф. базы
        /// </summary>
        /// <returns>Все шаблоны инф. базы</returns>
        IOrderedQueryable<DbTemplate> GetAllDbTemplates();

        /// <summary>
        /// Добавить шаблон
        /// </summary>
        /// <param name="itemToAdd">Данные для добавления шаблона</param>
        /// <returns>Данные после добавления</returns>
        AddDbTemplateResultDto AddDbTemplateItem(DbTemplateItemDto itemToAdd);

        /// <summary>
        /// Обновить шаблон
        /// </summary>
        /// <param name="itemToUpdate">Данные для обновления шаблона</param>
        void UpdateDbTemplateItem(DbTemplateItemDto itemToUpdate);

        /// <summary>
        /// Удалить шаблон
        /// </summary>
        /// <param name="args">Данные для удаления шаблона</param>
        void DeleteDbTemplateItem(DeleteDbTemplateDataItemDto args);

        /// <summary>
        /// Получить коллекцию шаблонов
        /// </summary>
        /// <param name="args">Параметры поиска получения коллекции шаблонов</param>
        /// <returns>Список шаблонов с пагинацией</returns>
        DbTemplatesDataResultDto GetDbTemplatesItems(GetDbTemplatesParamsDto args);

        /// <summary>
        /// Найти шаблон по имени
        /// </summary>
        /// <param name="dbTemplateName">Название шаблона</param>
        /// <returns>Возвращает найденный шаблон</returns>
        DbTemplateItemDto GetDbTemplateByName(string dbTemplateName);

        /// <summary>
        /// Найти шаблон по ID
        /// </summary>
        /// <param name="dbTemplateId">ID шаблона</param>
        /// <returns>Возвращает true при успешном удалении</returns>
        DbTemplateItemDto GetDbTemplateById(Guid dbTemplateId);

        /// <summary>
        /// Получить список названий конфигураций
        /// </summary>
        /// <returns>список названий конфигураций</returns>
        List<string> GetConfiguration1CName();
    }
}
