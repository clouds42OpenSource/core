﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces
{
    /// <summary>
    /// Провадйер планирования ТиИ информационной базы
    /// </summary>
    public interface IPlanTehSupportAccountDatabaseProvider
    {
        /// <summary>
        /// Запланировать проведение ТиИ для информационной базы.
        /// </summary>
        /// <param name="support">Модель поддержки инф. базы</param>
        /// <returns>Результат планирования</returns>
        bool PlanTehSupportAccountDatabase(AcDbSupport support);
    }
}