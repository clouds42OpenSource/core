﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces
{
    /// <summary>
    /// Провайдер планирования обновления версии информационной базы.
    /// </summary>
    public interface IPlanUpdateVersionAccountDatabaseProvider
    {
        /// <summary>
        /// Запланировать обновление версии ИБ
        /// </summary>
        /// <param name="support">Модель поддержки инф. базы</param>
        /// <returns>Результат планирования</returns>
        bool PlanUpdateVersionAccountDatabase(AcDbSupport support);

        /// <summary>
        /// возможно ли подключить автообновление
        /// </summary>
        /// <param name="support"></param>
        /// <returns></returns>
        bool CanPlugAutoUpdate(AcDbSupport support);
    }
}
