﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using PagedListExtensionsNetFramework;

namespace Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces
{
    /// <summary>
    /// Провайдер данных для планирования поддержки инф. баз
    /// </summary>
    public interface IPlanSupportAccountDatabaseDataProvider
    {
        /// <summary>
        /// Получить постраничный список информационых баз, для которых нужно спланировать ТиИ.
        /// </summary>
        /// <param name="pageNumber">Номер запрашиваемой страницы.</param>
        /// <returns>Список инф. баз</returns>
        List<AccountDatabasesOfAccountModelDto> GetAcDatabasesForPlanTarWithPagination(int pageNumber);

        /// <summary>
        /// Получить постраничный список информационых баз, для которых нужно спланировать АО.
        /// </summary>
        /// <param name="pageNumber">Номер запрашиваемой страницы.</param>
        /// <returns>Список инф. баз</returns>
        PagedDto<AutoUpdateAccountDatabaseDto> GetAcDatabasesForPlanAuWithPagination(
            int pageNumber);
    }
}
