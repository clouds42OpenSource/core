﻿namespace Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces
{
    /// <summary>
    /// Провайдер планировния поддержки инф. баз
    /// </summary>
    public interface IPlanSupportAccountDatabaseProvider
    {
        /// <summary>
        /// Запустить планирование
        /// </summary>
        void RunPlaning();
    }
}