﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.History.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными
    /// истории технической поддержки инф. базы
    /// </summary>
    public interface IAcDbSupportHistoryDataProvider
    {
        /// <summary>
        /// Получить актуальную историю технической
        /// поддержки инф. базы по каждой операции
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <returns>Актуальная история технической
        /// поддержки инф. базы по каждой операции</returns>
        IQueryable<AcDbSupportHistory> GetActualAcDbSupportHistories(Guid databaseId);
    }
}
