﻿using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;

namespace Clouds42.AccountDatabase.Contracts.Tomb.Interfaces
{
    /// <summary>
    /// Провайдер для создания задач по управлению инф. базой в склепе
    /// </summary>
    public interface ITombTaskCreatorProvider
    {
        /// <summary>
        /// Создать новую задачу воркера на удаление в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        /// <param name="delayDateTime"></param>
        void CreateTaskForDeleteAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParam, DateTime? delayDateTime = null);

        /// <summary>
        /// Создать новую задачу воркера на удаление в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        /// <param name="delayDateTime"></param>
        /// <returns>Created task identifier</returns>
        Guid CreateTaskForDeleteAccountDatabaseToTombForBackup(DeleteAccountDatabaseToTombJobParamsDto taskParam, DateTime? delayDateTime = null);

        /// <summary>
        /// Создать новую задачу воркера на архивацию в склепе
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        void CreateTaskForArchivateAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParam);

        /// <summary>
        /// Создать новую задачу воркера на восстановление информационной базы из склепа
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        void CreateTaskForRestoreAccountDatabaseFromTomb(RestoreAccountDatabaseFromTombWorkerTaskParam taskParam);

        /// <summary>
        /// Создать новую задачу воркера на отправку бэкапа информационной базы в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        void CreateTaskForSendBackupAccountDatabaseToTomb(SendBackupAccountDatabaseToTombJobParamsDto taskParam);

        /// <summary>
        /// Создать новую задачу воркера на архивацию файлов аккаунта в склеп
        /// </summary>
        /// <param name="taskParam">Параметры задачи</param>
        void CreateTaskForArchivateAccountFilesToTomb(DeleteAccountFilesToTombJobParamsDto taskParam);
    }
}
