﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Tomb.Interfaces
{
    public interface ITombManager
    {
        /// <summary>
        /// Скачать архив со склепа.
        /// </summary>
        /// <param name="accountDatabaseBackup">Данные бэкапа.</param>
        ManagerResult<string> DownloadBackup(AccountDatabaseBackup accountDatabaseBackup);

        /// <summary>
        ///     Перенести бэкап информационной базы в склеп
        /// </summary>
        /// <param name="accountDatabaseId">Текущая информационная база</param>
        /// <param name="accountDatabaseBackupId">ID бэкапа информационной базы</param>
        /// <param name="forceUpload">Принудительная загрузка файла</param>
        ManagerResult<bool> MoveBackupToTomb(Guid accountDatabaseId, Guid accountDatabaseBackupId, bool forceUpload);
    }
}
