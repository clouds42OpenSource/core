﻿using Clouds42.DataContracts.Service.WebAdministration;

namespace Clouds42.AccountDatabase.Contracts.WebAdministration.Interfaces
{
    /// <summary>
    /// Исполнитель команд веб администрирования через PowerShell
    /// </summary>
    public interface IWebAdministrationPowerShellCommandExecutor
    {
        /// <summary>
        /// Выполнить команду в нескольких потоках
        /// </summary>
        /// <typeparam name="TParam">Тип параметров команды</typeparam>
        /// <typeparam name="TResult">Тип результата</typeparam>
        /// <typeparam name="TCommand">Тип команды</typeparam>
        /// <param name="contentServerNodes">Ноды сервера публикаций</param>
        /// <param name="commandParam">Параметры команды</param>
        /// <returns>Результат выполнения</returns>
        List<CommandExecutionResultDto<TResult>> ExcecuteCommandInMultipleThreads<TCommand, TParam, TResult>(
            IEnumerable<string> contentServerNodes, TParam commandParam)
            where TCommand : IWebAdministrationPowerShellCommand<TParam, TResult>;
    }
}
