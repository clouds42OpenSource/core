﻿using Clouds42.DataContracts.Service.WebAdministration;

namespace Clouds42.AccountDatabase.Contracts.WebAdministration.Interfaces
{
    /// <summary>
    /// Команда веб администрирования PowerShell
    /// </summary>
    /// <typeparam name="TParam">Тип параметров команды</typeparam>
    /// <typeparam name="TResult">Тип результата выполнения</typeparam>
    public interface IWebAdministrationPowerShellCommand<in TParam, TResult>
    {
        /// <summary>
        /// Выполнить команду и зафиксировать время ее выполнения
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <param name="contentServerNodeAddress">Адрес ноды публикации</param>
        /// <returns>Результат выполнения</returns>
        CommandExecutionResultDto<TResult> ExecuteWithCountingExecutionTime(TParam commandParams,
            string contentServerNodeAddress);
    }
}
