﻿namespace Clouds42.AccountDatabase.Contracts.UploadZipAccount.Interfaces
{
    /// <summary>
    /// Провайдер для загрузки ZIP файлов инф. базы
    /// </summary>
    public interface IUploadAccountDatabaseZipFileProvider
    {
        /// <summary>
        /// Загрузить ZIP файл по частям
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>ID загруженного файла от МС</returns>
        Guid UploadZipFileByChunks(Guid uploadedFileId);
    }
}
