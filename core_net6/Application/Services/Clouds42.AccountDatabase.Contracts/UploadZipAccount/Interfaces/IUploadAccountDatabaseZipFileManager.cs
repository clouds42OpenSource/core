﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.AccountDatabase.Contracts.UploadZipAccount.Interfaces
{
    public interface IUploadAccountDatabaseZipFileManager
    {
        /// <summary>
        /// Загрузить ZIP файл по частям
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>ID загруженного файла от МС</returns>
        ManagerResult<Guid> UploadZipFileByChunks(Guid uploadedFileId);
    }
}
