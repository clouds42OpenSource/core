﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;

namespace Clouds42.AccountDatabase.Contracts.Data.Interfaces
{
    /// <summary>
    /// Провайдер для работы с общими/базовыми
    /// данными нформационных баз
    /// </summary>
    public interface IAccountDatabasesCommonDataProvider
    {
        /// <summary>
        /// Получить общие/базовые данные
        /// для работы с информационными базами
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Общие/базовые данные для работы с информационными базами</returns>
        CommonDataForWorkingWithDatabasesDto GetCommonDataForWorkingWithDatabases(Guid accountId, Guid accountUserId);
    }
}
