﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;

namespace Clouds42.AccountDatabase.Contracts.Data.Interfaces
{
    /// <summary>
    /// Провайдер данных для создания инф. баз
    /// </summary>
    public interface ICreateAccountDatabaseDataProvider
    {
        /// <summary>
        /// Получить данные для создания инф. базы из шаблона
        /// </summary>
        /// <returns>Данные для создания инф. базы из шаблона</returns>
        Task<CreateAcDbFromTemplateDataDto> GetDataForCreateAcDbFromTemplate(Guid accountId, bool onlyTemplatesOnDelimiters);

        /// <summary>
        /// Получить список пользователей для предоставления доступа
        /// </summary>
        /// <returns>Список пользователей для предоставления доступа</returns>
        List<UserInfoDataDto> GetAvailableUsersForGrantAccess(Guid accountId);

        /// <summary>
        /// Получить список пользователей для предоставления доступа в базу, восстанавливаемую из бекапа
        /// </summary>
        /// <param name="backupId">Id бекапа информационной базы</param>
        /// <returns>Модель данных для восстановления инф. базы из бекапа</returns>
        RestoreAcDbFromBackupDataDto GetAvailableUsersForGrantAccessToRestoringDb(Guid backupId);
    }
}
