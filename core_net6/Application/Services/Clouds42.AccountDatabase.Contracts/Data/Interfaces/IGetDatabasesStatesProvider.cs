﻿using Clouds42.AccountDatabase.Contracts.Models;

namespace Clouds42.AccountDatabase.Contracts.Data.Interfaces
{
    /// <summary>
    /// Провайдер для получения статусов баз
    /// </summary>
    public interface IGetDatabasesStatesProvider
    {
        /// <summary>
        /// Получить статусы по запрашиваемым базам
        /// </summary>
        /// <param name="databasesIds">Массив Id баз</param>
        /// <returns>Статусы по запрашиваемым базам</returns>
        List<AccountDatabaseDc> GetAccountDatabaseStates(Guid[] databasesIds);
    }
}
