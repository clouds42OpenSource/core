﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;

namespace Clouds42.AccountDatabase.Contracts.Data.Interfaces
{
    /// <summary>
    /// Провайдер для работы с дополнительными
    /// данными информационных баз
    /// </summary>
    public interface IAccountDatabasesAdditionalDataProvider
    {
        /// <summary>
        /// Получить дополнительные данные для работы с информационными базами
        /// </summary>
        /// <returns>Дополнительные данные для работы с информационными базами</returns>
        AdditionalDataForWorkingWithDatabasesDto GetAdditionalDataForWorkingWithDatabases();
    }
}
