﻿namespace Clouds42.AccountDatabase.Contracts.Data.Interfaces
{
    /// <summary>
    /// Провайдер активации сервиса Аренда 1С при создании инф. базы
    /// </summary>
    public interface IActivateRent1CAtCreationDatabaeProvider
    {
        /// <summary>
        /// Активировать если необходимо
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        void ActivateIfNeeded(Guid accountId);
    }
}
