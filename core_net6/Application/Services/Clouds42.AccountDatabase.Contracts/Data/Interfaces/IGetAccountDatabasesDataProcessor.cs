﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;

namespace Clouds42.AccountDatabase.Contracts.Data.Interfaces
{
    /// <summary>
    /// Процессор для получения данных информационных баз
    /// </summary>
    public interface IGetAccountDatabasesDataProcessor
    {
        /// <summary>
        /// Получить данные информационных баз
        /// по фильтру
        /// </summary>
        /// <param name="args">Модель фильтра для выбора баз</param>
        /// <returns>Данные информационных баз</returns>
        AccountDatabaseDataDto GetDatabasesDataByFilter(AccountDatabasesFilterParamsDto args);
    }
}
