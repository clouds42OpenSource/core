﻿using Clouds42.Domain.DataModels.AccountDatabases;

namespace Clouds42.AccountDatabase.Contracts.TerminateSessions
{
    /// <summary>
    /// Провайдер для работы с данными
    /// завершения сеансов в информационной базе
    /// </summary>
    public interface ITerminationSessionsInDatabaseDataProvider
    {
        /// <summary>
        /// Получить запись о завершении сеансов
        /// в информационной базе или выкинуть исключение
        /// </summary>
        /// <param name="id">Id записи/процесса завершения</param>
        /// <returns>Запись о завершении сеансов
        /// в информационной базе</returns>
        TerminationSessionsInDatabase GetTerminationSessionsInDatabaseOrThrowException(Guid id);

        /// <summary>
        /// Получить информационную базу по Id процессу завершения сеансов
        /// </summary>
        /// <param name="id">Id записи/процесса завершения</param>
        /// <returns>Информационная база</returns>
        Domain.DataModels.AccountDatabase GetDatabaseByTerminationSessionsIdOrThrowException(Guid id);

        /// <summary>
        /// Проверить существует ли процесс завершения сеансов в базе данных
        /// Запись со статусом "В процессе завершения сеансов"
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Признак указывающий что существует
        /// процесс завершения сеансов в базе данных</returns>
        bool IsExistTerminatingSessionsProcessInDatabase(Guid databaseId);
    }
}
