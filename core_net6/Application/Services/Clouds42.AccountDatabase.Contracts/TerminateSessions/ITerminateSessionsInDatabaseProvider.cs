﻿using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;

namespace Clouds42.AccountDatabase.Contracts.TerminateSessions
{
    /// <summary>
    /// Провайдер для завершения сеансов в информационной базе
    /// </summary>
    public interface ITerminateSessionsInDatabaseProvider
    {
        /// <summary>
        /// Попытаться завершить сеансы в информационной базе
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат завершения сеансов в информационной базе</returns>
        bool TryTerminate(TerminateSessionsInDatabaseJobParamsDto model, out string errorMessage);

        /// <summary>
        /// Завершить сеансы в информационной базе
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Результат завершения сеансов в информационной базе</returns>
        void Terminate(TerminateSessionsInDatabaseJobParamsDto model);
    }
}
