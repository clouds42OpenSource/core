﻿using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;

namespace Clouds42.AccountDatabase.Contracts.TerminateSessions
{
    /// <summary>
    /// Провайдер для регистрации результата завершения
    /// сеансов в информационной базе
    /// </summary>
    public interface IRegisterTerminationSessionsInDatabaseResultProvider
    {
        /// <summary>
        /// Зарегестрировать результат завершения
        /// сеансов в информационной базе
        /// </summary>
        /// <param name="model">Модель регистрации результата завершения
        /// сеансов в информационной базе</param>
        void Register(RegisterTerminationSessionsInDatabaseResultDto model);
    }
}
