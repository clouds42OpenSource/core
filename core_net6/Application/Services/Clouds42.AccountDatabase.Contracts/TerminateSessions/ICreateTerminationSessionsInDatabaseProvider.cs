﻿using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;

namespace Clouds42.AccountDatabase.Contracts.TerminateSessions
{
    /// <summary>
    /// Провайдер для создания записи завершения сеансов в информационной базе 
    /// </summary>
    public interface ICreateTerminationSessionsInDatabaseProvider
    {
        /// <summary>
        /// Создать запись завершения сеансов в информационной базе
        /// </summary>
        /// <param name="model">Модель создания записи завершения сеансов в информационной базе </param>
        /// <returns>Id созданной записи</returns>
        Guid Create(CreateTerminationSessionsInDatabaseDto model);
    }
}
