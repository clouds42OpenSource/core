﻿using Clouds42.DataContracts.Account;

namespace Clouds42.AccountDatabase.Contracts.Archiving.Interfaces
{
    /// <summary>
    /// Провайдер архивации файлов аккаунта в склеп
    /// </summary>
    public interface IArchiveAccounFilesToTombProvider
    {
        /// <summary>
        /// Перенести файлы аккаунта в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>Результат переноса</returns>
        void DetachAccounFilesToTomb(DeleteAccountFilesToTombJobParamsDto taskParams);
    }
}
