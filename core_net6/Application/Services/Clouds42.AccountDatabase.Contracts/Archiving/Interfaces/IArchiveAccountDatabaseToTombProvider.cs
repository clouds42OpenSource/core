﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Archiving.Interfaces
{
    /// <summary>
    /// Провайдер архивации инф. баз в склеп
    /// </summary>
    public interface IArchiveAccountDatabaseToTombProvider
    {
        /// <summary>
        /// Перенести инф. базу в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>Результат переноса</returns>
        void DetachAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParams);
    }
}
