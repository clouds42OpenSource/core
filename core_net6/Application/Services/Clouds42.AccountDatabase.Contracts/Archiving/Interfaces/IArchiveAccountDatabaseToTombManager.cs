﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Archiving.Interfaces
{
    public interface IArchiveAccountDatabaseToTombManager
    {
        /// <summary>
        ///     Перенос информационной базы в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        ManagerResult DetachAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParams);
    }
}