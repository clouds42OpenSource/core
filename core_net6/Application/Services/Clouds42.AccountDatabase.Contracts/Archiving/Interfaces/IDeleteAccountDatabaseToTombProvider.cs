﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Archiving.Interfaces
{
    /// <summary>
    /// Провайдер удаления инф. баз в склеп
    /// </summary>
    public interface IDeleteAccountDatabaseToTombProvider
    {
        /// <summary>
        /// Удалить инф. базу в склеп
        /// </summary>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>Результат переноса</returns>
        void DeleteAccountDatabaseToTomb(DeleteAccountDatabaseToTombJobParamsDto taskParams);
    }
}
