﻿using Clouds42.DataContracts.AccountDatabase.UploadFiles;

namespace Clouds42.AccountDatabase.Contracts.Merge.Interfaces
{
    /// <summary>
    /// Провайдер для склейки файлов
    /// </summary>
    public interface IMergeFileProvider
    {
        /// <summary>
        /// Склеить части в исходный файл
        /// </summary>
        /// <param name="mergeChunksToInitialFileParams">Параметры склейки</param>
        void MergeChunksToInitialFile(MergeChunksToInitialFIleParamsDto mergeChunksToInitialFileParams);
    }
}
