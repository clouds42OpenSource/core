﻿using Clouds42.Domain.Enums.Files;

namespace Clouds42.AccountDatabase.Contracts.Backup.Models
{
    /// <summary>
    /// Модель информации о статусе загрузки файла
    /// </summary>
    public class UploadedFileInfoDto
    {
        /// <summary>
        /// Статус загрузки файла
        /// </summary>
        public UploadedFileStatus UploadedFileStatus { get; set; }

        /// <summary>
        /// Комментарий к статусу загрузки файла
        /// </summary>
        public string? StatusComment { get; set; }
    }
}
