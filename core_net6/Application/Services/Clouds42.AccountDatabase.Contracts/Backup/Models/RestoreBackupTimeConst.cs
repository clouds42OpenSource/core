﻿
namespace Clouds42.AccountDatabase.Contracts.Backup.Models
{
    /// <summary>
    /// Константы для рассчета времени восстановления бекапа
    /// </summary>
    public static class RestoreBackupTimeConst
    {
        /// <summary>
        /// Начальное значение первого промежутка
        /// </summary>
        public const int FirstGapStart = 0;

        /// <summary>
        /// Конечное значение первого промежутка
        /// </summary>
        public const int FirstGapFinish = 6;

        /// <summary>
        /// Время восстановления для первого промежутка
        /// </summary>
        public const int FirstGapResult = 13;

        /// <summary>
        /// Начальное значение второго промежутка
        /// </summary>
        public const int SecondGapStart = 5;

        /// <summary>
        /// Конечное значение второго промежутка
        /// </summary>
        public const int SecondGapFinish = 11;

        /// <summary>
        /// Время восстановления для второго промежутка
        /// </summary>
        public const int SecondGapResult = 22;

        /// <summary>
        /// Начальное значение третьего промежутка
        /// </summary>
        public const int ThirdGapStart = 10;

        /// <summary>
        /// Конечное значение третьего промежутка
        /// </summary>
        public const int ThirdGapFinish = 16;

        /// <summary>
        /// Время восстановления для третьего промежутка
        /// </summary>
        public const int ThirdGapResult = 30;

        /// <summary>
        /// Начальное значение четвертого промежутка
        /// </summary>
        public const int FourthGapStart = 15;

        /// <summary>
        /// Конечное значение четвертого промежутка
        /// </summary>
        public const int FourthGapFinish = 20;

        /// <summary>
        /// Время восстановления для четвертого промежутка
        /// </summary>
        public const int FourthGapResult = 60;

        /// <summary>
        /// Максимальный промежуток
        /// </summary>
        public const int MaxGapValue = 20;

        /// <summary>
        /// Время восстановления для максимального промежутка
        /// </summary>
        public const int MaxGapValueResult = 180;

        /// <summary>
        /// Коэффициент запаса времени для восстановления базы
        /// </summary>
        public const double RecoveryTimeFactor = 0.3;
    }


}
