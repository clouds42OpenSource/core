﻿
namespace Clouds42.AccountDatabase.Contracts.Backup.Models
{
    /// <summary>
    /// Константы для Истории бэкапов баз аккаунта
    /// </summary>
    public static class AccountDatabaseBackupConstants
    {
        /// <summary>
        /// Кол-во элементов на одну страницу для пагинации
        /// </summary>
        public const int ItemsPerPage = 50;

        /// <summary>
        /// Архив отсутcтвует 7 и более дней
        /// </summary>
        public const int DaysLeft = 7;

        /// <summary>
        /// Архив отсутcтвует от 3 дней
        /// </summary>
        public const int FromDaysLeft = 3;

        /// <summary>
        /// Архив отсутcтвует до 6 дней
        /// </summary>
        public const int ToDaysLeft = 6;

        /// <summary>
        /// Первая начальная страница пагинации
        /// </summary>
        public const int FirstPage = 1;

        /// <summary>
        /// Минус 3 месяца от текущей даты
        /// </summary>
        public const int BeforeThreeMonth = -3;
    }

}
