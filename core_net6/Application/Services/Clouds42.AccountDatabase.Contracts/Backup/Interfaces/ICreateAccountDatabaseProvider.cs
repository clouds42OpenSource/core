﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Cloud42Services;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Провайдер создания инф. баз
    /// </summary>
    public interface ICreateAccountDatabaseProvider
    {
        /// <summary>
        /// Обработать создание инф. баз
        /// </summary>
        /// <param name="databases">Список инф. баз</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns></returns>
        CreateCloud42ServiceModelDto Process(List<InfoDatabaseDomainModelDto> databases, Guid accountId);

        /// <summary>
        /// Создать информационную базу из бэкапа
        /// </summary>
        /// <param name="infoDatabaseModel">Модель инф. базы</param>
        CreateCloud42ServiceModelDto CreateDbOnDelimitersFromBackup(InfoDatabaseDomainModelDto infoDatabaseModel);
    }
}