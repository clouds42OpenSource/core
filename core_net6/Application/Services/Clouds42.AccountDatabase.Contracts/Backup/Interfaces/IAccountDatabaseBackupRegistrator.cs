﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Регистратор бэкапов инфо баз.
    /// </summary>
    public interface IAccountDatabaseBackupRegistrator
    {
        /// <summary>
        /// Зарегистрировать копию информационной базы.
        /// </summary>	
        void RegisterBackup(RegisterAccountDatabaseBackupRequestDto model);

    }
}