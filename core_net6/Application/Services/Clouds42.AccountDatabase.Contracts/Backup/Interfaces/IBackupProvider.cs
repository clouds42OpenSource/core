﻿using Clouds42.AccountDatabase.Contracts.Backup.Models;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Провайдер для работы с бэкапами
    /// </summary>
    public interface IBackupProvider
    {
        /// <summary>
        /// Получить данные для восстановления инф. базы из бекапа
        /// </summary>
        /// <param name="backupId">ID бекапа</param>
        /// <returns>Данные для восстановления инф. базы из бекапа</returns>
        BackupRestoringDto GetDataForRestoreAccountDatabase(Guid backupId);

        /// <summary>
        /// Получить информацию о статусе загрузки файла
        /// </summary>
        /// <param name="uploadedFileId">Идентификатор загружаемого файла</param>
        /// <returns>Модель информации о статусе загрузки файла</returns>
        UploadedFileInfoDto GetUploadedFileStatusInfo(Guid uploadedFileId);

        /// <summary>
        /// Начать процесс загрузки файла бэкапа
        /// </summary>
        /// <param name="backupId">Id бэкапа</param>
        /// <param name="uploadedFileId">Id файла бэкапа</param>
        void StartBackupFileFetching(Guid backupId, Guid uploadedFileId);

        /// <summary>
        /// Запустить задачу восстановления инф. базы из бекапа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="restoreAcDbFromBackupModel">Модель параметров восстановления инф. базы из бекапа</param>
        /// <returns>Результат выполнения</returns>
        CreateAccountDatabasesResultDto StartTaskForRestoreAccoundDatabase(Guid accountId,
            RestoreAcDbFromBackupDto restoreAcDbFromBackupModel);

        void DeleteOldBackups(DeleteOldBackupsDto  deleteOldBackupsDto);
    }
}
