﻿using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.Cloud42Services;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Создатель информационной базы из Dt
    /// </summary>
    public interface ICreateDatabaseFromDtCreator
    {
        /// <summary>
        /// Создать инф. базу из DT файла
        /// </summary>
        /// <param name="accountId">Id аккаунта </param>
        /// <param name="createAcDbFromDtDto">модель данных</param>
        /// <returns>Результат создания</returns>
        CreateCloud42ServiceModelDto CreateFromDt(Guid accountId, CreateAcDbFromDtDto createAcDbFromDtDto);

        /// <summary>
        /// Загрузить инф. базу из DT файла после конвертации
        /// </summary>
        /// <param name="createAcDbFromDtDto">модель данных</param>
        /// <returns>Результат создания</returns>
        CreateCloud42ServiceModelDto LoadFromDtАfterСonversion(CreateAcDbDtDto createAcDbFromDtDto);
    }
}
