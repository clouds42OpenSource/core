﻿namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Провайдер загрузки бэкапов инф. баз с гугл диска
    /// </summary>
    public interface IGoogleDriveBackupFileProvider : IUploadedFileFetcher
    {
    }
}
