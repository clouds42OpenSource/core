﻿namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Провайдер загрузки локальных бэкапов инф. баз
    /// </summary>
    public interface ILocalBackupFileProvider : IUploadedFileFetcher
    {
    }
}
