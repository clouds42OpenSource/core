﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Провайдер загрузки файлов бэкапов
    /// </summary>
    public interface IBackupFileProvider
    {
        /// <summary>
        /// Загрузить файл бэкапа
        /// </summary>
        /// <param name="backupFileFetchingParams">Параметры задачи по загрузке файла бэкапа</param>
        void FetchBackupFile(BackupFileFetchingJobParamsDto backupFileFetchingParams);

        /// <summary>
        /// Проверить путь к бэкапу и обновить его при необходимости
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        void CheckBackupPathAndUpdateIfNeed(Guid accountDatabaseBackupId);
    }
}
