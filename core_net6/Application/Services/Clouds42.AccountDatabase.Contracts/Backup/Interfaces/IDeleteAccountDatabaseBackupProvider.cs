﻿
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Провайдер удаления бэкапов инф. баз
    /// </summary>
    public interface IDeleteAccountDatabaseBackupProvider
    {
        /// <summary>
        /// Удалить бэкап инф. базы
        /// </summary>
        /// <param name="model">Параметры удаления бэкапа инф. базы</param>
        void Delete(DeleteAccountDatabaseBackupRequestDto model);
    }
}
