﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Cloud42Services;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    public interface ICreateDatabaseFromBackupCreator
    {        

        /// <summary>
        /// Создать информационные базы. 
        /// </summary>		
        CreateCloud42ServiceModelDto CreateDatabases(Guid accountId, List<InfoDatabaseDomainModelDto> databases);
    }
}