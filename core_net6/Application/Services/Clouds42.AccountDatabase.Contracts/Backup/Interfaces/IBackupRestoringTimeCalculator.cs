﻿namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    public interface IBackupRestoringTimeCalculator
    {
        int Calculate(int dbSizeInMb);
    }
}