﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Cloud42Services;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Класс создания информационных баз
    /// </summary>
    public interface IDatabaseCreator
    {

        /// <summary>
        /// Создать информационные базы. 
        /// </summary>		
        CreateCloud42ServiceModelDto CreateDatabases(Guid accountId, List<InfoDatabaseDomainModelDto> databases);
    }
}