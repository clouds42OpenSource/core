﻿using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Загрузчик загружаемых файлов бэкапов
    /// </summary>
    public interface IUploadedFileFetcher
    {
        /// <summary>
        /// Загрузить файл бэкапа
        /// </summary>
        /// <param name="backup">Бекап инф. базы</param>
        /// <param name="uploadedFile">Загружаемый файл</param>
        void Fetch(AccountDatabaseBackup backup, UploadedFile uploadedFile);
    }
}
