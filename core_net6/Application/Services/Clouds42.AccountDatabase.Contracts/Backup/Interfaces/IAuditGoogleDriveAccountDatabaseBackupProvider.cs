﻿namespace Clouds42.AccountDatabase.Contracts.Backup.Interfaces
{
    /// <summary>
    /// Провайдер для аудита бэкапов инф. баз
    /// </summary>
    public interface IAuditGoogleDriveAccountDatabaseBackupProvider
    {
        /// <summary>
        /// Провести аудит бэкапов
        /// </summary>
        void ProcessAudit();
    }
}
