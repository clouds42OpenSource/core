﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;

namespace Clouds42.AccountDatabase.Contracts.Activity.Interfaces
{
    /// <summary>
    /// Провайдер фиксации активности инф. баз
    /// </summary>
    public interface IFixactionAccountDatabaseActivityProvider
    {
        /// <summary>
        /// Зафиксировать последнюю активность запуска инф. базы или RDP
        /// </summary>
        /// <param name="model">Модель для фиксации последней активности запуска инф. базы или RDP</param>
        /// <param name="clientIpAddress">IP адрес клиента</param>
        /// <param name="needThrowExceptionOnError">Признак необходимости выбрасывать исключение при ошибке</param>
        void FixLaunchActivity(PostRequestSetActivityDateDto model, string clientIpAddress, bool needThrowExceptionOnError = true);

        /// <summary>
        /// Зафиксировать дату активности в инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        void FixDatabaseLastActivity(Guid accountDatabaseId);
    }
}
