﻿using Clouds42.DataContracts.AccountDatabase.UploadFiles;

namespace Clouds42.AccountDatabase.Contracts.Create.Interfaces
{
    /// <summary>
    /// Парсинг zip архива информационной базы
    /// </summary>
    public interface IAccountDatabaseParseZipFileProvider
    {

        /// <summary>
        /// Получить информацию об информационной базе из загруженного архива.
        /// </summary>
        /// <param name="uploadedFileId">Идентификатор загруженного архива.</param>
        /// <returns>Информация о базе.</returns>
        AccountDatabaseInfoDto GetDatabaseInfo(Guid uploadedFileId);

        /// <summary>
        /// Получить данные о пользователях из ZIP файла загруженной базы
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        List<UserAccountDatabaseZipDto> ParseUsersXmlInZipFile(Guid uploadedFileId);

        /// <summary>
        /// Проверка совместимости релиза шаблона и релиза из zip файла информационной базы
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        void ValidateVersionXmlInZipFile(Guid uploadedFileId);
    }
}