﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.Contracts.Create.Interfaces
{
    /// <summary>
    /// Провайдер создания инф. базы из бэкапа
    /// </summary>
    public interface ICreateAccountDatabaseFromBackupProvider
    {
        /// <summary>
        /// Создать новую информационную базу на основе бэкапа
        /// </summary>
        /// <param name="accountDatabaseBackup">Бэкап информационной базы</param>
        /// <param name="accountDatabaseName">Название инф. базы</param>
        /// <param name="parentProcessFlowKey">Ключ родительского рабочего процесса</param>
        /// <returns>ID новой базы</returns>
        IAccountDatabase Create(AccountDatabaseBackup accountDatabaseBackup, string accountDatabaseName, string parentProcessFlowKey);
    }
}