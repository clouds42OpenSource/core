﻿using Clouds42.Common.Http;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;

namespace Clouds42.AccountDatabase.Contracts.Create.Interfaces
{
    /// <summary>
    /// Провайдер загруженных файлов
    /// </summary>
    public interface IUploadedFileProvider
    {
        /// <summary>
        /// Получить загруженный файл
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Загруженный файл</returns>
        UploadedFile GetUploadedFile(Guid uploadedFileId);
        UploadedFile GetUploadedFile(ThinHttpRequest httpRequest);
        
        /// <summary>
        /// Попытаться получить имя загруженного файла
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <param name="uploadedFileName">Имя загруженного файла</param>
        /// <returns>Имя загруженного файла</returns>
        bool TryGetUploadedFileName(Guid uploadedFileId, out string uploadedFileName);

        /// <summary>
        /// Создать загруженный файл
        /// </summary>
        /// <param name="initUploadFileRequestDc">Модель запроса инициализации загрузки файла</param>
        /// <returns>загруженный файл</returns>
        UploadedFile CreateUploadedFile(InitUploadFileRequestDto initUploadFileRequestDc);

        /// <summary>
        /// Обновить статус загруженного файла
        /// </summary>
        /// <param name="uploadedFileId"></param>
        /// <param name="status"></param>
        /// <param name="comment">Комментарий</param>
        void UpdateUploadedFileStatus(Guid uploadedFileId, UploadedFileStatus status, string comment);
    }
}
