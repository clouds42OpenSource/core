﻿using Clouds42.Common.Http;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;

namespace Clouds42.AccountDatabase.Contracts.Create.Interfaces
{
    /// <summary>
    /// Провайдер загружаемых файлов
    /// </summary>
    public interface IUploadFilesProvider
    {
        /// <summary>
        /// Инициировать процесс загрузки файла
        /// </summary>
        /// <param name="initUploadFileRequestDc">Модель запроса инициализации загрузки файла</param>
        /// <returns>ID объекта загружаемого файла</returns>
        Guid InitUploadProcess(InitUploadFileRequestDto initUploadFileRequestDc);

        /// <summary>
        /// Загрузить часть файла
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        /// <returns>Ok - если часть файла загружена успешно</returns>
        bool UploadChunkOfDbFile(UploadChunkDto uploadChunkDto);
        bool UploadChunkOfDbFile(ThinHttpRequest httpRequest);

        /// <summary>
        /// Проверить загруженный файл на соответствие требованиям
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        /// <returns>Сообщение с результатом проверки</returns>
        string CheckLoadFileValidity(ThinHttpRequest httpRequest);

        /// <summary>
        /// Запустить процесс склейки частей в исходный файл
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        void InitMergeChunksToInitialFileProcess(Guid uploadFileId, int countOfChunks);
        void InitMergeChunksToInitialFileProcess(ThinHttpRequest httpRequest);

        /// <summary>
        /// Проверить статус загрузки файла
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        /// <returns>true - если файл загружен успешно</returns>
        bool CheckFileUploadStatus(Guid uploadFileId);
        bool CheckFileUploadStatus(ThinHttpRequest httpRequest);

    }
}
