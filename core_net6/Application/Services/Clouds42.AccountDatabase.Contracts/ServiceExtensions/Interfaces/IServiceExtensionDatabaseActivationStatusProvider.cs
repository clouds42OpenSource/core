﻿namespace Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces
{
    /// <summary>
    /// Провайдер для работы со статусом активации
    /// расширения сервиса для инф. базы
    /// </summary>
    public interface IServiceExtensionDatabaseActivationStatusProvider
    {
        /// <summary>
        /// Установить статус активации расширения сервиса для инф. базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="isActive">Признак активности</param>
        /// <param name="isDemo">Признак демо периода</param>
        void SetActivationStatus(Guid accountId, bool isActive = false, Guid? serviceId = null, bool isDemo = false);
    }
}
