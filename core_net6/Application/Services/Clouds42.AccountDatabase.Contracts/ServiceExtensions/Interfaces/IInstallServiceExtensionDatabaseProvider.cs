﻿using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces
{
    /// <summary>
    /// Провайдер для установки расширения сервиса для информационной базы
    /// </summary>
    public interface IInstallServiceExtensionDatabaseProvider
    {
        /// <summary>
        /// Попытаться установить сервис для созданной инф. базы
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        void TryInstalExtensionForCreatedDatabase(Guid serviceId, Guid accountDatabaseId);

        /// <summary>
        /// Установить расширение сервиса для информационных баз
        /// </summary>
        /// <param name="installServiceExtensionModel">Модель установки расширения сервиса
        /// для информационных баз</param>
        void InstallServiceExtensionForDatabases(InstallServiceExtensionForDatabasesDto installServiceExtensionModel);

        /// <summary>
        /// Установить расширение сервиса для информационной базы
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="databaseId">Id информационной базы</param>
        /// <param name="needDelay">Признак необходимости создавать задачу с задержкой выполнения</param>
        void InstallServiceExtensionForDatabase(Guid serviceId, Guid databaseId, bool needDelay = false);

        /// <summary>
        /// Установить расширение сервиса для области информационной базы
        /// </summary>
        /// <param name="installServiceExtensionModel">Модель данных для установки расширения сервиса
        /// для области инф. базы</param>
        void InstallServiceExtensionForDatabaseZone(
            InstallServiceExtensionForDatabaseZoneDto installServiceExtensionModel);
    }
}
