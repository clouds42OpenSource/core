﻿using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными расширения сервиса
    /// для информационной базы
    /// </summary>
    public interface IServiceExtensionDatabaseDataProvider
    {

        /// <summary>
        /// Получить список расширений сервиса,
        /// совместимых с инф. базой по фильтру
        /// </summary>
        /// <param name="filter">Модель фильтра на получение
        /// подходящих расширений сервиса для инф. базы</param>
        /// <returns>Список расширений сервиса,
        /// совместимых с инф. базой</returns>
        Task<List<AvailableServiceExtensionForDatabaseDto>> GetCompatibleWithDatabaseServiceExtensions(
            AvailableServiceExtensionsForDatabaseFilterDto filter);

        /// <summary>
        /// Получить Id информационной базы по номеру области
        /// </summary>
        /// <param name="zone">Номер области базы</param>
        /// <returns>Id информационной базы</returns>
        Guid GetDatabaseIdForManageServiceExtensionByZone(int zone);

        /// <summary>
        /// Получить список баз аккаунта, в которые установлено расширение сервиса
        /// </summary>
        /// <param name="accountId">Id аккауна</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список баз аккаунта, в которые установлено расширение сервиса</returns>
        IQueryable<AvailableDatabaseForService> GetAccountServiceExtensionDatabases(Guid accountId, Guid serviceId);

        /// <summary>
        /// Получить статус расширения сервиса для информационной базы
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        /// <returns>Статус расширения</returns>
        ServiceExtensionDatabaseStatusDto GetServiceExtensionDatabaseStatus(Guid serviceId,
            Guid accountDatabaseId);

        /// <summary>
        /// Получить информационные базы
        /// для сервиса 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="configurations"></param>
        /// <returns>Список инф. баз</returns>
        List<Domain.DataModels.AccountDatabase> GetDatabasesForServiceExtension(Guid accountId,
            List<string> configurations);
    }
}
