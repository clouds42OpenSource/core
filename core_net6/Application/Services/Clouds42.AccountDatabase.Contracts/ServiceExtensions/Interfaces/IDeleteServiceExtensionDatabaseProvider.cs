﻿
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces
{
    /// <summary>
    /// Провайдер для удаления расширения сервиса из информационной базы
    /// </summary>
    public interface IDeleteServiceExtensionDatabaseProvider
    {
        /// <summary>
        /// Удалить расширение сервиса из информационной базы
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabase">Модель удаления расширения сервиса
        /// из информационной базы</param>
        void DeleteServiceExtensionFromDatabase(DeleteServiceExtensionFromDatabaseDto deleteServiceExtensionFromDatabase);

        /// <summary>
        /// Удалить расширение сервиса из области информационной базы
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabaseZone">Модель удаления расширения сервиса
        /// из области информационной базы</param>
        void DeleteServiceExtensionFromDatabaseZone(
            DeleteServiceExtensionFromDatabaseZoneDto deleteServiceExtensionFromDatabaseZone);
    }
}
