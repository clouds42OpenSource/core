﻿using Clouds42.Common.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.DigitalSignatures
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddScramAuthorization(this IServiceCollection services)
        {
            services.AddTransient<ICryptoRandomProvider, CryptoRandomProvider>();

            return services;
        }
    }
}
