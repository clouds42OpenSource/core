﻿using Clouds42.Configurations.Configurations;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.AccountUserJsonWebToken.Interfaces;

namespace Clouds42.Segment.AccountUserJsonWebToken.Providers
{
    /// <summary>
    /// Провайдер для удаления устаревших json токенов пользователей 
    /// </summary>
    internal class RemoveObsoleteUserTokensProvider(IUnitOfWork dbLayer, IHandlerException handlerException)
        : IRemoveObsoleteUserTokensProvider
    {
        private readonly Lazy<int> _timeToLiveJwtInMinutes = new(CloudConfigurationProvider.JsonWebToken.GetTimeToLiveInMinutes);
        private readonly Lazy<int> _userTokensStoragePeriodInMinutes = new(CloudConfigurationProvider.AccountUserJsonWebToken.GetTokensStoragePeriodInMinutes);

        /// <summary>
        /// Удалить устаревшие токены
        /// пользователей
        /// </summary>
        public void Remove()
        {
            var userJsonWebTokens = GetTokensForRemoving().ToList();

            if (!userJsonWebTokens.Any())
                return;
            
            try
            {
                dbLayer.BulkDelete(userJsonWebTokens);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка удаления устаревших токенов пользователей]");
                
                throw;
            }
        }

        /// <summary>
        /// Получить дату, до которой можно удалять токены пользователей
        /// </summary>
        /// <returns>Дата, до которой можно удалять токены пользователей</returns>
        private DateTime GetDateUntilWhichTokensCanBeRemoved()
        {
            var userTokensStoragePeriod = _userTokensStoragePeriodInMinutes.Value < _timeToLiveJwtInMinutes.Value
                ? _timeToLiveJwtInMinutes.Value
                : _userTokensStoragePeriodInMinutes.Value;

            return DateTime.Now.AddMinutes(-userTokensStoragePeriod);
        }

        /// <summary>
        /// Получить токены пользователей для удаления
        /// </summary>
        /// <returns>Токены пользователей для удаления</returns>
        private IEnumerable<Domain.DataModels.JsonWebToken.AccountUserJsonWebToken> GetTokensForRemoving()
        {
            var dateUntilWhichTokensCanBeRemoved = GetDateUntilWhichTokensCanBeRemoved();

            return dbLayer.GetGenericRepository<Domain.DataModels.JsonWebToken.AccountUserJsonWebToken>()
                .WhereLazy(ut => ut.Created < dateUntilWhichTokensCanBeRemoved && ut.HasBeenTakenRefreshToken).AsEnumerable();
        }
    }
}
