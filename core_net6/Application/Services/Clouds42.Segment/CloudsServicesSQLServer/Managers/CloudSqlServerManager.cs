﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces;
using Clouds42.Segment.SegmentElementsHelper;
using Clouds42.Validators.CloudServer;

namespace Clouds42.Segment.CloudsServicesSQLServer.Managers
{
    /// <summary>
    /// Менеджер для работы с sql сервером
    /// </summary>
    public class CloudSqlServerManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloudSqlServerProvider cloudSqlServerProvider,
        ICreateCloudSqlServerProvider createCloudSqlServerProvider,
        IDeleteCloudSqlServerProvider deleteCloudSqlServerProvider,
        IEditCloudSqlServerProvider editCloudSqlServerProvider,
        IHandlerException handlerException,
        SegmentElementDeletionLogEventHelper segmentElementDeletionLogEventHelper,
        SegmentElementCreationLogEventHelper segmentElementCreationLogEventHelper,
        SegmentElementChangesLogEventHelper segmentElementChangesLogEventHelper,
        CloudSqlServerModelsValidator cloudSqlServerModelsValidator,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Создать sql сервер
        /// </summary>
        /// <param name="createCloudSqlServerDto">Модель создания Sql сервера</param>
        /// <returns>Id созданного sql сервера</returns>
        public ManagerResult<Guid> CreateCloudSqlServer(CreateCloudSqlServerDto createCloudSqlServerDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                if (!cloudSqlServerModelsValidator.TryValidateCreation(createCloudSqlServerDto, out var errorMessage))
                {
                    LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, errorMessage);
                    return PreconditionFailed<Guid>(errorMessage);
                }

                var sqlServerId = createCloudSqlServerProvider.Create(createCloudSqlServerDto);

                var message = segmentElementCreationLogEventHelper.GetCreationLogEventDescription(createCloudSqlServerDto);
                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);

                return Ok(sqlServerId);
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка добавления нового Sql сервера \"{createCloudSqlServerDto.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать sql сервер
        /// </summary>
        /// <param name="cloudSqlServerDto">Модель Sql сервера</param>
        public ManagerResult EditCloudSqlServer(CloudSqlServerDto cloudSqlServerDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                if (!cloudSqlServerModelsValidator.TryValidateEditing(cloudSqlServerDto, cloudSqlServerDto.Id, out var errorMessage))
                {
                    LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, errorMessage);
                    return PreconditionFailed<Guid>(errorMessage);
                }

                var currentSqlServer = cloudSqlServerProvider.GetCloudSqlServerDto(cloudSqlServerDto.Id);
                var logEventDescription =
                    segmentElementChangesLogEventHelper.GetChangesDescription(currentSqlServer, cloudSqlServerDto);

                editCloudSqlServerProvider.Edit(cloudSqlServerDto);

                logger.Trace(logEventDescription + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, logEventDescription);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка редактирования sql сервера \"{cloudSqlServerDto.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить sql сервер
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        public ManagerResult DeleteCloudSqlServer(Guid sqlServerId)
        {
            var getSqlServerResult = GetCloudSqlServerDto(sqlServerId);
            if (getSqlServerResult.Error)
                return PreconditionFailed(getSqlServerResult.Message);

            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                deleteCloudSqlServerProvider.Delete(sqlServerId);

                var message = segmentElementDeletionLogEventHelper.GetDeletionLogEventDescription(getSqlServerResult.Result);
                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления sql сервера \"{getSqlServerResult.Result.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель sql сервера
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        /// <returns>Модель sql сервера</returns>
        public ManagerResult<CloudSqlServerDto> GetCloudSqlServerDto(Guid sqlServerId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var cloudSqlServer = cloudSqlServerProvider.GetCloudSqlServerDto(sqlServerId);
                return Ok(cloudSqlServer);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения sql сервера {sqlServerId}]");
                return PreconditionFailed<CloudSqlServerDto>(ex.Message);
            }
        }
    }
}
