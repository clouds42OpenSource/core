﻿using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces;

namespace Clouds42.Segment.CloudsServicesSQLServer.Providers
{
    /// <summary>
    /// Провайдер удаления sql сервера
    /// </summary>
    internal class DeleteCloudSqlServerProvider(IUnitOfWork dbLayer, ICloudSqlServerProvider cloudSqlServerProvider)
        : IDeleteCloudSqlServerProvider
    {
        /// <summary>
        /// Удалить sql сервер
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        public void Delete(Guid sqlServerId)
        {
            var sqlServer = cloudSqlServerProvider.GetCloudSqlServer(sqlServerId);

            if (!AbilityDeleteSqlServer(sqlServerId))
                throw new ValidateException(
                    $"Удаление sql сервера \"{sqlServer.Name}\" не возможно так он используется в сегменте");

            dbLayer.CloudServicesSqlServerRepository.Delete(sqlServer);
            dbLayer.Save();
        }

        /// <summary>
        /// Возможность удалить sql сервер
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        private bool AbilityDeleteSqlServer(Guid sqlServerId) =>
            dbLayer.CloudServicesSegmentRepository.FirstOrDefault(sts =>
                sts.SQLServerID == sqlServerId) == null;
    }
}
