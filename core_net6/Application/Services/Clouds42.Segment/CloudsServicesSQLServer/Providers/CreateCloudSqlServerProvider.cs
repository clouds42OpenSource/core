﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces;

namespace Clouds42.Segment.CloudsServicesSQLServer.Providers
{
    /// <summary>
    /// Провайдер для создания Sql сервера
    /// </summary>
    internal class CreateCloudSqlServerProvider(IUnitOfWork dbLayer) : ICreateCloudSqlServerProvider
    {
        /// <summary>
        /// Создать sql сервер
        /// </summary>
        /// <param name="createCloudSqlServerDto">Модель создания Sql сервера</param>
        /// <returns>Id созданного sql сервера</returns>
        public Guid Create(CreateCloudSqlServerDto createCloudSqlServerDto)
        {
            var cloudServicesSqlServer = new CloudServicesSqlServer
            {
                ID = Guid.NewGuid(),
                Name = createCloudSqlServerDto.Name,
                Description = createCloudSqlServerDto.Description,
                ConnectionAddress = createCloudSqlServerDto.ConnectionAddress,
                RestoreModelType = createCloudSqlServerDto.RestoreModelType
            };

            dbLayer.CloudServicesSqlServerRepository.Insert(cloudServicesSqlServer);
            dbLayer.Save();

            return cloudServicesSqlServer.ID;
        }
    }
}
