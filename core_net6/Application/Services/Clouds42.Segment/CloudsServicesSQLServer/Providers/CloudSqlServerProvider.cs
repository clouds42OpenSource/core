﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces;

namespace Clouds42.Segment.CloudsServicesSQLServer.Providers
{
    /// <summary>
    /// Провайдер для работы с sql сервером
    /// </summary>
    internal class CloudSqlServerProvider(IUnitOfWork dbLayer) : ICloudSqlServerProvider
    {
        /// <summary>
        /// Получить sql сервер
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        /// <returns>Sql сервер</returns>
        public CloudServicesSqlServer GetCloudSqlServer(Guid sqlServerId) =>
            dbLayer.CloudServicesSqlServerRepository.FirstOrDefault(ss => ss.ID == sqlServerId) ??
            throw new NotFoundException($"Не удалось получить sql сервер по Id = '{sqlServerId}'");

        /// <summary>
        /// Получить модель sql сервера
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        /// <returns>Модель sql сервера</returns>
        public CloudSqlServerDto GetCloudSqlServerDto(Guid sqlServerId)
        {
            var sqlServer = GetCloudSqlServer(sqlServerId);

            return new CloudSqlServerDto
            {
                Id = sqlServer.ID,
                Name = sqlServer.Name,
                Description = sqlServer.Description,
                ConnectionAddress = sqlServer.ConnectionAddress,
                RestoreModelType = sqlServer.RestoreModelType
            };
        }
    }
}
