﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces;

namespace Clouds42.Segment.CloudsServicesSQLServer.Providers
{
    /// <summary>
    /// Провайдер для редактирования sql сервера
    /// </summary>
    internal class EditCloudSqlServerProvider(IUnitOfWork dbLayer, ICloudSqlServerProvider cloudSqlServerProvider)
        : IEditCloudSqlServerProvider
    {
        /// <summary>
        /// Редактировать sql сервер
        /// </summary>
        /// <param name="cloudSqlServerDto">Модель Sql сервера</param>
        public void Edit(CloudSqlServerDto cloudSqlServerDto)
        {
            var sqlServer = cloudSqlServerProvider.GetCloudSqlServer(cloudSqlServerDto.Id);
            sqlServer.Name = cloudSqlServerDto.Name;
            sqlServer.Description = cloudSqlServerDto.Description;
            sqlServer.ConnectionAddress = cloudSqlServerDto.ConnectionAddress;
            sqlServer.RestoreModelType = cloudSqlServerDto.RestoreModelType;

            dbLayer.CloudServicesSqlServerRepository.Update(sqlServer);
            dbLayer.Save();
        }
    }
}
