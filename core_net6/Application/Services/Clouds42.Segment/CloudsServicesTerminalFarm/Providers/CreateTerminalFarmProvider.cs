﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces;

namespace Clouds42.Segment.CloudsServicesTerminalFarm.Providers
{
    /// <summary>
    /// Провайдер создания ферм ТС
    /// </summary>
    internal class CreateTerminalFarmProvider(IUnitOfWork dbLayer) : ICreateTerminalFarmProvider
    {
        /// <summary>
        /// Добавить новую ферму ТС
        /// </summary>
        /// <param name="terminalFarmDto">Ферма ТС</param>
        public Guid Create(CloudTerminalFarmDto terminalFarmDto)
        {
            var enterpriseServer = new CloudServicesTerminalFarm
            {
                UsingOutdatedWindows = terminalFarmDto.UsingOutdatedWindows,
                ConnectionAddress = terminalFarmDto.ConnectionAddress,
                Description = terminalFarmDto.Description,
                Name = terminalFarmDto.Name,
                ID = Guid.NewGuid()
            };

            dbLayer.CloudServicesTerminalFarmRepository.Insert(enterpriseServer);
            dbLayer.Save();

            return enterpriseServer.ID;
        }
    }
}
