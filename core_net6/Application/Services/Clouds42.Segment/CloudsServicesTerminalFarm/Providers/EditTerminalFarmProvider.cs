﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudsServicesTerminalFarm.Helpers;
using Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces;

namespace Clouds42.Segment.CloudsServicesTerminalFarm.Providers
{
    /// <summary>
    /// Провайдер редактирования ферм ТС
    /// </summary>
    internal class EditTerminalFarmProvider(
        TerminalFarmDataHelper terminalFarmDataHelper,
        IUnitOfWork dbLayer)
        : IEditTerminalFarmProvider
    {
        /// <summary>
        /// Изменить ферму ТС
        /// </summary>
        /// <param name="terminalFarmDto">Обновленная ферма ТС</param>
        public void Edit(CloudTerminalFarmDto terminalFarmDto)
        {
            var terminalFarm = terminalFarmDataHelper.GetTerminalFarmOrThrowException(terminalFarmDto.Id);

            terminalFarm.Description = terminalFarmDto.Description;
            terminalFarm.Name = terminalFarmDto.Name;
            terminalFarm.ConnectionAddress = terminalFarmDto.ConnectionAddress;
            terminalFarm.UsingOutdatedWindows = terminalFarmDto.UsingOutdatedWindows;

            dbLayer.CloudServicesTerminalFarmRepository.Update(terminalFarm);
            dbLayer.Save();
        }
    }
}
