﻿using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudsServicesTerminalFarm.Helpers;
using Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces;

namespace Clouds42.Segment.CloudsServicesTerminalFarm.Providers
{
    /// <summary>
    /// Провайдер удаления ферм ТС
    /// </summary>
    internal class DeleteTerminalFarmProvider(
        TerminalFarmDataHelper terminalFarmDataHelper,
        IUnitOfWork dbLayer)
        : IDeleteTerminalFarmProvider
    {
        /// <summary>
        /// Удалить ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        public void Delete(Guid terminalFarmId)
        {
            var terminalFarm = terminalFarmDataHelper.GetTerminalFarmOrThrowException(terminalFarmId);

            dbLayer.CloudServicesTerminalFarmRepository.Delete(terminalFarm);
            dbLayer.Save();
        }
    }
}
