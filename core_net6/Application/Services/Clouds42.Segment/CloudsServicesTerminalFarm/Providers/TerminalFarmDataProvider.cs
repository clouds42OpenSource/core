﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Segment.CloudsServicesTerminalFarm.Helpers;
using Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces;

namespace Clouds42.Segment.CloudsServicesTerminalFarm.Providers
{
    /// <summary>
    /// Провайдер для работы с фермами ТС
    /// </summary>
    internal class TerminalFarmDataProvider(TerminalFarmDataHelper terminalFarmDataHelper) : ITerminalFarmDataProvider
    {
        /// <summary>
        /// Получить ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        /// <returns>Ферма ТС</returns>
        public CloudTerminalFarmDto GetTerminalFarm(Guid terminalFarmId)
        {
            var terminalFarm = terminalFarmDataHelper.GetTerminalFarmOrThrowException(terminalFarmId);

            return new CloudTerminalFarmDto
            {
                Id = terminalFarm.ID,
                Name = terminalFarm.Name, 
                Description = terminalFarm.Description,
                ConnectionAddress = terminalFarm.ConnectionAddress
            };
        }
    }
}
