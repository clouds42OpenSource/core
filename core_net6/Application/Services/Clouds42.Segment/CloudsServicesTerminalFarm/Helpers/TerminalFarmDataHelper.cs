﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Segment.CloudsServicesTerminalFarm.Helpers
{
    /// <summary>
    /// Хэлпер данных ферм ТС
    /// </summary>
    public class TerminalFarmDataHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        /// <returns>Ферма ТС</returns>
        public CloudServicesTerminalFarm GetTerminalFarmOrThrowException(Guid terminalFarmId)
            => dbLayer.CloudServicesTerminalFarmRepository.FirstOrDefault(farm => farm.ID == terminalFarmId)
               ?? throw new NotFoundException($"Ферма ТС по ID {terminalFarmId} не найдена");

        /// <summary>
        /// Признак что такой адрес фермы ТС уже существует
        /// </summary>
        /// <param name="terminalFarm">Ферма ТС</param>
        /// <returns>Признак что такой адрес фермы ТС уже существует</returns>
        public bool ConnectionAddressAlreadyExist(CloudTerminalFarmDto terminalFarm)
            => dbLayer.CloudServicesTerminalFarmRepository.FirstOrDefault(farm =>
                   farm.ConnectionAddress.ToLower() == terminalFarm.ConnectionAddress.ToLower() &&
                   farm.ID != terminalFarm.Id) !=
               null;

        /// <summary>
        /// Признак что такое название фермы ТС уже существует
        /// </summary>
        /// <param name="terminalFarm">Ферма ТС</param>
        /// <returns>Признак что такое название фермы ТС уже существует</returns>
        public bool NameAlreadyExist(CloudTerminalFarmDto terminalFarm)
            => dbLayer.CloudServicesTerminalFarmRepository.FirstOrDefault(farm =>
                   farm.Name.ToLower() == terminalFarm.Name.ToLower() &&
                   farm.ID != terminalFarm.Id) !=
               null;

        /// <summary>
        /// Признак что ферма ТС используется
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        /// <returns>Признак что ферма ТС используется</returns>
        public bool TerminalFarmUsed(Guid terminalFarmId)
            => dbLayer.CloudServicesSegmentRepository.FirstOrDefault(x =>
                   x.ServicesTerminalFarmID == terminalFarmId) != null;

    }
}
