﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Segment.CloudsServicesTerminalFarm.Helpers;

namespace Clouds42.Segment.CloudsServicesTerminalFarm.Validators
{
    /// <summary>
    /// Валидатор для ферм ТС
    /// </summary>
    public class TerminalFarmValidator(TerminalFarmDataHelper terminalFarmDataHelper)
    {
        /// <summary>
        /// Проверить ферму ТС перед удалением
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        /// <returns>Результат валидации</returns>
        public string ValidateTerminalFarmBeforeDelete(Guid terminalFarmId)
            => terminalFarmDataHelper.TerminalFarmUsed(terminalFarmId)
                ? "Удаление невозможно, так как эта ферма ТС используется в сегменте"
                : string.Empty;

        /// <summary>
        /// Проверить модель фермы ТС
        /// </summary>
        /// <param name="terminalFarmDto">Обновленная ферма ТС</param>
        /// <returns>Результат валидации</returns>
        public string ValidateTerminalFarmDto(CloudTerminalFarmDto terminalFarmDto)
        {
            if (string.IsNullOrEmpty(terminalFarmDto.ConnectionAddress))
                return "Поле Адрес обязательное для заполнения";

            if (string.IsNullOrEmpty(terminalFarmDto.Name))
                return "Поле Название обязательное для заполнения";

            if (terminalFarmDataHelper.ConnectionAddressAlreadyExist(terminalFarmDto))
                return "Ферма ТС с таким адресом подключения уже существует";

            return terminalFarmDataHelper.NameAlreadyExist(terminalFarmDto) 
                ? "Ферма ТС с таким названием уже существует"
                : string.Empty;
        }
    }
}
