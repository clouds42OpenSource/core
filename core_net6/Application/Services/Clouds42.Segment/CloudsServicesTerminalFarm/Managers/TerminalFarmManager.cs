﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudsServicesTerminalFarm.Validators;
using Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces;
using Clouds42.Segment.SegmentElementsHelper;

namespace Clouds42.Segment.CloudsServicesTerminalFarm.Managers
{
    /// <summary>
    /// Менеджер для работы с фермами ТС
    /// </summary>
    public class TerminalFarmManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ITerminalFarmDataProvider terminalFarmDataProvider,
        IHandlerException handlerException,
        IDeleteTerminalFarmProvider deleteTerminalFarmProvider,
        IEditTerminalFarmProvider editTerminalFarmProvider,
        ICreateTerminalFarmProvider createTerminalFarmProvider,
        TerminalFarmValidator terminalFarmValidator,
        SegmentElementChangesLogEventHelper segmentElementChangesLogEventHelper,
        SegmentElementCreationLogEventHelper segmentElementCreationLogEventHelper,
        SegmentElementDeletionLogEventHelper segmentElementDeletionLogEventHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        /// <returns>Ферма ТС</returns>
        public ManagerResult<CloudTerminalFarmDto> GetTerminalFarmDto(Guid terminalFarmId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ViewCloudServiceTerminalFarm,
                    () => AccessProvider.ContextAccountId);

                return Ok(terminalFarmDataProvider.GetTerminalFarm(terminalFarmId));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения фермы ТС {terminalFarmId} пользователем {AccessProvider.Name}.]");
                return PreconditionFailed<CloudTerminalFarmDto>(ex.Message);
            }
        }

        /// <summary>
        /// Добавить новую ферму ТС
        /// </summary>
        /// <param name="terminalFarmDto">Ферма ТС</param>
        /// <returns>Результат добавления</returns>
        public ManagerResult<Guid> AddNewTerminalFarm(CloudTerminalFarmDto terminalFarmDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewCloudServiceTerminalFarm,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = terminalFarmValidator.ValidateTerminalFarmDto(terminalFarmDto);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError<Guid>(validationMessage);

                var message = segmentElementCreationLogEventHelper.GetCreationLogEventDescription(terminalFarmDto);

                var terminalFarmId = createTerminalFarmProvider.Create(terminalFarmDto);

                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);

                return Ok(terminalFarmId);
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка добавления фермы ТС \"{terminalFarmDto.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Удалить ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult DeleteTerminalFarm(Guid terminalFarmId)
        {
            var getTerminalFarmResult = GetTerminalFarmDto(terminalFarmId);
            if (getTerminalFarmResult.Error)
                return PreconditionFailed(getTerminalFarmResult.Message);

            try
            {
                AccessProvider.HasAccess(ObjectAction.DeleteCloudServiceTerminalFarm,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = terminalFarmValidator.ValidateTerminalFarmBeforeDelete(terminalFarmId);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError(validationMessage);

                deleteTerminalFarmProvider.Delete(terminalFarmId);

                var message = segmentElementDeletionLogEventHelper.GetDeletionLogEventDescription(getTerminalFarmResult.Result);
                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления фермы ТС \"{getTerminalFarmResult.Result.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Изменить ферму ТС
        /// </summary>
        /// <param name="terminalFarmDto">Обновленная ферма ТС</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult EditTerminalFarm(CloudTerminalFarmDto terminalFarmDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdateCloudServiceTerminalFarm,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = terminalFarmValidator.ValidateTerminalFarmDto(terminalFarmDto);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError(validationMessage);

                var currentTerminalFarm = terminalFarmDataProvider.GetTerminalFarm(terminalFarmDto.Id);
                var logEventDescription =
                    segmentElementChangesLogEventHelper.GetChangesDescription(currentTerminalFarm, terminalFarmDto);

                editTerminalFarmProvider.Edit(terminalFarmDto);

                logger.Trace(logEventDescription + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, logEventDescription);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка редактирования фермы ТС \"{terminalFarmDto.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
