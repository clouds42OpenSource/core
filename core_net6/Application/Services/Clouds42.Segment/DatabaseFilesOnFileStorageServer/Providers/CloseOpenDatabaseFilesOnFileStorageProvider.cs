﻿using Clouds42.HandlerExeption.Contract;
using Clouds42.PowerShellClient;
using Clouds42.Segment.Contracts.DatabaseFilesOnFileStorageServer.Interfaces;

namespace Clouds42.Segment.DatabaseFilesOnFileStorageServer.Providers
{
    /// <summary>
    /// Провайдер для закрытия открытых файлов инф. базы на файловом хранилище
    /// </summary>
    internal class CloseOpenDatabaseFilesOnFileStorageProvider(
        IOpenDatabaseFilesOnFileStorageDataProvider openDatabaseFilesOnFileStorageDataProvider,
        PowerShellClientWrapper powerShellClientWrapper,
        IHandlerException handlerException)
        : ICloseOpenDatabaseFilesOnFileStorageProvider
    {
        /// <summary>
        /// Закрыть открытые файлы инф. базы на файловом хранилище
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        public void Close(Guid databaseId)
        {
            var manageOpenDatabaseFilesData =
                openDatabaseFilesOnFileStorageDataProvider.GetDataForManageOpenDatabaseFilesOnFileStorage(databaseId);

            try
            {
                powerShellClientWrapper.ExecuteScript(
                    $"Get-SmbOpenFile | where {{$_.Path –like \"*{manageOpenDatabaseFilesData.V82Name}*\"}} | Close-SmbOpenFile -Force",
                    manageOpenDatabaseFilesData.DnsName);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $@"[Ошибка закрытия открытых файлов инф. базы] : '[{databaseId}]-{manageOpenDatabaseFilesData.V82Name}' 
                                на сервере '{manageOpenDatabaseFilesData.DnsName}'");
                throw;
            }
        }
    }
}
