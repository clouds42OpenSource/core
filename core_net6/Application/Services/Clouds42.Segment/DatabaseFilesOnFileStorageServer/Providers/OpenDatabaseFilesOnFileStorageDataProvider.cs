﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.DatabaseFilesOnFileStorageServer.Interfaces;

namespace Clouds42.Segment.DatabaseFilesOnFileStorageServer.Providers
{
    /// <summary>
    /// Провайдер для работы с данными открытых файлов
    /// инф. базы на файловом хранилище
    /// </summary>
    internal class OpenDatabaseFilesOnFileStorageDataProvider(IUnitOfWork dbLayer)
        : IOpenDatabaseFilesOnFileStorageDataProvider
    {
        /// <summary>
        /// Получить данные для управления открытыми файлами
        /// инф. базы на файловом хранилище
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Данные для управления открытыми файлами
        /// инф. базы на файловом хранилище</returns>
        public ManageOpenDatabaseFilesOnFileStorageDataDto GetDataForManageOpenDatabaseFilesOnFileStorage(
            Guid databaseId) =>
            (from database in dbLayer.DatabasesRepository.WhereLazy()
                join fileStorageServer in dbLayer.CloudServicesFileStorageServerRepository.WhereLazy() on
                    database.FileStorageID equals fileStorageServer.ID
                where database.Id == databaseId && fileStorageServer.DnsName != null
                select new ManageOpenDatabaseFilesOnFileStorageDataDto
                {
                    V82Name = database.V82Name,
                    DnsName = fileStorageServer.DnsName
                }).FirstOrDefault() ?? throw new NotFoundException(
                $"Не удалось получить данные для управления открытыми файлами инф. базы '{databaseId}' на файловом хранилище");
    }
}