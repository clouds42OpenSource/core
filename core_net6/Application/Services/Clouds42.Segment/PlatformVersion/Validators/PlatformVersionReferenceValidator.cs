﻿using System.Text.RegularExpressions;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Segment.PlatformVersion.Validators
{
    /// <summary>
    ///     Валидатор для PlatformVersionReference
    /// </summary>
    public class PlatformVersionReferenceValidator(IUnitOfWork unitOfWork)
    {
        private readonly Regex _platformVersionRegex = new(@"^\d+[0-9.]*$", RegexOptions.None, TimeSpan.FromSeconds(.5));

        /// <summary>
        /// Проверить платформу
        /// </summary>
        /// <param name="platformVersionReference">Версия платформы</param>
        /// <param name="errorMessage">Результат валидации</param>
        /// <returns>Результат валидации</returns>
        public void ValidatePlatform(PlatformVersion1CDto platformVersionReference, out string errorMessage)
        {
            try
            {
                ValidateForCorrectInput(platformVersionReference);
                ValidateForExistence(platformVersionReference.Version);
                errorMessage = null;
            }
            catch (Exception exception)
            {
                errorMessage = exception.Message;
            }
        }

        /// <summary>
        /// Проверить платформу перед удалением
        /// </summary>
        /// <param name="platformVersion">Версия платформы</param>
        /// <returns>Результат валидации</returns>
        public string ValidatePlatformBeforeDelete(string platformVersion)
        {
            var segmentsWhichUsePlatform = unitOfWork.CloudServicesSegmentRepository.Where(segment =>
                segment.Alpha83Version.Equals(platformVersion) || segment.Stable82Version.Equals(platformVersion) ||
                segment.Stable83Version.Equals(platformVersion));

            return segmentsWhichUsePlatform.Any() 
                ? "Убедитесь, что на данную платформу не ссылаются на другие элементы" 
                : string.Empty;
        }

        /// <summary>
        ///     Проверка на верный ввод данных
        /// при создании или редактировании
        /// </summary>
        private void ValidateForCorrectInput(PlatformVersion1CDto platformVersionReference)
        {
            if (!_platformVersionRegex.IsMatch(platformVersionReference.Version))
                throw new ValidateException($"Введенная вами версия ({platformVersionReference.Version}) не валидна. Версия может состоять только из чисел и точек");
        }

        /// <summary>
        ///     Проверка на дубликат в базе
        /// </summary>
        private void ValidateForExistence(string version)
        {
            var existedPlatformVersionReference =
                unitOfWork.PlatformVersionReferencesRepository.FirstOrDefault(x => x.Version == version);

            if (existedPlatformVersionReference != null)
                throw new ValidateException($"Объект с версией {version} уже существует!");
        }
    }
}
