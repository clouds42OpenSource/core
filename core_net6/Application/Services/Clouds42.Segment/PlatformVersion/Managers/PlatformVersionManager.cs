﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PlatformVersion.Interfaces;
using Clouds42.Segment.PlatformVersion.Validators;

namespace Clouds42.Segment.PlatformVersion.Managers
{
    /// <summary>
    /// Менеджер для работы с версией платформы
    /// </summary>
    public class PlatformVersionManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IEditPlatformVersionProvider editPlatformVersionProvider,
        ICreatePlatformVersionProvider createPlatformVersionProvider,
        IDeletePlatformVersionProvider deletePlatformVersionProvider,
        IHandlerException handlerException,
        PlatformVersionReferenceValidator platformVersionReferenceValidator,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        ///     Метод добавления новой платформы
        /// </summary>
        /// <param name="newPlatformVersionReference">Новый объект PlatformVersionReference.</param>
        public ManagerResult AddNewPlatformVersionReference(PlatformVersion1CDto newPlatformVersionReference)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewPlatformVersionReference,
                    () => AccessProvider.ContextAccountId);

                platformVersionReferenceValidator.ValidatePlatform(newPlatformVersionReference, out var errorMessage);

                if (!string.IsNullOrWhiteSpace(errorMessage))
                    return ValidationError(errorMessage);

                createPlatformVersionProvider.Create(newPlatformVersionReference);

                var message = $"Создана платформа \"{newPlatformVersionReference.Version}\".";
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);
                logger.Trace(message);
                return Ok();
            }
            catch (Exception exception)
            {
                var errorMessage = $"[Ошибка добавления новой платформы \"{newPlatformVersionReference.Version}\".]";
                handlerException.Handle(exception, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, $"{errorMessage}. Причина: {exception.Message}");
                return PreconditionFailed(exception.Message);
            }
        }

        /// <summary>
        ///     Метод редактирования платформы
        /// </summary>
        /// <param name="platformVersionReference">Обновленная платформа.</param>
        public ManagerResult EditExistedPlatformVersionReference(PlatformVersion1CDto platformVersionReference)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdatePlatformVersionReference,
                    () => AccessProvider.ContextAccountId);

                editPlatformVersionProvider.Edit(platformVersionReference);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement,
                    $"Платформа {platformVersionReference.Version} отредактирована");
                logger.Trace(
                    $"Успешно отредактирована платформа {platformVersionReference.Version} пользователем {AccessProvider.Name}");
                return Ok();
            }
            catch (Exception exception)
            {
                var errorMessage = $"[Ошибка редактирования платформы \"{platformVersionReference.Version}\".]";
                handlerException.Handle(exception, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, $"{errorMessage}. Причина: {exception.Message}");

                return PreconditionFailed(exception.Message);
            }
        }

        /// <summary>
        ///     Удаление платформы
        /// </summary>
        /// <param name="platformVersion">Версия платформы</param>
        public ManagerResult<bool> DeletePlatformVersionReference(string platformVersion)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeletePlatformVersionReference,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = platformVersionReferenceValidator.ValidatePlatformBeforeDelete(platformVersion);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError<bool>(validationMessage);

                deletePlatformVersionProvider.Delete(platformVersion);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement,
                    $"Удалена платформа {platformVersion}");
                logger.Trace($"Успешно удалена платформа {platformVersion} пользователем {AccessProvider.Name}");
                return Ok(true);
            }
            catch (Exception exception)
            {
                handlerException.Handle(exception,
                    $"[Ошибка удаления платформы {platformVersion} пользователем {AccessProvider.Name}.]");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement,
                    $"Ошибка удаления платформы {platformVersion}. Причина: {exception.Message}");
                return PreconditionFailed<bool>(exception.Message);
            }
        }
    }
}
