﻿using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PlatformVersion.Interfaces;

namespace Clouds42.Segment.PlatformVersion.Providers
{
    /// <summary>
    /// Провайдер для удаления версии платформы
    /// </summary>
    internal class DeletePlatformVersionProvider(IUnitOfWork dbLayer, IPlatformVersionProvider platformVersionProvider)
        : IDeletePlatformVersionProvider
    {
        /// <summary>
        /// Удалить версию платформы
        /// </summary>
        /// <param name="version">Версия платформы</param>
        public void Delete(string version)
        {
            var existedPlatform = (PlatformVersionReference) platformVersionProvider.GetPlatformVersion(version);
            dbLayer.PlatformVersionReferencesRepository.Delete(existedPlatform);
            dbLayer.Save();
        }
    }
}
