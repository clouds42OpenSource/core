﻿using Clouds42.Common.Exceptions;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PlatformVersion.Interfaces;

namespace Clouds42.Segment.PlatformVersion.Providers
{
    /// <summary>
    /// Провайдер для работы с версией платформы
    /// </summary>
    internal class PlatformVersionProvider(IUnitOfWork dbLayer) : IPlatformVersionProvider
    {
        /// <summary>
        /// Получить версию платформы
        /// </summary>
        /// <param name="version">Версия</param>
        /// <returns>Версия платформы</returns>
        public IPlatformVersionReference GetPlatformVersion(string version) =>
            dbLayer.PlatformVersionReferencesRepository.FirstOrDefault(pv => pv.Version == version) ??
            throw new NotFoundException($"Не удалось получить версию платформы {version}");
    }
}
