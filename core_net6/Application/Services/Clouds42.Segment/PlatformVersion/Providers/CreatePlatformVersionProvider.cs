﻿using System.Text;
using Clouds42.Configurations;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PlatformVersion.Interfaces;

namespace Clouds42.Segment.PlatformVersion.Providers
{
    /// <summary>
    /// Провайдер для создания версии платформы
    /// </summary>
    internal class CreatePlatformVersionProvider(IUnitOfWork dbLayer) : ICreatePlatformVersionProvider
    {
        /// <summary>
        /// Создать версию платформы
        /// </summary>
        /// <param name="platformVersion">Модель версии платформы</param>
        public void Create(PlatformVersion1CDto platformVersion)
        {
            var newPlatformVersionReference = new PlatformVersionReference
            {
                Version = platformVersion.Version,
                PathToPlatform = platformVersion.PathToPlatform,
                PathToPlatfromX64 = platformVersion.PathToPlatformX64,
                MacOsThinClientDownloadLink = platformVersion.MacOsThinClientDownloadLink,
                WindowsThinClientDownloadLink = platformVersion.WindowsThinClientDownloadLink
            };

            dbLayer.PlatformVersionReferencesRepository.Insert(newPlatformVersionReference);
            dbLayer.Save();

            SendEmailToSupport(CreateEmailBody(platformVersion));
        }

        /// <summary>
        /// Отправка сообщения на саппорт
        /// </summary>
        /// <param name="text">Текст письма</param>
        private void SendEmailToSupport(string text)
        {
            var hotlineMail = ConfigurationHelper.GetConfigurationValue("HotlineEmail");

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(hotlineMail)
                .Subject("Новая версия платформы 1С")
                .Body(text)
                .SendViaNewThread();
        }

        /// <summary>
        /// Сформировать тело письма
        /// </summary>
        /// <param name="platformVersion">Версия плаформы 1С</param>
        /// <returns>Тело письма</returns>
        private string CreateEmailBody(PlatformVersion1CDto platformVersion)
        {
            var emailBody = new StringBuilder();
            
            emailBody.AppendLine(
                $"Была добавлена новая платформа - {platformVersion.Version}.<br/>");
            emailBody.AppendLine($"Необходимо установить версии х32 (путь: {platformVersion.PathToPlatform}) и х64 (х64 обязательно нужно устанавливать с компонентом - Администрирование сервера 1С:Предприятия) (путь: {platformVersion.PathToPlatformX64}) <br/>");
            emailBody.AppendLine("на следующих серверах: <br/>");

            var coreWorkerAddresses = GetCoreWorkerAddresses();
            coreWorkerAddresses.ForEach(workerAddress => { emailBody.AppendLine($"-{workerAddress} <br/>"); });

            var autoUpdateNodes = GetAutoUpdateNodes();
            autoUpdateNodes.ForEach(node => { emailBody.AppendLine($"-{node} <br/>"); });

            return emailBody.ToString();
        }

        /// <summary>
        /// Получить список адресов воркеров, выполняющих задачи
        /// </summary>
        /// <returns>Список адресов воркеров, выполняющих задачу AccountDatabaseAutoUpdateJob</returns>
        private List<string> GetCoreWorkerAddresses() => dbLayer
            .GetGenericRepository<CoreWorkerAvailableTasksBag>()
            .Where(x => x.CoreWorkerTask.TaskName == CoreWorkerTasksCatalog.AccountDatabaseAutoUpdateJob || 
                        x.CoreWorkerTask.TaskName == CoreWorkerTasksCatalog.TehSupportAccountDatabasesJob ||
                        x.CoreWorkerTask.TaskName == CoreWorkerTasksCatalog.TerminateSessionsInDatabaseJob || 
                        x.CoreWorkerTask.TaskName == CoreWorkerTasksCatalog.RestoreAccountDatabaseFromTombJob || 
                        x.CoreWorkerTask.TaskName == CoreWorkerTasksCatalog.DeleteAccountDatabaseJob)
            .Select(x => x.CoreWorker.CoreWorkerAddress)
            .Distinct()
            .ToList();

        private List<string> GetAutoUpdateNodes() => dbLayer.AutoUpdateNodeRepository
            .AsQueryableNoTracking()
            .Select(x => x.NodeAddress)
            .Distinct()
            .ToList();

    }
}
