﻿using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PlatformVersion.Interfaces;

namespace Clouds42.Segment.PlatformVersion.Providers
{
    /// <summary>
    /// Провайдер для редактирования версии платформы
    /// </summary>
    internal class EditPlatformVersionProvider(IUnitOfWork dbLayer, IPlatformVersionProvider platformVersionProvider)
        : IEditPlatformVersionProvider
    {
        /// <summary>
        /// Редактировать версию платформы
        /// </summary>
        /// <param name="platformVersion">Версия платформы</param>
        public void Edit(PlatformVersion1CDto platformVersion)
        {
            var existedPlatform =
                (PlatformVersionReference) platformVersionProvider.GetPlatformVersion(platformVersion.Version);

            existedPlatform.PathToPlatform = platformVersion.PathToPlatform;
            existedPlatform.PathToPlatfromX64 = platformVersion.PathToPlatformX64;
            existedPlatform.MacOsThinClientDownloadLink = platformVersion.MacOsThinClientDownloadLink;
            existedPlatform.WindowsThinClientDownloadLink = platformVersion.WindowsThinClientDownloadLink;

            dbLayer.PlatformVersionReferencesRepository.Update(existedPlatform);
            dbLayer.Save();
        }
    }
}
