﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Validators.CloudServer;

namespace Clouds42.Segment.CloudServicesFileStorageServer.Validators
{
    /// <summary>
    /// Валидатор моделей файлового хранилища
    /// </summary>
    public static class CloudFileStorageServerValidator
    {
        /// <summary>
        /// Выполнить валидацию модели создания файлового хранилища
        /// </summary>
        /// <param name="createCloudFileStorageServerDto">Модель создания файлового хранилища</param>
        public static void ValidateModel(this CreateCloudFileStorageServerDto createCloudFileStorageServerDto)
        {
            createCloudFileStorageServerDto.Validate();

            if (createCloudFileStorageServerDto.DnsName.IsNullOrEmpty())
                throw new ValidateException("Поле 'DNS имя хранилища' обязательное для заполнения");
        }
    }
}
