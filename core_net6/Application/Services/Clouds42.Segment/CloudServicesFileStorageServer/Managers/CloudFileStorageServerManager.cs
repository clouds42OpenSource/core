﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces;
using Clouds42.Segment.SegmentElementsHelper;

namespace Clouds42.Segment.CloudServicesFileStorageServer.Managers
{
    /// <summary>
    /// Менеджер для работы с файловым хранилищем
    /// </summary>
    public class CloudFileStorageServerManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateCloudFileStorageServerProvider createCloudFileStorageServerProvider,
        IEditCloudFileStorageServerProvider editCloudFileStorageServerProvider,
        IDeleteCloudFileStorageServerProvider deleteCloudFileStorageServerProvider,
        ICloudFileStorageServerProvider cloudFileStorageServerProvider,
        IHandlerException handlerException,
        SegmentElementDeletionLogEventHelper segmentElementDeletionLogEventHelper,
        SegmentElementCreationLogEventHelper segmentElementCreationLogEventHelper,
        SegmentElementChangesLogEventHelper segmentElementChangesLogEventHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), ICloudFileStorageServerManager
    {


        /// <summary>
        /// Создать файловое хранилище
        /// </summary>
        /// <param name="createCloudFileStorageServerDto">Модель создания
        /// файлового хранилища</param>
        /// <returns>Id созданного файлового хранилища</returns>
        public ManagerResult<Guid> CreateCloudFileStorageServer(
            CreateCloudFileStorageServerDto createCloudFileStorageServerDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var fileStorageId = createCloudFileStorageServerProvider.Create(createCloudFileStorageServerDto);

                var message = segmentElementCreationLogEventHelper.GetCreationLogEventDescription(createCloudFileStorageServerDto);
                logger.Trace(
                    $"Успешно добавлено новое файловое хранилище {createCloudFileStorageServerDto.Name} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);
                return Ok(fileStorageId);
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"[Ошибка добавления нового файлового хранилища \"{createCloudFileStorageServerDto.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать файловое хранилище
        /// </summary>
        /// <param name="cloudFileStorageServerDto">Модель файлового хранилища</param>
        public ManagerResult EditCloudFileStorageServer(CloudFileStorageServerDto cloudFileStorageServerDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                var currentFileStorageServer = cloudFileStorageServerProvider.GetCloudFileStorageServerDto(cloudFileStorageServerDto.Id);
                var logEventDescription =
                    segmentElementChangesLogEventHelper.GetChangesDescription(currentFileStorageServer, cloudFileStorageServerDto);

                editCloudFileStorageServerProvider.Edit(cloudFileStorageServerDto);

                logger.Trace($"{logEventDescription} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, logEventDescription);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка редактирования файлового хранилища \"{cloudFileStorageServerDto.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить файловое хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        public ManagerResult DeleteCloudFileStorageServer(Guid fileStorageId)
        {
            var getFileStorageServerResult = GetCloudFileStorageServerDto(fileStorageId);
            if (getFileStorageServerResult.Error)
                return PreconditionFailed(getFileStorageServerResult.Message);

            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                deleteCloudFileStorageServerProvider.Delete(fileStorageId);
                
                logger.Trace(
                    $"Успешно удалено файловое хранилище {fileStorageId} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement,
                    segmentElementDeletionLogEventHelper.GetDeletionLogEventDescription(getFileStorageServerResult.Result));

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления файлового хранилища \"{getFileStorageServerResult.Result.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель файлового хранилища
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Модель файлового хранилища</returns>
        public ManagerResult<CloudFileStorageServerDto> GetCloudFileStorageServerDto(Guid fileStorageId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var cloudBackupStorage = cloudFileStorageServerProvider.GetCloudFileStorageServerDto(fileStorageId);
                return Ok(cloudBackupStorage);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения файлового хранилища {fileStorageId}]");
                return PreconditionFailed<CloudFileStorageServerDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить ID файлового хранилища по умолчанию
        /// </summary>
        /// <returns>ID файлового хранилища по умолчанию</returns>
        public async Task<ManagerResult<Guid>> GetDefaultFileStorageServerId()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CloudFileStorageServers_View);
                var defaultFileStorageId = await cloudFileStorageServerProvider.GetDefaultFileStorageIdAsync();
                return Ok(defaultFileStorageId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка получения ID файлового хранилища по умолчанию]");

                return NotFound<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Получить путь файлового хранилища
        /// </summary>
        /// <param name="fileStorageServerId">ID файлового хранилища</param>
        /// <returns>Путь файлового хранилища</returns>
        public async Task<ManagerResult<string>> GetFileStorageServerPath(Guid fileStorageServerId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CloudFileStorageServers_View);
                var fileStoragePath = await cloudFileStorageServerProvider.GetFileStoragePathAsync(fileStorageServerId);
                return Ok(fileStoragePath);
            }
            catch (Exception ex)
            {
                logger.Warn(ex,$"Ошибка получения пути файлового хранилища {fileStorageServerId}");
                return NotFound<string>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные для маунта файлового хранилища
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <returns>Данные для маунта файлового хранилища</returns>
        public ManagerResult<CloudFileStorageServerInfoDto> GetFileStorageMountInfo(string userName)
        {
            try
            {
                return Ok(cloudFileStorageServerProvider.GetFileStorageMountInfo(userName));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка данных для маунта файлового хранилища для пользователя {userName}]");
                return NotFound<CloudFileStorageServerInfoDto>(ex.Message);
            }
        }
    }
}
