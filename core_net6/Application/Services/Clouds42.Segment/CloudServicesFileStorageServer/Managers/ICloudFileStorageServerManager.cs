﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;

namespace Clouds42.Segment.CloudServicesFileStorageServer.Managers;

public interface ICloudFileStorageServerManager
{
    /// <summary>
    /// Создать файловое хранилище
    /// </summary>
    /// <param name="createCloudFileStorageServerDto">Модель создания
    /// файлового хранилища</param>
    /// <returns>Id созданного файлового хранилища</returns>
    ManagerResult<Guid> CreateCloudFileStorageServer(
        CreateCloudFileStorageServerDto createCloudFileStorageServerDto);

    /// <summary>
    /// Редактировать файловое хранилище
    /// </summary>
    /// <param name="cloudFileStorageServerDto">Модель файлового хранилища</param>
    ManagerResult EditCloudFileStorageServer(CloudFileStorageServerDto cloudFileStorageServerDto);

    /// <summary>
    /// Удалить файловое хранилище
    /// </summary>
    /// <param name="fileStorageId">Id файлового хранилища</param>
    ManagerResult DeleteCloudFileStorageServer(Guid fileStorageId);

    /// <summary>
    /// Получить модель файлового хранилища
    /// </summary>
    /// <param name="fileStorageId">Id файлового хранилища</param>
    /// <returns>Модель файлового хранилища</returns>
    ManagerResult<CloudFileStorageServerDto> GetCloudFileStorageServerDto(Guid fileStorageId);

    /// <summary>
    /// Получить ID файлового хранилища по умолчанию
    /// </summary>
    /// <returns>ID файлового хранилища по умолчанию</returns>
    Task<ManagerResult<Guid>> GetDefaultFileStorageServerId();

    /// <summary>
    /// Получить путь файлового хранилища
    /// </summary>
    /// <param name="fileStorageServerId">ID файлового хранилища</param>
    /// <returns>Путь файлового хранилища</returns>
    Task<ManagerResult<string>> GetFileStorageServerPath(Guid fileStorageServerId);

    /// <summary>
    /// Получить данные для маунта файлового хранилища
    /// </summary>
    /// <param name="userName">Логин пользователя</param>
    /// <returns>Данные для маунта файлового хранилища</returns>
    ManagerResult<CloudFileStorageServerInfoDto> GetFileStorageMountInfo(string userName);
}
