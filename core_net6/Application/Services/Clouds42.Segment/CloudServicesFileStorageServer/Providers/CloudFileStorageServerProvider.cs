﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Segment.CloudServicesFileStorageServer.Providers
{
    /// <summary>
    /// Провайдер для работы с файловым хранилищем
    /// </summary>
    internal class CloudFileStorageServerProvider(
        IUnitOfWork dbLayer,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        IServiceProvider serviceProvider)
        : ICloudFileStorageServerProvider
    {
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider = serviceProvider.GetRequiredService<IAccountConfigurationDataProvider>();

        /// <summary>
        /// Получить файловое хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Файловое хранилище</returns>
        public Domain.DataModels.CloudServicesFileStorageServer GetCloudFileStorageServer(Guid fileStorageId) =>
            dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(fs => fs.ID == fileStorageId) ??
            throw new NotFoundException($"Не удалось получить файловое франилище {fileStorageId}");

        /// <summary>
        /// Получить модель файлового хранилища
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Модель файлового хранилища</returns>
        public CloudFileStorageServerDto GetCloudFileStorageServerDto(Guid fileStorageId)
        {
            var fileStorage = GetCloudFileStorageServer(fileStorageId);

            return new CloudFileStorageServerDto
            {
                Id = fileStorage.ID,
                Name = fileStorage.Name,
                Description = fileStorage.Description,
                ConnectionAddress = fileStorage.ConnectionAddress,
                DnsName = fileStorage.DnsName
            };
        }

        /// <summary>
        /// Получить список аккаунтов
        /// которые используют хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Список аккаунтов
        /// которые используют хранилище</returns>
        public List<Account> GetAccountsThatUseStorage(Guid fileStorageId) =>
            _accountConfigurationDataProvider.GetAccountsWithConfiguration()
                .Where(accountWithConfiguration =>
                    accountWithConfiguration.AccountConfiguration.FileStorageId == fileStorageId &&
                    (string.IsNullOrEmpty(accountWithConfiguration.Account.Status) ||
                     accountWithConfiguration.Account.Status != "Deleted"))
                .Select(accountWithConfiguration => accountWithConfiguration.Account).ToList();

        /// <summary>
        /// Получить список информационных баз
        /// которые используют хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Список баз
        /// которые используют хранилище</returns>
        public List<Domain.DataModels.AccountDatabase> GetAccountDatabasesThatUseStorage(Guid fileStorageId) =>
            dbLayer.DatabasesRepository.WhereLazy(x =>
                x.FileStorageID == fileStorageId && x.State == DatabaseState.Ready.ToString()).ToList();

        /// <summary>
        /// Получить список сегментов
        /// которые используют хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Список сегментов
        /// которые используют хранилище</returns>
        public List<CloudServicesSegmentStorage> GetSegmentsThatUseStorage(Guid fileStorageId) => dbLayer
            .CloudServicesSegmentStorageRepository.Where(x => x.FileStorageID == fileStorageId).ToList();

        /// <summary>
        /// Получить истории миграций инф. баз
        /// для хранилища
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Истории миграций инф. баз
        /// для хранилища</returns>
        public List<MigrationAccountDatabaseHistory>
            GetMigrationDbHistoriesForStorage(Guid fileStorageId) => dbLayer.MigrationAccountDatabaseHistoryRepository
            .Where(x => x.SourceFileStorageId == fileStorageId || x.TargetFileStorageId == fileStorageId).ToList();

        /// <summary>
        /// Получить ID файлового хранилища по умолчанию
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>ID файлового хранилища по умолчанию</returns>
        public Guid GetDefaultFileStorageId(Guid localeId)
        {
            var defaultSegmentId = localesConfigurationDataProvider.GetDefaultSegmentId(localeId);
            var defaultSegment =
                dbLayer.CloudServicesSegmentRepository.FirstOrDefault(segm => segm.ID == defaultSegmentId)
                ?? throw new NotFoundException($"Сегмент по умолчанию по ID {defaultSegmentId} не найден");

            return defaultSegment.FileStorageServersID;
        }

        /// <summary>
        /// Получить ID файлового хранилища по умолчанию
        /// </summary>
        /// <returns>ID файлового хранилища по умолчанию</returns>
        public async Task<Guid> GetDefaultFileStorageIdAsync()
        {
            var defaultSegment =
                await dbLayer.CloudServicesSegmentRepository.FirstOrDefaultAsync(segm => segm.IsDefault)
                ?? throw new NotFoundException("Сегмент по умолчанию не найден");

            return defaultSegment.FileStorageServersID;
        }

        /// <summary>
        /// Получить путь файлового хранилища
        /// </summary>
        /// <param name="fileStorageServerId">ID файлового хранилища</param>
        /// <returns>Путь файлового хранилища</returns>
        public async Task<string> GetFileStoragePathAsync(Guid fileStorageServerId)
        {
            var fileStorageServer =
                await dbLayer.CloudServicesFileStorageServerRepository.GetByIdAsync(fileStorageServerId) ??
                throw new NotFoundException($"Файловое хранилище по ID {fileStorageServerId} не найдено");

            return fileStorageServer.ConnectionAddress;
        }

        /// <summary>
        /// Получить данные для маунта файлового хранилища
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <returns>Данные для маунта файлового хранилища</returns>
        public CloudFileStorageServerInfoDto GetFileStorageMountInfo(string userName)
        {
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(acUs => acUs.Login == userName)
                              ?? throw new NotFoundException($"Пользователь по логину {userName} не найден");

            var account = dbLayer.AccountsRepository.FirstOrDefault(ac => ac.Id == accountUser.AccountId)
                          ?? throw new NotFoundException($"Аккаунт по ID {accountUser.AccountId} не найден");

            return new CloudFileStorageServerInfoDto
            {
                AccountId = account.Id,
                AccountUser = accountUser.Id,
                CompanyNumber = account.IndexNumber,
                StorageId = serviceProvider.GetRequiredService<ISegmentHelper>()
                    .GetClientFileStorage(account).ID
            };
        }
    }
}