﻿using Clouds42.Common.Exceptions;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces;

namespace Clouds42.Segment.CloudServicesFileStorageServer.Providers
{
    /// <summary>
    /// Провайдер удаления файлового хранилища
    /// </summary>
    internal class DeleteCloudFileStorageServerProvider(
        IUnitOfWork dbLayer,
        ICloudFileStorageServerProvider cloudFileStorageServerProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IDeleteCloudFileStorageServerProvider
    {
        /// <summary>
        /// Удалить файловое хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        public void Delete(Guid fileStorageId)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                logger.Trace($"Начинаем удаление файлового хранилища {fileStorageId}");
                var storage = cloudFileStorageServerProvider.GetCloudFileStorageServer(fileStorageId);

                CheckAbilityToDeleteFileStorage(fileStorageId);
                RemoveMigrationDbHistoriesForStorage(fileStorageId);

                dbLayer.CloudServicesFileStorageServerRepository.Delete(storage);
                dbLayer.Save();

                dbScope.Commit();
                logger.Trace("Файловое хранилище удачно удалено");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления файлового хранилища] {fileStorageId}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Удалить истории миграций инф. баз
        /// для файлового хранилища
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        private void RemoveMigrationDbHistoriesForStorage(Guid fileStorageId)
        {
            var migrationAccountDatabaseHistories =
                cloudFileStorageServerProvider.GetMigrationDbHistoriesForStorage(fileStorageId);

            if (!migrationAccountDatabaseHistories.Any())
                return;

            logger.Trace("Начало удаления истории миграции ИБ");
            foreach (var data in migrationAccountDatabaseHistories)
                dbLayer.MigrationAccountDatabaseHistoryRepository.Delete(data.HistoryId);
            dbLayer.Save();
        }

        /// <summary>
        /// Проверить возможность удаления файлового хранилища
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        private void CheckAbilityToDeleteFileStorage(Guid fileStorageId)
        {
            var usedAccuontStorage = cloudFileStorageServerProvider.GetAccountsThatUseStorage(fileStorageId)
                .Select(a => a.IndexNumber).ToList();

            if (usedAccuontStorage.Any())
            {
                logger.Warn($"Eсть аккаунты которые используют это файловое хранилище {fileStorageId}");
                throw new ValidateException(
                    "В этом файловом хранилище есть аккаунты" +
                    $" ({string.Join(",", usedAccuontStorage.Count > 20 ? usedAccuontStorage.Take(20) : usedAccuontStorage)}) которые используют этот сегмент");
            }

            var usedAccountDatabasesStorage = cloudFileStorageServerProvider.GetAccountDatabasesThatUseStorage(fileStorageId);
            if (usedAccountDatabasesStorage.Any())
            {
                logger.Warn($"Есть ИБ которые используют это файловое хранилище {fileStorageId}");
                throw new ValidateException("В этом файловом хранилище есть ИБ которые используют это файловое хранилище");
            }

            var usedSegmentStorage = cloudFileStorageServerProvider.GetSegmentsThatUseStorage(fileStorageId);

            if (usedSegmentStorage.Count > 0)
                throw new ValidateException($"Данное файловое хранилище используют {usedSegmentStorage.Count} сегментов");
        }
    }
}
