﻿using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudServicesFileStorageServer.Validators;
using Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces;

namespace Clouds42.Segment.CloudServicesFileStorageServer.Providers
{
    /// <summary>
    /// Провайдер для редактирования файлового хранилища
    /// </summary>
    internal class EditCloudFileStorageServerProvider(
        IUnitOfWork dbLayer,
        ICloudFileStorageServerProvider cloudFileStorageServerProvider)
        : IEditCloudFileStorageServerProvider
    {
        /// <summary>
        /// Редактировать файловое хранилище
        /// </summary>
        /// <param name="cloudFileStorageServerDto">Модель файлового хранилища</param>
        public void Edit(CloudFileStorageServerDto cloudFileStorageServerDto)
        {
            cloudFileStorageServerDto.ValidateModel();
            var fileStorage = cloudFileStorageServerProvider.GetCloudFileStorageServer(cloudFileStorageServerDto.Id);
            fileStorage.Name = cloudFileStorageServerDto.Name;
            fileStorage.ConnectionAddress = cloudFileStorageServerDto.ConnectionAddress;
            fileStorage.Description = cloudFileStorageServerDto.Description;
            fileStorage.PhysicalPath = cloudFileStorageServerDto.ConnectionAddress;
            fileStorage.DnsName = cloudFileStorageServerDto.DnsName;

            dbLayer.CloudServicesFileStorageServerRepository.Update(fileStorage);
            dbLayer.Save();
        }
    }
}
