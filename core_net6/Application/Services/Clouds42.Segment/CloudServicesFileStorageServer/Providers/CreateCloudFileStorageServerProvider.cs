﻿using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudServicesFileStorageServer.Validators;
using Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces;

namespace Clouds42.Segment.CloudServicesFileStorageServer.Providers
{
    /// <summary>
    /// Провайдер для создания файлового хранилища
    /// </summary>
    internal class CreateCloudFileStorageServerProvider(IUnitOfWork dbLayer) : ICreateCloudFileStorageServerProvider
    {
        /// <summary>
        /// Создать файловое хранилище
        /// </summary>
        /// <param name="createCloudFileStorageServerDto">Модель создания
        /// файлового хранилища</param>
        /// <returns>Id созданного файлового хранилища</returns>
        public Guid Create(CreateCloudFileStorageServerDto createCloudFileStorageServerDto)
        {
            createCloudFileStorageServerDto.ValidateModel();

            var cloudFileStorageServer = new Domain.DataModels.CloudServicesFileStorageServer
            {
                ID = Guid.NewGuid(),
                Name = createCloudFileStorageServerDto.Name,
                Description = createCloudFileStorageServerDto.Description,
                ConnectionAddress = createCloudFileStorageServerDto.ConnectionAddress,
                PhysicalPath = createCloudFileStorageServerDto.ConnectionAddress,
                DnsName = createCloudFileStorageServerDto.DnsName
            };

            dbLayer.CloudServicesFileStorageServerRepository.Insert(cloudFileStorageServer);
            dbLayer.Save();

            return cloudFileStorageServer.ID;
        }
    }
}
