﻿using System.Text;
using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.CloudServicesSegment.Interface;
using Clouds42.Common.Extensions;

namespace Clouds42.Segment.SegmentElementsHelper
{
    /// <summary>
    /// Хэлпер для генерации сообщений логирования при изменении элементов сегмента
    /// </summary>
    public class SegmentElementChangesLogEventHelper
    {
        private readonly Dictionary<Type, string> _mapSegmentElementTypeToChangesHeader = new()
        {
            { typeof(CloudTerminalFarmDto), "Ферма ТС отредактирована." },
            { typeof(CloudSqlServerDto), "Sql сервер отредактирован." },
            { typeof(CloudGatewayTerminalDto), "Терминальный шлюз отредактирован." },
            { typeof(CloudFileStorageServerDto), "Файловое хранилище отредактировано." },
            { typeof(CloudBackupStorageDto), "Хранилище бекапов отредактировано." }
        };

        /// <summary>
        /// Получить описание изменений полей элемента сегмента
        /// </summary>
        /// <param name="currentCloudServer">Текущая модель сервера</param>
        /// <param name="editedCloudServer">Изменяемая модель сервера</param>
        /// <returns>Описание изменений полей элемента сегмента</returns>
        public string GetChangesDescription(ICloudServerDto currentCloudServer, ICloudServerDto editedCloudServer)
        {
            var messageBuilder = new StringBuilder();
            var changesHeader = _mapSegmentElementTypeToChangesHeader[currentCloudServer.GetType()];

            messageBuilder.AppendLine(changesHeader);
            AppendNameChangesIfNeeded(messageBuilder, currentCloudServer, editedCloudServer);
            AppendConnectionAddressChangesIfNeeded(messageBuilder, currentCloudServer, editedCloudServer);
            AppendDescriptionChangesIfNeeded(messageBuilder, currentCloudServer, editedCloudServer);

            AppendAdditionalChangesIfNeeded(messageBuilder, currentCloudServer, editedCloudServer);

            return messageBuilder.ToString();
        }

        /// <summary>
        /// Добавить строку с изменением названия если это необходимо
        /// </summary>
        /// <param name="messageBuilder">Билдер сообщения</param>
        /// <param name="currentCloudServer">Текущая модель сервера</param>
        /// <param name="editedCloudServer">Изменяемая модель сервера</param>
        private void AppendNameChangesIfNeeded(StringBuilder messageBuilder, ICloudServerDto currentCloudServer,
            ICloudServerDto editedCloudServer)
        {
            if(currentCloudServer.Name.Equals(editedCloudServer.Name))
                return;
            messageBuilder.AppendLine($"Изменено имя c \"{currentCloudServer.Name}\" на \"{editedCloudServer.Name}\".");
        }

        /// <summary>
        /// Добавить строку с изменением адреса подключения если это необходимо
        /// </summary>
        /// <param name="messageBuilder">Билдер сообщения</param>
        /// <param name="currentCloudServer">Текущая модель сервера</param>
        /// <param name="editedCloudServer">Изменяемая модель сервера</param>
        private void AppendConnectionAddressChangesIfNeeded(StringBuilder messageBuilder, ICloudServerDto currentCloudServer,
            ICloudServerDto editedCloudServer)
        {
            if (currentCloudServer.ConnectionAddress.Equals(editedCloudServer.ConnectionAddress))
                return;
            messageBuilder.AppendLine($"Изменен адрес подключения c \"{currentCloudServer.ConnectionAddress}\" на \"{editedCloudServer.ConnectionAddress}\".");
        }

        /// <summary>
        /// Добавить строку с изменением описания если это необходимо
        /// </summary>
        /// <param name="messageBuilder">Билдер сообщения</param>
        /// <param name="currentCloudServer">Текущая модель сервера</param>
        /// <param name="editedCloudServer">Изменяемая модель сервера</param>
        private void AppendDescriptionChangesIfNeeded(StringBuilder messageBuilder, ICloudServerDto currentCloudServer,
            ICloudServerDto editedCloudServer)
        {
            var currentCloudServiceDescription =
                GetSegmentElementDescriptionRepresentation(currentCloudServer.Description);
            var editedCloudServiceDescription =
                GetSegmentElementDescriptionRepresentation(editedCloudServer.Description);

            if (currentCloudServiceDescription.Equals(editedCloudServiceDescription))
                return;
            messageBuilder.AppendLine($"Изменено описание c \"{currentCloudServiceDescription}\" на \"{editedCloudServiceDescription}\".");
        }

        /// <summary>
        /// Добавить дополнительные изменения если они есть
        /// </summary>
        /// <param name="messageBuilder">Билдер сообщения</param>
        /// <param name="currentCloudServer">Текущая модель сервера</param>
        /// <param name="editedCloudServer">Изменяемая модель сервера</param>
        private void AppendAdditionalChangesIfNeeded(StringBuilder messageBuilder, ICloudServerDto currentCloudServer, ICloudServerDto editedCloudServer)
        {
            if (currentCloudServer.GetType() != typeof(CloudSqlServerDto))
                return;

            AppendSqlServerRestoreModelTypeChangesIfNeeded(messageBuilder, currentCloudServer, editedCloudServer);
        }

        /// <summary>
        /// Добавить изменения модели восстановления sql сервера если они есть
        /// </summary>
        /// <param name="messageBuilder">Билдер сообщения</param>
        /// <param name="currentCloudServer">Текущая модель сервера</param>
        /// <param name="editedCloudServer">Изменяемая модель сервера</param>
        private void AppendSqlServerRestoreModelTypeChangesIfNeeded(StringBuilder messageBuilder,
            ICloudServerDto currentCloudServer, ICloudServerDto editedCloudServer)
        {
            var currentSqlServer = (CloudSqlServerDto) currentCloudServer;
            var editedSqlServer = (CloudSqlServerDto) editedCloudServer;

            if (currentSqlServer.RestoreModelType == editedSqlServer.RestoreModelType)
                return;

            messageBuilder.AppendLine(
                $"Изменена модель восстановления c \"{currentSqlServer.RestoreModelType.Description()}\" на \"{editedSqlServer.RestoreModelType.Description()}\".");
        }

        /// <summary>
        /// Получить представление описания элемента сегмента
        /// </summary>
        /// <param name="description">Описание элемента сегмента</param>
        /// <returns>Представление описания элемента сегмента</returns>
        private string GetSegmentElementDescriptionRepresentation(string description)
            => string.IsNullOrEmpty(description)
                ? "---"
                : description;
    }
}
