﻿using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.Segment.SegmentElementsHelper
{
    /// <summary>
    /// Хэлпер для генерации сообщений логирования при удалении элементов сегмента
    /// </summary>
    public class SegmentElementDeletionLogEventHelper
    {
        private readonly Dictionary<Type, Func<ICloudServerDto, string>> _mapSegmentElementTypeToCreationDescription;

        public SegmentElementDeletionLogEventHelper()
        {
            _mapSegmentElementTypeToCreationDescription = new Dictionary<Type, Func<ICloudServerDto, string>>
            {
                { typeof(CloudTerminalFarmDto), cloudServer => $"Удалена ферма ТС \"{cloudServer.Name}\"" },
                { typeof(CloudSqlServerDto), cloudServer => $"Удален sql сервер \"{cloudServer.Name}\"" },
                { typeof(CloudGatewayTerminalDto), cloudServer => $"Удален терминальный шлюз \"{cloudServer.ConnectionAddress}\"" },
                { typeof(CloudFileStorageServerDto), cloudServer => $"Удалено файловое хранилище \"{cloudServer.Name}\"" },
                { typeof(CloudBackupStorageDto), cloudServer => $"Удалено хранилище бекапов \"{cloudServer.Name}\"" },
            };
        }

        /// <summary>
        /// Получить описание записи в лог при удалении элемента сегмента
        /// </summary>
        /// <param name="cloudServer">Модель сервера</param>
        /// <returns>Описание записи в лог при удалении элемента сегмента</returns>
        public string GetDeletionLogEventDescription(ICloudServerDto cloudServer)
        {
            return !_mapSegmentElementTypeToCreationDescription.ContainsKey(cloudServer.GetType())
                ? $"Удален элемент сегмента \"{cloudServer.ConnectionAddress}\""
                : _mapSegmentElementTypeToCreationDescription[cloudServer.GetType()](cloudServer);
        }
    }
}
