﻿using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.Segment.SegmentElementsHelper
{
    /// <summary>
    /// Хэлпер для генерации сообщений логирования при создании элементов сегмента
    /// </summary>
    public class SegmentElementCreationLogEventHelper
    {
        private readonly Dictionary<Type, Func<ICloudServerDto, string>> _mapSegmentElementTypeToCreationDescription;

        public SegmentElementCreationLogEventHelper()
        {
            _mapSegmentElementTypeToCreationDescription = new Dictionary<Type, Func<ICloudServerDto, string>>
            { 
                { typeof(CloudTerminalFarmDto), cloudServer => $"Создана ферма ТС \"{cloudServer.ConnectionAddress}\"" },
                { typeof(CreateCloudSqlServerDto), cloudServer => $"Создан sql сервер \"{cloudServer.Name}\"" },
                { typeof(CreateCloudGatewayTerminalDto), cloudServer => $"Создан терминальный шлюз \"{cloudServer.ConnectionAddress}\"" },
                { typeof(CreateCloudFileStorageServerDto), cloudServer => $"Создано файловое хранилише \"{cloudServer.Name}\"" },
                { typeof(CreateCloudBackupStorageDto), cloudServer => $"Создано хранилише бекапов \"{cloudServer.Name}\"" },
            };
        }

        /// <summary>
        /// Получить описание записи в лог при создании элемента сегмента
        /// </summary>
        /// <param name="cloudServer">Модель сервера</param>
        /// <returns>Описание записи в лог при создании элемента сегмента</returns>
        public string GetCreationLogEventDescription(ICloudServerDto cloudServer)
        {
            return !_mapSegmentElementTypeToCreationDescription.ContainsKey(cloudServer.GetType()) 
                ? $"Создан элемент сегмента \"{cloudServer.ConnectionAddress}\"" 
                : _mapSegmentElementTypeToCreationDescription[cloudServer.GetType()](cloudServer);
        }
    }
}
