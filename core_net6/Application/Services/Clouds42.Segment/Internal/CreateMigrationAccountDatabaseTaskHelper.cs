﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.CoreWorkerTask.Contracts.Models;
using Clouds42.CoreWorkerTask.Helpers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts;

namespace Clouds42.Segment.Internal
{

    /// <summary>
    /// Помошник создания задачи на миграцию информационной базы.
    /// </summary>
    public class CreateMigrationAccountDatabaseTaskHelper(
        ICoreWorkerTasksQueueManager coreWorkerTasksQueueManager,
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider)
        : ICreateMigrationAccountDatabaseTaskHelper
    {
        /// <summary>
        /// Создать задачу на миграцию
        /// </summary>
        /// <param name="accountDatabaseIds">Список ID инф. баз</param>
        /// <param name="targetFileStorageId">ID хранилища для переноса</param>
        public void CreateTask(List<Guid> accountDatabaseIds, Guid targetFileStorageId)
            => coreWorkerTasksQueueManager.AddTaskQueue(CreateCoreWorkerTask(accountDatabaseIds, targetFileStorageId));

        /// <summary>
        /// Создать задачу на миграцию
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="targetFileStorageId">ID хранилища для переноса</param>
        public void CreateTask(Guid accountDatabaseId, Guid targetFileStorageId)
        {
            coreWorkerTasksQueueManager.AddTaskQueue(CreateCoreWorkerTask([accountDatabaseId], targetFileStorageId));
        }

        /// <summary>
        /// Создать задачу воркера
        /// </summary>
        /// <param name="accountDatabaseIds">Список ID инф. баз</param>
        /// <param name="targetFileStorageId">ID хранилища для переноса</param>
        /// <returns>Задача воркера</returns>
        private CoreWorkerTasksDto CreateCoreWorkerTask(List<Guid> accountDatabaseIds, Guid targetFileStorageId)
            => new()
            {
                Comment = "Перенос баз между хранилищами",
                CloudTaskId = GetAccountDatabaseMigrationJobId(),
                TaskParams = CreateTaskParams(accountDatabaseIds, targetFileStorageId)
            };

        /// <summary>
        /// Получить ID задачи миграции инф. базы аккаунта
        /// </summary>
        /// <returns>ID задачи миграции инф. базы аккаунта</returns>
        private Guid GetAccountDatabaseMigrationJobId()
        {
            var task = dbLayer.CoreWorkerTaskRepository.FirstOrDefault(
                t => t.TaskName == CoreWorkerTaskType.AccountDatabaseMigrationJob.ToString())
                       ?? throw new InvalidOperationException("Таска не найдена по имени");
            return task.ID;
        }

        /// <summary>
        /// Создать параметры задачи
        /// </summary>
        /// <returns>Параметры задачи</returns>
        private string CreateTaskParams(List<Guid> accountDatabaseIds, Guid targetFileStorageId)
        {
            var initiator = accessProvider.GetUser();
            if (initiator == null)
                throw new ArgumentException("Аккаунт инициирующий задачу не найден");

            var paramModel = new DatabasesMigrationParams
            {
                AccountDatabaseIds = accountDatabaseIds,
                ToFileStorageId = targetFileStorageId,
                InitiatorId = initiator.Id,
            };
            var param = new CoreWorkerTaskParamSerializer<DatabasesMigrationParams>().Serialize(paramModel);
            return param;
        }
    }
}
