﻿using Clouds42.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts;

namespace Clouds42.Segment.Internal
{
    /// <summary>
    /// Провайдер для регистрации историй миграции аккаунтов
    /// </summary>
    public class SegmentHistoryProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : ISegmentHistoryProvider
    {
        /// <summary>
        /// Заргегистрировать историю миграции аккаунта.
        /// </summary>        
        public void RegisterMigrationHistory(MigrationAccountHistory history)
        {
            try
            {
                dbLayer.MigrationAccountHistoryRepository.Insert(history);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка записи истории по миграции аккаунта]  с сегмента : '{history.SourceSegmentId}' на сегмент : '{history.TargetSegmentId}'");
            }
        }        

        /// <summary>
        /// Создано новый объект истории миграции
        /// </summary>        
        public MigrationHistory CreateMigrationHistoryObject(Guid accountUserInitiatorId, DateTime dateTimeBeforeMigration, bool res)
        {
            return new MigrationHistory
            {
                HistoryId = Guid.NewGuid(),
                InitiatorId = accountUserInitiatorId,
                StartDateTime = dateTimeBeforeMigration,
                FinishDateTime = DateTime.Now,
                Status = res ? MigrationStatusEnum.Success : MigrationStatusEnum.Error
            };
        }

        /// <summary>
        /// Можно ли повторить поптыку миграции аккаунта.
        /// </summary>       
        public bool CanTryMigrateAccountAgain(Guid accountId, Guid sourceSegmentId, Guid targetSegmentId)
        {
            var countMigrationOver24Hourse = GetTriesAccountMigrationCountForPeriod(DateTime.Now.AddDays(-1),
                DateTime.Now, accountId, sourceSegmentId, targetSegmentId);

            var retryMigratesCount = ConfigurationHelper.GetConfigurationValue<int>("RetryMigratesCount");
            return retryMigratesCount > countMigrationOver24Hourse;
        }

        /// <summary>
        /// Получить кол-во миграция аккаунта за определенный промежуток.
        /// </summary>    
        public int GetTriesAccountMigrationCountForPeriod(DateTime periodFrom, DateTime periodTo, Guid accountId,
            Guid sourceSegmentId, Guid targetSegmentId)
        {
            return dbLayer.MigrationAccountHistoryRepository.WhereLazy(h =>
                h.AccountId == accountId && h.SourceSegmentId == sourceSegmentId &&
                h.TargetSegmentId == targetSegmentId && h.MigrationHistory.StartDateTime >= periodFrom &&
                h.MigrationHistory.StartDateTime <= periodTo).Count();
        }


        /// <summary>
        /// Заргегистрировать историю миграции инфомационной базы.
        /// </summary>        
        public void RegisterMigrationAccountDatabaseHistory(MigrationAccountDatabaseHistory history)
        {
            try
            {
                dbLayer.MigrationAccountDatabaseHistoryRepository.Insert(history);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка записи истории по миграции базы] с хранилища : '{history.SourceFileStorageId}' на сегмент : '{history.TargetFileStorageId}'");
            }
        }

        /// <summary>
        /// Можно ли повторить поптыку миграции информационной базы.
        /// </summary>       
        public bool CanTryMigrateAccountDatabaseAgain(Guid accountDatabaseId, Guid? sourceFileStorageId, Guid targetFileStorageId)
        {
            var countMigrationOver24Hourse = GetTriesAccountDatabaseMigrationCountForPeriod(DateTime.Now.AddDays(-1),
                DateTime.Now, accountDatabaseId, sourceFileStorageId, targetFileStorageId);

            var retryMigratesCount = ConfigurationHelper.GetConfigurationValue<int>("RetryMigratesCount");
            return retryMigratesCount > countMigrationOver24Hourse;
        }

        /// <summary>
        /// Получить кол-во миграций инфо баз за определенный промежуток.
        /// </summary>    
        public int GetTriesAccountDatabaseMigrationCountForPeriod(DateTime periodFrom, DateTime periodTo, Guid accountDatabaseId,
            Guid? sourceFileStorageId, Guid targetFileStorageId)
        {
            return dbLayer.MigrationAccountDatabaseHistoryRepository.WhereLazy(h =>
                h.AccountDatabaseId == accountDatabaseId && h.SourceFileStorageId == sourceFileStorageId &&
                h.TargetFileStorageId == targetFileStorageId && h.MigrationHistory.StartDateTime >= periodFrom &&
                h.MigrationHistory.StartDateTime <= periodTo).Count();
        }

    }
}
