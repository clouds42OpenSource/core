﻿using Clouds42.CoreWorkerTask.Helpers;
using Clouds42.DataContracts.Segment;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Segment.Contracts;

namespace Clouds42.Segment.Internal
{
    /// <summary>
    /// Помошник создания задачи на миграцию.
    /// </summary>
    public class CreateMigrationAccountTaskHelper(IRegisterTaskInQueueProvider coreWorkerTasksQueueManager)
        : ICreateMigrationAccountTaskHelper
    {
        /// <summary>
        /// Создать задачу на миграцию.
        /// </summary>
        /// <param name="segmentMigrationParams">Параметры смены сегмента</param>
        public void CreateTask(SegmentMigrationParamsDto segmentMigrationParams)
        {
            CreateTaskAux(segmentMigrationParams);
        }

        private void CreateTaskAux(SegmentMigrationParamsDto segmentMigrationParams, DateTime? dateTimeDelayOperation = null)
        {
            var parameters = new CoreWorkerTaskParamSerializer<SegmentMigrationParamsDto>().Serialize(segmentMigrationParams);

            coreWorkerTasksQueueManager.RegisterTask(
                taskType: CoreWorkerTaskType.SegmentMigrationJob,
                comment: "Миграция сегментов",
                parametrizationModel: new DataContracts.CoreWorker.ParametrizationModelDto(parameters),
                 dateTimeDelayOperation: dateTimeDelayOperation);
        }
    }
}
