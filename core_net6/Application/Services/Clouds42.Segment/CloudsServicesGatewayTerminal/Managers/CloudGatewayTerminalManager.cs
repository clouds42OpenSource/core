﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces;
using Clouds42.Segment.SegmentElementsHelper;

namespace Clouds42.Segment.CloudsServicesGatewayTerminal.Managers
{
    /// <summary>
    ///  Менеджер для работы с терминальными шлюзами
    /// </summary>
    public class CloudGatewayTerminalManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloudGatewayTerminalProvider cloudGatewayTerminalProvider,
        ICreateCloudGatewayTerminalProvider createCloudGatewayTerminalProvider,
        IDeleteCloudGatewayTerminalProvider deleteCloudGatewayTerminalProvider,
        IEditCloudGatewayTerminalProvider editCloudGatewayTerminalProvider,
        IHandlerException handlerException,
        SegmentElementChangesLogEventHelper segmentElementChangesLogEventHelper,
        SegmentElementCreationLogEventHelper segmentElementCreationLogEventHelper,
        SegmentElementDeletionLogEventHelper segmentElementDeletionLogEventHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Создать терминальный шлюз
        /// </summary>
        /// <param name="createCloudGatewayTerminalDto">Модель создания
        /// терминального шлюза</param>
        /// <returns>Id созданного шлюза</returns>
        public ManagerResult<Guid> CreateCloudGatewayTerminal(CreateCloudGatewayTerminalDto createCloudGatewayTerminalDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var gatewayTerminalId = createCloudGatewayTerminalProvider.Create(createCloudGatewayTerminalDto);

                var message = segmentElementCreationLogEventHelper.GetCreationLogEventDescription(createCloudGatewayTerminalDto);
                logger.Trace($"{message} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);
                return Ok(gatewayTerminalId);
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"[Ошибка добавления нового терминального шлюза \"{createCloudGatewayTerminalDto.ConnectionAddress}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать терминальный шлюз
        /// </summary>
        /// <param name="cloudGatewayTerminalDto">Модель терминального шлюза</param>
        public ManagerResult EditCloudGatewayTerminal(CloudGatewayTerminalDto cloudGatewayTerminalDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                var currentGatewayTerminal = cloudGatewayTerminalProvider.GetCloudGatewayTerminalDto(cloudGatewayTerminalDto.Id);
                var logEventDescription =
                    segmentElementChangesLogEventHelper.GetChangesDescription(currentGatewayTerminal, cloudGatewayTerminalDto);

                editCloudGatewayTerminalProvider.Edit(cloudGatewayTerminalDto);

                logger.Trace(
                    $"Успешно отредактирован терминальный шлюз {cloudGatewayTerminalDto.Id} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, logEventDescription);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"]Ошибка редактирования терминального шлюза \"{cloudGatewayTerminalDto.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить терминальный шлюз
        /// </summary>
        /// <param name="cloudGatewayTerminalId">Id терминального шлюза</param>
        public ManagerResult DeleteCloudGatewayTerminal(Guid cloudGatewayTerminalId)
        {
            var getGatewayTerminalResult = GetCloudGatewayTerminalDto(cloudGatewayTerminalId);
            if (getGatewayTerminalResult.Error)
                return PreconditionFailed(getGatewayTerminalResult.Message);

            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                deleteCloudGatewayTerminalProvider.Delete(cloudGatewayTerminalId);

                logger.Trace(
                    $"Успешно удален терминальный шлюз {cloudGatewayTerminalId} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement,
                    segmentElementDeletionLogEventHelper.GetDeletionLogEventDescription(getGatewayTerminalResult.Result));
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления терминального шлюза \"{getGatewayTerminalResult.Result.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель терминального шлюза
        /// </summary>
        /// <param name="cloudGatewayTerminalId">Id терминального шлюза</param>
        /// <returns>Модель терминального шлюза</returns>
        public ManagerResult<CloudGatewayTerminalDto> GetCloudGatewayTerminalDto(Guid cloudGatewayTerminalId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var terminalServer = cloudGatewayTerminalProvider.GetCloudGatewayTerminalDto(cloudGatewayTerminalId);
                return Ok(terminalServer);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения терминального шлюза {cloudGatewayTerminalId}]");
                return PreconditionFailed<CloudGatewayTerminalDto>(ex.Message);
            }
        }
    }
}
