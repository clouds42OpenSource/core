﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces;

namespace Clouds42.Segment.CloudsServicesGatewayTerminal.Providers
{
    /// <summary>
    /// Провайдер для работы с терминальным шлюзом
    /// </summary>
    internal class CloudGatewayTerminalProvider(IUnitOfWork dbLayer) : ICloudGatewayTerminalProvider
    {
        /// <summary>
        /// Получить терминальный шлюз
        /// </summary>
        /// <param name="gatewayTerminalId">Id терминального шлюза</param>
        /// <returns>Терминальный шлюз</returns>
        public CloudServicesGatewayTerminal GetCloudGatewayTerminal(Guid gatewayTerminalId) =>
            dbLayer.CloudServicesGatewayTerminalRepository.FirstOrDefault(gt => gt.ID == gatewayTerminalId) ??
            throw new NotFoundException($"Не удалось получить терминальный шлюз по Id {gatewayTerminalId}");

        /// <summary>
        /// Получить модель терминального шлюза
        /// </summary>
        /// <param name="gatewayTerminalId">Id терминального шлюза</param>
        /// <returns>Модель терминального шлюза</returns>
        public CloudGatewayTerminalDto GetCloudGatewayTerminalDto(Guid gatewayTerminalId)
        {
            var terminalGateway = GetCloudGatewayTerminal(gatewayTerminalId);

            return new CloudGatewayTerminalDto
            {
                Id = terminalGateway.ID,
                Name = terminalGateway.Name,
                Description = terminalGateway.Description,
                ConnectionAddress = terminalGateway.ConnectionAddress
            };
        }
    }
}
