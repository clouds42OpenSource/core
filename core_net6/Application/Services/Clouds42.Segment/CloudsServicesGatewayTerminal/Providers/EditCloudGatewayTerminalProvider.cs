﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces;
using Clouds42.Validators.CloudServer;

namespace Clouds42.Segment.CloudsServicesGatewayTerminal.Providers
{
    /// <summary>
    /// Провайдер для редактирования терминального шлюза
    /// </summary>
    internal class EditCloudGatewayTerminalProvider(
        IUnitOfWork dbLayer,
        ICloudGatewayTerminalProvider cloudGatewayTerminalProvider)
        : IEditCloudGatewayTerminalProvider
    {
        /// <summary>
        /// Редактировать терминальный шлюз
        /// </summary>
        /// <param name="cloudGatewayTerminalDto">Модель терминального шлюза</param>
        public void Edit(CloudGatewayTerminalDto cloudGatewayTerminalDto)
        {
            cloudGatewayTerminalDto.Validate();
            var gatewayTerminal = cloudGatewayTerminalProvider.GetCloudGatewayTerminal(cloudGatewayTerminalDto.Id);
            gatewayTerminal.Name = cloudGatewayTerminalDto.Name;
            gatewayTerminal.Description = cloudGatewayTerminalDto.Description;
            gatewayTerminal.ConnectionAddress = cloudGatewayTerminalDto.ConnectionAddress;

            dbLayer.CloudServicesGatewayTerminalRepository.Update(gatewayTerminal);
            dbLayer.Save();
        }
    }
}
