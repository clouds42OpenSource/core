﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces;
using Clouds42.Validators.CloudServer;

namespace Clouds42.Segment.CloudsServicesGatewayTerminal.Providers
{
    /// <summary>
    /// Провайдер для создания терминального шлюза
    /// </summary>
    internal class CreateCloudGatewayTerminalProvider(IUnitOfWork dbLayer) : ICreateCloudGatewayTerminalProvider
    {
        /// <summary>
        /// Создать терминальный шлюз
        /// </summary>
        /// <param name="createCloudGatewayTerminalDto">Модель создания
        /// терминального шлюза</param>
        /// <returns>Id созданного терминального шлюза</returns>
        public Guid Create(CreateCloudGatewayTerminalDto createCloudGatewayTerminalDto)
        {
            createCloudGatewayTerminalDto.Validate();

            var gatewayTerminal = new CloudServicesGatewayTerminal
            {
                ID = Guid.NewGuid(),
                Name = createCloudGatewayTerminalDto.Name,
                Description = createCloudGatewayTerminalDto.Description,
                ConnectionAddress = createCloudGatewayTerminalDto.ConnectionAddress
            };

            dbLayer.CloudServicesGatewayTerminalRepository.Insert(gatewayTerminal);
            dbLayer.Save();

            return gatewayTerminal.ID;
        }
    }
}
