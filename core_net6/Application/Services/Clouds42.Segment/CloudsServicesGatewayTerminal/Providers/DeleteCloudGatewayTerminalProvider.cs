﻿using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces;

namespace Clouds42.Segment.CloudsServicesGatewayTerminal.Providers
{
    /// <summary>
    /// Провайдер удаления терминального шлюза
    /// </summary>
    internal class DeleteCloudGatewayTerminalProvider(
        IUnitOfWork dbLayer,
        ICloudGatewayTerminalProvider cloudGatewayTerminalProvider)
        : IDeleteCloudGatewayTerminalProvider
    {
        /// <summary>
        /// Удалить терминальный шлюз
        /// </summary>
        /// <param name="gatewayTerminalId">Id терминального шлюза</param>
        public void Delete(Guid gatewayTerminalId)
        {
            var cloudGatewayTerminal = cloudGatewayTerminalProvider.GetCloudGatewayTerminal(gatewayTerminalId);

            if (!AbilityDeleteGatewayTerminal(gatewayTerminalId))
                throw new ValidateException(
                    $"Удаление терминального шлюза \"{cloudGatewayTerminal.Name}\" не возможно так он используется в сегменте");

            dbLayer.CloudServicesGatewayTerminalRepository.Delete(cloudGatewayTerminal);
            dbLayer.Save();
        }

        /// <summary>
        /// Возможность удалить терминальный шлюз
        /// </summary>
        /// <param name="gatewayTerminalId">Id терминального шлюза</param>
        private bool AbilityDeleteGatewayTerminal(Guid gatewayTerminalId) =>
            dbLayer.CloudServicesSegmentTerminalServerRepository.FirstOrDefault(sts =>
                sts.TerminalServerId == gatewayTerminalId) == null;
    }
}
