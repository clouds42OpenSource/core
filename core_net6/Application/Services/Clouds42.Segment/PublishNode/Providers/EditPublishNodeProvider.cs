﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PublishNode.Interfaces;
using Clouds42.Segment.PublishNode.Helpers;

namespace Clouds42.Segment.PublishNode.Providers
{
    /// <summary>
    /// Провайдер редактирования нод публикаций
    /// </summary>
    internal class EditPublishNodeProvider(
        PublishNodeDataHelper publishNodeDataHelper,
        IUnitOfWork dbLayer)
        : IEditPublishNodeProvider
    {
        /// <summary>
        /// Изменить ноду публикаций
        /// </summary>
        /// <param name="publishNodeDto">Обновленная нода публикаций.</param>
        public void Edit(PublishNodeDto publishNodeDto)
        {
            var publishNode = publishNodeDataHelper.GetPublishNodeOrThrowException(publishNodeDto.Id);

            publishNode.Description = publishNodeDto.Description;
            publishNode.Address = publishNodeDto.Address;
            dbLayer.PublishNodeReferenceRepository.Update(publishNode);
            dbLayer.Save();
        }
    }
}
