﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PublishNode.Interfaces;

namespace Clouds42.Segment.PublishNode.Providers
{
    /// <summary>
    /// Провайдер создания нод публикаций
    /// </summary>
    internal class CreatePublishNodeProvider(IUnitOfWork dbLayer) : ICreatePublishNodeProvider
    {
        /// <summary>
        /// Добавить новую ноду публикаций
        /// </summary>
        /// <param name="publishNodeDto">Нода публкаций</param>
        public Guid Create(PublishNodeDto publishNodeDto)
        {
            var publishNode = new PublishNodeReference
            {
                Address = publishNodeDto.Address,
                Description = publishNodeDto.Description,
                ID = Guid.NewGuid()
            };

            dbLayer.PublishNodeReferenceRepository.Insert(publishNode);
            dbLayer.Save();

            return publishNode.ID;
        }
    }
}
