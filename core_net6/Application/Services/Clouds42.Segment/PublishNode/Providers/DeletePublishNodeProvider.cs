﻿using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PublishNode.Interfaces;
using Clouds42.Segment.PublishNode.Helpers;

namespace Clouds42.Segment.PublishNode.Providers
{
    /// <summary>
    /// Провайдер удаления нод публикаций
    /// </summary>
    internal class DeletePublishNodeProvider(
        PublishNodeDataHelper publishNodeDataHelper,
        IUnitOfWork dbLayer)
        : IDeletePublishNodeProvider
    {
        /// <summary>
        /// Удалить ноду публикаций
        /// </summary>
        /// <param name="publishNodeId">ID ноды публикаций</param>
        public void Delete(Guid publishNodeId)
        {
            var publishNode = publishNodeDataHelper.GetPublishNodeOrThrowException(publishNodeId);

            dbLayer.PublishNodeReferenceRepository.Delete(publishNode);
            dbLayer.Save();
        }
    }
}
