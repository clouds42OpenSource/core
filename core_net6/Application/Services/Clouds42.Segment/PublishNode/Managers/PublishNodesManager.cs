﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.PublishNode.Interfaces;
using Clouds42.Segment.PublishNode.Validators;

namespace Clouds42.Segment.PublishNode.Managers
{
    /// <summary>
    /// Менеджер для работы с нодами публикаций
    /// </summary>
    public class PublishNodesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IDeletePublishNodeProvider deletePublishNodeProvider,
        IEditPublishNodeProvider editPublishNodeProvider,
        ICreatePublishNodeProvider createPublishNodeProvider,
        PublishNodeValidator publishNodeValidator,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Добавить новую ноду публикаций
        /// </summary>
        /// <param name="publishNode">Нода публкаций</param>
        /// <returns>Результат добавления</returns>
        public ManagerResult<Guid> AddNewPublishNode(PublishNodeDto publishNode)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewPublishNodeReference,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = publishNodeValidator.ValidatePublishNodeDto(publishNode);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError<Guid>(validationMessage);

                var publishNodeId = createPublishNodeProvider.Create(publishNode);

                var message = $"Создана нода публикаций. Адрес: \"{publishNode.Address}\"";
                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);
                return Ok(publishNodeId);
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка добавления ноды публикаций \"{publishNode.Address}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Удалить ноду публикаций
        /// </summary>
        /// <param name="publishNodeId">ID ноды публикаций</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult DeletePublishNode(Guid publishNodeId)
        {
            var publishNodeName = string.Empty;
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeletePublishNodeReference,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = publishNodeValidator.ValidatePublishNodeBeforeDelete(publishNodeId);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError(validationMessage);

                var publishedNode = DbLayer.PublishNodeReferenceRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefault(x => x.ID == publishNodeId);

                publishNodeName = publishedNode?.Address;

                deletePublishNodeProvider.Delete(publishNodeId);

                var message = $"Удалена нода публикаций. Адрес: \"{publishNodeName}\"";
                logger.Trace(message);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, message);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления ноды публикаций \"{publishNodeName}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Изменить ноду публикаций
        /// </summary>
        /// <param name="publishNode">Обновленная нода публикаций.</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult EditPublishNode(PublishNodeDto publishNode)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdatePublishNodeReference,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = publishNodeValidator.ValidatePublishNodeDto(publishNode);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError(validationMessage);

                editPublishNodeProvider.Edit(publishNode);
                logger.Trace($"Успешно обновлена нода публикаций {publishNode.Id} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement,
                    $"Нода публикаций отредактирована. Адрес: \"{publishNode.Address}\"");
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка редактирования ноды публикаций \"{publishNode.Address}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
