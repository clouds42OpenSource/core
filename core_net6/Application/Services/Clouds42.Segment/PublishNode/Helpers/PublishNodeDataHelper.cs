﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Segment.PublishNode.Helpers
{
    /// <summary>
    /// Хэлпер данных нод публикаций
    /// </summary>
    public class PublishNodeDataHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить ноду публикаций
        /// </summary>
        /// <param name="publishNodeId">ID ноды публикаций</param>
        /// <returns>Нода публикаций</returns>
        public PublishNodeReference GetPublishNodeOrThrowException(Guid publishNodeId)
            => dbLayer.PublishNodeReferenceRepository.FirstOrDefault(w => w.ID == publishNodeId)
               ?? throw new NotFoundException("Нода публикации по ID не найдена");

        /// <summary>
        /// Признак что такой адрес ноды уже существует
        /// </summary>
        /// <param name="publishNode">Нода публикаций</param>
        /// <returns>Признак что такой адрес ноды уже существует</returns>
        public bool ConnectionAddressAlreadyExist(PublishNodeDto publishNode)
            => dbLayer.PublishNodeReferenceRepository.FirstOrDefault(node =>
                   node.Address.ToLower() == publishNode.Address.ToLower() &&
                   node.ID != publishNode.Id) !=
               null;

        /// <summary>
        /// Признак что такое описание ноды уже существует
        /// </summary>
        /// <param name="publishNode">Нода публикаций</param>
        /// <returns>Признак что такое описание ноды уже существует</returns>
        public bool DescriptionAlreadyExist(PublishNodeDto publishNode)
            => dbLayer.PublishNodeReferenceRepository.FirstOrDefault(node =>
                   node.Description.ToLower() == publishNode.Description.ToLower() &&
                   node.ID != publishNode.Id) !=
               null;

        /// <summary>
        /// Признак что нода публикации используется
        /// </summary>
        /// <param name="publishNodeId">ID ноды публикаций</param>
        /// <returns>Признак что нода публикации используется</returns>
        public bool PublishNodeUsed(Guid publishNodeId)
        {
            var publishNode = GetPublishNodeOrThrowException(publishNodeId);

            var cloudServicesContentServerNode =
                dbLayer.CloudServicesContentServerNodeRepository.FirstOrDefault(w =>
                    w.NodeReferenceId == publishNode.ID);

            return cloudServicesContentServerNode != null;
        }
    }
}
