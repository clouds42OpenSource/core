﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Segment.PublishNode.Helpers;

namespace Clouds42.Segment.PublishNode.Validators
{
    /// <summary>
    /// Валидатор для нод публикаций
    /// </summary>
    public class PublishNodeValidator(PublishNodeDataHelper publishNodeDataHelper)
    {
        /// <summary>
        /// Проверить ноду публикации перед удалением
        /// </summary>
        /// <param name="publishNodeId">ID ноды публикации</param>
        /// <returns>Результат валидации</returns>
        public string ValidatePublishNodeBeforeDelete(Guid publishNodeId)
            => publishNodeDataHelper.PublishNodeUsed(publishNodeId)
                ? "Удаление невозможно, так как эта нода публикаций используется в сегменте"
                : string.Empty;

        /// <summary>
        /// Проверить модель ноды публикации
        /// </summary>
        /// <param name="publishNodeDto">Обновленная нода публикаций</param>
        /// <returns>Результат валидации</returns>
        public string ValidatePublishNodeDto(PublishNodeDto publishNodeDto)
        {
            if (string.IsNullOrEmpty(publishNodeDto.Address))
                return "Поле Адрес обязательное для заполнения";

            if (publishNodeDataHelper.ConnectionAddressAlreadyExist(publishNodeDto))
                return "Нода публикации с таким адресом подключения уже существует";

            return publishNodeDataHelper.DescriptionAlreadyExist(publishNodeDto) 
                ? "Нода публикации с таким описанием уже существует" 
                : string.Empty;
        }
    }
}
