﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces;

namespace Clouds42.Segment.CloudServicesBackupStorage.Providers
{
    /// <summary>
    /// Провайдер для работы с хранилищем бэкапов
    /// </summary>
    internal class CloudBackupStorageProvider(IUnitOfWork dbLayer) : ICloudBackupStorageProvider
    {
        /// <summary>
        /// Получить хранилище бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        /// <returns>Хранилище бэкапов</returns>
        public Domain.DataModels.CloudServicesBackupStorage GetCloudBackupStorage(Guid backupStorageId) =>
            dbLayer.CloudServicesBackupStorageRepository.FirstOrDefault(bs => bs.ID == backupStorageId) ??
            throw new NotFoundException($"Не удалось получить хранилище бэкапов по Id= '{backupStorageId}'");

        /// <summary>
        /// Получить модель хранилища бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        /// <returns>Модель хранилища бэкапов</returns>
        public CloudBackupStorageDto GetCloudBackupStorageDto(Guid backupStorageId)
        {
            var backupStorage = GetCloudBackupStorage(backupStorageId);
            
            return new CloudBackupStorageDto
            {
                Id = backupStorage.ID,
                Name = backupStorage.Name,
                Description = backupStorage.Description,
                ConnectionAddress = backupStorage.ConnectionAddress,
                PhysicalPath = backupStorage.PhysicalPath
            };
        }
    }
}
