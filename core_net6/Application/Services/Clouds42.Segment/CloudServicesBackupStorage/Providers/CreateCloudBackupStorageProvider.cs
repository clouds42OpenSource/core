﻿using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces;
using Clouds42.Validators.CloudServer;

namespace Clouds42.Segment.CloudServicesBackupStorage.Providers
{
    /// <summary>
    /// Провайдер для создания хранилища бэкапов
    /// </summary>
    internal class CreateCloudBackupStorageProvider(IUnitOfWork dbLayer) : ICreateCloudBackupStorageProvider
    {
        /// <summary>
        /// Создать хранилище бэкапов
        /// </summary>
        /// <param name="createCloudBackupStorageDto">Модель создания
        /// хранилища бэкапов</param>
        /// <returns>Id созданного хранилища бэкапов</returns>
        public Guid Create(CreateCloudBackupStorageDto createCloudBackupStorageDto)
        {
            createCloudBackupStorageDto.Validate();

            var backupStorage = new Domain.DataModels.CloudServicesBackupStorage
            {
                ID = Guid.NewGuid(),
                Name = createCloudBackupStorageDto.Name,
                Description = createCloudBackupStorageDto.Description,
                ConnectionAddress = createCloudBackupStorageDto.ConnectionAddress,
                PhysicalPath = createCloudBackupStorageDto.PhysicalPath
            };

            dbLayer.CloudServicesBackupStorageRepository.Insert(backupStorage);
            dbLayer.Save();

            return backupStorage.ID;
        }
    }
}
