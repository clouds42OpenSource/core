﻿using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces;

namespace Clouds42.Segment.CloudServicesBackupStorage.Providers
{
    /// <summary>
    /// Провайдер удаления хранилища бэкапов
    /// </summary>
    internal class DeleteCloudBackupStorageProvider(
        IUnitOfWork dbLayer,
        ICloudBackupStorageProvider cloudBackupStorageProvider)
        : IDeleteCloudBackupStorageProvider
    {
        /// <summary>
        /// Удалить хранилище бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        public void Delete(Guid backupStorageId)
        {
            var backupStorage = cloudBackupStorageProvider.GetCloudBackupStorage(backupStorageId);

            if (!AbilityDeleteBackupStorage(backupStorageId))
                throw new ValidateException(
                    $"Удаление хранилища бэкапов \"{backupStorage.Name}\" не возможно так он используется в сегменте");

            dbLayer.CloudServicesBackupStorageRepository.Delete(backupStorage);
            dbLayer.Save();
        }

        /// <summary>
        /// Возможность удалить хранилище бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        private bool AbilityDeleteBackupStorage(Guid backupStorageId) =>
            dbLayer.CloudServicesSegmentRepository.FirstOrDefault(sts =>
                sts.BackupStorageID == backupStorageId) == null;
    }
}
