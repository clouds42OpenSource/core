﻿using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces;
using Clouds42.Validators.CloudServer;

namespace Clouds42.Segment.CloudServicesBackupStorage.Providers
{
    /// <summary>
    /// Провайдер для редактирования хранилища бэкапов
    /// </summary>
    internal class EditCloudBackupStorageProvider(
        IUnitOfWork dbLayer,
        ICloudBackupStorageProvider cloudBackupStorageProvider)
        : IEditCloudBackupStorageProvider
    {
        /// <summary>
        /// Редактировать хранилище бэкапов
        /// </summary>
        /// <param name="cloudBackupStorageDto">Модель хранилища бэкапов</param>
        public void Edit(CloudBackupStorageDto cloudBackupStorageDto)
        {
            cloudBackupStorageDto.Validate();
            var backupStorage = cloudBackupStorageProvider.GetCloudBackupStorage(cloudBackupStorageDto.Id);
            backupStorage.Name = cloudBackupStorageDto.Name;
            backupStorage.Description = cloudBackupStorageDto.Description;
            backupStorage.ConnectionAddress = cloudBackupStorageDto.ConnectionAddress;

            dbLayer.CloudServicesBackupStorageRepository.Update(backupStorage);
            dbLayer.Save();
        }
    }
}
