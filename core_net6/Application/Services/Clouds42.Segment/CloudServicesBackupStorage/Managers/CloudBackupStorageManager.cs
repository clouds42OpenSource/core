﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces;
using Clouds42.Segment.SegmentElementsHelper;

namespace Clouds42.Segment.CloudServicesBackupStorage.Managers
{
    /// <summary>
    /// Менеджер для работы с хранилищем бэкапов
    /// </summary>
    public class CloudBackupStorageManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloudBackupStorageProvider cloudBackupStorageProvider,
        ICreateCloudBackupStorageProvider createCloudBackupStorageProvider,
        IEditCloudBackupStorageProvider editCloudBackupStorageProvider,
        IDeleteCloudBackupStorageProvider deleteCloudBackupStorageProvider,
        IHandlerException handlerException,
        SegmentElementChangesLogEventHelper segmentElementChangesLogEventHelper,
        SegmentElementCreationLogEventHelper segmentElementCreationLogEventHelper,
        SegmentElementDeletionLogEventHelper segmentElementDeletionLogEventHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Создать хранилище бэкапов
        /// </summary>
        /// <param name="createCloudBackupStorageDto">Модель создания
        /// хранилища бэкапов</param>
        /// <returns>Id созданного хранилища бэкапов</returns>
        public ManagerResult<Guid> CreateCloudBackupStorage(CreateCloudBackupStorageDto createCloudBackupStorageDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var backupStorageId = createCloudBackupStorageProvider.Create(createCloudBackupStorageDto);

                var message = segmentElementCreationLogEventHelper.GetCreationLogEventDescription(createCloudBackupStorageDto);
                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);

                return Ok(backupStorageId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка добавления нового хранилища бэкапов \"{createCloudBackupStorageDto.Name}\"]");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement,
                    $"Ошибка добавления нового хранилища бэкапов \"{createCloudBackupStorageDto.Name}\". Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать хранилище бэкапов
        /// </summary>
        /// <param name="cloudBackupStorageDto">Модель хранилища бэкапов</param>
        public ManagerResult EditCloudBackupStorage(CloudBackupStorageDto cloudBackupStorageDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                var currentBackupStorage = cloudBackupStorageProvider.GetCloudBackupStorageDto(cloudBackupStorageDto.Id);
                var logEventDescription =
                    segmentElementChangesLogEventHelper.GetChangesDescription(currentBackupStorage, cloudBackupStorageDto);

                editCloudBackupStorageProvider.Edit(cloudBackupStorageDto);
                
                logger.Trace(logEventDescription + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, logEventDescription);

                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка редактирования хранилища бэкапов \"{cloudBackupStorageDto.Name}\"]");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement,
                    $"Ошибка редактирования хранилища бэкапов \"{cloudBackupStorageDto.Name}\". Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить хранилище бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        public ManagerResult DeleteCloudBackupStorage(Guid backupStorageId)
        {
            var getBackupStorageResult = GetCloudBackupStorageDto(backupStorageId);
            if (getBackupStorageResult.Error)
                return PreconditionFailed(getBackupStorageResult.Message);

            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                deleteCloudBackupStorageProvider.Delete(backupStorageId);

                var message = segmentElementDeletionLogEventHelper.GetDeletionLogEventDescription(getBackupStorageResult.Result);
                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления хранилища бэкапов \"{getBackupStorageResult.Result.Name}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель хранилища бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        /// <returns>Модель хранилища бэкапов</returns>
        public ManagerResult<CloudBackupStorageDto> GetCloudBackupStorageDto(Guid backupStorageId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var cloudBackupStorage = cloudBackupStorageProvider.GetCloudBackupStorageDto(backupStorageId);
                return Ok(cloudBackupStorage);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения хранилища бэкапов {backupStorageId}]");
                return PreconditionFailed<CloudBackupStorageDto>(ex.Message);
            }
        }
    }
}
