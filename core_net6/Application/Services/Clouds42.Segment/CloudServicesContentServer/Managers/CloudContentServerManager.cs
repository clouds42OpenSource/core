﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudServicesContentServer.Validators;
using Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces;

namespace Clouds42.Segment.CloudServicesContentServer.Managers
{
    /// <summary>
    /// Менеджер для работы с сервером публикации
    /// </summary>
    public class CloudContentServerManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateCloudContentServerProvider createCloudContentServerProvider,
        IEditCloudContentServerProvider editCloudContentServerProvider,
        IDeleteCloudContentServerProvider deleteCloudContentServerProvider,
        ICloudContentServerProvider cloudContentServerProvider,
        CloudContentServerValidator cloudContentServerValidator,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить модели доступных нод публикации
        /// </summary>
        /// <returns>Доступные ноды публикации</returns>
        public ManagerResult<List<PublishNodeDto>> GetAvailablePublishNodes()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var result = cloudContentServerProvider.GetAvailablePublishNodesDto();
                return Ok(result);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка доступных нод публикации]");
                return PreconditionFailed<List<PublishNodeDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Создать сервер публикации
        /// </summary>
        /// <param name="createCloudContentServerDto">Модель создания
        /// сервера публикации</param>
        /// <returns>Id созданного сервера публикации</returns>
        public ManagerResult<Guid> CreateCloudContentServer(CreateCloudContentServerDto createCloudContentServerDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                if (!cloudContentServerValidator.ValidateCreateCloudContentServerDto(createCloudContentServerDto,
                    out var errorMessage))
                    return ValidationError<Guid>(errorMessage);

                var contentServerId = createCloudContentServerProvider.Create(createCloudContentServerDto);

                var message =
                    $"Создан сервер публикаций. Имя: \"{createCloudContentServerDto.Name}\", Адрес: \"{createCloudContentServerDto.PublishSiteName}\"";

                logger.Trace(message + $" пользователем {AccessProvider.Name}");

                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);

                return Ok(contentServerId);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Ошибка добавления нового сервера публикации \"{createCloudContentServerDto.Name}\".]";
                handlerException.Handle(ex, message);

                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement,
                    $"{message} Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать сервер публикации
        /// </summary>
        /// <param name="cloudContentServerDto">Модель сервера публикации</param>
        public ManagerResult EditCloudContentServer(CloudContentServerDto cloudContentServerDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                if (!cloudContentServerValidator.ValidateCloudContentServerDto(cloudContentServerDto,
                    out var message))
                    return ValidationError<Guid>(message);

                editCloudContentServerProvider.Edit(cloudContentServerDto);

                var logMessage =
                    $"Сервер публикации отредактирован. Имя: \"{cloudContentServerDto.Name}\", Адрес: \"{cloudContentServerDto.PublishSiteName}\" пользователем {AccessProvider.Name}";

                logger.Trace(logMessage);

                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, logMessage);

                return Ok();
            }
            catch (Exception ex)
            {

                var message = $"[Ошибка редактирования сервера публикаций \"{cloudContentServerDto.Name}\"]";
                handlerException.Handle(ex,message);

                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement,
                    $"message: {ex.Message}");

                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить сервер публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        public ManagerResult DeleteCloudContentServer(Guid contentServerId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                if (!cloudContentServerValidator.AbilityDeleteContentServer(contentServerId))
                    return ValidationError<Guid>("Не возможно удалить сервер, так как он используется в сегменте");

                var contentServer = cloudContentServerProvider.GetCloudContentServer(contentServerId);

                deleteCloudContentServerProvider.Delete(contentServerId);

                var message =
                    $"Удален сервер публикаций. Имя: \"{contentServer.Name}\", Адрес: \"{contentServer.PublishSiteName}\"";
                
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, message);

                logger.Trace(message);

                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка удаления сервера публикации {contentServerId}]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель сервера публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <returns>Модель сервера публикации</returns>
        public ManagerResult<CloudContentServerDto> GetCloudContentServerDto(Guid contentServerId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var cloudContentServer = cloudContentServerProvider.GetCloudContentServerDto(contentServerId);
                return Ok(cloudContentServer);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения сервера публикации {contentServerId}]");
                return PreconditionFailed<CloudContentServerDto>(ex.Message);
            }
        }
    }
}
