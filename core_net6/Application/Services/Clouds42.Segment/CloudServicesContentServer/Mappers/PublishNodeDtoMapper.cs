﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Domain.DataModels;

namespace Clouds42.Segment.CloudServicesContentServer.Mappers
{
    /// <summary>
    /// Маппер модели ноды публикации
    /// </summary>
    public static class PublishNodeDtoMapper
    {
        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="publishNodeReference">Нода публикации</param>
        /// <returns>Модель ноды публикации</returns>
        public static PublishNodeDto MapToPublishNodeDto(this PublishNodeReference publishNodeReference) => 
            new()
            {
            Id = publishNodeReference.ID,
            Description = publishNodeReference.Description,
            Address = publishNodeReference.Address
        };
    }
}
