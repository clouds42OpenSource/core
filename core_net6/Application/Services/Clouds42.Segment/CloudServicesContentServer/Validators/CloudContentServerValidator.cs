﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Segment.CloudServicesContentServer.Validators
{
    /// <summary>
    /// Валидатор сервера публикации
    /// </summary>
    public class CloudContentServerValidator(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Возможность удалить сервер публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        public bool AbilityDeleteContentServer(Guid contentServerId) =>
            dbLayer.CloudServicesSegmentRepository.FirstOrDefault(sts =>
                sts.ContentServerID == contentServerId) == null;


        /// <summary>
        /// Выполнить валидацию модели создания сервера публикации
        /// </summary>
        /// <param name="createCloudContentServerDto">Модель создания сервера публикации</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public bool ValidateCreateCloudContentServerDto(CreateCloudContentServerDto createCloudContentServerDto, out string message)
        {
            if (!ValidateBaseCloudContentServer(createCloudContentServerDto, out message))
                return false;

            if (!createCloudContentServerDto.PublishNodes.Any())
                return true;

            if (!CheckThatPublicationNodesAreNotUsed(createCloudContentServerDto.PublishNodes.Select(pn => pn.Id)
                .ToList())) 
            {
                message = "Создать сервер публикации с такими нодами не возможно. Одна из нод используется другим сервером";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Выполнить валидацию модели создания сервера публикации
        /// </summary>
        /// <param name="cloudContentServerDto">Модель сервера публикации</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public bool ValidateCloudContentServerDto(CloudContentServerDto cloudContentServerDto, out string message)
        {
            if (!ValidateBaseCloudContentServer(cloudContentServerDto, out message))
                return false;

            if (GetCloudContentServerNodesCount(cloudContentServerDto.Id) > 0 &&
                !cloudContentServerDto.PublishNodes.Any())
            {
                message = $"Не возможно удалить единственную ноду сервера публикации '{cloudContentServerDto.Id}'";
                return false;
            }

            if (!CheckThatPublicationNodesAreNotUsed(cloudContentServerDto.Id,
                cloudContentServerDto.PublishNodes.Select(pn => pn.Id).ToList())) 
            {
                message = $"Изменить список нод для сервера {cloudContentServerDto.Id} не возможно. Одна из нод используется другим сервером";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Получить колическтво используемых нод сервером публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <returns>Количество нод</returns>
        private int GetCloudContentServerNodesCount(Guid contentServerId) => dbLayer
            .CloudServicesContentServerNodeRepository.Where(sn => sn.ContentServerId == contentServerId).Count();

        /// <summary>
        /// Проверить что ноды побликации не используются
        /// </summary>
        /// <param name="publishNodes">Ноды публикации</param>
        /// <returns>Признак что ноды не используются</returns>
        private bool CheckThatPublicationNodesAreNotUsed(List<Guid> publishNodes) => !dbLayer
            .CloudServicesContentServerNodeRepository.Where(sn => publishNodes.Any(pn => pn == sn.NodeReferenceId))
            .Any();

        /// <summary>
        /// Проверить что ноды побликации не используются
        /// для других сервером публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <param name="publishNodes">Ноды публикации</param>
        /// <returns>Признак что ноды не используются</returns>
        private bool CheckThatPublicationNodesAreNotUsed(Guid contentServerId, List<Guid> publishNodes) =>
            !dbLayer.CloudServicesContentServerNodeRepository.Where(sn =>
                publishNodes.Any(pn => pn == sn.NodeReferenceId) && sn.ContentServerId != contentServerId).Any();

        /// <summary>
        /// Выполнить базовую валидацию модели сервера публикации
        /// </summary>
        /// <param name="createCloudContentServerDto">Модель создания сервера публикации</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <returns></returns>
        private bool ValidateBaseCloudContentServer(CreateCloudContentServerDto createCloudContentServerDto,
            out string message)
        {
            message = null;

            if (createCloudContentServerDto.Name.IsNullOrEmpty())
            {
                message = @"Поле ""Наименование"" обязательное для заполнения";
                return false;
            }

            if (createCloudContentServerDto.PublishSiteName.IsNullOrEmpty())
            {
                message = @"Поле ""Сайт публикации"" обязательное для заполнения";
                return false;
            }

            return true;
        }
    }
}
