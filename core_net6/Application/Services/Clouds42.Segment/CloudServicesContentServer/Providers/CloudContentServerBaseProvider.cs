﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces;

namespace Clouds42.Segment.CloudServicesContentServer.Providers
{
    /// <summary>
    /// Базовый провайдер для работы с сервером публикации
    /// </summary>
    public abstract class CloudContentServerBaseProvider(
        IUnitOfWork dbLayer,
        ICloudContentServerProvider cloudContentServerProvider)
    {
        protected readonly IUnitOfWork DbLayer = dbLayer;
        protected readonly ICloudContentServerProvider CloudContentServerProvider = cloudContentServerProvider;

        /// <summary>
        /// Создать связь нод публикации с сервером публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <param name="publishNodes">Ноды публикации</param>
        protected void CreateCloudContentServerNodes(Guid contentServerId, List<PublishNodeDto> publishNodes)
        {
            if (!publishNodes.Any())
                return;

            publishNodes.ForEach(pn =>
            {
                var cloudContentServerNode = new CloudServicesContentServerNode
                {
                    ID = Guid.NewGuid(),
                    ContentServerId = contentServerId,
                    NodeReferenceId = pn.Id
                };

                DbLayer.CloudServicesContentServerNodeRepository.Insert(cloudContentServerNode);
            });

            DbLayer.Save();
        }

        /// <summary>
        /// Удалить связь нод публикации с сервером публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        protected void DeleteCloudContentServerNodes(Guid contentServerId)
        {
            var contentServerNodes = CloudContentServerProvider.GetCloudServicesContentServerNodes(contentServerId);
            DbLayer.CloudServicesContentServerNodeRepository.DeleteRange(contentServerNodes);
            DbLayer.Save();
        }
    }
}
