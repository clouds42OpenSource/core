﻿using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces;

namespace Clouds42.Segment.CloudServicesContentServer.Providers
{
    /// <summary>
    /// Провайдер для работы с данными сервера публикации
    /// </summary>
    internal class CloudContentServerDataProvider(IUnitOfWork dbLayer) : ICloudContentServerDataProvider
    {
        /// <summary>
        /// Получить адреса нод сервера публикации
        /// </summary>
        /// <param name="contentServerId">ID сервера публикаций</param>
        /// <returns>Адреса нод сервера публикации</returns>
        public IEnumerable<string> GetContentServerPublishNodeAddresses(Guid contentServerId) =>
            from contentServerNode in dbLayer.CloudServicesContentServerNodeRepository.WhereLazy()
            join publishNode in dbLayer.PublishNodeReferenceRepository.WhereLazy() on contentServerNode
                .NodeReferenceId equals publishNode.ID
            where contentServerNode.ContentServerId == contentServerId
            select publishNode.Address;
    }
}
