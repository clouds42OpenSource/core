﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces;

namespace Clouds42.Segment.CloudServicesContentServer.Providers
{
    /// <summary>
    /// Провайдер для создания сервера публикации
    /// </summary>
    internal class CreateCloudContentServerProvider(
        IUnitOfWork dbLayer,
        ICloudContentServerProvider cloudContentServerProvider,
        IHandlerException handlerException)
        : CloudContentServerBaseProvider(dbLayer, cloudContentServerProvider), ICreateCloudContentServerProvider
    {
        /// <summary>
        /// Создать сервер публикации
        /// </summary>
        /// <param name="createCloudContentServerDto">Модель создания
        /// сервера публикации</param>
        /// <returns>Id созданного сервера публикации</returns>
        public Guid Create(CreateCloudContentServerDto createCloudContentServerDto)
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var contentServer = new Domain.DataModels.CloudServicesContentServer
                {
                    ID = Guid.NewGuid(),
                    Name = createCloudContentServerDto.Name,
                    Description = createCloudContentServerDto.Description,
                    PublishSiteName = createCloudContentServerDto.PublishSiteName,
                    GroupByAccount = createCloudContentServerDto.GroupByAccount,
                };

                DbLayer.CloudServicesContentServerRepository.Insert(contentServer);
                DbLayer.Save();
                CreateCloudContentServerNodes(contentServer.ID, createCloudContentServerDto.PublishNodes);

                dbScope.Commit();

                return contentServer.ID;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания сервера публикации] {createCloudContentServerDto.Name}");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
