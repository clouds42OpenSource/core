﻿using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces;

namespace Clouds42.Segment.CloudServicesContentServer.Providers
{
    /// <summary>
    /// Провайдер удаления сервера публикации
    /// </summary>
    internal class DeleteCloudContentServerProvider(
        IUnitOfWork dbLayer,
        ICloudContentServerProvider cloudContentServerProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : CloudContentServerBaseProvider(dbLayer, cloudContentServerProvider), IDeleteCloudContentServerProvider
    {
        /// <summary>
        /// Удалить сервер публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        public void Delete(Guid contentServerId)
        {
            var contentServer = CloudContentServerProvider.GetCloudContentServer(contentServerId);

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                logger.Info($"Удаление сервера публикации {contentServerId}");
                DeleteCloudContentServerNodes(contentServerId);
                DbLayer.CloudServicesContentServerRepository.Delete(contentServer);
                DbLayer.Save();
                dbScope.Commit();
                logger.Info($"Удаление сервера публикации {contentServerId} завершилось успешно.");

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления сервера публикации] {contentServerId}");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
