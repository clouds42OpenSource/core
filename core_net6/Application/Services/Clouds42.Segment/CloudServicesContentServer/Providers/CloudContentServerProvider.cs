﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudServicesContentServer.Mappers;
using Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces;

namespace Clouds42.Segment.CloudServicesContentServer.Providers
{
    /// <summary>
    /// Провайдер для работы с сервером публикации
    /// </summary>
    internal class CloudContentServerProvider(IUnitOfWork dbLayer) : ICloudContentServerProvider
    {
        /// <summary>
        /// Получить сервер публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <returns>Сервер публикации</returns>
        public Domain.DataModels.CloudServicesContentServer GetCloudContentServer(Guid contentServerId) =>
            dbLayer.CloudServicesContentServerRepository.FirstOrDefault(cs => cs.ID == contentServerId) ??
            throw new NotFoundException($"Не удалось получить сервер публикации {contentServerId}");

        /// <summary>
        /// Получить модель сервера публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <returns>Модель сервера публикации</returns>
        public CloudContentServerDto GetCloudContentServerDto(Guid contentServerId)
        {
            var contentServer = GetCloudContentServer(contentServerId);

            return new CloudContentServerDto
            {
                Id = contentServer.ID,
                Name = contentServer.Name,
                Description = contentServer.Description,
                GroupByAccount = contentServer.GroupByAccount,
                PublishSiteName = contentServer.PublishSiteName,
                PublishNodes = GetCloudServicesContentServerNodes(contentServerId).
                    Select(cs => cs.PublishNodeReference.MapToPublishNodeDto()).ToList(),
                AvailablePublishNodes = GetAvailablePublishNodesDto()
            };
        }

        /// <summary>
        /// Получить список связей нод публикации
        /// с сервером публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <returns>Список связей</returns>
        public List<CloudServicesContentServerNode>
            GetCloudServicesContentServerNodes(Guid contentServerId) => dbLayer
            .CloudServicesContentServerNodeRepository.Where(w => w.ContentServerId == contentServerId).ToList();

        /// <summary>
        /// Получить модели доступных нод публикации
        /// </summary>
        /// <returns></returns>
        public List<PublishNodeDto> GetAvailablePublishNodesDto() =>
            GetAvailablePublishNodes().Select(pn => pn.MapToPublishNodeDto()).ToList();

        /// <summary>
        /// Получить свободные ноды публикации
        /// </summary>
        /// <returns>Свободные ноды публикации</returns>
        public List<PublishNodeReference> GetAvailablePublishNodes() =>
            (from pn in dbLayer.PublishNodeReferenceRepository.WhereLazy()
                join sn in dbLayer.CloudServicesContentServerNodeRepository.WhereLazy() on pn.ID equals
                    sn.NodeReferenceId into csn
                from s in csn.DefaultIfEmpty()
                where s == null
                select pn).ToList();
    }
}
