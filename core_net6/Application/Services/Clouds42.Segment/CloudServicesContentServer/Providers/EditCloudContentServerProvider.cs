﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces;

namespace Clouds42.Segment.CloudServicesContentServer.Providers
{
    /// <summary>
    /// Провайдер для редактирования сервера публикации
    /// </summary>
    internal class EditCloudContentServerProvider(
        IUnitOfWork dbLayer,
        ICloudContentServerProvider cloudContentServerProvider,
        IHandlerException handlerException)
        : CloudContentServerBaseProvider(dbLayer, cloudContentServerProvider), IEditCloudContentServerProvider
    {
        /// <summary>
        /// Редактировать сервер публикации
        /// </summary>
        /// <param name="cloudContentServerDto">Модель сервера публикации</param>
        public void Edit(CloudContentServerDto cloudContentServerDto)
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var contentServer = CloudContentServerProvider.GetCloudContentServer(cloudContentServerDto.Id);
                contentServer.Name = cloudContentServerDto.Name;
                contentServer.Description = cloudContentServerDto.Description;
                contentServer.GroupByAccount = cloudContentServerDto.GroupByAccount;
                contentServer.PublishSiteName = cloudContentServerDto.PublishSiteName;
                DbLayer.CloudServicesContentServerRepository.Update(contentServer);

                DeleteCloudContentServerNodes(contentServer.ID);
                CreateCloudContentServerNodes(contentServer.ID, cloudContentServerDto.PublishNodes);
                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования сервера публикации] {cloudContentServerDto.Id} ");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
