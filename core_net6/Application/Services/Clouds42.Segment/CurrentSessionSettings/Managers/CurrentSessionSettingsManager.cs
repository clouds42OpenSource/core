﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Logging.CurrentSessionSettings;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Segment.CurrentSessionSettings.Managers
{
    /// <summary>
    /// Менеджер по выборке настроек для текущей сессии
    /// </summary>
    public sealed class CurrentSessionSettingsManager : BaseManager
    {
        /// <summary>
        /// Конструктор для класса <see cref="CurrentSessionSettingsManager"/>
        /// </summary>
        /// <param name="accessProvider">Access провайдер</param>
        /// <param name="dbLayer">DB контекст</param>
        /// <param name="handlerException">Обработчик ошибок</param>
        public CurrentSessionSettingsManager(IAccessProvider accessProvider,
            IUnitOfWork dbLayer, IHandlerException handlerException)
            : base(accessProvider, dbLayer, handlerException)
        {
        }

        /// <summary>
        /// Получить настройки текущей сессии
        /// </summary>
        /// <param name="isCurrentContextAccount">Если <code>false</code>, то находится в чужом аккаунте</param>
        /// <returns>Возвращает настройки текущей сессии</returns>
        public async Task<ManagerResult<CurrentSessionSettingsModelDto>> GetCurrentSessionSettings(bool isCurrentContextAccount)
        {
            try
            {
                var currentUser = await AccessProvider.GetUserAsync();
                
                var sessionSettings = new CurrentSessionSettingsModelDto
                {
                    CurrentContextInfo = new CurrentSessionContextInfoDto
                    {
                        IsDifferentContextAccount = !isCurrentContextAccount,
                        ContextAccountIndex = AccessProvider.ContextAccountIndex,
                        ContextAccountName = AccessProvider.ContextAccountName,
                        CurrentUserGroups = currentUser.Groups.ToArray(),
                        IsPhoneVerified = currentUser.IsPhoneVerified,
                        IsEmailVerified = currentUser.IsEmailVerified,
                        IsPhoneExists = !string.IsNullOrEmpty(currentUser.PhoneNumber),
                        IsEmailExists = !string.IsNullOrEmpty(currentUser.Email),
                        UserId = currentUser.Id,
                        AccountId = currentUser.RequestAccountId,
                        ContextAccountId = currentUser.ContextAccountId,
                        IsNew = currentUser.IsNew,
                        Locale = currentUser.Locale,
                        UserSource = currentUser.UserSource,
                        ShowSupportMessage = await ShouldShowAoAndTiiMessage(currentUser.Id)
                    }
                };

                return Ok(sessionSettings);
            }
            catch (Exception ex)
            {
                Logger.Warn(ex, "Ошибка получения настроек для текущего сеанса");

                return PreconditionFailed<CurrentSessionSettingsModelDto>(ex.Message);
            }
            
        }
        private async Task<bool> ShouldShowAoAndTiiMessage(Guid userId)
        {
            var result = await DbLayer.AccountsRepository.AsQueryableNoTracking()
                .Where(a => a.AccountUsers.Any(u => u.Id == userId))
                .Select(a => new 
                {
                    HasDatabases = a.AccountDatabases.Any(ad => ad.State == "Ready" && ad.AccountDatabaseOnDelimiter == null),
                    HasSupportHistory = a.AccountDatabases
                        .Where(ad => ad.State == "Ready" && ad.AccountDatabaseOnDelimiter == null && ad.AcDbSupport != null)
                        .Any(ad => ad.AcDbSupport.AcDbSupportHistories.Any() || ad.AcDbSupport.HasSupport || ad.AcDbSupport.HasAutoUpdate)
                })
                .FirstOrDefaultAsync();

            return result != null && result.HasDatabases && !result.HasSupportHistory;

        }
    }
    
}
