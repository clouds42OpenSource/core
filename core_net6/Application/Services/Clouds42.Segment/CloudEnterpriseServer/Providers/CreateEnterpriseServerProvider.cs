﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudEnterpriseServer.Interfaces;

namespace Clouds42.Segment.CloudEnterpriseServer.Providers
{
    /// <summary>
    /// Провайдер создания серверов 1С:Предприятие
    /// </summary>
    internal class CreateEnterpriseServerProvider(IUnitOfWork dbLayer) : ICreateEnterpriseServerProvider
    {
        /// <summary>
        /// Добавить новый сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerDto">Сервер 1С:Предприятие</param>
        public Guid Create(EnterpriseServerDto enterpriseServerDto)
        {
            var enterpriseServer = new CloudServicesEnterpriseServer
            {
                ConnectionAddress = enterpriseServerDto.ConnectionAddress,
                Description = enterpriseServerDto.Description,
                Name = enterpriseServerDto.Name,
                AdminPassword = enterpriseServerDto.AdminPassword,
                AdminName = enterpriseServerDto.AdminName,
                Version = enterpriseServerDto.VersionEnum.ToString(),
                ID = Guid.NewGuid(),
                ClusterSettingsPath = enterpriseServerDto.ClusterSettingsPath
            };

            dbLayer.CloudServicesEnterpriseServerRepository.Insert(enterpriseServer);
            dbLayer.Save();

            return enterpriseServer.ID;
        }
    }
}
