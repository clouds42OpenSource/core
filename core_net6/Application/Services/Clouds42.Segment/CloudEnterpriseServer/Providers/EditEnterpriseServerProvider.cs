﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudEnterpriseServer.Helpers;
using Clouds42.Segment.Contracts.CloudEnterpriseServer.Interfaces;

namespace Clouds42.Segment.CloudEnterpriseServer.Providers
{
    /// <summary>
    /// Провайдер редактирования серверов 1С:Предприятие
    /// </summary>
    internal class EditEnterpriseServerProvider(
        EnterpriseServerDataHelper enterpriseServerDataHelper,
        IUnitOfWork dbLayer)
        : IEditEnterpriseServerProvider
    {
        /// <summary>
        /// Изменить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerDto">Обновленный сервер 1С:Предприятие.</param>
        public void Edit(EnterpriseServerDto enterpriseServerDto)
        {
            var enterpriseServer = enterpriseServerDataHelper.GetEnterpriseServerOrThrowException(enterpriseServerDto.Id);

            enterpriseServer.Description = enterpriseServerDto.Description;
            enterpriseServer.Name = enterpriseServerDto.Name;
            enterpriseServer.ConnectionAddress = enterpriseServerDto.ConnectionAddress;
            enterpriseServer.AdminPassword = enterpriseServerDto.AdminPassword;
            enterpriseServer.AdminName = enterpriseServerDto.AdminName;
            enterpriseServer.Version = enterpriseServerDto.VersionEnum.ToString();
            enterpriseServer.ClusterSettingsPath = enterpriseServerDto.ClusterSettingsPath;

            dbLayer.CloudServicesEnterpriseServerRepository.Update(enterpriseServer);
            dbLayer.Save();
        }
    }
}
