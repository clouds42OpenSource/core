﻿using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudEnterpriseServer.Helpers;
using Clouds42.Segment.Contracts.CloudEnterpriseServer.Interfaces;

namespace Clouds42.Segment.CloudEnterpriseServer.Providers
{
    /// <summary>
    /// Провайдер удаления серверов 1С:Предприятие
    /// </summary>
    internal class DeleteEnterpriseServerProvider(
        EnterpriseServerDataHelper enterpriseServerDataHelper,
        IUnitOfWork dbLayer)
        : IDeleteEnterpriseServerProvider
    {
        /// <summary>
        /// Удалить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера 1С:Предприятие</param>
        public void Delete(Guid enterpriseServerId)
        {
            var enterpriseServer = enterpriseServerDataHelper.GetEnterpriseServerOrThrowException(enterpriseServerId);

            dbLayer.CloudServicesEnterpriseServerRepository.Delete(enterpriseServer);
            dbLayer.Save();
        }
    }
}
