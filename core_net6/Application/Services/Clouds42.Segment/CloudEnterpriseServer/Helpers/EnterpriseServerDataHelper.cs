﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Segment.CloudEnterpriseServer.Helpers
{
    /// <summary>
    /// Хэлпер данных серверов 1С:Предприятие
    /// </summary>
    public class EnterpriseServerDataHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера 1С:Предприятие</param>
        /// <returns>Сервер 1С:Предприятие</returns>
        public CloudServicesEnterpriseServer GetEnterpriseServerOrThrowException(Guid enterpriseServerId)
            => dbLayer.CloudServicesEnterpriseServerRepository.FirstOrDefault(server => server.ID == enterpriseServerId)
               ?? throw new NotFoundException($"Сервер 1С:Предприятие по ID {enterpriseServerId} не найден");

        /// <summary>
        /// Признак что такой адрес сервера 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServer">Сервер 1С:Предприятие</param>
        /// <returns>Признак что такой адрес сервера 1С:Предприятие уже существует</returns>
        public bool ConnectionAddressAlreadyExist(EnterpriseServerDto enterpriseServer)
            => dbLayer.CloudServicesEnterpriseServerRepository.FirstOrDefault(server =>
                   server.ConnectionAddress.ToLower() == enterpriseServer.ConnectionAddress.ToLower() &&
                   server.ID != enterpriseServer.Id) !=
               null;

        /// <summary>
        /// Признак что такое название сервера 1С:Предприятие уже существует
        /// </summary>
        /// <param name="enterpriseServer">Сервер 1С:Предприятие</param>
        /// <returns>Признак что такое название сервера 1С:Предприятие уже существует</returns>
        public bool NameAlreadyExist(EnterpriseServerDto enterpriseServer)
            => dbLayer.CloudServicesEnterpriseServerRepository.FirstOrDefault(server =>
                   server.Name.ToLower() == enterpriseServer.Name.ToLower() &&
                   server.ID != enterpriseServer.Id) !=
               null;

        /// <summary>
        /// Признак что сервер 1С:Предприятие используется
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера 1С:Предприятие</param>
        /// <returns>Признак что сервер 1С:Предприятие используется</returns>
        public bool EnterpriseServerUsed(Guid enterpriseServerId)
            => dbLayer.CloudServicesSegmentRepository.FirstOrDefault(segment =>
                   segment.EnterpriseServer82ID == enterpriseServerId ||
                   segment.EnterpriseServer83ID == enterpriseServerId) != null;

    }
}
