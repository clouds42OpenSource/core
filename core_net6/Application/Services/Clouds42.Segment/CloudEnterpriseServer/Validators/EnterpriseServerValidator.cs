﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.Enums._1C;
using Clouds42.Segment.CloudEnterpriseServer.Helpers;

namespace Clouds42.Segment.CloudEnterpriseServer.Validators
{
    /// <summary>
    /// Валидатор для серверов 1С:Предприятие
    /// </summary>
    public class EnterpriseServerValidator(EnterpriseServerDataHelper enterpriseServerDataHelper)
    {
        /// <summary>
        /// Проверить сервер 1С:Предприятие перед удалением
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера 1С:Предприятие</param>
        /// <returns>Результат валидации</returns>
        public string ValidateEnterpriseServerBeforeDelete(Guid enterpriseServerId)
            => enterpriseServerDataHelper.EnterpriseServerUsed(enterpriseServerId)
                ? "Удаление невозможно, так как этот сервер 1С:Предприятие используется в сегменте"
                : string.Empty;

        /// <summary>
        /// Проверить модель сервера 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerDto">Обновленный сервер 1С:Предприятие</param>
        /// <returns>Результат валидации</returns>
        public string ValidateEnterpriseServerDto(EnterpriseServerDto enterpriseServerDto)
        {
            if (string.IsNullOrEmpty(enterpriseServerDto.ConnectionAddress))
                return "Поле Адрес обязательное для заполнения";

            if (string.IsNullOrEmpty(enterpriseServerDto.Name))
                return "Поле Название обязательное для заполнения";

            if(enterpriseServerDto.VersionEnum == PlatformType.Undefined)
                return "Необходимо указать версию платформы";

            if (enterpriseServerDataHelper.ConnectionAddressAlreadyExist(enterpriseServerDto))
                return "Сервер 1С:Предприятие с таким адресом подключения уже существует";

            return enterpriseServerDataHelper.NameAlreadyExist(enterpriseServerDto) 
                ? "Сервер 1С:Предприятие с таким названием уже существует"
                : string.Empty;
        }
    }
}
