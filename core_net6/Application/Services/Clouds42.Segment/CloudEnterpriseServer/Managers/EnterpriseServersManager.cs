﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudEnterpriseServer.Helpers;
using Clouds42.Segment.CloudEnterpriseServer.Validators;
using Clouds42.Segment.Contracts.CloudEnterpriseServer.Interfaces;

namespace Clouds42.Segment.CloudEnterpriseServer.Managers
{
    /// <summary>
    /// Менеджер для работы с серверами 1С:Предприятие
    /// </summary>
    public class EnterpriseServersManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IDeleteEnterpriseServerProvider deleteEnterpriseServerProvider,
        IEditEnterpriseServerProvider editEnterpriseServerProvider,
        ICreateEnterpriseServerProvider createEnterpriseServerProvider,
        EnterpriseServerValidator enterpriseServerValidator,
        EnterpriseServerDataHelper enterpriseServerDataHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Добавить новый сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServer">Сервер 1С:Предприятие</param>
        /// <returns>Результат добавления</returns>
        public ManagerResult<Guid> AddNewEnterpriseServer(EnterpriseServerDto enterpriseServer)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewCloudServiceEnterpriseServer,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = enterpriseServerValidator.ValidateEnterpriseServerDto(enterpriseServer);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError<Guid>(validationMessage);

                var enterpriseServerId = createEnterpriseServerProvider.Create(enterpriseServer);

                var message = $"Создан сервер 1С:Предприятие \"{enterpriseServer.Name}\"";

                logger.Trace($"{message} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);

                return Ok(enterpriseServerId);
            }
            catch (Exception ex)
            {
                var messageError = $"Ошибка добавления сервера 1С:Предприятие Имя: \"{enterpriseServer.Name}\", Адрес: \"{enterpriseServer.ConnectionAddress}\"";

                handlerException.Handle(ex, $"[{messageError} пользователем {AccessProvider.Name}.]");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, messageError + $" Причина: {ex.Message}");

                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Удалить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера 1С:Предприятие</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult DeleteEnterpriseServer(Guid enterpriseServerId)
        {
            var enterpriseServerName = string.Empty;
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeleteCloudServiceEnterpriseServer,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = enterpriseServerValidator.ValidateEnterpriseServerBeforeDelete(enterpriseServerId);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError(validationMessage);

                var enterpriseServer =
                    enterpriseServerDataHelper.GetEnterpriseServerOrThrowException(enterpriseServerId);
                enterpriseServerName = enterpriseServer.Name;

                deleteEnterpriseServerProvider.Delete(enterpriseServerId);

                var message = $"Удален сервер 1С:Предприятие \"{enterpriseServerName}\"";

                logger.Trace($"{message} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка удаления сервера 1С:Предприятие \"{enterpriseServerName}\"";
                handlerException.Handle(ex, $"[{errorMessage} пользователем {AccessProvider.Name}.]");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Изменить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServer">Обновленный сервер 1С:Предприятие.</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult EditEnterpriseServer(EnterpriseServerDto enterpriseServer)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdateCloudServiceEnterpriseServer,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = enterpriseServerValidator.ValidateEnterpriseServerDto(enterpriseServer);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError(validationMessage);

                editEnterpriseServerProvider.Edit(enterpriseServer);

                var message = $"Сервер 1С:Предприятие \"{enterpriseServer.Name}\" отредактирован.";

                logger.Trace($"{message} пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка редактирования сервера 1С:Предприятие \"{enterpriseServer.Name}\" пользователем {AccessProvider.Name}.]");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement,
                    $"Ошибка редактирования сервера 1С:Предприятие \"{enterpriseServer.Name}\". Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
