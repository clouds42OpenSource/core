﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CoreHosting.Interfaces;
using Clouds42.Segment.CoreHosting.Validator;

namespace Clouds42.Segment.CoreHosting.Providers
{
    /// <summary>
    /// Провайдер для редактирования хостинга облака
    /// </summary>
    internal class EditCoreHostingProvider(IUnitOfWork dbLayer) : IEditCoreHostingProvider
    {
        /// <summary>
        /// Изменить хостинг
        /// </summary>
        /// <param name="coreHostingDc">Модель контракта хостинга</param>
        public void EditCoreHosting(CoreHostingDto coreHostingDc)
        {
            var coreHosting = dbLayer.CoreHostingRepository.FirstOrDefault(w => w.Id == coreHostingDc.Id) ??
                              throw new NotFoundException($"Хостинг по ID {coreHostingDc.Id} не найден");

            CoreHostingValidator.ValidateCoreHosting(coreHostingDc);

            coreHosting.Name = coreHostingDc.Name;
            coreHosting.UploadApiUrl = coreHostingDc.UploadApiUrl;
            coreHosting.UploadFilesPath = coreHostingDc.UploadFilesPath;

            dbLayer.CoreHostingRepository.Update(coreHosting);
            dbLayer.Save();
        }
    }
}
