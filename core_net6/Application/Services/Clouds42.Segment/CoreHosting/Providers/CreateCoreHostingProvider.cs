﻿using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CoreHosting.Interfaces;
using Clouds42.Segment.CoreHosting.Mappers;
using Clouds42.Segment.CoreHosting.Validator;

namespace Clouds42.Segment.CoreHosting.Providers
{
    /// <summary>
    /// Провайдер создания хостинга облака
    /// </summary>
    internal class CreateCoreHostingProvider(IUnitOfWork dbLayer) : ICreateCoreHostingProvider
    {
        /// <summary>
        /// Создать хостинг
        /// </summary>
        /// <param name="coreHostingDc">Модель контракта хостинга</param>
        public void CreateCoreHosting(CoreHostingDto coreHostingDc)
        {
            CoreHostingValidator.ValidateCoreHosting(coreHostingDc);

            coreHostingDc.Id = Guid.NewGuid();
            var coreHosting = coreHostingDc.MapToCoreHosting();

            dbLayer.CoreHostingRepository.Insert(coreHosting);
            dbLayer.Save();
        }
    }
}
