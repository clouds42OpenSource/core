﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CoreHosting.Interfaces;
using Clouds42.Segment.CoreHosting.Mappers;

namespace Clouds42.Segment.CoreHosting.Providers
{
    /// <summary>
    /// Провайдер для хостинга облака
    /// </summary>
    internal class CoreHostingProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : ICoreHostingProvider
    {
        /// <summary>
        /// Получить модель хостинга облака
        /// </summary>
        /// <param name="coreHostingId">ID хостинга облака</param>
        /// <returns>Модель хостинга облака</returns>
        public CoreHostingDto GetCoreHostingById(Guid coreHostingId)
        {
            try
            {
                var coreHosting = dbLayer.CoreHostingRepository.FirstOrDefault(w => w.Id == coreHostingId) ??
                                  throw new NotFoundException($"Хостинг по ID {coreHostingId} не найден");

                return coreHosting.MapToCoreHostingDc();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения хостинга] {coreHostingId}");
                throw;
            }
        }

        /// <summary>
        /// Удалить хостинг
        /// </summary>
        /// <param name="coreHostingId">ID хостинга</param>
        public void DeleteCoreHosting(Guid coreHostingId)
        {
            var coreHosting = dbLayer.CoreHostingRepository.FirstOrDefault(w => w.Id == coreHostingId) ??
                              throw new NotFoundException($"Хостинг по ID {coreHostingId} не найден");

            var coreHostingUsageCount =
                dbLayer.CloudServicesSegmentRepository.Where(w => w.CoreHostingId == coreHostingId).Count();

            if (coreHostingUsageCount > 0)
                throw new InvalidOperationException(
                    $"Невозможно удалить хостинг '{coreHosting.Name}' так как он используется в сегменте");

            dbLayer.CoreHostingRepository.Delete(coreHosting);
            dbLayer.Save();
        }
    }
}
