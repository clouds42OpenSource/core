﻿using Clouds42.DataContracts.Service.CoreHosting;

namespace Clouds42.Segment.CoreHosting.Mappers
{
    /// <summary>
    /// Маппер для хостинга облака
    /// </summary>
    public static class CoreHostingMapper
    {
        /// <summary>
        /// Смапить доменную модель хостинга к модели контракта
        /// </summary>
        /// <param name="coreHosting">Доменная модель хостинга</param>
        /// <returns>Модель контракта</returns>
        public static CoreHostingDto MapToCoreHostingDc(this Domain.DataModels.CoreHosting coreHosting)
            => new()
            {
                Id = coreHosting.Id,
                Name = coreHosting.Name,
                UploadApiUrl = coreHosting.UploadApiUrl,
                UploadFilesPath = coreHosting.UploadFilesPath
            };

        /// <summary>
        /// Смапить модель контракта к доменной модели хостинга
        /// </summary>
        /// <param name="coreHostingDc">Модель контракта</param>
        /// <returns>Доменная модель хостинга</returns>
        public static Domain.DataModels.CoreHosting MapToCoreHosting(this CoreHostingDto coreHostingDc)
            => new()
            {
                Id = coreHostingDc.Id,
                Name = coreHostingDc.Name,
                UploadFilesPath = coreHostingDc.UploadFilesPath,
                UploadApiUrl = coreHostingDc.UploadApiUrl
            };
    }
}
