﻿using Clouds42.DataContracts.Service.CoreHosting;

namespace Clouds42.Segment.CoreHosting.Validator
{
    /// <summary>
    /// Валидатор модели хостинга
    /// </summary>
    public static class CoreHostingValidator
    {
        /// <summary>
        /// Проверить модель хостинга
        /// </summary>
        /// <param name="coreHostingDc">Модель хостинга</param>
        /// <returns>Результат валидации</returns>
        public static void ValidateCoreHosting(CoreHostingDto coreHostingDc)
        {
            ValidateModelField("Название хостинга", coreHostingDc.Name);
            ValidateModelField("Путь для загружаемых файлов", coreHostingDc.UploadFilesPath);
            ValidateModelField("Адрес API для загрузки файлов", coreHostingDc.UploadApiUrl);
        }

        /// <summary>
        /// Проверить валидность поля
        /// </summary>
        /// <param name="fieldName">Имя поля</param>
        /// <param name="fieldValue">Занчение поля</param>
        private static void ValidateModelField(string fieldName, string fieldValue)
        {
            if (string.IsNullOrEmpty(fieldValue))
                throw new InvalidOperationException($"Необходимо указать значение: {fieldName}");
        }
    }
}
