﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CoreHosting.Interfaces;

namespace Clouds42.Segment.CoreHosting.Managers
{
    /// <summary>
    /// Менеджер хостинга облака
    /// </summary>
    public class CoreHostingManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICoreHostingProvider coreHostingProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Удалить хостинг
        /// </summary>
        /// <param name="coreHostingId">ID хостинга</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult DeleteCoreHosting(Guid coreHostingId)
        {
            var coreHostingName = string.Empty;
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Delete, () => AccessProvider.ContextAccountId);

                var coreHosting = coreHostingProvider.GetCoreHostingById(coreHostingId);
                coreHostingName = coreHosting.Name;
                coreHostingProvider.DeleteCoreHosting(coreHostingId);

                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement,
                    $"Удален хостинг \"{coreHostingName}\".");

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления хостинга \"{coreHostingName}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
