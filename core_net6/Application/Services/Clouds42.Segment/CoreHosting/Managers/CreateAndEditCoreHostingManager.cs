﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CoreHosting.Interfaces;

namespace Clouds42.Segment.CoreHosting.Managers
{
    /// <summary>
    /// Менеджер для создания и редактирования хостинга облака
    /// </summary>
    public class CreateAndEditCoreHostingManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateCoreHostingProvider createCoreHostingProvider,
        IEditCoreHostingProvider editCoreHostingProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Создать хостинг
        /// </summary>
        /// <param name="coreHostingDc">Модель контракта хостинга</param>
        /// <returns>Результат создания хостинга</returns>
        public ManagerResult CreateCoreHosting(CoreHostingDto coreHostingDc)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Add, () => AccessProvider.ContextAccountId);

                createCoreHostingProvider.CreateCoreHosting(coreHostingDc);

                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement,
                    $"Создан хостинг \"{coreHostingDc.Name}\".");

                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания хостинга] {coreHostingDc.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement,
                    $"Ошибка создания хостинга \"{coreHostingDc.Name}\". Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Изменить хостинг
        /// </summary>
        /// <param name="coreHostingDc">Модель контракта хостинга</param>
        /// <returns>Результат изменения хостинга</returns>
        public ManagerResult EditCoreHosting(CoreHostingDto coreHostingDc)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                editCoreHostingProvider.EditCoreHosting(coreHostingDc);

                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement,
                    $"Хостинг \"{coreHostingDc.Name}\" отредактирован.");

                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования хостинга] {coreHostingDc.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement,
                    $"Ошибка редактирования хостинга \"{coreHostingDc.Name}\". Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
