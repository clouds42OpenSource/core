﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;

namespace Clouds42.Segment.CloudsServicesSegment.Mappers
{
    /// <summary>
    /// Маппер модели элемента сегмента
    /// </summary>
    public static class SegmentElementDtoMapper
    {
        /// <summary>
        /// Выполнить маппинг моделей
        /// </summary>
        /// <param name="segmentFileStorageServers">Данные файловых хранилищ сегмента</param>
        /// <returns>Список элементов сегмента</returns>
        public static List<SegmentElementDto>
            MapToSegmentElementList(this IEnumerable<SegmentFileStorageServerDataDto> segmentFileStorageServers) =>
            segmentFileStorageServers.Select(ms => new SegmentElementDto
            {
                Id = ms.Id,
                Name = ms.Name
            }).ToList();
    }
}
