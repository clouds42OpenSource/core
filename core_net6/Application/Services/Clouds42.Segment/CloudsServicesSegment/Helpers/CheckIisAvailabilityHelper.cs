﻿using System.Net.NetworkInformation;
using Clouds42.Logger;
using Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces;

namespace Clouds42.Segment.CloudsServicesSegment.Helpers
{
    /// <summary>
    /// Хелпер проверки доступности ИИС
    /// </summary>
    public class CheckIisAvailabilityHelper(ILogger42 logger) : ICheckIisAvailabilityHelper
    {
        /// <summary>
        /// Проверить доступность IIS на сервере
        /// </summary>
        /// <param name="address">Адрес</param>
        /// <returns>Доступность IIS</returns>
        public bool CheckIisAvailability(string address)
        {
            try
            {
                using var ping = new Ping();
                var reply = ping.Send(address);

                return reply.Status == IPStatus.Success;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
