﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Segment.CloudsServicesSegment.Helpers
{
    /// <summary>
    /// Хэлпер для работы с сегментами
    /// </summary>
    public class CloudSegmentDataHelper(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        /// Получить сегмент
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Сегмент</returns>
        public CloudServicesSegment GetSegmentOrThrowException(Guid segmentId)
            => dbLayer.CloudServicesSegmentRepository.FirstOrDefault(segment => segment.ID == segmentId) ??
               throw new NotFoundException($"Сегмент по ID {segmentId} не найден");

        /// <summary>
        /// Получить список миграций для сегмента
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Список миграций для сегмента</returns>
        public List<AvailableMigration> GetSegmentAvailableMigrations(Guid segmentId)
            => dbLayer.AvailableMigrationRepository
                .Where(migration => migration.SegmentIdFrom == segmentId || migration.SegmentIdTo == segmentId)
                .ToList();

        /// <summary>
        /// Получить список историй миграций аккаунта
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Список историй миграций аккаунта</returns>
        public List<MigrationAccountHistory> GetSegmentAccountMigrationHistories(Guid segmentId)
            => dbLayer.MigrationAccountHistoryRepository
                .Where(history => history.SourceSegmentId == segmentId || history.TargetSegmentId == segmentId)
                .ToList();

        /// <summary>
        /// Получить список хранилищ сегмента
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Список хранилищ сегмента</returns>
        public List<CloudServicesSegmentStorage> GetSegmentStorages(Guid segmentId)
            => dbLayer.CloudServicesSegmentStorageRepository
                .Where(segmentStorage => segmentStorage.SegmentID == segmentId)
                .ToList();

        /// <summary>
        /// Получить аккаунты сегмента
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Аккаунты сегмента</returns>
        public IQueryable<Account> GetSegmentAccounts(Guid segmentId)
            => accountConfigurationDataProvider.GetAccountConfigurationsDataBySegment()
                .Where(accountConfig => accountConfig.Segment.ID == segmentId)
                .Select(accountConfig => (Account)accountConfig.Account);

        /// <summary>
        /// Получить список опубликованных баз сегмента
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Список опубликованных баз сегмента</returns>
        public IQueryable<Domain.DataModels.AccountDatabase> GetSegmentPublishedDatabases(Guid segmentId) =>
            from database in GetSegmentDatabases(segmentId)
            where database.PublishState ==
                  PublishState.Published.ToString()
                  && database.AccountDatabaseOnDelimiter == null
                  && database.State == DatabaseState.Ready.ToString()
            select database;

        /// <summary>
        /// Получить список инф. баз сегмента на альфа версии платформы
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Список инф. баз сегмента на альфа версии платформы</returns>
        public IQueryable<Domain.DataModels.AccountDatabase> GetSegmentDatabasesOnAlfaVersion(Guid segmentId)
            => GetSegmentDatabases(segmentId)
                .Where(database => database.DistributionType == DistributionType.Alpha.ToString()
                                   && database.AccountDatabaseOnDelimiter == null
                                   && database.State == DatabaseState.Ready.ToString());

        /// <summary>
        /// Получить файловое хранилище
        /// </summary>
        /// <param name="fileStorageId">ID файлового хранилища</param>
        /// <returns>Файловое хранилище</returns>
        public Domain.DataModels.CloudServicesFileStorageServer GetFileStorageOrThrowException(Guid fileStorageId)
            => dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(storage =>
                   storage.ID == fileStorageId) ??
               throw new ArgumentNullException(
                   $"Файловое хранилище по ID {fileStorageId} не найдено");

        /// <summary>
        /// Получить файловое хранилище сегмента
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <param name="storageId">ID хранилища</param>
        /// <returns>Файловое хранилище сегмента</returns>
        public CloudServicesSegmentStorage GetSegmentFileStorage(Guid segmentId, Guid storageId)
            => dbLayer.CloudServicesSegmentStorageRepository.FirstOrDefault(storage =>
                storage.SegmentID == segmentId && storage.FileStorageID == storageId);

        /// <summary>
        /// Получить список баз на хранилище сегмента
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <param name="storageId">ID хранилища</param>
        /// <returns>Список баз на хранилище сегмента</returns>
        public IQueryable<Domain.DataModels.AccountDatabase> GetSegmentFileStorageAccountDatabases(Guid segmentId, Guid storageId)
            => GetSegmentDatabases(segmentId).Where(database =>
                database.FileStorageID == storageId &&
                database.IsFile.HasValue &&
                database.IsFile == true &&
                (
                    database.State == DatabaseState.Ready.ToString() ||
                    database.State == DatabaseState.TransferDb.ToString() ||
                    database.State == DatabaseState.NewItem.ToString() ||
                    database.State == DatabaseState.ProcessingSupport.ToString()
                ));

        /// <summary>
        /// Получить информационные базы по сегменту
        /// </summary>
        /// <param name="segmentId">Id сегмента</param>
        /// <returns>Информационные базы по сегменту</returns>
        private IQueryable<Domain.DataModels.AccountDatabase> GetSegmentDatabases(Guid segmentId) =>
            from accountConfiguration in accountConfigurationDataProvider.GetAccountConfigurationsDataBySegment()
            join database in dbLayer.DatabasesRepository.WhereLazy() on accountConfiguration.Account.Id equals
                database.AccountId
            where accountConfiguration.Segment.ID == segmentId
            select database;
    }
}
