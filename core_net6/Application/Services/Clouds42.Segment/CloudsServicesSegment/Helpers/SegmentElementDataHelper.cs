﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.Enums;

namespace Clouds42.Segment.CloudsServicesSegment.Helpers
{
    /// <summary>
    /// Хелпер для работы с данными элементов сегмента
    /// </summary>
    public static class SegmentElementDataHelper
    {
        /// <summary>
        /// Получить элементы сегмента по типу
        /// </summary>
        /// <param name="segmentElements">Список данных элементов сегмента</param>
        /// <param name="type">Тип элемента сегмента</param>
        /// <returns>Элементы сегмента</returns>
        public static List<SegmentElementDto> GetSegmentElementsByType(this List<SegmentElementDataDto> segmentElements,
            CloudSegmentElementType type) => segmentElements.Where(se => se.CloudSegmentElementType == type)
            .Select(se => new SegmentElementDto {Id = se.Id, Name = se.Name}).ToList();
    }
}
