﻿using System.Text;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Segment.CloudsServicesSegment.Helpers
{
    /// <summary>
    /// Хелпер для генерации сообщения логов при изменениях сегмента
    /// </summary>
    public class LogSegmentEventHelper
    {
        private readonly IUnitOfWork _dbLayer;

        private readonly List<(
            Func<EditCloudServicesSegmentDto, CloudServicesSegment, bool> functionToCheckEdit,
            Action<CloudServicesSegment, EditCloudServicesSegmentDto, StringBuilder> actionToAddEditMessage
            )> _appendSegmentChangesFuncList;

        public LogSegmentEventHelper(IUnitOfWork dbLayer)
        {
            _dbLayer = dbLayer;

            _appendSegmentChangesFuncList =
            [
                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.Name, editedSegmentData.Name),
                    AppendNameChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.Description, editedSegmentData.Description),
                    AppendDescriptionChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.BackupStorageID, editedSegmentData.BackupStorageID),
                    AppendBackupStorageChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.EnterpriseServer82ID,
                            editedSegmentData.EnterpriseServer82ID),
                    AppendEnterpriseServer82Changes),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.ContentServerID, editedSegmentData.ContentServerID),
                    AppendContentServerIdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.ServicesTerminalFarmID,
                            editedSegmentData.ServicesTerminalFarmID),
                    AppendServicesTerminalFarmIdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.Stable82Version, editedSegmentData.Stable82VersionId),
                    AppendStable82VersionIdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.Alpha83Version, editedSegmentData.Alpha83VersionId),
                    AppendAlpha83VersionIdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.FileStorageServersID,
                            editedSegmentData.FileStorageServersID),
                    AppendFileStorageServersIdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.EnterpriseServer83ID,
                            editedSegmentData.EnterpriseServer83ID),
                    AppendEnterpriseServer83IdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.SQLServerID, editedSegmentData.SQLServerID),
                    AppendSqlServerIdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.GatewayTerminalsID,
                            editedSegmentData.GatewayTerminalsID),
                    AppendGatewayTerminalsIdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.Stable83Version, editedSegmentData.Stable83VersionId),
                    AppendStable83VersionIdChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.CustomFileStoragePath,
                            editedSegmentData.CustomFileStoragePath),
                    AppendCustomFileStoragePathChanges),


                ((editedSegmentData, oldSegmentData)
                        => CheckFieldToEdit(oldSegmentData.DelimiterDatabaseMustUseWebService,
                            editedSegmentData.DelimiterDatabaseMustUseWebService),
                    AppendDelimiterDatabaseMustUseWebServiceChanges)
            ];
        }

        /// <summary>
        /// Проверить поля на изменение
        /// </summary>
        /// <param name="editedField"> Поле после редактирования </param>
        /// <param name="oldField"> Поле до редактирования </param>
        /// <returns> Результат проверки </returns>
        private static bool CheckFieldToEdit(object editedField, object oldField) => !Equals(editedField, oldField);

        /// <summary>
        /// Получить список сообщений об изменениях сегмента
        /// </summary>
        /// <param name="newSegmentData"> Сегмент после изменений </param>
        /// <param name="oldSegmentData"> Сегмент до изменений </param>
        /// <returns> Сообщение об изменениях сегмента </returns>
        public string GenerateMessageSegmentChanges(EditCloudServicesSegmentDto newSegmentData,
            CloudServicesSegment oldSegmentData)
        {
            var messageBuilder = new StringBuilder($"Сегмент \"{oldSegmentData.Name}\" отредактирован. ");

            _appendSegmentChangesFuncList.Where(tuple => tuple.functionToCheckEdit(newSegmentData, oldSegmentData))
                .ToList().ForEach(tuple => tuple.actionToAddEditMessage(oldSegmentData, newSegmentData, messageBuilder));

            return messageBuilder.ToString();
        }

        /// <summary>
        /// Сформировать сообщение об изменении поля в лог
        /// </summary>
        /// <param name="fieldName"> Что изменено </param>
        /// <param name="oldValue"> С какого значения </param>
        /// <param name="newValue"> На какое значение </param>
        /// <returns> Сообщение об изменении поля в сегменте </returns>
        private static string CreateLogMessageForEditedField(string fieldName, string oldValue, string newValue)
            => $"Изменено поле {fieldName} с \"{oldValue}\" на \"{newValue}\".\n";

        /// <summary>
        /// Добавить изменение наименования сегмента
        /// </summary>
        /// <param name="oldSegmentData"> Сегмент до изменений </param>
        /// <param name="newSegmentData"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendNameChanges(CloudServicesSegment oldSegmentData,
            EditCloudServicesSegmentDto newSegmentData, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Наименование",
                GetValueOrDefault(oldSegmentData.Name),
                GetValueOrDefault(newSegmentData.Name)));

        /// <summary>
        /// Добавить изменение сервера бэкапов
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendBackupStorageChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Сервер бэкапов",
                GetValueOrDefault(_dbLayer.CloudServicesBackupStorageRepository.GetById(oldSegment.BackupStorageID).Name),
                GetValueOrDefault(_dbLayer.CloudServicesBackupStorageRepository.GetById(newSegment.BackupStorageID).Name)));

        /// <summary>
        /// Добавить изменение сервера Предприятие 8.2
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendEnterpriseServer82Changes(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Предприятие 8.2",
                GetValueOrDefault(_dbLayer.CloudServicesEnterpriseServerRepository.GetById(oldSegment.EnterpriseServer82ID).Name),
                GetValueOrDefault(_dbLayer.CloudServicesEnterpriseServerRepository.GetById(newSegment.EnterpriseServer82ID).Name)));

        /// <summary>
        /// Добавить изменение сервера публикации
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendContentServerIdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Сервер публикации",
                GetValueOrDefault(_dbLayer.CloudServicesContentServerRepository.GetById(oldSegment.ContentServerID).Name),
                GetValueOrDefault(_dbLayer.CloudServicesContentServerRepository.GetById(newSegment.ContentServerID).Name)));

        /// <summary>
        /// Добавить изменение Фермы ТС
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendServicesTerminalFarmIdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Ферма ТС",
                GetValueOrDefault(_dbLayer.CloudServicesTerminalFarmRepository.GetById(oldSegment.ServicesTerminalFarmID).Name),
                GetValueOrDefault(_dbLayer.CloudServicesTerminalFarmRepository.GetById(newSegment.ServicesTerminalFarmID).Name)));

        /// <summary>
        /// Добавить изменение стабильной версии 8.2
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendStable82VersionIdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Cтабильная версий 8.2",
                GetValueOrDefault(oldSegment.Stable82Version),
                GetValueOrDefault(newSegment.Stable82VersionId)));

        /// <summary>
        /// Добавить изменение альфа версии 8.3
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendAlpha83VersionIdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Альфа версия 8.3",
                GetValueOrDefault(oldSegment.Alpha83Version),
                GetValueOrDefault(newSegment.Alpha83VersionId)));

        /// <summary>
        /// Добавить изменение описания
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendDescriptionChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Описание",
                GetValueOrDefault(oldSegment.Description),
                GetValueOrDefault(newSegment.Description)));

        /// <summary>
        /// Добавить изменение Хранилище клиентских файлов
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendFileStorageServersIdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Хранилище клиентских файлов",
                GetValueOrDefault(_dbLayer.CloudServicesFileStorageServerRepository.GetById(oldSegment.FileStorageServersID).Name),
                GetValueOrDefault(_dbLayer.CloudServicesFileStorageServerRepository.GetById(newSegment.FileStorageServersID).Name)));

        /// <summary>
        /// Добавить изменение предприятия 8.3
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendEnterpriseServer83IdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Предприятие 8.3",
                GetValueOrDefault(_dbLayer.CloudServicesEnterpriseServerRepository.GetById(oldSegment.EnterpriseServer83ID).Name),
                GetValueOrDefault(_dbLayer.CloudServicesEnterpriseServerRepository.GetById(newSegment.EnterpriseServer83ID).Name)));

        /// <summary>
        /// Добавить изменение SQL сервера
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendSqlServerIdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("SQL сервер",
                GetValueOrDefault(_dbLayer.CloudServicesSqlServerRepository.GetById(oldSegment.SQLServerID).Name),
                GetValueOrDefault(_dbLayer.CloudServicesSqlServerRepository.GetById(newSegment.SQLServerID).Name)));

        /// <summary>
        /// Добавить изменение шлюза
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendGatewayTerminalsIdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Шлюз",
                GetValueOrDefault(_dbLayer.CloudServicesGatewayTerminalRepository.GetById(oldSegment.GatewayTerminalsID)?.Name),
                GetValueOrDefault(_dbLayer.CloudServicesGatewayTerminalRepository.GetById(newSegment.GatewayTerminalsID)?.Name)));

        /// <summary>
        /// Добавить изменение стабильной версии 8.3
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendStable83VersionIdChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Cтабильная версия 8.3",
                GetValueOrDefault(oldSegment.Stable83Version),
                GetValueOrDefault(newSegment.Stable83VersionId)));

        /// <summary>
        /// Добавить изменение пути к файлам пользователей
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendCustomFileStoragePathChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
            => messageBuilder.AppendLine(CreateLogMessageForEditedField("Путь к файлам пользователей",
                GetValueOrDefault(oldSegment.CustomFileStoragePath),
                GetValueOrDefault(newSegment.CustomFileStoragePath)));

        /// <summary>
        /// Добавить изменение чекбокса "Открывать базы в тонком клиенте по http ссылке"
        /// </summary>
        /// <param name="oldSegment"> Сегмент до изменений </param>
        /// <param name="newSegment"> Сегмент после изменений </param>
        /// <param name="messageBuilder"> Билдер сообщений в логе </param>
        private void AppendDelimiterDatabaseMustUseWebServiceChanges(CloudServicesSegment oldSegment,
            EditCloudServicesSegmentDto newSegment, StringBuilder messageBuilder)
        {
            messageBuilder.AppendLine(
                CreateLogMessageForEditedField("Открывать базы в тонком клиенте по http ссылке",
                GetCheckBoxStateDescription(newSegment.DelimiterDatabaseMustUseWebService),
                GetCheckBoxStateDescription(oldSegment.DelimiterDatabaseMustUseWebService)
                ));
        }

        /// <summary>
        /// Получить описание булевого значения
        /// </summary>
        /// <param name="state"> Статус чек бокса </param>
        /// <returns> Текстовое описание чекбокса </returns>
        private string GetCheckBoxStateDescription(bool state)
            => state ? "Включен" : "Выключен";

        /// <summary>
        /// Получить значение поля сегмента или значение по умолчанию
        /// </summary>
        /// <param name="segmentDataValue"> Значение поля сегмента </param>
        /// <returns> Значение поля сегмента </returns>
        private string GetValueOrDefault(string segmentDataValue)
            => segmentDataValue.IsNullOrEmpty() ? string.Empty : segmentDataValue;
    }
}
