﻿using System.Text;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudsServicesSegment.Helpers;

namespace Clouds42.Segment.CloudsServicesSegment.Validators
{
    /// <summary>
    /// Валидатор для сегментов
    /// </summary>
    public class CloudServicesSegmentValidator(
        CloudSegmentDataHelper cloudSegmentDataHelper,
        IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Проверить возможность удаления файлового хранилища из доступных
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <param name="storageId">ID хранилища</param>
        /// <returns>Результат проверки</returns>
        public string CheckAbilityToDeleteFileStorage(Guid segmentId, Guid storageId)
        {
            var segmentFileStorage =
                cloudSegmentDataHelper.GetSegmentFileStorage(segmentId, storageId);

            if (segmentFileStorage == null)
                return string.Empty;

            if (segmentFileStorage.IsDefault)
                return "Удаление не возможно. Это хранилище по умолчанию.";

            var segmentStorageAccountDatabases =
                cloudSegmentDataHelper.GetSegmentFileStorageAccountDatabases(segmentId, storageId);

            return segmentStorageAccountDatabases.Any() 
                ? $"Удаление не возможно. В данном хранилище находятся инф. базы. Количество: {segmentStorageAccountDatabases.Count()}" 
                : string.Empty;
        }

        /// <summary>
        /// Проверить сегмент перед удалением
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Результат валидации</returns>
        public string ValidateSegmentBeforeDelete(Guid segmentId)
        {
            var segment = cloudSegmentDataHelper.GetSegmentOrThrowException(segmentId);

            var segmentAccounts = cloudSegmentDataHelper.GetSegmentAccounts(segment.ID);
            if (segmentAccounts.Any())
                return $"Удаление не возможно. Данный сегмент используют {segmentAccounts.Count()} аккаунтов";

            var segmentStorages = cloudSegmentDataHelper.GetSegmentStorages(segment.ID);
            return segmentStorages.Count > 1
                ? $"Удаление не возможно. Данный сегмент используют {segmentStorages.Count} хранилищ"
                : string.Empty;
        }

        /// <summary>
        /// Проверить сегмент перед созданием
        /// </summary>
        /// <param name="segmentDto">Создаваемый сегмент</param>
        /// <returns>Результат валидации</returns>
        public string ValidateSegmentBeforeCreate(CreateCloudServicesSegmentDto segmentDto)
        {
            if (segmentDto.CoreHostingId.IsNullOrEmpty())
                return "Поле Хостинг обязательное для заполнения";

            var checkSegmentNameToUnique = CheckSegmentNameForUniqueness(segmentDto.Name, null);
            if (!checkSegmentNameToUnique)
                return "Сегмент с таким названием уже существует!";

            var commonValidation = ValidateSegmentDto(segmentDto);
            if (!string.IsNullOrEmpty(commonValidation))
                return commonValidation;

            return string.Empty;
        }

        /// <summary>
        /// Проверить сегмент перед редактированием
        /// </summary>
        /// <param name="segmentDto">Обновленный сегмент</param>
        /// <returns>Результат валидации</returns>
        public string ValidateSegmentBeforeEdit(EditCloudServicesSegmentDto segmentDto)
        {
            var segment = cloudSegmentDataHelper.GetSegmentOrThrowException(segmentDto.Id);

            var checkSegmentNameToUnique = CheckSegmentNameForUniqueness(segmentDto.Name, segment.ID);
            if (!checkSegmentNameToUnique)
                return "Сегмент с таким названием уже существует!";

            var commonValidation = ValidateSegmentDto(segmentDto);
            if (!string.IsNullOrEmpty(commonValidation))
                return commonValidation;

            var checkSegmentPublishedDbs = CheckSegmentPublishedDbs(segment, segmentDto);
            if (!string.IsNullOrEmpty(checkSegmentPublishedDbs))
                return checkSegmentPublishedDbs;

            var validateAlphaVersionsWhenEditSegment = ValidateAlphaVersionsWhenEditSegment(segment, segmentDto);
            if (!string.IsNullOrEmpty(validateAlphaVersionsWhenEditSegment))
                return validateAlphaVersionsWhenEditSegment;

            var validateFileStorageWhenEditingSegment = ValidateFileStorageWhenEditSegment(segment, segmentDto);
            return !string.IsNullOrEmpty(validateFileStorageWhenEditingSegment) 
                ? validateFileStorageWhenEditingSegment 
                : string.Empty;
        }

        /// <summary>
        /// Проверить наименование сегмента на уникальность
        /// </summary>
        /// <param name="segmentName"> Наименование сегмента </param>
        /// <param name="segmentId"> Id сегмента </param>
        /// <returns> true - если уникальное, false - если не уникальное </returns>
        private bool CheckSegmentNameForUniqueness(string? segmentName, Guid? segmentId)
        {
            segmentName ??= string.Empty;

            return !dbLayer.CloudServicesSegmentRepository
                .Any(segment => segment.Name.Trim() == segmentName.Trim() && segment.ID != segmentId);
        }

        /// <summary>
        /// Проверить модель сегмента
        /// </summary>
        /// <param name="segmentDto">Обновленный сегмент</param>
        /// <returns>Результат валидации</returns>
        private static string ValidateSegmentDto(CloudServicesSegmentBaseDataDto segmentDto)
        {
            if (string.IsNullOrEmpty(segmentDto.Name))
                return "Поле Наименование обязательное для заполнения";

            if (segmentDto.ServicesTerminalFarmID.IsNullOrEmpty())
                return "Поле Ферма ТС обязательное для заполнения";

            if (segmentDto.EnterpriseServer82ID.IsNullOrEmpty())
                return "Поле Предприятие 8.2 обязательное для заполнения";

            if (segmentDto.EnterpriseServer83ID.IsNullOrEmpty())
                return "Поле Предприятие 8.3 обязательное для заполнения";

            if (segmentDto.ContentServerID.IsNullOrEmpty())
                return "Поле Сервер публикаций обязательное для заполнения";

            if (segmentDto.FileStorageServersID.IsNullOrEmpty())
                return "Поле Хранилище клиентских файлов обязательное для заполнения";

            if (segmentDto.SQLServerID.IsNullOrEmpty())
                return "Поле SQL сервер обязательное для заполнения";

            if (segmentDto.BackupStorageID.IsNullOrEmpty())
                return "Поле Хранилище бекапов обязательное для заполнения";

            if (string.IsNullOrEmpty(segmentDto.Stable82VersionId))
                return "Поле Стабильная версия 8.2 обязательное для заполнения";

            if (string.IsNullOrEmpty(segmentDto.Stable83VersionId))
                return "Поле Стабильная версия 8.3 обязательное для заполнения";

            return string.Empty;
        }

        /// <summary>
        /// Проверить существование опубликованных баз на сегменте
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="segmentDto">Обновленный сегмент</param>
        /// <returns>Результат валидации</returns>
        private string CheckSegmentPublishedDbs(CloudServicesSegment segment,
            EditCloudServicesSegmentDto segmentDto)
        {
            if (segment.ContentServerID == segmentDto.ContentServerID)
                return string.Empty;

            var segmentPublishedDatabases = cloudSegmentDataHelper.GetSegmentPublishedDatabases(segment.ID);

            return segmentPublishedDatabases.Any()
                ? "Обнаружены опубликованные базы, перед сменой сервера публикации нужно снять все базы с публикации на текущем сервере"
                : string.Empty;
        }

        /// <summary>
        /// Проверить возможность смены альфа версии 8.3 при редактировании сегмента
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="segmentDto">Обновленный сегмент</param>
        /// <returns>Результат валидации</returns>
        private string ValidateAlphaVersionsWhenEditSegment(CloudServicesSegment segment, EditCloudServicesSegmentDto segmentDto)
        {
            var databasesWithAlphaPlatforms = cloudSegmentDataHelper.GetSegmentDatabasesOnAlfaVersion(segment.ID);

            if (segment.Alpha83Version == segmentDto.Alpha83VersionId || segmentDto.Alpha83VersionId != null)
                return string.Empty;

            return databasesWithAlphaPlatforms.Any() 
                ? "Невозможно отредактировать сегмент, так как не указана альфа версию 8.3, но обнаруженны файловые базы которые используют её" 
                : string.Empty;
        }

        /// <summary>
        /// Валидация смены файлового хранилища при редактировании сегмента
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="segmentDto">Модель данных</param>
        private string ValidateFileStorageWhenEditSegment(CloudServicesSegment segment, EditCloudServicesSegmentDto segmentDto)
        {
            if (segment.FileStorageServersID == segmentDto.FileStorageServersID)
                return string.Empty;

            var segmentAccounts = cloudSegmentDataHelper.GetSegmentAccounts(segment.ID);

            var newFileStorage = cloudSegmentDataHelper.GetFileStorageOrThrowException(segmentDto.FileStorageServersID);

            var stringBuilder = new StringBuilder();

            segmentAccounts.ToList().ForEach(account =>
            {
                var oldClientFileFolder = GetAccountFileFolder(segment.CloudServicesFileStorageServer.ConnectionAddress,
                    account.IndexNumber, segment.ClientFileFolder);
                var newClientFileFolder = GetAccountFileFolder(newFileStorage.ConnectionAddress,
                    account.IndexNumber, segment.ClientFileFolder);

                if (newClientFileFolder == oldClientFileFolder)
                    return;

                if (!Directory.Exists(oldClientFileFolder))
                    return;

                stringBuilder.Append($"<p>Для аккаунта {account.IndexNumber} есть файлы по пути {oldClientFileFolder}</p>");
            });

            return stringBuilder.Length > 0 
                ? $"Невозможно сменить файловое хранилище сегмента, так как: {stringBuilder}" 
                : string.Empty;
        }

        /// <summary>
        /// Получить путь к папке аккаунта
        /// </summary>
        /// <param name="fileStorageConnectionAddress">Адрес файлового хранилища</param>
        /// <param name="accountIndexNumber">Номер аккаунта</param>
        /// <param name="clientFileFolder">Путь к папкам клиентов</param>
        /// <returns>Путь к папке аккаунта</returns>
        private static string GetAccountFileFolder(string fileStorageConnectionAddress, int accountIndexNumber,
            string clientFileFolder)
            => Path.Combine(fileStorageConnectionAddress,
                $@"company_{accountIndexNumber}\{clientFileFolder}");

        
    }
}
