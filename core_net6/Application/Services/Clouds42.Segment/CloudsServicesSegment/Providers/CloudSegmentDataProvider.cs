﻿using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces;

namespace Clouds42.Segment.CloudsServicesSegment.Providers
{
    /// <summary>
    /// Провадер для работы с данными сегмента
    /// </summary>
    internal class CloudSegmentDataProvider(IUnitOfWork dbLayer) : ICloudSegmentDataProvider
    {
        /// <summary>
        /// Получить название сегмента
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Название сегмента</returns>
        public string GetSegmentName(Guid segmentId)
            => dbLayer.CloudServicesSegmentRepository.FirstOrThrowException(x => x.ID == segmentId).Name;
    }
}
