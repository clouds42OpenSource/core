﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces;

namespace Clouds42.Segment.CloudsServicesSegment.Providers
{
    /// <summary>
    /// Провайдер для создания сегментов
    /// </summary>
    internal class CreateCloudSegmentProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : ICreateCloudSegmentProvider
    {
        /// <summary>
        /// Создать сегмент
        /// </summary>
        /// <param name="segmentDto">Сегмент</param>
        public void Create(CreateCloudServicesSegmentDto segmentDto)
        {
            var segmentId = Guid.NewGuid();

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var segment = new CloudServicesSegment
                {
                    ID = segmentId,
                    Name = segmentDto.Name,
                    Description = segmentDto.Description,
                    CustomFileStoragePath = segmentDto.CustomFileStoragePath,
                    EnterpriseServer82ID = segmentDto.EnterpriseServer82ID,
                    EnterpriseServer83ID = segmentDto.EnterpriseServer83ID,
                    SQLServerID = segmentDto.SQLServerID,
                    ServicesTerminalFarmID = segmentDto.ServicesTerminalFarmID,
                    BackupStorageID = segmentDto.BackupStorageID,
                    FileStorageServersID = segmentDto.FileStorageServersID,
                    ContentServerID = segmentDto.ContentServerID,
                    GatewayTerminalsID = segmentDto.GatewayTerminalsID == Guid.Empty ? null : segmentDto.GatewayTerminalsID,
                    IsDefault = segmentDto.IsDefault,
                    Stable82Version = segmentDto.Stable82VersionId,
                    Alpha83Version = string.IsNullOrWhiteSpace(segmentDto.Alpha83VersionId) ? null : segmentDto.Alpha83VersionId,
                    Stable83Version = segmentDto.Stable83VersionId,
                    CoreHostingId = segmentDto.CoreHostingId,
                    DelimiterDatabaseMustUseWebService = segmentDto.DelimiterDatabaseMustUseWebService,
                    NotMountDiskR = segmentDto.NotMountDiskR
                };

                var defStorage = new CloudServicesSegmentStorage
                {
                    ID = Guid.NewGuid(),
                    SegmentID = segmentId,
                    FileStorageID = segmentDto.FileStorageServersID,
                    IsDefault = true
                };

                dbLayer.CloudServicesSegmentRepository.Insert(segment);
                dbLayer.CloudServicesSegmentStorageRepository.Insert(defStorage);
                dbLayer.Save();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания сегмента] {segmentDto.Name}");
                transaction.Rollback();
                throw;
            }
        }
    }
}
