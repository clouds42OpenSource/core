﻿using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudsServicesSegment.Helpers;
using Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces;

namespace Clouds42.Segment.CloudsServicesSegment.Providers
{
    /// <summary>
    /// Провайдер удаления сегментов
    /// </summary>
    internal class DeleteCloudSegmentProvider(
        CloudSegmentDataHelper cloudSegmentDataHelper,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : IDeleteCloudSegmentProvider
    {
        /// <summary>
        /// Удалить сегмент
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        public void Delete(Guid segmentId)
        {
            var segment = cloudSegmentDataHelper.GetSegmentOrThrowException(segmentId);
            var migrationHistories = cloudSegmentDataHelper.GetSegmentAvailableMigrations(segmentId);
            var migrationAccountHistories = cloudSegmentDataHelper.GetSegmentAccountMigrationHistories(segmentId);
            var segmentStorages = cloudSegmentDataHelper.GetSegmentStorages(segmentId);

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                DeleteDefaultSegmentStorage(segmentStorages);

                DeleteSegmentAccountMigrationHistories(migrationAccountHistories);
                DeleteSegmentAvailableMigrations(migrationHistories);

                dbLayer.CloudServicesSegmentRepository.Delete(segment);
                dbLayer.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления сегмента] {segmentId} ");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Удалить дефолтное файловое хранилище сегмента
        /// </summary>
        /// <param name="segmentStorages">Хранилища сегмента</param>
        private void DeleteDefaultSegmentStorage(List<CloudServicesSegmentStorage> segmentStorages)
        {
            if (segmentStorages.Count == 1 && segmentStorages.First().IsDefault)
                dbLayer.CloudServicesSegmentStorageRepository.Delete(segmentStorages.First());
        }

        /// <summary>
        /// Удалить список историй миграций аккаунта
        /// </summary>
        /// <param name="migrationAccountHistories">Список историй миграций аккаунта</param>
        private void DeleteSegmentAccountMigrationHistories(List<MigrationAccountHistory> migrationAccountHistories)
        {
            if (!migrationAccountHistories.Any())
                return;

            dbLayer.MigrationAccountHistoryRepository.DeleteRange(migrationAccountHistories);
        }

        /// <summary>
        /// Удалить список миграций для сегмента
        /// </summary>
        /// <param name="migrationHistories">Список миграций для сегмента</param>
        private void DeleteSegmentAvailableMigrations(List<AvailableMigration> migrationHistories)
        {
            if (!migrationHistories.Any())
                return;

            dbLayer.AvailableMigrationRepository.DeleteRange(migrationHistories);
        }
    }
}
