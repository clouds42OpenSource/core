﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.CoreWorker.JobWrappers.PublishDatabase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudsServicesSegment.Helpers;
using Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces;

namespace Clouds42.Segment.CloudsServicesSegment.Providers
{
    /// <summary>
    /// Провайдер для редактирования сегментов
    /// </summary>
    internal class EditCloudSegmentProvider(
        CloudSegmentDataHelper cloudSegmentDataHelper,
        IUnitOfWork dbLayer,
        IRepublishSegmentDatabasesJobWrapper republishSegmentDatabasesJobWrapper,
        IUpdateAccountConfigurationProvider updateAccountConfigurationProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : IEditCloudSegmentProvider
    {
        /// <summary>
        /// Изменить сегмент
        /// </summary>
        /// <param name="segmentDto">Обновленная модель сегмента</param>
        public void Edit(EditCloudServicesSegmentDto segmentDto)
        {
            var segment = cloudSegmentDataHelper.GetSegmentOrThrowException(segmentDto.Id);

            var currentStable82 = segment.Stable82Version;
            var currentStable83 = segment.Stable83Version;
            var currentAlpha83 = segment.Alpha83Version;
            var currentEnterpriseServerV83 = segment.EnterpriseServer83ID;
            var currentPublishServer = segment.ContentServerID;

            using (var transaction = dbLayer.SmartTransaction.Get())
            {
                try
                {
                    segment.Name = segmentDto.Name;
                    segment.CustomFileStoragePath = segmentDto.CustomFileStoragePath;
                    segment.Description = segmentDto.Description;
                    segment.EnterpriseServer82ID = segmentDto.EnterpriseServer82ID;
                    segment.EnterpriseServer83ID = segmentDto.EnterpriseServer83ID;

                    var currentFileStorageId = segment.FileStorageServersID;
                    segment.FileStorageServersID = segmentDto.FileStorageServersID;

                    segment.GatewayTerminalsID = segmentDto.GatewayTerminalsID == Guid.Empty
                        ? null
                        : segmentDto.GatewayTerminalsID;

                    if (segment.GatewayTerminalsID == null)
                        segment.CloudServicesGatewayTerminal = null;

                    segment.SQLServerID = segmentDto.SQLServerID;
                    segment.ServicesTerminalFarmID = segmentDto.ServicesTerminalFarmID;
                    segment.BackupStorageID = segmentDto.BackupStorageID;
                    segment.ContentServerID = segmentDto.ContentServerID;

                    segment.Stable82Version = segmentDto.Stable82VersionId;
                    segment.Alpha83Version = segmentDto.Alpha83VersionId;
                    segment.Stable83Version = segmentDto.Stable83VersionId;
                    segment.DelimiterDatabaseMustUseWebService = segmentDto.DelimiterDatabaseMustUseWebService;
                    segment.AutoUpdateNodeId = segmentDto.AutoUpdateNodeId;
                    segment.NotMountDiskR = segmentDto.NotMountDiskR;

                    UpdateAvailableMigrationSegments(segmentDto);
                    UpdateSegmentTerminalServers(segmentDto);
                    UpdateSegmentFileStorages(segmentDto);

                    dbLayer.CloudServicesSegmentRepository.Update(segment);
                    dbLayer.Save();

                    if (currentFileStorageId != segmentDto.FileStorageServersID)
                        ChangeFileStorageForAccounts(segmentDto.Id, segmentDto.FileStorageServersID);

                    transaction.Commit();
                    logger.Info("Изменения в базе по сегменту окончены");
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Ошибка редактирования сегмента] {segmentDto.Id}");
                    transaction.Rollback();
                    throw;
                }
            }

            ProcessRepublishDatabases(currentStable82, currentStable83, currentAlpha83, currentEnterpriseServerV83, currentPublishServer, segmentDto);
        }

        /// <summary>
        /// Изменить платформу сегмента
        /// </summary>
        /// <param name="stable83VersionId">Идентификатор платформы
        /// <param name="segmentId">Идентификатор сегмента</param>
        public void EditPlatformVersion(string stable83VersionId, Guid segmentId)
        {
            var segment = cloudSegmentDataHelper.GetSegmentOrThrowException(segmentId);

                try
                {
                    segment.Stable83Version = stable83VersionId;

                    dbLayer.CloudServicesSegmentRepository.Update(segment);
                    dbLayer.Save();
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Ошибка редактирования сегмента] {segmentId}");
                    throw;
                }

            RepublishAccountDatabases(segmentId, PlatformType.V83, DistributionType.Stable);
        }

        /// <summary>
        /// Обновить доступные сегменты для миграции
        /// </summary>
        /// <param name="segmentDto">Обновленная модель сегмента</param>
        private void UpdateAvailableMigrationSegments(EditCloudServicesSegmentDto segmentDto)
        {
            var availableMigrationSegments = dbLayer.AvailableMigrationRepository.Where(migr =>
                migr.SegmentIdFrom == segmentDto.Id || migr.SegmentIdTo == segmentDto.Id).ToList();

            dbLayer.AvailableMigrationRepository.DeleteRange(availableMigrationSegments);

            if (!segmentDto.AvailableMigrationSegment.Any())
                return;

            segmentDto.AvailableMigrationSegment.ForEach(migrationSegment =>
            {
                dbLayer.AvailableMigrationRepository.Insert(new AvailableMigration
                {
                    Id = Guid.NewGuid(),
                    SegmentIdTo = migrationSegment.Id,
                    SegmentIdFrom = segmentDto.Id
                });

                dbLayer.AvailableMigrationRepository.Insert(new AvailableMigration
                {
                    Id = Guid.NewGuid(),
                    SegmentIdTo = segmentDto.Id,
                    SegmentIdFrom = migrationSegment.Id
                });
            });
        }

        /// <summary>
        /// Обновить терминальные сервера сегмента
        /// </summary>
        /// <param name="segmentDto">Обновленная модель сегмента</param>
        private void UpdateSegmentTerminalServers(EditCloudServicesSegmentDto segmentDto)
        {
            var segmentTerminalServers = dbLayer.CloudServicesSegmentTerminalServerRepository
                .Where(ts => ts.SegmentId == segmentDto.Id).ToList();

            dbLayer.CloudServicesSegmentTerminalServerRepository.DeleteRange(segmentTerminalServers);

            if(!segmentDto.SegmentTerminalServers.Any())
                return;

            segmentDto.SegmentTerminalServers.ForEach(terminalServer =>
            {
                dbLayer.CloudServicesSegmentTerminalServerRepository.Insert(new CloudServicesSegmentTerminalServer
                {
                    Id = Guid.NewGuid(),
                    SegmentId = segmentDto.Id,
                    TerminalServerId = terminalServer.Id
                });
            });
        }

        /// <summary>
        /// Обновить файловые хранилища сегмента
        /// </summary>
        /// <param name="segmentDto">Обновленная модель сегмента</param>
        private void UpdateSegmentFileStorages(EditCloudServicesSegmentDto segmentDto)
        {
            var segmentStorages =
                dbLayer.CloudServicesSegmentStorageRepository
                    .Where(storage => storage.SegmentID == segmentDto.Id).ToList();

            dbLayer.CloudServicesSegmentStorageRepository.DeleteRange(segmentStorages);

            if (!segmentDto.SegmentFileStorageServers.Any())
                return; 

            segmentDto.SegmentFileStorageServers.ForEach(storage =>
            {
                dbLayer.CloudServicesSegmentStorageRepository.Insert(new CloudServicesSegmentStorage
                {
                    ID = Guid.NewGuid(),
                    SegmentID = segmentDto.Id,
                    FileStorageID = storage.Id,
                    IsDefault = segmentDto.DefaultSegmentStorageId.Equals(storage.Id)
                });
            });
        }

        /// <summary>
        /// Сменить файловое хранилище для аккаунта
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <param name="newFileStorageId">ID нового файлового хранилища</param>
        private void ChangeFileStorageForAccounts(Guid segmentId, Guid newFileStorageId)
        {
            var segmentAccounts = cloudSegmentDataHelper.GetSegmentAccounts(segmentId).ToList();

            if (!segmentAccounts.Any())
                return;

            segmentAccounts.ForEach(account =>
                updateAccountConfigurationProvider.UpdateFileStorageId(account.Id, newFileStorageId));
        }

        /// <summary>
        /// Переопубликовать инф. базы
        /// если изменилась версия платформы
        /// </summary>
        /// <param name="currentAlpha83">Текущая альфа 8.3 версия</param>
        /// <param name="currentStable82">Текущая стабильная 8.2 версия</param>
        /// <param name="currentStable83">Текущая стабильная 8.3 версия</param>
        /// <param name="currentPublishServer"></param>
        /// <param name="segmentDto">Обновленная модель сегмента</param>
        /// <param name="currentEnterpriseServerV83"></param>
        private void ProcessRepublishDatabases(string currentStable82, string currentStable83,
            string currentAlpha83, Guid currentEnterpriseServerV83, Guid currentPublishServer, EditCloudServicesSegmentDto segmentDto)
        {
            var platformTypeToVersionMapping = new List<(PlatformType, DistributionType, Func<string>, Func<string>)>
            {
                (PlatformType.V82, DistributionType.Stable, () => currentStable82, () => segmentDto.Stable82VersionId),
                (PlatformType.V83, DistributionType.Stable, () => currentStable83, () => segmentDto.Stable83VersionId),
                (PlatformType.V83, DistributionType.Alpha, () => currentAlpha83, () => segmentDto.Alpha83VersionId)
            };

            foreach (var (platformType, distributionType, currentPlatformVersion, newPlatformVersion) in platformTypeToVersionMapping)
            {
                if (currentPlatformVersion() == newPlatformVersion() && currentEnterpriseServerV83 == segmentDto.EnterpriseServer83ID && currentPublishServer == segmentDto.ContentServerID)
                    continue;

                RepublishAccountDatabases(segmentDto.Id, platformType, distributionType);
            }
        }

        /// <summary>
        /// Переопубликовать инф. базы аккаунта
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <param name="platformType">Тип платформы</param>
        /// <param name="distributionType">Тип распространения</param>
        private void RepublishAccountDatabases(Guid segmentId,
            PlatformType platformType, DistributionType distributionType)
        {
            var publishedDbs = dbLayer.DatabasesRepository.AnyPublishedDbs(segmentId, platformType, distributionType);

            if (!publishedDbs) 
                return;

            logger.Info($"Запускаю таску по переопубликации инф. баз сегмента {segmentId}");

            republishSegmentDatabasesJobWrapper.Start(new RepublishSegmentDatabasesJobParamsDto
            {
                SegmentId = segmentId,
                DistributionType = distributionType,
                PlatformType = platformType
            });
        }
    }
}
