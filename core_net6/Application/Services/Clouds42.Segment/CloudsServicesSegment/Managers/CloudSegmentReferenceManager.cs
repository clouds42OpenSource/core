﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.CloudsServicesSegment.Helpers;
using Clouds42.Segment.CloudsServicesSegment.Validators;
using Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces;
using Newtonsoft.Json;

namespace Clouds42.Segment.CloudsServicesSegment.Managers
{
    /// <summary>
    /// Менеджер для работы с сегментами
    /// </summary>
    public class CloudSegmentReferenceManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        CloudServicesSegmentValidator cloudServicesSegmentValidator,
        IDeleteCloudSegmentProvider deleteCloudSegmentProvider,
        IEditCloudSegmentProvider editCloudSegmentProvider,
        ICreateCloudSegmentProvider createCloudSegmentProvider,
        LogSegmentEventHelper logSegmentEventHelper,
        CloudSegmentDataHelper cloudSegmentDataHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить доступные сегменты для миграции
        /// включая текущий сегмент
        /// </summary>
        /// <param name="segmentId">Id сегмента</param>
        /// <returns>Доступные сегменты для миграции</returns>
        public ManagerResult<List<SegmentElementDto>> GetAvailableMigrationSegmentsWithCurrent(Guid segmentId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_View);

                var availableMigrations = DbLayer.AvailableMigrationRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.SegmentIdFrom == segmentId)
                    .Select(x => new SegmentElementDto { Id = x.SegmentIdTo, Name = x.SegmentTo.Name, })
                    .Distinct()
                    .ToList();

                var segment = DbLayer.CloudServicesSegmentRepository
                    .AsQueryableNoTracking()
                    .Select(x => new SegmentElementDto { Id = x.ID, Name = x.Name, })
                    .FirstOrDefault(x => x.Id == segmentId);


                availableMigrations.Add(segment!);
                
                return Ok(availableMigrations.Distinct().OrderBy(x => x.Name).ToList());

            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения доступных сегментов для миграции пользователем {AccessProvider.Name}.]");

                return PreconditionFailed<List<SegmentElementDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Добавить новый сегмент
        /// </summary>
        /// <param name="segmentDto">Сегмент</param>
        /// <returns>Результат добавления</returns>
        public ManagerResult AddNewSegment(CreateCloudServicesSegmentDto segmentDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewCloudServiceSegment,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = cloudServicesSegmentValidator.ValidateSegmentBeforeCreate(segmentDto);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError(validationMessage);

                createCloudSegmentProvider.Create(segmentDto);

                var message = $"Создан сегмент \"{segmentDto.Name}\"";
                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var model = segmentDto == null
                    ? "Входящая модель в контроллер была пуста."
                    : JsonConvert.SerializeObject(segmentDto);
                var name = segmentDto?.Name ?? "Отсутствует имя. Проверьте валидность переданных данных и их формат";
                var errorMessage = $"[Ошибка редактирования сегмента \"{name}\". ]" + model;
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить сегмент
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult DeleteSegment(Guid segmentId)
        {
            var segmentName = string.Empty;
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeleteCloudServiceSegment,
                    () => AccessProvider.ContextAccountId);

                var validationMessage = cloudServicesSegmentValidator.ValidateSegmentBeforeDelete(segmentId);
                if (!string.IsNullOrEmpty(validationMessage))
                    return ValidationError(validationMessage);

                var segment = cloudSegmentDataHelper.GetSegmentOrThrowException(segmentId);
                segmentName = segment.Name;

                deleteCloudSegmentProvider.Delete(segmentId);

                var message = $"Удален сегмент \"{segmentName}\"";

                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления сегмента \"{segmentName}\".]";
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Изменить сегмент
        /// </summary>
        /// <param name="servicesSegmentDto">Обновленный сегмент</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult EditSegment(EditCloudServicesSegmentDto servicesSegmentDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdateCloudServiceSegment,
                    () => AccessProvider.ContextAccountId);
                logger.Info("Начинаю редактирование сегмента");
                var validationMessage = cloudServicesSegmentValidator.ValidateSegmentBeforeEdit(servicesSegmentDto);
                if (!string.IsNullOrEmpty(validationMessage))
                {
                    logger.Info(validationMessage);
                    return ValidationError(validationMessage);
                }

                var segmentOld = cloudSegmentDataHelper.GetSegmentOrThrowException(servicesSegmentDto.Id);

                var message = logSegmentEventHelper.GenerateMessageSegmentChanges(servicesSegmentDto, segmentOld);

                editCloudSegmentProvider.Edit(servicesSegmentDto);

                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var model = servicesSegmentDto == null
                    ? "Входящая модель в контроллер была пуста."
                    : JsonConvert.SerializeObject(servicesSegmentDto);
                var name = servicesSegmentDto?.Name ?? "Отсутствует имя. Проверьте валидность переданных данных и их формат";
                var errorMessage = $"[Ошибка редактирования сегмента] \"{name}\". " + model;
                handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }


        /// <summary>
        /// Изменить сегмент
        /// </summary>
        /// <param name="stable83VersionId">Идентификатор платформы</param>
        /// <param name="segmentId">Идентификатор сегмента</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult EditPlatformVersion(string stable83VersionId, Guid segmentId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdateCloudServiceSegment,
                    () => AccessProvider.ContextAccountId);

                logger.Trace($"Начало изменения поля Предприятие 8.3 для сегмента {segmentId}");

                var segmentOld = cloudSegmentDataHelper.GetSegmentOrThrowException(segmentId);

                var message = $"Изменено поле Предприятие 8.3 для сегмента \"{segmentOld.Name}\"c \"{segmentOld.Stable83Version}\" на \"{stable83VersionId}\".\n"; ;

                editCloudSegmentProvider.EditPlatformVersion(stable83VersionId, segmentId);

                logger.Trace(message + $" пользователем {AccessProvider.Name}");

                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, ex.Message);
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, $"Ошибка при смене платформы для списка сегментов. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
