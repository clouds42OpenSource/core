﻿using System.Text;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces;

namespace Clouds42.Locales.Helpers
{
    /// <summary>
    /// Хэлпер для записи в логирование для локалей
    /// </summary>
    public class LocaleLogEventHelper(ICloudSegmentDataProvider cloudSegmentDataProvider)
    {
        /// <summary>
        /// Сгенерировать сообщение о редактировании локали
        /// </summary>
        /// <returns>Сообщение о редактировании локали</returns>
        public string GenerateEditLocaleMessage(LocaleItemDto localeData, EditLocaleDto model)
        {
            var messageBuilder = new StringBuilder();
            messageBuilder.AppendLine($"Локаль \"{localeData.Name} ({localeData.Country})\" отредактирована. ");

            if (!localeData.CpSiteUrl.Equals(model.CpSiteUrl))
                messageBuilder.AppendLine(
                    $"Изменен адрес сайта личного кабинета с \"{localeData.CpSiteUrl}\" на \"{model.CpSiteUrl}\".");

            if (!localeData.DefaultSegmentId.Equals(model.DefaultSegmentId))
                messageBuilder.AppendLine(
                    $"Изменен сегмент по умолчанию с \"{cloudSegmentDataProvider.GetSegmentName(localeData.DefaultSegmentId)}\" " +
                    $"на \"{cloudSegmentDataProvider.GetSegmentName(model.DefaultSegmentId)}\".");

            return messageBuilder.ToString();
        }
    }
}
