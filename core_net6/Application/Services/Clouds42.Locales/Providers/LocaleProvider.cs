﻿using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.LocaleContext;
using Core42.Application.Contracts.Features.LocaleContext.Filters;
using MediatR;
using LocaleDto = Core42.Application.Contracts.Features.LocaleContext.Dtos.LocaleDto;

namespace Clouds42.Locales.Providers
{
    /// <summary>
    /// Провайдер для работы с локалью
    /// </summary>
    internal class LocaleProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException, ISender sender)
        : ILocaleProvider
    {
        /// <summary>
        /// Получить данные локалей
        /// </summary>
        /// <returns>Данные локалей</returns>
        public List<LocaleDataDto> GetLocales() =>
            dbLayer.LocaleRepository.GetAll().ToList().Select(l =>
                new LocaleDataDto
                {
                    Id = l.ID,
                    Name = l.Name,
                    Country = l.Country
                }).ToList();

        /// <summary>
        /// Получить локаль
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Локаль</returns>
        public Locale GetLocale(string localeName) =>
            dbLayer.LocaleRepository.FirstOrThrowException(loc => loc.Name == localeName);

        /// <summary>
        /// Получить данные локалей по фильтру
        /// </summary>
        /// <param name="filterData">Данные фильтра</param>
        /// <returns>Данные локалей по фильтру</returns>
        public LocalesDataDto GetByFilter(LocalesFilterDto filterData)
        {
            const int itemsPerPage = GlobalSettingsConstants.ReferenceDataPages.DefaultDataGridRecordsCount;

            var querySorting = filterData.SortingData != null && !string.IsNullOrEmpty(filterData.SortingData.FieldName)
                ? $",{filterData.SortingData.FieldName}.{filterData.SortingData.SortKind.ToString()}" : string.Empty;

            var data = sender.Send(new GetFilteredLocalesQuery
            {
                Filter = new LocaleFilter { SearchLine = filterData.Filter.SearchString },
                OrderBy = $"{nameof(LocaleDto.Country)}.desc{querySorting}",
                PageNumber = filterData.PageNumber ?? 1,
                PageSize = itemsPerPage
            }).Result;

            return new LocalesDataDto
            {
                Records = data.Result.Records.Select(x => new LocaleItemDto{Country = x.Country, Id = x.Id, Name = x.Name, Currency = x.Currency, CpSiteUrl = x.CpSiteUrl, CurrencyCode = x.CurrencyCode, DefaultSegmentId = x.DefaultSegmentId}).ToArray(),
                Pagination = new PaginationBaseDto(filterData.PageNumber ?? 1, data.Result.Metadata.TotalItemCount, itemsPerPage)
            };
        }

        /// <summary>
        /// Получить данные локали по ID
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>Данные локали</returns>
        public LocaleItemDto GetById(Guid localeId)
            => (from locale in dbLayer.LocaleRepository.WhereLazy()
                join configuration in dbLayer.GetGenericRepository<LocaleConfiguration>().WhereLazy() on
                    locale.ID
                    equals configuration.LocaleId
                where locale.ID == localeId
                select new LocaleItemDto
                {
                    Id = locale.ID,
                    Name = locale.Name,
                    Country = locale.Country,
                    Currency = locale.Currency,
                    CurrencyCode = locale.CurrencyCode,
                    CpSiteUrl = configuration.CpSiteUrl,
                    DefaultSegmentId = configuration.DefaultSegmentId
                }
               ).FirstOrDefault() ?? throw new NotFoundException($"Данные локали по ID {localeId} не найдены");

        /// <summary>
        /// Изменить локаль
        /// </summary>
        /// <param name="model">Модель редактирования локали</param>
        public void Edit(EditLocaleDto model)
        {
            var localeConfiguration = dbLayer.GetGenericRepository<LocaleConfiguration>()
                .FirstOrThrowException(conf => conf.LocaleId == model.Id,
                    $"Конфигурация локали по ID {model.Id} не найдена");

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                localeConfiguration.DefaultSegmentId = model.DefaultSegmentId;
                localeConfiguration.CpSiteUrl = model.CpSiteUrl;
                localeConfiguration.ChangeDate = DateTime.Now;

                dbLayer.GetGenericRepository<LocaleConfiguration>().Update(localeConfiguration);
                dbLayer.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка редактирования локали] {localeConfiguration.LocaleId}");
                transaction.Rollback();
                throw;
            }
        }
    }
}
