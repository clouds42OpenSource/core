﻿using System.Collections.Concurrent;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Locales.Providers
{
    public interface ILocalesConfigurationProvider
    {
        /// <summary>
        /// Получить конфигурацию локали или выкинуть исключение
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>Конфигурация локали</returns>
        LocaleConfigurationDto GetLocaleConfigurationOrThrowException(Guid localeId);

        /// <summary>
        /// Определить конфигурацию локали по названию хоста сайта личного кабинета
        /// </summary>
        /// <param name="hostName">Название хоста сайта личного кабинета</param>
        /// <returns>Конфигурация локали</returns>
        LocaleConfigurationDto DefineLocaleConfigurationByCpSiteHostName(string hostName);
    }

    /// <summary>
    /// Провайдер конфигураций локалей
    /// </summary>
    public class LocalesConfigurationProvider : ILocalesConfigurationProvider
    {
        private readonly IUnitOfWork _unitOfWork;
        public LocalesConfigurationProvider(IUnitOfWork unitOfWork)
        {
            _unitOfWork= unitOfWork;
            InitializeLocalesConfigurationsDictionary();
            CheckUpdatesDate = DateTime.Now;
        }

        /// <summary>
        /// Lazy значение интервала проверки обновлений в минутах
        /// </summary>
        private readonly Lazy<int> _checkUpdatesIntervalInMinutesLazy =
            new(CloudConfigurationProvider.LocaleConfiguration.GetCheckUpdatesIntervalInMinutes);

        /// <summary>
        /// Значение интервала проверки обновлений в минутах
        /// </summary>
        private int CheckUpdatesIntervalInMinutes => _checkUpdatesIntervalInMinutesLazy.Value;

        /// <summary>
        /// Словарь/справочник конфигураций локалей
        /// </summary>
        private ConcurrentDictionary<Guid, LocaleConfigurationDto> LocalesConfigurationsDictionary { get; set; }

        /// <summary>
        /// Дата проверки обновлений
        /// </summary>
        private DateTime CheckUpdatesDate { get; set; }

        /// <summary>
        /// Получить конфигурацию локали или выкинуть исключение
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>Конфигурация локали</returns>
        public LocaleConfigurationDto GetLocaleConfigurationOrThrowException(Guid localeId) =>
            ExecuteGettingActualLocaleConfiguration(() =>
                LocalesConfigurationsDictionary.TryGetValue(localeId, out var localeConfiguration)
                    ? localeConfiguration
                    : throw new NotFoundException($"Не удалось получить конфигурацию локали по ID {localeId}"));

        /// <summary>
        /// Определить конфигурацию локали по названию хоста сайта личного кабинета
        /// </summary>
        /// <param name="hostName">Название хоста сайта личного кабинета</param>
        /// <returns>Конфигурация локали</returns>
        public LocaleConfigurationDto DefineLocaleConfigurationByCpSiteHostName(string hostName) =>
            ExecuteGettingActualLocaleConfiguration(() => LocalesConfigurationsDictionary.Values.FirstOrDefault(
                localesConfiguration =>
                    new Uri(localesConfiguration.CpSiteUrl).Host.ToLower() == hostName.ToLower()));

        /// <summary>
        /// Выполнить получение актуальной конфигурации локали
        /// </summary>
        /// <param name="getLocaleConfigurationFunc">Функция для получения конфигурации локали</param>
        /// <returns>Актуальная конфигурация локали</returns>
        private LocaleConfigurationDto ExecuteGettingActualLocaleConfiguration(
            Func<LocaleConfigurationDto> getLocaleConfigurationFunc)
        {
            RefreshLocalesConfigurationsList();
            return getLocaleConfigurationFunc();
        }

        /// <summary>
        /// Инициализировать cловарь/справочник конфигураций локали
        /// </summary>
        private void InitializeLocalesConfigurationsDictionary() => LocalesConfigurationsDictionary =
            new ConcurrentDictionary<Guid, LocaleConfigurationDto>(GetLocalesConfigurationsList()
                .Select(localesConfiguration =>
                    new KeyValuePair<Guid, LocaleConfigurationDto>(localesConfiguration.LocaleId,
                        localesConfiguration)));

        /// <summary>
        /// Перезаписать данные cловаря/справочника конфигураций локали
        /// </summary>
        private void RefreshLocalesConfigurationsList()
        {
            if (DateTime.Now.AddMinutes(-CheckUpdatesIntervalInMinutes) < CheckUpdatesDate)
                return;

            GetLocalesConfigurationsList().ForEach(RefreshLocaleConfiguration);
            CheckUpdatesDate = DateTime.Now;
        }

        /// <summary>
        /// Перезаписать данные конфигурации локали в словаре конфигураций локали
        /// </summary>
        /// <param name="localeConfiguration">Конфигурация локали</param>
        private void RefreshLocaleConfiguration(LocaleConfigurationDto localeConfiguration) =>
            LocalesConfigurationsDictionary.AddOrUpdate(localeConfiguration.LocaleId,
                localeConfiguration, (localeId, oldLocalesConfiguration) =>
                {
                    oldLocalesConfiguration.CpSiteUrl = localeConfiguration.CpSiteUrl;
                    oldLocalesConfiguration.DefaultSegmentId = localeConfiguration.DefaultSegmentId;
                    return oldLocalesConfiguration;
                });

        /// <summary>
        /// Получить список конфигураций локали
        /// </summary>
        /// <returns>Список конфигураций локали</returns>
        private List<LocaleConfigurationDto> GetLocalesConfigurationsList()
        {
            return
            (
                from locale in _unitOfWork.LocaleRepository.WhereLazy()
                join configuration in _unitOfWork.GetGenericRepository<LocaleConfiguration>().WhereLazy() on
                    locale.ID equals configuration.LocaleId
                select new LocaleConfigurationDto
                {
                    LocaleId = locale.ID,
                    LocaleName = locale.Name,
                    CpSiteUrl = configuration.CpSiteUrl,
                    DefaultSegmentId = configuration.DefaultSegmentId
                }
            ).ToList();
        }
    }
}
