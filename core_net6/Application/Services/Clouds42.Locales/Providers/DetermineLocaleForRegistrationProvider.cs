﻿using Clouds42.Common.Constants;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;

namespace Clouds42.Locales.Providers
{
    /// <summary>
    /// Провайдер определения локали для регистрации аккаунта
    /// </summary>
    internal class DetermineLocaleForRegistrationProvider(
        ILocaleProvider localeProvider,
        ILocalesConfigurationProvider localesConfigurationProvider,
        ILogger42 logger) : IDetermineLocaleForRegistrationProvider
    {
        private readonly Lazy<List<LocaleDataDto>> _allLocalesLazyList = new Lazy<List<LocaleDataDto>>(localeProvider.GetLocales);

        /// <summary>
        /// Определить название локали
        /// для регистрации аккаунта
        /// </summary>
        /// <param name="model">Модель данных для определения локали,
        /// для регистрации аккаунта</param>
        /// <returns>Название локали</returns>
        public string? DetermineLocaleName(DetermineLocaleNameForRegistrationDto model)
        {
            if (TryGetLocaleNameBySearchString(model.LocaleNameFromParameters, out var localeName))
            {
                logger.Debug($"Локаль указана в модели. Возвращаем название локали {localeName}");
                return localeName;
            }

            if (TryGetLocaleNameBySearchString(model.LocaleNameFromReferralLink, out localeName))
            {
                logger.Debug(
                    $"Локаль не указана в модели, но есть в реферальной ссылке, возвращаем локаль {localeName}");
                return localeName;
            }

            if (TryGetLocaleNameByCpSiteHostName(model.CpSiteHostName, out localeName))
            {
                logger.Debug(
                    $"Локаль не указана в модели, но определена по названию домена, возвращаем локаль {localeName}");
                return localeName;
            }

            logger.Debug($"Локаль не указана в модели выставляем локаль по умолчанию {LocaleConst.Russia}");
            return FindLocaleByName(LocaleConst.Russia)?.Name;
        }

        /// <summary>
        /// Попытаться получить название локали
        /// по названию хоста сайта личного кабинета
        /// </summary>
        /// <param name="hostName">Название хоста сайта личного кабинета</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Результат получения названия локали
        /// по названию хоста сайта личного кабинета</returns>
        private bool TryGetLocaleNameByCpSiteHostName(string hostName, out string? localeName)
        {
            localeName = null;

            if (hostName.IsNullOrEmpty())
                return false;

            localeName = localesConfigurationProvider.DefineLocaleConfigurationByCpSiteHostName(hostName)?.LocaleName;

            return !string.IsNullOrWhiteSpace(localeName);
        }

        /// <summary>
        /// Попытаться получить название локали по строке поиска
        /// </summary>
        /// <param name="searchString">Строка поиска</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Результат получения названия локали по строке поиска</returns>
        private bool TryGetLocaleNameBySearchString(string searchString, out string? localeName)
        {
            localeName = null;

            if (string.IsNullOrWhiteSpace(searchString))
                return false;

            localeName = FindLocaleByName(searchString)?.Name;

            return !string.IsNullOrWhiteSpace(localeName);
        }

        /// <summary>
        /// Найти локаль по имени
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Найденная локаль</returns>
        private LocaleDataDto? FindLocaleByName(string localeName) =>
            AllLocales.FirstOrDefault(locale => locale.Name == localeName);

        /// <summary>
        /// Все локали облака
        /// </summary>
        private List<LocaleDataDto> AllLocales => _allLocalesLazyList.Value;
    }
}
