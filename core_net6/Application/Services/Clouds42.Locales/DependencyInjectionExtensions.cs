﻿using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Locales.Helpers;
using Clouds42.Locales.Managers;
using Clouds42.Locales.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Locales
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddLocale(this IServiceCollection services)
        {
            services.AddTransient<ILocaleProvider, LocaleProvider>();
            services.AddScoped<ILocalesConfigurationProvider, LocalesConfigurationProvider>();
            services.AddTransient<IDetermineLocaleForRegistrationProvider, DetermineLocaleForRegistrationProvider>();
            services.AddTransient<ILocalesConfigurationDataProvider, LocalesConfigurationDataProvider>();
            services.AddTransient<LocaleManager>();
            services.AddTransient<LocaleLogEventHelper>();

            return services;
        }
    }
}
