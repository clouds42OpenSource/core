﻿using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Domain.Enums;

namespace Clouds42.Locales
{
    /// <summary>
    /// Локализатор названия сервиса
    /// </summary>
    public static class ServiceNameLocalizer
    {
        /// <summary>
        /// Локализировать название сервиса для аккаунта
        /// </summary>
        /// <param name="cloudLocalizer"></param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="systemService">Тип системного сервиса</param>
        /// <returns>Локализированное название сервиса для аккаунта</returns>
        public static string LocalizeForAccount(ICloudLocalizer cloudLocalizer, Guid accountId, string serviceName, Clouds42Service? systemService)
        {
            if (!systemService.HasValue || systemService != Clouds42Service.MyDatabases && systemService != Clouds42Service.MyEnterprise)
                return serviceName;

            return cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId);
        }
    }
}
