﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Locales.Helpers;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Locales.Managers
{
    /// <summary>
    /// Менеджер для работы с локалью
    /// </summary>
    public class LocaleManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ILocaleProvider localeProvider,
        IHandlerException handlerException,
        LocaleLogEventHelper localeLogEventHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить данные локалей
        /// </summary>
        /// <returns>Данные локалей</returns>
        public ManagerResult<List<LocaleDataDto>> GetLocales()
        {
            logger.Info("Получение локалей аккаунта");
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUsers_GetLocale, () => AccessProvider.ContextAccountId);

                var data = localeProvider.GetLocales();
                return Ok(data);

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения локалей аккаунта]");
                return PreconditionFailed<List<LocaleDataDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные локалей по фильтру
        /// </summary>
        /// <param name="filter">Данные фильтра</param>
        /// <returns>Данные локалей по фильтру</returns>
        public ManagerResult<LocalesDataDto> GetByFilter(LocalesFilterDto filter)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Locales_Edit, () => AccessProvider.ContextAccountId);
                return Ok(localeProvider.GetByFilter(filter));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения локалей по фильтру]");
                return PreconditionFailed<LocalesDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные локали по ID
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>Данные локали</returns>
        public ManagerResult<LocaleItemDto> GetById(Guid localeId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Locales_Edit, () => AccessProvider.ContextAccountId);
                return Ok(localeProvider.GetById(localeId));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения данных локали по ID {localeId}]");
                return PreconditionFailed<LocaleItemDto>(ex.Message);
            }
        }

        /// <summary>
        /// Изменить локаль
        /// </summary>
        /// <param name="model">Модель редактирования локали</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult Edit(EditLocaleDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Locales_Edit, () => AccessProvider.ContextAccountId);
                var editLocaleMessage = localeLogEventHelper.GenerateEditLocaleMessage(localeProvider.GetById(model.Id), model);

                localeProvider.Edit(model);
                LogEvent(() => AccessProvider.GetUser().RequestAccountId, LogActions.EditLocale, editLocaleMessage);
                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования данных локали {model.Id}]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
