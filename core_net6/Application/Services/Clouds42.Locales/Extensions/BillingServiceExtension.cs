﻿using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Locales.Extensions
{
    /// <summary>
    /// Расширение сервиса биллинга
    /// </summary>
    public static class BillingServiceExtension
    {
        /// <summary>
        /// Получить название сервиса с учетом локализации
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="billingService">Сервис биллинга</param>
        /// <returns>Название сервиса с учетом локализации</returns>
        public static string GetNameBasedOnLocalization(this IBillingService billingService, ICloudLocalizer cloudLocalizer, Guid accountId) =>
            ServiceNameLocalizer.LocalizeForAccount(cloudLocalizer, accountId, billingService.Name, billingService.SystemService);
    }
}
