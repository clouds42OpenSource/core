﻿using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Locales.Extensions
{
    /// <summary>
    /// Расширение для работы с ресурс конфигурациями
    /// </summary>
    public static class ResourcesConfigurationExtension
    {
        /// <summary>
        /// Определить активность для аккаунта
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация</param>
        /// <returns>Статус сервиса (активность) для аккаунта</returns>
        public static BillingServiceStatusForAccountEnum DetectActivityForAccount(this
            ResourcesConfiguration resourcesConfiguration) =>
            resourcesConfiguration == null ||
            resourcesConfiguration.FrozenValue
                ? BillingServiceStatusForAccountEnum.NotActiveForAccount
                : BillingServiceStatusForAccountEnum.Activated;

        /// <summary>
        /// Получить локализованное название сервиса по конфигурации ресурсов
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурсов</param>
        /// <returns>Локализованное название сервиса</returns>
        public static string GetLocalizedServiceName(this ResourcesConfiguration resourcesConfiguration, ICloudLocalizer cloudLocalizer) =>
            resourcesConfiguration.BillingService.GetNameBasedOnLocalization(cloudLocalizer, resourcesConfiguration.AccountId);
    }
}
