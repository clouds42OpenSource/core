﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;

namespace Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces
{
    /// <summary>
    /// Провайдер для создания терминального шлюза
    /// </summary>
    public interface ICreateCloudGatewayTerminalProvider
    {
        /// <summary>
        /// Создать терминальный шлюз
        /// </summary>
        /// <param name="createCloudGatewayTerminalDto">Модель создания
        /// терминального шлюза</param>
        /// <returns>Id созданного терминального шлюза</returns>
        Guid Create(CreateCloudGatewayTerminalDto createCloudGatewayTerminalDto);
    }
}

