﻿namespace Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces
{
    /// <summary>
    /// Провайдер удаления терминального шлюза
    /// </summary>
    public interface IDeleteCloudGatewayTerminalProvider
    {
        /// <summary>
        /// Удалить терминальный шлюз
        /// </summary>
        /// <param name="gatewayTerminalId">Id терминального шлюза</param>
        void Delete(Guid gatewayTerminalId);
    }
}

