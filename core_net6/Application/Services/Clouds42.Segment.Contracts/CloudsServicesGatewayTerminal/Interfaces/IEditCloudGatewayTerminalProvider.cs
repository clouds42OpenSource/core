﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;

namespace Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces
{
    /// <summary>
    /// Провайдер для редактирования терминального шлюза
    /// </summary>
    public interface IEditCloudGatewayTerminalProvider
    {
        /// <summary>
        /// Редактировать терминальный шлюз
        /// </summary>
        /// <param name="cloudGatewayTerminalDto">Модель терминального шлюза</param>
        void Edit(CloudGatewayTerminalDto cloudGatewayTerminalDto);
    }
}

