﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.DataModels;

namespace Clouds42.Segment.Contracts.CloudsServicesGatewayTerminal.Interfaces
{
    /// <summary>
    /// Провайдер для работы с терминальным шлюзом
    /// </summary>
    public interface ICloudGatewayTerminalProvider
    {
        /// <summary>
        /// Получить терминальный шлюз
        /// </summary>
        /// <param name="gatewayTerminalId">Id терминального шлюза</param>
        /// <returns>Терминальный шлюз</returns>
        CloudServicesGatewayTerminal GetCloudGatewayTerminal(Guid gatewayTerminalId);

        /// <summary>
        /// Получить модель терминального шлюза
        /// </summary>
        /// <param name="gatewayTerminalId">Id терминального шлюза</param>
        /// <returns>Модель терминального шлюза</returns>
        CloudGatewayTerminalDto GetCloudGatewayTerminalDto(Guid gatewayTerminalId);
    }
}

