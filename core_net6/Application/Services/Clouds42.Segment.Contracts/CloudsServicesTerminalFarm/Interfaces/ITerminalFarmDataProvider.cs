﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;

namespace Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces
{
    /// <summary>
    /// Провайдер для работы с фермами ТС
    /// </summary>
    public interface ITerminalFarmDataProvider
    {

        /// <summary>
        /// Получить ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        /// <returns>Ферма ТС</returns>
        CloudTerminalFarmDto GetTerminalFarm(Guid terminalFarmId);
    }
}

