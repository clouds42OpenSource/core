﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;

namespace Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces
{
    /// <summary>
    /// Провайдер создания ферм ТС
    /// </summary>
    public interface ICreateTerminalFarmProvider
    {
        /// <summary>
        /// Добавить новую ферму ТС
        /// </summary>
        /// <param name="terminalFarmDto">Ферма ТС</param>
        Guid Create(CloudTerminalFarmDto terminalFarmDto);
    }
}

