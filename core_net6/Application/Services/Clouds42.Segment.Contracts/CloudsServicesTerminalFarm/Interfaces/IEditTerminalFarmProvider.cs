﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;

namespace Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces
{
    /// <summary>
    /// Провайдер редактирования ферм ТС
    /// </summary>
    public interface IEditTerminalFarmProvider
    {
        /// <summary>
        /// Изменить ферму ТС
        /// </summary>
        /// <param name="terminalFarmDto">Обновленная ферма ТС</param>
        void Edit(CloudTerminalFarmDto terminalFarmDto);
    }
}

