﻿namespace Clouds42.Segment.Contracts.CloudsServicesTerminalFarm.Interfaces
{
    /// <summary>
    /// Провайдер удаления ферм ТС
    /// </summary>
    public interface IDeleteTerminalFarmProvider
    {
        /// <summary>
        /// Удалить ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">ID фермы ТС</param>
        void Delete(Guid terminalFarmId);
    }
}

