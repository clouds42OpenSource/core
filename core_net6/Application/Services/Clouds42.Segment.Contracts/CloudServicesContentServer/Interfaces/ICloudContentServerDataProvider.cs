﻿namespace Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными сервера публикации
    /// </summary>
    public interface ICloudContentServerDataProvider
    {
        /// <summary>
        /// Получить адреса нод сервера публикации
        /// </summary>
        /// <param name="contentServerId">ID сервера публикаций</param>
        /// <returns>Адреса нод сервера публикации</returns>
        IEnumerable<string> GetContentServerPublishNodeAddresses(Guid contentServerId);
    }
}

