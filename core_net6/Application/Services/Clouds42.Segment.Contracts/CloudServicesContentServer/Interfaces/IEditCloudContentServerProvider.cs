﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;

namespace Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces
{
    /// <summary>
    /// Провайдер для редактирования сервера публикации
    /// </summary>
    public interface IEditCloudContentServerProvider
    {
        /// <summary>
        /// Редактировать сервер публикации
        /// </summary>
        /// <param name="cloudContentServerDto">Модель сервера публикации</param>
        void Edit(CloudContentServerDto cloudContentServerDto);
    }
}

