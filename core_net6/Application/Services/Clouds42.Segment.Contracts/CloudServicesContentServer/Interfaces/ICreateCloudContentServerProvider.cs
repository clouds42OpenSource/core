﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;

namespace Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces
{
    /// <summary>
    /// Провайдер для создания сервера публикации
    /// </summary>
    public interface ICreateCloudContentServerProvider
    {
        /// <summary>
        /// Создать сервер публикации
        /// </summary>
        /// <param name="createCloudContentServerDto">Модель создания
        /// сервера публикации</param>
        /// <returns>Id созданного сервера публикации</returns>
        Guid Create(CreateCloudContentServerDto createCloudContentServerDto);
    }
}

