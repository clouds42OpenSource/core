﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Domain.DataModels;

namespace Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces
{
    /// <summary>
    /// Провайдер для работы с сервером публикации
    /// </summary>
    public interface ICloudContentServerProvider
    {
        /// <summary>
        /// Получить сервер публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <returns>Сервер публикации</returns>
        Domain.DataModels.CloudServicesContentServer GetCloudContentServer(Guid contentServerId);

        /// <summary>
        /// Получить модель сервера публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <returns>Модель сервера публикации</returns>
        CloudContentServerDto GetCloudContentServerDto(Guid contentServerId);

        /// <summary>
        /// Получить список связей нод публикации
        /// с сервером публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        /// <returns>Список связей</returns>
        List<CloudServicesContentServerNode> GetCloudServicesContentServerNodes(Guid contentServerId);

        /// <summary>
        /// Получить модели доступных нод публикации
        /// </summary>
        /// <returns>Доступные ноды публикации</returns>
        List<PublishNodeDto> GetAvailablePublishNodesDto();

        /// <summary>
        /// Получить свободные ноды публикации
        /// </summary>
        /// <returns>Свободные ноды публикации</returns>
        List<PublishNodeReference> GetAvailablePublishNodes();
    }
}

