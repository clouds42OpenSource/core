﻿namespace Clouds42.Segment.Contracts.CloudServicesContentServer.Interfaces
{
    /// <summary>
    /// Провайдер удаления сервера публикации
    /// </summary>
    public interface IDeleteCloudContentServerProvider
    {
        /// <summary>
        /// Удалить сервер публикации
        /// </summary>
        /// <param name="contentServerId">Id сервера публикации</param>
        void Delete(Guid contentServerId);
    }
}

