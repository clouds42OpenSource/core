﻿namespace Clouds42.Segment.Contracts.PublishNode.Interfaces
{
    /// <summary>
    /// Провайдер удаления нод публикаций
    /// </summary>
    public interface IDeletePublishNodeProvider
    {
        /// <summary>
        /// Удалить ноду публикаций
        /// </summary>
        /// <param name="publishNodeId">ID ноды публикаций</param>
        void Delete(Guid publishNodeId);
    }
}

