﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;

namespace Clouds42.Segment.Contracts.PublishNode.Interfaces
{
    /// <summary>
    /// Провайдер редактирования нод публикаций
    /// </summary>
    public interface IEditPublishNodeProvider
    {
        /// <summary>
        /// Изменить ноду публикаций
        /// </summary>
        /// <param name="publishNodeDto">Обновленная нода публикаций.</param>
        void Edit(PublishNodeDto publishNodeDto);
    }
}

