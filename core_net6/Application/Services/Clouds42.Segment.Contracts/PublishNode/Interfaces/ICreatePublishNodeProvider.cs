﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;

namespace Clouds42.Segment.Contracts.PublishNode.Interfaces
{
    /// <summary>
    /// Провайдер создания нод публикаций
    /// </summary>
    public interface ICreatePublishNodeProvider
    {
        /// <summary>
        /// Добавить новую ноду публикаций
        /// </summary>
        /// <param name="publishNodeDto">Нода публкаций</param>
        Guid Create(PublishNodeDto publishNodeDto);
    }
}

