﻿namespace Clouds42.Segment.Contracts
{
    public interface ICreateMigrationAccountDatabaseTaskHelper
    {
        void CreateTask(Guid accountDatabaseId, Guid targetFileStorageId);
        void CreateTask(List<Guid> accountDatabaseIds, Guid targetFileStorageId);
    }
}