﻿namespace Clouds42.Segment.Contracts.DatabaseFilesOnFileStorageServer.Interfaces
{
    /// <summary>
    /// Провайдер для закрытия открытых файлов инф. базы на файловом хранилище
    /// </summary>
    public interface ICloseOpenDatabaseFilesOnFileStorageProvider
    {
        /// <summary>
        /// Закрыть открытые файлы инф. базы на файловом хранилище
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        void Close(Guid databaseId);
    }
}

