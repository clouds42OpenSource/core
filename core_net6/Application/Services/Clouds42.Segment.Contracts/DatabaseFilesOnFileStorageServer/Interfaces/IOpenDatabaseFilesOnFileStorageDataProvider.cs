﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.Segment.Contracts.DatabaseFilesOnFileStorageServer.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными открытых файлов
    /// инф. базы на файловом хранилище
    /// </summary>
    public interface IOpenDatabaseFilesOnFileStorageDataProvider
    {
        /// <summary>
        /// Получить данные для управления открытыми файлами
        /// инф. базы на файловом хранилище
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Данные для управления открытыми файлами
        /// инф. базы на файловом хранилище</returns>
        ManageOpenDatabaseFilesOnFileStorageDataDto GetDataForManageOpenDatabaseFilesOnFileStorage(Guid databaseId);
    }
}

