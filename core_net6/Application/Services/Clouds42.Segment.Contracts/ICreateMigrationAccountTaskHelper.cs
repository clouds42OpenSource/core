﻿using Clouds42.DataContracts.Segment;

namespace Clouds42.Segment.Contracts
{
    public interface ICreateMigrationAccountTaskHelper
    {
        void CreateTask(SegmentMigrationParamsDto segmentMigrationParams);
    }
}
