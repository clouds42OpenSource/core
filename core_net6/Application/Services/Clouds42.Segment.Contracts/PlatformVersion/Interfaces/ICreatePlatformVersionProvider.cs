﻿using Clouds42.DataContracts.Configurations1c.PlatformVersion;

namespace Clouds42.Segment.Contracts.PlatformVersion.Interfaces
{
    /// <summary>
    /// Провайдер для создания версии платформы
    /// </summary>
    public interface ICreatePlatformVersionProvider
    {
        /// <summary>
        /// Создать версию платформы
        /// </summary>
        /// <param name="platformVersion">Модель версии платформы</param>
        void Create(PlatformVersion1CDto platformVersion);
    }
}
