﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.Segment.Contracts.PlatformVersion.Interfaces
{
    /// <summary>
    /// Провайдер для работы с версией платформы
    /// </summary>
    public interface IPlatformVersionProvider
    {
        /// <summary>
        /// Получить версию платформы
        /// </summary>
        /// <param name="version">Версия</param>
        /// <returns>Версия платформы</returns>
        IPlatformVersionReference GetPlatformVersion(string version);
    }
}

