﻿using Clouds42.DataContracts.Configurations1c.PlatformVersion;

namespace Clouds42.Segment.Contracts.PlatformVersion.Interfaces
{
    /// <summary>
    /// Провайдер для редактирования версии платформы
    /// </summary>
    public interface IEditPlatformVersionProvider
    {
        /// <summary>
        /// Редактировать версию платформы
        /// </summary>
        /// <param name="platformVersion"></param>
        void Edit(PlatformVersion1CDto platformVersion);
    }
}

