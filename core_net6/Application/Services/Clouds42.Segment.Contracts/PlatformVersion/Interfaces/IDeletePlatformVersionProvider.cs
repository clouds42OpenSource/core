﻿namespace Clouds42.Segment.Contracts.PlatformVersion.Interfaces
{
    /// <summary>
    /// Провайдер для удаления версии платформы
    /// </summary>
    public interface IDeletePlatformVersionProvider
    {
        /// <summary>
        /// Удалить версию платформы
        /// </summary>
        /// <param name="version">Версия платформы</param>
        void Delete(string version);
    }
}

