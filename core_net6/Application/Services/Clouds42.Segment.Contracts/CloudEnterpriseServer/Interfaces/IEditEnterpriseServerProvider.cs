﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;

namespace Clouds42.Segment.Contracts.CloudEnterpriseServer.Interfaces
{
    /// <summary>
    /// Провайдер редактирования серверов 1С:Предприятие
    /// </summary>
    public interface IEditEnterpriseServerProvider
    {
        /// <summary>
        /// Изменить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerDto">Обновленный сервер 1С:Предприятие.</param>
        void Edit(EnterpriseServerDto enterpriseServerDto);
    }
}

