﻿namespace Clouds42.Segment.Contracts.CloudEnterpriseServer.Interfaces
{
    /// <summary>
    /// Провайдер удаления серверов 1С:Предприятие
    /// </summary>
    public interface IDeleteEnterpriseServerProvider
    {
        /// <summary>
        /// Удалить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера 1С:Предприятие</param>
        void Delete(Guid enterpriseServerId);
    }
}

