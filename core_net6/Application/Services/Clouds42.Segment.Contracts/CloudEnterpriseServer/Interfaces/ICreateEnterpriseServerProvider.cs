﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;

namespace Clouds42.Segment.Contracts.CloudEnterpriseServer.Interfaces
{
    /// <summary>
    /// Провайдер создания серверов 1С:Предприятие
    /// </summary>
    public interface ICreateEnterpriseServerProvider
    {
        /// <summary>
        /// Добавить новый сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerDto">Сервер 1С:Предприятие</param>
        Guid Create(EnterpriseServerDto enterpriseServerDto);
    }
}

