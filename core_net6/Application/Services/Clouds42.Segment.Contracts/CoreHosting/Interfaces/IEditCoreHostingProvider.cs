﻿using Clouds42.DataContracts.Service.CoreHosting;

namespace Clouds42.Segment.Contracts.CoreHosting.Interfaces
{
    /// <summary>
    /// Провайдер для редактирования хостинга облака
    /// </summary>
    public interface IEditCoreHostingProvider
    {
        /// <summary>
        /// Изменить хостинг
        /// </summary>
        /// <param name="coreHostingDc">Модель контракта хостинга</param>
        void EditCoreHosting(CoreHostingDto coreHostingDc);
    }
}
