﻿using Clouds42.DataContracts.Service.CoreHosting;

namespace Clouds42.Segment.Contracts.CoreHosting.Interfaces
{
    /// <summary>
    /// Провайдер для хостинга облака
    /// </summary>
    public interface ICoreHostingProvider
    {
        /// <summary>
        /// Получить модель хостинга облака
        /// </summary>
        /// <param name="coreHostingId">ID хостинга облака</param>
        /// <returns>Модель хостинга облака</returns>
        CoreHostingDto GetCoreHostingById(Guid coreHostingId);

        /// <summary>
        /// Удалить хостинг
        /// </summary>
        /// <param name="coreHostingId">ID хостинга</param>
        void DeleteCoreHosting(Guid coreHostingId);
    }
}
