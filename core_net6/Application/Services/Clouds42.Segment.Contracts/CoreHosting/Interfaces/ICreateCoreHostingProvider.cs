﻿using Clouds42.DataContracts.Service.CoreHosting;

namespace Clouds42.Segment.Contracts.CoreHosting.Interfaces
{
    /// <summary>
    /// Провайдер создания хостинга облака
    /// </summary>
    public interface ICreateCoreHostingProvider
    {
        /// <summary>
        /// Создать хостинг
        /// </summary>
        /// <param name="coreHostingDc">Модель контракта хостинга</param>
        void CreateCoreHosting(CoreHostingDto coreHostingDc);
    }
}
