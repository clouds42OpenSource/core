﻿using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;

namespace Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces
{
    /// <summary>
    /// Провайдер для создания файлового хранилища
    /// </summary>
    public interface ICreateCloudFileStorageServerProvider
    {
        /// <summary>
        /// Создать файловое хранилище
        /// </summary>
        /// <param name="createCloudFileStorageServerDto">Модель создания
        /// файлового хранилища</param>
        /// <returns>Id созданного файлового хранилища</returns>
        Guid Create(CreateCloudFileStorageServerDto createCloudFileStorageServerDto);
    }
}

