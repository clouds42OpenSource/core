﻿namespace Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces
{
    /// <summary>
    /// Провайдер удаления файлового хранилища
    /// </summary>
    public interface IDeleteCloudFileStorageServerProvider
    {
        /// <summary>
        /// Удалить файловое хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        void Delete(Guid fileStorageId);
    }
}

