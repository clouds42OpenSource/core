﻿using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Domain.DataModels;

namespace Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces
{
    /// <summary>
    /// Провайдер для работы с файловым хранилищем
    /// </summary>
    public interface ICloudFileStorageServerProvider
    {
        /// <summary>
        /// Получить файловое хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Файловое хранилище</returns>
        Domain.DataModels.CloudServicesFileStorageServer GetCloudFileStorageServer(Guid fileStorageId);

        /// <summary>
        /// Получить модель файлового хранилища
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Модель файлового хранилища</returns>
        CloudFileStorageServerDto GetCloudFileStorageServerDto(Guid fileStorageId);

        /// <summary>
        /// Получить список аккаунтов
        /// которые используют хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Список аккаунтов
        /// которые используют хранилище</returns>
        List<Account> GetAccountsThatUseStorage(Guid fileStorageId);

        /// <summary>
        /// Получить список информационных баз
        /// которые используют хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Список баз
        /// которые используют хранилище</returns>
        List<AccountDatabase> GetAccountDatabasesThatUseStorage(Guid fileStorageId);

        /// <summary>
        /// Получить список сегментов
        /// которые используют хранилище
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Список сегментов
        /// которые используют хранилище</returns>
        List<CloudServicesSegmentStorage> GetSegmentsThatUseStorage(Guid fileStorageId);

        /// <summary>
        /// Получить истории миграций инф. баз
        /// для хранилища
        /// </summary>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        /// <returns>Истории миграций инф. баз
        /// для хранилища</returns>
        List<MigrationAccountDatabaseHistory> GetMigrationDbHistoriesForStorage(Guid fileStorageId);

        /// <summary>
        /// Получить ID файлового хранилища по умолчанию
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>ID файлового хранилища по умолчанию</returns>
        Guid GetDefaultFileStorageId(Guid localeId);

        /// <summary>
        /// Получить ID файлового хранилища по умолчанию
        /// </summary>
        /// <returns>ID файлового хранилища по умолчанию</returns>
        Task<Guid> GetDefaultFileStorageIdAsync();

        /// <summary>
        /// Получить путь файлового хранилища
        /// </summary>
        /// <param name="fileStorageServerId">ID файлового хранилища</param>
        /// <returns>Путь файлового хранилища</returns>
        Task<string> GetFileStoragePathAsync(Guid fileStorageServerId);

        /// <summary>
        /// Получить данные для маунта файлового хранилища
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <returns>Данные для маунта файлового хранилища</returns>
        CloudFileStorageServerInfoDto GetFileStorageMountInfo(string userName);
    }
}

