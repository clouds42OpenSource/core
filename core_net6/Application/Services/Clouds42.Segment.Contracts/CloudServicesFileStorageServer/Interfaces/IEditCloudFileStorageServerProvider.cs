﻿using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;

namespace Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces
{
    /// <summary>
    /// Провайдер для редактирования файлового хранилища
    /// </summary>
    public interface IEditCloudFileStorageServerProvider
    {
        /// <summary>
        /// Редактировать файловое хранилище
        /// </summary>
        /// <param name="cloudFileStorageServerDto">Модель файлового хранилища</param>
        void Edit(CloudFileStorageServerDto cloudFileStorageServerDto);
    }
}

