﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;

namespace Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces
{
    /// <summary>
    /// Провайдер для редактирования сегментов
    /// </summary>
    public interface IEditCloudSegmentProvider
    {
        /// <summary>
        /// Изменить сегмент
        /// </summary>
        /// <param name="segmentDto">Обновленная модель сегмента</param>
        void Edit(EditCloudServicesSegmentDto segmentDto);

        /// <summary>
        /// Изменить платформу сегмента
        /// </summary>
        /// <param name="stable83VersionId">Идентификатор платвормы
        /// <param name="segmentId">Идентификатор сегмента</param>
        void EditPlatformVersion(string stable83VersionId, Guid segmentId);
    }
}

