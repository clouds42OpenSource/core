﻿namespace Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces
{
    /// <summary>
    /// Хелпер проверки доступности ИИС
    /// </summary>
    public interface ICheckIisAvailabilityHelper
    {
        /// <summary>
        /// Проверить доступность сервера ARR
        /// </summary>
        /// <param name="address"></param>
        /// <returns>Доступность ноды</returns>
        bool CheckIisAvailability(string address);
    }
}
