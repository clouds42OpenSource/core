﻿namespace Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces
{
    /// <summary>
    /// Провадер для работы с данными сегмента
    /// </summary>
    public interface ICloudSegmentDataProvider
    {
        /// <summary>
        /// Получить название сегмента
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Название сегмента</returns>
        string GetSegmentName(Guid segmentId);
    }
}

