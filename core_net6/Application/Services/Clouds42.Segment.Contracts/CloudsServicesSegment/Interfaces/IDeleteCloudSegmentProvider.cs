﻿namespace Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces
{
    /// <summary>
    /// Провайдер удаления сегментов
    /// </summary>
    public interface IDeleteCloudSegmentProvider
    {
        /// <summary>
        /// Удалить сегмент
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        void Delete(Guid segmentId);
    }
}

