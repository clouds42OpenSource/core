﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;

namespace Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces
{
    /// <summary>
    /// Провайдер для создания сегментов
    /// </summary>
    public interface ICreateCloudSegmentProvider
    {
        /// <summary>
        /// Создать сегмент
        /// </summary>
        /// <param name="segmentDto">Сегмент</param>
        void Create(CreateCloudServicesSegmentDto segmentDto);
    }
}

