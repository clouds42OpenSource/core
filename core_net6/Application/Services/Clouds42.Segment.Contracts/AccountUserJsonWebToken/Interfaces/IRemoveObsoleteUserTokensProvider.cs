﻿namespace Clouds42.Segment.Contracts.AccountUserJsonWebToken.Interfaces
{
    /// <summary>
    /// Провайдер для удаления устаревших json токенов пользователей 
    /// </summary>
    public interface IRemoveObsoleteUserTokensProvider
    {
        /// <summary>
        /// Удалить устаревшие токены
        /// пользователей
        /// </summary>
        void Remove();
    }
}

