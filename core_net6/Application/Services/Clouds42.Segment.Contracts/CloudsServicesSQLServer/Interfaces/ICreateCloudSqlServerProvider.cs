﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;

namespace Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces
{
    /// <summary>
    /// Провайдер для создания Sql сервера
    /// </summary>
    public interface ICreateCloudSqlServerProvider
    {
        /// <summary>
        /// Создать sql сервер
        /// </summary>
        /// <param name="createCloudSqlServerDto">Модель создания Sql сервера</param>
        /// <returns>Id созданного sql сервера</returns>
        Guid Create(CreateCloudSqlServerDto createCloudSqlServerDto);
    }
}

