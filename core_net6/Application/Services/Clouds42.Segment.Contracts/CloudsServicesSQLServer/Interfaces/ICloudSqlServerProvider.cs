﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Domain.DataModels;

namespace Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces
{
    /// <summary>
    /// Провайдер для работы с sql сервером
    /// </summary>
    public interface ICloudSqlServerProvider
    {
        /// <summary>
        /// Получить sql сервер
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        /// <returns>Sql сервер</returns>
        CloudServicesSqlServer GetCloudSqlServer(Guid sqlServerId);

        /// <summary>
        /// Получить модель sql сервера
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        /// <returns>Модель sql сервера</returns>
        CloudSqlServerDto GetCloudSqlServerDto(Guid sqlServerId);
    }
}

