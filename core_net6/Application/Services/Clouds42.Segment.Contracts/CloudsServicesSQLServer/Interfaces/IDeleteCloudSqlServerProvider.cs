﻿namespace Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces
{
    /// <summary>
    /// Провайдер удаления sql сервера
    /// </summary>
    public interface IDeleteCloudSqlServerProvider
    {
        /// <summary>
        /// Удалить sql сервер
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        void Delete(Guid sqlServerId);
    }
}
