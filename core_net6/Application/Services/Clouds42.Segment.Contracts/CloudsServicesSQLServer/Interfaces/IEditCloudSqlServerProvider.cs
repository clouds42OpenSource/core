﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;

namespace Clouds42.Segment.Contracts.CloudsServicesSQLServer.Interfaces
{
    /// <summary>
    /// Провайдер для редактирования sql сервера
    /// </summary>
    public interface IEditCloudSqlServerProvider
    {
        /// <summary>
        /// Редактировать sql сервер
        /// </summary>
        /// <param name="cloudSqlServerDto">Модель Sql сервера</param>
        void Edit(CloudSqlServerDto cloudSqlServerDto);
    }
}

