﻿using Clouds42.Domain.DataModels;

namespace Clouds42.Segment.Contracts
{
    public interface ISegmentHistoryProvider
    {
        /// <summary>
        /// Заргегистрировать историю миграции аккаунта.
        /// </summary>        
        void RegisterMigrationHistory(MigrationAccountHistory history);

        /// <summary>
        /// Создано новый объект истории миграции
        /// </summary>        
        MigrationHistory CreateMigrationHistoryObject(Guid accountUserInitiatorId, DateTime dateTimeBeforeMigration, bool res);

        /// <summary>
        /// Можно ли повторить поптыку миграции аккаунта.
        /// </summary>       
        bool CanTryMigrateAccountAgain(Guid accountId, Guid sourceSegmentId, Guid targetSegmentId);

        /// <summary>
        /// Получить кол-во миграция аккаунта за определенный промежуток.
        /// </summary>    
        int GetTriesAccountMigrationCountForPeriod(DateTime periodFrom, DateTime periodTo, Guid accountId,
            Guid sourceSegmentId, Guid targetSegmentId);

        /// <summary>
        /// Заргегистрировать историю миграции инфомационной базы.
        /// </summary>        
        void RegisterMigrationAccountDatabaseHistory(MigrationAccountDatabaseHistory history);

        /// <summary>
        /// Можно ли повторить поптыку миграции информационной базы.
        /// </summary>       
        bool CanTryMigrateAccountDatabaseAgain(Guid accountDatabaseId, Guid? sourceFileStorageId, Guid targetFileStorageId);

        /// <summary>
        /// Получить кол-во миграций инфо баз за определенный промежуток.
        /// </summary>    
        int GetTriesAccountDatabaseMigrationCountForPeriod(DateTime periodFrom, DateTime periodTo, Guid accountDatabaseId,
            Guid? sourceFileStorageId, Guid targetFileStorageId);
    }
}

