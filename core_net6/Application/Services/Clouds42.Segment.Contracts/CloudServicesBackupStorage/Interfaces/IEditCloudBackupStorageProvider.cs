﻿using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;

namespace Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces
{
    /// <summary>
    /// Провайдер для редактирования хранилища бэкапов
    /// </summary>
    public interface IEditCloudBackupStorageProvider
    {
        /// <summary>
        /// Редактировать хранилище бэкапов
        /// </summary>
        /// <param name="cloudBackupStorageDto">Модель хранилища бэкапов</param>
        void Edit(CloudBackupStorageDto cloudBackupStorageDto);
    }
}

