﻿using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;

namespace Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces
{
    /// <summary>
    /// Провайдер для работы с хранилищем бэкапов
    /// </summary>
    public interface ICloudBackupStorageProvider
    {
        /// <summary>
        /// Получить хранилище бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        /// <returns>Хранилище бэкапов</returns>
        Domain.DataModels.CloudServicesBackupStorage GetCloudBackupStorage(Guid backupStorageId);

        /// <summary>
        /// Получить модель хранилища бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        /// <returns>Модель хранилища бэкапов</returns>
        CloudBackupStorageDto GetCloudBackupStorageDto(Guid backupStorageId);
    }
}

