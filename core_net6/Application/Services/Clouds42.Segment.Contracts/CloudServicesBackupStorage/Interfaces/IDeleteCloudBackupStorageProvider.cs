﻿namespace Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces
{
    /// <summary>
    /// Провайдер удаления хранилища бэкапов
    /// </summary>
    public interface IDeleteCloudBackupStorageProvider
    {
        /// <summary>
        /// Удалить хранилище бэкапов
        /// </summary>
        /// <param name="backupStorageId">Id хранилища бэкапов</param>
        void Delete(Guid backupStorageId);
    }
}
