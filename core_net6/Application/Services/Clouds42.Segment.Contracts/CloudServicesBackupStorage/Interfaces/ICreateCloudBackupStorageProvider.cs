﻿using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;

namespace Clouds42.Segment.Contracts.CloudServicesBackupStorage.Interfaces
{
    /// <summary>
    /// Провайдер для создания хранилища бэкапов
    /// </summary>
    public interface ICreateCloudBackupStorageProvider
    {
        /// <summary>
        /// Создать хранилище бэкапов
        /// </summary>
        /// <param name="createCloudBackupStorageDto">Модель создания
        /// хранилища бэкапов</param>
        /// <returns>Id созданного хранилища бэкапов</returns>
        Guid Create(CreateCloudBackupStorageDto createCloudBackupStorageDto);
    }
}

