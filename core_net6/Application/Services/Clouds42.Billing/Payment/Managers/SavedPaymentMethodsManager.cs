﻿using Clouds42.Billing.Contracts.Payment;
using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Payment.Managers
{
    public class SavedPaymentMethodsManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ISavedPaymentMethodsDataProvider savedPaymentMethodsDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить сохраненные способы оплат
        /// </summary>
        /// <param name="accountId">Фильтр по аккаунту</param>
        public async Task<ManagerResult<List<SavedPaymentMethodDto>>> GetAllSavedPaymentMethodsAsync(Guid? accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_System, () => accountId);
                return Ok(await savedPaymentMethodsDataProvider.GetSavedPaymentMethodsAsync(accountId));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[При получении сохраненных спсособов оплаты возникла ошибка]");
                return PreconditionFailed<List<SavedPaymentMethodDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Удалить сохраненный способ оплаты
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Удачно/Неудачно</returns>
        public async Task<ManagerResult<bool>> DeleteSavedPaymentMethodAsync(Guid id)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_System);
                var result = await savedPaymentMethodsDataProvider.DeleteSavedPaymentMethodAsync(id);

                return !result ? PreconditionFailed<bool>("Не удалось удалить способ оплаты") : Ok(result);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,$"[При удалении сохраненного спсособа оплаты с Id: {id} возникла ошибка]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Установить значение по умолчанию для автосписаний
        /// </summary>
        /// <param name="model">Аккаунт & Сохраненный способ оплаты</param>
        /// <returns>Удачно/Неудачно</returns>
        public ManagerResult<bool> MakeDefault(AccountPaymentMethodDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_System);
                var result = savedPaymentMethodsDataProvider.MakeDefault(model);
                return !result ? PreconditionFailed<bool>("Не удалось установить значение по умолчанию для автосписаний") : Ok(result);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,$"[При установке значения по умолчанию для автосписаний] сохраненного спсособа оплаты с Id: {model.PaymentMethodId}" +
                                        $" и аккаунта с Id: {model.AccountId} возникла ошибка");

                return PreconditionFailed<bool>(ex.Message);
            }
        }
    }
}
