﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.Billing.Contracts.Payment;
using Clouds42.Billing.Factories.PreparePaymentModelFactory;
using Clouds42.Billing.Factories.PreparePaymentModelFactory.PaymentsModels;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Constants;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.Payment.Managers
{
    public class PaymentSystemDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IPayYookassaAggregatorPaymentProvider prepareYookassaAggregatorPaymentProvider,
        ICloudCoreDataProvider cloudCoreDataProvider,
        IConfiguration configuration,
        PreparePaymentModelFactory preparePaymentModelFactory)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Подготовка и получение данных для совершения платежа
        /// </summary>
        public async Task<ManagerResult<PaymentServiceAggreagtorDataResult>> GetPaymentSystemAggregatorDataAsync(PreparePaymentSystemDto model)
        {
            AccessProvider.HasAccess(ObjectAction.Payments_System, () => model.AccountId);

            var preparePayment = new PreparePaymentStruct { AccountId = model.AccountId, Sum = model.TotalSum };

            var locale = await DbLayer.AccountsRepository.GetLocaleAsync(model.AccountId, Guid.Parse(configuration["DefaultLocale"]?? ""));

            switch (locale.Name)
            {
                case LocaleConst.Ukraine:
                    return UkrPayModel(preparePayment);
                case LocaleConst.Kazakhstan:
                    return PayBoxModel(preparePayment);
                case LocaleConst.Uzbekistan:
                    return PayBoxModel(preparePayment);
                case LocaleConst.Russia:
                    return cloudCoreDataProvider.GetRussianLocalePaymentAggregator() != RussianLocalePaymentAggregatorEnum.Yookassa
                        ? RobokassaModel(preparePayment)
                        : YookassaModel(preparePayment, model);
                default:
                    break;
            }

            return PreconditionFailed<PaymentServiceAggreagtorDataResult>("Не удалось распознать локаль");
        }

        private ManagerResult<PaymentServiceAggreagtorDataResult> UkrPayModel(PreparePaymentStruct preparePayment)
        {
            preparePayment.PaymentSystem = PaymentSystem.UkrPays;
            var ukrPaysModel = "";

            return Ok(new PaymentServiceAggreagtorDataResult(preparePayment.PaymentSystem.ToString(), ukrPaysModel));
        }

        private ManagerResult<PaymentServiceAggreagtorDataResult> PayBoxModel(PreparePaymentStruct preparePayment)
        {
            preparePayment.PaymentSystem = PaymentSystem.Paybox;
            var payboxModel =
                preparePaymentModelFactory
                    .PreparePaymentModel<PreparePayboxModel, string>(preparePayment);
            return Ok(new PaymentServiceAggreagtorDataResult(preparePayment.PaymentSystem.ToString(), payboxModel));
        }

        private ManagerResult<PaymentServiceAggreagtorDataResult> RobokassaModel(PreparePaymentStruct preparePayment)
        {
            preparePayment.PaymentSystem = PaymentSystem.Robokassa;
            var robokassaModel = preparePaymentModelFactory
                .PreparePaymentModel<PrepareRobokassModel, string>(preparePayment);
            return Ok(new PaymentServiceAggreagtorDataResult(preparePayment.PaymentSystem.ToString(), robokassaModel));
        }

        private ManagerResult<PaymentServiceAggreagtorDataResult> YookassaModel(PreparePaymentStruct preparePayment, PreparePaymentSystemDto model)
        {
            preparePayment.PaymentSystem = PaymentSystem.Yookassa;
            var yookassaModel = new PrepareYookassaAggregatorPaymentDto
            {
                AccountId = model.AccountId,
                PaymentSum = model.TotalSum,
                ReturnUrl = model.ReturnUrl,
                PaymentMethodId = null,
                IsAutoPay = model.IsAutoPay
            };
            var yookassaResult = prepareYookassaAggregatorPaymentProvider.PreparePaymentOrPayWithSavedPaymentMethod(yookassaModel);
            return Ok(new PaymentServiceAggreagtorDataResult(yookassaResult.PaymentId, preparePayment.PaymentSystem.ToString(), yookassaResult));
        }


        public async Task<ManagerResult<PaymentConfirmationDto>> GetConfirmationInfo(Guid paymentId)
        {
            var payment = await DbLayer.PaymentRepository.FirstOrDefaultAsync(x => x.Id == paymentId);

            return Ok(new PaymentConfirmationDto 
                    { 
                        Status = payment?.StatusEnum, 
                        Token = payment?.ConfirmationToken, 
                        PaymentId = payment?.Id, 
                        RedirectUrl = payment?.ConfirmationRedirectUrl,
                        Type = payment?.ConfirmationType
                    });
        }
    }
}
