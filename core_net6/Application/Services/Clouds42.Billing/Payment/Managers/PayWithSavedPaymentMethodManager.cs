﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.Payment.Managers
{
    public class PayWithSavedPaymentMethodManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ILogger42 logger,
        IPayYookassaAggregatorPaymentProvider prepareYookassaAggregatorPaymentProvider,
        IConfiguration configuration)
        : BaseManager(accessProvider, dbLayer, handlerException), IPayWithSavedPaymentMethodManager
    {
        /// <summary>
        /// Оплата без ввода данных с сохраненным способом оплаты 
        /// </summary>
        /// <returns></returns>
        public async Task<ManagerResult<PaymentResultDto>> PayWithSavedPaymentMethodAsync(PayWithSavedPaymentMethodDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_System, () => model.AccountId);

                var locale = await DbLayer.AccountsRepository.GetLocaleAsync(model.AccountId, Guid.Parse(configuration["DefaultLocale"] ?? ""));

                switch (locale.Name)
                {
                    case LocaleConst.Russia:
                        var yookassaModel = new PrepareYookassaAggregatorPaymentDto
                        {
                            AccountId = model.AccountId,
                            PaymentSum = model.TotalSum,
                            PaymentMethodId = model.PaymentMethodId,
                            IsAutoPay = model.IsAutoPay
                        };

                        var result = prepareYookassaAggregatorPaymentProvider.PreparePaymentOrPayWithSavedPaymentMethod(yookassaModel);
                        
                        return Ok(new PaymentResultDto(result));
                }

                return PreconditionFailed<PaymentResultDto>("Не удалось расознать локаль пользователя");
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка оплаты без ввода данных с Id способа оплаты] {model.PaymentMethodId}" +
                                        $" и аккаунта с Id: {model.AccountId}");
                return PreconditionFailed<PaymentResultDto>(ex.Message);
            }
        }
    }
}
