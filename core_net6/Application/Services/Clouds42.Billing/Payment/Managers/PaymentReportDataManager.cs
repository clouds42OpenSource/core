﻿using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Payment.Managers
{
    /// <summary>
    /// Менеджер для работы с данными платежа для отчета
    /// </summary>
    public class PaymentReportDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IPaymentReportDataProvider paymentReportDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить список моделей данных по счету для отчета
        /// </summary>
        /// <returns>Список моделей данных по счету для отчета</returns>
        public ManagerResult<List<PaymentReportDataDto>> GetPaymentReportDataDcs()
        {
            try
            {
                logger.Info("Получение список моделей данных по платежу для отчета");
                AccessProvider.HasAccess(ObjectAction.GetPaymentReportDataDcs);
                var data = paymentReportDataProvider.GetPaymentReportDataDcs();
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка моделей данных по платежу для отчета]");
                return PreconditionFailed<List<PaymentReportDataDto>>(ex.Message);
            }
        }


    }
}
