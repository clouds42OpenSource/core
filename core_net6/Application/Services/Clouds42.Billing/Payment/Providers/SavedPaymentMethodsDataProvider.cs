﻿using Clouds42.Billing.Contracts.Payment;
using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Payment.Providers
{
    public class SavedPaymentMethodsDataProvider(
        IHandlerException handlerException,
        ICloudChangesProvider cloudChangesProvider,
        ILogger42 logger42,
        IUnitOfWork unitOfWork)
        : ISavedPaymentMethodsDataProvider
    {

        /// <summary>
        /// Получить сохраненные способы оплат
        /// </summary>
        /// <param name="accountId">Фильтр по аккаунту</param>
        /// <returns>DTO сохраненных спсособов оплат</returns>
        public async Task<List<SavedPaymentMethodDto>> GetSavedPaymentMethodsAsync(Guid? accountId)
        {
            var savedPaymentMethods = accountId == null
                ? await unitOfWork.SavedPaymentMethodRepository.GetAllAsync()
                : await unitOfWork.SavedPaymentMethodRepository.GetAllByAccountIdAsync(accountId.Value);

            var autoPayMethod = accountId != null
                ? await unitOfWork.AccountAutoPayConfigurationRepository.GetByAccountIdAsync(accountId.Value)
                : null;

            return savedPaymentMethods.Select(spm => new SavedPaymentMethodDto
            {
                Id = spm.Id,
                AccountId = spm.AccountId,
                CreatedOn = spm.CreatedOn,
                Title = spm.Title,
                Type = spm.Type,
                Metadata = spm.Metadata,
                Token = spm.Token,
                BankCardDetails = spm.BankCard == null
                    ? null
                    : new SavedPaymentMethodBankCardDto
                    {
                        FirstSixDigits = spm.BankCard.FirstSixDigits,
                        LastFourDigits = spm.BankCard.LastFourDigits,
                        CardType = spm.BankCard.CardType,
                        ExpiryMonth = spm.BankCard.ExpiryMonth,
                        ExpiryYear = spm.BankCard.ExpiryYear
                    },
                DefaultPaymentMethod = autoPayMethod != null && autoPayMethod.SavedPaymentMethodId == spm.Id
            }).ToList();
        }

        /// <summary>
        /// Получить сохраненный способ оплаты
        /// </summary>
        /// <param name="id">Фильтр идентификатору</param>
        /// <returns>DTO сохраненного спсособа оплат</returns>
        public async Task<SavedPaymentMethodDto?> GetSavedPaymentMethodAsync(Guid id)
        {
            var savedPaymentMethod = await unitOfWork.SavedPaymentMethodRepository.GetByIdAsync(id);
            if (savedPaymentMethod == null)
                return null;

            var autoPayMethod = await unitOfWork.AccountAutoPayConfigurationRepository.GetByAccountIdAsync(savedPaymentMethod.AccountId);

            return new SavedPaymentMethodDto
            {
                Id = savedPaymentMethod.Id,
                AccountId = savedPaymentMethod.Id,
                CreatedOn = savedPaymentMethod.CreatedOn,
                Title = savedPaymentMethod.Title,
                Type = savedPaymentMethod.Type,
                Metadata = savedPaymentMethod.Metadata,
                Token = savedPaymentMethod.Token,
                BankCardDetails = savedPaymentMethod.BankCard == null
                    ? null
                    : new SavedPaymentMethodBankCardDto
                    {
                        CardType = savedPaymentMethod.BankCard.CardType,
                        ExpiryMonth = savedPaymentMethod.BankCard.ExpiryMonth,
                        ExpiryYear = savedPaymentMethod.BankCard.ExpiryYear,
                        FirstSixDigits = savedPaymentMethod.BankCard.FirstSixDigits,
                        LastFourDigits = savedPaymentMethod.BankCard.LastFourDigits,
                    },
                DefaultPaymentMethod = autoPayMethod != null && autoPayMethod.SavedPaymentMethodId == savedPaymentMethod.Id
            };
        }

        /// <summary>
        /// Удалиить сохраненный способ оплат
        /// </summary>
        /// <param name="id">Идентификатор сохраненного способа оплат</param>
        /// <returns>Удачно/Неудачно</returns>
        public async Task<bool> DeleteSavedPaymentMethodAsync(Guid id)
        {
            using var dbScope = unitOfWork.SmartTransaction.Get();
            try
            {
                var paymentMethod = await unitOfWork.SavedPaymentMethodRepository.GetByIdAsync(id);
                if (paymentMethod == null)
                    throw new ArgumentException($"Не найден сохраненный способ оплаты по Id: {id}");

                var paymentMethodBankCard = await unitOfWork.SavedPaymentMethodBankCardRepository.GetByPaymentMethodIdAsync(id);
                var accountAutoPayConfiguration = await unitOfWork.AccountAutoPayConfigurationRepository.GetByPaymentMethodIdAsync(id);

                if (paymentMethodBankCard != null)
                    unitOfWork.SavedPaymentMethodBankCardRepository.Delete(paymentMethodBankCard);

                if (accountAutoPayConfiguration != null)
                    unitOfWork.AccountAutoPayConfigurationRepository.Delete(accountAutoPayConfiguration);

                unitOfWork.SavedPaymentMethodRepository.Delete(paymentMethod);

                await unitOfWork.SaveAsync();
                dbScope.Commit();
                cloudChangesProvider.LogEvent(paymentMethod.AccountId, LogActions.RefillByPaymentSystem, $"Удалена карта {paymentMethod.Title} с которой совершался автоплатеж");
                return true;
            }
            catch (Exception ex)
            {
                dbScope.Rollback();
                handlerException.Handle(ex,
                    $"[Ошибка удаления сохраненного способа оплаты] с Id: '{id}'");

                return false;
            }
        }

        /// <summary>
        /// Установить по умолчанию способ для автосписаний
        /// </summary>
        /// <param name="model">Модель способа оплаты и аккаунта</param>
        /// <returns>Удачно/Неудачно</returns>
        public bool MakeDefault(AccountPaymentMethodDto model)
        {
            using var dbScope = unitOfWork.SmartTransaction.Get();
            try
            {
                logger42.Info($"Начинаем поиск существующего автоплатежа по аккаунту {model.AccountId}");
                var existAutoPay = unitOfWork.AccountAutoPayConfigurationRepository
                    .GetByAccountId(model.AccountId);


                if (existAutoPay == null)
                {
                    logger42.Info($"Автоплатеж по аккаунту {model.AccountId} не найден. Инициируем создание нового автоплатежа");
                    var autoPay = new AccountAutoPayConfiguration
                    {
                        AccountId = model.AccountId,
                        SavedPaymentMethodId = model.PaymentMethodId
                    };
                    unitOfWork.AccountAutoPayConfigurationRepository.Insert(autoPay);
                    unitOfWork.Save();
                    logger42.Info($"Автоплатеж по аккаунту {model.AccountId} успешно создан");
                }

                else if (existAutoPay.AccountId == model.AccountId &&
                         existAutoPay.SavedPaymentMethodId == model.PaymentMethodId)
                {
                    logger42.Info($"Автоплатеж по аккаунту {model.AccountId} найден. " +
                                   $"Инициировали проверку по идентификатору аккаунта и платежному методу {existAutoPay.SavedPaymentMethodId}. " +
                                   $"Создание или обновление автоплатежа не требуется.");
                    return true;
                }


                else
                {
                    logger42.Info($"Автоплатеж по аккаунту {model.AccountId} найден. " +
                                   $"Проверка по идентификатору и платежному методу завершилась отрицательно. " +
                                   $"Обновленный идентификатор платежа {model.PaymentMethodId}. " +
                                   $"Выполняется обновление автоплатежа.");

                    var savedPaymentMethodBelongsToUser = unitOfWork.SavedPaymentMethodRepository.GetById(model.PaymentMethodId);

                    if (savedPaymentMethodBelongsToUser == null)
                        throw new ArgumentException($"Не найден сохраненный способ оплаты c Id: {model.PaymentMethodId} для установки по умолчанию");
                    if (savedPaymentMethodBelongsToUser.AccountId != model.AccountId)
                        throw new ArgumentException($"Способ оплаты с Id: {model.PaymentMethodId} не принадлежит пользователю к аккаунту с Id: {model.AccountId}");

                    existAutoPay.SavedPaymentMethodId = model.PaymentMethodId;

                    unitOfWork.AccountAutoPayConfigurationRepository.Update(existAutoPay);
                    unitOfWork.Save();
                    logger42.Info($"Автоплатеж по аккаунту {model.AccountId} успешно обновлен");
                }

                dbScope.Commit();
                return true;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Непредвиденная ошибка при установке значения по умолчанию для автосписаний]" +
                                             $" с Id аккаунта: {model.AccountId} и Id способа оплаты: {model.PaymentMethodId}");
                dbScope.Rollback();
                return false;
            }
        }
    }
}
