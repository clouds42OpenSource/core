﻿using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Billing.Payment.Providers
{
    /// <summary>
    /// Провайдер для работы с данными платежа для отчета
    /// </summary>
    internal class PaymentReportDataProvider(IUnitOfWork dbLayer) : IPaymentReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по платежу для отчета
        /// </summary>
        /// <returns>Список моделей данных по платежу для отчета</returns>
        public List<PaymentReportDataDto> GetPaymentReportDataDcs()
        {
            var lastOneAndHalfDateTime = DateTime.Now.AddYears(-1).AddMonths(-6);

            return dbLayer.PaymentRepository
                .AsQueryableNoTracking()
                .Where(x => x.Sum > 0 && x.Date > lastOneAndHalfDateTime)
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountSaleManager)
                .ThenInclude(x => x.AccountUser)
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Locale)
                .Select(x => new PaymentReportDataDto
                {
                    Id = x.Id,
                    AccountNumber = x.Account.IndexNumber,
                    Date = x.Date,
                    Description = x.Description,
                    OperationType = x.OperationType,
                    Sum = x.Sum,
                    Status = x.Status,
                    Country = x.Account.AccountConfiguration.Locale.Country,
                    SaleManager = x.Account.AccountSaleManager,
                    TransactionType = x.TransactionType
                })
                .OrderByDescending(x => x.Date)
                .ToList();
        }

    }
}
