﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Processors
{
    /// <summary>
    /// Обработчик начисления агентского вознаграждения.
    /// </summary>
    internal class IncreaseAgentPaymentProcessor(
        IUnitOfWork dbLayer,
        ICalculateAgentRewardProvider calculateAgentRewardProvider,
        IHandlerException handlerException)
        : IIncreaseAgentPaymentProcessor
    {
        /// <summary>
        /// Обработать начисления агентского вознаграждения.
        /// </summary>
        /// <param name="paymentId">Номер клиентского платежа.</param>
        public void Process(Guid paymentId)
        {
            var agentRewardInfo = calculateAgentRewardProvider.Calculate(paymentId);

            var clientPayment = dbLayer.PaymentRepository.FindPayment(paymentId) ??
                                throw new NotFoundException($"По номеру '{paymentId}'не найден клиентский платеж.");

            if (!agentRewardInfo.AccountId.HasValue || clientPayment.Sum == 0)
                return;
            
            try
            {
                dbLayer.AgentWalletRepository.GetAgentWalletOrCreateNew(agentRewardInfo.AccountId.Value);
                dbLayer.AgentPaymentRepository.InsertAgentPaymentAndUpdateAgentWallet(
                    agentRewardInfo.AccountId.Value,
                    agentRewardInfo.RewardSum, paymentId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка обработки начисления агентского вознаграждения]");
                throw;
            }
        }

        /// <summary>
        /// Обработать начисления агентского вознаграждения.
        /// </summary>
        /// <param name="payments">Номера клиентских платежей.</param>
        public void Process(List<Guid> payments)
        {
            try
            {
                foreach (var payment in payments)
                {
                    Process(payment);
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка обработки начисления агентского вознаграждения]");
                throw;
            }
        }
    }
}
