﻿using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Processors
{
    /// <summary>
    /// Обработчик по принятию демо подписки сервиса.
    /// </summary>
    public class ApplyDemoServiceSubscribeProcessor(
        IUnitOfWork dbLayer,
        IBillingServiceInfoProvider billingServiceInfoProvider,
        IBillingPaymentsDataProvider billingPaymentsDataProvider,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IDeleteUnusedResourcesProcessor deleteUnusedResourcesProcessor,
        IIncreaseAgentPaymentProcessor increaseAgentPaymentProcessor,
        IBillingPaymentsProvider billingPaymentsProvider,
        IServiceExtensionDatabaseActivationStatusProvider serviceExtensionDatabaseActivationStatusProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IHandlerException handlerException)
        : IApplyDemoServiceSubscribeProcessor
    {
        private IPromisePaymentProvider _promisePaymentProvider;

        public void SetPromisePaymentProvider(IPromisePaymentProvider promisePaymentProvider)
        {
            _promisePaymentProvider = promisePaymentProvider;
        }

        /// <summary>
        /// Принять подписку демо сервиса.
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="usePromisePayment">Покупка за счет обещанного платежа.</param>
        public ApplyDemoServiceSubscribeResultDto Apply(Guid serviceId, Guid accountId, bool usePromisePayment)
        {
            try
            {
                var serviceInfo = billingServiceInfoProvider.GetBillingService(serviceId, accountId);

                var result = ChildServiceApplySubscribe(serviceInfo, accountId, usePromisePayment);

                var resConfig = 
                    resourceConfigurationDataProvider.GetResourceConfigurationOrThrowException(serviceInfo.Id, accountId);
                deleteUnusedResourcesProcessor.Process(resConfig);

                dbLayer.Save();
                return result;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка принятия подписки сервиса] '{serviceId}' аккаунтом '{accountId}'");
                throw;
            }
        }

        /// <summary>
        /// Получить информацию о оплате сервиса.
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public BillingServiceAmountDataModelDto GetBillingServiceAmountData(Guid serviceId, Guid accountId)
        {
            try
            {
                var serviceInfo = billingServiceInfoProvider.GetBillingService(serviceId, accountId);
                return serviceInfo.AmountData;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка при получении информации о стоимости сервиса] '{serviceId}' аккаунта '{accountId}'");
                throw;
            }
        }

        /// <summary>
        /// Принять демо подписку.
        /// </summary>
        /// <param name="serviceInfo">Данные о сервисе.</param>        
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="usePromisePayment">Использовать ОП.</param>
        /// <returns>Результат принятия демо периода.</returns>
        private ApplyDemoServiceSubscribeResultDto ChildServiceApplySubscribe(BillingServiceDto serviceInfo, Guid accountId, bool usePromisePayment)
        {
            if (serviceInfo.AmountData.ServicePartialAmount is > 0)
            {
                return PayAndApplySubscribe(serviceInfo, accountId, usePromisePayment);
            }

            ApplySubscribe(serviceInfo, accountId);

            return new ApplyDemoServiceSubscribeResultDto
            {
                Complete = true
            };
        }

        /// <summary>
        /// Купить и активировать подписку сервиса.
        /// </summary>
        /// <param name="serviceInfo">Данные о сервисе.</param>        
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="usePromisePayment">Использовать ОП.</param>
        /// <returns>Результат принятия демо периода.</returns>
        private ApplyDemoServiceSubscribeResultDto PayAndApplySubscribe(BillingServiceDto serviceInfo, Guid accountId,
            bool usePromisePayment)
        {
            var result = usePromisePayment
                ? MakePromisePayment(serviceInfo, accountId)
                : MakePayment(serviceInfo, accountId);

            if (result.Complete)
                ApplySubscribe(serviceInfo, accountId);

            return result;
        }

        /// <summary>
        /// Провести обещанный платеж.
        /// </summary>
        /// <param name="serviceInfo">Данные о сервисе.</param>        
        /// <param name="accountId">Номер аккаунта.</param>        
        /// <returns>Результат принятия демо периода.</returns>
        private ApplyDemoServiceSubscribeResultDto MakePromisePayment(BillingServiceDto serviceInfo, Guid accountId)
        {
            var paymentSum = serviceInfo.AmountData.ServicePartialAmount ?? 0;
            var canCreatePaymentResult =
                billingPaymentsProvider.CanCreatePayment(accountId, paymentSum, out var enouthMoney);

            if (canCreatePaymentResult == PaymentOperationResult.Ok)
                return MakePayment(serviceInfo, accountId);

            var paymentPurposeText = billingServiceInfoProvider.GetPaymentPurposeText(serviceInfo.Id, accountId);
            var createPromisePaymentResult = _promisePaymentProvider.CreatePromisePayment(accountId,
                new CalculationOfInvoiceRequestModel
                {
                    SuggestedPayment = new SuggestedPaymentModelDto
                    {
                        PaymentSum = enouthMoney,
                        ServiceTypeContent = paymentPurposeText
                    }
                });

            if (!createPromisePaymentResult.Complete)
                return CreateApplyResult(accountId, false,
                    errorMessage:
                    $"Не удалось получить обещанный платеж на сумму {enouthMoney} {accountConfigurationDataProvider.GetAccountLocale(accountId).Name}");

            return MakePayment(serviceInfo, accountId);
        }

        /// <summary>
        /// Провести платеж.
        /// </summary>
        /// <param name="serviceInfo">Данные о сервисе.</param>        
        /// <param name="accountId">Номер аккаунта.</param>        
        /// <returns>Результат принятия демо периода.</returns>
        private ApplyDemoServiceSubscribeResultDto MakePayment(BillingServiceDto serviceInfo, Guid accountId)
        {
            var paymentSum = serviceInfo.AmountData.ServicePartialAmount ?? 0;
            var canCreatePaymentResult =
                billingPaymentsProvider.CanCreatePayment(accountId, paymentSum, out var enouthMoney);

            if (canCreatePaymentResult != PaymentOperationResult.Ok)
            {
                return CreateApplyResult(accountId, false, enouthMoney);
            }

            var createPaymentResult = CreatePaymentTransaction(serviceInfo, accountId, paymentSum);

            if (createPaymentResult.OperationStatus != PaymentOperationResult.Ok)
                return CreateApplyResult(accountId, false,
                    errorMessage: createPaymentResult.OperationStatus.GetDisplayName());

            if (paymentSum > 0)
            {
                increaseAgentPaymentProcessor.Process(createPaymentResult.PaymentIds);
            }

            return new ApplyDemoServiceSubscribeResultDto { Complete = true };
        }

        /// <summary>
        /// Активировать подписку сервиса.
        /// </summary>
        /// <param name="serviceInfo">Данные о сервисе.</param>        
        /// <param name="accountId">Номер аккаунта.</param>        
        /// <returns>Результат принятия демо периода.</returns>
        private void ApplySubscribe(BillingServiceDto serviceInfo, Guid accountId)
        {
            var resourceConfiguration =
                resourceConfigurationDataProvider.GetResourceConfigurationOrThrowException(serviceInfo.Id, accountId);

            if (resourceConfiguration.FrozenValue)
                serviceExtensionDatabaseActivationStatusProvider.SetActivationStatus(accountId, true, serviceInfo.Id);

            resourceConfiguration.IsDemoPeriod = false;
            resourceConfiguration.ExpireDate = null;
            resourceConfiguration.Frozen = null;
            resourceConfiguration.IsAutoSubscriptionEnabled = null;

            if (serviceInfo.IsHybridService)
            {
                resourceConfiguration.ExpireDate = DateTime.Now.AddMonths(1);
                resourceConfiguration.Frozen = false;
            }
            dbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
            dbLayer.Save();
        }

        /// <summary>
        /// Создать объект результата по принятию подписки.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="complete">Признак успешности операции принятия подписки.</param>
        /// <param name="enouthMoney">Сумма денег, которой не хватает.</param>
        /// <param name="errorMessage">Сообщение об ошибке.</param>
        /// <returns></returns>
        private ApplyDemoServiceSubscribeResultDto CreateApplyResult(Guid accountId, bool complete,
            decimal? enouthMoney = null, string errorMessage = null)
        {
            return new ApplyDemoServiceSubscribeResultDto
            {
                Complete = complete,
                NeedMoney = enouthMoney,
                CanUsePromisePayment = AccountCanCreatePromisePayment(accountId),
                ErrorMessage = errorMessage
            };
        }

        /// <summary>
        /// Создать платежную транзакцию.
        /// </summary>
        /// <param name="serviceInfo">Данные о сервисе за которые производится оплата.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="paymentSum">Сумма транзакции.</param>
        /// <returns>Резльтат создания платежной тразакции.</returns>
        private CreatePaymentResultDto CreatePaymentTransaction(BillingServiceDto serviceInfo, Guid accountId,
            decimal paymentSum)
            => billingPaymentsProvider.MakePayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = billingServiceInfoProvider.GetPaymentPurposeText(serviceInfo.Id, accountId),
                BillingServiceId = serviceInfo.Id,
                Status = PaymentStatus.Done,
                Total = paymentSum,
                OperationType = PaymentType.Outflow,
                OriginDetails = "core-cp"
            });

        /// <summary>
        /// Проверить возможность аккаунтом брать обещанный платеж.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>true- аккаунт может азять ОП и нет в противном случае.</returns>
        private bool AccountCanCreatePromisePayment(Guid accountId)
            => billingPaymentsDataProvider.CanGetPromisePayment(accountId);
    }
}
