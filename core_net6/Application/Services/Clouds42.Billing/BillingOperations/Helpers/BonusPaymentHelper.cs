﻿using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using InvoiceEntity = Clouds42.Domain.DataModels.billing.Invoice;

namespace Clouds42.Billing.BillingOperations.Helpers
{
    /// <summary>
    /// Хэлпер для бонусных платежей
    /// </summary>
    public class BonusPaymentHelper
    {
        /// <summary>
        /// Создать объект платежа
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="operationType">Тип операции</param>
        /// <param name="invoice">Счет на оплату</param>
        /// <returns>Платеж</returns>
        public Domain.DataModels.billing.Payment CreateBonusPaymentObject(Guid accountId, decimal paymentSum, PaymentType operationType, InvoiceEntity invoice)
            => new()
            {
                Id = Guid.NewGuid(),
                PaymentSystem = PaymentSystem.ControlPanel.ToString(),
                AccountId = accountId,
                Date = DateTime.Now,
                Sum = paymentSum,
                OperationType = operationType.ToString(),
                Status = PaymentStatus.Done.ToString(),
                Description = GetBonusPaymentDescription(invoice, operationType),
                OriginDetails = "core-cp",
                TransactionType = TransactionType.Bonus
            };

        /// <summary>
        /// Получить описание для бонусного платежа
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <param name="operationType">Тип операции</param>
        /// <returns>Описание для бонусного платежа</returns>
        private static string GetBonusPaymentDescription(InvoiceEntity invoice, PaymentType operationType)
        {
            var paymentDescription = operationType == PaymentType.Inflow
                ? $"Начисление бонусов согласно оплаты по счету №{CloudConfigurationProvider.Invoices.UniqPrefix()}{invoice.Uniq} от {invoice.Date:dd.MM.yyyy}"
                : $"Списание бонусов на основании счета на оплату №{CloudConfigurationProvider.Invoices.UniqPrefix()}{invoice.Uniq} от {invoice.Date:dd.MM.yyyy}";

            return paymentDescription;
        }
    }
}
