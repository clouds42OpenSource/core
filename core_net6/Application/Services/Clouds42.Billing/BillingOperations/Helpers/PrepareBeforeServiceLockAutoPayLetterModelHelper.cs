﻿using System.Text;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.IncomingModels;
using Clouds42.Locales.Extensions;

namespace Clouds42.Billing.BillingOperations.Helpers
{
    /// <summary>
    /// Хелпер для подготовки модели письма о скорой блокировке сервиса
    /// </summary>
    public static class PrepareBeforeServiceLockAutoPayLetterModelHelper
    {
        private static readonly string RouteForOpenBalancePage =
            CloudConfigurationProvider.Cp.GetRouteForOpenBalancePage();

        /// <summary>
        /// Подготовить модель письма о скорой блокировке сервиса
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="invoiceDc">Модель счета</param>
        /// <param name="documentBuilderResult">Результат билда документа</param>
        /// <param name="siteAuthorityUrl">Урл сайта ЛК</param>
        /// <param name="invoicePeriod">Период счета</param>
        /// <param name="paymentMethodInfo"></param>
        /// <param name="cloudLocalizer"></param>
        /// <returns>Модель письма о скорой блокировке сервиса</returns>
        public static BeforeServiceLockAutoPayLetterModel PrepareBeforeServiceLockAutoPayLetterModel(
            ResourcesConfiguration resourcesConfiguration, InvoiceDc invoiceDc,
            DocumentBuilderResult documentBuilderResult, string siteAuthorityUrl, int? invoicePeriod, (string paymentMethodType, SavedPaymentMethodBankCard bankCardInfo) paymentMethodInfo, ICloudLocalizer cloudLocalizer)
        {
            var invoicePeriodValue = invoicePeriod is null or (int)PayPeriod.None
                ? (int)PayPeriod.Month1
                : invoicePeriod.Value;

            return new BeforeServiceLockAutoPayLetterModel
            {
                AccountId = resourcesConfiguration.AccountId,
                BillingServiceName = resourcesConfiguration.GetLocalizedServiceName(cloudLocalizer),
                LockServiceDate = resourcesConfiguration.ExpireDate ?? DateTime.Now,
                AutoPayDate = resourcesConfiguration.ExpireDate?.AddDays(-1) ?? DateTime.Now,
                PaymentMethod = GetPaymentMethodInfo(paymentMethodInfo.paymentMethodType, paymentMethodInfo.bankCardInfo),
                Invoice = new InvoiceDataDto
                {
                    InvoiceNumber = invoiceDc.Uniq,
                    InvoiceDocBytes = documentBuilderResult.Bytes,
                    InvoiceDocFileName = CloudConfigurationProvider.Invoices.GetInvoiceAttachFileName()
                },
                ServiceCost = invoiceDc.InvoiceSum / invoicePeriodValue,
                InvoiceSum = invoiceDc.InvoiceSum,
                BalancePageUrl =
                    new Uri(
                            $"{siteAuthorityUrl}/{RouteForOpenBalancePage}")
                        .AbsoluteUri
            };
        }

        private static string GetPaymentMethodInfo(string paymentMethodType, SavedPaymentMethodBankCard bankCardInfo)
        {
            var result = new StringBuilder();
            if (bankCardInfo == null)
                throw new ArgumentNullException(nameof(bankCardInfo));

            switch (paymentMethodType.GetEnumByStringValue<SavedPaymentMethodType>())
            {
                case SavedPaymentMethodType.BankCard:
                    result.Append($"карты {bankCardInfo.FirstSixDigits[..4]} ");
                    result.Append($"{bankCardInfo.FirstSixDigits.Substring(4, 2)}");
                    result.Append("** ****");
                    result.Append($"{bankCardInfo.LastFourDigits}");
                    break;
            }

            return result.ToString();
        }
    }
}
