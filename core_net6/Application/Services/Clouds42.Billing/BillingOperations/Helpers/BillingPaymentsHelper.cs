﻿using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Helpers
{
    /// <summary>
    /// Хэлпер для платежей биллинга
    /// </summary>
    public class BillingPaymentsHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Создать список транзакций на основании описания платежа
        /// </summary>
        /// <param name="paymentDefinition">Описание платежа</param>
        /// <returns>Список транзакций основании описания</returns>
        public List<PaymentDefinitionDto> CreatePaymentDefinitions(PaymentDefinitionDto paymentDefinition)
        {
            paymentDefinition.Id = Guid.NewGuid();
            var paymentDefinitions = new List<PaymentDefinitionDto>();

            var billingAccount = dbLayer.BillingAccountRepository.GetBillingAccount(paymentDefinition.Account);

            if (paymentDefinition.Total <= billingAccount.Balance)
            {
                paymentDefinitions.Add(paymentDefinition);
                return paymentDefinitions;
            }

            var bonusCountForProlong = paymentDefinition.Total - billingAccount.Balance;

            var moneyPayment = CreatePaymentDefinition(paymentDefinition, TransactionType.Money, billingAccount.Balance);
            var bonusPayment = CreatePaymentDefinition(paymentDefinition, TransactionType.Bonus, bonusCountForProlong);

            paymentDefinitions.Add(moneyPayment);
            paymentDefinitions.Add(bonusPayment);

            return paymentDefinitions;
        }

        /// <summary>
        /// Создать новое описание платежа
        /// </summary>
        /// <param name="paymentDefinition">Текущее описание платежа</param>
        /// <param name="transactionType">Тип транзакции</param>
        /// <param name="cost">Стоимость</param>
        /// <returns>Новое описание платежа</returns>
        private PaymentDefinitionDto CreatePaymentDefinition(PaymentDefinitionDto paymentDefinition, TransactionType transactionType, decimal cost)
            => new()
            {
                Account = paymentDefinition.Account,
                Date = DateTime.Now,
                Description = paymentDefinition.Description,
                Id = Guid.NewGuid(),
                BillingServiceId = paymentDefinition.BillingServiceId,
                Status = PaymentStatus.Done,
                Total = cost,
                System = paymentDefinition.System,
                OperationType = paymentDefinition.OperationType,
                OriginDetails = paymentDefinition.OriginDetails,
                CanOverdraft = paymentDefinition.CanOverdraft,
                TransactionType = transactionType
            };
    }
}
