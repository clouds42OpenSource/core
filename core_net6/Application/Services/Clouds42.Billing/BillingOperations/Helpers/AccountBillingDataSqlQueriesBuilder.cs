﻿using Clouds42.Domain.Enums;

namespace Clouds42.Billing.BillingOperations.Helpers
{
    /// <summary>
    /// Конструктор запросов для получения данных биллинга для аккаунта
    /// </summary>
    public static class AccountBillingDataSqlQueriesBuilder
    {
        /// <summary>
        /// Получить запрос, на получение данных
        /// по сервисам биллинга для аккаунта
        /// </summary>
        public static string GetAccountServicesDataQuery => $@"
        SELECT s.Id, s.Name,
        (CASE 
        WHEN s.SystemService IS NOT NULL THEN CAST(1 as BIT)
        WHEN s.IsActive = 0 THEN CAST(0 as BIT)
        WHEN NOT EXISTS(
        SELECT r.Id
        FROM billing.ServiceTypes AS st
        INNER JOIN billing.Resources r ON r.BillingServiceTypeId = st.Id
        WHERE r.Subject IS NOT NULL AND (r.AccountId = rc.AccountId OR r.AccountSponsorId = rc.AccountId)
        AND st.ServiceId = s.Id
        ) OR (rc.Frozen IS NOT NULL AND rc.IsDemoPeriod = 1 AND rc.Frozen = 1)
        THEN CAST(0 as BIT)
        ELSE CAST(1 as BIT) END) AS IsActive,
        s.SystemService
        FROM billing.ResourcesConfigurations rc
        INNER JOIN billing.Services s ON s.Id = rc.BillingServiceId

        WHERE (s.SystemService IS NULL OR s.SystemService = {(int)Clouds42Service.MyEnterprise}) AND rc.AccountId= @accountId
        ORDER BY s.SystemService DESC";
    }
}
