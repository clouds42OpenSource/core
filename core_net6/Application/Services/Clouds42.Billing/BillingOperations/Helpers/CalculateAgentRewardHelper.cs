﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.BillingOperations.Helpers
{
    /// <summary>
    /// Класс хелпер для калькуляции агентского вознаграждения
    /// </summary>
    public static class CalculateAgentRewardHelper
    {
        /// <summary>
        /// Проверить возможность начислить вознаграждение агенту
        /// </summary>
        /// <param name="calculateAgentRewardData">Модель данных для калькуляции агентского вознаграждения</param>
        /// <returns>Возможность начислить вознаграждение агенту</returns>
        public static bool CheckAbilityToAccrueAgentReward(CalculateAgentRewardDataDto calculateAgentRewardData)
        {
            if (calculateAgentRewardData is { ServiceOwnerId: not null, SystemService: null })
                return true;

            if (!calculateAgentRewardData.ReferralAccountId.HasValue ||
                calculateAgentRewardData.TransactionType == TransactionType.Bonus)
                return false;

            if (!CheckAbilityToAccrueAgentRewardBySystemService(calculateAgentRewardData.SystemService))
                return false;

            return true;
        }

        /// <summary>
        /// Проверить возможность начислить
        /// вознаграждение агенту по системному сервису
        /// </summary>
        /// <param name="systemService">Тип системного сервиса</param>
        /// <returns>Возможность начислить
        /// вознаграждение агенту по системному сервису</returns>
        public static bool CheckAbilityToAccrueAgentRewardBySystemService(Clouds42Service? systemService) =>
            systemService != Clouds42Service.Esdl && systemService != Clouds42Service.Recognition &&
            systemService != Clouds42Service.MyDatabases;
    }
}
