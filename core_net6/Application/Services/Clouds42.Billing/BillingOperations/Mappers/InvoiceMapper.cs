﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.Enums;
using InvoiceEntity = Clouds42.Domain.DataModels.billing.Invoice;

namespace Clouds42.Billing.BillingOperations.Mappers
{
    /// <summary>
    /// Маппер для счетов на оплату
    /// </summary>
    public static class InvoiceMapper
    {
        /// <summary>
        /// Смапить в модель информации о счете на оплату
        /// </summary>
        /// <param name="invoice">Доменная модель</param>
        /// <returns>Модель контракта</returns>
        public static InvoiceInfoDto MapToInvoiceInfoDc(this InvoiceEntity invoice)
            => new()
            {
                Id = invoice.Id,
                InvoiceDate = invoice.Date,
                InvoiceSum = invoice.Sum,
                Description = invoice.Description,
                Uniq = CloudConfigurationProvider.Invoices.UniqPrefix() + invoice.Uniq,
                ActId = invoice.ActID,
                RequiredSignature = invoice.RequiredSignature,
                Status = invoice.Status,
                ActDescription = invoice.ActDescription,
                ReceiptFiscalNumber = invoice.InvoiceReceipt?.FiscalNumber,
                State = !string.IsNullOrWhiteSpace(invoice.State)
                    ? (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), invoice.State)
                    : InvoiceStatus.Processing
            };
    }
}
