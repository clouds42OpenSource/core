﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Billing.BillingOperations.Mappers
{
    /// <summary>
    /// Маппер транзакций
    /// </summary>
    public static class TransactionMapper
    {
        /// <summary>
        /// Смапить модель платежа к модели операции биллинга
        /// </summary>
        /// <param name="payment">Модель платежа</param>
        /// <returns>Модель операции биллинга</returns>
        public static BillingOperationItemDto MapToBillingOperationItemDm(this Domain.DataModels.billing.Payment payment)
            => new()
            {
                Id = payment.Id,
                Title = payment.Description,
                Date = payment.Date,
                PaySum = payment.Sum,
                Status = payment.StatusEnum,
                Type = payment.OperationTypeEnum,
                TransactionType = payment.TransactionType,
                Remains = payment.Remains
            };
    }
}
