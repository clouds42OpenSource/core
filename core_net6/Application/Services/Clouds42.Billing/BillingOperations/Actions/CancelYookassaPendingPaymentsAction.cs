﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Billing.BillingOperations.Actions
{
    /// <summary>
    /// Действие для отмены зависших платежей агрегатора ЮKassa
    /// </summary>
    public class CancelYookassaPendingPaymentsAction : CancelAgregatorPendingPaymentsBaseAction
    {
        private readonly IYookassaAggregatorPaymentDataProvider _yookassaAggregatorPaymentDataProvider;
        private readonly Lazy<double> _allowedPendingPaymentsDaysCount;
        private readonly IHandlerException _handlerException;
        public CancelYookassaPendingPaymentsAction(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _yookassaAggregatorPaymentDataProvider = serviceProvider.GetRequiredService<IYookassaAggregatorPaymentDataProvider>();
            _allowedPendingPaymentsDaysCount = new Lazy<double>(CloudConfigurationProvider.YookassaAggregatorPayment
                .GetAllowedPendingPaymentsDaysCount);
            _handlerException = ServiceProvider.GetRequiredService<IHandlerException>();
        }

        /// <summary>
        /// Отменить зависшие платежи агрегатора ЮKassa
        /// </summary>
        public override void Cancel()
        {
            var dateForPayments = DateTime.Now.AddDays(-_allowedPendingPaymentsDaysCount.Value);
            var yookassaPayments = _yookassaAggregatorPaymentDataProvider.GetPendingPayments(dateForPayments.Date,
                dateForPayments.Date.AddDays(1).AddSeconds(-1)).ToList();

            if (!yookassaPayments.Any())
                return;

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                yookassaPayments.ForEach(CancelPayment);

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    "[Ошибка отмены зависших платежей агрегатора ЮKassa]");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Получить название агрегатора, для которого отменяются платежи
        /// </summary>
        /// <returns>Название агрегатора</returns>
        public override string GetAgregatorName() => PaymentSystem.Yookassa.ToString();
        
    }
}
