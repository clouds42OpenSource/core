﻿using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Billing.BillingOperations.Actions
{
    /// <summary>
    /// Базовое действие для отмены зависших платежей агрегатора
    /// </summary>
    public abstract class CancelAgregatorPendingPaymentsBaseAction(IServiceProvider serviceProvider)
    {
        protected readonly IServiceProvider ServiceProvider = serviceProvider;
        protected readonly IUnitOfWork DbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();

        /// <summary>
        /// Отменить зависшие платежи агрегатора
        /// </summary>
        public abstract void Cancel();

        /// <summary>
        /// Получить название агрегатора, для которого отменяются платежи
        /// </summary>
        /// <returns>Название агрегатора</returns>
        public abstract string GetAgregatorName();

        /// <summary>
        /// Отменить платеж системы
        /// </summary>
        /// <param name="payment">Платеж системы</param>
        protected void CancelPayment(Domain.DataModels.billing.Payment payment)
        {
            payment.Status = PaymentStatus.Canceled.ToString();
            DbLayer.PaymentRepository.Update(payment);
            DbLayer.Save();
        }
    }
}
