﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.YookassaAggregator.Providers.Interfaces;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер для проверки статуса платежа агрегатора ЮKassa
    /// Получает статус платежа по API ЮKassa и вызывает
    /// обработку события на изменение статуса
    /// </summary>
    internal class FinalizeYookassaAggregatorPaymentProvider(
        IGetYookassaPaymentObjectInfoProvider getYookassaPaymentObjectInfoProvider,
        IYookassaAggregatorPaymentDataProvider yookassaAggregatorPaymentDataProvider,
        IYookassaAggregatorPaymentFinalizerProvider yookassaAggregatorPaymentFinalizerProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IFinalizeYookassaAggregatorPaymentProvider
    {
        /// <summary>
        /// Проверить изменение статуса платежа агрегатора ЮKassa
        /// При изменении вызовится обработчик события на изменение статуса
        /// </summary>
        /// <param name="yookassaPaymentId">Id платежа агрегатора ЮKassa</param>
        /// <returns>Результат проверки изменения статуса</returns>
        public bool FinalizePayment(Guid yookassaPaymentId)
        {
            var data = getYookassaPaymentObjectInfoProvider.GetPaymentObjectInfo(
                GenerateGetYookassaPaymentObjectInfoDc(yookassaPaymentId));

            if (data.Status != YookassaPaymentStatusEnum.Succeeded.GetStringValue() &&
                data.Status != YookassaPaymentStatusEnum.Canceled.GetStringValue())
                return false;

            var result = yookassaAggregatorPaymentFinalizerProvider.FinalizePayment(data);
            return result;
        }

        /// <summary>
        /// Сформировать модель для получения информации
        /// об объекте платежа агрегатора ЮKassa
        /// </summary>
        /// <param name="yookassaPaymentId">Id платежа агрегатора ЮKassa</param>
        /// <returns>Модель для получения информации
        /// об объекте платежа агрегатора ЮKassa</returns>
        private GetYookassaPaymentObjectInfoDto GenerateGetYookassaPaymentObjectInfoDc(Guid yookassaPaymentId)
        {
            var payment =
                yookassaAggregatorPaymentDataProvider.GetPaymentByYookassaPaymentIdOrThrowException(yookassaPaymentId);

            var service = yookassaAggregatorPaymentDataProvider.GetService(payment.BillingServiceId);

            var supplierId = service is { InternalCloudService: InternalCloudServiceEnum.AlpacaMeet } ? yookassaAggregatorPaymentDataProvider.GetSupplier("ООО \"УчетОнлайн\"") : accountConfigurationDataProvider.GetAccountConfigurationDc(payment.AccountId).SupplierId;

            return new GetYookassaPaymentObjectInfoDto
            {
                IdempotenceKey = $"{payment.Id:N}",
                SupplierId = supplierId,
                YookassaPaymentId = yookassaPaymentId
            };
        }
    }
}
