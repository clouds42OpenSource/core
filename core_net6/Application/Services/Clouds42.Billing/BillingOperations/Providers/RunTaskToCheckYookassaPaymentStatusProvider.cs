﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.CoreWorker.BillingJobs.FinalizeYokassaPayments;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер для запуска задачи по проверке статуса платежа агрегатора ЮKassa
    /// </summary>
    internal class RunTaskToCheckYookassaPaymentStatusProvider(
        FinalizeYookassaPaymentJobWrapper finalizeYookassaPaymentJobWrapper,
        IUnitOfWork dbLayer)
        : IRunTaskToCheckYookassaPaymentStatusProvider
    {
        /// <summary>
        /// Запустить задачу по проверке
        /// статуса платежа агрегатора ЮKassa
        /// </summary>
        /// <param name="paymentId">Id системного платежа</param>
        public void RunTask(Guid paymentId)
        {
            var payment = GetPayment(paymentId);

            if (payment.Status != PaymentStatus.Waiting.ToString())
                throw new InvalidOperationException(
                    "Запуск задачи для проверки статуса платежа агрегатора ЮKassa не возможен.");

            finalizeYookassaPaymentJobWrapper.Start(new FinalizeYookassaPaymentJobParamsDto
            {
                YookassaAgregatorPaymentId = new Guid(payment.PaymentSystemTransactionId)
            });
        }

        /// <summary>
        /// Получить платеж агрегатора ЮKassa
        /// </summary>
        /// <param name="paymentId">Id системного платежа</param>
        /// <returns>Платеж агрегатора ЮKassa</returns>
        private Domain.DataModels.billing.Payment GetPayment(Guid paymentId) =>
            dbLayer.PaymentRepository.GetById(paymentId) ??
            throw new NotFoundException($"Не удалось получить платеж по Id '{paymentId}'");
    }
}
