﻿using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Domain.Enums;
using InvoiceEntity = Clouds42.Domain.DataModels.billing.Invoice;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Обработчик бонусных платежей
    /// </summary>
    public class BonusPaymentProvider(
        BonusPaymentHelper bonusPaymentHelper,
        IBillingPaymentsProvider billingPaymentsProvider)
        : IBonusPaymentProvider
    {
        /// <summary>
        /// Создать бонусный платеж
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoice">Счет на оплату</param>
        public PaymentOperationResult CreateInflowBonusPayment(Guid accountId, InvoiceEntity invoice)
        {
            if (invoice.BonusReward == null)
                throw new ArgumentException(
                    $"Не возможно создать бонусный платеж по счету {invoice.Uniq}. Не определено бонусное вознаграждение.");

            var payment =
                bonusPaymentHelper.CreateBonusPaymentObject(accountId, invoice.BonusReward.Value, PaymentType.Inflow, invoice);

            return billingPaymentsProvider.AddPayment(payment);
        }

        /// <summary>
        /// Создать исходящий бонусный платеж
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        public PaymentOperationResult CreateOutflowBonusPayment(InvoiceEntity invoice)
        {
            if (invoice.BonusReward == null)
                return PaymentOperationResult.Ok;

            var payment =
                bonusPaymentHelper.CreateBonusPaymentObject(invoice.AccountId, invoice.BonusReward.Value, PaymentType.Outflow, invoice);

            return billingPaymentsProvider.AddPayment(payment);
        }
    }
}
