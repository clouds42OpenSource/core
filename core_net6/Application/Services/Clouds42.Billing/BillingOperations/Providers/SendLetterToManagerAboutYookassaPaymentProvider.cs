﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Configurations.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using System.Text;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер для отправки письма менеджеру
    /// о совершении оплаты через ЮKassa
    /// </summary>
    internal class SendLetterToManagerAboutYookassaPaymentProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ICloudLocalizer cloudLocalizer,
        IAccountRequisitesDataProvider accountRequisitesDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
        : ISendLetterToManagerAboutYookassaPaymentProvider
    {
        private readonly Lazy<string> _managerEmail = new(CloudConfigurationProvider.Emails.Get42CloudsManagerEmail);
        private readonly Lazy<string> _senderDescription = new(CloudConfigurationProvider.LetterNotification.GetSenderDescription);

        /// <summary>
        /// Отправить письмо менеджеру о совершении оплаты
        /// через ЮKassa
        /// </summary>
        /// <param name="model">Модель данных для отправки письма менеджеру
        /// о совершении оплаты через ЮKassa</param>
        public void SendLetter(SendLetterToManagerAboutYookassaPaymentDto model)
        {
            var account = dbLayer.AccountsRepository.GetAccount(model.AccountId);
            var paymentSystem = "ЮKassa";

            var content = GenerateMessage(model, account);
            SendLetter($"Оплата по {paymentSystem}", content);

            LogEventHelper.LogEvent(dbLayer, account.Id, accessProvider, LogActions.MailNotification,
                $"Письмо \"Оплата по {paymentSystem}\" отправлено на адрес {_managerEmail.Value}");

            logger.Trace($"Отправили аккаунту {account.IndexNumber} письмо по зачислению оплаты через {paymentSystem}");
        }

        /// <summary>
        /// Отправить письмо менеджеру
        /// </summary>
        /// <param name="subject">Тема письма</param>
        /// <param name="content">Тело письма</param>
        private void SendLetter(string subject, string content) =>
            new Manager42CloudsMail()
                .DisplayName(_senderDescription.Value)
                .To(_managerEmail.Value)
                .Subject(subject)
                .Body(content)
                .SendViaNewThread();

        /// <summary>
        /// Сформировать сообщение для менеджера
        /// о совершении оплаты через ЮKassa
        /// </summary>
        /// <param name="model">Модель данных для отправки письма менеджеру
        /// о совершении оплаты через ЮKassa</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Сообщение для менеджера о совершении оплаты через ЮKassa</returns>
        private string GenerateMessage(SendLetterToManagerAboutYookassaPaymentDto model, Account account)
        {
            var accountRequisites = accountRequisitesDataProvider.GetIfExistsOrCreateNew(account.Id);

            var builder = new StringBuilder();
            var locale = accountConfigurationDataProvider.GetAccountLocale(account.Id);
            builder.Append(
                $"{model.PaymentDate:dd.MM.yyyy HH:mm} зачислена оплата в размере <b>{model.PaymentSum:0.00} {locale.Currency}.</b>");
            builder.Append("<br>");
            var accountName = GetAccountName(account);
            builder.Append($"Номер аккаунта : {accountName}");
            builder.Append("<br>");
            builder.Append($"E-mail покупателя : {GetAccountAdminEmail(model.AccountId)}");
            if (string.IsNullOrEmpty(accountRequisites.Inn))
            {
                return string.Format(builder.ToString());
            }

            builder.Append("<br>");
            builder.Append(
                $"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Inn, account.Id)} : {accountRequisites.Inn}");

            return string.Format(builder.ToString());
        }

        /// <summary>
        /// Получить электронную почту админа аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Электронная почта админа аккаунта</returns>
        private string GetAccountAdminEmail(Guid accountId)
        {
            var accountAdminIds = dbLayer.AccountsRepository.GetAccountAdminIds(accountId);
            return dbLayer.AccountUsersRepository.Where(accountUser => accountAdminIds.Contains(accountUser.Id))
                .FirstOrDefault()?.Email?? "";
        }

        /// <summary>
        /// Получить название аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Название аккаунта</returns>
        private static string GetAccountName(Account account) => account.AccountCaption == account.IndexNumber.ToString()
            ? account.AccountCaption
            : $"{account.IndexNumber} ({account.AccountCaption})";
    }
}
