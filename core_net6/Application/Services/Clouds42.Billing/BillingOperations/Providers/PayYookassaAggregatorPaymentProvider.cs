﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.YookassaAggregator.Providers.Interfaces;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер для подготовки платежа агрегатора ЮKassa
    /// </summary>
    internal class PayYookassaAggregatorPaymentProvider(
        IUnitOfWork dbLayer,
        ICreateYookassaPaymentObjectProvider createYookassaPaymentObjectProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : IPayYookassaAggregatorPaymentProvider
    {
        /// <summary>
        /// Подготовить платеж агрегатора ЮKassa для клиента
        /// </summary>
        /// <param name="model">Модель для подготовки платежа агрегатора ЮKassa</param>
        /// <returns>Данные для подтверждения платежа ЮKassa клиентом</returns>
        public ConfirmYookassaPaymentForClientDataDto PreparePaymentOrPayWithSavedPaymentMethod(
            PrepareYookassaAggregatorPaymentDto model)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var account = dbLayer.BillingAccountRepository.GetBillingAccount(model.AccountId);

                var paymentNumber = PrepareAndGetPaymentNumber(account, model.PaymentSum, model.IsAutoPay, model.ServiceId);

                var (paymentId, supplierId) =
                    GetDataToCreateYookassaPaymentObject(paymentNumber);
                var paymentMethodToken = GetYookassaPaymentMethodToken(model.PaymentMethodId);

                supplierId = model.SupplierId ?? supplierId;

                var yookassaPaymentObjectDto =
                    CreateYookassaPaymentObject(model.PaymentSum, paymentId,
                        supplierId, paymentNumber, account.Account.IndexNumber , model.IsAutoPay, account.Id, paymentMethodToken);

                UpdatePayment(paymentId,
                    yookassaPaymentObjectDto);

                dbScope.Commit();

                return GenerateConfirmYookassaPaymentForClientDataDto(!string.IsNullOrEmpty(paymentMethodToken)
                        ? null
                        : yookassaPaymentObjectDto.PaymentConfirmationData.ConfirmationToken,
                    paymentId,
                    model.PaymentSum,
                    model.ReturnUrl, yookassaPaymentObjectDto.PaymentConfirmationData?.ConfirmationUrl, yookassaPaymentObjectDto.PaymentConfirmationData?.Type);
            }
            catch (Exception ex)
            {
                logger.Warn(ex,
                    $"[Ошибка подготовки платежа ЮKassa для клиента] '{model.AccountId}'");

                dbScope.Rollback();

                throw;
            }
        }

        /// <summary>
        /// Создать объект платежа в агрегаторе ЮKassa 
        /// </summary>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="paymentId">Id платежа</param>
        /// <param name="supplierId">Id поставщика</param>
        /// <param name="paymentNumber">Номер платежа</param>
        /// <param name="paymentMethodToken"></param>
        /// <returns>Объект платежа в агрегаторе ЮKassa </returns>
        private YookassaPaymentObjectDto CreateYookassaPaymentObject(decimal paymentSum,
            Guid paymentId, Guid supplierId, int paymentNumber, int indexnumber, bool isAutoPay, Guid accountId,
            string? paymentMethodToken = null)
        {
            var accountsEmail = dbLayer.AccountsRepository.GetEmails(accountId);

            return createYookassaPaymentObjectProvider.Create(
                new YookassaPaymentObjectCreationDataDto
                {
                    PaymentSum = paymentSum,
                    Description = $"Платеж №{paymentNumber} Id компании {indexnumber}, Email {accountsEmail.FirstOrDefault()}",
                    IdempotencyKey = $"{paymentId:N}",
                    SupplierId = supplierId,
                    AggregatorPaymentMethodId = paymentMethodToken,
                    IsAutoPay = isAutoPay
                }) ?? throw new InvalidOperationException(
                $"Не удалось создать объект платежа ЮKassa, для системного платежа №{paymentNumber}");
        }
           

        /// <summary>
        /// Получить данные для создания объекта платежа ЮKassa
        /// </summary>
        /// <param name="paymentNumber">Номер платежа</param>
        /// <returns>Данные для создания объекта платежа ЮKassa</returns>
        private (Guid PaymentId, Guid SupplierId) GetDataToCreateYookassaPaymentObject(int paymentNumber)
        {
            var data = dbLayer.PaymentRepository
                .WhereLazy()
                .Join(dbLayer.AccountConfigurationRepository.WhereLazy(), z => z.AccountId, x => x.AccountId,
                    (payment, configuration) => new { payment, configuration })
                .FirstOrDefault(x => x.payment.Number == paymentNumber) ??
                       throw new NotFoundException(
                           $"Не удалось получить данные для создания объекта платежа ЮKassa по номеру {paymentNumber}");

            return (PaymentId: data.payment.Id, data.configuration.SupplierId);
            
        }

        /// <summary>
        /// Сформировать модель данных
        /// для подтверждения платежа ЮKassa клиентом
        /// </summary>
        /// <param name="confirmationToken">Токен подтверждения</param>
        /// <param name="paymentId">Id системного платежа</param>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="returnUrl">Урл на который пользователь вернется после оплаты</param>
        /// <param name="redirectUrl">Урл на который пользователя надо редиректить для подтверждения</param>
        /// <param name="type">тип подтверждения</param>
        /// <returns>Данные для подтверждения платежа ЮKassa клиентом</returns>
        private static ConfirmYookassaPaymentForClientDataDto GenerateConfirmYookassaPaymentForClientDataDto(string? confirmationToken, 
            Guid paymentId, decimal paymentSum, string returnUrl, string? redirectUrl, string? type) =>
            new()
            {
                PaymentId = paymentId,
                TotalSum = paymentSum,
                PaymentSystem = PaymentSystem.Yookassa.ToString(),
                ConfirmationToken = confirmationToken,
                ReturnUrl = returnUrl,
                RedirectUrl = redirectUrl,
                Type = type
            };

        /// <summary>
        /// Получить токен сохраненного способа оплаты аггрегатора YooKassa
        /// </summary>
        /// <param name="paymentMethodId"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        private string? GetYookassaPaymentMethodToken(Guid? paymentMethodId)
        {
            if (paymentMethodId == null)
            {
                return null;
            }

            var paymentMethod = dbLayer.SavedPaymentMethodRepository.FirstOrDefault(apm => apm.Id == paymentMethodId);

            if (paymentMethod == null)
                throw new NotFoundException($"{nameof(paymentMethod)} with Id: {paymentMethodId} was not found");

            var paymentMethodToken = paymentMethod.Token;

            return paymentMethodToken;

        }

        private void UpdatePayment(Guid paymentId, YookassaPaymentObjectDto yookassaPaymentObjectDto)
        {
            try
            {
                var payment = dbLayer.PaymentRepository.GetById(paymentId);
                payment.PaymentSystemTransactionId = yookassaPaymentObjectDto.Id.ToString();

                payment.ConfirmationRedirectUrl = yookassaPaymentObjectDto.PaymentConfirmationData?.ConfirmationUrl;
                payment.ConfirmationToken = yookassaPaymentObjectDto.PaymentConfirmationData?.ConfirmationToken;
                payment.ConfirmationType = yookassaPaymentObjectDto.PaymentConfirmationData?.Type;

                dbLayer.PaymentRepository.Update(payment);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,$"[Возникла непредвиденная ошибка при установке значения] " +
                                        $"{nameof(yookassaPaymentObjectDto.Id)} с Id: {yookassaPaymentObjectDto.Id} для платежа с Id: {paymentId}");
            }
        }

        private int PrepareAndGetPaymentNumber(BillingAccount? account, decimal sum, bool isAutoPay, Guid? serviceId = null)
        {
            if (account == null)
            {
                var message = $"При создании платежа для компании(ID) на сумму {sum} по системе " +
                        $"{PaymentSystem.Yookassa} - Причина: нет аккаунта";
                handlerException.HandleWarning(message, "[Нет аккауанта для создания платежа]");

                return -1;
            }

            var payment = new Domain.DataModels.billing.Payment
            {
                AccountId = account.Id,
                Sum = sum,
                Status = PaymentStatus.Waiting.ToString(),
                PaymentSystem = PaymentSystem.Yookassa.ToString(),
                OriginDetails = $"{PaymentSystem.Yookassa} import",
                Date = DateTime.Now,
                Description = $"Ввод средств через платежную систему {PaymentSystem.Yookassa}",
                OperationType = PaymentType.Inflow.ToString(),
                Id = Guid.NewGuid(),
                IsAutoPay = isAutoPay,
                BillingServiceId = serviceId
            };
            var num = dbLayer.PaymentRepository.PreparePayment(payment);

            logger.Info($"Номер платежа ({num}), Успешно создан платеж для компании(ID) {account.Id} на сумму {sum} по системе {PaymentSystem.Yookassa}");

            return num;
        }
    }
}
