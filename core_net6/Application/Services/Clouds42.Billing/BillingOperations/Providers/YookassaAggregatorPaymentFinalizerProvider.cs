﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingOperations.Validators;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Newtonsoft.Json;
using Serilog.Events;

namespace Clouds42.Billing.BillingOperations.Providers
{
    public class YookassaAggregatorPaymentFinalizerProvider(
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        ConfirmPaymentManager confirmPaymentManager,
        PaymentReceiptManager paymentReceiptManager,
        ISendLetterToManagerAboutYookassaPaymentProvider sendLetterToManagerAboutYookassaPaymentProvider,
        IYookassaAggregatorPaymentDataProvider yookassaAggregatorPaymentDataProvider,
        ISavedPaymentMethodsDataProvider savedPaymentMethodsDataProvider,
        ICloudChangesProvider cloudChangesProvider,
        ILogger42 logger42,
        IAccessProvider accessProvider)
        : IYookassaAggregatorPaymentFinalizerProvider
    {
        public bool FinalizePayment(YookassaFinalPaymentObjectDto paymentObject)
        {
            try
            {
                var yookassaPaymentId = paymentObject?.Id ?? throw new ValidateException("Не указан Id платежа ЮKassa");

                var payment = unitOfWork.PaymentRepository.GetByPaymentSystemTransactionId(yookassaPaymentId.ToString());
                if (payment.Status != PaymentStatus.Waiting.ToString())
                    return true;

                var newPaymentStatus = MapYookassaPaymentStatusToPaymentStatus(paymentObject.Status);

                YookassaAggregatorPaymentFinalizerValidator.ValidateYookassaPaymentStatus(newPaymentStatus, payment.Status);

                logger42.Info($"{yookassaPaymentId} Cтатус платежа: {newPaymentStatus},  Является автоплатежем: {payment.IsAutoPay}, Метод оплаты Yookassa: {JsonConvert.SerializeObject(paymentObject.PaymentMethod)}");

                if (newPaymentStatus == PaymentStatus.Done.ToString())
                {
                    var savedPaymentMethod = CompletePayment(payment, paymentObject);
                    logger42.Info($"{yookassaPaymentId} Cтатус платежа: {newPaymentStatus},  Найденный сохраненный способ оплаты {savedPaymentMethod?.Id.ToString() ?? "отсутствует"}");

                    if (!payment.IsAutoPay || savedPaymentMethod == null)
                    {
                        return true;
                    }

                    logger42.Info($"{yookassaPaymentId} Начинаем создание конфигурации для автоплатежа");

                    var isSuccess = savedPaymentMethodsDataProvider.MakeDefault(new AccountPaymentMethodDto
                    {
                        AccountId = payment.AccountId, PaymentMethodId = savedPaymentMethod.Id
                    });



                    if (isSuccess)
                    {
                        logger42.Info($"{yookassaPaymentId} Результат создания конфигурации автоплатежа: {isSuccess}");
                        logger42.Info($"Вхожу в логгер с параметрами {payment.AccountId}, {savedPaymentMethod.Title}");
                        LogEventHelper.LogEvent(unitOfWork, payment.AccountId,accessProvider, LogActions.RefillByPaymentSystem, $"Добавлена карта {savedPaymentMethod.Title} для автоплатежа." );
                    }
                }

                else
                {
                    payment.Status = newPaymentStatus;
                    payment.ConfirmationToken = paymentObject.Confirmation?.ConfirmationToken;
                    payment.ConfirmationRedirectUrl = paymentObject.Confirmation?.ConfirmationRedirectUrl;
                    payment.ConfirmationType = paymentObject.Confirmation?.Type;

                    unitOfWork.PaymentRepository.Update(payment);
                    unitOfWork.Save();
                }

                return true;
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,"[При финализации платежа произошла непредвиденная ошибка.]");
                return false;
            }
        }

        private SavedPaymentMethod? CompletePayment(Domain.DataModels.billing.Payment payment, YookassaFinalPaymentObjectDto paymentObject)
        {
            YookassaAggregatorPaymentFinalizerValidator.ValidateSucceededPayment(paymentObject);
            logger42.Info($"{paymentObject.Id} Провели валидацию платежа");
            var result = confirmPaymentManager.ConfirmPaymentAnonymous(payment.Number, payment.Sum, true);
            logger42.Info($"{paymentObject.Id} Подтверждение платежа закончилось с результатом {result.Result}");
            paymentReceiptManager.CreateFiscalReceiptTask(payment.Number);

            YookassaAggregatorPaymentFinalizerValidator.ValidatePaymentMethod(paymentObject.PaymentMethod);
            
            SavedPaymentMethod? savedPaymentMethod = null;
            if (paymentObject.PaymentMethod?.IsSaved ?? false)
                savedPaymentMethod = yookassaAggregatorPaymentDataProvider.SaveYookassaAccountPaymentMethodAndBankCardInfoIfExist(paymentObject.PaymentMethod, paymentObject.Id);

            sendLetterToManagerAboutYookassaPaymentProvider.SendLetter(new SendLetterToManagerAboutYookassaPaymentDto
            {
                AccountId = payment.AccountId,
                PaymentSum = payment.Sum,
                PaymentDate = payment.Date
            });

            return savedPaymentMethod;
        }

        private static string MapYookassaPaymentStatusToPaymentStatus(string yookassaPaymentStatus)
        {
            return yookassaPaymentStatus.GetEnumByStringValue<YookassaPaymentStatusEnum>() switch
            {
                YookassaPaymentStatusEnum.Pending => PaymentStatus.Waiting.ToString(),
                YookassaPaymentStatusEnum.WaitingForCapture => PaymentStatus.Waiting.ToString(),
                YookassaPaymentStatusEnum.Canceled => PaymentStatus.Canceled.ToString(),
                YookassaPaymentStatusEnum.Succeeded => PaymentStatus.Done.ToString(),
                _ => throw new ValidationException(
                    $"Не удалось определить статус платежа на стороне агрегатора. Статус: {yookassaPaymentStatus}")
            };
        }
    }
}
