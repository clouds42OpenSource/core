﻿using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Billing.BillingOperations.Constants;
using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер для калькуляции агентского вознаграждения
    /// </summary>
    internal class CalculateAgentRewardProvider(
        ICalculateAgentRewardDataProvider calculateAgentRewardDataProvider,
        IAgencyAgreementDataProvider agencyAgreementDataProvider)
        : ICalculateAgentRewardProvider
    {
        /// <summary>
        /// Посчитать агентское вознаграждение
        /// </summary>
        /// <param name="clientPaymentId">Номер клиенской платежной транзакции,
        /// на основание данного платежа будет производится начисление агенту.</param>
        /// <returns>Результат расчета агентского вознаграждения</returns>
        public CalculateAgentRewardResultDto Calculate(Guid clientPaymentId)
        {
            var calculateAgentRewardData =
                calculateAgentRewardDataProvider.GetDataFoCalculateAgentReward(clientPaymentId);

            if (!CalculateAgentRewardHelper.CheckAbilityToAccrueAgentReward(calculateAgentRewardData))
                return new CalculateAgentRewardResultDto(0, calculateAgentRewardData.AgentAccountId);

            var rewardSum = CalculateAgentReward(calculateAgentRewardData.PaymentSum,
                calculateAgentRewardData.IsVipClientAccount, calculateAgentRewardData.SystemService);

            return new CalculateAgentRewardResultDto(rewardSum, calculateAgentRewardData.AgentAccountId);
        }

        /// <summary>
        /// Посчитать агентское вознаграждение
        /// </summary>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="isVipClientAccount">Аккаунт клиента является ВИП</param>
        /// <param name="systemService">Тип системного сервиса</param>
        /// <returns>Результат расчета агентского вознаграждения</returns>
        public decimal Calculate(decimal paymentSum, bool isVipClientAccount, Clouds42Service? systemService)
        {
            return !CalculateAgentRewardHelper.CheckAbilityToAccrueAgentRewardBySystemService(systemService) ? 0 : CalculateAgentReward(paymentSum, isVipClientAccount, systemService);
        }

        /// <summary>
        /// Посчитать вознаграждение агента
        /// </summary>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="isVipClientAccount">Аккаунт клиента является ВИП</param>
        /// <param name="systemService">Тип системного сервиса</param>
        /// <returns>Вознаграждение агента</returns>
        private decimal CalculateAgentReward(decimal paymentSum, bool isVipClientAccount,
            Clouds42Service? systemService)
        {
            return GenerateAgentRewardCalculators(paymentSum, systemService, isVipClientAccount);
        }

        /// <summary>
        /// Создать функцию для калькуляции агентского вознаграждения
        /// </summary>
        /// <param name="paymentSum"></param>
        /// <param name="rewardPercent">Вознаграждение в процентах</param>
        /// <returns>Функцию для калькуляции агентского вознаграждения</returns>
        private static decimal CreateCalculationFunc(decimal paymentSum, decimal rewardPercent)
        {
            return paymentSum * (rewardPercent / CalculateAgentRewardConst.MaximumRewardPercent);
        }

        private decimal GenerateAgentRewardCalculators(decimal paymentSum, Clouds42Service? systemService, bool isVip)
        {
            var actualAgencyAgreement = agencyAgreementDataProvider.GetActualAgencyAgreement();

            switch (systemService)
            {
                case Clouds42Service.Unknown:
                    break;
                case Clouds42Service.Recognition:
                    break;
                case Clouds42Service.Esdl:
                    break;
                case Clouds42Service.MyDatabases:
                    break;
                case null when isVip:
                    return CreateCalculationFunc(paymentSum, actualAgencyAgreement.ServiceOwnerRewardPercentForVipAccounts);
                case null when !isVip:
                    return CreateCalculationFunc(paymentSum, actualAgencyAgreement.ServiceOwnerRewardPercent);
                case Clouds42Service.MyEnterprise when isVip:
                    return CreateCalculationFunc(paymentSum, actualAgencyAgreement.Rent1CRewardPercentForVipAccounts);
                case Clouds42Service.MyEnterprise when !isVip:
                    return CreateCalculationFunc(paymentSum, actualAgencyAgreement.Rent1CRewardPercent);
                case Clouds42Service.MyDisk when isVip:
                    return CreateCalculationFunc(paymentSum, actualAgencyAgreement.MyDiskRewardPercentForVipAccounts);
                case Clouds42Service.MyDisk when !isVip:
                    return CreateCalculationFunc(paymentSum, actualAgencyAgreement.MyDiskRewardPercent);
                default:
                    throw new ArgumentOutOfRangeException(nameof(systemService), systemService, null);
            }

            return 0;
        }
    }
}
