﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Newtonsoft.Json;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер для работы с данными платежей агрегатора ЮKassa
    /// </summary>
    internal class YookassaAggregatorPaymentDataProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ILogger42 logger)
        : IYookassaAggregatorPaymentDataProvider
    {
        /// <summary>
        /// Получить зависшие(не обработанные) платежи
        /// за период
        /// </summary>
        /// <param name="periodFrom">Дата периода с</param>
        /// <param name="periodTo">Дата периода по</param>
        /// <returns>Зависшие(не обработанные) платежи за период</returns>
        public IEnumerable<Domain.DataModels.billing.Payment> GetPendingPayments(DateTime periodFrom,
            DateTime periodTo) => dbLayer.GetGenericRepository<Domain.DataModels.billing.Payment>().WhereLazy(p =>
            p.Status == PaymentStatus.Waiting.ToString() && p.Date >= periodFrom &&
            p.Date <= periodTo).AsEnumerable();

        /// <summary>
        /// Получить платеж системы по Id платежа ЮKassa
        /// или выкинуть исключение
        /// </summary>
        /// <param name="yookassaPaymentId">Id платежа ЮKassa</param>
        /// <returns>Платеж системы</returns>
        public Domain.DataModels.billing.Payment GetPaymentByYookassaPaymentIdOrThrowException(Guid yookassaPaymentId) =>
            dbLayer.PaymentRepository.FirstOrDefault(p => p.PaymentSystemTransactionId == yookassaPaymentId.ToString())
            ?? throw new NotFoundException($"Не удалось получить платеж по Id платежу ЮKassa '{yookassaPaymentId}'");

        /// <summary>
        /// Получить сервис по Id 
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис</returns>
        public BillingService GetService(Guid? serviceId) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(p => p.Id == serviceId);

        /// <summary>
        /// Получить идентификатор поставщика 
        /// </summary>
        /// <param name="supplierName">название поставщика</param>
        /// <returns>идентификатор поставщика</returns>
        public Guid GetSupplier(string supplierName) =>
            dbLayer.SupplierRepository.FirstOrDefault(s => s.Name == supplierName).Id;

        /// <summary>
        /// Сохраняет данные о способе оплаты пользователя
        /// </summary>
        /// <param name="model">Сохраненный способ оплаты</param>
        /// <param name="yookassaPaymentId">Идентификатор юкассы</param>
        /// <returns>Сохраненный способ оплаты</returns>
        public SavedPaymentMethod? SaveYookassaAccountPaymentMethodAndBankCardInfoIfExist(YookassaPaymentMethodDto model, Guid yookassaPaymentId)
        {

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var payment = dbLayer.PaymentRepository.GetByPaymentSystemTransactionId(yookassaPaymentId.ToString());

                if (payment == null)
                    throw new NotFoundException($"Не удалось получить платеж по Id платежу ЮKassa '{yookassaPaymentId}'");

                if (model.Card != null)
                {
                    logger.Info($"{yookassaPaymentId} Платеж был с картой {JsonConvert.SerializeObject(model.Card)}, начинаем поиск существующей карты и сохраненного метода оплаты");

                    var savedPayment = dbLayer.SavedPaymentMethodBankCardRepository
                       .AsQueryable()
                       .FirstOrDefault(x => x.CardType == model.Card.CardType &&
                                            x.ExpiryMonth == model.Card.ExpiryMonth &&
                                            x.ExpiryYear == model.Card.ExpiryYear &&
                                            x.FirstSixDigits == model.Card.FirstSixDigits &&
                                            x.LastFourDigits == model.Card.LastFourDigits)?.SavedPaymentMethod;

                    var message = savedPayment != null
                        ? $"Найден платежный метод {savedPayment.Id}"
                        : "Платежный метод не найден, производим сохранение карты и платежного метода";

                    logger.Info($"{yookassaPaymentId} {message}");

                    if (savedPayment != null)
                        return savedPayment;
                }

                var savedPaymentMethod = dbLayer.SavedPaymentMethodRepository.FirstOrDefault(x => x.Token == model.PaymentTemplateId.ToString());
                
                if (savedPaymentMethod == null)
                {
                    savedPaymentMethod = CreateYookassaSavedPaymentMethod(model, payment.AccountId);

                    if (model.Card != null)
                        CreateYookassaSavedPaymentMethodBankCard(model.Card, savedPaymentMethod.Id);
                }

                dbScope.Commit();

                return savedPaymentMethod;
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Сохранение платежных данных ЮKassa для клиента по платежу '{yookassaPaymentId}' завершилась ошибкой]");

                dbScope.Rollback();
                throw;
            }
        }

        private SavedPaymentMethod CreateYookassaSavedPaymentMethod(YookassaPaymentMethodDto paymentMethodInfo, Guid accountId)
        {
            var savedPaymentMethod = new SavedPaymentMethod
            {
                AccountId = accountId,
                PaymentSystem = PaymentSystem.Yookassa.ToString(),
                Type = paymentMethodInfo.Type,
                CreatedOn = DateTime.UtcNow,
                Title = GetTitle(paymentMethodInfo.Type, paymentMethodInfo.Card),
                Token = paymentMethodInfo.PaymentTemplateId.ToString(),
                Metadata = paymentMethodInfo.ToJson()
            };

            dbLayer.SavedPaymentMethodRepository.Insert(savedPaymentMethod);
            dbLayer.Save();

            return savedPaymentMethod;
        }

        private void CreateYookassaSavedPaymentMethodBankCard(YookassaPaymentCardDto card, Guid paymentMethodId)
        {
            var bankCard = new SavedPaymentMethodBankCard
            {
                CardType = card.CardType,
                FirstSixDigits = card.FirstSixDigits,
                ExpiryMonth = card.ExpiryMonth,
                ExpiryYear = card.ExpiryYear,
                LastFourDigits = card.LastFourDigits,
                SavedPaymentMethodId = paymentMethodId
            };

            dbLayer.SavedPaymentMethodBankCardRepository.Insert(bankCard);
            dbLayer.Save();
        }

        private static string GetTitle(string type, YookassaPaymentCardDto card)
        {
            return type == SavedPaymentMethodType.BankCard.Description() ? $"**** **** **** {card.LastFourDigits}" : string.Empty;
        }
    }
}
