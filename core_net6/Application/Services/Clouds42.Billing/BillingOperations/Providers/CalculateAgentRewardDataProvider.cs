﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер для работы с данными калькуляции вознаграждения агента
    /// </summary>
    internal class CalculateAgentRewardDataProvider(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : ICalculateAgentRewardDataProvider
    {
        /// <summary>
        /// Получить данные для калькуляции агентского вознаграждения
        /// </summary>
        /// <param name="clientPaymentId">Id клиенской платежной транзакции</param>
        /// <returns>Данные для калькуляции агентского вознаграждения</returns>
        public CalculateAgentRewardDataDto GetDataFoCalculateAgentReward(Guid clientPaymentId) =>
            (from payment in dbLayer.PaymentRepository.WhereLazy()
             join service in dbLayer.BillingServiceRepository.WhereLazy() on payment.BillingServiceId
                 equals service.Id
             join accountWithConfiguration in accountConfigurationDataProvider.GetAccountsWithConfiguration() on
                 payment.AccountId equals accountWithConfiguration.Account.Id
             where payment.Id == clientPaymentId
             select new CalculateAgentRewardDataDto
             {
                 PaymentSum = payment.Sum,
                 TransactionType = payment.TransactionType,
                 SystemService = service.SystemService,
                 ServiceOwnerId = service.AccountOwnerId,
                 ReferralAccountId = accountWithConfiguration.Account.ReferralAccountID,
                 IsVipClientAccount = accountWithConfiguration.AccountConfiguration.IsVip,
                 AgentAccountId = service.AccountOwnerId != null && service.SystemService == null
                     ? service.AccountOwnerId
                     : accountWithConfiguration.Account.ReferralAccountID
             }).FirstOrDefault() ??
            throw new NotFoundException(
                $"По номеру '{clientPaymentId}' не удалось найти данные клиентского платежа.");
    }
}