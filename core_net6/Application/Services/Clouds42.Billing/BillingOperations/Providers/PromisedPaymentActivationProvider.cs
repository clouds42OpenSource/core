﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Microsoft.Extensions.Configuration;
using BillingAccountEntity = Clouds42.Domain.DataModels.billing.BillingAccount;
using InvoiceEntity = Clouds42.Domain.DataModels.billing.Invoice;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Contracts.DataManagers.Payments.Internal;
using Clouds42.Common.Exceptions;
using Clouds42.Logger;
using Clouds42.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <inheritdoc/>
    public class PromisedPaymentActivationProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IInflowPaymentProcessor inflowPaymentProcessor,
        IBillingPaymentsProvider billingPaymentsProvider,
        IConfiguration configuration,
        ILogger42 logger,
        IBillingDataProvider billingDataProvider)
        : IPromisedPaymentActivationProvider
    {
        /// <inheritdoc/>
        public async Task ActivatePromisedPaymentAsync(Guid accountId, Guid invoiceId, bool prolongServices = true)
        {
            var billingAccount = await dbLayer.BillingAccountRepository
               .GetBillingAccountOrThrowAsync(accountId);
            var locale = await dbLayer.AccountsRepository
                .GetLocaleOrThrowAsync(accountId, Guid.Parse(configuration["DefaultLocale"]?? ""));

            InvoiceEntity? invoice = null;
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                invoice = await dbLayer.InvoiceRepository.GetByIdAsync(invoiceId);
                if (invoice == null || invoice.AccountId != accountId)
                    throw new NotFoundException($"Инвойс {invoiceId} не найден для указанного аккаунта");
                if (invoice.StateEnum != InvoiceStatus.Processing)
                    throw new InvalidOperationException($"Инвойс {invoice.Uniq} имеет финальный статус");

                billingAccount.ActivatePromisedPayment(invoice.Sum);
                await dbLayer.SaveAsync();

                if (!billingAccount.PromisePaymentDate.HasValue ||
               !billingAccount.PromisePaymentSum.HasValue ||
               billingAccount.PromisePaymentSum <= 0)
                    throw new InvalidOperationException(
                        $"Не удалось создать транзакцию обещанного платежа для аккаунта {billingAccount.Id}. Обещаный платеж не активирован.");

                AddPaymentEntity(billingAccount, "Активация обещанного платежа", billingAccount.PromisePaymentSum.Value);
                await dbLayer.SaveAsync();

                if (prolongServices)
                {
                    inflowPaymentProcessor.Process(billingAccount.Id, false);
                    await dbLayer.SaveAsync();
                }

                transaction.Commit();
                LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.TakePromisePayment,
                    $"Активирован обещанный платеж на сумму {invoice.Sum:0} {locale.Currency}.");
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                string logErrorMessage = invoice != null ?
                    $"Ошибка взятия обещанного платежа для инвойса {invoice.Uniq} на сумму {invoice.Sum:0} {locale.Currency}" :
                    $"Ошибка взятия обещанного платежа для инвойса {invoiceId}";
                logger.Warn(ex, $"[Ошибка активации обещанного платежа аккаунта {accountId} для инвойса {invoiceId}]");
                LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.TakePromisePayment, logErrorMessage);
                throw;
            }
        }

        /// <summary>
        /// Увеличение обещанного платежа для аккаунта на основании инвойса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoiceId">ID исходного инвойса</param>
        /// <param name="prolongServices">Нужно ли продливать сервисы после успешной активации</param>
        /// <returns></returns>
        public async Task IncreasePromisedPaymentAsync(Guid accountId, Guid invoiceId, bool prolongServices = true)
        {
            var billingAccount = await dbLayer.BillingAccountRepository
               .GetBillingAccountOrThrowAsync(accountId);
            var locale = await dbLayer.AccountsRepository
                .GetLocaleOrThrowAsync(accountId, Guid.Parse(configuration["DefaultLocale"] ?? ""));

            var promisePaymentDays = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");

            var regularPayment = billingDataProvider.GetAccountRegularPayment(accountId);

            InvoiceEntity? invoice = null;
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                invoice = await dbLayer.InvoiceRepository.GetByIdAsync(invoiceId);
                if (invoice == null || invoice.AccountId != accountId)
                    throw new NotFoundException($"Инвойс {invoiceId} не найден для указанного аккаунта");
                if (invoice.StateEnum != InvoiceStatus.Processing)
                    throw new InvalidOperationException($"Инвойс {invoice.Uniq} имеет финальный статус");

                billingAccount.PromisePaymentSum = billingAccount.PromisePaymentSum + invoice.Sum;

                if(billingAccount.PromisePaymentSum > (regularPayment*2))
                    throw new InvalidOperationException($"Превышен лимит {(regularPayment * 2):0.0} допустимого увеличения обещанного платежа");

                if (billingAccount.PromisePaymentDate.HasValue && DateTime.Now >= billingAccount.PromisePaymentDate.Value.AddDays(promisePaymentDays))
                    throw new InvalidOperationException($"Обещанный платеж не активирован или просрочен");

                await dbLayer.SaveAsync();

                AddPaymentEntity(billingAccount, "Увеличение обещанного платежа", invoice.Sum);
                await dbLayer.SaveAsync();

                if (prolongServices)
                {
                    inflowPaymentProcessor.Process(billingAccount.Id, false);
                    await dbLayer.SaveAsync();
                }

                transaction.Commit();
                LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.TakePromisePayment,
                    $"Увеличен обещанный платеж на сумму {invoice.Sum:0} {locale.Currency} и  равен {billingAccount.PromisePaymentSum:0} {locale.Currency}.");
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                string logErrorMessage = invoice != null ?
                    $"Ошибка увеличения обещанного платежа для инвойса {invoice.Uniq} на сумму {invoice.Sum:0} {locale.Currency}" :
                    $"Ошибка увеличения обещанного платежа для инвойса {invoiceId}";
                logger.Warn(ex, $"[Ошибка увеличения обещанного платежа аккаунта {accountId} для инвойса {invoiceId}]");
                LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.TakePromisePayment, logErrorMessage);
                throw;
            }
        }

        private void AddPaymentEntity(BillingAccountEntity billingAccount, string description, decimal sum)
        {
            if (!billingAccount.PromisePaymentDate.HasValue ||
                !billingAccount.PromisePaymentSum.HasValue ||
                billingAccount.PromisePaymentSum <= 0)
                throw new InvalidOperationException(
                    $"Не удалось создать транзакцию обещанного платежа для аккаунта {billingAccount.Id}. Обещаный платеж не активирован.");

            var date = billingAccount.PromisePaymentDate.Value;

            var paymentDefinition = new PaymentDefinitionDto
            {
                System = PaymentSystem.ControlPanel,
                Account = billingAccount.Id,
                Date = date,
                Description = description,
                Status = PaymentStatus.Done,
                Total = sum,
                OperationType = PaymentType.Inflow,
                OriginDetails = "PromisePayment"
            };
            var createResult = billingPaymentsProvider.CreatePayment(paymentDefinition);
            if (createResult != PaymentOperationResult.Ok)
                throw new InvalidOperationException($"Не удалось создать платеж входящий платеж на сумму:{sum}");
        }

    }
}
