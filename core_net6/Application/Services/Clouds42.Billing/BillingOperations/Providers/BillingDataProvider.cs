﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.BillingOperations.Mappers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Billing.Rate;
using Clouds42.Locales;
using Clouds42.Locales.Extensions;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using CommonLib.Enums;
using PagedList.Core;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.Domain.Access;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using AccountEntity = Clouds42.Domain.DataModels.Account;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер данных биллинга
    /// </summary>
    internal class BillingDataProvider : IBillingDataProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly IBillingServiceInfoProvider _billingServiceInfoProvider;
        private readonly ICloudLocalizer _cloudLocalizer;
        private readonly IMyDatabasesServiceTypeResourcesDataProvider _myDatabasesServiceTypeResourcesDataProvider;
        private readonly IAdditionalResourcesDataProvider _additionalResourcesDataProvider;
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider;
        private readonly Lazy<string> _invoiceUniqPrefix;
        private readonly Lazy<int> _myDiskFreeTariffSize;
        private readonly IDictionary<PayPeriod, int> _mapPayPeriodToBonusReward;

        public BillingDataProvider(IUnitOfWork dbLayer,
            IBillingServiceInfoProvider billingServiceInfoProvider,
            ICloudLocalizer cloudLocalizer,
            IMyDatabasesServiceTypeResourcesDataProvider myDatabasesServiceTypeResourcesDataProvider,
            IAdditionalResourcesDataProvider additionalResourcesDataProvider,
            IAccountConfigurationDataProvider accountConfigurationDataProvider)
        {
            _dbLayer = dbLayer;
            _billingServiceInfoProvider = billingServiceInfoProvider;
            _myDatabasesServiceTypeResourcesDataProvider = myDatabasesServiceTypeResourcesDataProvider;
            _additionalResourcesDataProvider = additionalResourcesDataProvider;
            _accountConfigurationDataProvider = accountConfigurationDataProvider;
            _cloudLocalizer = cloudLocalizer;
            _myDiskFreeTariffSize =
                new Lazy<int>(CloudConfigurationProvider.BillingServices.MyDisk.GetMyDiskFreeTariffSize);
            _invoiceUniqPrefix = new Lazy<string>(CloudConfigurationProvider.Invoices.UniqPrefix);

            var bonusRewardFor3Month =
                new Lazy<int>(CloudConfigurationProvider.Invoices.GetBonusRewardFor3MonthPayPeriod);
            var bonusRewardFor6Month =
                new Lazy<int>(CloudConfigurationProvider.Invoices.GetBonusRewardFor6MonthPayPeriod);
            var bonusRewardFor12Month =
                new Lazy<int>(CloudConfigurationProvider.Invoices.GetBonusRewardFor12MonthPayPeriod);

            _mapPayPeriodToBonusReward = new Dictionary<PayPeriod, int>
            {
                {PayPeriod.Month1, 0},
                {PayPeriod.Month3, bonusRewardFor3Month.Value},
                {PayPeriod.Month6, bonusRewardFor6Month.Value},
                {PayPeriod.Month12, bonusRewardFor12Month.Value}
            };
        }

        /// <summary>
        /// Получить регулярный платеж аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Регулярный платеж аккаунта</returns>
        public decimal GetAccountRegularPayment(Guid accountId) =>
            GetRegularPaymentForAccounts().FirstOrDefault(rp => rp.AccountId == accountId)?.RegularPaymentSum ??
            0;

        /// <summary>
        /// Получить информацию об аккаунте биллинга
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Информацию об аккаунте биллинга</returns>
        public PagitationCollection<InvoiceInfoDto> GetInvoicesForAccount(Guid accountId, int page = 1)
        {
            const int pageCount = 5;
            var invoiceIgnoreStatus = InvoiceStatus.Canceled.ToString();
            var invoices = _dbLayer.InvoiceRepository
                .WhereLazy(i => i.AccountId == accountId && i.State != invoiceIgnoreStatus)
                .OrderByDescending(o => o.Date).ToPagedList(page, pageCount);

            var invoicesCollection = new List<InvoiceInfoDto>();

            invoices.ToList().ForEach(invoice => { invoicesCollection.Add(invoice.MapToInvoiceInfoDc()); });

            return new PagitationCollection<InvoiceInfoDto>(invoicesCollection,
                new PaginationBaseDto(page, invoices.TotalItemCount, pageCount));
        }

        /// <summary>
        /// Получить информацию об аккаунте биллинга
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="request"></param>
        /// <returns>Информацию об аккаунте биллинга</returns>
        public SelectDataResultCommonDto<InvoiceInfoDto> GetPaginatedInvoicesForAccount(Guid accountId,
            SelectDataCommonDto<InvoicesFilterDto> request)
        {
            const int pageCount = 5;
            var invoiceIgnoreStatus = InvoiceStatus.Canceled.ToString();
            var invoices = _dbLayer.InvoiceRepository
                .WhereLazy(i => i.AccountId == accountId && i.State != invoiceIgnoreStatus)
                .OrderByDescending(o => o.Date).ToPagedList(request?.PageNumber ?? 1, pageCount);

            var invoicesCollection = invoices.ToList().Select(x => x.MapToInvoiceInfoDc());

            return new SelectDataResultCommonDto<InvoiceInfoDto>
            {
                Pagination = new PaginationBaseDto(request?.PageNumber ?? 1, invoices.TotalItemCount, pageCount),
                Records = invoicesCollection.ToArray()
            };
        }

        /// <summary>
        /// Получить список транзакций аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="filter">Фильтр поиска</param>
        /// <returns>Список транзакций аккаунта</returns>
        public BillingOperationDataDto GetTransactionsPagers(Guid accountId,
            ProvidedServiceAndTransactionsFilterDto filter)
        {
            var pageSize = 10;
            var query = GetAccountPayments(accountId, filter).Where(o => o.Sum != 0).OrderByDescending(o => o.Date);
            var transactionsPagedList = query.ToPagedList(filter.PageNumber, pageSize);
            var moneyDifference = GetDifferenceSumByFilter(filter, accountId, TransactionType.Money);
            var bonusDifference = GetDifferenceSumByFilter(filter, accountId, TransactionType.Bonus);

            var list = transactionsPagedList.ToList().Select(w => w.MapToBillingOperationItemDm()).ToList();
            var accountLocale = _accountConfigurationDataProvider.GetAccountLocale(accountId);
            return new BillingOperationDataDto
            {
                ShowBalance = filter.Service == null,
                Locale = new LocaleDto
                {
                    Name = accountLocale.Name,
                    Currency = accountLocale.Currency
                },
                CurrentBalance = GetAccountBalance(accountId) + moneyDifference,
                BonusBalance = GetAccountBalance(accountId, TransactionType.Bonus) + bonusDifference,
                Transactions = new PagitationCollection<BillingOperationItemDto>(list,
                    new PaginationBaseDto(filter.PageNumber, transactionsPagedList.TotalItemCount, pageSize))
            };
        }


        /// <summary>
        /// Получить список транзакций аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список транзакций аккаунта</returns>
        public SelectDataResultCommonDto<BillingOperationItemDto> GetPaginatedTransactionsForAccount(
            Guid accountId,
            SelectDataCommonDto<PaymentsFilterDto> request)
        {
            var pageSize = 10;
            var filter = new ProvidedServiceAndTransactionsFilterDto
            {
                PageNumber = request?.PageNumber ?? 1,
                DateFrom = request?.Filter?.DateFrom,
                DateTo = request?.Filter?.DateTo,
                Service = request?.Filter?.Service
            };
            var query = GetAccountPayments(accountId, filter).Where(o => o.Sum != 0).OrderByDescending(o => o.Date);
            var transactionsPagedList = query.ToPagedList(filter.PageNumber, pageSize);

            var list = transactionsPagedList.AsQueryable()
                .Select(w => w.MapToBillingOperationItemDm())
                .ToList();


            return new SelectDataResultCommonDto<BillingOperationItemDto>
            {
                Records = list.ToArray(),
                Pagination = new PaginationBaseDto(filter.PageNumber, transactionsPagedList.TotalItemCount, pageSize)
            };
        }

        /// <summary>
        /// Получить разницу суммы по фильтру
        /// </summary>
        /// <param name="filter">Фильтр поиска</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакции</param>
        /// <returns>Разница суммы</returns>
        private decimal GetDifferenceSumByFilter(ProvidedServiceAndTransactionsFilterDto filter, Guid accountId,
            TransactionType transactionType) => GetFilterDateDifference(filter, accountId, transactionType) +
                                                GetPagesDifference(filter, accountId, transactionType);

        /// <summary>
        /// Получить данные для формы пополнения баланса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак, что используется ОП</param>
        /// <param name="suggestedPaymentModel">Модель предлагаемого платежа</param>
        /// <returns>Данные для формы пополнения баланса</returns>
        public ReplenishBalanceInfoDto GetDataForReplenishBalance(Guid accountId, bool isPromisePayment,
            SuggestedPaymentModelDto suggestedPaymentModel)
        {
            var billingAccount = _dbLayer.BillingAccountRepository.GetBillingAccount(accountId)
                                 ?? throw new NotFoundException($"Аккаунт биллинга {accountId} не найден");

            var accountSupplier = _accountConfigurationDataProvider.GetAccountSupplier(accountId);
            var accountConfiguration = _accountConfigurationDataProvider.GetAccountConfiguration(billingAccount.Id);

            return new ReplenishBalanceInfoDto
            {
                SupplierName = accountSupplier.Name,
                BuyerName = billingAccount.Account.AccountCaption,
                InvoiceNumber = GetCreatingInvoiceNumberMask(),
                BaseBillingServices = GetBaseBillingServices(accountId, accountConfiguration),
                AdditionalBillingServices = GetAdditionalBillingServices(),
                CurrencyCode = _accountConfigurationDataProvider.GetAccountLocale(accountId).Currency,
                ListOfUsersForSendEmail = GetListOfUsersForSendEmail(accountId),
                PayPeriodsList = GetPayPeriods(accountConfiguration.IsVip),
                IsPromisePayment = isPromisePayment,
                SuggestedPayment = suggestedPaymentModel,
                AdditionalResourcesForRent1C = GetAdditionalResourcesForRent1C(billingAccount.Account),
                AccountIsVip = accountConfiguration.IsVip
            };
        }

        /// <summary>
        /// Получить список периодов оплаты
        /// </summary>
        /// <param name="isVipAccount">Признак указывающий что аккаунт VIP</param>
        /// <returns>Список периодов оплаты</returns>
        private List<PayPeriodInfoDto> GetPayPeriods(bool isVipAccount)
        {
            var payPeriods = new List<PayPeriodInfoDto>();

            foreach (PayPeriod payPeriod in Enum.GetValues(typeof(PayPeriod)))
            {
                if (payPeriod == PayPeriod.None)
                    continue;

                var bonusReward = !isVipAccount
                    ? _mapPayPeriodToBonusReward[payPeriod]
                    : 0;

                payPeriods.Add(new PayPeriodInfoDto
                {
                    PeriodDescription = payPeriod.Description(),
                    Period = (int)payPeriod,
                    BonusReward = bonusReward
                });
            }

            return payPeriods;
        }

        /// <summary>
        /// Получить список пользователей для отправки письма
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список пользователей для отправки письма</returns>
        private List<AccountUserInfoForSendEmailDto> GetListOfUsersForSendEmail(Guid accountId) => _dbLayer
            .AccountUsersRepository.GetAccountUsers(accountId).Select(accountUser => new AccountUserInfoForSendEmailDto
            {
                AccountUserId = accountUser.Id,
                AccountUserEmail = accountUser.Email,
                IsAccountAdmin =
                    accountUser.AccountUserRoles.Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin)
            }).ToList();

        /// <summary>
        /// Получить список ресурсов биллинга для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountConfiguration">Конфигурация аккаунта</param>
        /// <returns>Список ресурсов биллинга для аккаунта</returns>
        private ViewPageBaseBillingServicesDto GetBaseBillingServices(Guid accountId,
            AccountConfiguration accountConfiguration)
        {
            var payPeriod = PayPeriod.Month1;

            return new ViewPageBaseBillingServicesDto
            {
                BillingServiceTypeResources = GetBillingServiceTypeResourcesForAccount(accountId, (int)payPeriod),
                MyDiskResource = GetDiskResourceForAccount(accountId, (int)payPeriod, accountConfiguration),
                PayPeriod = payPeriod,
                MyDiskFreeTariffSize = _myDiskFreeTariffSize.Value
            };
        }

        /// <summary>
        /// Получить список дополнительных сервисов биллинга для аккаунта
        /// </summary>
        /// <returns>Список дополнительных сервисов биллинга для аккаунта</returns>
        private EsdlAvailableRatesDto GetAdditionalBillingServices()
        {
            var recRates = _dbLayer.RateRepository
                .Where(r =>
                    r.BillingServiceType.Service.SystemService == Clouds42Service.Recognition &&
                    r.BillingServiceType.SystemServiceType.HasValue).ToList();

            var esdlRate = _dbLayer.RateRepository
                .FirstOrDefault(r =>
                    r.BillingServiceType.Service.SystemService == Clouds42Service.Esdl &&
                    r.BillingServiceType.SystemServiceType.HasValue);

            var recognitionRates = new List<RecognitionRateInfoDto>();

            recRates.ForEach(rate =>
            {
                recognitionRates.Add(new RecognitionRateInfoDto
                {
                    Cost = rate.Cost,
                    PagesCount = rate.BillingServiceType.SystemServiceType.GetDataValue()
                });
            });

            return new EsdlAvailableRatesDto
            {
                ServiceName = NomenclaturesNameConstants.Esdl,
                LicenseYearCost = esdlRate.Cost,
                RecognitionRates = recognitionRates.OrderBy(w => w.PagesCount).ToList()
            };
        }

        /// <summary>
        /// Получить ресурсы услуг билилнга для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="payPeriod">Период оплаты</param>
        /// <returns>Услуги биллинга</returns>
        private List<BillingServiceTypeResourceDto> GetBillingServiceTypeResourcesForAccount(Guid accountId, int payPeriod)
        {
            var billingServices = _dbLayer.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .Where(x => !x.BillingService.SystemService.HasValue || x.BillingService.SystemService == Clouds42Service.MyEnterprise && x.AccountId == accountId)
                .Select(x => new AccountServiceDataDto
                {
                    Id = x.BillingServiceId,
                    IsActive = _dbLayer.BillingServiceTypeRepository.AsQueryable().Any(y =>
                                   y.ServiceId == x.BillingServiceId && y.Resources.Any(z =>
                                       z.Subject.HasValue && (z.AccountId == x.AccountId ||
                                                              z.AccountSponsorId == x.AccountId))) &&
                               x.Frozen.HasValue && !x.Frozen.Value && !x.IsDemoPeriod,
                    SystemService = x.BillingService.SystemService,
                    Name = x.BillingService.Name,
                })
                .ToList();

            var billingServiceTypeResources = new List<BillingServiceTypeResourceDto>();
          
            billingServices.ForEach(service =>
            {
                var serviceTypes = _billingServiceInfoProvider.GetBillingServiceTypesInfo(service.Id, accountId)
                    .Select(w => new BillingServiceTypeResourceDto
                    {
                        Id = w.Id,
                        ServiceName =
                            ServiceNameLocalizer.LocalizeForAccount(_cloudLocalizer, accountId, service.Name, service.SystemService),
                        ServiceTypeName = w.Name,
                        CountLicenses = w.UsedLicenses - w.MeSponsorLicenses,
                        BillingType = w.BillingType,
                        Amount = w.Amount * payPeriod,
                        CostPerOneLicense = w.CostPerOneLicense,
                        SystemServiceType = w.SystemServiceType,
                        IsActiveService = service.IsActive
                    }).ToList();

                billingServiceTypeResources.AddRange(serviceTypes);
            });

            billingServiceTypeResources.AddRange(
                _myDatabasesServiceTypeResourcesDataProvider.GetResourcesDataForMyDatabasesServiceTypes(accountId,
                    payPeriod));

            return billingServiceTypeResources.OrderByDescending(rt => rt.SystemServiceType != null).ToList();
        }

        /// <summary>
        /// Получить дополнительные ресурсы для Аренды 1С
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Дополнительные ресурсы для Аренды 1С</returns>
        private BillingServiceTypeResourceDto GetAdditionalResourcesForRent1C(AccountEntity account)
        {
            var additionalResourcesData = _additionalResourcesDataProvider.GetAdditionalResourcesData(account.Id);

            if (!additionalResourcesData.IsAvailabilityAdditionalResources)
                return new BillingServiceTypeResourceDto();

            var rent1CService =
                _dbLayer.BillingServiceRepository.FirstOrDefault(w => w.SystemService == Clouds42Service.MyEnterprise);

            var additionalResourceCost = additionalResourcesData.AdditionalResourceCost ?? 0;

            return new BillingServiceTypeResourceDto
            {
                Id = Guid.NewGuid(),
                ServiceName = rent1CService.GetNameBasedOnLocalization(_cloudLocalizer, account.Id),
                ServiceTypeName = additionalResourcesData.AdditionalResourceName,
                CountLicenses = 1,
                BillingType = BillingTypeEnum.ForAccount,
                Amount = additionalResourceCost,
                CostPerOneLicense = additionalResourceCost,
                SystemServiceType = ResourceType.MyEntUser,
                IsActiveService = true
            };
        }

        /// <summary>
        /// Получить ресурс диска для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="payPeriod">Период оплаты</param>
        /// <param name="accountConfiguration">Конфигурация аккаунта</param>
        /// <returns>Ресурс диска</returns>
        private MyDiskResourceDto GetDiskResourceForAccount(Guid accountId, int payPeriod,
            AccountConfiguration accountConfiguration)
        {
            var activeDiskResource = _dbLayer.DiskResourceRepository.FirstOrDefault(r =>
                r.Resource.AccountId == accountId && r.Resource.Subject == accountId);

            var myDiskCost = (activeDiskResource?.Resource.Cost ?? 0) * payPeriod;

            var serviceType =
                _dbLayer.BillingServiceTypeRepository.FirstOrDefault(w =>
                    w.SystemServiceType == ResourceType.DiskSpace);

            var myDiskSize = accountConfiguration.IsVip
                ? activeDiskResource?.VolumeGb ?? _myDiskFreeTariffSize.Value
                : (activeDiskResource?.VolumeGb ?? _myDiskFreeTariffSize.Value) - _myDiskFreeTariffSize.Value;

            var diskRate = _dbLayer.RateRepository.FirstOrThrowException(rate =>
                rate.BillingServiceTypeId == serviceType.Id && rate.LocaleId == accountConfiguration.LocaleId &&
                rate.UpperBound > _myDiskFreeTariffSize.Value);

            var myDiskServiceTypeName = accountConfiguration.IsVip
                ? serviceType.Name
                : $"{serviceType.Service.Name}, (свыше {_myDiskFreeTariffSize.Value} Гб)";

            return new MyDiskResourceDto
            {
                Id = serviceType.Id,
                ServiceTypeName = myDiskServiceTypeName,
                ServiceName = serviceType.Service.Name,
                DiskCost = myDiskCost,
                DiskSize = myDiskSize,
                CostPerOneGb = diskRate.Cost
            };
        }

        /// <summary>
        /// Получить список платежей аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="filter">Фильтр поиска</param>
        /// <returns>Список платежей аккаунта</returns>
        private IQueryable<Domain.DataModels.billing.Payment> GetAccountPayments(Guid accountId, ProvidedServiceAndTransactionsFilterDto filter)
        {
            var query = _dbLayer.PaymentRepository.WhereLazy(p => p.AccountId == accountId);
            if (filter == null)
                return query;

            if (filter.Service.HasValue)
            {
                query = query.Where(p => p.BillingService.SystemService == filter.Service.Value);
            }

            if (filter.DateFrom.HasValue)
            {
                query = query.Where(p => filter.DateFrom <= p.Date);
            }

            if (filter.DateTo.HasValue)
            {
                var dateTo = filter.DateTo.Value.AddDays(1).AddSeconds(-1);
                query = query.Where(p => p.Date <= dateTo);
            }

            return query;
        }

        /// <summary>
        /// Плучить разницу дат фильтра поиска
        /// </summary>
        /// <param name="filter">Фильтр поиска</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакции</param>
        /// <returns>Разница дат</returns>
        private decimal GetFilterDateDifference(ProvidedServiceAndTransactionsFilterDto filter, Guid accountId,
            TransactionType transactionType)
        {
            if (!filter.DateTo.HasValue || filter.DateTo.Value.Date >= DateTime.Now.Date) return 0;
            var dateDiff = GetAccountPayments(accountId, new ProvidedServiceAndTransactionsFilterDto
            {
                DateFrom = filter.DateTo.Value.AddDays(1)
            }).Select(x =>
                x.Status == PaymentStatus.Done.ToString() && transactionType == x.TransactionType
                    ? x.Sum * (x.OperationType == PaymentType.Inflow.ToString() ? -1 : 1)
                    : 0)
                .DefaultIfEmpty(0).Sum();

            return dateDiff;
        }

        /// <summary>
        /// Получить разницу страниц пользователя
        /// </summary>
        /// <param name="filter">Фильтр поиска</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакции</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <returns>Разница страниц</returns>
        private decimal GetPagesDifference(ProvidedServiceAndTransactionsFilterDto filter, Guid accountId,
            TransactionType transactionType, int pageSize = 10)
        {
            var query = GetAccountPayments(accountId, filter).Where(o => o.Sum != 0).OrderByDescending(o => o.Date);

            if (filter.PageNumber == 1) return 0;

            var perPageData = query
                .Take(pageSize * (filter.PageNumber - 1))
                .Select(x => new { x.Sum, x.OperationType, x.Status, x.TransactionType })
                .ToList();

            return perPageData.Sum(x =>
                x.Status == PaymentStatus.Done.ToString() && x.TransactionType == transactionType
                    ? x.Sum * (x.OperationType == PaymentType.Inflow.ToString() ? -1 : 1)
                    : 0);
        }

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакций</param>
        /// <returns>Баланс аккаунта</returns>
        private decimal GetAccountBalance(Guid accountId, TransactionType transactionType = TransactionType.Money)
        {
            var billingAccount = _dbLayer.BillingAccountRepository.GetBillingAccount(accountId);
            return transactionType == TransactionType.Bonus
                ? billingAccount.BonusBalance
                : billingAccount.Balance;
        }

        /// <summary>
        /// Получить маску номера для создаваемого счета на оплату
        /// </summary>
        /// <returns>Маска номера для создаваемого счета на оплату</returns>
        private string GetCreatingInvoiceNumberMask()
            => $"{_invoiceUniqPrefix.Value}________";

        /// <summary>
        /// Получить регулярные платежи для аккаунтов
        /// </summary>
        /// <returns>Регулярные платежи для аккаунтов</returns>
        private IQueryable<AccountRegularPaymentDto> GetRegularPaymentForAccounts() =>
            from resourceConfiguration in _dbLayer.ResourceConfigurationRepository.WhereLazy()
            join account in _dbLayer.AccountsRepository.WhereLazy() on resourceConfiguration.AccountId equals
                account.Id
            join service in _dbLayer.BillingServiceRepository.WhereLazy() on resourceConfiguration.BillingServiceId
                equals service.Id
            where service.SystemService != Clouds42Service.Esdl
                  && service.SystemService !=
                  Clouds42Service.Recognition &&
                  !resourceConfiguration.IsDemoPeriod
            group resourceConfiguration by resourceConfiguration.AccountId
            into groupedData
            select new AccountRegularPaymentDto
            {
                AccountId = groupedData.Key,
                RegularPaymentSum = groupedData.Sum(rc => rc.Cost)
            };
    }
}
