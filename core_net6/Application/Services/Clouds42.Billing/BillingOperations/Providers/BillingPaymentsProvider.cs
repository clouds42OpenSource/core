﻿using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Providers
{
    /// <summary>
    /// Провайдер платежей биллинга
    /// </summary>
    public class BillingPaymentsProvider(
        IUnitOfWork dbLayer,
        BillingPaymentsHelper billingPaymentsHelper,
        ILogger42 logger,
        IHandlerException handlerException)
        : IBillingPaymentsProvider
    {
        /// <summary>
        /// Получить доступный баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Доступный баланс аккаунта</returns>
        public decimal GetAccountAvailableBalance(Guid accountId)
        {
            var account = dbLayer.BillingAccountRepository.GetBillingAccount(accountId);
            return account.GetAvailableBalance();
        }

        /// <summary>
        /// Провести транзакции по описанию платежа
        /// </summary>
        /// <param name="paymentDefinition">Описание платежа</param>
        /// <returns>Результат проведения транзакций</returns>
        public CreatePaymentResultDto MakePayment(PaymentDefinitionDto paymentDefinition)
        {
            var paymentDefinitions = billingPaymentsHelper.CreatePaymentDefinitions(paymentDefinition);

            var paymentIdsList = new List<Guid>();

            paymentDefinitions.ForEach(payment =>
            {
                var result = CreatePayment(payment);
                if (result == PaymentOperationResult.Ok)
                    paymentIdsList.Add(payment.Id);
            });

            return paymentIdsList.Count != paymentDefinitions.Count
                ? new CreatePaymentResultDto(PaymentOperationResult.ErrorInvalidOperation)
                : new CreatePaymentResultDto(PaymentOperationResult.Ok, paymentIdsList);
        }

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="paymentDefinition">Описание платежа</param>
        /// <returns>Результат операции</returns>
        public PaymentOperationResult CreatePayment(PaymentDefinitionDto paymentDefinition)
        {
            var account = dbLayer.BillingAccountRepository.GetBillingAccount(paymentDefinition.Account);
            if (account == null)
            {
                logger.Warn($"Ошибка создания платежа. Компания не найдена: {paymentDefinition.Account}");
                return PaymentOperationResult.ErrorAccountNotFound;
            }

            if (paymentDefinition is { Total: > 0, OperationType: PaymentType.Outflow })
            {
                var paymentOperationResult = CanCreatePayment(paymentDefinition, out var needMoney);
                if (paymentOperationResult != PaymentOperationResult.Ok)
                {
                    logger.Trace(
                        $"Acc = {paymentDefinition.Account}. Ошибка создания платежа. Не достаточно денег на счету в размере '{needMoney}', текущий баланс '{account.Balance}', сумма платежа '{paymentDefinition.Total}'.");
                    return paymentOperationResult;
                }
            }

            var res = AddPayment(new Domain.DataModels.billing.Payment
            {
                TransactionType = paymentDefinition.TransactionType,
                AccountId = account.Id,
                Date = paymentDefinition.Date,
                Sum = paymentDefinition.Total,
                OperationType = paymentDefinition.OperationType.ToString(),
                Status = paymentDefinition.Status.ToString(),
                PaymentSystem = paymentDefinition.System.ToString(),
                ExchangeDataId = paymentDefinition.ExchangeIdentifier,
                Description = paymentDefinition.Description,
                OriginDetails = paymentDefinition.OriginDetails,
                BillingServiceId = paymentDefinition.BillingServiceId,
                Id = paymentDefinition.Id,
                Remains = paymentDefinition.Remains,
            });

            TracePayment(logger, paymentDefinition);
            return res;
        }

        private static void TracePayment(ILogger42 logger, PaymentDefinitionDto payment)
        {
            logger.Trace("Платеж создан. Acc = {0}; Date = {1}; Sum = {2}; Type = {3}; Status = {4}; PaymentSystem = {5}; Description = {6}",
                payment.Account, payment.Date, payment.Total, payment.OperationType, payment.Status, payment.System, payment.Description);
        }

        /// <summary>
        /// Добавить платеж
        /// </summary>
        /// <param name="payment">Модель платежа</param>
        /// <returns>Результат операции</returns>
        public PaymentOperationResult AddPayment(Domain.DataModels.billing.Payment payment)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var account = dbLayer.BillingAccountRepository.FirstOrDefault(acc => acc.Id == payment.AccountId);

                InsertPayment(payment);

                if (account.MonthlyPaymentDate == null && payment.OperationType == PaymentType.Inflow.ToString())
                {
                    account.MonthlyPaymentDate = payment.Date.AddMonths(1);
                }

                var accountTotalBalanceSum = GetAccountTotalBalanceSum(payment.AccountId, payment.TransactionType);

                var balanceType = payment.TransactionType == TransactionType.Money
                    ? "Денежный"
                    : "Бонусный";

                logger.Trace($"{balanceType} баланс компании {payment.AccountId} обновлен и равен {accountTotalBalanceSum}.");

                if (payment.TransactionType == TransactionType.Bonus)
                {
                    account.BonusBalance = accountTotalBalanceSum;
                }
                else
                {
                    account.Balance = accountTotalBalanceSum;
                }

                dbLayer.BillingAccountRepository.Update(account);
                dbLayer.Save();
                transaction.Commit();

                return PaymentOperationResult.Ok;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка пополнения баланс компании] {payment.AccountId}");
                transaction.Rollback();
                return PaymentOperationResult.ErrorInvalidOperation;
            }
        }

        /// <summary>
        /// Установить данные для обещанного платежа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="date">Дата взятия ОП</param>
        /// <param name="sum">Сумма ОП</param>
        public void SetPromisePaymentCredentials(Guid accountId, DateTime date, decimal sum)
        {
            var account = dbLayer.BillingAccountRepository.GetBillingAccount(accountId);

            if (account == null)
                throw new InvalidOperationException($"Account {accountId} не существует, и не был создан");

            account.PromisePaymentDate = date;
            account.PromisePaymentSum = sum;
            dbLayer.BillingAccountRepository.Update(account);
            dbLayer.Save();
        }

        /// <summary>
        /// Пересчитать баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакции (денежная или бонусная)</param>
        public void RecalculateAccountTotals(Guid accountId, TransactionType transactionType = TransactionType.Money)
        {
            var total = GetAccountTotalBalanceSum(accountId, transactionType);
            SetBalance(accountId, total, transactionType);
        }

        /// <summary>
        /// Проверить возможность проведения денежной операции.
        /// </summary>
        /// <param name="paymentDefinition">Описание платежа</param>
        /// <param name="needMoney">Сумма денег которой не хватает для проведения.</param>
        /// <returns>Результат проведения транзакции.</returns>
        public PaymentOperationResult CanCreatePayment(PaymentDefinitionDto paymentDefinition, out decimal needMoney)
        {
            return CanCreatePayment(paymentDefinition.Account, paymentDefinition.Total, out needMoney);
        }

        /// <summary>
        /// Проверить возможность создания денежной проводки.
        /// </summary>
        /// <param name="accountId">Номер аккантуа.</param>
        /// <param name="paymentSum">Сумма платежа.</param>
        /// <param name="needMoney">Сумма денег которой не хватает для проведения.</param>
        /// <returns>Результат проведения транзакции.</returns>
        public PaymentOperationResult CanCreatePayment(Guid accountId, decimal paymentSum, out decimal needMoney)
        {
            var account = dbLayer.BillingAccountRepository.GetBillingAccount(accountId);
            var availableAccountBalance = account.GetAvailableBalance();

            if (availableAccountBalance < paymentSum)
            {
                needMoney = paymentSum - availableAccountBalance;
                logger.Trace(
                    $"Acc = {accountId}. Ошибка создания платежа. Не достаточно денег на счету в размере '{needMoney}', текущий баланс '{account.Balance}', сумма платежа '{paymentSum}'.");
                return PaymentOperationResult.ErrroNotEnoughBalance;
            }

            needMoney = 0;
            return PaymentOperationResult.Ok;
        }

        /// <summary>
        /// Записать платеж
        /// </summary>
        /// <param name="payment">Модель платежа</param>
        private void InsertPayment(Domain.DataModels.billing.Payment payment)
        {
            if (payment.Id == Guid.Empty)
                payment.Id = Guid.NewGuid();

            dbLayer.PaymentRepository.Insert(payment);
            dbLayer.Save();
        }

        /// <summary>
        /// Получить общий баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакций</param>
        /// <returns>Общий баланс аккаунта</returns>
        private decimal GetAccountTotalBalanceSum(Guid accountId, TransactionType transactionType)
        {
            var payments = dbLayer.PaymentRepository
                .Where(p => p.AccountId == accountId &&
                            p.Status == PaymentStatus.Done.ToString() &&
                            p.TransactionType == transactionType)
                .ToList();

            return !payments.Any() ? 0m : payments.Sum(p => p.Sum * (p.OperationType == "Inflow" ? 1 : -1));
        }

        /// <summary>
        /// Установить баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="balance">Баланс</param>
        /// <param name="balanceType">Тип баланса (денежный или бонусный)</param>
        private void SetBalance(Guid accountId, decimal balance, TransactionType balanceType)
        {
            var account = dbLayer.BillingAccountRepository.GetBillingAccount(accountId);

            if (account == null)
                throw new NotFoundException($"Аккаунт {accountId} не существует, и не был создан");

            if (balanceType == TransactionType.Money)
            {
                account.Balance = balance;
            }
            else
            {
                account.BonusBalance = balance;
            }

            dbLayer.BillingAccountRepository.Update(account);
            dbLayer.Save();
        }
    }
}
