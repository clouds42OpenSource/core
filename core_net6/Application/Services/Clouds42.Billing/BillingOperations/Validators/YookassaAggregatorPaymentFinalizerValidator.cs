﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.BillingOperations.Validators
{
    /// <summary>
    /// Валидатор модели обработки события изменения статуса платежа ЮKassa
    /// </summary>
    public static class YookassaAggregatorPaymentFinalizerValidator
    {
        /// <summary>
        /// Выполнить валидацию модели обработки события на уставновку
        /// успешного статуса платежа ЮKassa
        /// </summary>
        /// <param name="newStatus">Новый статус платежа</param>
        /// <param name="currentStatus">Текущий статус платежа</param>
        public static void ValidateYookassaPaymentStatus(string newStatus, string currentStatus)
        {
            if (newStatus == currentStatus)
                throw new ValidationException("Финальный статус платежа не отличается от текущего статуса");
        }

        public static void ValidateSucceededPayment(YookassaFinalPaymentObjectDto paymentObject)
        {
            if (!paymentObject.IsPaid)
                throw new ValidationException("Платеж не оплачен");

            if (paymentObject.Id.IsNullOrEmpty())
                throw new ValidationException("Поле 'Id' не должно быть null");
        }


        /// <summary>
        /// Провалидировать модель метода платежа
        /// </summary>
        /// <param name="model">Объект метода платежа</param>
        public static void ValidatePaymentMethod(YookassaPaymentMethodDto model)
        {
            if (model == null)
                throw new ValidationException("Не заполнен объект 'Модель платежных данных'");

            if (model.PaymentTemplateId.IsNullOrEmpty())
                throw new ValidationException("Не заполнено поле 'Id шаблона платежа'");

            if (model.Type.IsNullOrEmpty())
                throw new ValidationException("Не заполнено поле 'Тип способа оплаты'");

            if (model.Type == SavedPaymentMethodType.BankCard.Description())
                ValidatePaymentCard(model.Card);
        }

        /// <summary>
        /// Провалидировать модель платежной карты
        /// </summary>
        /// <param name="model">Модель платежной карты</param>
        private static void ValidatePaymentCard(YookassaPaymentCardDto model)
        {
            if (model == null)
                throw new ValidationException("Не заполнен объект 'Модель платежной карты'");

            if (model.CardType.IsNullOrEmpty())
                throw new ValidationException("Не заполнено поле 'Тип карты'");

            if (model.ExpiryMonth.IsNullOrEmpty())
                throw new ValidationException("Не заполнено поле 'Месяц истечения срока действия карты'");

            if (model.ExpiryYear.IsNullOrEmpty())
                throw new ValidationException("Не заполнено поле 'Год истечения срока действия карты'");

            if (model.FirstSixDigits.IsNullOrEmpty())
                throw new ValidationException("Не заполнено поле 'Первые 6 цифр номера карты'");

            if (model.LastFourDigits.IsNullOrEmpty())
                throw new ValidationException("Не заполнено поле 'Последние 4 цифры номера карты'");

            if (model.IssuerCountry.IsNullOrEmpty())
                throw new ValidationException("Не заполнено поле 'Страна эмитента'");
        }
    }
}
