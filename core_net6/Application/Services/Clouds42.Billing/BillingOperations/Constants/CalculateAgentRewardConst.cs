﻿namespace Clouds42.Billing.BillingOperations.Constants
{
    /// <summary>
    /// Константы для калькуляции агентского вознаграждения
    /// </summary>
    public static class CalculateAgentRewardConst
    {
        /// <summary>
        /// Максимальное значение вознаграждения агента в процентах
        /// </summary>
        public const decimal MaximumRewardPercent = 100;
    }
}
