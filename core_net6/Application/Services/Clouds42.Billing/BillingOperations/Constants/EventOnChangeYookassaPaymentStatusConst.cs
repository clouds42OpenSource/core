﻿namespace Clouds42.Billing.BillingOperations.Constants
{
    /// <summary>
    /// Константы события изменения статуса платежа ЮKassa
    /// </summary>
    public static class EventOnChangeYookassaPaymentStatusConst
    {
        /// <summary>
        /// Тип способа оплаты - оплата банковской картой
        /// </summary>
        public const string PaymentMethodTypeBankCard = "bank_card";
    }
}
