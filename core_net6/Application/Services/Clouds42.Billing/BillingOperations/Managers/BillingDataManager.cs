﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Managers
{
    /// <summary>
    /// Менеджер данных биллинга
    /// </summary>
    public class BillingDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IBillingDataProvider billingDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить данные для формы пополнения баланса
        /// </summary>
        /// <param name="isPromisePayment">Признак, что используется ОП</param>
        /// <param name="suggestedPaymentModel">Модель предлагаемого платежа</param>
        /// <returns>Данные для формы пополнения баланса</returns>
        public ManagerResult<ReplenishBalanceInfoDto> GetDataForReplenishBalance(bool isPromisePayment,
            SuggestedPaymentModelDto suggestedPaymentModel)
        {
            var accountId = AccessProvider.ContextAccountId;
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_View, () => accountId);
                return Ok(billingDataProvider.GetDataForReplenishBalance(accountId, isPromisePayment, suggestedPaymentModel));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения данных для формы пополнения баланса]", () => accountId);
                return PreconditionFailed<ReplenishBalanceInfoDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список транзакций аккаунта
        /// </summary>
        /// <param name="filter">Фильтр поиска</param>
        /// <returns>Список транзакций аккаунта</returns>
        public ManagerResult<BillingOperationDataDto> GetTransactionsList(ProvidedServiceAndTransactionsFilterDto filter)
        {
            var accountId = AccessProvider.ContextAccountId;
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_View, () => accountId);
                return Ok(billingDataProvider.GetTransactionsPagers(accountId, filter));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка транзакций]", () => accountId);
                return PreconditionFailed<BillingOperationDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список транзакций аккаунта
        /// </summary>
        /// <param name="filter">Фильтр поиска</param>
        /// <returns>Список транзакций аккаунта</returns>
        public ManagerResult<SelectDataResultCommonDto<BillingOperationItemDto>> GetPaginatedTransactionsForAccount(
            Guid accountId,
            SelectDataCommonDto<PaymentsFilterDto> request)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_View, () => accountId);
                return Ok(billingDataProvider.GetPaginatedTransactionsForAccount(accountId, request));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка транзакций]", () => accountId);
                return PreconditionFailed<SelectDataResultCommonDto<BillingOperationItemDto>>(ex.Message);
            }
        }

    }
}
