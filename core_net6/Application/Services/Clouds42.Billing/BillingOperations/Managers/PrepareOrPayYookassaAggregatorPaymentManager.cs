﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Managers
{
    /// <summary>
    /// Менеджер для подготовки платежа агрегатора ЮKassa
    /// </summary>
    public class PrepareOrPayYookassaAggregatorPaymentManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IPayYookassaAggregatorPaymentProvider payYookassaAggregatorPaymentProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Подготовить платеж агрегатора ЮKassa для клиента
        /// </summary>
        /// <param name="model">Модель для подготовки платежа агрегатора ЮKassa</param>
        /// <returns>Данные для подтверждения платежа ЮKassa клиентом</returns>
        public ManagerResult<ConfirmYookassaPaymentForClientDataDto> PreparePayment(
            PrepareYookassaAggregatorPaymentDto model)
        {
            if (model.PaymentSum <= 0)
                return PreconditionFailed<ConfirmYookassaPaymentForClientDataDto>("Сумма платежа должна быть больше 0");

            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_System, () => model.AccountId);

                var data = payYookassaAggregatorPaymentProvider.PreparePaymentOrPayWithSavedPaymentMethod(model);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"""
                     [Подготовка платежа ЮKassa] на сумму '{model.PaymentSum}' 
                                                     для аккаунта '{model.AccountId}' завершилась с ошибкой {ex.Message}
                     """);

                return PreconditionFailed<ConfirmYookassaPaymentForClientDataDto>(ex.Message);
            }
        }
    }
}
