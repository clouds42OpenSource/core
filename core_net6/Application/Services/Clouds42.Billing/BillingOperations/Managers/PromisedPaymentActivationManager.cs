﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Managers
{

    public class PromisedPaymentActivationManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IPromisedPaymentActivationProvider promisedPaymentActivationProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IPromisedPaymentActivationManager
    {

        /// <summary>
        /// Активировать обещанный платеж для аккаунта на основании инвойса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoiceId">ID исходного инвойса</param>
        /// <returns></returns>
        public async Task<ManagerResult> ActivatePromisedPaymentAsync(Guid accountId, Guid invoiceId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.PromisePayment_CreatePromisePayment, () => accountId);
                await promisedPaymentActivationProvider.ActivatePromisedPaymentAsync(accountId, invoiceId);
                return Ok(true);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка взятия обещанного платежа].", () => accountId, () => invoiceId);
                return PreconditionFailed<bool>($"Ошибка взятия обещанного платежа. {ex.Message}", ex);
            }
        }

        /// <summary>
        /// Увеличение обещанного платежа для аккаунта на основании инвойса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoiceId">ID исходного инвойса</param>
        /// <returns></returns>
        public async Task<ManagerResult> IncreasePromisedPaymentAsync(Guid accountId, Guid invoiceId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.PromisePayment_CreatePromisePayment, () => accountId);
                await promisedPaymentActivationProvider.IncreasePromisedPaymentAsync(accountId, invoiceId);
                return Ok(true);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка увеличения обещанного платежа].", () => accountId, () => invoiceId);
                return PreconditionFailed<bool>($"Ошибка увеличения обещанного платежа. {ex.Message}", ex);
            }
        }
    }
}
