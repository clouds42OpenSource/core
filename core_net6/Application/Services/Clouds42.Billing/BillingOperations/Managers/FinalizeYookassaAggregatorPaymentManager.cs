﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Managers
{
    /// <summary>
    /// Менеджер для финализации платежа агрегатора ЮKassa
    /// Получает статус платежа и вызывает
    /// обработку события на изменение статуса
    /// </summary>
    public class FinalizeYookassaAggregatorPaymentManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IFinalizeYookassaAggregatorPaymentProvider finalizeYookassaAggregatorPaymentProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IFinalizeYookassaAggregatorPaymentManager
    {
        /// <summary>
        /// Финализация платежа агрегатора ЮKassa
        /// </summary>
        /// <param name="yookassaPaymentId">Id платежа агрегатора ЮKassa</param>
        /// <returns>Результат проверки изменения статуса</returns>
        public ManagerResult FinalizePayment(Guid yookassaPaymentId)
        {
            try
            {
                var checkResult = finalizeYookassaAggregatorPaymentProvider.FinalizePayment(yookassaPaymentId);

                return !checkResult ? PreconditionFailed("Статус платежа не изменился") : Ok();
            }
            catch (Exception ex)
            {
                Logger.Warn(ex,
                    $@"[Проверка изменения статуса платежа] '{yookassaPaymentId}' агрегатора ЮKassa завершилась с ошибкой {ex.Message}");

                return PreconditionFailed(ex.Message);
            }
        }
    }
}
