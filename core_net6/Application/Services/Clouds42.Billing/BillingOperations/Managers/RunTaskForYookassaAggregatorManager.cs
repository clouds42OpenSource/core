﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingOperations.Managers
{
    /// <summary>
    /// Менеджер запуска задач для агрегатора ЮKassa
    /// </summary>
    public class RunTaskForYookassaAggregatorManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IRunTaskToCheckYookassaPaymentStatusProvider runTaskToCheckYookassaPaymentStatusProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Запустить задачу по проверке
        /// статуса платежа агрегатора ЮKassa
        /// </summary>
        /// <param name="paymentId">Id системного платежа</param>
        /// <returns>Результат запуска</returns>
        public ManagerResult RunTaskToCheckYookassaPaymentStatus(Guid paymentId)
        {
            try
            {
                runTaskToCheckYookassaPaymentStatusProvider.RunTask(paymentId);
                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"""
                     Запуск задачи по проверке статуса платежа 
                                                     агрегатора ЮKassa завершился с ошибкой {ex.Message}]
                     """);

                return PreconditionFailed(ex.Message);
            }
        }
    }
}
