﻿using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing
{
    /// <summary>
    /// Хелпер для сохранения тестовых данных
    /// </summary>
    public static class SaveTestDataHelper
    {
        /// <summary>
        /// Сохранить пользователей
        /// </summary>
        /// <param name="accountUsers">Список пользователей</param>
        /// <param name="dbLayer">IUnitOfWork</param>
        public static void SaveAccountUsers(this List<AccountUser> accountUsers, IUnitOfWork dbLayer)
        {
            dbLayer.AccountUsersRepository.InsertRange(accountUsers);
            dbLayer.Save();
        }

        /// <summary>
        /// Сохранить информационные базы
        /// </summary>
        /// <param name="accountDbs">Список баз</param>
        /// <param name="dbLayer">IUnitOfWork</param>
        public static void SaveAccountDatabases(this List<Domain.DataModels.AccountDatabase> accountDbs, IUnitOfWork dbLayer)
        {
            dbLayer.DatabasesRepository.InsertRange(accountDbs);
            dbLayer.Save();
        }

        /// <summary>
        /// Сохранить права пользователей к базам
        /// </summary>
        /// <param name="acDbAccesses">Список прав</param>
        /// <param name="dbLayer">IUnitOfWork</param>
        public static void SaveAcDbAccesses(this List<AcDbAccess> acDbAccesses, IUnitOfWork dbLayer)
        {
            dbLayer.AcDbAccessesRepository.InsertRange(acDbAccesses);
            dbLayer.Save();
        }

        /// <summary>
        /// Сохранить аккаунты
        /// </summary>
        /// <param name="accounts">Список аккаунтов</param>
        /// <param name="dbLayer">IUnitOfWork</param>
        public static void SaveAccounts(this List<Account> accounts, IUnitOfWork dbLayer)
        {
            dbLayer.AccountsRepository.InsertRange(accounts);
            dbLayer.Save();
        }
    }
}
