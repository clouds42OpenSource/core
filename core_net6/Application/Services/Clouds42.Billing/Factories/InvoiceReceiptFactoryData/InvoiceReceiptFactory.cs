﻿using Clouds42.Billing.Contracts.Factories;
using Clouds42.Common.Constants;
using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Billing.Factories.InvoiceReceiptFactoryData
{
    /// <summary>
    ///     Фабрика получения фискальных чеков
    /// </summary>
    public class InvoiceReceiptFactory(IInvoiceReceiptBuilder builder)
    {
        private readonly Dictionary<string, IInvoiceReceiptBuilder> _builders = new()
        {
            { LocaleConst.Russia, builder }
        };

        /// <summary>
        ///     Получить фискальный чек
        /// </summary>
        /// <param name="receiptInfo">Информация о счете</param>
        /// <param name="locale">Локаль</param>
        public InvoiceReceiptDto GetReceipt(InvoiceReceiptDto receiptInfo, string locale)
        {
            if (!_builders.ContainsKey(locale))
            {
                receiptInfo.FiscalData = new InvoiceReceiptFiscalDataDto
                {
                    Errors = "Нет доступного фискального сервиса для локали: " + locale
                };

                return receiptInfo;
            }

            receiptInfo.FiscalAnyBuilder = true;
            return _builders[locale].Create(receiptInfo);
        }
    }
}