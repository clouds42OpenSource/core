﻿using Clouds42.Billing.Contracts.Factories;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business;
using Clouds42.OnlineCashier.Infrastructure;
using Clouds42.OnlineCashier.Infrastructure.IModels;

namespace Clouds42.Billing.Factories.InvoiceReceiptFactoryData.ReceiptBuilders
{
    /// <summary>
    ///     Фискальный сервис для России
    /// </summary>
    public class RussiaReceiptBuilder(IOnlineCashBuilder chekOnlineBuilder) : IInvoiceReceiptBuilder
    {
        public InvoiceReceiptDto Create(InvoiceReceiptDto receiptInfo)
        {
            var authData = new ChequeOnlineAuth
            {
                Url = CloudConfigurationProvider.OnlineCashier.ChekOnline.GetUrlApi(receiptInfo.SupplierRazorTemplate.SupplierId),
                CertificateData = CloudConfigurationProvider.OnlineCashier.ChekOnline.GetCertificateData(receiptInfo.SupplierRazorTemplate.SupplierId),
                CertificatePassword = CloudConfigurationProvider.OnlineCashier.ChekOnline.GetCertificatePassword(receiptInfo.SupplierRazorTemplate.SupplierId)
            };

            var request = new ChequeOnlineRequest
            {
                TaxCode = CloudConfigurationProvider.OnlineCashier.ChekOnline.GetTaxCode(receiptInfo.SupplierRazorTemplate.SupplierId),
                ClientEmail = receiptInfo.ClientEmail,
                InvoiceId = receiptInfo.InvoiceId,
                Products = receiptInfo.Products.Select(w =>
                        new ChequeOnlineRequestProduct
                        {
                            Name = w.Description.Replace("Аренда 1С", "Лицензионное ПО"),
                            Amount = w.ServiceSum,
                            Count = w.Count
                        })
                    .Cast<IOnlineCashierProduct>()
                    .ToList()
            };

            if (!chekOnlineBuilder.ValidateRequest(authData, request, out var errorValidate))
            {
                receiptInfo.FiscalData = new InvoiceReceiptFiscalDataDto { Errors = errorValidate };
                return receiptInfo;
            }

            var response = chekOnlineBuilder.BuildCheck(authData, request);

            if (!response.IsSuccess || response is not ChequeOnlineResponse)
            {
                receiptInfo.FiscalData = new InvoiceReceiptFiscalDataDto
                {
                    Errors = response.Errors
                };

                return receiptInfo;
            }

            var chekOnlineResponse = response as ChequeOnlineResponse;


            if (chekOnlineResponse == null) 
            {
                receiptInfo.FiscalData = new InvoiceReceiptFiscalDataDto
                {
                    Errors = response.Errors
                };

                return receiptInfo;
            }

            receiptInfo.FiscalData = new InvoiceReceiptFiscalDataDto
            {
                IsSuccess = chekOnlineResponse.IsSuccess,
                Errors = chekOnlineResponse.Errors,
                DocumentDateTime = chekOnlineResponse.DocumentDateTime,
                QrCodeData = chekOnlineResponse.QrCodeData,
                QrCodeFormat = chekOnlineResponse.QrCodeFormat,
                ReceiptInfo = chekOnlineResponse.ReceiptInfo,
                FiscalSign = chekOnlineResponse.FiscalData?.Sign,
                FiscalNumber = chekOnlineResponse.FiscalData?.Number,
                FiscalSerial = chekOnlineResponse.FiscalData?.Serial
            };

            return receiptInfo;
        }
    }
}
