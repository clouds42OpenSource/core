﻿using System.Security.Cryptography;
using System.Text;
using Clouds42.Billing.Contracts.Factories.Models;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;

namespace Clouds42.Billing.Factories.Helpers
{
    public static class UkrPaysHelper
    {
        private static readonly string _serviceId = "582";
        private static readonly string DomainUrl = CloudConfigurationProvider.Cp.GetSiteProAuthorityUrl();
        private static readonly string UkrPaysUrl = CloudConfigurationProvider.BillingAndPayment.UkrPays.GetUrl();
        private static readonly string _secretKey = "rpQPi6Y9IsMwgkjMFiw9juoq4OxUSN5i";

        public enum UkrPaysSystem
        {
            VisaMastercard = 1,
            LiqPay = 12,
            WebMoney = 17,
            PrivatBank = 21, 
            Терминал24NonStop = 131,
            ТерминалTyme = 132,
            ТерминалEasySoft = 133,
            ТерминалEpay = 134,
            ТерминалФкСистема = 136,
            ТерминалCity24 = 137,
            ТерминалIBox = 139
        }

        /// <summary>
        /// Формирование данных для отправки на api платежки
        /// </summary>
        /// <param name="order"></param>
        /// <param name="money"></param>
        /// <returns></returns>
        public static UkrPayInitDataModel GetPaymentInitData(string order, decimal money)
        {
            var url = DomainUrl + "/billing";
            return new UkrPayInitDataModel
            {
                ServiceId = _serviceId,
                Successurl = url,
                RequestUrl = UkrPaysUrl,
                Order = order,
                Amount = money,
                Encoding = "UTF-8"
            };
        }

        /// <summary>
        /// Проверка контрольной суммы
        /// </summary>
        /// <param name="idUps"></param>
        /// <param name="order"></param>
        /// <param name="amount"></param>
        /// <param name="date"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        public static bool IsValidControlKey(string idUps, string order, string amount, string date, string hash)
        {
            var stringKey = string.Concat(idUps, order, amount, date, _secretKey);
            using MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(stringKey));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(sBuilder.ToString(), hash))
            {
                return true;
            }
            return false;
        }

        public static string GetPaySystemValue(string key)
        {
            var systemDictionary = new Dictionary<string,string>();
            systemDictionary.Add("1","Visa MasterCard");
            systemDictionary.Add("12","LiqPay");
            systemDictionary.Add("17","WebMoney");
            systemDictionary.Add("21","PrivatBank");
            systemDictionary.Add("131","Терминал 24NonStop");
            systemDictionary.Add("132","Терминал Tyme");
            systemDictionary.Add("133","Терминал EasySoft");
            systemDictionary.Add("134","Терминал E-Pay");
            systemDictionary.Add("136","Терминал Фк Система");
            systemDictionary.Add("137","Терминал City24");
            systemDictionary.Add("139","Терминал IBox");

            if (systemDictionary.ContainsKey(key))
            {
                return systemDictionary.GetValue(key);
            }
            return "undefined system";
        }
    }
}
