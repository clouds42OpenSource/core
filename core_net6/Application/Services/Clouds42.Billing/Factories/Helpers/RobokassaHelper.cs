﻿using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.HandlerExeption.Contract;
 
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Factories.Helpers
{
    public class RobokassaHelper(IUnitOfWork dbLayer)
    {
        private readonly ILogger42 _logger = Logger42.GetLogger();
        private readonly MD5 _md5Provider = MD5.Create();
        private readonly IHandlerException _handlerException = HandlerException42.GetHandler();
        private readonly Lazy<string> _stringFormatForPaymentSum = new(CloudConfigurationProvider.BillingAndPayment.Robokassa.GetStringFormatForPaymentSum);

        /// <summary>
        /// Сгенерировать ссылку на оплату через Робокассу
        /// </summary>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="paymentId">ID платежа</param>
        /// <param name="description">Описание платежа</param>
        public string BuildPaymentUrl(decimal paymentSum, int paymentId, string description)
        {
            var payment = dbLayer.PaymentRepository.FirstOrDefault(w => w.Number == paymentId);
            var supplierId = GetSupplierIdByAccountId(payment.AccountId);

            var url = CloudConfigurationProvider.BillingAndPayment.Robokassa.GetUrl();
            var login = CloudConfigurationProvider.BillingAndPayment.Robokassa.GetLogin(supplierId);
            var secretKey =
                CloudConfigurationProvider.BillingAndPayment.Robokassa.GetRequestSecretkey(supplierId);

            var signature =
                ConvertToMd5(
                    $"{login}:{paymentSum.ToString("F", CultureInfo.InvariantCulture)}:{paymentId}:{secretKey}");
            var resultUrl = $"{url}MrchLogin={login}&OutSum={paymentSum.ToString("F", CultureInfo.InvariantCulture)}" +
                            $"&InvId={paymentId}&Desc={description}&SignatureValue={signature}&IncCurrLabel=&Culture=ru";

            _logger.Info($"Ссылка оплаты через Робокассу для платежа {paymentId} на сумму {paymentSum}: {resultUrl}");

            return resultUrl;
        }

        /// <summary>
        /// Проверка ответа робокассы
        /// </summary>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="paymentId">ID платежа</param>
        /// <param name="incomingSignature">Сигнатура агрегатора</param>
        public bool CheckResponse(decimal paymentSum, int paymentId, string incomingSignature)
        {
            try
            {
                var payment = dbLayer.PaymentRepository.FirstOrDefault(w => w.Number == paymentId);

                var secretKey =
                    CloudConfigurationProvider.BillingAndPayment.Robokassa.GetResponseSecretkey(
                        GetSupplierIdByAccountId(payment.AccountId));

                var signature =
                    ConvertToMd5(
                        $"{paymentSum.ToString(_stringFormatForPaymentSum.Value, CultureInfo.InvariantCulture)}:{paymentId}:{secretKey}");

                var result = incomingSignature.Equals(signature, StringComparison.CurrentCultureIgnoreCase);

                if (!result)
                    _logger.Trace(
                        $"Ошибка при валидации сигнатуры для платежа {paymentId} на сумму {paymentSum}. Ответ робокассы: {incomingSignature}. Наша сигнатура: {signature}");

                return result;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка при проверке валидации ответа Робокассы для платежа] {paymentId} на сумму {paymentSum}");
                return false;
            }
        }

        /// <summary>
        /// Конвертировать в хэш сумму MD5
        /// </summary>
        /// <param name="input">Строка</param>
        private string ConvertToMd5(string input)
        {
            var computeHash = _md5Provider.ComputeHash(Encoding.UTF8.GetBytes(input));
            var stringBuilder = new StringBuilder();

            foreach (var b in computeHash)
                stringBuilder.Append($"{b:x2}");

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Получить Id поставщика по Id аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id поставщика</returns>
        private Guid GetSupplierIdByAccountId(Guid accountId) =>
            dbLayer.GetGenericRepository<AccountConfiguration>()
                .FirstOrDefault(accountConfiguration => accountConfiguration.AccountId == accountId)?.SupplierId ??
            throw new NotFoundException($"Не удалось определить поставщика для аккаунта '{accountId}'");
    }
}
