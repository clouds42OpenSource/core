﻿using Clouds42.Billing.Contracts.Factories;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Billing.Factories.PreparePaymentModelFactory
{
    public class PreparePaymentModelFactory(
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IServiceProvider serviceProvider,
        ILogger42 logger)
    {
        public TParam PreparePaymentModel<TModel, TParam>(PreparePaymentStruct param)
            where TModel : IPreparePaymentModel<TParam>
        {
            try
            {
                var modelFactory = serviceProvider.GetRequiredService<TModel>();
                
                var paymentModel = modelFactory.PrepareModel(unitOfWork, accessProvider, param, logger);
                logger.Info($"Иницировали платеж на сумму={param.Sum}, Платежная система={param.PaymentSystem} для аккаунта={param.AccountId}");
                return paymentModel;
            }
            catch (Exception exception)
            {
                handlerException.Handle(exception,$"[Ошибка формировании платежа] на сумму={param.Sum}, Платежная система={param.PaymentSystem} для аккаунта={param.AccountId}.");
                logger.Info($"Ошибка при формировании платежа: {exception.GetFullInfo()}");
                throw new InvalidOperationException($"Ошибка при формировании платежа: { exception.GetFullInfo()}");
            }
        }
    }
}
