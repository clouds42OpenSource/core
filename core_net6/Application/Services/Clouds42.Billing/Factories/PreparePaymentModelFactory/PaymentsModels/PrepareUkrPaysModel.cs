﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.Billing.Contracts.Factories.Models;
using Clouds42.Billing.Factories.Helpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Factories.PreparePaymentModelFactory.PaymentsModels
{
    public class PrepareUkrPaysModel(PaymentsManager paymentManager) : IPreparePaymentModel<UkrPayInitDataModel>
    {
        public UkrPayInitDataModel PrepareModel(IUnitOfWork unitOfWork, IAccessProvider accessProvider,
            PreparePaymentStruct parameters, ILogger42 logger)
        {
            var readyForPay = paymentManager.PreparePayment(parameters.AccountId, parameters.Sum, parameters.PaymentSystem);
            var helperModel = UkrPaysHelper.GetPaymentInitData(readyForPay.PaymentNumber + "_" + readyForPay.Account.IndexNumber, parameters.Sum);
            return helperModel;
        }
    }
}
