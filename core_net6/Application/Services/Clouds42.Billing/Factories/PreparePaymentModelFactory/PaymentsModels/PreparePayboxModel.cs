﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Factories.PreparePaymentModelFactory.PaymentsModels
{
    public class PreparePayboxModel(PaymentsManager paymentManager) : IPreparePaymentModel<string>
    {
        public string PrepareModel(IUnitOfWork unitOfWork, IAccessProvider accessProvider, PreparePaymentStruct parameters, ILogger42 logger)
        {   
            var readyForPay = paymentManager.PreparePayment(parameters.AccountId, parameters.Sum, parameters.PaymentSystem);
            var helperModel = PayboxCryptoProvider.GetPayboxRedirectUrl(parameters.Sum, readyForPay.PaymentNumber, readyForPay.Account.AccountConfiguration.SupplierId, $"Id компании = {readyForPay.Account.IndexNumber}");
            return helperModel;
        }
    }
}
