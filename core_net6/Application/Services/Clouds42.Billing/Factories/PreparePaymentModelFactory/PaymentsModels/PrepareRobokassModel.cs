﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.Billing.Factories.Helpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Factories.PreparePaymentModelFactory.PaymentsModels
{
    public class PrepareRobokassModel(PaymentsManager paymentManager) : IPreparePaymentModel<string>
    {
        public string PrepareModel(IUnitOfWork unitOfWork, IAccessProvider accessProvider, PreparePaymentStruct parameters, ILogger42 logger)
        {
            var readyForPay = paymentManager.PreparePayment(parameters.AccountId, parameters.Sum, parameters.PaymentSystem);
            if (readyForPay.Account == null)
                throw new InvalidOperationException("Не удалось получить аккаунт для инициализации платежа");

            return new RobokassaHelper(unitOfWork)
                .BuildPaymentUrl(parameters.Sum, readyForPay.PaymentNumber, $"id компании = {readyForPay.Account.IndexNumber}");
        }
    }
}