﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для работы с тарифом слуги
    /// </summary>
    public class BillingServiceTypeRateHelper(
        IUnitOfWork dbLayer,
        IOptimalRateProvider optimalRateProvider)
    {
        /// <summary>
        /// Получить все стоимости
        /// </summary>
        /// <param name="billingAccount">Аккаунт</param>
        /// <param name="invites">Модель данных</param>
        public IDictionary<Guid, decimal> GetRates(BillingAccount billingAccount,
            IEnumerable<CalculateBillingServiceTypeResultDto> invites)
        {
            var allServiceTypes = invites.Select(x => x.BillingServiceTypeId).Distinct();

            var rates = allServiceTypes
                .Select(w => new KeyValuePair<Guid, decimal>(w,
                    optimalRateProvider.GetOptimalRate(billingAccount, w)?.Cost
                    ?? throw new NotFoundException(
                        $"Не найдена ставка для услуги {w}, для аккаунта {billingAccount.Id}")));

            return rates.ToDictionary(w => w.Key, w => w.Value);
        }

        /// <summary>
        /// Получить все стоимости
        /// </summary>
        /// <param name="billingAccount">Аккаунт</param>
        /// <param name="billingServices">Услуги</param>
        public IDictionary<Guid, decimal> GetRates(BillingAccount billingAccount, IEnumerable<Guid> billingServices)
        {
            var rates = billingServices
                .Select(w => new KeyValuePair<Guid, decimal>(w,
                    optimalRateProvider.GetOptimalRate(billingAccount, w)?.Cost
                    ?? throw new NotFoundException(
                        $"Не найдена ставка для услуги {w}, для аккаунта {billingAccount.Id}")))
                .Distinct();

            return rates.ToDictionary(w => w.Key, w => w.Value);
        }

        /// <summary>
        /// Получить все стоимости
        /// </summary>
        /// <param name="billingServiceId">Сервис</param>
        /// <param name="accountId">Аккаунт</param>
        public IDictionary<Guid, decimal> GetRates(Guid billingServiceId, Guid accountId)
        {
            var billingAccount = dbLayer.AccountsRepository.FirstOrDefault(w => w.Id == accountId).BillingAccount;
            var billingServices = dbLayer.BillingServiceTypeRepository.WhereLazy(w => w.ServiceId == billingServiceId)
                .Select(w => w.Id).ToList();

            var rates = billingServices
                .Select(w => new KeyValuePair<Guid, decimal>(w,
                    optimalRateProvider.GetOptimalRate(billingAccount, w)?.Cost
                    ?? throw new NotFoundException(
                        $"Не найдена ставка для услуги {w}, для аккаунта {billingAccount.Id}")))
                .Distinct();

            return rates.ToDictionary(w => w.Key, w => w.Value);
        }

        /// <summary>
        /// Получить стоимость для услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Стоимость услуги</returns>
        public decimal GetRateForServiceType(Guid serviceTypeId, Guid accountId)
        {
            var billingAccount = dbLayer.AccountsRepository.FirstOrDefault(w => w.Id == accountId).BillingAccount;
            if (billingAccount == null)
            {
                throw new NotFoundException(
                       $"Не найден билинг для аккаунта {accountId} при активации демо ресурсов услуги {serviceTypeId}");
            }
            return optimalRateProvider.GetOptimalRate(billingAccount, serviceTypeId)?.Cost ??
                   throw new NotFoundException(
                       $"Не найдена ставка для услуги {serviceTypeId}, для аккаунта {billingAccount.Id}");
        }
    }
}
