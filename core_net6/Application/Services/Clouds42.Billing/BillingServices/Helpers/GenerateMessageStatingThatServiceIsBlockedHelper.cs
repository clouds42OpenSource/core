﻿using Clouds42.Configurations.Configurations;
using Clouds42.Configurations.Configurations.MailConfigurations;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для формирования сообщения о том что сервис заблокирован
    /// </summary>
    public static class GenerateMessageStatingThatServiceIsBlockedHelper
    {
        /// <summary>
        /// Сформировать сообщение о том что сервис заблокирован
        /// </summary>
        /// <param name="serviceName">Название сервиса</param>
        /// <returns>Сообщение о том что сервис заблокирован</returns>
        public static string GenerateMessage(string serviceName) =>
            $@"Сервис “{serviceName}” больше не поддерживается его разработчиком. 
            Вы можете подобрать аналог в <a href=""{CloudConfigurationProvider.Market.GetUrl()}"">Маркет42</a> или обратиться за помощью к нашим специалистам 
            <a href=""mailto:{MailConfiguration.Efsol.Credentials.Manager.GetLogin()}"">{MailConfiguration.Efsol.Credentials.Manager.GetLogin()}</a>";
    }
}
