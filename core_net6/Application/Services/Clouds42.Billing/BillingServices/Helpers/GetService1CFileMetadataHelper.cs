﻿using System.Xml;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для получения метаданных файла разработки 1С
    /// </summary>
    public static class GetService1CFileMetadataHelper
    {
        /// <summary>
        /// Определить тип перечисления для расширения файла разработки 1С
        /// </summary>
        /// <param name="fileName">Имя файла разработки 1С</param>
        /// <returns>Тип перечисления</returns>
        public static Service1CFileExtensionEnum DefineEnumTypeForService1CFileExtension(this string fileName)
        {
            var enumType = Enum.GetValues(typeof(Service1CFileExtensionEnum)).Cast<Service1CFileExtensionEnum>()
                .FirstOrDefault(extension => extension.Description() == GetService1CFileExtension(fileName));

            if (enumType == default)
                throw new InvalidOperationException(
                    $"Не удалось определить тип перечисления для расширения файла разработки 1С по имени {fileName}");

            return enumType;
        }

        /// <summary>
        /// Получить значение тэга
        /// </summary>
        /// <param name="xmlNode">Xml элемент</param>
        /// <param name="tagName">Название тэга</param>
        /// <returns>Значение тэга</returns>
        public static string GetTagValue(this XmlNode xmlNode, string tagName) =>
            xmlNode[tagName]?.InnerXml ??
            throw new InvalidOperationException($"Не удалось получить значение тэга {tagName}");

        /// <summary>
        /// Получить расширение файла разработки 1С
        /// </summary>
        /// <param name="fileName">Название файла</param>
        /// <returns>Расширение файла разработки 1С</returns>
        private static string GetService1CFileExtension(string fileName) => Path.GetExtension(fileName);
    }
}