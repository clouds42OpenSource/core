﻿namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для создания сервиса
    /// </summary>
    public static class CreateBillingServiceHelper
    {
        /// <summary>
        /// Получить дефолтное название сервиса биллинга
        /// </summary>
        /// <param name="countServicesForAccount">Количество сервисов у аккаунта</param>
        /// <param name="indexNumber">Номер</param>
        /// <returns>Дефолтное название для сервиса</returns>
        public static string GetDefaultBillingServiceName(int countServicesForAccount, int indexNumber)
            => $"{indexNumber}_Service_{countServicesForAccount + 1}";
    }
}
