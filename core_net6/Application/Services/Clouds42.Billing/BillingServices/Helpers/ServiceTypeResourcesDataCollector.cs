﻿using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Класс для сбора данных по ресурсам услуги биллинга
    /// </summary>
    public class ServiceTypeResourcesDataCollector(IUnitOfWork unitOfWork)
    {
        /// <summary>
        /// Собрать данные по ресурсам услуги биллинга
        /// </summary>
        /// <param name="serviceType">Услуга биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="resourcesForBillingInfo">Ресурсы для информации биллинга сервиса</param>
        /// <param name="rateService"></param>
        /// <returns>Данные по ресурсам услуги биллинга</returns>
        public ServiceTypeResourcesDataModel Collect(BillingServiceType serviceType, Guid accountId,
            List<Resource> resourcesForBillingInfo, Rate rateService)
        {
            var serviceTypeResourcesDataModel = new ServiceTypeResourcesDataModel();
            var (childServiceTypeIds, parentServiceTypeIds) = GetServiceTypeRelations(serviceType, accountId);
            var parentResourcesByUser = GetResourcesByUser(resourcesForBillingInfo, parentServiceTypeIds);
            var childResourcesByUser = GetResourcesByUser(resourcesForBillingInfo, childServiceTypeIds);

            var filteredResources = resourcesForBillingInfo
                .Where(w => w.BillingServiceTypeId == serviceType.Id);

            foreach (var resource in filteredResources)
            {
                var subject = resource.Subject;
                if (parentResourcesByUser.ContainsKey(subject) && parentResourcesByUser[subject].Any())
                    continue;

                var (iamSponsorCount, meSponsorCount) =
                    GetSponsoredLicensesCountData(resource, accountId);

                var childResourcesCost = childResourcesByUser.TryGetValue(subject, out var value)
                    ? value.Sum(s => s.Cost)
                    : 0;

                if (serviceType.Name == "Дополнительный сеанс")
                {
                    serviceTypeResourcesDataModel.UsedLicensesCount = (int)(resource.Cost / rateService.Cost);
                }

                else
                {
                    serviceTypeResourcesDataModel.UsedLicensesCount++;
                }

                serviceTypeResourcesDataModel.IamSponsorLicensesCount +=
                    iamSponsorCount;

                serviceTypeResourcesDataModel.MeSponsorLicensesCount +=
                    meSponsorCount;

                serviceTypeResourcesDataModel.ServiceTypeAmount +=
                    GetServiceTypeAmountByResources(resource, accountId, childResourcesCost);
            }

            return serviceTypeResourcesDataModel;
        }

        /// <summary>
        /// Группирует ресурсы по пользователю для заданных типов услуг
        /// </summary>
        private static Dictionary<Guid?, List<Resource>> GetResourcesByUser(
            List<Resource> resources, List<Guid> serviceTypeIds)
        {
            return resources
                .Where(w => serviceTypeIds.Contains(w.BillingServiceTypeId))
                .GroupBy(w => w.Subject)
                .ToDictionary(g => g.Key, g => g.ToList());
        }

        /// <summary>
        /// Получить стоимость услуги по ресурсам
        /// </summary>
        /// <param name="resource">Текущий ресурс услуги</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="childResourcesCost">Стоимость дочерних ресурсов</param>
        /// <returns>Стоимость услуги по ресурсам</returns>
        private static decimal GetServiceTypeAmountByResources(Resource resource, Guid accountId, decimal childResourcesCost)
        {
            if (resource.AccountId == accountId &&
                resource.AccountSponsorId == null ||
                resource.AccountSponsorId == accountId)
                return resource.Cost + childResourcesCost;

            return decimal.Zero;
        }

        /// <summary>
        /// Получить данные по количеству спонсорских лицензий(принадлежащих аккаунту и спонсору)
        /// </summary>
        /// <param name="resource">Ресурс услуги сервиса</param>
        /// <param name="currentAccountId">Id текущего аккаунта</param>
        /// <returns>Данные по количеству спонсорских лицензий(принадлежащих аккаунту и спонсору)</returns>
        private static (int IamSponsorLicensesCount, int MeSponsorLicensesCount) GetSponsoredLicensesCountData(
            Resource resource, Guid currentAccountId) =>
        (
            IamSponsorLicensesCount: resource.AccountSponsorId == currentAccountId ? 1 : 0,
            MeSponsorLicensesCount: resource.AccountId == currentAccountId && resource.AccountSponsorId != null ? 1 : 0
        );

        /// <summary>
        /// Получить данные по зависимостям услуги
        /// </summary>
        /// <param name="serviceType">Услуга биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные по зависимостям услуги</returns>
        private (List<Guid> ChildServiceTypeIds, List<Guid> ParentServiceTypeIds) GetServiceTypeRelations(
            BillingServiceType serviceType, Guid accountId)
        {
            var ids  = unitOfWork.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .Where(x => x.ServiceId == serviceType.ServiceId && x.BillingServiceTypesActivities.Any(z => z.AvailabilityDateTime > DateTime.Now && z.AccountId == accountId))
                .Select(x => x.Id)
                .ToList();

            return (GetDependencyRelations(serviceType, ids, st => st.ChildRelations.Select(x => x.ChildServiceType)).ToList(),
                GetDependencyRelations(serviceType, ids, st => st.ParentRelations.Select(x => x.MainServiceType)).ToList());

        }

        /// <summary>
        /// Получить зависимости для услуги
        /// </summary>
        /// <param name="billingServiceType">Услуга</param>
        /// <param name="serviceTypeIds">Список Id услуг</param>
        /// <param name="getRelations">Функция для получения зависимостей</param>
        /// <returns>Список Id зависимостей</returns>
        private static IEnumerable<Guid> GetDependencyRelations(
            BillingServiceType billingServiceType,
            List<Guid> serviceTypeIds,
            Func<BillingServiceType, IEnumerable<BillingServiceType>> getRelations)
        {
            var result = new HashSet<Guid>();
            var stack = new Stack<BillingServiceType>();
            stack.Push(billingServiceType);

            while (stack.Count > 0)
            {
                var currentServiceType = stack.Pop();

                var relatedServiceTypes = getRelations(currentServiceType)
                    .Where(st => serviceTypeIds.Contains(st.Id) || !st.IsDeleted);

                foreach (var serviceType in relatedServiceTypes)
                {
                    if (result.Add(serviceType.Id))
                    {
                        stack.Push(serviceType);
                    }
                }
            }

            return result;
        }
    }
}
