﻿using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Класс для выбора данных сервиса биллинга
    /// </summary>
    public class BillingServiceDataSelector(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить временные данные для обновления информации
        /// о сервисе биллинга в МС
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Модель временных данных
        /// для обновления информации о сервисе биллинга в МС</returns>
        public UpdateServiceInfoInMsTempDataModel GetTempDataForUpdateServiceInfoInMs(Guid serviceId) =>
            (from service in dbLayer.BillingServiceRepository.WhereLazy()
             where service.Id == serviceId
             select new UpdateServiceInfoInMsTempDataModel
             {
                 Id = service.Id,
                 Name = service.Name,
                 ShortDescription = service.ShortDescription,
                 Opportunities = service.Opportunities,
             }).FirstOrDefault() ??
            throw new NotFoundException(
                $"Не удалось получить данные сервиса '{serviceId}' для обновлении информации в МС");
    }
}
