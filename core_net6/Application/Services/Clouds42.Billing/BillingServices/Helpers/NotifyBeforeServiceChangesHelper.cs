﻿using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для уведомления об изменении сервиса
    /// </summary>
    public class NotifyBeforeServiceChangesHelper(IUnitOfWork dbLayer)
    {
        private readonly Lazy<string> _cpSite = new(CloudConfigurationProvider.Cp.GetSiteAuthorityUrl);
        private readonly Lazy<string> _routeValueForOpenBillingServicePage = new(CloudConfigurationProvider.Cp.GetRouteValueForOpenBillingServicePage);

        /// <summary>
        /// Получить ссылку для открытия страницы сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Ссылка для открытия страницы сервиса</returns>
        public string GetUrlForOpenBillingServicePage(Guid serviceId) =>
            $"{_cpSite.Value}/{_routeValueForOpenBillingServicePage.Value}{serviceId}";

        /// <summary>
        /// Получить файл разработки 1С
        /// </summary>
        /// <param name="service1CFileVersionId">Id версии файла разработки 1С</param>
        /// <returns>Файл разработки 1С</returns>
        public BillingService1CFile GetBillingService1CFile(Guid service1CFileVersionId) =>
            dbLayer.GetGenericRepository<BillingService1CFile>()
                .FirstOrDefault(sf => sf.Service1CFileVersionId == service1CFileVersionId) ??
            throw new NotFoundException($"Не удалось получить файл разработки по Id версии {service1CFileVersionId}");

        /// <summary>
        /// Получить названия услуг 
        /// </summary>
        /// <param name="serviceTypeIds">Список Id услуг</param>
        /// <returns>Названия услуг</returns>
        public List<string> GetServiceTypeNames(List<Guid> serviceTypeIds) =>
            (from serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy()
             join serviceTypeId in serviceTypeIds on serviceType.Id equals serviceTypeId
             select serviceType.Name).ToList();

        /// <summary>
        /// Получить сервис или выкинуть исключение
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        public BillingService GetBillingServiceOrThrowException(Guid serviceId) =>
            dbLayer.BillingServiceRepository.FirstOrThrowException(service => service.Id == serviceId,
                $"Не удалось получить сервис по Id '{serviceId}'");
    }
}
