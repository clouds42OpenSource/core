﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Класс для выбора данных биллинга по услугам сервиса
    /// </summary>
    public class BillingServiceInfoDataSelector(
        IUnitOfWork dbLayer,
        IBillingServiceTypeRelationHelper billingServiceTypeRelationHelper)
    {
        /// <summary>
        /// Выбрать временные данные биллинга по услугам сервиса для аккаунта
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Временные данные биллинга по услугам сервиса для аккаунта</returns>
        public List<BillingServiceTypeInfoTempDataModel> SelectBillingServiceTypeInfoTempDataModels(
            Guid billingServiceId,
            Guid accountId)
        {
            var account = dbLayer.BillingAccountRepository.AsQueryableNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .FirstOrDefault(x => x.Id == accountId);

            var result = dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .Include(x => x.Rates.Where(z =>
                    x.SystemServiceType.HasValue || z.LocaleId == account!.Account.AccountConfiguration.LocaleId &&
                    z.AccountType == account.AccountType))
                .Include(x => x.AccountRates.Where(z => z.AccountId == accountId))
                .Include(x => x.ChildRelations)
                .Include(x => x.ParentRelations)
                .Include(x => x.Service)
                .Include(x => x.BillingServiceTypesActivities)
                .Where(x => (!x.IsDeleted ||
                             x.BillingServiceTypesActivities.Any(z =>
                                 z.AccountId == accountId && z.AvailabilityDateTime > DateTime.Now) &&
                             x.ParentRelations.Any() &&
                             x.ChildRelations.Any()) &&
                            x.ServiceId == billingServiceId)
                .GroupBy(x => x.Id)
                .Select(x => new BillingServiceTypeInfoTempDataModel
                    {
                        ServiceTypeId = x.Key,
                        ServiceTypeCost = x.FirstOrDefault()!.AccountRates.FirstOrDefault() == null
                            ? x.FirstOrDefault()!.Rates.FirstOrDefault()!.Cost
                            : x.FirstOrDefault()!.AccountRates.FirstOrDefault()!.Cost,
                        ParentServiceTypeIds =
                            x.FirstOrDefault()!.ParentRelations.Select(z => z.MainServiceTypeId),
                        ChildServiceTypeIds = x.FirstOrDefault()!.ChildRelations
                            .Select(z => z.ChildServiceTypeId)
                    }
                ).ToList();

            var serviceTypeIds = result.Select(x => x.ServiceTypeId).ToList();

            var serviceTypes = dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .Where(x => serviceTypeIds.Contains(x.Id))
                .ToList();

            result.GroupJoin(serviceTypes, z => z.ServiceTypeId, x => x.Id, (model, types) => (model, types))
                .ToList()
                .ForEach(x =>
                {
                    if (x.types.Any())
                    {
                        x.model.ServiceType = x.types.First();
                    }
                });

            return result
                .OrderBy(x => billingServiceTypeRelationHelper.GetChildDependencyRelationsCount(x.ServiceType,
                    accountId))
                .ThenBy(serviceTypeData => serviceTypeData.ServiceType.Name)
                .ToList();
        }


        /// <summary>
        /// Выбрать ресурсы для информации биллинга сервиса по аккаунту
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Ресурсы для информации биллинга сервиса по аккаунту</returns>
        public List<Resource> SelectResourcesForBillingInfoByAccount(Guid billingServiceId, Guid accountId)
        {
            return dbLayer.ResourceRepository
                .AsQueryableNoTracking()
                .Where(x => (x.AccountId == accountId || x.AccountSponsorId == accountId) &&
                    x.BillingServiceType.ServiceId == billingServiceId && x.Subject.HasValue)
                .ToList();
        }

        /// <summary>
        /// Выбрать данные сервиса биллинга для аккаунта        
        /// <param name="accountId">Id аккаунта</param>
        /// </summary>
        /// <returns>Данные по сервисам биллинга для аккаунта</returns>
        public IQueryable<AccountBillingServiceDataDto> GetAccountServicesDataQuery(Guid accountId) =>
            from service in dbLayer.BillingServiceRepository.WhereLazy()
            join rc in dbLayer.ResourceConfigurationRepository.WhereLazy()
                on service.Id equals rc.BillingServiceId
            join activeResources in
                from serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy()
                join resource in dbLayer.ResourceRepository.WhereLazy()
                   on serviceType.Id equals resource.BillingServiceTypeId
                where
                   resource.Subject.HasValue &&
                   (resource.AccountId == accountId || resource.AccountSponsorId == accountId)
                select new { serviceType.ServiceId }
                on service.Id equals activeResources.ServiceId into activeResources
            where
                rc.AccountId == accountId &&
                (!service.SystemService.HasValue || service.SystemService == Clouds42Service.MyEnterprise)
            orderby service.SystemService descending
            select new AccountBillingServiceDataDto
            {
                Id = service.Id,
                Name = service.Name,
                SystemService = service.SystemService,
                IsActive =
                    service.SystemService.HasValue ||
                    service.IsActive && activeResources.Any() && (rc.Frozen != true || !rc.IsDemoPeriod)
            };
    }
}
