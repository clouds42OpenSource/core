﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Синхронизатор изменения подписки сервиса Тардис с апи Тардис.
    /// </summary>
    public class ChangeTardisServiceSubscriptionSynchronizator(
        IHandlerException handlerException,
        ITardisSynchronizer tardisSynchronizer,
        IUnitOfWork dbLayer)
        : IChangeTardisServiceSubscriptionSynchronizator
    {
        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="resources">Список изменяемых ресурсов.</param>
        public void SynchronizeWithTardis(List<Resource> resources)
            => resources.ForEach(SynchronizeWithTardis);

        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="resource">Изменяемый ресурс.</param>
        public void SynchronizeWithTardis(Resource resource)
        {
            if (!resource.Subject.HasValue)
                return;

            var accountUserId = resource.Subject.Value;
            if (resource.BillingServiceType.Service.InternalCloudService != InternalCloudServiceEnum.Tardis)
                return;

            ExecuteRequest(accountUserId);
        }

        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        public void SynchronizeWithTardis(Guid accountUserId)
        {
            if (!NeedSyncronize(accountUserId))
                return;

            ExecuteRequest(accountUserId);
        }

        /// <summary>
        /// Признак необходимости выполнять синхронизацию
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>true - если у пользователя есть ресурсы сервиса Тардис</returns>
        private bool NeedSyncronize(Guid accountUserId)
            => dbLayer.ResourceRepository.Any(res =>
                res.Subject == accountUserId && res.BillingServiceType.Service.InternalCloudService ==
                InternalCloudServiceEnum.Tardis);

        /// <summary>
        /// Выполнить запрос в Тардис
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        private void ExecuteRequest(Guid accountUserId)
        {
            try
            {
                tardisSynchronizer.Synchronize<UpdateAccountUserTrigger, AccountUserIdDto>(new AccountUserIdDto
                {
                    AccountUserId = accountUserId
                });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка уведомления Тардис о изменени подписки Саюри премиум у пользователя.]");
            }
        }
    }
}
