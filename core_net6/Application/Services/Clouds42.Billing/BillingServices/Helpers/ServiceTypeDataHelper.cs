﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для работы с данными услуги сервиса
    /// </summary>
    public class ServiceTypeDataHelper(
        IUnitOfWork dbLayer,
        IResourceDataProvider resourceDataProvider,
        ILimitOnFreeCreationDbForAccountDataProvider limitOnFreeCreationDbForAccountDataProvider,
        IAdditionalResourcesDataProvider additionalResourcesDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        /// Получить описание платных услуг сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <param name="paymentActionType">Тип действия платежа</param>
        /// <returns>Описание платных услуг сервиса</returns>
        public string GetPaidServiceTypesDefinition(Guid accountId, Guid billingServiceId,
            PaymentActionType paymentActionType)
        {
            var limit = limitOnFreeCreationDbForAccountDataProvider.GetLimitOnFreeCreationDbValue(accountId);
            var serviceTypeResourcesData = GetMyDatabasesServiceResourcesData(accountId, billingServiceId).ToList();

            if (!serviceTypeResourcesData.Any())
                return string.Empty;

            var licenseCountPrefix = paymentActionType == PaymentActionType.PayService
                ? "+"
                : string.Empty;

            AdjustResourcesCount(serviceTypeResourcesData);

            var resourcesInfoMessage = serviceTypeResourcesData.Aggregate(string.Empty,
                (current, next) =>
                {
                    var licensesOwner = next.BillingType == BillingTypeEnum.ForAccountUser
                        ? "пользователи "
                        : string.Empty;

                    if (next.SystemServiceType != ResourceType.CountOfDatabasesOverLimit)
                        return $"{current} {licensesOwner}{next.Name} {licenseCountPrefix}{next.VolumeInQuantity};";

                    var databasesCountForWhichYouNeedToPay =
                        limitOnFreeCreationDbForAccountDataProvider.GetDatabasesCountForWhichYouNeedToPay(accountId,
                            next.VolumeInQuantity, limit);

                    return databasesCountForWhichYouNeedToPay == 0
                        ? string.Empty
                        : $"{current} количество баз свыше {limit}: {databasesCountForWhichYouNeedToPay};";
                }).TrimStart().TrimEnd(';');

            resourcesInfoMessage += GenerateAdditionalResourcesDefinition(accountId);

            return resourcesInfoMessage;
        }

        /// <summary>
        /// Получить описание доп. ресурса аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Описание доп. ресурса</returns>
        private string GenerateAdditionalResourcesDefinition(Guid accountId)
        {
            var additionalResourcesData = additionalResourcesDataProvider.GetAdditionalResourcesData(accountId);
            return accountConfigurationDataProvider.IsVipAccount(accountId) &&
                   additionalResourcesData.AdditionalResourceName != null
                ? $"; {additionalResourcesData.AdditionalResourceName.Replace(Environment.NewLine, ", ")}"
                : string.Empty;
        }

        /// <summary>
        /// Скорректировать кол-во ресурсов
        /// </summary>
        /// <param name="serviceTypeResourcesData"></param>
        private static void AdjustResourcesCount(List<ServiceTypeResourceDataModel> serviceTypeResourcesData)
        {
            if (serviceTypeResourcesData.FirstOrDefault()?.SystemService != Clouds42Service.MyEnterprise)
                return;

            if (serviceTypeResourcesData.All(res => res.SystemServiceType != ResourceType.MyEntUser))
                return;

            var standartCount = serviceTypeResourcesData
                                    .FirstOrDefault(res => res.SystemServiceType == ResourceType.MyEntUser)
                                    ?.VolumeInQuantity ?? 0;

            var webServiceType =
                serviceTypeResourcesData.FirstOrDefault(res => res.SystemServiceType == ResourceType.MyEntUserWeb);
            if (webServiceType == null)
                return;

            webServiceType.VolumeInQuantity = Math.Abs(webServiceType.VolumeInQuantity - standartCount);
            if (webServiceType.VolumeInQuantity == 0)
                serviceTypeResourcesData.Remove(webServiceType);
        }

        /// <summary>
        /// Получить данные по ресурсам сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <returns>Данные по ресурсам сервиса</returns>
        private IQueryable<ServiceTypeResourceDataModel> GetMyDatabasesServiceResourcesData(Guid accountId,
            Guid billingServiceId) =>
            from resourcesData in resourceDataProvider.GetUsedPaidResourcesDataByServiceTypes(accountId)
            join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resourcesData.ServiceTypeId
                equals serviceType.Id
            join service in dbLayer.BillingServiceRepository.WhereLazy() on serviceType.ServiceId equals service.Id
            where service.Id == billingServiceId &&
                  (service.SystemService != Clouds42Service.MyDatabases || resourcesData.TotalAmount > 0)
            select new ServiceTypeResourceDataModel
            {
                Name = serviceType.Name,
                VolumeInQuantity = resourcesData.VolumeInQuantity,
                SystemServiceType = serviceType.SystemServiceType,
                SystemService = service.SystemService,
                BillingType = serviceType.BillingType
            };
    }
}
