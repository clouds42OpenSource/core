﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хэлпер для свзяей услуг сервиса
    /// </summary>
    public class BillingServiceTypeRelationHelper(IUnitOfWork dbLayer) : IBillingServiceTypeRelationHelper
    {
        /// <summary>
        ///     Получить все зависимые услуги
        /// </summary>
        /// <param name="billingServiceType">Услуга</param>
        /// <param name="accountId">ID аккаунта</param>
        public IEnumerable<BillingServiceType> GetAllDependencyRelations(BillingServiceType billingServiceType,
            Guid accountId)
        {
            var result = new List<BillingServiceType>();
            result.AddRange(GetParentDependencyRelations(billingServiceType, accountId));
            result.AddRange(GetChildDependencyRelations(billingServiceType, accountId));
            return result.Distinct();
        }

        /// <summary>
        /// Найти всех предков
        /// </summary>
        /// <param name="billingServiceType">Услуга</param>
        /// <param name="accountId">ID аккаунта</param>
        public IEnumerable<BillingServiceType> GetParentDependencyRelations(BillingServiceType billingServiceType,
            Guid accountId)
        {
            var deletedServiceTypes = GetDeletedButStillAvailableServiceTypeIdsForAccount(billingServiceType.ServiceId, accountId);
            var result = new List<BillingServiceType>();

            if (!deletedServiceTypes.Any()) 
                return result;

            var parents = billingServiceType.ParentRelations.Select(x => x.MainServiceType).Where(st => deletedServiceTypes.Contains(st.Id) || !st.IsDeleted).ToList();

            
            foreach (var serviceType in parents)
            {
                result.Add(serviceType);
                result.AddRange(GetParentDependencyRelations(serviceType, accountId));
            }

            return result.Distinct();
        }

        /// <summary>
        /// Найти всех потомков
        /// </summary>
        /// <param name="billingServiceType">Услуга</param>
        /// <param name="accountId">ID аккаунта</param>
        public IEnumerable<BillingServiceType> GetChildDependencyRelations(BillingServiceType billingServiceType,
            Guid accountId)
        {
            var deletedServiceTypes = GetDeletedButStillAvailableServiceTypeIdsForAccount(billingServiceType.ServiceId, accountId);

            var result = new List<BillingServiceType>();
            if (!deletedServiceTypes.Any())
                return result;

            var childs = billingServiceType.ChildRelations
                .Select(x => x.ChildServiceType)
                .Where(st => deletedServiceTypes.Contains(st.Id) || !st.IsDeleted).ToList();

            
            foreach (var serviceType in childs)
            {
                result.Add(serviceType);
                result.AddRange(GetChildDependencyRelations(serviceType, accountId));
            }

            return result.Distinct();
        }

        /// <summary>
        /// Получить количество всех потомков для услуги
        /// </summary>
        /// <param name="billingServiceType">Услуга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Количество всех потомков</returns>
        public int GetChildDependencyRelationsCount(BillingServiceType billingServiceType, Guid accountId) =>
            GetChildDependencyRelations(billingServiceType, accountId).Count();

        /// <summary>
        /// Получить список Id удаленных, но еще доступных услуг для аккаунта
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Cписок Id удаленных, но еще доступных услуг для аккаунта</returns>
        private List<Guid> GetDeletedButStillAvailableServiceTypeIdsForAccount(Guid billingServiceId,
            Guid accountId)
        {
            return dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .Where(x => x.ServiceId == billingServiceId && x.BillingServiceTypesActivities.Any(z => z.AvailabilityDateTime > DateTime.Now && z.AccountId == accountId))
                .Select(x => x.Id)
                .ToList();
        }

        /// <summary>
        /// Найти всех потомков, включая себя
        /// </summary>
        /// <param name="billingServiceType">Услуга</param>
        /// <param name="accountId">ID аккаунта</param>
        public IEnumerable<BillingServiceType> GetChildDependencyRelationsWithCurrent(
            BillingServiceType billingServiceType, Guid accountId)
        {
            var serviceTypes = GetChildDependencyRelations(billingServiceType, accountId).ToList();
            serviceTypes.Add(billingServiceType);
            return serviceTypes.Distinct();
        }
    }
}
