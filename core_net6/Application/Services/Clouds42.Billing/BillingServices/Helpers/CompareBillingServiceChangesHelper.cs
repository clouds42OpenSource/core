﻿
namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для сравнения изменений сервиса
    /// </summary>
    public static class CompareBillingServiceChangesHelper
    {

        /// <summary>
        /// Сравнить значение строк
        /// </summary>
        /// <param name="oldValue">Старое значение</param>
        /// <param name="newValue">Новое значение</param>
        /// <returns>true - если строки равны</returns>
        public static bool CompareStringValues(string oldValue, string newValue)
            => newValue.Equals(oldValue, StringComparison.InvariantCulture);

    }
}
