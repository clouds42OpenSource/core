﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для работы с услугой сервиса
    /// </summary>
    public static class BillingServiceTypeHelper
    {
        /// <summary>
        /// Получить услуги сервиса по типу биллинга
        /// </summary>
        /// <param name="billingService">Сервис</param>
        /// <param name="billingType">Тип биллинга</param>
        /// <returns>Услуги сервиса</returns>
        public static IEnumerable<BillingServiceType> GetServiceTypesByBillingType(
            this BillingService billingService, BillingTypeEnum billingType) =>
            billingService.BillingServiceTypes.Where(st => !st.IsDeleted && st.BillingType == billingType);
    }
}
