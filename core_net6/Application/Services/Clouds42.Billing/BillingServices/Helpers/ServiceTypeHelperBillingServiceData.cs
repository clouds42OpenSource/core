﻿using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для работы с услугами сервиса
    /// </summary>
    public static class ServiceTypeHelperBillingServiceData
    {
        /// <summary>
        /// Карта сопоставления системной услуги к типу подключения к информационной базе
        /// </summary>
        private static readonly Dictionary<ResourceType, ConnectionToDatabaseTypeEnum> MapResourceTypeToConnectionToDatabaseType =
            new()
            {
                [ResourceType.MyEntUser] = ConnectionToDatabaseTypeEnum.WebAndRdpConnection,
                [ResourceType.MyEntUserWeb] = ConnectionToDatabaseTypeEnum.WebOrRdpConnection
            };

        /// <summary>
        /// Выполнить маппинг системной услуги
        /// к типу подключения к информационной базе
        /// </summary>
        /// <param name="resourceType">Cистемная услуга биллинга</param>
        /// <returns>Тип подключения к информационной базе</returns>
        public static ConnectionToDatabaseTypeEnum MapToConnectionToDatabaseType(ResourceType? resourceType)
        {
            if (!resourceType.HasValue)
                return ConnectionToDatabaseTypeEnum.WithoutConnection;

            return MapResourceTypeToConnectionToDatabaseType.TryGetValue(resourceType.Value, out var value)
                ? value
                : ConnectionToDatabaseTypeEnum.WithoutConnection;
        }
    }
}
