﻿using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.BillingServices.Helpers
{
    public class BillingServiceDependencyHelper(IUnitOfWork dbLayer, ICloudLocalizer cloudLocalizer)
    {
        /// <summary>
        /// Проверить есть ли у данного пользователя все необходимые услуги от Аренды 1С
        /// </summary>
        /// <param name="subject">Владелец ресурса</param>
        /// <param name="serviceTypeId">Текущая услуга на подключение</param>
        public CalculateBillingServiceTypeErrorDto? CheckAccountUserToRent1CConnection(Guid subject, Guid serviceTypeId)
        {
            var billingServiceType = dbLayer.BillingServiceTypeRepository.FirstOrDefault(w => w.Id == serviceTypeId);
            if (!billingServiceType.DependServiceTypeId.HasValue)
                return null;

            var isAccountLicense = dbLayer.AccountsRepository.Any(a => a.Id == subject);

            var resourceAny = isAccountLicense
                ? dbLayer.ResourceRepository.Any(w => w.AccountId == subject && w.BillingServiceTypeId == billingServiceType.DependServiceTypeId)
                : dbLayer.ResourceRepository.Any(w => w.Subject == subject && w.BillingServiceTypeId == billingServiceType.DependServiceTypeId);

            if (resourceAny)
                return null;

            return new CalculateBillingServiceTypeErrorDto
            {
                Code = CalculateBillingServiceTypeErrorCode.Rent1CIsNotActive,
                Description = GetErrorMessageAboutRent1CActivation(subject, billingServiceType)
            };
        }

        /// <summary>
        /// Проверить является ли пользователь заблокированным
        /// </summary>
        /// <param name="subject">Id пользователя</param>
        /// <param name="isUserSponsored">Признак что пользователь спонсируется</param>
        /// <returns>Результат активации услуги сервиса</returns>
        public CalculateBillingServiceTypeErrorDto? CheckAccountUserIsActivated(Guid subject, bool isUserSponsored)
        {
            var accountUserInfo = GetShortAccountUserInfo(subject);

            if (accountUserInfo == null || accountUserInfo.Activated)
                return null;

            return new CalculateBillingServiceTypeErrorDto
            {
                Code = CalculateBillingServiceTypeErrorCode.AccountUserIsNotActivated,
                Description = GenerateMessageForDisabledUser(accountUserInfo.Login, accountUserInfo.AccountCaption,
                    isUserSponsored)
            };
        }

        /// <summary>
        /// Получить сокращенную информацию по пользователю
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <returns>Модель информации по пользователю</returns>
        private AccountUserShortInfoDto? GetShortAccountUserInfo(Guid userId) =>
            (from user in dbLayer.AccountUsersRepository.WhereLazy()
             join account in dbLayer.AccountsRepository.WhereLazy() on user.AccountId equals account.Id
             where user.Id == userId
             select new AccountUserShortInfoDto
             {
                 Login = user.Login,
                 AccountCaption = account.AccountCaption,
                 Activated = user.Activated
             }).FirstOrDefault();

        /// <summary>
        /// Сгенерировать сообщение об ошибке отключенного пользователя
        /// </summary>
        /// <param name="userLogin">Логин пользователя</param>
        /// <param name="accountName">Название аккаунта</param>
        /// <param name="isUserSponsored">Признак что пользователь спонсируется</param>
        /// <returns>Строка об ошибке отключенного пользователя</returns>
        private static string GenerateMessageForDisabledUser(string userLogin, string accountName, bool isUserSponsored) =>
            $"Пользователь <b>{userLogin}</b> заблокирован администратором аккаунта" +
            (isUserSponsored
                ? $" <b>{accountName}</b>. Подключение к сервисам недоступно."
                : ". Подключение к сервисам недоступно. Разблокировать пользователя вы сможете в меню Пользователи.");

        /// <summary>
        /// Получить полное имя пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <returns>Полное имя пользователя</returns>
        private static string GetUserName(AccountUser? accountUser)
        {
            if (accountUser == null)
                return "";

            return !string.IsNullOrEmpty(accountUser.FirstName + accountUser.MiddleName + accountUser.LastName)
                ? $"{accountUser.Login} ({accountUser.FirstName} {accountUser.MiddleName} {accountUser.LastName})"
                : accountUser.Login;
        }

        /// <summary>
        /// Получить пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <returns>Пользователь аккаунта</returns>
        private AccountUser GetUser(Guid userId) =>
            dbLayer.AccountUsersRepository.FirstOrDefault(w => w.Id == userId);

        /// <summary>
        /// Получить сообщение о необходимости активации Аренды 1С
        /// </summary>
        /// <returns>Сообщение о необходимости активации Аренды 1С</returns>
        private string GetErrorMessageAboutRent1CActivation(Guid accountUserId, BillingServiceType billingServiceType)
        {
            var accountUser = GetUser(accountUserId);
            var rent1CName = accountUser != null
                ? cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountUser.AccountId)
                : Clouds42Service.MyEnterprise.GetDisplayName();

            if (billingServiceType.Service.SystemService != Clouds42Service.MyDatabases)
                return $"Пользователь <b>{GetUserName(accountUser)}</b> не подключен к сервису " +
                       $"{rent1CName} по услуге \"<b>{GetServiceName(billingServiceType.DependServiceType)}</b>\". " +
                       $"Работа с сервисом \"<b>{billingServiceType.Name}</b>\" " +
                       $"возможна только при подключенной услуге \"<b>{GetServiceName(billingServiceType.DependServiceType)}</b>\"";

            return $"Пользователь <b>{GetUserName(accountUser)}</b> не подключен к сервису {rent1CName}. " +
                   $"Работа с сервисом \"<b>{billingServiceType.Name}</b>\" возможна только при подключенном сервисе {rent1CName}.";
        }

        /// <summary>
        /// Получить скорректированное название услуги сервиса
        /// </summary>
        private static string? GetServiceName(IBillingServiceType billingServiceType)
        {
            return billingServiceType.SystemServiceType switch
            {
                ResourceType.MyEntUser => "Стандарт",
                ResourceType.MyEntUserWeb => "WEB",
                _ => null
            };
        }
    }
}
