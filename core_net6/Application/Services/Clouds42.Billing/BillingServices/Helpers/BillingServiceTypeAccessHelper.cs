﻿using Clouds42.Billing.BillingServices.Handlers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Logger;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Хелпер для работы с доступами к услугам сервиса
    /// </summary>
    public class BillingServiceTypeAccessHelper(
        IUnitOfWork dbLayer,
        BillingServiceTypeRateHelper typeRateHelper,
        IChangeTardisServiceSubscriptionSynchronizator changeTardisServiceSubscriptionSynchronizator,
        ITardisBillingServiceProvider tardisBillingServiceProvider,
        IBillingServiceTypeRelationHelper billingServiceTypeRelationHelper,
        EnableServiceTypesToUsersOperationHandler enableServiceTypesToUsersOperationHandler,
        ILogger42 logger,
        IDeleteServiceExtensionDatabaseProvider deleteServiceExtensionDatabaseProvider)
    {
        /// <summary>
        /// Применить ресурсы
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="dataAccountUsers">Список инвайтов</param>
        public void ApplyResources(IAccount account, ICollection<CalculateBillingServiceTypeResultDto> dataAccountUsers)
        {
            logger.Trace($"Начинаем обработку ресурсов для аккаунта '{account.IndexNumber}'");
            var services = GetBillingServiceTypes(dataAccountUsers);

            DisableResourcesForInactiveInvites((Account)account, dataAccountUsers, services);
            EnableResourcesForActiveInvites((Account)account, dataAccountUsers, services);

            DeleteServicesWithoutUsers(account, services.Select(s => s.ServiceId).FirstOrDefault(), services);

            logger.Trace($"Обработка ресурсов для аккаунта '{account.IndexNumber}' завершена");
        }

        /// <summary>
        /// Получить пользователя по Id
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        public AccountUser? GetAccountUserById(Guid? accountUserId)
        {
            if (accountUserId != null)
                return dbLayer.AccountUsersRepository.GetAccountUser(accountUserId.Value);

            return null;
        }

        /// <summary>
        /// Проверка на отсутствие ресурса для Subject
        /// </summary>
        /// <param name="calculateBillingServiceTypeResultDto">Инвайт</param>
        /// <returns>Результат проверки</returns>
        public bool IsNotExistResource(CalculateBillingServiceTypeResultDto calculateBillingServiceTypeResultDto)
            => dbLayer.ResourceRepository.FirstOrDefault(resource =>
                   resource.BillingServiceTypeId == calculateBillingServiceTypeResultDto.BillingServiceTypeId
                   && resource.Subject == calculateBillingServiceTypeResultDto.Subject) == null;

        /// <summary>
        /// Отключить все ресурсы на выключенные инвайты
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="dataAccountUsers">Услуги на подключение к пользователям</param>
        /// <param name="serviceTypes">Услуги</param>
        private void DisableResourcesForInactiveInvites(Account account,
            ICollection<CalculateBillingServiceTypeResultDto> dataAccountUsers, List<BillingServiceType> serviceTypes)
        {
            var services = new List<Guid>();

            var inviteServices = (from dataAccountUser in dataAccountUsers.Where(data => !data.Status)
                                  join resource in dbLayer.ResourceRepository.WhereLazy(resource => resource.Subject != null) on
                                      dataAccountUser
                                          .BillingServiceTypeId equals resource.BillingServiceTypeId
                                  select dataAccountUser.BillingServiceTypeId).Distinct().ToList();

            foreach (var dependencyRelations in inviteServices.Select(id => serviceTypes.FirstOrDefault(w => w.Id == id)?? new BillingServiceType()).Select(billingServiceType => billingServiceTypeRelationHelper
                         .GetChildDependencyRelationsWithCurrent(billingServiceType, account.Id)
                         .Select(x => x.Id)
                         .Distinct()
                         .ToList()))
            {
                services.AddRange(dependencyRelations);
            }

            var subjects = dataAccountUsers.Where(w => !w.Status).Select(w => w.Subject).Cast<Guid?>().Distinct();

            var resources = dbLayer.ResourceRepository
                .WhereLazy(w =>
                    (w.AccountId == account.Id || w.AccountSponsorId == account.Id) &&
                    services.Contains(w.BillingServiceTypeId) && subjects.Contains(w.Subject))
                .ToList();

            foreach (var resource in resources)
            {
                if (resource.AccountSponsorId != null && resource.AccountSponsorId != Guid.Empty)
                {
                    resource.AccountId = resource.AccountSponsorId.Value;
                    resource.AccountSponsorId = null;
                }

                logger.Trace(
                    $"Отключили ресурс '{resource.BillingServiceType.Name}'-'{resource.Id}' у пользователя '{GetAccountUserById(resource.Subject)?.Login}'");

                tardisBillingServiceProvider.ManageAccessToTardisAccountDatabase(resource.Subject,
                    resource.BillingServiceType.Service.Id, UpdateAcDbAccessActionType.Delete);
                changeTardisServiceSubscriptionSynchronizator.SynchronizeWithTardis(resource);

                resource.Subject = null;
                dbLayer.ResourceRepository.Update(resource);
            }

            if (resources.Any())
                dbLayer.Save();
        }

        /// <summary>
        /// Подключить все ресурсы на включенные инвайты
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="serviceTypesData">Услуги на управление лицензиями</param>
        /// <param name="serviceTypes">Список услуг на управление лицензиями</param>
        private void EnableResourcesForActiveInvites(Account account,
            ICollection<CalculateBillingServiceTypeResultDto> serviceTypesData,
            List<BillingServiceType> serviceTypes)
        {
            var serviceTypesDataForEnablingToUsers = GetServiceTypesDataForEnablingToUsers(account.BillingAccount,
                SelectServiceTypesDataForEnablingWithoutResources(serviceTypesData),
                serviceTypes);

            enableServiceTypesToUsersOperationHandler.Handle(serviceTypesDataForEnablingToUsers, account.Id);
        }

        /// <summary>
        /// Выбрать данные услуг на подключение,
        /// по которым у пользователей нет ресурсов
        /// </summary>
        /// <param name="serviceTypesData">Услуги на управление лицензиями</param>
        /// <returns>Данные услуг на подключение,
        /// по которым у пользователей нет ресурсов</returns>
        private List<CalculateBillingServiceTypeResultDto> SelectServiceTypesDataForEnablingWithoutResources(
            ICollection<CalculateBillingServiceTypeResultDto> serviceTypesData) => serviceTypesData
            .Where(serviceTypeData => serviceTypeData.Status && IsNotExistResource(serviceTypeData))
            .ToList();

        /// <summary>
        /// Получить список моделей данных по услугам для подключения к пользователям
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="serviceTypesDataForEnablingToUsers">Данные по услугам на подключение к пользователям</param>
        /// <param name="serviceTypes">Список услуг на управление лицензиями</param>
        /// <returns>Список моделей данных по услугам для подключения к пользователям</returns>
        private List<ServiceTypesDataForEnablingToUserModel> GetServiceTypesDataForEnablingToUsers(
            BillingAccount billingAccount,
            ICollection<CalculateBillingServiceTypeResultDto> serviceTypesDataForEnablingToUsers,
            List<BillingServiceType> serviceTypes)
        {
            var tempDataList = SelectServiceTypeIdsWithDependencyRelations(billingAccount.Id,
                serviceTypesDataForEnablingToUsers, serviceTypes);

            var rates = typeRateHelper.GetRates(billingAccount,
                tempDataList.SelectMany(tempData => tempData.DependencyRelations));

            return (from serviceTypesDataForEnablingToUser in serviceTypesDataForEnablingToUsers
                    join tempData in tempDataList on serviceTypesDataForEnablingToUser.BillingServiceTypeId equals tempData
                        .ServiceTypeId into tempDataForServiceTypeList
                    from tempData in tempDataForServiceTypeList.Take(1)
                    select new ServiceTypesDataForEnablingToUserModel
                    {
                        ServiceTypeEnablingData = serviceTypesDataForEnablingToUser,
                        ServiceTypesDataForEnabling = (from serviceTypeId in tempData.DependencyRelations
                                                       join rate in rates on serviceTypeId equals rate.Key
                                                       select new ServiceTypeDataModel
                                                       {
                                                           ServiceTypeId = serviceTypeId,
                                                           ServiceTypeCost = rate.Value
                                                       }).ToList()
                    }).ToList();
        }

        /// <summary>
        /// Выбрать список Id услуг с зависимыми услугами
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypesDataForEnablingToUsers">Данные по услугам на подключение к пользователям</param>
        /// <param name="serviceTypes">Список услуг на управление лицензиями</param>
        /// <returns>Список Id услуг с зависимыми услугами</returns>
        private List<(Guid ServiceTypeId, List<Guid> DependencyRelations)> SelectServiceTypeIdsWithDependencyRelations(
            Guid accountId, ICollection<CalculateBillingServiceTypeResultDto> serviceTypesDataForEnablingToUsers,
            List<BillingServiceType> serviceTypes) =>
            (from serviceTypeDataToEnable in serviceTypesDataForEnablingToUsers
             join serviceType in serviceTypes on serviceTypeDataToEnable.BillingServiceTypeId equals serviceType.Id
             select
             (
                 ServiceTypeId: serviceTypeDataToEnable.BillingServiceTypeId,
                 DependencyRelations: GetChildDependencyRelationsWithCurrentServiceTypeIds(serviceType, accountId)
             )).ToList();

        /// <summary>
        /// Получить список Id дочерних услуг включая текущую услугу 
        /// </summary>
        /// <param name="serviceType">Услуга сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список Id дочерних услуг включая текущую услугу </returns>
        private List<Guid> GetChildDependencyRelationsWithCurrentServiceTypeIds(BillingServiceType serviceType,
            Guid accountId) => billingServiceTypeRelationHelper
            .GetChildDependencyRelationsWithCurrent(serviceType, accountId)
            .Select(x => x.Id)
            .Distinct()
            .ToList();

        /// <summary>
        /// Получить список типов сервисов
        /// </summary>
        /// <param name="dataAccountUsers">Список инвайтов</param>
        private List<BillingServiceType> GetBillingServiceTypes(
            IEnumerable<CalculateBillingServiceTypeResultDto> dataAccountUsers)
        {
            var ids = dataAccountUsers.Select(w => w.BillingServiceTypeId).Distinct();

            return dbLayer.BillingServiceTypeRepository.WhereLazy(w => ids.Contains(w.Id)).ToList();
        }

        /// <summary>
        /// Удалить сервисы, к которым не подключен ни один пользователь
        /// </summary>
        /// <param name="account"></param>
        /// <param name="serviceId">Идентификатор сервиса</param>
        /// <param name="serviceTypes">Коллекция типов сервиса </param>
        private void DeleteServicesWithoutUsers(IAccount account, Guid serviceId, ICollection<BillingServiceType> serviceTypes)
        {
            var billingServiceTypesId = serviceTypes.Select(x => x.Id).ToList();

            var anyActiveResourcesForAccount = dbLayer.ResourceRepository
                .AsQueryableNoTracking()
                .Any(x => x.AccountId == account.Id && x.Subject.HasValue &&
                          billingServiceTypesId.Contains(x.BillingServiceTypeId));
                
            if (anyActiveResourcesForAccount)
            {
                return;
            }

            logger.Trace($"Сервис {serviceId} удален для аккаунта {account.IndexNumber}");

            var accountDatabases = dbLayer.DatabasesRepository.WhereLazy(d => d.AccountId == account.Id).
                Join(dbLayer.GetGenericRepository<ServiceExtensionDatabase>().All(),
                    d => d.Id,
                    se => se.AccountDatabaseId,
                    (d, se) => se).
                Where(se => se.ServiceId == serviceId && (
                    se.ServiceExtensionDatabaseStatus == ServiceExtensionDatabaseStatusEnum.DoneInstall ||
                    se.ServiceExtensionDatabaseStatus == ServiceExtensionDatabaseStatusEnum.ProcessingInstall)).
                Select(se => new DeleteServiceExtensionFromDatabaseDto
                {
                    ServiceId = se.ServiceId,
                    DatabaseId = se.AccountDatabaseId,
                    Install = false
                }).ToList();

            accountDatabases.ForEach(deleteServiceExtensionDatabaseProvider.DeleteServiceExtensionFromDatabase);
        }
    }
}
