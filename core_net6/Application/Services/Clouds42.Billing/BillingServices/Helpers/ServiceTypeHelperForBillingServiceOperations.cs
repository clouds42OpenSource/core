﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Helpers
{
    /// <summary>
    /// Класс хелпер для работы
    /// с услугами сервиса биллинга
    /// </summary>
    public class ServiceTypeHelperForBillingServiceOperations(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить стоимость услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Стоимость услуги</returns>
        public decimal GetServiceTypeCost(Guid serviceTypeId) =>
            dbLayer.RateRepository.FirstOrDefault(w => w.BillingServiceTypeId == serviceTypeId)?.Cost ?? 0;

        /// <summary>
        /// Сджойнить услуги и их изменения
        /// </summary>
        /// <param name="serviceTypes">Услуги</param>
        /// <param name="serviceTypeChanges">Изменения по услугам</param>
        /// <returns>Связь услуг и их изменений</returns>
        public Dictionary<BillingServiceType, BillingServiceTypeChange> JoinServiceTypesWithItsChanges(
            ICollection<BillingServiceType> serviceTypes, ICollection<BillingServiceTypeChange> serviceTypeChanges) =>
            serviceTypes.Join(
                serviceTypeChanges, oldSt => oldSt.Id,
                newSt => newSt.BillingServiceTypeId,
                (oldSt, newSt) => new
                {
                    ServiceType = oldSt,
                    ServiceTypeChange = newSt
                })
            .ToDictionary(st => st.ServiceType, st => st.ServiceTypeChange);
    }
}
