﻿using Clouds42.BLL.Common.Extensions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.BillingService.BillingService1C;
using Microsoft.AspNetCore.Http;

namespace Clouds42.Billing.BillingServices.Mappers
{
    /// <summary>
    /// Маппер входящей модели процесса для получения метаданных 
    /// </summary>
    public static class IncomingModelOfMetadataGettingProcessMapper
    {
        /// <summary>
        /// Выполнить маппинг
        /// </summary>
        /// <param name="file">Файл разработки 1С</param>
        /// <returns>Входящая модель процесса для получения метаданных</returns>
        public static IncomingModelOfMetadataGettingProcessDto PerformMapping(this CloudFileDataDto<IFormFile> file) =>
            new()
            {
                FileName = file.FileName,
                File = file.Content.ConvertToByteArray()
            };
    }
}
