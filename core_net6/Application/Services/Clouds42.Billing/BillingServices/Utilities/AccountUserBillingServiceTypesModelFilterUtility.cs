﻿using Clouds42.Billing.Contracts.BillingServices.Models;

namespace Clouds42.Billing.BillingServices.Utilities
{
    /// <summary>
    /// Утилита для фильтра моделей данных услуг по пользователям
    /// </summary>
    public static class AccountUserBillingServiceTypesModelFilterUtility
    {
        /// <summary>
        /// Применить фильтр для моделей данных услуг по пользователям
        /// </summary>
        /// <param name="serviceTypesDataByAccountUsers">Данные услуг по пользователям</param>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>Отфильтрованные данные услуг по пользователям</returns>
        public static IQueryable<AccountUserBillingServiceTypesModel> ApplyFilter(
            this IQueryable<AccountUserBillingServiceTypesModel> serviceTypesDataByAccountUsers, string searchString) =>
            from serviceTypesDataByAccountUser in serviceTypesDataByAccountUsers
            where string.IsNullOrEmpty(searchString) ||
                  serviceTypesDataByAccountUser.AccountUserInfo.FirstName.ToLower().Contains(searchString) ||
                  serviceTypesDataByAccountUser.AccountUserInfo.MiddleName.ToLower().Contains(searchString) ||
                  serviceTypesDataByAccountUser.AccountUserInfo.FirstName.ToLower().Contains(searchString) ||
                  serviceTypesDataByAccountUser.AccountUserInfo.Login.ToLower().Contains(searchString)
            select serviceTypesDataByAccountUser;
    }
}