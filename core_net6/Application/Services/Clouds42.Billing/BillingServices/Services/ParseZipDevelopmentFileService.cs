﻿using System.IO.Compression;
using System.Xml;
using Clouds42.AgencyAgreement.Contracts.AgentFileStorage;
using Clouds42.Billing.BillingServices.Constants;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Services;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;

namespace Clouds42.Billing.BillingServices.Services
{
    /// <summary>
    /// Служба парсинга zip файла разработки 1С 
    /// </summary>
    internal class ParseZipDevelopmentFileService(IAgentFileStorageProvider agentFileStorageProvider)
        : IParseZipDevelopmentFileService
    {
        /// <summary>
        /// Выполнить парсинг файла
        /// </summary>
        /// <param name="fileData">Данные файла</param>
        /// <returns>Xml документ содержащий метаданные</returns>
        public XmlDocument Parse(byte[] fileData)
        {
            var fullPathExtractedManifest =
                Path.Combine(agentFileStorageProvider.GetTemporaryFolderInAgentStorage(), $"{Guid.NewGuid():D}");

            ExtractXmlFileWithMetadata(fullPathExtractedManifest, fileData);

            var xmlDoc = fullPathExtractedManifest.GetXmlDocumentFromPath();
            File.Delete(fullPathExtractedManifest);
            return xmlDoc;
        }

        /// <summary>
        /// Извлечь файл с метаданными из zip архива
        /// </summary>
        /// <param name="fullPathForExtractFile">Путь для извлечения файла</param>
        /// <param name="fileData">Данные файла</param>
        private void ExtractXmlFileWithMetadata(string fullPathForExtractFile, byte[] fileData)
        {
            using var memoryStream = new MemoryStream(fileData);
            using var zip = new ZipArchive(memoryStream, ZipArchiveMode.Read);
            var fileInZip =
                zip.Entries.FirstOrDefault(file => file.Name == Service1CFileMetadataConst.ManifestName) ??
                throw new NotFoundException(
                    $"Не удалось найти файл {Service1CFileMetadataConst.ManifestName} в zip архиве");

            fileInZip.ExtractToFile(fullPathForExtractFile);
        }
    }
}
