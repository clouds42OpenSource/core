﻿using Clouds42.Billing.BillingServices.Providers;
using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.BillingServices.Actions
{
    /// <summary>
    /// Действие по созданию черновика сервиса биллинга
    /// </summary>
    internal class CreateBillingServiceDraftAction(CreateServiceProviderBase createServiceProviderBase)
        : IServiceOperationAction<CreateBillingServiceDto, Guid>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="inputModel">Модель создания сервиса</param>
        /// <returns>ID сервиса</returns>
        public Guid Do(CreateBillingServiceDto inputModel)
        {
            return createServiceProviderBase.SaveBillingService(inputModel);
        }
    }
}
