﻿namespace Clouds42.Billing.BillingServices.Actions
{
    /// <summary>
    /// Действие над сервисом биллинга
    /// </summary>
    public interface IServiceOperationAction<in TInputModel, out TOutputModel>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="inputModel">Входная модель данных</param>
        /// <returns>Результат выполнения действия</returns>
        TOutputModel Do(TInputModel inputModel);
    }
}
