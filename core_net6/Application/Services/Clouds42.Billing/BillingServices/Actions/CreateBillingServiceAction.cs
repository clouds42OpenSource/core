﻿using Clouds42.Billing.BillingServices.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.BillingServices.Actions
{
    /// <summary>
    /// Действие по созданию сервиса биллинга
    /// </summary>
    internal class CreateBillingServiceAction(
        CreateServiceProviderBase createServiceProviderBase,
        ICreateServiceRequestProvider createServiceRequestProvider)
        : IServiceOperationAction<CreateBillingServiceDto, Guid>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="inputModel">Модель создания сервиса</param>
        /// <returns>ID сервиса</returns>
        public Guid Do(CreateBillingServiceDto inputModel)
        {
            createServiceProviderBase.ValidateCreationOfNewBillingService(inputModel);
            var billingServiceId = createServiceProviderBase.SaveBillingService(inputModel);

            createServiceRequestProvider.Create(billingServiceId, inputModel.Name, inputModel.AccountId, inputModel.CreateServiceRequestId);

            return billingServiceId;
        }
    }
}
