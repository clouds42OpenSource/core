﻿using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.BillingServices.Validators;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Базовый провайдер создания сервиса биллинга
    /// </summary>
    internal class CreateServiceProviderBase(
        IUnitOfWork dbLayer,
        BillingServiceValidator billingServiceValidator,
        ICloudFileProvider cloudFileProvider,
        ICreateRateProvider createRateProvider,
        IRemoveRateProvider removeRateProvider,
        ILogger42 logger,
        IConfiguration configuration)
        : BaseOperationServiceProvider(dbLayer,
            billingServiceValidator, cloudFileProvider, removeRateProvider,
            logger)
    {
        /// <summary>
        /// Сохранить данные сервиса
        /// </summary>
        /// <param name="createBillingServiceDto">Модель создания сервиса</param>
        /// <returns>ID сервиса</returns>
        public Guid SaveBillingService(CreateBillingServiceDto createBillingServiceDto)
        {
            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                var accountId = createBillingServiceDto.TryGetAccountId();
                var indexNumber = DbLayer.AccountsRepository.GetAccount(accountId)?.IndexNumber;

                if (indexNumber == null)
                    throw new NotFoundException(
                        $"Во время создания сервиса произошла ошибка. Аккаунт с Id={accountId} не существует.");

                var countServicesForAccount = DbLayer.BillingServiceRepository
                    .Where(w => w.AccountOwnerId == accountId).Count();

                var serviceName = !string.IsNullOrEmpty(createBillingServiceDto.Name)
                    ? createBillingServiceDto.Name
                    : CreateBillingServiceHelper.GetDefaultBillingServiceName(countServicesForAccount,
                        (int)indexNumber);

                var service = new BillingService
                {
                    Id = createBillingServiceDto.Key,
                    Key = createBillingServiceDto.Key,
                    IsHybridService = createBillingServiceDto.IsHybridService,
                    IsActive = true,
                    Name = serviceName,
                    AccountOwnerId = accountId,
                    BillingServiceStatus = createBillingServiceDto.BillingServiceStatus,
                    StatusDateTime = DateTime.Now
                };

                DbLayer.BillingServiceRepository.Insert(service);
                DbLayer.Save();
                SaveBillingServiceData(createBillingServiceDto, service.Id, accountId);

                DbLayer.Save();
                transaction.Commit();

                return service.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Провалидировать создаваемый сервис биллинга
        /// </summary>
        /// <param name="createBillingServiceDto">Модель создания сервиса</param>
        public void ValidateCreationOfNewBillingService(CreateBillingServiceDto createBillingServiceDto)
        {
            var validateResult = BillingServiceValidator.ValidateCreationOfNewBillingService(createBillingServiceDto);

            if (!validateResult.Success)
                throw new ValidateException(validateResult.Message);
        }

        /// <summary>
        /// Сохранить данные сервиса
        /// </summary>
        /// <param name="createBillingServiceDto">Модель создания сервиса</param>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        private void SaveBillingServiceData(CreateBillingServiceDto createBillingServiceDto, Guid serviceId,
            Guid accountId)
        {
            var locale = DbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(configuration["DefaultLocale"]?? ""));

            SaveBillingServiceTypesForBillingService(serviceId, createBillingServiceDto.BillingServiceTypes);
            createBillingServiceDto.BillingServiceTypes.ForEach(bs =>
                createRateProvider.Create(bs.Id, bs.ServiceTypeCost, locale.ID));
            createBillingServiceDto.BillingServiceTypes.ForEach(w =>
                SaveBillingServiceTypeRelation(w.Id, w.ServiceTypeRelations));
        }

        
    }
}
