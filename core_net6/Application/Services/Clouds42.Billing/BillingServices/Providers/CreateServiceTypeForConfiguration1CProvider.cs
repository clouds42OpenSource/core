﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер создания услуги сервиса биллинга
    /// для конфигурации 1С (Для сервиса "Мои инф. базы")
    /// </summary>
    internal class CreateServiceTypeForConfiguration1CProvider(
        IUnitOfWork dbLayer,
        ICreateConfigurationServiceTypeRelationProvider createConfigurationServiceTypeRelationProvider,
        ICreateRateProvider createRateProvider,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IHandlerException handlerException)
        : ICreateServiceTypeForConfiguration1CProvider
    {
        /// <summary>
        /// Создать услугу сервиса для конфигурации 1С
        /// </summary>
        /// <param name="model">Модель создания услуги для конфигурации 1С</param>
        public void Create(PerformOperationOnServiceTypeByConfig1CDto model)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var serviceTypeId = CreateServiceTypeByConfigurationName(model.ConfigurationName);
                createRateProvider.Create(serviceTypeId, model.ConfigurationCost);
                createConfigurationServiceTypeRelationProvider.Create(serviceTypeId, model.ConfigurationName);

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка Создания услуги сервиса для конфигурации 1С] {model.ConfigurationName}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать услугу по названию конфигурации
        /// </summary>
        /// <param name="configuration1CName">Название конфигурации</param>
        /// <returns>Id созданной услуги</returns>
        private Guid CreateServiceTypeByConfigurationName(string configuration1CName)
        {
            var serviceTypeId = Guid.NewGuid();
            var serviceTypeDescription = $"Пользователи “{configuration1CName}”";

            var serviceType = new BillingServiceType
            {
                Id = serviceTypeId,
                Name = configuration1CName,
                Description = serviceTypeDescription,
                Key = serviceTypeId,
                ServiceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId(),
                BillingType = BillingTypeEnum.ForAccountUser,
                IsDeleted = false,
                SystemServiceType = ResourceType.AccessToConfiguration1C,
                DependServiceTypeId = myDatabasesServiceDataProvider.GetServiceTypeIdOnWhichServiceDepends()
            };

            dbLayer.GetGenericRepository<BillingServiceType>().Insert(serviceType);
            dbLayer.Save();

            return serviceTypeId;
        }
    }
}
