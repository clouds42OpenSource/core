﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.RecalculateServiceCost;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Repositories.Interfaces.Common;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;

namespace Clouds42.Billing.BillingServices.Providers
{

    public class RecalculationServiceCostQuery: ISortedQuery, IPagedQuery, IHasFilter<RecalculationServiceCostFilter>
    {
        public string OrderBy { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public RecalculationServiceCostFilter Filter { get; set; }
    }

    public class RecalculationServiceCostFilter : IQueryFilter
    {
        public string? Status { get; set; }
        public Guid? ServiceId { get; set; }

        public DateTime? CreationDateTimeFrom { get; set; }
        public DateTime? CreationDateTimeTo { get; set; }
        public DateTime? RecalculationDateFrom { get; set; }
        public DateTime? RecalculationDateTo { get; set; }
    }

    public static class RecalculationServiceCostSpecification
    {
        public static Spec<RecalculationServiceCostDataDto> ByCreationDate(DateTime? from, DateTime? to)
        {
            return new Spec<RecalculationServiceCostDataDto>(x =>
                (!from.HasValue || x.CreationDate >= from.Value) && (!to.HasValue || x.CreationDate <= to.Value));
        }

        public static Spec<RecalculationServiceCostDataDto> ByRecalculationDate(DateTime? from, DateTime? to)
        {
            return new Spec<RecalculationServiceCostDataDto>(x =>
                (!from.HasValue || x.RecalculationDate >= from.Value) && (!to.HasValue || x.RecalculationDate <= to.Value));
        }
    }

    /// <summary>
    /// Провайдер для работы с данными
    /// по пересчету стоимости сервиса 
    /// </summary>
    internal class RecalculationServiceCostDataProvider(IUnitOfWork dbLayer) : IRecalculationServiceCostDataProvider
    {
        /// <summary>
        /// Получить данные
        /// по пересчету стоимости сервисов
        /// </summary>
        /// <param name="filter">Фильтр данных</param>
        /// <returns>Данные по пересчету стоимости сервисов</returns>
        public PaginationDataResultDto<RecalculationServiceCostDataDto> GetData(RecalculationServiceCostFilterDto filter)
        {
            var queryFilter = new RecalculationServiceCostQuery
            {
                Filter = new RecalculationServiceCostFilter
                {
                    CreationDateTimeFrom = filter.PeriodCreationDateFrom,
                    CreationDateTimeTo = filter.PeriodCreationDateTo,
                    RecalculationDateFrom = filter.PeriodRecalculationDateFrom,
                    RecalculationDateTo = filter.PeriodRecalculationDateTo,
                    ServiceId = filter.ServiceId,
                    Status = filter.QueueStatus?.ToString()
                },
                OrderBy = string.IsNullOrEmpty(filter.SortFieldName) && !filter.SortType.HasValue
                    ? $"{nameof(RecalculationServiceCostDataDto.CreationDate)}.desc"
                    : $"{filter.SortFieldName}.{(filter.SortType.ToString()!.ToLower())}",
                PageNumber = filter.PageNumber,
                PageSize = filter.PageSize
            };

            var data = dbLayer
                .GetGenericRepository<RecalculationServiceCostAfterChange>()
                .AsQueryableNoTracking()
                .Join(dbLayer.GetGenericRepository<ChangeServiceRequest>().AsQueryable(),
                    x => x.BillingServiceChangesId, z => z.BillingServiceChangesId,
                    (change, request) => new { change, request })
                .Join(dbLayer.BillingServiceRepository.AsQueryable(), x => x.request.BillingServiceId, z => z.Id,
                    (changeRequest, service) => new { changeRequest, service })
                .Select(x => new RecalculationServiceCostDataDto
                {
                    ServiceId = x.service.Id,
                    ServiceName = x.service.Name,
                    CreationDate = x.changeRequest.change.CoreWorkerTasksQueue.CreateDate,
                    RecalculationDate = x.changeRequest.change.CoreWorkerTasksQueue.DateTimeDelayOperation,
                    Comment = x.changeRequest.change.CoreWorkerTasksQueue.Comment,
                    Status = x.changeRequest.change.CoreWorkerTasksQueue.Status,
                    CoreWorkerTasksQueueId = x.changeRequest.change.CoreWorkerTasksQueueId,
                    OldServiceCost = x.changeRequest.change.OldServiceCost,
                    NewServiceCost = x.changeRequest.change.NewServiceCost
                })
                .AutoFilter(queryFilter.Filter)
                .AutoSort(queryFilter)
                .Where(RecalculationServiceCostSpecification.ByCreationDate(queryFilter.Filter.CreationDateTimeFrom,
                           queryFilter.Filter.CreationDateTimeTo) &
                       RecalculationServiceCostSpecification.ByRecalculationDate(
                           queryFilter.Filter.RecalculationDateFrom,
                           queryFilter.Filter.RecalculationDateTo))
                .ToPagedList(queryFilter.PageNumber ?? 1, queryFilter.PageSize ?? 50)
                .ToPagedDto();

            return new PaginationDataResultDto<RecalculationServiceCostDataDto>(data.Records, data.Records.Count, queryFilter.PageSize ?? 1, queryFilter.PageNumber ?? 50);
        }
    }
}
