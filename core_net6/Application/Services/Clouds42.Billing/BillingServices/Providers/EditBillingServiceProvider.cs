﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для редактирования сервиса
    /// </summary>
    internal class EditBillingServiceProvider(
        IEditServiceRequestProvider editServiceRequestProvider,
        BaseOperationServiceProvider baseOperationServiceProvider)
        : IEditBillingServiceProvider
    {
        /// <summary>
        /// Отредактировать сервис
        /// </summary>
        /// <param name="editBillingServiceDto">Модель изменения сервиса</param>
        public void Edit(EditBillingServiceDto editBillingServiceDto)
        {
            baseOperationServiceProvider.ValidateEditService(editBillingServiceDto);
            editServiceRequestProvider.Create(editBillingServiceDto);
        }
    }
}
