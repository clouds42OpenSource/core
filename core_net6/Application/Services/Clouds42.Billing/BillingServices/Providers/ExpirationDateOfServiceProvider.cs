﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Exceptions;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для получения даты окончания работы сервиса
    /// </summary>
    public class ExpirationDateOfServiceProvider(
        ICloudServiceAdapter cloudServiceAdapter,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider)
    {
        /// <summary>
        /// Получить дату окончания работы сервиса у аккаунта
        /// </summary>
        /// <param name="billingService"></param>
        /// <param name="accountId"></param>
        /// <returns>Дата окончания работы сервиса</returns>
        public DateTime GetExpirationDate(IBillingService billingService, Guid accountId)
        {
            var resConfig = resourceConfigurationDataProvider.GetResourceConfigurationOrThrowException(billingService, accountId);

            return resConfig.IsDemoPeriod
                ? resConfig.ExpireDate ?? throw new FailedResourceConfigurationErrorsException("У сервиса в режиме демо 'ExpireDate' не может быть null", resConfig)
                : cloudServiceAdapter.GetMainConfigurationOrThrowException(resConfig).ExpireDate ?? throw new FailedResourceConfigurationErrorsException("У основного сервиса 'ExpireDate' не может быть null", resConfig);
        }

    }
}
