﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы с данными стоимости сервиса
    /// </summary>
    internal class BillingServiceCostDataProvider(IUnitOfWork dbLayer) : IBillingServiceCostDataProvider
    {
        /// <summary>
        /// Получить минимальную стоимость сервиса
        /// (минимальная стоимость по услуге сервиса с учетом зависимых услуг)
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Минимальная стоимость сервиса</returns>
        public decimal GetMinimalCost(Guid serviceId)
        {
            var query = (
                from services in dbLayer.BillingServiceRepository.WhereLazy(s => s.Id == serviceId)
                join billingServiceType in dbLayer.BillingServiceTypeRepository.WhereLazy(st => !st.IsDeleted) on services.Id equals
                    billingServiceType.ServiceId
                join rate in dbLayer.RateRepository.WhereLazy() on billingServiceType.Id equals rate
                    .BillingServiceTypeId into rates
                from rate in rates.Take(1).DefaultIfEmpty()
                select new
                {
                    ServiceTypeId = billingServiceType.Id,
                    Cost = rate != null ? rate.Cost : 0
                }).AsEnumerable();

            return query.Select(data => new
            {
                data.ServiceTypeId,
                Cost = data.Cost + GetCostOfDependsServiceTypes(data.ServiceTypeId)
            }).OrderBy(data => data.Cost).FirstOrDefault()?.Cost ?? 0;
        }

        /// <summary>
        /// Получить стоимость зависимых услуг
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Стоимость зависимых услуг</returns>
        private decimal GetCostOfDependsServiceTypes(Guid serviceTypeId)
        {
            var childServiceTypeIds = dbLayer.BillingServiceTypeRelationRepository
                .WhereLazy(st => st.MainServiceTypeId == serviceTypeId).Select(st => st.ChildServiceTypeId);

            if (!childServiceTypeIds.Any())
                return 0;

            var allChildServiceTypeIds = GetAllChildServiceTypeIds(childServiceTypeIds).Distinct();

            return (from billingServiceTypeId in allChildServiceTypeIds
                    join rate in dbLayer.RateRepository.WhereLazy() on billingServiceTypeId equals rate
                        .BillingServiceTypeId into rates
                    from rate in rates.Take(1).DefaultIfEmpty()
                    select rate != null ? rate.Cost : 0).Sum();
        }

        /// <summary>
        /// Получить Id всех завсимых услуг
        /// </summary>
        /// <param name="serviceTypeIds"></param>
        /// <returns>Id завсимых услуг</returns>
        private IQueryable<Guid> GetAllChildServiceTypeIds(IQueryable<Guid> serviceTypeIds)
        {
            var query = from serviceTypeId in serviceTypeIds
                        join billingServiceTypeRelation in dbLayer.BillingServiceTypeRelationRepository.WhereLazy() on
                            serviceTypeId equals billingServiceTypeRelation.MainServiceTypeId
                        join childServiceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on billingServiceTypeRelation
                            .ChildServiceTypeId equals childServiceType.Id
                        select childServiceType.Id;

            if (!query.Any())
                return serviceTypeIds;

            return serviceTypeIds.Union(GetAllChildServiceTypeIds(query));
        }
    }
}
