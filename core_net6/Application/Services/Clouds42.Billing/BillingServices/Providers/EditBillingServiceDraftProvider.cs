﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для редактирования черновика сервиса
    /// </summary>
    internal class EditBillingServiceDraftProvider(IServiceProvider serviceProvider) : IEditBillingServiceDraftProvider
    {
        private readonly BaseOperationServiceProvider _baseOperationServiceProvider = serviceProvider.GetRequiredService<BaseOperationServiceProvider>();
        private readonly IUnitOfWork _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly ICreateServiceRequestProvider _createServiceRequestProvider = serviceProvider.GetRequiredService<ICreateServiceRequestProvider>();
        private readonly ICreateRateProvider _createRateProvider = serviceProvider.GetRequiredService<ICreateRateProvider>();
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider = serviceProvider.GetRequiredService<IAccountConfigurationDataProvider>();
        private readonly IHandlerException _handlerException = serviceProvider.GetRequiredService<IHandlerException>();

        /// <summary>
        /// Редактировать черновик сервиса
        /// </summary>
        /// <param name="editBillingServiceDraftDto">Модель редактирования черновика</param>
        public void Edit(EditBillingServiceDraftDto editBillingServiceDraftDto)
        {
            var billingService = GetBillingService(editBillingServiceDraftDto.Id);

            if (editBillingServiceDraftDto.BillingServiceStatus == BillingServiceStatusEnum.OnModeration)
                _baseOperationServiceProvider.ValidateEditServiceDraft(editBillingServiceDraftDto);

            using var transaction = _dbLayer.SmartTransaction.Get();
            try
            {
                UpdateBaseDataForService(billingService, editBillingServiceDraftDto);

                UpdateBillingServiceTypesData(billingService, editBillingServiceDraftDto.BillingServiceTypes);

                CreateAdditionalServiceEntities(editBillingServiceDraftDto);

                _dbLayer.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка редактирования сервиса] {editBillingServiceDraftDto.Id} {editBillingServiceDraftDto.Name}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать дополнительные сущности сервиса
        /// </summary>
        /// <param name="editBillingServiceDraftDto">Модель редактирования черновика</param>
        private void CreateAdditionalServiceEntities(EditBillingServiceDraftDto editBillingServiceDraftDto)
        {
            if (editBillingServiceDraftDto.BillingServiceStatus != BillingServiceStatusEnum.OnModeration)
                return;

            _createServiceRequestProvider.Create(editBillingServiceDraftDto.Id, editBillingServiceDraftDto.Name,
                editBillingServiceDraftDto.AccountId, Guid.NewGuid());
        }

        /// <summary>
        /// Обновить данные услуг сервиса биллинга
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <param name="billingServiceTypes">Список измененных услуг сервиса биллинга</param>
        private void UpdateBillingServiceTypesData(BillingService billingService,
            List<BillingServiceTypeDto> billingServiceTypes)
        {
            _baseOperationServiceProvider.RemoveOldServiceTypesAtService(billingService);

            _baseOperationServiceProvider.SaveBillingServiceTypesForBillingService(billingService.Id,
                billingServiceTypes);

            var accountLocaleId = _accountConfigurationDataProvider.GetAccountLocaleId(billingService.AccountOwner.Id);

            billingServiceTypes.ForEach(bs =>
                _createRateProvider.Create(bs.Id, bs.ServiceTypeCost, accountLocaleId));

            billingServiceTypes.ForEach(w =>
                _baseOperationServiceProvider.SaveBillingServiceTypeRelation(w.Id, w.ServiceTypeRelations));
        }

        /// <summary>
        /// Обновить базовые данные для сервиса биллинга
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <param name="editBillingServiceDraftDto">Модель редактирования черновика</param>
        private void UpdateBaseDataForService(BillingService billingService,
            EditBillingServiceDraftDto editBillingServiceDraftDto)
        {
            billingService.Name = !string.IsNullOrEmpty(editBillingServiceDraftDto.Name)
                ? editBillingServiceDraftDto.Name
                : billingService.Name;

            if (billingService.BillingServiceStatus != editBillingServiceDraftDto.BillingServiceStatus)
                billingService.StatusDateTime = DateTime.Now;

            billingService.BillingServiceStatus = editBillingServiceDraftDto.BillingServiceStatus;

            _dbLayer.BillingServiceRepository.Update(billingService);
            _dbLayer.Save();
        }

        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        private BillingService GetBillingService(Guid serviceId) =>
            _dbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == serviceId) ??
            throw new NotFoundException("Не возможно изменить сервис биллинга. Такой сервис не существует");
    }
}
