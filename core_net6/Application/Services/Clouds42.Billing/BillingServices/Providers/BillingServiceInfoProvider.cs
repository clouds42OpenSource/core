﻿using System.Data;
using Microsoft.EntityFrameworkCore;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Domain.IDataModels;
using Clouds42.Locales;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Guid = System.Guid;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для получения информации о сервисе
    /// </summary>
    public class BillingServiceInfoProvider(
        IUnitOfWork dbLayer,
        IServiceStatusHelper serviceStatusHelper,
        BillingServiceTypeRateHelper typeRateHelper,
        IPartialAmountForApplyDemoServiceSubsribesCalculator partialAmountForApplyDemoServiceSubscribesCalculator,
        IBillingServiceTypeRelationHelper billingServiceTypeRelationHelper,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IServiceAccountDataHelper serviceAccountDataHelper,
        IAccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        Contracts.BillingServices.Interfaces.Providers.IBillingServiceDataProvider billingServiceDataProvider,
        BillingServiceInfoDataSelector dataSelector,
        ServiceTypeResourcesDataCollector serviceTypeResourcesDataCollector,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IServiceExtensionDatabaseDataProvider serviceExtensionDatabaseDataProvider,
        ICloudLocalizer cloudLocalizer,
        IGetServiceInfoCommand getServiceInfoCommand)
        : IBillingServiceInfoProvider
    {
        /// <summary>
        /// Получить базовую информацию о сервисе
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public BillingServiceDto GetBillingService(Guid billingServiceId, Guid accountId)
        {
            serviceAccountDataHelper.CheckAbilityToManageService(accountId);

            var billingService = billingServiceDataProvider.GetBillingServiceOrThrowNewException(billingServiceId);
            var serviceName = billingService.GetNameBasedOnLocalization(cloudLocalizer, accountId);

            if (!billingService.IsActive)
                throw new InvalidExpressionException(
                   GenerateMessageStatingThatServiceIsBlockedHelper.GenerateMessage(serviceName));

            var resourceConfiguration = dbLayer.ResourceConfigurationRepository.FirstOrDefault(x =>
                                            x.AccountId == accountId && x.BillingServiceId == billingServiceId) ??
                                        throw new NotFoundException(
                                            $"У аккаунта '{accountId}' сервис '{serviceName}' не активирован.");

            var rent1CResConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId &&
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var needCreateDatabaseToActivateService =
                billingServiceDataProvider.NeedCreateDatabaseToActivateService(billingService, accountId);

            var canInstallExtension =
                billingService.CanInstallServiceExtension() && !needCreateDatabaseToActivateService;

            var dependServiceType = (!billingService.IsHybridService || rent1CResConfig != null)
                    ? billingService.BillingServiceTypes.FirstOrDefault(w => w.DependServiceTypeId != null)?.DependServiceType
                    : null;

            var dto = new BillingServiceDto
            {
                Id = billingService.Id,
                Name = serviceName,
                Opportunities = billingService.Opportunities,
                ShortDescription = billingService.ShortDescription,
                SystemService = billingService.SystemService,
                AccountOwnerId = billingService.AccountOwnerId,
                MainServiceId = dependServiceType?.ServiceId,
                IconCloudFileId = billingService.IconCloudFileId,
                DependServiceTypeId = dependServiceType?.Id,
                BillingServiceStatus = billingService.BillingServiceStatus,
                ServiceActivationDate = billingService.ServiceActivationDate,
                IsActive = billingService.IsActive,
                IsHybridService = billingService.IsHybridService,
                ServiceExpireDate = resourceConfiguration.ExpireDateValue,
                IsDemoPeriod = resourceConfiguration.IsDemoPeriod,
                ServiceDependsOnRent = dependServiceType != null,
                AccountDatabaseToRun = GetAccountDatabaseToRun(billingService, accountId),
                InstructionServiceId = billingService.InstructionServiceCloudFileId,
                MyDiskStatusModel = !billingService.IsHybridService 
                    ? serviceStatusHelper.CreateServiceStatusModel(accountId,
                    resourceConfigurationDataProvider.GetResourceConfigurationOrThrowException(accountId, Clouds42Service.MyDisk)) 
                    : new ServiceStatusModelDto(),
                CanInstallExtension = canInstallExtension,
                NeedCreateDatabaseToActivateService = needCreateDatabaseToActivateService
            };

            if (billingService.SystemService == null )
            {
                InformationForServiceDto? serviceInfoModel = getServiceInfoCommand.Execute(billingServiceId);
                if (serviceInfoModel != null)
                {
                    dto.IconUrl = serviceInfoModel.ServiceIcon;
                    dto.InstructionServiceUrl = serviceInfoModel.InstructionURL;
                    dto.InstructionServiceId = null;
                }
            }

            if (dependServiceType != null)
            {
                var mainServiceResConfig =
                    dbLayer.ResourceConfigurationRepository.FirstOrDefault(x => x.AccountId == accountId && x.BillingServiceId == dependServiceType.ServiceId) 
                        ?? throw new NotFoundException($"У аккаунта '{accountId}' для сервиса '{serviceName}' " +
                        $"не найден ресурс конфигурации сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)}.");

                InitServiceBillingData(accountId, dto, resourceConfiguration, mainServiceResConfig);
            }

            else
            {
                InitServiceBillingData(accountId, dto, resourceConfiguration);
            }

            if (billingService.IconCloudFile != null)
                dto.IconCloudFile = new CloudFileDto
                {
                    ContentType = billingService.IconCloudFile.ContentType,
                    Content = billingService.IconCloudFile.Content,
                    FileName = billingService.IconCloudFile.FileName
                };

            return dto;
        }

        /// <summary>
        /// Получить информацию о сервисе для его активации
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Информация о сервисе</returns>
        public BillingServiceActivationInfoDto GetBillingServiceForActivation(Guid billingServiceId, Guid accountId)
        {
            var locale = accountConfigurationDataProvider.GetAccountLocale(accountId);

            if (locale.Name != LocaleConst.Russia)
                throw new InvalidOperationException(
                    $"Активация сервиса '{billingServiceId}' возможна только для пользователей из РФ.");

            var billingService = billingServiceDataProvider.GetBillingServiceOrThrowNewException(billingServiceId);
            var serviceName = billingService.GetNameBasedOnLocalization(cloudLocalizer, accountId);

            var serviceResConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(x =>
                x.AccountId == accountId && x.BillingServiceId == billingServiceId);

            if (serviceResConfig != null)
                throw new ValidateException(
                    $"У аккаунта '{accountId}' сервис '{serviceName}' уже активирован.");



            var billingServiceActivationInfo = new BillingServiceActivationInfoDto
            {
                Id = billingService.Id,
                ShortDescription = billingService.ShortDescription,
                Name = serviceName,
                ServiceActivationLink = GetLinkForActivateService(billingService),
                IconCloudFileId = billingService.IconCloudFileId,
            };


            if (billingService.IconCloudFile != null)
                billingServiceActivationInfo.IconCloudFile = new CloudFileDto
                {
                    ContentType = billingService.IconCloudFile.ContentType,
                    Content = billingService.IconCloudFile.Content,
                    FileName = billingService.IconCloudFile.FileName
                };

            if (billingService.SystemService != null)
            {
                return billingServiceActivationInfo;
            }

            InformationForServiceDto? serviceInfoModel = getServiceInfoCommand.Execute(billingServiceId);
            if (serviceInfoModel != null)
            {
                billingServiceActivationInfo.IconCloudUrl = serviceInfoModel.ServiceIcon;
            }

            return billingServiceActivationInfo;
        }

        /// <summary>
        /// Получить базу на разделителях для запуска
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <returns>База на разделителях для запуска</returns>
        public AccountDatabaseOnDelimitersToRunDto GetAccountDatabaseOnDelimitersToRun(Guid accountDatabaseId)
        {
            var accountDatabase =
                dbLayer.DatabasesRepository.FirstOrDefault(w =>
                    w.Id == accountDatabaseId && w.AccountDatabaseOnDelimiter != null) ??
                throw new NotFoundException($"База на разделителях {accountDatabaseId} не найдена");

            return new AccountDatabaseOnDelimitersToRunDto
            {
                Id = accountDatabaseId,
                Caption = accountDatabase.Caption,
                DatabaseState = accountDatabase.StateEnum,
                WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(accountDatabase),
            };
        }

        /// <summary>
        /// Получить модель запуска базы
        /// </summary>
        /// <param name="billingServiceId">Id cервиса биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель запуска базы</returns>
        public async Task<AccountDatabaseToRunDto> GetAccountDatabaseToRun(Guid billingServiceId, Guid accountId)
        {
            var billingService = (await dbLayer.BillingServiceRepository.AsQueryableNoTracking()
                                     .FirstOrDefaultAsync(x => x.Id == billingServiceId)) ??
                                 throw new NotFoundException($"Сервис '{billingServiceId}' не найден!");

            return GetAccountDatabaseToRun(billingService, accountId);
        }

        /// <summary>
        /// Попробовать получить ID сервиса по ключу(Id)
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Id сервиса</returns>
        public Guid TryGetServiceIdByKey(Guid key) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Key == key || s.Id == key)?.Id ??
            throw new NotFoundException($"Не удалось найти сервис по ключу {key}");

        /// <summary>
        /// Попробовать получить ID услуги по ключу(Id)
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Id услуги</returns>
        public Guid TryGetServiceTypeIdByKey(Guid key) =>
            dbLayer.BillingServiceTypeRepository.FirstOrDefault(s => s.Key == key || s.Id == key)?.Id ??
            throw new NotFoundException($"Не удалось найти услугу по ключу {key}");

        /// <summary>
        /// Получить модель запуска базы
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель запуска базы</returns>
        private AccountDatabaseToRunDto GetAccountDatabaseToRun(
            BillingService billingService, Guid accountId)
        {
            if (billingService.InternalCloudService == InternalCloudServiceEnum.Tardis)
                return GetTardisAccountDatabaseToRun();

            var configurations1C =
                billingServiceDataProvider.GetConfigurationsCompatibleWithService(billingService.Id).ToList();

            if (!configurations1C.Any())
                return new AccountDatabaseToRunDto();

            var compatibleDatabases =
                from db in dbLayer.DatabasesRepository.WhereLazy(db => db.AccountId == accountId &&
                                                                         db.AccountDatabaseOnDelimiter != null)
                join serviceConf in configurations1C on db.AccountDatabaseOnDelimiter
                    .DbTemplateDelimiterCode equals serviceConf
                select db;

            var databasesWithStatusReadyExist =
                compatibleDatabases.Where(db => db.State == DatabaseState.Ready.ToString()).ToList();

            var basesWithInstalledExtension = serviceExtensionDatabaseDataProvider.GetDatabasesForServiceExtension(accountId, configurations1C);

            var defaultConfiguration1C = configurations1C.FirstOrDefault();
            var template = dbLayer.DbTemplateDelimitersReferencesRepository
                .FirstOrDefault(w => w.ConfigurationId == defaultConfiguration1C);

            var compatibleConfigurationsNames = dbLayer.DbTemplateDelimitersReferencesRepository
                .Where(temp => configurations1C.Contains(temp.ConfigurationId)).Select(temp => temp.Name);

            var accountDatabasesOnDelimitersToRun =
                billingService.CanInstallServiceExtension()
                    ? GetAccountDatabasesOnDelimitersToRun(basesWithInstalledExtension)
                    : GetAccountDatabasesOnDelimitersToRun(databasesWithStatusReadyExist);

            return new AccountDatabaseToRunDto
            {
                IsCompatibleDatabasesWithStatusReadyExist = databasesWithStatusReadyExist.Any(),
                IsCompatibleDatabasesWithStatusNewExist =
                    compatibleDatabases.Any(db => db.State == DatabaseState.NewItem.ToString()),
                AvailabilityOfSuitableConfigurations1C = true,
                TemplateId = template.TemplateId,
                TemplateNames = string.Join(" , ", compatibleConfigurationsNames),
                AccountDatabasesOnDelimitersToRun = accountDatabasesOnDelimitersToRun
            };
        }


        /// <summary>
        /// Получить информацию по сервисам доступным аккаунту
        /// </summary>        
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные по доступным сервисам</returns>
        public List<AccountBillingServiceDataDto> GetAccountServicesData(Guid accountId)
        {
            return dataSelector.GetAccountServicesDataQuery(accountId).ToList();
        }

        /// <summary>
        /// Получить информацию о услугах сервиса для аккаунта
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public List<BillingServiceTypeInfoDto> GetBillingServiceTypesInfo(Guid billingServiceId, Guid accountId)
        {
            var serviceTypesData =
                dataSelector.SelectBillingServiceTypeInfoTempDataModels(billingServiceId, accountId);

            var resources = dataSelector.SelectResourcesForBillingInfoByAccount(billingServiceId, accountId);
            var account = dbLayer.AccountsRepository.GetAccount(accountId);

            return serviceTypesData
                .Select(serviceTypeData => CreateBillingServiceTypeInfo(serviceTypeData, account, resources)).ToList();
        }

        /// <summary>
        /// Получить информацию о услугах сервиса для аккаунта
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public DopSessionServiceDto? GetBillingServiceInfo(Guid serviceId, Guid accountId)
        {
            var resourcesConfiguration =  dbLayer.ResourceConfigurationRepository.FirstOrDefault(x =>
                                            x.AccountId == accountId && x.BillingServiceId == serviceId) ??
                                        throw new NotFoundException(
                                            $"У аккаунта '{accountId}' сервис '{serviceId}' не активирован.");
                      

            var billingService = dbLayer.BillingServiceRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(s=>s.Id == serviceId);

            var billingServiceType = billingService!.BillingServiceTypes.FirstOrDefault(t => t.ServiceId == serviceId) ?? throw new NotFoundException($"Cервис '{serviceId}' не существует.");

            var resource = dbLayer.ResourceRepository.FirstOrDefault(r => r.AccountId == accountId && 
                                                                           r.BillingServiceTypeId == billingServiceType.Id && 
                                                                           r.Subject == accountId);

            var rateService = dbLayer.RateRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(r => r.BillingServiceTypeId == billingServiceType.Id);

            var dependServiceType = billingService.BillingServiceTypes
                .FirstOrDefault(w => w.DependServiceTypeId != null) ?? throw new NotFoundException($"Для сервиса '{serviceId}' не существует зависимого сервиса.");
            
            var dependResource = dbLayer.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(x => x.AccountId == accountId && 
                                     x.BillingServiceId == dependServiceType.DependServiceType.ServiceId);

            var billingAccount = dbLayer.BillingAccountRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(b => b.Id == accountId);

            var promisePaymentDate = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");

            var dto = new DopSessionServiceDto
            {
                Id = billingService.Id,
                Name = billingService.Name,
                ServiceExpireDate = billingAccount!.PromisedPaymentIsActive() && billingAccount.PromisePaymentDate.HasValue ? 
                billingAccount.PromisePaymentDate.Value.AddDays(promisePaymentDate)
                : dependResource!.ExpireDateValue,
                DependServiceId = dependServiceType.DependServiceType.ServiceId,
                ServiceDependsOnRent = dependServiceType.DependServiceType != null,
                IsActivatedPromisPayment = billingAccount.PromisedPaymentIsActive(),
                Amount = resourcesConfiguration.Cost,
                UsedLicenses = resourcesConfiguration.Cost == 0 ? 0 : ((int)(resource != null ? resource.Cost / rateService!.Cost : resourcesConfiguration.Cost / rateService!.Cost)),
                Currency = rateService!.Locale.Currency
            };

            return dto;
        }

        /// <summary>
        /// Получить стоимость услуги
        /// влючая стоимость услуг которые входят в нее
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Стоимость услуги</returns>
        public decimal GetServiceTypeAndServiceTypeIncludesCost(Guid serviceTypeId, IAccount account)
        {
            var serviceType = dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(w => w.Id == serviceTypeId) ?? throw new NotFoundException($"Услуга с Id = {serviceTypeId} не сущетсвует");

            var serviceTypeCost = typeRateHelper.GetRateForServiceType(serviceType.Id, account.Id);

            var dependencyRelations = billingServiceTypeRelationHelper
                .GetChildDependencyRelations(serviceType, account.Id)
                .Select(x => x.Id)
                .ToList();

            dependencyRelations.ForEach(w =>
                serviceTypeCost += typeRateHelper.GetRateForServiceType(w, account.Id));

            return serviceTypeCost;
        }

        /// <summary>
        /// Получить типы подключения к информационной базе 1С
        /// </summary>
        /// <returns>Подключения к инф. базе</returns>
        public TypesOfConnectionToInformationDbDto GetTypesOfConnectionToInformationDb()
        {
            var myEntUserServiceTypeId = dbLayer.BillingServiceTypeRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefault(w => w.SystemServiceType == ResourceType.MyEntUser)!.Id;

            var myEntUserWebServiceTypeId = dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(w => w.SystemServiceType == ResourceType.MyEntUserWeb)!.Id;

            return new TypesOfConnectionToInformationDbDto
            {
                MyEntUserServiceTypeId = myEntUserServiceTypeId,
                MyEntUserWebServiceTypeId = myEntUserWebServiceTypeId
            };
        }

        /// <summary>
        ///     Получить все конфигураций 1С на разделителях
        /// </summary>
        /// <returns>Конфигурации 1С</returns>
        public List<Configuration1COnDelimitersDto> GetAllConfigurations1COnDelimiters()
        {
            var listConfigurations1C = dbLayer.DbTemplateDelimitersReferencesRepository
                .AsQueryableNoTracking()
                .Select(w => new Configuration1COnDelimitersDto { Name = w.Name, ConfigurationId = w.ConfigurationId }).ToList();

            return listConfigurations1C;
        }

        /// <summary>
        ///     Получить все отрасли
        /// </summary>
        /// <returns>Отрасли</returns>
        public List<IndustryDto> GetAllIndustries()
        {
            var listIndustries = dbLayer.IndustryRepository
                .AsQueryableNoTracking()
                .Select(w => new IndustryDto { Id = w.Id, Name = w.Name }).ToList();

            return listIndustries;
        }

        /// <summary>
        /// Проверить уникальность названия нового сервиса
        /// </summary>
        /// <param name="serviceName">Название сервиса</param>
        /// <returns>Результат проверки</returns>
        public bool CheckUniquenessOfNameOfNewService(string serviceName)
        {
            return !dbLayer.BillingServiceRepository
                .AsQueryableNoTracking()
                .Any(w => w.Name == serviceName);
        }

        /// <summary>
        /// Проверить уникальность названия сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="serviceName">Название сервиса</param>
        /// <returns>Результат проверки</returns>
        public bool CheckUniquenessOfServiceName(Guid? serviceId, string serviceName)
        {
           return !dbLayer.BillingServiceRepository
               .AsQueryableNoTracking()
               .Any(w => w.Name == serviceName && w.Id != serviceId);
        }

        /// <summary>
        /// Получить текстовое описание назначения платежа по сервису.
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>        
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>Результат проверки</returns>
        public string GetPaymentPurposeText(Guid serviceId, Guid accountId)
        {
            var serviceInfo = GetBillingService(serviceId, accountId);

            return
                $"Оплата за использование сервиса " +
                $"\"{ServiceNameLocalizer.LocalizeForAccount(cloudLocalizer, accountId, serviceInfo.Name, serviceInfo.SystemService)}\" " +
                $"до {serviceInfo.MainServiceData.GetExpireDateValue():D} ";
        }

        /// <summary>
        /// Проинициализировать в моделе биллинговую часть сервиса.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="resourceConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="mainResourceConfiguration">Ресурс конфигурация основного сервиса.</param>
        /// <param name="dto">инициализируемая модель с данными.</param>
        private void InitServiceBillingData(
            Guid accountId,
            BillingServiceDto dto,
            ResourcesConfiguration resourceConfiguration,
            ResourcesConfiguration mainResourceConfiguration = null)
        {
            mainResourceConfiguration ??= resourceConfiguration;

            dto.MainServiceData = mainResourceConfiguration;

            if (resourceConfiguration.IsDemoPeriod)
            {
                dto.ServiceExpireDate = resourceConfiguration.ExpireDateValue;
                dto.ServiceStatus = serviceStatusHelper.CreateServiceStatusModel(accountId, resourceConfiguration);
            }
            else
            {
                dto.ServiceExpireDate = mainResourceConfiguration.ExpireDateValue;
                dto.ServiceStatus = serviceStatusHelper.CreateServiceStatusModel(accountId, mainResourceConfiguration);
            }

            dto.AmountData = new BillingServiceAmountDataModelDto
            {
                ServiceAmount = resourceConfiguration.Cost,
                Currency = accountConfigurationDataProvider.GetAccountLocale(accountId),
                ServicePartialAmount =
                    partialAmountForApplyDemoServiceSubscribesCalculator.Calculate(resourceConfiguration,
                        mainResourceConfiguration),
                AmountForOneDay = partialAmountForApplyDemoServiceSubscribesCalculator.CalculateForDays(resourceConfiguration,
                        mainResourceConfiguration, 1) ?? 0
            };
        }

        /// <summary>
        /// Получить ссылку на активацию сервиса
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <returns>Ссылка на активацию сервиса</returns>
        private static string GetLinkForActivateService(BillingService billingService)
        {
            var cpSite = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();

            var uri = new Uri(
                $"{cpSite}/signup?ReferralAccountId={billingService.AccountOwnerId}&CloudServiceId={billingService.Id}");

            return uri.AbsoluteUri;
        }

        /// <summary>
        /// Получить инф. базу Тардис для запуска
        /// </summary>
        /// <returns>Модель запуска базы</returns>
        private AccountDatabaseToRunDto GetTardisAccountDatabaseToRun()
        {
            var tardisAccountDatabaseId = CloudConfigurationProvider.Tardis.GetAccountDatabaseId();

            var tardisAccountDatabase = dbLayer.DatabasesRepository.GetAccountDatabase(tardisAccountDatabaseId)
                                        ?? throw new NotFoundException("Инф. база Тардис по ID не найдена.");

            var accountDatabasesOnDelimitersToRun = new List<AccountDatabaseOnDelimitersToRunDto>();
            if (tardisAccountDatabase.PublishStateEnum == PublishState.Published)
            {
                accountDatabasesOnDelimitersToRun.Add(new AccountDatabaseOnDelimitersToRunDto
                {
                    Id = tardisAccountDatabase.Id,
                    DatabaseState = tardisAccountDatabase.StateEnum,
                    Caption = tardisAccountDatabase.Caption,
                    WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(tardisAccountDatabase),
                });
            }

            return new AccountDatabaseToRunDto
            {
                IsCompatibleDatabasesWithStatusReadyExist = true,
                IsCompatibleDatabasesWithStatusNewExist = false,
                AvailabilityOfSuitableConfigurations1C = true,
                TemplateId = tardisAccountDatabase.DbTemplate.Id,
                TemplateNames = tardisAccountDatabase.DbTemplate.DefaultCaption,
                AccountDatabasesOnDelimitersToRun = accountDatabasesOnDelimitersToRun
            };
        }

        /// <summary>
        /// Получить список баз для запуска
        /// </summary>
        /// <param name="accountDatabases">Список баз</param>
        /// <returns>Список баз для запуска</returns>
        private List<AccountDatabaseOnDelimitersToRunDto> GetAccountDatabasesOnDelimitersToRun(
            IEnumerable<Domain.DataModels.AccountDatabase> accountDatabases)
            => accountDatabases.Select(w =>
                new AccountDatabaseOnDelimitersToRunDto
                {
                    Id = w.Id,
                    DatabaseState = w.StateEnum,
                    Caption = w.Caption,
                    WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(w),
                }).ToList();

        /// <summary>
        /// Создать модель информации об услуге сервиса биллинга
        /// </summary>
        /// <param name="serviceTypeTempData">Временные данные биллинга по услуге сервиса</param>
        /// <param name="account">Аккаунт</param>
        /// <param name="resourcesForBillingInfo">Ресурсы для информации биллинга сервиса</param>
        /// <returns>Модель информации об услуге сервиса биллинга</returns>
        private BillingServiceTypeInfoDto CreateBillingServiceTypeInfo(
            BillingServiceTypeInfoTempDataModel serviceTypeTempData, Account account,
            List<Resource> resourcesForBillingInfo)
        {
            var rateService = dbLayer.RateRepository.FirstOrDefault(r => r.BillingServiceTypeId == serviceTypeTempData.ServiceType.Id);
            var serviceTypeResourcesData = serviceTypeResourcesDataCollector.Collect(serviceTypeTempData.ServiceType,
                account.Id,
                resourcesForBillingInfo, rateService);
            var currency = account.AccountConfiguration.Locale.Currency;

            return new BillingServiceTypeInfoDto
            {
                Id = serviceTypeTempData.ServiceType.Id,
                Name = serviceTypeTempData.ServiceType.Name,
                BillingType = serviceTypeTempData.ServiceType.BillingType,
                Amount = serviceTypeResourcesData.ServiceTypeAmount,
                Currency = currency,
                Cost = serviceTypeTempData.ServiceTypeCost,
                CostPerOneLicense =
                    GetServiceTypeAndServiceTypeIncludesCost(serviceTypeTempData.ServiceType.Id, account),
                UsedLicenses = serviceTypeResourcesData.UsedLicensesCount,
                IamSponsorLicenses = serviceTypeResourcesData.IamSponsorLicensesCount,
                MeSponsorLicenses = serviceTypeResourcesData.MeSponsorLicensesCount,
                ChildServiceTypes = serviceTypeTempData.ChildServiceTypeIds.ToList(),
                ParentServiceTypes = serviceTypeTempData.ParentServiceTypeIds.ToList(),
                SystemServiceType = serviceTypeTempData.ServiceType.SystemServiceType,
                ServiceId = serviceTypeTempData.ServiceType.ServiceId,
                ServiceName = serviceTypeTempData.ServiceType.Service?.Name,
                Clouds42Service = serviceTypeTempData.ServiceType.Service?.SystemService
            };
        }

        /// <summary>
        /// Получить список опций услуги
        /// </summary>
        /// <param name="serviceId">Для какой услуги получить список опций</param>
        /// <returns>Список опций услуги</returns>
        public List<ServiceOptionDto> GetServiceOptions(Guid serviceId)
        {
            return dbLayer.BillingServiceTypeRepository
                .Where((item) => item.ServiceId == serviceId)
                .Select(item => new ServiceOptionDto
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    BillingType = item.BillingType,
                    SystemServiceType = item.SystemServiceType,
                    DependServiceTypeId = item.DependServiceTypeId
                }).ToList();
        }
    }
}
