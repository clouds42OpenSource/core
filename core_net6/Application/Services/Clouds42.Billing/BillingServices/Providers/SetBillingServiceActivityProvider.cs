﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для установки активности сервиса биллинга
    /// </summary>
    internal class SetBillingServiceActivityProvider(
        IUnitOfWork dbLayer,
        ISetBillingServiceActivityDataProvider setBillingServiceActivityDataProvider,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IDisableServiceInMarketCommand disableServiceInMarketCommand,
        ISender sender)
        : ISetBillingServiceActivityProvider
    {
        /// <summary>
        /// Установить активность сервиса
        /// </summary>
        /// <param name="model">Модель для установки активности сервиса биллинга</param>
        public void SetActivity(SetBillingServiceActivityDto model)
        {
            var service = setBillingServiceActivityDataProvider.GetService(model.ServiceId);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                service.IsActive = model.IsActive;
                dbLayer.BillingServiceRepository.Update(service);
                dbLayer.Save();

                if (!model.IsActive)
                    DisableServiceForUsers(model.ServiceId, service.SystemService);

                DisableServiceInMarket(model.ServiceId, model.IsActive);

                LogEventHelper.LogEvent(dbLayer, service.AccountOwnerId ?? accessProvider.ContextAccountId,
                    accessProvider, LogActions.EditBillingService,
                    GetLogDescription(service.Name, model.IsActive));

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка установки активности сервиса] '{model.ServiceId}'");

                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Отключения сервиса
        /// </summary>
        /// <param name="model">Модель для отключения сервиса биллинга</param>
        public void ShutdownService(SetBillingServiceActivityDto model)
        {
            var service = setBillingServiceActivityDataProvider.GetServiceNoThrowException(model.ServiceId);

            if (service == null)
                return;

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                service.IsActive = model.IsActive;
                dbLayer.BillingServiceRepository.Update(service);
                dbLayer.Save();

                DisableServiceForUsers(model.ServiceId, service.SystemService);

                LogEventHelper.LogEvent(dbLayer, service.AccountOwnerId ?? accessProvider.ContextAccountId,
                    accessProvider, LogActions.EditBillingService,
                    $"Сервис {service.Name} отключен");

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка отключения сервиса] '{model.ServiceId}'");

                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Отключить сервис у пользователей
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="service"></param>
        private void DisableServiceForUsers(Guid serviceId, Clouds42Service? service)
        {
            var resources = setBillingServiceActivityDataProvider.GetServiceResources(serviceId).ToList();
            DisableServiceResources(resources);

            var accountsThatUseService = resources.Select(r => r.AccountId).Distinct().ToList();

            sender.Send(new RecalculateResourcesConfigurationCostCommand(accountsThatUseService, [serviceId], service)).Wait();
        }

        /// <summary>
        /// Отключить сервис в маркете
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        private void DisableServiceInMarket(Guid serviceId, bool isActive)
        {
            disableServiceInMarketCommand.Execute(serviceId, isActive);
        }

        /// <summary>
        /// Отключить ресурсы сервиса у пользователей
        /// </summary>
        /// <param name="resources">Список ресурсов</param>
        private void DisableServiceResources(List<Resource> resources)
        {
            resources.ForEach(resource =>
            {
                resource.Subject = null;
            });

            dbLayer.BulkUpdate(resources);

            dbLayer.Save();
        }

        /// <summary>
        /// Получить описание лога
        /// </summary>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="isActive">Сервис активен</param>
        /// <returns>Описание лога</returns>
        private string GetLogDescription(string serviceName, bool isActive)
        {
            var operation = isActive ? "включен" : "отключен";
            return $"Сервис \"{serviceName}\" {operation}.";
        }
    }
}
