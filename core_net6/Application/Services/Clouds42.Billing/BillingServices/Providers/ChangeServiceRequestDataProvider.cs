﻿using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;

namespace Clouds42.Billing.BillingServices.Providers
{
    public static class ChangeServiceRequestDataSpecification
    {
        public static Spec<ChangeServiceRequestDataDto> ByDate(DateTime? from, DateTime? to)
        {
            return new Spec<ChangeServiceRequestDataDto>(x =>
                (!from.HasValue || x.CreationDateTime >= from) && (!to.HasValue || x.CreationDateTime <= to));
        }
    }
    public class ChangeServiceRequestDataQuery: IPagedQuery, ISortedQuery
    {
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public string OrderBy { get; set; }
        public ChangeServiceRequestDataFilter? Filter { get; set; }
    }

    public class ChangeServiceRequestDataFilter : IQueryFilter
    {
        public Guid? BillingServiceId { get; set; }
        public ChangeRequestTypeEnum? ChangeRequestType { get; set; }
        public ChangeRequestStatusEnum? Status { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }

    /// <summary>
    /// Провайдер для работы с данными заявок
    /// сервиса на модерацию
    /// </summary>
    internal class ChangeServiceRequestDataProvider(IUnitOfWork dbLayer) : IChangeServiceRequestDataProvider
    {
        /// <summary>
        /// Получить порцию данных
        /// заявок сервиса на модерацию
        /// </summary>
        /// <param name="filter">Модель фильтра заявок сервиса на модерацию</param>
        /// <returns>Порция данных
        /// заявок сервиса на модерацию</returns>
        public PaginationDataResultDto<ChangeServiceRequestDataDto> GetChangeServiceRequests(
            ChangeServiceRequestsFilterDto filter)
        {
            var queryFilter = new ChangeServiceRequestDataQuery
            {
                OrderBy = $"{nameof(ChangeServiceRequestDataDto.CreationDateTime)}.desc",
                PageNumber = filter.PageNumber,
                PageSize = filter.PageSize,
                Filter = new ChangeServiceRequestDataFilter
                {
                    BillingServiceId = filter.ServiceId,
                    ChangeRequestType = filter.ChangeRequestType,
                    Status = filter.Status,
                    From = filter.PeriodFrom,
                    To = filter.PeriodTo
                }
            };

            var result = dbLayer.GetGenericRepository<ChangeServiceRequest>()
                .AsQueryableNoTracking()
                .Select(x => new ChangeServiceRequestDataDto
                {
                    Id = x.Id,
                    CreationDateTime = x.CreationDateTime,
                    BillingServiceId = x.BillingServiceId,
                    Status = x.Status,
                    StatusDateTime = x.StatusDateTime,
                    ChangeRequestType = x.ChangeRequestType,
                    ModeratorComment = x.ModeratorComment,
                    BillingServiceName = x.BillingService.Name
                })
                .Where(ChangeServiceRequestDataSpecification.ByDate(queryFilter.Filter.From, queryFilter.Filter.To))
                .AutoFilter(queryFilter.Filter)
                .AutoSort(queryFilter)
                .ToPagedList(queryFilter.PageNumber ?? 1, queryFilter.PageSize ?? 50)
                .ToPagedDto();

            return new PaginationDataResultDto<ChangeServiceRequestDataDto>(result.Records, result.Records.Count, filter.PageSize, filter.PageNumber);
        }

        /// <summary>
        /// Получить модель изменений по сервису биллинга
        /// </summary>
        /// <param name="editServiceRequestId">Id заявки редактирования сервиса на модерацию</param>
        /// <returns>Модель изменений по сервису биллинга</returns>
        public BillingServiceChangesDto GetBillingServiceChangesDto(Guid editServiceRequestId)
        {
            var billingServiceChanges = GetBillingServiceChanges(editServiceRequestId);
            var billingServiceChangesDto = new BillingServiceChangesDto
            {
                Name = billingServiceChanges.Name,
                Opportunities = billingServiceChanges.Opportunities,
                ShortDescription = billingServiceChanges.ShortDescription,
                BillingServiceTypes = GetBillingServiceTypeChanges(billingServiceChanges),
                IsHybridService = billingServiceChanges.IsHybridService,
            };

            return billingServiceChangesDto;
        }


        /// <summary>
        /// Получить измененные услуги сервиса
        /// </summary>
        /// <param name="billingServiceChanges">Изменения по сервису</param>
        /// <returns>Измененные услуги сервиса</returns>
        private static List<BillingServiceTypeChangeDto>
            GetBillingServiceTypeChanges(BillingServiceChanges billingServiceChanges) => billingServiceChanges
            .BillingServiceTypeChanges.Select(st => new BillingServiceTypeChangeDto
            {
                Id = st.Id,
                IsNew = st.IsNew,
                ServiceTypeId = st.BillingServiceTypeId,
                Name = st.Name,
                Description = st.Description,
                ServiceTypeCost = st.Cost,
                BillingType = st.BillingType,
                ConnectionToDatabaseType = ServiceTypeHelperBillingServiceData.MapToConnectionToDatabaseType(st.DependServiceType?.SystemServiceType),
                DependServiceTypeId = st.DependServiceTypeId
            }).ToList();

        /// <summary>
        /// Получить изменения по сервису биллинга
        /// </summary>
        /// <param name="editServiceRequestId">Id заявки редактирования сервиса на модерацию</param>
        /// <returns>Изменения по сервису биллинга</returns>
        public BillingServiceChanges GetBillingServiceChanges(Guid editServiceRequestId) =>
            dbLayer.GetGenericRepository<EditServiceRequest>().FirstOrDefault(esr => esr.Id == editServiceRequestId)
                ?.BillingServiceChanges ??
            throw new NotFoundException($"Не удалось найти изменения сервиса для заявки {editServiceRequestId}");

        /// <summary>
        /// Получить заявку на модерацию сервиса
        /// </summary>
        /// <param name="changeServiceRequestId">Id заявки</param>
        /// <returns>Заявка на модерацию сервиса</returns>
        public ChangeServiceRequest GetChangeServiceRequest(Guid changeServiceRequestId) =>
            dbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.Id == changeServiceRequestId) ??
            throw new NotFoundException($"Не удалось получить заявку по Id {changeServiceRequestId}");
        
    }
}
