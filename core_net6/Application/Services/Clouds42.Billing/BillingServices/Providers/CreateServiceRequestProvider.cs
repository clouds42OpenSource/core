﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы с заявками
    /// сервиса на модерацию при создании
    /// </summary>
    internal class CreateServiceRequestProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ILogger42 logger)
        : ICreateServiceRequestProvider
    {
        /// <summary>
        /// Создать заявку сервиса на модерацию при создании
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="сreateServiceRequestId">Id заявки на модерацию</param>
        public void Create(Guid serviceId, string serviceName, Guid? accountId, Guid? сreateServiceRequestId)
        {
            var createServiceRequest = new CreateServiceRequest
            {
                Id = сreateServiceRequestId ?? Guid.NewGuid(),
                BillingServiceId = serviceId,
                Status = ChangeRequestStatusEnum.OnModeration,
                StatusDateTime = DateTime.Now,
                ChangeRequestType = ChangeRequestTypeEnum.Creation
            };

            dbLayer.GetGenericRepository<CreateServiceRequest>().Insert(createServiceRequest);

            var message = $"Создана заявка на модерацию сервиса \"{serviceName}\" " +
                          $"с типом \"{createServiceRequest.ChangeRequestType.Description()}\"";

            if (accountId != null)
                LogEventHelper.LogEvent(dbLayer, (Guid)accountId, accessProvider, LogActions.EditBillingService, message);

            dbLayer.Save();

            logger.Info(message);
        }
    }
}
