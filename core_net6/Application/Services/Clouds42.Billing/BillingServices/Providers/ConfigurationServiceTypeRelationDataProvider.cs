﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы с данными связи конфигурации 1С
    /// с услугой сервиса(Мои информационные базы)
    /// </summary>
    internal class ConfigurationServiceTypeRelationDataProvider(IUnitOfWork dbLayer)
        : IConfigurationServiceTypeRelationDataProvider
    {
        /// <summary>
        /// Получить услугу по названию конфигурации
        /// или выкинуть исключение
        /// </summary>
        /// <param name="configurationName">Название конфигурации</param>
        /// <returns>Услуга сервиса(Мои информационные базы)</returns>
        public BillingServiceType GetServiceTypeOrThrowException(string configurationName) =>
            dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>()
                .FirstOrDefault(relation => relation.Configuration1CName == configurationName)?.ServiceType ??
            throw new NotFoundException($"Не удалось определить услугу по названию конфигурации '{configurationName}'");

        /// <summary>
        /// Получить Id услуги по названию конфигурации
        /// или выкинуть исключение
        /// </summary>
        /// <param name="configurationName">Название конфигурации</param>
        /// <returns>Id услуги сервиса(Мои информационные базы)</returns>
        public Guid GetServiceTypeIdOrThrowException(string configurationName) =>
            GetServiceTypeOrThrowException(configurationName).Id;
    }
}
