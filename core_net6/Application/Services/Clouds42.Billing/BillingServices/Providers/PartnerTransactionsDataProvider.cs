﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{

    public class PartnerTransactionQuery : IPagedQuery, IHasFilter<PartnerTransactionFilter>, ISortedQuery
    {
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public PartnerTransactionFilter Filter { get; set; }
        public string OrderBy { get; set; }

    }

    public class PartnerTransactionFilter : IQueryFilter
    {
        public string? AccountCaption { get; set; }
        public string? PartnerAccountCaption { get; set; }
        public Guid? PartnerAccountId { get; set; }
        public string? Operation { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }

    public static class PartnerTransactionSpecification
    {
        public static Spec<PartnerTransactionDto> ByDate(DateTime? from, DateTime? to)
        {
            return new Spec<PartnerTransactionDto>(x =>
                (!from.HasValue || x.TransactionDate >= from.Value) && (!to.HasValue || x.TransactionDate <= to.Value));
        }
    }

    /// <summary>
    /// Провайдер данных транзакций партнера
    /// </summary>
    internal class PartnerTransactionsDataProvider(IUnitOfWork dbLayer) : IPartnerTransactionsDataProvider
    {
        /// <summary>
        /// Получить порцию данных транзакций партнера
        /// </summary>
        /// <param name="filter">Данные фильтра.</param>
        /// <returns>Порция данных транзакций</returns>
        public PaginationDataResultDto<PartnerTransactionDto> GetPartnerTransactions(PartnerTransactionsFilterDto filter)
        {
            var queryFilter = new PartnerTransactionQuery
            {
                Filter = new PartnerTransactionFilter
                {
                    AccountCaption = filter.AccountData,
                    From = filter.PeriodFrom,
                    To = filter.PeriodTo,
                    Operation = filter.OperationData,
                    PartnerAccountCaption = filter.PartnerAccountData,
                    PartnerAccountId = filter.AccountId
                },
                OrderBy = string.IsNullOrEmpty(filter.SortFieldName) && !filter.SortType.HasValue
                    ? $"{nameof(PartnerTransactionDto.TransactionDate)}.desc"
                    : $"{filter.SortFieldName}.{(filter.SortType == SortType.Asc ? "asc" : "desc")}",
                PageNumber = filter.PageNumber,
                PageSize = filter.PageSize,
            };

            var query = (from ap in dbLayer.AgentPaymentRepository.AsQueryable()
                        join a in dbLayer.AccountsRepository.AsQueryable() on ap.AccountOwnerId equals a.Id
                        join accConf in dbLayer.AccountConfigurationRepository.AsQueryable() on a.Id equals accConf.AccountId
                        join p in dbLayer.PaymentRepository.AsQueryable() on ap.ClientPaymentId equals p.Id into paymentsJoin
                        from p in paymentsJoin.DefaultIfEmpty()
                        join serv in dbLayer.BillingServiceRepository.AsQueryable() on p.BillingServiceId equals serv.Id into servicesJoin
                        from serv in servicesJoin.DefaultIfEmpty()
                        join apr in dbLayer.GetGenericRepository<AgentPaymentRelation>().AsQueryable() on ap.AgentPaymentRelationId equals apr.Id into aprJoin
                        from apr in aprJoin.DefaultIfEmpty()
                        join ac in dbLayer.AccountsRepository.AsQueryable() on p.AccountId equals ac.Id into acJoin
                        from ac in acJoin.DefaultIfEmpty()
                        join accConfAc in dbLayer.AccountConfigurationRepository.AsQueryable() on ac.Id equals accConfAc.AccountId into accConfAcJoin
                        from accConfAc in accConfAcJoin.DefaultIfEmpty()
                        join acr in dbLayer.AccountsRepository.AsQueryable() on apr.AccountId equals acr.Id into acrJoin
                        from acr in acrJoin.DefaultIfEmpty()
                        join accConfAcr in dbLayer.AccountConfigurationRepository.AsQueryable() on acr.Id equals accConfAcr.AccountId into accConfAcrJoin
                        from accConfAcr in accConfAcrJoin.DefaultIfEmpty()
                        where serv == null || serv.SystemService == null || serv.SystemService != Clouds42Service.MyDatabases
                        select new PartnerTransactionDto
                        {
                            TransactionDate = (p.Date == null ? ap.PaymentDateTime : p.Date),
                            AccountCaption = (ac.AccountCaption ?? acr.AccountCaption ?? a.AccountCaption),
                            AccountIndexNumber = ac.IndexNumber == null ? acr.IndexNumber == null ? a.IndexNumber : acr.IndexNumber : ac.IndexNumber,
                            IsVipAccount = (ac.Id != null ? (accConfAc.IsVip == null ? false : accConfAc.IsVip) : (acr.Id != null ? (accConfAcr.IsVip == null ? false : accConfAcr.IsVip) : (accConf.IsVip == null ? false : accConf.IsVip))),
                            AccountId = (ac.Id == null ? acr.Id == null ? a.Id : acr.Id : ac.Id),
                            Operation = (p.Description ?? ap.Comment),
                            TransactionSum = ap.Sum,
                            ClientPaymentSum = (p.Sum == null ? apr.Sum == null ? 0 : apr.Sum : p.Sum),
                            PartnerAccountId = ap.AccountOwnerId,
                            PartnerAccountCaption = a.AccountCaption,
                            PartnerAccountIndexNumber = a.IndexNumber,
                            AgentPaymentSourceType = ap.AgentPaymentSourceType,
                            PaymentType = ap.PaymentType,
                            TransactionType = (p.TransactionType == null ? TransactionType.Money : p.TransactionType)
                        })
                .AutoFilter(queryFilter.Filter)
                .AutoSort(queryFilter)
                .Where(PartnerTransactionSpecification.ByDate(queryFilter.Filter.From, queryFilter.Filter.To))
                .ToPagedList(queryFilter.PageNumber ?? 1, queryFilter.PageSize ?? 50)
                .ToPagedDto();

            return new PaginationDataResultDto<PartnerTransactionDto>(query.Records, query.Records.TotalItemCount, queryFilter.PageSize ?? 1, queryFilter.PageNumber ?? 50);
        }
    }
}
