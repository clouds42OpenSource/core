﻿using Clouds42.Billing.BillingServices.Validators;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер базовых операций над сервисом
    /// </summary>
    public class BaseOperationServiceProvider(
        IUnitOfWork dbLayer,
        BillingServiceValidator billingServiceValidator,
        ICloudFileProvider cloudFileProvider,
        IRemoveRateProvider removeRateProvider,
        ILogger42 logger)
    {
        protected readonly BillingServiceValidator BillingServiceValidator = billingServiceValidator;
        protected readonly IUnitOfWork DbLayer = dbLayer;
        protected readonly ILogger42 Logger = logger;
        protected readonly ICloudFileProvider CloudFileProvider = cloudFileProvider;


        /// <summary>
        /// Провалидировать карточку сервиса для редактирования
        /// </summary>
        /// <param name="editBillingServiceDto">Модель карточки сервиса</param>
        public void ValidateEditService(EditBillingServiceDto editBillingServiceDto)
        {
            var validateResult = BillingServiceValidator.ValidateEditService(editBillingServiceDto);

            if (!validateResult.Success)
                throw new ValidateException(validateResult.Message);
        }

        /// <summary>
        /// Провалидировать карточку сервиса "редактирование черновика"
        /// </summary>
        /// <param name="editBillingServiceDraftDto">Модель карточки сервиса черновика</param>
        public void ValidateEditServiceDraft(EditBillingServiceDraftDto editBillingServiceDraftDto)
        {
            var validateResult = BillingServiceValidator.ValidateEditServiceDraft(editBillingServiceDraftDto);

            if (!validateResult.Success)
                throw new ValidateException(validateResult.Message);
        }


        /// <summary>
        /// Сохранить услуги для сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="billingServiceTypesDto">Услуги сервиса</param>
        public void SaveBillingServiceTypesForBillingService(Guid serviceId,
            List<BillingServiceTypeDto> billingServiceTypesDto)
        {
            foreach (var billingServiceTypeDto in billingServiceTypesDto)
            {
                var billingServiceType = new BillingServiceType
                {
                    Id = billingServiceTypeDto.Id,
                    Key = billingServiceTypeDto.Key,
                    Name = billingServiceTypeDto.Name,
                    Description = billingServiceTypeDto.Description,
                    ServiceId = serviceId,
                    BillingType = billingServiceTypeDto.BillingType,
                    DependServiceTypeId = billingServiceTypeDto.DependServiceTypeId
                };

                DbLayer.BillingServiceTypeRepository.Insert(billingServiceType);
            }

            DbLayer.Save();
        }

        /// <summary>
        /// Сохранить связи услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="serviceTypeRelations">Связи услуги</param>
        public void SaveBillingServiceTypeRelation(Guid serviceTypeId, List<Guid> serviceTypeRelations)
        {
            serviceTypeRelations.ForEach(w =>
            {
                var billingServiceTypeRelation = new BillingServiceTypeRelation
                {
                    MainServiceTypeId = serviceTypeId,
                    ChildServiceTypeId = w
                };

                DbLayer.BillingServiceTypeRelationRepository.Insert(billingServiceTypeRelation);
            });

            DbLayer.Save();
        }

        /// <summary>
        /// Удалить старые услуги у сервиса
        /// </summary>
        /// <param name="billingService">Cервис</param>
        public void RemoveOldServiceTypesAtService(BillingService billingService)
        {
            var serviceTypes = billingService.BillingServiceTypes.ToList();

            foreach (var serviceType in serviceTypes)
            {
                removeRateProvider.Remove(serviceType.Id);

                var childRelations = serviceType.ChildRelations.ToList();
                var parentRelations = serviceType.ParentRelations.ToList();

                DbLayer.BillingServiceTypeRelationRepository.DeleteRange(childRelations);
                DbLayer.BillingServiceTypeRelationRepository.DeleteRange(parentRelations);
                DbLayer.Save();
            }

            DbLayer.BillingServiceTypeRepository.DeleteRange(serviceTypes);
        }

        /// <summary>
        /// Проверить существование старой заявки на изменение сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Заявка на изменение сервиса</returns>
        public bool IsExistsOldChangeServiceRequest(Guid serviceId) =>
            DbLayer.GetGenericRepository<ChangeServiceRequest>()
                .FirstOrDefault(sr =>
                    sr.BillingServiceId == serviceId && sr.Status != ChangeRequestStatusEnum.OnModeration) != null;

        
        
        
    }
}
