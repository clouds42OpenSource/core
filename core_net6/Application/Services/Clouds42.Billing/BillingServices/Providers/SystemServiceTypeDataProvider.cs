﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы с данными системной услуги сервиса биллинга
    /// </summary>
    internal class SystemServiceTypeDataProvider(IUnitOfWork dbLayer) : ISystemServiceTypeDataProvider
    {
        /// <summary>
        /// Получить услугу по типу системной услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Системная услуга биллинга</returns>
        public BillingServiceType GetServiceTypeBySystemServiceType(ResourceType systemServiceType) =>
            dbLayer.BillingServiceTypeRepository.FirstOrDefault(st => st.SystemServiceType == systemServiceType)
            ?? throw new NotFoundException(
                $"Не заполнен справочник billing.ServiceTypes для системной услуги {systemServiceType}");

        /// <summary>
        /// Получить Id услуги по типу системной услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Id системной услуги биллинга</returns>
        public Guid GetServiceTypeIdBySystemServiceType(ResourceType systemServiceType) =>
            GetServiceTypeBySystemServiceType(systemServiceType).Id;
    }
}
