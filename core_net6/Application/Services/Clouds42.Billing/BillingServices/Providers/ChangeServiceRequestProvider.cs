﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы
    /// с заявкой изменения сервиса на модерацию
    /// </summary>
    internal class ChangeServiceRequestProvider(
        IUnitOfWork dbLayer,
        IBillingServiceCardDataProvider billingServiceCardDataProvider,
        IChangeServiceRequestDataProvider changeServiceRequestDataProvider)
        : IChangeServiceRequestProvider
    {
        /// <summary>
        /// Получить заявку изменения сервиса на модерацию
        /// </summary>
        /// <param name="changeServiceRequestId">Id заявки</param>
        /// <returns>Заявка изменения сервиса на модерацию</returns>
        public ChangeServiceRequestDto Get(Guid changeServiceRequestId)
        {
            var changeServiceRequest =
                dbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.Id == changeServiceRequestId) ??
                throw new NotFoundException(
                    $"Не получить заявку изменения сервиса на модерацию по Id {changeServiceRequestId}");

            var changeServiceRequestDto = new ChangeServiceRequestDto
            {
                Id = changeServiceRequest.Id,
                ChangeRequestType = changeServiceRequest.ChangeRequestType,
                Status = changeServiceRequest.Status,
                ModeratorComment = changeServiceRequest.ModeratorComment,
                CurrentServiceData = billingServiceCardDataProvider.GetData(changeServiceRequest.BillingServiceId)
            };

            if (changeServiceRequest.ChangeRequestType == ChangeRequestTypeEnum.Creation)
                return changeServiceRequestDto;

            changeServiceRequestDto.NewServiceData =
                changeServiceRequestDataProvider.GetBillingServiceChangesDto(changeServiceRequest.Id);

            var serviceTypeChangesRelations = changeServiceRequestDto.CurrentServiceData.BillingServiceTypes.Join(
                    changeServiceRequestDto.NewServiceData.BillingServiceTypes, oldSt => oldSt.Id,
                    newSt => newSt.ServiceTypeId,
                    (oldSt, newSt) => new
                    {
                        ServiceType = oldSt,
                        ServiceTypeChange = newSt
                    })
                .ToDictionary(st => st.ServiceType, st => st.ServiceTypeChange);


            UpdateNewServiceData(changeServiceRequestDto.CurrentServiceData,
                changeServiceRequestDto.NewServiceData);

            UpdateNewServiceTypesData(serviceTypeChangesRelations);

            return changeServiceRequestDto;
        }

        /// <summary>
        /// Получить заявку на изменение сервиса
        /// </summary>
        /// <param name="billingServiceChangesId">Id изменения по сервису биллинга</param>
        /// <returns>Заявка на изменение сервиса</returns>
        public EditServiceRequest GetEditServiceRequest(Guid billingServiceChangesId) =>
            dbLayer.GetGenericRepository<EditServiceRequest>()
                .FirstOrDefault(bc => bc.BillingServiceChangesId == billingServiceChangesId) ??
            throw new NotFoundException($"Не удалось найти заявку на изменение сервиса по Id {billingServiceChangesId}");

        /// <summary>
        /// Обновить новые данные услуг
        /// Если изменений у услуги нет, записать текущие данные услуи
        /// </summary>
        /// <param name="serviceTypeChangesRelations">Связь услуг и их изменений</param>
        private void UpdateNewServiceTypesData(
            Dictionary<BillingServiceTypeDto, BillingServiceTypeChangeDto> serviceTypeChangesRelations)
        {
            foreach (var relation in serviceTypeChangesRelations)
            {
                relation.Value.ServiceTypeRelations =
                    relation.Key.ServiceTypeRelations;

                relation.Value.BillingType =
                    relation.Key.BillingType;

                relation.Value.ConnectionToDatabaseType =
                    relation.Key.ConnectionToDatabaseType;

                relation.Value.DependServiceTypeId =
                    relation.Key.DependServiceTypeId;

                relation.Value.Name = GetNewValueNonNull(relation.Key.Name,
                    relation.Value.Name);

                relation.Value.Description = GetNewValueNonNull(relation.Key.Description,
                    relation.Value.Description);

                relation.Value.ServiceTypeCost ??= relation.Key.ServiceTypeCost;
            }
        }

        /// <summary>
        /// Обновить новые данные сервиса
        /// Если изменений у сервиса нет, записать текущие данные сервиса
        /// </summary>
        /// <param name="billingServiceCardDto">Текущие данные сервиса</param>
        /// <param name="billingServiceChangesDto">Новые данные сервиса</param>
        private void UpdateNewServiceData(BillingServiceCardDto billingServiceCardDto,
            BillingServiceChangesDto billingServiceChangesDto)
        {
            billingServiceChangesDto.Name =
                GetNewValueNonNull(billingServiceCardDto.Name, billingServiceChangesDto.Name);
        }

        /// <summary>
        /// Получить новое значение отличное от null
        /// Если новое значение не null вернуть его,
        /// иначе вернуть старое значение
        /// </summary>
        /// <param name="oldValue">Старое значение</param>
        /// <param name="newValue">Новое значение</param>
        /// <returns></returns>
        private static string GetNewValueNonNull(string oldValue, string newValue) =>
            string.IsNullOrEmpty(newValue) ? oldValue : newValue;
    }
}
