﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы с данными сервиса биллинга
    /// </summary>
    public class BillingServiceDataProvider(
        IUnitOfWork dbLayer,
        IGetConfigurationDatabaseCommand getConfigurationDatabaseCommand)
        : IBillingServiceDataProvider
    {
        /// <summary>
        /// Получить конфигурации 1С совместимые с сервисом биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса биллинга</param>
        /// <returns>Конфигурации 1С совместимые с сервисом биллинга</returns>
        public IEnumerable<string> GetConfigurationsCompatibleWithService(Guid serviceId) =>
            getConfigurationDatabaseCommand.Execute(serviceId);


        /// <summary>
        /// Получить сервис биллинга
        /// или выкинуть ошибку
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        public BillingService GetBillingServiceOrThrowNewException(Guid billingServiceId) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(x => x.Id == billingServiceId) ??
            throw new NotFoundException($"Сервис '{billingServiceId}' не найден!");

        /// <summary>
        /// Необходимость создавать информационную базу для активации сервиса
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Признак, указывающий, нужно ли создавать базу для активации сервиса</returns>
        public bool NeedCreateDatabaseToActivateService(BillingService billingService, Guid accountId)
        {
            if (billingService.InternalCloudService == InternalCloudServiceEnum.Tardis)
                return false;

            return !GetAccountDatabasesWithEqualServiceConfiguration(billingService, accountId).Any();
        }

        /// <summary>
        /// Получить список совместимых с конфигурацией сервиса инф. баз
        /// </summary>
        /// <param name="billingService">Сервис облака</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список совместимых с конфигурацией сервиса инф. баз</returns>
        public IEnumerable<Domain.DataModels.AccountDatabase> GetAccountDatabasesWithEqualServiceConfiguration(
            BillingService billingService, Guid accountId)
        {
            var configurationDependencies =
                GetConfigurationsCompatibleWithService(billingService.Id).ToList();

            return dbLayer.DatabasesRepository.Where(db =>
                (db.State == DatabaseState.Ready.ToString() || db.State == DatabaseState.NewItem.ToString()) &&
                db.AccountId == accountId &&
                db.AccountDatabaseOnDelimiter != null  &&
                configurationDependencies.Contains(
                    db.AccountDatabaseOnDelimiter.DbTemplateDelimiter.ConfigurationId)).AsEnumerable();
        }

        /// <summary>
        /// Получить список данных о шаблонах инф. баз
        /// которые совместимы с сервисом
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список данных о шаблонах инф. баз
        /// которые совместимы с сервисом</returns>
        public IEnumerable<DatabaseTemplateDataDto> GetDatabaseTemplatesCompatibleWithService(Guid serviceId)
        {
            var configurationDependencies =
                GetConfigurationsCompatibleWithService(serviceId).ToList();

            if (!configurationDependencies.Any())
                return new List<DatabaseTemplateDataDto>();

            return (from templateDelimiters in dbLayer.DbTemplateDelimitersReferencesRepository.WhereLazy()
                    join configurationDependency in configurationDependencies on templateDelimiters.ConfigurationId equals
                        configurationDependency
                    join dbTemplate in dbLayer.DbTemplateRepository.WhereLazy() on templateDelimiters.TemplateId equals
                        dbTemplate.Id
                    select new DatabaseTemplateDataDto
                    {
                        TemplateId = templateDelimiters.TemplateId,
                        TemplateName = templateDelimiters.Name,
                        DatabaseTemplateImageCssClass = dbTemplate.ImageUrl
                    }).AsEnumerable();
        }
    }
}
