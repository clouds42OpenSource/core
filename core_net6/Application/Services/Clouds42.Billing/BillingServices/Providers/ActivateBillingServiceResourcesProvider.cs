﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер активации ресурсов сервиса.
    /// </summary>
    public class ActivateBillingServiceResourcesProvider(
        IUnitOfWork dbLayer,
        IDemoResourceRegistrator demoResourceRegistrator,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Создать сервиса с демо ресурсами.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountId">Номер аккаунта которому нужно подключить сервис</param>
        /// <param name="isDemoPeriod">Активировать как демо.</param>        
        public void CreateServiceWithDemoResources(Guid serviceId, Guid accountId, bool isDemoPeriod = false)
        {
            var service = dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId);

            if (service == null)
                throw new NotFoundException($"По номеру {serviceId} не найден сервис облака");

            logger.Trace($"Начинаем активацию сервиса {service.Name} с демо ресурсами для аккаунта {accountId}");
            CreateServiceWithDemoResources(service, accountId, isDemoPeriod);
            logger.Trace($"Активация сервиса {service.Name} с демо ресурсами для аккаунта {accountId} завершена");
        }

        /// <summary>
        /// Создать сервиса с демо ресурсами.
        /// </summary>
        /// <param name="service">Сервис.</param>
        /// <param name="accountId">Номер аккаунта которому нужно подключить сервис</param>
        /// <param name="isDemoPeriod">Активировать как демо.</param>  
        public void CreateServiceWithDemoResources(BillingService service, Guid accountId, bool isDemoPeriod = false)
        {
            try
            {
                if (service.BillingServiceStatus != BillingServiceStatusEnum.IsActive)
                    throw new InvalidOperationException("Можно активировать только прошедший модерацию сервис");
            
                var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);
            
                if (account == null)
                    throw new NotFoundException($"По номеру {accountId} не найден аккаунт");
            
                var resConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.BillingServiceId == service.Id && rc.AccountId == accountId);
            
                if (resConfig == null)
                    RegisterResourceConfigurationAndDemoResources(service, account, isDemoPeriod);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка создания сервиса с демо ресурсами]");
                throw;
            }

        }

        /// <summary>
        /// Зарегистрировать ресурс конфигурацию с демо ресурсами.
        /// </summary>
        /// <param name="service">Сервис.</param>
        /// <param name="account">Аккаунт которому нужно подключить сервис.</param>
        /// <param name="isDemoPeriod">Активировать как демо.</param>
        private void RegisterResourceConfigurationAndDemoResources(BillingService service, IAccount account, bool isDemoPeriod)
        {
            logger.Trace($"Начинаем создание записи ResourceConfiguration сервиса {service.Name} для аккаунта {account.IndexNumber}");
            var resConfig = CreateResourcesConfigurationRecord(service, account, isDemoPeriod);
            dbLayer.ResourceConfigurationRepository.Insert(resConfig);
            dbLayer.Save();

            logger.Trace($"Запись ResourceConfiguration сервиса {service.Name} для аккаунта {account.IndexNumber} создана");

            var freeResourceCount = CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod();

            demoResourceRegistrator.RegisterDemoResources(resConfig.BillingService, account, freeResourceCount);

            dbLayer.Save();
            recalculateResourcesConfigurationCostProvider.Recalculate(account.Id, service);
        }

        /// <summary>
        /// Создать запись в БД ресурс конфигурации. 
        /// </summary>
        /// <param name="service">Сервис.</param>
        /// <param name="account">Аккаунт.</param>
        /// <param name="isDemoPeriod">Активировать как демо.</param>
        /// <returns>Вставленный в Бд объект ресурс конфигурации.</returns>
        private ResourcesConfiguration CreateResourcesConfigurationRecord(
            BillingService service,
            IAccount account,
            bool isDemoPeriod)
        {
            return new ResourcesConfiguration
            {
                AccountId = account.Id,
                BillingServiceId = service.Id,
                Cost = 0,
                CostIsFixed = false,
                DiscountGroup = 0,
                ExpireDate = CalculateExpireDate(service, isDemoPeriod),
                Id = Guid.NewGuid(),
                Frozen = CalculateFrozen(service),
                CreateDate = DateTime.Now,
                IsDemoPeriod = isDemoPeriod,
                IsAutoSubscriptionEnabled = service.InternalCloudService == InternalCloudServiceEnum.AlpacaMeet
            };
        }

        /// <summary>
        /// Подсчитать дату окончания сервиса.
        /// </summary>
        /// <param name="service">Сервис.</param>
        /// <param name="isDemoPeriod">Активировать как демо.</param>
        /// <returns>Дата окончания сервиса.</returns>
     
        private static DateTime? CalculateExpireDate(BillingService service, bool isDemoPeriod)
        {
            if(service.InternalCloudService ==  InternalCloudServiceEnum.AlpacaMeet)
                return DateTime.Now.AddDays(3);

            var mainService = service.BillingServiceTypes.All(w => w.DependServiceTypeId == null);
            if (mainService || !isDemoPeriod)
                return null;

            return DateTime.Now.AddDays(Clouds42SystemConstants.MyEnterpriseDemoPeriodDays);
        }

        /// <summary>
        /// Подсчитать признак блокировки сервиса.
        /// </summary>
        /// <param name="service">Сервис.</param>
        /// <returns>Признак блокировки сервиса.</returns>
        private static bool? CalculateFrozen(BillingService service)
        {
            if (service.BillingServiceTypes.Any(w => w.DependServiceTypeId != null))
                return null;

            return false;
        }

    }
}
