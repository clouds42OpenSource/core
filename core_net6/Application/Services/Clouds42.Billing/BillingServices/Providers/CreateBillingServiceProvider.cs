﻿using Clouds42.Billing.BillingServices.Actions;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для создания сервиса биллинга
    /// </summary>
    internal class CreateBillingServiceProvider : ICreateBillingServiceProvider
    {
        private readonly List<(Func<BillingServiceStatusEnum, bool>, IServiceOperationAction<CreateBillingServiceDto, Guid>)> _createServiceActionList;

        public CreateBillingServiceProvider(IServiceProvider serviceProvider)
        {
            _createServiceActionList =
            [
                (serviceStatus => serviceStatus == BillingServiceStatusEnum.Draft,
                    serviceProvider.GetRequiredService<CreateBillingServiceDraftAction>()),
                (serviceStatus => serviceStatus == BillingServiceStatusEnum.OnModeration,
                    serviceProvider.GetRequiredService<CreateBillingServiceAction>())
            ];
        }

        /// <summary>
        /// Создать сервис
        /// </summary>
        /// <param name="createBillingServiceDto">Модель создания сервиса</param>
        /// <returns>ID сервиса</returns>
        public Guid Create(CreateBillingServiceDto createBillingServiceDto)
        {
            var createAction = _createServiceActionList
                                   .FirstOrDefault(tuple => tuple.Item1(createBillingServiceDto.BillingServiceStatus))
                                   .Item2
                               ?? throw new InvalidOperationException(
                                   $"Не удалось найти действие для создания сервиса со статусом {createBillingServiceDto.BillingServiceStatus}");
            return createAction.Do(createBillingServiceDto);
        }
    }
}
