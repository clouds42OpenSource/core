﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы с данными владельца сервиса
    /// </summary>
    internal class BillingServiceOwnerDataProvider(IUnitOfWork dbLayer) : IBillingServiceOwnerDataProvider
    {
        /// <summary>
        /// Получить админа аккаунта
        /// для владельца сервиса
        /// </summary>
        /// <param name="service">Сервис биллинга</param>
        /// <returns>Админ аккаунта</returns>
        public AccountUser GetAccountAdminOwnerOfService(BillingService service)
        {
            if (!service.AccountOwnerId.HasValue)
                throw new InvalidOperationException(
                    $"Не определен владелец для сервиса {service.Id}");

            var accountAdmin = dbLayer.AccountUsersRepository.FirstOrDefault(au =>
                                   au.AccountUserRoles.Any(aur => aur.AccountUserGroup == AccountUserGroup.AccountAdmin) &&
                                   au.AccountId == service.AccountOwnerId.Value && au.Email != null) ??
                               throw new NotFoundException(
                                   $"Не удалось найти админа аккаунта {service.AccountOwnerId.Value}");

            return accountAdmin;
        }

        /// <summary>
        /// Получить админа аккаунта
        /// для владельца сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса биллинга</param>
        /// <returns>Админ аккаунта</returns>
        public AccountUser GetAccountAdminOwnerOfService(Guid serviceId) =>
            GetAccountAdminOwnerOfService(GetBillingServiceOrThrowException(serviceId));

        /// <summary>
        /// Получить сервис или выкинуть исключение
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        private BillingService GetBillingServiceOrThrowException(Guid serviceId) =>
            dbLayer.BillingServiceRepository.FirstOrThrowException(service => service.Id == serviceId,
                $"Не удалось получить сервис по Id '{serviceId}'");

        /// <summary>
        /// Получить аккаунта владелец сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Id аккаунта владельца</returns>
        public Guid GetAccountOwnerByServiceId(Guid serviceId)
        {
            var service = GetBillingServiceOrThrowException(serviceId);

            if (!service.AccountOwnerId.HasValue)
                throw new NotFoundException($"Не удалось получить аккаунт владелец сервисом {serviceId}");

            return service.AccountOwnerId.Value;
        }
    }
}
