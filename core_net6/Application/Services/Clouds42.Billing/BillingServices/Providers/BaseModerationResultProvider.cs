﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.NotifyBeforeChangeServiceCost;
using Clouds42.CoreWorker.JobWrappers.RecalculateServiceCostAfterChanges;
using Clouds42.Logger;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.DataModels.History;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Базовый провайдер для работы с
    /// результатом модерации заявки на изменения сервиса
    /// </summary>
    public abstract class BaseModerationResultProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IServiceProvider serviceProvider,
        ICreateRateProvider createRateProvider,
        IRateDataProvider rateDataProvider,
        ICreateAccountRateProvider createAccountRateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        protected readonly ILogger42 Logger = logger;
        protected readonly IUnitOfWork DbLayer = dbLayer;
        protected readonly IAccessProvider AccessProvider = accessProvider;
        private readonly IRecalculateServiceCostAfterChangesJobWrapper _recalculateServiceCostJobWrapper = serviceProvider.GetRequiredService<IRecalculateServiceCostAfterChangesJobWrapper>();
        private readonly INotifyBeforeChangeServiceCostJobWrapper _notifyBeforeChangeServiceCostJobWrapper = serviceProvider.GetRequiredService<INotifyBeforeChangeServiceCostJobWrapper>();
        private readonly Lazy<int> _beforeRecalculationServiceMonthsCount = new(CloudConfigurationProvider.BillingServices
            .GetBeforeRecalculationCostOfServiceMonthsCount);

        /// <summary>
        /// Проверить возможность обработать
        /// заявку изменений сервиса на модерацию
        /// </summary>
        /// <param name="changeServiceRequest">Заявка изменения сервиса на модерацию</param>
        /// <param name="moderationResult">Результат модерации заявки</param>
        protected static void CheckAbilityProcessChangeServiceRequest(ChangeServiceRequest changeServiceRequest,
            ChangeServiceRequestModerationResultDto moderationResult)
        {
            if (changeServiceRequest.Status != ChangeRequestStatusEnum.OnModeration)
                throw new InvalidOperationException(
                    $"Результат модерации по заявке {changeServiceRequest.Id} обработать не возможно.");

            if (string.IsNullOrEmpty(moderationResult.ModeratorComment))
                throw new InvalidOperationException($"{nameof(moderationResult.ModeratorComment)} обязательное поле.");
        }

        /// <summary>
        /// Обновить заявку на модерацию сервиса
        /// на основании модели "Результат модерации заявки
        /// на изменения сервиса"
        /// </summary>
        /// <param name="changeServiceRequest">Заявка на модерацию сервиса</param>
        /// <param name="moderationResult">Результат модерации заявки</param>
        protected void UpdateChangeServiceRequest(ChangeServiceRequest changeServiceRequest,
            ChangeServiceRequestModerationResultDto moderationResult)
        {
            changeServiceRequest.ModeratorComment = moderationResult.ModeratorComment;
            changeServiceRequest.Status = moderationResult.Status;
            changeServiceRequest.StatusDateTime = DateTime.Now;
            changeServiceRequest.ModeratorId = AccessProvider.GetUser()?.Id;

            DbLayer.GetGenericRepository<ChangeServiceRequest>().Update(changeServiceRequest);
            DbLayer.Save();
        }

        /// <summary>
        /// Получить заявку на модерацию сервиса
        /// </summary>
        /// <param name="changeServiceRequestId">Id заявки</param>
        /// <returns>Заявка на модерацию сервиса</returns>
        protected ChangeServiceRequest GetChangeServiceRequest(Guid changeServiceRequestId) =>
            DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.Id == changeServiceRequestId)
                ?? throw new NotFoundException($"Не удалось получить заявку на модерацию сервиса по Id {changeServiceRequestId}");

        /// <summary>
        /// Получить изменения сервиса биллинга по заявке
        /// </summary>
        /// <param name="changeServiceRequestId">Id заявки</param>
        /// <returns>Изменения сервиса биллинга по заявке</returns>
        protected BillingServiceChanges GetBillingServiceChanges(Guid changeServiceRequestId) =>
            DbLayer.GetGenericRepository<EditServiceRequest>().FirstOrDefault(esr => esr.Id == changeServiceRequestId)?.BillingServiceChanges 
                ?? throw new NotFoundException($"Не удалось найти изменения сервиса по заявке {changeServiceRequestId}");

        /// <summary>
        /// Получить новое значение
        /// при условии если оно не пустое
        /// (если пустое то вернет старое значение)
        /// </summary>
        /// <param name="oldValue">Старое значение</param>
        /// <param name="newValue">Новое значение</param>
        /// <returns></returns>
        protected static string GetNewValueIfNonEmpty(string oldValue, string newValue) =>
            newValue.IsNullOrEmpty() ? oldValue : newValue;

        /// <summary>
        /// Получить новое значение
        /// при условии если оно не пустое
        /// (если пустое то вернет старое значение)
        /// </summary>
        /// <param name="oldValue">Старое значение</param>
        /// <param name="newValue">Новое значение</param>
        /// <returns></returns>
        protected static Guid? GetNewValueIfNonEmpty(Guid? oldValue, Guid? newValue) =>
            newValue.HasValue && newValue != Guid.Empty ? newValue : oldValue;

        
        /// <summary>
        /// Обновить услуги сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="billingServiceChangesId">Id изменений по сервису биллинга</param>
        /// <param name="serviceTypeChangesRelations">Связь услуг и их изменений</param>
        protected void UpdateBillingServiceTypes(Guid serviceId, Guid billingServiceChangesId,
            Dictionary<BillingServiceType, BillingServiceTypeChange> serviceTypeChangesRelations)
        {
            if (!serviceTypeChangesRelations.Any())
                return;

            var message = $"Обновление услуг сервиса {serviceId}";
            Logger.Info(message);

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                foreach (var bss in serviceTypeChangesRelations)
                    UpdateBillingServiceType(bss);


                if (serviceTypeChangesRelations.All(str => str.Value.Cost == null))
                {
                    dbScope.Commit();
                    return;
                }

                Logger.Info($"Обновление стоимости услуг для сервиса {serviceId}");
                UpdateBillingServiceTypesCost(CreateUpdateBillingServiceTypesCostDto(serviceId,
                    billingServiceChangesId,
                    serviceTypeChangesRelations));

                dbScope.Commit();
                Logger.Info($"{message} завершилось успешно.");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка обновления услуг сервиса] {serviceId}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать новые услуги сервиса биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="newBillingServiceTypes">Новые услуги сервиса биллинга</param>
        /// <param name="localId">Id локали</param>
        protected void CreateNewServiceTypes(Guid serviceId,
            List<BillingServiceTypeChange> newBillingServiceTypes, Guid localId)
        {
            if (!newBillingServiceTypes.Any())
                return;

            var message = $"[{serviceId}] - Создание новых услуг";
            Logger.Info(message);

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                newBillingServiceTypes.ForEach(st => CreateNewServiceType(serviceId, st, localId));
                DbLayer.Save();
                dbScope.Commit();
                Logger.Info($"{message} завершилось успешно.");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания новых услуг] {serviceId}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Получить услуги сервиса для удаления
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <param name="billingServiceTypeChanges">Изменения по услугам сервиса</param>
        /// <returns>Услуги сервиса для удаления</returns>
        protected static List<BillingServiceType> GetBillingServiceTypesForRemove(BillingService billingService,
            List<BillingServiceTypeChange> billingServiceTypeChanges)
        {
            var billingServiceTypes = billingService.BillingServiceTypes.Where(st => !st.IsDeleted).ToList();
            var billingServiceTypeChangesId = billingServiceTypeChanges.Where(chSt => !chSt.IsNew)
                .Select(chSt => chSt.BillingServiceTypeId);
            return billingServiceTypes.Where(st => !billingServiceTypeChangesId.Contains(st.Id)).ToList();
        }

        /// <summary>
        /// Создание новой услуги сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="billingServiceTypeChange">Изменения услуги</param>
        /// <param name="localId">Id локали</param>
        private void CreateNewServiceType(Guid serviceId, BillingServiceTypeChange billingServiceTypeChange,
            Guid localId)
        {
            if (!billingServiceTypeChange.BillingType.HasValue)
                return;

            if (!billingServiceTypeChange.Cost.HasValue)
                return;

            var prefix = $"[{serviceId}]-[{billingServiceTypeChange.Id}]";

            Logger.Info(
                $"{prefix} - Создание услуги {billingServiceTypeChange.Name}");

            var serviceType = new BillingServiceType
            {
                Id = billingServiceTypeChange.Id,
                Key = billingServiceTypeChange.Id,
                Name = billingServiceTypeChange.Name,
                Description = billingServiceTypeChange.Description,
                ServiceId = serviceId,
                BillingType = billingServiceTypeChange.BillingType.Value,
                DependServiceTypeId = billingServiceTypeChange.DependServiceTypeId
            };

            DbLayer.BillingServiceTypeRepository.Insert(serviceType);

            Logger.Info(
                $"{prefix} - Создание тарифа для услуги на сумму {billingServiceTypeChange.Cost}");

            createRateProvider.Create(serviceType.Id, billingServiceTypeChange.Cost.Value, localId);
        }

        /// <summary>
        /// Обновить стоимость услуг сервиса
        /// </summary>
        /// <param name="updateBillingServiceTypesCostDto">
        /// Модель обновления стоимости услуг сервиса</param>
        private void UpdateBillingServiceTypesCost(UpdateBillingServiceTypesCostDto updateBillingServiceTypesCostDto)
        {
            var accountIdsWithBlockedRent1C = SelectAccountsWithBlockedServiceFromList(
                updateBillingServiceTypesCostDto.AccountIdsThatUseService,
                updateBillingServiceTypesCostDto.BillingServiceId);

            var accountIdsWithActiveRent1C = updateBillingServiceTypesCostDto.AccountIdsThatUseService
                .Where(a => !accountIdsWithBlockedRent1C.Contains(a)).ToList();

            foreach (var serviceTypeChangesRelation in updateBillingServiceTypesCostDto.ServiceTypeChangesRelations)
                UpdateBillingServiceTypeRate(serviceTypeChangesRelation, accountIdsWithActiveRent1C);

            var serviceCostChangeDate = DateTime.Now.AddMonths(_beforeRecalculationServiceMonthsCount.Value);

            RunTaskRecalculatingServiceCost(updateBillingServiceTypesCostDto, accountIdsWithBlockedRent1C,
                DateTime.Now);

            RunTaskRecalculatingServiceCost(updateBillingServiceTypesCostDto, accountIdsWithActiveRent1C,
                serviceCostChangeDate);

            RunTaskForNotifyBeforeChangeServiceCost(updateBillingServiceTypesCostDto.BillingServiceChangesId,
                accountIdsWithActiveRent1C, serviceCostChangeDate);
        }

        /// <summary>
        /// Запустить задачу по уведомлению аккаунтов
        /// о скором изменении стоимости сервиса
        /// </summary>
        /// <param name="billingServiceChangesId">ID изменений по сервису биллинга</param>
        /// <param name="accountIds">Список Id аккаунтов
        /// для которых нужно сделать уведомление</param>
        /// <param name="serviceCostChangeDate">Дата изменения стоимости сервиса</param>
        private void RunTaskForNotifyBeforeChangeServiceCost(Guid billingServiceChangesId, List<Guid> accountIds,
            DateTime serviceCostChangeDate)
        {
            if (!accountIds.Any())
                return;

            _notifyBeforeChangeServiceCostJobWrapper.Start(new NotifyBeforeChangeServiceCostJobParams
            {
                BillingServiceChangesId = billingServiceChangesId,
                AccountIds = accountIds,
                ServiceCostChangeDate = serviceCostChangeDate
            });
        }

        /// <summary>
        /// Обновить тариф для услуги сервиса
        /// </summary>
        /// <param name="serviceTypeChangesRelation">Связь услуги и ее изменений</param>
        /// <param name="accountIds">Список Id аккаунтов для которых
        /// нужно сохранить старую стоимость тарифа</param>
        private void UpdateBillingServiceTypeRate(
            KeyValuePair<BillingServiceType, BillingServiceTypeChange> serviceTypeChangesRelation,
            List<Guid> accountIds)
        {
            if (!serviceTypeChangesRelation.Value.Cost.HasValue)
                return;

            var message = $"Обновление стоимости услуги {serviceTypeChangesRelation.Key.Id}";

            var serviceTypeRate = rateDataProvider.GetRateOrThrowException(serviceTypeChangesRelation.Key.Id);

            accountIds.ForEach(accountId =>
                createAccountRateProvider.Create(new CreateAccountRateDto
                {
                    AccountId = accountId,
                    BillingServiceTypeId = serviceTypeChangesRelation.Key.Id,
                    Cost = serviceTypeRate.Cost
                }));

            CreateBillingServiceTypeCostChangedHistory(serviceTypeChangesRelation.Value.Id, serviceTypeRate.Cost);
            serviceTypeRate.Cost = serviceTypeChangesRelation.Value.Cost.Value;
            DbLayer.RateRepository.Update(serviceTypeRate);
            DbLayer.Save();
            Logger.Info($"{message} завершилось успешно.");
        }

        /// <summary>
        /// Создать историю изменения стоимости услуги сервиса
        /// </summary>
        /// <param name="billingServiceTypeChangeId">Id измененной услуги</param>
        /// <param name="oldCost">Старая стоимость</param>
        private void CreateBillingServiceTypeCostChangedHistory(Guid billingServiceTypeChangeId, decimal oldCost)
        {
            Logger.Info($"Создание истории на изменение стоимости услуги {billingServiceTypeChangeId}");

            var history = new BillingServiceTypeCostChangedHistory
            {
                Id = Guid.NewGuid(),
                BillingServiceTypeChangeId = billingServiceTypeChangeId,
                OldCost = oldCost
            };

            DbLayer.GetGenericRepository<BillingServiceTypeCostChangedHistory>().Insert(history);
            DbLayer.Save();
        }

        /// <summary>
        /// Создать модель обновления стоимости услуг сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="billingServiceChangesId">Id изменений по сервису биллинга</param>
        /// <param name="serviceTypeChangesRelations">Связь услуг и их изменений</param>
        private UpdateBillingServiceTypesCostDto CreateUpdateBillingServiceTypesCostDto(Guid serviceId,
            Guid billingServiceChangesId,
            Dictionary<BillingServiceType, BillingServiceTypeChange> serviceTypeChangesRelations)
        {
            var accountIdsThatUseService = DbLayer.BillingServiceRepository.GetAccountIdsThatUseService(serviceId);

            var updateBillingServiceTypesCostDto = new UpdateBillingServiceTypesCostDto
            {
                BillingServiceId = serviceId,
                BillingServiceChangesId = billingServiceChangesId,
                NewServiceTypesCost = serviceTypeChangesRelations.Sum(str =>
                    str.Value.Cost ?? rateDataProvider.GetRateOrThrowException(str.Key.Id).Cost),
                OldServiceTypesCost =
                    serviceTypeChangesRelations.Sum(str => rateDataProvider.GetRateOrThrowException(str.Key.Id).Cost),
                ServiceTypeChangesRelations = serviceTypeChangesRelations,
                AccountIdsThatUseService = accountIdsThatUseService
            };

            return updateBillingServiceTypesCostDto;
        }

        /// <summary>
        /// Запустить задачу по пересчету стоимости сервиса
        /// </summary>
        /// <param name="updateBillingServiceTypesCostDto">Модель обновления стоимости услуг сервиса</param>
        /// <param name="accountIds">Список Id аккаунтов которым нужно
        /// пересчитать стоимость сервиса</param>
        /// <param name="dateTimeDelayOperation">Дата задержки операции по пересчету</param>
        private void RunTaskRecalculatingServiceCost(UpdateBillingServiceTypesCostDto updateBillingServiceTypesCostDto,
            List<Guid> accountIds, DateTime dateTimeDelayOperation)
        {
            if (!accountIds.Any())
                return;

            _recalculateServiceCostJobWrapper.Start(
                new RecalculateServiceCostAfterChangesJobParams
                {
                    BillingServiceChangesId = updateBillingServiceTypesCostDto.BillingServiceChangesId,
                    AccountIds = accountIds
                }, dateTimeDelayOperation);

            var taskQueueId = _recalculateServiceCostJobWrapper.GetTaskQueueId() ??
                              throw new InvalidOperationException(
                                  $"Не удалось получить Id запущенной задачи по пересчету сервиса {updateBillingServiceTypesCostDto.BillingServiceChangesId}");

            var recalculationServiceCostAfterChange = new RecalculationServiceCostAfterChange
            {
                BillingServiceChangesId = updateBillingServiceTypesCostDto.BillingServiceChangesId,
                CoreWorkerTasksQueueId = taskQueueId,
                OldServiceCost = updateBillingServiceTypesCostDto.OldServiceTypesCost,
                NewServiceCost = updateBillingServiceTypesCostDto.NewServiceTypesCost
            };

            DbLayer.GetGenericRepository<RecalculationServiceCostAfterChange>()
                .Insert(recalculationServiceCostAfterChange);
            DbLayer.Save();
        }

        /// <summary>
        /// Обновить услугe сервиса
        /// </summary>
        /// <param name="serviceTypeChangesRelation">Связь услуги и ее изменений</param>
        private void UpdateBillingServiceType(
            KeyValuePair<BillingServiceType, BillingServiceTypeChange> serviceTypeChangesRelation)
        {
            serviceTypeChangesRelation.Key.Name = GetNewValueIfNonEmpty(serviceTypeChangesRelation.Key.Name,
                serviceTypeChangesRelation.Value.Name);

            serviceTypeChangesRelation.Key.Description = GetNewValueIfNonEmpty(
                serviceTypeChangesRelation.Key.Description, serviceTypeChangesRelation.Value.Description);

            DbLayer.BillingServiceTypeRepository.Update(serviceTypeChangesRelation.Key);
            DbLayer.Save();
        }

        /// <summary>
        /// Выбрать из списка аккаунты, у которых истек демо период сервиса или заблокирована Аренда 1С
        /// </summary>
        /// <param name="accountIds">Список Id аккаунтов</param>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <returns>Список Id аккаунтов, у которых истек демо период сервиса или заблокирована Аренда 1С</returns>
        private List<Guid> SelectAccountsWithBlockedServiceFromList(List<Guid> accountIds, Guid billingServiceId)
            => (from accountId in accountIds
                join rentConf in DbLayer.ResourceConfigurationRepository.WhereLazy(rc =>
                    rc.BillingService.SystemService == Clouds42Service.MyEnterprise) on accountId equals rentConf
                    .AccountId
                join servConf in DbLayer.ResourceConfigurationRepository.WhereLazy(rc =>
                    rc.BillingServiceId == billingServiceId) on accountId equals servConf.AccountId
                where rentConf.Frozen == true || rentConf.ExpireDate < DateTime.Now ||
                      servConf.IsDemoPeriod && servConf.ExpireDate < DateTime.Now
                select accountId).Distinct().ToList();

    }
}
