﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для создания связи конфигурации 1С
    /// с услугой сервиса(Мои информационные базы)
    /// </summary>
    internal class CreateConfigurationServiceTypeRelationProvider(IUnitOfWork dbLayer)
        : ICreateConfigurationServiceTypeRelationProvider
    {
        /// <summary>
        /// Создать связь конфигурации 1С
        /// с услугой сервиса
        /// </summary>
        /// <param name="serviceTypeId">Id услуги сервиса</param>
        /// <param name="configurationName">Название конфигурации 1С</param>
        public void Create(Guid serviceTypeId, string configurationName)
        {
            var configurationServiceTypeRelation = new ConfigurationServiceTypeRelation
            {
                ServiceTypeId = serviceTypeId,
                Configuration1CName = configurationName
            };

            dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>()
                .Insert(configurationServiceTypeRelation);

            dbLayer.Save();
        }
    }
}
