﻿using System.Data;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер оплаты услуг сервиса
    /// </summary>
    public class BillingServiceTypePayProvider(
        IUnitOfWork dbLayer,
        BillingServiceTypeAccessHelper accessHelper,
        BillingServiceTypeAccessProvider billingServiceTypeAccessProvider,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IServiceUnlocker serviceUnlocker,
        IPromisePaymentProvider promisePaymentProvider,
        IIncreaseAgentPaymentProcessor increaseAgentPaymentProcessor,
        IBillingPaymentsProvider billingPaymentsProvider,
        ICreatePaymentHelper createPaymentHelper,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        IOnDisabledMyDatabasesServiceTypeForUsersTrigger onDisabledMyDatabasesServiceTypeForUsersTrigger,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException,
        ISender sender)
        : IBillingServiceTypePayProvider
    {
        /// <summary>
        /// Применить все изменения указанные для аккаунта и пользователей
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Обещанный платеж</param>
        /// <param name="dataAccount">Услуги на подключение к аккаунту</param>
        /// <param name="dataAccountUsers">Услуги на подключение к пользователям</param>
        public BillingServiceTypeApplyOrPayResultDto ApplyOrPayForAllChangesAccountServiceTypes(Guid accountId,
            Guid billingServiceId, bool isPromisePayment,
            ICollection<CalculateBillingServiceTypeDto> dataAccount,
            ICollection<CalculateBillingServiceTypeDto> dataAccountUsers)
        {
            var service = dbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == billingServiceId);
            if (!service.IsActive)
                throw new InvalidExpressionException("Сервис не активен");

            var accountConfigData =
                accountConfigurationDataProvider.GetAccountConfigurationByLocaleData(accountId);

            var invites = MapDataModelsToInvites((Account)accountConfigData.Account, service, dataAccount,
                dataAccountUsers);

            var paidInvites =
                invites.Where(invite => invite.Cost > 0 && accessHelper.IsNotExistResource(invite)).ToList();

            var amount = paidInvites.Sum(invite => invite.Cost);

            var paymentDefinition = GetPaymentDefinition(accountId, service, paidInvites);

            var resourceConfiguration = dbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .FirstOrDefault(w =>  w.AccountId == accountId && w.BillingServiceId == billingServiceId);

            var oldCostOfService =
                resourceConfigurationDataProvider.GetTotalServiceCost(resourceConfiguration);

            var paymentResult = TryCreatePayment(accountConfigData, service, amount, isPromisePayment,
                paymentDefinition);

            if (!paymentResult.IsComplete)
            {
                return paymentResult;
            }

            if (paymentResult.PaymentIds.Any())
                increaseAgentPaymentProcessor.Process(paymentResult.PaymentIds);

            accessHelper.ApplyResources((Account)accountConfigData.Account, invites);

            if (service.SystemService == Clouds42Service.MyDatabases)
                ExecuteTriggerOnDisabledMyDatabasesServiceType(invites);

            recalculateResourcesConfigurationCostProvider.Recalculate(accountId, service);
            RecalculateSponsorshipServiceCost(invites, service);

            dbLayer.ResourceConfigurationRepository.Reload(resourceConfiguration);
            serviceUnlocker.ProlongServiceIfTheCostIsLess(accountId, oldCostOfService, resourceConfiguration);

            return new BillingServiceTypeApplyOrPayResultDto
            {
                IsComplete = true,
                Amount = amount,
                Currency = accountConfigData.Locale.Currency
            };
        }

        /// <summary>
        /// Выполнить триггер на отключение
        /// лицензии по услуге у пользователей
        /// </summary>
        /// <param name="invites">Инвайты(данные) по изменениям лицензий услуги</param>
        public void ExecuteTriggerOnDisabledMyDatabasesServiceType(
            ICollection<CalculateBillingServiceTypeResultDto> invites)
        {
            var disabledLicenses = invites
                .Where(i => !i.Status)
                .GroupBy(i => i.BillingServiceTypeId)
                .Select(groupedData => new OnDisabledMyDatabasesServiceTypeForUsersTriggerDto
                {
                    ServiceTypeId = groupedData.Key, AccountUserIds = groupedData.Select(gd => gd.Subject).ToList()
                }).ToList();

            if (!disabledLicenses.Any())
                return;

            disabledLicenses.ForEach(onDisabledMyDatabasesServiceTypeForUsersTrigger.Execute);
        }

        /// <summary>
        /// Пересчитать спонсорские конфигурации
        /// </summary>
        /// <param name="invites">Инвайты</param>
        /// <param name="service">Сервис</param>
        private void RecalculateSponsorshipServiceCost(IEnumerable<CalculateBillingServiceTypeResultDto> invites,
            IBillingService service)
        {
            var filteredInvites = invites.Where(x => !x.Sponsorship.Me).Select(x => x.Subject).ToList();

            var accountIds = dbLayer.AccountUsersRepository
                .AsQueryableNoTracking()
                .Where(w => filteredInvites.Contains(w.Id)).Select(x => x.AccountId).ToList();

            sender.Send(new RecalculateResourcesConfigurationCostCommand(accountIds,
                [service.Id], service.SystemService)).Wait();
        }

        /// <summary>
        /// Конвертация в инвайты и получение стоимостей
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="service">Сервис</param>
        /// <param name="dataAccount">Данные</param>
        /// <param name="dataAccountUsers">Данные</param>
        private ICollection<CalculateBillingServiceTypeResultDto> MapDataModelsToInvites(IAccount account,
            IBillingService service,
            ICollection<CalculateBillingServiceTypeDto> dataAccount,
            ICollection<CalculateBillingServiceTypeDto> dataAccountUsers)
        {
            var result = new List<CalculateBillingServiceTypeResultDto>();

            if (dataAccountUsers?.Any() ?? false)
                result.AddRange(dataAccountUsers.Select(w => new CalculateBillingServiceTypeResultDto
                {
                    BillingServiceTypeId = w.BillingServiceTypeId,
                    Subject = w.Subject,
                    Status = w.Status,
                    Sponsorship = w.Sponsorship
                }));

            if (dataAccount?.Any() ?? false)
                result.AddRange(dataAccount.Select(w => new CalculateBillingServiceTypeResultDto
                {
                    BillingServiceTypeId = w.BillingServiceTypeId,
                    Subject = w.Subject,
                    Status = w.Status,
                    Sponsorship = w.Sponsorship
                }));

            billingServiceTypeAccessProvider.ApplyCostForActiveServiceTypes(account, service, result);

            return result;
        }

        /// <summary>
        /// Попытка создания платежа
        /// </summary>
        /// <param name="accountConfigData">Модель данных конфигурации аккаунта по локали</param>
        /// <param name="service">Сервис</param>
        /// <param name="amount">Сумма платежа</param>
        /// <param name="isPromisePayment">Обещанный платеж</param>
        /// <param name="paymentDefinition">Описание платежа</param>
        private BillingServiceTypeApplyOrPayResultDto TryCreatePayment(
            AccountConfigurationByLocaleDataDto accountConfigData, IBillingService service,
            decimal amount, bool isPromisePayment, string paymentDefinition)
        {
            if (amount <= 0)
                return new BillingServiceTypeApplyOrPayResultDto { IsComplete = true };

            var operationAttemptResult = billingPaymentsProvider
                .CanCreatePayment(
                    new PaymentDefinitionDto
                    {
                        Account = accountConfigData.Account.Id,
                        Date = DateTime.Now,
                        Id = Guid.NewGuid(),
                        BillingServiceId = service.Id,
                        Status = PaymentStatus.Done,
                        Total = amount,
                        OperationType = PaymentType.Outflow,
                        OriginDetails = "core-cp"
                    }, out var notEnoughMoney);

            if (operationAttemptResult != PaymentOperationResult.Ok && !isPromisePayment)
                return new BillingServiceTypeApplyOrPayResultDto
                {
                    IsComplete = false,
                    Currency = accountConfigData.Locale.Currency,
                    NotEnoughMoney = notEnoughMoney,
                    CanUsePromisePayment = CanUsePromisePayment((Account)accountConfigData.Account)
                };

            if (!isPromisePayment || notEnoughMoney <= 0)
            {
                return MakePayment((Account)accountConfigData.Account, service, amount, paymentDefinition);
            }

            var promisePaymentResult = promisePaymentProvider
                .CreatePromisePayment(accountConfigData.Account.Id,
                    new CalculationOfInvoiceRequestModel
                        { SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = notEnoughMoney } }, false);

            if (!promisePaymentResult.Complete)
                throw new InvalidOperationException(
                    $"Не удалось создать ОП для покупки сервиса {service.GetNameBasedOnLocalization(cloudLocalizer, accountConfigData.Account.Id)} на сумму {notEnoughMoney}");

            return MakePayment((Account)accountConfigData.Account, service, amount, paymentDefinition);
        }

        /// <summary>
        /// Сделать платеж
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="service">Сервис биллинга</param>
        /// <param name="amount">Сумма</param>
        /// <param name="paymentDefinition">Описание платежа</param>
        /// <returns>Результат операции</returns>
        private BillingServiceTypeApplyOrPayResultDto MakePayment(IAccount account, IBillingService service,
            decimal amount, string paymentDefinition)
        {
            var serviceResConfig =
                dbLayer.ResourceConfigurationRepository.FirstOrDefault(w =>
                    w.AccountId == account.Id && w.BillingServiceId == service.Id);

            var operationResult = createPaymentHelper.MakePayment(serviceResConfig,
                paymentDefinition, amount, PaymentSystem.ControlPanel, PaymentType.Outflow,
                "core-cp");

            if (operationResult.OperationStatus != PaymentOperationResult.Ok || !operationResult.PaymentIds.Any())
                throw new InvalidOperationException(
                    $"Ошибка проведения платежа для покупки сервиса {service.GetNameBasedOnLocalization(cloudLocalizer, account.Id)} на сумму {amount}");

            return new BillingServiceTypeApplyOrPayResultDto
            {
                IsComplete = true,
                PaymentIds = operationResult.PaymentIds
            };
        }

        /// <summary>
        /// Проверить, доступен ли ОП для данного аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        private bool CanUsePromisePayment(IAccount account)
        {
            var type = PaymentType.Inflow.ToString();
            var promisePaymentCost = new BillingPaymentsDataProvider(dbLayer, logger, handlerException).GetPromisePaymentCost(account.Id);
            var inflowPayments = dbLayer.PaymentRepository.Any(p =>
                p.AccountId == account.Id && p.OperationType == type && p.Sum > 0 && p.Status == "Done");
            return inflowPayments && promisePaymentCost is not > 0;
        }

        /// <summary>
        /// Получить описание платежа
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Сервис</param>
        /// <param name="paidInvites">Платные счета</param>
        /// <returns>Описание платежа</returns>
        private string GetPaymentDefinition(Guid accountId, IBillingService service,
            List<CalculateBillingServiceTypeResultDto> paidInvites)
        {
            var paymentDefinition = $"Покупка сервиса \"{service.GetNameBasedOnLocalization(cloudLocalizer, accountId)}\"";

            if (service.SystemService != Clouds42Service.MyDatabases)
                return paymentDefinition;

            if (!paidInvites.Any())
                return paymentDefinition;

            var groupedServiceTypes = paidInvites
                .GroupBy(inv => inv.BillingServiceTypeId)
                .Select(groupedServiceType => new
                {
                    groupedServiceType.FirstOrDefault()?.ServiceTypeName,
                    Count = groupedServiceType.Count()
                });

            var serviceTypeDefinitions = groupedServiceTypes.Aggregate(string.Empty,
                    (current, next) => $"{current} пользователи {next.ServiceTypeName} +{next.Count};").TrimStart()
                .TrimEnd(';');

            return $"{paymentDefinition} ({serviceTypeDefinitions})";
        }
    }
}
