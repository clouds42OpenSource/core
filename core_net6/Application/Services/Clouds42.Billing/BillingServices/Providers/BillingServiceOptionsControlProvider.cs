﻿using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Управление сервисом
    /// </summary>
    public class BillingServiceOptionsControlProvider(
        IUnitOfWork dbLayer,
        IRateProvider rateProvider,
        BillingServiceTypeRateHelper typeRateHelper,
        IServiceExtensionDatabaseActivationStatusProvider serviceExtensionDatabaseActivationStatusProvider,
        IChangeTardisServiceSubscriptionSynchronizator changeTardisServiceSubscriptionSynchronizator,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider)
    {
        /// <summary>
        /// Сохранить новые данные из управления сервисом
        /// </summary>
        /// <param name="model">Модель данных</param>
        public void SaveNewOptionsControl(BillingServiceOptionsControlDto model)
        {
            using var smartTransaction = dbLayer.SmartTransaction.Get();
            try
            {
                var resourceConfiguration = dbLayer.ResourceConfigurationRepository
                    .FirstOrDefault(x => x.AccountId == model.AccountId && x.BillingServiceId == model.BillingServiceId);

                var oldFrozenValue = resourceConfiguration.FrozenValue;
                resourceConfiguration.ExpireDate = model.NewExpireDate;
                resourceConfiguration.Frozen = model.NewExpireDate <= DateTime.Now;
                dbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);

                if (oldFrozenValue != resourceConfiguration.Frozen)
                    serviceExtensionDatabaseActivationStatusProvider.SetActivationStatus(
                        resourceConfiguration.AccountId, !resourceConfiguration.FrozenValue);

                var rates = typeRateHelper.GetRates(model.BillingServiceId, model.AccountId);
                var newRates = model.NewCosts
                    .Where(w => rates[w.Key] != w.Value)
                    .Select(w => new RateChangeItem { BillingServiceTypeId = w.Key, Cost = w.Value })
                    .ToArray();

                var account = dbLayer.AccountsRepository.FirstOrDefault(w => w.Id == model.AccountId);
                rateProvider.UpdateAccountRates(account.BillingAccount.Id, resourceConfiguration.BillingService, newRates);
                dbLayer.Save();

                var resources = dbLayer.ResourceRepository.WhereLazy(w => w.AccountId == model.AccountId && w.BillingServiceType.ServiceId == model.BillingServiceId).ToList();
                if (oldFrozenValue != resourceConfiguration.Frozen)
                    changeTardisServiceSubscriptionSynchronizator.SynchronizeWithTardis(resources);
                
                resources
                    .GroupJoin(newRates, z => z.BillingServiceTypeId, x => x.BillingServiceTypeId,
                        (resource, rts) => (resource, rts))
                    .ToList()
                    .ForEach(w =>
                    {
                        var newRate = w.rts.FirstOrDefault();
                        if (newRate == null)
                            return;

                        w.resource.Cost = newRate.Cost;
                    });

                dbLayer.BulkUpdate(resources);
                dbLayer.Save();

                var service = dbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == model.BillingServiceId);

                recalculateResourcesConfigurationCostProvider.Recalculate(model.AccountId, service);

                smartTransaction.Commit();
            }
            catch (Exception)
            {
                smartTransaction.Rollback();
                throw;
            }
        }
    }
}
