﻿using System.Data;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Helpers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы пользователей с услугами сервиса
    /// </summary>
    public class BillingServiceTypeAccessProvider(
        IUnitOfWork dbLayer,
        BillingServiceDependencyHelper dependencyHelper,
        BillingServiceTypeRateHelper typeRateHelper,
        IResourcesService resourcesService,
        IServiceUnlocker serviceUnlocker,
        IPromisePaymentProvider promisePaymentProvider,
        IBillingPaymentsProvider billingPaymentsProvider,
        IBillingServiceInfoProvider billingServiceInfoProvider,
        ExpirationDateOfServiceProvider expirationDateOfServiceProvider,
        IBillingServiceTypeRelationHelper billingServiceTypeRelationHelper,
        IResourcesConfigurationDataProvider resourceConfigurationDataProvider,
        IDeleteUnusedResourcesProcessor deleteUnusedResourcesProcessor,
        IBillingPaymentsDataProvider billingPaymentsDataProvider,
        IHandlerException handlerException,
        IAbilityToCreatePaymentProvider abilityToCreatePaymentProvider,
        ICloudServiceAdapter cloudServiceAdapter,
        ILogger42 logger)
    {

        /// <summary>
        /// Получить список опций с типом биллинга по сервису
        /// </summary>
        /// <param name="serviceId">ID сервиса, по которому получить список опций с типом биллинга</param>
        /// <returns>Список опций с типом биллинга</returns>
        public List<ServiceOptionsResultDto> GetServiceOptionsBillingTypeOfService(Guid serviceId)
        {
            return dbLayer.BillingServiceTypeRepository.WhereLazy(w => w.ServiceId == serviceId)
               .Select(item => new ServiceOptionsResultDto
               {
                   BillingType = item.BillingType,
                   ServiceOptionId = item.Id
               }).ToList();
        }


        /// <summary>
        /// Получить ID сервиса по опции сервиса
        /// </summary>
        /// <param name="serviceOptionId">Опция сервиса, для которой получить ID сервиса</param>
        /// <returns>ID сервиса</returns>
        /// <exception cref="NotFoundException">Вызывается, когда опция сервиса не найдена.</exception>
        public Guid GetServiceIdByServiceOptionId(Guid serviceOptionId)
        {
            var found = dbLayer.BillingServiceTypeRepository.WhereLazy(w => w.Id == serviceOptionId)
               .Select(item => item.ServiceId).ToArray();

            return found.Length == 0
                ? throw new NotFoundException($"Опция сервиса \"{serviceOptionId}\" не найдена.")
                : found[0];
        }

        /// <summary>
        /// Получить активные опции услуги
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>       
        /// <param name="billingType">Тип биллинга</param>
        /// <returns>Список активных опций услуги</returns>
        public List<ActiveAccountServiceOptionsResultDto> GetActiveAccountServiceOptionsFor(Guid serviceId, Guid accountId, BillingTypeEnum billingType)
        {
            IQueryable<Resource> accountServiceOptions = billingType switch
            {
                BillingTypeEnum.ForAccount => dbLayer.ResourceRepository.WhereLazy(w =>
                    w.Subject == accountId && w.AccountId == accountId && w.BillingServiceType.ServiceId == serviceId &&
                    w.BillingServiceType.BillingType == billingType),
                BillingTypeEnum.ForAccountUser => dbLayer.ResourceRepository.WhereLazy(w =>
                    w.Subject != null && w.AccountId == accountId && w.BillingServiceType.ServiceId == serviceId &&
                    w.BillingServiceType.BillingType == billingType),
                _ => throw new ArgumentOutOfRangeException(nameof(billingType), "Не поддерживается.")
            };

            return accountServiceOptions.Select(w => new ActiveAccountServiceOptionsResultDto
            {
                ServiceoptionId = w.BillingServiceTypeId,
                Subject = (Guid)w.Subject
            })
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// Получить активные услуги (Услуги на аккаунт)
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>       
        public List<Guid> GetActiveAccountServiceTypes(Guid billingServiceId, Guid accountId)
        {
            var dtos = dbLayer.ResourceRepository
                .WhereLazy(w =>
                    w.Subject == accountId && w.AccountId == accountId &&
                    w.BillingServiceType.ServiceId == billingServiceId)
                .Select(w => w.BillingServiceTypeId)
                .Distinct()
                .ToList();

            return dtos;
        }

        /// <summary>
        /// Расчитать стоимость услуг сервиса для пользователей
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="current">Текущая услуга для пользователя</param>
        /// <param name="dataModels">Полная модель данных</param>
        public List<CalculateBillingServiceTypeResultDto> CalculateBillingServiceType(Guid accountId,
            Guid billingServiceId, CalculateBillingServiceTypeDto current, IEnumerable<CalculateBillingServiceTypeDto> dataModels)
        {
            var invites = dataModels.Select(model =>
                new CalculateBillingServiceTypeResultDto
                {
                    BillingServiceTypeId = model.BillingServiceTypeId,
                    Subject = model.Subject,
                    Status = model.Status,
                    Sponsorship = model.Sponsorship
                }).ToList();

            var service = dbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == billingServiceId);
            if (!service.IsActive)
                throw new InvalidExpressionException("Сервис не активен");
            var account = dbLayer.AccountsRepository.FirstOrDefault(w => w.Id == accountId);

            ApplyStatusesWithDependencies(current, invites, account.Id);
            ApplyCostForActiveServiceTypes(account, service, invites);
            CheckCurrentInvite(current, invites);

            return invites;
        }

        /// <summary>
        /// Расчитать стоимость услуг сервиса Дополнительные сеансы
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="usedLicenses"></param>
        public TryChangeTariffResultDto CalculateDopSessionServiceType(Guid accountId,
            Guid billingServiceId, int usedLicenses)
        {
            var resourceConfiguration = resourcesService.GetResourcesConfigurationOrThrowException(accountId, billingServiceId);
            var oldCost = resourceConfiguration.Cost;
            
            var result = TryChangeTariff(usedLicenses, resourceConfiguration);

            if (string.IsNullOrEmpty(result.Error))
            {
                serviceUnlocker.ProlongServiceIfTheCostIsLess(accountId, oldCost,
                    resourceConfiguration);
            }

            return result;
        }

        /// <summary>
        /// Оплатить сервис сервиса.
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="usePromisePayment">Покупка за счет обещанного платежа.</param>
        /// <param name="costOfTariff">Стоимость которую надо оплатить.</param>
        /// <param name="dopSessionCount"></param>
        public PaymentServiceResultDto PaymentDopSessionService(Guid serviceId, Guid accountId, bool usePromisePayment, decimal costOfTariff,int dopSessionCount)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var resourceConfiguration = resourcesService.GetResourcesConfigurationOrThrowException(accountId, serviceId);
                
                var result = usePromisePayment
                    ? MakePromisePayment(serviceId, accountId, costOfTariff)
                    : MakePayment(serviceId, accountId, costOfTariff);

                if (result.Complete)
                    ApplyAfterPaymentService(resourceConfiguration, accountId, dopSessionCount);

                deleteUnusedResourcesProcessor.Process(resourceConfiguration);

                dbLayer.Save();
                dbScope.Commit();

                return result;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка принятия подписки сервиса] '{serviceId}' аккаунтом '{accountId}'");
                dbScope.Rollback();
                throw;
            }
        }

        public PaymentServiceResultDto PaymentService(Guid serviceId, Guid accountId,
            decimal cost, bool usePromisePayment)
        {
            try
            {
                var service = dbLayer.BillingServiceRepository.FirstOrDefault(x => x.Id == serviceId);
                if (service == null)
                {
                    return new PaymentServiceResultDto
                    {
                        Complete = false,
                        ErrorMessage =
                            $"Не удалось провести платеж на {cost} по причине не найдена услуга для оплаты по идентификатору {serviceId}",
                    };
                }
                var result = usePromisePayment 
                    ? MakePromisePayment(serviceId, accountId, cost, $"Взятие ОП на использование услуги '{service.Name}'") 
                    : MakePayment(serviceId, accountId, cost, $"Оплата за использование услуги '{service.Name}'");
                
                dbLayer.Save();

                return result;
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка принятия подписки сервиса] '{serviceId}' аккаунтом '{accountId}'");

                throw;
            }
        }

        /// <summary>
        /// Провести обещанный платеж.
        /// </summary>
        /// <param name="serviceId">Данные о сервисе.</param>        
        /// <param name="accountId">Номер аккаунта.</param>        
        /// <param name="costOfTariff">Сумма.</param>        
        /// <param name="description">Описание платежа</param>        
        /// <returns>Результат принятия демо периода.</returns>
        private PaymentServiceResultDto MakePromisePayment(Guid serviceId, Guid accountId, decimal costOfTariff, string? description = null)
        {
            var canCreatePaymentResult =
                billingPaymentsProvider.CanCreatePayment(accountId, costOfTariff, out var needMoney);

            if (canCreatePaymentResult == PaymentOperationResult.Ok)
                return MakePayment(serviceId, accountId, costOfTariff, description);

            var paymentPurposeText = string.IsNullOrEmpty(description) ? billingServiceInfoProvider.GetPaymentPurposeText(serviceId, accountId) : description;
            var createPromisePaymentResult = promisePaymentProvider.CreatePromisePayment(accountId,
                new CalculationOfInvoiceRequestModel
                {
                    SuggestedPayment = new SuggestedPaymentModelDto
                    {
                        PaymentSum = needMoney,
                        ServiceTypeContent = paymentPurposeText
                    }
                });

            if (!createPromisePaymentResult.Complete)
                return new PaymentServiceResultDto
                {
                    Complete = false,
                    ErrorMessage = $"Не удалось получить обещанный платеж на сумму {needMoney}"
                };

            return MakePayment(serviceId, accountId, costOfTariff, description);
        }

        /// <summary>
        /// Провести платеж.
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса.</param>        
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="costOfTariff">Стоимость услуги</param>
        /// <param name="description">Описание платежа</param>
        /// <returns>Результат принятия демо периода.</returns>
        private PaymentServiceResultDto MakePayment(Guid serviceId, Guid accountId, decimal costOfTariff, string? description = null)
        {
            var canCreatePaymentResult =
                billingPaymentsProvider.CanCreatePayment(accountId, costOfTariff, out var enoughMoney);

            if (canCreatePaymentResult != PaymentOperationResult.Ok)
            {
                return new PaymentServiceResultDto              {
                    Complete = false,
                    ErrorMessage = $"Не удалось провести платеж на {costOfTariff} по причине {canCreatePaymentResult.GetDisplayName()}",
                    Amount = costOfTariff,
                    NotEnoughMoney = enoughMoney,
                    CanUsePromisePayment = abilityToCreatePaymentProvider.CanUsePromisePayment(accountId)
                }; 
            }

            var createPaymentResult = CreatePaymentTransaction(serviceId, accountId, costOfTariff, description);

            if (createPaymentResult.OperationStatus != PaymentOperationResult.Ok)
                return new PaymentServiceResultDto
                {
                    Complete = false,
                    ErrorMessage = $"Не удалось провести платеж на {costOfTariff} по причине {createPaymentResult.OperationStatus}"
                };

            return new PaymentServiceResultDto { Complete = true}; 
        }

        /// <summary>
        /// Создать платежную транзакцию.
        /// </summary>
        /// <param name="serviceId">Данные о сервисе за которые производится оплата.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="paymentSum">Сумма транзакции.</param>
        /// <param name="description">Сумма транзакции.</param>
        /// <returns>Резльтат создания платежной тразакции.</returns>
        private CreatePaymentResultDto CreatePaymentTransaction(Guid serviceId, Guid accountId,
            decimal paymentSum, string? description = null)
            => billingPaymentsProvider.MakePayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = string.IsNullOrEmpty(description) ? billingServiceInfoProvider.GetPaymentPurposeText(serviceId, accountId) : description,
                BillingServiceId = serviceId,
                Status = PaymentStatus.Done,
                Total = paymentSum,
                OperationType = PaymentType.Outflow,
                OriginDetails = "core-cp"
            });

        public TryChangeTariffResultDto TryChangeTariff(int count, ResourcesConfiguration resourceConfiguration)
        {
            var expireDate = resourceConfigurationDataProvider.GetExpireDateOrThrowException(resourceConfiguration);

            var accountInfo = dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == resourceConfiguration.AccountId);
            if (accountInfo is null)
            {
                return new TryChangeTariffResultDto
                {
                    Complete = false,
                    Error = "Не удалось получить информацию о компании. Обратитесь в техподдержку."
                };
            }
            var currency = accountInfo.AccountConfiguration.Locale.Currency;

            var billingServiceType = dbLayer.BillingServiceTypeRepository
                .FirstOrDefault(s => s.ServiceId == resourceConfiguration.BillingServiceId) ?? throw new NotFoundException($"Cервис '{resourceConfiguration.BillingServiceId}' не существует.");
            
            var maxAvailableResource = dbLayer.SessionResourceRepository
                .OrderByDescending(r => r.SessionCount)
                .FirstOrDefault(r => r.Resource.AccountId == resourceConfiguration.AccountId);

            var rateService = dbLayer.RateRepository.FirstOrDefault(r => r.BillingServiceTypeId == billingServiceType.Id);

            //ежемесячный платеж
            var monthlyCost = count * rateService.Cost;

            if (expireDate < DateTime.Now)
            {
                var mainService = cloudServiceAdapter.GetMainConfigurationOrThrowException(resourceConfiguration);
                if (mainService.Cost > accountInfo.BillingAccount.GetAvailableBalance())
                {
                    return new TryChangeTariffResultDto
                    {
                        Balance = accountInfo.BillingAccount.GetAvailableBalance(),
                        Currency = currency,
                        Complete = false,
                        EnoughMoney = Math.Abs(monthlyCost - accountInfo.BillingAccount.GetAvailableBalance()),
                        CostOftariff = monthlyCost,
                        MonthlyCost = monthlyCost
                    };
                }

                SetTariffPlan(count, monthlyCost, resourceConfiguration.AccountId, billingServiceType.Id);

                resourceConfiguration.Cost = monthlyCost;
                dbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
                dbLayer.Save();

                return new TryChangeTariffResultDto
                {
                    Balance = accountInfo.BillingAccount.GetAvailableBalance(),
                    Currency = currency,
                    Complete = true,
                    EnoughMoney = mainService.Cost + monthlyCost,
                    CostOftariff = monthlyCost,
                    MonthlyCost = monthlyCost
                };

            }
            //стоимость покупки.
            var costOfPay = monthlyCost;
            int freeMaxAvailableSize = 0;
            if (maxAvailableResource != null)
            {
                costOfPay = Math.Max(0, costOfPay - maxAvailableResource.Resource.Cost);
                freeMaxAvailableSize = maxAvailableResource.SessionCount;
            }

            //Стоимость покупки до конца месяца
            var partialCost = decimal.Round(
                BillingServicePartialCostCalculator.CalculateUntilExpireDateWithoutRound(costOfPay, expireDate),
                0,
                MidpointRounding.AwayFromZero);

            if (freeMaxAvailableSize >= count || partialCost <= 0)
            {
                SetTariffPlan(count, monthlyCost, resourceConfiguration.AccountId, billingServiceType.Id);

                resourceConfiguration.Cost = monthlyCost;
                dbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
                dbLayer.Save();

                return new TryChangeTariffResultDto
                {
                    Complete = true
                };
            }

            if (partialCost <= accountInfo.BillingAccount.GetAvailableBalance())
            {
                return new TryChangeTariffResultDto
                {
                    Balance = accountInfo.BillingAccount.GetAvailableBalance(),
                    Currency = currency,
                    Complete = false,
                    EnoughMoney = 0,
                    CostOftariff = partialCost,
                    MonthlyCost = monthlyCost
                };
            }


            return new TryChangeTariffResultDto
            {
                Balance = accountInfo.BillingAccount.GetAvailableBalance(),
                Currency = currency,
                Complete = false,
                CanGetPromisePayment = billingPaymentsDataProvider.CanGetPromisePayment(resourceConfiguration.AccountId),
                CostOftariff = partialCost,
                EnoughMoney = partialCost - accountInfo.BillingAccount.GetAvailableBalance(),
                MonthlyCost = monthlyCost
            };
        }
        private void SetTariffPlan(int dopSessionCount, decimal cost, Guid accountId, Guid serviceTypeId)
        {
            logger.Info($"Изменить тариф на {dopSessionCount} стоимостью {cost} для аккаунта {accountId}");

            var resources = dbLayer.ResourceRepository.Where(x => x.BillingServiceTypeId == serviceTypeId && x.Subject == accountId).ToList();
            logger.Info($"Нашли для аккаунта {accountId} ресурсов в кол-ве {resources.Count}");

            foreach (var resource in resources)
            {
                resource.Subject = null;
                dbLayer.ResourceRepository.Update(resource);
                dbLayer.Save();
            }
            var sessionResources = dbLayer.SessionResourceRepository
                .FirstOrDefault(r => r.Resource.AccountId == accountId && r.SessionCount == dopSessionCount);

            if (sessionResources == null)
            {
                logger.Info($"Для аккаунта не нашли подходящее ресурса с таким кол-вом сессий, создаем новые");
                var newResource = new Resource
                {
                    Id = Guid.NewGuid(),
                    AccountId = accountId,
                    Cost = cost,
                    Subject = accountId,
                    BillingServiceTypeId = serviceTypeId
                };

                dbLayer.ResourceRepository.Insert(newResource);
                dbLayer.Save();

                sessionResources = new SessionResource
                {
                    SessionCount = dopSessionCount,
                    ResourceId = newResource.Id
                };

                dbLayer.SessionResourceRepository.Insert(sessionResources);
                dbLayer.Save();
            }
            else
            {
                logger.Info($"Нашли для аккаунта {accountId} запись в таблице с сессиями в количестве {sessionResources.SessionCount}, заполняем ресурс");
                var resource = dbLayer.ResourceRepository.FirstOrDefault(x => x.Id == sessionResources.ResourceId && x.AccountId == accountId);
                resource.Subject = accountId;
                resource.Cost = cost;

                dbLayer.ResourceRepository.Update(resource);
                dbLayer.Save();
            }
        }

        

        /// <summary>
        /// Начисление ресурсов после покупки сервиса.
        /// </summary>
        /// <param name="resourceConfiguration">Данные о сервисе.</param>        
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="dopSessionCount"></param>
        /// <returns>Результат принятия демо периода.</returns>
        private void ApplyAfterPaymentService(ResourcesConfiguration resourceConfiguration, Guid accountId, int dopSessionCount)
        {
            var billingServiceType = dbLayer.BillingServiceTypeRepository.FirstOrDefault(s => s.ServiceId == resourceConfiguration.BillingServiceId);
            var rateService = dbLayer.RateRepository.FirstOrDefault(r => r.BillingServiceTypeId == billingServiceType.Id);

            //ежемесячный платеж
            var monthlyCost = dopSessionCount * rateService.Cost;

            SetTariffPlan(dopSessionCount, monthlyCost, accountId, billingServiceType.Id);

            resourceConfiguration.Cost = monthlyCost;
            dbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
            dbLayer.Save();
        }



        /// <summary>
        /// Применить статусы к зависимостям
        /// </summary>
        /// <param name="current">Текущая услуга для пользователя</param>
        /// <param name="invites">Модель данных</param>
        /// <param name="accountId">ID аккаунта</param>
        private void ApplyStatusesWithDependencies(CalculateBillingServiceTypeDto current,
            ICollection<CalculateBillingServiceTypeResultDto> invites, Guid accountId)
        {
            var serviceType =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(w => w.Id == current.BillingServiceTypeId);
            var serviceTypeRelations = billingServiceTypeRelationHelper
                .GetAllDependencyRelations(serviceType, accountId)
                .Select(x => x.Id)
                .ToList();

            foreach (var serviceTypeRelation in serviceTypeRelations.Where(serviceTypeRelation => !invites.Any(w => w.BillingServiceTypeId == serviceTypeRelation && w.Subject == current.Subject)))
            {
                invites.Add(new CalculateBillingServiceTypeResultDto
                {
                    Subject = current.Subject,
                    BillingServiceTypeId = serviceTypeRelation,
                    Sponsorship = current.Sponsorship
                });
            }

            var dataModelRelations = invites.Where(w =>
                serviceTypeRelations.Contains(w.BillingServiceTypeId) && w.Subject == current.Subject);
            foreach (var invite in dataModelRelations) invite.Status = false;
        }

        /// <summary>
        /// Применить стоимость ко всем активным инвайтам
        /// </summary>
        /// <param name="service">Cервис</param>
        /// <param name="account">Аккаунт</param>
        /// <param name="invites">Модель данных</param>
        public void ApplyCostForActiveServiceTypes(IAccount account, IBillingService service,
            ICollection<CalculateBillingServiceTypeResultDto> invites)
        {
            var freeResources = GetFreeResources(account.Id, service.Id, invites).ToList();
            var rates = typeRateHelper.GetRates(((Account)account).BillingAccount, invites);
            var billingServiceTypes = GetBillingServiceTypes(invites).ToList();

            var serviceExpireDate = expirationDateOfServiceProvider.GetExpirationDate(service, account.Id);

            foreach (var invite in invites)
            {
                if (!invite.Status)
                    continue;

                var billingServiceType = billingServiceTypes.FirstOrDefault(w => w.Id == invite.BillingServiceTypeId) ??
                                         throw new NotFoundException(
                                             $"Услуга сервиса по ID {invite.BillingServiceTypeId} не найдена");
                var dependencyRelations = billingServiceTypeRelationHelper
                    .GetChildDependencyRelationsWithCurrent(billingServiceType, account.Id)
                    .Select(x => x.Id)
                    .ToList();

                var resources = dependencyRelations.Select(dependencyRelation => freeResources.FirstOrDefault(x => x.BillingServiceTypeId == dependencyRelation && x.Cost == rates[dependencyRelation])).OfType<Resource>().ToList();

                if (resources.Any())
                {
                    var costWithFreeDependedResource = rates.Where(w =>
                            dependencyRelations.Contains(w.Key) && resources.All(x => x.BillingServiceTypeId != w.Key))
                        .Sum(x => x.Value);
                    invite.Cost =
                        BillingServicePartialCostCalculator.CalculateUntilExpireDateWithRoundedUp(
                            costWithFreeDependedResource, serviceExpireDate);
                    resources.ForEach(w => { freeResources.Remove(w); });
                    continue;
                }

                var cost = rates.Where(w => dependencyRelations.Contains(w.Key)).Sum(x => x.Value);
                invite.Cost =
                    BillingServicePartialCostCalculator.CalculateUntilExpireDateWithRoundedUp(cost, serviceExpireDate);
                invite.ServiceTypeName = billingServiceType.Name;
            }
        }

        /// <summary>
        /// Получить все выбранные услуги
        /// </summary>
        /// <param name="invites">Модель данных</param>
        private IEnumerable<BillingServiceType> GetBillingServiceTypes(
            IEnumerable<CalculateBillingServiceTypeResultDto> invites)
        {
            var ids = invites.Select(w => w.BillingServiceTypeId).Distinct();
            return dbLayer.BillingServiceTypeRepository.WhereLazy(w => ids.Contains(w.Id)).ToList();
        }

        /// <summary>
        /// Найти свободные лицензии
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="invites">Модель данных</param>
        /// <returns>Свободные лицензии</returns>
        private IEnumerable<Resource> GetFreeResources(Guid accountId, Guid serviceId,
            IEnumerable<CalculateBillingServiceTypeResultDto> invites)
        {
            var allResources = dbLayer.ResourceRepository.WhereLazy(w =>
                (w.AccountId == accountId || w.AccountSponsorId == accountId) &&
                w.BillingServiceType.ServiceId == serviceId).ToList();

            var buyStopInvites = invites
                .Where(w => allResources.Any(x =>
                    x.Subject == w.Subject && x.BillingServiceTypeId == w.BillingServiceTypeId))
                .ToList();

            var freeResources = allResources
                .Where(w => w.Subject == null || buyStopInvites.Any(x =>
                                x.Subject == w.Subject && x.BillingServiceTypeId == w.BillingServiceTypeId))
                .ToList();

            return freeResources;
        }

        /// <summary>
        /// Проверить текущий инвайт на ошибки
        /// </summary>
        /// <param name="current">Текущая услуга для пользователя</param>
        /// <param name="invites">Модель данных</param>
        private void CheckCurrentInvite(CalculateBillingServiceTypeDto current,
            IEnumerable<CalculateBillingServiceTypeResultDto> invites)
        {
            var invite = invites.FirstOrDefault(w =>
                w.Subject == current.Subject && w.BillingServiceTypeId == current.BillingServiceTypeId);
            if (invite is not { Status: true })
                return;

            invite.Errors = new List<CalculateBillingServiceTypeErrorDto?>
            {
                dependencyHelper.CheckAccountUserIsActivated(current.Subject, current.Sponsorship.I),
                dependencyHelper.CheckAccountUserToRent1CConnection(current.Subject, current.BillingServiceTypeId)
            }.Where(w => w != null);
        }

        /// <summary>
        /// Включить автоподписку для сервиса, после окончания демо периода
        /// </summary>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <param name="serviceId">Для какого сервиса</param>
        public void EnableAutoSubscription(Guid accountId, Guid serviceId)
        {
            var resourceConfiguration = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == accountId && rc.BillingServiceId == serviceId);

            if (resourceConfiguration == null)
            {
                throw new NotFoundException($"Сервис \"{serviceId}\" не активирован для аккаунта \"{accountId}\".");
            }

            resourceConfiguration.IsAutoSubscriptionEnabled = true;

            dbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);

            dbLayer.Save();
        }
    }
}
