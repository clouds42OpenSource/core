﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Domain.Enums;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы со спонсированием сервиса биллинга
    /// </summary>
    public class BillingServiceSponsorshipProvider(
        IUnitOfWork dbLayer,
        ICheckAccountDataProvider checkAccountProvider,
        ICloudLocalizer cloudLocalizer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        ///     Найти внешнего пользователя для спонсирования
        /// </summary>
        /// <param name="model">Email пользователя</param>
        /// <returns>Модель поиска внешнего пользователя для спонсирования</returns>
        public AccountUserBillingServiceTypeDto SearchExternalAccountUserForSponsorship(
            SearchExternalAccountUserForSponsorshipDto model)
        {
            if (string.IsNullOrEmpty(model.Email))
                throw new ArgumentException("Введите email");

            var findAccountUser =
                dbLayer.AccountUsersRepository.FirstOrDefault(x => x.Email.ToLower() == model.Email.Trim().ToLower());

            if (findAccountUser == null)
                throw new NotFoundException($"Не найден пользователь с email {model.Email}");

            ValidateByResourceConfiguration(model.AccountId, model.ServiceId);

            var resourceConfiguration = dbLayer.ResourceConfigurationRepository.FirstOrDefault(x =>
                x.AccountId == model.AccountId && x.BillingServiceId == model.ServiceId);

            var accountUserAdmin = dbLayer.AccountUsersRepository.FirstOrDefault(x => x.AccountId == model.AccountId);
            if (checkAccountProvider.CheckAccountIsDemo(accountUserAdmin.Account))
                throw new ValidateException(
                    "Спонсирование пользователей внешних аккаунтов будет доступно после оплаты сервиса.");

            var findResourceConfiguration = dbLayer.ResourceConfigurationRepository.FirstOrDefault(w =>
                w.AccountId == findAccountUser.AccountId && w.BillingServiceId == model.ServiceId);

            if (findResourceConfiguration == null || findResourceConfiguration.FrozenValue)
                throw new ValidateException(
                    $"Пользователь {model.Email} принадлежит аккаунту, для которого " +
                    $"сервис \"{resourceConfiguration.GetLocalizedServiceName(cloudLocalizer)}\" неактивен.");

            ValidateBySponsoredResources(model, findAccountUser.Id);
            ValidateByHavingActiveLicense(model, findAccountUser.Id);

            if (findAccountUser.AccountId == model.AccountId)
                throw new ValidateException("Можно добавлять пользователей только чужих аккаунтов.");

            var accountLocale = accountConfigurationDataProvider.GetAccountLocale(accountUserAdmin.AccountId);
            var findAccountLocale = accountConfigurationDataProvider.GetAccountLocale(findAccountUser.AccountId);

            if (accountLocale.Name != findAccountLocale.Name)
                throw new ValidateException(
                    $"Допускается спонсирование только пользователей из вашей страны ({accountLocale.Name})");

            return new AccountUserBillingServiceTypeDto
            {
                AccountUserId = findAccountUser.Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = true,
                    Label = findAccountUser.Account.AccountCaption ?? findAccountUser.Account.IndexNumber.ToString()
                },
                AccountUserName =
                    !string.IsNullOrEmpty(findAccountUser.FirstName + findAccountUser.MiddleName +
                                          findAccountUser.LastName)
                        ? $"{findAccountUser.Login} ({$"{findAccountUser.FirstName} {findAccountUser.MiddleName} {findAccountUser.LastName}".Trim()})"
                            .Trim()
                        : findAccountUser.Login
            };
        }

        /// <summary>
        /// Выполнить валидацию по спонсорским ресурсам
        /// Если пользователь уже проспонсирован, то вернет исключение
        /// </summary>
        /// <param name="model">Модель поиска внешнего пользователя для спонсирования</param>
        /// <param name="accountUserId">Id пользователя</param>
        private void ValidateBySponsoredResources(SearchExternalAccountUserForSponsorshipDto model, Guid accountUserId)
        {
            var sponsoredResources = dbLayer.ResourceRepository.WhereLazy(x =>
                x.Subject == accountUserId && x.AccountSponsorId != null &&
                (model.ServiceTypeId == null && x.BillingServiceType.ServiceId == model.ServiceId ||
                 model.ServiceTypeId == x.BillingServiceTypeId)).Any();

            if (sponsoredResources)
                throw new ValidateException($"Пользователь '{model.Email}' уже проспонсирован.");
        }

        /// <summary>
        /// Выполнить валидацию по наличию активной лицензии
        /// </summary>
        /// <param name="model">Модель поиска внешнего пользователя для спонсирования</param>
        /// <param name="accountUserId">Id пользователя</param>
        private void ValidateByHavingActiveLicense(SearchExternalAccountUserForSponsorshipDto model,
            Guid accountUserId)
        {
            var resources = dbLayer.ResourceRepository.WhereLazy(x =>
                x.Subject == accountUserId &&
                (model.ServiceTypeId == null && x.BillingServiceType.ServiceId == model.ServiceId ||
                 model.ServiceTypeId == x.BillingServiceTypeId)).Any();

            if (resources)
                throw new ValidateException($"Для пользователя '{model.Email}' уже подключена услуга.");
        }

        /// <summary>
        /// Проверить на ресурсы
        /// </summary>
        private void ValidateByResourceConfiguration(Guid accountId, Guid serviceId)
        {
            var dependServiceType = dbLayer.BillingServiceTypeRepository
                .WhereLazy(w => w.ServiceId == serviceId && w.DependServiceTypeId.HasValue).FirstOrDefault();

            if (dependServiceType != null)
            {
                var rent1CName = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId);

                var dependResourceConfiguration = dbLayer.ResourceConfigurationRepository.FirstOrDefault(x =>
                    x.AccountId == accountId && x.BillingServiceId == dependServiceType.ServiceId);

                if (dependResourceConfiguration == null || dependResourceConfiguration.FrozenValue)
                    throw new ValidateException(
                        $"Ваш сервис \"{rent1CName}\" заблокирован. Пожалуйста, оплатите сервис \"{rent1CName}\".");

                return;
            }

            var currentResourceConfiguration =
                dbLayer.ResourceConfigurationRepository.FirstOrDefault(x =>
                    x.AccountId == accountId && x.BillingServiceId == serviceId);

            if (currentResourceConfiguration == null || currentResourceConfiguration.FrozenValue)
                throw new ValidateException("Ваш сервис заблокирован. Пожалуйста, оплатите сервис.");
        }
    }
}
