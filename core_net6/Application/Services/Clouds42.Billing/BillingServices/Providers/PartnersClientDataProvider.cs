﻿using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Billing.Billing.ModelProcessors;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using LinqExtensionsNetFramework;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Clouds42.Billing.BillingServices.Providers
{
    public class PartnersClientQuery : ISortedQuery, IPagedQuery
    {
        public string OrderBy { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public PartnersClientFilter Filter { get; set; }
    }

    public class PartnersClientFilter : IHasSpecificSearch, IQueryFilter
    {
        public string? SearchLine { get; set; }
        public string? PartnerSearchLine { get; set; }
        public Guid? PartnerAccountId { get; set; }
        public Guid? ServiceId { get; set; }
        public bool? ServiceIsActiveForClient { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public DateTime? ServiceExpireDateFrom { get; set; }
        public DateTime? ServiceExpireDateTo { get; set; }
        public TypeOfPartnership? TypeOfPartnership { get; set; }
    }

    public static class PartnersClientSpecification
    {
        public static Spec<PartnerClientDto> BySearchLine(string? value)
        {
            var lowerValue = value?.ToLower();

            return new Spec<PartnerClientDto>(x => string.IsNullOrEmpty(lowerValue) ||
                                                   x.AccountCaption.ToLower().Contains(lowerValue) ||
                                                   x.AccountIndexNumber.ToString().ToLower().Contains(lowerValue) ||
                                                   x.Inn.ToLower().Contains(lowerValue));
        }

        public static Spec<PartnerClientDto> ByPartnerSearchLine(string? value)
        {
            var lowerValue = value?.ToLower();

            return new Spec<PartnerClientDto>(x => string.IsNullOrEmpty(lowerValue) ||
                                                   x.PartnerAccountCaption.ToLower().Contains(lowerValue) ||
                                                   x.PartnerAccountIndexNumber.ToString().ToLower().Contains(lowerValue) ||
                                                   x.PartnerInn.ToLower().Contains(lowerValue));
        }

        public static Spec<PartnerClientDto> ByDate(DateTime? from, DateTime? to)
        {
            return new Spec<PartnerClientDto>(x => (!from.HasValue || x.ClientActivationDate >= from) && (!to.HasValue || x.ClientActivationDate <= to));
        }

        public static Spec<PartnerClientDto> ByServiceExpirationDate(DateTime? from, DateTime? to)
        {
            return new Spec<PartnerClientDto>(x => (!from.HasValue || x.ServiceExpireDate >= from) && (!to.HasValue || x.ServiceExpireDate <= to));
        }
    }
    /// <summary>
    /// Провайдер данных клиентов агента.
    /// </summary>
    internal class PartnersClientDataProvider(
        IUnitOfWork dbLayer,
        IAgencyAgreementDataProvider agencyAgreementDataProvider,
        BillingDataDomainModelProcessor billingDataDomainModelProcessor)
        : IPartnersClientDataProvider
    {
        /// <summary>
        /// Получить порцию данных клиентов агента.
        /// </summary>
        /// <param name="filter">Данные фильтра.</param>
        /// <returns>Порция данных клиентов.</returns>
        public PaginationDataResultDto<PartnerClientDto> GetPartnerClients(PartnersClientFilterDto filter)
        {
            var resultList = GetQuery(filter, out var queryFilter)
                .ToPagedList(queryFilter.PageNumber ?? 1, queryFilter.PageSize is null or 0 ? 50 : queryFilter.PageSize ?? 50);

            return new PaginationDataResultDto<PartnerClientDto>(resultList.ToPagedDto().Records, resultList.TotalItemCount, queryFilter.PageSize ?? 50,
                queryFilter.PageNumber ?? 1);
        }

        private IQueryable<PartnerClientDto> GetQuery(PartnersClientFilterDto filter, out PartnersClientQuery queryFilter)
        {
            var actualAgencyAgreement = agencyAgreementDataProvider.GetActualAgencyAgreement();

            queryFilter = new PartnersClientQuery
            {
                OrderBy = string.IsNullOrEmpty(filter.SortFieldName) && !filter.SortType.HasValue
                    ? $"{nameof(PartnerClientDto.ClientActivationDate)}.desc"
                    : $"{filter.SortFieldName}.{filter.SortType.ToString()!.ToLower()}",
                PageNumber = filter.PageNumber is null or 0 ? 1 : filter.PageNumber,
                PageSize = filter.PageSize is null or 0 ? 50 : filter.PageSize,
                Filter = new PartnersClientFilter
                {
                    From = filter.PeriodFrom,
                    To = filter.PeriodTo,
                    PartnerAccountId = filter.AccountId,
                    PartnerSearchLine = filter.PartnerAccountData,
                    TypeOfPartnership = filter.TypeOfPartnership,
                    ServiceId = filter.ServiceId,
                    ServiceIsActiveForClient = filter.ServiceIsActiveForClient,
                    SearchLine = filter.AccountData,
                    ServiceExpireDateFrom = filter.PeriodServiceExpireDateFrom,
                    ServiceExpireDateTo = filter.PeriodServiceExpireDateTo
                }
            };

           return GetFirstQuery(actualAgencyAgreement)
                .Union(GetSecondQuery(actualAgencyAgreement))
                .AutoFilter(queryFilter.Filter)
                .AutoSort(queryFilter)
                .Where(PartnersClientSpecification.BySearchLine(queryFilter.Filter.SearchLine) &
                       PartnersClientSpecification.ByDate(queryFilter.Filter.From, queryFilter.Filter.To) &
                       PartnersClientSpecification.ByPartnerSearchLine(queryFilter.Filter.PartnerSearchLine) &
                       PartnersClientSpecification.ByServiceExpirationDate(queryFilter.Filter.ServiceExpireDateFrom,
                           queryFilter.Filter.ServiceExpireDateTo));
        }

        private IQueryable<PartnerClientDto> GetFirstQuery(
            Domain.DataModels.billing.AgencyAgreement actualAgencyAgreement)
        {
            return dbLayer.AccountsRepository
                .AsQueryableNoTracking()
                .Include(x => x.AccountConfiguration)
                .Join(
                    dbLayer.AccountsRepository.AsQueryable().Where(x => x.ReferralAccountID.HasValue),
                    account => account.ReferralAccountID!.Value,
                    refAccount => refAccount.Id,
                    (account, refAccount) => new { Account = account, RefAccount = refAccount }
                )
                .Join(
                    dbLayer.GetGenericRepository<AccountRequisites>().AsQueryable(),
                    combined => combined.RefAccount.Id,
                    requisites => requisites.AccountId,
                    (combined, requisites) => new { combined.Account, combined.RefAccount, Requisites = requisites }
                )
                .Join(
                    dbLayer.ResourceConfigurationRepository.AsQueryable(),
                    combined => combined.Account.Id,
                    resConf => resConf.AccountId,
                    (combined, resConf) => new { combined.Account, combined.RefAccount, combined.Requisites, ResConf = resConf }
                )
                .Join(
                    dbLayer.BillingServiceRepository.AsQueryable(),
                    combined => combined.ResConf.BillingServiceId,
                    billingService => billingService.Id,
                    (combined, billingService) => new { combined.Account, combined.RefAccount, combined.Requisites, combined.ResConf, BillingService = billingService }
                )
                .Join(
                    dbLayer.BillingServiceTypeRepository.AsQueryable(),
                    combined => combined.BillingService.Id,
                    serviceType => serviceType.ServiceId,
                    (combined, serviceType) => new { combined.Account, combined.RefAccount, combined.Requisites, combined.ResConf, combined.BillingService, ServiceType = serviceType }
                )
                .GroupJoin(
                    dbLayer.BillingServiceTypeRepository.AsQueryable().Where(x => x.DependServiceTypeId.HasValue),
                    combined => combined.ServiceType.DependServiceTypeId!.Value,
                    dependedServiceType => dependedServiceType.Id,
                    (combined, dependedServiceType) => new { combined.Account, combined.RefAccount, combined.Requisites, combined.ResConf, combined.BillingService, combined.ServiceType, DependedServiceType = dependedServiceType}
                )
                .SelectMany(x => x.DependedServiceType.DefaultIfEmpty(), (x, type) => new { x.Account, x.BillingService, DependedServiceType  = type, x.Requisites, x.RefAccount, x.ResConf, x.ServiceType } )
                .GroupJoin(
                    dbLayer.ResourceConfigurationRepository.AsQueryable(),
                    combined => new { AccountId = combined.Account.Id, BillingServiceId = combined.DependedServiceType!.ServiceId },
                    depResConf => new { depResConf.AccountId, depResConf.BillingServiceId },
                    (combined, depResConf) => new { combined.Account, combined.RefAccount, combined.Requisites, combined.ResConf, combined.BillingService, combined.ServiceType, combined.DependedServiceType, DepResConf = depResConf}
                )
                .SelectMany(x => x.DepResConf.DefaultIfEmpty(), (x, depResConf) => new { x.Account, x.BillingService, x.Requisites, x.RefAccount, x.ResConf, x.ServiceType, DepResConf = depResConf })
                .Where(x => (x.BillingService.SystemService == Clouds42Service.MyEnterprise || x.BillingService.SystemService == Clouds42Service.MyDisk) &&
                            (x.ResConf.ExpireDate.HasValue || (x.DepResConf != null && x.DepResConf.ExpireDate.HasValue)) && x.Account.AccountConfiguration != null)
                .Select(x => new PartnerClientDto
                {
                    AccountId = x.Account.Id,
                    AccountIndexNumber = x.Account.IndexNumber,
                    AccountCaption = x.Account.AccountCaption,
                    IsVipAccount = x.Account.AccountConfiguration.IsVip,
                    PartnerAccountIndexNumber = x.RefAccount.IndexNumber,
                    PartnerAccountCaption = x.RefAccount.AccountCaption,
                    TypeOfPartnership = TypeOfPartnership.Agent,
                    ServiceName = x.BillingService.Name,
                    ServiceId = x.BillingService.Id,
                    ClientActivationDate = x.ResConf.CreateDate,
                    ServiceIsActiveForClient = x.DepResConf != null && x.DepResConf.Frozen.HasValue ? !x.DepResConf.Frozen.Value : x.ResConf.Frozen.HasValue && !x.ResConf.Frozen.Value,
                    IsDemoPeriod = !x.Account.Payments.Any(p =>
                        p.Status == PaymentStatus.Done.ToString() &&
                        p.Sum > 0 &&
                        p.OperationType == PaymentType.Inflow.ToString()),
                    MonthlyBonus = !x.Account.AccountConfiguration.IsVip && x.BillingService.SystemService == Clouds42Service.MyEnterprise ? x.ResConf.Cost * (actualAgencyAgreement.Rent1CRewardPercent / 100.0m)
                            : x.Account.AccountConfiguration.IsVip &&
                              x.BillingService.SystemService == Clouds42Service.MyEnterprise
                                ? x.ResConf.Cost * (actualAgencyAgreement.Rent1CRewardPercentForVipAccounts / 100.0m)
                                : x.Account.AccountConfiguration.IsVip &&
                                  x.BillingService.SystemService == Clouds42Service.MyDisk
                                    ? x.ResConf.Cost * (actualAgencyAgreement.MyDiskRewardPercentForVipAccounts / 100.0m)
                                    :  x.ResConf.Cost * (actualAgencyAgreement.MyDiskRewardPercent / 100.0m),
                    ServiceExpireDate = x.DepResConf != null && x.DepResConf.ExpireDate.HasValue ? x.DepResConf.ExpireDate.Value : x.ResConf != null && x.ResConf.ExpireDate.HasValue ? x.ResConf.ExpireDate.Value : null,
                    Inn = x.Account.AccountRequisites.Inn,
                    PartnerAccountId = x.RefAccount.Id,
                    PartnerInn = x.Requisites.Inn
                });
        }

        private IQueryable<PartnerClientDto> GetSecondQuery(Domain.DataModels.billing.AgencyAgreement actualAgencyAgreement)
        {

            return dbLayer.BillingServiceRepository
                .AsQueryableNoTracking()
                .Join(dbLayer.AccountsRepository.AsQueryable(),
                    ser => ser.AccountOwnerId,
                    refA => refA.Id,
                    (ser, refA) => new { ser, refA })
                .Join(dbLayer.GetGenericRepository<AccountRequisites>().AsQueryable(),
                    combined => combined.refA.Id,
                    refRequisites => refRequisites.AccountId,
                    (combined, refRequisites) => new { combined.ser, combined.refA, refRequisites })
                .Join(dbLayer.ResourceConfigurationRepository.AsQueryable(),
                    combined => combined.ser.Id,
                    resConf => resConf.BillingServiceId,
                    (combined, resConf) => new { combined.ser, combined.refA, combined.refRequisites, resConf })
                .Join(dbLayer.BillingServiceTypeRepository.AsQueryable(),
                    combined => combined.ser.Id,
                    st => st.ServiceId,
                    (combined, st) => new { combined.ser, combined.refA, combined.refRequisites, combined.resConf, st })
                .Join(dbLayer.BillingAccountRepository.AsQueryable(),
                    combined => combined.resConf.AccountId,
                    ba => ba.Id,
                    (combined, ba) => new { combined.ser, combined.refA, combined.refRequisites, combined.resConf, combined.st, ba })
                .Join(dbLayer.AccountsRepository.AsQueryable(),
                    combined => combined.ba.Id,
                    a => a.Id,
                    (combined, a) => new { combined.ser, combined.refA, combined.refRequisites, combined.resConf, combined.st, combined.ba, a })
                .Join(dbLayer.AccountConfigurationRepository.AsQueryable(),
                    combined => combined.a.Id,
                    accConf => accConf.AccountId,
                    (combined, accConf) => new { combined.ser, combined.refA, combined.refRequisites, combined.resConf, combined.st, combined.ba, combined.a, accConf })
                .Join(dbLayer.GetGenericRepository<AccountRequisites>().AsQueryable(),
                    combined => combined.a.Id,
                    requisites => requisites.AccountId,
                    (combined, requisites) => new { combined.ser, combined.refA, combined.refRequisites, combined.resConf, combined.st, combined.ba, combined.a, combined.accConf, requisites })
                .Join(dbLayer.BillingServiceTypeRepository.AsQueryable(),
                    combined => combined.st.DependServiceTypeId,
                    dst => dst.Id,
                    (combined, dst) => new { combined.ser, combined.refA, combined.refRequisites, combined.resConf, combined.st, combined.ba, combined.a, combined.accConf, combined.requisites, dst })
                .Join(dbLayer.ResourceConfigurationRepository.AsQueryable(),
                    combined => new { combined.dst.ServiceId, combined.a.Id },
                    dResConf => new { ServiceId = dResConf.BillingServiceId, Id = dResConf.AccountId },
                    (combined, dResConf) => new { combined.ser, combined.refA, combined.refRequisites, combined.resConf, combined.st, combined.ba, combined.a, combined.accConf, combined.requisites, combined.dst, dResConf })
                .Where(x => x.ser.SystemService == null)
                .Select(x => new PartnerClientDto
                {
                    AccountId = x.a.Id,
                    AccountIndexNumber = x.a.IndexNumber,
                    AccountCaption = x.a.AccountCaption,
                    IsVipAccount = x.accConf.IsVip,
                    PartnerAccountIndexNumber = x.refA.IndexNumber,
                    PartnerAccountCaption = x.refA.AccountCaption,
                    TypeOfPartnership = TypeOfPartnership.SoftOwner,
                    ServiceName = x.ser.Name,
                    ServiceId = x.ser.Id,
                    ClientActivationDate = x.resConf.CreateDate,
                    ServiceIsActiveForClient = x.resConf.IsDemoPeriod == true
                        ? x.resConf.Frozen == false || x.resConf.Frozen == null
                        : x.dResConf.Frozen == false || x.dResConf.Frozen == null,
                    IsDemoPeriod = x.resConf == null || x.resConf.IsDemoPeriod == true,
                    MonthlyBonus = x.accConf.IsVip == false
                        ? (x.resConf != null ? x.resConf.Cost * (actualAgencyAgreement.ServiceOwnerRewardPercent / 100.0m) : 0)
                        : (x.resConf != null ? x.resConf.Cost * (actualAgencyAgreement.ServiceOwnerRewardPercentForVipAccounts / 100.0m): 0),
                    ServiceExpireDate = (x.resConf != null && x.resConf.IsDemoPeriod == true) || x.st.DependServiceTypeId == null
                        ? x.resConf != null ? x.resConf.ExpireDate : null
                        : x.dResConf.ExpireDate,
                    Inn = x.requisites.Inn,
                    PartnerAccountId = x.ser.AccountOwnerId ?? Guid.Empty,
                    PartnerInn = x.refRequisites.Inn
                });
        }


        /// <summary>
        /// Получить порцию данных клиентов агента с дополнительными месяцами если хватает денег на балансе.
        /// </summary>
        /// <param name="filter">Данные фильтра.</param>
        /// <returns>Порция данных клиентов.</returns>
        public PaginationDataResultDto<PartnerClientDto> GetPartnerClientsConsiderAdditionalMonths(PartnersClientFilterDto filter)
        {
            var partnerClientsChunk = GetPartnerClients(filter).ChunkDataOfPagination;
            foreach (var client in partnerClientsChunk)
            {
                var billingData = billingDataDomainModelProcessor.GetBillingDataForAccount(client.AccountId);
                client.ServiceExpireDate = client.ServiceExpireDate?.AddMonths(GetAdditionalMonths(billingData));
            }
            return new PaginationDataResultDto<PartnerClientDto>(partnerClientsChunk, partnerClientsChunk.Count(), filter.PageSize ?? 50,
                filter.PageNumber ?? 1);
        }

        /// <summary>
        /// Считает колиечство месяцев, которое нужно добавить пользователю в зависимости от регулярного платежа
        /// и текущего баланса.
        /// </summary>
        /// <param name="data">Данные счета аккаунта.</param>
        /// <returns>Значение месяца, которое нужно прибавить к исходной дате окончания. Не превосходит 12.</returns>
        private static int GetAdditionalMonths(BillingDataDto data)
        {
            if (data.AccountIsDemo || data.RegularPayment == 0) return 0;

            int months = (int)decimal.Floor((data.CurrentBalance + data.CurrentBonusBalance) / data.RegularPayment);
            return Math.Min(months, 12);
        }
       
    }
}
