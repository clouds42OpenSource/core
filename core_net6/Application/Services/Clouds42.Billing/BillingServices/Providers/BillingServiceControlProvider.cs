﻿using Clouds42.Billing.BillingServices.Validators;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для управления сервисом
    /// </summary>
    internal class BillingServiceControlProvider(
        IUnitOfWork dbLayer,
        BillingServiceValidator billingServiceValidator,
        IHandlerException handlerException)
        : IBillingServiceControlProvider
    {
        /// <summary>
        /// Выполнить управление
        /// </summary>
        /// <param name="manageBillingServiceDto">Модель управления сервисом</param>
        public void Manage(ManageBillingServiceDto manageBillingServiceDto)
        {
            var billingService =
                dbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == manageBillingServiceDto.Id)
                ?? throw new NotFoundException($"Сервис биллинга по ID {manageBillingServiceDto.Id} не найден");

            if (billingService.BillingServiceStatus != BillingServiceStatusEnum.Draft &&
                manageBillingServiceDto.BillingServiceStatus == BillingServiceStatusEnum.Draft)
                throw new InvalidOperationException(
                    $"Нельзя установить статус черновик для сервиса ${billingService.Id}");

            ValidateInternalCloudService(billingService.Id, manageBillingServiceDto.InternalCloudService);

            try
            {
                billingService.ServiceActivationDate = GetServiceActivationDate(
                    billingService.BillingServiceStatus, manageBillingServiceDto.BillingServiceStatus,
                    billingService.ServiceActivationDate);

                if (billingService.BillingServiceStatus != manageBillingServiceDto.BillingServiceStatus)
                {
                    billingService.StatusDateTime = DateTime.Now;
                    billingService.BillingServiceStatus = manageBillingServiceDto.BillingServiceStatus;
                }

                billingService.InternalCloudService = manageBillingServiceDto.InternalCloudService;

                dbLayer.BillingServiceRepository.Update(billingService);
                dbLayer.Save();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования сервиса] {manageBillingServiceDto.Id} {billingService.Name}");
                throw;
            }
        }

        /// <summary>
        /// Получить дату активации сервиса
        /// </summary>
        /// <param name="currentBillingServiceStatus">Текущий статус сервиса</param>
        /// <param name="newBillingServiceStatus">Новый статус сервиса</param>
        /// <param name="currentServiceActivationDate">Текущая дата активации сервиса</param>
        /// <returns>Дата активации сервиса</returns>
        private static DateTime? GetServiceActivationDate(BillingServiceStatusEnum currentBillingServiceStatus,
            BillingServiceStatusEnum newBillingServiceStatus, DateTime? currentServiceActivationDate)
        {
            if (currentBillingServiceStatus == newBillingServiceStatus)
                return currentServiceActivationDate;

            if (newBillingServiceStatus == BillingServiceStatusEnum.IsActive)
                return DateTime.Now;

            return null;
        }

        /// <summary>
        /// Проверить внутренний облачный сервис
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="internalCloudService">Внтуренний облачный сервис</param>
        private void ValidateInternalCloudService(Guid billingServiceId, InternalCloudServiceEnum? internalCloudService)
        {
            var validationResult =
                billingServiceValidator.CheckInternalCloudService(billingServiceId,
                    internalCloudService);

            if (!validationResult.Success)
                throw new InvalidOperationException(validationResult.Message);
        }
    }
}
