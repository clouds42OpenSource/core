﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.RecalculateServiceCost;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.DataModels.History;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Services;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для уведомления
    /// о скором изменении стоимости сервиса
    /// </summary>
    internal class NotifyBeforeChangeServiceCostProvider(
        IChangeServiceRequestProvider changeServiceRequestProvider,
        ServiceTypeHelperForBillingServiceOperations serviceTypeHelper,
        IUnitOfWork dbLayer,
        NotifyBeforeServiceChangesHelper notifyBeforeServiceChangesHelper,
        IAccountDataProvider accountDataProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        IBillingServiceOwnerDataProvider billingServiceOwnerDataProvider,
        ILogger42 logger)
        : INotifyBeforeChangeServiceCostProvider
    {
        /// <summary>
        /// Уведомить аккаунтов о скором
        /// изменении стоимости сервиса
        /// </summary>
        /// <param name="billingServiceChangesId">Id изменений сервиса</param>
        /// <param name="accountIds">Список Id аккаунтов для которых
        /// нужно сделать уведомление</param>
        /// <param name="serviceCostChangeDate">Дата изменения стоимости сервиса</param>
        public void NotifyAccounts(Guid billingServiceChangesId, List<Guid> accountIds, DateTime serviceCostChangeDate)
        {
            if (!accountIds.Any())
                throw new InvalidOperationException(
                    $"Список аккаунтов для уведомления об именении стоимости сервиса {billingServiceChangesId} пуст.");

            var editServiceRequest =
                changeServiceRequestProvider.GetEditServiceRequest(billingServiceChangesId);

            var serviceTypeCostChanges = GetServiceTypeCostChanges(editServiceRequest);

            if (!serviceTypeCostChanges.Any())
                throw new InvalidOperationException(
                    $"Нет услуг с имененной стоимостью для сервиса {editServiceRequest.BillingServiceId}");

            var urlForOpenBillingServiceCard =
                notifyBeforeServiceChangesHelper.GetUrlForOpenBillingServicePage(editServiceRequest.BillingServiceId);

            var accountAdminOwnerOfService =
                billingServiceOwnerDataProvider.GetAccountAdminOwnerOfService(editServiceRequest.BillingService);


            accountIds.ForEach(accountId =>
            {
                logger.Info(
                    $"Уведомление аккаунта {accountId} о скором изменении стоимости сервиса {editServiceRequest.BillingService.Name}");

                var accountInfoForNotify = accountDataProvider.GetAccountInfoForNotifyDto(accountId);

                var sendStatus = letterNotificationProcessor
                    .TryNotify<BeforeChangeServiceCostLetterNotification, BeforeChangeServiceCostLetterModelDto>(
                        new BeforeChangeServiceCostLetterModelDto
                        {
                            AccountId = accountId,
                            BillingServiceChangesId = billingServiceChangesId,
                            ServiceName = editServiceRequest.BillingService.Name,
                            EmailsToCopy = accountInfoForNotify.Emails,
                            AccountUserId = accountInfoForNotify.AccountAdmin.Id,
                            UrlForOpenBillingServiceCard = urlForOpenBillingServiceCard,
                            ServiceTypeCostChanges = serviceTypeCostChanges,
                            ServiceCostChangeDate = serviceCostChangeDate,
                            ServiceOwnerPhoneNumber = accountAdminOwnerOfService.PhoneNumber,
                            ServiceOwnerEmail = accountAdminOwnerOfService.Email
                        });

                logger.Info(
                    $"Отправили ли сообщение клиенту - {sendStatus}, Аккаунт - {accountId}, Email - {accountInfoForNotify.AccountAdmin.Email}");
            });
        }

        /// <summary>
        /// Получить список изменений стоимости услуг
        /// </summary>
        /// <param name="editServiceRequest">Заявка на редактирование сервиса</param>
        /// <returns>Cписок изменений стоимости услуг</returns>
        private List<ServiceTypeCostChangedDto> GetServiceTypeCostChanges(EditServiceRequest editServiceRequest) =>
            serviceTypeHelper
            .JoinServiceTypesWithItsChanges(editServiceRequest.BillingService.BillingServiceTypes,
                editServiceRequest.BillingServiceChanges.BillingServiceTypeChanges)
            .Where(str => str.Value.Cost != null).Select(str => new ServiceTypeCostChangedDto
            {
                Name = str.Key.Name,
                NewCost = str.Value.Cost.Value,
                OldCost = GetOldServiceTypeCost(str.Value.Id)
            }).ToList();


        /// <summary>
        /// Получить старую стоимость услуги
        /// </summary>
        /// <param name="billingServiceTypeChangeId">Id измененной услуги</param>
        /// <returns>Старая стоимость услуги</returns>
        private decimal GetOldServiceTypeCost(Guid billingServiceTypeChangeId) =>
            dbLayer.GetGenericRepository<BillingServiceTypeCostChangedHistory>()
                .FirstOrDefault(sth => sth.BillingServiceTypeChangeId == billingServiceTypeChangeId)?.OldCost ??
            throw new NotFoundException(
                $"Не удалось получить старую стоимость измененной услуги {billingServiceTypeChangeId}");
    }
}
