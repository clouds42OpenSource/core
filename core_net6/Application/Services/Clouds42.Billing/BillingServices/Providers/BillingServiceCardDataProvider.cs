﻿using Clouds42.Configurations.Configurations;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы с данными карточки сервиса
    /// </summary>
    internal class BillingServiceCardDataProvider(
        IUnitOfWork dbLayer,
        IBillingServiceOwnerDataProvider billingServiceOwnerDataProvider,
        IRateDataProvider rateDataProvider)
        : IBillingServiceCardDataProvider
    {
        private readonly Lazy<string> _cpSite = new(CloudConfigurationProvider.Cp.GetSiteAuthorityUrl);
        private readonly Lazy<int> _beforeNextEditServiceCostMonthsCount = new(CloudConfigurationProvider.BillingServices
            .GetBeforeNextPossibleEditServiceCostMonthsCount);
        private readonly Lazy<int> _beforeDeletingServiceTypesMonthsCount = new(CloudConfigurationProvider.BillingServices
            .GetBeforeDeletingServiceTypesMonthsCount);
        private IBillingServiceOwnerDataProvider _billingServiceOwnerDataProvider = billingServiceOwnerDataProvider;


        /// <summary>
        /// Получить данные карточки сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Модель карточки сервиса биллинга</returns>
        public BillingServiceCardDto GetData(Guid serviceId)
        {
            var billingService = GetBillingService(serviceId);

            if (billingService.AccountOwnerId == null)
                throw new InvalidOperationException($"У сервиса {serviceId} не определен владелец");

            var billingServiceCardDto = new BillingServiceCardDto();
            InitBillingServiceCardDto(billingServiceCardDto, billingService);

            return billingServiceCardDto;
        }

        /// <summary>
        /// Попытаться получить дату следующего доступного
        /// изменения стоимости сервиса
        /// Если у сервиса стоимость не менялась то вернет Null
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Дата следующего доступного
        /// изменения стоимости сервиса</returns>
        public DateTime? TryGetNextPossibleEditServiceCostDate(Guid serviceId)
        {
            var lastUpdateCostService = (from s in dbLayer.BillingServiceRepository.WhereLazy(s => s.Id == serviceId)
                                         join esr in dbLayer.GetGenericRepository<EditServiceRequest>()
                                             .WhereLazy(esr => esr.Status == ChangeRequestStatusEnum.Implemented) on s.Id equals esr
                                             .BillingServiceId
                                         join bsc in dbLayer.GetGenericRepository<BillingServiceChanges>()
                                             .WhereLazy() on esr.BillingServiceChangesId equals bsc.Id
                                         join stc in dbLayer.GetGenericRepository<BillingServiceTypeChange>()
                                                 .WhereLazy(stc => stc.Cost != null && !stc.IsNew)
                                             on bsc.Id equals stc.BillingServiceChangesId
                                         orderby esr.CreationDateTime descending
                                         select (DateTime?)esr.CreationDateTime).FirstOrDefault();

            return lastUpdateCostService?.AddMonths(_beforeNextEditServiceCostMonthsCount.Value);
        }

        /// <summary>
        /// Проверить что существует заявка на изменение/создание сервиса биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Результат проверки</returns>
        public bool IsExistsChangeServiceRequest(Guid serviceId) =>
            dbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                csr.BillingServiceId == serviceId) != null;

        /// <summary>
        /// Инициализировать модель карточки сервиса биллинга
        /// </summary>
        /// <param name="billingServiceCardDto">Модель карточки сервиса биллинга</param>
        /// <param name="billingService">Сервис</param>
        private void InitBillingServiceCardDto(BillingServiceCardDto billingServiceCardDto,
            BillingService billingService)
        {
            billingServiceCardDto.Id = billingService.Id;
            billingServiceCardDto.AccountId = billingService.AccountOwnerId;
            billingServiceCardDto.Name = billingService.Name;
            billingServiceCardDto.Key = billingService.Key;
            billingServiceCardDto.BillingServiceStatus = billingService.BillingServiceStatus;
            billingServiceCardDto.InternalCloudService = billingService.InternalCloudService;
            billingServiceCardDto.IsActive = billingService.IsActive;
            billingServiceCardDto.MessageStatingThatServiceIsBlocked =
                GenerateMessageStatingThatServiceIsBlockedHelper.GenerateMessage(billingService.Name);
            billingServiceCardDto.LinkToActivateDemoPeriod = GetLinkToActivateDemoPeriod(billingService);
            billingServiceCardDto.ReferralLinkForMarket = GetReferralLinkForMarket(billingService);
            billingServiceCardDto.BillingServiceTypes = GetBillingServiceTypeDtos(billingService.BillingServiceTypes);
            billingServiceCardDto.AvailabilityChangeServiceRequestOnModeration =
                AvailabilityChangeServiceRequestOnModeration(billingService.Id);
            billingServiceCardDto.IsExistsChangeServiceRequest = IsExistsChangeServiceRequest(billingService.Id);
            billingServiceCardDto.NextPossibleEditServiceCostDate =
                TryGetNextPossibleEditServiceCostDate(billingService.Id);
            billingServiceCardDto.BeforeNextEditServiceCostMonthsCount =
                _beforeNextEditServiceCostMonthsCount.Value;
            billingServiceCardDto.BeforeDeletingServiceTypesMonthsCount = _beforeDeletingServiceTypesMonthsCount.Value;
            billingServiceCardDto.ServiceOwnerEmail =
                _billingServiceOwnerDataProvider.GetAccountAdminOwnerOfService(billingService).Email;
            billingServiceCardDto.IsHybridService = billingService.IsHybridService;
        }

        /// <summary>
        /// Получить ссылку для активации демо периода для сервиса
        /// </summary>
        /// <param name="billingService">Сервис</param>
        /// <returns>Ссылка для активации демо периода</returns>
        private string GetLinkToActivateDemoPeriod(BillingService billingService) =>
            new Uri(
                    $"{_cpSite.Value}/signup?ReferralAccountId={billingService.AccountOwnerId}&CloudServiceId={billingService.Key}")
                .AbsoluteUri;

        /// <summary>
        /// Получить реферальную ссылку
        /// активации сервиса для Маркета
        /// </summary>
        /// <param name="billingService">Сервис</param>
        /// <returns>Реферальная ссылка
        /// активации сервиса для Маркета</returns>
        private string GetReferralLinkForMarket(BillingService billingService) =>
            new Uri(
                    $"{_cpSite.Value}/signup?CloudServiceId={billingService.Key}&RegistrationSource={ExternalClient.Market}")
                .AbsoluteUri;

        /// <summary>
        /// Получить модели услуг
        /// </summary>
        /// <param name="billingServiceTypes">Услуги сервиса</param>
        /// <returns>Модели услуг сервиса</returns>
        private List<BillingServiceTypeDto> GetBillingServiceTypeDtos(
            ICollection<BillingServiceType> billingServiceTypes) =>
            billingServiceTypes.Where(st => !st.IsDeleted).Select(serviceType =>
                new BillingServiceTypeDto
                {
                    Id = serviceType.Id,
                    Key = serviceType.Key,
                    DependServiceTypeId = serviceType.DependServiceTypeId,
                    Name = serviceType.Name,
                    Description = serviceType.Description,
                    BillingType = serviceType.BillingType,
                    ServiceTypeCost = rateDataProvider.GetServiceTypeCost(serviceType.Id),
                    ServiceTypeRelations = GetServiceTypeRelations(serviceType),
                    ConnectionToDatabaseType =
                        ServiceTypeHelperBillingServiceData.MapToConnectionToDatabaseType(
                            serviceType.DependServiceType?.SystemServiceType)
                }).OrderByDescending(w => w.ServiceTypeCost).ToList();

        /// <summary>
        /// Получить связи услуги
        /// </summary>
        /// <param name="serviceType">Услуга сервиса</param>
        /// <returns>Связи услуги</returns>
        private static List<Guid> GetServiceTypeRelations(BillingServiceType serviceType) =>
            serviceType.ChildRelations.Select(cr => cr.ChildServiceTypeId).ToList();

        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса биллинга</param>
        /// <returns>Сервис биллинга</returns>
        private BillingService GetBillingService(Guid serviceId) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId) ??
            throw new NotFoundException($"Не удалось найти указанный сервис {serviceId}");

        /// <summary>
        /// Наличие заявки на модерацию для сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Наличие заявки</returns>
        private bool AvailabilityChangeServiceRequestOnModeration(Guid serviceId) =>
            dbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                csr.BillingServiceId == serviceId && csr.Status == ChangeRequestStatusEnum.OnModeration) != null;
    }
}
