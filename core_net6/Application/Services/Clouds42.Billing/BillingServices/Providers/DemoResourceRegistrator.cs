﻿using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Регистратор демо ресурсов.
    /// </summary>
    internal class DemoResourceRegistrator(
        IResourcesService resourcesService,
        BillingServiceTypeRateHelper billingServiceTypeRate,
        ILogger42 logger,
        IHandlerException handlerException)
        : IDemoResourceRegistrator
    {
        /// <summary>
        /// Зарегистрировать демо ресурсы. 
        /// </summary>
        /// <param name="resConfigBillingService">Сервис.</param>
        /// <param name="account">Аккаунт.</param>
        /// <param name="resourceCount">Количество демо ресмурсов.</param>
        public void RegisterDemoResources(BillingService resConfigBillingService, IAccount account, int resourceCount)
        {
            logger.Trace($"Начинаем процесс регистрации демо ресурсов сервиса {resConfigBillingService.Name} для аккаунта {account.IndexNumber}. Количество демо ресурсов: {resourceCount}");

            foreach (var st in resConfigBillingService.GetServiceTypesByBillingType(BillingTypeEnum.ForAccountUser))
                Register(st, account, resourceCount);

            foreach (var st in resConfigBillingService.GetServiceTypesByBillingType(BillingTypeEnum.ForAccount))
                Register(st, account, 1);

            logger.Trace($"Процесс регистрации демо ресурсов сервиса {resConfigBillingService.Name} для аккаунта {account.IndexNumber} завершен");
        }

        /// <summary>
        /// Зарегистрировать демо ресурсы. 
        /// </summary>
        /// <param name="serviceType">Сервис.</param>
        /// <param name="account">Аккаунт.</param>
        /// <param name="freeResourceCount">Кол-во демо лицензий.</param>
        public void Register(IBillingServiceType serviceType, IAccount account, int freeResourceCount)
        {
            try
            {
                var cost = billingServiceTypeRate.GetRateForServiceType(serviceType.Id, account.Id);

                for (var iterator = 0; iterator < freeResourceCount; iterator++)
                {
                    InsertResourceRecord(serviceType, account, cost);
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка создания сервиса с демо реурсами]");
            }

        }

        /// <summary>
        /// Вставить в БД запись о ресурсе.
        /// </summary>
        /// <param name="serviceType">Услуга сервиса.</param>
        /// <param name="account">Аккаунт.</param>
        /// <param name="cost">Стоимость</param>
        private void InsertResourceRecord(IBillingServiceType serviceType, IAccount account, decimal cost)
        {
            resourcesService.CreateResource(
                new Resource
                {
                    AccountId = account.Id,
                    Cost = cost,
                    Subject = null,
                    BillingServiceTypeId = serviceType.Id,
                    Id = Guid.NewGuid(),
                    DemoExpirePeriod = null,
                    IsFree = false,
                    Tag = null,
                });
        }
    }
}
