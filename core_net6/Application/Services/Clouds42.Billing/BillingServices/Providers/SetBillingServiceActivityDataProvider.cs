﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер данных для установки активности сервиса
    /// </summary>
    internal class SetBillingServiceActivityDataProvider(IUnitOfWork dbLayer) : ISetBillingServiceActivityDataProvider
    {
        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        public BillingService GetService(Guid serviceId) =>
            dbLayer.BillingServiceRepository.FirstOrThrowException(s => s.Id == serviceId,
                $"Не удалось получить сервис по Id {serviceId}");

        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        public BillingService GetServiceNoThrowException(Guid serviceId) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId);

        /// <summary>
        /// Получить ресурсы сервиса биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Ресурсы сервиса биллинга</returns>
        public IEnumerable<Resource> GetServiceResources(Guid serviceId) =>
            (from serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy()
             join resource in dbLayer.ResourceRepository.WhereLazy() on serviceType.Id equals resource
                 .BillingServiceTypeId
             where serviceType.ServiceId == serviceId
             select resource).AsEnumerable();

        /// <summary>
        /// Проверить наличие активных конфигураций ресурсов
        /// у сервиса биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Наличие активных конфигураций ресурсов у сервиса</returns>
        public bool CheckAvailabilityActiveResourceConfigurationAtService(Guid serviceId) =>
            (from rent1CResConf in dbLayer.ResourceConfigurationRepository.WhereLazy(rc =>
                    rc.BillingService.SystemService == Clouds42Service.MyEnterprise)
             join resConf in dbLayer.ResourceConfigurationRepository.WhereLazy(resConf => resConf.BillingServiceId == serviceId) on
                 rent1CResConf.AccountId equals resConf.AccountId
             where !resConf.IsDemoPeriod && resConf.Cost > 0 &&
                   (rent1CResConf.Frozen == false || rent1CResConf.ExpireDate > DateTime.Now)
             select resConf).Any();
    }
}
