﻿using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер для работы с заявками
    /// сервиса на модерацию при редактировании
    /// </summary>
    internal class EditServiceRequestProvider(
        IUnitOfWork dbLayer,
        ServiceTypeHelperForBillingServiceOperations serviceTypeHelper,
        IAccessProvider accessProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IEditServiceRequestProvider
    {
        /// <summary>
        /// Создать заявку сервиса на модерацию при редактировании
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        public void Create(EditBillingServiceDto editBillingServiceDto)
        {
            CheckAbilityToCreateRequest(editBillingServiceDto);

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var billingServiceChangesId = CreateBillingServiceChanges(editBillingServiceDto);
                CreateEditServiceRequest(editBillingServiceDto, billingServiceChangesId);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка создания заявки на модерацию сервиса] {editBillingServiceDto.Name}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Проверить возможность создания заявки на модерацию 
        /// </summary>
        /// <param name="editBillingServiceDto">Модель изменения сервиса</param>
        private void CheckAbilityToCreateRequest(EditBillingServiceDto editBillingServiceDto)
        {
            var existChangeRequest =
                dbLayer.GetGenericRepository<ChangeServiceRequest>()
                    .FirstOrDefault(req =>
                        req.BillingServiceId == editBillingServiceDto.Id &&
                        req.Status == ChangeRequestStatusEnum.OnModeration);

            if (existChangeRequest != null)
                throw new InvalidOperationException(
                    "Невозможно создать заявку на модерацию. У вас есть не обработанные заявки");
               
        }

        /// <summary>
        /// Создать изменения сервиса
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        /// <returns>ID изменений сервиса</returns>
        private Guid CreateBillingServiceChanges(EditBillingServiceDto editBillingServiceDto)
        {

            var billingService = GetBillingService(editBillingServiceDto.Id);

            if (billingService.BillingServiceStatus == BillingServiceStatusEnum.Draft 
                && editBillingServiceDto.BillingServiceStatus == BillingServiceStatusEnum.OnModeration)
            {
                logger.Info($"Переводим сервис {billingService.Name} из черновика на модерацию");

                billingService.BillingServiceStatus = editBillingServiceDto.BillingServiceStatus;
                dbLayer.BillingServiceRepository.Update(billingService);
                dbLayer.Save();
            }
            
            var billingServiceChanges = new BillingServiceChanges
            {
                Id = Guid.NewGuid(),
                Name = GetValue(billingService.Name, editBillingServiceDto.Name),
                IsHybridService = editBillingServiceDto.IsHybridService
            };

            dbLayer.GetGenericRepository<BillingServiceChanges>().Insert(billingServiceChanges);
            dbLayer.Save();

            SaveBillingServiceTypeChanges(billingServiceChanges.Id, billingService.BillingServiceTypes.ToList(),
                editBillingServiceDto.BillingServiceTypes);

            dbLayer.Save();

            return billingServiceChanges.Id;
        }

        /// <summary>
        /// Сохранить изменения по услугам сервиса
        /// </summary>
        /// <param name="billingServiceChangesId">Id изменений сервиса</param>
        /// <param name="serviceTypes">Услуги сервиса</param>
        /// <param name="serviceTypeChanges">Измененные услуги сервиса</param>
        private void SaveBillingServiceTypeChanges(Guid billingServiceChangesId, List<BillingServiceType> serviceTypes,
            List<BillingServiceTypeDto> serviceTypeChanges)
        {
            var serviceTypeChangesRelations = serviceTypes.Join(serviceTypeChanges, oldSt => oldSt.Id,
                newSt => newSt.Id,
                (oldSt, newSt) => new
                {
                    ServiceType = oldSt,
                    ServiceTypeChange = newSt
                }).ToList();

            serviceTypeChangesRelations.ForEach(relation =>
                SaveBillingServiceTypeChange(billingServiceChangesId, relation.ServiceType,
                    relation.ServiceTypeChange));

            foreach (var stc in serviceTypeChanges.Where(st => st.IsNew))
                SaveNewServiceTypeForChange(billingServiceChangesId, stc);

        }

        /// <summary>
        /// Сохранить новую услугу в таблицу изменений услуг
        /// </summary>
        /// <param name="billingServiceChangesId">Id изменений сервиса</param>
        /// <param name="serviceType">Новая услуга сервиса</param>
        private void SaveNewServiceTypeForChange(Guid billingServiceChangesId, BillingServiceTypeDto serviceType)
        {
            var billingServiceTypeChange = new BillingServiceTypeChange
            {
                Id = serviceType.Key,
                BillingServiceChangesId = billingServiceChangesId,
                Name = serviceType.Name,
                Description = serviceType.Description,
                Cost = serviceType.ServiceTypeCost,
                IsNew = serviceType.IsNew,
                BillingType = serviceType.BillingType,
                DependServiceTypeId = serviceType.DependServiceTypeId
            };

            dbLayer.GetGenericRepository<BillingServiceTypeChange>().Insert(billingServiceTypeChange);
            dbLayer.Save();
        }

        /// <summary>
        /// Сохранить изменения по услуге сервиса
        /// </summary>
        /// <param name="billingServiceChangesId">Id изменений сервиса</param>
        /// <param name="serviceType">Услуга сервиса</param>
        /// <param name="serviceTypeChange">Изменения по услуге сервиса</param>
        private void SaveBillingServiceTypeChange(Guid billingServiceChangesId, BillingServiceType serviceType,
            BillingServiceTypeDto serviceTypeChange)
        {
            var serviceTypeCost = serviceTypeHelper.GetServiceTypeCost(serviceType.Id);

            var billingServiceTypeChange = new BillingServiceTypeChange
            {
                Id = Guid.NewGuid(),
                BillingServiceChangesId = billingServiceChangesId,
                BillingServiceTypeId = serviceType.Id,
                Name = GetValue(serviceType.Name, serviceTypeChange.Name),
                Description = GetValue(serviceType.Description, serviceTypeChange.Description),
                Cost = serviceTypeCost != serviceTypeChange.ServiceTypeCost
                    ? serviceTypeChange.ServiceTypeCost
                    : (decimal?)null
            };

            dbLayer.GetGenericRepository<BillingServiceTypeChange>().Insert(billingServiceTypeChange);
            dbLayer.Save();
        }

        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <returns>Сервис биллинга</returns>
        private BillingService GetBillingService(Guid billingServiceId)
            => dbLayer.BillingServiceRepository.FirstOrDefault(service => service.Id == billingServiceId) ??
               throw new NotFoundException($"Сервис биллинга по ID {billingServiceId} не найден");

        
        /// <summary>
        /// Создать заявку на модерацию сервиса
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        /// <param name="billingServiceChangesId">ID изменений сервиса</param>
        private void CreateEditServiceRequest(EditBillingServiceDto editBillingServiceDto,
            Guid billingServiceChangesId)
        {
            var editServiceRequest = new EditServiceRequest
            {
                Id = editBillingServiceDto.CreateServiceRequestId,
                BillingServiceId = editBillingServiceDto.Id,
                ChangeRequestType = ChangeRequestTypeEnum.Editing,
                Status = ChangeRequestStatusEnum.OnModeration,
                StatusDateTime = DateTime.Now,
                BillingServiceChangesId = billingServiceChangesId,
            };

            dbLayer.GetGenericRepository<EditServiceRequest>().Insert(editServiceRequest);

            var message = $"Создана заявка на модерацию сервиса \"{editBillingServiceDto.Name}\" " +
                          $"с типом \"{editServiceRequest.ChangeRequestType.Description()}\"";

            if (editBillingServiceDto.AccountId != null)
                LogEventHelper.LogEvent(dbLayer, (Guid)editBillingServiceDto.AccountId, accessProvider,
                    LogActions.EditBillingService, message);

            dbLayer.Save();

            logger.Info(message);

        }

        /// <summary>
        /// Получить значение для записи
        /// </summary>
        /// <param name="oldValue">Старое значение</param>
        /// <param name="newValue">Новое значение</param>
        /// <returns>Значение для записи</returns>
        private static string? GetValue(string oldValue, string newValue)
            => !newValue.Equals(oldValue, StringComparison.InvariantCulture)
                ? newValue
                : null;
    }
}
