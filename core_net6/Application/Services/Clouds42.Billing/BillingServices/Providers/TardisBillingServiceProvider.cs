﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Providers
{
    /// <summary>
    /// Провайдер сервиса биллинга Тардис
    /// </summary>
    internal class TardisBillingServiceProvider(
        IUnitOfWork dbLayer,
        IAcDbAccessHelper acDbAccessHelper)
        : ITardisBillingServiceProvider
    {
        private readonly Lazy<Guid> _tardisAccountDatabaseId = new(CloudConfigurationProvider.Tardis.GetAccountDatabaseId);

        /// <summary>
        /// Изменить доступ к инф. базе тардис
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <param name="updateAcDbAccessActionType">Тип действия</param>
        public void ManageAccessToTardisAccountDatabase(Guid? accountUserId, Guid billingServiceId, UpdateAcDbAccessActionType updateAcDbAccessActionType)
        {
            if (!NeedManageAccess(billingServiceId) || !accountUserId.HasValue)
                return;

            var tardisAccountDatabase = dbLayer.DatabasesRepository.GetAccountDatabase(_tardisAccountDatabaseId.Value)
                                        ?? throw new NotFoundException(
                                            $"Инф. база Тардис по ID {_tardisAccountDatabaseId.Value} не найдена");

            var accountUser = dbLayer.AccountUsersRepository.GetAccountUser(accountUserId.Value)
                              ?? throw new NotFoundException(
                                  $"Пользователь по ID {accountUserId} не найден");

            if (updateAcDbAccessActionType == UpdateAcDbAccessActionType.Grant)
            {
                GrantAccessToTardisAccountDatabase(accountUser, tardisAccountDatabase);
                return;
            }

            acDbAccessHelper.DeleteAccessFromDb(tardisAccountDatabase, accountUser);
        }

        /// <summary>
        /// Выдать доступ к инф. базе Тардис
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="accountDatabase">Инф. база</param>
        private void GrantAccessToTardisAccountDatabase(AccountUser accountUser, Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.AccountId == accountUser.AccountId)
            {
                acDbAccessHelper.GrandInternalAccessToDb(accountDatabase, accountUser);
                return;
            }
            acDbAccessHelper.GrandExternalAccessToDb(accountDatabase, accountUser.Email);
        }

        /// <summary>
        /// Признак необходимости изменять доступ
        /// </summary>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <returns>true - если это сервис Тардис</returns>
        private bool NeedManageAccess(Guid billingServiceId)
        {
            var billingService = dbLayer.BillingServiceRepository
                .FirstOrDefault(service => service.Id == billingServiceId || service.Key == billingServiceId);

            return billingService is { InternalCloudService: InternalCloudServiceEnum.Tardis };
        }
    }
}
