﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.RecalculateServiceCost;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер для работы с данными
    /// по пересчету стоимости сервиса 
    /// </summary>
    public class RecalculationServiceCostDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IRecalculationServiceCostDataProvider recalculationServiceCostDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить данные
        /// по пересчету стоимости сервисов
        /// </summary>
        /// <param name="filter">Фильтр данных</param>
        /// <returns>Данные по пересчету стоимости сервисов</returns>
        public ManagerResult<PaginationDataResultDto<RecalculationServiceCostDataDto>> GetData(RecalculationServiceCostFilterDto filter)
        {
            var message = "Получение данных по пересчету стоимости сервисов";

            try
            {
                logger.Info(message);
                AccessProvider.HasAccess(ObjectAction.BillingService_Edit);
                var recalculationServiceCostData = recalculationServiceCostDataProvider.GetData(filter);
                return Ok(recalculationServiceCostData);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");
                return PreconditionFailed<PaginationDataResultDto<RecalculationServiceCostDataDto>>(ex.Message);
            }
        }
    }
}
