﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер для работы с данными карточки сервиса
    /// </summary>
    public class BillingServiceCardDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IBillingServiceCardDataProvider billingServiceCardDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить данные карточки сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Модель карточки сервиса биллинга</returns>
        public ManagerResult<BillingServiceCardDto> GetData(Guid serviceId)
        {
            var accountId = AccountIdByServiceId(serviceId);
            var message =
                $"Получение карточки сервиса {serviceId} биллинга, для аккаунта {accountId}";

            try
            {
                Logger.Info(message);
                AccessProvider.HasAccess(ObjectAction.BillingService_Edit, () => accountId);
                var billingServiceCardDto = billingServiceCardDataProvider.GetData(serviceId);
                return Ok(billingServiceCardDto);
            }
            catch (AccessDeniedException ex)
            {
                return Forbidden<BillingServiceCardDto>(ex.Message);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");
                return PreconditionFailed<BillingServiceCardDto>(ex.Message);
            }
        }
    }
}
