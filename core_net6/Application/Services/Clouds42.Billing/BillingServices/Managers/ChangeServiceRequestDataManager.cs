﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер для работы с данными заявок
    /// сервиса на модерацию
    /// </summary>
    public class ChangeServiceRequestDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IChangeServiceRequestDataProvider changeServiceRequestDataProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить порцию данных
        /// заявок сервиса на модерацию
        /// </summary>
        /// <param name="filter">Модель фильтра заявок сервиса на модерацию</param>
        /// <returns>Порция данных
        /// заявок сервиса на модерацию</returns>
        public ManagerResult<PaginationDataResultDto<ChangeServiceRequestDataDto>> GetChangeServiceRequests(
            ChangeServiceRequestsFilterDto filter)
        {
            var accountId = filter.ServiceId.HasValue
                ? AccountIdByServiceId(filter.ServiceId.Value)
                : AccessProvider.ContextAccountId;

            var message = $"Получение данных заявок сервиса для аккаунта {accountId} на модерацию";
            try
            {
                logger.Info(message);
                AccessProvider.HasAccess(ObjectAction.ChangeServiceRequest_GetData,
                    () => accountId);

                var data = changeServiceRequestDataProvider.GetChangeServiceRequests(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");

                return PreconditionFailed<PaginationDataResultDto<ChangeServiceRequestDataDto>>(ex.Message);
            }
        }
    }
}
