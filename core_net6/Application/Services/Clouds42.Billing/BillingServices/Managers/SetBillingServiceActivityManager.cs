﻿using Clouds42.Billing.BillingServices.Validators;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер для установки результата аудита версии файла разработки 1С
    /// </summary>
    public class SetBillingServiceActivityManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ISetBillingServiceActivityProvider setBillingServiceActivityProvider,
        IHandlerException handlerException,
        SetBillingServiceActivityValidator setBillingServiceActivityValidator)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Установить активность сервиса
        /// </summary>
        /// <param name="model">Модель для установки активности сервиса биллинга</param>
        public ManagerResult<bool> SetActivity(SetBillingServiceActivityDto model)
        {
            var accountId = AccountIdByServiceId(model.ServiceId) ?? AccessProvider.ContextAccountId;

            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_SetActivity,
                    () => accountId);

                if (!setBillingServiceActivityValidator.TryValidate(model))
                    return Ok(false);

                setBillingServiceActivityProvider.SetActivity(model);
                return Ok(true);
            }
            catch (Exception ex)
            {
                LogEvent(() => accountId, LogActions.EditBillingService,
                    $"Установка активности сервиса завершилась с ошибкой: {ex.Message}");

                handlerException.Handle(ex, "[Ошибка установки активности сервиса]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Отключение сервиса
        /// </summary>
        /// <param name="model">Модель для отключения сервиса биллинга</param>
        public ManagerResult<bool> ShutdownService(SetBillingServiceActivityDto model)
        {
            var accountId = AccountIdByServiceId(model.ServiceId) ?? AccessProvider.ContextAccountId;

            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_SetActivity,
                    () => accountId);

                setBillingServiceActivityProvider.ShutdownService(model);
                return Ok(true);
            }
            catch (Exception ex)
            {
                LogEvent(() => accountId, LogActions.EditBillingService,
                    $"Отключение сервиса завершилась с ошибкой: {ex.Message}");

                handlerException.Handle(ex, "[Ошибка отключения сервиса]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }
    }
}
