﻿using Clouds42.Billing.BillingServices.Validators;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер по созданию сервиса
    /// </summary>
    public class CreateBillingServiceManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICreateBillingServiceProvider createBillingServiceProvider,
        ILogger42 logger,
        BillingServiceValidator billingServiceValidator)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Создать сервис биллинга
        /// </summary>
        /// <param name="createBillingServiceDto">Модель создания сервиса</param>
        /// <returns>Результат сохранения</returns>
        public ManagerResult<Guid> CreateBillingService(CreateBillingServiceDto createBillingServiceDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_Create, () => createBillingServiceDto.AccountId);
                var serviceId = createBillingServiceProvider.Create(createBillingServiceDto);

                var message = $"Создан сервис \"{createBillingServiceDto.Name}\"";
                logger.Info(message);
                LogEvent(() => createBillingServiceDto.AccountId, LogActions.CreateBillingService, message);

                return Ok(serviceId);
            }
            catch (Exception ex)
            {
                var message = $"Создание сервиса {createBillingServiceDto.Name} завершилось ошибкой.";
                LogEvent(() => createBillingServiceDto.AccountId, LogActions.CreateBillingService, $"{message} Описание ошибки: {ex.Message}");

                HandleException(ex, message);
                return PreconditionFailed<Guid>(ex.Message);
            }
        }


        /// <summary>
        /// Проверить модель билинга перед созданием сервиса
        /// </summary>
        /// <param name="createBillingServiceDto">Модель создания сервиса</param>
        /// <returns>Результат валидации</returns>
        public ManagerResult<ValidateResultModelDto> ValidateCreationOfNewBillingService(CreateBillingServiceDto createBillingServiceDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_Create, () => createBillingServiceDto.AccountId);

                logger.Info($"Начало валидации сервиса {createBillingServiceDto.Name} при создании");
                var validateResult = billingServiceValidator.ValidateCreationOfNewBillingService(createBillingServiceDto);

                return Ok(validateResult);
            }
            catch (Exception ex)
            {
                var message = $"Валидации сервиса {createBillingServiceDto.Name} завершилось ошибкой.";
                HandleException(ex, message);
                return PreconditionFailed<ValidateResultModelDto>(ex.Message);
            }
        }

        /// <summary>
        /// Проверить модель билинга перед редактированием сервиса
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        /// <returns>Результат валидации</returns>
        public ManagerResult<ValidateResultModelDto> ValidateEditBillingService(EditBillingServiceDto editBillingServiceDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_Edit, () => editBillingServiceDto.AccountId);

                logger.Info($"Начало валидации сервиса {editBillingServiceDto.Name} при редактировании");
                var validateResult = billingServiceValidator.ValidateEditService(editBillingServiceDto);

                return Ok(validateResult);
            }
            catch (Exception ex)
            {
                var message = $"Валидации сервиса {editBillingServiceDto.Name} завершилось ошибкой.";
                HandleException(ex, message);
                return PreconditionFailed<ValidateResultModelDto>(ex.Message);
            }
        }

        /// <summary>
        /// Обработать исключение
        /// </summary>
        /// <param name="exception">Исключение</param>
        /// <param name="message">Сообщение об ошибке</param>
        private void HandleException(Exception exception, string message)
        {
            if (exception.GetType() == typeof(ValidateException))
            {
                handlerException.HandleWarning(exception.Message, message);
                return;
            }

            handlerException.Handle(exception, $"[Обработка исключений {message}] ");
        }
    }
}
