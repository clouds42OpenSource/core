﻿using Clouds42.Billing.BillingServices.Processors;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер для обработки результата модерации
    /// заявки на изменения сервиса
    /// </summary>
    public class ModerationResultManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IModerationResultProcessor moderationResultProcessor,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Обработать результат модерации заявки
        /// на изменения сервиса
        /// </summary>
        /// <param name="changeServiceRequestModerationResult">Результат модерации заявки</param>
        /// <returns>Результат обработки</returns>
        public ManagerResult ProcessModerationResult(
            ChangeServiceRequestModerationResultDto changeServiceRequestModerationResult)
        {
            var message =
                $"Обработка результата модерации заявки {changeServiceRequestModerationResult.Id} на изменения сервиса";

            try
            {
                logger.Info(message);
                AccessProvider.HasAccess(ObjectAction.ChangeServiceRequest_ProcessModerationResult,
                    () => AccountIdByChangeServiceRequestId(changeServiceRequestModerationResult.Id));

                moderationResultProcessor.Process(changeServiceRequestModerationResult);
                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[ {message}  завершилось ошибкой.]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
