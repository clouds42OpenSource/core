﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер для редактирования сервиса биллинга
    /// </summary>
    public class EditBillingServiceManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IEditBillingServiceProvider editBillingServiceProvider,
        IHandlerException handlerException,
        IEditBillingServiceDraftProvider editBillingServiceDraftProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Изменить сервис биллинга
        /// </summary>
        /// <param name="editBillingServiceDto">Модель изменения сервиса</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult EditBillingService(EditBillingServiceDto editBillingServiceDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_Edit, () => editBillingServiceDto.AccountId);
                editBillingServiceProvider.Edit(editBillingServiceDto);

                var message = $"Сервис \"{editBillingServiceDto.Name}\" отредактирован.";

                logger.Info(message);
                LogEvent(() => editBillingServiceDto.AccountId, LogActions.EditBillingService, message);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"Редактирование сервиса \"{editBillingServiceDto.Name}\" завершилось ошибкой.";
                LogEvent(() => editBillingServiceDto.AccountId, LogActions.EditBillingService, $"{errorMessage} Описание ошибки: {ex.GetFullInfo(false)}");
                HandleException(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать черновик сервиса
        /// </summary>
        /// <param name="editBillingServiceDraftDto">Модель редактирования черновика</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult EditBillingServiceDraft(EditBillingServiceDraftDto editBillingServiceDraftDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_Edit, () => editBillingServiceDraftDto.AccountId);
                editBillingServiceDraftProvider.Edit(editBillingServiceDraftDto);

                var message = $"Черновик сервиса \"{editBillingServiceDraftDto.Name}\" отредактирован.";

                logger.Info(message);
                LogEvent(() => editBillingServiceDraftDto.AccountId, LogActions.EditBillingService, message);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"Редактирование сервиса \"{editBillingServiceDraftDto.Name}\" завершилось ошибкой.";
                LogEvent(() => editBillingServiceDraftDto.AccountId, LogActions.EditBillingService, $"{errorMessage} Описание ошибки: {ex.GetFullInfo(false)}");
                HandleException(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Обработать исключение
        /// </summary>
        /// <param name="exception">Исключение</param>
        /// <param name="message">Сообщение об ошибке</param>
        private void HandleException(Exception exception, string message)
        {
            if (exception.GetType() == typeof(ValidateException))
            {
                handlerException.HandleWarning(exception.Message, message);
                return;
            }

            handlerException.Handle(exception, $"[Обработка исключений {message}] ");
        }
    }
}
