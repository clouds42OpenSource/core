﻿using System.Text;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.BillingServices.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.DataModels;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IndustryDto = Clouds42.DataContracts.BillingService.IndustryDto;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Управление сервисами
    /// </summary>
    public class BillingServiceManager(
        BillingServiceInfoProvider billingServiceInfoProvider,
        BillingServiceTypeAccessProvider billingServiceTypeAccessProvider,
        BillingServiceTypePayProvider billingServiceTypePayProvider,
        IPartnersClientDataProvider partnersClientDataProvider,
        IPartnerTransactionsDataProvider partnerTransactionsDataProvider,
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        BillingServiceOptionsControlProvider billingServiceOptionsControlProvider,
        BillingServiceSponsorshipProvider billingServiceSponsorshipProvider,
        IApplyDemoServiceSubscribeProcessor applyDemoServiceSubscribeProcessor,
        IHandlerException handlerException,
        BillingServiceTypeAccessHelper billingServiceTypeAccessHelper,
        Contracts.Billing.Interfaces.Providers.IBillingServiceDataProvider billingServiceDataProvider,
        BillingServiceTypeRateHelper billingServiceTypeRateHelper, IPromisePaymentProvider promisePaymentProvider, ICloudLocalizer cloudLocalizer,
        ICreateAccountUsersSessionsInMsCommand createAccountUsersSessionsInMsCommand) : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly Lazy<IUserPrincipalDto> _currentUser = new(accessProvider.GetUser);

        /// <summary>
        /// Получить базовую информацию о сервисе
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public ManagerResult<BillingServiceDto> GetBillingService(Guid billingServiceId, Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => accountId);

            try
            {
                var message = $"Получение базовой информации о сервисе {billingServiceId}, для аккаунта {accountId}";
                Logger.Info(message);
                var data = billingServiceInfoProvider.GetBillingService(billingServiceId, accountId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения базовой информации о сервисе] {billingServiceId}, для аккаунта {accountId}");
                return PreconditionFailed<BillingServiceDto>(ex.Message);
            }
        }

        /// <summary>
        /// Попробовать получить ID сервиса по ключу(Id)
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Id сервиса</returns>
        public ManagerResult<Guid> TryGetServiceIdByKey(Guid key)
        {
            var message = $"Получение ID сервиса по ключу({key})";
            try
            {
                Logger.Info(message);
                var data = billingServiceInfoProvider.TryGetServiceIdByKey(key);
                Logger.Info($"{message} завершилось успешно");
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось ошибкой]");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Попробовать получить ID услуги по ключу(Id)
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Id услуги</returns>
        public ManagerResult<Guid> TryGetServiceTypeIdByKey(Guid key)
        {
            var message = $"Получение ID услуги по ключу({key})";
            try
            {
                Logger.Info(message);
                var data = billingServiceInfoProvider.TryGetServiceTypeIdByKey(key);
                Logger.Info($"{message} завершилось успешно");
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось ошибкой]");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Получить информацию о сервисе для его активации
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Информация о сервисе</returns>
        public ManagerResult<BillingServiceActivationInfoDto> GetBillingServiceForActivation(Guid billingServiceId,
            Guid accountId)
        {
            var checkResult = CheckAbilityToActivateService(billingServiceId, accountId);
            if (checkResult.Error)
                return PreconditionFailed<BillingServiceActivationInfoDto>(checkResult.Message);

            AccessProvider.HasAccess(ObjectAction.BillingService_ActivateForExistingAccount, () => accountId);

            try
            {
                var data = billingServiceInfoProvider.GetBillingServiceForActivation(billingServiceId, accountId);
                var message = $"Получение информации о сервисе {billingServiceId} для активации у аккаунта {accountId}";
                Logger.Info(message);
                return Ok(data);
            }
            catch(ValidateException ex)
            {
                Logger.Warn($" Получение информации о сервисе {billingServiceId} для активации у аккаунта {accountId} завершилось ошибкой. :: {ex.Message}");
                return PreconditionFailed<BillingServiceActivationInfoDto>(ex.Message);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения информации о сервисе] {billingServiceId} для активации у аккаунта {accountId}");
                return PreconditionFailed<BillingServiceActivationInfoDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить информацию о услугах сервиса для аккаунта
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public ManagerResult<List<BillingServiceTypeInfoDto>> GetBillingServiceTypesInfo(Guid billingServiceId,
            Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingServiceTypes_GetInfo, () => accountId);

            try
            {
                var message = $"Получение информации о услугах сервиса {billingServiceId} для аккаунта {accountId}";
                Logger.Info(message);
                var data = billingServiceInfoProvider.GetBillingServiceTypesInfo(billingServiceId, accountId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Получение информации о услугах сервиса] {billingServiceId} для аккаунта {accountId} завершилось ошибкой.";
                handlerException.Handle(ex, message);
                return PreconditionFailed<List<BillingServiceTypeInfoDto>>(ex.Message);
            }
        }


        /// <summary>
        /// Получить информацию о услуге сервиса для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public ManagerResult<DopSessionServiceDto?> GetBillingServiceTypeInfo(Guid serviceId,
            Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingServiceTypes_GetInfo, () => accountId);

            try
            {
                var message = $"Получение информации о услуге сервиса {serviceId} для аккаунта {accountId}";
                Logger.Info(message);
                var data = billingServiceInfoProvider.GetBillingServiceInfo(serviceId, accountId);

                return Ok(data);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Ошибка получения услуги сервиса] Получение информации о услуге сервиса {serviceId} для аккаунта {accountId} завершилось ошибкой.";
                handlerException.Handle(ex, message);
                return PreconditionFailed<DopSessionServiceDto?>(ex.Message);
            }
        }

        /// <summary>
        /// Get Rent1C expire date
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ManagerResult<Rent1CExpireDateDto>> GetRent1CExpireDate(Guid accountId)
        {
            var service = Clouds42Service.MyEnterprise;
            try
            {
                var resourcesConfiguration = await dbLayer.ResourceConfigurationRepository.AsQueryable()
                    .Include(res => res.BillingService)
                    .FirstOrDefaultAsync(res => res.AccountId == accountId && res.BillingService.SystemService == service);

                if(resourcesConfiguration == null)
                    return PreconditionFailed<Rent1CExpireDateDto>($"У аккаунта '{accountId}' не активирован сервис '{service}'");


                return Ok(new Rent1CExpireDateDto
                {
                    ExpireDate = resourcesConfiguration.ExpireDate,
                    IsDemo = resourcesConfiguration.IsDemoPeriod
                });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Получение информации о сервисе {service} для аккаунта {accountId} завершилось ошибкой.]");
                return PreconditionFailed<Rent1CExpireDateDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить активные услуги (Услуги на аккаунт)
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public ManagerResult<List<Guid>> GetActiveAccountServiceTypes(Guid billingServiceId, Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingServiceTypes_GetInfo, () => accountId);

            try
            {
                var message = $"Получение активных услуг сервиса {billingServiceId} для аккаунта {accountId}";
                Logger.Info(message);

                var data = billingServiceTypeAccessProvider.GetActiveAccountServiceTypes(billingServiceId, accountId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения активных услуг сервиса] {billingServiceId} для аккаунта {accountId}");
                return PreconditionFailed<List<Guid>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить активные опции услуги
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="billingType">Тип биллинга</param>
        /// <returns>Список активных опций услуги</returns>
        public ManagerResult<List<ActiveAccountServiceOptionsResultDto>> GetActiveAccountServiceOptionsFor(Guid serviceId, Guid accountId, BillingTypeEnum billingType)
        {
            AccessProvider.HasAccess(ObjectAction.BillingServiceTypes_GetInfo, () => accountId);

            try
            {
                var message = $"Получение активных опций сервиса {serviceId} для аккаунта {accountId}";
                Logger.Info(message);

                var data = billingServiceTypeAccessProvider.GetActiveAccountServiceOptionsFor(serviceId, accountId, billingType);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения активных опций сервиса] {serviceId} для аккаунта {accountId}");
                return PreconditionFailed<List<ActiveAccountServiceOptionsResultDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Расчитать стоимость услуг сервиса для пользователей
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="current">Текущая услуга для пользователя</param>
        /// <param name="dataModels">Полная модель данных</param>
        public ManagerResult<List<CalculateBillingServiceTypeResultDto>> CalculateBillingServiceType(Guid accountId,
            Guid billingServiceId,
            CalculateBillingServiceTypeDto current, IEnumerable<CalculateBillingServiceTypeDto> dataModels)
        {
            AccessProvider.HasAccess(ObjectAction.BillingServiceTypes_CalculateCost, () => accountId);

            try
            {
                var message =
                    $"Калькуляция стоимости услуг сервиса {billingServiceId} для пользователей аккаунта {accountId}";
                Logger.Info(message);

                var data = billingServiceTypeAccessProvider.CalculateBillingServiceType(accountId, billingServiceId,
                    current, dataModels);
                return Ok(data);
            }
            catch (Exception ex)
            {
                var message = "[Калькуляция стоимости услуг сервиса для пользователей аккаунта завершилось ошибкой.]";
                handlerException.Handle(ex, message);

                return PreconditionFailed<List<CalculateBillingServiceTypeResultDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Расчитать стоимость услуги дополнительные сеансы
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="dopSessionCount"></param>
        /// <param name="userSessions"></param>
        public ManagerResult<TryChangeTariffResultDto> CalculateDopSessionServiceType(Guid accountId,
            Guid billingServiceId, int dopSessionCount, List<AccountUserSession>? userSessions = null)
        {
            AccessProvider.HasAccess(ObjectAction.BillingServiceTypes_CalculateCost, () => accountId);

            try
            {
                Logger.Info($"Калькуляция стоимости услуг сервиса {billingServiceId} для пользователей аккаунта {accountId}");

                var sessionResources = dbLayer.SessionResourceRepository
                    .OrderByDescending(x => x.SessionCount)
                    .FirstOrDefault(r => r.Resource.AccountId == accountId && r.Resource.Subject == accountId);

                var usedLicenses = dopSessionCount + (userSessions?.Sum(x => x.Sessions) ?? 0);
                var data = billingServiceTypeAccessProvider.CalculateDopSessionServiceType(accountId, billingServiceId, usedLicenses);

                if (!data.Complete)
                {
                    return Ok(data);
                }

                createAccountUsersSessionsInMsCommand.CreateAccountUsersSessionsInMs(dopSessionCount, userSessions, accountId);
                LogEvent(() => accountId, LogActions.EditResourceCost,
                    $"Количество дополнительных сеансов изменено с {sessionResources?.SessionCount ?? 0} на {usedLicenses}.");

                return Ok(data);
            }
            catch (Exception ex)
            {
                const string message = "[Калькуляция стоимости услуг сервиса для пользователей аккаунта завершилось ошибкой.]";
                handlerException.Handle(ex, message);

                return PreconditionFailed<TryChangeTariffResultDto>(ex.Message);
            }
        }


        /// <summary>
        /// Оплатить сервис
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Покупка за счет обещанного платежа.</param>
        /// <param name="costOfTariff"></param>
        /// <param name="dopSessionCount"></param>
        /// <param name="userSessions"></param>
        public ManagerResult<PaymentServiceResultDto> PaymentDopSessionService(Guid serviceId,
            Guid accountId, bool isPromisePayment, decimal costOfTariff, int dopSessionCount, List<AccountUserSession>? userSessions = null)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_ChangeSubscribe, () => accountId);

            var service = billingServiceInfoProvider.GetBillingServiceInfo(serviceId, accountId);

            if (service == null)
                return PreconditionFailed<PaymentServiceResultDto>($"Не найден сервис по Id: {serviceId}");

            var sessionResources = dbLayer.SessionResourceRepository
                .AsQueryableNoTracking()
                .OrderByDescending(x => x.SessionCount)
                .FirstOrDefault(r => r.Resource.AccountId == accountId && r.Resource.Subject == accountId);


            var existsCount = sessionResources?.SessionCount ?? 0;

            var sumCount = dopSessionCount + (userSessions?.Sum(x => x.Sessions) ?? 0);

            int sessionDifferent = existsCount >= sumCount ? 0 : sumCount - existsCount;
            var sumSessions = existsCount + sessionDifferent;
            try
            {
                var result = billingServiceTypeAccessProvider.PaymentDopSessionService(serviceId, accountId, isPromisePayment, costOfTariff, sumCount);

                if (!result.Complete)
                {
                    var description = $"Ошибка покупки сервиса {service.Name} в количестве {sessionDifferent} сессий," +
                   $" на сумму {costOfTariff}. Причина:{result.ErrorMessage}";

                    Logger.Info(description);

                    LogEvent(() => accountId, LogActions.BuyDopSession, description);
                    return Ok(result);
                }
                createAccountUsersSessionsInMsCommand.CreateAccountUsersSessionsInMs(dopSessionCount, userSessions, accountId);

                var confAccount = dbLayer.AccountConfigurationRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.Locale)
                    .FirstOrDefault(a=> a.AccountId == accountId);

                var logEventDescription = $"Подтверждение покупки сервиса {service.Name} в количестве {sessionDifferent} сессий,"+
                    $" на сумму {costOfTariff} {confAccount!.Locale.Currency}. Общее количество {sumSessions} сессий после покупки";

                Logger.Info(logEventDescription);

                LogEvent(() => accountId, LogActions.BuyDopSession, logEventDescription);
                
                return Ok(result);
            }
            catch (Exception ex)
            {
                var message =
                    $"Оплата сервиса \"{service.Name}\" в количестве {sessionDifferent} сессий, завершилось ошибкой.";

                handlerException.Handle(ex, $"[Ошибка оплаты сервиса] {message}");

                LogEvent(() => accountId, LogActions.BuyDopSession,
                    $"{message} Описание ошибки: {ex.Message}");

                return PreconditionFailed<PaymentServiceResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Применить все изменения указанные для аккаунта и пользователей
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="isPromisePayment">Обещанный платеж</param>
        /// <param name="dataAccount">Услуги на подключение к аккаунту</param>
        /// <param name="dataAccountUsers">Услуги на подключение к пользователям</param>
        public ManagerResult<BillingServiceTypeApplyOrPayResultDto> ApplyOrPayForAllChangesAccountServiceTypes(
            Guid accountId, Guid billingServiceId, bool isPromisePayment,
            ICollection<CalculateBillingServiceTypeDto> dataAccount,
            ICollection<CalculateBillingServiceTypeDto> dataAccountUsers)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_ChangeSubscribe, () => accountId);

                var dto = billingServiceTypePayProvider.ApplyOrPayForAllChangesAccountServiceTypes(accountId,
                    billingServiceId, isPromisePayment, dataAccount, dataAccountUsers);

                WriteSubscriptionChangesToLog(accountId, billingServiceId, dataAccountUsers, dto);

                return Ok(dto);
            }
            catch (Exception ex)
            {
                var message =
                    $"Неуспешное изменение подписки сервиса '{billingServiceId}'.";
                handlerException.Handle(ex, $"[Ошибка изменение подписки сервиса] '{billingServiceId}'.");
                LogEvent(() => accountId, LogActions.ApplyOrPayForAllChangesAccountServiceTypes,
                    $"{message} Причина: {ex.Message}");
                return PreconditionFailed<BillingServiceTypeApplyOrPayResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить клиентов партнера
        /// </summary>
        /// <param name="filter">Данные фильтра</param>
        /// <returns>Клиенты партнера</returns>
        public ManagerResult<PaginationDataResultDto<PartnerClientDto>> GetPartnerClients(
            PartnersClientFilterDto filter)
        {
            AccessProvider.HasAccess(ObjectAction.Partner_GetClients, () => filter.AccountId);
            try
            {
                var message = $"Получение клиентов партнера {filter.AccountId}";
                Logger.Info(message);
                var data = partnersClientDataProvider.GetPartnerClients(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения клиентов партнера] {filter.AccountId}");
                return PreconditionFailed<PaginationDataResultDto<PartnerClientDto>>(ex.Message);
            }
        }
        /// <summary>
        /// Получить клиентов партнера с запасом месяцев, которые могут быть оплачены если хватает денег
        /// </summary>
        /// <param name="filter">Данные фильтра</param>
        /// <returns>Клиенты партнера</returns>
        public ManagerResult<PaginationDataResultDto<PartnerClientDto>> GetPartnerClientsConsiderAdditionalMonths(
            PartnersClientFilterDto filter)
        {
            AccessProvider.HasAccess(ObjectAction.Partner_GetClients, () => filter.AccountId);
            try
            {
                var message = $"Получение клиентов партнера учитывая дополнительные месяцы {filter.AccountId}";
                Logger.Info(message);
                var data = partnersClientDataProvider.GetPartnerClientsConsiderAdditionalMonths(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения клиентов партнера] {filter.AccountId}");
                return PreconditionFailed<PaginationDataResultDto<PartnerClientDto>>(ex.Message);
            }
        }
        
        

        /// <summary>
        /// Проверить возможность активации сервиса пользователем
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Результат проверки</returns>
        public ManagerResult CheckAbilityToActivateService(Guid serviceId, Guid? accountId)
        {
            var service = billingServiceDataProvider.GetBillingServiceOrThrowException(serviceId);
            var serviceName = service.GetNameBasedOnLocalization(cloudLocalizer, accountId ?? AccessProvider.ContextAccountId);

            if (!accountId.HasValue)
                return PreconditionFailed(
                    $"Для подключения сервиса “{serviceName}” необходимо обратиться к администратору аккаунта");

            var accountAdmin = GetAccountAdminFirstOrDefault(accountId.Value);
            var errorMessage =
                $@"Для подключения сервиса “{serviceName}” необходимо обратиться к администратору аккаунта: </br>
                        ФИО: {accountAdmin?.LastName} {accountAdmin?.FirstName} {accountAdmin?.MiddleName};</br>
                        Тел.: {accountAdmin?.PhoneNumber};</br>
                        E-mail: {accountAdmin?.Email}.";

            if (_currentUser.Value != null && _currentUser.Value.IsAccountUser())
                return PreconditionFailed(errorMessage);

            if (!service.IsActive)
                return PreconditionFailed(
                    GenerateMessageStatingThatServiceIsBlockedHelper.GenerateMessage(serviceName));

            return Ok();
        }

        /// <summary>
        /// Получить транзакции партнера
        /// </summary>
        /// <param name="filter">Данные фильтра</param>
        /// <returns>Транзакции партнера</returns>
        public ManagerResult<PaginationDataResultDto<PartnerTransactionDto>> GetPartnerTransactions(
            PartnerTransactionsFilterDto filter)
        {
            AccessProvider.HasAccess(ObjectAction.Partner_GetTransactions, () => filter.AccountId);

            try
            {
                var message = $"Получение транзакций партнера {filter.AccountId}";
                Logger.Info(message);
                var data = partnerTransactionsDataProvider.GetPartnerTransactions(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения транзакций партнера] {filter.AccountId}");
                return PreconditionFailed<PaginationDataResultDto<PartnerTransactionDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить типы подключения к информационной базе 1С
        /// </summary>
        /// <returns>Подключения к инф. базе</returns>
        public ManagerResult<TypesOfConnectionToInformationDbDto> GetTypesOfConnectionToInformationDb()
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_GetReferenceData);

            try
            {
                var message = "Получение типов подключения к информационной базе 1С";
                Logger.Info(message);
                var data = billingServiceInfoProvider.GetTypesOfConnectionToInformationDb();
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения типов подключения к информационной базе 1С]");
                return PreconditionFailed<TypesOfConnectionToInformationDbDto>(ex.Message);
            }
        }

        /// <summary>
        ///     Получить все конфигураций 1С на разделителях
        /// </summary>
        /// <returns>Конфигурации 1С</returns>
        public ManagerResult<List<Configuration1COnDelimitersDto>> GetAllConfigurations1COnDelimiters()
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_GetReferenceData);

            try
            {
                var data = billingServiceInfoProvider.GetAllConfigurations1COnDelimiters();
                return Ok(data);
            }
            catch (Exception ex)
            {
                var message =
                    $"Получение всех {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Configurations1C, accessProvider.ContextAccountId)} на разделителях завершилось ошибкой.";
                handlerException.Handle(ex, $"[Ошибка получения всех конфигураций] {message}");
                return PreconditionFailed<List<Configuration1COnDelimitersDto>>(message);
            }
        }

        /// <summary>
        ///     Получить все отрасли
        /// </summary>
        /// <returns>Отрасли</returns>
        public ManagerResult<List<IndustryDto>> GetAllIndustries()
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_GetReferenceData);

            try
            {
                var message = "Получение всех отраслей";
                Logger.Info(message);
                var data = billingServiceInfoProvider.GetAllIndustries();
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения всех отраслей].");
                return PreconditionFailed<List<IndustryDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Проверить уникальность названия нового сервиса
        /// </summary>
        /// <param name="serviceName">Название сервиса</param>
        /// <returns>Результат проверки</returns>
        public ManagerResult<bool> CheckUniquenessOfNameOfNewService(string serviceName)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_FieldValidation);

            try
            {
                var message = $"Проверка на уникальность названия нового сервиса {serviceName}";
                Logger.Info(message);
                var data = billingServiceInfoProvider.CheckUniquenessOfNameOfNewService(serviceName);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка проверки на уникальность названия нового сервиса] {serviceName}");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Проверить уникальность названия сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="serviceName">Название сервиса</param>
        /// <returns>Результат проверки</returns>
        public ManagerResult<bool> CheckUniquenessOfServiceName(Guid? serviceId, string serviceName)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_FieldValidation,
                () => _currentUser.Value.RequestAccountId);

            try
            {
                var message = $"Проверка на уникальность названия сервиса {serviceName}. Id сервиса {serviceId}";
                Logger.Info(message);

                var data = billingServiceInfoProvider.CheckUniquenessOfServiceName(serviceId, serviceName);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка проверки на уникальность названия сервиса] {serviceName}");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Получить текстовое описание назначения платежа по сервису.
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="cost">Сумма платежа.</param>
        /// <returns>Результат проверки</returns>
        public ManagerResult<string> GetPaymentPurposeText(Guid serviceId, Guid accountId, decimal cost)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_GetPaymentPurposeText, () => accountId);

            try
            {
                var message =
                    $"Получение текстового описания назначения платежа по сервису {serviceId} для аккаунта {accountId} на сумму {cost}";
                Logger.Info(message);
                return Ok(billingServiceInfoProvider.GetPaymentPurposeText(serviceId, accountId));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения текстового описания назначения платежа по сервису] {serviceId} для аккаунта {accountId} завершилось ошибкой.");
                return PreconditionFailed<string>(ex.Message);
            }
        }

        /// <summary>
        /// Сохранить новые данные из управления сервисом
        /// </summary>
        /// <param name="model">Модель данных</param>
        public ManagerResult SaveNewOptionsControl(BillingServiceOptionsControlDto model)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_EditResources);

            try
            {
                var billingService =
                    billingServiceInfoProvider.GetBillingService(model.BillingServiceId, model.AccountId);
                var changedRatesForBillingService = GetChangedRatesForBillingService(model);
                billingServiceOptionsControlProvider.SaveNewOptionsControl(model);
                WriteNewOptionsControlChangesToLog(model, billingService, changedRatesForBillingService);
                return Ok();
            }
            catch (Exception ex)
            {
                var service = billingServiceDataProvider.GetBillingServiceOrThrowException(model.BillingServiceId);
                var message = $"Изменение конфигурации сервисa \"{service.Name}\" завершено с ошибкой.";

                handlerException.Handle(ex, $"[Ошибка изменения конфигурации сервисa] \"{service.Name}\"");
                LogEvent(() => model.AccountId, LogActions.SaveNewOptionsControl,
                    $"{message} Причина: {ex.GetFullInfo(false)}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        ///     Найти внешнего пользователя для спонсирования
        /// </summary>
        /// <param name="model">Модель поиска внешнего пользователя для спонсирования</param>
        public ManagerResult<AccountUserBillingServiceTypeDto> SearchExternalAccountUserForSponsorship(
            SearchExternalAccountUserForSponsorshipDto model)
        {
            AccessProvider.HasAccess(ObjectAction.ManageResources, () => model.AccountId);

            try
            {
                var message =
                    $"Поиск внешнего пользователя для спонсирования. Email= {model.Email}, ServiceId= {model.ServiceId}, accountId= {model.AccountId}";
                Logger.Info(message);

                var data = billingServiceSponsorshipProvider.SearchExternalAccountUserForSponsorship(model);

                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка поиска внешнего пользователя для спонсирования]");
                return PreconditionFailed<AccountUserBillingServiceTypeDto>(ex.Message);
            }
        }

        /// <summary>
        /// Принять подписку демо сервиса.
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Покупка за счет обещанного платежа.</param>
        public ManagerResult<ApplyDemoServiceSubscribeResultDto> ApplyDemoServiceSubscribe(Guid serviceId,
            Guid accountId, bool isPromisePayment)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_ApplyDemoSubscribe, () => accountId);

            var service = billingServiceInfoProvider.GetBillingService(serviceId, accountId);
            if (service == null)
                return PreconditionFailed<ApplyDemoServiceSubscribeResultDto>($"Не найден сервис по Id: {serviceId}");

            try
            {
                applyDemoServiceSubscribeProcessor.SetPromisePaymentProvider(promisePaymentProvider);
                var result = applyDemoServiceSubscribeProcessor.Apply(serviceId, accountId, isPromisePayment);

                var logEventDescription = GenerateMessageAboutApplyDemoServiceSubscribe(service, accountId);
                Logger.Info(logEventDescription);
                LogEvent(() => accountId, LogActions.ApplicationDemoServiceSubscribe, logEventDescription);

                return Ok(result);
            }
            catch (Exception ex)
            {
                var message =
                    $"Применение подписки демо сервиса \"{service.GetNameBasedOnLocalization(cloudLocalizer, accountId)}\" завершилось ошибкой.";

                handlerException.Handle(ex, $"[Ошибка применения подписки демо сервиса] \"{service.GetNameBasedOnLocalization(cloudLocalizer, accountId)}\"");
                LogEvent(() => accountId, LogActions.ApplicationDemoServiceSubscribe,
                    $"{message} Описание ошибки: {ex.Message}");

                return PreconditionFailed<ApplyDemoServiceSubscribeResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель запуска базы
        /// </summary>
        /// <param name="billingServiceId">Id cервиса биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель запуска базы</returns>
        public async Task<ManagerResult<AccountDatabaseToRunDto>> GetAccountDatabaseToRun(Guid billingServiceId, Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => accountId);

            try
            {
                var data = await billingServiceInfoProvider.GetAccountDatabaseToRun(billingServiceId, accountId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения модели для запуска базы для сервиса] {billingServiceId}, для аккаунта {accountId}");
                return PreconditionFailed<AccountDatabaseToRunDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить базу на разделителях для запуска
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <returns>База на разделителях для запуска</returns>
        public ManagerResult<AccountDatabaseOnDelimitersToRunDto> GetAccountDatabaseOnDelimitersToRun(
            Guid accountDatabaseId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_GetInfo,
                () => AccountIdByAccountDatabase(accountDatabaseId));

            try
            {
                var message = $"Получение базы на разделителях {accountDatabaseId} для запуска";
                Logger.Info(message);

                var data = billingServiceInfoProvider.GetAccountDatabaseOnDelimitersToRun(accountDatabaseId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения базы на разделителях] {accountDatabaseId} для запуска");
                return PreconditionFailed<AccountDatabaseOnDelimitersToRunDto>(ex.Message);
            }
        }

        /// <summary>
        /// Записать изменения стоимости подписки аккаунта в лог
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="dataAccountUsers">Услуги на подключение к пользователям</param>
        /// <param name="dto">Результат операции применения\оплаты</param>
        private void WriteSubscriptionChangesToLog(Guid accountId, Guid billingServiceId,
            ICollection<CalculateBillingServiceTypeDto> dataAccountUsers, BillingServiceTypeApplyOrPayResultDto dto)
        {
            var accountIndexNumber = billingServiceTypeAccessHelper
                .GetAccountUserById(dataAccountUsers?.FirstOrDefault()?.Subject)?.Account.IndexNumber;

            var billingService = billingServiceInfoProvider.GetBillingService(billingServiceId, accountId);
            var serviceName = billingService.GetNameBasedOnLocalization(cloudLocalizer, accountId);

            if (dto.IsComplete)
            {
                var messageBuilder = new StringBuilder();
                messageBuilder.AppendLine(
                    $"Изменена подписка сервиса \"{serviceName}\".");

                var billingServiceTypesInfo =
                    billingServiceInfoProvider.GetBillingServiceTypesInfo(billingServiceId, accountId);

                if (dataAccountUsers != null && dataAccountUsers.Any())
                {
                    var accountUsers = dataAccountUsers.Select(w => w.Subject).Distinct().ToList();

                    foreach (var accountUserId in accountUsers)
                    {
                        var changes = dataAccountUsers.Where(w => w.Subject == accountUserId).ToList();

                        var accountUser =
                            billingServiceTypeAccessHelper.GetAccountUserById(accountUserId);

                        var enabledServiceTypes =
                            changes.Where(w => w.Status).Select(w => w.BillingServiceTypeId).ToList();

                        enabledServiceTypes.ForEach(serviceTypeId =>
                        {
                            var serviceType = billingServiceTypesInfo.FirstOrDefault(w => w.Id == serviceTypeId);
                            if (serviceType == null)
                                return;

                            messageBuilder.AppendLine(
                                $"Пользователю \"{accountUser?.Login}\" подключена услуга \"{serviceType.Name}\".");
                        });
                    }
                }

                messageBuilder.AppendLine($"Оплачено {dto.Amount:0.00} {dto.Currency}.");
                var logEventDescription = messageBuilder.ToString();
                LogEvent(() => accountId, LogActions.ApplyOrPayForAllChangesAccountServiceTypes, logEventDescription);
                Logger.Info(logEventDescription);
            }
            else
            {
                var errorMessage =
                    $"Подписка сервиса \"{serviceName}\" не изменена. Причина: не хватает {dto.NotEnoughMoney} {dto.Currency}.";
                Logger.Info($"[{accountIndexNumber}]::{errorMessage}");
                LogEvent(() => accountId, LogActions.ApplyOrPayForAllChangesAccountServiceTypes, errorMessage);
            }
        }

        /// <summary>
        /// Записать новые данные из управления сервисом в лог
        /// </summary>
        /// <param name="model">Модель для применения новых данных из управления</param>
        /// <param name="billingService">Сервис облака</param>
        /// <param name="changedRatesForBillingService">Изменения по стоимости услуг сервиса</param>
        private void WriteNewOptionsControlChangesToLog(BillingServiceOptionsControlDto model,
            BillingServiceDto billingService, List<RateChangeItem> changedRatesForBillingService)
        {
            var billingServiceTypesInfo =
                billingServiceInfoProvider.GetBillingServiceTypesInfo(model.BillingServiceId, model.AccountId);

            var message =
                $"Конфигурация сервисa \"{billingService.GetNameBasedOnLocalization(cloudLocalizer, model.AccountId)}\" изменена. ";

            if (model.NewExpireDate != billingService.ServiceExpireDate)
                message =
                    $"{message} Старая дата завершения услуг {billingService.ServiceExpireDate:dd.MM.yyyy HH:mm:ss} , установленная {model.NewExpireDate:dd.MM.yyyy HH:mm:ss}.";
            var localeCurrency = billingService.AmountData.Currency;

            changedRatesForBillingService.ForEach(rate =>
            {
                var serviceType = billingServiceTypesInfo.FirstOrDefault(st => st.Id == rate.BillingServiceTypeId);
                var newRate =
                    model.NewCosts.FirstOrDefault(newRateItem => newRateItem.Key == rate.BillingServiceTypeId);
                message =
                    $"{message} Стоимость лицензии \"{serviceType?.Name}\" изменена с {(int)rate.Cost} {localeCurrency.Currency}. на {(int)newRate.Value} {localeCurrency.Currency}.";
            });

            Logger.Info(message);
            LogEvent(() => model.AccountId, LogActions.SaveNewOptionsControl, message);
        }

        /// <summary>
        /// Получить изменения по стоимости услуг сервиса
        /// </summary>
        /// <param name="model">Модель для применения новых данных из управления</param>
        /// <returns>Изменения по стоимости услуг сервиса</returns>
        private List<RateChangeItem> GetChangedRatesForBillingService(
            BillingServiceOptionsControlDto model)
        {
            var rates = billingServiceTypeRateHelper.GetRates(model.BillingServiceId, model.AccountId);
            return model.NewCosts
                .Where(w => rates[w.Key] != w.Value)
                .Select(w => new RateChangeItem { BillingServiceTypeId = w.Key, Cost = rates[w.Key] })
                .ToList();
        }

        /// <summary>
        /// Сгенерировать сообщение о принятии подписки на демо-сервис
        /// </summary>
        /// <param name="service">Сервис</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Сообщение о принятии подписки на демо-сервис</returns>
        private string GenerateMessageAboutApplyDemoServiceSubscribe(BillingServiceDto service, Guid accountId)
        {
            var messageBuilder = new StringBuilder();
            var billingServiceTypesInfo =
                billingServiceInfoProvider.GetBillingServiceTypesInfo(service.Id, accountId);
            messageBuilder.AppendLine(
                $"Подтверждение подписки на сервис \"{service.GetNameBasedOnLocalization(cloudLocalizer, accountId)}\".");

            billingServiceTypesInfo.ForEach(billingServiceTypeInfo =>
            {
                if (billingServiceTypeInfo.UsedLicenses == 0)
                    return;
                messageBuilder.AppendLine(
                    $"Пользователи \"{billingServiceTypeInfo.Name}\" - {billingServiceTypeInfo.UsedLicenses}. ");
            });

            return messageBuilder.ToString();
        }

        /// <summary>
        /// Получить список опций услуги
        /// </summary>
        /// <param name="serviceId">Для какой услуги получить список опций</param>
        /// <returns>Список опций услуги</returns>
        public ManagerResult<List<ServiceOptionDto>> GetServiceOptions(Guid serviceId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => AccessProvider.GetUser()?.RequestAccountId);

            try
            {
                return Ok(billingServiceInfoProvider.GetServiceOptions(serviceId));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения опций сервиса] {serviceId}");
                return PreconditionFailed<List<ServiceOptionDto>>(ex.Message);
            }
        }


        /// <summary>
        /// Включить автоподписку для сервиса, после окончания демо периода
        /// </summary>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <param name="serviceId">Для какого сервиса</param>
        public ManagerResult<bool> EnableAutoSubscription(Guid accountId, Guid serviceId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_ApplyDemoSubscribe, () => accountId);

            try
            {
                billingServiceTypeAccessProvider.EnableAutoSubscription(accountId, serviceId);
                return Ok(true);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка включения авто-подписки для сервиса] \"{serviceId}\"");
                return PreconditionFailed<bool>(ex.Message);
            }

        }

        /// <summary>
        /// Поиск подходящих сервисов по имени
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public ManagerResult<IEnumerable<SelectListItem>> SearchServicesByName(string searchLine)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Partner_GetBillingServices, () =>  AccessProvider.ContextAccountId);

                var data = string.IsNullOrEmpty(searchLine)
                    ? dbLayer.BillingServiceRepository.AsQueryable().Take(20).ToList().Select(w => new SelectListItem(w.Name, w.Id.ToString()))

                    : dbLayer.BillingServiceRepository.AsQueryable().Where(service => service.Name.Contains(searchLine))
                    .Take(20).Select(w => new SelectListItem(w.Name, w.Id.ToString()));

                return Ok(data);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<IEnumerable<SelectListItem>>(ex.Message);
            }
        }

        /// <summary>
        /// Продление демо сервиса
        /// </summary>
        /// <returns></returns>
        public ManagerResult ProlongDemo(Guid serviceId, Guid accountId, int prolongDayCount)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_EditResources);

                var resourceConfiguration = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == accountId && rc.BillingServiceId == serviceId);

                if (resourceConfiguration == null)
                    return PreconditionFailed($"Даннный сервис {serviceId} не подключен у аккаунта {accountId}");

                if (!resourceConfiguration.IsDemoPeriod)
                    return PreconditionFailed($"У даннного сервиса {serviceId} закончился демо период и продлить его нельзя! ");

                resourceConfiguration.IsDemoPeriod = true;
                resourceConfiguration.ExpireDate = resourceConfiguration.ExpireDate?.AddDays(prolongDayCount) ?? DateTime.Now.AddDays(prolongDayCount);
                resourceConfiguration.Frozen = false;

                dbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
                dbLayer.Save();

                Logger.Info($"У сервиса {resourceConfiguration.BillingService.Name} для аккаунта {resourceConfiguration.BillingAccounts.Account.IndexNumber} продлен демо период");
                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
