﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер для работы
    /// с заявкой изменения сервиса на модерацию
    /// </summary>
    public class ChangeServiceRequestManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IChangeServiceRequestProvider changeServiceRequestProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить заявку изменения сервиса на модерацию
        /// </summary>
        /// <param name="changeServiceRequestId">Id заявки</param>
        /// <returns>Заявка изменения сервиса на модерацию</returns>
        public ManagerResult<ChangeServiceRequestDto> Get(Guid changeServiceRequestId)
        {
            var message = $"Получение заявки изменения сервиса на модерацию по Id {changeServiceRequestId}";
            try
            {
                logger.Info(message);
                AccessProvider.HasAccess(ObjectAction.ChangeServiceRequest_GetData,
                    () => AccountIdByChangeServiceRequestId(changeServiceRequestId));

                var data = changeServiceRequestProvider.Get(changeServiceRequestId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");
                return PreconditionFailed<ChangeServiceRequestDto>(ex.Message);
            }
        }
    }
}
