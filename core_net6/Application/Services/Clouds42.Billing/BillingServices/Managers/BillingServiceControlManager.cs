﻿using System.Text;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Managers
{
    /// <summary>
    /// Менеджер для управления сервисом
    /// </summary>
    public class BillingServiceControlManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICloudLocalizer cloudLocalizer,
        IBillingServiceControlProvider billingServiceControlProvider,
        IBillingServiceDataProvider billingServiceDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Выполнить управление
        /// </summary>
        /// <param name="manageBillingServiceDto">Модель управления сервисом</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult Manage(ManageBillingServiceDto manageBillingServiceDto)
        {
            var accountId = AccountIdByServiceId(manageBillingServiceDto.Id);
            if (!TryGetBillingServiceById(manageBillingServiceDto.Id, out var billingService))
                return PreconditionFailed($"Сервис биллинга по ID {manageBillingServiceDto.Id} не найден");
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_Control, () => accountId);

                var logEventDescription =
                    GenerateMessageAboutBillingServiceChanges(billingService, manageBillingServiceDto);

                billingServiceControlProvider.Manage(manageBillingServiceDto);

                LogEvent(() => accountId, LogActions.SaveBillingServiceChanges, logEventDescription);
                logger.Info(logEventDescription);
                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    $"[Ошибка сохранений изменений сервиса биллинга] \"{billingService.GetNameBasedOnLocalization(cloudLocalizer, AccessProvider.ContextAccountId)}\".";
                LogEvent(() => accountId, LogActions.SaveBillingServiceChanges,
                    $"{message} Описание ошибки: {ex.Message}");
                handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Сгенерировать сообщение об изменениях по сервису
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <param name="manageBillingServiceDto">Модель управления сервисом</param>
        /// <returns>Сообщение об изменениях по сервису</returns>
        private string GenerateMessageAboutBillingServiceChanges(BillingService billingService,
            ManageBillingServiceDto manageBillingServiceDto)
        {
            var messageBuilder = new StringBuilder();
            messageBuilder.AppendLine($"Изменения для сервиса \"{billingService.GetNameBasedOnLocalization(cloudLocalizer, AccessProvider.ContextAccountId)}\" сохранены.");

            if (billingService.InternalCloudService != manageBillingServiceDto.InternalCloudService)
                messageBuilder.AppendLine(
                    "Изменен внутренний облачный сервис " +
                    $"с \"{billingService.InternalCloudService?.Description() ?? "---"}\" " +
                    $"на \"{manageBillingServiceDto.InternalCloudService?.Description() ?? "---"}\".");

            if (billingService.BillingServiceStatus != manageBillingServiceDto.BillingServiceStatus)
                messageBuilder.AppendLine(
                    $"Изменен статус сервиса с \"{billingService.BillingServiceStatus.Description()}\" " +
                    $"на \"{manageBillingServiceDto.BillingServiceStatus.Description()}\"");

            return messageBuilder.ToString();
        }

        /// <summary>
        /// Попытаться получить сервис биллинга по ID
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="billingService">Сервис биллинга</param>
        /// <returns>Результат попытки</returns>
        private bool TryGetBillingServiceById(Guid billingServiceId, out BillingService billingService)
        {
            try
            {
                billingService = billingServiceDataProvider.GetBillingServiceOrThrowNewException(billingServiceId);
                return true;
            }
            catch (Exception)
            {
                billingService = new BillingService();
                return false;
            }
        }
    }
}
