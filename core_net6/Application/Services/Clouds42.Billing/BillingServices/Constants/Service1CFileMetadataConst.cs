﻿namespace Clouds42.Billing.BillingServices.Constants
{
    /// <summary>
    /// Вспомогательные константы
    /// для получения метаданных файла разработки 1С
    /// </summary>
    public static class Service1CFileMetadataConst
    {
        /// <summary>
        /// Название файла XML, который содержит метаданные
        /// </summary>
        public const string ManifestName = "ExtensionManifest.xml";

        /// <summary>
        /// Xml тэг Info
        /// </summary>
        public const string InfoTag = "Info";

        /// <summary>
        /// Xml тэг Name
        /// </summary>
        public const string NameTag = "Name";

        /// <summary>
        /// Xml тэг Synonym
        /// </summary>
        public const string SynonymTag = "Synonym";

        /// <summary>
        /// Xml тэг ObjectName
        /// </summary>
        public const string ObjectNameTag = "ObjectName";

        /// <summary>
        /// Xml тэг Version
        /// </summary>
        public const string VersionTag = nameof(Version);

        /// <summary>
        /// Xml тэг Result
        /// </summary>
        public const string ResultTag = "Result";

        /// <summary>
        /// Тип контента
        /// </summary>
        public const string ContentType = "Content-Type";
    }
}
