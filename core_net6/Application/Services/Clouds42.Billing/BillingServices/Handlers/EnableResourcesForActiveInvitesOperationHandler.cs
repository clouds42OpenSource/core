﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Handlers
{
    /// <summary>
    /// Обработчик операции подключения услуг сервиса пользователям
    /// </summary>
    public class EnableServiceTypesToUsersOperationHandler(
        IUnitOfWork dbLayer,
        ITardisBillingServiceProvider tardisBillingServiceProvider,
        IChangeTardisServiceSubscriptionSynchronizator changeTardisServiceSubscriptionSynchronizator,
        ILogger42 logger)
    {
        /// <summary>
        /// Обработать операцию подключения услуг сервиса пользователям
        /// </summary>
        /// <param name="serviceTypesDataForEnablingToUsers">Список моделей данных по услугам для подключения к пользователям</param>
        /// <param name="accountId">Id аккаунта</param>
        public void Handle(List<ServiceTypesDataForEnablingToUserModel> serviceTypesDataForEnablingToUsers,
            Guid accountId)
        {
            var freeResources = GetFreeResourcesByServiceTypeIds(serviceTypesDataForEnablingToUsers, accountId);

            foreach (var serviceTypesDataForEnablingToUser in serviceTypesDataForEnablingToUsers
                    .OrderBy(serviceTypesDataForEnablingToUser => serviceTypesDataForEnablingToUser.ServiceTypeEnablingData.Cost))
            {
                EnableServiceTypesForUser(serviceTypesDataForEnablingToUser, accountId, freeResources);
            }

            if (serviceTypesDataForEnablingToUsers.Any())
                dbLayer.Save();
        }

        /// <summary>
        /// Подключить услуги для пользователя
        /// </summary>
        /// <param name="serviceTypesDataForEnablingToUser">Модель данных по услугам для подключения к пользователю</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="freeResources">Список бесплатных ресурсов(свободных)</param>
        private void EnableServiceTypesForUser(ServiceTypesDataForEnablingToUserModel serviceTypesDataForEnablingToUser,
            Guid accountId, List<Resource> freeResources) =>
            serviceTypesDataForEnablingToUser.ServiceTypesDataForEnabling.ForEach(serviceTypeData =>
                EnableServiceTypeForUser(serviceTypesDataForEnablingToUser, serviceTypeData, accountId, freeResources));

        /// <summary>
        /// Подключить услугу для пользователя
        /// </summary>
        /// <param name="serviceTypesDataForEnablingToUser">Модель данных по услугам для подключения к пользователю</param>
        /// <param name="serviceTypeData">Модель данных услуги сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="freeResources">Список бесплатных ресурсов(свободных)</param>
        private void EnableServiceTypeForUser(ServiceTypesDataForEnablingToUserModel serviceTypesDataForEnablingToUser,
            ServiceTypeDataModel serviceTypeData, Guid accountId, List<Resource> freeResources)
        {
            if (serviceTypesDataForEnablingToUser.ServiceTypeEnablingData.Sponsorship.I)
            {
                EnableSponsorshipResourceToUserByServiceType(serviceTypesDataForEnablingToUser, serviceTypeData,
                    accountId, freeResources);

                return;
            }

            EnableResourceToUserByServiceType(serviceTypesDataForEnablingToUser, serviceTypeData, accountId,
                freeResources);
        }

        /// <summary>
        /// Подключить спонсорскую лицензию для пользователя по услуге
        /// </summary>
        /// <param name="serviceTypesDataForEnablingToUser">Модель данных по услугам для подключения к пользователю</param>
        /// <param name="serviceTypeData">Модель данных услуги сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="freeResources">Список бесплатных ресурсов(свободных)</param>
        private void EnableSponsorshipResourceToUserByServiceType(
            ServiceTypesDataForEnablingToUserModel serviceTypesDataForEnablingToUser,
            ServiceTypeDataModel serviceTypeData, Guid accountId, List<Resource> freeResources)
        {
            if (serviceTypesDataForEnablingToUser.ServiceTypeEnablingData.Cost > 0 ||
                !TrySelectFreeResourceForSponsorshipFromList(freeResources, serviceTypeData,
                    out var sponsorshipResource)
            )
            {
                CreateSponsorshipResource(serviceTypesDataForEnablingToUser.ServiceTypeEnablingData,
                    serviceTypeData.ServiceTypeId, accountId, serviceTypeData.ServiceTypeCost);
                return;
            }

            UpdateExistingSponsorshipResource(sponsorshipResource, accountId,
                serviceTypesDataForEnablingToUser.ServiceTypeEnablingData.Subject,
                serviceTypeData.ServiceTypeCost);

            freeResources.Remove(sponsorshipResource);
        }

        /// <summary>
        /// Подключить лицензию для пользователя по услуге
        /// </summary>
        /// <param name="serviceTypesDataForEnablingToUser">Модель данных по услугам для подключения к пользователю</param>
        /// <param name="serviceTypeData">Модель данных услуги сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="freeResources">Список бесплатных ресурсов(свободных)</param>
        private void EnableResourceToUserByServiceType(
            ServiceTypesDataForEnablingToUserModel serviceTypesDataForEnablingToUser,
            ServiceTypeDataModel serviceTypeData, Guid accountId, List<Resource> freeResources)
        {
            if (TrySelectFreeResourceFromList(freeResources, accountId, serviceTypeData, out var existingResource))
            {
                UpdateExistingResource(existingResource,
                    serviceTypesDataForEnablingToUser.ServiceTypeEnablingData.Subject,
                    serviceTypeData.ServiceTypeCost);

                freeResources.Remove(existingResource);
                return;
            }

            InsertResourceForAccountUser(serviceTypesDataForEnablingToUser.ServiceTypeEnablingData,
                serviceTypeData.ServiceTypeId, accountId, serviceTypeData.ServiceTypeCost);

            logger.Trace(
                $"Создали новый ресурс '{serviceTypeData.ServiceTypeId}' пользователю '{serviceTypesDataForEnablingToUser.ServiceTypeEnablingData.Subject}'");
        }

        /// <summary>
        /// Обновить существующий спонсорский ресурс
        /// </summary>
        /// <param name="sponsorshipResource">Существующий спонсорский ресурс</param>
        /// <param name="sponsorAccountId">Id аккаунта спонсора</param>
        /// <param name="resourceSubject">Id субъекта ресурса</param>
        /// <param name="cost">Стоимость ресурса</param>
        private void UpdateExistingSponsorshipResource(Resource sponsorshipResource,
            Guid sponsorAccountId, Guid resourceSubject, decimal cost)
        {
            var sponsorshipAccount =
                GetSponsorshipAccountByResourceSubjectId(resourceSubject);

            sponsorshipResource.AccountId = sponsorshipAccount.Id;
            sponsorshipResource.Cost = cost;
            sponsorshipResource.Subject = resourceSubject;
            sponsorshipResource.AccountSponsorId = sponsorAccountId;
            dbLayer.ResourceRepository.Update(sponsorshipResource);

            SynchronizeResourceChangesWithTardis(sponsorshipResource, sponsorshipResource.BillingServiceType.ServiceId,
                resourceSubject);
        }

        /// <summary>
        /// Обновить существующий ресурс
        /// </summary>
        /// <param name="existingResource">Существующий ресурс</param>
        /// <param name="resourceSubject">Id субъекта ресурса</param>
        /// <param name="cost">Стоимость ресурса</param>
        private void UpdateExistingResource(Resource existingResource, Guid resourceSubject, decimal cost)
        {
            existingResource.Cost = cost;
            existingResource.Subject = resourceSubject;
            dbLayer.ResourceRepository.Update(existingResource);

            logger.Trace(
                $"Подключили ресурс '{existingResource.BillingServiceType.Name}'-'{existingResource.Id}' пользователю '{resourceSubject}'");

            SynchronizeResourceChangesWithTardis(existingResource, existingResource.BillingServiceType.ServiceId,
                resourceSubject);
        }

        /// <summary>
        /// Синхронизировать изменение ресурса с Тардисом
        /// </summary>
        /// <param name="resource">Ресурс</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="resourceSubject">Id субъекта ресурса</param>
        private void SynchronizeResourceChangesWithTardis(Resource resource, Guid serviceId, Guid resourceSubject)
        {
            tardisBillingServiceProvider.ManageAccessToTardisAccountDatabase(resourceSubject,
                serviceId, UpdateAcDbAccessActionType.Grant);
            changeTardisServiceSubscriptionSynchronizator.SynchronizeWithTardis(resource);
        }

        /// <summary>
        /// Попытаться выбрать свободный ресурс из списка
        /// </summary>
        /// <param name="freeResources">Список свободных ресурсов</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeData">Данные по услуге</param>
        /// <param name="freeResource">Свободный выбранный ресурс из списка</param>
        /// <returns>Результат выбора/получения свободного ресурса(если удалось найти)</returns>
        private static bool TrySelectFreeResourceFromList(List<Resource> freeResources, Guid accountId,
            ServiceTypeDataModel serviceTypeData, out Resource freeResource)
        {
            freeResource = freeResources.FirstOrDefault(resource =>
                resource.BillingServiceTypeId == serviceTypeData.ServiceTypeId && resource.AccountId == accountId &&
                resource.Cost == serviceTypeData.ServiceTypeCost);

            return freeResource != null;
        }

        /// <summary>
        /// Попытаться выбрать свободный ресурс для спонсирования из списка
        /// </summary>
        /// <param name="freeResources">Список свободных ресурсов</param>
        /// <param name="serviceTypeData">Данные по услуге</param>
        /// <param name="freeResource">Свободный выбранный ресурс из списка</param>
        /// <returns>Результат выбора/получения свободного ресурса(если удалось найти)</returns>
        private static bool TrySelectFreeResourceForSponsorshipFromList(List<Resource> freeResources,
            ServiceTypeDataModel serviceTypeData, out Resource freeResource)
        {
            freeResource = freeResources.FirstOrDefault(resource =>
                resource.BillingServiceTypeId == serviceTypeData.ServiceTypeId && resource.AccountSponsorId == null &&
                resource.Cost == serviceTypeData.ServiceTypeCost);

            return freeResource != null;
        }

        /// <summary>
        /// Получить спонсируемый аккаунт по Id субъекта ресурса
        /// </summary>
        /// <param name="resourceSubject">Id субъекта ресурса</param>
        /// <returns>Спонсируемый аккаунт</returns>
        private Account GetSponsorshipAccountByResourceSubjectId(Guid resourceSubject) =>
            dbLayer.AccountUsersRepository.FirstOrDefault(accountUser =>
                accountUser.Id == resourceSubject)?.Account ??
            dbLayer.AccountsRepository.FirstOrDefault(account =>
                account.Id == resourceSubject);

        /// <summary>
        /// Добавить лицензию для пользователя
        /// </summary>
        /// <param name="data">Текущий инвайт</param>
        /// <param name="billingServiceTypeId">Текущая услуга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="cost">Стоимость</param>
        private void InsertResourceForAccountUser(CalculateBillingServiceTypeDto data,
            Guid billingServiceTypeId, Guid accountId, decimal cost = 0)
        {
            var existingUserResource = dbLayer.ResourceRepository.FirstOrDefault(resource =>
                resource.AccountId == accountId && resource.Subject == data.Subject &&
                resource.BillingServiceTypeId == billingServiceTypeId);

            if (existingUserResource != null)
                return;

            var newResource = CreateResourceObject(data.Subject, billingServiceTypeId, accountId, cost);
            dbLayer.ResourceRepository.Insert(newResource);
            dbLayer.Save();

            SynchronizeResourceChangesWithTardis(newResource, GetBillingService(billingServiceTypeId).Id, data.Subject);
        }

        /// <summary>
        /// Создать спонсорскую лицензию
        /// </summary>
        /// <param name="data">Текущий инвайт</param>
        /// <param name="billingServiceTypeId">Текущая услуга</param>
        /// <param name="accountId">Спонсорский аккаунт</param>
        /// <param name="serviceTypeCost">Стоимость услуги</param>
        private void CreateSponsorshipResource(CalculateBillingServiceTypeDto data, Guid billingServiceTypeId,
            Guid accountId, decimal serviceTypeCost)
        {
            var sponsorshipAccount = GetSponsorshipAccountByResourceSubjectId(data.Subject);

            var newResource = new Resource
            {
                Id = Guid.NewGuid(),
                Subject = data.Subject,
                BillingServiceTypeId = billingServiceTypeId,
                AccountId = sponsorshipAccount.Id,
                AccountSponsorId = accountId,
                Cost = serviceTypeCost,
                IsFree = false
            };
            dbLayer.ResourceRepository.Insert(newResource);

            SynchronizeResourceChangesWithTardis(newResource, GetBillingService(billingServiceTypeId).Id,
                newResource.Subject.Value);
        }

        /// <summary>
        /// Создать объект ресурса
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="billingServiceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="cost">Стоимость</param>
        /// <returns>Объект ресурса</returns>
        private static Resource CreateResourceObject(Guid accountUserId, Guid billingServiceTypeId, Guid accountId,
            decimal cost = 0)
            => new()
            {
                Id = Guid.NewGuid(),
                Subject = accountUserId,
                BillingServiceTypeId = billingServiceTypeId,
                AccountId = accountId,
                Cost = cost,
                IsFree = false
            };

        /// <summary>
        /// Получить свободные ресурсы по списку Id услуг
        /// </summary>
        /// <param name="serviceTypesDataForEnablingToUsers">Cписк моделей данных по услугам на подключение к пользователям</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Свободные ресурсы по списку услуг</returns>
        private List<Resource> GetFreeResourcesByServiceTypeIds(
            List<ServiceTypesDataForEnablingToUserModel> serviceTypesDataForEnablingToUsers, Guid accountId)
        {
            var serviceTypeIds = serviceTypesDataForEnablingToUsers.SelectMany(serviceTypeData =>
                    serviceTypeData.ServiceTypesDataForEnabling.Select(data => data.ServiceTypeId))
                .ToList();

            return dbLayer
                .ResourceRepository
                .WhereLazy(resource =>
                    (resource.AccountId == accountId || resource.AccountSponsorId == accountId) &&
                    serviceTypeIds.Contains(resource.BillingServiceTypeId) && resource.Subject == null)
                .ToList();
        }

        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="billingServiceTypeId">ID услуги сервиса биллинга</param>
        /// <returns>Сервис биллинга</returns>
        private BillingService GetBillingService(Guid billingServiceTypeId)
            => dbLayer.BillingServiceTypeRepository
                   .FirstOrDefault(serviceType => serviceType.Id == billingServiceTypeId)?.Service ??
               throw new NotFoundException($"Сервис биллинга по ID услуги {billingServiceTypeId} не найден");
    }
}
