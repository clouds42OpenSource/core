﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.BillingServices.Validators
{
    /// <summary>
    /// Валидатор установки активности сервиса
    /// </summary>
    public class SetBillingServiceActivityValidator(
        ISetBillingServiceActivityDataProvider setBillingServiceActivityDataProvider)
    {
        /// <summary>
        /// Попытаться выполнить валидацию
        /// </summary>
        /// <param name="model">Модель для установки активности сервиса биллинга</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public bool TryValidate(SetBillingServiceActivityDto model)
        {

            if (model.IsActive)
                return true;

            if (!setBillingServiceActivityDataProvider.CheckAvailabilityActiveResourceConfigurationAtService(
                model.ServiceId))
                return true;

            return false;
        }
    }
}
