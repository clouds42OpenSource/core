﻿using System.Text;
using System.Text.RegularExpressions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Validators
{
    /// <summary>
    /// Валидатор сервиса биллинга
    /// </summary>
    public class BillingServiceValidator(
        IUnitOfWork dbLayer,
        IBillingServiceCardDataProvider billingServiceCardDataProvider,
        ServiceTypeHelperForBillingServiceOperations serviceTypeHelper)
    {
        /// <summary>
        /// Максимальная длина строки 'Названия сервиса'
        /// </summary>
        public static int MaxLengthServiceName => 100;

        /// <summary>
        /// Максимальная длина строки 'Краткое описание сервиса'
        /// </summary>
        public static int MaxLengthShortDescription => 200;

        /// <summary>
        /// Максимальная длина строки 'Возможности сервиса'
        /// </summary>
        public static int MaxLengthOpportunities => 2000;

        /// <summary>
        /// Максимальная длина строки 'Название услуги'
        /// </summary>
        public static int MaxLengtServiceTypeName => 100;

        /// <summary>
        /// Максимальная длина строки 'Описсание услуги'
        /// </summary>
        public static int MaxLengtServiceTypeDescription => 200;

        /// <summary>
        /// Максимальное количество скриншотов
        /// </summary>
        public static int MaxCountScreenshots => 10;

        /// <summary>
        /// Максимальная длина строки "Описание изменений файла 1С"
        /// </summary>
        public static int MaxLengtDescriptionOf1CFileChanges => 2000;

        /// <summary>
        /// Максимальная длина строки "Комментарий для модератора"
        /// </summary>
        public static int MaxLengtCommentForModerator => 2000;

        /// <summary>
        /// Регулярное выражение для проверки версии файла 1С
        /// </summary>
        public static Regex RegexVersionFile1C { get; } = new(@"(^\d{1,5}\.){1}((\d{1,5}){1,3})");

        /// <summary>
        /// Регулярное выражение названия сервиса
        /// </summary>
        private Regex RegexName { get; } = new(@"^[^/,\\,',\""]+$");

        /// <summary>
        /// Список связей услуг
        /// </summary>
        private List<Guid> ServiceTypeRelations { get; set; } = [];

        /// <summary>
        /// Список услуг
        /// </summary>
        private List<BillingServiceTypeDto> ServiceTypes { get; set; } = [];

        /// <summary>
        /// Выполнить валидацию создания нового сервиса
        /// </summary>
        /// <param name="createBillingServiceDto">Модель создания сервиса</param>
        /// <returns>Результат валидации</returns>
        public ValidateResultModelDto ValidateCreationOfNewBillingService(CreateBillingServiceDto createBillingServiceDto)
        {
            if (createBillingServiceDto.BillingServiceStatus == BillingServiceStatusEnum.Draft)
                return new ValidateResultModelDto { Success = true };

            var validateResults = ValidateBaseOperationBillingServiceDto(createBillingServiceDto, null);

            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Выполнить валидацию редактирования сервиса
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        /// <returns>Результат валидации</returns>
        public ValidateResultModelDto ValidateEditService(EditBillingServiceDto editBillingServiceDto)
        {
            CheckAbilityToChangeServiceStatus(editBillingServiceDto);
            
            var validateResults =
                ValidateBaseOperationBillingServiceDto(editBillingServiceDto, editBillingServiceDto.Id);

            validateResults.Add(CheckAbilityToCreateRequest(editBillingServiceDto));
            validateResults.Add(CheckServiceTypesWhenCreatingChangeServiceRequest(editBillingServiceDto));

            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Выполнить валидацию редактирования черновика
        /// </summary>
        /// <param name="editBillingServiceDraftDto">Модель сервиса в статусе Драфт</param>
        /// <returns>Результат валидаации</returns>
        public ValidateResultModelDto ValidateEditServiceDraft(EditBillingServiceDraftDto editBillingServiceDraftDto)
        {
            var validateResults =
                ValidateBaseOperationBillingServiceDto(editBillingServiceDraftDto, editBillingServiceDraftDto.Id);
            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Выполонить валидацию общей модели BaseOperationBillingServiceDto
        /// </summary>
        /// <param name="baseOperationBillingServiceDto">Общая модель сервиса</param>
        /// <param name="id">Id сервиса</param>
        /// <returns>Список прохождения всех валидаций</returns>
        private List<ValidateResultModelDto> ValidateBaseOperationBillingServiceDto(
            BaseOperationBillingServiceDto baseOperationBillingServiceDto, Guid? id)
        {
            var validateResults = new List<ValidateResultModelDto>
            {
                CheckServiceName(baseOperationBillingServiceDto.Name, id),
                CheckServiceKey(baseOperationBillingServiceDto.Key, id),
                CheckServiceTypeKeys(baseOperationBillingServiceDto.BillingServiceTypes, id),
                CheckServiceTypes(baseOperationBillingServiceDto.BillingServiceTypes),
                CheckServiceTypeRelations(baseOperationBillingServiceDto.BillingServiceTypes)
            };

            return validateResults;
        }

        /// <summary>
        /// Проверить внутренний облачный сервис
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="internalCloudService">Внутренний облачный сервис</param>
        /// <returns>Результат проверки</returns>
        public ValidateResultModelDto CheckInternalCloudService(Guid serviceId,
            InternalCloudServiceEnum? internalCloudService)
        {
            if (internalCloudService == null)
                return new ValidateResultModelDto { Success = true };

            var anotherInternalCloudService =
                dbLayer.BillingServiceRepository.FirstOrDefault(w =>
                    w.InternalCloudService == internalCloudService && w.Id != serviceId);

            if (anotherInternalCloudService != null)
                return new ValidateResultModelDto
                {
                    Success = false,
                    Message = $"Внутренний облачный сервис {internalCloudService.Description()} уже существует"
                };

            return new ValidateResultModelDto { Success = true };
        }

        
        /// <summary>
        /// Проверить ключи услуг
        /// </summary>
        /// <param name="billingServiceTypes">Услуги</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceTypeKeys(List<BillingServiceTypeDto> billingServiceTypes,
            Guid? serviceId = null)
        {
            if (billingServiceTypes.Any(st => st.Key.IsNullOrEmpty()))
                return new ValidateResultModelDto { Message = "Для каждой услуги должен быть указан валидный ключ" };

            if (!serviceId.IsNullOrEmpty() && serviceId.HasValue &&
                GetNumberOfNewKeys(billingServiceTypes.Select(st => st.Key).ToList(), serviceId.Value) == 0)
                return new ValidateResultModelDto { Success = true };

            var keys = billingServiceTypes.Select(st => st.Key).ToList();

            if (keys.Count != keys.Distinct().Count())
                return new ValidateResultModelDto { Message = "Для каждой услуги ключ должен быть уникальным" };

            foreach (var key in keys)
            {
                var availabilityServiceTypeMatchingKey = !serviceId.IsNullOrEmpty() && serviceId.HasValue
                    ? CheckAvailabilityServiceTypeMatchingKey(serviceId.Value, key)
                    : dbLayer.BillingServiceTypeRepository.FirstOrDefault(s => s.Key == key) != null;

                if (availabilityServiceTypeMatchingKey)
                    return new ValidateResultModelDto { Message = $"Услуга с ключом {key} уже существует" };
            }

            return new ValidateResultModelDto { Success = true };
        }

        /// <summary>
        /// Получить количество новых ключей
        /// </summary>
        /// <param name="serviceTypeKeys">Ключи</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Количество новых ключей</returns>
        private int GetNumberOfNewKeys(List<Guid> serviceTypeKeys, Guid serviceId)
        {
            var hashSetList = new HashSet<Guid>(serviceTypeKeys);
            var serviceTypeKeysCount = hashSetList.Count;

            var oldKeys = dbLayer.BillingServiceTypeRepository.Where(st => st.ServiceId == serviceId)
                .Select(st => st.Key).ToList();

            oldKeys.ForEach(key => hashSetList.Add(key));
            return hashSetList.Count - serviceTypeKeysCount;
        }

        /// <summary>
        /// Проверить наличие конфигурации ресурса для сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Наличие конфигурации ресурса</returns>
        private bool CheckAvailabilityResourcesConfigurationForService(Guid serviceId) =>
            dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == serviceId) != null;

        /// <summary>
        /// Проверить ключ сервиса
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceKey(Guid key, Guid? serviceId = null)
        {
            if (key.IsNullOrEmpty())
                return new ValidateResultModelDto { Message = "Укажите валидный ключ для сервиса" };

            var service = dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId);

            if (!serviceId.IsNullOrEmpty() && service.Key == key)
                return new ValidateResultModelDto { Success = true };

            if (!serviceId.IsNullOrEmpty() && serviceId.HasValue &&
                CheckAvailabilityResourcesConfigurationForService(serviceId.Value))
                return new ValidateResultModelDto
                { Message = "Изменение ключа не возможно. У сервиса есть подключенные пользователи" };

            if (!serviceId.IsNullOrEmpty() && serviceId.HasValue)
            {
                var availabilityServiceMatchingKey = CheckAvailabilityServiceMatchingKey(serviceId.Value, key);

                if (availabilityServiceMatchingKey)
                    return new ValidateResultModelDto { Message = $"Сервис с ключом {key} уже существует" };
            }
            else
            {
                var availabilityServiceMatchingKey =
                    dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Key == key) != null;

                if (availabilityServiceMatchingKey)
                    return new ValidateResultModelDto { Message = $"Сервис с ключом {key} уже существует" };
            }

            return new ValidateResultModelDto { Success = true };
        }

        /// <summary>
        /// Проверить наличие услуги совподающей по ключу
        /// </summary>
        /// <param name="serviceId">Id текущего сервиса</param>
        /// <param name="key">Ключ</param>
        /// <returns>Наличие услуги</returns>
        private bool CheckAvailabilityServiceTypeMatchingKey(Guid serviceId, Guid key) =>
            dbLayer.BillingServiceTypeRepository.FirstOrDefault(s => s.Key == key && s.ServiceId != serviceId) != null;

        /// <summary>
        /// Проверить наличие сервиса совподающего по ключу
        /// </summary>
        /// <param name="serviceId">Id текущего сервиса</param>
        /// <param name="key">Ключ</param>
        /// <returns>Наличие сервиса</returns>
        private bool CheckAvailabilityServiceMatchingKey(Guid serviceId, Guid key) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Key == key && s.Id != serviceId) != null;

        /// <summary>
        /// Проверить название сервиса
        /// </summary>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceName(string serviceName, Guid? serviceId = null)
        {
            var validateResults = new List<ValidateResultModelDto>();

            if (string.IsNullOrEmpty(serviceName))
                validateResults.Add(new ValidateResultModelDto
                { Success = false, Message = "Необходимо указать название сервиса" });

            if (!string.IsNullOrEmpty(serviceName) && !RegexName.IsMatch(serviceName))
                validateResults.Add(new ValidateResultModelDto
                { Success = false, Message = "В названии сервиса указаны недопустимые символы / \\ \' \"" });

            if (!string.IsNullOrEmpty(serviceName) && serviceName.Length > MaxLengthServiceName)
                validateResults.Add(new ValidateResultModelDto
                { Message = $"Название сервиса может содержать не более {MaxLengthServiceName} символов" });

            if (serviceId == null)
            {
                var anotherService = dbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
                if (anotherService != null)
                    validateResults.Add(new ValidateResultModelDto
                    { Success = false, Message = "Сервис с таким названием уже существует" });
            }
            else
            {
                var anotherService =
                    dbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName && w.Id != serviceId);
                if (anotherService != null)
                    validateResults.Add(new ValidateResultModelDto
                    { Success = false, Message = "Сервис с таким названием уже существует" });
            }

            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Проверить услуги биллинга
        /// </summary>
        /// <param name="serviceTypes">Услуги биллинга</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceTypes(List<BillingServiceTypeDto> serviceTypes)
        {
            var validateResults = new List<ValidateResultModelDto>
            {
                CheckServiceTypeNames(serviceTypes.Select(w => w.Name).ToList()),
                CheckServiceTypeDescriptions(serviceTypes.Select(st => st.Description).ToList()),
                CheckServiceTypesCost(serviceTypes),
                CheckAvailabilityOfRent1CDependentServiceType(serviceTypes)
            };

            if (!serviceTypes.Any())
                validateResults.Add(new ValidateResultModelDto
                { Success = false, Message = "Необходимо указать хотя бы одну услугу" });

            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Проверить возможность создания заявки на модерацию 
        /// </summary>
        /// <param name="editBillingServiceDto">Модель изменения сервиса</param>
        private ValidateResultModelDto CheckAbilityToCreateRequest(EditBillingServiceDto editBillingServiceDto)
        {
            var validateResults = new List<ValidateResultModelDto>();
            var existChangeRequest =
                dbLayer.GetGenericRepository<ChangeServiceRequest>()
                    .FirstOrDefault(req =>
                        req.BillingServiceId == editBillingServiceDto.Id &&
                        req.Status == ChangeRequestStatusEnum.OnModeration);

            if (existChangeRequest != null)
                validateResults.Add(new ValidateResultModelDto
                {
                    Message = "\"Не возможно создать заявку на модерацию. У вас есть не обработанные заявки\""
                });
          
            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Проверить услуги биллинга
        /// при создании заявки сервиса на модерацию
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceTypesWhenCreatingChangeServiceRequest(
            EditBillingServiceDto editBillingServiceDto)
        {
            if (!editBillingServiceDto.BillingServiceTypes.Any())
                return new ValidateResultModelDto
                {
                    Message = "Необходимо указать хотя бы одну услугу"
                };

            var validateResults = new List<ValidateResultModelDto>
            {
                CheckServiceTypeNames(editBillingServiceDto.BillingServiceTypes.Select(w => w.Name).ToList()),
                CheckServiceTypeDescriptions(editBillingServiceDto.BillingServiceTypes.Select(st => st.Description)
                    .ToList()),
                CheckServiceTypesCost(editBillingServiceDto.BillingServiceTypes)
            };

            var serviceTypeChangesRelations = dbLayer.BillingServiceTypeRepository
                .Where(st => st.ServiceId == editBillingServiceDto.Id).ToList()
                .Join(editBillingServiceDto.BillingServiceTypes, oldSt => oldSt.Id, newSt => newSt.Id,
                    (oldSt, newSt) => new
                    {
                        ServiceType = oldSt,
                        ServiceTypeDto = newSt
                    })
                .ToDictionary(st => st.ServiceType, st => st.ServiceTypeDto);

            var nextPossibleEditServiceCostDate =
                billingServiceCardDataProvider.TryGetNextPossibleEditServiceCostDate(editBillingServiceDto.Id);

            if (serviceTypeChangesRelations.Any(str =>
                    serviceTypeHelper.GetServiceTypeCost(str.Key.Id) != str.Value.ServiceTypeCost) &&
                nextPossibleEditServiceCostDate.HasValue && nextPossibleEditServiceCostDate > DateTime.Now)
                validateResults.Add(new ValidateResultModelDto
                {
                    Message =
                        $"Нельзя изменить стоимость услуги. Следующее изменение стоимости сервиса будет доступно {nextPossibleEditServiceCostDate:dd-MM-yyyy}"
                });

            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Проверить наличие зависимой от Аренды 1С услуги
        /// </summary>
        /// <param name="serviceTypes">Услуги сервиса</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckAvailabilityOfRent1CDependentServiceType(
            List<BillingServiceTypeDto> serviceTypes)
        {
            var serviceType = serviceTypes.FirstOrDefault(w => !w.DependServiceTypeId.IsNullOrEmpty());

            if (serviceType == null)
                return new ValidateResultModelDto
                {
                    Success = false,
                    Message = "Сервис должен иметь хотя бы одну услугу с типом подключения к информационной базе 1С"
                };

            return new ValidateResultModelDto { Success = true };
        }

         /// <summary>
        /// Проверить связи услуг
        /// </summary>
        /// <param name="serviceTypes">Услуги</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceTypeRelations(List<BillingServiceTypeDto> serviceTypes)
        {
            var validateResults = new List<ValidateResultModelDto>();
            ServiceTypes = serviceTypes;
            foreach (var serviceType in serviceTypes)
            {
                ServiceTypeRelations = [];
                SearchParentsServiceType(serviceType.Id);
                var serviceDependenciesFound =
                    serviceType.ServiceTypeRelations.Where(w => ServiceTypeRelations.Contains(w)).ToList();

                if (serviceDependenciesFound.Any())
                    validateResults.Add(new ValidateResultModelDto
                    {
                        Success = false,
                        Message = $"У услуги {serviceType.Name} нарушена зависимость при комбинировании"
                    });
            }

            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Поиск предков услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        private void SearchParentsServiceType(Guid serviceTypeId)
        {
            var serviceDependenciesFound = new List<Guid>();

            var anotherServiceTypes = ServiceTypes.Where(w => w.Id != serviceTypeId).ToList();

            anotherServiceTypes.ForEach(anotherServiceType =>
            {
                var relation = anotherServiceType.ServiceTypeRelations?.FirstOrDefault(w => w == serviceTypeId);
                if (!relation.HasValue || relation == Guid.Empty)
                    return;

                if (ServiceTypeRelations.Contains(anotherServiceType.Id))
                    return;

                ServiceTypeRelations.Add(anotherServiceType.Id);
                serviceDependenciesFound.Add(anotherServiceType.Id);
            });

            serviceDependenciesFound.ForEach(SearchParentsServiceType);
        }

        /// <summary>
        /// Проверить стоимсоть услуг
        /// </summary>
        /// <param name="serviceTypes">Услуг</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceTypesCost(List<BillingServiceTypeDto> serviceTypes)
        {
            var validateResults = new List<ValidateResultModelDto>();
            foreach (var serviceType in serviceTypes)
            {
                if (serviceType.ServiceTypeCost < 0)
                    validateResults.Add(new ValidateResultModelDto
                    {
                        Success = false,
                        Message = $"Стоимость услуги {serviceType.Name} не может быть отрицательным числом"
                    });
            }

            return ProcessValidateResults(validateResults);
        }

        /// <summary>
        /// Проверить названия услуг сервиса
        /// </summary>
        /// <param name="serviceTypeNames">Названия услуг сервиса</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceTypeNames(List<string> serviceTypeNames)
        {
            foreach (var serviceTypeName in serviceTypeNames)
            {
                if (string.IsNullOrEmpty(serviceTypeName))
                    return new ValidateResultModelDto { Success = false, Message = "Необходимо указать название услуги" };

                if (!RegexName.IsMatch(serviceTypeName))
                    return new ValidateResultModelDto
                    {
                        Success = false,
                        Message = $"В названии услуги {serviceTypeName} указаны недопустимые символы / \\ \' \""
                    };

                if (serviceTypeName.Length > MaxLengtServiceTypeName)
                    return new ValidateResultModelDto
                    { Message = $"Название услуги может содержать не более {MaxLengtServiceTypeName} символов" };
            }

            if (serviceTypeNames.Count != serviceTypeNames.Distinct().Count())
                return new ValidateResultModelDto
                { Success = false, Message = "Название каждой услуги должно быть уникальным" };

            return new ValidateResultModelDto { Success = true };
        }

        /// <summary>
        /// Проверить описание услуг
        /// </summary>
        /// <param name="serviceTypeDescriptions">Описание услуг</param>
        /// <returns>Результат проверки</returns>
        private ValidateResultModelDto CheckServiceTypeDescriptions(List<string> serviceTypeDescriptions)
        {
            foreach (var serviceTypeDescription in serviceTypeDescriptions)
            {
                if (string.IsNullOrEmpty(serviceTypeDescription))
                    return new ValidateResultModelDto { Success = false, Message = "Необходимо указать описание услуги" };

                if (serviceTypeDescription.Length > MaxLengtServiceTypeDescription)
                    return new ValidateResultModelDto
                    {
                        Message = $"Описание услуги может содержать не более {MaxLengtServiceTypeDescription} символов"
                    };
            }

            return new ValidateResultModelDto { Success = true };
        }


        /// <summary>
        /// Проверить возможность смены статуса для сервиса
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        private void CheckAbilityToChangeServiceStatus(EditBillingServiceDto editBillingServiceDto)
        {
            var usageOfService =
                dbLayer.ResourceRepository.Where(w => w.BillingServiceType.ServiceId == editBillingServiceDto.Id);

            if (editBillingServiceDto.BillingServiceStatus == BillingServiceStatusEnum.Draft
                && usageOfService.Any())
                throw new InvalidOperationException(
                    $"Невозможно сменить статус сервиса на \"{BillingServiceStatusEnum.Draft.Description()}\", так как к нему уже подключены пользователи");
        }

        /// <summary>
        /// Обработать результаты валидации
        /// </summary>
        /// <param name="validateResults">Результаты валидации</param>
        /// <returns>Результат обработки</returns>
        private ValidateResultModelDto ProcessValidateResults(List<ValidateResultModelDto> validateResults)
        {
            var errorResults = validateResults.Where(w => !w.Success).ToList();

            if (!errorResults.Any())
                return new ValidateResultModelDto { Success = true };

            var builder = new StringBuilder();

            errorResults.ForEach(w => { builder.AppendLine($"{w.Message}"); });

            var message = builder.ToString();

            return new ValidateResultModelDto { Success = false, Message = message };
        }

    }
}
