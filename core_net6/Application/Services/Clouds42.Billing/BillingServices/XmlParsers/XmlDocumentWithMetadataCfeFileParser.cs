﻿using System.Xml;
using Clouds42.Billing.BillingServices.Constants;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService.BillingService1C;

namespace Clouds42.Billing.BillingServices.XmlParsers
{
    /// <summary>
    /// Парсер xml документа с метаданными файла cfe
    /// </summary>
    public static class XmlDocumentWithMetadataCfeFileParser
    {
        /// <summary>
        /// Выбрать метаданные из Xml документа
        /// </summary>
        /// <param name="xmlDocument">Xml документ</param>
        /// <returns>Метаданные файла разработки 1С</returns>
        public static Service1CFileMetadataDto SelectMetadataFromXml(XmlDocument xmlDocument)
        {
            var manifest = xmlDocument?.SelectSingleNode(Service1CFileMetadataConst.ResultTag) ??
                           throw new NotFoundException($"Не удалось получить тэг {Service1CFileMetadataConst.ResultTag}");

            return new Service1CFileMetadataDto
            {
                Synonym = manifest.GetTagValue(Service1CFileMetadataConst.SynonymTag),
                Name = manifest.GetTagValue(Service1CFileMetadataConst.NameTag),
                Version = manifest.GetTagValue(Service1CFileMetadataConst.VersionTag)
            };
        }
    }
}
