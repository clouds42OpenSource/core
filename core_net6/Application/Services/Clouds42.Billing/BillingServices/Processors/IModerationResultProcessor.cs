﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.BillingServices.Processors
{
    /// <summary>
    /// Процессор для обработки результата модерации
    /// заявки на изменения сервиса
    /// </summary>
    public interface IModerationResultProcessor
    {
        /// <summary>
        /// Обработать результат модерации
        /// </summary>
        /// <param name="changeServiceRequestModerationResult">Результат модерации заявки
        /// на изменения сервиса</param>
        void Process(ChangeServiceRequestModerationResultDto changeServiceRequestModerationResult);
    }
}
