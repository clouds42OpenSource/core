﻿using System.Xml;
using Clouds42.Billing.BillingServices.XmlParsers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Services;
using Clouds42.DataContracts.BillingService.BillingService1C;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Billing.BillingServices.Processors.Processes
{
    /// <summary>
    /// Процесс получения метаданных файла разработки 1С
    /// из zip файла
    /// </summary>
    public class GetService1CFileMetadataFromZipFileProcess(IServiceProvider serviceProvider)
        : GetService1CFileMetadataBaseProcess(serviceProvider)
    {
        private readonly IParseZipDevelopmentFileService _parseZipDevelopmentFileService = serviceProvider.GetRequiredService<IParseZipDevelopmentFileService>();

        /// <summary>
        /// Получить xml документ с метаданными файла разработки 1С
        /// </summary>
        /// <param name="model">Входящая модель процесса для получения метаданных</param>
        /// <returns>Xml документ с метаданными файла разработки</returns>
        protected override XmlDocument GetXmlDocumentWithMetadataForService1CFile(
            IncomingModelOfMetadataGettingProcessDto model) => _parseZipDevelopmentFileService.Parse(model.File);

        /// <summary>
        /// Выбрать метаданные из Xml документа
        /// </summary>
        /// <param name="xmlDocument">Xml документ</param>
        /// <returns>Метаданные файла разработки 1С</returns>
        protected override Service1CFileMetadataDto SelectMetadataFromXml(XmlDocument xmlDocument) =>
            XmlDocumentWithMetadataZipFileParser.SelectMetadataFromXml(xmlDocument);
    }
}
