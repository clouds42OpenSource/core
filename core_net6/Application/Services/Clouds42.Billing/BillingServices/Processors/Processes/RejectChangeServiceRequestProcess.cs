﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.BillingServices.Providers;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Processors.Processes
{
    /// <summary>
    /// Процесс по отклонению заявки на изменения сервиса
    /// </summary>
    internal class RejectChangeServiceRequestProcess : BaseModerationResultProvider,
        IChangeServiceRequestProcess<ChangeServiceRequestModerationResultDto>
    {
        /// <summary>
        /// Список правил с сопоставлением действий
        /// для отклонения заявки на модерацию
        /// </summary>
        private readonly
            List<(Func<ChangeRequestTypeEnum, bool>,
                Action<ChangeServiceRequest, ChangeServiceRequestModerationResultDto>)> _listRejectAction;
        private readonly IHandlerException _handlerException;
        public RejectChangeServiceRequestProcess(IUnitOfWork dbLayer,
            IAccessProvider accessProvider,
            IServiceProvider serviceProvider,
            ICreateRateProvider createRateProvider,
            IRateDataProvider rateDataProvider,
            ICreateAccountRateProvider createAccountRateProvider,
            ILogger42 logger, IHandlerException handlerException)
            : base(dbLayer, accessProvider, serviceProvider, createRateProvider, rateDataProvider,
                createAccountRateProvider, logger, handlerException)
        {
            _handlerException = handlerException;
            _listRejectAction =
            [
                (requestType => requestType == ChangeRequestTypeEnum.Creation, RejectCreateServiceRequest),
                (requestType => requestType == ChangeRequestTypeEnum.Editing,
                    RejectEditServiceRequest)
            ];
        }

        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="inputModel">Результат модерации заявки
        /// на изменения сервиса</param>
        public void Execute(ChangeServiceRequestModerationResultDto inputModel)
        {
            var changeServiceRequest = GetChangeServiceRequest(inputModel.Id);
            CheckAbilityProcessChangeServiceRequest(changeServiceRequest, inputModel);

            var rejectChangeServiceRequestAction = _listRejectAction
                                                       .FirstOrDefault(tuple =>
                                                           tuple.Item1(changeServiceRequest.ChangeRequestType))
                                                       .Item2
                                                   ?? throw new InvalidOperationException(
                                                       $"Не удалось найти действие для отклонения заявки {changeServiceRequest.Id} на модерацию.");

            rejectChangeServiceRequestAction(changeServiceRequest, inputModel);
        }

        /// <summary>
        /// Отклонить заявку на создание сервиса
        /// </summary>
        /// <param name="changeServiceRequest">Заявка сервиса на модерацию</param>
        /// <param name="moderationResult">Результат модерации заявки</param>
        private void RejectCreateServiceRequest(ChangeServiceRequest changeServiceRequest,
            ChangeServiceRequestModerationResultDto moderationResult)
        {
            var message = $"Отклонение заявки '{moderationResult.Id}' на создание сервиса";
            Logger.Info(message);

            try
            {
                UpdateChangeServiceRequest(changeServiceRequest, moderationResult);
                changeServiceRequest.BillingService.BillingServiceStatus = BillingServiceStatusEnum.Draft;
                changeServiceRequest.BillingService.StatusDateTime = DateTime.Now;

                DbLayer.BillingServiceRepository.Update(changeServiceRequest.BillingService);
                DbLayer.Save();

                Logger.Info($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка отклонения заявки на создание сервиса] {moderationResult.Id}");
                throw;
            }
        }

        /// <summary>
        /// Отклонить заявку на редактирование сервиса
        /// </summary>
        /// <param name="changeServiceRequest">Заявка сервиса на модерацию</param>
        /// <param name="moderationResult">Результат модерации заявки</param>
        private void RejectEditServiceRequest(ChangeServiceRequest changeServiceRequest,
            ChangeServiceRequestModerationResultDto moderationResult)
        {
            Logger.Info($"Отклонение заявки '{moderationResult.Id}' на редактирование сервиса");
            UpdateChangeServiceRequest(changeServiceRequest, moderationResult);
        }
    }
}
