﻿using Clouds42.DataContracts.BillingService.BillingService1C;
using System.Xml;

namespace Clouds42.Billing.BillingServices.Processors.Processes
{
    /// <summary>
    /// Базовый процесс получения метаданных файла разработки 1С
    /// </summary>
    public abstract class GetService1CFileMetadataBaseProcess(IServiceProvider serviceProvider)
    {
        protected readonly IServiceProvider ServiceProvider = serviceProvider;

        /// <summary>
        /// Получить метаданные файла разработки 1С
        /// </summary>
        /// <param name="model">Входящая модель процесса для получения метаданных</param>
        /// <returns>Метаданные файла разработки 1С</returns>
        public Service1CFileMetadataDto GetMetadata(IncomingModelOfMetadataGettingProcessDto model) =>
            SelectMetadataFromXml(GetXmlDocumentWithMetadataForService1CFile(model));

        /// <summary>
        /// Получить xml документ с метаданными файла разработки 1С
        /// </summary>
        /// <param name="model">Входящая модель процесса для получения метаданных</param>
        /// <returns>Xml документ с метаданными файла разработки</returns>
        protected abstract XmlDocument GetXmlDocumentWithMetadataForService1CFile(
            IncomingModelOfMetadataGettingProcessDto model);

        /// <summary>
        /// Выбрать метаданные из Xml документа
        /// </summary>
        /// <param name="xmlDocument">Xml документ</param>
        /// <returns>Метаданные файла разработки 1С</returns>
        protected abstract Service1CFileMetadataDto SelectMetadataFromXml(XmlDocument xmlDocument);
    }
}
