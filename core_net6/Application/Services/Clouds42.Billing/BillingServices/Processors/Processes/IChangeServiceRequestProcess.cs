﻿namespace Clouds42.Billing.BillingServices.Processors.Processes
{
    /// <summary>
    /// Процесс над заявкой на изменение сервиса
    /// </summary>
    public interface IChangeServiceRequestProcess<in TInputModel>
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="inputModel">Входная модель данных</param>
        void Execute(TInputModel inputModel);
    }
}