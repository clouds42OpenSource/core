﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Logger;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.BillingServices.Providers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BillingService;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Services;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Processors.Processes
{
    /// <summary>
    /// Процесс по уведомлению о результате модерации
    /// заявки сервиса
    /// </summary>
    internal class NotifyAboutModerationResultProcess(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IServiceProvider serviceProvider,
        ICreateRateProvider createRateProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        IRateDataProvider rateDataProvider,
        ICreateAccountRateProvider createAccountRateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : BaseModerationResultProvider(dbLayer,
                accessProvider, serviceProvider, createRateProvider, rateDataProvider, createAccountRateProvider,
                logger, handlerException),
            IChangeServiceRequestProcess<ChangeServiceRequestModerationResultDto>
    {
        private readonly Lazy<string> _cpSite = new(CloudConfigurationProvider.Cp.GetSiteAuthorityUrl);
        private readonly Lazy<string> _routeValueForOpenBillingServiceCard = new(CloudConfigurationProvider.Cp.GetRouteValueForOpenBillingServiceCard);

        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="inputModel">Результат модерации заявки
        /// на изменения сервиса</param>
        public void Execute(ChangeServiceRequestModerationResultDto inputModel)
        {
            var changeServiceRequest = GetChangeServiceRequest(inputModel.Id);

            if (changeServiceRequest.BillingService.AccountOwnerId == null)
                throw new InvalidOperationException(
                    $"У сервиса {changeServiceRequest.BillingService.Name} не указан владелец");

            Logger.Info(
                $"Уведомление аккаунта {changeServiceRequest.BillingService.AccountOwnerId.Value}" +
                $" о результате модерации сервиса {changeServiceRequest.BillingService.Name}. Результат модерации= {inputModel.Status.Description()}");

            var sendStatus =
                letterNotificationProcessor
                    .TryNotify<ModerationServiceResultLetterNotification, ModerationServiceResultLetterModelDto>(
                        new ModerationServiceResultLetterModelDto
                        {
                            AccountId = changeServiceRequest.BillingService.AccountOwnerId.Value,
                            ModerationResult = inputModel,
                            ServiceName = changeServiceRequest.BillingService.Name,
                            UrlForOpenBillingServiceCard =
                                GetUrlForOpenBillingServiceCard(changeServiceRequest.BillingServiceId)
                        });

            Logger.Info(
                $"Отправили ли сообщение клиенту - {sendStatus}, Аккаунт - {changeServiceRequest.BillingService.AccountOwnerId.Value}");
        }

        /// <summary>
        /// Получить ссылку для открытия карточки сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Ссылка для открытия карточки сервиса</returns>
        private string GetUrlForOpenBillingServiceCard(Guid serviceId) =>
            $"{_cpSite.Value}/{_routeValueForOpenBillingServiceCard.Value}?serviceId={serviceId}&viewMode=true";
    }
}
