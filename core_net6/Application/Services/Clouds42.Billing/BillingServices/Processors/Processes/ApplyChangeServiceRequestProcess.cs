﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Microsoft.Extensions.Configuration;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.BillingServices.Providers;
using Clouds42.DataContracts.BillingService;
using Clouds42.CoreWorker.AccountJobs.BillingServiceTypes;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.BillingServices.Processors.Processes
{
    /// <summary>
    /// Процесс по принятию заявки на изменения сервиса
    /// </summary>
    internal class ApplyChangeServiceRequestProcess : BaseModerationResultProvider,
        IChangeServiceRequestProcess<ChangeServiceRequestModerationResultDto>
    {
        private readonly ServiceTypeHelperForBillingServiceOperations _serviceTypeHelper;

        private readonly ProcessDeletedServiceTypesForAccountsJobWrapper
            _processDeletedServiceTypesForAccountsJobWrapper;
        private readonly IHandlerException _handlerException;
        private readonly IConfiguration _configuration;
        /// <summary>
        /// Список правил с сопоставлением действий
        /// для принятия заявки на модерацию
        /// </summary>
        private readonly
            List<(Func<ChangeRequestTypeEnum, bool>,
                Action<ChangeServiceRequest, ChangeServiceRequestModerationResultDto>)> _listApplyAction;

        public ApplyChangeServiceRequestProcess(IUnitOfWork dbLayer, IAccessProvider accessProvider,
            ICreateRateProvider createRateProvider,
            IServiceProvider serviceProvider,
            ServiceTypeHelperForBillingServiceOperations serviceTypeHelper,
            ProcessDeletedServiceTypesForAccountsJobWrapper processDeletedServiceTypesForAccountsJobWrapper,
            IRateDataProvider rateDataProvider,
            ICreateAccountRateProvider createAccountRateProvider,
            ILogger42 logger, IConfiguration configuration, IHandlerException handlerException) : base(dbLayer, accessProvider, serviceProvider,
            createRateProvider,
            rateDataProvider, createAccountRateProvider, logger, handlerException)
        {
            _serviceTypeHelper = serviceTypeHelper;
            _processDeletedServiceTypesForAccountsJobWrapper = processDeletedServiceTypesForAccountsJobWrapper;
            _listApplyAction =
            [
                (requestType => requestType == ChangeRequestTypeEnum.Creation, ApplyCreateServiceRequest),
                (requestType => requestType == ChangeRequestTypeEnum.Editing, ApplyEditServiceRequest)
            ];
            _configuration = configuration;
            _handlerException = handlerException;
        }

        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="inputModel">Результат модерации заявки
        /// на изменения сервиса</param>
        public void Execute(ChangeServiceRequestModerationResultDto inputModel)
        {
            var changeServiceRequest = GetChangeServiceRequest(inputModel.Id);
            CheckAbilityProcessChangeServiceRequest(changeServiceRequest, inputModel);

            var applyChangeServiceRequestAction = _listApplyAction
                                                      .FirstOrDefault(tuple =>
                                                          tuple.Item1(changeServiceRequest.ChangeRequestType))
                                                      .Item2
                                                  ?? throw new InvalidOperationException(
                                                      $"Не удалось найти действие для обработки результата модерации заявки {changeServiceRequest.Id}");

            applyChangeServiceRequestAction(changeServiceRequest, inputModel);
        }

        /// <summary>
        /// Принять заявку на создание сервиса
        /// </summary>
        /// <param name="changeServiceRequest">Заявка сервиса на модерацию</param>
        /// <param name="moderationResult">Результат модерации заявки</param>
        private void ApplyCreateServiceRequest(ChangeServiceRequest changeServiceRequest,
            ChangeServiceRequestModerationResultDto moderationResult)
        {
            var message = $"Принятие заявки '{moderationResult.Id}' на создание сервиса";
            Logger.Info(message);

            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                UpdateChangeServiceRequest(changeServiceRequest, moderationResult);
                changeServiceRequest.BillingService.BillingServiceStatus = BillingServiceStatusEnum.IsActive;
                changeServiceRequest.BillingService.StatusDateTime = DateTime.Now;
                changeServiceRequest.BillingService.ServiceActivationDate = DateTime.Now;
                DbLayer.BillingServiceRepository.Update(changeServiceRequest.BillingService);
                DbLayer.Save();

                transaction.Commit();

                Logger.Info($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка принятия заявки на создание сервиса] {moderationResult.Id}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Принять заявку на редактирования сервиса
        /// </summary>
        /// <param name="changeServiceRequest">Заявка сервиса на модерацию</param>
        /// <param name="moderationResult">Результат модерации заявки</param>
        private void ApplyEditServiceRequest(ChangeServiceRequest changeServiceRequest,
            ChangeServiceRequestModerationResultDto moderationResult)
        {
            var message = $"Принятие заявки '{moderationResult.Id}' на редактирование сервиса";
            Logger.Info(message);

            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                var billingService = changeServiceRequest.BillingService;
                var billingServiceChanges = GetBillingServiceChanges(changeServiceRequest.Id);

                billingService.Name = GetNewValueIfNonEmpty(billingService.Name, billingServiceChanges.Name);

                billingService.ShortDescription =
                    GetNewValueIfNonEmpty(billingService.ShortDescription, billingServiceChanges.ShortDescription);

                billingService.Opportunities = GetNewValueIfNonEmpty(billingService.Opportunities,
                    billingServiceChanges.Opportunities);

                billingService.IsHybridService = billingServiceChanges.IsHybridService;

                billingService.BillingServiceStatus = BillingServiceStatusEnum.IsActive;

                DbLayer.BillingServiceRepository.Update(billingService);
                DbLayer.Save();

                var serviceTypeChangesRelations =
                    _serviceTypeHelper.JoinServiceTypesWithItsChanges(billingService.BillingServiceTypes,
                        billingServiceChanges.BillingServiceTypeChanges);

                UpdateBillingServiceTypes(billingService.Id, billingServiceChanges.Id, serviceTypeChangesRelations);

                if (billingService.AccountOwnerId == null)
                    throw new InvalidOperationException(
                        $"Не удалось определить вледельца сервиса {billingService.Name}");

                var newServiceTypes =
                    billingServiceChanges.BillingServiceTypeChanges.Where(st => st.IsNew).ToList();

                var locale = DbLayer.AccountsRepository.GetLocale(billingService.AccountOwnerId.Value, 
                    Guid.Parse(_configuration["DefaultLocale"]?? ""));

                var billingServiceTypesForRemove = GetBillingServiceTypesForRemove(billingService,
                    billingServiceChanges.BillingServiceTypeChanges.ToList());

                CreateNewServiceTypes(billingService.Id, newServiceTypes, locale.ID);

                UpdateChangeServiceRequest(changeServiceRequest, moderationResult);

                transaction.Commit();

                StartTaskForProcessDeletedServiceTypesForAccounts(billingServiceChanges.Id,
                    billingServiceTypesForRemove);

                Logger.Info($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка принятия заявки на редактирование сервиса] {moderationResult.Id}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Запустить задачу по обработке удаленных услуг для аккаунтов
        /// </summary>
        /// <param name="billingServiceChangesId">Id изменений сервиса</param>
        /// <param name="billingServiceTypesForRemove">Список услуг сервиса для удаления</param>
        private void StartTaskForProcessDeletedServiceTypesForAccounts(Guid billingServiceChangesId,
            List<BillingServiceType> billingServiceTypesForRemove)
        {
            if (!billingServiceTypesForRemove.Any())
                return;

            _processDeletedServiceTypesForAccountsJobWrapper.Start(new ProcessDeletedServiceTypesForAccountsDto
            {
                BillingServiceChangesId = billingServiceChangesId,
                ServiceId = billingServiceTypesForRemove.First().ServiceId,
                ServiceTypeIds = billingServiceTypesForRemove.Select(st => st.Id).ToList()
            });
        }

    }
}
