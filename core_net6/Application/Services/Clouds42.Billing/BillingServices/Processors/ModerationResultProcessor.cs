﻿using Clouds42.Billing.BillingServices.Processors.Processes;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Billing.BillingServices.Processors
{
    /// <summary>
    /// Процессор для обработки результата модерации
    /// заявки на изменения сервиса
    /// </summary>
    internal class ModerationResultProcessor : IModerationResultProcessor
    {
        private readonly NotifyAboutModerationResultProcess _notifyAboutModerationResultProcess;

        /// <summary>
        /// Список правил с сопоставлением процессов
        /// для обработки результата модерации
        /// </summary>
        private readonly
            List<(Func<ChangeRequestStatusEnum, bool>,
                IChangeServiceRequestProcess<ChangeServiceRequestModerationResultDto>)> _moderationResultProcessesList;

        public ModerationResultProcessor(ApplyChangeServiceRequestProcess applyChangeServiceRequestProcess,
            RejectChangeServiceRequestProcess rejectChangeServiceRequestProcess,
            NotifyAboutModerationResultProcess notifyAboutModerationResultProcess)
        {
            _notifyAboutModerationResultProcess = notifyAboutModerationResultProcess;
            _moderationResultProcessesList =
            [
                (serviceStatus => serviceStatus == ChangeRequestStatusEnum.Implemented,
                    applyChangeServiceRequestProcess),
                (serviceStatus => serviceStatus == ChangeRequestStatusEnum.Rejected, rejectChangeServiceRequestProcess)
            ];
        }

        /// <summary>
        /// Обработать результат модерации
        /// </summary>
        /// <param name="changeServiceRequestModerationResult">Результат модерации заявки
        /// на изменения сервиса</param>
        public void Process(ChangeServiceRequestModerationResultDto changeServiceRequestModerationResult)
        {
            var moderationResultProcess = _moderationResultProcessesList
                                    .FirstOrDefault(tuple => tuple.Item1(changeServiceRequestModerationResult.Status))
                                    .Item2
                                ?? throw new InvalidOperationException(
                                    $"Не удалось найти действие для обработки результата модерации со статусом {changeServiceRequestModerationResult.Status}");

            moderationResultProcess.Execute(changeServiceRequestModerationResult);
            _notifyAboutModerationResultProcess.Execute(changeServiceRequestModerationResult);
        }
    }
}
