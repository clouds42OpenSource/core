﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Processors;
using Clouds42.Billing.BillingServices.Processors.Processes;
using Clouds42.Billing.BillingServices.Mappers;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.BillingService.BillingService1C;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Billing.BillingServices.Processors
{
    /// <summary>
    /// Процессор для получения метаданных файла разработки 1С
    /// </summary>
    internal class GetService1CFileMetadataProcessor : IGetService1CFileMetadataProcessor
    {
        /// <summary>
        /// Список создателей процессов
        /// </summary>
        private readonly
            List<(Func<Service1CFileExtensionEnum, bool> CompareFileExtension, Func<GetService1CFileMetadataBaseProcess>
                CreateProcess)> _listProcessCreators;

        public GetService1CFileMetadataProcessor(IServiceProvider serviceProvider)
        {
            _listProcessCreators =
            [
                (extension => extension == Service1CFileExtensionEnum.Zip,
                    () => serviceProvider.GetRequiredService<GetService1CFileMetadataFromZipFileProcess>())

            ];
        }

        /// <summary>
        /// Получить метаданные файла разработки 1С
        /// </summary>
        /// <param name="file">Файл разработки 1С</param>
        /// <returns>Метаданные файла разработки 1С</returns>
        public Service1CFileMetadataDto GetMetadata(CloudFileDataDto<IFormFile> file) =>
            CreateProcess(file.FileName).GetMetadata(file.PerformMapping());

        /// <summary>
        /// Создать процесс получения метаданных файла разработки 1С
        /// </summary>
        /// <param name="fileName">Название файла разработки</param>
        /// <returns>Процесс получения метаданных файла разработки 1С</returns>
        private GetService1CFileMetadataBaseProcess CreateProcess(string fileName)
        {
            var foundCreator = _listProcessCreators
                .FirstOrDefault(creator =>
                    creator.CompareFileExtension(fileName.DefineEnumTypeForService1CFileExtension()));

            if (foundCreator == default)
                throw new InvalidOperationException(
                    $"Не удалось определить процесс получения метаданных для файла {fileName}");

            return foundCreator.CreateProcess();
        }
    }
}
