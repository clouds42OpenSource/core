﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для работы с данными дополнительных ресурсов аккаунта
    /// </summary>
    public class AdditionalResourcesDataProvider(IUnitOfWork dbLayer) : IAdditionalResourcesDataProvider
    {
        /// <summary>
        /// Получить данные дополнительных ресурсов аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель данных о дополнительных ресурсах аккаунта</returns>
        public AdditionalResourcesDataDto GetAdditionalResourcesData(Guid accountId) =>
            (from billingAccount in dbLayer.BillingAccountRepository.WhereLazy(account =>
                    account.Id == accountId)
             join accountConfiguration in dbLayer.GetGenericRepository<AccountConfiguration>()
                     .WhereLazy() on
                 billingAccount.Id equals accountConfiguration.AccountId
             select new AdditionalResourcesDataDto
             {
                 AdditionalResourceName = billingAccount.AdditionalResourceName,
                 AdditionalResourceCost = billingAccount.AdditionalResourceCost,
                 IsVipAccount = accountConfiguration.IsVip
             }).FirstOrDefault() ??
            throw new NotFoundException(
                $"Не удалось получить данные дополнительных ресурсов аккаунта '{accountId}'");
    }
}