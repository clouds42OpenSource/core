﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер смены локали
    /// </summary>
    public class ChangeLocaleProvider(
        IUnitOfWork dbLayer,
        IResourcesService resourcesService,
        CalculationOfInvoiceProvider calculationOfInvoiceProvider,
        ICheckAccountDataProvider checkAccountProvider,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        IRegisterChangeOfLocaleForAccountProvider registerChangeOfLocaleForAccountProvider,
        IUpdateAccountConfigurationProvider updateAccountConfigurationProvider,
        IRateProvider rateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Сменить локаль для аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="localeId">Локаль для аккаунта</param>
        public void ChangeLocaleForAccount(Account account, Guid localeId)
        {
            var checkResult = checkAccountProvider.CanChangeLocaleForAccount(account.Id);
            if (!checkResult.Item1)
                throw new InvalidOperationException($"Невозможно сменить локаль для аккаунта {account.Id} по причине : {checkResult.Item2}");

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                logger.Info($"[{account.Id}]: Начало смены локали на {localeId}");
                updateAccountConfigurationProvider.UpdateLocaleId(account.Id, localeId);
                RecalculateCostResources(account.Id);
                RecalculateInvoicesForAccount(account);
                registerChangeOfLocaleForAccountProvider.Register(account.Id);

                transaction.Commit();
                logger.Trace($"[{account.Id}]:Cмена локали для аккаунта завершена");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"{account.Id} [Ошибка смены локали]");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Пересчитать стоимость ресурсов для аккаунта
        /// </summary>
        /// <param name="accountId">Индификатор аккаунта</param>
        private void RecalculateCostResources(Guid accountId)
        {
            logger.Info($"[{accountId}]: Пересчет ресурсов для аккаунта");
            try
            {
                var accountResourcesConfiguration =
                    dbLayer.ResourceConfigurationRepository.FirstOrDefault(a =>
                        a.AccountId == accountId && a.BillingService.SystemService == Clouds42Service.MyEnterprise);

                if (accountResourcesConfiguration == null)
                    return;

                var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(accountId);

                var myEntUser =
                    accountResourcesConfiguration.BillingService.BillingServiceTypes.FirstOrDefault(x =>
                        x.SystemServiceType == ResourceType.MyEntUser);

                var myEntUserWeb =
                    accountResourcesConfiguration.BillingService.BillingServiceTypes.FirstOrDefault(x =>
                        x.SystemServiceType == ResourceType.MyEntUserWeb);

                if (myEntUser == null || myEntUserWeb == null)
                    return;

                var myEntRate = rateProvider.GetOptimalRate(billingAccount.Id,myEntUser.Id);
                var myEntRateWeb = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

                UpdateResourceCost(accountId, ResourceType.MyEntUser, myEntRate.Cost);
                UpdateResourceCost(accountId, ResourceType.MyEntUserWeb, myEntRateWeb.Cost);
                recalculateResourcesConfigurationCostProvider.Recalculate(accountId, accountResourcesConfiguration.BillingService);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"{accountId}: [Ошибка пересчета ресурсов] ");

                throw;
            }
        }

        /// <summary>
        /// Пересчитать счета для аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        private void RecalculateInvoicesForAccount(Account account)
        {
            logger.Info($"[{account.Id}]: Пересчет счетов для аккаунта");

            try
            {
                var invoices = dbLayer.InvoiceRepository
               .Where(a => a.AccountId == account.Id && a.State == InvoiceStatus.Processing.ToString() && a.Sum > 0).ToList();

                if (!invoices.Any())
                {
                    logger.Info($"[{account.Id}]: Для аккаунта нет счетов для пересчета");
                    return;
                }

                var defaultPeriod = CloudConfigurationProvider.Invoices.DefaultPeriodForInvoice();

                foreach (var invoice in invoices)
                {
                    var invoiceProducts =
                        invoice.InvoiceProducts.Where(i =>
                            i.ServiceSum > 0 && i.ServiceTypeId != null &&
                            i.ServiceType.SystemServiceType != null &&
                            i.ServiceType.Service.SystemService != Clouds42Service.Esdl &&
                            i.ServiceType.Service.SystemService != Clouds42Service.Recognition).ToList();

                    if (!invoiceProducts.Any())
                        continue;

                    var calculationOfInvoiceRequestModel = new CalculationOfInvoiceRequestModel
                    {
                        MyDiskSize = invoiceProducts.FirstOrDefault(w => w.ServiceType.SystemServiceType == ResourceType.DiskSpace)?.Count ?? 0,
                        PayPeriod = invoice.Period == null ? (PayPeriod)defaultPeriod : (PayPeriod)invoice.Period,
                        CalculationBaseBillingServiceTypes = invoiceProducts.Where(w => w.ServiceType.SystemServiceType != ResourceType.DiskSpace).Select(w =>
                            new CalculationBaseBillingServiceTypeModel
                            {
                                Id = w.ServiceTypeId.Value,
                                CountLicenses = w.Count

                            }).ToList()
                    };

                    var calculationsOfInvoiceResult =
                        calculationOfInvoiceProvider.GetCalculationsOfInvoiceResult(account.Id,
                            calculationOfInvoiceRequestModel);

                    invoice.Sum = calculationsOfInvoiceResult.TotalSum;
                    dbLayer.InvoiceRepository.Update(invoice);
                    UpdateCostInvoiceProducts(invoiceProducts, calculationsOfInvoiceResult);

                    dbLayer.Save();
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"{account.Id}: [Ошибка пересчета счетов для аккаунта]");
                throw;
            }
        }

        /// <summary>
        /// Обновить стоимость продуктам счета
        /// </summary>
        /// <param name="invoiceProducts">Продукты счета</param>
        /// <param name="calculationOfInvoiceResultModel">Результат калькуляции</param>
        private void UpdateCostInvoiceProducts(List<InvoiceProduct> invoiceProducts,
            CalculationOfInvoiceResultModel calculationOfInvoiceResultModel)
        {
            invoiceProducts.ForEach(i =>
            {
                i.ServiceSum = calculationOfInvoiceResultModel.CalculationBaseBillingServiceTypes
                                   .FirstOrDefault(w => w.Id == i.ServiceTypeId)?.Amount ?? 0;

                dbLayer.InvoiceProductRepository.Update(i);
            });

            var invoiceProductForMyDisk =
                invoiceProducts.FirstOrDefault(w => w.ServiceType.SystemServiceType == ResourceType.DiskSpace);

            if (invoiceProductForMyDisk == null)
                return;

            invoiceProductForMyDisk.ServiceSum = calculationOfInvoiceResultModel.MyDiskCost;
            dbLayer.InvoiceProductRepository.Update(invoiceProductForMyDisk);
        }

        /// <summary>
        /// Обновить стоимость ресурса
        /// </summary>
        /// <param name="accountId">гуид аккаунта</param>
        /// <param name="resourceType">тип ресурса</param>
        /// <param name="cost">Стоимость</param>
        private void UpdateResourceCost(Guid accountId, ResourceType resourceType, decimal cost)
        {
            var resources = resourcesService
                .GetResources(accountId, Clouds42Service.MyEnterprise, resourceType)
                .Select(x => 
                { 
                    x.Cost = cost;

                    return x;
                });

            dbLayer.BulkUpdate(resources);
            dbLayer.Save();
        }
    }
}
