﻿using Clouds42.Billing.Billing.Models;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.Extensions;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;

namespace Clouds42.Billing.Billing.Providers
{
    internal class PayboxCryptoProvider
    {
        private readonly ILogger42 _logger = Logger42.GetLogger();
        /// <summary>
        ///     Получение Url для перехода на Paybox
        /// </summary>
        /// <param name="paymentSum"></param>
        /// <param name="paymentId"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static string GetPayboxRedirectUrl(decimal paymentSum, int paymentId, Guid supplierId, string description = "")
        {
            var randomString = EncryptionToMd5Hash.GenerateRandomString();
            var merchantId = CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetMerchantId(supplierId);
            var resultUrl = ConfigurationHelper.GetFullPathToAction(CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetFullResultUrl(supplierId));
            var siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
            var successAndFailUrl = ConfigurationHelper.GetFullPathToAction(CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetFullSuccessOrFailUrl(supplierId));
            var payboxPaymentSite = CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetPayboxPaymentSite(supplierId);
            var payboxTestingMode = CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetPayboxTestingMode(supplierId);

            var initPayModel = new PayboxInitPaymentModel
            {
                pg_amount = paymentSum,
                pg_description = description,
                pg_failure_url = successAndFailUrl,
                pg_merchant_id = merchantId,
                pg_order_id = paymentId,
                pg_result_url = resultUrl,
                pg_request_method = "POST",
                pg_salt = randomString,
                pg_site_url = siteUrl,
                pg_success_url = successAndFailUrl,
                pg_testing_mode = payboxTestingMode,
                pg_currency = "KZT"
            };

            initPayModel.pg_sig = initPayModel.GenerateSignature(supplierId);

            string url = payboxPaymentSite +
                         "pg_merchant_id=" + initPayModel.pg_merchant_id +
                         "&pg_salt=" + initPayModel.pg_salt +
                         "&pg_amount=" + initPayModel.pg_amount +
                         "&pg_order_id=" + initPayModel.pg_order_id +
                         "&pg_description=" + initPayModel.pg_description +
                         "&pg_result_url=" + initPayModel.pg_result_url +
                         "&pg_request_method=" + initPayModel.pg_request_method +
                         "&pg_success_url=" + initPayModel.pg_success_url +
                         "&pg_failure_url=" + initPayModel.pg_failure_url +
                         "&pg_site_url=" + initPayModel.pg_site_url +
                         "&pg_testing_mode=" + initPayModel.pg_testing_mode +
                         "&pg_sig=" + initPayModel.pg_sig +
                         "&pg_currency=KZT";
            return url;
        }

        /// <summary>
        ///     Генерирование Xml файла для ответа Paybox-у
        /// </summary>
        /// <param name="salt">Случайная строка с запроса</param>
        /// <param name="supplierId"></param>
        /// <param name="isError">Если нужно сгенерировать ответ "Ошибка"</param>
        /// <param name="failureDescription">Описание ошибки, если она есть</param>
        /// <returns>Сериализованый в строку Xml файл</returns>
        public string BuildResponseXml(string salt, Guid supplierId, bool isError = false, string failureDescription = "")
        {
            if (string.IsNullOrWhiteSpace(salt))
            {
                _logger.Info("Случайная строка пуста или содержит пробелы!!");
                throw new InvalidOperationException("Случайная строка пуста или содержит пробелы");
            }

            var xmlDocument = new PayboxResultResponse();

            if (isError)
            {
                _logger.Info("Начинаем формировать Xml с ответом 'error'");
                xmlDocument.pg_error_description = failureDescription;
                xmlDocument.pg_description = failureDescription;
                xmlDocument.pg_status = "error";
            }
            else
            {
                _logger.Info("Начинаем формировать Xml с ответом 'ok'");
                xmlDocument.pg_status = "ok";
            }

            xmlDocument.pg_salt = salt;
            xmlDocument.pg_sig = xmlDocument.GenerateSignature(supplierId);

            var serialXml = xmlDocument.SerializeToXml();

            if (string.IsNullOrEmpty(serialXml))
                throw new InvalidOperationException("PayboxXmlBuilder :: Что то пошло не так, не удалось сериализировать файл");

            _logger.Info("Сгенерировали Xml для ответа Paybox-у");

            return serialXml;
        }

        /// <summary>
        ///     Метод добавления пароля
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static string AppendSecretKey(string inputData, Guid supplierId)
        {
            var secretKey = CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetSecretKey(supplierId);
            if (inputData.Length == 0)
                throw new InvalidOperationException("Строка пуста");

            inputData += $"{secretKey}";

            return inputData;
        }

        /// <summary>
        ///     Метод генерации подписи в виде MD5
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public string GenerateMd5Hash(string inputData)
        {
            if (inputData.Length == 0)
                throw new InvalidOperationException("Строка пуста");

            return EncryptionToMd5Hash.ComputeMd5Hash(inputData);
        }

        /// <summary>
        ///     Сравниваем строки
        /// </summary>
        /// <param name="firstSignature">Сгенерированная подпись</param>
        /// <param name="secondSignature">Входящая подпись из запроса</param>
        /// <returns>Одинаковы или нет</returns>
        public bool IsSignatureValid(string firstSignature, string secondSignature)
        {
            if (string.IsNullOrWhiteSpace(firstSignature))
            {
                _logger.Warn("Сгенерированная подпись не должна быть пустой или содержать пробелы");
                return false;
            }

            if (string.IsNullOrWhiteSpace(secondSignature))
            {
                _logger.Warn("Входящая подпись не должна быть пустой или содержать пробелы");
                return false;
            }

            return firstSignature.Equals(secondSignature, StringComparison.OrdinalIgnoreCase);
        }
    }
}
