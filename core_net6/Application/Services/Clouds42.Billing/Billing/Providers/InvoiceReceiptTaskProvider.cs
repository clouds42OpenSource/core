﻿using Clouds42.CoreWorker.JobWrappers.Invoices;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для создания задач по получению фискального чека
    /// </summary>
    public class InvoiceReceiptTaskProvider(CreateInvoiceFiscalReceiptJobWrapper createInvoiceFiscalReceiptJobWrapper)
    {
        /// <summary>
        ///     Создать задачу на получение фискального чека
        /// </summary>
        /// <param name="invoiceId">ID счета на оплату</param>
        public void CreateFiscalReceiptTask(Guid invoiceId)
        {
            createInvoiceFiscalReceiptJobWrapper.Start(new CreateInvoiceFiscalReceiptJobParamsDc
            {
                InvoiceId = invoiceId
            });
        }
    }
}