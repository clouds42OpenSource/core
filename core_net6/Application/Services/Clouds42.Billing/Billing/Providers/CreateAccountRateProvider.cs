﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для создания тарифа услуги на аккаунт
    /// </summary>
    internal class CreateAccountRateProvider(IUnitOfWork dbLayer) : ICreateAccountRateProvider
    {
        /// <summary>
        /// Создать тариф услуги на аккаунт
        /// </summary>
        /// <param name="model">Модель создания тарифа на аккаунт</param>
        public void Create(CreateAccountRateDto model)
        {
            var accountRate = new AccountRate
            {
                Id = Guid.NewGuid(),
                AccountId = model.AccountId,
                BillingServiceTypeId = model.BillingServiceTypeId,
                Cost = model.Cost
            };

            dbLayer.AccountRateRepository.Insert(accountRate);
            dbLayer.Save();
        }
    }
}
