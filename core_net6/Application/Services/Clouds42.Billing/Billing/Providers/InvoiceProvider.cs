﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Locales;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для работы со счетом
    /// </summary>
    internal class InvoiceProvider : IInvoiceProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly IAccessProvider _accessProvider;
        private readonly ILogger42 _logger;
        private readonly IResourceConfigurationDataProvider _resourceConfigurationDataProvider;
        private readonly IBillingServiceInfoProvider _billingServiceInfoProvider;
        private readonly CalculationOfInvoiceProvider _calculationOfInvoiceProvider;
        private readonly ISystemServiceTypeDataProvider _systemServiceTypeDataProvider;
        private readonly IMyDatabasesServiceTypeBillingDataProvider _myDatabasesServiceTypeBillingDataProvider;
        private readonly IAdditionalResourcesDataProvider _additionalResourcesDataProvider;
        private readonly IAccountRequisitesDataProvider _accountRequisitesDataProvider;
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider;
        private readonly ICloudLocalizer _cloudLocalizer;

        private readonly IDictionary<PayPeriod, int> _mapPayPeriodToBonusReward;

        public InvoiceProvider(IUnitOfWork dbLayer,
            IAccessProvider accessProvider,
            IResourceConfigurationDataProvider resourceConfigurationDataProvider,
            IBillingServiceInfoProvider billingServiceInfoProvider,
            CalculationOfInvoiceProvider calculationOfInvoiceProvider,
            ISystemServiceTypeDataProvider systemServiceTypeDataProvider,
            IMyDatabasesServiceTypeBillingDataProvider myDatabasesServiceTypeBillingDataProvider,
            IAdditionalResourcesDataProvider additionalResourcesDataProvider,
            IAccountRequisitesDataProvider accountRequisitesDataProvider,
            IAccountConfigurationDataProvider accountConfigurationDataProvider,
            ILogger42 logger, ICloudLocalizer cloudLocalizer)
        {
            _dbLayer = dbLayer;
            _accessProvider = accessProvider;
            _resourceConfigurationDataProvider = resourceConfigurationDataProvider;
            _billingServiceInfoProvider = billingServiceInfoProvider;
            _calculationOfInvoiceProvider = calculationOfInvoiceProvider;
            _systemServiceTypeDataProvider = systemServiceTypeDataProvider;
            _myDatabasesServiceTypeBillingDataProvider = myDatabasesServiceTypeBillingDataProvider;
            _additionalResourcesDataProvider = additionalResourcesDataProvider;
            _accountRequisitesDataProvider = accountRequisitesDataProvider;
            _accountConfigurationDataProvider = accountConfigurationDataProvider;
            _logger = logger;
            _cloudLocalizer = cloudLocalizer;

            var bonusRewardFor3Month =
                new Lazy<int>(CloudConfigurationProvider.Invoices.GetBonusRewardFor3MonthPayPeriod);
            var bonusRewardFor6Month =
                new Lazy<int>(CloudConfigurationProvider.Invoices.GetBonusRewardFor6MonthPayPeriod);
            var bonusRewardFor12Month =
                new Lazy<int>(CloudConfigurationProvider.Invoices.GetBonusRewardFor12MonthPayPeriod);

            _mapPayPeriodToBonusReward = new Dictionary<PayPeriod, int>
            {
                {PayPeriod.None, 0},
                {PayPeriod.Month1, 0},
                {PayPeriod.Month3, bonusRewardFor3Month.Value},
                {PayPeriod.Month6, bonusRewardFor6Month.Value},
                {PayPeriod.Month12, bonusRewardFor12Month.Value}
            };
        }

        /// <summary>
        /// Получить или создать счет по конфигурации ресурса
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="billingAccount">Аккаунт билинга</param>
        /// <returns>Счет</returns>
        public Domain.DataModels.billing.Invoice GetOrCreateInvoiceByResourcesConfiguration(ResourcesConfiguration resourcesConfiguration,
            BillingAccount billingAccount)
        {
            var lastInvoicePaid = GetLatestPaidInvoiceFromAccount(billingAccount.Id);
            var period = lastInvoicePaid?.Period ?? CloudConfigurationProvider.Invoices.DefaultPeriodForInvoice();

            var resConfModel = _resourceConfigurationDataProvider.GetConfigWithDependencies(resourcesConfiguration);
            var totalCost = _resourceConfigurationDataProvider.GetTotalServiceCost(resConfModel) * period;

            var lastActualInvoiceForService = GetRecentUnpaidInvoiceFromAccount(billingAccount.Id, totalCost);

            if (lastActualInvoiceForService != null)
            {
                RecalculateBonusRewardForExistingInvoice(lastActualInvoiceForService, billingAccount);
                return lastActualInvoiceForService;
            }

            if (lastInvoicePaid != null)
            {
                var lastActualInvoiceForSuggestedPayment =
                    GetRecentUnpaidInvoiceFromAccount(billingAccount.Id, lastInvoicePaid.Sum);

                if (lastActualInvoiceForSuggestedPayment != null)
                    return lastActualInvoiceForSuggestedPayment;
            }

            if (resourcesConfiguration.BillingService.SystemService is Clouds42Service.MyEnterprise or null)
            {
                var newInvoice = AddInvoiceForBillingService(billingAccount, resConfModel, period, totalCost);
                return newInvoice;
            }

            var invoice = AddInvoice(billingAccount.Account, totalCost, period);

            WriteLogEventCreatingInvoice(invoice, $"на {invoice.Period ?? 0} мес.");

            return invoice;
        }

        /// <summary>
        /// Записать в лог событие "Создание счета"
        /// </summary>
        /// <param name="invoice">Счет</param>
        /// <param name="invoiceSumPurpose">Назначение суммы счета</param>
        private void WriteLogEventCreatingInvoice(Domain.DataModels.billing.Invoice invoice, string invoiceSumPurpose)
        {
            var localeName = _accountConfigurationDataProvider.GetAccountLocale(invoice.AccountId).Name;
            var message = $"Выставлен счет №{CloudConfigurationProvider.Invoices.UniqPrefix()}{invoice.Uniq} " +
                          $"{invoiceSumPurpose} " +
                          $"на сумму {invoice.Sum:0.00} {localeName}.";

            LogEventHelper.LogEvent(_dbLayer, invoice.AccountId, _accessProvider, LogActions.AddInvoice, message);

            _logger.Info($"{message} Для аккаунта {invoice.AccountId}");
        }

        /// <summary>
        /// Создать счёт для аккаунта на указанную сумму
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="amount">Сумма счета</param>
        /// <param name="period">Период счета</param>
        /// <returns>Счет на оплату</returns>
        public Domain.DataModels.billing.Invoice CreateInvoiceForSpecifiedInvoiceAmount(Guid accountId, decimal amount, int? period = null)
        {
            var account = _dbLayer.AccountsRepository.GetAccount(accountId);
            var invoice = AddInvoice(account, amount, period);
            WriteLogEventCreatingInvoice(invoice, "произвольный");

            return invoice;
        }

        /// <summary>
        /// Создать счет на основании модели калькуляции счета
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="calculationOfInvoiceRequest">Модель запроса на калькуляцию счета</param>
        /// <returns>Счет на оплату</returns>
        public Domain.DataModels.billing.Invoice CreateInvoiceBasedOnCalculationInvoiceModel(Guid accountId,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequest)
        {
            var account = _dbLayer.AccountsRepository.GetAccount(accountId);
            var calculationOfInvoiceResult = _calculationOfInvoiceProvider.GetCalculationsOfInvoiceResult(accountId,
                calculationOfInvoiceRequest);

            var bonusReward = calculationOfInvoiceRequest.BonusReward;
            var suggestedPaymentSum = calculationOfInvoiceRequest.SuggestedPayment?.PaymentSum ?? 0;
            var sumOfInvoice = suggestedPaymentSum <= 0 ? calculationOfInvoiceResult.TotalSum : suggestedPaymentSum;
            var invoicePeriod = suggestedPaymentSum <= 0 ? (int) calculationOfInvoiceRequest.PayPeriod : (int?) null;

            if (sumOfInvoice <= 0)
                throw new InvalidOperationException("Создание счета возможно на сумму, большую 0");

            using var transaction = _dbLayer.SmartTransaction.Get();
            try
            {
                var newInvoice = AddInvoice(account, sumOfInvoice, invoicePeriod, bonusReward);
                AddInvoiceProductsBasedOnCalculationInvoiceModels(newInvoice.Id, calculationOfInvoiceRequest,
                    calculationOfInvoiceResult, account);
                transaction.Commit();

                WriteLogEventCreatingInvoice(newInvoice,
                    $"{GetInvoiceSumPurpose(newInvoice, calculationOfInvoiceRequest)}");

                return newInvoice;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new InvalidOperationException($"Произошла ошибка при создании счета {ex}");
            }
        }

        /// <summary>
        /// Получить назначение суммы счета
        /// </summary>
        /// <param name="invoice">Счет</param>
        /// <param name="calculationOfInvoiceRequest">Модель запроса на калькуляцию счета</param>
        /// <returns>Назначение суммы счета</returns>
        private static string GetInvoiceSumPurpose(Domain.DataModels.billing.Invoice invoice,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequest)
        {
            if (calculationOfInvoiceRequest.SuggestedPayment is { PaymentSum: > 0 })
                return "на произвольную сумму";

            if ((calculationOfInvoiceRequest.EsdlLicenseYears != null ||
                 calculationOfInvoiceRequest.EsdlPagesCount != null) &&
                (calculationOfInvoiceRequest.EsdlPagesCount > 0 ||
                 calculationOfInvoiceRequest.EsdlLicenseYears > 0))
                return "на доп. сервисы";

            return $"на {invoice.Period ?? 0} мес.";
        }

        /// <summary>
        /// Добавить счет для сервиса биллинга
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="resConfModel">Структура ресурс конфигурации сервиса с зависимостями</param>
        /// <param name="period">Период</param>
        /// <param name="totalCost">Итоговая стоимость</param>
        /// <returns>Счет на оплату сервиса Аренда 1С</returns>
        private Domain.DataModels.billing.Invoice AddInvoiceForBillingService(BillingAccount billingAccount,
            ResConfigDependenciesModel resConfModel, int period, decimal totalCost)
        {
            _logger.Info(
                $"Добавление счета для сервиса {resConfModel.ResourcesConfiguration.BillingService.Name}. Id аккаунта {billingAccount.Id}");

            using var transaction = _dbLayer.SmartTransaction.Get();
            try
            {
                var billingServiceTypesInfo = _billingServiceInfoProvider.GetBillingServiceTypesInfo(
                    resConfModel.ResourcesConfiguration.BillingServiceId,
                    billingAccount.Id);

                var bonusReward = !_accountConfigurationDataProvider.IsVipAccount(billingAccount.Id)
                    ? CalculateBonusRewardForNewInvoice(period, billingServiceTypesInfo) +
                      CalculateBonusRewardForMyDiskService(
                          resConfModel.DependedResourceConfigurations.FirstOrDefault(drc =>
                              drc.BillingService.SystemService == Clouds42Service.MyDisk), period)
                    : (decimal?) null;

                var newInvoice = AddInvoice(billingAccount.Account, totalCost, period, bonusReward);

                AddInvoiceProductsByService(newInvoice.Id, billingServiceTypesInfo,
                    resConfModel.ResourcesConfiguration.GetLocalizedServiceName(_cloudLocalizer), period, billingAccount.Id);
                
                resConfModel.DependedResourceConfigurations
                    .Where(resourcesConfiguration => resourcesConfiguration.Cost > 0).ToList().ForEach(
                        resourcesConfiguration =>
                        {
                            if (resourcesConfiguration.BillingService.SystemService == Clouds42Service.MyDatabases)
                            {
                                AddInvoiceProductsForMyDatabasesService(resourcesConfiguration.AccountId,
                                    newInvoice.Id, period);

                                return;
                            }

                            billingServiceTypesInfo = _billingServiceInfoProvider.GetBillingServiceTypesInfo(
                                resourcesConfiguration.BillingServiceId,
                                billingAccount.Id);

                            AddInvoiceProductsByService(newInvoice.Id, billingServiceTypesInfo,
                                resourcesConfiguration.GetLocalizedServiceName(_cloudLocalizer),
                                period, billingAccount.Id);
                        });

                AddInvoiceProductForAdditionalResources(newInvoice.Id, billingAccount.Account);

                transaction.Commit();
                WriteLogEventCreatingInvoice(newInvoice, $"на {newInvoice.Period ?? 0} мес на сумму");

                return newInvoice;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new InvalidOperationException($"Произошла ошибка при создании счета {ex}");
            }
        }

        /// <summary>
        /// Создать продукты счета для сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="invoiceId">Id счета</param>
        /// <param name="payPeriod">Период оплаты</param>
        private void AddInvoiceProductsForMyDatabasesService(Guid accountId, Guid invoiceId,
            int payPeriod)
        {
            var serviceTypesBillingData = _myDatabasesServiceTypeBillingDataProvider
                .GetBillingDataForMyDatabasesServiceTypes(accountId).Where(serviceTypeBillingData =>
                    serviceTypeBillingData is { VolumeInQuantity: > 0, TotalAmount: > 0 }).ToList();

            serviceTypesBillingData.ForEach(serviceTypeBillingData =>
            {
                var amount = serviceTypeBillingData.TotalAmount * payPeriod;

                var serviceTypeName = serviceTypeBillingData.SystemServiceType == ResourceType.AccessToConfiguration1C
                    ? serviceTypeBillingData.ServiceTypeDescription
                    : serviceTypeBillingData.ServiceTypeName;

                AddInvoiceProduct(invoiceId, serviceTypeBillingData.VolumeInQuantity, amount,
                    $"{serviceTypeBillingData.ServiceName} - {serviceTypeName}", serviceTypeName,
                    serviceTypeBillingData.ServiceTypeId);
            });
        }

        /// <summary>
        /// Посчитать бонусное вознаграждение для сервиса Мой диск
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="period">Период оплаты</param>
        /// <returns>Бонусное вознаграждение</returns>
        private decimal? CalculateBonusRewardForMyDiskService(ResourcesConfiguration? resourcesConfiguration, int period)
        {
            if (resourcesConfiguration == null ||
                resourcesConfiguration.BillingService.SystemService != Clouds42Service.MyDisk ||
                resourcesConfiguration.Cost == 0)
                return 0;

            var bonusReward = !_accountConfigurationDataProvider.IsVipAccount(resourcesConfiguration.AccountId)
                ? GetBonusRewardForServiceType(period, resourcesConfiguration.Cost)
                : (decimal?) null;

            return bonusReward;
        }

        /// <summary>
        /// Добавить счет
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="sum">Сумма счета</param>
        /// <param name="period">Период счета</param>
        /// <param name="bonusReward">Бонусное вознаграждение</param>
        /// <returns>Добавленный счет</returns>
        private Domain.DataModels.billing.Invoice AddInvoice(Account account, decimal sum, int? period = null, decimal? bonusReward = null)
        {
            var accountRequisites = _accountRequisitesDataProvider.GetIfExistsOrCreateNew(account.Id);
            var newInvoice = new Domain.DataModels.billing.Invoice
            {
                Id = Guid.NewGuid(),
                AccountId = account.Id,
                Date = DateTime.Now,
                Requisite = accountRequisites.Inn,
                Sum = sum,
                State = InvoiceStatus.Processing.ToString(),
                Period = period,
                BonusReward = bonusReward
            };
            _dbLayer.InvoiceRepository.AddInvoice(newInvoice);
            _dbLayer.Save();

            return newInvoice;
        }


        /// <summary>
        /// Добавить продукты счета
        /// на основании моделей калькуляции
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="calculationOfInvoiceRequest">Модель запроса на калькуляцию счета</param>
        /// <param name="calculationOfInvoiceResult">Модель результата калькуляции счета</param>
        /// <param name="account">Аккаунт</param>
        private void AddInvoiceProductsBasedOnCalculationInvoiceModels(Guid invoiceId,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequest,
            CalculationOfInvoiceResultModel calculationOfInvoiceResult, Account account)
        {
            if (calculationOfInvoiceRequest.SuggestedPayment is { PaymentSum: > 0 })
            {
                AddInvoiceProduct(invoiceId, 1, calculationOfInvoiceRequest.SuggestedPayment.PaymentSum,
                    calculationOfInvoiceRequest.SuggestedPayment.ServiceTypeContent ?? "-", "Comment");
                return;
            }

            AddInvoiceProductForMyDiskServiceBasedOnCalculationInvoiceModels(invoiceId, calculationOfInvoiceRequest,
                calculationOfInvoiceResult);

            AddInvoiceProductForEsdlServiceBasedOnCalculationInvoiceModels(invoiceId, calculationOfInvoiceRequest,
                calculationOfInvoiceResult);

            AddInvoiceProductsForBaseBillingServiceTypesBasedOnCalculationInvoiceModel(account.Id, invoiceId,
                calculationOfInvoiceResult.CalculationBaseBillingServiceTypes);

            AddInvoiceProductForAdditionalResources(invoiceId, account,
                calculationOfInvoiceRequest.AdditionalResourcesDisabled);
        }

        /// <summary>
        /// Добавить продукт счета для
        /// дополнительных ресурсов
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        /// <param name="account">Аккаунт</param>
        /// <param name="additionalResourcesDisabled">
        /// Дополнительные ресурсы отключены (Отключена в калькуляторе на странице Баланс)</param>
        private void AddInvoiceProductForAdditionalResources(Guid invoiceId,
            Account account, bool additionalResourcesDisabled = false)
        {
            var additionalResourcesData = _additionalResourcesDataProvider.GetAdditionalResourcesData(account.Id);

            if (!additionalResourcesData.IsAvailabilityAdditionalResources ||
                !additionalResourcesData.AdditionalResourceCost.HasValue || additionalResourcesDisabled)
                return;

            var invoiceProduct = new InvoiceProduct
            {
                Id = Guid.NewGuid(),
                Count = 1,
                InvoiceId = invoiceId,
                ServiceName = "Дополнительные ресурсы",
                ServiceSum = additionalResourcesData.AdditionalResourceCost.Value,
                Description = additionalResourcesData.AdditionalResourceName
            };

            _dbLayer.InvoiceProductRepository.Insert(invoiceProduct);
            _dbLayer.Save();

            _logger.Info($"Добавление продукта счета Id = {invoiceId} для дополнительных ресурсов");
        }

        /// <summary>
        /// Добавить продукт счета для сервиса Мой диск
        /// на основании моделей калькуляции
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        /// <param name="calculationOfInvoiceRequest">Модель запроса на калькуляцию счета</param>
        /// <param name="calculationOfInvoiceResult">Модель результата калькуляции счета</param>
        private void AddInvoiceProductForMyDiskServiceBasedOnCalculationInvoiceModels(Guid invoiceId,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequest,
            CalculationOfInvoiceResultModel calculationOfInvoiceResult)
        {
            if (calculationOfInvoiceResult.MyDiskCost <= 0)
                return;

            AddInvoiceProduct(invoiceId, calculationOfInvoiceRequest.MyDiskSize ?? 0,
                calculationOfInvoiceResult.MyDiskCost, "Мой Диск", Clouds42Service.MyDisk.ToString(),
                _systemServiceTypeDataProvider.GetServiceTypeIdBySystemServiceType(ResourceType.DiskSpace));
        }

        /// <summary>
        /// Добавить продукт счета для сервиса Загрузка документов
        /// на основании моделей калькуляции
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        /// <param name="calculationOfInvoiceRequest">Модель запроса на калькуляцию счета</param>
        /// <param name="calculationOfInvoiceResult">Модель результата калькуляции счета</param>
        private void AddInvoiceProductForEsdlServiceBasedOnCalculationInvoiceModels(Guid invoiceId,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequest,
            CalculationOfInvoiceResultModel calculationOfInvoiceResult)
        {
            if (calculationOfInvoiceRequest.EsdlLicenseYears == null ||
                calculationOfInvoiceRequest.EsdlPagesCount == null)
                return;

            if (calculationOfInvoiceRequest.EsdlPagesCount > 0)
                AddInvoiceProduct(invoiceId, (int) calculationOfInvoiceRequest.EsdlPagesCount,
                    calculationOfInvoiceResult.EsdlPagesCost,
                    $"{NomenclaturesNameConstants.Esdl} Recognition 42 Тариф {calculationOfInvoiceRequest.EsdlPagesCount}",
                    Clouds42Service.Recognition.ToString(),
                    _systemServiceTypeDataProvider.GetServiceTypeIdBySystemServiceType(
                        GetResourceTypeByEsdlPagesCount(calculationOfInvoiceRequest.EsdlPagesCount.Value)));

            if (calculationOfInvoiceRequest.EsdlLicenseYears > 0)
                AddInvoiceProduct(invoiceId, (int) calculationOfInvoiceRequest.EsdlLicenseYears,
                    calculationOfInvoiceResult.EsdlLicenseCost,
                    $"{NomenclaturesNameConstants.Esdl} Лицензия",
                    Clouds42Service.Esdl.ToString(),
                    _systemServiceTypeDataProvider.GetServiceTypeIdBySystemServiceType(ResourceType.Esdl));
        }

        /// <summary>
        /// Добавить продукт счета для
        /// основных услуг биллинга
        /// на основании моделей калькуляции
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="invoiceId">Id счета</param>
        /// <param name="calculationBaseBillingServiceTypes">Калькуляция основных услуг биллинга</param>
        private void AddInvoiceProductsForBaseBillingServiceTypesBasedOnCalculationInvoiceModel(Guid accountId,
            Guid invoiceId,
            List<CalculationBaseBillingServiceTypeModel> calculationBaseBillingServiceTypes)
        {
            if (!calculationBaseBillingServiceTypes.Any())
                return;

            var dataForCreateInvoiceProducts =
                GetDataForCreateInvoiceProducts(calculationBaseBillingServiceTypes).ToList();

            dataForCreateInvoiceProducts.ForEach(data =>
            {
                var serviceName =
                    ServiceNameLocalizer.LocalizeForAccount(_cloudLocalizer, accountId, data.ServiceName, data.SystemService);

                AddInvoiceProduct(invoiceId, data.CountLicenses, data.Amount,
                    $"{serviceName} - {data.ServiceTypeName}", data.ServiceTypeName, data.ServiceTypeId);
            });
        }

        /// <summary>
        /// Добавить продукты счета для сервиса
        /// </summary>
        /// <param name="invoiceId">Id сервиса</param>
        /// <param name="billingServiceTypesInfo">Услуги</param>
        /// <param name="billingServiceName">Название сервиса</param>
        /// <param name="period">Период за который необходим счет</param>
        /// <param name="accountId">Id аккаунта</param>
        private void AddInvoiceProductsByService(Guid invoiceId,
            List<BillingServiceTypeInfoDto> billingServiceTypesInfo, string billingServiceName, int period,
            Guid accountId)
        {
            foreach (var billingServiceTypeInfo in billingServiceTypesInfo)
            {
                if (billingServiceTypeInfo.SystemServiceType == ResourceType.DiskSpace)
                {
                    AddInvoiceProductForMyDiskService(invoiceId, period, accountId, billingServiceTypeInfo);
                    continue;
                }

                if (billingServiceTypeInfo.UsedLicenses == 0)
                    continue;

                AddInvoiceProduct(invoiceId, billingServiceTypeInfo.UsedLicenses,
                    billingServiceTypeInfo.Amount * period,
                    $"{billingServiceName} - {billingServiceTypeInfo.Name}",
                    billingServiceTypeInfo.Name, billingServiceTypeInfo.Id);
            }
        }

        /// <summary>
        /// Добавить продукт счета для услуги
        /// Дисковое пространство
        /// </summary>
        /// <param name="billingServiceTypeInfo">Модель услуги</param>
        /// <param name="period">Период оплаты</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="invoiceId">Id счета</param>
        private void AddInvoiceProductForMyDiskService(Guid invoiceId, int period, Guid accountId,
            BillingServiceTypeInfoDto billingServiceTypeInfo)
        {
            var capacityDisk = GetCapacityDisk(accountId);

            AddInvoiceProduct(invoiceId, capacityDisk, billingServiceTypeInfo.Amount * period,
                Clouds42Service.MyDisk.GetDisplayName(),
                Clouds42Service.MyDisk.ToString(), billingServiceTypeInfo.Id);
        }

        /// <summary>
        /// Получить объем диска
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Объем диска</returns>
        private int GetCapacityDisk(Guid accountId) =>
            _dbLayer.DiskResourceRepository.FirstOrDefault(w => w.Resource.Subject == accountId)?.VolumeGb ?? 0;

        /// <summary>
        /// Добавить продукт счета
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        /// <param name="count">Количество/объем</param>
        /// <param name="serviceSum">Сумма</param>
        /// <param name="description">Описание</param>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="serviceTypeId">Id услуги</param>
        private void AddInvoiceProduct(Guid invoiceId, int count, decimal serviceSum, string description,
            string serviceName, Guid? serviceTypeId = null)
        {
            var invoiceProduct = new InvoiceProduct
            {
                Id = Guid.NewGuid(),
                Count = count,
                Description = description,
                InvoiceId = invoiceId,
                ServiceName = serviceName,
                ServiceSum = serviceSum,
                ServiceTypeId = serviceTypeId
            };

            _dbLayer.InvoiceProductRepository.Insert(invoiceProduct);
            _dbLayer.Save();

            _logger.Info($"Добавление продукта счета Id = {invoiceId}");
        }

        /// <summary>
        /// Получить недавний не оплаченный счет у аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="sum">Сумма счета</param>
        /// <returns>Актуальный не оплаченный счет</returns>
        private Domain.DataModels.billing.Invoice? GetRecentUnpaidInvoiceFromAccount(Guid accountId, decimal sum)
        {
            _logger.Info($"Получение недавнего не оплаченного счета у аккаунта Id= {accountId}");
            var sixDaysAgo = DateTime.Now.AddDays(-6);
            var lastActualInvoiceForService =
                _dbLayer.InvoiceRepository.FirstOrDefault(
                    i =>
                        i.State == InvoiceStatus.Processing.ToString() && i.AccountId == accountId &&
                        i.Date >= sixDaysAgo && i.Sum == sum);

            var searchStatusForMessage = lastActualInvoiceForService == null
                ? "не найден."
                : $"найден Id={lastActualInvoiceForService.Id}.";
            _logger.Info($"Недавний не оплаченный счет {searchStatusForMessage}");

            return lastActualInvoiceForService;
        }

        /// <summary>
        /// Получить последний оплаченный счет у аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Последний оплаченный счет аккаунта</returns>
        private Domain.DataModels.billing.Invoice? GetLatestPaidInvoiceFromAccount(Guid accountId)
        {
            _logger.Info($"Получение последнего оплаченного счета у аккаунта Id = {accountId}");
            var lastInvoicePaid = _dbLayer.InvoiceRepository
                .AsQueryable()
                .Where(i => i.State == InvoiceStatus.Processed.ToString() && i.AccountId == accountId &&
                            i.Period != null)
                .OrderByDescending(w => w.Date)
                .FirstOrDefault();

            var searchStatusForMessage = lastInvoicePaid == null
                ? "не найден."
                : $"найден Id={lastInvoicePaid.Id}.";
            _logger.Info($"Последний оплаченный счет {searchStatusForMessage}");

            return lastInvoicePaid;
        }

        /// <summary>
        /// Получить тип услуги по количеству
        /// страниц загрузки документов
        /// </summary>
        /// <param name="pageCount">Количество страниц</param>
        /// <returns>Тип услуги</returns>
        private static ResourceType GetResourceTypeByEsdlPagesCount(int pageCount)
        {
            foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
                if (resourceType.TryGetDataValue(out var dataValue) && dataValue == pageCount)
                    return resourceType;

            throw new InvalidOperationException($"Для количества страниц {pageCount} не найден тариф");
        }

        /// <summary>
        /// Расчитать бонусное вознаграждение для нового счета
        /// </summary>
        /// <param name="period">Период оплаты</param>
        /// <param name="billingServiceTypesInfo">Список услуг сервиса</param>
        /// <returns>Бонусное вознаграждение для нового счета</returns>
        private decimal CalculateBonusRewardForNewInvoice(int period,
            List<BillingServiceTypeInfoDto> billingServiceTypesInfo)
        {
            if (period == (int) PayPeriod.Month1)
                return 0;

            var bonusReward = decimal.Zero;

            billingServiceTypesInfo.ForEach(serviceType =>
            {
                if (serviceType.SystemServiceType != ResourceType.MyEntUser)
                    return;
                bonusReward += GetBonusRewardForServiceType(period, serviceType.Amount);
            });

            return bonusReward;
        }

        /// <summary>
        /// Пересчитать количество бонусов для существующего счета
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        private void RecalculateBonusRewardForExistingInvoice(Domain.DataModels.billing.Invoice invoice, BillingAccount billingAccount)
        {
            if (invoice.Period == null)
                return;

            if (_accountConfigurationDataProvider.IsVipAccount(billingAccount.Id))
            {
                UpdateInvoiceBonusReward(invoice, 0);
                return;
            }

            var bonusReward = decimal.Zero;

            invoice.InvoiceProducts.ToList().ForEach(product =>
            {
                if (product.ServiceType.SystemServiceType != ResourceType.MyEntUser &&
                    product.ServiceType.SystemServiceType != ResourceType.DiskSpace)
                    return;

                bonusReward += GetBonusRewardForInvoiceProduct(invoice.Period.Value, product.ServiceSum);
            });

            UpdateInvoiceBonusReward(invoice, bonusReward);
        }

        /// <summary>
        /// Получить бонусное вознаграждение для услуги сервиса
        /// </summary>
        /// <param name="payPeriod">Период оплаты</param>
        /// <param name="serviceTypeCost">Стоимость сервиса</param>
        /// <returns>Бонусное вознаграждение для услуги сервиса</returns>
        private decimal GetBonusRewardForServiceType(int payPeriod, decimal serviceTypeCost)
        {
            var payPeriodEnum = payPeriod.ToEnum<PayPeriod>();

            if (payPeriodEnum == PayPeriod.None || payPeriodEnum == PayPeriod.Month1)
                return 0;

            var bonusRewardPercentage = _mapPayPeriodToBonusReward[payPeriodEnum];

            return (serviceTypeCost * payPeriod * bonusRewardPercentage) / 100;
        }

        /// <summary>
        /// Получить бонусное вознаграждение для услуги счета
        /// </summary>
        /// <param name="payPeriod">Период оплаты</param>
        /// <param name="productCost">Стоимость услуги счета</param>
        /// <returns>Бонусное вознаграждение для услуги счета</returns>
        private decimal GetBonusRewardForInvoiceProduct(int payPeriod, decimal productCost)
        {
            var payPeriodEnum = payPeriod.ToEnum<PayPeriod>();

            if (payPeriodEnum == PayPeriod.None || payPeriodEnum == PayPeriod.Month1)
                return 0;

            var bonusRewardPercentage = _mapPayPeriodToBonusReward[payPeriodEnum];

            return (productCost * bonusRewardPercentage) / 100;
        }

        /// <summary>
        /// Обновить сумму бонусного вознаграждения в счете на оплату
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <param name="bonusReward">Бонусное вознаграждение</param>
        private void UpdateInvoiceBonusReward(Domain.DataModels.billing.Invoice invoice, decimal bonusReward)
        {
            if (invoice.BonusReward == bonusReward)
                return;

            invoice.BonusReward = bonusReward;

            _dbLayer.InvoiceRepository.Update(invoice);
            _dbLayer.Save();
        }

        /// <summary>
        /// Получить данные для создания продуктов счета
        /// на основании моделей калькуляции услуг
        /// </summary>
        /// <param name="calculationBaseBillingServiceTypes">Модели калькуляции основных услуг биллинга</param>
        /// <returns>Данные для создания продуктов счета</returns>
        private IEnumerable<CreateInvoiceProductDataDto> GetDataForCreateInvoiceProducts(
            List<CalculationBaseBillingServiceTypeModel> calculationBaseBillingServiceTypes)
        {
            var serviceTypesInfo = calculationBaseBillingServiceTypes
                .Select(cm => new {cm.Id, cm.CountLicenses, cm.Amount});

            return (from serviceTypeInfo in serviceTypesInfo
                join serviceType in _dbLayer.BillingServiceTypeRepository.WhereLazy() on serviceTypeInfo.Id equals
                    serviceType.Id
                join service in _dbLayer.BillingServiceRepository.WhereLazy() on serviceType.ServiceId equals service.Id
                where serviceTypeInfo.CountLicenses > 0 && serviceTypeInfo.Amount > 0
                select new CreateInvoiceProductDataDto
                {
                    ServiceTypeId = serviceType.Id,
                    ServiceName = service.Name,
                    SystemService = service.SystemService,
                    ServiceTypeName = serviceType.SystemServiceType == ResourceType.AccessToConfiguration1C
                        ? serviceType.Description
                        : serviceType.Name,
                    Amount = serviceTypeInfo.Amount,
                    CountLicenses = serviceTypeInfo.CountLicenses
                }).AsEnumerable();
        }
    }
}
