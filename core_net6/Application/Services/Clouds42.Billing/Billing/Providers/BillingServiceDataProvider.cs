﻿using System.Configuration;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для получения данных по сервисам биллинга
    /// </summary>
    public class BillingServiceDataProvider(IUnitOfWork dbLayer) : IBillingServiceDataProvider
    {
        /// <summary>
        /// Получить данные о системном сервисе.
        /// </summary>
        /// <param name="systemServiceType">Тип системного сервиса.</param>        
        public IBillingService GetSystemService(Clouds42Service systemServiceType)
        {
            var billingService =
                dbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == systemServiceType) ?? throw new ConfigurationErrorsException($"В справочнике сервисы биллинга, не сконфигурирован сервис '{systemServiceType.GetDisplayName()}'.");
            
            return billingService;
        }
        

        /// <summary>
        /// Получить данные о сервисе.
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса.</param>        
        public IBillingService GetBillingServiceOrThrowException(Guid serviceId)
        {
            var billingService =
                dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId) ??  throw new NotFoundException($"В справочнике сервисы биллинга, не найден сервис по идентификатору'{serviceId}'.");
            
            return billingService;
        }

        /// <summary>
        /// Получить данные о системной услуги сервиса.
        /// </summary>
        /// <param name="resourceType">Тип услуги.</param>
        /// <returns></returns>
        public IBillingServiceType GetSystemServiceTypeOrThrowException(ResourceType resourceType)
        {
            var billingServiceType =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(s => s.SystemServiceType == resourceType) ?? throw new ConfigurationErrorsException($"В справочнике услуги сервиса биллинга, не сконфигурированна услуга '{resourceType.GetDisplayName()}'.");
            
            return billingServiceType;
        }
    }
}
