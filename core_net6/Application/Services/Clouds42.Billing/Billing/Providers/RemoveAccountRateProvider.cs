﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для удаления тарифа услуги на аккаунт
    /// </summary>
    internal class RemoveAccountRateProvider(IAccountRateDataProvider accountRateDataProvider, IUnitOfWork dbLayer)
        : IRemoveAccountRateProvider
    {
        /// <summary>
        /// Удалить тариф услуги на аккаунт
        /// </summary>
        /// <param name="accountId">Id аккаунт</param>
        /// <param name="serviceTypeId">Id услуги</param>
        public void Remove(Guid accountId, Guid serviceTypeId)
        {
            var accountRate = accountRateDataProvider.GetAccountRate(accountId, serviceTypeId);

            if (accountRate == null)
                return;

            dbLayer.AccountRateRepository.Delete(accountRate);
            dbLayer.Save();
        }

        /// <summary>
        /// Удалить тарифы услуг на аккаунт по сервису
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        public void RemoveAccountRatesByService(Guid accountId, Guid serviceId)
        {
            var accountRates = accountRateDataProvider.GetAccountRatesForService(accountId, serviceId).ToList();

            if (!accountRates.Any())
                return;

            dbLayer.AccountRateRepository.DeleteRange(accountRates);
            dbLayer.Save();
        }
    }
}
