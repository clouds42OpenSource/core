﻿using Clouds42.Billing.Billing.ModelProcessors;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Locales.Extensions;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер обработки события скорой блокировки сервиса.
    /// </summary>
    public class BeforeLockServiceProvider(
        ICloudServiceProcessor cloudServiceProcessor,
        IServiceProvider serviceProvider,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Уведомить клиента о скорой блокировке сервиса.
        /// </summary>        
        public void TryNotifyBeforeLockService(ResourcesConfiguration resourceConfiguration,
            StringBuilder stringBuilder)
        {
            try
            {
                if (resourceConfiguration.ExpireDate.HasValue)
                {
                    var daysUntilExpiration = (resourceConfiguration.ExpireDate.Value - DateTime.UtcNow).TotalDays;

                    if (Math.Abs(daysUntilExpiration - 3) < 1 || Math.Abs(daysUntilExpiration - 5) < 1)
                    {
                        cloudServiceProcessor.Process(resourceConfiguration,
                            (service) =>
                            {
                                serviceProvider.GetRequiredService<Before5DayNotificationProcessor>()
                                    .Process(service);
                            },
                            () =>
                            {
                                serviceProvider.GetRequiredService<Before5DayBillingServiceNotificationCreator>()
                                    .CreateNotification(resourceConfiguration);
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                var message =
                    $"[Ошибка уведомления аккаунта о скорой блокировке сервиса] acc: '{resourceConfiguration.AccountId}' " +
                    $"сервис '{resourceConfiguration.GetLocalizedServiceName(cloudLocalizer)}'";

                stringBuilder.AppendLine(message);
                handlerException.Handle(ex, message);
            }
        }
    }
}
