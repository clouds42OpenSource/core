﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.DataManagers.Payments.Internal;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Процессор для обработки входящих платежей
    /// </summary>
    public class InflowPaymentProcessor(
        IUnitOfWork dbLayer,
        IDocLoaderByRent1CProlongationProcessor docLoaderByRent1CProlongationProcessor,
        IPromisedPaymentProcessor promisedPaymentProcessor,
        IServiceUnlocker serviceUnlocker,
        ILogger42 logger,
        IApplyDemoServiceSubscribeProcessor applyDemoServiceSubscribeProcessor)
        : IInflowPaymentProcessor
    {
        /// <summary>
        /// Обработать входящий платеж.
        /// </summary>  
        public void Process(Guid accountId, bool needRepayPromisePayment = true)
        {
            var billingAccount = dbLayer.BillingAccountRepository.FirstOrDefault(x => x.Id == accountId);
            var billingAccountAvailableBalance = billingAccount.Balance + billingAccount.BonusBalance;

            if (needRepayPromisePayment &&
                billingAccount.PromisePaymentSum is > 0)
            {
                logger.Trace($"{accountId} :: есть ОП на сумму {billingAccount.PromisePaymentSum}");
                if (billingAccountAvailableBalance >= billingAccount.PromisePaymentSum)
                {
                    promisedPaymentProcessor.RepayPromisedPayment(billingAccount, billingAccount.GetPromisePaymentSum());
                    serviceUnlocker.UnlockExpiredOrPayLockedServicesAccount(accountId);
                    logger.Trace($"{accountId} :: ОП на сумму {billingAccount.PromisePaymentSum} погашен, заблокированные сервисы восстановлены.");
                }
                else
                {
                    logger.Trace($"{accountId} :: ОП на сумму {billingAccount.PromisePaymentSum} не можем погасить из за нехватки баланса.");
                }
            }
            else
            {
                logger.Trace($"{accountId} :: задолжности нет, разблокируем просроченные сервисы.");
                serviceUnlocker.UnlockExpiredOrPayLockedServicesAccount(accountId);
            }

            ApplySubscriptionWithPaymentForFrozenDemoServices(accountId);

            docLoaderByRent1CProlongationProcessor.ProcessIncrease(accountId);
        }

        /// <summary>
        /// Принять подписку, разблокировать и оплатить за заблокированный демо сервис
        /// </summary>
        /// <param name="accountId">Для какого аккаунта</param>
        private void ApplySubscriptionWithPaymentForFrozenDemoServices(Guid accountId)
        {
            logger.Trace($"{accountId} :: Авто-принятие подписки сервиса, разблокировка и оплата до окончания аренды 1С.");

            var frozenDemoServices = dbLayer.ResourceConfigurationRepository.GetAllFrozenDemoResourcesConfigurationsWithAutoSubscription(accountId);
            
            foreach (var _ in frozenDemoServices.Where(frozenDemoService => !ApplySubscriptionWithPayment(frozenDemoService, applyDemoServiceSubscribeProcessor)))
            {
                break;
            }
        }

        /// <summary>
        /// Принять подписку и оплатить за оставшееся время до конца Аренды 1С
        /// </summary>
        /// <param name="frozenDemoService">Заблокированный сервис для которого нужно принять подписку и оплатить</param>
        /// <param name="applyDemoServiceSubscribeProcessor">Процессор принятия подписки и оплаты сервиса после демо</param>
        /// <returns>true - если подписка принята и оплата произведена, иначе false</returns>
        private static bool ApplySubscriptionWithPayment(ResourcesConfiguration frozenDemoService, IApplyDemoServiceSubscribeProcessor applyDemoServiceSubscribeProcessor)
        {
            var paymentResult = applyDemoServiceSubscribeProcessor.Apply(frozenDemoService.BillingServiceId, frozenDemoService.AccountId, false);
            return paymentResult.Complete;
        }
    }
}
