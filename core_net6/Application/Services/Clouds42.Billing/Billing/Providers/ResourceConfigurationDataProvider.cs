﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Locales.Extensions;
using Microsoft.EntityFrameworkCore;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер получения данных ресурс конфигурации.
    /// </summary>
    public class ResourceConfigurationDataProvider(IUnitOfWork dbLayer, ICloudServiceAdapter cloudServiceAdapter, ICloudLocalizer cloudLocalizer)
        : IResourceConfigurationDataProvider
    {
        /// <summary>
        /// Получить ресурс конфигурацию сервиса аккаунта
        /// </summary>
        /// <param name="billingServiceId">Номер сервиса облака</param>
        /// <param name="accountId">Номер аккаунта</param>
        /// <returns>Ресурс конфигурация сервиса</returns>
        public ResourcesConfiguration GetResourceConfigurationOrThrowException(Guid billingServiceId,
            Guid accountId)
            => dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                   rc.AccountId == accountId && rc.BillingServiceId == billingServiceId) ??
               throw new NotFoundException(
                   $"Не удалось получить ресурс конфигурацию для сервиса '{billingServiceId}' у аккаунта '{accountId}'");

        /// <summary>
        /// Получить ресурс конфигурацию сервиса аккаунта
        /// </summary>
        /// <param name="billingService">Сервис облака</param>
        /// <param name="accountId">Номер аккаунта</param>
        /// <returns>Ресурс конфигурация сервиса</returns>
        public ResourcesConfiguration GetResourceConfigurationOrThrowException(IBillingService billingService,
            Guid accountId)
            => GetResourceConfigurationOrThrowException(billingService.Id, accountId);

        /// <summary>
        /// Получить конфигурацию ресурса для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Системный сервис</param>
        /// <returns>Конфигурация ресурса</returns>
        public ResourcesConfiguration GetResourceConfigurationOrThrowException(Guid accountId, Clouds42Service service) =>
            GetResourceConfiguration(accountId, service) ??
            throw new NotFoundException($"Не удалось найти конфигурацию ресурса для аккаунта{accountId}");

        /// <summary>
        /// Получить конфигурацию ресурса для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Системный сервис</param>
        /// <returns>Конфигурация ресурса</returns>
        public ResourcesConfiguration GetResourceConfiguration(Guid accountId, Clouds42Service service) =>
            dbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == accountId && config.BillingService.SystemService == service);

        /// <summary>
        /// Получить общую стоимость сервиса
        /// </summary>
        /// <param name="resourceConfiguration">Ресурс конфигурация сервиса.</param>
        /// <returns>Общая стоимость сервиса</returns>
        public decimal GetTotalServiceCost(ResourcesConfiguration resourceConfiguration)
        {
            var mainService = cloudServiceAdapter.GetMainConfigurationOrThrowException(resourceConfiguration);
            var resConfModel = GetConfigWithDependencies(mainService);
            return GetTotalServiceCost(resConfModel);
        }

        /// <summary>
        /// Получить общую стоимость сервиса
        /// </summary>
        /// <param name="resConfModel">Ресурс конфигурация сервиса с зависимостями.</param>
        /// <returns>Общая стоимость сервиса</returns>
        public decimal GetTotalServiceCost(ResConfigDependenciesModel resConfModel)
        {
            var totalServiceCost = resConfModel.ResourcesConfiguration.Cost;

            resConfModel.DependedResourceConfigurations.Where(rc => !rc.IsDemoPeriod).ToList()
                .ForEach(rc => { totalServiceCost += rc.Cost; });

            return totalServiceCost;
        }

        /// <summary>
        /// Получить данные по ресурс конфигурации со всеми зависимыми сервисами.
        /// </summary>
        /// <param name="resourceConfiguration">Конфигурация ресурса</param>
        /// <returns>Данные по ресурс конфигурации со всеми зависимыми сервисами</returns>
        public ResConfigDependenciesModel GetConfigWithDependencies(ResourcesConfiguration resourceConfiguration) =>
            new(resourceConfiguration,
                GetDependedResourceConfigurations(resourceConfiguration, false));

        /// <summary>
        /// Получить данные по ресурс конфигурации со всеми зависимыми сервисами
        /// включая демо сервисы
        /// </summary>
        /// <param name="resourceConfiguration">Конфигурация ресурса</param>
        /// <returns>Данные по ресурс конфигурации со всеми зависимыми сервисами
        /// включая демо</returns>
        public ResConfigDependenciesModel GetConfigWithDependenciesIncludingDemo(ResourcesConfiguration resourceConfiguration) =>
            new(resourceConfiguration,
                GetDependedResourceConfigurations(resourceConfiguration));

        /// <summary>
        /// Получить название сервисов
        /// </summary>
        /// <param name="resConf">Конфигурация ресурсов</param>
        /// <returns>Название сервисов</returns>
        public string GetServiceNames(ResourcesConfiguration resConf)
        {
            var resConfigDependencies = GetConfigWithDependenciesIncludingDemo(resConf);

            var mainServiceName =
                resConfigDependencies.ResourcesConfiguration.GetLocalizedServiceName(cloudLocalizer);

            if (!resConfigDependencies.DependedResourceConfigurations.Any())
                return mainServiceName;

            var serviceCollections = new List<string> { mainServiceName };

            serviceCollections.AddRange(resConfigDependencies.DependedResourceConfigurations.Where(drc => drc.Cost > 0)
                .Select(resourcesConfiguration => resourcesConfiguration.GetLocalizedServiceName(cloudLocalizer)));

            return string.Join(", ", serviceCollections.Distinct());
        }

        /// <summary>
        /// Получить дату пролонгации
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <returns>Дата пролонгации</returns>
        public DateTime GetExpireDateOrThrowException(ResourcesConfiguration resourcesConfiguration)
        {
            return resourcesConfiguration.IsDemoPeriod
                ? resourcesConfiguration.ExpireDate ??
                  throw new NotFoundException("У сервиса в режиме демо дата пролонгации не может быть null")
                : cloudServiceAdapter.GetMainConfigurationOrThrowException(resourcesConfiguration).ExpireDate ??
                  throw new NotFoundException("У основного сервиса не указана дата пролонгации");
        }

        /// <summary>
        /// Получить дату пролонгации
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <returns>Дата пролонгации</returns>
        public DateTime? GetExpireDate(ResourcesConfiguration resourcesConfiguration)
        {
            return resourcesConfiguration.IsDemoPeriod
                ? resourcesConfiguration.ExpireDate
                : cloudServiceAdapter.GetMainConfiguration(resourcesConfiguration)?.ExpireDate;
        }

        /// <summary>
        /// Получить список зависимых сервисов.
        /// </summary>
        /// <param name="resourceConfiguration">Сервис.</param>
        /// <param name="needConsiderDemoConfigurations">Нужно учитывать демо конфигурации</param>
        /// <returns>Список зависимых сервисов.</returns>
        private List<ResourcesConfiguration> GetDependedResourceConfigurations(
            ResourcesConfiguration resourceConfiguration, bool needConsiderDemoConfigurations = true)
        {
            var dependedResourceConfigurations =
                dbLayer.ResourceConfigurationRepository
                    .AsQueryable()
                    .Include(i => i.BillingService)
                    .ThenInclude(i => i.BillingServiceTypes)
                    .Where(rc => rc.AccountId == resourceConfiguration.AccountId && rc.Id != resourceConfiguration.Id)
                    .ToList()
                    .Where(rc => rc.BillingService.BillingServiceTypes
                            .Where(st => st.DependServiceTypeId.HasValue)
                            .Select(st => st.DependServiceTypeId.Value)
                            .Intersect(resourceConfiguration.BillingService.BillingServiceTypes.Select(st => st.Id))
                            .Any())
                    .ToList();

            return needConsiderDemoConfigurations
                ? dependedResourceConfigurations
                : dependedResourceConfigurations.Where(rc => !rc.IsDemoPeriod).ToList();
        }
    }
}
