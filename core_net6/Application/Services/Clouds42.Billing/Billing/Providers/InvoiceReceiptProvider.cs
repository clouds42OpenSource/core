﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Factories.InvoiceReceiptFactoryData;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    ///     Создание фискального чека
    /// </summary>
    public class InvoiceReceiptProvider(
        IUnitOfWork dbLayer,
        InvoiceReceiptFactory factory,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
        : IInvoiceReceiptProvider
    {
        /// <summary>
        ///     Создать фискальный чек по счету на оплату
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        public void CreateFiscalReceiptByInvoice(Domain.DataModels.billing.Invoice invoice)
        {
            try
            {

                logger.Info($"[{invoice.Id}] :: Создание фискального чека");

                var emails = dbLayer.AccountsRepository.GetEmails(invoice.AccountId);
                
                if (IsFiscalReceiptExist(invoice.Id))
                {
                    logger.Warn($"Ошибка создания фиксального чека. Чек для счета {invoice.Id} уже существует");

                    return;
                }

                var receiptInfo = new InvoiceReceiptDto
                {
                    InvoiceId = invoice.Id,
                    ClientEmail = emails?.FirstOrDefault() ?? CloudConfigurationProvider.Emails.Get42CloudsManagerEmail(),
                    Products = GetProducts(invoice),
                    SupplierRazorTemplate = GetSupplierRazorTemplate(invoice)
                };

                logger.Info($"[{invoice.Id}] :: Данные для чека: {receiptInfo.ToJson()}");

                var receipt = factory.GetReceipt(receiptInfo,
                    accountConfigurationDataProvider.GetAccountLocale(invoice.AccountId).Name);

                switch (receipt.FiscalAnyBuilder)
                {
                    case false:
                        logger.Info($"[{invoice.Id}] :: {receipt.FiscalData.Errors}");
                        return;
                    case true when !receipt.FiscalData.IsSuccess:
                    {
                        var error = $"Ошибка при получении фискального чека: {receipt.FiscalData.Errors}";
                        logger.Info($"[{invoice.Id}] :: {error}");
                        throw new InvalidOperationException(error);
                    }
                }

                logger.Info($"[{invoice.Id}] :: Сохранение фискального чека");

                SaveFiscalReceipt(receipt);

                logger.Info($"[{invoice.Id}] :: Сохранение фискального чека выполнено");
            }

            catch (Exception ex)
            {
                logger.Error(ex, $"Ошибка при создании фискального чека для счета {invoice.Id}");

                throw;
            }

        }

        /// <summary>
        /// Проверить наличие фискального чека
        /// для счета
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        /// <returns>Наличие фискального чека</returns>
        private bool IsFiscalReceiptExist(Guid invoiceId) =>
            dbLayer.InvoiceReceiptRepository.FirstOrDefault(ir => ir.InvoiceId == invoiceId) != null;

        /// <summary>
        ///     Сохранить фискальный чек
        /// </summary>
        /// <param name="receiptDc">Информация о счете</param>
        private void SaveFiscalReceipt(InvoiceReceiptDto receiptDc)
        {
            var receipt = new InvoiceReceipt
            {
                InvoiceId = receiptDc.InvoiceId,
                DocumentDateTime = receiptDc.FiscalData.DocumentDateTime,
                QrCodeData = receiptDc.FiscalData.QrCodeData,
                QrCodeFormat = receiptDc.FiscalData.QrCodeFormat,
                ReceiptInfo = receiptDc.FiscalData.ReceiptInfo,
                FiscalNumber = receiptDc.FiscalData.FiscalNumber,
                FiscalSign = receiptDc.FiscalData.FiscalSign,
                FiscalSerial = receiptDc.FiscalData.FiscalSerial
            };

            dbLayer.InvoiceReceiptRepository.Insert(receipt);
            dbLayer.Save();
        }

        /// <summary>
        ///     Получить оплаченные товары
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        private List<InvoiceProductDto> GetProducts(Domain.DataModels.billing.Invoice invoice)
        {
            var payedServicesOfInvoice = invoice.InvoiceProducts.Where(p => p.ServiceSum > 0).ToList();
            if (payedServicesOfInvoice.Any())
                return payedServicesOfInvoice.Select(ConvertToProductDc).ToList();

            var alpacaMeetproduct = invoice.InvoiceProducts.FirstOrDefault(p => p.ServiceName == "AlpacaMeet");
            var supplier = alpacaMeetproduct != null && invoice.InvoiceProducts.Count == 1
                ? dbLayer.SupplierRepository.FirstOrDefault(s => s.Name == "ООО \"УчетОнлайн\"")
                : accountConfigurationDataProvider.GetAccountSupplier(invoice.AccountId);

            return
            [
                new()
                {
                    ServiceSum = decimal.Round(invoice.Sum, 2),
                    Description = supplier.Code == "00001"
                        ? "EFSOL 42clouds"
                        : "Предоставление права для использования лицензионного обеспечения",
                    Count = 1
                }
            ];
        }

        /// <summary>
        ///     Конвертация продукта в DataContract модель
        /// </summary>
        private InvoiceProductDto ConvertToProductDc(InvoiceProduct product)
        {
            return product.ServiceName switch
            {
                "WebUsers" => new InvoiceProductDto
                {
                    ServiceSum = decimal.Round(product.ServiceSum, 2),
                    Description = $"{product.Description} {product.Count} шт",
                    Count = 1
                },
                "RdpUsers" => new InvoiceProductDto
                {
                    ServiceSum = decimal.Round(product.ServiceSum, 2),
                    Description = $"{product.Description} {product.Count} шт",
                    Count = 1
                },
                "MyDisk" => new InvoiceProductDto
                {
                    ServiceSum = decimal.Round(product.ServiceSum, 2),
                    Description = $"{product.Description} {product.Count} гб",
                    Count = 1
                },
                "Pages" => new InvoiceProductDto
                {
                    ServiceSum = decimal.Round(product.ServiceSum, 2), Description = product.Description, Count = 1
                },
                "License" => new InvoiceProductDto
                {
                    ServiceSum = decimal.Round(product.ServiceSum, 2),
                    Description = $"{product.Description} {product.Count} год",
                    Count = 1
                },
                _ => new InvoiceProductDto
                {
                    ServiceSum = decimal.Round(product.ServiceSum, 2),
                    Description = product.Description,
                    Count = product.Count
                }
            };
        }

        /// <summary>
        ///     Получить данные о поставщике
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <returns>Шаблон счета на оплату</returns>
        private SupplierRazorTemplateDto GetSupplierRazorTemplate(Domain.DataModels.billing.Invoice invoice)
        {
            var alpacaMeetproduct = invoice.InvoiceProducts.FirstOrDefault(p => p.ServiceName == "AlpacaMeet");
            var supplier = alpacaMeetproduct != null && invoice.InvoiceProducts.Count == 1
                ? dbLayer.SupplierRepository.FirstOrDefault(s => s.Name == "ООО \"УчетОнлайн\"")
                : accountConfigurationDataProvider.GetAccountSupplier(invoice.AccountId);

            logger.Info($"[{invoice.Id}] :: Поставщик услуг {supplier.Name}");

            return new SupplierRazorTemplateDto
            {
                SupplierId = supplier.Id,
                RazorData = supplier.PrintedHtmlFormInvoiceReceipt.HtmlData,
                Attachments = supplier.PrintedHtmlFormInvoiceReceipt.Files.Select(w => new CloudFileDto
                {
                    Id = w.CloudFileId,
                    FileName = w.CloudFile.FileName,
                    ContentType = w.CloudFile.ContentType,
                    Base64 = Convert.ToBase64String(w.CloudFile.Content),
                    Bytes = w.CloudFile.Content
                }).ToList()
            };
        }
    }
}
