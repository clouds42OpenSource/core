﻿using System.Text;
using Clouds42.Billing.Billing.ModelProcessors;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер обработки события скорой автооплате перед блокировкой сервиса.
    /// </summary>
    public class BeforeLockServiceAutoPayProvider(
        ICloudServiceProcessor cloudServiceProcessor,
        ILogger42 logger,
        ICloudLocalizer cloudLocalizer,
        Before5DayNotificationProcessor processor,
        BeforeSomeDayBillingServiceLockAutoPayNotifyCreator autoPayNotifyCreator)
    {
        /// <summary>
        /// Уведомить клиента о скорой автооплате перед блокировкой сервиса.
        /// </summary>        
        public void TryNotifyBeforeLockAutoPayService(ResourcesConfiguration resourceConfiguration,
            StringBuilder stringBuilder)
        {
            try
            {
                cloudServiceProcessor.Process(resourceConfiguration,
                    processor.Process,
                    () =>
                    {
                        autoPayNotifyCreator
                            .CreateNotification(resourceConfiguration);
                    });
            }
            catch (Exception ex)
            {
                var message =
                    $"Ошибка уведомления аккаунта '{resourceConfiguration.AccountId}' о скорой блокировке " +
                    $"сервиса '{resourceConfiguration.GetLocalizedServiceName(cloudLocalizer)}' :: {ex.GetFullInfo()}";

                stringBuilder.AppendLine(message);
                logger.Error(message);
            }
        }
    }
}
