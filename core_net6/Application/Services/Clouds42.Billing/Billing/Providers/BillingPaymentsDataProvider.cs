﻿using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.BillingOperations.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для получения данных по платежам биллинга
    /// </summary>
    public class BillingPaymentsDataProvider(IUnitOfWork uow, ILogger42 logger, IHandlerException handlerException)
        : IBillingPaymentsDataProvider
    {
        private readonly IBillingPaymentsProvider _billingPaymentsProvider = new BillingPaymentsProvider(uow, new BillingPaymentsHelper(uow), logger, handlerException);

        /// <summary>
        /// Пересчет баланса компании. Вызывается после каждой платежной операции.
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакций</param> 
        public void RecalculateAccountTotals(Guid accountId, TransactionType transactionType = TransactionType.Money)
        {
            _billingPaymentsProvider.RecalculateAccountTotals(accountId, transactionType);
        }

        #region ForCPViews

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="accountUid">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакций</param>
        /// <returns>Баланс аккаунта</returns>
        public decimal GetBalance(Guid accountUid, TransactionType transactionType = TransactionType.Money)
        {
            return uow.BillingAccountRepository.GetBalance(accountUid, transactionType);
        }

        /// <summary>
        /// Получить сумму ОП
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Сумма ОП</returns>
        public decimal? GetPromisePaymentCost(Guid accountId)
        {
            return uow.BillingAccountRepository.GetBillingAccount(accountId).PromisePaymentSum;
        }

        /// <summary>
        /// Аккаунт может взять обещанный платеж.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>true - аккаунт может вязть ОП и нет в проивном случае.</returns>
        public bool CanGetPromisePayment(Guid accountId)
        {

            var billingAccount = uow.BillingAccountRepository.FirstOrDefault(a => a.Id == accountId) ??
                                 throw new NotFoundException(
                                     $"Не удалсь получись данные о состояние баланса аккаунта '{accountId}'");

            return uow.PaymentRepository.Any(p =>
                p.AccountId == accountId && p.OperationType == PaymentType.Inflow.ToString() && p.Sum > 0 &&
                p.Status == PaymentStatus.Done.ToString()) &&
                    (billingAccount.PromisePaymentSum == null || billingAccount.PromisePaymentSum <= 0);
        }

        /// <summary>
        /// Получить дату взятия ОП
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Дата взятия ОП</returns>
        public DateTime? GetPromiseDateTime(Guid accountId)
        {
            return uow.BillingAccountRepository.GetBillingAccount(accountId).PromisePaymentDate;
        }

        #endregion

        #region For PaymentSystem

        /// <summary>
        /// Получить номер платежа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="sum">Сумма</param>
        /// <param name="paymentSystem">Платежная система</param>
        /// <returns>Номер платежа</returns>
        public int PreparePayment(Guid accountId, decimal sum, PaymentSystem paymentSystem)
        {
            var account = uow.BillingAccountRepository.GetBillingAccount(accountId);

            if (account == null)
            {
                logger.Warn("При создании платежа для компании(ID) {0} на сумму {1} по системе {2} - Причина: нет аккаунта", accountId, sum, paymentSystem.ToString());
                return -1;
            }

            var payment = new Domain.DataModels.billing.Payment
            {
                AccountId = accountId,
                Sum = sum,
                Status = PaymentStatus.Waiting.ToString(),
                PaymentSystem = paymentSystem.ToString(),
                OriginDetails = $"{paymentSystem} import",
                Date = DateTime.Now,
                Description = $"Ввод средств через платежную систему {paymentSystem}",
                OperationType = PaymentType.Inflow.ToString(),
                Id = Guid.NewGuid()
            };
            var num = uow.PaymentRepository.PreparePayment(payment);

            logger.Trace("Номер платежа ({0}), Успешно создан платеж для компании(ID) {1} на сумму {2} по системе {3}", num, accountId, sum, paymentSystem.ToString());

            return num;
        }

        #endregion

        /// <summary>
        /// Получить список платежей
        /// </summary>
        /// <param name="account">ID аккаунта</param>
        /// <returns>Список платежей</returns>
        public List<PaymentDefinitionDto> GetPaymentsList(Guid account)
        {
            return uow.PaymentRepository.GetPayments(account).Select(ToPaymentDefinition).ToList();
        }

        /// <summary>
        /// Смапить платеж к описанию платежа
        /// </summary>
        /// <param name="p">Платеж</param>
        /// <returns>Описание платежа</returns>
        private PaymentDefinitionDto ToPaymentDefinition(Domain.DataModels.billing.Payment p)
        {
            Enum.TryParse(p.PaymentSystem, out PaymentSystem paymentSystem);
            return new PaymentDefinitionDto
            {
                Id = p.Id,
                Account = p.AccountId,
                Date = p.Date,
                Total = p.Sum,
                OperationType = p.OperationTypeEnum,
                Status = p.StatusEnum,
                System = paymentSystem,
                Description = p.Description,
                ExchangeIdentifier = p.ExchangeDataId,
                OriginDetails = p.OriginDetails,
                BillingServiceId = p.BillingServiceId,
                Number = p.Number
            };
        }
    }
}
