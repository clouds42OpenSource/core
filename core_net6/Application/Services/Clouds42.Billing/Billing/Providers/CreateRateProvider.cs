﻿using Clouds42.Billing.Billing.Constants;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для создания тарифа для услуг
    /// </summary>
    internal class CreateRateProvider(IUnitOfWork dbLayer) : ICreateRateProvider
    {
        /// <summary>
        /// Создать тариф для услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="serviceTypeCost">Стоимость услуги</param>
        /// <param name="localeId">Id локали</param>
        public void Create(Guid serviceTypeId, decimal serviceTypeCost, Guid? localeId = null)
        {
            var rate = new Rate
            {
                Id = Guid.NewGuid(),
                LocaleId = localeId,
                AccountType = RateConst.AccountTypeStandart,
                RatePeriod = RateConst.RatePeriodMonth,
                BillingServiceTypeId = serviceTypeId,
                Cost = serviceTypeCost
            };

            dbLayer.RateRepository.Insert(rate);
            dbLayer.Save();
        }
    }
}
