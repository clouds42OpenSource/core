﻿using System.Text;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Constants;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для отправки писем о совершении платежей
    /// </summary>
    public class PaymentEmailProvider(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloudLocalizer cloudLocalizer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
    {
        /// <summary>
        ///     Отправка письма в случае зачисления оплаты
        /// </summary>
        /// <param name="paymentNumber">Номер платежа</param>
        public void SendSuccessImportReport(int paymentNumber)
        {
            var payment = dbLayer.PaymentRepository.FindPayment(paymentNumber);

            var accountConfigByLocaleData =
                accountConfigurationDataProvider.GetAccountConfigurationByLocaleData(payment.AccountId);
            var accountLocaleName = accountConfigByLocaleData.Locale.Name;
            var paymentSystem = "";
            var accountAdminIds = dbLayer.AccountsRepository.GetAccountAdminIds(accountConfigByLocaleData.Account.Id);
            var accountAdmin = dbLayer.AccountUsersRepository
                .Where(accountUser => accountAdminIds.Contains(accountUser.Id)).FirstOrDefault();

            var accountRequisites = dbLayer.GetGenericRepository<AccountRequisites>()
                .FirstOrDefault(requisites => requisites.AccountId == accountConfigByLocaleData.Account.Id);

            var builder = new StringBuilder();

            paymentSystem = accountLocaleName switch
            {
                LocaleConst.Russia => "Робокассе",
                LocaleConst.Kazakhstan or LocaleConst.Uzbekistan => "Paybox",
                LocaleConst.Ukraine => "UkrPays",
                _ => paymentSystem
            };

            builder.Append(
                $"{payment.Date:dd.MM.yyyy HH:mm} зачислена оплата в размере <b>{payment.Sum:0.00} {accountConfigByLocaleData.Locale.Currency}.</b>");
            builder.Append("<br>");
            var accountName = accountConfigByLocaleData.Account.AccountCaption ==
                              accountConfigByLocaleData.Account.IndexNumber.ToString()
                ? accountConfigByLocaleData.Account.AccountCaption
                : $"{accountConfigByLocaleData.Account.IndexNumber} ({accountConfigByLocaleData.Account.AccountCaption})";
            builder.Append($"Номер аккаунта : {accountName}");
            builder.Append("<br>");
            builder.Append($"E-mail покупателя : {accountAdmin?.Email}");
            if (!string.IsNullOrEmpty(accountRequisites?.Inn))
            {
                builder.Append("<br>");
                builder.Append(
                    $"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Inn,accountConfigByLocaleData.Account.Id)} : {accountRequisites.Inn}");
            }

            var content = string.Format(builder.ToString());
            var to = CloudConfigurationProvider.Emails.Get42CloudsManagerEmail();

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(to)
                .Subject($"Оплата по {paymentSystem}")
                .Body(content)
                .SendViaNewThread();

            LogEventHelper.LogEvent(dbLayer, accountConfigByLocaleData.Account.Id, accessProvider,
                LogActions.MailNotification,
                $"Письмо \"Оплата по {paymentSystem}\" отправлено на адрес {to}");

            logger.Trace(
                $"Отправили аккаунту {accountConfigByLocaleData.Account.IndexNumber} письмо по зачислению оплаты через {paymentSystem}");
        }
    }
}
