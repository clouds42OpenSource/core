﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер обновления тарифа для услуги
    /// </summary>
    internal class UpdateRateProvider(IUnitOfWork dbLayer, IRateDataProvider rateDataProvider) : IUpdateRateProvider
    {
        /// <summary>
        /// Обновить стоимость тарифа для услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="newCost">Новая услуга</param>
        public void UpdateRateCost(Guid serviceTypeId, decimal newCost) =>
            UpdateRateCost(rateDataProvider.GetRateOrThrowException(serviceTypeId), newCost);

        /// <summary>
        /// Обновить стоимость тарифа для услуги
        /// </summary>
        /// <param name="rate">Тариф услуги</param>
        /// <param name="newCost">Новая услуга</param>
        public void UpdateRateCost(Rate rate, decimal newCost)
        {
            rate.Cost = newCost;
            dbLayer.RateRepository.Update(rate);
            dbLayer.Save();
        }
    }
}
