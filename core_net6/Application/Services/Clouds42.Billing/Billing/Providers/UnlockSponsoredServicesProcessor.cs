﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Обработчик продления заблокированных сервисов у спонсируемых аккаунтов.
    /// </summary>
    internal class UnlockSponsoredServicesProcessor(
        IDeleteUnusedResourcesProcessor deleteUnusedResourcesProcessor,
        IUnitOfWork dbLayer,
        ICloudServiceLockUnlockProvider cloudServiceLockUnlockProvider,
        IAccessProvider accessProvider,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
        : IUnlockSponsoredServicesProcessor
    {
        /// <summary>
        /// Выполнить обработку.
        /// </summary>
        /// <param name="configuration">Ресурс конфигурация сервиса.</param>
        public void Process(ResourcesConfiguration configuration)
        {
            var serviceName = configuration.GetLocalizedServiceName(cloudLocalizer);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                deleteUnusedResourcesProcessor.Process(configuration);                                     

                if (configuration.FrozenValue)
                    cloudServiceLockUnlockProvider.Unlock(configuration);

                configuration.ExpireDate = DateTime.Now.AddMonths(1);
                dbLayer.ResourceConfigurationRepository.Update(configuration);

                dbLayer.Save();

                LogEventHelper.LogEvent(dbLayer, configuration.AccountId, accessProvider, LogActions.ProlongationService,
                    $"Сервис {serviceName} продлен по спонсроской лицензии до {configuration.ExpireDate}.");
                dbScope.Commit();                    
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка покупки сервиса у компании] {configuration.BillingService.Name}  {configuration.AccountId}");
                LogEventHelper.LogEvent(dbLayer, configuration.AccountId, accessProvider, LogActions.ProlongationService,
                    $"Ошибка продления сервиса {serviceName} по спонсроской лицензии. {ex.Message}");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
