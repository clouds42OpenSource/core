﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для работы с данными тарифа услуги
    /// </summary>
    internal class RateDataProvider(IUnitOfWork dbLayer) : IRateDataProvider
    {
        /// <summary>
        /// Получить тариф услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="accountType">Тип аккаунта биллинга</param>
        /// <param name="localId">Id локали</param>
        /// <returns>Тариф услуги</returns>
        public Rate? GetRate(Guid serviceTypeId, string accountType, Guid? localId = null) =>
            dbLayer.RateRepository
                .OrderBy(r => r.Cost)
                .FirstOrDefault(r => r.BillingServiceType.Id == serviceTypeId &&
                                     r.AccountType == accountType &&
                                     (!localId.HasValue || r.LocaleId == localId));

        /// <summary>
        /// Получить тариф услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф услуги</returns>
        public Rate GetRate(Guid serviceTypeId) =>
            dbLayer.RateRepository.FirstOrDefault(w => w.BillingServiceTypeId == serviceTypeId);

        /// <summary>
        /// Получить тариф услуги или выкинуть исключение
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф услуги</returns>
        public Rate GetRateOrThrowException(Guid serviceTypeId) =>
            GetRate(serviceTypeId) ??
            throw new NotFoundException($"Не удалось получить тариф для услуги '{serviceTypeId}'");

        /// <summary>
        /// Получить стоимость услуги по тарифу
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Стоимость услугипо тарифу</returns>
        public decimal GetServiceTypeCost(Guid serviceTypeId) => GetRate(serviceTypeId)?.Cost ?? 0;

    }
}
