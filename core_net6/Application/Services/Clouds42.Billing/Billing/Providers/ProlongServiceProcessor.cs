﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Logger;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Обработчик продления сервиса.
    /// </summary>
    internal class ProlongServiceProcessor(
        ICloudServiceProcessor cloudServiceProcessor,
        IServiceUnlocker serviceUnlocker,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        ILogger42 logger)
        : IProlongServiceProcessor
    {
        /// <summary>
        /// Пролонгировать сервис.
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="prolongConditionFunc">Условие выполнения пролонгации сервиса.</param>
        public void ProlongService(ResourcesConfiguration resourcesConfiguration,
            Func<ResourcesConfiguration, bool> prolongConditionFunc)
        {
            if (!prolongConditionFunc(resourcesConfiguration))
            {
                logger.Trace(
                    $"Не выполненено условие выполнения пролонгации сервиса {resourcesConfiguration.BillingService.Name} у аккаунта {resourcesConfiguration.AccountId}");
                return;
            }

            if (!CanProlongServiceWithDependencies(resourcesConfiguration, out var reasonError))
            {
                logger.Trace(
                    $"Сервис {resourcesConfiguration.BillingService.Name} у аккаунта {resourcesConfiguration.AccountId} не будет продлен по причине '{reasonError}'");
                return;
            }

            serviceUnlocker.UnlockExpiredOrPayLockedService(resourcesConfiguration);
        }


        /// <summary>
        /// Проверить возможность продления основного сервиса
        /// и зависимых от него сервисов
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="reasonMsg">Текст причины не возможности пролонгации.</param>
        /// <returns>Признак возможности пролонгации.</returns>
        public bool CanProlongServiceWithDependencies(ResourcesConfiguration resourcesConfiguration,
            out string? reasonMsg)
        {
            var res = CanProlong(resourcesConfiguration, out reasonMsg);

            if (!res)
                return false;

            var dependentResConfs =
                resourceConfigurationDataProvider.GetConfigWithDependencies(resourcesConfiguration);

            if (!dependentResConfs.DependedResourceConfigurations.Any())
                return true;

            foreach (var dependentResConf in dependentResConfs.DependedResourceConfigurations)
            {
                res = CanProlong(dependentResConf, out reasonMsg);

                if (!res)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Проверить возможность продления сервиса
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="reasonMsg">Текст причины не возможности пролонгации.</param>
        /// <returns>Признак возможности пролонгации.</returns>
        private bool CanProlong(ResourcesConfiguration resourcesConfiguration, out string? reasonMsg)
        {
            string? reasonInter = null;
            var res = cloudServiceProcessor.Process(resourcesConfiguration,
                systemService => CanProlongSystemService(systemService, out reasonInter),
                () => true);

            reasonMsg = reasonInter;
            return res;
        }


        /// <summary>
        /// Определить возможность продлить системный сервис
        /// </summary>
        /// <param name="cloud42ServiceBase">Служба системного сервиса облака</param>
        /// <param name="reasonMsg">Текст причины не возможности пролонгации</param>
        /// <returns>Возможность продлить системный сервис</returns>
        private static bool CanProlongSystemService(Cloud42ServiceBase cloud42ServiceBase, out string? reasonMsg)
        {
            reasonMsg = null;

            if (cloud42ServiceBase.CloudServiceType == Clouds42Service.MyDisk)
                return true;

            return cloud42ServiceBase.CanProlong(out reasonMsg);
        }
    }
}
