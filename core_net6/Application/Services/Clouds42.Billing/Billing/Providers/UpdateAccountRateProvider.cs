﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для обновления тарифа услуги на аккаунт
    /// </summary>
    internal class UpdateAccountRateProvider(
        IAccountRateDataProvider accountRateDataProvider,
        IUnitOfWork dbLayer,
        ICreateAccountRateProvider createAccountRateProvider)
        : IUpdateAccountRateProvider
    {
        /// <summary>
        /// Обновить тариф услуги на аккаунт
        /// </summary>
        /// <param name="model">Модель обновления тарифа на аккаунт</param>
        public void Update(UpdateAccountRateDto model) => Update(
            accountRateDataProvider.GetAccountRateOrThrowException(model.AccountId, model.BillingServiceTypeId),
            model.Cost);

        /// <summary>
        /// Обновить тариф услуги на аккаунт
        /// если он существует, иначе создать
        /// </summary>
        /// <param name="model">Модель обновления тарифа на аккаунт</param>
        public void UpdateIfAvailableOtherwiseCreate(UpdateAccountRateDto model)
        {
            var accountRate = accountRateDataProvider.GetAccountRate(model.AccountId, model.BillingServiceTypeId);

            if (accountRate != null)
            {
                Update(accountRate, model.Cost);
                return;
            }

            createAccountRateProvider.Create(model);
        }

        /// <summary>
        /// Обновить тариф услуги на аккаунт
        /// </summary>
        /// <param name="accountRate">Тариф на аккант</param>
        /// <param name="cost">Новая стоимость тарифа</param>
        public void Update(AccountRate accountRate, decimal cost)
        {
            accountRate.Cost = cost;
            dbLayer.AccountRateRepository.Update(accountRate);
            dbLayer.Save();
        }
    }
}