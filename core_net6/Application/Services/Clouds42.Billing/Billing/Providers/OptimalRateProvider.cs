﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.Rate;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для работы с оптимальным тарифом
    /// </summary>
    internal class OptimalRateProvider(
        IAccountRateDataProvider accountRateDataProvider,
        IUnitOfWork dbLayer,
        IRateDataProvider rateDataProvider,
        ILogger42 logger,
        IConfiguration configuration)
        : IOptimalRateProvider
    {
        /// <summary>
        /// Получить оптимальный тариф для услуги
        /// (с учетом тарифа на аккаунт)
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Оптимальный тариф на услугу</returns>
        public OptimalRateDataDcDto GetOptimalRate(BillingAccount billingAccount, ResourceType systemServiceType) =>
            GetOptimalRate(billingAccount, GetServiceTypeId(systemServiceType));

        /// <summary>
        /// Получить оптимальный тариф для услуги
        /// (с учетом тарифа на аккаунт)
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Оптимальный тариф на услугу</returns>
        public OptimalRateDataDcDto GetOptimalRate(BillingAccount billingAccount, Guid serviceTypeId)
        {
            var accountRate = accountRateDataProvider.GetAccountRate(billingAccount.Id, serviceTypeId);
            if (accountRate != null)
            {
                logger.Debug($"Для аккаунта {billingAccount.Id} найден существующий тариф");
                return new OptimalRateDataDcDto(accountRate.Cost);
            }

            var rate = FindRate(billingAccount.Id, serviceTypeId, billingAccount.AccountType);

            if (rate != null)
            {
                return new OptimalRateDataDcDto(rate.Cost);
            }

            logger.Warn($"У данного сервиса {serviceTypeId} для аккаунта {billingAccount.Id} не найдено подходящего тарифа");

            throw new ArgumentNullException($"У данной услуги {serviceTypeId} не найдено подходящего тарифа");

        }

        /// <summary>
        /// Найти оптимальный тариф
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="accountType">Тип аккаунта биллинга</param>
        /// <returns>Оптимальный тариф услуги</returns>
        private Rate? FindRate(Guid accountId, Guid serviceTypeId, string accountType)
        {
            try
            {
                logger.Info($"Начало поиске тарифа услуги {serviceTypeId} для аккаунта {accountId} с типом билинга {accountType}");
                var locale = GetLocalId(accountId);
                var rate = rateDataProvider.GetRate(serviceTypeId, accountType, locale) ?? rateDataProvider.GetRate(serviceTypeId, accountType);

                logger.Info($"Закончили поиске тарифа услуги {serviceTypeId} для аккаунта {accountId} с типом билинга {accountType} нашли цену {rate?.Cost}");
                return rate;
            }
            catch (Exception ex)
            {
                logger.Warn($"При поиске тарифа услуги {serviceTypeId} для аккаунта {accountId} с типом билинга {accountType} возникла ошибка  {ex.Message}");
                var rate = rateDataProvider.GetRate(serviceTypeId, accountType);

                logger.Info($"Закончили поиске тарифа услуги {serviceTypeId} для аккаунта {accountId} с типом билинга {accountType} нашли цену {rate?.Cost}");

                return rate;
            }
        }
            

        /// <summary>
        /// Получить Id локали
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id локали</returns>
        private Guid GetLocalId(Guid accountId) =>
            dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(configuration["DefaultLocale"] ?? ""))?.ID ??
            throw new NotFoundException(
                $"По идентификатору аккаунта '{accountId}' не удалось найти локаль аккаунта.");

        /// <summary>
        /// Получить Id системной услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Id системной услуги</returns>
        private Guid GetServiceTypeId(ResourceType systemServiceType) =>
            dbLayer.BillingServiceTypeRepository.FirstOrDefault(st => st.SystemServiceType == systemServiceType)?.Id ??
            throw new NotFoundException($"Не удалось определить услугу по типу {systemServiceType}");
    }
}
