﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для удаления тарифа услуги
    /// </summary>
    internal class RemoveRateProvider(IUnitOfWork dbLayer, IRateDataProvider rateDataProvider) : IRemoveRateProvider
    {
        /// <summary>
        /// Удалить тариф для услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        public void Remove(Guid serviceTypeId)
        {
            var rate = rateDataProvider.GetRateOrThrowException(serviceTypeId);
            dbLayer.RateRepository.Delete(rate);
            dbLayer.Save();
        }
    }
}
