﻿using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Invoice.Helpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Invoice;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    ///     Класс для обработки уведомлений "Выставление счета"
    /// </summary>
    internal class InvoiceNotificationHelper(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        IAccountRequisitesDataProvider accountRequisitesDataProvider,
        ICloudLocalizer cloudLocalizer,
        ILogger42 logger)
        : IInvoiceNotificationHelper
    {
        private readonly Lazy<string> _invoiceAttachFileName = new(CloudConfigurationProvider.Invoices.GetInvoiceAttachFileName);
        private readonly Lazy<string> _uniqPrefix = new(CloudConfigurationProvider.Invoices.UniqPrefix);
        private readonly Lazy<string> _routeForOpenBalancePage = new(CloudConfigurationProvider.Cp.GetRouteForOpenBalancePage);
        private readonly Lazy<int> _promisePaymentDays = new(CloudConfigurationProvider.BillingAndPayment.PromisePayment.GetPromisePaymentDays);

        /// <summary>
        ///     Обработка уведомления Обещанного платежа
        /// </summary>
        /// <param name="invoice">Счет</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="promiseSum">Сумма</param>
        /// <param name="date">Дата</param>
        /// <param name="productsInfo">Список услуг</param>
        /// <param name="attachFile">Прикрепленный файл</param>
        public void CreatePromisePaymentNotify(Domain.DataModels.billing.Invoice invoice, Guid accountId, decimal promiseSum, DateTime date,
            string productsInfo, byte[] attachFile)
        {
            logger.Trace($"Id счета - {invoice.Id}");

            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);
            var accountUserId = accessProvider.GetAccountAdmins(account.Id).FirstOrDefault();

            if (accountUserId == Guid.Empty)
                accountUserId = accessProvider.GetUser().Id;

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountUserId);
            var accountRequisites = accountRequisitesDataProvider.GetIfExistsOrCreateNew(account.Id);

            var messageBody = ServicesEmailBuilder.GetPromisePaymentNotificationEmail(
                productsInfo,
                date,
                account,
                accountUser,
                accountRequisites.Inn,
                promiseSum, cloudLocalizer);

            logger.Trace($"Сформировали сообщение для сервисов/менеджера/партнеру - {messageBody}");

            var emailsForSendEmailCopy = GetEmailsForSendEmailCopy(accountId, accountUser);
            var sendStatus =
                letterNotificationProcessor
                    .TryNotify<TakingPromisePaymentLetterNotification, TakingPromisePaymentLetterModelDto>(
                        new TakingPromisePaymentLetterModelDto
                        {
                            AccountId = accountId,
                            InvoiceId = invoice.Id,
                            InvoiceDocument = new DocumentDataDto
                            {
                                FileName = _invoiceAttachFileName.Value,
                                Bytes = attachFile
                            },
                            EmailsToCopy = emailsForSendEmailCopy,
                            PromisePaymentDays = _promisePaymentDays.Value,
                            InvoiceSum = invoice.Sum,
                            InvoiceNumber = $"{_uniqPrefix.Value}{invoice.Uniq}"
                        });

            logger.Trace(
                $"Отправили ли сообщение клиенту - {sendStatus}, Аккаунт - {accountId}, Email - {accountUser.Email}");

            NotifyManager(messageBody);

            // уведомление в КОРП
            NotifyCorp(messageBody);

            logger.Trace($"Обработка завершена, счет - {invoice.Id}, аккаунт - {accountId}");
        }

        /// <summary>
        /// Создать уведомление счета
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="productsInfo">Информация о продуктах счета</param>
        /// <param name="attachFile">Прикрепленный файл</param>
        /// <param name="calculationOfInvoiceRequestModel">Модель калькуляции счета</param>
        public void CreateInvoiceNotify(Guid invoiceId, Guid accountId, string productsInfo, byte[] attachFile,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequestModel)
        {
            var invoice = dbLayer.InvoiceRepository.GetInvoiceById(invoiceId);
            logger.Trace($"Id счета - {invoice.Id}");

            var accountUserId = calculationOfInvoiceRequestModel.AccountUserIdForSendNotify.IsNullOrEmpty()
                ? accessProvider.GetUser().Id
                : calculationOfInvoiceRequestModel.AccountUserIdForSendNotify;

            var accountUser = GetAccountUserOrThrowException(accountUserId);

            var emailsForSendEmailCopy = GetEmailsForSendEmailCopy(accountId, accountUser);

            if (!calculationOfInvoiceRequestModel.NeedSendEmail)
                return;

            var siteAuthorityUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountId);

            var sendStatus = letterNotificationProcessor
                .TryNotify<SendInvoiceToClientLetterNotification, SendInvoiceToClientLetterModelDto>(
                    new SendInvoiceToClientLetterModelDto
                    {
                        AccountId = accountId,
                        InvoiceId = invoiceId,
                        AccountUserId = accountUserId,
                        InvoiceDocument = new DocumentDataDto
                        {
                            FileName = _invoiceAttachFileName.Value,
                            Bytes = attachFile
                        },
                        EmailsToCopy = emailsForSendEmailCopy,
                        InvoiceSum = invoice.Sum,
                        InvoiceNumber = $"{_uniqPrefix.Value}{invoice.Uniq}",
                        BalancePageUrl = new Uri($"{siteAuthorityUrl}/{_routeForOpenBalancePage.Value}").AbsoluteUri
                    });

            logger.Trace(
                $"Отправили ли сообщение клиенту - {sendStatus}, Аккаунт - {accountId}, Email - {accountUser.Email}");
        }

        /// <summary>
        /// Получить пользователя или выкинуть исключение
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Пользователь</returns>
        private AccountUser GetAccountUserOrThrowException(Guid accountUserId) =>
            dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountUserId) ??
            throw new NotFoundException($"Не удалось получить пользователя по Id {accountUserId}");

        /// <summary>
        /// Получить эл. адреса для отправки копии письма
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUser">Пользователь облака</param>
        /// <returns>Список эл. адресов для отправки копии письма</returns>
        private List<string> GetEmailsForSendEmailCopy(Guid accountId, AccountUser accountUser)
            => dbLayer.AccountEmailRepository.WhereLazy(w => w.AccountId == accountId).Select(w => w.Email)
                .Where(email => email != accountUser.Email).ToList();

        private static void NotifyManager(string messageBody)
        {
            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(CloudConfigurationProvider.Emails.Get42CloudsManagerEmail())
                .Body(messageBody)
                .Subject("Клиент выставил счет на оплату")
                .SendViaNewThread();
        }

        private static void NotifyCorp(string messageBody)
        {
            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(CloudConfigurationProvider.Emails.GetEfsolSalesEmail())
                .Body(messageBody)
                .Subject("Выставление счета - 42 облака")
                .SendViaNewThread();
        }
    }
}
