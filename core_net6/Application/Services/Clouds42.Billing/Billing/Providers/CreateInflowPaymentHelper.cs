﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Payments.Internal;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Хэлпер для создания входящих платежей
    /// </summary>
    public interface ICreateInflowPaymentHelper
    {
        /// <summary>
        /// Создать платеж и провести обработку входящего платежа.
        /// </summary>
        /// <param name="paymentDefinition">Данные платежа.</param>        
        PaymentOperationResult CreatePaymentAndProcessInflowPayment(PaymentDefinitionDto paymentDefinition);
    }

    /// <summary>
    /// Хэлпер для создания входящих платежей
    /// </summary>
    internal class CreateInflowPaymentHelper(
        IInflowPaymentProcessor inflowPaymentProcessor,
        IBillingPaymentsProvider billingPaymentsProvider)
        : ICreateInflowPaymentHelper
    {
        /// <summary>
        /// Создать платеж и провести обработку входящего платежа.
        /// </summary>
        /// <param name="paymentDefinition">Данные платежа.</param>        
        public PaymentOperationResult CreatePaymentAndProcessInflowPayment(PaymentDefinitionDto paymentDefinition)
        {
            var result = billingPaymentsProvider.CreatePayment(paymentDefinition);

            if (result == PaymentOperationResult.Ok && paymentDefinition.OperationType == PaymentType.Inflow)
                inflowPaymentProcessor.Process(paymentDefinition.Account);

            return result;
        }

    }
}
