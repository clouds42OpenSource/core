﻿using Clouds42.Billing.Billing.Models;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Billing.Billing.Providers
{
    public class ImportPayboxPaymentProvider(
        IHandlerException handlerException,
        IUnitOfWork uow,
        IConfirmPaymentProvider confirmPaymentProvider,
        ILogger42 logger,
        IAccessProvider accessProvider): IImportPayboxPaymentProvider
    {
        private readonly PayboxCryptoProvider _payboxCryptoProvider = new();

        /// <summary>
        /// Обработка платежа с Paybox.
        /// </summary>
        /// <returns></returns>
        public string PayboxImportResult(PayboxImportResultModel modelHandlingDto)
        {

            logger.Info($"(PayboxImportResult) :: Начало проверки платежа {modelHandlingDto.pg_order_id}");

            var payment = uow.PaymentRepository
                .AsQueryable()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .FirstOrDefault(p => p.Number == modelHandlingDto.pg_order_id);
           
            if (payment == null)
            {
                var message = $"По номеру '{modelHandlingDto.pg_order_id}' не найден платеж.";
                handlerException.HandleWarning($"Ошибка при импорте платежа {modelHandlingDto.pg_order_id}. {message}", modelHandlingDto.ToJson());
                var supplierId = uow.AccountsRepository.GetAccountByUserId(accessProvider.GetUser().Id)
                    .AccountConfiguration.SupplierId;
                return _payboxCryptoProvider.BuildResponseXml(modelHandlingDto.pg_salt, supplierId, true, message);
            }

            var signature = modelHandlingDto.GenerateSignature(payment.Account.AccountConfiguration.SupplierId);

            if (!_payboxCryptoProvider.IsSignatureValid(signature, modelHandlingDto.pg_sig))
            {
                var message = $"Ошибка при импорте платежа {modelHandlingDto.pg_order_id}. Контрольные суммы не совпадают!";
                handlerException.HandleWarning(message, modelHandlingDto.ToJson());
                return _payboxCryptoProvider.BuildResponseXml(modelHandlingDto.pg_salt, payment.Account.AccountConfiguration.SupplierId, true, message);
            }

            logger.Info($"(PayboxImportResult) :: Начало обработки платежа {modelHandlingDto.pg_order_id} на сумму тг.");

            if (payment.PaymentSystemTransactionId != null)
            {
                var message = $"Платеж с номером {modelHandlingDto.pg_payment_id} уже был осуществлен";
                return _payboxCryptoProvider.BuildResponseXml(modelHandlingDto.pg_salt, payment.Account.AccountConfiguration.SupplierId, true, message);
            }

            return ConfirmPayment(modelHandlingDto, payment);
        }

        private string ConfirmPayment(PayboxImportResultModel modelHandlingDto, Domain.DataModels.billing.Payment payment)
        {
            using var dbScope = uow.SmartTransaction.Get();
            try
            {
                if (modelHandlingDto.pg_result == 0)
                {
                    confirmPaymentProvider.ConfirmPayment(modelHandlingDto.pg_order_id, modelHandlingDto.pg_amount, false);

                    logger.Info(
                        $"Paybox: Отменяем платеж по номеру {modelHandlingDto.pg_order_id}! Результат платежа в системе Paybox == false");

                    LogEventHelper.LogEvent(uow, payment.AccountId, null, LogActions.RefillByPaymentSystem,
                        $"Отмена платежа через Paybox на сумму {modelHandlingDto.pg_amount} тг.");
                }
                else
                {
                    payment.Description += !string.IsNullOrEmpty(modelHandlingDto.pg_payment_system)
                        ? " (" + modelHandlingDto.pg_payment_system + ")"
                        : "";

                    var confirmResult = confirmPaymentProvider.ConfirmPayment(modelHandlingDto.pg_order_id,
                        modelHandlingDto.pg_amount, true);
                    payment.PaymentSystemTransactionId = modelHandlingDto.pg_payment_id.ToString();

                    logger.Info(
                        $"(PayboxImportResult) :: Подтвержение платежа {modelHandlingDto.pg_order_id} на сумму {modelHandlingDto.pg_amount} в статусе : {confirmResult}");

                    LogEventHelper.LogEvent(uow, payment.AccountId, null, LogActions.RefillByPaymentSystem,
                        $"Пополнение баланса через Paybox на сумму {modelHandlingDto.pg_amount} тг.");
                }

                var resultXml = _payboxCryptoProvider.BuildResponseXml(modelHandlingDto.pg_salt, payment.Account.AccountConfiguration.SupplierId);

                dbScope.Commit();

                return resultXml;
            }
            catch (Exception exception)
            {
                handlerException.Handle(exception, $"[Ошибка регистрации запроса Paybox]. Номер патежа '{modelHandlingDto.pg_order_id}'");
                dbScope.Rollback();
                return _payboxCryptoProvider.BuildResponseXml(modelHandlingDto.pg_salt, payment.Account.AccountConfiguration.SupplierId, true, exception.Message);
            }
        }
    }
}
