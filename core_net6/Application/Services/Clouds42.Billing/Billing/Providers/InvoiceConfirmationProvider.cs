﻿using System.Globalization;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер обработки принятия счетов на оплату.
    /// </summary>
    internal class InvoiceConfirmationProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ICreateInflowPaymentHelper createInflowPaymentHelper,
        IBonusPaymentProvider bonusPaymentProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
        : IInvoiceConfirmationProvider
    {
        /// <summary>
        /// Подтвердить счет на оплату.
        /// </summary>
        public void ConfirmInvoice(ConfirmationInvoiceDto confirmationInvoice)
        {
            using var dbTransaction = dbLayer.SmartTransaction.Get();
            try
            {
                var invoice = dbLayer.InvoiceRepository.GetInvoiceById(confirmationInvoice.ID) ??
                              throw new NotFoundException(
                                  $"По номеру '{confirmationInvoice.ID}' не удалось получить счет на оплату.");

                var localeCurrency = accountConfigurationDataProvider.GetAccountLocale(invoice.AccountId).Currency;
                var newPayment = CreateInflowPaymentDefinition(confirmationInvoice, invoice);

                var paymentState = createInflowPaymentHelper.CreatePaymentAndProcessInflowPayment(newPayment);

                if (paymentState == PaymentOperationResult.Ok)
                {
                    if (confirmationInvoice.sum != invoice.Sum)
                        invoice.BonusReward = null;

                    CreateInflowBonusPayment(confirmationInvoice, invoice);

                    invoice.State = InvoiceStatus.Processed.ToString();
                    invoice.PaymentID = newPayment.Id;
                    invoice.Sum = confirmationInvoice.sum;

                    dbLayer.InvoiceRepository.Update(invoice);
                    dbLayer.Save();

                    LogEventHelper.LogEvent(dbLayer, invoice.AccountId, accessProvider, LogActions.RefillByCorp,
                        $"Пополнение баланса по счету № {CloudConfigurationProvider.Invoices.UniqPrefix() + invoice.Uniq} на сумму {invoice.Sum} {localeCurrency} через {confirmationInvoice.GetPaymentSystem()}.");
                }
                else
                {
                    var message = $"Ошибка пополнения из КОРП. Статус - {paymentState}";
                    logger.Trace($"не удалось принять {message}");
                    LogEventHelper.LogEvent(dbLayer, invoice.AccountId, accessProvider, LogActions.RefillByCorp,
                        message);
                }

                dbTransaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка подтверждения счета на оплату] {confirmationInvoice.ID}");
                dbTransaction.Rollback();
                LogEventHelper.LogEvent(dbLayer, accessProvider.ContextAccountId, accessProvider,
                    LogActions.RefillByCorp, $"Ошибка пополнения из КОРП. {ex.Message}");
                throw new InvalidOperationException($"Ошибка пополнения из КОРП. {ex.Message}");
            }
        }

        /// <summary>
        /// Создать описание входящего платежа
        /// </summary>
        /// <param name="confirmationInvoice">Параметры подтверждения счета</param>
        /// <param name="invoice">Счет на оплату</param>
        /// <returns></returns>
        private static PaymentDefinitionDto CreateInflowPaymentDefinition(ConfirmationInvoiceDto confirmationInvoice,
            Domain.DataModels.billing.Invoice invoice)
            => new()
            {
                Account = invoice.AccountId,
                Date = confirmationInvoice.Date ?? DateTime.Now,
                OriginDetails = confirmationInvoice.OriginDetails ?? confirmationInvoice.GetPaymentSystem().ToString(),
                Id = Guid.NewGuid(),
                OperationType = PaymentType.Inflow,
                System = confirmationInvoice.GetPaymentSystem(),
                Status = PaymentStatus.Done,
                Description = string.IsNullOrEmpty(confirmationInvoice.Description)
                    ? $"Пополнение баланса по счету №{CloudConfigurationProvider.Invoices.UniqPrefix() + invoice.Uniq} от {invoice.Date.ToString("dd MMMM yyyy", CultureInfo.GetCultureInfo("ru-RU"))} г."
                    : confirmationInvoice.Description,
                Total = confirmationInvoice.sum
            };

        /// <summary>
        /// Создать входящий бонусный платеж
        /// </summary>
        /// <param name="confirmationInvoice">Модель подтверждения счета</param>
        /// <param name="invoice">Счет на оплату</param>
        private void CreateInflowBonusPayment(ConfirmationInvoiceDto confirmationInvoice, Domain.DataModels.billing.Invoice invoice)
        {
            if (confirmationInvoice.sum != invoice.Sum)
                return;

            if (!invoice.BonusReward.HasValue)
                return;

            if (accountConfigurationDataProvider.IsVipAccount(invoice.AccountId))
                return;

            bonusPaymentProvider.CreateInflowBonusPayment(invoice.AccountId, invoice);
        }
    }
}
