﻿using Clouds42.Billing.Billing.Helpers;
using Clouds42.Billing.Billing.ProvidedServices.Internal;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.BLL.Common.Extensions;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Разблокировщик платных сервисов аккаунта.
    /// </summary>
    public class ServiceUnlocker(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloudServiceLockUnlockProvider cloudServiceLockUnlockProvider,
        IDocLoaderByRent1CProlongationProcessor docLoaderByRent1CProlongationProcessor,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        ICreatePaymentHelper createPaymentHelper,
        IDeleteUnusedResourcesProcessor deleteUnusedResourcesProcessor,
        ICloudServiceAdapter cloudServiceAdapter,
        IIncreaseAgentPaymentProcessor increaseAgentPaymentProcessor,
        IBillingPaymentsProvider billingPaymentsProvider,
        ServiceTypesAvailabilityDateTimeForAccountHelper serviceTypesAvailabilityDateTimeForAccountHelper,
        IResourcesService resourcesService,
        ILogger42 logger,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
        : IServiceUnlocker
    {

        /// <summary>
        /// Разблокировать просроченные или купить заблокированные сервисы аккаунта.
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void UnlockExpiredOrPayLockedServicesAccount(Guid accountId)
        {
            var lockedResourceConfigurations = dbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Include(x => x.BillingService)
                .Where(x => x.Frozen == true && x.AccountId == accountId &&
                            x.BillingService.BillingServiceTypes.All(st => !st.DependServiceTypeId.HasValue))
                .ToList();

            if (lockedResourceConfigurations.Count == 0)
            {
                logger.Trace($"{accountId} :: Просроченных сервисов для продления не найдено.");
                return;
            }

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                lockedResourceConfigurations.ForEach(resConfig =>
                {
                    if (!NeedProcessingLockedResourcesConfiguration(resConfig))
                        return;
                    UnlockExpiredOrPayLockedService(resConfig);
                });

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                var message = $"{accountId} :: Ошибка разблокировки сервисов -> {ex.Message}";
                handlerException.Handle(ex, $"[Ошибка разблокировки сервисов] {accountId} ");
                LogEventHelper.LogEvent(dbLayer, accountId, accessProvider,
                    LogActions.ProlongationService, message);

                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Разблокировать просроченный или купить заблокированный сервис.
        /// </summary>
        /// <param name="configuration">Ресурс конфигурация сервиса.</param>
        public void UnlockExpiredOrPayLockedService(ResourcesConfiguration configuration)
        {
            if (configuration.ExpireDate?.Date <= DateTime.Now.Date)
            {
                if (configuration.BillingService.SystemService is Clouds42Service.Esdl or Clouds42Service.Recognition)
                    return;

                logger.Trace(
                    $"{configuration.AccountId} :: {configuration.BillingService.Name} :: попытка покупки доступа.");

                TryPayService(configuration);
            }
            else
            {
                logger.Trace(
                    $"{configuration.AccountId} :: {configuration.BillingService.Name} :: попытка разблокировать доступ.");

                cloudServiceLockUnlockProvider.Unlock(configuration);

                dbLayer.ResourceConfigurationRepository.Update(configuration);
                dbLayer.Save();

                LogEventHelper.LogEvent(dbLayer, configuration.AccountId, accessProvider,
                    LogActions.ProlongationService,
                    $"Сервис \"{configuration.GetLocalizedServiceName(cloudLocalizer)}\" разблокирован.");
            }
        }

        /// <summary>
        /// Покупка заблокированого сервиса если его стоимость 
        /// изменилась и денег на балансе достаточно для продления.        
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="oldCostOfService">Цена за сервис до пересчета</param>
        /// <param name="resourceConfiguration">Сервис</param>
        /// <param name="paymentActionType">Тип действия платежа</param>
        public void ProlongServiceIfTheCostIsLess(Guid accountId, decimal oldCostOfService,
            ResourcesConfiguration resourceConfiguration,
            PaymentActionType paymentActionType = PaymentActionType.PayService)
        {
            var serviceCost = resourceConfigurationDataProvider.GetTotalServiceCost(resourceConfiguration);
            var mainService = cloudServiceAdapter.GetMainConfigurationOrThrowException(resourceConfiguration);

            var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(accountId);

            if (serviceCost == oldCostOfService
                || !mainService.FrozenValue
                || billingAccount.GetAvailableBalance() < serviceCost
                || serviceCost <= 0 || billingAccount.IsPromisedPaymentExpired())
            {
                return;
            }

            logger.Info(
                $"Уменьшилась стоимость сервиса, попытка продлить сервис {mainService.BillingService.Name} для аккаунта {mainService.AccountId}");

            PayLockedService(mainService, paymentActionType);

            docLoaderByRent1CProlongationProcessor.ProcessIncrease(accountId);


            logger.Trace(
                $"Сервис {mainService.BillingService.Name} успешно продлен для акк. {mainService.AccountId}");

        }

        /// <summary>
        /// Попытка купить сервис.
        /// </summary>
        /// <param name="configuration">Ресурс конфигурация сервиса.</param>
        private void TryPayService(ResourcesConfiguration configuration)
        {
            var resConfModel = resourceConfigurationDataProvider.GetConfigWithDependencies(configuration);
            var totalServiceCost = resourceConfigurationDataProvider.GetTotalServiceCost(resConfModel);
            var canCreatePayment =
                billingPaymentsProvider.CanCreatePayment(resConfModel.ResourcesConfiguration.AccountId,
                    totalServiceCost, out _);

            if (canCreatePayment == PaymentOperationResult.Ok)
            {
                PayLockedService(configuration);
            }
        }

        /// <summary>
        /// Покупка заблокированного просроченного сервиса.
        /// </summary>
        /// <param name="configuration">Ресурс конфигурация сервиса.</param>
        /// <param name="paymentActionType">Тип действия платежа</param>
        private void PayLockedService(ResourcesConfiguration configuration,
            PaymentActionType paymentActionType = PaymentActionType.ProlongService)
        {
            if (!configuration.FrozenValue)
                return;

            var serviceName = configuration.GetLocalizedServiceName(cloudLocalizer);
            
                try
                {
                    var resConfModel =
                        resourceConfigurationDataProvider.GetConfigWithDependenciesIncludingDemo(configuration);
                    deleteUnusedResourcesProcessor.Process(resConfModel);
                    serviceTypesAvailabilityDateTimeForAccountHelper.Delete(
                        resConfModel.DependedResourceConfigurations);

                    var payments =
                        createPaymentHelper.MakePaymentsForResConfigDependencies(resConfModel, paymentActionType);
                    increaseAgentPaymentProcessor.Process(payments);

                    logger.Trace(
                        $"{configuration.AccountId}::{configuration.BillingService.Name} :: проведен платеж на сумму '{configuration.Cost}'");

                    cloudServiceLockUnlockProvider.Unlock(configuration);

                    configuration.ExpireDate = DateTime.Now.AddMonths(1);
                    dbLayer.ResourceConfigurationRepository.Update(configuration);

                    dbLayer.Save();

                    LogEventHelper.LogEvent(dbLayer, configuration.AccountId, accessProvider,
                        LogActions.ProlongationService,
                        $"Сервис \"{serviceName}\" продлен до {configuration.ExpireDate}.");
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Ошибка покупки сервиса у компании] {configuration.BillingService.Name} {configuration.AccountId} ");
                    LogEventHelper.LogEvent(dbLayer, configuration.AccountId, accessProvider,
                        LogActions.ProlongationService,
                        $"Ошибка покупки сервиса \"{serviceName}\". Описание ошибки: {ex.Message}");
                   
                    throw;
                }

            new ProvidedServiceHelper(dbLayer, handlerException).RegisterServiceProlongation(configuration);
        }

        /// <summary>
        /// Признак неообходимости обрабатывать заблокированную ресурс конфигурацию сервиса
        /// </summary>
        /// <param name="configuration">Ресурс конфигурация сервиса</param>
        /// <returns>true - если стоимость больше 0 либо есть активные спонсируемые лицензии</returns>
        private bool NeedProcessingLockedResourcesConfiguration(ResourcesConfiguration configuration)
            => configuration.Cost > 0 ||
               dbLayer.ResourceConfigurationRepository.IsMainServiceHasActiveSponsoredLicenses(configuration);
    }
}
