﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для работы с данными тарифа на аккаунт
    /// </summary>
    internal class AccountRateDataProvider(IUnitOfWork dbLayer) : IAccountRateDataProvider
    {
        /// <summary>
        /// Получить тариф услуги на аккаунт
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф услуги на аккаунт</returns>
        public AccountRate GetAccountRate(Guid accountId, Guid serviceTypeId) =>
            dbLayer.AccountRateRepository.FirstOrDefault(ar =>
                ar.AccountId == accountId && ar.BillingServiceTypeId == serviceTypeId);

        /// <summary>
        /// Получить тариф услуги на аккаунт
        /// или выкинуть исключение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф услуги на аккаунт</returns>
        public AccountRate GetAccountRateOrThrowException(Guid accountId, Guid serviceTypeId) =>
            GetAccountRate(accountId, serviceTypeId) ??
            throw new NotFoundException($"Не удалось получить тариф услуги '{serviceTypeId}' на аккаунт '{accountId}'");

        /// <summary>
        /// Получить тарифы услуг на аккаунт для сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Тарифы услуг на аккаунт для сервиса</returns>
        public IEnumerable<AccountRate> GetAccountRatesForService(Guid accountId, Guid serviceId) =>
            (from accountRate in dbLayer.AccountRateRepository.WhereLazy()
             join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on accountRate
                 .BillingServiceTypeId equals serviceType.Id
             where accountRate.AccountId == accountId && serviceType.ServiceId == serviceId
             select accountRate).AsEnumerable();
    }
}