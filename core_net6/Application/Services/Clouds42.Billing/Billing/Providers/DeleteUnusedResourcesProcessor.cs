﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Обработчик по удалению неиспользуемых ресурсов.
    /// </summary>
    public class DeleteUnusedResourcesProcessor(
        IResourcesService resourcesService,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        IActualizeMyDatabasesResourcesProvider actualizeMyDatabasesResourcesProvider,
        ILogger42 logger)
        : IDeleteUnusedResourcesProcessor
    {
        /// <summary>
        /// Удалить неиспользуемые ресурсы и перерасчитать стоимость сервиса. 
        /// </summary>        
        public void Process(IResourcesConfiguration service)
        {
            var billingService = service.GetBillingService();
            logger.Trace($"{service.AccountId}::{billingService.Name} :: очистка пустых ресурсвов и пересчет стоимости сервиса");
            resourcesService.DeleteUnusedResources(service.AccountId, billingService, service.IsDemoPeriod);
            ActualizeResources(service.AccountId, billingService);
            recalculateResourcesConfigurationCostProvider.Recalculate(service.AccountId, billingService);
        }

        /// <summary>
        /// Удалить неиспользуемые ресурсы и перерасчитать стоимость для сервиса
        /// и зависимых от него сервисов
        /// </summary>
        /// <param name="resConfModel">Запись о сервисе и его зависимых сервисах</param>
        public void Process(ResConfigDependenciesModel resConfModel)
        {
            Process(resConfModel.ResourcesConfiguration);

            foreach (var dependedResourceConfiguration in resConfModel.DependedResourceConfigurations)
            {
                Process(dependedResourceConfiguration);
            }
        }

        /// <summary>
        /// Актуализировать ресурсы для сервиса
        /// </summary>
        private void ActualizeResources(Guid accountId, IBillingService service)
        {
            if (service.SystemService != Clouds42Service.MyDatabases)
                return;

            actualizeMyDatabasesResourcesProvider.ActualizeForAccount(accountId);
        }
    }
}
