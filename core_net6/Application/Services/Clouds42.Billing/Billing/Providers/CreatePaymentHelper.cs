﻿using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.BillingOperations.Providers;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Хэлпер для создания платежей
    /// </summary>
    public sealed class CreatePaymentHelper : ICreatePaymentHelper
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly IResourceConfigurationDataProvider _resourceConfigurationDataProvider;
        private readonly ServiceTypeDataHelper _serviceTypeDataHelper;
        private readonly IBillingPaymentsProvider _billingPaymentsProvider;
        private readonly Lazy<Guid> _myDatabasesServiceId;
        private readonly Lazy<Guid> _rent1CServiceId;
        private readonly IHandlerException _handlerException;
        private readonly ICloudLocalizer _cloudLocalizer;
        public CreatePaymentHelper(
            IUnitOfWork dbLayer,
            IResourceConfigurationDataProvider resourceConfigurationDataProvider,
            ServiceTypeDataHelper serviceTypeDataHelper,
            ILogger42 logger, IHandlerException handlerException, ICloudLocalizer cloudLocalizer)
        {
            _dbLayer = dbLayer;
            _resourceConfigurationDataProvider = resourceConfigurationDataProvider;
            _serviceTypeDataHelper = serviceTypeDataHelper;
            _billingPaymentsProvider = new BillingPaymentsProvider(dbLayer, new BillingPaymentsHelper(dbLayer), logger, handlerException);
            _myDatabasesServiceId = new Lazy<Guid>(() => GetSystemServiceId(Clouds42Service.MyDatabases));
            _rent1CServiceId = new Lazy<Guid>(() => GetSystemServiceId(Clouds42Service.MyEnterprise));
            _handlerException = handlerException;
            _cloudLocalizer = cloudLocalizer;
        }

        /// <summary>
        /// Провести платежи за услуги
        /// </summary>
        /// <param name="resConfModel">Ресурс конфигурация сервиса с зависимостями</param>
        /// <param name="paymentActionType">Тип действия платежа</param>
        /// <returns>Результат платежной операции</returns>
        public List<Guid> MakePaymentsForResConfigDependencies(
            ResConfigDependenciesModel resConfModel,
            PaymentActionType paymentActionType = PaymentActionType.ProlongService)
        {
            var resultList = new List<Guid>();

            var totalServiceCost = _resourceConfigurationDataProvider.GetTotalServiceCost(resConfModel.ResourcesConfiguration);
            var canCreatePayment = _billingPaymentsProvider.CanCreatePayment(
                resConfModel.ResourcesConfiguration.AccountId,
                totalServiceCost, out _);

            if (canCreatePayment != PaymentOperationResult.Ok)
            {
                throw new InvalidOperationException(
                    $"Не удалось провести платеж аккаунту '{resConfModel.ResourcesConfiguration.AccountId}' на сумму '{totalServiceCost}'");
            }

            using var transaction = _dbLayer.SmartTransaction.Get();
            try
            {
                var errorMessage =
                    $"Во время создания платежа для аккаунта {resConfModel.ResourcesConfiguration.AccountId} произошла ошибка.";

                var paymentResult = MakePayment(resConfModel.ResourcesConfiguration,
                    GetPaymentDefinition(resConfModel.ResourcesConfiguration, paymentActionType),
                    resConfModel.ResourcesConfiguration.Cost, PaymentSystem.ControlPanel, PaymentType.Outflow,
                    "core-scheduler");

                if (paymentResult.OperationStatus != PaymentOperationResult.Ok || !paymentResult.PaymentIds.Any())
                    throw new InvalidOperationException(errorMessage);

                resultList.AddRange(paymentResult.PaymentIds);

                var dependedResourceConfigurationsWhichIsNotDemo = resConfModel.DependedResourceConfigurations
                    .Where(w => !w.IsDemoPeriod).ToList();

                dependedResourceConfigurationsWhichIsNotDemo.ForEach(resourcesConfiguration =>
                {
                    paymentResult = MakePayment(resourcesConfiguration,
                        GetPaymentDefinition(resourcesConfiguration, paymentActionType),
                        resourcesConfiguration.Cost, PaymentSystem.ControlPanel, PaymentType.Outflow,
                        "core-scheduler");

                    if (paymentResult.OperationStatus != PaymentOperationResult.Ok ||
                        !paymentResult.PaymentIds.Any())
                        throw new InvalidOperationException(errorMessage);

                    resultList.AddRange(paymentResult.PaymentIds);
                });

                transaction.Commit();

                return resultList;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка перевода платежей за услуги]");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Проводим платеж за услугу.
        /// </summary>        
        /// <param name="cost">Сумма платежа</param>
        /// <param name="configuration">Услуга по которой производится платеж.</param>        
        /// <param name="paymentDescription">Комментарий</param>
        /// <param name="paymentSystem">Платежная система</param>
        /// <param name="type">Тип - поступление/списание</param>
        /// <param name="originDetails">Комментарий к платежу</param>
        /// <param name="canOverdraft">Платеж в долг.</param>
        public CreatePaymentResultDto MakePayment(ResourcesConfiguration configuration, string paymentDescription,
            decimal cost, PaymentSystem paymentSystem, PaymentType type, string originDetails,
            bool canOverdraft = false)
        {
            var paymentDefinitions = CreatePaymentDefinitionsForService(configuration, paymentDescription, cost,
                paymentSystem, type, originDetails, canOverdraft);

            var paymentIdsList = new List<Guid>();

            paymentDefinitions.ForEach(payment =>
            {
                var result = _billingPaymentsProvider.CreatePayment(payment);
                if (result == PaymentOperationResult.Ok)
                    paymentIdsList.Add(payment.Id);
            });

            return paymentIdsList.Count != paymentDefinitions.Count
                ? new CreatePaymentResultDto(PaymentOperationResult.ErrorInvalidOperation)
                : new CreatePaymentResultDto(PaymentOperationResult.Ok, paymentIdsList);
        }

        /// <summary>
        /// Создать список платежей за услугу.
        /// </summary>        
        /// <param name="cost">Сумма платежа</param>
        /// <param name="configuration">Услуга по которой производится платеж.</param>        
        /// <param name="paymentDescription">Комментарий</param>
        /// <param name="paymentSystem">Платежная система</param>
        /// <param name="operationType">Тип - поступление/списание</param>
        /// <param name="originDetails">Комментарий к платежу</param>
        /// <param name="canOverdraft">Платеж в долг.</param>
        /// <returns>Список платежей за услугу</returns>
        private List<PaymentDefinitionDto> CreatePaymentDefinitionsForService(ResourcesConfiguration configuration,
            string paymentDescription,
            decimal cost, PaymentSystem paymentSystem, PaymentType operationType, string originDetails,
            bool canOverdraft = false)
        {
            var paymentDefinitions = new List<PaymentDefinitionDto>();

            var billingAccount = _dbLayer.BillingAccountRepository.GetBillingAccount(configuration.AccountId);

            if (cost <= billingAccount.Balance)
            {
                paymentDefinitions.Add(CreatePaymentDefinition(configuration, paymentDescription, cost, paymentSystem,
                    operationType, originDetails, TransactionType.Money, canOverdraft));
                return paymentDefinitions;
            }

            var bonusCountForProlong = cost - billingAccount.Balance;

            var moneyPayment = CreatePaymentDefinition(configuration, paymentDescription, billingAccount.Balance,
                paymentSystem, operationType, originDetails, TransactionType.Money, canOverdraft);
            var bonusPayment = CreatePaymentDefinition(configuration, paymentDescription, bonusCountForProlong,
                paymentSystem, operationType, originDetails, TransactionType.Bonus, canOverdraft);

            paymentDefinitions.Add(moneyPayment);
            paymentDefinitions.Add(bonusPayment);

            return paymentDefinitions;
        }

        /// <summary>
        /// Создать описание платежа
        /// </summary>
        /// <param name="cost">Сумма платежа</param>
        /// <param name="configuration">Услуга по которой производится платеж.</param>        
        /// <param name="paymentDescription">Комментарий</param>
        /// <param name="paymentSystem">Платежная система</param>
        /// <param name="operationType">Тип - поступление/списание</param>
        /// <param name="originDetails">Комментарий к платежу</param>
        /// <param name="transactionType">Тип транзакции</param>
        /// <param name="canOverdraft">Платеж в долг.</param>
        /// <returns></returns>
        private static PaymentDefinitionDto CreatePaymentDefinition(ResourcesConfiguration configuration,
            string paymentDescription,
            decimal cost, PaymentSystem paymentSystem, PaymentType operationType, string originDetails,
            TransactionType transactionType,
            bool canOverdraft = false)
            => new()
            {
                Account = configuration.AccountId,
                Date = DateTime.Now,
                Description = paymentDescription,
                Id = Guid.NewGuid(),
                BillingServiceId = configuration.BillingServiceId,
                Status = PaymentStatus.Done,
                Total = cost,
                System = paymentSystem,
                OperationType = operationType,
                OriginDetails = originDetails,
                CanOverdraft = canOverdraft,
                TransactionType = transactionType
            };

        /// <summary>
        /// Получить описание платежа
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса</param>
        /// <param name="paymentActionType">Тип действия платежа</param>
        /// <returns>Описание платежа</returns>
        private string GetPaymentDefinition(ResourcesConfiguration resourcesConfiguration,
            PaymentActionType paymentActionType)
        {
            var serviceName = resourcesConfiguration.GetLocalizedServiceName(_cloudLocalizer);

            var paymentDefinition =
                $"{paymentActionType.Description()} \"{serviceName}\"";

            if (resourcesConfiguration.BillingServiceId != _myDatabasesServiceId.Value &&
                resourcesConfiguration.BillingServiceId != _rent1CServiceId.Value)
                return paymentDefinition;

            return
                $"{paymentDefinition} ({_serviceTypeDataHelper.GetPaidServiceTypesDefinition(resourcesConfiguration.AccountId, resourcesConfiguration.BillingServiceId, paymentActionType)})";
        }

        /// <summary>
        /// Получить Id системного сервиса
        /// </summary>
        /// <returns>Id системного сервиса</returns>
        private Guid GetSystemServiceId(Clouds42Service systemService) =>
            _dbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == systemService)?.Id ??
            throw new NotFoundException($"Не удалось получить сервис - {systemService}");
    }
}
