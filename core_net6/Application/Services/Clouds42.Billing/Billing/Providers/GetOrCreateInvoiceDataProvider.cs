﻿using System.Text.RegularExpressions;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Invoice.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер получения или создания нового счета на оплату.
    /// </summary>
    internal class GetOrCreateInvoiceDataProvider(
        IUnitOfWork dbLayer,
        PaymentsManager paymentsManager,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
        : IGetOrCreateInvoiceDataProvider
    {
        private readonly Lazy<string> _invoiceNumberPrefix = new(CloudConfigurationProvider.Invoices.UniqPrefix);

        /// <summary>
        /// Получить счет на оплату или создать по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="invoiceNumber">Номер счета.</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        public async Task<InvoicePaymentsDto?> GetProcessingOrCreateNewInvoiceAsync(string invoiceNumber, decimal sum)
        {
            var uniq = ExtractUniqFromInvoiceNumber(invoiceNumber);
            logger.Info($"Получить счет на оплату: номер инвойса {invoiceNumber}, на сумму {sum} и uniq {uniq} ");
            var invoice =
                await dbLayer.InvoiceRepository.GetProcessingOrCreateNewInvoiceAsync(sum, uniq,
                    _invoiceNumberPrefix.Value);

            if (invoice != null)
            {
                logger.Info($"Получили счет на оплату: номер инвойса {invoice.Uniq}, на сумму {invoice.Sum} и статус {invoice.State} ");
                return CreateInvoiceDto(invoice);
            }

            logger.Info($"Не нашли номер инвойса {invoiceNumber}, на сумму {sum} и uniq {uniq} ");
            return null;
        }

        /// <summary>
        /// Получить необработанный инвойс на заданную сумму.
        /// </summary>
        /// <param name="sum">Сумма счёта.</param>
        /// <param name="inn">ИНН аккаунта.</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        public async Task<InvoicePaymentsDto?> GetProcessingInvoice(string inn, decimal sum)
        {
            var invoice = await dbLayer.InvoiceRepository.GetProcessingInvoiceAsync(inn, sum);
            return CreateInvoiceDto(invoice);
        }

        /// <summary>
        /// Получить или создать счёт по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="forceCreate">Флаг принудительного создания счёта</param>
        /// <param name="invoiceNumber">Номер инвойса.</param>
        /// <param name="inn">ИНН аккаунта для которого будет сформирован или получен счёт</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        public async Task<InvoicePaymentsDto?> GetOrCreateInvoiceAsync(string invoiceNumber, bool forceCreate, string inn,
            decimal sum)
        {
            var uniq = ExtractUniqFromInvoiceNumber(invoiceNumber);

            var invoice = await dbLayer.InvoiceRepository.GetOrCreateInvoiceAsync(forceCreate, inn, sum, uniq);

            return invoice != null ? CreateInvoiceDto(invoice) : null;
        }

        /// <summary>
        /// Создать ответ InvoideDto
        /// </summary>
        /// <param name="invoice">Доменная модель счета на оплату.</param>
        private InvoicePaymentsDto CreateInvoiceDto(Domain.DataModels.billing.Invoice invoice)
        {
            var locale = accountConfigurationDataProvider.GetAccountLocale(invoice.AccountId);

            return new InvoicePaymentsDto
            {
                Id = invoice.Id,
                AccountId = invoice.AccountId,
                State = invoice.State,
                Sum = invoice.Sum,
                Description = InvoiceAdditionalDataHelper.GetProductsDescription(invoice, locale.Name),
                Date = invoice.Date,
                Requisite = invoice.Requisite,
                Uniq = paymentsManager.ConstructInvoiceNumberFromUniq(invoice.Uniq),
                Currency = locale.CurrencyCode.ToString()
            };
        }

        /// <summary>
        /// Проверяет соответствие строкового номера счёта invoiceNumber, регулярному выражению,
        /// состоящему из префикса, хранящегося в файле App.config под элементом с названием Invoice_UniqPrefix
        /// и некоторого уникального числа Uniq, по которому осуществляется индексирование счетов в таблице Invoices.
        /// </summary>
        /// <param name="invoiceNumber">Строковое представление номера счёта</param>
        /// <returns>Величина типа long, соответствующая извлеченному номеру Uniq или -1, если извлечение прошло неудачно</returns>
        private long ExtractUniqFromInvoiceNumber(string invoiceNumber)
        {
            var invoiceNumberRegEx = new Regex($"^{_invoiceNumberPrefix.Value}(?<uniq>\\d*)");

            var matches = invoiceNumberRegEx.Matches(invoiceNumber);

            if (matches.Count == 1 && long.TryParse(matches[0].Groups["uniq"].Value, out var result)) return result;
            return -1;
        }
    }
}
