﻿using System.Text;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    internal class PayboxPaymentStatusProvider(IUnitOfWork dbLayer, ILogger42 logger)
    {
        /// <summary>
        ///     Проверка платежа после удачного/неудачного вызова метода Result
        /// </summary>
        /// <param name="paymentNumber"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string GetPaymentStatus(int paymentNumber, string description = "")
        {
            var payment = dbLayer.PaymentRepository.FirstOrDefault(x => x.Number == paymentNumber);
            var message = new StringBuilder();

            if (payment == null)
            {
                message.Append($"Платеж по номеру {paymentNumber} не найден!");
                logger.Trace("(Paybox :: CheckPaymentStatus) :: " + message);
                return message.ToString();
            }

            if (payment.StatusEnum == PaymentStatus.Done)
            {
                message.Append($"Платеж по номеру {payment.Number} оплачен!");
                logger.Trace("(Paybox :: CheckPaymentStatus) :: " + message);
                return message.ToString();
            }
            if (payment.StatusEnum == PaymentStatus.Canceled)
            {
                message.Append($"Платеж по номеру {payment.Number} отменен! Причина - {description}");
                logger.Trace("(Paybox :: CheckPaymentStatus) :: " + message);
                return message.ToString();
            }
            if (payment.StatusEnum == PaymentStatus.Waiting)
            {
                message.Append($"Ожидайте зачисления оплаты в течении двух часов. Платеж по номеру {payment.Number}");
                logger.Trace("(Paybox :: CheckPaymentStatus) :: " + message);
                return message.ToString();
            }

            message.Append("Не известный статус платежа");
            logger.Trace("(Paybox :: CheckPaymentStatus) :: " + message);
            return message.ToString();
        }
    }
}
