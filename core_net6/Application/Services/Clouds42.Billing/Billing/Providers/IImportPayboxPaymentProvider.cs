﻿using Clouds42.Billing.Billing.Models;

namespace Clouds42.Billing.Billing.Providers
{
    public interface IImportPayboxPaymentProvider
    {
        /// <summary>
        /// Обработка платежа с Paybox.
        /// </summary>
        /// <returns></returns>
        string PayboxImportResult(PayboxImportResultModel modelHandlingDto);
    }
}
