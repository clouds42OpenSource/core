﻿using System.Text;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Billing.Helpers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Обработчик продления сервиса или блокирования.
    /// </summary>
    internal class ProlongActiveOrLockServiceProcessor(
        IUnitOfWork dbLayer,
        ICloudServiceLockUnlockProvider cloudServiceLockUnlockProvider,
        IAccessProvider accessProvider,
        IDeleteUnusedResourcesProcessor deleteUnusedResourcesProcessor,
        ICreatePaymentHelper createPaymentHelper,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IDocLoaderByRent1CProlongationProcessor docLoaderByRent1CProlongationProcessor,
        IIncreaseAgentPaymentProcessor increaseAgentPaymentProcessor,
        IProlongServiceProcessor prolongServiceProcessor,
        IProvidedServiceHelper providedServiceHelper,
        IHandlerException handlerException,
        IBillingPaymentsProvider billingPaymentsProvider,
        ServiceTypesAvailabilityDateTimeForAccountHelper serviceTypesAvailabilityDateTimeHelper,
        IBeforeLockServiceNotificateProcessor beforeLockServiceNotificationProcessor,
        ICloudLocalizer cloudLocalizer,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
        : IProlongActiveOrLockServiceProcessor
    {
        private readonly Lazy<string> _routeForOpenBalancePage = new(CloudConfigurationProvider.Cp.GetRouteForOpenBalancePage);

        /// <summary>
        /// Продлить или заблокировать активный сервис.
        /// </summary>
        /// <param name="resourceConfiguration">Конфигурация сервиса.</param>        
        public void Process(ResourcesConfiguration resourceConfiguration)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var resConfModel =
                    resourceConfigurationDataProvider
                        .GetConfigWithDependenciesIncludingDemo(resourceConfiguration);

                deleteUnusedResourcesProcessor.Process(resConfModel);

                if (TryPayIfCanProlong(resConfModel, out var reasonMsg, out var payments,
                        out var needSendInvoice))
                {
                    ProlongService(resConfModel, resourceConfiguration, payments);
                }
                else
                {
                    LockService(resConfModel.ResourcesConfiguration, reasonMsg);
                    beforeLockServiceNotificationProcessor.Notify(resourceConfiguration, reasonMsg, needSendInvoice);
                }

                serviceTypesAvailabilityDateTimeHelper.Delete(resConfModel.DependedResourceConfigurations);

                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"[Ошибка пролонгации сервиса] '{resourceConfiguration.BillingService.Name}' у аккаунта '{resourceConfiguration.AccountId}'";
                handlerException.Handle(ex, errorMessage);

                dbScope.Rollback();
            }
        }

        /// <summary>
        /// Выполнить обработку по начислению лицензий в рамках активного сервиса Аренда 1С.
        /// </summary>
        /// <param name="needProcess">Признак нужно ли выполнять обработку.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        private void DocLoaderProlongationProcess(bool needProcess, Guid accountId)
        {
            if (needProcess)
            {
                logger.Trace($"Запускаем обработку начисления беслптных ЗД для аккаунтиа '{accountId}'");
                docLoaderByRent1CProlongationProcessor.ProcessIncrease(accountId);
            }
        }

        /// <summary>
        /// Попытаться провести оплату за сервис если есть возможность пролонгации.
        /// </summary>
        /// <param name="resConfModel">Ресурс конфигурация сервиса с зависимостями.</param>
        /// <param name="reasonMsg">Текст причины ошибки.</param>
        /// <param name="paymentsIds">Список созданных платежей.</param>
        /// <param name="needSendInvoice">Необходимость отправлять счет</param>
        /// <returns>Признак успешности проведенного платежа.</returns>
        private bool TryPayIfCanProlong(ResConfigDependenciesModel resConfModel,
            out string? reasonMsg, out List<Guid> paymentsIds, out bool needSendInvoice)
        {
            needSendInvoice = false;

            if (!prolongServiceProcessor.CanProlongServiceWithDependencies(resConfModel.ResourcesConfiguration,
                out reasonMsg))
            {
                paymentsIds = [];
                logger.Trace(
                    $"У аккаунта '{resConfModel.ResourcesConfiguration.AccountId}' сервис '{resConfModel.ResourcesConfiguration.BillingService.Name}' не будет продлен по причине :: '{reasonMsg}'");
                return false;
            }

            var totalServiceCost =
                resourceConfigurationDataProvider.GetTotalServiceCost(resConfModel.ResourcesConfiguration);

            var canCreatePayment = billingPaymentsProvider.CanCreatePayment(
                resConfModel.ResourcesConfiguration.AccountId,
                totalServiceCost, out _);

            if (canCreatePayment != PaymentOperationResult.Ok)
            {
                reasonMsg = GenerateMessageAboutLackOfBalance(totalServiceCost,
                    resConfModel.ResourcesConfiguration.BillingAccounts);

                paymentsIds = [];
                needSendInvoice = true;
                return false;
            }

            paymentsIds = createPaymentHelper.MakePaymentsForResConfigDependencies(resConfModel);
            return true;
        }

        /// <summary>
        /// Сгенерировать сообщение о недостаточном балансе
        /// </summary>
        /// <param name="totalServiceCost">Общая стоимость сервисов</param>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <returns>Сообщение</returns>
        private string GenerateMessageAboutLackOfBalance(decimal totalServiceCost, BillingAccount billingAccount)
        {
            var siteAuthorityUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(billingAccount.Id);
            var currencyCode = accountConfigurationDataProvider.GetAccountLocale(billingAccount.Id).Currency;
            var balanceUrl = new Uri($"{siteAuthorityUrl}/{_routeForOpenBalancePage.Value}").AbsoluteUri;

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("К сожалению, мы не получили от вас оплату за продление подписки. <br/> <br/>");
            builder.AppendLine(
                $@"Если вы хотите продолжить работу, пожалуйста, пополните баланс в личном кабинете или воспользуйтесь услугой «Обещанный платеж» (услуга доступна всем, кто хоть раз проводил оплату). 
                Активировать услугу можно в личном кабинете во вкладке <a href=""{balanceUrl}"">«Баланс»</a>. <br/> <br/>");
            builder.AppendLine($"Стоимость сервисов: {totalServiceCost:0.00} {currencyCode} <br/> <br/>");
            builder.AppendLine("Текущий баланс <br/>");
            builder.AppendLine($"&emsp;- основной счет: {billingAccount.Balance:0.00} {currencyCode} <br/>");
            builder.AppendLine($"&emsp;- бонусный счет: {billingAccount.BonusBalance:0.00} {currencyCode} <br/>");

            return builder.ToString();
        }

        /// <summary>
        /// Продлить или разблокировать сервис.
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        private void ProlongOrUnlockService(ResourcesConfiguration resourcesConfiguration)
        {
            if (resourcesConfiguration.FrozenValue)
            {
                UnlockService(resourcesConfiguration);
            }

            resourcesConfiguration.ExpireDate = DateTime.Now.AddMonths(1);
            dbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);

            dbLayer.Save();

            LogEventHelper.LogEvent(dbLayer, resourcesConfiguration.AccountId, accessProvider,
                LogActions.ProlongationService,
                $"Сервис \"{resourcesConfiguration.GetLocalizedServiceName(cloudLocalizer)}\" продлен до \"{resourcesConfiguration.ExpireDateValue:D}\"");
        }

        /// <summary>
        /// Заблокировать сервис.
        /// </summary>        
        private void LockService(ResourcesConfiguration configuration, string? reasonMsg)
        {
            if (configuration.FrozenValue)
                return;

            cloudServiceLockUnlockProvider.Lock(configuration);

            LogEventHelper.LogEvent(dbLayer, configuration.AccountId, accessProvider, LogActions.LockService,
                $"Сервис \"{configuration.GetLocalizedServiceName(cloudLocalizer)}\" заблокирован. Причина: {reasonMsg.ClearHtmlTagsFromString()}");
        }

        /// <summary>
        /// Разблокировать сервис.
        /// </summary>        
        private void UnlockService(ResourcesConfiguration configuration)
        {
            if (!configuration.FrozenValue)
                return;

            cloudServiceLockUnlockProvider.Unlock(configuration);
        }

        /// <summary>
        /// Продлить сервис
        /// </summary>
        /// <param name="resConfModel">Структура ресурс конфигурации сервиса с зависимостями</param>
        /// <param name="resourceConfiguration">Ресурс конфигурация сервиса</param>
        /// <param name="paymentsIds">Список ID платежей</param>
        private void ProlongService(ResConfigDependenciesModel resConfModel,
            ResourcesConfiguration resourceConfiguration, List<Guid> paymentsIds)
        {
            ProlongOrUnlockService(resConfModel.ResourcesConfiguration);
            DocLoaderProlongationProcess(
                resourceConfiguration.BillingService.SystemService == Clouds42Service.MyEnterprise,
                resourceConfiguration.AccountId);
            increaseAgentPaymentProcessor.Process(paymentsIds);
            providedServiceHelper.RegisterServiceProlongation(resourceConfiguration);
        }
    }
}
