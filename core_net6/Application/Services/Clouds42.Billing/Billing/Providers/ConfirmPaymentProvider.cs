﻿using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Payments.Internal;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер подтверждения платежей
    /// </summary>
    internal class ConfirmPaymentProvider(
        IUnitOfWork dbLayer,
        IInflowPaymentProcessor inflowPaymentProcessor,
        IBonusPaymentProvider bonusPaymentProvider,
        IBillingPaymentsProvider billingPaymentsProvider,
        IAccountRequisitesDataProvider accountRequisitesDataProvider,
        CreatePaymentManager createPaymentManager,
        ILogger42 logger)
        : IConfirmPaymentProvider
    {
        /// <summary>
        /// Подтвердить платеж.
        /// </summary>
        /// <param name="num">Номер платежа</param>
        /// <param name="sum">Сумма платежа</param>
        /// <param name="accepted">Принятый</param>
        /// <returns>Результат платежной операции</returns>
        public PaymentOperationResult ConfirmPayment(int num, decimal sum, bool accepted)
        {
            Domain.DataModels.billing.Invoice invoice = new Domain.DataModels.billing.Invoice();
            var payment = dbLayer.PaymentRepository.FindPayment(num);
            if (payment == null)
                return PaymentOperationResult.ErrorPaymentNotFound;

            var account = dbLayer.BillingAccountRepository.GetBillingAccount(payment.AccountId);
            if (account == null)
                return PaymentOperationResult.ErrorAccountNotFound;

            if (payment.Status == PaymentStatus.Done.ToString()) return PaymentOperationResult.Ok;
            if (payment.Status == PaymentStatus.Canceled.ToString())
            {
                logger.Trace($"Попытка подтвердить отмененный платеж {num}");
                return PaymentOperationResult.ErrorInvalidOperation;
            }

            if (payment.Status != PaymentStatus.Waiting.ToString())
                return PaymentOperationResult.ErrorInvalidOperation;

            if (accepted)
                payment.Sum = sum;

            payment.Status = accepted ? PaymentStatus.Done.ToString() : PaymentStatus.Canceled.ToString();
            payment.Date = DateTime.Now;
            dbLayer.PaymentRepository.Update(payment);
            dbLayer.Save();

            if (accepted) //создали счет
            {
                invoice = UpdateOrCreateInvoice(payment);

                if (invoice.BonusReward.HasValue)
                    bonusPaymentProvider.CreateInflowBonusPayment(payment.AccountId, invoice);
            }

            logger.Trace("Платеж № {0} на сумму {1}. В статусе: {2} .", num, sum, payment.Status);

            billingPaymentsProvider.RecalculateAccountTotals(account.Id);
            
            var product = invoice?.InvoiceProducts?.FirstOrDefault(p => p.ServiceName == "AlpacaMeet");

            if(payment.OperationTypeEnum == PaymentType.Inflow && product != null && product.Count == 1)
            {
                var newPayment = new PaymentDefinitionDto
                {
                    Account = payment.AccountId,
                    Date = payment.Date,
                    OriginDetails = "core-cp",
                    Id = Guid.NewGuid(),
                    OperationType = PaymentType.Outflow,
                    System = PaymentSystem.ControlPanel,
                    Status = PaymentStatus.Done,
                    Description = "Списание денежных средств за минуты в сервисе AlpacaMeet",
                    Total = payment.Sum,
                    TransactionType = payment.TransactionType
                };

                var result = createPaymentManager.AddPayment(newPayment);
                return result.Result;
            }


            if (payment.OperationTypeEnum == PaymentType.Inflow)
                inflowPaymentProcessor.Process(account.Id);

            return PaymentOperationResult.Ok;
        }

        /// <summary>
        /// Обновить или создать счет на оплату
        /// </summary>
        /// <param name="payment">Платеж</param>
        /// <returns>Счет на оплату</returns>
        private Domain.DataModels.billing.Invoice UpdateOrCreateInvoice(Domain.DataModels.billing.Payment payment)
        {
            var invoice = dbLayer.InvoiceRepository
                .Where(x => x.AccountId == payment.AccountId &&
                            x.State == InvoiceStatus.Processing.ToString() &&
                            x.Sum == payment.Sum).MaxBy(z => z.Date);

            if (invoice == null)
            {
                invoice = CreateInvoiceObject(payment);
                dbLayer.InvoiceRepository.AddInvoice(invoice);
            }
            else
            {
                invoice.Date = payment.Date;
                invoice.State = InvoiceStatus.Processed.ToString();
                invoice.PaymentID = payment.Id;
                dbLayer.InvoiceRepository.Update(invoice);
            }

            dbLayer.Save();
            return invoice;
        }

        /// <summary>
        /// Создать объект счета на оплату
        /// </summary>
        /// <param name="payment">Платеж</param>
        /// <returns>Счет на оплату</returns>
        private Domain.DataModels.billing.Invoice CreateInvoiceObject(Domain.DataModels.billing.Payment payment)
            => new()
            {
                Id = Guid.NewGuid(),
                AccountId = payment.AccountId,
                Date = payment.Date,
                Requisite = accountRequisitesDataProvider.GetIfExistsOrCreateNew(payment.AccountId).Inn,
                Sum = payment.Sum,
                State = InvoiceStatus.Processed.ToString(),
                PaymentID = payment.Id,
                
            };
    }
}
