﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Configurations;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер проверки возможности создать платеж
    /// </summary>
    internal class AbilityToCreatePaymentProvider(
        IBillingPaymentsProvider billingPaymentsProvider,
        IBillingPaymentsDataProvider billingPaymentsDataProvider,
        IPromisePaymentProvider promisePaymentProvider,
        IUnitOfWork dbLayer,
        ICloudLocalizer cloudLocalizer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IBillingDataProvider billingDataProvider)
        : IAbilityToCreatePaymentProvider
    {
        /// <summary>
        /// Проверить возможность создать платеж на указанную сумму и взять ОП если необходимо
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <param name="amount">Сумма</param>
        /// <param name="isPromisePayment">Признак что это ОП</param>
        /// <returns>Результат проверки</returns>
        public CheckAbilityToPayForServiceTypeResultDto CheckAbilityAndGetPromisePaymentIfNeeded(Guid accountId,
            Guid billingServiceId, decimal amount,
            bool isPromisePayment)
        {
            var service =
                dbLayer.BillingServiceRepository.FirstOrThrowException(billingService =>
                    billingService.Id == billingServiceId);

            if (amount <= 0)
                return new CheckAbilityToPayForServiceTypeResultDto { IsComplete = true };

            var operationAttemptResult = billingPaymentsProvider
                .CanCreatePayment(CreatePaymentDefinition(accountId, billingServiceId, amount),
                    out var notEnoughMoney);

            if (operationAttemptResult != PaymentOperationResult.Ok && !isPromisePayment)
                return new CheckAbilityToPayForServiceTypeResultDto
                {
                    IsComplete = false,
                    Currency = accountConfigurationDataProvider.GetAccountLocale(accountId).Currency,
                    NotEnoughMoney = notEnoughMoney,
                    CanUsePromisePayment = CanUsePromisePayment(accountId)
                };

            if (!isPromisePayment || notEnoughMoney <= 0)
                return new CheckAbilityToPayForServiceTypeResultDto { IsComplete = true };

            var promisePaymentResult = promisePaymentProvider
                .CreatePromisePayment(accountId,
                    new CalculationOfInvoiceRequestModel
                    { SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = notEnoughMoney } }, false);

            if (!promisePaymentResult.Complete)
                throw new InvalidOperationException(
                    $"Не удалось создать ОП для покупки сервиса {service.GetNameBasedOnLocalization(cloudLocalizer, accountId)} на сумму {notEnoughMoney}");

            return new CheckAbilityToPayForServiceTypeResultDto { IsComplete = true };
        }

        /// <summary>
        /// Создать описание платежа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <param name="amount">Сумма</param>
        /// <returns>Описание платежа</returns>
        private PaymentDefinitionDto CreatePaymentDefinition(Guid accountId, Guid billingServiceId, decimal amount)
            => new()
            {
                Account = accountId,
                Date = DateTime.Now,
                Id = Guid.NewGuid(),
                BillingServiceId = billingServiceId,
                Status = PaymentStatus.Done,
                Total = amount,
                OperationType = PaymentType.Outflow,
                OriginDetails = "core-cp"
            };

        /// <summary>
        /// Проверить, доступен ли ОП для данного аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Результат проверки</returns>
        public bool CanUsePromisePayment(Guid accountId)
        {
            var type = PaymentType.Inflow.ToString();
            var promisePaymentCost = billingPaymentsDataProvider.GetPromisePaymentCost(accountId);
            var inflowPayments = dbLayer.PaymentRepository.Any(p =>
                p.AccountId == accountId && p.OperationType == type && p.Sum > 0 &&
                p.Status == PaymentStatus.Done.ToString());
            return inflowPayments && (!promisePaymentCost.HasValue || promisePaymentCost.Value <= 0);
        }

        /// <summary>
        /// Проверить, доступно ли увеличение ОП для данного аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Результат проверки</returns>
        public bool CanIncreasePromisePayment(Guid accountId)
        {
            var paymentList = billingPaymentsDataProvider.GetPaymentsList(accountId);
            if (!paymentList.Any(x =>
                x.OperationType == PaymentType.Inflow && x is { Total: > 0, Status: PaymentStatus.Done }))
                return false;

            var promiseDate = billingPaymentsDataProvider.GetPromiseDateTime(accountId);
            var promisePaymentDays = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");
            var regularPayment = billingDataProvider.GetAccountRegularPayment(accountId);
            var currentPromisePaymentSum =billingPaymentsDataProvider.GetPromisePaymentCost(accountId) ?? decimal.Zero;
            return currentPromisePaymentSum > 0 && (currentPromisePaymentSum <= (regularPayment * 2) || DateTime.Now <= promiseDate?.AddDays(promisePaymentDays));
        }
    }
}
