﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Payments.Internal;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.Billing.Invoice.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для работы с обещанным платежом
    /// </summary>
    public class PromisePaymentProvider(
        IUnitOfWork dbLayer,
        CalculationOfInvoiceProvider calculationOfInvoiceProvider,
        IInvoiceProvider invoiceProvider,
        IInvoiceDocumentBuilder invoiceBuilderFactory,
        IInvoiceNotificationHelper invoiceNotificationHelper,
        IInflowPaymentProcessor inflowPaymentProcessor,
        IBillingPaymentsProvider billingPaymentsProvider,
        IAccessProvider accessProvider,
        IInvoiceDataProvider invoiceDataProvider,
        IConfiguration configuration,
        IHandlerException handlerException)
        : IPromisePaymentProvider
    {
        /// <summary>
        /// Начисление обещанного платежа
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        /// <param name="promiseSum">Сумма обещанного платежа.</param>
        /// <param name="prolongServices">Пролонгировать сервисы.</param>
        /// <param name="date">Дата и время создания ОП.</param>
        /// <returns>Признок успешности создания ОП.</returns>
        public bool CreatePromisePayment(Guid accountId, decimal promiseSum, bool prolongServices, out DateTime date)
        {
            var billingAccount = dbLayer.BillingAccountRepository.FirstOrDefault(x => x.Id == accountId);
            var promisePaymentSum = billingAccount.PromisePaymentSum;
            date = DateTime.Now;

            if (promisePaymentSum is > decimal.Zero)
            {
                return false;
            }

            var result = billingPaymentsProvider.CreatePayment(new PaymentDefinitionDto
            {
                System = PaymentSystem.ControlPanel,
                Account = accountId,
                Date = date,
                Description = "Активация обещанного платежа",
                Status = PaymentStatus.Done,
                Total = promiseSum,
                OperationType = PaymentType.Inflow,
                OriginDetails = "PromisePayment"
            });

            if (result != PaymentOperationResult.Ok)
            {
                return false;
            }

            billingPaymentsProvider.SetPromisePaymentCredentials(accountId, date, promiseSum);

            if (prolongServices)
                inflowPaymentProcessor.Process(accountId, false);

            return true;

        }

        /// <summary>
        /// Создать обещанный платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель запроса на калькуляцию счета</param>
        /// <param name="prolongServices">Выполнить пролонгацию сервисов</param>
        /// <returns>Результат создания обещанного платежа</returns>
        public CreatePromisePaymentResult CreatePromisePayment(Guid accountId,
            CalculationOfInvoiceRequestModel model, bool prolongServices = true)
        {
            var totalSum = GetTotalSumForCalculationOfInvoiceRequestModel(accountId, model);

            DateTime date;
            Domain.DataModels.billing.Invoice invoice;
            var locale = dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(configuration["DefaultLocale"] ?? ""));
            try
            {
                var createPromisePaymentResult =
                    CreatePromisePayment(accountId, totalSum, prolongServices, out date);

                if (!createPromisePaymentResult)
                {
                    var message = $"Ошибка взятия обещанного платежа на сумму {totalSum:0} {locale.Currency}.";
                    LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.TakePromisePayment,
                        message);
                    return new CreatePromisePaymentResult
                    { Complete = false, Locale = locale, PromisePaymentSum = totalSum };
                }

                invoice = invoiceProvider.CreateInvoiceBasedOnCalculationInvoiceModel(accountId, model);
                dbLayer.Save();
                LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.TakePromisePayment,
                    $"Активирован обещанный платеж на сумму {totalSum:0} {locale.Currency}.");
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка выдачи обещанного платежа]");
                return new CreatePromisePaymentResult
                { Complete = false, Locale = locale, PromisePaymentSum = totalSum };
            }

            CreatePromisePaymentNotify(invoice, totalSum, date, locale.Name);
            return new CreatePromisePaymentResult { Complete = true, Locale = locale, PromisePaymentSum = totalSum };
        }

        /// <summary>
        /// Получить итоговую стоимость
        /// для модели запроса на калькуляцию счета
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель запроса на калькуляцию счета</param>
        /// <returns>Итоговая стоимость</returns>
        private decimal GetTotalSumForCalculationOfInvoiceRequestModel(Guid accountId,
            CalculationOfInvoiceRequestModel model)
        {
            if (model.SuggestedPayment is { PaymentSum: > 0 })
                return model.SuggestedPayment.PaymentSum;

            var totalSum = calculationOfInvoiceProvider.GetCalculationsOfInvoiceResult(accountId, model).TotalSum;

            return totalSum;
        }

        /// <summary>
        /// Создать уведомление обещанного платежа
        /// </summary>
        /// <param name="invoice">Счет</param>
        /// <param name="totalSum">Итоговая сумма</param>
        /// <param name="date">Дата начисления обещанного платежа</param>
        /// <param name="localeName">Название локали</param>
        private void CreatePromisePaymentNotify(Domain.DataModels.billing.Invoice invoice, decimal totalSum, DateTime date, string localeName)
        {
            var productsInfo = InvoiceAdditionalDataHelper.GetNomenclatureWithProductsDescription(invoice, localeName);
            var invoiceDc = invoiceDataProvider.GetInvoiceData(invoice);
            var invoiceDocument = invoiceBuilderFactory.Build(invoiceDc);
            invoiceNotificationHelper.CreatePromisePaymentNotify(invoice,
                invoice.AccountId, totalSum, date, productsInfo, invoiceDocument.Bytes);
        }
    }
}
