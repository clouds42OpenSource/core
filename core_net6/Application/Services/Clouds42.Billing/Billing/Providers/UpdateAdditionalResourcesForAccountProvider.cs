﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер для обновления дополнительных ресурсов аккаунта
    /// </summary>
    internal class UpdateAdditionalResourcesForAccountProvider(IUnitOfWork dbLayer)
        : IUpdateAdditionalResourcesForAccountProvider
    {
        /// <summary>
        /// Обновить дополнительные ресурсы для аккаунта
        /// </summary>
        /// <param name="model">Модель для обновления дополнительных ресурсов аккаунта</param>
        public void Update(UpdateAdditionalResourcesForAccountDto model)
        {
            var billingAccount = dbLayer.BillingAccountRepository.GetBillingAccount(model.AccountId) ??
                                 throw new NotFoundException(
                                     $"Не удалось получить аккаунт биллинга по Id '{model.AccountId}'");

            billingAccount.AdditionalResourceName = model.AdditionalResourceName;
            billingAccount.AdditionalResourceCost = model.AdditionalResourceCost;

            dbLayer.BillingAccountRepository.Update(billingAccount);
            dbLayer.Save();
        }
    }
}
