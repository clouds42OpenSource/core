﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер пролонгации услуг.
    /// </summary>
    public class BillingServicesProvider(
        ILogger42 logger,
        IProlongActiveOrLockServiceProcessor prolongActiveOrLockServiceProcessor,
        IDemoPeriodIsComingToEndNotifacotionProcessor comingToEndNotificationProcessor,
        IPromisedPaymentProcessor paymentProcessor,
        BeforeLockServiceProvider beforeLockServiceProvider,
        BeforeLockServiceAutoPayProvider beforeLockServiceAutoPayProvider,
        IUnlockSponsoredServicesProcessor sponsoredServicesProcessor,
        IServiceExtensionDatabaseActivationStatusProvider serviceExtensionDatabaseActivationStatusProvider,
        ICloudServiceLockUnlockProvider cloudServiceLockUnlockProvider,
        IApplyDemoServiceSubscribeProcessor applyDemoServiceSubscribeProcessor,
        IDemoPeriodEndedPaymentRequiredNotifacotionProcessor demoPeriodEndedPaymentRequiredNotificationProcessor,
        ILockDemoServiceNotifacotionProcessor lockDemoServiceNotificationProcessor,
        IUnitOfWork unitOfWork,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IBillingPaymentsProvider billingPaymentsProvider,
        IInvoiceProvider invoiceProvider)
        : IProlongServicesProvider
    {
        /// <summary>
        /// Обработать просроченные обещанные платежи.
        /// </summary>
        public void ProcessExpiredPromisePayments()
        {

            var daysForLock = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");

            var accountsWithExpiredPromise =
                unitOfWork.BillingAccountRepository.GetAccountsWithExpiredPromise(daysForLock);
            
            foreach (var prAccount in accountsWithExpiredPromise)
            {
                logger.Trace($"Обрабатываем просроченный ОП:: accountId '{prAccount.Id}', balanseSum '{prAccount.Balance}' promiseSum '{prAccount.PromisePaymentSum}'");

                paymentProcessor.ProcessRegularOperation(prAccount);
            }
        }

        /// <summary>
        /// Продлить или заблокировать сервисы.
        /// </summary>
        public void ProlongOrLockExpiredServices()
        {
            logger.Trace("Начата обработка по продлению или блокировки сервисов.");

            var ids = unitOfWork.ResourceConfigurationRepository
                .GetActiveMainServicesForProlongOrLock().Select(rc => rc.Id).ToList();
            var errorMessageBuilder = new StringBuilder();

            foreach (var rcId in ids)
            {
                try
                {
                    var resourceConfiguration = GetResourcesConfiguration(rcId);
                    prolongActiveOrLockServiceProcessor.Process(resourceConfiguration);
                }
                catch (Exception ex)
                {
                    errorMessageBuilder.Append(
                        $" - ошибка продления/блокировки сервиса по ресурс конфигурации {rcId}. Причина: {ex.GetFullInfo(false)}");
                }
            }
            ProcessErrorMessageBuilderMessage(errorMessageBuilder);
        }

        /// <summary>
        /// Уведомить клиентов о скорой блокировке сервисов.
        /// </summary>
        public void NotifyBeforeLockServices()
        {
            var resourceConfigurations = unitOfWork.ResourceConfigurationRepository.GetSoonExpiredMainServices(beforeLockDays: 5);

            var notificationErrorMessageBuilder = new StringBuilder();
          
            foreach (var resourcesConfiguration in resourceConfigurations)
            {
                var autoPayMethod = unitOfWork.AccountAutoPayConfigurationRepository.GetByAccountId(resourcesConfiguration.AccountId);

                if (autoPayMethod != null)
                    continue;

                beforeLockServiceProvider.TryNotifyBeforeLockService(resourcesConfiguration, notificationErrorMessageBuilder);
            }

            if (!string.IsNullOrEmpty(notificationErrorMessageBuilder.ToString()))
                throw new InvalidOperationException(notificationErrorMessageBuilder.ToString());
        }
        
        /// <summary>
        /// Асинхронно создает счета-фактуры для услуг с датой окончания действия, не превышающей 5 дней.
        /// </summary>
        /// <returns>Задача, представляющая асинхронную операцию.</returns>
        public void IssueInvoicesBeforeRentalEnd()
        {
            try
            {
                var resourceConfigurations = unitOfWork
                    .ResourceConfigurationRepository
                    .GetSoonExpiredMainServices(beforeLockDays: 5);
                IssueInvoices(resourceConfigurations);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка с выписыванием счета за 5 дней до окончания аренды");
                throw;
            }
        }
        
        /// <summary>
        /// Создает счета-фактуры для заданного списка конфигураций ресурсов, если возможно создать платеж.
        /// </summary>
        /// <param name="services">Список конфигураций ресурсов для которых необходимо создать счета.</param>
        private void IssueInvoices(List<ResourcesConfiguration> services)
        {
            foreach (var service in services)
            {
                try
                {
                    var resConfModel = resourceConfigurationDataProvider.GetConfigWithDependencies(service);
                    var totalServiceCost = resourceConfigurationDataProvider.GetTotalServiceCost(resConfModel);
                    var canCreatePayment =
                        billingPaymentsProvider.CanCreatePayment(resConfModel.ResourcesConfiguration.AccountId,
                            totalServiceCost, out var needMoney);

                    if (needMoney > 0 && canCreatePayment != PaymentOperationResult.Ok)
                    {
                        invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(service, service.BillingAccounts);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"Ошибка при выписывании счета для ресурса {service.Id}");
                }
            }
        }

        /// <summary>
        /// Уведомить клиентов о скором автосписании для продления сервисов.
        /// </summary>
        public void NotifyBeforeLockServicesAutoPay()
        {
            var resourceConfigurations = unitOfWork.ResourceConfigurationRepository.GetSoonExpiredMainServices(beforeLockDays: 2);

            var notificationErrorMessageBuilder = new StringBuilder();
            
            foreach (var resourcesConfiguration in resourceConfigurations)
                    beforeLockServiceAutoPayProvider.TryNotifyBeforeLockAutoPayService(resourcesConfiguration, notificationErrorMessageBuilder);

            if (!string.IsNullOrEmpty(notificationErrorMessageBuilder.ToString()))
                throw new InvalidOperationException(notificationErrorMessageBuilder.ToString());
        }

        /// <summary>
        /// Разблокировать платные заблокированные сервисы которые заблокированны, на которые есть деньги что бы разблокировать и у аккаунтов которых нет просроченного обещанного платежа.
        /// </summary>
        public void ProlongPaidLockedServices()
        {
            var promisePaymentDays = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");

            logger.Trace("Начата обработка по разблокированию заблоченных сервисов.");

            var resourceConfigurationsIds = unitOfWork.ResourceConfigurationRepository
                    .GetPaidLockedServicesWithAvailableBalanceOnAccount(promisePaymentDays);

            var errorMessageBuilder = new StringBuilder();

            foreach (var rcId in resourceConfigurationsIds)
            {
                try
                {
                    var rc = GetResourcesConfiguration(rcId);
                    prolongActiveOrLockServiceProcessor.Process(rc);
                }

                catch (Exception ex)
                {
                    errorMessageBuilder.Append($" - ошибка разблокировки сервиса по ресурс конфигурации {rcId}. Причина: {ex.GetFullInfo(false)}");
                }
            }

            ProcessErrorMessageBuilderMessage(errorMessageBuilder);
        }

        /// <summary>
        /// Продление заблокированных сервисов у спонсируемых аккаунтов.
        /// </summary>
        public void UnlockSponsoredServices()
        {
            var promisePaymentDays = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");

            var resultConfigurations =
                unitOfWork.ResourceConfigurationRepository.GetLockedSponsoredServiceConfigurations(promisePaymentDays);

            foreach (var resourceConfiguration in resultConfigurations)
            {
                sponsoredServicesProcessor.Process(resourceConfiguration);
            }
        }

        /// <summary>
        /// Заблокировать сервисы с просроченным демо периодом
        /// </summary>
        public void LockExpiredDemoServices()
        {
            logger.Info("Начата обработка по блокированию просроченных демо сервисов.");
                var resourceConfigurations = unitOfWork.ResourceConfigurationRepository.GetAllExpiredDemoServices().ToList();

           if (!resourceConfigurations.Any())
           {
               logger.Info("Обработка по блокированию просроченных демо сервисов завершена.");
               return;
           }

           foreach (var resourceConfiguration in resourceConfigurations)
           {
               try
               {
                   if (resourceConfiguration.IsAutoSubscriptionEnabled == true)
                   {
                       TryMakePaymentForDemoExpiredServiceOrLockWithNotification(unitOfWork, resourceConfiguration);
                   }
                   else
                   {
                       LockDemoExpiredServiceWithNotification(resourceConfiguration);
                   }
               }
               catch(Exception ex)
               {
                   logger.Error($"Во время обработки по блокированию просроченных демо сервисов произошла ошибка {ex.Message}.");
                   break;
               }
               
           }
        }

        /// <summary>
        /// Продлить или заблокировать гибридные сервисы.
        /// </summary>
        public void ProlongOrLockHybridExpiredServices()
        {
            logger.Info("Начата обработка гибридных сервисов сервисов.");

            var resourceConfigurations = unitOfWork.ResourceConfigurationRepository.GetAllExpiredHybridServices().ToList();

            if (!resourceConfigurations.Any())
            {
                logger.Info("Обработка по блокированию просроченных гибридных сервисов завершена.");
                return;
            }

            foreach (var resourceConfiguration in resourceConfigurations)
            {
                try
                {
                    var resConfigRent1c = unitOfWork.ResourceConfigurationRepository.FirstOrDefault(config =>
                        config.AccountId == resourceConfiguration.AccountId && config.BillingService.SystemService == Clouds42Service.MyEnterprise);

                    if (resConfigRent1c == null)
                    {
                        prolongActiveOrLockServiceProcessor.Process(resourceConfiguration);
                    }
                    
                }
                catch (Exception ex)
                {
                    logger.Error(ex,
                        $" - ошибка разблокировки сервиса по ресурс конфигурации {resourceConfiguration.Id}. Причина: {ex.Message}");
                }
            }


        }

        /// <summary>
        /// Заблокировать сервис и уведомить клиента о том,что демо период завершён, нужна оплата
        /// </summary>
        /// <param name="dbLayer">DB контекст</param>
        /// <param name="resourceConfiguration">Ресурс, который требуется заблокировать</param>
        private void LockDemoExpiredServiceWithNotification(ResourcesConfiguration resourceConfiguration)
        {
            LockExpiredDemoService(resourceConfiguration);
            lockDemoServiceNotificationProcessor.Process(resourceConfiguration);
        }


        /// <summary>
        /// Попытаться оплатить за сервис у которого закончился демо или заблокировать сервис и уведомить клиента о том,что демо период завершён, нужна оплата
        /// </summary>
        /// <param name="dbLayer">DB контекст</param>
        /// <param name="resourceConfiguration">Ресурс, который требуется оплатить млм заблокировать</param>
        public void TryMakePaymentForDemoExpiredServiceOrLockWithNotification(IUnitOfWork dbLayer, ResourcesConfiguration resourceConfiguration)
        {
            logger.Trace("Начата обработка по блокированию просроченных демо сервисов, у которых есть авто-подписка и нет денег на счету.");

                var notificationErrorMessageBuilder = new StringBuilder();
            
                var paymentResult = applyDemoServiceSubscribeProcessor.Apply(resourceConfiguration.BillingServiceId, resourceConfiguration.AccountId, false);

                if (paymentResult.Complete)
                {
                    return;
                }

                LockExpiredDemoService(resourceConfiguration);
            
                demoPeriodEndedPaymentRequiredNotificationProcessor.Process(resourceConfiguration, notificationErrorMessageBuilder);


                if (notificationErrorMessageBuilder.Length > 0)
                {
                    logger.Warn(notificationErrorMessageBuilder.ToString());
                }

        }

        /// <summary>
        /// Получить ресурс конфигурацию сервиса
        /// </summary>
        /// <param name="rcId">ID ресур конфигурации</param>
        /// <returns>Ресурс конфигурация сервиса</returns>
        private ResourcesConfiguration GetResourcesConfiguration(Guid rcId)
        {
            var resourceConfiguration =
                unitOfWork.ResourceConfigurationRepository.FirstOrDefault(rc => rc.Id == rcId) ??
                throw new NotFoundException($"По Id '{rcId}' не найдена запись ResourceConfiguration");
            return resourceConfiguration;
        }

        /// <summary>
        /// Обработать сообщение об ошибке сборщика сообщений
        /// </summary>
        /// <param name="errorMessageBuilder">Сборщик сообщений об ошибках</param>
        private void ProcessErrorMessageBuilderMessage(StringBuilder errorMessageBuilder)
        {
            var errorMessage = errorMessageBuilder.ToString();
            logger.Warn(errorMessage);
            if (!string.IsNullOrEmpty(errorMessage))
                throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Уведомить клиентов о том, что заканчивается демо период.
        /// </summary>
        public void NotifyServiceDemoPeriodIsComingToEnd()
        {
            var resourceConfigurations = unitOfWork.ResourceConfigurationRepository
                .GetAllDemoResourcesConfigurationsWithNumberOfRemainingDays(IDemoPeriodIsComingToEndNotifacotionProcessor.RemainingNumberOfDays);

            var foundResources = string.Join(";", resourceConfigurations.OrderBy(item=>item.AccountId).Select(item => $"{item.AccountId}.acc=>{item.BillingServiceId}.svc"));

            logger.Trace($"Начата обработка по уведомлению клиентов ({foundResources}), о том, что заканчивается демо период.");

            var notificationErrorMessageBuilder = new StringBuilder();
            
            resourceConfigurations.ForEach(rc => comingToEndNotificationProcessor.Process(rc, notificationErrorMessageBuilder));

            if(notificationErrorMessageBuilder.Length > 0)
            {
                logger.Warn(notificationErrorMessageBuilder.ToString());
            }
        }


        /// <summary>
        /// заблокировать сервис, у которого закончен демо и нет оплаты 
        /// </summary>
        /// <param name="rc">Конфигурация сервиса которую заблокировать</param>
        private void LockExpiredDemoService(ResourcesConfiguration rc)
        {
            cloudServiceLockUnlockProvider.Lock(rc);
            serviceExtensionDatabaseActivationStatusProvider.SetActivationStatus(rc.AccountId, false, rc.BillingServiceId, true);
        }
    }
}
