﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Providers
{
    /// <summary>
    /// Провайдер калькуляции счета
    /// </summary>
    public class CalculationOfInvoiceProvider(
        IUnitOfWork dbLayer,
        IBillingServiceInfoProvider billingServiceInfoProvider,
        ICloud42ServiceHelper cloud42ServiceHelper,
        IAdditionalResourcesDataProvider additionalResourcesDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        /// Получить результат калькуляции счета
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="requestModel">Модель запроса на калькуляцию счета</param>
        /// <returns>Модель результата калькуляции счета</returns>
        public CalculationOfInvoiceResultModel GetCalculationsOfInvoiceResult(Guid accountId,
            CalculationOfInvoiceRequestModel requestModel)
        {
            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);

            var result = new CalculationOfInvoiceResultModel();
            var period = Math.Max(1, (int) requestModel.PayPeriod);

            result.AdditionalServiceCost =
                GetAdditionalResourceCost(accountId, period, requestModel.AdditionalResourcesDisabled);
            result.MyDiskCost =
                GetMyDiskCost(account, requestModel.MyDiskSize ?? 0, period, requestModel.MyDiskDisabled);
            CalculateCostForBaseBillingServiceTypes(account, requestModel.CalculationBaseBillingServiceTypes, period);
            result.CalculationBaseBillingServiceTypes = requestModel.CalculationBaseBillingServiceTypes;
            CalculateCostForEsdlService(requestModel, result);

            return result;
        }

        /// <summary>
        /// Получить стоимость сервиса
        /// Мой диск для аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="diskSize">Размер диска</param>
        /// <param name="period">Период оплаты</param>
        /// <param name="myDiskDisabled">Мой диск заблокирован</param>
        /// <returns>Стоимость сервиса Мой диск</returns>
        private decimal GetMyDiskCost(Account account, int diskSize, int period, bool myDiskDisabled)
        {
            if (!accountConfigurationDataProvider.IsVipAccount(account.Id))
                return period * cloud42ServiceHelper.CalculateActualCostOfMyDiskService(diskSize, account);

            if (diskSize <= 0 || myDiskDisabled)
                return 0;

            var myDiskResConfig =
                account.BillingAccount.ResourcesConfigurations.FirstOrDefault(r =>
                    r.BillingService.SystemService == Clouds42Service.MyDisk);
            return period * myDiskResConfig?.Cost ?? 0;
        }

        /// <summary>
        /// Посчитать стоимость для основных услуг биллинга
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="calculationBaseBillingServiceTypes">Модели калькуляции основных услуг биллинга</param>
        /// <param name="period">Период оплаты</param>
        private void CalculateCostForBaseBillingServiceTypes(Account account,
            List<CalculationBaseBillingServiceTypeModel> calculationBaseBillingServiceTypes, int period)
        {
            foreach (var calculationBaseBillingServiceType in calculationBaseBillingServiceTypes)
            {
                if (calculationBaseBillingServiceType.CountLicenses < 0)
                {
                    calculationBaseBillingServiceType.CountLicenses = 0;
                    calculationBaseBillingServiceType.Amount = 0;
                    continue;
                }

                calculationBaseBillingServiceType.Amount =
                    billingServiceInfoProvider.GetServiceTypeAndServiceTypeIncludesCost(
                        calculationBaseBillingServiceType.Id,
                        account) * calculationBaseBillingServiceType.CountLicenses * period;
            }
        }

        /// <summary>
        /// Посчитать стоимость
        /// для сервиса Загрузка документов
        /// </summary>
        /// <param name="requestModel">Модель запроса на калькуляцию счета</param>
        /// <param name="resultModel">Модель результата калькуляции счета</param>
        private static void CalculateCostForEsdlService(CalculationOfInvoiceRequestModel requestModel,
            CalculationOfInvoiceResultModel resultModel)
        {
            if (requestModel.EsdlLicenseYears == null || requestModel.EsdlPagesCount == null)
                return;

            resultModel.EsdlLicenseCost = ConfigurationHelper.GetConfigurationValue<decimal>("ESDLLicenseCost") *
                                          requestModel.EsdlLicenseYears.Value;

            var packageCost = ConfigurationHelper.GetConfigurationValue<decimal>($"PagePackage{requestModel.EsdlPagesCount}");

            resultModel.EsdlPagesCost = packageCost;
        }

        /// <summary>
        /// Получить дополнительную стоимость ресурсов для аккаунта на период.
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="period">Период</param>
        /// <param name="additionalResourcesDisabled">Дополнительные ресурсы отключены</param>
        /// <returns>Дополнительная стоимость</returns>
        private decimal GetAdditionalResourceCost(Guid accountId, int period, bool additionalResourcesDisabled)
        {
            var additionalResourcesData = additionalResourcesDataProvider.GetAdditionalResourcesData(accountId);

            if (additionalResourcesData is { IsVipAccount: true, AdditionalResourceCost: not null } 
                && !additionalResourcesDisabled)
                return additionalResourcesData.AdditionalResourceCost.Value * period;

            return decimal.Zero;
        }
    }
}
