﻿using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Models.PaymentSystem.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;

namespace Clouds42.Billing.Billing.Models
{
    /// <summary>
    ///     Ответ Paybox-у в виде Xml, о том что мы обработали их запрос
    /// </summary>
    [XmlRoot("response")]
    [DataContract(Name = "response")]
    public class PayboxResultResponse : ISignatureGenerator
    {
        /// <summary>
        /// Статус
        /// </summary>
        [XmlElement(ElementName = nameof(pg_status))]
        [DataMember(Name = nameof(pg_status))]
        public string pg_status { get; set; }

        /// <summary>
        /// Описание в случае ошибки
        /// </summary>
        [XmlElement(ElementName = nameof(pg_description))]
        [DataMember(Name = nameof(pg_description))]
        public string pg_description { get; set; }

        /// <summary>
        /// Описание ошибки если статус = error
        /// </summary>
        [XmlElement(ElementName = nameof(pg_error_description))]
        [DataMember(Name = nameof(pg_error_description))]
        public string pg_error_description { get; set; }

        /// <summary>
        /// Случайная строка
        /// </summary>
        [XmlElement(ElementName = nameof(pg_salt))]
        [DataMember(Name = nameof(pg_salt))]
        public string pg_salt { get; set; }

        /// <summary>
        /// Подпись
        /// </summary>
        [XmlElement(ElementName = nameof(pg_sig))]
        [DataMember(Name = nameof(pg_sig))]
        public string pg_sig { get; set; }

        /// <summary>
        ///     Генерируем подпсись для дальнейшей проверки
        /// </summary>
        /// <returns></returns>
        public string GenerateSignature(Guid supplierId)
        {
            var url = CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetResultUrlScript(supplierId);

            var cryptoProvider = new PayboxCryptoProvider();

            if (string.IsNullOrWhiteSpace(url))
                throw new NotFoundException("Ошибка :: Параметр не был указан или указано пустое поле " +
                                    ":: Параметр URL обязателен для составления подписи!!");

            var allProperties = new StringBuilder($"{url};");

            if (!string.IsNullOrWhiteSpace(pg_description))
                allProperties.Append($"{pg_description};");

            if (!string.IsNullOrWhiteSpace(pg_error_description))
                allProperties.Append($"{pg_error_description};");

            if (!string.IsNullOrWhiteSpace(pg_salt))
                allProperties.Append($"{pg_salt};");

            if (!string.IsNullOrWhiteSpace(pg_status))
                allProperties.Append($"{pg_status};");

            var allData = PayboxCryptoProvider.AppendSecretKey(allProperties.ToString(), supplierId);

            return cryptoProvider.GenerateMd5Hash(allData);
        }
    }
}
