﻿using System.Text;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Models.PaymentSystem.Interfaces;
using Clouds42.Configurations.Configurations;

namespace Clouds42.Billing.Billing.Models
{
    /// <summary>
    ///     Модель параметров при редиректе с онлайн оплаты Paybox-а
    /// </summary>
    public class PayboxImportSuccessOrFailureModel : ISignatureGenerator
    {
        /// <summary>
        /// Код авторизации. Этот параметр передается только
        /// в случае успешной оплаты банковской картой.
        /// </summary>
        public string pg_auth_code { get; set; }

        /// <summary>
        /// 0 или 1. Передается только в случае успешной
        /// оплаты банковской картой и показывает, был ли
        /// произведен клиринг в момент авторизации
        /// </summary>
        public string pg_captured { get; set; }

        /// <summary>
        /// Бренд карты: CA – MasterCard и их продукты, VI –
        /// Visa, AX – AmericanExpress.Этот параметр
        /// передается только в случае успешной оплаты
        /// банковской картой.
        /// </summary>
        public string pg_card_brand { get; set; }

        /// <summary>
        /// Маскированный номер карты (часть цифр номера
        /// карты скрыты). Этот параметр передается только в
        /// случае успешной оплаты банковской картой.
        /// </summary>
        public string pg_card_pan { get; set; }

        /// <summary>
        /// Код ошибки
        /// </summary>
        public string pg_failure_code { get; set; }

        /// <summary>
        /// Описание ошибки
        /// </summary>
        public string pg_failure_description { get; set; }

        /// <summary>
        /// Идентификатор платежа в Core
        /// </summary>
        public int pg_order_id { get; set; }

        /// <summary>
        /// Сумма переплаты в валюте платежной системы.
        /// Параметр передается только в случае когда клиент
        /// оплатил больше, чем от него ожидалось, и
        /// переплата разрешена в настройках магазина.
        /// </summary>
        public decimal? pg_overpayment { get; set; }

        /// <summary>
        /// Внутренний идентификатор платежа в системе Paybox
        /// </summary>
        public int pg_payment_id { get; set; }

        /// <summary>
        /// Дата, до которой рекуррентный профиль доступен к использованию
        /// </summary>
        public string pg_recurring_profile_expiry_date { get; set; }

        /// <summary>
        /// Идентификатор профиля рекуррентных платежей
        /// </summary>
        public int? pg_recurring_profile_id { get; set; }

        /// <summary>
        /// Случайная строка
        /// </summary>
        public string pg_salt { get; set; }

        /// <summary>
        /// Подпись
        /// </summary>
        public string pg_sig { get; set; }

        /// <summary>
        ///     Генерация подписи для проверки при редиректе
        /// </summary>
        /// <returns>Подпись в виде MD5</returns>
        public string GenerateSignature(Guid supplierId)
        {
            var url = CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetSuccessAndFailScript(supplierId);

            var cryptoProvider = new PayboxCryptoProvider();

            if (string.IsNullOrWhiteSpace(url))
                throw new InvalidOperationException("Ошибка :: Параметр не был указан или указано пустое поле " +
                                    ":: Параметр URL обязателен для составления подписи!!");

            var allProperties = new StringBuilder($"{url};");

            if (!string.IsNullOrWhiteSpace(pg_auth_code))
                allProperties.Append($"{pg_auth_code};");

            if (!string.IsNullOrWhiteSpace(pg_captured))
                allProperties.Append($"{pg_captured};");

            if (!string.IsNullOrWhiteSpace(pg_card_brand))
                allProperties.Append($"{pg_card_brand};");

            if (!string.IsNullOrWhiteSpace(pg_card_pan))
                allProperties.Append($"{pg_card_pan};");

            if (!string.IsNullOrWhiteSpace(pg_failure_code))
                allProperties.Append($"{pg_failure_code};");

            if (!string.IsNullOrWhiteSpace(pg_failure_description))
                allProperties.Append($"{pg_failure_description};");

            allProperties.Append($"{pg_order_id};");

            if (pg_overpayment != null)
                allProperties.Append($"{pg_overpayment};");

            allProperties.Append($"{pg_payment_id};");

            if (!string.IsNullOrWhiteSpace(pg_recurring_profile_expiry_date))
                allProperties.Append($"{pg_recurring_profile_expiry_date};");

            if (pg_recurring_profile_id != null)
                allProperties.Append($"{pg_recurring_profile_id};");

            if (!string.IsNullOrWhiteSpace(pg_salt))
                allProperties.Append($"{pg_salt};");

            var allData = PayboxCryptoProvider.AppendSecretKey(allProperties.ToString(), supplierId);

            return cryptoProvider.GenerateMd5Hash(allData);
        }
    }
}
