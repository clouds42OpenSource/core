﻿using System.Text;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Models.PaymentSystem.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;

namespace Clouds42.Billing.Billing.Models
{
    /// <summary>
    ///     Модель для приема параметров
    /// </summary>
    public class PayboxImportResultModel : ISignatureGenerator
    {
        public IEnumerable<KeyValuePair<string, string>> _sourceDataCollection =
            [];

        /// <summary>
        /// Обновляет SourceDataCollection для запроса
        /// </summary>
        /// <param name="sourceDataCollection">Новая SourceDataCollection</param>
        public void SetSourceDataCollection(IEnumerable<KeyValuePair<string, string>> sourceDataCollection)
        {
            _sourceDataCollection = sourceDataCollection ?? [];
        }

        /// <summary>
        /// Сумма выставленного счета
        /// </summary>
        public decimal pg_amount { get; set; }

        /// <summary>
        /// Код авторизации. Передается только в случае успешной оплаты банковской картой
        /// </summary>
        public string pg_auth_code { get; set; }

        /// <summary>
        /// Можно ли откатить платеж назад. По умолчанию = 0
        /// </summary>
        public int? pg_can_reject { get; set; }

        /// <summary>
        /// Клиринг банковской карты. Передается только в случае успешной оплаты банковской картой
        /// </summary>
        public int? pg_captured { get; set; }

        /// <summary>
        /// Бренд карты: CA – MasterCard и их продукты, VI – Visa, AX – AmericanExpress.
        /// </summary>
        public string pg_card_brand { get; set; }

        /// <summary>
        /// Маскированный номер карты (часть цифр номера карты скрыты)
        /// </summary>
        public string pg_card_pan { get; set; }

        /// <summary>
        /// Валюта выставленного счета
        /// </summary>
        public string pg_currency { get; set; }

        /// <summary>
        /// Описание. Может не передаватся или приходить пустым
        /// </summary>
        public string pg_description { get; set; }

        /// <summary>
        /// Код причины отказа
        /// </summary>
        public int? pg_failure_code { get; set; }

        /// <summary>
        /// Описание причины неудачи платежа
        /// </summary>
        public string pg_failure_description { get; set; }

        /// <summary>
        /// Сумма (в валюте pg_currency), которая будет перечислена магазину
        /// </summary>
        public string pg_net_amount { get; set; }

        /// <summary>
        /// Идентификатор платежа в Core
        /// </summary>
        public int pg_order_id { get; set; }

        /// <summary>
        /// Сумма переплаты, если она разрешена в настройках магазина
        /// </summary>
        public string pg_overpayment { get; set; }

        /// <summary>
        /// Идентификатор платежа в Paybox
        /// </summary>
        public int pg_payment_id { get; set; }

        /// <summary>
        /// Идентификатор платежной системы
        /// </summary>
        public string pg_payment_system { get; set; }

        /// <summary>
        /// Сумма счета (в валюте pg_ps_currency), выставленного в ПС. Поле может отсутствовать в случае неудачного платежа.
        /// </summary>
        public string pg_ps_amount { get; set; }

        /// <summary>
        /// Валюта, в которой был произведен платеж в платежной системе. Поле может отсутствовать в случае неудачного платежа.
        /// </summary>
        public string pg_ps_currency { get; set; }

        /// <summary>
        /// Полная сумма (в валюте pg_ps_currency), которую заплатил покупатель с учетом всех комиссий.Поле может отсутствовать в случае неудачного платежа.
        /// </summary>
        public string pg_ps_full_amount { get; set; }

        /// <summary>
        /// Дата, до которой рекуррентный профиль доступен к использованию
        /// </summary>
        public string pg_recurring_profile_expiry_date { get; set; }

        /// <summary>
        /// Идентификатор профиля рекуррентных платежей
        /// </summary>
        public int? pg_recurring_profile_id { get; set; }

        /// <summary>
        /// Результат платежа. 1 – успех, 0 – неудача
        /// </summary>
        public int pg_result { get; set; }

        /// <summary>
        /// Случайная строка
        /// </summary>
        public string pg_salt { get; set; }

        /// <summary>
        /// Телефон покупателя (указанный при инициализации платежа)
        /// </summary>
        public string pg_user_phone { get; set; }

        /// <summary>
        /// Подпись
        /// </summary>
        public string pg_sig { get; set; }

        /// <summary>
        /// Дата и время совершения платежа в формате YYYY-MM-DD HH:MM:SS
        /// </summary>
        public string pg_payment_date { get; set; }

        /// <summary>
        /// Нужно ли оповещать по телефону
        /// </summary>
        public int? pg_need_phone_notification { get; set; }

        /// <summary>
        /// Email плательщика
        /// </summary>
        public string pg_user_contact_email { get; set; }

        /// <summary>
        /// Оповещение по почте
        /// </summary>
        public int? pg_need_email_notification { get; set; }

        /// <summary>
        /// Тестовая транзакция
        /// </summary>
        public int? pg_testing_mode { get; set; }

        /// <summary>
        /// Срок истечения
        /// </summary>
        public string pg_card_exp { get; set; }

        /// <summary>
        /// Владелец карты
        /// </summary>
        public string pg_card_owner { get; set; }


        /// <summary>
        /// Процент скидки, если присутствует
        /// </summary>
        public string pg_discount_percent { get; set; }

        /// <summary>
        /// Сумма скидки, если присутствует
        /// </summary>
        public string pg_discount_amount { get; set; }

        /// <summary>
        /// Метод платежа
        /// </summary>
        public string pg_payment_method { get; set; }


        /// <summary>
        ///     Генерация подписи для проверки при входящем результате о платеже
        /// </summary>
        /// <returns></returns>
        public string GenerateSignature(Guid supplierId)
        {
            var url = CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetResultUrlScript(supplierId);

            var cryptoProvider = new PayboxCryptoProvider();

            if (string.IsNullOrWhiteSpace(url))
                throw new NotFoundException("Ошибка :: Параметр не был указан или указано пустое поле " +
                                    ":: Параметр URL обязателен для составления подписи!!");

            var allProperties = _sourceDataCollection
                .Where(x => x.Key != nameof(pg_sig))
                .Where(x => !string.IsNullOrWhiteSpace(x.Value))
                .OrderBy(x => x.Key)
                .Aggregate(
                    new StringBuilder(url).Append(";"),
                    (sb, kv) => sb.Append(kv.Value).Append(";"))
                .ToString();


            var allData = PayboxCryptoProvider.AppendSecretKey(allProperties, supplierId);

            return cryptoProvider.GenerateMd5Hash(allData);
        }
    }
}
