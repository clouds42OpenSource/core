﻿using System.Text;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Models.PaymentSystem.Interfaces;
using Clouds42.Configurations.Configurations;

namespace Clouds42.Billing.Billing.Models
{
    /// <summary>
    ///     Модель для инициализации платежа
    /// </summary>
    public class PayboxInitPaymentModel : ISignatureGenerator
    {
        /// <summary>
        /// Сумма платежа в валюте pg_currency
        /// </summary>
        public decimal pg_amount { get; set; }

        /// <summary>
        /// URL для сообщения о проведении
        /// магазина клиринга платежа по банковской карте.
        /// </summary>
        public string pg_capture_url { get; set; }

        /// <summary>
        /// URL для проверки возможности
        /// магазина платежа.
        /// </summary>
        public string pg_check_url { get; set; }

        /// <summary>
        /// Валюта, в которой указана сумма. KZT, USD,
        /// EUR.В случае выбора покупателем способа
        /// платежа в другой валюте, производится
        /// пересчет по курсу ЦБ на день платежа.
        /// </summary>
        public string pg_currency { get; set; }

        /// <summary>
        /// (string[1024]) Описание товара или услуги.
        /// Отображается покупателю в процессе платежа.
        /// Передается в кодировке pg_encoding.
        /// </summary>
        public string pg_description { get; set; }

        /// <summary>
        /// Кодировка, в которой указаны другие поля
        /// запроса(только в случае использования
        /// методов GET или POST)
        /// </summary>
        public string pg_encoding { get; set; }

        /// <summary>
        /// url, на который отправляется
        /// пользователь в случае неуспешного платежа
        /// (только для online систем)
        /// </summary>
        public string pg_failure_url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string pg_failure_url_method { get; set; }

        /// <summary>
        /// Язык платежных страниц на сайте PayBoxа и
        /// (если возможно) платежных систем.Значение
        /// ru устанавливает русский язык, en – английский.
        /// </summary>
        public string pg_language { get; set; }

        /// <summary>
        /// Время (в секундах) в течение которого платеж
        /// должен быть завершен, в противном случае
        /// заказ при проведении платежа PayBox откажет
        /// платежной системе в проведении.
        /// </summary>
        public int? pg_lifetime { get; set; }

        /// <summary>
        /// Идентификатор продавца в PayBox. Выдается
        /// при подключении.
        /// </summary>
        public int pg_merchant_id { get; set; }

        /// <summary>
        /// Идентификатор платежа в Core
        /// </summary>
        public int pg_order_id { get; set; }

        /// <summary>
        /// Идентификатор выбранной ПС или группы ПС
        /// </summary>
        public string pg_payment_system { get; set; }

        /// <summary>
        /// Создание платежа с отложенной оплатой
        /// если в этом параметре передано «1».
        /// </summary>
        public string pg_postpone_payment { get; set; }

        /// <summary>
        /// Время на продолжении которого продавец
        /// рассчитывает использовать профиль
        /// рекуррентных платежей
        /// </summary>
        public string pg_recurring_lifetime { get; set; }

        /// <summary>
        /// Флаг, принимает значение 0 или 1.
        /// </summary>
        public int? pg_recurring_start { get; set; }

        /// <summary>
        /// URL для сообщения об отмене
        /// платежа.Вызывается после платежа в случае
        /// отмены платежа на стороне PayBoxа или ПС.
        /// </summary>
        public string pg_refund_url { get; set; }

        /// <summary>
        /// GET, POST или XML – метод вызова
        /// магазина скриптов магазина Check URL, Result URL,
        /// Refund URL, Capture URL для передачи
        /// информации от платежного гейта.
        /// </summary>
        public string pg_request_method { get; set; }

        /// <summary>
        /// URL для сообщения о результате
        /// платежа.Вызывается после платежа в случае
        /// успеха или неудачи.
        /// </summary>
        public string pg_result_url { get; set; }

        /// <summary>
        /// URL сайта магазина для показа покупателю
        /// ссылки, по которой он может вернуться на сайт
        /// магазина после создания счета.Применяется
        /// для offline ПС (наличные).
        /// </summary>
        public string pg_site_url { get; set; }

        /// <summary>
        /// URL скрипта на сайте магазина,
        /// куда перенаправляется покупатель для
        /// ожидания ответа от платежной системы
        /// </summary>
        public string pg_state_url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string pg_state_url_method { get; set; }

        /// <summary>
        ///  url, на который отправляется
        /// пользователь в случае успешного платежа
        /// (только для online систем)
        /// </summary>
        public string pg_success_url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string pg_success_url_method { get; set; }

        /// <summary>
        /// Тестовый режим для транзакции
        /// </summary>
        public int? pg_testing_mode { get; set; }

        /// <summary>
        ///  Контактный адрес электронной
        /// почты пользователя.Если указан, на этот
        /// адрес будут высылаться уведомления об
        /// изменении статуса транзакции.
        /// </summary>
        public string pg_user_contact_email { get; set; }

        /// <summary>
        /// адрес электронной почты
        /// пользователя, в платежной системе
        /// Деньги@Mail.ru.Необходим, если выбрана эта
        /// платежная система.Если не указан, выбор
        /// будет предложен пользователю на сайте
        /// платежного гейта.
        /// </summary>
        public string pg_user_email { get; set; }

        /// <summary>
        /// IP-адрес клиента. Необходим для разбора
        /// спорных ситуаций в случае подозрения на
        /// мошенничество.Параметр можно не
        /// передавать при передаче информации через
        /// браузер пользователя, в этом случае будет
        /// записан тот IP, с которого пользователь
        /// перешёл на страницу инициализации платежа
        /// </summary>
        public string pg_user_ip { get; set; }

        /// <summary>
        /// телефон пользователя (для России начиная с цифр 79..),
        /// необходим для идентификации покупателя.
        /// Если не указан, выбор будет предложен пользователю на
        /// сайтеплатежного гейта.
        /// </summary>
        public int? pg_user_phone { get; set; }

        /// <summary>
        /// Случайная строка
        /// </summary>
        public string pg_salt { get; set; }

        /// <summary>
        /// Подпись
        /// </summary>
        public string pg_sig { get; set; }

        /// <summary>
        ///     Генерация подписи для создания платежа
        /// </summary>
        /// <returns></returns>
        public string GenerateSignature(Guid supplierId)
        {
            var url = CloudConfigurationProvider.BillingAndPayment.PayboxConst.GetPayboxPaymentScript(supplierId);

            var cryptoProvider = new PayboxCryptoProvider();

            if (string.IsNullOrWhiteSpace(url))
                throw new InvalidOperationException("Ошибка :: Параметр не был указан или указано пустое поле " +
                                                    ":: Параметр URL обязателен для составления подписи!!");

            var allProperties = new StringBuilder($"{url};");

            AppendPaymentCommonInfo(allProperties);
            AppendMerchantSettings(allProperties);
            AppendPaymentSystemSettings(allProperties);
            AppendUrlSettings(allProperties);
            AppendPaymentSettings(allProperties);

            var allData = PayboxCryptoProvider.AppendSecretKey(allProperties.ToString(), supplierId);

            return cryptoProvider.GenerateMd5Hash(allData);
        }

        /// <summary>
        /// Добавить общую информацию о платеже
        /// </summary>
        /// <param name="propertiesBuilder">Билдер свойств</param>
        private void AppendPaymentCommonInfo(StringBuilder propertiesBuilder)
        {
            if (pg_amount > 0)
                propertiesBuilder.Append($"{pg_amount};");

            if (!string.IsNullOrWhiteSpace(pg_capture_url))
                propertiesBuilder.Append($"{pg_capture_url};");

            if (!string.IsNullOrWhiteSpace(pg_check_url))
                propertiesBuilder.Append($"{pg_check_url};");

            if (!string.IsNullOrWhiteSpace(pg_currency))
                propertiesBuilder.Append($"{pg_currency};");

            if (!string.IsNullOrWhiteSpace(pg_description))
                propertiesBuilder.Append($"{pg_description};");

            if (!string.IsNullOrWhiteSpace(pg_encoding))
                propertiesBuilder.Append($"{pg_encoding};");

            if (!string.IsNullOrWhiteSpace(pg_failure_url))
                propertiesBuilder.Append($"{pg_failure_url};");

            if (!string.IsNullOrWhiteSpace(pg_failure_url_method))
                propertiesBuilder.Append($"{pg_failure_url_method};");
        }

        /// <summary>
        /// Добавить настройки продавца
        /// </summary>
        /// <param name="propertiesBuilder">Билдер свойств</param>
        private void AppendMerchantSettings(StringBuilder propertiesBuilder)
        {
            if (!string.IsNullOrWhiteSpace(pg_language))
                propertiesBuilder.Append($"{pg_language};");

            if (pg_lifetime != null)
                propertiesBuilder.Append($"{pg_lifetime};");

            if (pg_merchant_id > 0)
                propertiesBuilder.Append($"{pg_merchant_id};");

            if (pg_order_id > 0)
                propertiesBuilder.Append($"{pg_order_id};");
        }

        /// <summary>
        /// Добавить настройки платежной системы
        /// </summary>
        /// <param name="propertiesBuilder">Билдер свойств</param>
        private void AppendPaymentSystemSettings(StringBuilder propertiesBuilder)
        {
            if (!string.IsNullOrWhiteSpace(pg_payment_system))
                propertiesBuilder.Append($"{pg_payment_system};");

            if (!string.IsNullOrWhiteSpace(pg_postpone_payment))
                propertiesBuilder.Append($"{pg_postpone_payment};");

            if (!string.IsNullOrWhiteSpace(pg_recurring_lifetime))
                propertiesBuilder.Append($"{pg_recurring_lifetime};");

            if (pg_recurring_start != null)
                propertiesBuilder.Append($"{pg_recurring_start};");

            if (!string.IsNullOrWhiteSpace(pg_refund_url))
                propertiesBuilder.Append($"{pg_refund_url};");

            if (!string.IsNullOrWhiteSpace(pg_request_method))
                propertiesBuilder.Append($"{pg_request_method};");
        }

        /// <summary>
        /// Добавить настройки адресов
        /// </summary>
        /// <param name="propertiesBuilder">Билдер свойств</param>
        private void AppendUrlSettings(StringBuilder propertiesBuilder)
        {
            if (!string.IsNullOrWhiteSpace(pg_result_url))
                propertiesBuilder.Append($"{pg_result_url};");

            if (!string.IsNullOrWhiteSpace(pg_salt))
                propertiesBuilder.Append($"{pg_salt};");

            if (!string.IsNullOrWhiteSpace(pg_site_url))
                propertiesBuilder.Append($"{pg_site_url};");

            if (!string.IsNullOrWhiteSpace(pg_state_url))
                propertiesBuilder.Append($"{pg_state_url};");

            if (!string.IsNullOrWhiteSpace(pg_state_url_method))
                propertiesBuilder.Append($"{pg_state_url_method};");

            if (!string.IsNullOrWhiteSpace(pg_success_url))
                propertiesBuilder.Append($"{pg_success_url};");

            if (!string.IsNullOrWhiteSpace(pg_success_url_method))
                propertiesBuilder.Append($"{pg_success_url_method};");
        }

        /// <summary>
        /// Добавить настройки платежа
        /// </summary>
        /// <param name="propertiesBuilder">Билдер свойств</param>
        private void AppendPaymentSettings(StringBuilder propertiesBuilder)
        {
            if (pg_testing_mode != null)
                propertiesBuilder.Append($"{pg_testing_mode};");

            if (!string.IsNullOrWhiteSpace(pg_user_contact_email))
                propertiesBuilder.Append($"{pg_user_contact_email};");

            if (!string.IsNullOrWhiteSpace(pg_user_email))
                propertiesBuilder.Append($"{pg_user_email};");

            if (!string.IsNullOrWhiteSpace(pg_user_ip))
                propertiesBuilder.Append($"{pg_user_ip};");

            if (pg_user_phone != null)
                propertiesBuilder.Append($"{pg_user_phone};");
        }
    }
}
