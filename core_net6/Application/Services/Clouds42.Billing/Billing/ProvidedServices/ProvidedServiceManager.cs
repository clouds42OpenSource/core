﻿using Clouds42.Billing.Billing.ModelProcessors;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.Account.AccountBilling.ProvidedServices;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.ProvidedServices
{
    /// <summary>
    /// Менеджер оказанных услуг
    /// </summary>
    public class ProvidedServiceManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ProvidedServicesAndTransactionsDmProcessor providedServicesAndTransactionsDmProcessor,
        ILogger42 logger,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить список оказанных услуг V2
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="query"></param>
        /// <returns>Cписок оказанных услуг</returns>
        public ManagerResult<SelectDataResultCommonDto<ProvidedServiceDto>> GetPaginatedProvidedServicesForAccount(Guid accountId,
            SelectDataCommonDto<ProvidedServiceFilterDto> query)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ProvidedServices_View, () => accountId);
                return Ok(providedServicesAndTransactionsDmProcessor.GetPaginatedProvidedServicesForAccount(accountId, query));
            }
            catch (Exception ex)
            {
                var message =
                    $"Получение списка оказанных услуг для аккаунта '{accountId}' завершено с ошибкой.";
                logger.Warn($"{message} Exception: {ex.GetFullInfo()}");
                return PreconditionFailed<SelectDataResultCommonDto<ProvidedServiceDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список оказанных услуг по долговременным сервисам (ЗД и рекогнишен)
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="query"></param>
        /// <returns>Список оказанных услуг по долговременным сервисам</returns>
        public ManagerResult<SelectDataResultCommonDto<ProvidedServiceDto>> GetPaginatedProvidedLongServicesForAccount(
            Guid accountId, SelectDataCommonDto<ProvidedLongServiceFilterDto> query)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ProvidedServices_View, () => accountId);
                return Ok(providedServicesAndTransactionsDmProcessor.GetPaginatedProvidedLongServicesForAccount(accountId, query));
            }
            catch (Exception ex)
            {
                var message =
                    $"Получение списка оказанных услуг для аккаунта '{accountId}' завершено с ошибкой.";
                logger.Warn($"{message} Exception: {ex.GetFullInfo()}");
                return PreconditionFailed<SelectDataResultCommonDto<ProvidedServiceDto>>(ex.Message);
            }

        }
    }
}
