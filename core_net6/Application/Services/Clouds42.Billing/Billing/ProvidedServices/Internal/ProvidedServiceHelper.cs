﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling.ProvidedServices;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.Billing.ProvidedServices.Internal
{
    /// <summary>
    /// Хэлпер для оказанных услуг
    /// </summary>
    public class ProvidedServiceHelper(IUnitOfWork dbLayer, IHandlerException handlerException) : IProvidedServiceHelper
    {
        private readonly DateTime _now = DateTime.Now;

        /// <summary>
        /// Зарегистрировать активацию сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Тип сервиса</param>
        /// <param name="serviceExpireDate">Дата окончания сервиса</param>
        /// <param name="resourceType">Тип лицензии</param>
        /// <param name="kind">Тип предоставления услуги</param>
        /// <param name="freeResourceCount">Количество свободных ресурсов</param>
        public void RegisterServiceActivation(Guid accountId, Clouds42Service service, DateTime? serviceExpireDate, ResourceType resourceType, 
            KindOfServiceProvisionEnum kind, int freeResourceCount = 1)
        {
            var newProvidedServiceRow = CreateProvidedServiceObject(accountId, service, serviceExpireDate, resourceType,
                kind, freeResourceCount);

            try
            {
                dbLayer.ProvidedServiceRepository.Insert(newProvidedServiceRow);
                dbLayer.Save();             
            }
            catch (Exception ex)
            {             
                handlerException.Handle(ex, $"[Ошибка фиксации активации сервиса] '{resourceType}'");
                throw;
            }
        }

        /// <summary>
        /// Зарегистрировать разовый платеж
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConf">Ресурс конфигурация</param>
        /// <param name="resourceType">Тип лицензии</param>
        /// <param name="rate">Тариф</param>
        public void RegisterSinglePay(Guid accountId, ResourcesConfiguration resConf, 
            ResourceType resourceType, decimal rate)
        {
            var newProvidedServiceRow = CreateProvidedServiceObject(accountId, resConf, resourceType, rate);

            try
            {
                dbLayer.ProvidedServiceRepository.Insert(newProvidedServiceRow);
                dbLayer.Save();
            }
            catch (Exception ex)
            {             
                handlerException.Handle(ex, "[Ошибка фиксации движения услуги]");
                throw;
            }
        }

        /// <summary>
        /// Зарегистрировать покупку дискового пространства
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConf">Ресурс конфигурация</param>
        /// <param name="rate">Тариф</param>
        public void RegisterPayDisk(Guid accountId, ResourcesConfiguration resConf, decimal rate)
        {
            var purchaseDate = DateTime.Now;
            var recDate = resConf.ExpireDateValue;

            var histObj = dbLayer.ProvidedServiceRepository.FirstOrDefault(f =>
                f.AccountId == accountId && f.Service == resConf.BillingService.Name &&
                f.DateTo == recDate);

            var newProvidedServiceRow = CreateProvidedServiceObject(accountId, resConf, rate, purchaseDate);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                if (histObj != null)
                {                        
                    histObj.DateTo = purchaseDate;
                    dbLayer.ProvidedServiceRepository.Update(histObj);
                }

                dbLayer.ProvidedServiceRepository.Insert(newProvidedServiceRow);
                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                dbScope.Rollback();
                handlerException.Handle(ex, "[Ошибка фиксации покупки диска]");
                throw;
            }
        }

        /// <summary>
        /// Зарегистрировать изменения Аренды 1С
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConf">Ресурс конфигурация</param>
        /// <param name="buyRent1CData">Данные биллинга</param>
        public void RegisterRent1CChanges(Guid accountId, ResourcesConfiguration resConf,
            AccountBillingDataForBuyRent1CDto buyRent1CData)
        {
            try
            {
                if (resConf.FrozenValue)
                    return;

                RegisterProvidedServiceForRent1CStandart(accountId, resConf, buyRent1CData);
                RegisterProvidedServiceForRent1CWeb(accountId, resConf, buyRent1CData);
                RegisterSponsoredLicenses(accountId, resConf, buyRent1CData);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка фиксации оказанных услуг] для аккаунта {accountId}");
            }
        }

        /// <summary>
        /// Зарегистрировать пролонгацию сервиса
        /// </summary>
        /// <param name="resConfig">Ресурс конфигурация сервиса</param>
        public void RegisterServiceProlongation(ResourcesConfiguration resConfig)
        {
            if (!resConfig.BillingService.SystemService.HasValue)
                return;

            if (!NeedToRegisterProlongation(resConfig))
                return;

            var listRes = GetItemsListForRegisterProlongation(resConfig);
            
            try
            {
                dbLayer.ProvidedServiceRepository.InsertRange(listRes);
                dbLayer.Save();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка записи движения сервиса] '{resConfig.BillingService.SystemService.Value}' у accountId = '{resConfig.AccountId}'");

                throw;
            }
        }

        /// <summary>
        /// Сменить дату окончания Аренды 1С
        /// </summary>
        /// <param name="resConfig">Ресурс конфигурация сервиса</param>
        /// <param name="newDateToValue">Новая дата окончания</param>
        public void ChangeRent1CExpireDate(ResourcesConfiguration resConfig, DateTime newDateToValue)
        {
            if (!resConfig.BillingService.SystemService.HasValue 
                && resConfig.BillingService.SystemService != Clouds42Service.MyEnterprise)
                return;

            var providedService = dbLayer.ProvidedServiceRepository.FirstOrDefault(w =>
                w.AccountId == resConfig.AccountId && w.Service == Clouds42Service.MyEnterprise.ToString() && w.DateTo == resConfig.ExpireDateValue);

            if (providedService == null)
                return;

            providedService.DateTo = newDateToValue;

            dbLayer.ProvidedServiceRepository.Update(providedService);
            dbLayer.Save();
        }

        /// <summary>
        /// Признак необходимости регистрировать пролонгацию сервиса
        /// </summary>
        /// <param name="resConfig">Ресурс конфигурация сервиса</param>
        /// <returns>true - если сервис это Мой Диск или Аренда 1С</returns>
        private bool NeedToRegisterProlongation(ResourcesConfiguration resConfig)
        {
            if (resConfig == null || resConfig.FrozenValue ||
                (resConfig.BillingService.SystemService != Clouds42Service.MyEnterprise &&
                resConfig.BillingService.SystemService != Clouds42Service.MyDisk))
                return false;


            return true;
        }

        /// <summary>
        /// Зарегистрировать оказанные услуги по Аренде 1С для спонсируемых аккаунтов
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConf">Ресурс конфигурация</param>
        /// <param name="buyRent1CData">Данные биллинга</param>
        private void RegisterSponsoredLicenses(Guid accountId, ResourcesConfiguration resConf,
            AccountBillingDataForBuyRent1CDto buyRent1CData)
        {
            var sponsoredAccountIds = dbLayer.ResourceRepository.Where(w =>
                    w.AccountSponsorId == accountId &&
                    (w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser ||
                     w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb))
                .Select(w => w.AccountId).Distinct().ToList();

            foreach (var sponsoredAccountId in sponsoredAccountIds)
            {
                RegisterProvidedServiceForRent1CStandart(accountId, resConf, buyRent1CData, sponsoredAccountId);
                RegisterProvidedServiceForRent1CWeb(accountId, resConf, buyRent1CData, sponsoredAccountId);
            }
        }

        /// <summary>
        /// Зарегистрировать оказанные услуги по Аренде 1С Standart
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConfig">Ресурс конфигурация</param>
        /// <param name="buyRent1CData">Данные биллинга</param>
        /// <param name="sponsoredAccountId">ID спонсируемого аккаунта</param>
        private void RegisterProvidedServiceForRent1CStandart(Guid accountId, ResourcesConfiguration resConfig,
            AccountBillingDataForBuyRent1CDto buyRent1CData,
            Guid? sponsoredAccountId = null)
        {
            var accountStandartResources = dbLayer.ResourceRepository.Where(w =>
                w.AccountId == accountId && w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser).ToList();

            if (sponsoredAccountId != null)
            {
                accountStandartResources = dbLayer.ResourceRepository.Where(w =>
                    w.AccountSponsorId == accountId && w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser &&
                    w.Subject != null).ToList();
            }

            var oldProvidedService = dbLayer.ProvidedServiceRepository.OrderByDescending(w => w.CreationDateTime).FirstOrDefault(w =>
                w.AccountId == accountId && w.ServiceType == ResourceType.MyEntUser.ToString() && w.SponsoredAccountId == sponsoredAccountId);

            if (oldProvidedService == null && accountStandartResources.Count > 0)
            {

                var newProvidedService = CreateRent1CProvidedService(accountId, resConfig,
                    accountStandartResources.Count, buyRent1CData.RdpRateCost + buyRent1CData.WebRateCost,
                    ResourceType.MyEntUser.ToString(), sponsoredAccountId);

                dbLayer.ProvidedServiceRepository.Insert(newProvidedService);
                dbLayer.Save();

                return;
            }

            if (oldProvidedService == null || oldProvidedService.Count >= accountStandartResources.Count && !RecordIsHistorical(oldProvidedService))
                return;

            if (accountStandartResources.Count > 0)
            {
                var newProvidedService = CreateRent1CProvidedService(accountId, resConfig,
                    accountStandartResources.Count, buyRent1CData.RdpRateCost + buyRent1CData.WebRateCost,
                    ResourceType.MyEntUser.ToString(), sponsoredAccountId);

                dbLayer.ProvidedServiceRepository.Insert(newProvidedService);
            }

            ProvidedServiceSetExpired(oldProvidedService);
        }

        /// <summary>
        /// Зарегистрировать оказанные услуги по Аренде 1С WEB 
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConfig">Ресурс конфигурация</param>
        /// <param name="buyRent1CData">Данные биллинга</param>
        /// <param name="accountSponsorId"></param>
        private void RegisterProvidedServiceForRent1CWeb(Guid accountId, ResourcesConfiguration resConfig,
            AccountBillingDataForBuyRent1CDto buyRent1CData, Guid? accountSponsorId = null)
        {
            
            var activeRdpCount = GetResourcesCount(ResourceType.MyEntUser, accountId, accountSponsorId);
            var activeWebCount = GetResourcesCount(ResourceType.MyEntUserWeb, accountId, accountSponsorId);

            var onlyWebResourcesCount = Math.Max(0, activeWebCount - activeRdpCount);

            var oldProvidedService = dbLayer.ProvidedServiceRepository.OrderByDescending(w => w.CreationDateTime).FirstOrDefault(w =>
                w.AccountId == accountId && w.ServiceType == ResourceType.MyEntUserWeb.ToString() && w.SponsoredAccountId == accountSponsorId);

            if (oldProvidedService == null && onlyWebResourcesCount > 0)
            {
                var newProvidedService = CreateRent1CProvidedService(accountId, resConfig,
                    onlyWebResourcesCount, buyRent1CData.WebRateCost,
                    ResourceType.MyEntUserWeb.ToString(), accountSponsorId);

                dbLayer.ProvidedServiceRepository.Insert(newProvidedService);
                dbLayer.Save();

                return;
            }

            if (oldProvidedService == null || onlyWebResourcesCount == oldProvidedService.Count && !RecordIsHistorical(oldProvidedService))
                return;

            if (onlyWebResourcesCount > 0)
            {
                var newProvidedService = CreateRent1CProvidedService(accountId, resConfig,
                    onlyWebResourcesCount, buyRent1CData.WebRateCost,
                    ResourceType.MyEntUserWeb.ToString(), accountSponsorId);
                dbLayer.ProvidedServiceRepository.Insert(newProvidedService);
            }

            ProvidedServiceSetExpired(oldProvidedService);

        }

        private bool RecordIsHistorical(ProvidedService providedService)
            => providedService.DateTo.HasValue && providedService.DateTo.Value <= DateTime.Now;

        private int GetResourcesCount(ResourceType resourceType, Guid accountId, Guid? accountSponsorId)
        {
            return dbLayer.ResourceRepository.WhereLazy().Count(w =>
                w.AccountId == accountId && w.BillingServiceType.SystemServiceType == resourceType &&
                w.AccountSponsorId == accountSponsorId);
        }

        private void ProvidedServiceSetExpired(ProvidedService providedService)
        {
            providedService.DateTo = _now;
            dbLayer.ProvidedServiceRepository.Update(providedService);
            dbLayer.Save();
        }

        /// <summary>
        /// Создать объект оказанной услуги по Аренде 1С
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConfig">Ресурс конфигурация сервиса</param>
        /// <param name="count">Кол-во лицензий</param>
        /// <param name="rate">Тариф</param>
        /// <param name="serviceType">Тип услуги</param>
        /// <param name="sponsoredAccountId">ID спонсируемого аккаунта</param>
        /// <returns>Объект оказанной услуги по Аренде 1С</returns>
        private ProvidedService CreateRent1CProvidedService(Guid accountId, 
            ResourcesConfiguration resConfig, 
            int count, decimal rate, string serviceType, Guid? sponsoredAccountId)
            => new()
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Count = count,
                DateFrom = DateTime.Now,
                DateTo = resConfig.ExpireDateValue,
                KindOfServiceProvision = KindOfServiceProvisionEnum.Periodic,
                Rate = rate,
                Service = Clouds42Service.MyEnterprise.ToString(),
                ServiceType = serviceType,
                SponsoredAccountId = sponsoredAccountId
            };

        /// <summary>
        /// Получить список элементов для регистрации пролонгации
        /// </summary>
        /// <returns>Список элементов для регистрации пролонгации</returns>
        private List<ProvidedService> GetItemsListForRegisterProlongation(ResourcesConfiguration resourcesConfiguration)
        {
            var resources = dbLayer.ResourceRepository.Where(
                r => r.Subject != null && r.BillingServiceType.Service.SystemService == resourcesConfiguration.BillingService.SystemService.Value &&
                     ((r.AccountId == resourcesConfiguration.AccountId && r.AccountSponsorId == null) || r.AccountSponsorId == resourcesConfiguration.AccountId)).ToList();

            var listRes = new List<ProvidedService>();
            foreach (var resource in resources)
            {
                if (resource.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb)
                {
                    if (!resources.Any(l => l.BillingServiceType.SystemServiceType == ResourceType.MyEntUser && l.Subject == resource.Subject))
                    {
                        AddToResList(listRes, new ResourceItemDto(resource), resourcesConfiguration);
                    }
                }
                else if (resource.BillingServiceType.SystemServiceType == ResourceType.MyEntUser)
                {
                    var webLicense = resources.FirstOrDefault(l => l.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb && l.Subject == resource.Subject);
                    var resourceItem = new ResourceItemDto(resource);
                    resourceItem.Cost += webLicense?.Cost ?? 0;
                    AddToResList(listRes, resourceItem, resourcesConfiguration);
                }
                else
                {
                    AddToResList(listRes, new ResourceItemDto(resource), resourcesConfiguration);
                }
            }

            return listRes;
        }

        /// <summary>
        /// Добавить элемент в список ресурсов
        /// </summary>
        /// <param name="listRes">Список ресурсов</param>
        /// <param name="resource">Элемент для добавления</param>
        /// <param name="resConf"></param>
        private void AddToResList(List<ProvidedService> listRes, ResourceItemDto resource, ResourcesConfiguration resConf)
        {

            Guid? //Спонсируемый аккаунт 
                  sponsoredAccountId = null;
            if (resource.AccountSponsorId.HasValue)
            {
                sponsoredAccountId = resource.AccountId;
            }

            var exist = listRes.FirstOrDefault(e => e.Rate == resource.Cost && e.ServiceType == resource.ResourceType.ToString() &&
                e.SponsoredAccountId == sponsoredAccountId);

            if (exist != null)
            {
                exist.Count += 1;
                return;
            }
            
            listRes.Add(new()
            {
                Id = Guid.NewGuid(),
                AccountId = resConf.AccountId,
                Service = resConf.BillingService.SystemService.ToString(),
                KindOfServiceProvision = KindOfServiceProvisionEnum.Periodic,
                DateFrom = DateTime.Now,
                DateTo = resConf.ExpireDateValue,
                ServiceType = resource.ResourceType.ToString(),
                Rate = resource.Cost,
                SponsoredAccountId = sponsoredAccountId,
                Count = 1
            });
        }

        /// <summary>
        /// Создать объект оказанной услуги
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConf">Ресурс конфигурация сервиса</param>
        /// <param name="rate">Тариф</param>
        /// <param name="purchaseDate">Дата покупки</param>
        /// <returns>Объект оказанной услуги</returns>
        private ProvidedService CreateProvidedServiceObject(Guid accountId, ResourcesConfiguration resConf, 
            decimal rate, DateTime purchaseDate)
            => new()
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Service = resConf.BillingService.SystemService.ToString(),
                ServiceType = ResourceType.DiskSpace.ToString(),
                Rate = rate,
                DateFrom = purchaseDate,
                DateTo = resConf.ExpireDateValue,
                Count = 1,
                KindOfServiceProvision = KindOfServiceProvisionEnum.Periodic
            };

        /// <summary>
        /// Создать объект оказанной услуги
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Тип сервиса</param>
        /// <param name="serviceExpireDate">Дата окончания сервиса</param>
        /// <param name="resourceType">Тип лицензии</param>
        /// <param name="kind">Тип предоставления услуги</param>
        /// <param name="freeResourceCount">Количество свободных ресурсов</param>
        /// <returns>Объект оказанной услуги</returns>
        private ProvidedService CreateProvidedServiceObject(Guid accountId, 
            Clouds42Service service, DateTime? serviceExpireDate, ResourceType resourceType,
            KindOfServiceProvisionEnum kind, int freeResourceCount)
            => new()
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Service = service.ToString(),
                ServiceType = resourceType.ToString(),
                Rate = 0,
                DateFrom = DateTime.Now,
                DateTo = serviceExpireDate,
                Count = freeResourceCount,
                KindOfServiceProvision = kind
            };

        /// <summary>
        /// Создать объект оказанной услуги
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConf">Ресурс конфигурация сервиса</param>
        /// <param name="resourceType">Тип лицензии</param>
        /// <param name="rate">Тариф</param>
        /// <returns>Объект оказанной услуги</returns>
        private ProvidedService CreateProvidedServiceObject(Guid accountId, ResourcesConfiguration resConf,
            ResourceType resourceType, decimal rate)
        {
            var resourceTypeStr = resourceType.ToString();
            var purchaseDate = DateTime.Now;

            DateTime? dateTo = resConf.ExpireDateValue;
            if (resConf.BillingService.SystemService == Clouds42Service.Recognition)
                dateTo = null;

            return new ProvidedService
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Service = resConf.BillingService.SystemService.ToString(),
                ServiceType = resourceTypeStr,
                Rate = rate,
                DateFrom = purchaseDate,
                DateTo = dateTo,
                Count = 1,
                KindOfServiceProvision = KindOfServiceProvisionEnum.Single
            };
        }
    }
}
