﻿namespace Clouds42.Billing.Billing.Constants
{
    /// <summary>
    /// Константы для тарифа
    /// </summary>
    public static class RateConst
    {
        /// <summary>
        /// Тип аккаунта Стандарт
        /// </summary>
        public const string AccountTypeStandart = "Standart";

        /// <summary>
        /// Месячный период тарифа
        /// </summary>
        public const string RatePeriodMonth = "Month";
    }
}
