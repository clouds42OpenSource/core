﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing
{

    /// <summary>
    /// Менеджер управления биллингом.
    /// </summary>
    public class BillingManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IProlongServicesProvider prolongServicesProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), IBillingManager
    {
        /// <summary>
        /// Обработать просроченные обещанные платежи.
        /// </summary>
        public ManagerResult ProcessExpiredPromisePayments()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_ProcessExpiredPromisePayments);
                prolongServicesProvider.ProcessExpiredPromisePayments();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При обработке просроченных ОП произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Продлить или заблокировать просроченные сервисы.
        /// </summary>        
        public ManagerResult ProlongOrLockExpiredServices()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_ProlongOrLockExpiredServices);
                prolongServicesProvider.ProlongOrLockExpiredServices();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При блокировке/продлении сервисов произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Разблокировать заблокированные сервисы на которые есть деньги что бы разблокировать.
        /// </summary>        
        public ManagerResult ProlongLockedServices()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_ProlongLockedServices);
                prolongServicesProvider.ProlongPaidLockedServices();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При разблокировке сервисов произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Уведомить клиентов о скорой блокировке сервисов.
        /// </summary>
        public  ManagerResult NotifyBeforeLockServices()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_NotifyBeforeLockServices);
                prolongServicesProvider.NotifyBeforeLockServices();
                prolongServicesProvider.IssueInvoicesBeforeRentalEnd();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При уведомлении о скорой блокировке сервисов произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Продление заблокированных сервисов у спонсируемых аккаунтов
        /// при нулевой цене и активном спонсировании 
        /// </summary>
        public ManagerResult UnlockSponsoredServices()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_ProlongLockedServices);
                prolongServicesProvider.UnlockSponsoredServices();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При разблокировке сервисов у спонсируемых аккаунтов произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Заблокировать сервисы с просроченным демо периодом
        /// </summary>
        public ManagerResult LockExpiredDemoServices()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_LockExpireDemoServices);
                prolongServicesProvider.LockExpiredDemoServices();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При блокировке сервисов с просроченным демо периодом произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }


        /// <summary>
        /// Заблокировать или разблокировать сервисы гибридные
        /// </summary>
        public ManagerResult ProlongOrLockHybridExpiredServices()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_LockExpireDemoServices);
                prolongServicesProvider.ProlongOrLockHybridExpiredServices();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При блокировке сервисов с просроченным демо периодом произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Уведомить клиентов о том, что заканчивается демо период.
        /// </summary>
        public ManagerResult NotifyServiceDemoPeriodIsComingToEnd()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_NotifyDemoPeriodIsComingToEnd);
                prolongServicesProvider.NotifyServiceDemoPeriodIsComingToEnd();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При отправке сообщений о скором завершении демо произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Уведомить клиентов о скором автосписании для продления сервисов
        /// </summary>
        public ManagerResult NotifyBeforeLockServicesAutoPay()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingManager_NotifyBeforeLockServices);
                prolongServicesProvider.NotifyBeforeLockServicesAutoPay();
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = "[При уведомлении о скором автосписании для продления сервисов произошла ошибка.]";
                logger.Warn(errorMessage + ex.Message);
                handlerException.Handle(ex, errorMessage);
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
