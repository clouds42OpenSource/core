﻿using Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Billing.Helpers
{
    /// <summary>
    /// Хелпер для работы с временем доступности услуг сервиса для аккаунта
    /// </summary>
    public class ServiceTypesAvailabilityDateTimeForAccountHelper(
        IBillingServiceTypeAvailabilityManager billingServiceTypeAvailabilityManager)
    {
        /// <summary>
        /// Удалить время доступности услуг сервиса для аккаунта
        /// </summary>
        /// <param name="dependedResourceConfigurations">Список ресур конфигураций кастомных сервисов аккаунта</param>
        public void Delete(List<ResourcesConfiguration> dependedResourceConfigurations)
        {
            dependedResourceConfigurations.ForEach(resConfig =>
            {
                billingServiceTypeAvailabilityManager.DeleteAvailabilityDateTimeForAccountByBillingServiceId(
                    resConfig.BillingServiceId, resConfig.AccountId);
            });
        }
    }
}
