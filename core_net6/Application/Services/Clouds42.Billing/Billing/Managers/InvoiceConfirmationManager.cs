﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Managers
{

    /// <summary>
    /// Менеджер по обработке принятия счетов на оплату.
    /// </summary>
    public class InvoiceConfirmationManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IInvoiceConfirmationProvider invoiceConfirmationProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Провести платеж в отдельном потоке путем принятия счета на оплату.
        /// </summary>
        /// <param name="confirmationInvoice">Данные платежа.</param>
        public ManagerResult ConfirmInvoiceViaNewThread(ConfirmationInvoiceDto confirmationInvoice)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Invoices_Confirmation);

                var invoice = DbLayer.InvoiceRepository.GetInvoiceById(confirmationInvoice.ID);
                if (invoice == null)
                    return NotFound($"Invoice {confirmationInvoice.ID} not found");

                if (invoice.State == InvoiceStatus.Processed.ToString())
                    return PreconditionFailed($"payment {confirmationInvoice.ID} Processed");

                invoiceConfirmationProvider.ConfirmInvoice(confirmationInvoice);

                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed($"Payment {confirmationInvoice.ID} confirm error {ex.Message}");
            }
            
        }

    }
}
