﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Billing.Billing.Managers
{
    /// <summary>
    /// Менеджер по работе с актами счетов на оплату
    /// </summary>
    public class InvoiceActManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ISynchronizationCorpHelper synchronizationCorpHelper,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly InvoiceActHelper _invoiceActHelper = new(dbLayer);

        /// <summary>
        /// Установить номер акта для счета на оплату.
        /// </summary>
        /// <param name="invoiceId">Номер счета.</param>
        /// <param name="actId">Номер присваемого акта.</param>
        /// <param name="description">Коментарий к прикрепленному акту.</param>
        /// <param name="requiresSignature">Требуется ли подпись акта</param>
        public async Task<ManagerResult> SetAct(Guid invoiceId, Guid? actId, string description, bool requiresSignature)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Invoices_Confirmation);

                Logger.Info($"Начало прикрепления акта №{actId} на основании счета на оплату №{invoiceId}");

                var needWriteLogEvent = actId == null ||
                                        _invoiceActHelper.NeedToLogSettingActId(invoiceId, actId, description);

                var invoice = await DbLayer.InvoiceRepository.FirstOrDefaultAsync(i => i.Id == invoiceId) ??
                              throw new NotFoundException($"По номеру {invoiceId} не найден счет на оплату.");

                await DbLayer.InvoiceRepository.SetActId(invoiceId, actId, description, requiresSignature);

                try
                {
                    if (!needWriteLogEvent)
                        return Ok();

                    var logEventDescription =
                        _invoiceActHelper.GetActOperationLogEventDescription(invoice.Uniq, description, actId,
                            invoice.ActDescription);

                    LogEventHelper.LogEvent(dbLayer, invoice.AccountId, accessProvider,
                        LogActions.FormServiceAct, logEventDescription);
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Сбой логирования установки номера акта] :: {invoiceId}");
                }
            }
            catch (Exception ex)
            {
                handlerException.HandleWarning(ex.Message, $"Не удалось установить номер акта инвойсу {invoiceId}");
                return PreconditionFailed(ex.Message);
            }

            return Ok();
        }
        
        /// <summary>
        /// Получить файл акта из хранилища
        /// </summary>
        /// <param name="actId"></param>
        /// <returns>Файл акта</returns>
        public async Task<CloudFile?> GetActFileAsync(Guid? actId) =>
            await dbLayer.CloudFileRepository.AsQueryable().FirstOrDefaultAsync(f => f.Id == actId);

        
        /// <summary>
        /// Удалить файл акта из хранилища
        /// </summary>
        /// <param name="actId"></param>
        /// <returns></returns>
        public async Task DeleteActFileAsync(Guid? actId)
        {
            
            if (!actId.HasValue)
            {
                return;
            }
            var act = await GetActFileAsync(actId.Value);
               if (act == null)
               {
                   return;
               }

            if (await GetActFileAsync(actId.Value) == null)
            {
                return;
            }
            dbLayer.CloudFileRepository.Delete(actId);
            await dbLayer.SaveAsync();
        }
        


        /// <summary>
        /// Сформировать PDF документ акта по инвойсу.
        /// </summary>
        /// <param name="actId">Идентификатор инвойса.</param>
        /// <returns>Байт массив полученного акта.</returns>
        public async Task<ManagerResult<(byte[] act, string fileName)>> BuildPdfFileForAct(Guid actId)
        {
          
            try
            {

                var invoice = await DbLayer.InvoiceRepository
                                  .AsQueryableNoTracking()
                                  .FirstOrDefaultAsync(i => i.ActID == actId) ??
                              throw new NotFoundException($"По идентификатору акта'{actId}' не найден счет на оплату.");

                var fileName = $"{invoice.ActDescription}.pdf";

                return Ok((await synchronizationCorpHelper.GetAct(actId), fileName));
            }
            catch (Exception ex)
            {
                Logger.Warn(ex, $"[Не удалось скачать акт по идентификатору] '{actId}'");

                return PreconditionFailed<(byte[] act, string fileName)>(ex.Message);
            }
        }

    }
}
