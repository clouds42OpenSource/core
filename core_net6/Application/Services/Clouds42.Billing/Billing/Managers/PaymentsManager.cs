﻿using System.Globalization;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.BillingOperations.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models.PaymentSystem;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.Billing.Contracts.Factories.Models;
using Clouds42.Billing.Factories.PreparePaymentModelFactory;
using Clouds42.Billing.Factories.PreparePaymentModelFactory.PaymentsModels;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Constants;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.Billing.Managers
{
    public partial class PaymentsManager : BaseManager
    {
        private readonly ILogger42 _logger;
        private readonly IBillingPaymentsDataProvider _billingPaymentsDataProvider;
        private readonly string _invoiceNumberPrefix;
        private readonly IBonusPaymentProvider _bonusPaymentProvider;
        private readonly ICloudCoreDataProvider _cloudCoreDataProvider;
        private readonly Lazy<string> _cpSite;
        private readonly Lazy<string> _routeForPayViaYookassa;
        private readonly IConfiguration _configuration;
        readonly PreparePaymentModelFactory _preparePaymentModelFactory;

        public PaymentsManager(IAccessProvider accessProvider,
            IUnitOfWork dbLayer,
            ILogger42 logger,
            IBillingPaymentsDataProvider billingPaymentsDataProvider,
            IConfiguration configuration,
            PreparePaymentModelFactory preparePaymentModelFactory,
            ICloudCoreDataProvider cloudCoreDataProvider, IHandlerException handlerException)
            : base(accessProvider, dbLayer, handlerException)
        {
            _logger = logger;
            _billingPaymentsDataProvider = billingPaymentsDataProvider;
            _invoiceNumberPrefix = Configurations.Configurations.CloudConfigurationProvider.Invoices.UniqPrefix();
            _bonusPaymentProvider = new BonusPaymentProvider(new BonusPaymentHelper(),
                new BillingPaymentsProvider(dbLayer, new BillingPaymentsHelper(dbLayer), _logger, handlerException));
            _cpSite = new Lazy<string>(Configurations.Configurations.CloudConfigurationProvider.Cp.GetSiteAuthorityUrl);
            _routeForPayViaYookassa = new Lazy<string>(Configurations.Configurations.CloudConfigurationProvider.Cp.GetRouteForPayViaYookassa);
            _cloudCoreDataProvider = cloudCoreDataProvider;
            _configuration = configuration;
            _preparePaymentModelFactory = preparePaymentModelFactory;
        }

        #region public 

        public void EditPaySystemDescription(int payNumber, string system)
        {
            var payment = DbLayer.PaymentRepository.FindPayment(payNumber);
            payment.Description += " (" + system + ")";
            DbLayer.PaymentRepository.Update(payment);
            DbLayer.Save();
        }

        public void SetExternalPaymentStatus(int payNumber, int idUps)
        {
            var payment = DbLayer.PaymentRepository.FindPayment(payNumber);
            payment.PaymentSystemTransactionId = idUps.ToString();
            DbLayer.PaymentRepository.Update(payment);
            DbLayer.Save();
        }

        public ReadyForPay PreparePayment(Guid accountId, decimal addedMoney, PaymentSystem paymentSystem)
        {
            AccessProvider.HasAccess(ObjectAction.Payments_PreparePayment, () => accountId);
            var account = DbLayer.AccountsRepository.AsQueryable().Include(x => x.AccountConfiguration).FirstOrDefault(x => x.Id == accountId);
            var preparePayment = _billingPaymentsDataProvider.PreparePayment(accountId, addedMoney, paymentSystem);

            var prepareForPayment = new ReadyForPay { Account = account, PaymentNumber = preparePayment };

            return prepareForPayment;
        }

        #endregion

        /// <summary>
        ///     Подготовка и получение модели платежа
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="totalSum">Сумма к оплате</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Модель платежа</returns>
        public PaymentModelDto GetPaymentModel(Guid accountId, decimal totalSum, string localeName)
        {
            AccessProvider.HasAccess(ObjectAction.Payments_System, () => accountId);

            var model = new PaymentModelDto();

            var preparePayment = new PreparePaymentStruct { AccountId = accountId, Sum = totalSum };

            switch (localeName)
            {
                case LocaleConst.Ukraine:
                {
                    preparePayment.PaymentSystem = PaymentSystem.UkrPays;
                    var ukrPaysModel =
                        _preparePaymentModelFactory.PreparePaymentModel<PrepareUkrPaysModel, UkrPayInitDataModel>(preparePayment);
                    model.ModelForUkrPays = ukrPaysModel;
                    return model;
                }
                case LocaleConst.Kazakhstan or LocaleConst.Uzbekistan:
                {
                    preparePayment.PaymentSystem = PaymentSystem.Paybox;
                    var payboxModel =
                        _preparePaymentModelFactory.PreparePaymentModel<PreparePayboxModel, string>(preparePayment);
                    model.OtherPaymentModels = payboxModel;
                    return model;
                }
                case LocaleConst.Russia:
                    model.OtherPaymentModels = GetUrlForPayViaRussianLocalePaymentAggregator(preparePayment);
                    break;
            }

            return model;
        }

        /// <summary>
        /// Получить урл для оплаты через агрегатор русской локали
        /// </summary>
        /// <param name="preparePayment">Модель данных для подготовки платежа</param>
        /// <returns>Урл для оплаты через агрегатор русской локали</returns>
        private string GetUrlForPayViaRussianLocalePaymentAggregator(PreparePaymentStruct preparePayment)
        {
            if (_cloudCoreDataProvider.GetRussianLocalePaymentAggregator() ==
                RussianLocalePaymentAggregatorEnum.Yookassa)
                return GetUrlForPayViaYookassa(preparePayment.Sum);

            preparePayment.PaymentSystem = PaymentSystem.Robokassa;

            return _preparePaymentModelFactory.PreparePaymentModel<PrepareRobokassModel, string>(preparePayment);
        }

        /// <summary>
        ///     Проверяем валидность подписи для Paybox
        /// </summary>
        /// <param name="generatedSign">Сгенерированная подпись</param>
        /// <param name="inputSign">Входящая подпись из запроса</param>
        /// <returns>Валидна или не валидна</returns>
        public bool IsPayboxSignatureValid(string generatedSign, string inputSign)
        {
            return new PayboxCryptoProvider().IsSignatureValid(generatedSign, inputSign);
        }

        /// <summary>
        ///     Проверка статуса платежа
        /// </summary>
        /// <param name="paymentNumber">Номер платежа</param>
        /// <param name="description">Описание (если ошибка)</param>
        /// <returns>Строка - сообщение</returns>
        public string CheckPayboxPaymentStatus(int paymentNumber, string description)
        {
            return new PayboxPaymentStatusProvider(DbLayer, _logger).GetPaymentStatus(paymentNumber, description);
        }

        /// <summary>
        /// Метод для Апи, получение информации о ПС
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="sum"></param>
        /// <returns></returns>
        public ManagerResult<PaymentSystemDataModel> GetPaymentSystem(Guid accountId, decimal sum)
        {
            AccessProvider.HasAccess(ObjectAction.Payments_System);

            var locale = DbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(_configuration["DefaultLocale"] ?? ""));
            if (locale == null)
                return NotFound<PaymentSystemDataModel>("Account not found");
            var model = new PaymentSystemDataModel();

            var getModel = GetPaymentModel(accountId, sum, locale.Name);

            switch (locale.Name)
            {
                case LocaleConst.Ukraine:
                    model.PaymentSystem = PaymentSystem.UkrPays;
                    model.Amount = getModel.ModelForUkrPays.Amount;
                    model.Encoding = getModel.ModelForUkrPays.Encoding;
                    model.Order = getModel.ModelForUkrPays.Order;
                    model.RequestUrl = getModel.ModelForUkrPays.RequestUrl;
                    model.ServiceId = getModel.ModelForUkrPays.ServiceId;
                    model.Successurl = getModel.ModelForUkrPays.Successurl;
                    return Ok(model);
                case LocaleConst.Kazakhstan:
                case LocaleConst.Uzbekistan:
                    model.PaymentSystem = PaymentSystem.Paybox;
                    model.PayboxUrl = getModel.OtherPaymentModels;
                    return Ok(model);
            }

            model.PaymentSystem = PaymentSystem.Robokassa;
            model.RobokassaUrl = getModel.OtherPaymentModels;
            return Ok(model);
        }

        /// <summary>
        /// Создаёт номер счёта из префикса, хранящегося в файле App.config под элементом с названием Invoice_UniqPrefix, и
        /// значения uniq, путём конкатенации строковых представлений двух величин.
        /// </summary>
        /// <param name="uniq">Уникальное значение, к которому слева будет добавлен префикс</param>
        /// <returns></returns>
        public string ConstructInvoiceNumberFromUniq(long uniq)
        {
            return string.Concat(_invoiceNumberPrefix, uniq);
        }

        /// <summary>
        /// Отменить счет на оплату
        /// </summary>
        /// <param name="invoiceNumber">Номер счета</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>true - если счет отменен</returns>
        public ManagerResult CancelInvoice(string invoiceNumber, Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.Invoices_Confirmation);

            if (invoiceNumber.Length > 2 && invoiceNumber[..2] == _invoiceNumberPrefix)
                invoiceNumber = invoiceNumber[_invoiceNumberPrefix.Length..];

            if (!long.TryParse(invoiceNumber, out var uniq))
                return PreconditionFailedError("Invoice Number not valid");

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(x => x.Uniq == uniq && x.AccountId == accountId);

            if (invoice == null)
            {
                return NotFound($"Invoice {invoiceNumber} not found");
            }

            if (invoice.State == InvoiceStatus.Processing.ToString() ||
                invoice.State == InvoiceStatus.Canceled.ToString())
            {
                return Ok();
            }

            if (invoice.State != InvoiceStatus.Processed.ToString())
                return PreconditionFailed($"State in Invoice isn't correct. Actual state is - {invoice.State}");

            using var dbTransaction = DbLayer.SmartTransaction.Get();
            try
            {
                invoice.State = InvoiceStatus.Processing.ToString();
                invoice.ActID = null;

                if (invoice.PaymentID == null)
                {
                    return PreconditionFailed("PaymentId is null");
                }

                var payment = DbLayer.PaymentRepository.FindPayment(invoice.PaymentID.Value);

                if (payment == null)
                {
                    return PreconditionFailed($"Payment {invoice.PaymentID} is null");
                }

                if (payment.Sum != invoice.Sum || payment.AccountId != invoice.AccountId)
                {
                    return PreconditionFailed(
                        $"Sum {payment.Sum} & AccId {payment.AccountId} in Payment and Sum {invoice.Sum} & AccId {invoice.AccountId} in Invoice are different");
                }

                if (DateTime.Now > payment.Date.AddDays(30))
                {
                    return PreconditionFailed("You cannot cancel a payment made more than 30 days ago");
                }

                payment.Status = PaymentStatus.Canceled.ToString();
                payment.Description += $" (Отменен {DateTime.Now.Date.ToString("dd MMMM yyyy", CultureInfo.GetCultureInfo("ru-RU"))}";
                DbLayer.InvoiceRepository.Update(invoice);
                DbLayer.PaymentRepository.Update(payment);
                DbLayer.Save();

                RecalculateAccountTotals(invoice);
                dbTransaction.Commit();

                var localeCurrency = payment.Account.AccountConfiguration.Locale.Currency;
                LogEventHelper.LogEvent(DbLayer, invoice.AccountId, AccessProvider, LogActions.RefillByCorp,
                    $"Отмена счета № {Configurations.Configurations.CloudConfigurationProvider.Invoices.UniqPrefix() + invoice.Uniq} на сумму {invoice.Sum:0} {localeCurrency}.");

                return Ok();
            }
            catch (Exception e)
            {
                dbTransaction.Rollback();

                return PreconditionFailed(e.Message);
            }
        }

        /// <summary>
        /// Получить сумму платежей в разрере платежных систем
        /// </summary>
        /// <param name="dateFrom">Начальный период</param>
        /// <param name="dateTo">Конечный период</param>
        /// <param name="accId">ID акта</param>
        /// <returns>Сумма платежей в разрере платежных систем</returns>
        public ManagerResult<PaymentSystemsMoney> GetPaymentSystemsMoneyAmount(DateTime dateFrom, DateTime dateTo,
            Guid? accId = null)
        {
            AccessProvider.HasAccess(ObjectAction.Payments_Amounts);

            var totalPayments = DbLayer.PaymentRepository
                .AsQueryableNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Locale)
                .Where(x => x.Date >= dateFrom &&
                            x.Date <= dateTo &&
                            x.Status == PaymentStatus.Done.ToString() &&
                            x.OperationType == PaymentType.Inflow.ToString() &&
                            (!accId.HasValue || x.AccountId == accId.Value))
                .ToList();

            return Ok(new PaymentSystemsMoney
            {
                RobokassaMoneyAmount = totalPayments.Where(x => x.PaymentSystem == PaymentSystem.Robokassa.ToString()).Sum(x => x.Sum),
                UkrPaysMoneyAmount = totalPayments.Where(x => x.PaymentSystem == PaymentSystem.UkrPays.ToString()).Sum(x => x.Sum),
                CorpUAMoneyAmount = totalPayments.Where(x => x.PaymentSystem == PaymentSystem.Corp.ToString() && x.Account.AccountConfiguration.Locale.Name == LocaleConst.Ukraine).Sum(x => x.Sum),
                CorpRUMoneyAmount = totalPayments.Where(x => x.PaymentSystem == PaymentSystem.Corp.ToString() && x.Account.AccountConfiguration.Locale.Name == LocaleConst.Russia).Sum(x => x.Sum)
            });
        }

        /// <summary>
        /// Пересчет баланса компании. Вызывается после каждой платежной операции.
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        private void RecalculateAccountTotals(Domain.DataModels.billing.Invoice invoice)
        {
            AccessProvider.HasAccess(ObjectAction.Payments_Edit, () => invoice.AccountId);
            _billingPaymentsDataProvider.RecalculateAccountTotals(invoice.AccountId);

            _bonusPaymentProvider.CreateOutflowBonusPayment(invoice);
        }

        /// <summary>
        /// Получить урл для оплаты через агрегатор ЮKassa
        /// </summary>
        /// <param name="totalSum">Сумма к оплате</param>
        /// <returns>Урл для оплаты через агрегатор ЮKassa</returns>
        private string GetUrlForPayViaYookassa(decimal totalSum) =>
            new Uri($"{_cpSite.Value}/{_routeForPayViaYookassa.Value}{totalSum}")
                .AbsoluteUri;
    }
}
