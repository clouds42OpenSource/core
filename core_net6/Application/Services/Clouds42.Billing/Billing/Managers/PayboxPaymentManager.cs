﻿using Clouds42.Billing.Billing.Models;
using Clouds42.Billing.Billing.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Managers
{
    public class PayboxPaymentManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IImportPayboxPaymentProvider importPayboxPaymentProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        public ManagerResult<string> PayboxImportResultAnonymous(PayboxImportResultModel resultModel)
        {
            try
            {

                return Ok(importPayboxPaymentProvider.PayboxImportResult(resultModel));

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"Ошибка регистрации запроса Paybox. Номер патежа '{resultModel.pg_order_id}'");

                return PreconditionFailed<string>(ex.Message);
            }

        }

    }
}
