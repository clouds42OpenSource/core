﻿using Clouds42.Domain.DataModels;

namespace Clouds42.Billing.Billing.Managers;

public partial class PaymentsManager
{
    /// <summary>
    /// Модель готовности к оплате
    /// </summary>
    public class ReadyForPay
    {
        /// <summary>
        /// Аккаунт
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Номер платежа
        /// </summary>
        public int PaymentNumber { get; set; }
    }
}
