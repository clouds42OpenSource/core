﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Billing.Billing.ModelProcessors;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.Billing.Invoice.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Managers
{
    /// <summary>
    /// Менеджер для работы с аккаунтом биллинга
    /// </summary>
    public class BillingAccountManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        BillingDataDomainModelProcessor billingDataDomainModelProcessor,
        IInvoiceProvider invoiceProvider,
        IInvoiceDocumentBuilder razorBuilder,
        IInvoiceNotificationHelper invoiceNotificationHelper,
        IHandlerException handlerException,
        IPromisePaymentProvider promisePaymentProvider,
        IPromisedPaymentProcessor promisedPaymentProcessor,
        IBillingPaymentsDataProvider billingPaymentsDataProvider,
        IInvoiceDataProvider invoiceDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        PaymentEmailProvider paymentEmailProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IBillingAccountManager
    {

        /// <summary>
        /// Получить данные биллинга для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные биллинга для аккаунта</returns>
        public ManagerResult<BillingAccountInfoDto> GetBillingData(Guid accountId)
        {
            try
            {
                var message = $"Получение данных биллинга для аккаунта {accountId}";
                Logger.Info(message);

                AccessProvider.HasAccess(ObjectAction.Payments_View, () => accountId);
                return Ok(billingDataDomainModelProcessor.GetBillingAccountInfo(accountId));
            }
            catch (Exception ex)
            {
                var message = "[Получение данных биллинга для аккаунта завершилось ошибкой]";
                handlerException.Handle(ex, message, () => accountId);

                return PreconditionFailed<BillingAccountInfoDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные биллинга для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные биллинга для аккаунта</returns>
        public ManagerResult<BillingDataDto> GetBillingDataV2(Guid accountId)
        {
            try
            {
                var message = $"[Получение данных биллинга для аккаунта] {accountId}";
                Logger.Info(message);

                AccessProvider.HasAccess(ObjectAction.Payments_View, () => accountId);
                return Ok(billingDataDomainModelProcessor.GetBillingDataForAccount(accountId));
            }
            catch (Exception ex)
            {
                var message = "[Получение данных биллинга для аккаунта завершилось ошибкой]";
                handlerException.Handle(ex, message, () => accountId);

                return PreconditionFailed<BillingDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список счетов на оплату аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Список счетов на оплату аккаунта</returns>
        public ManagerResult<PagitationCollection<InvoiceInfoDto>> GetInvoicesForAccount(Guid accountId, int page)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_View, () => accountId);
                return Ok(billingDataDomainModelProcessor.GetInvoicesForAccount(accountId, page));
            }
            catch (Exception ex)
            {
                var message = "[Получение списка счетов на оплату аккаунта завершилось ошибкой.]";
                handlerException.Handle(ex, message, () => accountId, () => page);

                return PreconditionFailed<PagitationCollection<InvoiceInfoDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список счетов на оплату аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="request">Детали фильтрации и пагинации</param>
        /// <returns>Список счетов на оплату аккаунта</returns>
        public ManagerResult<SelectDataResultCommonDto<InvoiceInfoDto>> GetPaginatedInvoicesForAccount(Guid accountId,
            SelectDataCommonDto<InvoicesFilterDto> request)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_View, () => accountId);
                return Ok(billingDataDomainModelProcessor.GetPaginatedInvoicesForAccount(accountId, request));
            }
            catch (Exception ex)
            {
                var message = "[Получение списка счетов на оплату аккаунта завершилось ошибкой.]";
                handlerException.Handle(ex, message, () => accountId, () => request.PageNumber ?? 1);

                return PreconditionFailed<SelectDataResultCommonDto<InvoiceInfoDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Создать счёт для аккаунта на указанную сумму
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="sum">Сумма, на которую будет выставлен счёт</param>
        /// <param name="period">Период</param>
        /// <returns>Идентификатор созданного счёта</returns>
        public ManagerResult<Guid> CreateInvoiceForSpecifiedInvoiceAmount(Guid accountId, decimal sum,
            int? period = null)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_Bill, () => accountId);
                var newInvoice = invoiceProvider.CreateInvoiceForSpecifiedInvoiceAmount(accountId, sum, period);
                return Ok(newInvoice.Id);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Ошибка создания счёта для аккаунта на произвольную сумму] {sum}";
                handlerException.Handle(ex, $"{message}");
                LogEvent(() => accountId, LogActions.AddInvoice, $"{message} Описание ошибки: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Создать счет на основании модели калькуляции счета
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="calculationOfInvoiceRequest">Модель запроса на калькуляцию счета</param>
        /// <returns>Идентификатор созданного счёта</returns>
        public ManagerResult<Guid> CreateInvoiceBasedOnCalculationInvoiceModel(Guid accountId,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequest)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_Bill, () => accountId);

                var newInvoice =
                    invoiceProvider.CreateInvoiceBasedOnCalculationInvoiceModel(accountId,
                        calculationOfInvoiceRequest);

                var invoiceDocument = razorBuilder.Build(GetInvoiceDc(newInvoice.Id).Result);
                var productsInfo = InvoiceAdditionalDataHelper.GetNomenclatureWithProductsDescription(newInvoice,
                    accountConfigurationDataProvider.GetAccountLocale(accountId).Name);

                invoiceNotificationHelper.CreateInvoiceNotify(newInvoice.Id, accountId, productsInfo,
                    invoiceDocument.Bytes, calculationOfInvoiceRequest);

                return Ok(newInvoice.Id);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка выставления счета на оплату аккаунту '{accountId}'.]");
                LogEvent(() => accountId, LogActions.AddInvoice,
                    $"Счет на оплату не выставлен. Описание ошибки: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель DC счета
        /// </summary>
        /// <param name="id">Id счета</param>
        /// <returns>Модель DC счета</returns>
        public ManagerResult<InvoiceDc> GetInvoiceDc(Guid id)
        {
            try
            {
                var invoice = DbLayer.InvoiceRepository.GetInvoiceById(id);
                if (invoice == null)
                    throw new NotFoundException($"Не удалось получить модель Dc счета по Id {id}");

                var message =
                    $"Получение модели Dc счета по Id {id}, для аккаунта {invoice.AccountId}";
                Logger.Info(message);

                AccessProvider.HasAccess(ObjectAction.Payments_View, () => invoice.AccountId);
                var invoiceDc = invoiceDataProvider.GetInvoiceData(invoice);
                return Ok(invoiceDc);
            }
            catch (Exception ex)
            {
                var message =
                    $"Получение модели Dc счета по Id {id} завершилось ошибкой.";
                Logger.Warn($"{message} Exception: {ex.GetFullInfo()}");
                return PreconditionFailed<InvoiceDc>(ex.Message);
            }
        }

        /// <summary>
        /// Создать обещанный платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель запроса на калькуляцию счета</param>
        /// <returns>Успешность создания</returns>
        public ManagerResult<bool> CreatePromisePayment(Guid accountId, CalculationOfInvoiceRequestModel model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.PromisePayment_CreatePromisePayment, () => accountId);
                var createPromisePaymentResult = promisePaymentProvider.CreatePromisePayment(accountId, model);

                if (!createPromisePaymentResult.Complete)
                    return Ok(false,
                        $"Ошибка взятия обещанного платежа на сумму {createPromisePaymentResult.PromisePaymentSum:0} {createPromisePaymentResult.Locale.Currency}.");

                return Ok(createPromisePaymentResult.Complete);
            }
            catch (Exception ex)
            {
                Logger.Warn("[Ошибка взятия обещанного платежа.]", () => accountId, () => model);
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Получить системный сервис биллинга
        /// </summary>
        /// <param name="clouds42Service">Тип системного сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Системный сервис биллинга</returns>
        public ManagerResult<IBillingService> GetSystemBillingService(Clouds42Service clouds42Service, Guid accountId)
        {
            try
            {
                var message = $"Получение системного сервиса биллинга {clouds42Service.Description()}";
                Logger.Info(message);

                AccessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => accountId);
                var service = billingDataDomainModelProcessor.GetSystemBillingService(clouds42Service);
                return Ok(service);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Получение системного сервиса биллинга] {clouds42Service.Description()} завершилось ошибкой.");
                return PreconditionFailed<IBillingService>(ex.Message);
            }
        }

        /// <summary>
        /// Преждeвременное погашение обещанного платежа
        /// </summary>
        /// <param name="billingAccount"></param>
        /// <returns></returns>
        public ManagerResult<bool> PromisePaymentEarlyRepay(BillingAccount billingAccount)
        {
            AccessProvider.HasAccess(ObjectAction.PromisePayment_RepayPromisePayment, () => billingAccount.Account.Id);

            try
            {
                return Ok(promisedPaymentProcessor.EarlyRepay(billingAccount));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка преждeвременного погашения обещанного платежа]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Продлить обещанный платёж
        /// </summary>
        /// <param name="billingAccount"></param>
        /// <returns></returns>
        public ManagerResult<bool> ProlongPromisePayment(BillingAccount billingAccount)
        {
            AccessProvider.HasAccess(ObjectAction.PromisePayment_Prolong, () => billingAccount.Account.Id);

            try
            {
                var res = promisedPaymentProcessor.ProlongPromisePayment(billingAccount);
                if (res)
                {
                    LogEventHelper.LogEvent(DbLayer, billingAccount.Id, AccessProvider, LogActions.ProlongationService,
                        "Обещанный платёж продлён на 1 день");
                }
                else
                {
                    LogEventHelper.LogEvent(DbLayer, billingAccount.Id, AccessProvider, LogActions.ProlongationService,
                        "Ошибка продления обещанного платежа на 1 день");
                }

                return Ok(res);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка продления обещанного платежа на 1 день]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Построить pdf документ счета на оплату
        /// </summary>
        /// <param name="invoiceId">ID счета на оплату</param>
        /// <returns>pdf документ счета на оплату</returns>
        public ManagerResult<DocumentBuilderResult> BuildInvoicePdfDocument(Guid invoiceId)
        {
            try
            {
                var invoice = DbLayer.InvoiceRepository.GetInvoiceById(invoiceId) 
                    ?? throw new InvalidOperationException($"Инвойс по id {invoiceId} не найден");

                var invoiceDc = invoiceDataProvider.GetInvoiceData(invoice);

                return Ok(razorBuilder.Build(invoiceDc));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка построения документа]");
                return PreconditionFailed<DocumentBuilderResult>(ex.Message);
            }
        }

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Баланс аккаунта</returns>
        public ManagerResult<decimal> GetAccountBalance(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingAccount_GetBalance, () => accountId);
                var balance = billingPaymentsDataProvider.GetBalance(accountId);
                return Ok(balance);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения баланса аккаунта {accountId}]");
                return PreconditionFailed<decimal>(ex.Message);
            }
        }

        /// <summary>
        /// Отправка письма в случае успешной оплаты
        /// </summary>
        /// <param name="paymenNumber">Номер платежа</param>
        /// <returns>Результат операции</returns>
        public ManagerResult SendRobokassaSuccessImportReport(int paymenNumber)
        {
            try
            {
                paymentEmailProvider.SendSuccessImportReport(paymenNumber);
                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Отправка письма в случае успешной оплаты через Robokassa]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Отменить счет на оплату
        /// </summary>
        /// <param name="invoiceId">Номер счета</param>
        /// <returns>true - если счет отменен</returns>
        public ManagerResult CancelInvoice(Guid invoiceId)
        {
            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(x => x.Id == invoiceId);

            if (invoice == null)
            {
                return NotFound($"Invoice {invoiceId} not found");
            }

            if (invoice.State == InvoiceStatus.Processed.ToString() ||
                invoice.State == InvoiceStatus.Canceled.ToString()
            )
            {
                return PreconditionFailed($"Счет в статусе {invoice.State} удалить нельзя");
            }

            if (invoice.State != InvoiceStatus.Processing.ToString())
                return PreconditionFailed($"State in Invoice isn't correct. Actual state is - {invoice.State}");

            try
            {
                invoice.State = InvoiceStatus.Canceled.ToString();

                if (invoice.PaymentID != null)
                {
                    return PreconditionFailed($"По счету {invoice.Uniq} есть платеж");
                }

                if (invoice.ActID != null)
                {
                    return PreconditionFailed($"К счету {invoice.Uniq} прикреплен акт {invoice.ActID}");
                }


                DbLayer.InvoiceRepository.Update(invoice);
                DbLayer.Save();

                var localeCurrency = invoice.Account.AccountConfiguration.Locale.Currency;
                LogEventHelper.LogEvent(DbLayer, invoice.AccountId, AccessProvider, LogActions.CancelIvoice,
                    $"Отмена счета № {Configurations.Configurations.CloudConfigurationProvider.Invoices.UniqPrefix() + invoice.Uniq} на сумму {invoice.Sum:0} {localeCurrency}.");
                return Ok();
            }
            catch (Exception e)
            {
                return PreconditionFailed(e.Message);
            }
        }
    }
}
