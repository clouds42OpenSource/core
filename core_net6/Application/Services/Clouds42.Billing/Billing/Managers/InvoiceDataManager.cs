﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Managers
{
    public class InvoiceDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IGetOrCreateInvoiceDataProvider getOrCreateInvoiceDataProvider,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить счет на оплату или создать по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="invoiceNumber">Номер счета.</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        public async Task<ManagerResult<InvoicePaymentsDto>> GetProcessingOrCreateNewInvoiceAsync(string invoiceNumber,
            decimal sum)
        {
            AccessProvider.HasAccess(ObjectAction.Invoices_View);

            try
            {
                var invoice =
                    await getOrCreateInvoiceDataProvider.GetProcessingOrCreateNewInvoiceAsync(invoiceNumber, sum);

                if (invoice == null)
                    return PreconditionFailed<InvoicePaymentsDto>(
                        $"По номеру '{invoiceNumber}' и сумме '{sum}' не найден счет на оплату.");

                return Ok(invoice);
            }
            catch (Exception ex)
            {
                handlerException.HandleWarning(ex.Message,
                    $"Ошибка получения счета на оплату по номеру {invoiceNumber} на сумму {sum}");
                return PreconditionFailed<InvoicePaymentsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить необработанный инвойс на заданную сумму.
        /// </summary>
        /// <param name="sum">Сумма счёта.</param>
        /// <param name="inn">ИНН аккаунта.</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        public async Task<ManagerResult<InvoicePaymentsDto>> GetProcessingInvoice(string inn, decimal sum)
        {
            AccessProvider.HasAccess(ObjectAction.Invoices_View);

            var innPhrase = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Inn, accessProvider.ContextAccountId);

            try
            {
                var invoice = await getOrCreateInvoiceDataProvider.GetProcessingInvoice(inn, sum);

                if (invoice == null)
                    return PreconditionFailed<InvoicePaymentsDto>($"Для аккаунта '{innPhrase}={inn}' не найден счет на сумму '{sum}'.");

                return Ok(invoice);
            }
            catch (Exception ex)
            {
                handlerException.HandleWarning(ex.Message,
                    $"Ошибка получения необработанного инвойса на сумму {sum} для компании {innPhrase} {inn}");

                return PreconditionFailed<InvoicePaymentsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить или создать счёт по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="forceCreate">Флаг принудительного создания счёта</param>
        /// <param name="invoiceNumber">Номер инвойса.</param>
        /// <param name="inn">ИНН аккаунта для которого будет сформирован или получен счёт</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        public async Task<ManagerResult<InvoicePaymentsDto>> GetOrCreateInvoiceAsync(string invoiceNumber, bool forceCreate,
            string inn, decimal sum)
        {
            AccessProvider.HasAccess(ObjectAction.Invoices_View);

            try
            {
                var invoice =
                    await getOrCreateInvoiceDataProvider.GetOrCreateInvoiceAsync(invoiceNumber, forceCreate, inn, sum);

                if (invoice == null)
                    return PreconditionFailed<InvoicePaymentsDto>("Не найден счет на оплату.");

                return Ok(invoice);
            }
            catch (Exception ex)
            {
                handlerException.HandleWarning(ex.Message,
                    $"Ошибка метода GetOrCreateInvoiceAsync параметры invoiceNumber='{invoiceNumber}' forceCreate='{forceCreate}' inn='{inn}' sum='{sum}'");
                return PreconditionFailed<InvoicePaymentsDto>(ex.Message);
            }
        }
    }
}
