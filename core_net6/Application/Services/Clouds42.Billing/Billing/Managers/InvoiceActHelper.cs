﻿using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Managers
{
    /// <summary>
    /// Хелпер для взаимодействия акта со счетом на оплату
    /// </summary>
    public class InvoiceActHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Проверить, нужно ли логировать прикрепление акта по счету на оплату
        /// </summary>
        /// <param name="invoiceId">Id счета на оплату</param>
        /// <param name="actId">Id акта</param>
        /// <param name="actDescription">Описание акта</param>
        /// <returns>Прризнак, что нужно логировать прикрепление акта по счету</returns>
        public bool NeedToLogSettingActId(Guid invoiceId, Guid? actId, string actDescription) =>
            dbLayer.InvoiceRepository.FirstOrDefault(invoice =>
                invoice.Id == invoiceId && invoice.ActID == actId && invoice.ActDescription == actDescription) == null;

        /// <summary>
        /// Получить описание логирования для операции с актом
        /// </summary>
        /// <param name="invoiceNumber">Номер счета</param>
        /// <param name="newActNumber">Новый номер акта</param>
        /// <param name="newActId">ID нового акта</param>
        /// <param name="currentActNumber">Текущий номер акта</param>
        /// <returns>Описание логирования для операции с актом</returns>
        public string GetActOperationLogEventDescription(long invoiceNumber, string newActNumber, Guid? newActId, string currentActNumber)
        {
            var operation = newActId == null
                ? "Откреплен"
                : "Прикреплен";

            var actNumber = !string.IsNullOrEmpty(newActNumber) && int.TryParse(newActNumber, out _)
                ? newActNumber
                : currentActNumber;

            return $"{operation} акт №{actNumber} на основании счета на оплату №{Configurations.Configurations.CloudConfigurationProvider.Invoices.UniqPrefix()}{invoiceNumber}";
        }
    }
}
