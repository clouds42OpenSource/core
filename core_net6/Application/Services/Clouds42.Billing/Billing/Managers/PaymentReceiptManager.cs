﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Interfaces;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Billing.Billing.Managers
{
    public class PaymentReceiptManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IInvoiceReceiptProvider receiptProvider,
        IInvoiceReceiptBuilderFactory invoiceReceiptBuilderFactory,
        InvoiceReceiptTaskProvider invoiceReceiptTaskProvider,
        IInvoiceDcProcessor invoiceDcProcessor,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
      
        /// <summary>
        ///     Создать задачу на получение фискального чека
        /// </summary>
        /// <param name="paymentNumber">Номер платежа</param>
        public ManagerResult<bool> CreateFiscalReceiptTask(int paymentNumber)
        {
            try
            {
                var invoice = DbLayer.PaymentRepository
                    .WhereLazy(x => x.Number == paymentNumber)
                    .Include(w => w.Invoices)
                    .SelectMany(w => w.Invoices)
                    .FirstOrDefault();

                if (invoice == null)
                    throw new InvalidOperationException("Не найден счет на оплату по номеру платежа" + paymentNumber);

                AccessProvider.HasAccess(ObjectAction.Invoices_CreateFiscalReceipt, () => invoice.AccountId);

                invoiceReceiptTaskProvider.CreateFiscalReceiptTask(invoice.Id);
                return Ok(true);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при создании фискального чека]  paymentNumber {paymentNumber}");
                return PreconditionFailed<bool>("Ошибка при создании фискального чека: " + ex.GetFullInfo());
            }
        }

        /// <summary>
        ///     Создать фискальный чек
        /// </summary>
        /// <param name="invoiceId">ID счета на оплату</param>
        /// <returns></returns>
        public ManagerResult<bool> CreateFiscalReceipt(Guid invoiceId)
        {
            try
            {
                var invoice = DbLayer.InvoiceRepository.GetInvoiceById(invoiceId) 
                              ?? throw new NotFoundException($"Не найден счет на оплату по id {invoiceId}");
                
                AccessProvider.HasAccess(ObjectAction.Invoices_CreateFiscalReceipt, () => invoice.AccountId);

                receiptProvider.CreateFiscalReceiptByInvoice(invoice);
                return Ok(true);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при создании фискального чека]: invoiceId {invoiceId} ");
                return PreconditionFailed<bool>("Ошибка при создании фискального чека: " + ex.GetFullInfo());
            }
        }

        /// <summary>
        ///     Получить фискальный чек в PDF 
        /// </summary>
        /// <param name="invoiceId">ID счета на оплату</param>
        public IDocumentBuilderResultDto BuildInvoiceReceiptPdfDocument(Guid invoiceId)
        {
            var invoiceReceiptDc = invoiceDcProcessor.GetReceiptModel(invoiceId);

            return invoiceReceiptBuilderFactory.Build(invoiceReceiptDc);
        }
    }
}
