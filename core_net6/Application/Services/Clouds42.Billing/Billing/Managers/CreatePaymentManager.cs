﻿using Clouds42.Billing.Billing.ModelProcessors;
using Clouds42.Billing.Billing.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.Billing.Managers
{
    public class CreatePaymentManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateInflowPaymentHelper createInflowPaymentHelper,
        IHandlerException handlerException,
        BillingDataDomainModelProcessor billingDataDomainModelProcessor,
        IConfiguration configuration)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Добавить платеж
        /// </summary>
        /// <param name="newPayment">Модель платежа</param>
        /// <returns>Результат добавления</returns>
        public ManagerResult<PaymentOperationResult> AddPayment(PaymentDefinitionDto newPayment)
        {
            AccessProvider.HasAccess(ObjectAction.Payments_Add, () => newPayment.Account);

            try
            {
                SetRemainingAmount(newPayment);
                var paymentResult = createInflowPaymentHelper.CreatePaymentAndProcessInflowPayment(newPayment);

                var message = newPayment.OperationType == PaymentType.Inflow ? "Добавление" : "Списание";
                var transactionTypeDescription = newPayment.TransactionType == TransactionType.Bonus
                    ? "бонусных "
                    : string.Empty;
                LogEvent(() => newPayment.Account, LogActions.ManualBalanceEdit,
                    $"{message} {transactionTypeDescription}средств на сумму {newPayment.Total}" +
                    $" {DbLayer.AccountsRepository.GetLocale(newPayment.Account, Guid.Parse(configuration["DefaultLocale"] ?? "")).Currency}.");
                return Ok(paymentResult);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка создания платежа.]");
                return PreconditionFailed<PaymentOperationResult>(ex.Message);
            }
            
        }
        
        
        /// <summary>
        /// Добавить остаточную сумму после транзакции
        /// </summary>
        /// <param name="newPayment">Модель платежа</param>
        /// <returns>Результат добавления</returns>
        private void SetRemainingAmount(PaymentDefinitionDto newPayment )
        {
          var billingData =  billingDataDomainModelProcessor.GetBillingDataForAccount(newPayment.Account);
          decimal totalAmount = billingData.CurrentBalance + billingData.CurrentBonusBalance;
          newPayment.Remains = newPayment.OperationType == PaymentType.Inflow ? totalAmount + newPayment.Total : totalAmount - newPayment.Total;
        }

    }
}
