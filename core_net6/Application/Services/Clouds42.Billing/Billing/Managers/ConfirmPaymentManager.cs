﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Managers
{
    public class ConfirmPaymentManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IConfirmPaymentProvider confirmPaymentProvider,
        IHandlerException handlerException,
        ILogger42 logger42)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Анонимный метод подтверждения платежа
        /// </summary>
        /// <param name="paymentNumber">Номер платежа</param>
        /// <param name="sum">Сумма платежа</param>
        /// <param name="accepted">Принятый</param>
        /// <returns>Результат платежной операции</returns>
        public ManagerResult<PaymentOperationResult> ConfirmPaymentAnonymous(int paymentNumber, decimal sum, bool accepted)
        {
            try
            {
                var paymentOperationResult = confirmPaymentProvider.ConfirmPayment(paymentNumber, sum, accepted);

                return paymentOperationResult == PaymentOperationResult.Ok 
                    ? Ok(paymentOperationResult) 
                    : throw new InvalidOperationException($"Ошибка подтверждения платежа {paymentNumber} на базе. {paymentOperationResult}");
            }
            catch (Exception ex)
            {
                logger42.Error($"Подтверждение платежа {paymentNumber}  завершилось ошибкой {ex.Message}.");
                handlerException.Handle(ex, $"[Подтверждение платежа {paymentNumber}  завершилось ошибкой.]");
                return PreconditionFailed<PaymentOperationResult>(ex.Message);
            }
        }

        /// <summary>
        /// Метод подтверждения платежа
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        /// <param name="paymentNumber">Номер платежа</param>
        /// <param name="sum">Сумма платежа</param>
        /// <param name="accepted">Принятый</param>
        /// <returns>Результат платежной операции</returns>
        public ManagerResult<PaymentOperationResult> ConfirmPayment(Guid accountId, int paymentNumber, decimal sum, bool accepted)
        {
            AccessProvider.HasAccess(ObjectAction.Payments_ConfirmPayment, () => accountId);
            return ConfirmPaymentAnonymous(paymentNumber, sum, accepted);
        }

    }
}
