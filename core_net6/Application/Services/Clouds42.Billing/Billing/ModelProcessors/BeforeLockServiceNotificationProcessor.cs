﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Invoice;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.ModelProcessors
{
    /// <summary>
    /// Процессор нотификаций аккаунта о блокировки сервиса
    /// </summary>
    internal class BeforeLockServiceNotificationProcessor(
        IInvoiceProvider invoiceProvider,
        BillingAccountManager billingAccountManager,
        IInvoiceDocumentBuilder invoiceBuilderFactory,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        IUnitOfWork dbLayer)
        : IBeforeLockServiceNotificateProcessor
    {
        private readonly Lazy<string> _invoiceAttachFileName = new(CloudConfigurationProvider.Invoices.GetInvoiceAttachFileName);

        /// <summary>
        /// Отпарвить уведомление о блокировке сервиса
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="reasonMsg">Причина блокировки</param>
        /// <param name="needSendInvoice">Необходимость отправлять счет</param>
        public void Notify(ResourcesConfiguration resourcesConfiguration, string? reasonMsg,
            bool needSendInvoice) => letterNotificationProcessor
            .TryNotify<LockServiceLetterNotification, LockServiceLetterModelDto>(
                new LockServiceLetterModelDto
                {
                    AccountId = resourcesConfiguration.AccountId,
                    ServiceNames = resourceConfigurationDataProvider.GetServiceNames(resourcesConfiguration),
                    LockReason = reasonMsg,
                    InvoiceDocument = needSendInvoice ? GetInvoiceData(resourcesConfiguration) : null,
                    EmailsToCopy = dbLayer.AccountsRepository.GetEmails(resourcesConfiguration.AccountId)
                });

        /// <summary>
        /// Получить данные счета
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <returns>Данные счета</returns>
        private DocumentDataDto GetInvoiceData(ResourcesConfiguration resourcesConfiguration)
        {
            var invoice = invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(
                resourcesConfiguration, resourcesConfiguration.BillingAccounts);

            var invoiceDc = billingAccountManager.GetInvoiceDc(invoice.Id).Result;
            var invoicePdf = invoiceBuilderFactory.Build(invoiceDc);

            return new DocumentDataDto
            {
                Bytes = invoicePdf.Bytes,
                FileName = _invoiceAttachFileName.Value
            };
        }
    }
}
