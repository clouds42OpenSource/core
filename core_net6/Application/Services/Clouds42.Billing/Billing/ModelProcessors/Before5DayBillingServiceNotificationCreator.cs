﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts.Helpers;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Invoice;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.ModelProcessors
{
    /// <summary>
    /// Креатор уведомлений о скорой блокировки сервиса 
    /// </summary>
    public class Before5DayBillingServiceNotificationCreator(
        IInvoiceProvider invoiceProvider,
        IUnitOfWork unitOfWork,
        BillingAccountManager billingAccountManager,
        IInvoiceDocumentBuilder razorBuilder,
        ICloudLocalizer cloudLocalizer,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
    {
        /// <summary>
        /// Создать уведомление
        /// </summary>
        public void CreateNotification(ResourcesConfiguration resourcesConfiguration)
        {
            var invoice = invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(resourcesConfiguration,
                resourcesConfiguration.BillingAccounts);

            var invoiceDc = billingAccountManager.GetInvoiceDc(invoice.Id).Result;
            var invoicePdf = razorBuilder.Build(invoiceDc);

            var siteAuthorityUrl =
                localesConfigurationDataProvider.GetCpSiteUrlForAccount(resourcesConfiguration.AccountId);

            letterNotificationProcessor.TryNotify<BeforeServiceLockLetterNotification, BeforeServiceLockLetterModelDto>(
                PrepareBeforeServiceLockLetterModelHelper.PrepareBeforeServiceLockLetterModel(unitOfWork,resourcesConfiguration,
                    invoiceDc, invoicePdf, siteAuthorityUrl, invoice.Period, cloudLocalizer));
        }
    }
}
