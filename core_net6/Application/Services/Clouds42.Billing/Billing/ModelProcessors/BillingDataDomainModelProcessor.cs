﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.Billing.ModelProcessors
{
    /// <summary>
    /// Процессор для работы с данными биллинга
    /// </summary>
    public class BillingDataDomainModelProcessor(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IBillingPaymentsDataProvider billingPaymentsDataProvider,
        IResourcesService resourcesService,
        IBillingDataProvider billingDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IConfiguration configuration,
        ICheckAccountDataProvider checkAccountDataProvider)
    {
        /// <summary>
        /// Получить информацию об аккаунте биллинга
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информацию об аккаунте биллинга</returns>
        public BillingAccountInfoDto GetBillingAccountInfo(Guid accountId)
        {
            return new BillingAccountInfoDto
            {
                BillingData = GetBillingDataForAccount(accountId),
                Invoices = GetInvoicesForAccount(accountId)
            };
        }

        /// <summary>
        /// Получить список счетов на оплату аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Список счетов на оплату аккаунта</returns>
        public PagitationCollection<InvoiceInfoDto> GetInvoicesForAccount(Guid accountId, int page = 1)
        {
            var invoices = billingDataProvider.GetInvoicesForAccount(accountId, page);

            InvoiceInfoDto? newInvoice;
            if ((newInvoice = invoices.Collection.OrderByDescending(o => o.InvoiceDate)
                    .FirstOrDefault(i => i.InvoiceDate >= DateTime.Now.AddDays(-1))) != null)
            {
                newInvoice.IsNewInvoice = true;
            }

            return invoices;
        }

        /// <summary>
        /// Получить список счетов на оплату аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="request">Детали фильтрации и пагинации</param>
        /// <returns>Список счетов на оплату аккаунта</returns>
        public SelectDataResultCommonDto<InvoiceInfoDto> GetPaginatedInvoicesForAccount(Guid accountId,
            SelectDataCommonDto<InvoicesFilterDto> request)
        {
            var invoices = billingDataProvider.GetPaginatedInvoicesForAccount(accountId, request);
            InvoiceInfoDto? lastInvoice = invoices.Records.MaxBy(o => o.InvoiceDate);
            if (lastInvoice != null)
                lastInvoice.IsNewInvoice = lastInvoice.InvoiceDate >= DateTime.Now.AddDays(-1);

            return invoices;
        }

        /// <summary>
        /// Получить системный сервис биллинга
        /// </summary>
        /// <param name="clouds42Service">Тип системного сервиса</param>
        /// <returns>Системный сервис биллинга</returns>
        public IBillingService GetSystemBillingService(Clouds42Service clouds42Service)
        {
            var service = dbLayer.BillingServiceRepository.FirstOrDefault(w => w.SystemService == clouds42Service);
            return service;
        }

        /// <summary>
        /// Получить данные биллинга для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные биллинга для аккаунта</returns>
        public BillingDataDto GetBillingDataForAccount(Guid accountId)
        {
            var billingData = new BillingDataDto();

            var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(accountId);
            var currentPromisePaymentSum =
                billingPaymentsDataProvider.GetPromisePaymentCost(accountId) ?? decimal.Zero;

            var canEarlyRepayPromisePayment =
                billingAccount.Balance + billingAccount.BonusBalance >= currentPromisePaymentSum &&
                currentPromisePaymentSum != decimal.Zero;

            billingData.Locale = GetLocaleForAccount(accountId);
            billingData.AccountIsVip = accountConfigurationDataProvider.IsVipAccount(accountId);
            billingData.CurrentBalance = billingPaymentsDataProvider.GetBalance(billingAccount.Id);
            billingData.CurrentBonusBalance =
                billingPaymentsDataProvider.GetBalance(billingAccount.Id, TransactionType.Bonus);
            billingData.RegularPayment = billingDataProvider.GetAccountRegularPayment(accountId);
            billingData.CanEarlyPromisePaymentRepay = canEarlyRepayPromisePayment;
            billingData.CanProlongPromisePayment = accessProvider.HasAccessBool(ObjectAction.PromisePayment_Prolong) &&
                                                   CanProlongPromisePayment(accountId);
            billingData.PromisePayment =
                GetPromisePaymentAbailableModelForAccount(accountId, currentPromisePaymentSum,
                    billingData.Locale.Currency);

            billingData.AccountIsDemo = checkAccountDataProvider.CheckAccountIsDemo(accountId);


            return billingData;
        }

        /// <summary>
        /// Получить локаль аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns></returns>
        private LocaleDto GetLocaleForAccount(Guid accountId)
        {
            var locale = dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(configuration["DefaultLocale"] ?? ""));
            return new LocaleDto
            {
                Name = locale.Name,
                Currency = locale.Currency
            };
        }

        /// <summary>
        /// Получить модель доступности обещанного платежа для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="currentPromisePaymentSum">Сумма текущего обещанного платежа</param>
        /// <param name="currency">Валюта</param>
        /// <returns>Модель доступности обещанного платежа</returns>
        private PromisePaymentAvailableDto GetPromisePaymentAbailableModelForAccount(Guid accountId,
            decimal currentPromisePaymentSum, string currency)
        {
            var paymentList = billingPaymentsDataProvider.GetPaymentsList(accountId);
            var promisePaymentAbailableModel = new PromisePaymentAvailableDto();
            var promiseDate = billingPaymentsDataProvider.GetPromiseDateTime(accountId);
            var promisePaymentDays = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");
            var regularPayment  = billingDataProvider.GetAccountRegularPayment(accountId);

            if (!paymentList.Any(x =>
                x.OperationType == PaymentType.Inflow && x is { Total: > 0, Status: PaymentStatus.Done }))
            {
                promisePaymentAbailableModel.CanTakePromisePayment = false;
                promisePaymentAbailableModel.CanIncreasePromisePayment = false;
                promisePaymentAbailableModel.BlockReason =
                    "Услуга Обещанный платеж доступна только пользователям, которые ранее оплачивали сервисы 42Clouds";

                return promisePaymentAbailableModel;
            }

            if (currentPromisePaymentSum > 0)
            {
                promisePaymentAbailableModel.CanTakePromisePayment = false;
                

                if(currentPromisePaymentSum >= (regularPayment * 2) || DateTime.Now >= promiseDate?.AddDays(promisePaymentDays))
                {
                    promisePaymentAbailableModel.CanIncreasePromisePayment = false;
                    promisePaymentAbailableModel.BlockReason =
                    $"Услуга Обещанный платеж не доступна. Просьба погасить задолженность на сумму {currentPromisePaymentSum:0.00} {currency}. до {promiseDate?.AddDays(promisePaymentDays):dd.MM.yy}";
                }
                else
                {
                    promisePaymentAbailableModel.BlockReason =
                    $"Услуга Обещанный платеж активирована, но вы можете увеличить ее сумму до {(regularPayment * 2):0.00} {currency}. Необходимо погасить существующую задолженность на сумму {currentPromisePaymentSum:0.00} {currency}. до {promiseDate?.AddDays(promisePaymentDays):dd.MM.yy}";
                    promisePaymentAbailableModel.CanIncreasePromisePayment = true;
                }
                    

                return promisePaymentAbailableModel;
            }
            promisePaymentAbailableModel.CanIncreasePromisePayment = false;
            promisePaymentAbailableModel.CanTakePromisePayment = true;
            return promisePaymentAbailableModel;
        }


        /// <summary>
        /// Провекрить возможность продлить обещанный прлотеж.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>Истина - продлить ОП можно, Ложь - продлить нельзя.</returns>
        private bool CanProlongPromisePayment(Guid accountId)
        {
            var billingAccountInfo = dbLayer.BillingAccountRepository.FirstOrDefault(b => b.Id == accountId);

            if (billingAccountInfo == null)
                throw new NotFoundException($"Не удалось получить данные о бансе аккаунта '{accountId}'");

            if (billingAccountInfo.PromisePaymentDate == null || billingAccountInfo.PromisePaymentSum == null ||
                billingAccountInfo.PromisePaymentSum == 0)
                return false;

            return true;
        }
    }
}
