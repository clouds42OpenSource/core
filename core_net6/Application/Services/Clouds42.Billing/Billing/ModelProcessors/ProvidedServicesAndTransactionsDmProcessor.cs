﻿using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Billing.Mappers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.Account.AccountBilling.ProvidedServices;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using PagedList.Core;

namespace Clouds42.Billing.Billing.ModelProcessors
{
    /// <summary>
    /// Процессор транзакций и оказанных услуг
    /// </summary>
    public class ProvidedServicesAndTransactionsDmProcessor(
        IUnitOfWork dbLayer,
        IResourcesService resourcesService,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
    {
        private const int ConstPageSize = 10;


        /// <summary>
        /// Получить пагинированный список оказанных услуг V2 
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="query"></param>
        /// <returns>Пагинированный список оказанных услуг</returns>
        public SelectDataResultCommonDto<ProvidedServiceDto> GetPaginatedProvidedServicesForAccount(Guid accountId,
            SelectDataCommonDto<ProvidedServiceFilterDto> query)
        {
            try
            {
                var filter = new ProvidedServiceAndTransactionsFilterDto
                {
                    DateFrom = query?.Filter?.DateFrom,
                    DateTo = query?.Filter?.DateTo,
                    PageNumber = query?.PageNumber ?? 1,
                    Service = query?.Filter?.Service
                };
                var providedServicesPagedList =
                    GetProvidedServices(accountId, filter)
                        .OrderByDescending(o => o.DateTo)
                        .ThenByDescending(o => o.DateFrom)
                        .Include(i => i.SponsoredAccount)
                        .ToPagedList(filter.PageNumber, ConstPageSize);

                var resourcesConfigurations = resourcesService.GetResourcesConfigurations(accountId);
                var list = providedServicesPagedList.ToList().Select(s => ProvidedServiceToDomainModel(s, resourcesConfigurations)).ToList();

                return new SelectDataResultCommonDto<ProvidedServiceDto>
                {
                    Pagination = new PaginationBaseDto(filter.PageNumber, providedServicesPagedList.TotalItemCount, ConstPageSize),
                    Records = list.ToArray()
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получении списка оказанных услуг] для аккаунта {accountId}");
                throw;
            }
        }

        /// <summary>
        /// Получить список оказанных услуг по долговременным сервисам (ЗД и рекогнишен)
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="query"></param>
        /// <returns>Список оказанных услуг по долговременным сервисам</returns>
        public SelectDataResultCommonDto<ProvidedServiceDto> GetPaginatedProvidedLongServicesForAccount(Guid accountId,
            SelectDataCommonDto<ProvidedLongServiceFilterDto> query)
        {
            try
            {
                var providedServicesPagedList =
                    dbLayer.ProvidedServiceRepository.WhereLazy(
                            p => p.AccountId == accountId &&
                                  (p.Service == Clouds42Service.Esdl.ToString() || p.Service == Clouds42Service.Recognition.ToString()))
                        .OrderByDescending(o => o.DateTo)
                        .ThenByDescending(o => o.DateFrom)
                        .ToPagedList(query?.PageNumber ?? 1, ConstPageSize);

                var list = providedServicesPagedList.ToList().Select(w => w.MapToProvidedServiceDm(false, null, cloudLocalizer)).ToList();
                return new SelectDataResultCommonDto<ProvidedServiceDto>
                {
                    Pagination = new PaginationBaseDto(query?.PageNumber ?? 1, providedServicesPagedList.TotalItemCount, ConstPageSize),
                    Records = list.ToArray()
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получении списка оказанных услуг] для аккаунта {accountId}");
                throw;
            }
        }

        /// <summary>
        /// Получить список оказанных услуг
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="filter">Фильтр поиска</param>
        /// <returns>Список оказанных услуг</returns>
        private IQueryable<ProvidedService> GetProvidedServices(Guid accountId, ProvidedServiceAndTransactionsFilterDto filter)
        {
            var query = dbLayer.ProvidedServiceRepository.WhereLazy(p =>
                p.AccountId == accountId && 
                p.Service != Clouds42Service.Esdl.ToString() &&
                p.Service != Clouds42Service.Recognition.ToString());

            if (filter == null)
                return query;

            if (filter.Service.HasValue)
            {
                var serviceStr = filter.Service.Value.ToString();
                query = query.Where(p => p.Service == serviceStr);
            }
            if (filter.DateFrom.HasValue)
            {
                query = query.Where(p => filter.DateFrom <= p.DateFrom);
            }

            if (!filter.DateTo.HasValue)
            {
                return query;
            }

            {
                var dateTo = filter.DateTo.Value.AddDays(1).AddSeconds(-1);
                query = query.Where(p => p.DateFrom <= dateTo);
            }
            return query;
        }

        /// <summary>
        /// Сконвертировать доменную модель оказанной услуги к модели представления
        /// </summary>
        /// <param name="providedService">Доменная модель оказанной услуги</param>
        /// <param name="resourcesConfigurations">Список ресурс конфигураций</param>
        /// <returns>Модель представления оказанной услуги</returns>
        private ProvidedServiceDto ProvidedServiceToDomainModel(ProvidedService providedService, List<ResourcesConfiguration> resourcesConfigurations)
        {
            var sponsoredAccountName = providedService.SponsoredAccount?.IndexNumber.ToString() ?? string.Empty;
            var resConfig = resourcesConfigurations.FirstOrDefault(r => r.BillingService.SystemService != null);
            var isHistoricalRow = resConfig != null &&
                resConfig.BillingService.SystemService != Clouds42Service.Recognition &&
                ((providedService.DateTo.HasValue && providedService.DateTo < DateTime.Now) || resConfig.FrozenValue);

            return providedService.MapToProvidedServiceDm(isHistoricalRow, sponsoredAccountName, cloudLocalizer);
        }
    }
}
