﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Domain.DataModels.billing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.IncomingModels;
using Clouds42.LetterNotification.LetterNotifications.Invoice;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.ModelProcessors
{
    public class BeforeSomeDayBillingServiceLockAutoPayNotifyCreator(
        IUnitOfWork unitOfWork,
        IInvoiceProvider invoiceProvider,
        BillingAccountManager billingAccountManager,
        IInvoiceDocumentBuilder razorBuilder,
        ICloudLocalizer cloudLocalizer,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
    {
        /// <summary>
        /// Создать уведомление
        /// </summary>
        public void CreateNotification(ResourcesConfiguration resourcesConfiguration)
        {
            var invoice = invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(resourcesConfiguration,
                resourcesConfiguration.BillingAccounts);

            var invoiceDc = billingAccountManager.GetInvoiceDc(invoice.Id).Result;
            var invoicePdf = razorBuilder.Build(invoiceDc);

            var siteAuthorityUrl =
                localesConfigurationDataProvider.GetCpSiteUrlForAccount(resourcesConfiguration.AccountId);

            var autoPayMethod = unitOfWork.AccountAutoPayConfigurationRepository.GetByAccountId(resourcesConfiguration.AccountId);

            if (autoPayMethod == null)
            {
                return;
            }

            var paymentMethod = unitOfWork.SavedPaymentMethodRepository.GetById(autoPayMethod.SavedPaymentMethodId);
            var paymentMethodBankCard = unitOfWork.SavedPaymentMethodBankCardRepository.GetByPaymentMethodId(paymentMethod.Id);

            letterNotificationProcessor.TryNotify<BeforeServiceLockAutoPayLetterNotification, BeforeServiceLockAutoPayLetterModel>(
                PrepareBeforeServiceLockAutoPayLetterModelHelper.PrepareBeforeServiceLockAutoPayLetterModel(resourcesConfiguration,
                    invoiceDc, invoicePdf, siteAuthorityUrl, invoice.Period, (paymentMethod.Type, paymentMethodBankCard), cloudLocalizer));
        }
    }
}
