﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.ModelProcessors
{
    /// <summary>
    /// Процессор для работы со счетом
    /// </summary>
    internal class InvoiceDcProcessor(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IInvoiceDcProcessor
    {
        /// <summary>
        ///     Получить DC модель чека
        /// </summary>
        /// <param name="invoiceId">ID счета на оплату</param>
        public InvoiceReceiptDto GetReceiptModel(Guid invoiceId)
        {
            var invoice = dbLayer.InvoiceRepository.GetInvoiceById(invoiceId);
            var supplier = accountConfigurationDataProvider.GetAccountSupplier(invoice.AccountId);
            var accountLocale = accountConfigurationDataProvider.GetAccountLocale(invoice.AccountId);

            if (invoice.InvoiceReceipt == null)
                throw new InvalidOperationException("Нет фискального чека у данного счета");

            return new InvoiceReceiptDto
            {
                InvoiceId = invoiceId,
                LocaleName = accountLocale.Name,
                FiscalData = new InvoiceReceiptFiscalDataDto
                {
                    FiscalNumber = invoice.InvoiceReceipt.FiscalNumber,
                    FiscalSerial = invoice.InvoiceReceipt.FiscalSerial,
                    FiscalSign = invoice.InvoiceReceipt.FiscalSign,
                    QrCodeData = invoice.InvoiceReceipt.QrCodeData,
                    QrCodeFormat = invoice.InvoiceReceipt.QrCodeFormat,
                    ReceiptInfo = invoice.InvoiceReceipt.ReceiptInfo,
                    DocumentDateTime = invoice.InvoiceReceipt.DocumentDateTime
                },
                SupplierRazorTemplate = new SupplierRazorTemplateDto
                {
                    SupplierId = supplier.Id,
                    RazorData = supplier.PrintedHtmlFormInvoiceReceipt.HtmlData,
                    Attachments = supplier.PrintedHtmlFormInvoiceReceipt.Files.Select(w => new CloudFileDto
                    {
                        Id = w.CloudFileId,
                        FileName = w.CloudFile.FileName,
                        ContentType = w.CloudFile.ContentType,
                        Base64 = Convert.ToBase64String(w.CloudFile.Content),
                        Bytes = w.CloudFile.Content
                    }).ToList()
                }
            };
        }
    }
}