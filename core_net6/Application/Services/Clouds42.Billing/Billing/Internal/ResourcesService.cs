﻿using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.Billing.Billing.Internal
{
    /// <summary>
    /// Класс для работы с ресурсами и ресурс конфигурациями сервиса
    /// </summary>
    public class ResourcesService(
        IUnitOfWork uow,
        ILogger42 logger,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        IHandlerException handlerException)
        : IResourcesService
    {
        #region Public methods

        /// <summary>
        /// Получить ресурс конфигурацию сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Тип системного сервиса</param>
        /// <returns>Ресурс конфигурация сервиса</returns>
        public ResourcesConfiguration? GetResourceConfig(Guid accountId, Clouds42Service service)
            => GetResourceConfiguration(accountId, service);

        /// <summary>
        /// Получить ресурс конфигурацию сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Сервис облака</param>
        /// <returns>Ресурс конфигурация сервиса</returns>
        public ResourcesConfiguration GetResourceConfig(Guid accountId, IBillingService service)
            => uow.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == accountId && config.BillingServiceId == service.Id);

        /// <summary>
        /// Получить ресурс конфигурацию сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceId">ID сервис</param>
        /// <returns>Ресурс конфигурация сервиса</returns>
        public ResourcesConfiguration GetResourcesConfigurationOrThrowException(Guid accountId, Guid serviceId) =>
            uow.ResourceConfigurationRepository.FirstOrThrowException(
                rc => rc.AccountId == accountId && rc.BillingServiceId == serviceId,
                $"Не удалось получить конфигурацию ресурса. Аккаунт = '{accountId}'. Сервис = '{serviceId}' ");

        /// <summary>
        /// Получить список ресурсов
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Тип системного сервиса</param>
        /// <param name="resourceType">Тип ресурсов</param>
        /// <returns>Cписок ресурсов</returns>
        public List<Resource> GetResources(Guid accountId, Clouds42Service service, ResourceType? resourceType = null)
            => GetResources(accountId, service,
                    resourceType != null ? [resourceType.Value] : null)
                .Select(res => res.Init())
                .ToList();

        /// <summary>
        /// Создать новый ресурс либо переназначем доступ освободившемуся.
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resourceConfig">Ресурс конфигурация</param>
        /// <param name="subjectId">ID пользователя</param>
        /// <param name="rateCost">Стоимость</param>
        /// <param name="resourceType">Тип ресурса</param>
        /// <param name="tag">Тэг</param> 
        public void AddOrResignResource(Guid accountId, ResourcesConfiguration resourceConfig, decimal rateCost,
            Guid subjectId, ResourceType resourceType, int? tag = null)
        {
            AddOrResignResource(accountId, resourceConfig.BillingService, subjectId, rateCost, resourceType, tag);
        }

        /// <summary>
        /// Получить список ресурс конфигураций аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список ресурс конфигураций аккаунта</returns>
        public List<ResourcesConfiguration> GetResourcesConfigurations(Guid accountId)
            => GetResourceConfigurations(accountId).ToList();

        /// <summary>
        /// Сохранить запись ресурса
        /// </summary>
        /// <param name="resource">Запись ресурса</param>
        public void CreateResource(Resource resource)
        {
            uow.ResourceRepository.Insert(resource);
            uow.Save();
        }

        /// <summary>
        /// Удалить ресурсы, которые не исопльзуются
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Сервис биллинга</param>
        /// <param name="isDemoService">Сервис является демо</param>
        public void DeleteUnusedResources(Guid accountId, IBillingService service, bool isDemoService)
        {
            var stillAvailableServiceTypesIdForAccount = uow.BillingServiceTypeActivityForAccountRepository
                .GetDeletedButStillAvailableServiceTypesIdForAccount(
                    service.Id, accountId);

            var resList = uow.ResourceRepository.Where(r =>
                r.AccountId == accountId && r.BillingServiceType.ServiceId == service.Id &&
                (!isDemoService && r.Subject == null || r.BillingServiceType.IsDeleted &&
                 !stillAvailableServiceTypesIdForAccount.Contains(
                     r.BillingServiceTypeId)));

            foreach (var res in resList)
            {
                if (res.BillingServiceType.SystemServiceType == ResourceType.DiskSpace)
                    ClearDiskResource(res.Id);
                if(res.BillingServiceType.Name == "Дополнительный сеанс")
                    ClearAdditionalResource(res.Id);

                uow.ResourceRepository.Delete(res);
            }

            uow.Save();
        }

        private void ClearAdditionalResource(Guid resourceId)
        {
            var additionalResource = uow.SessionResourceRepository.AsQueryable()
                .FirstOrDefault(x => x.ResourceId == resourceId);

            if(additionalResource != null)
                uow.SessionResourceRepository.Delete(additionalResource);
        }
        /// <summary>
        /// Получить аккаунт биллинга или создать новый
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт биллинга</returns>
        public BillingAccount GetAccountIfExistsOrCreateNew(Guid accountId)
        {
            var account = uow.BillingAccountRepository.FirstOrDefault(acc => acc.Id == accountId);
            return account ?? CreateAccount(accountId);
        }

        /// <summary>
        /// Добавить новую конфигурацию ресурса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Сервис облака</param>
        /// <param name="resConfExpireDate">Дата истечения срока годности</param>
        /// <returns>Конфигурация ресурса</returns>
        public ResourcesConfiguration CreateNewResourcesConfiguration(
            Guid accountId,
            IBillingService service,
            DateTime? resConfExpireDate = null)
        {
            logger.Trace(
                $"Добавление нового ресурса {service} для аккаунта {accountId} активный до: {resConfExpireDate?.ToString("dd.mm.yy hh:MM:ss") ?? "default"}");

            return CreateNewResourceConfiguration(accountId, service, false,
                resConfExpireDate ?? DateTime.Today.AddMonths(1));
        }

        /// <summary>
        /// Добавить новую конфигурацию ресурса для зависимого сервиса(от Аренды 1С)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Сервис облака</param>
        /// <returns>Конфигурация ресурса</returns>
        public ResourcesConfiguration CreateNewResourcesConfigurationForDependentService(
            Guid accountId,
            IBillingService service)
        {
            logger.Trace(
                $"Добавление нового ресурса {service} для аккаунта {accountId}");

            return CreateNewResourceConfiguration(accountId, service);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Удалить ресурсы диска.
        /// </summary>
        /// <param name="resourceId">ID ресурса</param>
        private void ClearDiskResource(Guid resourceId)
        {
            var diskRes = uow.DiskResourceRepository.FirstOrDefault(r => r.ResourceId == resourceId);
            if (diskRes == null)
                return;

            uow.DiskResourceRepository.Delete(diskRes);
            uow.Save();
        }

        /// <summary>
        /// Создать новый ресурс либо переназначем доступ освободившемуся.
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Сервис облака</param>
        /// <param name="subjectId">ID пользователя</param>
        /// <param name="cost">Стоимость</param>
        /// <param name="resourceType">Тип ресурса</param> 
        private void AddOrResignResource(Guid accountId, BillingService service, Guid subjectId, decimal cost,
            ResourceType resourceType, int? tag = null)
        {

           try
           {
               logger.Info($"[{accountId}] :: Ищем ресурс для пользователя {subjectId}");
               var resource = uow.ResourceRepository
                   .FirstOrDefault(res => res.AccountId == accountId &&
                                          res.BillingServiceType.ServiceId == service.Id &&
                                          res.Subject == subjectId &&
                                          res.BillingServiceType.SystemServiceType == resourceType);

               if (resource == null)
               {
                   logger.Info($"[{accountId}] :: Ресурс для пользователя {subjectId} не найден ищем свободный");
                   resource = uow.ResourceRepository
                       .FirstOrDefault(res => res.AccountId == accountId &&
                                              res.BillingServiceType.ServiceId == service.Id &&
                                              res.Subject == null &&
                                              res.BillingServiceType.SystemServiceType == resourceType);
               }

               if (resource == null)
               {
                   logger.Info($"[{accountId}] :: Свободный ресурс не найден");
                   resource = CreateResource(accountId, resourceType, cost, tag: tag);
               }

               logger.Info(
                   $"[{accountId}] :: На свободный сервис {service.Name} назначаем пользователя {subjectId} и выставляем стоимость {cost}");

               resource.Subject = subjectId;
               resource.Cost = cost;

               if (service.SystemService == Clouds42Service.MyEnterprise)
               {
                   var user = uow.AccountUsersRepository.FirstOrDefault(x => x.Id == subjectId);
                   if (user != null && user.AccountId != accountId)
                   {
                       resource.AccountId = user.AccountId;
                       resource.AccountSponsorId = accountId;
                   }
               }

               uow.ResourceRepository.Update(resource);
               uow.Save();

               logger.Info($"[{accountId}] :: Пересчитываем стоимость сервис {service.Name}");
               recalculateResourcesConfigurationCostProvider.Recalculate(accountId, resource.BillingServiceType.Service);

               uow.Save();
           }
           catch (Exception ex)
           {
               handlerException.Handle(ex, "[Ошибка создания нового ресурса]");
                throw;
           }
        }

        /// <summary>
        /// Получить список ресурсов
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Тип системного сервиса</param>
        /// <param name="resourceType">Список типов ресурсов</param>
        /// <returns>Cписок ресурсов</returns>
        private List<Resource> GetResources(Guid accountId, Clouds42Service service,
            List<ResourceType>? resourceType = null)
        {
            var resources = uow.ResourceRepository.Where(resource =>
                resource.AccountId == accountId && resource.BillingServiceType.Service.SystemService == service);

            if (resourceType == null)
                return resources.ToList();

            var stringResType = resourceType.Select(r => r).ToList();
            resources = resources.Where(resource =>
                resource.BillingServiceType.SystemServiceType.HasValue &&
                stringResType.Contains(resource.BillingServiceType.SystemServiceType.Value));

            return resources.ToList();
        }

        /// <summary>
        /// Получить ресурс конфигурацию сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Тип системного сервиса</param>
        /// <returns>Ресурс конфигурация сервиса</returns>
        private ResourcesConfiguration GetResourceConfiguration(Guid accountId, Clouds42Service service)
            => uow.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == accountId && config.BillingService.SystemService == service);

        /// <summary>
        /// Получить описание всех конфигураций ресурсов сервисов заданного аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Описание всех конфигураций ресурсов сервисов заданного аккаунта</returns>
        private List<ResourcesConfiguration> GetResourceConfigurations(Guid accountId)
            => uow.ResourceConfigurationRepository.Where(config => config.AccountId == accountId).ToList();

        /// <summary>
        /// Создать запись конфигурации ресурса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Сервис облака</param>
        /// <param name="frozen">Конфигурация заблокирована</param>
        /// <param name="expiredDate">Дата окончания</param>
        /// <returns>Запись конфигурации ресурса</returns>
        private ResourcesConfiguration CreateNewResourceConfiguration(Guid accountId, IBillingService service,
            bool? frozen = null, DateTime? expiredDate = null)
        {
            var resConfig = new ResourcesConfiguration
            {
                AccountId = accountId,
                Cost = 0,
                CostIsFixed = false,
                DiscountGroup = 0,
                ExpireDate = expiredDate,
                Id = Guid.NewGuid(),
                BillingServiceId = service.Id,
                Frozen = frozen,
                CreateDate = DateTime.Now
            };

            uow.ResourceConfigurationRepository.Insert(resConfig);
            uow.Save();
            logger.Trace($"Запись добавлена в таблицу ResourceConfiguration для аккаунта {accountId}");

            return resConfig;
        }

        /// <summary>
        /// Создать аккаунт биллинга
        /// </summary>
        /// <param name="companyId">ID аккаунта</param>
        /// <returns>Аккаунт биллинга</returns>
        private BillingAccount CreateAccount(Guid companyId)
        {
            var account = new BillingAccount
            {
                Id = companyId,
                AccountType = AccountLevel.Standart.ToString(),
                MonthlyPaymentDate = DateTime.Today.AddMonths(1),
            };

            uow.BillingAccountRepository.Insert(account);
            uow.Save();

            return account;
        }

        /// <summary>
        /// Создать запись ресурса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resourceType">Тип ресурса</param>
        /// <param name="cost">Стоимость</param>
        /// <param name="subjectId">ID пользователя</param>
        /// <param name="isFree">Признак что ресурс свободен</param>
        /// <param name="demoPeriod">Дата окончания демо периода</param>
        /// <param name="tag">Тэг</param>
        /// <returns>Запись ресурса</returns>
        private Resource CreateResource(Guid accountId, ResourceType resourceType, decimal cost, Guid? subjectId = null,
            bool isFree = false, DateTime? demoPeriod = null, int? tag = null)
        {
            var serviceType = new BillingServiceDataProvider(uow).GetSystemServiceTypeOrThrowException(resourceType);

            var res = new Resource
            {
                AccountId = accountId,
                Cost = cost,
                Subject = subjectId,
                BillingServiceTypeId = serviceType.Id,
                Id = Guid.NewGuid(),
                DemoExpirePeriod = demoPeriod,
                IsFree = isFree,
                Tag = tag
            };

            uow.ResourceRepository.Insert(res);
            uow.Save();

            return res;
        }
        #endregion
    }
}
