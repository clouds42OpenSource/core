﻿using System.Configuration;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.Rate;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing.Billing.Internal
{
    /// <summary>
    /// Провайдер тарифов
    /// </summary>
    public class RateProvider(
        IResourcesService resourcesService,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration,
        ISender sender)
        : IRateProvider
    {
        public void UpdateAccountRates(Guid billingAccountId, IBillingService service, params RateChangeItem[] ratesToChange)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var ids = ratesToChange.Select(s => s.BillingServiceTypeId).ToList();

                var resources = dbLayer.ResourceRepository.AsQueryable().Where(r =>
                    ((r.AccountId == billingAccountId && r.AccountSponsorId == null) || r.AccountSponsorId == billingAccountId) &&
                    r.BillingServiceType.ServiceId == service.Id &&
                    r.BillingServiceType.SystemServiceType.HasValue &&
                    ids.Contains(r.BillingServiceTypeId))
                    .ToList();

                var resourcesForUpdate = new List<Resource>();

                resources
                    .GroupJoin(ratesToChange, x => x.BillingServiceTypeId, z => z.BillingServiceTypeId, (resource, rates) => (resource, rates))
                    .ToList()
                    .ForEach(x =>
                    {
                        var rateToChange = x.rates.FirstOrDefault();
                        if (rateToChange == null)
                        {
                            return;
                        }
                        x.resource.Cost = rateToChange.Cost;

                        resourcesForUpdate.Add(x.resource);

                    });
                dbLayer.BulkUpdate(resourcesForUpdate);
                dbLayer.Save();

                var ratesToChangeIds = ratesToChange.Select(x => x.BillingServiceTypeId).ToList();

                var accountRates = dbLayer.AccountRateRepository.AsQueryable().Where(x =>
                    x.AccountId == billingAccountId && ratesToChangeIds.Contains(x.BillingServiceTypeId)).ToList();

                var accountRatesToUpdate = new List<AccountRate>();
                var accountRatesToAdd = new List<AccountRate>();

                ratesToChange.GroupJoin(accountRates, z => z.BillingServiceTypeId, x => x.BillingServiceTypeId, (rate, ar) => (rate, ar))
                    .ToList()
                    .ForEach(x =>
                    {
                        var accountRate = x.ar.FirstOrDefault();
                        if (accountRate != null)
                        {
                            accountRate.Cost = x.rate.Cost;
                            accountRatesToUpdate.Add(accountRate);
                        }
                          
                        else
                        {
                            var billingServiceType =  dbLayer.BillingServiceTypeRepository
                                .AsQueryableNoTracking()
                                .FirstOrDefault(s => s.Id == x.rate.BillingServiceTypeId) ?? throw new ConfigurationErrorsException($"В справочнике услуги сервиса биллинга, не сконфигурированна услуга '{x.rate.BillingServiceTypeId}'.");

                            accountRatesToAdd.Add(new AccountRate
                            {
                                Id = Guid.NewGuid(),
                                AccountId = billingAccountId,
                                BillingServiceTypeId = billingServiceType.Id,
                                Cost = x.rate.Cost
                            });
                        }
                    });

                dbLayer.BulkUpdate(accountRatesToUpdate);
                dbLayer.BulkInsert(accountRatesToAdd);

                dbLayer.Save();

                var resConfig = resourcesService.GetResourceConfig(billingAccountId, service);

                sender.Send(new RecalculateResourcesConfigurationCostCommand([billingAccountId],
                    [resConfig.BillingServiceId], resConfig.BillingService.SystemService)).Wait();


                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }
        /// <summary>
        /// Получить оптимальный тариф для сервиса
        /// </summary>
        /// <param name="billingServiceTypeId"> идентификатор сервиса</param>
        /// <returns>информация о базовом тарифе</returns>
        [Obsolete("Данный метод устарел. Используйте функционал сборки Clouds42.BLL.Rate")]
        public RateInfoBaseDc GetOptimalRate(Guid billingAccountId, Guid billingServiceTypeId)
        {
            var accountRate = dbLayer.AccountRateRepository.FirstOrDefault(r => r.AccountId == billingAccountId &&
                                                                                 r.BillingServiceType.Id == billingServiceTypeId);
            if (accountRate != null)
            {
                logger.Debug($"Для аккаунта {billingAccountId} найден существующий тариф");
                return AccountRateToResult(accountRate);
            }

            var rate = FindRate(billingAccountId, billingServiceTypeId) ?? throw new ArgumentNullException($"У данного сервиса {billingServiceTypeId} не найдено подходящего тарифа");
            
            return RateToCommonResult(rate);
        }

        private RateInfoCommonDc RateToCommonResult(Rate rate)
        {
            if (rate.BillingServiceType.SystemServiceType == ResourceType.DiskSpace)
            {
                if (!rate.LowerBound.HasValue || !rate.UpperBound.HasValue)
                    throw new InvalidOperationException($"у дискового тарифа {rate.Id} не определены диапозоны объема");

                var diskResult = new DiskRateInfoDc
                {
                    LowerBound = rate.LowerBound.Value,
                    UpperBound = rate.UpperBound.Value
                };

                FillingRateResult(diskResult, rate);
                return diskResult;
            }

            var standardResult = new StandardRateInfoDc();

            FillingRateResult(standardResult, rate);
            return standardResult;
        }

        private void FillingRateResult(RateInfoCommonDc rateResult, Rate source)
        {
            rateResult.Cost = source.Cost;
            rateResult.DiscountGroup = source.DiscountGroup;
            rateResult.AccountType = source.AccountType;
            rateResult.DemoPeriod = source.DemoPeriod;
            rateResult.LocaleId = source.LocaleId;
            rateResult.Remark = source.Remark;
            rateResult.RatePeriod = source.RatePeriod;
        }

        private AccountRateInfoDc AccountRateToResult(AccountRate accountRate)
        {
            return new AccountRateInfoDc
            {
                AccountId = accountRate.AccountId,
                Cost = accountRate.Cost
            };
        }

        private Rate? FindRate(Guid billingAccountId, Guid billingServiceTypeId)
        {
            var billingAccount = dbLayer.BillingAccountRepository.FirstOrDefault(x => x.Id == billingAccountId)
                ?? throw new NotFoundException($"Аккаунт {billingAccountId} не найден");
            var accountType = billingAccount.AccountType;
            var locale = dbLayer.AccountsRepository.GetLocale(billingAccountId, Guid.Parse(configuration["DefaultLocale"] ?? "")) ??
                         throw new NotFoundException($"По идентификатору аккаунта '{billingAccountId}' не удалось найти локаль аккаунта.");

            return dbLayer.RateRepository
                       .OrderBy(r => r.Cost)
                       .FirstOrDefault(r => r.BillingServiceType.Id == billingServiceTypeId &&
                                            r.AccountType == accountType &&
                                            r.LocaleId == locale.ID);
        }
    }
}
