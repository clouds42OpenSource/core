﻿using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Billing.Internal
{
    public class FastaResourcesService(
        ILogger42 logger,
        IHandlerException handlerException,
        ICloud42ServiceFactory cloud42ServiceFactory,
        IUnitOfWork uow,
        IAccessProvider accessProvider)
        : IFastaResourcesService
    {
        /// <summary>
        /// Покупка и страниц и лицензии
        /// </summary>
        /// <param name="accountId">Индификатор пользователя</param>
        /// <param name="yearsAmount">период лицензии</param>
        /// <param name="pagesAmount">количество страниц</param>
        /// <returns></returns>
        public BuyingEsdlResultModelDto BuyEsdlAndRecognition42(Guid accountId, int yearsAmount, int pagesAmount)
        {
            try
            {
                logger.Trace(
                    $"Аккаунт{accountId}:Проверяем возможность покупки лицензий на {yearsAmount} и страниц {pagesAmount} для сервиса {NomenclaturesNameConstants.Esdl}");

                var rec42Serv = cloud42ServiceFactory.Get<IRecognition42CloudService>(accountId);
                var needModey = rec42Serv.GetNeedModeyForBuyServices(pagesAmount, yearsAmount);

                if (needModey != null)
                {
                    logger.Trace(
                        $"Аккаунт{accountId}:Для покупки лицензий на {yearsAmount} и страниц {pagesAmount} для сервиса {NomenclaturesNameConstants.Esdl} недостает денег в размере: {needModey}");
                    return new BuyingEsdlResultModelDto
                    {
                        Complete = false,
                        NeedModey = needModey,
                        Comment = "Недостаточно денег для покупки сервиса."
                    };
                }

                logger.Trace(
                    $"Аккаунт{accountId}:Начало процесса покупки и лицензий на {yearsAmount} и страниц {pagesAmount} для сервиса {NomenclaturesNameConstants.Esdl}");

                //покупка страниц
                logger.Trace("Покупка страниц");
                var resRec42 = BuyRecognition42(accountId, pagesAmount);
                if (!resRec42.Complete)
                    return resRec42;

                //покупка лицензии
                logger.Trace("Покупка лицензий");
                var resEsdl = BuyEsdl(accountId, yearsAmount);

                return resEsdl;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка покупки и страниц и лицензий для сервиса] {NomenclaturesNameConstants.Esdl} Аккаунт {accountId}");
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    Comment = "Ошибка покупки страниц и лицензий. Обратитесь в тех. поддержку."
                };
            }
        }

        /// <summary>
        /// Покупка одних страниц
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="pagesAmount">количество страниц</param>
        /// <returns></returns>
        public BuyingEsdlResultModelDto BuyRecognition42(Guid accountId, int pagesAmount)
        {
            using var dbScope = uow.SmartTransaction.Get();
            try
            {
                var rec42Serv = cloud42ServiceFactory.Get<IRecognition42CloudService>(accountId);
                logger.Trace(
                    $"Аккаунт{accountId}:Начало процесса покупки одних страниц в количестве {pagesAmount} для сервиса {NomenclaturesNameConstants.Esdl}");
                var resRec42 = rec42Serv.BuyRecognition42(pagesAmount);
                LogEventHelper.LogEvent(uow, accountId, accessProvider, LogActions.BuyLicense, resRec42.Comment);
                logger.Trace(
                    $"Аккаунт{accountId}:Процесс покупки одних страниц для сервиса {NomenclaturesNameConstants.Esdl} завершился с результатом {resRec42.Comment}");
                dbScope.Commit();
                return resRec42;
            }
            catch (Exception ex)
            {
                dbScope.Rollback();
                handlerException.Handle(ex,
                    $"[Ошибка покупки одних страниц для сервиса] {NomenclaturesNameConstants.Esdl} Аккаунт {accountId}:");
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    Comment = "Ошибка покупки страниц. Обратитесь в тех. поддержку."
                };
            }
        }

        /// <summary>
        /// Покупка только лицензий
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="yearsAmount">количества лет на который хотят купить лицензию</param>
        /// <returns></returns>
        public BuyingEsdlResultModelDto BuyEsdl(Guid accountId, int yearsAmount)
        {
            using var dbScope = uow.SmartTransaction.Get();
            try
            {
                var esdlServ = cloud42ServiceFactory.Get<IEsdlCloud42Service>(accountId);
                logger.Trace(
                    $"Аккаунт{accountId}:Начало процесса покупки только лицензий на {yearsAmount} для сервиса {NomenclaturesNameConstants.Esdl}");
                var resEsdl = esdlServ.BuyEsdl(yearsAmount);
                LogEventHelper.LogEvent(uow, accountId, accessProvider, LogActions.BuyLicense, resEsdl.Comment);
                logger.Trace(
                    $"Аккаунт{accountId}:Процесс покупки только лицензий для сервиса {NomenclaturesNameConstants.Esdl} завершился с результатом {resEsdl.Comment}");
                dbScope.Commit();
                return resEsdl;
            }
            catch (Exception ex)
            {
                dbScope.Rollback();
                handlerException.Handle(ex,
                    $"[Ошибка покупки одних лицензий для сервиса] {NomenclaturesNameConstants.Esdl} Аккаунт {accountId}:");
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    Comment = "Ошибка покупки лицензий. Обратитесь в тех. поддержку."
                };
            }
        }
    }
}
