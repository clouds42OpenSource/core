﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.BillingOperations.Providers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.BLL.Common.Helpers;
using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Helpers;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;
using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;
using ResourceType = CommonLib.Enums.ResourceType;

namespace Clouds42.Billing.Billing.Internal
{
    /// <summary>
    /// Хелпер для работы с доступами к Аренде 1С
    /// </summary>
    public class Rent1CAccessHelper(
        IUnitOfWork dbLayer,
        ICommitMainServiceResourcesChangesHistoryProvider commitMainServiceResourcesChangesHistoryProvider,
        IRent1CServiceInfoDmProcessor rent1CServiceInfoDmProcessor,
        ICheckAccountDataProvider checkAccountDataProvider,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        ICloud42ServiceHelper cloud42ServiceHelper,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IResourcesService resourcesService,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        ILogger42 logger,
        IAccountUserDataProvider accountUserDataProvider,
        IBillingPaymentsDataProvider billingPaymentsDataProvider,
        IRateProvider rateProvider,
        IHandlerException handlerException,
        ISender sender,
        BillingServiceManager billingServiceManager,
        ICloudLocalizer cloudLocalizer,
        IOnDisabledMyDatabasesServiceTypeForUsersTrigger onDisabledMyDatabasesServiceTypeForUsersTrigger)
        : IRent1CAccessHelper
    {
        private readonly IBillingPaymentsProvider _billingPaymentsProvider = new BillingPaymentsProvider(dbLayer, new BillingPaymentsHelper(dbLayer), logger, handlerException);
        private readonly BillingServiceDataProvider _billingServiceDataProvider = new(dbLayer);

        private readonly IDictionary<string, string> _mapLocaleNameToCountryNameDictionary =
            new Dictionary<string, string>
            {
                {LocaleConst.Russia, "России"},
                {LocaleConst.Kazakhstan, "Казахстана"},
                {LocaleConst.Ukraine, "Украины"},
                {LocaleConst.Uzbekistan, "Узбекистана"},
            };

        /// <summary>
        /// Найти внешнего пользователя(пользователя другого аккаунта)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="email">Электронная поста</param>
        /// <returns>Внешний пользователь</returns>
        public SearchExternalUserRent1CResultDto SearchExternalUser(Guid accountId, string email)
        {
            logger.Trace($"Начало поиска аккаунтом {accountId} для спонсирования пользователя с email:{email}");
            var sponsoredAccountUser =
                dbLayer.AccountUsersRepository.FirstOrDefault(acUs => acUs.Email == email.Trim());

            if (!CanApplySponsorship(accountId, email, sponsoredAccountUser, out var errorModel))
                return errorModel;

            var sponsoredLicenseAccountName = !string.IsNullOrEmpty(sponsoredAccountUser.Account.AccountCaption)
                ? sponsoredAccountUser.Account.AccountCaption
                : sponsoredAccountUser.Account.IndexNumber.ToString();

            logger.Trace($"Название или номер компании которую спонсируют {sponsoredLicenseAccountName}");

            logger.Trace($"Начало подсчета стоимости спонсируемой лицензии для аккаунта {accountId}");
            rent1CServiceInfoDmProcessor
                .CalculatePartialCostForSponsorAccount(sponsoredAccountUser, out var partialRateWebCost,
                    out var partialRateRdpCost,
                    out var partialUserCostStandart);
            logger.Trace(
                $"Стоимость веб {partialRateWebCost}, стоимость рдп {partialRateRdpCost}, " +
                $"стоимость стандарт {partialUserCostStandart} для аккаунта {accountId}");

            return new SearchExternalUserRent1CResultDto
            {
                User = new LicenseOfRent1CDm
                {
                    Id = sponsoredAccountUser.Id,
                    FullUserName =
                        $"{sponsoredAccountUser.LastName} {sponsoredAccountUser.FirstName} {sponsoredAccountUser.MiddleName}"
                            .Trim(),
                    Login = sponsoredAccountUser.Login,
                    IsActive = sponsoredAccountUser.Activated,
                    Email = sponsoredAccountUser.Email,
                    Phone = sponsoredAccountUser.PhoneNumber,
                    RdpResourceId = null,
                    WebResourceId = null,
                    SponsoredLicenseAccountName = sponsoredLicenseAccountName,
                    PartialUserCostWeb = partialRateWebCost,
                    PartialUserCostRdp = partialRateRdpCost,
                    PartialUserCostStandart = partialUserCostStandart
                }
            };
        }

        /// <summary>
        /// Получить данные биллинга аккаунта для покупки сервиса Аренда 1С
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Данные биллинга аккаунта для покупки сервиса Аренда 1С</returns>
        public AccountBillingDataForBuyRent1CDto GetAccountBillingDataForBuyRent1C(Guid accountUserId)
        {
            var accountUser =
                dbLayer.AccountUsersRepository.WhereLazy(w => w.Id == accountUserId).Include(w => w.Account)
                    .FirstOrDefault() ??
                throw new NotFoundException($"По номеру '{accountUserId}' не найден пользователь.");

            var resource = dbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == accountUser.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var expireDateOfService =
                cloud42ServiceHelper.GetCurrentExpireDateOrNewDateToCalculateCostOfService(resource?.ExpireDateValue);

            return GetAccountBillingDataForBuyRent1C(accountUser.Account, expireDateOfService);
        }

        /// <summary>
        /// Получить данные биллинга аккаунта для покупки сервиса Аренда 1С
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="expireDateOfService">Дата пролонгации сервиса</param>
        /// <returns>Данные биллинга аккаунта для покупки сервиса Аренда 1С</returns>
        public AccountBillingDataForBuyRent1CDto GetAccountBillingDataForBuyRent1C(IAccount account,
            DateTime expireDateOfService)
        {
            logger.Trace($"[{account.Id}] ::Поиск ресурсов {Clouds42Service.MyEnterprise.ToString()}");
            var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(account.Id);
            logger.Trace($"[{account.Id}] :: Подсчет стоимости ресурсов");

            var myEntUser =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUser);

            var myEntUserWeb =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUserWeb);

            var rateRdp = rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
            var rateWeb = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

            logger.Trace($"[{account.Id}] :: Расчет частичной стоимости доступа до конца периода оплаты");

            var accountLocale = accountConfigurationDataProvider.GetAccountLocale(account.Id);

            var partialUserCostWeb =
                BillingServicePartialCostCalculator.CalculateUntilExpireDate(rateWeb.Cost, expireDateOfService,
                    accountLocale.Name);

            var partialUserCostRdp =
                BillingServicePartialCostCalculator.CalculateUntilExpireDate(rateRdp.Cost, expireDateOfService,
                    accountLocale.Name);

            var partialUserCostStandart = BillingServicePartialCostCalculator.CalculateUntilExpireDate(
                rateRdp.Cost + rateWeb.Cost,
                expireDateOfService, accountLocale.Name);

            logger.Trace(
                $"[{account.Id}] :: web {partialUserCostWeb}, rdp {partialUserCostRdp}, standart {partialUserCostStandart}");

            return new AccountBillingDataForBuyRent1CDto
            {
                RdpRateCost = rateRdp.Cost,
                WebRateCost = rateWeb.Cost,
                PartialCostOfWebLicense = partialUserCostWeb,
                PartialCostOfRdpLicense = partialUserCostRdp,
                PartialCostOfStandartLicense = partialUserCostStandart
            };
        }

        /// <summary>
        /// Проверить возможность оплатить лицензии сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changeRent1CAccesses">Изменения доступов к Аренде 1С</param>
        /// <param name="result">Результат обновления лицензий</param>
        /// <returns>Возможность оплатить лицензии сервиса Аренда 1С</returns>
        public bool CanPayRent1CAccesses(Guid accountId, List<ChangeRent1CAccessDto> changeRent1CAccesses,
            out UpdateAccessRent1CResultDto result)
        {
            var costForPay = DetermineCostToPayRent1CAccesses(accountId, changeRent1CAccesses);
            result = CanPayRent1CAccess(accountId, costForPay, out var enoughMoneyToPay);
            return enoughMoneyToPay;
        }

        /// <summary>
        /// Определить сумму/стоимость для оплаты лицензий сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changeRent1CAccesses">Изменения доступов к Аренде 1С</param>
        /// <returns>Cумма/стоимость для оплаты лицензий сервиса Аренда 1С</returns>
        private decimal DetermineCostToPayRent1CAccesses(Guid accountId,
            List<ChangeRent1CAccessDto> changeRent1CAccesses) => changeRent1CAccesses
            .Select(changeRent1CAccess => DetermineCostToPayRent1CAccess(accountId, changeRent1CAccess)).Sum();

        /// <summary>
        /// Определить сумму/стоимость для оплаты лицензии сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changeRent1CAccess">Изменение доступа к Аренде 1С</param>
        /// <returns>Cумма/стоимость для оплаты лицензии сервиса Аренда 1С</returns>
        private decimal DetermineCostToPayRent1CAccess(Guid accountId, ChangeRent1CAccessDto changeRent1CAccess)
        {
            if (TryDetermineCostToPayWebAccessIfNeed(accountId, changeRent1CAccess, out var costForPay))
                return costForPay;

            if (!NeedToPayForStandartAccess(changeRent1CAccess))
                return decimal.Zero;

            return DetermineCostToPayStandartAccess(accountId, changeRent1CAccess);
        }

        /// <summary>
        /// Попытаться определить сумму/стоимость для оплаты лицензии вэб Аренда 1С,
        /// если есть необходимость в оплате
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changeRent1CAccess">Изменение доступа к Аренде 1С</param>
        /// <param name="costToPay">Cумма/стоимость для оплаты лицензии вэб Аренда 1С</param>
        /// <returns>Необходимость оплачивать лицензию вэб Аренда 1С</returns>
        private bool TryDetermineCostToPayWebAccessIfNeed(Guid accountId, ChangeRent1CAccessDto changeRent1CAccess,
            out decimal costToPay)
        {
            costToPay = decimal.Zero;

            if (!changeRent1CAccess.AccessData.WebResource)
                return false;

            logger.Trace(
                $"[{accountId}] :: Подключение web для пользователя {changeRent1CAccess.AccessData.AccountUserId}");

            if (changeRent1CAccess.AccessData.WebResourceId.HasValue)
                return false;

            costToPay = changeRent1CAccess.BuyRent1CData.PartialCostOfWebLicense;
            logger.Trace($"[{accountId}] :: Нужно платить за веб {costToPay}");

            return true;
        }

        /// <summary>
        /// Определить сумму/стоимость для оплаты лицензии стандарт Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changeRent1CAccess">Изменение доступа к Аренде 1С</param>
        /// <returns>Cумма/стоимость для оплаты лицензии стандарт Аренда 1С</returns>
        private decimal DetermineCostToPayStandartAccess(Guid accountId, ChangeRent1CAccessDto changeRent1CAccess)
        {
            if (changeRent1CAccess.AccessData.WebResourceId.HasValue)
            {
                logger.Trace(
                    $"[{accountId}] :: Нужно платить за рдп {changeRent1CAccess.BuyRent1CData.PartialCostOfRdpLicense}");
                return changeRent1CAccess.BuyRent1CData.PartialCostOfRdpLicense;
            }

            if (changeRent1CAccess.AccessData.RdpResourceId.HasValue)
            {
                logger.Trace(
                    $"[{accountId}] :: Нужно платить за веб {changeRent1CAccess.BuyRent1CData.PartialCostOfWebLicense}");
                return changeRent1CAccess.BuyRent1CData.PartialCostOfWebLicense;
            }

            logger.Trace(
                $"[{accountId}] :: Нужно платить за всю лицензию полностью {changeRent1CAccess.BuyRent1CData.PartialCostOfStandartLicense}");
            return changeRent1CAccess.BuyRent1CData.PartialCostOfStandartLicense;
        }

        /// <summary>
        /// Проверить необходимость оплачивать лицензию стандарт Аренда 1С
        /// </summary>
        /// <param name="changeRent1CAccess">Изменение доступа к Аренде 1С</param>
        /// <returns>Необходимость оплачивать лицензию стандарт Аренда 1С</returns>
        private static bool NeedToPayForStandartAccess(ChangeRent1CAccessDto changeRent1CAccess) =>
            changeRent1CAccess.AccessData.StandartResource && (!changeRent1CAccess.AccessData.WebResourceId.HasValue ||
                                                               !changeRent1CAccess.AccessData.RdpResourceId.HasValue);

        /// <summary>
        /// Проверить возможность оплатить лицензии сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="costForPay">Сумма к оплате</param>
        /// <param name="enoughMoneyToPay">Признак указывающий что достаточно средств на оплату</param>
        /// <returns>Возможность оплатить лицензии сервиса Аренда 1С</returns>
        private UpdateAccessRent1CResultDto CanPayRent1CAccess(Guid accountId, decimal costForPay,
            out bool enoughMoneyToPay)
        {
            var billingService = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);
            var paymentOperationResult = CanCreatePayment(accountId, billingService.Id, costForPay, out var needMoney);
            enoughMoneyToPay = true;

            var updateAccessRent1CResultDm = new UpdateAccessRent1CResultDto
            {
                CostForPay = costForPay,
                EnoughMoney = needMoney
            };

            if (paymentOperationResult == PaymentOperationResult.Ok)
            {
                logger.Trace(
                    $"[{accountId}] :: Платеж создан. Стоимость сервисов '{costForPay}'.");

                return updateAccessRent1CResultDm;
            }

            logger.Trace(
                $"[{accountId}] :: Ошибка создания платежа. Не достаточно денег на счету в размере '{needMoney}', стоимость сервисов '{costForPay}'.");

            enoughMoneyToPay = false;
            updateAccessRent1CResultDm.CanGetPromisePayment = CanGetPromisePayment(accountId);
            return updateAccessRent1CResultDm;
        }

        /// <summary>
        /// Проверить возможность взять обещанный платеж для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Возможность взять обещанный платеж для аккаунта</returns>
        private bool CanGetPromisePayment(Guid accountId)
        {
            var inflowPaymentType = PaymentType.Inflow.ToString();
            var promisePaymentCost = billingPaymentsDataProvider.GetPromisePaymentCost(accountId);
            var hasInflowPayments = dbLayer.PaymentRepository.Any(p =>
                p.AccountId == accountId && p.OperationType == inflowPaymentType && p.Sum > 0 && p.Status == "Done");

            return hasInflowPayments && (!promisePaymentCost.HasValue || promisePaymentCost.Value <= 0);
        }

        /// <summary>
        /// Проверить возможность создания денежной проводки
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="costForPay">Сумма к оплате</param>
        /// <param name="needMoney">Сумма денег которой не хватает для проведения</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Возможность проведения транзакции</returns>
        private PaymentOperationResult CanCreatePayment(Guid accountId, Guid serviceId, decimal costForPay,
            out decimal needMoney) =>
            _billingPaymentsProvider.CanCreatePayment(
                new PaymentDefinitionDto
                {
                    Account = accountId,
                    Date = DateTime.Now,
                    Id = Guid.NewGuid(),
                    BillingServiceId = serviceId,
                    Status = PaymentStatus.Done,
                    Total = costForPay,
                    OperationType = PaymentType.Outflow,
                    OriginDetails = "core-cp"
                }, out needMoney);

        /// <summary>
        /// Поменять ресурсы Аренды 1С у аккаунта на список новых.
        /// </summary>
        /// <param name="account">Аккаунт.</param>        
        /// <param name="resources">Список новых подключений к лицензиям аренды 1С.</param>
        /// <returns></returns>
        public List<EditUserGroupsAdModelDto> ChangeRen1CResourcesOfAccount(IAccount account,
            List<ChangeRent1CAccessDto> resources)
        {
                try
                {
                    CommitMainServiceResourcesChangesHistory(account.Id, resources);
                    var editUserGroupsAdModelList = ProcessSponsoredResources((Account)account, resources);
                    editUserGroupsAdModelList.AddRange(ProcessAccountResources(resourcesService, (Account)account, resources));
                    var resConfig = resourcesService.GetResourceConfig(account.Id, Clouds42Service.MyEnterprise);

                    logger.Debug($"[{account.Id}] :: Пересчет стоимости ресурсов");
                    sender.Send(new RecalculateResourcesConfigurationCostCommand([account.Id],
                        [resConfig!.BillingService.Id], resConfig.BillingService.SystemService)).Wait();
                
                    return editUserGroupsAdModelList;
                }

                catch (Exception ex)
                {
                    handlerException.Handle(ex,
                        $"{account.Id} :: [Ошибка при смене ресурсов Аренды 1С на список новых]");
                    throw;
                }
        }

        /// <summary>
        /// Зафиксировать историю изменений ресурсов главного сервиса(Аренда1С)
        /// для вип аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="resources">Список новых подключений к лицензиям аренды 1С</param>
        private void CommitMainServiceResourcesChangesHistory(Guid accountId, List<ChangeRent1CAccessDto> resources)
        {
            if (!accountConfigurationDataProvider.IsVipAccount(accountId))
                return;

            var disabledResourcesCount =
                resources.Count(r => !r.AccessData.WebResource && !r.AccessData.StandartResource);

            var usersToEnabled = resources.Where(r => r.AccessData.StandartResource || r.AccessData.WebResource)
                .Select(r => r.AccessData.AccountUserId).ToList();

            var usersWithResources = SelectUserIdsWhoHaveResources(usersToEnabled);
            usersToEnabled.RemoveAll(userId => usersWithResources.Contains(userId));

            if (disabledResourcesCount == 0 && usersToEnabled.Count == 0)
                return;

            commitMainServiceResourcesChangesHistoryProvider.Commit(new CommitMainServiceResourcesChangesHistoryDto
            {
                AccountId = accountId,
                DisabledResourcesCount = disabledResourcesCount,
                EnabledResourcesCount = usersToEnabled.Count
            });
        }

        /// <summary>
        /// Выбрать Id пользователей у которых есть ресурсы Аренды 1С
        /// </summary>
        /// <param name="userIds">Список пользователей, которые участвуют в выборке</param>
        /// <returns>Список пользователей, у которых есть ресурсы Аренды 1С</returns>
        private IEnumerable<Guid> SelectUserIdsWhoHaveResources(List<Guid> userIds)
        {
            return dbLayer.ResourceRepository
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.BillingServiceType)
                .Where(x => userIds.Contains(x.Subject.Value) &&
                            (x.BillingServiceType.SystemServiceType == ResourceType.MyEntUser ||
                             x.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb))
                .Select(x => x.Subject.Value)
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// Обработать спонсорские лицензии
        /// </summary>
        /// <param name="account">Аккаунт.</param>        
        /// <param name="resources">Список новых подключений к лицензиям аренды 1С.</param>
        private List<EditUserGroupsAdModelDto> ProcessSponsoredResources(Account account,
            List<ChangeRent1CAccessDto> resources)
        {
            var editUserGroupsAdModelList = new List<EditUserGroupsAdModelDto>();

            foreach (var accessData in resources.Select(accessItem => accessItem.AccessData))
            {
                if (accessData.StandartResource || accessData.WebResource)
                {
                    var isExistSponsoredResourcesFromUser =
                        IsExistSponsoredResourcesFromUser(account.Id, accessData.AccountUserId);

                    var anotherResources = GetUserResourcesThatAreNotSponsored(accessData.AccountUserId);

                    if (accessData.SponsorAccountUser && anotherResources.Any())
                    {
                        logger.Debug(
                            $"[{account.Id}] :: Освобождение ресурсов пользователя {accessData.AccountUserId}, так как ресурсы не является спонсорским и выполняется спонсирование");
                        RemoveAccessToResourcesForUser(anotherResources);
                        logger.Debug(
                            $"[{account.Id}] :: Блокировка сервисов пользователя {accessData.AccountUserId}, так как ресурсы не является спонсорским и выполняется спонсирование");
                        editUserGroupsAdModelList.Add(LockUserServices(accessData.AccountUserId,
                            anotherResources));
                        logger.Debug(
                            $"[{account.Id}] :: Удаление ресурсов пользователя {accessData.AccountUserId}, так как ресурсы не является спонсорским и выполняется спонсирование, при отключенной Аренде 1С у спонсируемого");
                        RemoveResources(anotherResources, accessData.AccountUserId);
                    }

                    if (isExistSponsoredResourcesFromUser)
                    {
                        logger.Debug(
                            $"[{account.Id}] :: Очистка ресурса, так как ресурс является спонсорским для пользователя {accessData.AccountUserId}");
                        RemoveAccess(account, accessData);
                        dbLayer.Save();
                    }
                }
                else
                {
                    logger.Debug(
                        $"[{account.Id}] :: Отключение аренды для пользователя {accessData.AccountUserId}");
                    RemoveAccess(account, accessData);
                    dbLayer.Save();
                }
            }

            return editUserGroupsAdModelList;
        }

        /// <summary>
        /// Существуют ли спонсорские ресурсы у пользователя
        /// для сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта пользователя</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Флаг, указывающий на наличие спонсорских русурсов
        /// Аренды 1С у пользователя</returns>
        private bool IsExistSponsoredResourcesFromUser(Guid accountId, Guid accountUserId)
        {
            return dbLayer.ResourceRepository
                .AsQueryable()
                .AsNoTracking()
                .Any(x => x.AccountId == accountId && 
                          x.Subject == accountUserId && 
                          x.AccountSponsorId != null &&
                          x.AccountSponsorId != accountId &&
                          x.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise);
        }

        /// <summary>
        /// Обработать ресурсы аккаунта
        /// </summary>
        /// <param name="resourcesService">Провайдер ресурсов</param>
        /// <param name="account">Аккаунт.</param>        
        /// <param name="accessItems">Список новых подключений к лицензиям аренды 1С.</param>
        private List<EditUserGroupsAdModelDto> ProcessAccountResources(IResourcesService resourcesService, Account account,
            List<ChangeRent1CAccessDto> accessItems)
        {
            var editUserGroupsAdModelList = new List<EditUserGroupsAdModelDto>();
            foreach (var accessItem in accessItems)
            {
                if (accessItem.AccessData.StandartResource || accessItem.AccessData.WebResource)
                {
                    if (accessItem.AccessData.WebResource)
                    {
                        AddWebAccess(account, accessItem.BuyRent1CData, accessItem.AccessData, resourcesService);
                    }
                    else
                    {
                        AddStandartAccess(account, accessItem.BuyRent1CData, accessItem.AccessData, resourcesService);
                    }

                    dbLayer.Save();
                }
                else
                {
                    var anotherResources = GetUserResourcesThatAreNotSponsored(accessItem.AccessData.AccountUserId);

                    if (accessItem.AccessData.SponsorAccountUser || !anotherResources.Any())
                    {
                        continue;
                    }

                    logger.Debug(
                        $"[{account.Id}] :: Освобождение ресурсов пользователя {accessItem.AccessData.AccountUserId}, так как ресурсы не является спонсорским");
                    RemoveAccessToResourcesForUser(anotherResources);
                    logger.Debug(
                        $"[{account.Id}] :: Блокировка сервисов пользователя {accessItem.AccessData.AccountUserId}, так как ресурсы не является спонсорским");
                    editUserGroupsAdModelList.Add(LockUserServices(accessItem.AccessData.AccountUserId,
                        anotherResources));
                }
            }

            return editUserGroupsAdModelList;
        }

        /// <summary>
        /// Получить ресурсы пользователя которые не являются спонсорскими
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns></returns>
        private List<Resource> GetUserResourcesThatAreNotSponsored(Guid accountUserId)
        {
            var anotherResources = dbLayer.ResourceRepository.Where(w =>
                w.Subject == accountUserId && w.AccountSponsorId == null &&
                (w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser ||
                 w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb)).ToList();

            return anotherResources;
        }

        /// <summary>
        /// Удалить доступ в ресурсах для пользователя
        /// </summary>
        /// <param name="resources">Список ресурсов</param>
        private void RemoveAccessToResourcesForUser(List<Resource> resources)
        {
            resources.ForEach(w =>
            {
                w.Subject = null;
            });
            dbLayer.BulkUpdate(resources);
            dbLayer.Save();

            sender.Send(new RecalculateResourcesConfigurationCostCommand(resources.Select(x => x.AccountId).ToList(),
                resources.Select(x => x.BillingServiceType.ServiceId).ToList(), Clouds42Service.MyEnterprise)).Wait();
        }

        /// <summary>
        /// Удалить ресурсы
        /// </summary>
        /// <param name="resources">Список ресурсов</param>
        /// <param name="accountUserId">Id пользователя</param>
        private void RemoveResources(List<Resource> resources, Guid accountUserId)
        {
            var accountUser = accountUserDataProvider.GetAccountUserOrThrowException(accountUserId);

            logger.Debug($"Проверка активности Аренды 1С для аккаунта {accountUser.AccountId}");

            var resourceConfiguration =
                resourceConfigurationDataProvider.GetResourceConfigurationOrThrowException(accountUser.AccountId,
                    Clouds42Service.MyEnterprise);

            if (!resourceConfiguration.FrozenValue)
                return;

            logger.Debug("Удаление ресурсов в связи со спонсированием и заблокированной Арендой 1С");

            dbLayer.BulkDelete(resources);
            dbLayer.Save();
        }

        /// <summary>
        /// Блокировка сервисов пользователя
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="resources">Ресурсы</param>
        private EditUserGroupsAdModelDto LockUserServices(Guid accountUserId, List<Resource> resources)
        {
            var accountUser = dbLayer.AccountUsersRepository.GetAccountUser(accountUserId);

            var accountUserDbAccesses = dbLayer.AcDbAccessesRepository
                .AsQueryableNoTracking()
                .Where(x => x.AccountUserID == accountUserId)
                .Select(x => new AccountIndexNumberDbNumberDto { AccountIndexNumber = x.Account.IndexNumber, DbNumber = x.Database.DbNumber })
                .ToList();

            return CreateEditUserGroupsAdModelHelper
                .CreateEditUserGroupsAdModel(resources, accountUser,EditUserGroupsAdModelDto.OperationEnum.Exclude)
                .AppendAcDbAccesses(accountUserDbAccesses,accountUser, EditUserGroupsAdModelDto.OperationEnum.Exclude);
        }

        /// <summary>
        /// Предоставление доступа к веб ресурсу
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="buyRent1CData">Данные биллинга аккаунта для покупки сервиса Аренда 1С</param>
        /// <param name="accessItem">Модель обновления доступа к Аренде 1С</param>
        /// <param name="resourcesService">Класс для работы с ресурсами сервиса</param>
        private void AddWebAccess(Account account, AccountBillingDataForBuyRent1CDto buyRent1CData,
            UpdaterAccessRent1CRequestDto accessItem, IResourcesService resourcesService)
        {
            if (accessItem.WebResourceId.HasValue)
            {
                logger.Debug($"[{account.Id}] :: Предоставляем доступ к веб ресурсу для {accessItem.AccountUserId}");
                UpdateResource(account, accessItem.WebResourceId.Value, accessItem.AccountUserId);
                var rdpResource = dbLayer.ResourceRepository.FirstOrDefault(r =>
                    r.Subject == accessItem.AccountUserId &&
                    r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);

                if (rdpResource == null)
                {
                    return;
                }

                logger.Debug($"[{account.Id}] :: Удаляем стандарт для пользователя {accessItem.AccountUserId}");
                rdpResource.Subject = null;
                rdpResource.AccountSponsorId = null;
                rdpResource.AccountId = account.Id;
                dbLayer.ResourceRepository.Update(rdpResource);
            }
            else
            {
                CreateResource(account, buyRent1CData.WebRateCost, accessItem.AccountUserId, ResourceType.MyEntUserWeb,
                    resourcesService);
            }
        }

        /// <summary>
        /// Предоставление доступа к стандарт ресурсу
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="buyRent1CData">Данные биллинга аккаунта для покупки сервиса Аренда 1С</param>
        /// <param name="accessItem">Модель обновления доступа к Аренде 1С</param>
        /// <param name="resourcesService">Класс для работы с ресурсами сервиса</param>
        private void AddStandartAccess(Account account, AccountBillingDataForBuyRent1CDto buyRent1CData,
            UpdaterAccessRent1CRequestDto accessItem, IResourcesService resourcesService)
        {
            if (accessItem is { WebResourceId: not null, RdpResourceId: not null })
            {
                logger.Debug($"[{account.Id}] :: Предоставляем доступ к стандарт ресурсу для {accessItem.AccountUserId}");
                UpdateResource(account, accessItem.WebResourceId.Value, accessItem.AccountUserId);
                UpdateResource(account, accessItem.RdpResourceId.Value, accessItem.AccountUserId);
            }
            else if (accessItem.WebResourceId.HasValue)
            {
                logger.Debug(
                    $"[{account.Id}] :: Нет свободных ресурсов по рдп, добавляем новый и обновляем web для {accessItem.AccountUserId}");
                UpdateResource(account, accessItem.WebResourceId.Value, accessItem.AccountUserId);

                CreateResource(account, buyRent1CData.RdpRateCost, accessItem.AccountUserId, ResourceType.MyEntUser,
                    resourcesService);
            }
            else if (accessItem.RdpResourceId.HasValue)
            {
                logger.Debug(
                    $"[{account.Id}] :: Нет свободных ресурсов по web, добавляем новый web для {accessItem.AccountUserId}");
                UpdateResource(account, accessItem.RdpResourceId.Value, accessItem.AccountUserId);
                CreateResource(account, buyRent1CData.WebRateCost, accessItem.AccountUserId, ResourceType.MyEntUserWeb,
                    resourcesService);
            }
            else
            {
                logger.Debug($"[{account.Id}] :: Нет свободных ресурсов, добавляем новые для {accessItem.AccountUserId}");

                CreateResource(account, buyRent1CData.WebRateCost, accessItem.AccountUserId, ResourceType.MyEntUserWeb,
                    resourcesService);
                CreateResource(account, buyRent1CData.RdpRateCost, accessItem.AccountUserId, ResourceType.MyEntUser,
                    resourcesService);
            }
        }

        /// <summary>
        /// Проверка что пользователь уже имеет доступ
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="serviceTypeId">ID услуги сервиса</param>
        /// <returns>true - если пользователь уже имеет доступ</returns>
        private bool AccountUserHasAccess(Guid accountId, Guid accountUserId, Guid serviceTypeId)
        {
            var accountUserResource = dbLayer.ResourceRepository.FirstOrDefault(w => w.AccountId == accountId &&
                                                                                      w.Subject == accountUserId &&
                                                                                      w.BillingServiceTypeId == serviceTypeId);

            return accountUserResource != null;
        }

        /// <summary>
        /// Освобождение ресурсов
        /// </summary>
        /// <param name="account"></param>
        /// <param name="accessItem"></param>
        private void RemoveAccess(Account account, UpdaterAccessRent1CRequestDto accessItem)
        {
            var resources = dbLayer.ResourceRepository
                .AsQueryable()
                .Include(x => x.BillingServiceType)
                .ThenInclude(x => x.Service)
                .ThenInclude(x => x.ResourcesConfigurations)
                .Where(r => r.Subject == accessItem.AccountUserId && r.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise)
                .ToList();

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accessItem.AccountUserId);

            var resourcesForRecalculate = resources.Where(resource =>
                resource.AccountId == accountUser.Account.Id && resource.AccountSponsorId != null &&
                resource.AccountSponsorId != account.Id).ToList();

            var resourceConfigurations = resourcesForRecalculate.SelectMany(x =>
                x.BillingServiceType.Service.ResourcesConfigurations.Where(
                    z => z.AccountId == x.AccountSponsorId)).ToList();

            var accountIds = resourceConfigurations.Select(x => x.AccountId).ToList();
            var serviceIds = resourceConfigurations.Select(x => x.BillingServiceId).ToList();

            sender.Send(new RecalculateResourcesConfigurationCostCommand(accountIds, serviceIds, Clouds42Service.MyEnterprise)).Wait();

            var newResources = resourcesForRecalculate.Select(x =>
            {
                var newRes = x.Clone();
                newRes.AccountId = x.AccountSponsorId!.Value;
                newRes.AccountSponsorId = null;
                newRes.Subject = null;

                return newRes;

            }).ToList();

            dbLayer.BulkInsert(newResources);
            dbLayer.BulkDelete(resourcesForRecalculate);
            dbLayer.Save();

            var resourcesForUpdate = resources.Where(resource =>
                resource.AccountId != account.Id && resource.AccountSponsorId == account.Id).ToList();

            var forInsert = resourcesForUpdate.Select(x =>
            {
                var newRes = x.Clone();
                newRes.AccountId = account.Id;
                newRes.AccountSponsorId = null;
                newRes.Subject = null;

                return newRes;
            }).ToList();

            dbLayer.BulkInsert(forInsert);
            dbLayer.BulkDelete(resourcesForUpdate);
            dbLayer.Save();

            DeactivateDependServices(accountUser);

            var serverDbServiceType =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.AccessToServerDatabase)?.Id ??
                throw new InvalidOperationException("Не найдена услуга серверной базы");

            var onDisabledMyDatabasesServiceTypeForUsersTriggerDto =
                new OnDisabledMyDatabasesServiceTypeForUsersTriggerDto
                {
                    ServiceTypeId = serverDbServiceType, 
                    AccountUserIds = [accountUser.Id]
                };
            onDisabledMyDatabasesServiceTypeForUsersTrigger.Execute(onDisabledMyDatabasesServiceTypeForUsersTriggerDto);
        }

        /// <summary>
        /// Обновление ресурса
        /// </summary>
        /// <param name="account">Аккаунта</param>
        /// <param name="resourceId">ID ресурса</param>
        /// <param name="accountUserId">ID пользователя</param>
        private void UpdateResource(Account account, Guid resourceId, Guid accountUserId)
        {
            var resource = dbLayer.ResourceRepository.FirstOrDefault(r => r.Id == resourceId);

            var userHasAccess = AccountUserHasAccess(account.Id, accountUserId, resource.BillingServiceTypeId);
            if (userHasAccess)
                return;

            logger.Debug(
                $"[{account.Id}] :: Обновление ресурса {resource.BillingServiceType.SystemServiceType} для пользователя {accountUserId}");
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(a => a.Id == accountUserId);
            if (accountUser.AccountId != account.Id)
            {
                resource.AccountId = accountUser.AccountId;
                resource.AccountSponsorId = account.Id;
            }
            else
            {
                resource.AccountId = account.Id;
                if (resource.AccountSponsorId == account.Id)
                    resource.AccountSponsorId = null;
            }

            resource.Subject = accountUserId;
            dbLayer.ResourceRepository.Update(resource);
        }

        /// <summary>
        /// Добавление ресурса
        /// </summary>
        /// <param name="account"></param>
        /// <param name="cost"></param>
        /// <param name="subjectId"></param>
        /// <param name="resourceType"></param>
        /// <param name="resourcesService"></param>
        private void CreateResource(Account account, decimal cost, Guid subjectId, ResourceType resourceType,
            IResourcesService resourcesService)
        {
            logger.Debug(
                $"[{account.Id}] :: Добавление ресурса {resourceType} стоимость {cost} для пользователя {subjectId}");
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(a => a.Id == subjectId);
            Guid resourceAccountId;
            Guid? resourceAccountSponsorId;
            if (accountUser.AccountId == account.Id)
            {
                resourceAccountId = account.Id;
                resourceAccountSponsorId = null;
            }
            else
            {
                resourceAccountId = accountUser.AccountId;
                resourceAccountSponsorId = account.Id;
            }

            var serviceType = _billingServiceDataProvider.GetSystemServiceTypeOrThrowException(resourceType);
            var resource = new Resource
            {
                Id = Guid.NewGuid(),
                AccountId = resourceAccountId,
                AccountSponsorId = resourceAccountSponsorId,
                Cost = cost,
                Subject = subjectId,
                BillingServiceTypeId = serviceType.Id,
                IsFree = false
            };
            resourcesService.CreateResource(resource);
            recalculateResourcesConfigurationCostProvider.Recalculate(resource.AccountId,
                resource.BillingServiceType.Service);
        }


        /// <summary>
        /// Проверить возможность применить спонсирование
        /// </summary>
        /// <param name="accountSponsorId">ID аккаунта-спонсора</param>
        /// <param name="sponsoredAccountUserEmail">Почта спонсируемого аккаунта</param>
        /// <param name="sponsoredAccountUser">Спонсируемый пользователь</param>
        /// <param name="errorModel">Модель с ошибкой применения</param>
        /// <returns>Результат проверки</returns>
        private bool CanApplySponsorship(Guid accountSponsorId, string sponsoredAccountUserEmail,
            AccountUser sponsoredAccountUser, out SearchExternalUserRent1CResultDto errorModel)
        {
            var account = dbLayer.AccountsRepository.GetAccount(accountSponsorId);
            var sponsorAccountUser =
                dbLayer.AccountUsersRepository.FirstOrDefault(acUs => acUs.AccountId == accountSponsorId);

            if (!CanApplySponsorshipBySponsorRent1CResConfig(accountSponsorId, out var applySponsorshipErrorMessage))
            {
                errorModel = CreateErrorSearchExternalUserRent1CResultDmObj(applySponsorshipErrorMessage);
                return false;
            }

            var accountIsDemo = checkAccountDataProvider.CheckAccountIsDemo(account);
            if (accountIsDemo)
            {
                applySponsorshipErrorMessage =
                    "Спонсирование пользователей внешних аккаунтов будет доступно после оплаты сервиса.";
                logger.Warn(applySponsorshipErrorMessage);
                errorModel = CreateErrorSearchExternalUserRent1CResultDmObj(applySponsorshipErrorMessage);
                return false;
            }

            if (sponsoredAccountUser == null)
            {
                logger.Warn($"Пользователь с email {sponsoredAccountUserEmail} не найден");
                errorModel =
                    CreateErrorSearchExternalUserRent1CResultDmObj(
                        $"Не найден пользователь по эл.почте {sponsoredAccountUserEmail}");
                return false;
            }

            if (!CanApplySposnsorshipBySponsoredAccountRent1CResConfig(accountSponsorId, sponsoredAccountUser,
                out applySponsorshipErrorMessage))
            {
                errorModel = CreateErrorSearchExternalUserRent1CResultDmObj(applySponsorshipErrorMessage);
                return false;
            }

            if (sponsoredAccountUser.AccountId == accountSponsorId)
            {
                logger.Warn(
                    $"Попытка добавить аккаунтом {accountSponsorId} своего же пользователя {sponsoredAccountUser.Login}");
                errorModel =
                    CreateErrorSearchExternalUserRent1CResultDmObj(
                        "Можно добавлять пользователей только внешних аккаунтов.");
                return false;
            }

            if (!CanApplySponsorshipByLocale(sponsorAccountUser, sponsoredAccountUser,
                out applySponsorshipErrorMessage))
            {
                errorModel = CreateErrorSearchExternalUserRent1CResultDmObj(applySponsorshipErrorMessage);
                return false;
            }

            errorModel = new SearchExternalUserRent1CResultDto();
            return true;
        }

        /// <summary>
        /// Проверить возможность применить спонсирование по конфигурации Аренды спонсора
        /// </summary>
        /// <param name="accountSponsorId">ID аккаунта-спонсора</param>
        /// <param name="errorMessage">Сообщение об ошибке применения спонсирования</param>
        /// <returns>Результат проверки</returns>
        private bool CanApplySponsorshipBySponsorRent1CResConfig(Guid accountSponsorId, out string errorMessage)
        {
            var rent1CResourceConfiguration =
                resourceConfigurationDataProvider.GetResourceConfiguration(accountSponsorId,
                    Clouds42Service.MyEnterprise);

            if (rent1CResourceConfiguration is { FrozenValue: true })
            {
                logger.Warn($"Сервис {Clouds42Service.MyEnterprise} у аккаунта {accountSponsorId} заблокирован");
                errorMessage = "Ваш сервис заблокирован. Пожалуйста, оплатите сервис.";
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }

        /// <summary>
        /// Проверить возможность применить спонсирование по конфигурации Аренды спонсируемого аккаунта
        /// </summary>
        /// <param name="accountSponsorId">ID аккаунта-спонсора</param>
        /// <param name="sponsoredAccountUser">Спонсируемый пользователь</param>
        /// <param name="errorMessage">Сообщение об ошибке применения спонсирования</param>
        /// <returns>Результат проверки</returns>
        private bool CanApplySposnsorshipBySponsoredAccountRent1CResConfig(Guid accountSponsorId,
            AccountUser sponsoredAccountUser, out string errorMessage)
        {
            var rent1CResourceConfiguration =
                resourceConfigurationDataProvider.GetResourceConfiguration(sponsoredAccountUser.AccountId,
                    Clouds42Service.MyEnterprise);

            if (rent1CResourceConfiguration == null)
            {
                logger.Warn(
                    $"У аккаунта пользователя:{sponsoredAccountUser.AccountId} не найден сервис {Clouds42Service.MyEnterprise} в ResourceConfiguration");
                errorMessage = $"Пользователь {sponsoredAccountUser.Email} принадлежит аккаунту, для которого сервис " +
                               $"\"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountSponsorId)}\" неактивен.";
                return false;
            }

            var resources = dbLayer.ResourceRepository.Where(res =>
                res.Subject == sponsoredAccountUser.Id && res.AccountSponsorId != null &&
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise).ToList();

            if (resources.Any())
            {
                errorMessage = $"Пользователь '{sponsoredAccountUser.Email}' уже проспонсирован.";
                logger.Warn(errorMessage);
                return false;
            }

            errorMessage = "";
            return true;
        }

        /// <summary>
        /// Проверить возможность применить спонсирование по локали
        /// </summary>
        /// <param name="sponsorAccountUser">Пользователь-спонсор</param>
        /// <param name="sponsoredAccountUser">Спонсируемый пользователь</param>
        /// <param name="errorMessage">Сообщение об ошибке применения спонсирования</param>
        /// <returns>Результат проверки</returns>
        private bool CanApplySponsorshipByLocale(AccountUser sponsorAccountUser, AccountUser sponsoredAccountUser,
            out string errorMessage)
        {
            var sponsorAccountLocale = accountConfigurationDataProvider.GetAccountLocale(sponsorAccountUser.AccountId);
            var sponsoredAccountLocale =
                accountConfigurationDataProvider.GetAccountLocale(sponsoredAccountUser.AccountId);

            if (sponsorAccountLocale.Name == sponsoredAccountLocale.Name)
            {
                errorMessage = string.Empty;
                return true;
            }

            var sponsorAccounUserCountry = DefineCountryNameByLocaleName(sponsorAccountLocale.Name);
            var sponsoredAccountUserCountry = DefineCountryNameByLocaleName(sponsoredAccountLocale.Name);

            errorMessage =
                $"Допускается спонсирование только пользователей из {sponsorAccounUserCountry}, пользователь '{sponsoredAccountUser.Email}' - из {sponsoredAccountUserCountry}";
            return false;
        }

        /// <summary>
        /// Определить название страны по названию локали
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Название страны</returns>
        private string DefineCountryNameByLocaleName(string localeName)
            => !_mapLocaleNameToCountryNameDictionary.ContainsKey(localeName)
                ? string.Empty
                : _mapLocaleNameToCountryNameDictionary[localeName];

        /// <summary>
        /// Создать модель результата поиска внешнего пользователя для спонсирования с ошибкой
        /// </summary>
        /// <returns>Модель результата поиска внешнего пользователя для спонсирования с ошибкой</returns>
        private SearchExternalUserRent1CResultDto CreateErrorSearchExternalUserRent1CResultDmObj(string errorMessage)
            => new()
            {
                ErrorMessage = errorMessage
            };

            
        /// <summary>
        /// Выключить все сервисы связанные с арендой 
        /// </summary>
        /// <param name="accountUser"></param>
        private void DeactivateDependServices(AccountUser accountUser)
        {
            var resourcesTypes = new List<ResourceType> { ResourceType.MyEntUser, ResourceType.MyEntUserWeb };
            
            var activeDependServiceTypes =  dbLayer.ResourceRepository
                .AsQueryable()
                .Where(x => x.Subject == accountUser.Id)
                .Where(x => x.BillingServiceType.Service.SystemService != Clouds42Service.MyDatabases &&
                            x.BillingServiceType.Service.SystemService != Clouds42Service.MyEnterprise &&
                            x.BillingServiceType.DependServiceType.SystemServiceType.HasValue &&
                            resourcesTypes.Contains(x.BillingServiceType.DependServiceType.SystemServiceType.Value))
                .Select(x => x.BillingServiceType)
                .Distinct()
                .ToList();

            if (!activeDependServiceTypes.Any())
            {
                logger.Info("Нет зависимых сервисов");
                return;
            }

            foreach (var billingServiceType in activeDependServiceTypes)
            {
                logger.Info($"Выключаю сервис {billingServiceType.Service.Name} для юзера {accountUser.Id}");
                billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(
                    accountUser.AccountId, billingServiceType.ServiceId, false,
                    new List<CalculateBillingServiceTypeDto>(),
                    new List<CalculateBillingServiceTypeDto>
                    {
                        new()
                        {
                            BillingServiceTypeId = billingServiceType.Id,
                            Status = false,
                            Subject = accountUser.Id
                        }
                    });
            }
        }
    }
}
