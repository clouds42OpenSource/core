﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.ModelProcessors;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Billing.Billing.Internal.ModelProcessors
{
    /// <summary>
    /// Процессор для работы с моделью сервиса Мой диск
    /// </summary>
    public class ServiceMyDiskDomainModelProcessor(IServiceProvider serviceProvider)
        : IServiceMyDiskDomainModelProcessor
    {
        private readonly IUnitOfWork _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly IServiceStatusHelper _serviceStatusHelper = serviceProvider.GetRequiredService<IServiceStatusHelper>();
        private readonly ICloud42ServiceHelper _cloud42ServiceHelper = serviceProvider.GetRequiredService<ICloud42ServiceHelper>();
        private readonly IResourceConfigurationDataProvider _resourceConfigurationDataProvider = serviceProvider.GetRequiredService<IResourceConfigurationDataProvider>();
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider = serviceProvider.GetRequiredService<IAccountConfigurationDataProvider>();

        /// <summary>
        /// Получить модель сервиса Мой диск
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель сервиса Мой диск</returns>
        public ServiceMyDiskDto GetModel(Guid accountId)
        {
            var result = new ServiceMyDiskDto();
            InitializeMyDiskInfo(accountId, result);
            InitializeAccountInfo(accountId, result);
            InitializeResourceConfigurationInfo(accountId, result);
            GetTaskQueueId(accountId, result);

            return result;
        }

        /// <summary>
        /// Инициализировать информацию о состоянии
        /// дискового пространства для модели сервиса Мой диск 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceMyDiskDc">Модель сервиса Мой диск</param>
        private void InitializeMyDiskInfo(Guid accountId, ServiceMyDiskDto serviceMyDiskDc)
        {
            var myDiskInfo = _cloud42ServiceHelper.GetMyDiskInfo(accountId);

            serviceMyDiskDc.ClinetFilesCalculateDate = myDiskInfo.ClinetFilesCalculateDate;

            serviceMyDiskDc.MyDiskVolumeInfo = new MyDiskVolumeInfoDto
            {
                ClientFilesSize = myDiskInfo.ClientFilesSize,
                FileDataBaseSize = myDiskInfo.FileDataBaseSize,
                ServerDataBaseSize = myDiskInfo.ServerDataBaseSize,
                FreeSizeOnDisk = myDiskInfo.FreeSizeOnDisk,
                AvailableSize = myDiskInfo.AvailableSize
            };

            serviceMyDiskDc.CurrentTariff = myDiskInfo.BillingInfo.CurrentTariffInGb;
        }

        /// <summary>
        /// Инициализировать информацию об аккаунте
        /// для модели сервиса Мой диск 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceMyDiskDc">Модель сервиса Мой диск</param>
        private void InitializeAccountInfo(Guid accountId, ServiceMyDiskDto serviceMyDiskDc)
        {
            var locale = _accountConfigurationDataProvider.GetAccountLocale(accountId);
            serviceMyDiskDc.AccountIsVip = _accountConfigurationDataProvider.IsVipAccount(accountId);
            serviceMyDiskDc.Locale = new LocaleDto
            {
                Name = locale.Name,
                Currency = locale.Currency
            };
        }

        /// <summary>
        /// Инициализировать информацию о конфигурации ресурса
        /// для модели сервиса Мой диск 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceMyDiskDc">Модель сервиса Мой диск</param>
        private void InitializeResourceConfigurationInfo(Guid accountId,
            ServiceMyDiskDto serviceMyDiskDc)
        {
            var resourcesConfiguration =
                _resourceConfigurationDataProvider.GetResourceConfigurationOrThrowException(accountId,
                    Clouds42Service.MyDisk);

            var expireDate = _resourceConfigurationDataProvider.GetExpireDate(resourcesConfiguration);

            if (expireDate == null)
            {
                serviceMyDiskDc.ServiceStatus = null;
                return;
            }

            serviceMyDiskDc.ExpireDate = expireDate.Value;
            serviceMyDiskDc.MonthlyCost = resourcesConfiguration.Cost;
            serviceMyDiskDc.ServiceStatus =
                _serviceStatusHelper.CreateServiceStatusModel(accountId, resourcesConfiguration);
        }

        /// <summary>
        /// Получить Id задачи для пересчета дискового пространства
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceMyDiskDc">Модель сервиса Мой диск</param>
        private void GetTaskQueueId(Guid accountId, ServiceMyDiskDto serviceMyDiskDc)
        {
            var taskId = CloudConfigurationProvider.MyDiskRecalculation.GetCoreWorkerTaskId();

            var taskParameters = (from coreWorkerTaskQueue in _dbLayer.CoreWorkerTasksQueueRepository.WhereLazy()
                                  join coreWorkerTaskParameter in _dbLayer.GetGenericRepository<CoreWorkerTaskParameter>().WhereLazy()
                                      on
                                      coreWorkerTaskQueue.Id equals coreWorkerTaskParameter.TaskId
                                  where coreWorkerTaskQueue.CoreWorkerTaskId == taskId &&
                                        (coreWorkerTaskQueue.Status != CloudTaskQueueStatus.Ready.ToString() &&
                                         coreWorkerTaskQueue.Status != CloudTaskQueueStatus.Error.ToString())
                                  select new
                                  {
                                      TaskQueueId = coreWorkerTaskQueue.Id,
                                      TaskParameters = coreWorkerTaskParameter.TaskParams
                                  }).ToList();

            serviceMyDiskDc.RecalculationTaskId = taskParameters
                .FirstOrDefault(w => w.TaskParameters.Contains(accountId.ToString("D")))?.TaskQueueId;
        }
    }
}