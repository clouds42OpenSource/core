﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Billing.Billing.Internal
{
    /// <summary>
    /// Провайдер для работы с ресурсами
    /// </summary>
    public class ResourcesProvider(
        IUnitOfWork dbLayer,
        ICheckAccountDataProvider checkAccountProvider,
        IRent1CManageHelper rent1CManageHelper,
        ICloud42ServiceHelper cloud42ServiceHelper,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        IAdditionalResourcesDataProvider additionalResourcesDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
        : IResourcesProvider
    {
        /// <summary>
        /// Получить модель управления ресурсом
        /// </summary>
        /// <param name="resourceId">Id ресурса</param>
        /// <returns>Модель управления ресурсом</returns>
        public Rent1CUserManagerDto GetRent1CUserManagerDc(Guid resourceId) =>
            (from resource in dbLayer.ResourceRepository.WhereLazy()
             join user in dbLayer.AccountUsersRepository.WhereLazy() on resource.Subject equals user.Id into users
             from user in users.DefaultIfEmpty()
             join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource
                 .BillingServiceTypeId equals serviceType.Id
             where resource.Id == resourceId
             select new Rent1CUserManagerDto
             {
                 ResourceId = resource.Id,
                 Cost = resource.Cost,
                 Name = user != null ? user.LastName + " " + user.FirstName + " " + user.MiddleName : "-",
                 Login = user != null ? user.Login : "-",
                 ServiceTypeName = serviceType.Name
             }).FirstOrDefault() ??
            throw new NotFoundException($"Не удалось получить данные по ресурсу {resourceId}");

        /// <summary>
        /// Получить информацию о демо периоде аренды 1С
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Информация о демо периоде аренды 1С</returns>
        public Rent1CServiceManagerDto GetRent1DemoPeriodInfo(Guid accountId)
        {
            var account = GetAccountById(accountId);

            var resConfig = account.BillingAccount.ResourcesConfigurations.FirstOrDefault(r =>
                                r.BillingService.SystemService == Clouds42Service.MyEnterprise)
                            ?? throw new NotFoundException(
                                $"Ресурс конфигурация сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)} для аккаунта {accountId} не найдена");

            var additionalResourcesData = additionalResourcesDataProvider.GetAdditionalResourcesData(account.Id);

            return new Rent1CServiceManagerDto
            {
                ExpireDate = resConfig.ExpireDateValue,
                MaxDemoExpiredDate = GetMaxDemoExpiredDateForRent1C(account),
                AdditionalResourceCost = additionalResourcesData.AdditionalResourceCost,
                AdditionalResourceName = additionalResourcesData.AdditionalResourceName
            };
        }

        /// <summary>
        /// Изменить стоимость ресурса
        /// </summary>
        /// <param name="rent1CUserManagerDc">Модель управления ресурсом</param>
        /// <param name="accountId">Id аккаунта</param>
        public void EditResourceCost(Rent1CUserManagerDto rent1CUserManagerDc, Guid accountId)
        {
            if (rent1CUserManagerDc.Cost < 0)
                throw new ValidateException("Стоимость ресурса должна быть больше 0");

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var resource = GetResource(rent1CUserManagerDc.ResourceId);
                resource.Cost = rent1CUserManagerDc.Cost;
                dbLayer.ResourceRepository.Update(resource);
                dbLayer.Save();
                recalculateResourcesConfigurationCostProvider.Recalculate(accountId, resource.BillingServiceType.Service);
                transaction.Commit();
            }
            catch (Exception ex )
            {
                handlerException.Handle(ex, $"[Ошибка Редактирования ресурса] {rent1CUserManagerDc.ResourceId}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Получить ресурс
        /// </summary>
        /// <param name="resourceId">Id ресурса</param>
        /// <returns>Ресурс</returns>
        public Resource GetResource(Guid resourceId) =>
            dbLayer.ResourceRepository.FirstOrDefault(w => w.Id == resourceId) ??
            throw new InvalidOperationException($"Не удалось найти ресурс по Id {resourceId}");

        /// <summary>
        /// Изменить демо период для Аренды 1С и начислить свободные ресурсы 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель изменения аренды 1С</param>
        public void ChangeRent1CDemoPeriodExpireDateAndRegisterDemoResources(Guid accountId,
            Rent1CServiceManagerDto model)
        {
            try
            {
                if (!checkAccountProvider.CheckAccountIsDemo(accountId))
                    throw new InvalidOperationException(
                        $"Невозможно изменить демо период сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)} для аккаунта {accountId}. Причина: Аккаунт не в демо режиме.");

                rent1CManageHelper.ManageRent1C(model, accountId);

                logger.Trace($"Демо период Аренды 1С для аккаунта {accountId} успешно изменен");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка изменения демо периода Аренды 1С для аккаунта] {accountId}");
                throw;
            }
        }

        /// <summary>
        /// Получить модель управления сервисом "Мой диск"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель управления сервисом "Мой диск"</returns>
        public ServiceMyDiskManagerDto GetServiceMyDiskManagerInfo(Guid accountId)
        {
            var accountWithConfiguration = accountConfigurationDataProvider.GetAccountWithConfiguration(accountId);
            var locale = accountConfigurationDataProvider.GetAccountLocale(accountId);

            var resourcesConfiguration =
                resourceConfigurationDataProvider.GetResourceConfigurationOrThrowException(accountId,
                    Clouds42Service.MyDisk);

            var diskInfo = cloud42ServiceHelper.GetMyDiskInfo(accountId) ?? throw new NotFoundException(
                               $"Не удалось получить информацию по состоянию дискового пространства для аккаунта {accountId}");

            var expireDate = resourceConfigurationDataProvider.GetExpireDateOrThrowException(resourcesConfiguration);

            return new ServiceMyDiskManagerDto
            {
                AccountIsVip = accountWithConfiguration.AccountConfiguration.IsVip,
                Cost = resourcesConfiguration.Cost,
                DiscountGroup = resourcesConfiguration.DiscountGroup,
                ExpireDate = expireDate,
                Tariff = diskInfo.BillingInfo.CurrentTariffInGb,
                Locale = new LocaleDto
                {
                    Name = locale.Name,
                    Currency = locale.Currency
                }
            };
        }

        /// <summary>
        /// Получить аккаунт по Id 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Аккаунт</returns>
        private Account GetAccountById(Guid accountId) => dbLayer.AccountsRepository.GetAccount(accountId) ??
                                                          throw new NotFoundException(
                                                              $"Аккаунт по номеру {accountId} не найден");

        /// <summary>
        /// Получить максимально возможный срок действия демо периода
        /// </summary>
        /// <param name="account">Аккаунт облака</param>
        /// <returns>Максимально возможный срок действия демо периода</returns>
        private DateTime? GetMaxDemoExpiredDateForRent1C(Account account)
        {
            return account.RegistrationDate?.AddMonths(1);
        }
    }
}
