﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums.Billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.InvoiceCalculator.Providers
{
    public class InvoiceCalculatorProvider(
        ILogger42 logger,
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IServiceCalculatorsProvider serviceCalculatorProvider)
        : IInvoiceCalculatorProvider
    {
        /// <inheritdoc/>
        public IInvoiceCalculator GetInvoiceCalculatorForAccount(Guid accountId, InvoicePurposeType invoiceType)
        {
            var productTypes = GetServiceCategoriesByCalculatorType(invoiceType);
            return LoadInvoiceCalculatorAux(accountId, productTypes);
        }

        /// <inheritdoc/>
        public IInvoiceCalculator GetInvoiceCalculatorForAccount(Guid accountId,
            InvoicePurposeType invoiceType,
            int? payPeriod, IEnumerable<IInvoiceProductCalculatorInput> serviceInputs)
        {
            var products = GetServiceCategoriesByCalculatorType(invoiceType);
            serviceInputs = (serviceInputs ?? [])
                .Where(x => products.Contains(x.Identifier.ServiceCalculatorType));
            var calculator = LoadInvoiceCalculatorAux(
                accountId,
                services: serviceInputs);
            calculator.Set(payPeriod, serviceInputs, true);
            return calculator;
        }

        private IInvoiceCalculator LoadInvoiceCalculatorAux(Guid accountId,
            IEnumerable<ServiceCalculatorType>? serviceTypes = null,
            IEnumerable<IHasProductIdentifier>? services = null)
        {
            var account = dbLayer.AccountsRepository.GetAccount(accountId)
              ?? throw new NotFoundException($"Аккаунт {accountId} не найден");

            var accountSupplier = accountConfigurationDataProvider
                .GetAccountSupplier(accountId);
            var accountLocale = accountConfigurationDataProvider
                .GetAccountLocale(accountId);

            IEnumerable<IServiceCalculator> serviceCalculators;
            if (services == null)
                serviceCalculators = serviceCalculatorProvider
                    .GetProductCalculatorForAccount(accountId, serviceTypes);
            else
            {
                serviceCalculators = serviceCalculatorProvider
                    .GetProductCalculatorForAccount(accountId, services.Select(x => x.Identifier));
            }

            var productCalculators = new List<IServiceCalculator>();
            foreach (var group in serviceCalculators.GroupBy(x => x.Identifier))
            {
                if (group.Count() > 1)
                    logger.Warn($"Duplicate product calculators are found for identifier {group.Key}");

                productCalculators.Add(group.First());
            }
            return new Impl.InvoiceCalculator(new Impl.InvoiceCalculatorSettings
            {
                AccountId = account.Id,
                BuyerName = account.AccountCaption,
                SupplierId = accountSupplier.Id,
                SupplierName = accountSupplier.Name,
                Currency = accountLocale.Currency,
                CurrencyCode = accountLocale.CurrencyCode,
                ProductCalculators = productCalculators
            });
        }
        private static IEnumerable<ServiceCalculatorType> GetServiceCategoriesByCalculatorType(
            InvoicePurposeType invoiceType)
        {
            return invoiceType switch
            {
                InvoicePurposeType.BaseBillingServices =>
                [
                    ServiceCalculatorType.DiskSpace, ServiceCalculatorType.SubscriptionService,
                    ServiceCalculatorType.AdditionalResources
                ],
                InvoicePurposeType.AdditionalBillingServices => new[]
                {
                    ServiceCalculatorType.Esdl, ServiceCalculatorType.Recognition
                },
                _ => throw new ArgumentOutOfRangeException(nameof(invoiceType), "Unkown invoice calculator type")
            };
        }
    }
}
