﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Billing.InvoiceCalculator.Providers
{
    public class RootServiceCalculatorProvider : IServiceCalculatorsProvider
    {
        readonly IDictionary<ServiceCalculatorType, IServiceCalculatorsProvider> _providers
            = new Dictionary<ServiceCalculatorType, IServiceCalculatorsProvider>();

        public RootServiceCalculatorProvider AddTypeProvider(ServiceCalculatorType type, IServiceCalculatorsProvider provider)
        {
            _providers[type] = provider ?? throw new ArgumentNullException(nameof(provider));
            return this;
        }


        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(
            Guid accountId,
            IEnumerable<ServiceCalculatorType>? typeFilter = null)
        {
            var result = new List<IServiceCalculator>();
            var productTypes = typeFilter ?? _providers.Keys;
            foreach (var productType in productTypes)
            {
                if (!_providers.TryGetValue(productType, out var provider))
                    continue;
                var calculators = provider.GetProductCalculatorForAccount(accountId, productTypes);
                result.AddRange(calculators);
            }
            return result;
        }

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorIdentifier> serviceFilter)
        {
            var result = new List<IServiceCalculator>();
            var servicesByProductTypes = serviceFilter
                .GroupBy(x => x.ServiceCalculatorType);
            foreach (var group in servicesByProductTypes)
            {
                if (!_providers.TryGetValue(group.Key, out var provider))
                    continue;
                var calculators = provider.GetProductCalculatorForAccount(accountId, group);
                result.AddRange(calculators);
            }
            return result;
        }
    }
}
