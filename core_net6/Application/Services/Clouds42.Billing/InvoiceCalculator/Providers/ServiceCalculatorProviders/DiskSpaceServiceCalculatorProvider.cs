﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.InvoiceCalculator.Controls;
using Clouds42.Billing.InvoiceCalculator.Helpers;
using Clouds42.Billing.InvoiceCalculator.Impl;
using Clouds42.Billing.InvoiceCalculator.Processors;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Providers.ServiceCalculatorProviders
{
    public class DiskSpaceServiceCalculatorProvider(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IBonusRewardConfigurationProvider bonusRewardConfigurationProvider)
        : IServiceCalculatorsProvider
    {
        readonly Lazy<int> _myDiskFreeTariffSize = new(
            CloudConfigurationProvider.BillingServices.MyDisk.GetMyDiskFreeTariffSize
        );

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorType>? typeFilter = null)
        {
            if (typeFilter != null && !typeFilter.Contains(ServiceCalculatorType.DiskSpace))
                return [];
            return GetProductCalculatorForAccountAux(accountId);
        }

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorIdentifier> serviceFilter)
        {
            if (serviceFilter == null)
                return [];

            var serviceIdsfilter = serviceFilter
                .Where(x => x.ServiceCalculatorType == ServiceCalculatorType.DiskSpace)
                .Select(x => new { Valid = Guid.TryParse(x.Id, out Guid id), Id = id })
                .Where(x => x.Valid)
                .Select(x => x.Id);
            return GetProductCalculatorForAccountAux(accountId, serviceIdsfilter);
        }

        private IEnumerable<IServiceCalculator> GetProductCalculatorForAccountAux(Guid accountId,
            IEnumerable<Guid>? filterIds = null)
        {
            if (filterIds != null && !filterIds.Any())
                return [];

            var serviceType = dbLayer
                .BillingServiceTypeRepository
                .FirstOrDefault(w =>
                w.SystemServiceType == ResourceType.DiskSpace);

            if (filterIds != null && !filterIds.Contains(serviceType.Id))
                return Enumerable.Empty<IInvoiceProductCalculator>();

            var accountConfiguration = accountConfigurationDataProvider
                .GetAccountConfiguration(accountId);

            var activeDiskResource = dbLayer.DiskResourceRepository
                .FirstOrDefault(r =>
                    r.Resource.AccountId == accountId &&
                    r.Resource.Subject == accountId);

            var settings = new ServiceCalculatorSettings
            {
                Identifier = new ServiceCalculatorIdentifier(ServiceCalculatorType.DiskSpace, serviceType.Id),
                Name = serviceType.Service.Name,
                IsActiveService = true,
                SetPayPeriod = (int)PayPeriod.Month1,
                PayPeriodControl = ServiceQuantityControlResolver.GetControl(ResourceType.DiskSpace, serviceType.BillingType),
                NomenclatureName = Clouds42Service.MyDisk.ToString(),
                NomenclatureDescription = "Мой Диск",
                ServiceTypeId = serviceType.Id
            };

            if (accountConfiguration.IsVip)
            {
                settings.Description = serviceType.Name;
                settings.SetQuantity = activeDiskResource?.VolumeGb ?? _myDiskFreeTariffSize.Value;
                settings.QuantityControl = QuantityControls.Constant(settings.SetQuantity);
                settings.CalculationProcessor = CompositeProcessor.Compose(
                    TotalPriceForQuantityProcessor.Create(new Dictionary<int, decimal> { { settings.SetQuantity, activeDiskResource?.Resource.Cost ?? default } }),
                    ZeroBonusProcessor.Get());
            }
            else
            {
                var diskRate = dbLayer.RateRepository.FirstOrThrowException(r =>
                           r.BillingServiceTypeId == serviceType.Id &&
                           r.LocaleId == accountConfiguration.LocaleId &&
                           r.UpperBound > _myDiskFreeTariffSize.Value);

                settings.FreeQuantity = _myDiskFreeTariffSize.Value;
                settings.Description = $"{serviceType.Service.Name}, (свыше {_myDiskFreeTariffSize.Value} Гб)";
                settings.SetQuantity = Math.Max((activeDiskResource?.VolumeGb ?? _myDiskFreeTariffSize.Value) - _myDiskFreeTariffSize.Value, 0);
                settings.QuantityControl =
                    ServiceQuantityControlResolver.GetControl(serviceType.SystemServiceType, serviceType.BillingType);
                settings.CalculationProcessor = CompositeProcessor.Compose(
                    ConstantRateProcessor.Create(diskRate.Cost),
                    SimpleTotalPriceProcessor.Create(false),
                    BonusByPayPeriodProcessor.Create(bonusRewardConfigurationProvider.GetPayPeriodToBonusRewardMap()));
            }
            return new[] { new ServiceCalculator(settings) };
        }
    }
}
