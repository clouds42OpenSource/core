﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.InvoiceCalculator.Controls;
using Clouds42.Billing.InvoiceCalculator.Impl;
using Clouds42.Billing.InvoiceCalculator.Processors;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Providers.ServiceCalculatorProviders
{
    public class EsdlProductCalculatorProvider(
        IUnitOfWork dbLayer,
        ISystemServiceTypeDataProvider systemServiceTypeDataProvider)
        : IServiceCalculatorsProvider
    {
        private ServiceCalculatorIdentifier EsdlCalculatorIdentifier => new(ServiceCalculatorType.Esdl);

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorType>? typeFilter = null)
        {
            if (typeFilter != null && !typeFilter.Contains(ServiceCalculatorType.Esdl))
                return [];
            return GetProductCalculatorForAccountAux();
        }

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorIdentifier> serviceFilter)
        {
            if (serviceFilter != null && !serviceFilter.Contains(EsdlCalculatorIdentifier))
                return [];
            return GetProductCalculatorForAccountAux();
        }

        private IEnumerable<IServiceCalculator> GetProductCalculatorForAccountAux()
        {
            var esdlRate = dbLayer.RateRepository
                 .FirstOrDefault(r =>
                     r.BillingServiceType.Service.SystemService == Clouds42Service.Esdl &&
                     r.BillingServiceType.SystemServiceType.HasValue);

            var serviceTypeId = systemServiceTypeDataProvider.GetServiceTypeIdBySystemServiceType(ResourceType.Esdl);

            var settings = new ServiceCalculatorSettings
            {
                Identifier = EsdlCalculatorIdentifier,
                Name = "Fasta",
                Description = NomenclaturesNameConstants.Esdl,
                NomenclatureDescription = $"{NomenclaturesNameConstants.Esdl} Лицензия",
                NomenclatureName = Clouds42Service.Esdl.ToString(),
                ServiceTypeId = serviceTypeId,
                IsActiveService = true,
                SetQuantity = 1,
                QuantityControl = QuantityControls.Ranged(0, 3),
                CalculationProcessor = CompositeProcessor.Compose(
                    ConstantRateProcessor.Create(esdlRate.Cost),
                    SimpleTotalPriceProcessor.Create(true),
                    ZeroBonusProcessor.Get())
            };


            return new[] { new ServiceCalculator(settings) };
        }
    }
}
