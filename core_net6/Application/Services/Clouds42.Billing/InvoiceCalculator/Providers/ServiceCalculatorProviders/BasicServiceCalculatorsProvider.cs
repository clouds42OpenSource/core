﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.InvoiceCalculator.Controls;
using Clouds42.Billing.InvoiceCalculator.Helpers;
using Clouds42.Billing.InvoiceCalculator.Impl;
using Clouds42.Billing.InvoiceCalculator.Processors;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.Locales;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using CommonLib.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Providers.ServiceCalculatorProviders
{
    public class BasicServiceCalculatorsProvider : IServiceCalculatorsProvider
    {
        readonly IBillingServiceInfoProvider _billingServiceInfoProvider;
        readonly IBonusRewardConfigurationProvider _bonusRewardProvider;
        readonly IMyDatabasesServiceTypeBillingDataProvider _myDatabaseServiceTypesProvider;
        readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider;
        private readonly ICloudLocalizer _cloudLocalizer;

        readonly Lazy<IDictionary<int, decimal>> _payPeriodToBonusRewardMapLazy;
        IDictionary<int, decimal> PayPeriodToBonusRewardMap => _payPeriodToBonusRewardMapLazy.Value;

        public BasicServiceCalculatorsProvider(
            IAccountConfigurationDataProvider accountConfigurationDataProvider,
            IMyDatabasesServiceTypeBillingDataProvider myDatabaseServiceTypesProvider,
            IBillingServiceInfoProvider billingServiceInfoProvider,
            IBonusRewardConfigurationProvider bonusRewardProvider, ICloudLocalizer cloudLocalizer)
        {
            _accountConfigurationDataProvider = accountConfigurationDataProvider;
            _bonusRewardProvider = bonusRewardProvider;
            _cloudLocalizer = cloudLocalizer;
            _billingServiceInfoProvider = billingServiceInfoProvider;
            _myDatabaseServiceTypesProvider = myDatabaseServiceTypesProvider;
            _payPeriodToBonusRewardMapLazy = new Lazy<IDictionary<int, decimal>>(() =>
                _bonusRewardProvider.GetPayPeriodToBonusRewardMap());
        }

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorType>? typeFilter = null)
        {
            if (typeFilter != null && !typeFilter.Contains(ServiceCalculatorType.SubscriptionService))
                return [];
            return GetProductCalculatorForAccountAux(accountId);
        }

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorIdentifier> serviceFilter)
        {
            var filterIds = serviceFilter
                    .Where(x => x.ServiceCalculatorType == ServiceCalculatorType.SubscriptionService)
                    .Select(x => new { Parsed = Guid.TryParse(x.Id, out var id), Id = id })
                    .Where(x => x.Parsed)
                    .Select(x => x.Id);
            if (!filterIds.Any())
                return [];

            return GetProductCalculatorForAccountAux(accountId, filterIds);
        }

        private IEnumerable<IServiceCalculator> GetProductCalculatorForAccountAux(Guid accountId,
            IEnumerable<Guid>? filterIds = null)
        {
            if (filterIds != null && !filterIds.Any())
                return [];

            var accountConfiguration = _accountConfigurationDataProvider.GetAccountConfiguration(accountId);
            var result = GetBasicServiceCalculatorsForAccountAux(accountId, accountConfiguration.IsVip, filterIds);
            result.AddRange(GetMyDatabaseServiceCalculatorsForAccountAux(accountId, accountConfiguration.IsVip, filterIds));
            return result.OrderByDescending(rt => rt.ResourceType != null);
        }

        private List<IServiceCalculator> GetBasicServiceCalculatorsForAccountAux(Guid accountId, bool isVipAccount,
            IEnumerable<Guid>? filterIds = null)
        {
            var accountServices = _billingServiceInfoProvider
               .GetAccountServicesData(accountId);

            return accountServices
                .SelectMany(service => _billingServiceInfoProvider
                    .GetBillingServiceTypesInfo(service.Id, accountId)
                    .Where(serviceType => filterIds == null || filterIds.Contains(serviceType.Id))
                    .Select(serviceType => new { serviceType, service }))
                .DistinctBy(x => x.serviceType.Id)
                .Select(x => CreateBasicServiceCalculator(accountId, isVipAccount, x.serviceType, x.service))
                .ToList();
        }

        private List<IServiceCalculator> GetMyDatabaseServiceCalculatorsForAccountAux(Guid accountId, bool isVip,
            IEnumerable<Guid>? filterIds = null)
        {
            bool applyFilter = filterIds != null;
            filterIds ??= [];

            return _myDatabaseServiceTypesProvider
               .GetBillingDataForMyDatabasesServiceTypes(accountId)
               .Where(st => st.TotalAmount > 0)
               .Where(st => !applyFilter || filterIds.Contains(st.ServiceTypeId))
               .DistinctBy(st => st.ServiceTypeId)
               .Select(serviceType => CreateMyDatabaseServiceCalculator(accountId, isVip, serviceType))
               .ToList();
        }

        private IServiceCalculator CreateBasicServiceCalculator(Guid accountId, bool isVipAccount,
            BillingServiceTypeInfoDto serviceType, AccountBillingServiceDataDto service)
        {
            ICalculationProcessor bonusProcessor = GetBonusProcessor(isVipAccount, serviceType.SystemServiceType);
            var nomenclatureServiceName = ServiceNameLocalizer
                .LocalizeForAccount(_cloudLocalizer, accountId, service.Name, service.SystemService);

            var serviceTypeSystem = service.Name == "Дополнительные сеансы"
                ? ResourceType.CountOfDatabasesOverLimit 
                : serviceType.SystemServiceType;

            var settings = new ServiceCalculatorSettings
            {
                Identifier = new ServiceCalculatorIdentifier(ServiceCalculatorType.SubscriptionService, serviceType.Id),
                Name = nomenclatureServiceName,
                Description = serviceType.Name,
                ServiceTypeId = serviceType.Id,
                Clouds42Service = service.SystemService,
                ResourceType = serviceType.SystemServiceType,
                NomenclatureDescription = $"{nomenclatureServiceName} - {serviceType.Name}",
                NomenclatureName = serviceType.Name,
                IsActiveService = service.IsActive,
                QuantityControl = ServiceQuantityControlResolver.GetControl(serviceTypeSystem, serviceType.BillingType),
                CalculationProcessor = CompositeProcessor.Compose(
                    ConstantRateProcessor.Create(serviceType.CostPerOneLicense),
                    SimpleTotalPriceProcessor.Create(false),
                    bonusProcessor),
                PayPeriodControl = PayPeriodControls.Default(),
                SetQuantity = serviceType.UsedLicenses,
                SetPayPeriod = (int)PayPeriod.Month1
            };
            return new ServiceCalculator(settings);
        }

        private IServiceCalculator CreateMyDatabaseServiceCalculator(Guid accountId, bool isVipAccount,
            MyDatabasesServiceTypeBillingDataDto serviceType)
        {
            ICalculationProcessor bonusProcessor = GetBonusProcessor(isVipAccount, serviceType.SystemServiceType);

            var nomenclautreServiceName = ServiceNameLocalizer.LocalizeForAccount(_cloudLocalizer,
                accountId,
                serviceType.ServiceName,
                serviceType.Clouds42ServiceType);

            var settings = new ServiceCalculatorSettings
            {
                Identifier = new ServiceCalculatorIdentifier(ServiceCalculatorType.SubscriptionService, serviceType.ServiceTypeId),
                Name = serviceType.ServiceName,
                Description = serviceType.SystemServiceType == ResourceType.CountOfDatabasesOverLimit
                        ? $"{serviceType.ServiceTypeDescription}, (свыше {serviceType.LimitOnFreeLicenses} шт)"
                            : serviceType.ServiceTypeDescription,
                Clouds42Service = serviceType.Clouds42ServiceType,
                ResourceType = serviceType.SystemServiceType,
                IsActiveService = serviceType.IsActiveService,
                SetPayPeriod = (int)PayPeriod.Month1,
                SetQuantity = serviceType.VolumeInQuantity,
                ServiceTypeId = serviceType.ServiceTypeId,
                NomenclatureDescription = $"{nomenclautreServiceName} - {serviceType.ServiceTypeName}",
                NomenclatureName = serviceType.ServiceTypeName,
                QuantityControl = ServiceQuantityControlResolver.GetControl(serviceType.SystemServiceType, serviceType.BillingType),
                PayPeriodControl = PayPeriodControls.Default(),
                CalculationProcessor = CompositeProcessor.Compose(
                    ConstantRateProcessor.Create(serviceType.CostPerOneLicense),
                    SimpleTotalPriceProcessor.Create(false),
                    bonusProcessor)
            };
            return new ServiceCalculator(settings);
        }

        private ICalculationProcessor GetBonusProcessor(bool isVipAccount, ResourceType? resourceType)
        {
            if (!isVipAccount && (resourceType == ResourceType.MyEntUser || resourceType == ResourceType.DiskSpace))
                return BonusByPayPeriodProcessor.Create(PayPeriodToBonusRewardMap);
            return ZeroBonusProcessor.Get();
        }
    }
}
