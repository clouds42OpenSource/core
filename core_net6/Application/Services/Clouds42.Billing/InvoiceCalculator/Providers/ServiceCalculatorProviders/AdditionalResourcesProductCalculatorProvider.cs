﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.InvoiceCalculator.Controls;
using Clouds42.Billing.InvoiceCalculator.Impl;
using Clouds42.Billing.InvoiceCalculator.Processors;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.InvoiceCalculator.Providers.ServiceCalculatorProviders
{
    public class AdditionalResourcesProductCalculatorProvider(
        IUnitOfWork dbLayer,
        ICloudLocalizer cloudLocalizer,
        IAdditionalResourcesDataProvider additionalResourcesDataProvider)
        : IServiceCalculatorsProvider
    {
        private static ServiceCalculatorIdentifier AdditionalServicesCalculatorIdentifier
            => new(ServiceCalculatorType.AdditionalResources);

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorType>? typeFilter = null)
        {
            if (typeFilter != null && !typeFilter.Contains(ServiceCalculatorType.AdditionalResources))
                return [];
            return GetProductCalculatorForAccountAux(accountId);
        }

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId, IEnumerable<ServiceCalculatorIdentifier> serviceFilter)
        {
            if (serviceFilter != null && !serviceFilter.Contains(AdditionalServicesCalculatorIdentifier))
                return [];
            return GetProductCalculatorForAccountAux(accountId);
        }


        private IEnumerable<IServiceCalculator> GetProductCalculatorForAccountAux(Guid accountId)
        {
            var additionalResourcesData = additionalResourcesDataProvider.GetAdditionalResourcesData(accountId);
            if (!additionalResourcesData.IsAvailabilityAdditionalResources)
                return Enumerable.Empty<IInvoiceProductCalculator>();

            var rent1CService = dbLayer.BillingServiceRepository
                .FirstOrDefault(w => w.SystemService == Clouds42Service.MyEnterprise);

            var settings = new ServiceCalculatorSettings
            {
                Identifier = AdditionalServicesCalculatorIdentifier,
                Name = rent1CService.GetNameBasedOnLocalization(cloudLocalizer, accountId),
                Description = additionalResourcesData.AdditionalResourceName,
                NomenclatureDescription = additionalResourcesData.AdditionalResourceName,
                NomenclatureName = "Дополнительные ресурсы",
                SetQuantity = 1,
                SetPayPeriod = (int)PayPeriod.Month1,
                IsActiveService = true,
                QuantityControl = QuantityControls.Constant(1),
                PayPeriodControl = PayPeriodControls.Default(),
                CalculationProcessor = CompositeProcessor.Compose(
                    ConstantRateProcessor.Create(additionalResourcesData.AdditionalResourceCost ?? 0),
                    SimpleTotalPriceProcessor.Create(false),
                    ZeroBonusProcessor.Get()
                )
            };
            return new[] { new ServiceCalculator(settings) };
        }
    }
}
