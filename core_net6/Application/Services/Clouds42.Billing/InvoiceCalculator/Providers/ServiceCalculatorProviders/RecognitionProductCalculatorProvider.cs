﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.InvoiceCalculator.Controls;
using Clouds42.Billing.InvoiceCalculator.Impl;
using Clouds42.Billing.InvoiceCalculator.Processors;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Attributes;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Providers.ServiceCalculatorProviders
{
    public class RecognitionProductCalculatorProvider(IUnitOfWork dbLayer) : IServiceCalculatorsProvider
    {
        ServiceCalculatorIdentifier RecCalculatorIdentifier => new(ServiceCalculatorType.Recognition);

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorType>? typeFilter = null)
        {
            if (typeFilter != null && !typeFilter.Contains(ServiceCalculatorType.Recognition))
                return [];
            return GetProductCalculatorForAccountAux();
        }

        /// <inheritdoc/>
        public IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorIdentifier> serviceFilter)
        {
            if (serviceFilter != null && !serviceFilter.Contains(RecCalculatorIdentifier))
                return [];
            return GetProductCalculatorForAccountAux();
        }

        private IEnumerable<IServiceCalculator> GetProductCalculatorForAccountAux()
        {
            var recRates = dbLayer.RateRepository
             .Where(r =>
                 r.BillingServiceType.Service.SystemService == Clouds42Service.Recognition &&
                 r.BillingServiceType.SystemServiceType.HasValue).ToList();

            var recQuantityToRateMap = recRates.ToDictionary(
                x => x.BillingServiceType.SystemServiceType.GetDataValue(),
                x => x.Cost);

            if (!recQuantityToRateMap.ContainsKey(0))
                recQuantityToRateMap[0] = 0m;

            var serviceTypeIdsByQuantity = GetServiceTypeToQuantityMap()
                .ToDictionary(x => x.Key, x => x.Value.Id);

            var initialQuantity = recQuantityToRateMap.Keys.Where(q => q > 0).OrderBy(q => q).FirstOrDefault();

            var settings = new ServiceCalculatorSettings
            {
                Identifier = RecCalculatorIdentifier,
                Name = "Fasta",
                Description = NomenclaturesNameConstants.Recognition,
                NomenclatureName = Clouds42Service.Recognition.ToString(),
                NomenclatureDescription = $"{NomenclaturesNameConstants.Esdl} Recognition 42",
                SetQuantity = initialQuantity,
                IsActiveService = true,
                QuantityControl = QuantityControls.PredefinedList(
                    recQuantityToRateMap.Keys.OrderBy(r => r)),
                CalculationProcessor = CompositeProcessor.Compose(
                    TotalPriceForQuantityProcessor.Create(recQuantityToRateMap, true),
                    ZeroBonusProcessor.Get()
                )
            };
            return new[]
            {
                new RecognitionServiceCalculator(settings, serviceTypeIdsByQuantity,
                    $"{NomenclaturesNameConstants.Esdl} Recognition 42 Тариф {{0}}")
            };
        }

        /// <summary>
        /// Получить маппинг количества страниц Recognition к ServiceType
        /// </summary>
        /// <returns></returns>
        private IDictionary<int, BillingServiceType> GetServiceTypeToQuantityMap()
        {
            var resourceTypeToQuantityMaps = GetResourceTypeToQuantityMap();
            var resourceTypes = resourceTypeToQuantityMaps
                .Select(x => x.resourceType).ToList();
            var resourceTypeToServiceType = dbLayer.BillingServiceTypeRepository
                .WhereLazy(st => st.SystemServiceType.HasValue)
                .Where(st => resourceTypes.Contains(st.SystemServiceType.Value))
                .AsEnumerable()
                .DistinctBy(x => x.SystemServiceType.Value)
                .ToDictionary(x => x.SystemServiceType.Value, x => x);


            var result = new Dictionary<int, BillingServiceType>();
            foreach (var (resourceType, quantity) in resourceTypeToQuantityMaps)
            {
                if (resourceTypeToServiceType.TryGetValue(resourceType, out var serviceType))
                    result[quantity] = serviceType;
            }
            return result;
        }

        /// <summary>
        /// Возвращает маппинг ResourceType к количеству страниц для Recognition
        /// </summary>
        /// <returns>Маппинг ResourceType к количеству</returns>
        private static IEnumerable<(ResourceType resourceType, int quantity)> GetResourceTypeToQuantityMap()
        {
            var recognitionResourceTypes = Enum.GetValues(typeof(ResourceType))
               .Cast<ResourceType>()
               .Where(rt => rt.HasAttribute<DependenciesEnumAttribute>(
                   attr => attr.Dependencies == (int)Clouds42Service.Recognition &&
                   attr.EnumType == typeof(Clouds42Service)));
            var result = new List<(ResourceType resourceType, int quantity)>();
            foreach (var rt in recognitionResourceTypes)
            {
                if (rt.TryGetDataValue(out int quantity))
                    result.Add((resourceType: rt, quantity));
            }
            return result;
        }
    }
}
