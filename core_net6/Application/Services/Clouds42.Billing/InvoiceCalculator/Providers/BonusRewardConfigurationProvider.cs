﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Providers
{
    public class BonusRewardConfigurationProvider : IBonusRewardConfigurationProvider
    {
        readonly Lazy<IDictionary<int, decimal>> _payPeriodToBonusMap = new(() =>
            new Dictionary<int, decimal>
            {
                {
                    (int)PayPeriod.Month1, 0m
                },
                {
                    (int)PayPeriod.Month3, (decimal)CloudConfigurationProvider.Invoices
                        .GetBonusRewardFor3MonthPayPeriod()/100
                },
                {
                    (int)PayPeriod.Month6, (decimal)CloudConfigurationProvider.Invoices
                        .GetBonusRewardFor6MonthPayPeriod()/100
                },
                {
                    (int)PayPeriod.Month12, (decimal)CloudConfigurationProvider.Invoices
                        .GetBonusRewardFor12MonthPayPeriod()/100
                }
            });

        public IDictionary<int, decimal> GetPayPeriodToBonusRewardMap() =>
            _payPeriodToBonusMap.Value;
    }
}
