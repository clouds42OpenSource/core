﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.InvoiceCalculator.Controls;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Helpers
{
    /// <summary>
    /// Вспомогательный класс для определения контроллера поля "Количество"
    /// </summary>
    public static class ServiceQuantityControlResolver
    {
        const int DiskSpaceMaxQuantity = 500;

        /// <summary>
        /// Получить контроллер количества для услуги
        /// </summary>
        /// <param name="resourceType">Тип ресурса системной услуги</param>
        /// <param name="billingType">Тип биллинга услуги</param>
        /// <returns></returns>
        public static ICalculatorControl<int> GetControl(ResourceType? resourceType, BillingTypeEnum billingType)
        {
            if (resourceType == ResourceType.DiskSpace)
                return QuantityControls.Ranged(0, DiskSpaceMaxQuantity);

            if (billingType == BillingTypeEnum.ForAccountUser ||
                billingType == BillingTypeEnum.ForAccount &&
                (resourceType == ResourceType.CountOfDatabasesOverLimit ||
                resourceType == ResourceType.ServerDatabasePlacement))
                return QuantityControls.Positive();
            return QuantityControls.Ranged(minValue: 0, maxValue: 1);
        }
    }
}
