﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;

namespace Clouds42.Billing.InvoiceCalculator.Controls.Impl
{
    /// <summary>
    /// Контрллер для любого значения между указанным верхним и нижним лимитами
    /// </summary>
    /// <typeparam name="TValue">Тип контролируемого поля</typeparam>
    public class RangedControl<TValue> : ICalculatorControl<TValue>
        where TValue : struct, IComparable<TValue>
    {
        /// <summary>
        /// Идентификатор типа контроллера
        /// </summary>
        public const string ControlType = "Ranged";

        readonly TValue? _minValue;
        readonly TValue? _maxValue;

        /// <summary>
        /// Контструктор
        /// </summary>
        /// <param name="minValue">Нижний порог. Если null - без нижнего порога</param>
        /// <param name="maxValue">Верхний порог. Если null - без верхнего порога</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public RangedControl(TValue? minValue, TValue? maxValue)
        {
            if (minValue.HasValue && maxValue.HasValue &&
                minValue.Value.CompareTo(maxValue.Value) > 0)
                throw new ArgumentOutOfRangeException(nameof(maxValue),
                    $"Максимальное значение не может быть меньше минимального: min:{minValue}, max: {maxValue}");
            _minValue = minValue;
            _maxValue = maxValue;
        }

        ///<inheritdoc/>
        public TValue GetValidated(TValue value)
        {
            if (_minValue.HasValue && value.CompareTo(_minValue.Value) < 0)
                return _minValue.Value;
            if (_maxValue.HasValue && value.CompareTo(_maxValue.Value) > 0)
                return _maxValue.Value;
            return value;
        }

        ///<inheritdoc/>
        public string GetControlType() => ControlType;

        ///<inheritdoc/>
        public IEnumerable<KeyValuePair<string, string>> GetControlSettings()
        {
            var res = new List<KeyValuePair<string, string>>();
            if (_maxValue.HasValue)
                res.Add(new KeyValuePair<string, string>("MaxValue", _maxValue.Value.ToString()));
            if (_minValue.HasValue)
                res.Add(new KeyValuePair<string, string>("MinValue", _minValue.Value.ToString()));
            return res;
        }
    }
}
