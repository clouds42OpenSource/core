﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;

namespace Clouds42.Billing.InvoiceCalculator.Controls.Impl
{
    /// <summary>
    /// Контроллер константного значения.
    /// Любое предоставленное значение будет заменено на установленную контсанту
    /// </summary>
    /// <typeparam name="TValue">Тип контролируемого поля</typeparam>
    public class ConstantValueControl<TValue> : ICalculatorControl<TValue>
    {
        /// <summary>
        /// Идентификатор типа контроллера
        /// </summary>
        public const string ControlType = "Constant";
        readonly TValue _value;

        /// <summary>
        /// Создать контроллер
        /// </summary>
        /// <param name="value">Константное значение</param>
        public ConstantValueControl(TValue value)
        {
            _value = value;
        }

        /// <inheritdoc/>
        public TValue GetValidated(TValue value)
        {
            return _value;
        }

        /// <inheritdoc/>
        public string GetControlType() => ControlType;

        /// <inheritdoc/>
        public IEnumerable<KeyValuePair<string, string>> GetControlSettings() => new[]
        {
            new KeyValuePair<string, string>("Value", _value.ToString())
        };
    }
}
