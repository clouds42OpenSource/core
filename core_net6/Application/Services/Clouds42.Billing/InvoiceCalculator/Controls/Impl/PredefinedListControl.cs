﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;

namespace Clouds42.Billing.InvoiceCalculator.Controls.Impl
{
    /// <summary>
    /// Контроллер для любого значения из предоставленного списка
    /// </summary>
    /// <typeparam name="TValue">Тип контролируемого поля</typeparam>
    public class PredefinedListControl<TValue> : ICalculatorControl<TValue>
    {
        /// <summary>
        /// Идентификатор типа контроллера
        /// </summary>
        public const string ControlType = "PredefinedList";
        readonly TValue[] _possibleValues;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="possibleValues">Допустимые значения контролируемого поля</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public PredefinedListControl(IEnumerable<TValue> possibleValues)
        {
            if (possibleValues == null) throw new ArgumentNullException(nameof(possibleValues));
            if (!possibleValues.Any())
                throw new ArgumentOutOfRangeException(nameof(possibleValues), "Список переданных значений пуст");
            _possibleValues = possibleValues.ToArray();
        }

        ///<inheritdoc/>
        public TValue GetValidated(TValue value)
        {
            if (!_possibleValues.Contains(value))
                return _possibleValues.First();
            return value;
        }

        ///<inheritdoc/>
        public string GetControlType() => ControlType;

        ///<inheritdoc/>
        public IEnumerable<KeyValuePair<string, string>> GetControlSettings()
        {
            var res = new List<KeyValuePair<string, string>>();
            bool isEnum = typeof(TValue).IsEnum;

            foreach (var value in _possibleValues)
            {
                var valueStr = isEnum ? Convert.ToInt32(value).ToString() : value.ToString();
                res.Add(new KeyValuePair<string, string>("Values", valueStr));
            }
            return res;
        }

        public class ControlSettings
        {
            public List<TValue> Values { get; set; }
        }
    }
}
