﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.InvoiceCalculator.Controls.Impl;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Controls
{
    /// <summary>
    /// Каталог возможных контроллеров поля "Платежный период"
    /// </summary>
    public static class PayPeriodControls
    {
        static readonly PredefinedListControl<int> _default = new(
            Enum.GetValues(typeof(PayPeriod))
            .Cast<PayPeriod>()
            .Where(x => x != PayPeriod.None)
            .Cast<int>());

        /// <summary>
        /// Дефолтный контроллер
        /// </summary>
        /// <returns>Контроллер</returns>
        public static ICalculatorControl<int> Default() => _default;
    }
}
