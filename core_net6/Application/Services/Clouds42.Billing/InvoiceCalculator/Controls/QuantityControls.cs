﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.InvoiceCalculator.Controls.Impl;

namespace Clouds42.Billing.InvoiceCalculator.Controls
{
    /// <summary>
    /// Каталог контроллеров поля "Количество"
    /// </summary>
    public static class QuantityControls
    {
        /// <summary>
        /// Контроллер для любого значения в указанных верхнем и нижнем порогах
        /// </summary>
        /// <param name="minValue">Минимальное значение</param>
        /// <param name="maxValue">Максимальное значение</param>
        /// <returns>Контроллер</returns>
        public static ICalculatorControl<int> Ranged(int minValue, int maxValue) =>
            new RangedControl<int>(minValue, maxValue);

        /// <summary>
        /// Контроллер для любого значения выше или равного нулю
        /// </summary>
        /// <returns>Контроллер</returns>
        public static ICalculatorControl<int> Positive() =>
            new RangedControl<int>(minValue: 0, maxValue: null);

        /// <summary>
        /// Контроллер для любого значения из предоставленного списка возможных значений
        /// </summary>
        /// <param name="values">Список допустимых значений</param>
        /// <returns>Контроллер</returns>
        public static ICalculatorControl<int> PredefinedList(IEnumerable<int> values) =>
            new PredefinedListControl<int>(values);

        /// <summary>
        /// Контроллер заменяющий любое предоставленного значение константным
        /// </summary>
        /// <param name="value">Константное значение</param>
        /// <returns>Контроллер</returns>
        public static ICalculatorControl<int> Constant(int value) =>
            new ConstantValueControl<int>(value);
    }
}
