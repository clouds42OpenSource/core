﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Impl
{

    /// <summary>
    /// Обертка над IServiceCalculator, имплементирующая IInvoiceProductCalculator
    /// </summary>
    /// <inheritdoc/>
    public class InvoiceProductCalculatorServiceCalculatorWrapper : IInvoiceProductCalculator
    {
        readonly IServiceCalculator _serviceCalculator;
        public InvoiceProductCalculatorServiceCalculatorWrapper(IServiceCalculator serviceCalculator)
        {
            _serviceCalculator = serviceCalculator;
            SetIsActive(serviceCalculator.IsActiveService);
        }

        #region IServiceCalculator pass-throght Properties and methods
        public ServiceCalculatorIdentifier Identifier => _serviceCalculator.Identifier;
        public int? PayPeriod => _serviceCalculator.PayPeriod;
        public int BillableQuantity => _serviceCalculator.BillableQuantity;
        public decimal Rate => _serviceCalculator.Rate;
        public decimal TotalBonus => _serviceCalculator.TotalBonus;
        public decimal TotalPrice => _serviceCalculator.TotalPrice;
        public int FreeQuantity => _serviceCalculator.FreeQuantity;
        public int TotalQuantity => _serviceCalculator.TotalQuantity;
        public string Name => _serviceCalculator.Name;
        public string Description => _serviceCalculator.Description;
        public ResourceType? ResourceType => _serviceCalculator.ResourceType;
        public Clouds42Service? Clouds42Service => _serviceCalculator.Clouds42Service;
        public string NomenclatureName => _serviceCalculator.NomenclatureName;
        public string NomenclatureDescription => _serviceCalculator.NomenclatureDescription;
        public Guid? ServiceTypeId => _serviceCalculator.ServiceTypeId;
        public bool IsActiveService => _serviceCalculator.IsActiveService;
        public ICalculatorControl<int> PayPeriodControl => _serviceCalculator.PayPeriodControl;
        public ICalculatorControl<int> QuantityControl => _serviceCalculator.QuantityControl;
        public ICalculationProcessor CalculationProcessor => _serviceCalculator.CalculationProcessor;
        public bool Set(int quantity, int? payPeriod) =>
            _serviceCalculator.Set(quantity, payPeriod);

        #endregion IServiceCalculator pass-throght Properties and methods

        public bool Set(int quantity, int? payPeriod, bool isActive)
        {
            bool isChanged = SetIsActive(isActive);
            return Set(quantity, payPeriod) || isChanged;
        }

        public bool IsActive { get; private set; }

        public bool SetIsActive(bool isActive)
        {
            bool isChanged = IsActive != isActive;
            IsActive = isActive;
            return isChanged;
        }
    }
}
