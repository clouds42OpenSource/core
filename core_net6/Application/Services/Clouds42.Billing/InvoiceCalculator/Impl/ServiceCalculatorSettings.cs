﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Impl
{
    /// <summary>
    /// Настройки калькулятора услуги
    /// </summary>
    public class ServiceCalculatorSettings
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public ServiceCalculatorIdentifier Identifier { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Тип ресурса системной услуги
        /// </summary>
        public ResourceType? ResourceType { get; set; }

        /// <summary>
        /// Тип системного сервиса
        /// </summary>
        public Clouds42Service? Clouds42Service { get; set; }

        /// <summary>
        /// Номенклатурное имя услуги
        /// </summary>
        public string NomenclatureName { get; set; }

        /// <summary>
        /// Номенклатурное описание услуги
        /// </summary>
        public string NomenclatureDescription { get; set; }

        /// <summary>
        /// Контрллер поля "Количество"
        /// </summary>
        public ICalculatorControl<int> QuantityControl { get; set; }

        /// <summary>
        /// Контроллер поля "Платежный Период"
        /// </summary>
        public ICalculatorControl<int> PayPeriodControl { get; set; }

        /// <summary>
        /// Процессор расчетов
        /// </summary>
        public ICalculationProcessor CalculationProcessor { get; set; }

        /// <summary>
        /// ID услуги сервиса
        /// </summary>
        public Guid? ServiceTypeId { get; set; }

        /// <summary>
        /// Беспланое количество
        /// </summary>
        public int FreeQuantity { get; set; }

        /// <summary>
        /// Начальное значение поля "количество"
        /// </summary>
        public int SetQuantity { get; set; }

        /// <summary>
        /// Начальное значение поля "Платежный период"
        /// </summary>
        public int? SetPayPeriod { get; set; }

        /// <summary>
        /// Сервис активен
        /// </summary>
        public bool IsActiveService { get; set; }
    }
}
