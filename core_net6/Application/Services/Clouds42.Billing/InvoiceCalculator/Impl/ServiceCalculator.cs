﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Models;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.InvoiceCalculator.Impl
{
    /// <summary>
    /// Калькулятор сервиса
    /// </summary>
    public class ServiceCalculator : IServiceCalculator
    {

        ServiceCalculatorState _calculation;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="settings">Настройки калькулятора сервиса</param>
        /// <exception cref="ArgumentNullException"></exception>
        public ServiceCalculator(ServiceCalculatorSettings settings)
        {
            if (settings == null)
                throw new ArgumentNullException(nameof(settings));

            Identifier = settings.Identifier;
            QuantityControl = settings.QuantityControl ?? throw new ArgumentNullException(nameof(settings));
            PayPeriodControl = settings.PayPeriodControl;
            CalculationProcessor = settings.CalculationProcessor ?? throw new ArgumentNullException(nameof(settings));
            Name = settings.Name;
            Description = settings.Description;
            ResourceType = settings.ResourceType;
            Clouds42Service = settings.Clouds42Service;
            NomenclatureName = settings.NomenclatureName;
            NomenclatureDescription = settings.NomenclatureDescription;
            ServiceTypeId = settings.ServiceTypeId;
            FreeQuantity = settings.FreeQuantity;
            IsActiveService = settings.IsActiveService;
            Set(settings.SetQuantity, settings.SetPayPeriod);
        }

        /// <inheritdoc/>
        public ServiceCalculatorIdentifier Identifier { get; }

        /// <inheritdoc/>
        public string Name { get; protected set; }

        /// <inheritdoc/>
        public string Description { get; protected set; }

        /// <inheritdoc/>
        public ResourceType? ResourceType { get; protected set; }

        /// <inheritdoc/>
        public Clouds42Service? Clouds42Service { get; protected set; }

        /// <inheritdoc/>
        public bool IsActiveService { get; protected set; }

        /// <inheritdoc/>
        public string NomenclatureName { get; protected set; }

        /// <inheritdoc/>
        public string NomenclatureDescription { get; protected set; }

        /// <inheritdoc/>
        public Guid? ServiceTypeId { get; protected set; }

        int _freeQuantity;

        /// <inheritdoc/>
        public int FreeQuantity
        {
            get => _freeQuantity;
            protected set => _freeQuantity = Math.Max(value, 0);
        }

        /// <inheritdoc/>
        public int TotalQuantity => BillableQuantity + FreeQuantity;

        /// <inheritdoc/>
        public int BillableQuantity => _calculation.Quantity;

        /// <inheritdoc/>
        public int? PayPeriod => _calculation.PayPeriod;

        /// <inheritdoc/>
        public decimal Rate => _calculation.Rate;

        /// <inheritdoc/>
        public decimal TotalPrice => _calculation.TotalPrice;

        /// <inheritdoc/>
        public decimal TotalBonus => _calculation.TotalBonus;

        /// <inheritdoc/>
        public ICalculatorControl<int> PayPeriodControl { get; }

        /// <inheritdoc/>
        public ICalculatorControl<int> QuantityControl { get; }

        /// <inheritdoc/>
        public ICalculationProcessor CalculationProcessor { get; }

        /// <inheritdoc/>
        public bool Set(int quantity, int? payPeriod)
        {
            var prevCalc = _calculation;
            _calculation = ProduceCalculationState(quantity, payPeriod);
            return _calculation != prevCalc;
        }


        /// <summary>
        /// Создает новое состояние расчета исходи из предоставленных параметров
        /// </summary>
        /// <param name="quantity">Количество</param>
        /// <param name="payPeriod">Платежный период</param>
        /// <returns>Новое состояние расчета сервиса</returns>
        /// <exception cref="InvalidOperationException"></exception>
        protected virtual ServiceCalculatorState ProduceCalculationState(int quantity, int? payPeriod)
        {
            if (PayPeriodControl != null && !payPeriod.HasValue)
                throw new InvalidOperationException("Платежный период является обязательным полем для данного калькулятора");

            var newCalculation = new ServiceCalculatorState
            {
                Quantity = QuantityControl.GetValidated(quantity),
                PayPeriod = PayPeriodControl?.GetValidated(payPeriod.Value)
            };
            if (_calculation?.PayPeriod == newCalculation.PayPeriod &&
                _calculation?.Quantity == newCalculation.Quantity)
                return _calculation;
            CalculationProcessor.Process(newCalculation);
            return newCalculation;
        }
    }
}
