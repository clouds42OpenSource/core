﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;

namespace Clouds42.Billing.InvoiceCalculator.Impl
{
    /// <summary>
    /// Содержит даные необходимые для создания калькулятора инвойсов
    /// </summary>
    public class InvoiceCalculatorSettings
    {
        /// <summary>
        /// Имя поставщика
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// Имя покупателя
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// ID поставщика
        /// </summary>
        public Guid SupplierId { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Имя валюты
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Код валюты
        /// </summary>
        public int? CurrencyCode { get; set; }


        /// <summary>
        /// Калькуляторы сервисов для продуктов
        /// </summary>
        public IEnumerable<IServiceCalculator> ProductCalculators { get; set; } = [];
    }
}
