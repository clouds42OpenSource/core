﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Models;

namespace Clouds42.Billing.InvoiceCalculator.Impl
{
    /// <summary>
    /// Калькулятор сериса для услуги Recognition
    /// </summary>
    /// <remarks>Вынесено в отдельный класс, т.к. для Recognition при изменении количества нужно менять ServiceTypeId</remarks>
    public class RecognitionServiceCalculator : ServiceCalculator
    {
        readonly IDictionary<int, Guid> _serviceTypeIdByQuantity;
        readonly string _descripionByQuantityTemplate;
        readonly bool _isInitialized;

        /// <param name="settings">Настройки калькулятора сервиса</param>
        /// <param name="serviceTypeIdByQuantity">Сопоставление ServiceTypeId к количеству</param>
        /// <param name="descripionByQuantityTemplate">Темплейт номенклатурного описания сервиса в зависимости от количества {0}</param>
        public RecognitionServiceCalculator(
            ServiceCalculatorSettings settings,
            IDictionary<int, Guid> serviceTypeIdByQuantity,
            string descripionByQuantityTemplate) : base(settings)
        {
            _serviceTypeIdByQuantity = serviceTypeIdByQuantity.ToDictionary(x => x.Key, x => x.Value);
            _descripionByQuantityTemplate = descripionByQuantityTemplate ?? string.Empty;
            _isInitialized = true;
            Set(BillableQuantity, PayPeriod);
        }

        ///<inheritdoc/>
        protected override ServiceCalculatorState ProduceCalculationState(int quantity, int? payPeriod)
        {
            var calculation = base.ProduceCalculationState(quantity, payPeriod);
            if (!_isInitialized)
                return calculation;

            ServiceTypeId = _serviceTypeIdByQuantity.TryGetValue(calculation.Quantity, out var serviceTypeId) ?
                serviceTypeId : default;
            NomenclatureDescription = string.Format(_descripionByQuantityTemplate, calculation.Quantity);
            return calculation;
        }
    }
}
