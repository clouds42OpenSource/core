﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.InvoiceCalculator.Controls;

namespace Clouds42.Billing.InvoiceCalculator.Impl
{
    /// <inheritdoc/>
    public class InvoiceCalculator : IInvoiceCalculator
    {
        readonly IDictionary<ServiceCalculatorIdentifier, IInvoiceProductCalculator> _productsByIdentifier;
        readonly ICollection<IInvoiceProductCalculator> _productsCollection;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="settings">Настройки</param>
        /// <exception cref="ArgumentNullException"></exception>
        public InvoiceCalculator(InvoiceCalculatorSettings settings)
        {
            if (settings == null) throw new ArgumentNullException(nameof(settings));
            if (settings.ProductCalculators == null) throw new ArgumentNullException(nameof(settings));
            if (string.IsNullOrWhiteSpace(settings.SupplierName)) throw new ArgumentNullException(nameof(settings));
            if (string.IsNullOrWhiteSpace(settings.BuyerName)) throw new ArgumentNullException(nameof(settings));

            AccountId = settings.AccountId;
            BuyerName = settings.BuyerName;
            SupplierId = settings.SupplierId;
            SupplierName = settings.SupplierName;
            CurrencyCode = settings.CurrencyCode;
            Currency = settings.Currency;

            //Use separate list of products to preserve initial order
            _productsCollection = new List<IInvoiceProductCalculator>();
            _productsByIdentifier = new Dictionary<ServiceCalculatorIdentifier, IInvoiceProductCalculator>();

            foreach (var serviceCalculator in settings.ProductCalculators ?? [])
            {
                if (serviceCalculator == null || _productsByIdentifier.ContainsKey(serviceCalculator.Identifier))
                    continue;
                var productCalculator = new InvoiceProductCalculatorServiceCalculatorWrapper(serviceCalculator);

                _productsByIdentifier.Add(productCalculator.Identifier, productCalculator);
                _productsCollection.Add(productCalculator);
                if (PayPeriodControl == null && productCalculator.PayPeriodControl != null)
                {
                    PayPeriodControl = PayPeriodControls.Default();
                    PayPeriod = 3;
                }
            }
            _isDirty = true;
            RecalculateTotalsAux();
        }

        /// <inheritdoc/>
        public ICalculatorControl<int> PayPeriodControl { get; }

        /// <inheritdoc/>
        public Guid SupplierId { get; }

        /// <inheritdoc/>
        public Guid AccountId { get; }

        /// <inheritdoc/>
        public string BuyerName { get; }

        /// <inheritdoc/>
        public string SupplierName { get; }

        /// <inheritdoc/>
        public string Currency { get; }

        /// <inheritdoc/>
        public int? CurrencyCode { get; }

        /// <inheritdoc/>

        decimal _totalBeforeVAT;
        decimal _totalBonus;
        bool _isDirty;

        /// <inheritdoc/>
        public int? PayPeriod { get; private set; }

        /// <inheritdoc/>
        public decimal TotalBeforeVAT { get { RecalculateTotalsIfDirty(); return _totalBeforeVAT; } }

        /// <inheritdoc/>
        public decimal VAT => 0m;

        /// <inheritdoc/>
        public decimal TotalAfterVAT => TotalBeforeVAT - VAT;

        /// <inheritdoc/>
        public decimal TotalBonus { get { RecalculateTotalsIfDirty(); return _totalBonus; } }

        /// <inheritdoc/>
        public IEnumerable<IInvoiceProductCalculator> Products => _productsCollection;

        /// <inheritdoc/>
        public bool Set(int? payPeriod, IEnumerable<IInvoiceProductCalculatorInput> productInputs, bool disableOtherProducts)
        {
            productInputs = productInputs ?? throw new ArgumentNullException(nameof(productInputs));
            bool isChanged = SetPayPeriodAux(payPeriod, false);
            isChanged = SetProductInputsAux(productInputs, disableOtherProducts) || isChanged;
            return isChanged;
        }

        /// <inheritdoc/>
        public bool SetPayPeriod(int? payPeriod)
        {
            return SetPayPeriodAux(payPeriod, true);
        }

        /// <inheritdoc/>
        public bool SetProducts(IEnumerable<IInvoiceProductCalculatorInput> productInputs, bool disableOtherProducts)
        {
            productInputs = productInputs ?? throw new ArgumentNullException(nameof(productInputs));
            return SetProductInputsAux(productInputs, disableOtherProducts);
        }

        /// <summary>
        /// Установить платежный период
        /// </summary>
        /// <param name="payPeriod">Новый платежный период</param>
        /// <param name="propagateToProducts">Нужно ли автоматически поменять платенжные период продуктов</param>
        /// <returns>Произошло ли изменение состояния</returns>
        /// <exception cref="InvalidOperationException"></exception>
        protected bool SetPayPeriodAux(int? payPeriod, bool propagateToProducts)
        {
            if (PayPeriodControl != null && !payPeriod.HasValue)
                throw new InvalidOperationException("Платежный период является обязательным полем");

            int? prevPayPeriod = PayPeriod;
            PayPeriod = PayPeriodControl?.GetValidated(payPeriod.Value) ?? default;

            bool isChanged = PayPeriod != prevPayPeriod;
            _isDirty = _isDirty || isChanged;

            if (!propagateToProducts)
                return isChanged;

            foreach (var service in _productsByIdentifier.Values)
            {
                isChanged = service.Set(service.BillableQuantity, PayPeriod) || isChanged;
                _isDirty = _isDirty || isChanged;
            }
            return isChanged;
        }

        /// <summary>
        /// Поменять состояние продуктов
        /// </summary>
        /// <param name="productInputs">Входные данные для продуктов</param>
        /// <param name="disableOtherProducts">
        /// Если true - продукты которых нет во входных данных будут отключены
        /// Если false - продукты которых нет будут оставлены в последнем валидном состоянии
        /// </param>
        /// <returns>Произошло ли изменение состояния</returns>
        protected bool SetProductInputsAux(IEnumerable<IInvoiceProductCalculatorInput> productInputs, bool disableOtherProducts)
        {
            bool isChanged = false;

            var seenProductIdentifiers = new HashSet<ServiceCalculatorIdentifier>();

            foreach (var input in productInputs)
            {
                if (!_productsByIdentifier.TryGetValue(input.Identifier, out var productCalculator))
                    continue;
                seenProductIdentifiers.Add(input.Identifier);
                isChanged = productCalculator.Set(input.Quantity, PayPeriod, input.IsActive) || isChanged;
            }

            if (disableOtherProducts)
                foreach (var product in _productsByIdentifier.Values)
                {
                    if (seenProductIdentifiers.Contains(product.Identifier))
                        continue;
                    isChanged =
                        (product.PayPeriod == PayPeriod ?
                        product.SetIsActive(false) :
                        product.Set(product.BillableQuantity, PayPeriod, false)) || isChanged;
                }

            _isDirty = _isDirty || isChanged;
            return isChanged;
        }

        /// <summary>
        /// Пересчитать суммы, если были изменения в параметрах
        /// </summary>
        protected void RecalculateTotalsIfDirty()
        {
            if (_isDirty) RecalculateTotalsAux();
        }

        /// <summary>
        /// Пересчитать суммы
        /// </summary>
        protected void RecalculateTotalsAux()
        {
            _totalBeforeVAT = 0;
            _totalBonus = 0;
            foreach (var service in _productsByIdentifier.Values.Where(s => s.IsActive))
            {
                if (service.PayPeriodControl != null && service.PayPeriod != PayPeriod)
                    service.Set(service.BillableQuantity, PayPeriod);
                _totalBeforeVAT += service.TotalPrice;
                _totalBonus += service.TotalBonus;
            }
            _isDirty = false;
        }
    }
}
