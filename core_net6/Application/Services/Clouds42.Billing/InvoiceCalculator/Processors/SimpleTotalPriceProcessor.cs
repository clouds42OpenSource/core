﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Models;

namespace Clouds42.Billing.InvoiceCalculator.Processors
{
    /// <summary>
    /// Процессор расчитывающий общую стомость продукта по простой формуле quantity * rate * periods или quantity * rate
    /// </summary>
    public class SimpleTotalPriceProcessor : ICalculationProcessor
    {
        static readonly SimpleTotalPriceProcessor PayPeriodInvariantInstance = new(true);
        static readonly SimpleTotalPriceProcessor PayPeriodDependentInstance = new(false);

        readonly bool _isPayPeriodInvariant;

        /// <param name="payPeriodInvariant">Признак обозначающий нужно ли учитывать количество периодов при расчете общей стоимости</param>
        public SimpleTotalPriceProcessor(bool payPeriodInvariant = false)
        {
            _isPayPeriodInvariant = payPeriodInvariant;
        }

        /// <inheritdoc/>
        public void Process(ServiceCalculatorState state)
        {
            state.TotalPrice =
                state.Rate *
                state.Quantity *
                (!_isPayPeriodInvariant ? state.PayPeriod ?? 0 : 1);
        }

        /// <inheritdoc/>
        public static SimpleTotalPriceProcessor Create(bool payPeriodInvariant = false)
        {
            return new SimpleTotalPriceProcessor(payPeriodInvariant);
        }

        /// <summary>
        /// Посторить простой процессор TotalPrice не учитывающий количество периодов
        /// </summary>
        /// <returns>Процессор</returns>
        public static SimpleTotalPriceProcessor GetPeriodInvariant() => PayPeriodInvariantInstance;

        /// <summary>
        /// Посторить простой процессор TotalPrice учитывающий количество периодов
        /// </summary>
        /// <returns>Процессор</returns>
        public static SimpleTotalPriceProcessor GetPeriodDependent() => PayPeriodDependentInstance;
    }
}
