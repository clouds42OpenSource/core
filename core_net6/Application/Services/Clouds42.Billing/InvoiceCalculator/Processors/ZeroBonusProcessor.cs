﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Models;

namespace Clouds42.Billing.InvoiceCalculator.Processors
{
    /// <summary>
    /// Процессор устанавливающий нулевое бонусное вознаграждение вне зависимости от других параметров
    /// </summary>
    public class ZeroBonusProcessor : ICalculationProcessor
    {
        static readonly ZeroBonusProcessor _instance = new();
        public ZeroBonusProcessor() { }

        /// <inheritdoc/>
        public void Process(ServiceCalculatorState state)
        {
            state.TotalBonus = 0;
        }

        /// <summary>
        /// Получить процессор
        /// </summary>
        /// <returns>Процессор</returns>
        public static ZeroBonusProcessor Get() => _instance;
    }
}
