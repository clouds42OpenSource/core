﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Models;

namespace Clouds42.Billing.InvoiceCalculator.Processors
{
    /// <summary>
    /// Процессор выставляющий фикированную стоимость единицы продукта
    /// </summary>
    public class ConstantRateProcessor : ICalculationProcessor
    {
        readonly decimal _constantRateValue;

        /// <param name="constantRateValue">Значение фиксированной стоимости единицы продукта</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public ConstantRateProcessor(decimal constantRateValue)
        {
            if (constantRateValue < 0) throw new ArgumentOutOfRangeException(nameof(constantRateValue));
            _constantRateValue = constantRateValue;
        }

        /// <inheritdoc/>
        public void Process(ServiceCalculatorState state)
        {
            state.Rate = _constantRateValue;
        }

        /// <summary>
        /// Построить процессор ConstantRateProcessor
        /// </summary>
        /// <param name="constantRate">Значение фиксированаай стоимости единицы продукта</param>
        /// <returns>Процессор</returns>
        public static ConstantRateProcessor Create(decimal constantRate) => new(constantRate);
    }
}
