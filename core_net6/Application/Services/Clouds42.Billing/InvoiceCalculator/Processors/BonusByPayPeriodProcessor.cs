﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Models;

namespace Clouds42.Billing.InvoiceCalculator.Processors
{
    /// <summary>
    /// Процессор определяющий расчитывающий бонусное вознаграждение продукта на основании выбранного количества периодов и стоимости продукта
    /// </summary>
    public class BonusByPayPeriodProcessor : ICalculationProcessor
    {
        readonly IDictionary<int, decimal> _payPeriodToBonusRewardMap;
        readonly int _roundToDecimals;

        /// <param name="payPeriodToBonusRewardMap">Маппинг количества периодов к бонусному вознаграждению (в % от стоимости)</param>
        /// <param name="roundToDecimals">До скольки знаков после запятой необходимо округлять расчитанную сумму бонусов</param>
        public BonusByPayPeriodProcessor(IDictionary<int, decimal> payPeriodToBonusRewardMap, int roundToDecimals = 2)
        {
            _payPeriodToBonusRewardMap = payPeriodToBonusRewardMap ?? new Dictionary<int, decimal>();
            if (roundToDecimals < 0)
                throw new ArgumentOutOfRangeException(nameof(roundToDecimals), "Значение должно быть больше или равно нулю");
            _roundToDecimals = roundToDecimals;
        }

        /// <inheritdoc/>
        public void Process(ServiceCalculatorState state)
        {
            var payPeriod = state.PayPeriod ?? 0;
            var maxBonusMultiplier = _payPeriodToBonusRewardMap
                .Where(x => x.Key <= payPeriod)
                .Select(x => x.Value)
                .Max();
            state.TotalBonus = Math.Round(maxBonusMultiplier * state.TotalPrice, _roundToDecimals);
        }

        /// <summary>
        /// Посторить процессор
        /// </summary>
        /// <param name="payPeriodToBonusRewardMap">Маппинг количества периодов к бонусному вознаграждению (в % от стоимости)</param>
        /// <returns>Процессор</returns>
        public static BonusByPayPeriodProcessor Create(IDictionary<int, decimal> payPeriodToBonusRewardMap) =>
            new(payPeriodToBonusRewardMap);
    }
}
