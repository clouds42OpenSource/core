﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Models;

namespace Clouds42.Billing.InvoiceCalculator.Processors
{
    /// <summary>
    /// Процессор состоящий из нескольких процессоров, вызываемых по порядку
    /// </summary>
    public class CompositeProcessor : ICalculationProcessor
    {
        readonly ICalculationProcessor[] _innerProcessors;

        /// <param name="innerProcessors">Внутрениие процессоры в порядке предполагаемого вызова</param>
        /// <exception cref="ArgumentNullException"></exception>
        public CompositeProcessor(IEnumerable<ICalculationProcessor> innerProcessors)
        {
            _innerProcessors = innerProcessors?.ToArray() ?? throw new ArgumentNullException(nameof(innerProcessors));
        }

        /// <inheritdoc/>
        public void Process(ServiceCalculatorState state)
        {
            foreach (var item in _innerProcessors)
            {
                item.Process(state);
            }
        }

        /// <summary>
        /// Объеденить процессоры в composite процессор
        /// </summary>
        /// <param name="innerProcessors">Внутрениие процессоры в порядке предполагаемого вызова</param>
        /// <returns>Процессор</returns>
        public static CompositeProcessor Compose(params ICalculationProcessor[] innerProcessors)
        {
            return new CompositeProcessor(innerProcessors ?? Enumerable.Empty<ICalculationProcessor>());
        }
    }
}
