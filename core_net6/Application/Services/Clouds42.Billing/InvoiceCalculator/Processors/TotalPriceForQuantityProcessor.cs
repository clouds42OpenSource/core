﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.Models;

namespace Clouds42.Billing.InvoiceCalculator.Processors
{
    /// <summary>
    /// Процессор устанавлюивающий общую стоимость продукта и стоиость одной единицы на основании предопрделенного маппинга quantity-total price    
    /// </summary>
    public class TotalPriceForQuantityProcessor : ICalculationProcessor
    {
        readonly IDictionary<int, decimal> _quantityToTotalPriceMap;
        readonly bool _isPayPeriodInvariant;
        readonly int _roundToDecimals;

        /// <param name="quantityToTotalPriceMap">Маппинг количества к общей стоимости продукта</param>
        /// <param name="isPayPeriodInvariant">Признак определяющий нужно ли учитывать платежный период при расчете стоимости одной единицы</param>
        /// <param name="roundToDecimals">До скольки знаков после запятой необходимо округлять суммы</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public TotalPriceForQuantityProcessor(
            IDictionary<int, decimal> quantityToTotalPriceMap,
            bool isPayPeriodInvariant = true,
            int roundToDecimals = 2)
        {
            if (quantityToTotalPriceMap == null)
                throw new ArgumentNullException(nameof(quantityToTotalPriceMap));
            if (!quantityToTotalPriceMap.Any())
                throw new ArgumentOutOfRangeException(nameof(quantityToTotalPriceMap));
            if (roundToDecimals < 0)
                throw new ArgumentOutOfRangeException(nameof(roundToDecimals));
            _quantityToTotalPriceMap = quantityToTotalPriceMap
                .ToDictionary(x => x.Key, x => x.Value);
            _isPayPeriodInvariant = isPayPeriodInvariant;
            _roundToDecimals = roundToDecimals;
        }

        /// <inheritdoc/>
        public void Process(ServiceCalculatorState state)
        {
            if (!_quantityToTotalPriceMap.ContainsKey(state.Quantity))
                throw new ArgumentOutOfRangeException(nameof(state));
            if (!_isPayPeriodInvariant && !state.PayPeriod.HasValue)
                throw new ArgumentException($"Отсутствует обязательное значение {nameof(state.PayPeriod)}");

            var payPeriod = !_isPayPeriodInvariant ? state.PayPeriod.Value : 1;
            var totalPricePerPeriod = _quantityToTotalPriceMap[state.Quantity];

            state.TotalPrice = totalPricePerPeriod * payPeriod;

            if (state.Quantity == 0 || payPeriod == 0)
            {
                state.Rate = 0;
                return;
            }
            state.Rate = Math.Round(totalPricePerPeriod / state.Quantity, _roundToDecimals);
        }

        /// <summary>
        /// Создать процессор
        /// </summary>
        /// <param name="quantityToTotalPriceMap">Маппинг количества к общей стоимости продукта</param>
        /// <param name="isPayPeriodInvariant">Признак определяющий нужно ли учитывать платежный период при расчете стоимости одной единицы</param>
        /// <returns>Процессор</returns>
        public static TotalPriceForQuantityProcessor Create(
            IDictionary<int, decimal> quantityToTotalPriceMap,
            bool isPayPeriodInvariant = false)
            => new(quantityToTotalPriceMap, isPayPeriodInvariant);
    }
}
