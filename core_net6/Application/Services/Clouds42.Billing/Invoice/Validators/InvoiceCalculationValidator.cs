﻿using Clouds42.Billing.Contracts.Invoice.Interfaces.Validators;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;

namespace Clouds42.Billing.Invoice.Validators
{
    /// <inheritdoc/>
    public class InvoiceCalculationValidator : IInvoiceCalculationValidator
    {
        readonly IInvoiceCalculator _calculator;

        /// <param name="calculator">Калькулятор используемый для валидации расчетов</param>
        public InvoiceCalculationValidator(IInvoiceCalculator calculator)
        {
            _calculator = calculator;
        }

        /// <inheritdoc/>
        public bool IsValid(InvoiceCalculationDto calculation)
        {
            calculation = calculation ?? throw new ArgumentNullException(nameof(calculation));

            if (!ValidateTotals(calculation))
                return false;
            return ValidateProductsCollection(calculation);
        }

        /// <summary>
        /// Проврка top-level полей расчета (общие суммы, платежные периоды и т.д.)
        /// </summary>
        private bool ValidateTotals(InvoiceCalculationDto calculation)
        {
            return (_calculator.PayPeriod ?? 0) == (calculation.PayPeriod ?? 0) &&
                _calculator.TotalAfterVAT == calculation.TotalAfterVAT &&
                _calculator.TotalBeforeVAT == calculation.TotalBeforeVAT &&
                _calculator.TotalBonus == calculation.TotalBonus;
        }

        /// <summary>
        /// Проверка валидности коллекции продуктов инвойса
        /// </summary>
        private bool ValidateProductsCollection(InvoiceCalculationDto calculation)
        {
            var calculationProducts = calculation.Products?
                .ToList() ?? [];
            var sampleProducts = _calculator.Products.Where(x => x.IsActive)
                .ToList();
            if (sampleProducts.Count != calculationProducts.Count)
                return false;

            var calculationProductsByIdentifier = calculationProducts
                .ToDictionary(x => ServiceCalculatorIdentifier.Parse(x.Identifier), x => x);

            var sampleProductsByIdentifier = sampleProducts
                .ToDictionary(x => x.Identifier, x => x);

            return
                calculationProductsByIdentifier
                .All(calulcationKv =>
                         sampleProductsByIdentifier.TryGetValue(calulcationKv.Key, out var sampleItem) &&
                         IsEquivalent(calulcationKv.Value, sampleItem)) &&
                sampleProductsByIdentifier
                .All(sampleKv =>
                        calculationProductsByIdentifier.TryGetValue(sampleKv.Key, out var calculationItem) &&
                        IsEquivalent(calculationItem, sampleKv.Value));
        }


        /// <summary>
        /// Проверить соответствие расчета продукта состоянию калькулятора продукта
        /// </summary>
        private static bool IsEquivalent(InvoiceProductCalculationDc product,
            IInvoiceProductCalculator productCalculator)
        {
            return
                productCalculator.Identifier == ServiceCalculatorIdentifier.Parse(product.Identifier) &&
                (productCalculator.PayPeriod ?? 0) == (product.PayPeriod ?? 0) &&
                productCalculator.BillableQuantity == product.Quantity &&
                productCalculator.Rate == product.Rate &&
                productCalculator.TotalPrice == product.TotalPrice &&
                productCalculator.TotalBonus == product.TotalBonus;
        }
    }

    public static class InvoiceCalculatorValidationExtensions
    {
        public static IInvoiceCalculationValidator AsValidator(this IInvoiceCalculator calculator)
        {
            if (calculator == null)
                throw new ArgumentNullException(nameof(calculator));
            return new InvoiceCalculationValidator(calculator);
        }
    }
}
