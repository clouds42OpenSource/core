﻿using System.Text;
using Clouds42.Billing.Invoice.Constants;
using Clouds42.Common.Constants;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Invoice.Helpers
{
    /// <summary>
    /// Хелпер для работы со счетом на оплату
    /// </summary>
    public static class InvoiceAdditionalDataHelper
    {
        /// <summary>
        /// Сопоставление названия услуги и единицы измерения
        /// </summary>
        private static readonly IDictionary<string, string> MapServiceTypenameToUnit = new Dictionary<string, string>
        {
            {InvoiceConsts.ServiceTypePages, string.Empty},
            {InvoiceConsts.ServiceTypeLicense, string.Empty},
            {InvoiceConsts.ServiceTypeMyDisk, InvoiceConsts.UnitGb}
        };

        /// <summary>
        /// Услуги, которые не входят в ежемесячный платеж
        /// </summary>
        private static readonly string[] ServiceTypesNotIncludedInMonthlyPayment =
        [
            Clouds42Service.Recognition.ToString(), Clouds42Service.Esdl.ToString(), InvoiceConsts.Comment
        ];

        /// <summary>
        /// Получить номенклатуру с описанием продуктов счетов
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Описание продуктов счета на оплату</returns>
        public static string GetNomenclatureWithProductsDescription(Domain.DataModels.billing.Invoice invoice, string localeName)
        {
            if (!invoice.InvoiceProducts.Any())
                return GetNomenclature(localeName);

            var products = new StringBuilder(InvoiceConsts.ServiceTypesList);
            products.Append(GetProductsDescription(invoice, localeName));
            return products.ToString();
        }

        /// <summary>
        /// Получить описание продуктов счета на оплату
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Описание продуктов счета на оплату</returns>
        public static string GetProductsDescription(Domain.DataModels.billing.Invoice invoice, string localeName) =>
            GetProductsDescription(
                invoice.InvoiceProducts.ToList(), invoice.Period, localeName);

        /// <summary>
        /// Получить описание продуктов счета на оплату
        /// </summary>
        /// <param name="invoiceProducts">Счет на оплату</param>
        /// <param name="period">Период оплаты</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Описание продуктов счета на оплату</returns>
        public static string GetProductsDescription(List<InvoiceProduct> invoiceProducts, int? period,
            string localeName) =>
            GenerateProductsDescription(SelectInvoiceProductsForGenerateDescription(invoiceProducts), period,
                localeName);

        /// <summary>
        /// Получить единицу измерения сервиса
        /// </summary>
        /// <param name="serviceTypeName">Название услуги</param>
        /// <returns>Единицу измерения сервиса</returns>
        private static string GetUnitOfServiceType(string serviceTypeName)
        {
            if (!MapServiceTypenameToUnit.TryGetValue(serviceTypeName, out var unit))
                return InvoiceConsts.UnitCount;

            return unit;
        }

        /// <summary>
        /// Получить номенклатуру для счета на оплату
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Номенклатура для счета на оплату</returns>
        private static string GetNomenclature(string localeName) =>
            localeName == LocaleConst.Ukraine
                ? InvoiceConsts.NomenclatureUa
                : InvoiceConsts.NomenclatureRu;

        /// <summary>
        /// Выбрать продукты счета для формирования описания 
        /// </summary>
        /// <param name="invoiceProducts">Продукты счета на оплату</param>
        /// <returns>Продукты счета на оплату, по которым будет формироваться описание</returns>
        private static List<InvoiceProduct> SelectInvoiceProductsForGenerateDescription(
            List<InvoiceProduct> invoiceProducts) => invoiceProducts
            .Where(invoiceProduct => invoiceProduct.ServiceSum > 0 && !string.IsNullOrEmpty(invoiceProduct.ServiceName))
            .ToList();

        /// <summary>
        /// Сформировать описание по продуктам счета на оплату
        /// </summary>
        /// <param name="products">Продукты счета на оплату</param>
        /// <param name="period">Период оплаты</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Описание по продуктам счета на оплату</returns>
        private static string GenerateProductsDescription(List<InvoiceProduct> products, int? period, string localeName)
        {
            var builder = new StringBuilder();

            var parts = products.Where(w => !ServiceTypesNotIncludedInMonthlyPayment.Contains(w.ServiceName))
                .Select(w => $"{w.Description} {w.Count} {GetUnitOfServiceType(w.ServiceName)}").ToList();

            if (parts.Any())
            {
                var joinedParts = string.Join(", ", parts);
                if (localeName == LocaleConst.Ukraine)
                    joinedParts = joinedParts.Replace(InvoiceConsts.Rent1CRu, InvoiceConsts.Rent1CUa)
                        .Replace(InvoiceConsts.MyDiskRu, InvoiceConsts.MyDiskUa);

                if (period.HasValue)
                {
                    builder.Append(localeName == LocaleConst.Ukraine
                        ? $"({joinedParts}) {InvoiceConsts.PeriodUa} {period.Value} {InvoiceConsts.MonthUa};"
                        : $"({joinedParts}) {InvoiceConsts.PeriodRu} {period.Value} {InvoiceConsts.MonthRu};"
                    );
                }
                else
                {
                    builder.Append($"{joinedParts};");
                }
            }

            var comments = products.FirstOrDefault(com => com.ServiceName == InvoiceConsts.Comment);
            if (comments != null)
            {
                builder.Append($"{comments.Description}");
                return builder.ToString();
            }

            var recognition = products.FirstOrDefault(w => w.ServiceName == Clouds42Service.Recognition.ToString());
            if (recognition != null)
                builder.Append($"{recognition.Description};");

            var doc = products.FirstOrDefault(w => w.ServiceName == Clouds42Service.Esdl.ToString());
            if (doc != null)
                builder.Append($"{doc.Description} {doc.Count} {InvoiceConsts.Year}");

            return builder.ToString();
        }
    }
}