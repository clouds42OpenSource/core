﻿using System.Text;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Invoice.Helpers
{
    /// <summary>
    ///     Класс для формирования тел писем для отправки сервисам
    /// </summary>
    internal static class ServicesEmailBuilder
    {
        /// <summary>
        ///     Получить тело письма о заявке на обещанный платеж
        /// </summary>
        /// <param name="productsInfo">Услуги</param>
        /// <param name="requestDateTime">Дата</param>
        /// <param name="account">Аккаунт</param>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="requestRequisite">ИНН</param>
        /// <param name="requestSum">Сумма</param>
        /// <returns>Письмо</returns>
        public static string GetPromisePaymentNotificationEmail(string productsInfo, DateTime requestDateTime,
            Account account,
            AccountUser accountUser, string requestRequisite, decimal requestSum, ICloudLocalizer cloudLocalizer)
        {
            var text = new StringBuilder();
            text.AppendLine("Сформирована заявка на обещанный платеж");
            text.AppendLine($"Дата: {requestDateTime} <br/>");
            text.AppendLine($"Компания: {account.AccountCaption} <br/>");
            text.AppendLine($"Телефон: {accountUser.PhoneNumber} <br/>");
            text.AppendLine($"Email: {accountUser.Email} <br/>");
            text.AppendLine($"Логин: {accountUser.Login} <br/>");
            text.AppendLine($"Содержание: {productsInfo} <br/>");
            text.AppendLine(
                $"Реквизиты ({cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Inn, account.Id)}): {requestRequisite} <br/>");
            text.AppendLine($"Сумма: {requestSum:f2} <br/>");
            text.AppendLine($"Валюта: {account.AccountConfiguration.Locale.Currency} <br/>");

            var messageBody = text.ToString();
            return messageBody;
        }
    }
}
