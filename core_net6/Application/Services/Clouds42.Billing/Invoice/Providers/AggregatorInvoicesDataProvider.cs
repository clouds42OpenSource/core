﻿using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.Billing.Invoice.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Invoice.Providers
{
    /// <summary>
    /// Провайдер для работы с данными платежей агрегаторов
    /// </summary>
    internal class AggregatorInvoicesDataProvider(
        IUnitOfWork dbLayer)
        : IAggregatorInvoicesDataProvider
    {
        private readonly Lazy<string> _uniqPrefix = new(CloudConfigurationProvider.Invoices.UniqPrefix);

        /// <summary>
        /// Получить данные счетов агрегаторов
        /// </summary>
        /// <param name="dateFrom">Начальный период</param>
        /// <param name="dateTo">Конечный период</param>
        /// <returns>Данные счетов агрегаторов</returns>
        public List<AggregatorInvoiceWithSupplierCodeDataDto> GetAggregatorInvoicesData(DateTime dateFrom,
            DateTime dateTo) => SelectAggregatorInvoicesData(dateFrom, dateTo).ToList();

        /// <summary>
        /// Выбрать данные счетов агрегаторов
        /// </summary>
        /// <param name="dateFrom">Начальный период</param>
        /// <param name="dateTo">Конечный период</param>
        /// <returns>Данные счетов агрегаторов</returns>
        private IEnumerable<AggregatorInvoiceWithSupplierCodeDataDto> SelectAggregatorInvoicesData(DateTime dateFrom,
            DateTime dateTo)
        {
            var paymentsSystems = new List<string>
            {
                PaymentSystem.Robokassa.ToString(),
                PaymentSystem.UkrPays.ToString(),
                PaymentSystem.Yookassa.ToString()
            };

            return dbLayer.PaymentRepository
                .AsQueryableNoTracking()
                .GroupJoin(dbLayer.SupplierRepository.AsQueryable(), z => "ООО \"УчетОнлайн\"", y => y.Name,
                    (payment, suppliers) => new { payment, suppliers })
                .Where(x => x.payment.Date >= dateFrom && x.payment.Date <= dateTo &&
                            x.payment.Invoices.Any(z => z.Date >= dateFrom && z.Date <= dateTo) &&
                            x.payment.Status == PaymentStatus.Done.ToString() &&
                            paymentsSystems.Contains(x.payment.PaymentSystem))
                .Select(x => new
                {
                    x.payment.Invoices,
                    supplierCode = x.payment.Account.AccountConfiguration.Supplier.Code,
                    paymentsSystem = x.payment.PaymentSystem,
                    locale = x.payment.Account.AccountConfiguration.Locale,
                    supplierYoCode = x.suppliers.Any() ? x.suppliers.FirstOrDefault()!.Code : null,
                })
                .ToList()
                .SelectMany(
                    x => x.Invoices.Select(y => new
                    {
                        invoice = y,
                        x.supplierCode,
                        x.paymentsSystem,
                        x.locale,
                        x.supplierYoCode
                    }))
                .Select(x => PrepareAggregatorInvoiceWithSupplierCodeDataDto(x.invoice, x.supplierCode,
                    x.paymentsSystem, x.locale, x.supplierYoCode));
        }

        /// <summary>
        /// Подготовить модель данных счета агрегатора
        /// c указанимем кода поставщика
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <param name="supplierCode">Код поставщика</param>
        /// <param name="paymentSystem">Платежная система</param>
        /// <param name="locale">Локаль аккаунта</param>
        /// <param name="supplierCodeYo"></param>
        /// <returns>Модель данных счета агрегатора
        /// c указанимем кода поставщика</returns>
        private AggregatorInvoiceWithSupplierCodeDataDto PrepareAggregatorInvoiceWithSupplierCodeDataDto(
            Domain.DataModels.billing.Invoice invoice, string supplierCode, string paymentSystem, Locale locale, string supplierCodeYo)
        {

            return new AggregatorInvoiceWithSupplierCodeDataDto
            {
                Id = invoice.Id,
                AccountId = invoice.AccountId,
                Description = InvoiceAdditionalDataHelper.GetProductsDescription(invoice, locale.Name),
                State = invoice.State,
                Uniq = $"{_uniqPrefix.Value}{invoice.Uniq}",
                Sum = invoice.Sum,
                Date = invoice.Date,
                Comment = invoice.Comment,
                Requisite = invoice.Requisite,
                Currency = $"{locale.CurrencyCode}",
                SupplierCode = invoice.InvoiceProducts.Any(x => x.ServiceName == "AlpacaMeet") ? supplierCodeYo : supplierCode,
                PaymentSystem = paymentSystem
            };
        }
            
    }
}
