﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.Invoice.Extensions;
using Clouds42.Billing.Invoice.Validators;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using InvoiceEntity = Clouds42.Domain.DataModels.billing.Invoice;

namespace Clouds42.Billing.Invoice.Providers
{
    /// <inheritdoc/>
    public class InvoiceCreationProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IAccountRequisitesDataProvider accountRequisitesDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IInvoiceCreationProvider
    {
        /// <inheritdoc/>
        public InvoiceEntity CreateInvoice(CreateInvoiceDto model)
        {
            AssertValid(model);
            var account = dbLayer.AccountsRepository.GetAccount(model.AccountId);
            if (account == null)
                throw new NotFoundException($"Не удалось найти аккаунт {model.AccountId}");
            int? payPeriod = model.PayPeriod;

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var newInvoice = AddInvoiceEntity(model.AccountId, model.Total, payPeriod, model.TotalBonus, model.AccountAdditionalCompanyId);
                foreach (var product in model.Products.Where(x => x.Quantity > 0 && x.TotalPrice > 0))
                {
                    AddInvoiceProductEntity(newInvoice, product.Quantity, product.TotalPrice,
                        product.Description, product.Name, product.ServiceTypeId);
                }
                LogInvoiceCreatedEvent(newInvoice);
                dbLayer.Save();
                transaction.Commit();
                return newInvoice;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new InvalidOperationException($"Произошла ошибка при создании счета {ex}");
            }
        }

        /// <inheritdoc/>
        public Task<InvoiceEntity> CreateInvoiceAsync(CreateInvoiceDto model)
        {
            AssertValid(model);
            return CreateInvoiceAuxAsync(model);
        }

        private void AssertValid(CreateInvoiceDto model)
        {
            if (model.Total <= 0)
                throw new InvalidOperationException("Создание счета возможно на сумму, большую 0");

            if (!model.Products.Any(x => x.Quantity > 0 && x.TotalPrice > 0))
                throw new InvalidOperationException("В калькуляции инвойсов нет ни одного продукта");

            if (model.PayPeriod is < 0)
                throw new InvalidOperationException("Платежный период не может быть отрицательным");
        }

        private async Task<InvoiceEntity> CreateInvoiceAuxAsync(CreateInvoiceDto invoiceModel)
        {
            var account = await dbLayer.AccountsRepository.GetAccountAsync(invoiceModel.AccountId);
            if (account == null)
                throw new NotFoundException($"Не удалось найти аккаунт {invoiceModel.AccountId}");
            int? payPeriod = invoiceModel.PayPeriod;
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var invoice = AddInvoiceEntity(invoiceModel.AccountId, invoiceModel.Total, payPeriod, invoiceModel.TotalBonus, invoiceModel.AccountAdditionalCompanyId);

                foreach (var product in invoiceModel.Products.Where(x => x.Quantity > 0 && x.TotalPrice > 0))
                {
                    AddInvoiceProductEntity(invoice, product.Quantity, product.TotalPrice,
                        product.Description, product.Name, product.ServiceTypeId);
                }
                await dbLayer.SaveAsync();
                LogInvoiceCreatedEvent(invoice);
                transaction.Commit();
                return invoice;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new InvalidOperationException($"Произошла ошибка при создании счета {ex}", ex);
            }
        }
        private InvoiceEntity AddInvoiceEntity(Guid accountId, decimal sum, int? period, decimal bonusReward, Guid? accountAdditionalCompanyId)
        {
            var accountRequisites = accountRequisitesDataProvider.GetIfExistsOrCreateNew(accountId);
            var inn = accountRequisites.Inn;

            if (accountAdditionalCompanyId.HasValue)
            {
                var additionalCompany = dbLayer.AccountAdditionalCompanyRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefault(x => x.Id == accountAdditionalCompanyId && x.AccountId == accountId);

                if (additionalCompany != null)
                {
                    inn = additionalCompany.Inn;
                }
            }

            var newInvoice = new InvoiceEntity
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Date = DateTime.Now,
                Requisite = inn,
                Sum = sum,
                State = InvoiceStatus.Processing.ToString(),
                Period = period,
                BonusReward = bonusReward,
                AccountAdditionalCompanyId = accountAdditionalCompanyId
            };
            dbLayer.InvoiceRepository.AddInvoice(newInvoice);
            return newInvoice;
        }
        private void AddInvoiceProductEntity(InvoiceEntity invoice, int count, decimal serviceSum,
            string description, string serviceName, Guid? serviceTypeId = null)
        {
            var invoiceProduct = new InvoiceProduct
            {
                Id = Guid.NewGuid(),
                Count = count,
                Description = description,
                InvoiceId = invoice.Id,
                ServiceName = serviceName,
                ServiceSum = serviceSum,
                ServiceTypeId = serviceTypeId
            };
            dbLayer.InvoiceProductRepository.Insert(invoiceProduct);
        }
        private void LogInvoiceCreatedEvent(InvoiceEntity invoice)
        {
            string purposeString = (invoice.Period ?? 0) == 0 ?
                "на доп. сервисы" :
                $"на {invoice.Period ?? 0} мес.";
            var invoiceTitlePrefix = CloudConfigurationProvider.Invoices.UniqPrefix();

            var localeName = accountConfigurationDataProvider.GetAccountLocale(invoice.AccountId).Name;
            var message = $"Выставлен счет №{invoiceTitlePrefix}{invoice.Uniq} {purposeString} на сумму {invoice.Sum:0.00} {localeName}.";

            LogEventHelper.LogEvent(dbLayer, invoice.AccountId, accessProvider, LogActions.AddInvoice, message);
        }
    }

    /// <inheritdoc/>
    public class ServiceCalulcInvoiceCreationProvider(
        IInvoiceCalculatorProvider invoiceCalculatorProvider,
        IInvoiceCreationProvider innerInvoiceCreationProvider)
        : IInvoiceCreationProvider<InvoiceCalculationDto>
    {
        /// <inheritdoc/>
        public Task<InvoiceEntity> CreateInvoiceAsync(InvoiceCalculationDto model)
        {
            //ValidateBinding and prepare dto for inner provider
            var createInvoiceDto = ValidateAndConvertToInvoiceCreationDc(model);

            //Process by inner provider
            return innerInvoiceCreationProvider.CreateInvoiceAsync(createInvoiceDto);
        }

        /// <inheritdoc/>
        public InvoiceEntity CreateInvoice(InvoiceCalculationDto model)
        {
            //ValidateBinding and prepare dto for inner provider
            var createInvoiceDto = ValidateAndConvertToInvoiceCreationDc(model);

            //Process by inner provider
            return innerInvoiceCreationProvider.CreateInvoice(createInvoiceDto);

        }

        private CreateInvoiceDto ValidateAndConvertToInvoiceCreationDc(InvoiceCalculationDto calculation)
        {
            //Load calculator
            var productInputs = calculation.Products.Select(x => new ProductCalculatorInput(x.Identifier, x.Quantity, true));
            var calculator = invoiceCalculatorProvider.GetInvoiceCalculatorForAccount(
                calculation.AccountId,
                calculation.InvoicePurposeType,
                calculation.PayPeriod,
                productInputs);

            //ValidateBinding provided calculation
            if (!calculator.AsValidator().IsValid(calculation))
                throw new InvalidOperationException("Предоставленная калькуляция инвойса не валидна");
            return calculator.GetCreateInvoiceDc(calculation.AccountId, calculation.AccountAdditionalCompanyId);
        }

        private sealed class ProductCalculatorInput(string identifier, int quantity, bool isActive)
            : IInvoiceProductCalculatorInput
        {
            public bool IsActive { get; } = isActive;
            public ServiceCalculatorIdentifier Identifier { get; } = ServiceCalculatorIdentifier.Parse(identifier);
            public int Quantity { get; } = quantity;
        }
    }

    /// <inheritdoc/>
    public class ArbitraryAmountInvoiceCreationProvider(IInvoiceCreationProvider innerProvider)
        : IInvoiceCreationProvider<CreateArbitraryAmountInvoiceDto>
    {
        /// <inheritdoc/>
        public InvoiceEntity CreateInvoice(CreateArbitraryAmountInvoiceDto model)
        {
            var createInvoiceDc = ValidateAndConvertToCreateInvoiceDc(model);
            return innerProvider.CreateInvoice(createInvoiceDc);
        }

        /// <inheritdoc/>
        public Task<InvoiceEntity> CreateInvoiceAsync(CreateArbitraryAmountInvoiceDto model)
        {
            var createInvoiceDc = ValidateAndConvertToCreateInvoiceDc(model);
            return innerProvider.CreateInvoiceAsync(createInvoiceDc);
        }

        private CreateInvoiceDto ValidateAndConvertToCreateInvoiceDc(CreateArbitraryAmountInvoiceDto arbitraryAmountModel)
        {
            if (arbitraryAmountModel.Amount <= 0)
                throw new InvalidOperationException("Сумма должна быть больше 0");
            var result = new CreateInvoiceDto
            {
                AccountId = arbitraryAmountModel.AccountId,
                PayPeriod = default,
                Total = arbitraryAmountModel.Amount,
                Products =
                [
                    new()
                    {
                        Description =
                            string.IsNullOrWhiteSpace(arbitraryAmountModel.Description)
                                ? "-"
                                : arbitraryAmountModel.Description,
                        Name = "Comment",
                        Quantity = 1,
                        TotalPrice = arbitraryAmountModel.Amount
                    }
                ]
            };
            return result;
        }

    }
}
