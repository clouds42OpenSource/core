﻿using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Invoice.Providers
{
    /// <summary>
    /// Провайдер для работы с данными счета для отчета 
    /// </summary>
    internal class InvoiceReportDataProvider(IUnitOfWork dbLayer) : IInvoiceReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по счету для отчета
        /// </summary>
        /// <returns>Список моделей данных по счету для отчета</returns>
        public List<InvoiceReportDataDto> GetInvoiceReportDataDcs()
        {
            var previousTwoYearsDate = DateTime.Now.AddYears(-2);

            return dbLayer.InvoiceRepository
                .AsQueryableNoTracking()
                .Where(x => x.Date > previousTwoYearsDate)
                .Select(x => new InvoiceReportDataDto
                {
                    AccountNumber = x.Account.IndexNumber, 
                    DateOfCreation = x.Date,
                    InvoiceNumber = $"{CloudConfigurationProvider.Invoices.UniqPrefix()}{x.Uniq}",
                    Sum = x.Sum,
                    Period = x.Period,
                    PaymentSystem = x.Payment.PaymentSystem,
                    StatusString = x.State,
                    Country = x.Account.AccountConfiguration.Locale.Country
                })
                .OrderByDescending(x => x.DateOfCreation)
                .ToList();
        }
    }
}
