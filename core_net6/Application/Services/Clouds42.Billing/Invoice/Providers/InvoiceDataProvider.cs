﻿using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Billing.Invoice.Extensions;
using Clouds42.Billing.Invoice.Selectors;
using Clouds42.Billing.Invoice.Helpers;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Domain.DataModels;

namespace Clouds42.Billing.Invoice.Providers
{
    /// <summary>
    /// Провайдер для работы с данными счета на оплату
    /// </summary>
    internal class InvoiceDataProvider(InvoiceDataSelector invoiceDataSelector, ICloudLocalizer cloudLocalizer) : IInvoiceDataProvider
    {
        /// <summary>
        /// Получить данные счета на оплату
        /// (модель включает в себя все возможнные данные по счету,
        /// которые нужны для формирования и печати)
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <returns>Модель данных счета на оплату</returns>
        public InvoiceDc GetInvoiceData(Domain.DataModels.billing.Invoice invoice)
        {
            var invoiceFullData = invoiceDataSelector.GetInvoiceFullData(invoice.Id);
            return new InvoiceDc
            {
                Id = invoice.Id,
                AccountId = invoice.AccountId,
                InvoiceDate = invoice.Date,
                InvoiceSum = invoice.Sum,
                Description = invoice.Description,
                Comment = invoice.Comment,
                Requisites = invoice.Requisite,
                Locale = invoiceFullData.Locale.MapToLocaleDc(),
                State = invoice.GetInvoiceStatus(),
                Uniq = invoice.GetInvoiceFullUniq(),
                ActId = invoice.ActID,
                ActDescription = invoice.ActDescription,
                BuyerDescription = invoice.GetBuyerDescription((Account)invoiceFullData.Account, invoiceFullData.AccountAdditionalCompany, cloudLocalizer),
                ReceiptFiscalNumber = invoiceFullData.ReceiptFiscalNumber,
                SupplierRazorTemplate = GetSupplierRazorTemplateDc(invoiceFullData),
                AdditionalDataDescription = GetAdditionalDataDescription(invoiceFullData, invoice.Period)
            };
        }

        /// <summary>
        /// Получить дополнительное описание счета на оплату
        /// </summary>
        /// <param name="invoiceFullData">Полные данные счета на оплату</param>
        /// <param name="invoicePeriod">Период оплаты счета</param>
        /// <returns>Дополнительное описание счета на оплату</returns>
        private static string GetAdditionalDataDescription(InvoiceFullDataDto invoiceFullData, int? invoicePeriod)
        {
            var description = InvoiceAdditionalDataHelper.GetProductsDescription(invoiceFullData.InvoiceProducts,
                invoicePeriod, invoiceFullData.Locale.Name);

            return !string.IsNullOrEmpty(description) ? description : "-";
        }

        /// <summary>
        /// Получить модель шаблона счета на оплату
        /// </summary>
        /// <param name="invoiceFullData">Полные данные счета на оплату</param>
        /// <returns>Модель шаблона счета на оплату</returns>
        private static SupplierRazorTemplateDto GetSupplierRazorTemplateDc(InvoiceFullDataDto invoiceFullData) =>
            SupplierModelsDtoMapper.MapToSupplierRazorTemplateDc(invoiceFullData.SupplierId,
                invoiceFullData.InvoiceHtmlFormData, invoiceFullData.InvoiceHtmlFormFiles);
    }
}
