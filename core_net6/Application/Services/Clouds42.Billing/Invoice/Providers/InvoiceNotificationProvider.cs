﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.DataContracts.BaseModel;
using Clouds42.LetterNotification.LetterNotifications.Invoice;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Invoice.Providers
{
    /// <summary>
    ///     Класс для обработки уведомлений "Выставление счета"
    /// </summary>
    public class InvoiceNotificationProvider(
        IUnitOfWork dbLayer,
        IInvoiceDataProvider invoiceDataProvider,
        IInvoiceDocumentBuilder invoiceDocumentBuilder,
        ILetterNotificationProcessor letterNatificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : IInvoiceNotificationProvider
    {
        private readonly Lazy<string> _invoiceAttachFileName = new(CloudConfigurationProvider.Invoices.GetInvoiceAttachFileName);
        private readonly Lazy<string> _uniqPrefix = new(CloudConfigurationProvider.Invoices.UniqPrefix);
        private readonly Lazy<string> _routeForOpenBalancePage = new(CloudConfigurationProvider.Cp.GetRouteForOpenBalancePage);

        /// <summary>
        /// Создать уведомление счета
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        public void NotifyInvoiceCreated(Guid invoiceId, Guid recipientAccountUserId)
        {
            var invoice = dbLayer.InvoiceRepository.GetInvoiceById(invoiceId) ??
                throw new NotFoundException($"Не удалось найти инвойс по Id {invoiceId}");
            var recipient = dbLayer.AccountUsersRepository
                .FirstOrDefault(au => au.Id == recipientAccountUserId &&
                    au.AccountId == invoice.AccountId) ??
                throw new NotFoundException($"Не удалось получить пользователя по Id {invoice.AccountId}");

            var fullInvoiceData = invoiceDataProvider.GetInvoiceData(invoice);
            var invoiceDocument = invoiceDocumentBuilder.Build(fullInvoiceData);

            var copyRecipientsEmailAddresses = dbLayer
                .AccountEmailRepository
                .WhereLazy(w => w.AccountId == invoice.AccountId)
                .Select(w => w.Email)
                .Where(email => email != recipient.Email)
                .ToList();

            var siteAuthorityUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(invoice.AccountId);

            var letterModel = new SendInvoiceToClientLetterModelDto
            {
                AccountId = invoice.AccountId,
                InvoiceId = invoiceId,
                AccountUserId = recipientAccountUserId,
                InvoiceDocument = new DocumentDataDto
                {
                    FileName = _invoiceAttachFileName.Value,
                    Bytes = invoiceDocument.Bytes
                },
                EmailsToCopy = copyRecipientsEmailAddresses,
                InvoiceSum = invoice.Sum,
                InvoiceNumber = $"{_uniqPrefix.Value}{invoice.Uniq}",
                BalancePageUrl = new Uri($"{siteAuthorityUrl}/{_routeForOpenBalancePage.Value}").AbsoluteUri
            };
            letterNatificationProcessor
                .TryNotify<SendInvoiceToClientLetterNotification, SendInvoiceToClientLetterModelDto>(letterModel);
        }
    }
}
