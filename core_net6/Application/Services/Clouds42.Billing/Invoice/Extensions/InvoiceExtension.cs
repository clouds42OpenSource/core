﻿using Clouds42.Billing.Invoice.Constants;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Invoice.Extensions
{
    /// <summary>
    /// Расширение для работы со счетом на оплату
    /// </summary>
    public static class InvoiceExtension
    {
        /// <summary>
        /// Префикс номера счета
        /// </summary>
        private static readonly string InvoiceUniqPrefix = CloudConfigurationProvider.Invoices.UniqPrefix();

        /// <summary>
        /// Получить статус счета на оплату
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <returns>Статус счета на оплату</returns>
        public static InvoiceStatus GetInvoiceStatus(this Domain.DataModels.billing.Invoice invoice) => invoice.State != null
            ? (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), invoice.State)
            : InvoiceStatus.Processing;

        /// <summary>
        /// Получить описание покупателя
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <param name="account">Аккаунт который принадлежит счету</param>
        /// <returns>Описание покупателя</returns>
        public static string GetBuyerDescription(this Domain.DataModels.billing.Invoice invoice, Account account, AccountAdditionalCompany accountAdditionalCompany, ICloudLocalizer cloudLocalizer)
        {
            var indexNumber = accountAdditionalCompany != null ? accountAdditionalCompany.Number : account.IndexNumber.ToString();
            var caption = accountAdditionalCompany != null ? accountAdditionalCompany.Name : account.AccountCaption;
            var buyerDescription =
                indexNumber != caption
                    ? caption
                    : $"{InvoiceConsts.Account42Clouds} №{indexNumber}";

            if (!string.IsNullOrEmpty(invoice.Requisite))
                buyerDescription += $", {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Inn, account.Id)} " +
                                    invoice.Requisite;

            return buyerDescription;
        }

        /// <summary>
        /// Получить полный номер счета(с префиксом)
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <returns>Полный номер счета</returns>
        public static string GetInvoiceFullUniq(this Domain.DataModels.billing.Invoice invoice) => InvoiceUniqPrefix + invoice.Uniq;
    }
}
