﻿using Clouds42.DataContracts.Account.Locale;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using Clouds42.DataContracts.Billing.Inovice;

namespace Clouds42.Billing.Invoice.Extensions
{
    public static class InvoiceCalculatorMapper
    {
        public static InvoiceCalculatorDto? CreateCalculatorDataContract(
            this IInvoiceCalculator source)
        {
            if (source == null) return null;
            var result = new InvoiceCalculatorDto
            {
                AccountId = source.AccountId,
                BuyerName = source.BuyerName,
                SupplierName = source.SupplierName,
                Currency = new CurrencyDto
                {
                    Currency = source.Currency,
                    CurrencyCode = source.CurrencyCode
                },
                PayPeriod = source.PayPeriod,
                TotalBeforeVAT = source.TotalBeforeVAT,
                VAT = CalculateVat(source.Currency, source.TotalAfterVAT, source.VAT),
                TotalAfterVAT = source.TotalAfterVAT,
                TotalBonus = source.TotalBonus,
                Products = source.Products.OrderBy(x => x.Identifier.ServiceCalculatorType)
                    .Select(p => p.CreateProductCalculatorDataContract())
                    .ToList(),
                PayPeriodControl = source.PayPeriodControl.CreateCalculatorControlDataContract()
            };

            return result;
        }

        private static decimal CalculateVat(string currency, decimal totalAfterVAT, decimal vat)
        {
            switch (currency)
            {
                case "грн":
                    return (totalAfterVAT * 20M) / 120M;
                case "тг":
                    return (totalAfterVAT / 1.12M) * 0.12M;
                default:
                    return vat;
            }
        }

        public static InvoiceProductCalculatorDc? CreateProductCalculatorDataContract(
            this IInvoiceProductCalculator source)
        {
            if (source == null) return null;
            var res = new InvoiceProductCalculatorDc
            {
                Description = source.Description,
                Identifier = source.Identifier.ToString(),
                IsActive = source.IsActive,
                Name = source.Name,
                PayPeriod = source.PayPeriod,
                Quantity = source.BillableQuantity,
                Rate = source.Rate,
                TotalBonus = source.TotalBonus,
                TotalPrice = source.TotalPrice,
                QuantityControl = source.QuantityControl.CreateCalculatorControlDataContract()
            };
            return res;
        }
        public static CalculatorControlDto? CreateCalculatorControlDataContract<T>(
            this ICalculatorControl<T> source)
        {
            if (source == null) return null;
            return new CalculatorControlDto
            {
                Type = source.GetControlType(),
                Settings = source.GetControlSettings().ToList()
            };
        }

        public static CreateInvoiceDto GetCreateInvoiceDc(this IInvoiceCalculator calculator,
            Guid accountId, Guid? accountAdditionalCompanyId) =>
            calculator == null ? new CreateInvoiceDto() :
            new CreateInvoiceDto
            {
                AccountId = accountId,
                PayPeriod = calculator.PayPeriod,
                Products = calculator.Products.Where(x => x.IsActive).Select(x =>
                      new CreateInvoiceProductDc
                      {
                          Name = x.NomenclatureName,
                          Description = x.NomenclatureDescription,
                          Quantity = x.TotalQuantity,
                          ServiceTypeId = x.ServiceTypeId,
                          TotalPrice = x.TotalPrice
                      }).ToList(),
                Total = calculator.TotalAfterVAT,
                TotalBonus = calculator.TotalBonus,
                AccountAdditionalCompanyId = accountAdditionalCompanyId
            };
    }
}
