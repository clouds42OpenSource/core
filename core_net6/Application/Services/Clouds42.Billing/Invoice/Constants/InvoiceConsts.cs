﻿namespace Clouds42.Billing.Invoice.Constants
{
    /// <summary>
    /// Константы счета на оплату
    /// </summary>
    public static class InvoiceConsts
    {
        /// <summary>
        /// Аккаунт 42 облака
        /// </summary>
        public const string Account42Clouds = "Аккаунт 42Clouds";

        /// <summary>
        /// Услуга - Страницы (распознование)
        /// </summary>
        public const string ServiceTypePages = "Pages";

        /// <summary>
        /// Услуга - Лицензии (распознование документов)
        /// </summary>
        public const string ServiceTypeLicense = "License";

        /// <summary>
        /// Услуга - Мой диск
        /// </summary>
        public const string ServiceTypeMyDisk = "MyDisk";

        /// <summary>
        /// Единица измерения в ГБ
        /// </summary>
        public const string UnitGb = "Гб";

        /// <summary>
        /// Единица измерения в количестве
        /// </summary>
        public const string UnitCount = "шт";

        /// <summary>
        /// Год
        /// </summary>
        public const string Year = "год";

        /// <summary>
        /// Период
        /// </summary>
        public const string PeriodRu = "период";

        /// <summary>
        /// Період
        /// </summary>
        public const string PeriodUa = "період";

        /// <summary>
        /// Міс
        /// </summary>
        public const string MonthUa = "міс";

        /// <summary>
        /// Месяц
        /// </summary>
        public const string MonthRu = "мес";

        /// <summary>
        /// Мій Дис
        /// </summary>
        public const string MyDiskUa = "Мій Диск";

        /// <summary>
        /// Мой Диск
        /// </summary>
        public const string MyDiskRu = "Мой Диск";

        /// <summary>
        /// Аренда
        /// </summary>
        public const string Rent1CRu = "Аренда";

        /// <summary>
        /// Оренда
        /// </summary>
        public const string Rent1CUa = "Оренда";

        /// <summary>
        /// Комментарий
        /// </summary>
        public const string Comment = nameof(Comment);

        /// <summary>
        /// Номенклатура на русском языке
        /// </summary>
        public const string NomenclatureRu = "Предоставление права для использования лицензионного обеспечения";

        /// <summary>
        /// Номенклатура на украинском языке
        /// </summary>
        public const string NomenclatureUa = "Надання прав на використання ліцензійного забезпечення";

        /// <summary>
        /// Список услуг
        /// </summary>
        public const string ServiceTypesList = "Список услуг: ";
    }
}
