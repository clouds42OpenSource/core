﻿using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.Invoice.Extensions;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums.Billing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Invoice.Managers
{
    /// <inheritdoc/>
    public class InvoiceCalculatorManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IInvoiceCalculatorProvider invoiceCalculatorProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IInvoiceCalculatorManager
    {
     
        /// <inheritdoc/>
        public ManagerResult<InvoiceCalculatorDto?> GetInvoiceCalculatorForAccount(Guid accountId, InvoicePurposeType invoiceType)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_View, () => accountId);
                var calculator = invoiceCalculatorProvider.GetInvoiceCalculatorForAccount(accountId, invoiceType);
                return Ok(calculator.CreateCalculatorDataContract());
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[При пересчете калькулятора инвйоса для {accountId} произошла ошибка]");
                return PreconditionFailed<InvoiceCalculatorDto?>(ex.Message);
            }
        }

        /// <inheritdoc/>
        public ManagerResult<InvoiceCalculatorDto?> RecalculateInvoiceForAccount(RecalculateInvoiceDto request)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_View, () => request.AccountId);
                var productInputsDto = request.Products
                    .Select(x => new ProductCalculatorInput(x.Identifier, x.Quantity, x.IsActive));
                var calculator = invoiceCalculatorProvider.GetInvoiceCalculatorForAccount(
                    request.AccountId,
                    request.InvoicePurposeType,
                    request.PayPeriod,
                    productInputsDto);
                return Ok(calculator.CreateCalculatorDataContract());
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[При пересчете калькулятора инвйоса произошла ошибка]");
                return PreconditionFailed<InvoiceCalculatorDto?>(ex.Message);
            }

        }

        /// <summary>
        /// DTO имплементация интерфейса IInvoiceProductCalculatorInput для передачи данных во внутриенний провайдер
        /// </summary>
        private sealed class ProductCalculatorInput(string identifier, int quantity, bool isActive)
            : IInvoiceProductCalculatorInput
        {
            /// <inheritdoc/>
            public bool IsActive { get; } = isActive;

            /// <inheritdoc/>
            public ServiceCalculatorIdentifier Identifier { get; } = ServiceCalculatorIdentifier.Parse(identifier);

            /// <inheritdoc/>
            public int Quantity { get; } = quantity;
        }
    }
}
