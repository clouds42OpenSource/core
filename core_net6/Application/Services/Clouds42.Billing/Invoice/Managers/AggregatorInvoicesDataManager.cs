﻿using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Invoice.Managers
{
    /// <summary>
    /// Менеджер для работы с данными платежей агрегаторов
    /// </summary>
    public class AggregatorInvoicesDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAggregatorInvoicesDataProvider aggregatorInvoicesDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IAggregatorInvoicesDataManager
    {

        /// <summary>
        /// Получить данные счетов агрегаторов
        /// </summary>
        /// <param name="dateFrom">Начальный период</param>
        /// <param name="dateTo">Конечный период</param>
        /// <returns>Данные счетов агрегаторов</returns>
        public ManagerResult<List<AggregatorInvoiceWithSupplierCodeDataDto>> GetAggregatorInvoicesData(
            DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Invoices_Confirmation);
                return Ok(aggregatorInvoicesDataProvider.GetAggregatorInvoicesData(dateFrom, dateTo));
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"При получении списка платежей агрегаторов за период с {dateFrom:dd.MM.yyyy HH:mm:ss} по {dateTo:dd.MM.yyyy HH:mm:ss} произошла ошибка.";
                handlerException.Handle(ex, $"[Ошибка получения списка платежей агрегатора] {errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed<List<AggregatorInvoiceWithSupplierCodeDataDto>>($"{errorMessage}. Причина: {ex.Message}");
            }
        }
    }
}
