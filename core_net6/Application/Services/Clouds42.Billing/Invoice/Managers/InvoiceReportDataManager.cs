﻿using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Invoice.Managers
{

    /// <summary>
    /// Менеджер для работы с данными счета для отчета 
    /// </summary>
    public class InvoiceReportDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IInvoiceReportDataProvider invoiceReportDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), IInvoiceReportDataManager
    {
        /// <summary>
        /// Получить список моделей данных по счету для отчета
        /// </summary>
        /// <returns>Список моделей данных по счету для отчета</returns>
        public ManagerResult<List<InvoiceReportDataDto>> GetInvoiceReportDataDcs()
        {
            try
            {
                logger.Info("Получение список моделей данных по счету для отчета");
                AccessProvider.HasAccess(ObjectAction.GetInvoiceReportDataDcs);
                var data = invoiceReportDataProvider.GetInvoiceReportDataDcs();
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка моделей данных по счету для отчета]");
                return PreconditionFailed<List<InvoiceReportDataDto>>(ex.Message);
            }
        }
    }
}
