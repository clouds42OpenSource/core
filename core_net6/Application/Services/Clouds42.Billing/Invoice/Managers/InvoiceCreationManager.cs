﻿using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Invoice.Managers
{
    /// <inheritdoc/>
    public class InvoiceCreationManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IInvoiceCreationProvider<InvoiceCalculationDto> invoiceCreationFromCalculationProvider,
        IInvoiceCreationProvider<CreateArbitraryAmountInvoiceDto> invoiceCreationFromArbitraryAmountProvider,
        IInvoiceNotificationProvider invoiceNotificationProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IInvoiceCreationManager
    {
        /// <inheritdoc/>
        public ManagerResult<Guid> CreateInvoiceFromCalculation(InvoiceCalculationDto calculation,
            Guid? notifyAccountUserId = null)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_Bill, () => calculation.AccountId);
                var invoice = invoiceCreationFromCalculationProvider.CreateInvoice(calculation);
                TryNotifyInvoice(invoice.Id, notifyAccountUserId);

                //Return Result
                return Ok(invoice.Id);
            }
            catch (Exception ex)
            {
                Logger.Warn(ex, "[Ошибка создания инвойса на основании калькуляции]");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <inheritdoc/>
        public ManagerResult<Guid> CreateArbitraryAmountInvoice(CreateArbitraryAmountInvoiceDto request,
            Guid? notifyAccountUserId = null)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Payments_Bill, () => request.AccountId);
                var invoice = invoiceCreationFromArbitraryAmountProvider.CreateInvoice(request);
                TryNotifyInvoice(invoice.Id, notifyAccountUserId);
                return Ok(invoice.Id);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[При создании инвойса на произольную сумму произошла ошибка.]");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        private void TryNotifyInvoice(Guid invoiceId, Guid? notifyAccountUserId)
        {
            if (!notifyAccountUserId.HasValue)
                return;
            try
            {
                invoiceNotificationProvider.NotifyInvoiceCreated(invoiceId, notifyAccountUserId.Value);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Не удалось отправить уведомление о создании инвойса {invoiceId} пользователю {notifyAccountUserId}]");
            }
        }
    }
}
