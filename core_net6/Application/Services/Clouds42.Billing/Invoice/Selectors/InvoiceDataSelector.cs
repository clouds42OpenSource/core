﻿using Clouds42.Billing.Contracts.Invoice.Interfaces.Selectors;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Billing.Invoice.Selectors
{
    /// <summary>
    /// Класс для выбора данных счета на оплату
    /// </summary>
    public class InvoiceDataSelector(
        IUnitOfWork dbLayer)
        : IInvoiceDataSelector
    {
        /// <summary>
        /// Получить все(полные) данные счета на оплату 
        /// </summary>
        /// <param name="invoiceId">Id счета на оплату</param>
        /// <returns>Полные данные счета на оплату</returns>
        public InvoiceFullDataDto GetInvoiceFullData(Guid invoiceId)
        {
            var invoiceModel = dbLayer
                .InvoiceRepository
                .AsQueryableNoTracking()
                .AsSplitQuery()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Locale)
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Supplier)
                .ThenInclude(x => x.PrintedHtmlFormInvoice)
                .ThenInclude(x => x.Files)
                .ThenInclude(x => x.CloudFile)
                .Include(x => x.InvoiceReceipt)
                .Include(x => x.InvoiceProducts)
                .FirstOrDefault(x => x.Id == invoiceId) ?? throw new NotFoundException($"Не удалось получить полные данные счета на оплату '{invoiceId}'");


            var invoiceDto = new InvoiceFullDataDto
                              {
                                  Account = invoiceModel.Account,
                                  AccountAdditionalCompanyId = invoiceModel.AccountAdditionalCompanyId,
                                  Locale = invoiceModel.Account.AccountConfiguration.Locale,
                                  SupplierId = invoiceModel.Account.AccountConfiguration.SupplierId,
                                  InvoiceHtmlFormData = invoiceModel.Account.AccountConfiguration.Supplier.PrintedHtmlFormInvoice.HtmlData,
                                  InvoiceHtmlFormFiles = invoiceModel.Account.AccountConfiguration.Supplier.PrintedHtmlFormInvoice.Files.Select(file => file.CloudFile).ToList(),
                                  ReceiptFiscalNumber = invoiceModel.InvoiceReceipt?.FiscalNumber,
                                  InvoiceProducts = invoiceModel.InvoiceProducts.ToList()
                              };

            if(invoiceDto.InvoiceProducts.FirstOrDefault(p => p.ServiceName == "AlpacaMeet") != null
                                  && invoiceDto.InvoiceProducts.Count == 1)
            {
                var invoiceDtoAlpaca = (from invoice in dbLayer.InvoiceRepository.WhereLazy()
                                  join supplier in dbLayer.SupplierRepository.WhereLazy() on "ООО \"УчетОнлайн\"" equals supplier.Name
                                        join printedHtmlFormInvoice in dbLayer.PrintedHtmlFormRepository.WhereLazy() on supplier
                                      .PrintedHtmlFormInvoiceId equals printedHtmlFormInvoice.Id
                                  join printedHtmlFormFile in dbLayer.PrintedHtmlFormFileRepository.WhereLazy() on
                                      printedHtmlFormInvoice.Id equals printedHtmlFormFile.PrintedHtmlFormId into printedHtmlFormFiles
                                  where invoice.Id == invoiceId
                                  select new InvoiceFullDataDto
                                  {
                                      SupplierId = supplier.Id,
                                      InvoiceHtmlFormData = printedHtmlFormInvoice.HtmlData,
                                      InvoiceHtmlFormFiles = printedHtmlFormFiles.Select(file => file.CloudFile).ToList(),
                                  }).FirstOrDefault() ??
                                    throw new NotFoundException($"Не удалось получить полные данные счета на оплату '{invoiceId}'");

                invoiceDto.SupplierId = invoiceDtoAlpaca.SupplierId;
                invoiceDto.InvoiceHtmlFormData = invoiceDtoAlpaca.InvoiceHtmlFormData;
                invoiceDto.InvoiceHtmlFormFiles = invoiceDtoAlpaca.InvoiceHtmlFormFiles;
            }


            if (invoiceDto.AccountAdditionalCompanyId.HasValue)
            {
                invoiceDto.AccountAdditionalCompany = dbLayer.AccountAdditionalCompanyRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefault(x => x.Id == invoiceDto.AccountAdditionalCompanyId);
            }

            return invoiceDto;

        }
           
    }
}
