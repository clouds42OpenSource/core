﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.Billing
{
    /// <summary>
    /// Модель отображения создания тестовых данных
    /// </summary>
    public class CreationTestDataViewModel
    {
        [Display(Name = "Аккаунт")]
        [Required(ErrorMessage = "Аккаунт - обязательное поле")]
        public string AccountCaption { get; set; }
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Количество пользователей аккаунта
        /// </summary>
        [Required(ErrorMessage = "Не установленно количество пользователей")]
        public int AccountUsersCount { get; set; }

        /// <summary>
        /// Количество информационных баз пользователя
        /// </summary>
        [Required(ErrorMessage = "Не установленно количество информационных баз")]
        public int AccountDatabasesCount { get; set; }
    }
}
