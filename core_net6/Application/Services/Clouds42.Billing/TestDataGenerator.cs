﻿using Clouds42.AlphaNumericsSupport;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Common.Constants;
using Clouds42.Common.Encrypt;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Domain.Enums.DbTemplateUpdates;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Billing
{
    /// <summary>
    /// Генератор тестовых данных
    /// </summary>
    public class TestDataGenerator
    {
        private readonly string _random;
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;

        public TestDataGenerator(IConfiguration configuration, IUnitOfWork unitOfWork)
        {
            var alphaNumericsGenerator = new AlphaNumericsGenerator(new CryptoRandomGeneric());
            _random = alphaNumericsGenerator.GetRandomString(4, AlphaNumericsTypes.Numerics);
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Сгенерировать пользователя
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns></returns>
        public AccountUser GenerateAccountUser(Guid accountId)
        {
            var accountUser = new AccountUser
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Activated = true,
                Login = $"Test{Guid.NewGuid():N}",
                Password = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric()).GeneratePassword(),
                Email = $"Email{Guid.NewGuid():N}@efsol.ru",
                FirstName = $"Роберт{_random}",
                LastName = $"Чарльз{_random}",
                MiddleName = $"Дарвин{_random}",
                FullName = "Чарльз Рооберт Дарвин",
                PhoneNumber = $"9964536742{_random}",
                Comment = "Тестовый",
                CreatedInAd = true,
                CreationDate = DateTime.Now,
                EmailStatus = "Checked",
                CorpUserSyncStatus = "Added"
            };
            return accountUser;
        }

        /// <summary>
        /// Сгенерировать список пользователей по количеству
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="count">Количество пользователей</param>
        /// <returns></returns>
        public List<AccountUser> GenerateListAccountUsersByCount(int count, Guid accountId)
        {
            var accountUsers = new List<AccountUser>();

            for (int i = 0; i < count; i++)
            {
                accountUsers.Add(GenerateAccountUser(accountId));
            }

            return accountUsers;
        }

        /// <summary>
        /// Сгенерировать поддержку информационной базы
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="configurationName">Название конфигурации</param>
        /// <param name="hasAutoUpdate">Наличие авто обновления</param>
        /// <param name="hasSupport">Наличие поддержки</param>
        /// <param name="supportState">Статус поддержки</param>
        /// <returns>Поддержка информационной базы</returns>
        public AcDbSupport GenerateAcDbSupport(Guid accountDatabaseId, string configurationName,
            bool hasAutoUpdate = true, bool hasSupport = true,
            SupportState supportState = SupportState.AutorizationSuccess)
        {
            return new AcDbSupport
            {
                AccountDatabasesID = accountDatabaseId,
                ConfigurationName = configurationName,
                CurrentVersion = "10.3.49.1",
                HasAutoUpdate = hasAutoUpdate,
                HasSupport = hasSupport,
                Login = "NikitaBeta",
                OldVersion = "10.3.49.1",
                Password = "123456",
                PreparedForTehSupport = true,
                PrepearedForUpdate = true,
                State = (int)supportState,
                TehSupportDate = DateTime.Now,
                UpdateVersion = "11.4.5.86",
                UpdateVersionDate = DateTime.Now
            };
        }


        /// <summary>
        /// Сгенерировать информационную базу
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="dbcount">Количество баз у аккаунта</param>
        /// <param name="indexNumber">Индекс</param>
        /// <param name="templateId">Номер шаблона</param>
        /// <param name="fileStorageId">Номер файлового хранилища</param>
        /// <param name="publishState">Статус публикации</param>
        /// <param name="state">Статус базы</param>
        /// <param name="createAccountDatabaseComment">Комментарий создания базы</param>
        /// <returns></returns>
        public Domain.DataModels.AccountDatabase GenerateAccountDatabase(Guid accountId, int dbcount, int indexNumber,
            Guid? templateId = null, Guid? fileStorageId = null, PublishState? publishState = null,
            DatabaseState state = DatabaseState.Ready,
            string? createAccountDatabaseComment = null)
        {
            var accountDatabase = new Domain.DataModels.AccountDatabase
            {
                Id = Guid.NewGuid(),
                Caption = $"NewTestDb_{Guid.NewGuid()}",
                CreationDate = DateTime.Now,
                LastActivityDate = DateTime.Now,
                DbNumber = dbcount,
                SizeInMB =
                    DateTime.Now.Millisecond * 100 + DateTime.Now.Millisecond * 5 + DateTime.Now.Millisecond * 10,
                StateEnum = state,
                AccountId = accountId,
                V82Name = $"{DatabaseConst.FileDatabasePrefix}{indexNumber}_{dbcount}",
                SqlName = $"db_{DatabaseConst.FileDatabasePrefix}{indexNumber}_{dbcount}",
                ServiceName = Clouds42Service.MyEnterprise.ToString(),
                IsFile = true,
                PublishStateEnum = publishState ?? PublishState.Unpublished,
                TemplateId = templateId,
                FileStorageID = fileStorageId,
                ApplicationName = "8.3",
                UsedWebServices = true,
                CreateAccountDatabaseComment = createAccountDatabaseComment
            };
            return accountDatabase;
        }

        /// <summary>
        /// Сгенерировать список информационных баз по количеству
        /// </summary>
        /// <param name="count">Количество записей</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="dbcount">Количесто баз у аккаунта</param>
        /// <param name="indexNumber">Индекс</param>
        /// <param name="templateId">Номер шаблона</param>
        /// <param name="fileStorageId">Номер файлового хранилища</param>
        /// <returns></returns>
        public List<Domain.DataModels.AccountDatabase> GenerateListAccountDatabasesByCount(int count, Guid accountId, int dbcount,
            int indexNumber, Guid? templateId = null, Guid? fileStorageId = null)
        {
            var accountDatabases = new List<Domain.DataModels.AccountDatabase>();

            for (int i = 0; i < count; i++)
            {
                accountDatabases.Add(
                    GenerateAccountDatabase(accountId, dbcount, indexNumber, templateId, fileStorageId));
                dbcount++;
            }

            return accountDatabases;
        }

        /// <summary>
        /// Сгенерировать права пользователя к базе
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="accountDbId">Id информационной базы</param>
        /// <returns></returns>
        public AcDbAccess GenerateAcDbAccess(Guid accountId, Guid accountUserId, Guid accountDbId)
        {
            var acDbAccess = new AcDbAccess
            {
                ID = Guid.NewGuid(),
                AccountDatabaseID = accountDbId,
                LocalUserID = Guid.NewGuid(),
                AccountID = accountId,
                AccountUserID = accountUserId,
                IsLock = false
            };

            return acDbAccess;
        }

        /// <summary>
        /// Сгенерировать список прав пользователю к базам
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="accountDatabases">Список баз</param>
        /// <returns></returns>
        public List<AcDbAccess> GenerateListAcDbAccess(AccountUser accountUser, List<Domain.DataModels.AccountDatabase> accountDatabases)
        {
            var acDbAccessList = new List<AcDbAccess>();

            foreach (var accountDatabase in accountDatabases)
                acDbAccessList.Add(GenerateAcDbAccess(accountUser.AccountId, accountUser.Id, accountDatabase.Id));

            return acDbAccessList;
        }

        /// <summary>
        /// Сгенерировать аккаунт
        /// </summary>
        /// <param name="refferalAccountId">Id реферального аккаунта </param>
        /// <param name="name">Название аккаунта</param>
        /// <returns>Сущность аккаунта</returns>
        public Account GenerateAccount(Guid? refferalAccountId = null, string? name = null) =>
            new()
            {
                Id = Guid.NewGuid(),
                RegistrationDate = DateTime.Now,
                AccountCaption = name ?? $"ForTest{_random}{DateTime.Now.Millisecond}",
                Description = $"TestAccount{_random}",
                Status = "Added",
                ReferralAccountID = refferalAccountId
            };

        /// <summary>
        /// Сгенерировать реквизиты аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Реквизиты аккаунта</returns>
        public AccountRequisites GenerateAccountRequisites(Guid accountId) => new()
        {
            AccountId = accountId,
            Inn = "914567890"
        };

        /// <summary>
        /// Сгенерировать свойства сервиса "Мой диск" по аккаунту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Свойства сервиса "Мой диск" по аккаунту</returns>
        public MyDiskPropertiesByAccount GenerateMyDiskPropertiesByAccount(Guid accountId) =>
            new()
            {
                AccountId = accountId
            };

        /// <summary>
        /// Сформировать конфигурацию аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="segmentId">Id сегмента</param>
        /// <param name="accountLocaleId">Id локали</param>
        /// <returns>Конфигурация аккаунта</returns>
        public AccountConfiguration GenerateAccountConfiguration(Guid accountId, Guid? segmentId = null,
            Guid? accountLocaleId = null)
        {
            var cloudServicesSegment = segmentId != null
                ? _unitOfWork.CloudServicesSegmentRepository.FirstOrDefault(segment => segment.ID == segmentId)
                : _unitOfWork.CloudServicesSegmentRepository.FirstOrDefault();

            var localeId = accountLocaleId ?? Guid.Parse(_configuration["DefaultLocale"] ?? "");
            var supplierId = _unitOfWork.SupplierRepository.FirstOrDefault(w => w.LocaleId == localeId).Id;

            return new AccountConfiguration
            {
                AccountId = accountId,
                FileStorageId = cloudServicesSegment.FileStorageServersID,
                LocaleId = localeId,
                SegmentId = cloudServicesSegment.ID,
                SupplierId = supplierId,
                Type = "42Clouds"
            };
        }

        /// <summary>
        /// Сгенерировать список аккаунтов по количеству
        /// </summary>
        /// <param name="count">Количество аккаунтов</param>
        /// <returns></returns>
        public List<Account> GenerateListAccountsByCount(int count)
        {
            var accounts = new List<Account>();

            for (var i = 0; i < count; i++)
            {
                accounts.Add(GenerateAccount());
            }

            return accounts;
        }

        /// <summary>
        /// Сгенерировать сейл менеджера для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="saleManagerId">Id сейл менеджера</param>
        /// <param name="division">Отдел</param>
        /// <returns>Сейл менеджер</returns>
        public AccountSaleManager GenerateAccountSaleManager(Guid accountId, Guid saleManagerId, string division)
        {
            return new AccountSaleManager
            {
                AccountId = accountId,
                SaleManagerId = saleManagerId,
                CreationDate = DateTime.Now,
                Division = division
            };
        }

        /// <summary>
        /// Сгенерировать конфигурацию ресурса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Сервис</param>
        /// <returns></returns>
        public ResourcesConfiguration GenerateResourcesConfiguration(Guid accountId, IBillingService service)
        {
            return new ResourcesConfiguration
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Cost = 1500,
                CreateDate = DateTime.Now,
                Frozen = false,
                BillingServiceId = service.Id
            };
        }

        /// <summary>
        /// Сгенерировать ресурс
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="subjectId">Id субъекта</param>
        /// <param name="accountSponsorId">Id спонсора</param>
        /// <param name="resourceType">Тип ресурса</param>
        /// <param name="cost">Стоимость ресурса</param>
        /// <returns>Сгенерированный ресурс</returns>
        public Resource GenerateResource(Guid accountId, ResourceType resourceType, Guid? subjectId = null,
            Guid? accountSponsorId = null, decimal cost = 0)
        {
            var serviceType =
                new BillingServiceDataProvider(_unitOfWork).GetSystemServiceTypeOrThrowException(resourceType);

            var resource = new Resource
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Cost = cost,
                Subject = subjectId,
                BillingServiceTypeId = serviceType.Id,
                IsFree = false,
                Tag = null,
                AccountSponsorId = accountSponsorId
            };
            return resource;
        }

        /// <summary>
        /// Сгенерировать билинг аккаунт
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns></returns>
        public BillingAccount GenerateBillingAccount(Guid accountId)
        {
            return new BillingAccount
            {
                Id = accountId,
                MonthlyPaymentDate = DateTime.Now,
                Balance = 594025,
                AccountType = "Standart"
            };
        }

        /// <summary>
        /// Сгенерировать шаблон информационной базы
        /// </summary>
        /// <returns>Шаблон базы</returns>
        public DbTemplate GenerateDbTemplate()
        {
            return new DbTemplate
            {
                Id = Guid.NewGuid(),
                Name = "db_1c_roznicha",
                DefaultCaption = "Розница Проф",
                Order = 47,
                CanWebPublish = true,
                Platform = "8,3",
                NeedUpdate = false
            };
        }

        /// <summary>
        /// Сгенерировать список шаблонов инф баз по количеству
        /// </summary>
        /// <param name="count">Количество инф баз</param>
        /// <returns>Список шаблонов инф баз</returns>
        public List<DbTemplate> GenerateDbTemplatesByCount(int count)
        {
            var dbTemplates = new List<DbTemplate>();

            for (int i = 0; i < count; i++)
            {
                dbTemplates.Add(GenerateDbTemplate());
            }

            return dbTemplates;
        }

        /// <summary>
        /// Сгенерировать шаблон базы на разделителях
        /// </summary>
        /// <param name="templateId">Id шаблона</param>
        /// <param name="configurationId">Id конфигурации</param>
        /// <returns>Шаблон базы на разделителях</returns>
        public DbTemplateDelimiters GenerateDbTemplateDelimiters(Guid templateId, string configurationId = "bp")
        {
            return new DbTemplateDelimiters
            {
                ConfigurationId = configurationId,
                TemplateId = templateId,
                Name = "Бухгалтерия предприятия 3.0",
                ConfigurationReleaseVersion = "3.0.67.74",
                MinReleaseVersion = "3.0.64.42"
            };
        }

        /// <summary>
        /// Сгенерировать базу на разделителях
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="databaseSourceId">Id базы источника на разделителях</param>
        /// <param name="dbTemplateDelimiterCode">Код шаблона базы на разделителях</param>
        /// <param name="isDemo">Демо база</param>
        /// <returns>База на разделителях</returns>
        public AccountDatabaseOnDelimiters GenerateAccountDatabaseOnDelimiters(Guid accountDatabaseId,
            Guid? databaseSourceId, string dbTemplateDelimiterCode, bool isDemo = false)
        {
            return new AccountDatabaseOnDelimiters
            {
                AccountDatabaseId = accountDatabaseId,
                DatabaseSourceId = databaseSourceId,
                Zone = 2350,
                LoadTypeEnum = DelimiterLoadType.UserCreateNew,
                IsDemo = isDemo,
                DbTemplateDelimiterCode = dbTemplateDelimiterCode
            };
        }

        /// <summary>
        /// Сгенерировать список баз на разделителях
        /// </summary>
        /// <param name="accountDatabases">Список информ. баз</param>
        /// <param name="databaseSourceId">Id базы источника на разделителях</param>
        /// <param name="dbTemplateDelimiterCode">Код шаблона базы на разделителях</param>
        /// <param name="isDemo">Демо база</param>
        /// <returns></returns>
        public List<AccountDatabaseOnDelimiters> GenerateListAccountDatabaseOnDelimiters(
            List<Domain.DataModels.AccountDatabase> accountDatabases, Guid databaseSourceId, string dbTemplateDelimiterCode,
            bool isDemo = false)
        {
            var accountDatabaseOnDelimitersList = new List<AccountDatabaseOnDelimiters>();
            foreach (var accountDatabase in accountDatabases)
                accountDatabaseOnDelimitersList.Add(GenerateAccountDatabaseOnDelimiters(accountDatabase.Id,
                    databaseSourceId, dbTemplateDelimiterCode, isDemo));

            return accountDatabaseOnDelimitersList;
        }

        /// <summary>
        /// Сгенерировать историю бекапа базы на разделителях
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="createDateTime">Дата создания бекапа</param>
        /// <returns></returns>
        public AccountDatabaseBackupHistory GenerateAccountDatabaseBackupHistory(Guid accountDatabaseId,
            DateTime? createDateTime = null)
        {
            return new AccountDatabaseBackupHistory
            {
                Id = Guid.NewGuid(),
                AccountDatabaseId = accountDatabaseId,
                State = AccountDatabaseBackupHistoryState.Skip,
                CreateDateTime = createDateTime ?? DateTime.Now.AddDays(-1),
                Message = "Test"
            };
        }

        /// <summary>
        /// Сгенерировать объект бэкпапа для записи в базе
        /// </summary>
        /// <param name="accountDatabaseId">Гуид базы</param>
        /// <param name="initiatorId">Пользователь-инициатор</param>
        /// <param name="createDateTime">Дата создания бэкапа</param>
        /// <returns>объект для записи в таблицу бекапов</returns>
        public AccountDatabaseBackup GenerateAccountDatabaseBackup(Guid accountDatabaseId, Guid initiatorId,
            DateTime? createDateTime = null)
        {
            return new AccountDatabaseBackup
            {
                Id = Guid.NewGuid(),
                AccountDatabaseId = accountDatabaseId,
                CreateDateTime = createDateTime ?? DateTime.Now,
                BackupPath = "https://drive.google.com/file/d/666",
                EventTrigger = CreateBackupAccountDatabaseTrigger.RegulationBackup,
                InitiatorId = initiatorId,
                SourceType = AccountDatabaseBackupSourceType.GoogleCloud,
                CreationBackupDateTime = createDateTime ?? DateTime.Now
            };
        }

        /// <summary>
        /// Сгенерировать объекты бэкпапа для записи в базе по количеству
        /// </summary>
        /// <param name="count">Количество записей</param>
        /// <param name="accountDatabaseId">Гуид базы</param>
        /// <param name="initiatorId">Пользователь-инициатор</param>
        /// <returns>Список объектов для записи в таблицу бекапов</returns>
        public List<AccountDatabaseBackup> GenerateAccountDatabaseBackupByCount(int count, Guid accountDatabaseId,
            Guid initiatorId)
        {
            var backups = new List<AccountDatabaseBackup>();

            for (var i = 0; i < count; i++)
            {
                var date = DateTime.Now;

                backups.Add(GenerateAccountDatabaseBackup(accountDatabaseId, initiatorId, date));
            }

            return backups;
        }

        /// <summary>
        /// Сгенерировать платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="paymentType">Тип платежа</param>
        /// <param name="sum">Сумма платежа</param>
        /// <param name="billingServiceId">Id сервиса</param>
        /// <param name="date">дата создания платежа</param>
        /// <param name="transactionType">Тип транзакции</param>
        /// <returns>Платеж</returns>
        public Domain.DataModels.billing.Payment GeneratePayment(Guid accountId, PaymentType paymentType = PaymentType.Inflow,
            decimal sum = 5000, Guid? billingServiceId = null, DateTime? date = null,
            TransactionType transactionType = TransactionType.Money)
        {
            return new Domain.DataModels.billing.Payment
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Date = date ?? DateTime.Now,
                Description = "Test Payment",
                OriginDetails = "Admin import",
                Status = PaymentStatus.Done.ToString(),
                OperationType = paymentType.ToString(),
                Sum = sum,
                PaymentSystem = "Admin",
                BillingServiceId = billingServiceId,
                TransactionType = transactionType
            };
        }

        /// <summary>
        /// Сгенерировать роль для пользователя
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="accountUserGroup">Роль</param>
        /// <returns></returns>
        public AccountUserRole GenerateAccountUserRole(Guid accountUserId, AccountUserGroup accountUserGroup)
        {
            return new AccountUserRole
            {
                Id = Guid.NewGuid(),
                AccountUserId = accountUserId,
                AccountUserGroup = accountUserGroup
            };
        }

        /// <summary>
        /// Сгенерировать сессию для пользователя
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns></returns>
        public AccountUserSession GenerateAccountUserSession(Guid accountUserId)
        {
            return new AccountUserSession
            {
                Id = Guid.NewGuid(),
                AccountUserId = accountUserId,
                ClientDescription = "Test",
                StaticToken = false,
                Token = Guid.NewGuid(),
                TokenCreationTime = DateTime.Now
            };
        }
        

        /// <summary>
        /// Сгенерировать Vrd файл
        /// </summary>
        /// <param name="ibPath">Физический путь информационной базы 1С</param>
        /// <param name="baseAddress">Web-адрес приложения (базы)</param>
        /// <returns>Сгенерированный vrd файл</returns>
        public string GenerateDefaultVrdFile(string ibPath, string baseAddress, string? thinkClientLink)
        {
            var text = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                <point xmlns=""http://v8.1c.ru/8.2/virtual-resource-system""
		                                xmlns:xs=""http://www.w3.org/2001/XMLSchema""
		                                xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
		                                base=""{baseAddress}""
		                                ib=""{ibPath}""
                                        pubdst=""{thinkClientLink}"">
	                                <ws enable=""false""/>
                                </point>";

            return text;
        }

        /// <summary>
        /// Сгенерировать файловое хранилище
        /// </summary>
        /// <param name="number">Номер хранилища</param>
        /// <returns>Сгенерированное файловое хранилище</returns>
        public CloudServicesFileStorageServer GenerateCloudServicesFileStorageServer(int number)
        {
            var systemDrivePath = Path.GetPathRoot(Environment.SystemDirectory)?? "";
            var cloudServicesFileStorageServer = new CloudServicesFileStorageServer
            {
                ID = Guid.NewGuid(),
                PhysicalPath = Path.Combine(systemDrivePath, "Temp", "PhysicalPath", number.ToString()),
                Name = $"Хранилище номер {number}",
                Description = "Файловое хранилище",
                ConnectionAddress = GenerateDirectory(number),
                DnsName = "TestServer"
            };

            return cloudServicesFileStorageServer;
        }

        /// <summary>
        /// Сгенерировать директорию
        /// </summary>
        /// <param name="number">Номер директории</param>
        /// <returns>Сгенерированная дериктория</returns>
        private string GenerateDirectory(int number)
        {
            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            return Path.Combine(path[6..], "TestData", "DefaultStorage", number.ToString());
        }

        /// <summary>
        /// Сгенерировать счет
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="inn">ИНН</param>
        /// <param name="sum">Сумма счета</param>
        /// <param name="invoiceStatus">Статус счета</param>
        /// <param name="period">Период счета</param>
        /// <param name="date">Дата создания счета</param>
        /// <param name="paymentId">Id платежа</param>
        /// <returns>Сгенерированный счет</returns>
        public Domain.DataModels.billing.Invoice GenerateInvoice(Guid accountId, string inn, decimal sum,
            InvoiceStatus invoiceStatus = InvoiceStatus.Processed, int? period = null, DateTime? date = null,
            Guid? paymentId = null)
        {
            var invoice = new Domain.DataModels.billing.Invoice
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Date = date ?? DateTime.Now,
                Requisite = inn,
                Sum = sum,
                State = invoiceStatus.ToString(),
                Period = period,
                PaymentID = paymentId
            };

            return invoice;
        }

        /// <summary>
        /// Сгенерировать продукт
        /// </summary>
        /// <param name="invoiceId">Гуид счёта</param>
        /// <param name="serviceName">Сервис</param>
        /// <param name="count">Количество определённого продукта</param>
        /// <param name="cost">Стоимость еденицы продукта </param>
        /// <returns>Сгенерированный продукт</returns>
        public InvoiceProduct GenerateInvoiceProduct(Guid invoiceId, string serviceName, int count, decimal cost)
        {
            var product = new InvoiceProduct
            {
                Id = Guid.NewGuid(),
                Count = count,
                Description = serviceName,
                InvoiceId = invoiceId,
                ServiceName = serviceName,
                ServiceSum = cost
            };

            return product;
        }

        /// <summary>
        /// Сгенерировать платеж
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="paymentSystem">Платежная система</param>
        /// <param name="sum">Сумма платежа</param>
        /// <returns>Новый платеж</returns>
        public Domain.DataModels.billing.Payment GeneratePayment(Guid accountId, PaymentSystem paymentSystem, decimal sum)
        {
            return new Domain.DataModels.billing.Payment
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Date = DateTime.Now,
                PaymentSystem = paymentSystem.ToString(),
                Description = "",
                Status = PaymentStatus.Done.ToString(),
                OperationType = PaymentType.Inflow.ToString(),
                Sum = sum,
                TransactionType = TransactionType.Money,
                OriginDetails = "Железный банк Браавоса"
            };
        }

        /// <summary>
        /// Сгенерировать много платежей
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="paymentSystem">Платежная система</param>
        /// <param name="sum">Сумма платежа</param>
        /// <param name="paymentsCount">Количество платежей</param>
        /// <returns>Список новых платежей</returns>
        public List<Domain.DataModels.billing.Payment> GenerateManyPayments(Guid accountId, PaymentSystem paymentSystem, decimal sum,
            int? paymentsCount = 1)
        {
            var payments = new List<Domain.DataModels.billing.Payment>();

            for (int i = 0; i < paymentsCount; i++)
            {
                var payment = GeneratePayment(accountId, paymentSystem, sum);
                payments.Add(payment);
            }

            return payments;
        }

        /// <summary>
        /// Сгенерировать заявку сервиса на модерацию 
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        /// <param name="moderatorComment">Комментарий модератора</param>
        /// <param name="changeRequestType">Тип заявки</param>
        /// <param name="status">Статус заявки</param>
        /// <returns>Заявка сервиса на модерацию</returns>
        public ChangeServiceRequest GenerateChangeServiceRequest(Guid billingServiceId, string moderatorComment,
            ChangeRequestTypeEnum changeRequestType = ChangeRequestTypeEnum.Creation,
            ChangeRequestStatusEnum status = ChangeRequestStatusEnum.OnModeration)
        {
            return new ChangeServiceRequest
            {
                Id = Guid.NewGuid(),
                BillingServiceId = billingServiceId,
                ChangeRequestType = changeRequestType,
                Status = status,
                CreationDateTime = DateTime.Now,
                StatusDateTime = DateTime.Now,
                ModeratorComment = moderatorComment
            };
        }

        /// <summary>
        /// Сгенерировать новую версию файла разработки 1С
        /// </summary>
        /// <param name="auditService1CFileResult">Результат аудита файла сервиса 1С</param>
        /// <param name="fileVersion">Версия файла в формате "1.0.0.0"</param>
        /// <param name="isActualVersion">Признак актуальность версии</param>
        /// <returns>Версия файла разработки 1С</returns>
        public Service1CFileVersion GenerateService1CFileVersion(AuditService1CFileResultEnum auditService1CFileResult,
            string fileVersion, bool isActualVersion)
        {
            return new Service1CFileVersion
            {
                Id = Guid.NewGuid(),
                IsActualVersion = isActualVersion,
                AuditResult = auditService1CFileResult,
                CommentForModerator = "New comment for moderator",
                DescriptionOf1CFileChanges = "New description of 1c file changes",
                ModeratorComment = "New moderator comment",
                NeedNotifyAboutVersionChanges = false,
                Version1CFile = fileVersion
            };
        }

        /// <summary>
        /// Сгенерировать модель создания инф. баз из ZIP
        /// </summary>
        /// <param name="dbCaption">Название инф. базы</param>
        /// <param name="uploadedFileId">Id загруженного файла</param>
        /// <param name="templateId">Id шаблона</param>
        /// <param name="userId">Id пользователя</param>
        /// <returns>Модель создания инф. базы из ZIP</returns>
        public CreateAcDbFromZipDto GenerateCreateAcDbFromZipDto(string dbCaption, Guid uploadedFileId, Guid templateId,
            Guid userId)
        {
            return new CreateAcDbFromZipDto
            {
                DbCaption = dbCaption,
                UploadedFileId = uploadedFileId,
                NeedUsePromisePayment = false,
                MyDatabasesServiceTypeId = null,
                TemplateId = templateId,
                UserFromZipPackageList =
                [
                    new() { AccountUserId = userId, AccountDatabaseZipUserId = Guid.NewGuid(), }
                ]
            };
        }

        /// <summary>
        /// Сгенерировать модель создания инф. базы из шаблона
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="templateId">Id шаблона</param>
        /// <param name="uploadedFileId">Id загруженного файла</param>
        /// <param name="userId">Id пользователя</param>
        /// <param name="dbName">Название инф. базы</param>
        /// <param name="dbTemplateDelimiters">Признак шаблон на разделителях</param>
        /// <returns>Модель создания инф. базы из шаблона</returns>
        public CreateAccountDatabasesFromTemplateDto GenerateCreateAccountDatabasesFromTemplateDto(Guid accountId,
            Guid templateId, Guid uploadedFileId, Guid userId, string dbName, bool dbTemplateDelimiters)
        {
            return new CreateAccountDatabasesFromTemplateDto
            {
                NeedUsePromisePayment = false,
                CreateDatabasesModelDc =
                [
                    new()
                    {
                        AccountId = accountId,
                        IsFile = true,
                        TemplateId = templateId,
                        UploadedFileId = uploadedFileId,
                        AccountDatabaseBackupId = null,
                        UsersToGrantAccess = [userId],
                        DataBaseName = dbName,
                        DbTemplateDelimiters = dbTemplateDelimiters,
                        DemoData = false,
                        IsChecked = true,
                        ListInfoAboutUserFromZipPackage =
                            [new() { AccountUserId = userId, AccountDatabaseZipUserId = Guid.NewGuid(), }],
                        Publish = false,
                        SizeZipFile = 10
                    }
                ]
            };
        }

        /// <summary>
        /// Сгенерировать модель создания инф. базы из бэкапа
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="templateId">Id шаблона</param>
        /// <param name="uploadedFileId">Id загруженного файла</param>
        /// <param name="userId">Id пользователя</param>
        /// <param name="dbName">Название инф. базы</param>
        /// <param name="dbTemplateDelimiters">Признак шаблон на разделителях</param>
        /// <param name="backupId">Id бэкапа инф. базы</param>
        /// <returns>Модель создания инф. базы из бэкапа</returns>
        public CreateAcDbOnDelimitersFromBackupDto GenerateCreateAcDbOnDelimitersFromBackupDto(Guid accountId,
            Guid templateId, Guid uploadedFileId, Guid userId, string dbName, bool dbTemplateDelimiters, Guid backupId)
        {
            return new CreateAcDbOnDelimitersFromBackupDto
            {
                NeedUsePromisePayment = false,
                MyDatabasesServiceTypeId = null,
                CreateAcDbModel = new InfoDatabaseDomainModelDto
                {
                    AccountId = accountId,
                    IsFile = true,
                    DataBaseName = dbName,
                    UploadedFileId = uploadedFileId,
                    UsersToGrantAccess = [userId],
                    DbTemplateDelimiters = dbTemplateDelimiters,
                    ListInfoAboutUserFromZipPackage =
                    [
                        new() { AccountUserId = userId, AccountDatabaseZipUserId = Guid.NewGuid(), }
                    ],
                    TemplateId = templateId,
                    SizeZipFile = 10,
                    Publish = false,
                    AccountDatabaseBackupId = backupId,
                    IsChecked = true,
                    DemoData = false
                }
            };
        }

        /// <summary>
        /// Сгенерировать модель восстановления инф. базы из бэкапа
        /// </summary>
        /// <param name="uploadFileId">Id загруженного файла</param>
        /// <param name="backupId">Id бэкапа</param>
        /// <param name="userId">Id пользователя для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">Id сервиса "Мои информационные базы"</param>
        /// <returns>Модель восстановления инф. базы из бэкапа</returns>
        public RestoreAcDbFromBackupDto GenerateRestoreAcDbFromBackupDto(Guid uploadFileId, Guid backupId, Guid userId,
            Guid myDatabasesServiceTypeId)
        {
            return new RestoreAcDbFromBackupDto
            {
                UploadedFileId = uploadFileId,
                NeedUsePromisePayment = false,
                AccountDatabaseBackupId = backupId,
                UsersIdForAddAccess = [userId],
                MyDatabasesServiceTypeId = myDatabasesServiceTypeId,
                AccountDatabaseName = "Test Account Database Name",
                RestoreType = AccountDatabaseBackupRestoreType.ToCurrent
            };
        }

        /// <summary>
        /// Сгенерировать модель параметров воркера для таски RestoreAccountDatabaseFromTombJob
        /// <param name="uploadFileId">Id загруженного файла</param>
        /// <param name="backupId">Id бэкапа</param>
        /// <param name="userId">Id пользователя для предоставления доступа</param>
        /// <returns>модель параметров воркера для таски RestoreAccountDatabaseFromTombJob</returns>
        public RestoreAccountDatabaseFromTombWorkerTaskParam GenerateRestoreAccountDatabaseFromTombWorkerTaskParam(
            Guid uploadFileId, Guid backupId, Guid userId)
        {
            return new RestoreAccountDatabaseFromTombWorkerTaskParam
            {
                UploadedFileId = uploadFileId,
                AccountDatabaseBackupId = backupId,
                UsersIdForAddAccess = [userId],
                AccountDatabaseName = "Test account database name",
                RestoreType = AccountDatabaseBackupRestoreType.ToCurrent,
                NeedChangeState = true,
                SendMailAboutError = false
            };
        }

        /// <summary>
        /// Сгенерировать модель обновления шаблона инф. базы
        /// </summary>
        /// <param name="templateId">Id шаблона</param>
        /// <param name="updateVersion">Версия обновления</param>
        /// <param name="updateTemplateDirectory">Папка для хранения обновлений</param>
        /// <returns>Модель обновления шаблона инф. базы</returns>
        public DbTemplateUpdate GenerateDbTemplateUpdateModel(Guid templateId, string updateVersion,
            string updateTemplateDirectory = "TestTemplateUpdatesDirectory")
        {
            return new DbTemplateUpdate
            {
                Id = Guid.NewGuid(),
                TemplateId = templateId,
                UpdateTemplatePath =
                    $"{CloudConfigurationProvider.Template.GetTemplatesDirectory()}/{updateTemplateDirectory}",
                UpdateVersion = updateVersion,
                UpdateVersionDate = DateTime.Now,
                ValidateState = DbTemplateUpdateState.New
            };
        }
    }
}
