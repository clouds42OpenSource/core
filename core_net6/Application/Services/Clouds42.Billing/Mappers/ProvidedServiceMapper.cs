﻿using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling.ProvidedServices;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.Mappers
{
    /// <summary>
    /// Маппер для моделей оказанных услуг
    /// </summary>
    public static class ProvidedServiceMapper
    {

        /// <summary>
        /// Смапить доменную модель оказанной услуги к модели представления
        /// </summary>
        /// <param name="providedService">Доменная модель оказанной услуги</param>
        /// <param name="isHistoricalRow">Признак что запись для истории</param>
        /// <param name="sponsoredAccountName">Имя аккаунта спонсора</param>
        /// <returns>Модель представления оказанной услуги</returns>
        public static ProvidedServiceDto MapToProvidedServiceDm(this ProvidedService providedService,
            bool isHistoricalRow, string sponsoredAccountName, ICloudLocalizer cloudLocalizer)
            => new()
            {
                ServiceDescription = GetProvidedServiceDescription(providedService.AccountId,
                    providedService.ServiceEnum, providedService.ServiceTypeEnum, cloudLocalizer),
                Service = providedService.ServiceEnum,
                IsHistoricalRow = isHistoricalRow,
                Count = providedService.Count,
                ResourceType = providedService.ServiceTypeEnum,
                Rate = providedService.Rate,
                From = providedService.DateFrom,
                To = providedService.DateTo,
                SposoredAccountName = sponsoredAccountName
            };

        /// <summary>
        /// Получить описание оказанной услуги
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Тип сервиса</param>
        /// <param name="serviceType">Тип услуги сервиса</param>
        /// <returns>Описание оказанной услуги</returns>
        private static string GetProvidedServiceDescription(Guid accountId, Clouds42Service service,
            ResourceType serviceType, ICloudLocalizer cloudLocalizer)
        {
            switch (service)
            {
                case Clouds42Service.MyDisk:
                    return "Покупка дискового пространства";
                case Clouds42Service.Esdl:
                    return "Покупка лицензии для сервиса 'Recognition'";
                case Clouds42Service.Recognition:
                    return $"Страницы {serviceType.GetDataValue()} штук  для сервиса 'Recognition'";
                case Clouds42Service.MyEnterprise:
                {
                    var serviceTypeStr = serviceType == ResourceType.MyEntUser ? "Стандарт" : "WEB";
                    return $"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)} '{serviceTypeStr}'";
                }
                case Clouds42Service.Unknown:
                case Clouds42Service.MyDatabases:
                default:
                    return "";
            }
        }
    }
}
