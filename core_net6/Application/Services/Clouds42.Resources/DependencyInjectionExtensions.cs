﻿using Clouds42.Resources.Contracts.Interfaces;
using Clouds42.Resources.Managers;
using Clouds42.Resources.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Resources
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddMyDatabasesResource(this IServiceCollection services)
        {
            services.AddTransient<ICreateMyDatabasesResourceProvider, CreateMyDatabasesResourceProvider>();
            services.AddTransient<IMyDatabasesResourceDataProvider, MyDatabasesResourceDataProvider>();
            services.AddTransient<IUpdateMyDatabasesResourceProvider, UpdateMyDatabasesResourceProvider>();
            services.AddTransient<IResourceReportDataProvider, ResourceReportDataProvider>();
            services.AddTransient<IResourceDataProvider, ResourceDataProvider>();
            services.AddTransient<ICreateResourceProvider, CreateResourceProvider>();
            services.AddTransient<IRecalculateResourcesConfigurationCostProvider, RecalculateResourcesConfigurationCostProvider>();
            services.AddTransient<IResourcesConfigurationDataProvider, ResourcesConfigurationDataProvider>();
            services.AddTransient<IResourcesConfigurationManager, ResourcesConfigurationManager>();

            return services;
        }
    }
}
