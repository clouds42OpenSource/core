﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.DataContracts.ResourceConfiguration;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.Resources.Providers
{
    /// <summary>
    /// Провайдер для работы с данными ресурса сервиса "Мои информационные базы"
    /// </summary>
    internal class MyDatabasesResourceDataProvider(IUnitOfWork dbLayer) : IMyDatabasesResourceDataProvider
    {
        /// <summary>
        /// Получить ресурс сервиса "Мои информационные базы" для аккаунта
        /// или выкинуть исключение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса
        /// "Мои информационные базы"(с типом биллинга по аккаунту)</param>
        /// <returns>Ресурс сервиса "Мои информационные базы"</returns>
        public MyDatabasesResource GetMyDatabasesResourceOrThrowException(Guid accountId,
            ResourceType systemServiceType) =>
            (from myDatabasesResourceData in GetDataForSampleMyDatabasesResources(accountId)
                where myDatabasesResourceData.ServiceType.SystemServiceType == systemServiceType
                select myDatabasesResourceData.MyDatabasesResource).FirstOrDefault() ?? throw new NotFoundException(
                $"Не удалось найти ресурс аккаунта '{accountId}' для системной услуги {systemServiceType}");

        /// <summary>
        /// Получить актуальные значения для ресурсов сервиса "Мои инф. базы"
        /// сгруппированные по аккаунту
        /// </summary>
        /// <returns>Актуальные значения для ресурсов сервиса "Мои инф. базы"
        /// сгруппированные по аккаунту</returns>
        public IQueryable<MyDatabasesResourcesActualValuesForAccountDto>
            GetActualValuesForMyDatabasesResourcesByAccounts() =>
            from database in GetActiveDatabasesForBilling()
            group database by database.AccountId
            into groupedDatabases
            select new MyDatabasesResourcesActualValuesForAccountDto
            {
                AccountId = groupedDatabases.Key,
                DatabasesCount = groupedDatabases.Count(),
                ServerDatabasesCount = groupedDatabases.Count(db =>
                    db.AccountDatabaseOnDelimiter == null && (db.IsFile == null || db.IsFile == false))
            };


        /// <summary>
        /// Получить активные базы для биллинга(для учета баз, в ресурсах)
        /// </summary>
        /// <returns>Активные базы для биллинга(для учета баз, в ресурсах)</returns>
        public IQueryable<Domain.DataModels.AccountDatabase> GetActiveDatabasesForBilling() =>
            dbLayer.DatabasesRepository.WhereLazy(database =>
                database.State == DatabaseState.Ready.ToString() ||
                database.State == DatabaseState.NewItem.ToString() ||
                database.State == DatabaseState.ProcessingSupport.ToString() ||
                database.State == DatabaseState.TransferDb.ToString() ||
                database.State == DatabaseState.RestoringFromTomb.ToString());

        /// <summary>
        /// Получить актуальные значения ресурсов сервиса "Мои инф. базы" для аккаунта
        /// </summary>
        /// <returns>Получить актуальные значения ресурсов сервиса "Мои инф. базы" для аккаунта</returns>
        public MyDatabasesResourcesActualValuesForAccountDto GetActualValuesForMyDatabasesResourcesForAccount(Guid accountId)
            => GetActualValuesForMyDatabasesResourcesByAccounts().FirstOrDefault(data => data.AccountId == accountId)
               ?? new MyDatabasesResourcesActualValuesForAccountDto
               {
                   AccountId = accountId
               };

        /// <summary>
        /// Получить данные ресурсов сервиса "Мои инф. базы" по аккаунту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные ресурсов сервиса "Мои инф. базы" по аккаунту</returns>
        public IQueryable<MyDatabasesResourceDataDto> GetMyDatabasesResourcesDataByAccount(Guid accountId) =>
            from myDatabasesResourceData in GetDataForSampleMyDatabasesResources(accountId)
            select new MyDatabasesResourceDataDto
            {
                ServiceTypeId = myDatabasesResourceData.ServiceType.Id,
                PaidDatabasesCount = myDatabasesResourceData.MyDatabasesResource.PaidDatabasesCount,
                ActualDatabasesCount = myDatabasesResourceData.MyDatabasesResource.ActualDatabasesCount,
                ResourceCost = myDatabasesResourceData.Resource.Cost,
                SystemServiceType = myDatabasesResourceData.ServiceType.SystemServiceType
            };

        /// <summary>
        /// Получить данные ресурсов "Мои инф. базы" для выборки
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные ресурсов "Мои инф. базы" для выборки</returns>
        private IQueryable<MyDatabasesResourceForSampleDataModelDto>
            GetDataForSampleMyDatabasesResources(Guid accountId) =>
            from resource in dbLayer.ResourceRepository.WhereLazy(r => r.AccountId == accountId)
            join myDatabasesResource in dbLayer.GetGenericRepository<MyDatabasesResource>()
                .WhereLazy() on resource.Id equals myDatabasesResource.ResourceId
            join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource.BillingServiceTypeId
                equals serviceType.Id
            select new MyDatabasesResourceForSampleDataModelDto
            {
                Resource = resource,
                MyDatabasesResource = myDatabasesResource,
                ServiceType = serviceType
            };
    }
}
