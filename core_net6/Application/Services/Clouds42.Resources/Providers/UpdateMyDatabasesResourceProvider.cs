﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.Resources.Providers
{
    /// <summary>
    /// Провайдер для обновления ресурса сервиса "Мои инф. базы"
    /// </summary>
    internal class UpdateMyDatabasesResourceProvider(
        IMyDatabasesResourceDataProvider myDatabasesResourceDataProvider,
        IUnitOfWork dbLayer)
        : IUpdateMyDatabasesResourceProvider
    {
        /// <summary>
        /// Увеличить количество оплаченных информационных баз
        /// для ресурса сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="paidDatabasesCount">Количество оплаченных информационных баз,
        /// на которое необходимо увеличить ресурс</param>
        public void IncreasePaidDatabasesCount(Guid accountId, ResourceType systemServiceType,
            int paidDatabasesCount) =>
            PerformChangeDatabasesCount(accountId, systemServiceType,
                myDatabasesResource => myDatabasesResource.PaidDatabasesCount += paidDatabasesCount);

        /// <summary>
        /// Увеличить актуальное количество информационных баз
        /// для ресурса сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="actualDatabasesCount">Актуальное количество инф. баз,
        /// на которое необходимо увеличить ресурс</param>
        public void IncreaseActualDatabasesCount(Guid accountId, ResourceType systemServiceType,
            int actualDatabasesCount) =>
            PerformChangeDatabasesCount(accountId, systemServiceType,
                myDatabasesResource => myDatabasesResource.ActualDatabasesCount += actualDatabasesCount);

        /// <summary>
        /// Обновить актуальное количество баз для ресурса сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="actualDatabasesCount">Актуальное количество инф. баз,
        /// которое необходимо обновить в ресурсе</param>
        public void UpdateActualDatabasesCount(Guid accountId, ResourceType systemServiceType,
            int actualDatabasesCount) =>
            PerformChangeDatabasesCount(accountId, systemServiceType,
                myDatabasesResource => myDatabasesResource.ActualDatabasesCount = actualDatabasesCount);

        /// <summary>
        /// Установить единое значение на количество информационных баз в ресурсе
        /// (Количество актуальных и оплаченных)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="databasesCount">Количество информационных баз,
        /// которое необходимо записать в ресурс</param>
        public void SetSingleValueForDatabasesCount(Guid accountId, ResourceType systemServiceType,
            int databasesCount) =>
            PerformChangeDatabasesCount(accountId, systemServiceType, myDatabasesResource =>
            {
                myDatabasesResource.PaidDatabasesCount = databasesCount;
                myDatabasesResource.ActualDatabasesCount = databasesCount;
            });

        /// <summary>
        /// Выполнить действие по изменению количества инф. баз
        /// для ресурса сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        /// <param name="changeDatabasesCountAction">Действие для изменения количества инф. баз</param>
        private void PerformChangeDatabasesCount(Guid accountId, ResourceType systemServiceType,
            Action<MyDatabasesResource> changeDatabasesCountAction)
        {
            var myDatabasesResource =
                myDatabasesResourceDataProvider.GetMyDatabasesResourceOrThrowException(accountId, systemServiceType);

            changeDatabasesCountAction(myDatabasesResource);
            UpdateMyDatabasesResource(myDatabasesResource);
        }

        /// <summary>
        /// Обновить ресурс сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="myDatabasesResource">Ресурс сервиса "Мои инф. базы"</param>
        private void UpdateMyDatabasesResource(MyDatabasesResource myDatabasesResource)
        {
            dbLayer.GetGenericRepository<MyDatabasesResource>().Update(myDatabasesResource);
            dbLayer.Save();
        }
    }
}