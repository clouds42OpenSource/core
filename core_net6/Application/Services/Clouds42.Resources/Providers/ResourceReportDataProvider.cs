﻿using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Resources.Providers
{
    /// <summary>
    /// Провайдер для работы с данными ресурсов для отчета
    /// </summary>
    internal class ResourceReportDataProvider(IUnitOfWork dbLayer) : IResourceReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных ресурсов для отчета
        /// </summary>
        /// <returns>Список моделей данных ресурсов для отчета</returns>
        public List<ResourceReportDataDto> GetResourceReportDataDcs()
        {
            var services = new List<Clouds42Service?> { Clouds42Service.Esdl, Clouds42Service.Recognition };

            return dbLayer.ResourceRepository
                            .AsQueryableNoTracking()
                            .Join(dbLayer.AccountsRepository.AsQueryable(),
                                res => res.AccountId,
                                account => account.Id,
                                (res, account) => new { res, account })
                            .Join(dbLayer.AccountConfigurationRepository.AsQueryable(),
                                combined => combined.account.Id,
                                accConf => accConf.AccountId,
                                (combined, accConf) => new { combined.res, combined.account, accConf })
                            .Join(dbLayer.BillingServiceTypeRepository.AsQueryable(),
                                combined => combined.res.BillingServiceTypeId,
                                serType => serType.Id,
                                (combined, serType) => new { combined.res, combined.account, combined.accConf, serType })
                            .Join(dbLayer.BillingServiceRepository.AsQueryable(),
                                combined => combined.serType.ServiceId,
                                ser => ser.Id,
                                (combined, ser) => new { combined.res, combined.account, combined.accConf, combined.serType, ser })
                            .Join(dbLayer.LocaleRepository.AsQueryable(),
                                combined => combined.accConf.LocaleId,
                                locale => locale.ID,
                                (combined, locale) => new { combined.res, combined.account, combined.accConf, combined.serType, combined.ser, locale })
                            .GroupJoin(dbLayer.AccountUsersRepository.AsQueryable(),
                                combined => combined.res.Subject,
                                accUsr => accUsr.Id,
                                (combined, accUsrGroup) => new { combined, accUsr = accUsrGroup})
                            .SelectMany(x => x.accUsr.DefaultIfEmpty(), (x, accUser) => new { x.combined, accUser })
                            .GroupJoin(dbLayer.ResourceConfigurationRepository.AsQueryable(),
                                combined => new { BillingServiceId = combined.combined.ser.Id, AccountId = combined.combined.account.Id },
                                resConfig => new { resConfig.BillingServiceId, resConfig.AccountId },
                                (combined, resConfigGroup) => new { combined, resConfig = resConfigGroup})
                            .SelectMany(x => x.resConfig.DefaultIfEmpty(), (x, resConf) => new { x.combined, resConf})
                            .GroupJoin(dbLayer.DiskResourceRepository.AsQueryable(),
                                combined => combined.combined.combined.res.Id,
                                diskRes => diskRes.ResourceId,
                                (combined, diskResGroup) => new { combined, diskRes = diskResGroup})
                            .SelectMany(x => x.diskRes.DefaultIfEmpty(), (x, diskRes) => new {x.combined, diskRes} )
                            .GroupJoin(dbLayer.AccountSaleManagerRepository
                                    .AsQueryable()
                                    .Include(x=> x.AccountUser),
                                combined => combined.combined.combined.combined.account.Id,
                                asm => asm.AccountId,
                                (combined, asmGroup) => new { combined, asm = asmGroup})
                            .SelectMany(x => x.asm.DefaultIfEmpty(), (x, asm) => new { x.combined, asm })
                            .Where(data =>
                                (data.combined.combined.resConf == null || !data.combined.combined.resConf.ExpireDate.HasValue || data.combined.combined.resConf.ExpireDate > DateTime.Now.AddYears(-2)) &&
                                (services.Contains(data.combined.combined.combined.combined.ser.SystemService) || data.combined.combined.combined.combined.res.Subject != null) &&
                                (data.combined.combined.combined.combined.ser.SystemService != Clouds42Service.MyDatabases || data.combined.combined.combined.combined.res.Cost > 0))
                            .OrderByDescending(data => data.combined.combined.resConf.ExpireDate)
                            .Select(data => new ResourceReportDataDto
                            {
                                Id = data.combined.combined.combined.combined.res.Id,
                                AccountNumber = data.combined.combined.combined.combined.account.IndexNumber,
                                LicenseName = data.combined.combined.combined.combined.ser.SystemService == Clouds42Service.MyDisk
                                    ? data.combined.combined.combined.combined.serType.Name + " " + data.combined.diskRes.VolumeGb + "Гб"
                                    : data.combined.combined.combined.combined.serType.Name,
                                BillingType = services.Contains(data.combined.combined.combined.combined.ser.SystemService)
                                    ? BillingTypeEnum.ForAccount
                                    : data.combined.combined.combined.combined.serType.BillingType,
                                AccountUserLogin = data.combined.combined.combined.accUser.Login,
                                BillingServiceName = data.combined.combined.combined.combined.ser.Name,
                                ExpireDate = data.combined.combined.resConf.ExpireDate,
                                Cost = (services.Contains(data.combined.combined.combined.combined.ser.SystemService)
                                    ? data.combined.combined.resConf.Cost
                                    : data.combined.combined.combined.combined.res.Cost),
                                SponsorAccountIndexNumber = dbLayer.AccountsRepository.AsQueryable()
                                    .Where(s => s.Id == data.combined.combined.combined.combined.res.AccountSponsorId)
                                    .Select(s => s.IndexNumber)
                                    .FirstOrDefault(),
                                Country = data.combined.combined.combined.combined.locale.Country,
                                SaleManager = data.asm,
                            })
                            .ToList();
        }
    }
}
