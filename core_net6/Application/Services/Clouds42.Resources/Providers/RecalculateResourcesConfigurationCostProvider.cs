﻿using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;

namespace Clouds42.Resources.Providers
{
    /// <summary>
    /// Провайдер для пересчета стоимости конфигурации ресурсов
    /// </summary>
    public class RecalculateResourcesConfigurationCostProvider(
        IUnitOfWork dbLayer,
        ISender sender) : IRecalculateResourcesConfigurationCostProvider
    {
        /// <summary>
        /// Пересчитать стоимость конфигурации ресурсов по сервису
        /// Учитываются только используемые ресурсы(по описанию ресурсов)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        public void Recalculate(Guid accountId, Guid serviceId) => Recalculate(accountId, GetBillingService(serviceId));

        /// <summary>
        /// Пересчитать стоимость конфигурации ресурсов по сервису
        /// Учитываются только используемые ресурсы(по описанию ресурсов)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="service">Id сервиса</param>
        public void Recalculate(Guid accountId, IBillingService service)
        {
            sender.Send(new RecalculateResourcesConfigurationCostCommand([accountId],
                [service.Id], service.SystemService)).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        private BillingService GetBillingService(Guid serviceId) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(x => x.Id == serviceId) ??
            throw new NotFoundException($"Сервис '{serviceId}' не найден!");
    }
}
