﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Resources.Providers
{
    /// <summary>
    /// Провайдер создания ресурса для сервиса "Мои информационные базы"
    /// </summary>
    internal class CreateMyDatabasesResourceProvider(
        IUnitOfWork dbLayer,
        ICreateResourceProvider createResourceProvider,
        ISystemServiceTypeDataProvider systemServiceTypeDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ICreateMyDatabasesResourceProvider
    {
        /// <summary>
        /// Создать ресурс для сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="model">Модель создания ресурса "Мои информационные базы"</param>
        public void Create(CreateMyDatabasesResourceDto model)
        {
            var message = $"Создание ресурса системной услуги {model.SystemServiceType} для аккаунта {model.AccountId}";

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var resourceId = CreateResource(model);
                CreateMyDatabasesResource(resourceId, model.PaidDatabasesCount, model.ActualDatabasesCount);

                dbScope.Commit();

                logger.Trace($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания ресурса системной услуги] {model.SystemServiceType} для аккаунта {model.AccountId}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать ресурс услуги 
        /// </summary>
        /// <param name="model">Модель создания ресурса "Мои информационные базы"</param>
        /// <returns>Id ресурса</returns>
        private Guid CreateResource(CreateMyDatabasesResourceDto model) =>
            createResourceProvider.Create(
                new CreateResourceDto
                {
                    AccountId = model.AccountId,
                    AccountSponsorId = model.AccountSponsorId,
                    Cost = model.ResourceCost,
                    SubjectId = model.AccountId,
                    IsFree = false,
                    BillingServiceTypeId =
                        systemServiceTypeDataProvider.GetServiceTypeIdBySystemServiceType(model.SystemServiceType),
                });

        /// <summary>
        /// Создать ресурс для сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="resourceId">Id созданного ресурса</param>
        /// <param name="paidDatabasesCount">Количество оплаченных информационных баз</param>
        /// <param name="actualDatabasesCount">Актуальное количество инф. баз</param>
        private void CreateMyDatabasesResource(Guid resourceId, int paidDatabasesCount, int actualDatabasesCount)
        {
            var myDatabasesResource = new MyDatabasesResource
            {
                ResourceId = resourceId,
                PaidDatabasesCount = paidDatabasesCount,
                ActualDatabasesCount = actualDatabasesCount
            };

            dbLayer.GetGenericRepository<MyDatabasesResource>().Insert(myDatabasesResource);
            dbLayer.Save();
        }
    }
}
