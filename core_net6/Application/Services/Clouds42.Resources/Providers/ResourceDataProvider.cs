﻿using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;
using System.Linq.Expressions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Resources.Providers
{
    /// <summary>
    /// Провайдер для работы с данными ресурсов
    /// </summary>
    public class ResourceDataProvider(IUnitOfWork dbLayer) : IResourceDataProvider
    {
        /// <summary>
        /// Получить список ресурсов
        /// </summary>
        /// <param name="expression">Выражение для поиска</param>
        /// <param name="service">Тип системного сервиса</param>
        /// <param name="resourceTypes">Типы ресурсов</param>
        /// <returns>Cписок ресурсов</returns>
        public List<Resource> GetResources(Clouds42Service service, Expression<Func<Resource, bool>> expression,
            params ResourceType[] resourceTypes)
        {
            var resources = GetResourcesLazy(service, expression, resourceTypes);
            return resources.ToList();
        }

        /// <summary>
        /// Получить список ресурсов
        /// </summary>
        /// <param name="expression">Выражение для поиска</param>
        /// <param name="service">Тип системного сервиса</param>
        /// <param name="resourceTypes">Типы ресурсов</param>
        /// <returns>Cписок ресурсов</returns>
        public IQueryable<Resource> GetResourcesLazy(Clouds42Service service,
            Expression<Func<Resource, bool>> expression, params ResourceType[] resourceTypes)
        {
            var resTypeStrings = resourceTypes.Select(s => s).ToList();
            var resources = dbLayer.ResourceRepository.WhereLazy(r =>
                r.BillingServiceType.Service.SystemService == service &&
                r.BillingServiceType.SystemServiceType.HasValue &&
                resTypeStrings.Contains(r.BillingServiceType.SystemServiceType.Value));
            resources = resources.Where(expression);
            return resources;
        }

        /// <summary>
        /// Получить все ресурсы аккаунта по услуге
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Ресурсы аккаунта по услуге</returns>
        public IQueryable<Resource> GetAllAccountResourcesForServiceType(Guid accountId, Guid serviceTypeId) =>
            dbLayer.ResourceRepository.WhereLazy(rs =>
                (rs.AccountId == accountId && rs.AccountSponsorId == null || rs.AccountSponsorId == accountId) &&
                rs.BillingServiceTypeId == serviceTypeId);

        /// <summary>
        /// Получить стоимость используемых ресурсов по сервису для аккаунта
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Стоимость используемых ресурсов по сервису для аккаунта</returns>
        public decimal GetUsedPaidResourcesCostByService(Guid serviceId, Guid accountId) =>
            GetAllAccountResourcesForService(accountId, serviceId).ToList()
                .Where(res => res.Subject.HasValue)
                .Where(res =>
                    !res.DemoExpirePeriod.HasValue || (res.DemoExpirePeriod.Value - DateTime.Now).Days == 0)
                .Sum(res => (decimal?)res.Cost) ?? 0;

        /// <summary>
        /// Получить все ресурсы аккаунта по сервису
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Все ресурсы аккаунта по сервису</returns>
        public IQueryable<Resource> GetAllAccountResourcesForService(Guid accountId, Guid serviceId) =>
            from resource in dbLayer.ResourceRepository.WhereLazy()
            join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource.BillingServiceTypeId
                equals serviceType.Id
            where serviceType.ServiceId == serviceId &&
                  (resource.AccountId == accountId && resource.AccountSponsorId == null ||
                   resource.AccountSponsorId == accountId)
            select resource;

        /// <summary>
        /// Получить сгрупированные ресурсы услуги по аккаунту
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Сгрупированные ресурсы услуги по аккаунту</returns>
        public IQueryable<GroupedResourcesByAccountDto> GetGroupedServiceTypeResourcesByAccount(Guid serviceTypeId) =>
            from resource in dbLayer.ResourceRepository.WhereLazy(r => r.BillingServiceTypeId == serviceTypeId)
            select new { resource, resourceAccountId = resource.AccountSponsorId ?? resource.AccountId }
            into selectedResources
            group selectedResources by selectedResources.resourceAccountId
            into groupedResources
            select new GroupedResourcesByAccountDto
            {
                AccountId = groupedResources.Key,
                Resources = groupedResources.Select(g => g.resource).ToList()
            };

        /// <summary>
        /// Получить данные оплаченных/используемых ресурсов по услугам
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные оплаченных/используемых ресурсов по услугам
        /// для аккаунта</returns>
        public IQueryable<ResourcesDataByServiceTypeDto> GetUsedPaidResourcesDataByServiceTypes(Guid accountId)
        {
            return dbLayer.ResourceRepository
                .AsQueryableNoTracking()
                .Include(x => x.MyDatabasesResource)
                .Where(x => x.Subject.HasValue && (x.AccountId == accountId && x.AccountSponsorId == null ||
                                                   x.AccountSponsorId == accountId))
                .Select(x => new
                {
                    VolumeInQuantity =
                        x.MyDatabasesResourceId.HasValue ? x.MyDatabasesResource.ActualDatabasesCount : 1,
                    ServiceTypeId = x.BillingServiceTypeId,
                    x.Cost
                })
                .GroupBy(x => x.ServiceTypeId)
                .Select(x => new ResourcesDataByServiceTypeDto
                {
                    ServiceTypeId = x.Key,
                    TotalAmount = x.Sum(gp => gp.Cost),
                    VolumeInQuantity = x.Sum(gp => gp.VolumeInQuantity)
                });
        }

        /// <summary>
        /// Получить свободные ресурсы по услуге для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Свободные ресурсы по услуге для аккаунта</returns>
        public IQueryable<Resource> GetFreeResourcesByServiceType(Guid accountId, Guid serviceTypeId) =>
            dbLayer.ResourceRepository.WhereLazy(r =>
                r.BillingServiceTypeId == serviceTypeId && r.AccountId == accountId && r.AccountSponsorId == null &&
                r.Subject == null);

        /// <summary>
        /// Получить ресурсы пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Список ресурсов пользователя</returns>
        public IQueryable<Resource> GetAccountUserResources(Guid accountUserId)
            => dbLayer.ResourceRepository.WhereLazy(res => res.Subject == accountUserId);

        /// <summary>
        /// Получить ресурсы сервиса для пользователя
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Список ресурсов сервиса для пользователя</returns>
        public IQueryable<Resource> GetServiceResourcesForAccountUser(Guid serviceId, Guid accountUserId)
            => dbLayer.ResourceRepository.WhereLazy(r =>
                r.Subject == accountUserId && r.BillingServiceType.ServiceId == serviceId);

        /// <summary>
        /// Получить ресурсы аккаунта сгруппированные по сервису
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Ресурсы аккаунта сгруппированные по сервису</returns>
        public IQueryable<GroupedResourcesByServiceDto> GetGroupedResourcesByService(Guid accountId) =>
            (from resource in dbLayer.ResourceRepository.WhereLazy()
             join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource.BillingServiceTypeId
                 equals serviceType.Id
             where resource.AccountId == accountId || resource.AccountSponsorId == accountId
             select new
             {
                 serviceType.ServiceId,
                 Resource = resource
             }).GroupBy(res => res.ServiceId).Select(res => new GroupedResourcesByServiceDto
             {
                 ServiceId = res.Key,
                 Resources = res.Select(r => r.Resource).ToList()
             });
    }
}
