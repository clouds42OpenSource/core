﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Resources.Providers
{
    /// <summary>
    /// Провайдер для работы с данными конфигурации ресурсов
    /// </summary>
    public class ResourcesConfigurationDataProvider(IUnitOfWork dbLayer, ICloudServiceAdapter cloudServiceAdapter)
        : IResourcesConfigurationDataProvider
    {
        /// <summary>
        /// Получить конфигурацию ресурсов сервиса
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Конфигурация ресурсов</returns>
        public ResourcesConfiguration GetResourcesConfiguration(Guid accountId, Guid serviceId) =>
            dbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == accountId && config.BillingServiceId == serviceId);

        /// <summary>
        /// Получить конфигурацию ресурсов сервиса
        /// для аккаунта или выкинуть исключение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Конфигурация ресурсов</returns>
        public ResourcesConfiguration GetResourcesConfigurationOrThrowException(Guid accountId, Guid serviceId) =>
            GetResourcesConfiguration(accountId, serviceId) ?? throw new NotFoundException(
                $"Не удалось получить конфигурацию ресурса сервиса '{serviceId}' для аккаунта '{accountId}'");

        /// <summary>
        /// Получить конфигурацию ресурсов сервиса
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemService">Тип системного сервиса</param>
        /// <returns>Конфигурация ресурсов</returns>
        public ResourcesConfiguration GetResourcesConfiguration(Guid accountId, Clouds42Service systemService) =>
            dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingService.SystemService == systemService);


        /// <summary>
        /// Получить дату пролонгации
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <returns>Дата пролонгации</returns>
        public DateTime GetExpireDateOrThrowException(ResourcesConfiguration resourcesConfiguration)
        {
            return resourcesConfiguration.IsDemoPeriod
                ? resourcesConfiguration.ExpireDate ??
                  throw new NotFoundException("У сервиса в режиме демо дата пролонгации не может быть null")
                : cloudServiceAdapter.GetMainConfigurationOrThrowException(resourcesConfiguration).ExpireDate ??
                  throw new NotFoundException("У основного сервиса не указана дата пролонгации");
        }
    }
}