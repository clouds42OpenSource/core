﻿using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Resources.Providers
{
    /// <summary>
    /// Провайдер для создания ресурса услуги
    /// </summary>
    internal class CreateResourceProvider(IUnitOfWork dbLayer) : ICreateResourceProvider
    {
        /// <summary>
        /// Создать ресурс услуги
        /// </summary>
        /// <param name="createResourceDc">Модель создания нового ресурса</param>
        /// <returns>Ресурс услуги</returns>
        public Guid Create(CreateResourceDto createResourceDc)
        {
            var resource = createResourceDc.MapToResource();
            dbLayer.ResourceRepository.Insert(resource);
            dbLayer.Save();

            return resource.Id;
        }
    }
}
