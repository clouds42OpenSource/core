﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.Resources.Managers
{
    /// <summary>
    /// Менеджер для работы с данными ресурсов для отчета 
    /// </summary>
    public class ResourceReportDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IResourceReportDataProvider resourceReportDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить список моделей данных ресурсов для отчета
        /// </summary>
        /// <returns>Список моделей данных ресурсов для отчета</returns>
        public ManagerResult<List<ResourceReportDataDto>> GetResourceReportDataDcs()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.GetResourceReportDataDcs);
                var data = resourceReportDataProvider.GetResourceReportDataDcs();
                logger.Info("Получение списка моделей данных ресурсов для отчета завершилось успешно");
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения списка моделей данных ресурсов для отчета]");
                return PreconditionFailed<List<ResourceReportDataDto>>(ex.Message);
            }
        }
    }
}
