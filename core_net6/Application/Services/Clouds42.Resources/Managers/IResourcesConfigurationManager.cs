﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Resources.Managers;

public interface IResourcesConfigurationManager
{
    ManagerResult<List<ResourcesConfiguration>> GetResourceConfigurations(string service, DateTime dateFrom, DateTime dateTo);

    /// <summary>
    /// Получить дату окончания срока действия для сервиса, привязанного к указанному аккаунту.
    /// </summary>
    /// <param name="accountId">Идентификатор аккаунта</param>
    /// <param name="service">Имя сервиса</param>
    /// <returns>Дата окончания срока действия указанного сервиса для указанного аккаунта или NotFound, 
    /// если отсутствует или не найдена</returns>
    Task<ManagerResult<DateTime>> GetServiceExpireDate(Guid accountId, string service);

    /// <summary>
    /// Метод для логирования роботы корворкера
    /// </summary>
    /// <param name="getAccountId">Функтор получения идентификатора аккаунта</param>
    /// <param name="log">Действие для записи в лог</param>
    /// <param name="description">Описание события</param>
    public void LogEvent(Func<Guid?> getAccountId, LogActions log, string description);
}
