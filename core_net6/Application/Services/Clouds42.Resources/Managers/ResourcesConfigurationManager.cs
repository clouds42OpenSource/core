﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Resources.Managers
{
    /// <summary>
    /// Класс для управления конфигурацией ресурсов.
    /// </summary>      
    public class ResourcesConfigurationManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IResourcesConfigurationManager
    {
        public ManagerResult<List<ResourcesConfiguration>> GetResourceConfigurations(string service, DateTime dateFrom, DateTime dateTo)
        {
            AccessProvider.HasAccess(ObjectAction.Resources_GetResConf);

            if (!Enum.TryParse(service, out Clouds42Service serviceEnum))
                return PreconditionFailed<List<ResourcesConfiguration>>($"Недопустимый тип системного сервиса '{service}' ");

            if (dateFrom > dateTo)
            {
                return BadRequest<List<ResourcesConfiguration>>("DateFrom must be less than dateTo.");
            }

            if (!DbLayer.ResourceConfigurationRepository.Any(r => r.BillingService.SystemService == serviceEnum))
            {
                return NotFound<List<ResourcesConfiguration>>("Service not found");
            }

            var result = DbLayer.ResourceConfigurationRepository
                .Where(r => r.BillingService.SystemService == serviceEnum &&
                            r.ExpireDate.Value > dateFrom &&
                            r.ExpireDate < dateTo).ToList();

            return Ok(result);
        }

        /// <summary>
        /// Получить дату окончания срока действия для сервиса, привязанного к указанному аккаунту.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="service">Имя сервиса</param>
        /// <returns>Дата окончания срока действия указанного сервиса для указанного аккаунта или NotFound, 
        /// если отсутствует или не найдена</returns>
        public async Task<ManagerResult<DateTime>> GetServiceExpireDate(Guid accountId, string service)
        {
            AccessProvider.HasAccess(ObjectAction.ResourceConfiguration_GetExpireDate, () => accountId);
            if (!Enum.TryParse(service, out Clouds42Service serviceEnum))
                return PreconditionFailed<DateTime>($"Недопустимый тип системного сервиса {service}.");

            var resConf = await DbLayer.ResourceConfigurationRepository.GetServiceExpirationDate(accountId, serviceEnum, ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays"));
       

            return !resConf.HasValue ? NotFound<DateTime>("ExpireDate is null") : Ok(resConf.Value);
        }
    }
}
