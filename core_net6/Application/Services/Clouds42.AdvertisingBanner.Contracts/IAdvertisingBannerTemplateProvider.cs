﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.DataModels.Marketing;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для работы с шаблоном рекламного баннера
    /// </summary>
    public interface IAdvertisingBannerTemplateProvider
    {
        /// <summary>
        /// Получить модель шаблона рекламного баннера
        /// </summary>
        /// <param name="id">Id шаблона рекламного баннера</param>
        /// <returns>Модель шаблона рекламного баннера</returns>
        AdvertisingBannerTemplateDetailsDto GetAdvertisingBannerTemplateDto(int id);

        /// <summary>
        /// Получить шаблон рекламного баннера
        /// </summary>
        /// <param name="id">Id шаблона рекламного баннера</param>
        /// <returns>Шаблон рекламного баннера</returns>
        AdvertisingBannerTemplate GetAdvertisingBannerTemplate(int id);

        /// <summary>
        /// Получить краткую информацию по шаблонам рекламного баннера
        /// </summary>
        /// <returns>Краткая информация по шаблонам рекламного баннера</returns>
        IEnumerable<AdvertisingBannerTemplateShortInfoDto> GetAdvertisingBannerTemplatesShortInfo();

        /// <summary>
        /// Получить изображение рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerId">Id шаблона рекламного баннера</param>
        /// <returns>Изображение рекламного баннера</returns>
        FileDataDto<byte[]> GetAdvertisingBannerImage(int advertisingBannerId);
    }
}
