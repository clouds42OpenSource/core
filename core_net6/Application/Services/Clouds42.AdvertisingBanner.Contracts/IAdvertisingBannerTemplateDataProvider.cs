﻿using Clouds42.DataContracts.Service.Market;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для работы с данными шаблона рекламного баннера
    /// </summary>
    public interface IAdvertisingBannerTemplateDataProvider
    {
        /// <summary>
        /// Получить данные шаблонов рекламных баннеров
        /// </summary>
        /// <returns>Список моделей данных шаблона рекламного баннера</returns>
        IEnumerable<AdvertisingBannerTemplateDataDto> GetAdvertisingBannerTemplates();
    }
}
