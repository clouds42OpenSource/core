﻿namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для удаления аудитории рекламного баннера
    /// </summary>
    public interface IDeleteAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Удалить аудиторию рекламного баннера
        /// </summary>
        /// <param name="bannerAudienceId">Id аудитории рекламного баннера</param>
        void Delete(int bannerAudienceId);
    }
}
