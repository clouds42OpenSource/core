﻿
namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для удаления рекламного баннера
    /// </summary>
    public interface IDeleteAdvertisingBannerTemplateProvider
    {
        /// <summary>
        /// Удалить рекламный баннер
        /// </summary>
        /// <param name="bannerTemplateId">Id рекламного баннера</param>
        void Delete(int bannerTemplateId);
    }
}
