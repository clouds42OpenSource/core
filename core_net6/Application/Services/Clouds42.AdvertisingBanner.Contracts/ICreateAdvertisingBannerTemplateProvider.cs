﻿using Clouds42.DataContracts.Service.Market;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для создания шаблона рекламного баннера
    /// </summary>
    public interface ICreateAdvertisingBannerTemplateProvider
    {
        /// <summary>
        /// Создать шаблон рекламного баннера
        /// </summary>
        /// <param name="model">Модель создания шаблона рекламного баннера</param>
        void Create(CreateAdvertisingBannerTemplateDto model);
    }
}
