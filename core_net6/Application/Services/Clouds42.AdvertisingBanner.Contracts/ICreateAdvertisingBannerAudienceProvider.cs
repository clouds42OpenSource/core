﻿using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для создания аудитории рекламного баннера
    /// </summary>
    public interface ICreateAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Создать аудиторию рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerAudienceDto">Модель аудитории рекламного баннера</param>
        /// <returns>Id созданной аудитории</returns>
        int Create(AdvertisingBannerAudienceDto advertisingBannerAudienceDto);
    }
}
