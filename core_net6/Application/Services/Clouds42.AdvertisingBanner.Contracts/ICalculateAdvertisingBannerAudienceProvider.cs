﻿using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для подсчета аудитории рекламного баннера
    /// </summary>
    public interface ICalculateAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Посчитать аудиторию рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerAudience">Модель аудитории рекламного баннера</param>
        /// <returns>Результат подсчета аудитории рекламного баннера</returns>
        CalculationAdvertisingBannerAudienceDto Calculate(AdvertisingBannerAudienceDto advertisingBannerAudience);
    }
}
