﻿using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для редактирования аудитории рекламного баннера
    /// </summary>
    public interface IEditAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Редактировать аудиторию рекламного баннера
        /// </summary>
        /// <param name="id">Id аудитории рекламного баннера</param>
        /// <param name="advertisingBannerAudienceDto">Модель аудитории рекламного баннера</param>
        void Edit(int id, AdvertisingBannerAudienceDto advertisingBannerAudienceDto);
    }
}
