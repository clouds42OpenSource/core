﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Domain.Enums.Marketing;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для работы с аудиторией рекламного баннера
    /// </summary>
    public interface IAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Получить аудиторию рекламного баннера
        /// </summary>
        /// <param name="id">Id рекламного баннера</param>
        /// <returns>Аудитория рекламного баннера</returns>
        AdvertisingBannerAudience GetAdvertisingBannerAudience(int id);

        /// <summary>
        /// Выбрать/подобрать аудиторию рекламного баннера
        /// </summary>
        /// <returns>Выбранные аккаунты(аудитория рекламного баннера)</returns>
        IQueryable<Account> SelectAdvertisingBannerAudience(List<Guid> locales, AccountTypeEnum accountType,
            AvailabilityPaymentEnum availabilityPayment, RentalServiceStateEnum rentalServiceState);
    }
}
