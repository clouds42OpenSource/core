﻿using Clouds42.DataContracts.Service.Market;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Провайдер для редактирования шаблона рекламного баннера
    /// </summary>
    public interface IEditAdvertisingBannerTemplateProvider
    {
        /// <summary>
        /// Редактировать шаблон рекламного баннера
        /// </summary>
        /// <param name="editAdvertisingBannerTemplateDto">Модель редактирования шаблона рекламного баннера</param>
        void Edit(EditAdvertisingBannerTemplateDto editAdvertisingBannerTemplateDto);
    }
}
