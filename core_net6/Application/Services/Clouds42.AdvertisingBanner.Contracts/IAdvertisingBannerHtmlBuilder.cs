﻿using Clouds42.Domain.DataModels.Marketing;

namespace Clouds42.AdvertisingBanner.Contracts
{
    /// <summary>
    /// Билдер Html рекламного баннера
    /// </summary>
    public interface IAdvertisingBannerHtmlBuilder
    {
        /// <summary>
        /// Сгенерировать Html для рекламного баннера
        /// </summary>
        /// <param name="template">Шаблон рекламного баннера</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Html рекламного баннера</returns>
        string Build(AdvertisingBannerTemplate template, Guid accountId);
    }
}
