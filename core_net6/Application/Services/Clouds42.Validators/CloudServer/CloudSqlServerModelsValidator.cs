﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.CloudServicesSegment.Interface;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Validators.CloudServer
{
    /// <summary>
    /// Валидатор моделей Sql сервера облака
    /// </summary>
    public class CloudSqlServerModelsValidator(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Попытаться выполнить валидацию создания
        /// Sql сервера облака
        /// </summary>
        /// <param name="cloudServer">Модель Sql сервера облака</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public bool TryValidateCreation(ICloudServerDto cloudServer, out string errorMessage) =>
            TryValidate(cloudServer, out errorMessage);

        /// <summary>
        /// Попытаться выполнить валидацию редактирования
        /// Sql сервера облака
        /// </summary>
        /// <param name="cloudServer">Модель Sql сервера облака</param>
        /// <param name="sqlServerId">Id Sql сервера для редактирования</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public bool TryValidateEditing(ICloudServerDto cloudServer, Guid sqlServerId, out string errorMessage) =>
            TryValidate(cloudServer, out errorMessage, sqlServerId);

        /// <summary>
        /// Попытаться выполнить валидацию модели
        /// Sql сервера облака
        /// </summary>
        /// <param name="cloudServer">Модель Sql сервера облака</param>
        /// <param name="sqlServerId">Id Sql сервера</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        private bool TryValidate(ICloudServerDto cloudServer, out string errorMessage, Guid? sqlServerId = null)
        {
            errorMessage = "";

            if (cloudServer.Name.IsNullOrEmpty())
            {
                errorMessage = "'Название' Sql сервера обязательное поле";
                return false;
            }

            if (cloudServer.ConnectionAddress.IsNullOrEmpty())
            {
                errorMessage = "'Адрес' Sql сервера обязательное поле";
                return false;
            }

            if (!IsUniqueName(cloudServer.Name, sqlServerId))
            {
                errorMessage = $"Sql сервер с таким '{cloudServer.Name}' названием уже существует";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверить что название сервера Sql уникально
        /// </summary>
        /// <param name="name">Название сервера Sql</param>
        /// <param name="sqlServerId">Id сервера Sql</param>
        /// <returns>Название является уникальным</returns>
        private bool IsUniqueName(string name, Guid? sqlServerId = null) =>
            dbLayer.CloudServicesSqlServerRepository.FirstOrDefault(ss =>
                ss.Name == name && ss.ID != sqlServerId) == null;
    }
}
