﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.Validators.CloudServer
{
    /// <summary>
    /// Валидатор модели сервера облака
    /// </summary>
    public static class CloudServerValidator
    {
        /// <summary>
        /// Выполнить валидацию
        /// </summary>
        /// <param name="cloudServer">Модель сервера облака</param>
        public static void Validate(this ICloudServerDto cloudServer)
        {
            if (cloudServer.Name.IsNullOrEmpty())
                throw new ValidateException("Поле 'Название' обязательное для заполнения");

            if (cloudServer.ConnectionAddress.IsNullOrEmpty())
                throw new ValidateException("Поле 'Адрес' обязательное для заполнения");
        }
    }
}
