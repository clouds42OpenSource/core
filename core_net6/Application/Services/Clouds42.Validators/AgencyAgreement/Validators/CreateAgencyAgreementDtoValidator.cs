﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.Validators.AgencyAgreement.Validators
{
    /// <summary>
    /// Валидатор модели создания агентского соглашения
    /// </summary>
    public static class CreateAgencyAgreementDtoValidator
    {
        /// <summary>
        /// Выполнить валидацию модели создания
        /// агентского соглашения
        /// </summary>
        /// <param name="createAgencyAgreementDto">Модель создания агентского соглашения</param>
        public static void Validate(this AddAgencyAgreementItemDto createAgencyAgreementDto)
        {
            if (createAgencyAgreementDto == null)
                throw new ArgumentException("Модель создания агентского соглашения отсутствует");

            if (string.IsNullOrEmpty(createAgencyAgreementDto.Name))
                throw new ArgumentException("Поле 'Название агентского соглашения' обязательное");

            if (createAgencyAgreementDto.PrintedHtmlFormId.Equals(Guid.Empty))
                throw new ArgumentException("Поле 'Шаблон агентского соглашения' обязательное");


            createAgencyAgreementDto.MyDiskRewardPercent.ValidateRewardPercent();
            createAgencyAgreementDto.Rent1CRewardPercent.ValidateRewardPercent();
            createAgencyAgreementDto.ServiceOwnerRewardPercent.ValidateRewardPercent();

            if (DateTime.Now.Date > createAgencyAgreementDto.EffectiveDate.Date)
                throw new ArgumentException("Дата вступления в силу не может быть прошедшей");
        }

        /// <summary>
        /// Выполнить валидацию % вознаграждения
        /// </summary>
        /// <param name="rewardPercent">% вознаграждение</param>
        private static void ValidateRewardPercent(this decimal rewardPercent)
        {
            if (rewardPercent > 100 || rewardPercent < 0)
                throw new ArgumentException("Поля ввода '% вознаграждения' могут принимать значение от 0% до 100%");
        }
    }
}
