﻿using Clouds42.Validators.Account.Helpers;
using Clouds42.Validators.Account.Validators;
using Clouds42.Validators.CloudServer;
using Clouds42.Validators.DelemiterSourceAccountDatabase.Validators;
using Clouds42.Validators.Partner.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Validators
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddTransient<DelimiterSourceAccountDatabaseDtoValidator>();
            services.AddTransient<AccountUserDataValidator>();
            services.AddTransient<CreateNewUserAvailableLoginValidator>();
            services.AddTransient<AccountsBindingDtoValidator>();
            services.AddTransient<AgencyPaymentCreationDtoValidator>();
            services.AddTransient<CloudSqlServerModelsValidator>();
            

            return services;
        }
    }
}
