﻿using System.Text.RegularExpressions;

namespace Clouds42.Validators.Partner.Dictionary
{
    /// <summary>
    /// Справочник регулярных выражений
    /// реквизитов агента
    /// </summary>
    public static class AgentRequisitesRegexReference
    {
        /// <summary>
        /// Регулярное выражение для валидации ФИО
        /// </summary>
        public static Regex FullNameRegx { get; } = new(@"^[А-ЯЁа-яё ]*$");

        /// <summary>
        /// Регулярное выражение для валидации ОГРН (физ и юр лиц)
        /// </summary>
        public static Regex OgrnRegx { get; } = new(@"^\d{13}$");

        /// <summary>
        /// Регулярное выражение для валидации ОГРН (ИП)
        /// </summary>
        public static Regex OgrnForSoleProprietorRegx { get; } = new(@"^\d{15}$");

        /// <summary>
        /// Регулярное выражение для валидации КПП
        /// </summary>
        public static Regex KppRegx { get; } =
            new(@"\d{4}[\dA-Z][\dA-Z]\d{3}", RegexOptions.None, TimeSpan.FromSeconds(.5));

        /// <summary>
        /// Регулярное выражение для валидации ИНН физических лиц и ИП
        /// </summary>
        public static Regex InnRegx { get; } = new(@"^\d{12}$");

        /// <summary>
        /// Регулярное выражение для валидации ИНН юр лиц
        /// </summary>
        public static Regex InnForLegalPersonRegx { get; } = new(@"^\d{10}$");

        /// <summary>
        /// Регулярное выражение для валидации расчетного счета
        /// </summary>
        public static Regex SettlementAccountRegx { get; } = new(@"^\d{20}$");

        /// <summary>
        /// Регулярное выражение для валидации БИК
        /// </summary>
        public static Regex BikRegx { get; } = new(@"^\d{9}$");

        /// <summary>
        /// Регулярное выражение для валидации корреспондентского счета
        /// </summary>
        public static Regex CorrespondentAccountRegx { get; } = new(@"^\d{20}$");

        /// <summary>
        /// Регулярное выражение для валидации серии паспорта
        /// </summary>
        public static  Regex PassportSeriesRegx { get; } = new(@"^\d{4}$");

        /// <summary>
        /// Регулярное выражение для валидации номера паспорта
        /// </summary>
        public static Regex PassportNumberRegx { get; } = new(@"^\d{6}$");

        /// <summary>
        /// Регулярное выражение для валидации СНИЛС
        /// </summary>
        public static Regex SnilsRegx { get; } = new(@"^\d{11}$");

        /// <summary>
        /// Регулярное выражение для полей с общими правилами
        /// </summary>
        public static Regex СommonRegx { get; } = new(@"^[А-Яа-яA-Za-z0-9 '/""-,.:;№]* $");

        /// <summary>
        /// Регулярное выражение для полей с общими правилами JS
        /// </summary>
        public static string СommonRegxJs { get; } = @"^[А-Яа-я0-9A-Za-z \'\/\"",.\-:;№]*$";

    }
}
