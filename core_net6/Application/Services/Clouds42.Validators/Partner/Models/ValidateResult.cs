﻿namespace Clouds42.Validators.Partner.Models
{
    /// <summary>
    /// Модель результата валидации
    /// </summary>
    public class ValidateResult
    {
        /// <summary>
        /// Успешность
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
