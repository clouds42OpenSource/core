﻿using System.Text;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Validators.Partner.Helpers
{
    /// <summary>
    /// Хелпер для работы с результатом валидации
    /// </summary>
    public static class ValidateResultHelper
    {
        /// <summary>
        /// Обработать результаты валидации
        /// </summary>
        /// <param name="validateResults">Результаты валидации</param>
        /// <returns>Результат валидации</returns>
        public static ValidateResult ProcessValidateResults(this List<ValidateResult> validateResults)
        {
            var errorResults = validateResults.Where(w => !w.Success).ToList();

            if (!errorResults.Any())
                return CreateSuccessResult();

            var builder = new StringBuilder();

            errorResults.ForEach(w =>
            {
                builder.AppendLine($"{w.Message}");
            });

            return CreateErrorResult(builder.ToString());
        }

        /// <summary>
        /// Создать успешный результат валидации
        /// </summary>
        /// <returns>Результат валидации</returns>
        public static ValidateResult CreateSuccessResult() => new() {Success = true};

        /// <summary>
        /// Создать результат валидации
        /// с ошибкой
        /// </summary>
        /// <param name="message">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public static ValidateResult CreateErrorResult(string message) =>
            new() {Success = false, Message = message};

        /// <summary>
        /// Добавить результат валидации
        /// с ошибкой в скисок результатов валидации
        /// </summary>
        /// <param name="validateResults"></param>
        /// <param name="message"></param>
        public static void AddErrorResult(this List<ValidateResult> validateResults, string message)
        {
            validateResults.Add(CreateErrorResult(message));
        }
    }
}
