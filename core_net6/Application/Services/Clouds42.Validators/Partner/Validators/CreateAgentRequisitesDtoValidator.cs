﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;
using Clouds42.Validators.Partner.Helpers;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Validators.Partner.Validators
{
    /// <summary>
    ///     Валидатор модели создания реквизитов агента
    /// </summary>
    public static class CreateAgentRequisitesDtoValidator
    {
        /// <summary>
        /// Карта маппинга типа реквизитов к методу валидации
        /// </summary>
        private static readonly IDictionary<AgentRequisitesTypeEnum, Func<CreateAgentRequisitesDto, ValidateResult>>
            MapAgentRequisitesTypeToValidationMethod = new Dictionary<AgentRequisitesTypeEnum, Func<CreateAgentRequisitesDto, ValidateResult>>
            {
                {AgentRequisitesTypeEnum.LegalPersonRequisites, model => model.LegalPersonRequisites.Validate()},
                {AgentRequisitesTypeEnum.SoleProprietorRequisites, model => model.SoleProprietorRequisites.Validate()},
                {AgentRequisitesTypeEnum.PhysicalPersonRequisites, model => model.PhysicalPersonRequisites.Validate()}
            };

        /// <summary>
        /// Выполнить валидацию модели создания реквизитов агента
        /// </summary>
        /// <param name="createAgentRequisites">Модель реквизитов агента</param>
        /// <returns>Результат валидации</returns>
        public static ValidateResult Validate(this CreateAgentRequisitesDto? createAgentRequisites)
        {
            if (createAgentRequisites == null)
                return ValidateResultHelper.CreateErrorResult("Для создания нужны реквизиты агента");

            return createAgentRequisites.AgentRequisitesStatus switch
            {
                AgentRequisitesStatusEnum.Draft => ValidateResultHelper.CreateSuccessResult(),
                AgentRequisitesStatusEnum.Verified => ValidateResultHelper.CreateErrorResult(
                    $"Создать реквизиты со статусом {AgentRequisitesStatusEnum.Verified.Description()} не возможно"),
                _ => MapAgentRequisitesTypeToValidationMethod[createAgentRequisites.AgentRequisitesType](
                    createAgentRequisites)
            };
        }
    }
}
