﻿using Clouds42.DataContracts.Service.Market;

namespace Clouds42.Validators.Partner.Validators
{
    /// <summary>
    /// Валидатор моделей шаблона рекламного баннера
    /// </summary>
    public static class AdvertisingBannerTemplateValidator
    {
        /// <summary>
        /// Максимальная длина заголовка баннера
        /// </summary>
        public const int MaxLengthHeader = 70;

        /// <summary>
        /// Максимальная длина текста баннера
        /// </summary>
        public const int MaxLengthBody = 200;

        /// <summary>
        /// Максимальная длина описания ссылки баннера
        /// </summary>
        public const int MaxLengthCaptionLink = 30;

        /// <summary>
        /// Выполнить валидацию модели создания шаблона рекламного баннера
        /// </summary>
        /// <param name="createAdvertisingBannerTemplateDto">Модель создания шаблона рекламного баннера</param>
        /// <param name="errorMessage">Собщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public static bool TryValidateObj(this CreateAdvertisingBannerTemplateDto createAdvertisingBannerTemplateDto,
            out string errorMessage)
        {
            errorMessage = "";

            if (string.IsNullOrEmpty(createAdvertisingBannerTemplateDto.Header))
            {
                errorMessage = "Укажите поле 'Заголовок баннера'";
                return false;
            }

            if (createAdvertisingBannerTemplateDto.Header.Length > MaxLengthHeader)
            {
                errorMessage = GetMaxLengthErrorMessage(MaxLengthHeader);
                return false;
            }

            if (string.IsNullOrEmpty(createAdvertisingBannerTemplateDto.Body))
            {
                errorMessage = "Укажите поле 'Тело баннера'";
                return false;
            }

            if (createAdvertisingBannerTemplateDto.Body.Length > MaxLengthBody)
            {
                errorMessage = GetMaxLengthErrorMessage(MaxLengthBody);
                return false;
            }

            if (string.IsNullOrEmpty(createAdvertisingBannerTemplateDto.CaptionLink))
            {
                errorMessage = "Укажите поле 'Описание ссылки'";
                return false;
            }

            if (createAdvertisingBannerTemplateDto.CaptionLink.Length > MaxLengthCaptionLink)
            {
                errorMessage = GetMaxLengthErrorMessage(MaxLengthCaptionLink);
                return false;
            }

            if (string.IsNullOrEmpty(createAdvertisingBannerTemplateDto.Link))
            {
                errorMessage = "Укажите поле 'Ссылка баннера'";
                return false;
            }

            if (string.IsNullOrEmpty(createAdvertisingBannerTemplateDto.Image?.FileName))
            {
                errorMessage = "Укажите изображение баннера";
                return false;
            }

            if (!createAdvertisingBannerTemplateDto.AdvertisingBannerAudience.Locales.Any())
            {
                errorMessage = "Укажите локали аккаунтов";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Получить сообщение об ошибке,
        /// по привышению максимального количества символов
        /// </summary>
        /// <param name="maxLength">Максимальное количество символов</param>
        /// <returns>Сообщение об ошибке</returns>
        private static string GetMaxLengthErrorMessage(int maxLength) =>
            $"Пожалуйста, введите не больше чем {maxLength} символов.";
    }

}
