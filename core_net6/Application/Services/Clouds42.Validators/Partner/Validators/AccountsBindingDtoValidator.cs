﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.DataContracts.Service.Partner.PartnerClients;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Validators.Partner.Validators
{
    /// <summary>
    /// Валидатор модели связи аккаунтов
    /// </summary>
    public class AccountsBindingDtoValidator(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        /// Выполнить валидацию
        /// </summary>
        /// <param name="accountsBindingDto">Модели связи аккаунтов</param>
        /// <returns>Результат валидации</returns>
        public ValidateResult ValidateBinding(AccountsBindingDto accountsBindingDto)
        {
            var clientAccount =
                dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountsBindingDto.ClientAccountId);

            var partnerAccount =
                dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountsBindingDto.PartnerAccountId);

            if (clientAccount == null)
                return new ValidateResult
                    {Message = $"Аккаунт клиента не найден по Id={accountsBindingDto.ClientAccountId}"};

            if (partnerAccount == null)
                return new ValidateResult
                    {Message = $"Аккаунт партнера не найден по Id={accountsBindingDto.PartnerAccountId}"};

            var clientAccountLocale = accountConfigurationDataProvider.GetAccountLocale(clientAccount.Id);
            var partnerAccountLocale = accountConfigurationDataProvider.GetAccountLocale(partnerAccount.Id);

            if (clientAccountLocale.Name != LocaleConst.Russia)
                return new ValidateResult {Message = $"Аккаунт клиента принадлежит локали {clientAccountLocale.Name}"};

            if (partnerAccountLocale.Name != LocaleConst.Russia)
                return new ValidateResult
                    {Message = $"Аккаунт партнера принадлежит локали {partnerAccountLocale.Name}"};

            if (partnerAccount.ReferralAccountID.HasValue && partnerAccount.ReferralAccountID == clientAccount.Id)
                return new ValidateResult
                {
                    Message =
                        $"Аккаунт партнера ({partnerAccount.IndexNumber}, {partnerAccount.AccountCaption}) уже принадлежит указанному клиенту ({clientAccount.IndexNumber}, {clientAccount.AccountCaption})"
                };

            if (clientAccount.Id == partnerAccount.Id)
                return new ValidateResult {Message = "Аккаунты для привязки должны быть разными"};

            return new ValidateResult {Success = true};
        }
    }
}
