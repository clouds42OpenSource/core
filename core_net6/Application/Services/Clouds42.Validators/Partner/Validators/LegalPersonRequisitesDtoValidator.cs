﻿using Clouds42.Common.DataValidations;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Validators.Partner.Dictionary;
using Clouds42.Validators.Partner.Helpers;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Validators.Partner.Validators
{
    /// <summary>
    /// Валидатор модели реквизитов для юр лица
    /// </summary>
    public static class LegalPersonRequisitesDtoValidator
    {
        /// <summary>
        /// Выполнить валидацию модели реквизитов для юр лица
        /// </summary>
        /// <param name="requisites">Модель реквизитов юр лица</param>
        /// <returns>Результат валидации</returns>
        public static ValidateResult Validate(this LegalPersonRequisitesDto requisites)
        {
            if (requisites == null)
                return ValidateResultHelper.CreateErrorResult("В модели нет реквизитов");

            var validateResults = new List<ValidateResult>();
            var phoneValidator = new PhoneNumberValidation(requisites.PhoneNumber);

            if (string.IsNullOrEmpty(requisites.OrganizationName))
                validateResults.AddErrorResult("Наименование организации обязательное поле");

            if (!AgentRequisitesRegexReference.FullNameRegx.IsMatch(requisites.HeadFullName))
                validateResults.AddErrorResult("Поле ФИО руководителя - не валидно");

            if (string.IsNullOrEmpty(requisites.HeadPosition))
                validateResults.AddErrorResult("Должность руководителя обязательное поле");

            if (string.IsNullOrEmpty(requisites.LegalAddress))
                validateResults.AddErrorResult("Юридический адрес обязательное поле");

            if (string.IsNullOrEmpty(requisites.PhoneNumber) || !phoneValidator.PhoneNumberMaskIsValid())
                validateResults.AddErrorResult("Номер телефона не валиден");

            if (!AgentRequisitesRegexReference.OgrnRegx.IsMatch(requisites.Ogrn))
                validateResults.AddErrorResult("ОГРН не валиден");

            if (!AgentRequisitesRegexReference.KppRegx.IsMatch(requisites.Kpp))
                validateResults.AddErrorResult("КПП не валиден");

            if (!AgentRequisitesRegexReference.InnForLegalPersonRegx.IsMatch(requisites.Inn))
                validateResults.AddErrorResult("ИНН не валиден");

            var basePersonRequisitesDtoValidateResult = requisites.ValidateBasePersonRequisitesDto();

            if (!basePersonRequisitesDtoValidateResult.Success)
                validateResults.AddErrorResult(basePersonRequisitesDtoValidateResult.Message);

            return validateResults.ProcessValidateResults();
        }
    }
}
