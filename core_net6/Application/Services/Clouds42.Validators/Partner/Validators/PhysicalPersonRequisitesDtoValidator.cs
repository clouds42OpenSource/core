﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Validators.Partner.Dictionary;
using Clouds42.Validators.Partner.Helpers;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Validators.Partner.Validators
{
    /// <summary>
    /// Валидатор модели реквизитов для физического лица
    /// </summary>
    public static class PhysicalPersonRequisitesDtoValidator
    {
        /// <summary>
        /// Выполнить валидацию модели реквизитов для физ лица
        /// </summary>
        /// <param name="requisites">Модель реквизитов физического лица</param>
        /// <returns>Результат валидации</returns>
        public static ValidateResult Validate(this PhysicalPersonRequisitesDto requisites)
        {
            if (requisites == null)
                return ValidateResultHelper.CreateErrorResult("В модели нет реквизитов");

            var validateResults = new List<ValidateResult>();
            var year = 1753;
            var minDate = new DateTime(year, 1, 1);

            if (!AgentRequisitesRegexReference.FullNameRegx.IsMatch(requisites.FullName))
                validateResults.AddErrorResult("Поле ФИО не валидно");

            if (requisites.DateOfBirth > DateTime.Now)
                validateResults.AddErrorResult("Поле дата рождения не валидно. Этот день ещё не наступил");

            if (requisites.DateOfBirth < minDate)
                validateResults.AddErrorResult($"Поле дата рождения не валидно. Минимальный год который вы можете указать {year}");

            if (!AgentRequisitesRegexReference.PassportSeriesRegx.IsMatch(requisites.PassportSeries))
                validateResults.AddErrorResult("Поле серия паспорта не валидно");

            if (!AgentRequisitesRegexReference.PassportNumberRegx.IsMatch(requisites.PassportNumber))
                validateResults.AddErrorResult("Поле номер паспорта не валидно");

            if (string.IsNullOrEmpty(requisites.WhomIssuedPassport))
                validateResults.AddErrorResult("Поле кем выдан паспорт не валидно");

            if (requisites.PassportDateOfIssue > DateTime.Now)
                validateResults.AddErrorResult("Поле дата выдаче паспорта не валидно. Этот день ещё не наступил");

            if (requisites.PassportDateOfIssue < minDate)
                validateResults.AddErrorResult($"Поле дата выдаче паспорта не валидно. Минимальный год который вы можете указать {year}");

            if (string.IsNullOrEmpty(requisites.RegistrationAddress))
                validateResults.AddErrorResult("Поле адрес прописки не валидно");

            if (!AgentRequisitesRegexReference.InnRegx.IsMatch(requisites.Inn))
                validateResults.AddErrorResult("Поле ИНН не валидно");

            if (!AgentRequisitesRegexReference.SnilsRegx.IsMatch(requisites.Snils))
                validateResults.AddErrorResult("Поле СНИЛС не валидно");

            var basePersonRequisitesDtoValidateResult = requisites.ValidateBasePersonRequisitesDto();

            if (!basePersonRequisitesDtoValidateResult.Success)
                validateResults.AddErrorResult(basePersonRequisitesDtoValidateResult.Message);

            return validateResults.ProcessValidateResults();
        }
    }
}
