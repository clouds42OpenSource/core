﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Validators.Partner.Validators
{
    /// <summary>
    /// Валидатор модели создания агентского платежа
    /// </summary>
    public class AgencyPaymentCreationDtoValidator(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Выполнить валидацию модели создания агентского платежа
        /// </summary>
        /// <param name="agencyPaymentCreation">Модель создания агентского платежа</param>
        /// <returns>Результат валидации</returns>
        public ValidateResult Validate(AgencyPaymentCreationDto agencyPaymentCreation)
        {
            if (agencyPaymentCreation.AgentAccountId.IsNullOrEmpty())
                return new ValidateResult{ Message = "Id аккаунта агента не валиден." };

            if (agencyPaymentCreation.Sum <= 0)
                return new ValidateResult{Message = "Сумма агентского платежа должна быть больше 0"};

            if (agencyPaymentCreation.Date.Date > DateTime.Now.Date)
                return new ValidateResult{Message = "Поле дата не валидно. Этот день еще не наступил"};

            var validateResult = ValidateManualInputPayment(agencyPaymentCreation);

            if (!validateResult.Success)
                return validateResult;

            return new ValidateResult{ Success = true };
        }

        /// <summary>
        /// Выполнить валидацию для платежа
        /// с ручным вводом
        /// </summary>
        /// <param name="agencyPaymentCreation">Модель создания агентского платежа</param>
        /// <returns>Результат валидации</returns>
        private ValidateResult ValidateManualInputPayment(AgencyPaymentCreationDto agencyPaymentCreation)
        {
            if (agencyPaymentCreation.AgentPaymentSourceType != AgentPaymentSourceTypeEnum.ManualInput)
                return new ValidateResult{Success = true};

            CheckRequiredFieldsAreFilled(agencyPaymentCreation, out var errorMessage);
            if(!string.IsNullOrEmpty(errorMessage))
                return new ValidateResult { Message = errorMessage};

            if (agencyPaymentCreation.PaymentType != PaymentType.Inflow)
                return new ValidateResult { Message = "Платеж должен быть входящим" };

            var account =
                dbLayer.AccountsRepository.FirstOrDefault(a => a.IndexNumber == agencyPaymentCreation.AccountNumber);

            if (account == null)
                return new ValidateResult{Message = $"Не удалось найти аккаунт по номеру {agencyPaymentCreation.AccountNumber}"};

            if (agencyPaymentCreation.ClientPaymentSum <= 0)
                return new ValidateResult { Message = "Сумма клиентского платежа должна быть больше 0" };

            return new ValidateResult{Success = true};
        }

        /// <summary>
        /// Проверить, что обязательные поля модели заполнены
        /// </summary>
        /// <param name="agencyPaymentCreation">Модель создания агентского платежа</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        private void CheckRequiredFieldsAreFilled(AgencyPaymentCreationDto agencyPaymentCreation,
            out string errorMessage)
        {
            errorMessage = string.Empty;

            if (!agencyPaymentCreation.ClientPaymentSum.HasValue)
                errorMessage += "Поле 'Сумма клиентского платежа' не заполнено. ";

            if (!agencyPaymentCreation.AccountNumber.HasValue)
                errorMessage += "Поле 'Номер аккаунта' не заполнено, либо заполнено неверно. ";
        }
    }
}
