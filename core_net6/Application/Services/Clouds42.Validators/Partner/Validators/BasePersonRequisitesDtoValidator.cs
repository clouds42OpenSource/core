﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Validators.Partner.Dictionary;
using Clouds42.Validators.Partner.Helpers;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Validators.Partner.Validators
{
    /// <summary>
    /// Валидатор модели базового объекта реквизитов
    /// </summary>
    public static class BasePersonRequisitesDtoValidator
    {
        /// <summary>
        /// Выполнить валидацию модели базового объекта реквизитов
        /// </summary>
        /// <param name="model">Модель базового объекта реквизитов</param>
        /// <param name="needToValidateFiles">Признак, что необходимо валидировать прикрепленные файлы</param>
        /// <returns>Результат валидации</returns>
        public static ValidateResult ValidateBasePersonRequisitesDto(this BasePersonRequisitesDto model, bool needToValidateFiles = true)
        {
            var validateResults = new List<ValidateResult>();

            if (!AgentRequisitesRegexReference.SettlementAccountRegx.IsMatch(model.SettlementAccount))
                validateResults.AddErrorResult("Расчетный счет не валиден");

            if (!AgentRequisitesRegexReference.BikRegx.IsMatch(model.Bik))
                validateResults.AddErrorResult("БИК не валиден");

            if (string.IsNullOrEmpty(model.BankName))
                validateResults.AddErrorResult("Наименование банка не валидено");

            if (!AgentRequisitesRegexReference.CorrespondentAccountRegx.IsMatch(model.CorrespondentAccount))
                validateResults.AddErrorResult("Корреспондентский счет не валиден");

            if (string.IsNullOrEmpty(model.AddressForSendingDocuments))
                validateResults.AddErrorResult("Адрес для отправки документов не валиден");

            if (needToValidateFiles && (model.Files == null || !model.Files.Any()))
                validateResults.AddErrorResult("Отсутсвуют прикрепленные файлы реквизитов");

            return validateResults.ProcessValidateResults();
        }
    }
}
