﻿using Clouds42.Common.DataValidations;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Validators.Partner.Dictionary;
using Clouds42.Validators.Partner.Helpers;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Validators.Partner.Validators
{
    /// <summary>
    /// Валидатор модели реквизитов для ИП
    /// </summary>
    public static class SoleProprietorRequisitesDtoValidator
    {
        /// <summary>
        /// Выполнить валидацию модели реквизитов для ИП
        /// </summary>
        /// <param name="requisites">Модель реквизитов ИП</param>
        /// <returns>Результат валидации</returns>
        public static ValidateResult Validate(this SoleProprietorRequisitesDto requisites)
        {
            if (requisites == null)
                return ValidateResultHelper.CreateErrorResult("В модели нет реквизитов");

            var validateResults = new List<ValidateResult>();
            var phoneValidator = new PhoneNumberValidation(requisites.PhoneNumber);

            if (!AgentRequisitesRegexReference.FullNameRegx.IsMatch(requisites.FullName))
                validateResults.AddErrorResult("Поле ФИО не валидно");

            if (string.IsNullOrEmpty(requisites.LegalAddress))
                validateResults.AddErrorResult("Юридический адрес обязательное поле");

            if (string.IsNullOrEmpty(requisites.PhoneNumber) || !phoneValidator.PhoneNumberMaskIsValid())
                validateResults.AddErrorResult("Номер телефона не валиден");

            if (!AgentRequisitesRegexReference.OgrnForSoleProprietorRegx.IsMatch(requisites.Ogrn))
                validateResults.AddErrorResult("ОГРН не валиден");

            if (!AgentRequisitesRegexReference.InnRegx.IsMatch(requisites.Inn))
                validateResults.AddErrorResult("ИНН не валиден");

            var basePersonRequisitesDtoValidateResult = requisites.ValidateBasePersonRequisitesDto(false);

            if (!basePersonRequisitesDtoValidateResult.Success)
                validateResults.AddErrorResult(basePersonRequisitesDtoValidateResult.Message);

            return validateResults.ProcessValidateResults();
        }
    }
}
