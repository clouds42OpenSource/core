﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Validators.DelemiterSourceAccountDatabase.Validators
{
    /// <summary>
    /// Валидатор модели материнской базы разделителей
    /// </summary>
    public class DelimiterSourceAccountDatabaseDtoValidator
    {
        private readonly IUnitOfWork _dbLayer;

        public DelimiterSourceAccountDatabaseDtoValidator(IUnitOfWork dbLayer)
        {
            dbLayer.CheckForNull();
            _dbLayer = dbLayer;
        }

        /// <summary>
        /// Выполнить валидацию
        /// </summary>
        /// <param name="delimiterSourceAccountDatabaseDto">Модель материнской базы разделителей</param>
        /// <param name="isEditMode">Режим редактирования</param>
        public void Validate(DelimiterSourceAccountDatabaseDto delimiterSourceAccountDatabaseDto,
            bool isEditMode = false)
        {
            delimiterSourceAccountDatabaseDto.CheckForNull();

            var delimiterSourceAccountDatabase =
                _dbLayer.DelimiterSourceAccountDatabaseRepository.FirstOrDefault(dsdb =>
                    dsdb.AccountDatabaseId == delimiterSourceAccountDatabaseDto.AccountDatabaseId &&
                    dsdb.DbTemplateDelimiterCode == delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode);

            if (delimiterSourceAccountDatabase != null && !isEditMode)
                throw new ArgumentException("Такая материнская база уже существует.");

            var accountDatabase =
                _dbLayer.DatabasesRepository.FirstOrDefault(db =>
                    db.Id == delimiterSourceAccountDatabaseDto.AccountDatabaseId) ??
                throw new NotFoundException(
                    $"Не удалось найти базу по Id = {delimiterSourceAccountDatabaseDto.AccountDatabaseId}");

            var dbTemlate = _dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(dbt =>
                dbt.ConfigurationId == delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode);

            if (dbTemlate == null)
                throw new NotFoundException($"Не удалось найти конфигурацию '{delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode}' разделителей.");

            if (accountDatabase.IsFile.HasValue && accountDatabase.IsFile.Value)
                throw new ArgumentException($"Информационная база {accountDatabase.V82Name} является файловой.");

            if (accountDatabase.StateEnum != DatabaseState.Ready)
                throw new ArgumentException($"Информационная база {accountDatabase.V82Name} должна быть в статусе {DatabaseState.Ready}");

            if (accountDatabase.AccountDatabaseOnDelimiter != null)
                throw new ArgumentException($"Информационная база {accountDatabase.V82Name} является базой на разделителях.");

            if (string.IsNullOrEmpty(delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress))
                throw new ArgumentNullException($"Не указан адрес публикации для инф. базы {accountDatabase.V82Name}");
        }
    }
}
