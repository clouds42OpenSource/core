﻿namespace Clouds42.Validators.Models
{
    /// <summary>
    /// Модель результата валидации
    /// </summary>
    public class ValidateResultModel
    {
        /// <summary>
        /// Успешность
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
