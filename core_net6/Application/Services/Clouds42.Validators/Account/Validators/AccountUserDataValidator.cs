﻿using Clouds42.Common.Constants;
using Clouds42.Common.DataModels;
using Clouds42.Common.DataValidations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Account.Helpers;

namespace Clouds42.Validators.Account.Validators
{
    /// <summary>
    /// Валидатор данных аккаунта и пользователей
    /// </summary>
    public class AccountUserDataValidator(
        IUnitOfWork dbLayer,
        CreateNewUserAvailableLoginValidator createNewUserAvailableLoginValidator,
        ILogger42 logger)
    {
        /// <summary>
        /// Провалидировать данные регистрации пользователя.
        /// </summary>
        /// <param name="accountInfoModel">Данные регистрации.</param>
        /// <returns>Список ошибок валидации.</returns>
        public async Task<string[]> ValidateAccountUserDataAsync(AccountUserRegistrationModelDto accountInfoModel)
        {
            var errorList = new List<string>();
            if (accountInfoModel == null)
            {
                errorList.Add("Введенная модель при создании пользователя пустая!");
                return errorList.ToArray();
            }

            var userLoginValidationResult = ValidateAccountUserLogin(accountInfoModel);
            if (!userLoginValidationResult.DataIsValid)
                errorList.AddRange(userLoginValidationResult.ValidationMessages);

            var passwordValidationResult = ValidateAccountUserPassword(accountInfoModel);
            if (!passwordValidationResult.DataIsValid)
                errorList.AddRange(passwordValidationResult.ValidationMessages);

            var emailValidationResult = ValidateAccountUserEmail(accountInfoModel);
            if (!emailValidationResult.DataIsValid)
                errorList.AddRange(emailValidationResult.ValidationMessages);

            var phoneNumberValidationResult = await ValidateAccountUserPhoneNumber(accountInfoModel);
            if (!phoneNumberValidationResult.DataIsValid)
                errorList.AddRange(phoneNumberValidationResult.ValidationMessages);

            return errorList.ToArray();
        }

        /// <summary>
        /// Провалидировать данные регистрации аккаунта.
        /// </summary>
        /// <param name="accountInfoModel">Данные регистрации.</param>
        /// <returns>Список ошибок валидации.</returns>
        public async Task<string[]> ValidateAccountData(AccountRegistrationModelDto accountInfoModel)
        {
            var errorList = new List<string>();
            if (accountInfoModel == null)
            {
                errorList.Add("Введенная модель при создании пользователя пустая!");
                return errorList.ToArray();
            }

            var userLoginValidationResult = ValidateAccountUserLogin(accountInfoModel);
            if (!userLoginValidationResult.DataIsValid)
                errorList.AddRange(userLoginValidationResult.ValidationMessages);

            var passwordValidationResult = ValidateAccountUserPassword(accountInfoModel);
            if (!passwordValidationResult.DataIsValid)
                errorList.AddRange(passwordValidationResult.ValidationMessages);

            var emailValidationResult = ValidateAccountUserEmail(accountInfoModel);
            if (!emailValidationResult.DataIsValid)
                errorList.AddRange(emailValidationResult.ValidationMessages);

            var phoneNumberValidationResult = await ValidateAccountUserPhoneNumber(accountInfoModel);
            if (!phoneNumberValidationResult.DataIsValid)
                errorList.AddRange(phoneNumberValidationResult.ValidationMessages);

            var accCaptionValidationResult = ValidateAccountCaption(accountInfoModel);
            if (!accCaptionValidationResult.DataIsValid)
                errorList.AddRange(accCaptionValidationResult.ValidationMessages);

            logger.Trace("Начало проверки возможности активации сервиса");

            if (accountInfoModel.LocaleName != LocaleConst.Ukraine ||
                accountInfoModel.RegistrationConfig?.ReferralAccountId != null ||
                accountInfoModel.RegistrationConfig?.CloudServiceId == null)
                return errorList.ToArray();

            var service = dbLayer.BillingServiceRepository.FirstOrDefaultAsync(s => s.Id == accountInfoModel.RegistrationConfig.CloudServiceId).Result;
            errorList.Add(
                $"Использование сервиса “{service?.Name}” доступно в конфигурациях 1С для Российской Федерации." +
                $"Для получения консультации о возможности использовать сервис “{service?.Name}” обратитесь, пожалуйста, на круглосуточную линию технической поддержки.");

            return errorList.ToArray();
        }

        /// <summary>
        /// Проверить логин пользователя на соответствие требованиям
        /// </summary>
        /// <param name="accountInfoModel">Данные регистрации</param>
        /// <returns>Результат проверки</returns>
        private AccountUserDataValidationResultDto ValidateAccountUserLogin(IRegistrationSourceInfoAdapter accountInfoModel)
        {
            logger.Trace("Начало проверки Логина");
            logger.Trace($"Логин: {accountInfoModel.Login} ");

            if (string.IsNullOrEmpty(accountInfoModel.Login))
                return new AccountUserDataValidationResultDto
                {
                    DataIsValid = true
                };

            var loginValidator = new LoginValidation(accountInfoModel.Login);
            var loginValidationResult = loginValidator.LoginIsValid();

            var loginIsAvailable = createNewUserAvailableLoginValidator.LoginIsAvailable(accountInfoModel.Login);
            if (loginIsAvailable)
                return loginValidationResult;

            loginValidationResult.ValidationMessages.Add("Пользователь с таким логином уже зарегистрирован в системе!");
            loginValidationResult.DataIsValid = false;

            return loginValidationResult;
        }

        /// <summary>
        /// Проверить пароль пользователя на соответствие требованиям
        /// </summary>
        /// <param name="accountInfoModel">Данные регистрации</param>
        /// <returns>Результат проверки</returns>
        private AccountUserDataValidationResultDto ValidateAccountUserPassword(IRegistrationSourceInfoAdapter accountInfoModel)
        {
            logger.Trace("Начало проверки пароля");

            if (string.IsNullOrEmpty(accountInfoModel.Password))
                return new AccountUserDataValidationResultDto
                {
                    DataIsValid = true
                };

            var passwordValidator = new PasswordValidation(accountInfoModel.Password);
            return passwordValidator.PasswordIsValid();
        }

        /// <summary>
        /// Проверить почту пользователя на соответствие требованиям
        /// </summary>
        /// <param name="accountInfoModel">Данные регистрации</param>
        /// <returns>Результат проверки</returns>
        private AccountUserDataValidationResultDto ValidateAccountUserEmail(IRegistrationSourceInfoAdapter accountInfoModel)
        {
            logger.Trace("Начало проверки Email");
            var validationResult = new AccountUserDataValidationResultDto();
            if(string.IsNullOrEmpty(accountInfoModel.Email) && string.IsNullOrEmpty(accountInfoModel.FullPhoneNumber))
            {
                validationResult.ValidationMessages.Add("Не заполнены поля почты и телефона. Заполните что-то одно!");
                return validationResult;
            }

            if(string.IsNullOrEmpty(accountInfoModel.Email))
                return validationResult;

            var emailValidator = new EmailValidation(accountInfoModel.Email);
            if (!emailValidator.EmailMaskIsValid())
                validationResult.ValidationMessages.Add("Почта содержит недопустимые символы");
            if (!emailValidator.EmailIsAvailable(dbLayer))
                validationResult.ValidationMessages.Add("Пользователь с такой почтой уже зарегистрирован в системе!");

            validationResult.DataIsValid = !validationResult.ValidationMessages.Any();

            return validationResult;
        }

        /// <summary>
        /// Проверить номер телефона пользователя на соответствие требованиям
        /// </summary>
        /// <param name="accountInfoModel">Данные регистрации</param>
        /// <returns>Результат проверки</returns>
        private async Task<AccountUserDataValidationResultDto> ValidateAccountUserPhoneNumber(IRegistrationSourceInfoAdapter accountInfoModel)
        {
            logger.Trace("Начало проверки номера телефона на валидность");
            logger.Trace($"Номер телефона: {accountInfoModel.FullPhoneNumber}");

            var validationResult = new AccountUserDataValidationResultDto();

            if (string.IsNullOrEmpty(accountInfoModel.FullPhoneNumber))
            {
                validationResult.DataIsValid = true;

                return validationResult;
            }

            var phoneValidator = new PhoneNumberValidation(accountInfoModel.FullPhoneNumber);

            if (!phoneValidator.PhoneNumberMaskIsValid())
                validationResult.ValidationMessages.Add("Номер телефона не валиден");

            if (!await phoneValidator.PhoneNumberIsAvailable(dbLayer))
                validationResult.ValidationMessages.Add("Пользователь с таким номером телефона уже зарегистрирован в системе!");

            validationResult.DataIsValid = !validationResult.ValidationMessages.Any();

            return validationResult;
        }

        /// <summary>
        /// Проверить подпись аккаунта на соответствие требованиям
        /// </summary>
        /// <param name="accountInfoModel">Данные регистрации</param>
        /// <returns>Результат проверки</returns>
        private AccountUserDataValidationResultDto ValidateAccountCaption(AccountRegistrationModelDto accountInfoModel)
        {
            logger.Trace("Начало проверки подписи пользователя");
            var accCaptionValidator = new AccountCaptionValidation(accountInfoModel.AccountCaption);
            return accCaptionValidator.CaptionIsValid();
        }
    }
}
