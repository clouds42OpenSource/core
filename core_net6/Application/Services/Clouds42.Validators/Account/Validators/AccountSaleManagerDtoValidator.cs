﻿using Clouds42.DataContracts.Account;

namespace Clouds42.Validators.Account.Validators
{
    /// <summary>
    /// Валидатор модели менеджера прикрепленного к аккаунту
    /// </summary>
    public static class AccountSaleManagerDtoValidator
    {
        /// <summary>
        /// Выполнить валидацию
        /// </summary>
        /// <param name="accountSaleManager">Модель менеджера прикрепленного к аккаунту</param>
        public static void Validate(AccountSaleManagerDto accountSaleManager)
        {
            if (accountSaleManager == null)
                throw new ArgumentException("Тело запроса отсутствует или заполнено некорректно.");

            if (string.IsNullOrEmpty(accountSaleManager.Division))
                throw new ArgumentException("Поле Отдел обязательно к заполнению.");
        }
    }
}
