﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.DataValidations;
using Clouds42.Common.Helpers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Validators.Account.Helpers
{
    /// <summary>
    /// Расишрения для валидации номера телефона
    /// </summary>
    public static class PhoneNumberValidationExtensions
    {
        /// <summary>
        /// Признак что номер телефона доступен
        /// </summary>
        /// <param name="validator">Экземпляр валидатора</param>
        /// <param name="unitOfWork"></param>
        /// <returns>Результат проверки</returns>
        public static async Task<bool> PhoneNumberIsAvailable(this PhoneNumberValidation validator, IUnitOfWork unitOfWork)
        {
            var phoneNumber = PhoneHelper.ClearNonDigits(validator.PhoneNumber);

            return !await unitOfWork.AccountUsersRepository
                .AsQueryableNoTracking()
                .AnyAsync(x => x.PhoneNumber == phoneNumber && 
                          x.IsPhoneVerified.HasValue && 
                          x.IsPhoneVerified.Value && 
                          (x.CorpUserSyncStatus != "SyncDeleted" || x.CorpUserSyncStatus != "Deleted"));
        }
    }
}
