﻿using Clouds42.AccountUsers.Contracts.AccountUser.Models;
using Clouds42.Common.Constants;
using Clouds42.Common.DataValidations;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Validators.Account.Helpers
{
    public class AccountUserModelValidator(
        CreateNewUserAvailableLoginValidator createNewUserAvailableLoginValidator,
        IUnitOfWork dbLayer,
        IConfiguration configuration)
    {
        /// <summary>
        /// Check user model before creating
        /// </summary>
        /// <returns></returns>
        public async Task<(bool, string)> IsValidForCreateNewAsync(AccountUserViewModel model, Guid contextAccountId)
        {
            if (!IsValidCommon(model, out string commonErrorMessage))
            {
                return (false, commonErrorMessage);
            }

            if (!createNewUserAvailableLoginValidator.LoginIsAvailable(model.Login))
            {
                return (false, "User with specified login already exists");
            }

            if (string.IsNullOrEmpty(model.Password))
            {
                return (false, "There is no password");
            }

            var emailValidation = new EmailValidation(model.Email);
            if (!emailValidation.EmailIsAvailable(dbLayer))
            {
                return (false, "User with specified email already exists");
            }

            if (!string.IsNullOrWhiteSpace(model.PhoneNumber))
            {
                var locale = (await dbLayer.AccountsRepository.GetLocaleAsync(contextAccountId, 
                    Guid.Parse(configuration["DefaultLocale"] ?? ""))).Name;

                var phoneNumberValidation = new PhoneNumberValidation(model.PhoneNumber);

                if (!phoneNumberValidation.PhoneNumberMaskIsValid(locale, out string phoneErrorMessage))
                {
                    return (false, phoneErrorMessage);
                }
                if (!await phoneNumberValidation.PhoneNumberIsAvailable(dbLayer))
                {
                    return (false, "User with specified phone number already exists");
                }
            }

            var passwordValidator = new PasswordValidation(model.Password);
            var passwordValidationResult = passwordValidator.PasswordIsValid();
            if (!passwordValidationResult.DataIsValid)
            {
                return (false, string.Join(" ", passwordValidationResult.ValidationMessages));
            }

            return model.Password == model.ConfirmPassword ? (true, "") : (false, "Password and confirmation must match");
        }

        /// <summary>
        /// Check password and login for validity
        /// </summary>
        /// <returns></returns>
        private bool IsValidCommon(AccountUserViewModel model, out string errorMessage)
        {
            errorMessage = "";
            var loginValidation = new LoginValidation(model.Login);

            if (string.IsNullOrEmpty(model.Login))
            {
                errorMessage = "Login not specified";
                return false;
            }

            if (!loginValidation.LoginSymbolsIsValid(out var validationMessage))
            {
                errorMessage = validationMessage;
                return false;
            }
            if (!loginValidation.LoginLengthIsValid())
            {
                errorMessage = $"Allowed number of characters in the login : {AccountUserConstants.MinLoginLength}–{AccountUserConstants.MaxLoginLength}";
                return false;
            }

            if (string.IsNullOrEmpty(model.Email))
            {
                errorMessage = "Invalid value for email";
                return false;
            }
 
            var emailValidation = new EmailValidation(model.Email);

            if (emailValidation.EmailMaskIsValid()) return true;

            errorMessage = "Invalid value for email";
            return false;
        }
    }
}
