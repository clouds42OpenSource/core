﻿using Clouds42.Common.DataValidations;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Validators.Account.Helpers
{
    /// <summary>
    /// Валидатор почты
    /// </summary>
    public static class EmailValidationExtensions
    {
        /// <summary>
        /// Признак что введенный адрес эл. почты доступен
        /// </summary>
        /// <param name="validator">Экземпляр валидатора</param>
        /// <param name="unitOfWork"></param>
        /// <param name="accountUserLogin">Логин пользователя</param>
        /// <returns>Результат проверки</returns>
        public static bool EmailIsAvailable(this EmailValidation validator, IUnitOfWork unitOfWork, string? accountUserLogin = null)
        {
            var accountUser = unitOfWork.AccountUsersRepository.GetByEmail(validator.Email);

            return accountUser == null || (!string.IsNullOrEmpty(accountUserLogin) && accountUser.Login == accountUserLogin);
        }
    }
}
