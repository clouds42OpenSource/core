﻿using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Validators.Account.Helpers
{
    /// <summary>
    ///   Валидатор доступности введенного логина при регистрации
    /// </summary>
    public class CreateNewUserAvailableLoginValidator(IUnitOfWork dbLayer)
    {
        /// <summary>
        ///     Проверка доступности логина
        /// </summary>
        /// <param name="login">введенный логин</param>
        /// <returns>признак доступности логина</returns>
        public bool LoginIsAvailable(string? login)
        {
            var accountUser = dbLayer.AccountUsersRepository.GetAccountUserByLogin(login);

            var res1 = accountUser == null || accountUser.CorpUserSyncStatus is "Deleted" or "SyncDeleted";

            return res1;
        }

    }
}
