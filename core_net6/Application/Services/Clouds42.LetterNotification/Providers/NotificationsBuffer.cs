﻿using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Буфер уведомлений
    /// </summary>
    public class NotificationsBuffer(IUserNotification notifyEvent, IUnitOfWork unitOfWork)
    {
        /// <summary>
        /// Проверка, отправлено ли сообщение в заданном периоде (с заданным ключем)
        /// </summary>
        /// <returns>true - если отправлено уже достаочное количество уведомлений</returns>
        public bool NotifyEventIsBuffered()
        {
            var bufferItem = FindBufferItem(unitOfWork);

            if (bufferItem == null) return false;

            return bufferItem.Counter >= notifyEvent.GetNotifyCount();
        }


        /// <summary>
        /// Зафиксировать уведомление в буфере
        /// </summary>
        public void BufferNotifyEvent()
        {
            var bufferItem = FindBufferItem(unitOfWork);
            if (bufferItem == null)
            {
                unitOfWork.NotificationBufferRepository.Insert(new NotificationBuffer
                {
                    Account = notifyEvent.AccountUser.AccountId,
                    ActualPeriod = notifyEvent.GetEventKeyPeriod(),
                    Counter = 1,
                    Id = Guid.NewGuid(),
                    NotifyKey = notifyEvent.GetEventKey(),
                });
            }
            else
            {
                bufferItem.Counter += 1;
                unitOfWork.NotificationBufferRepository.Update(bufferItem);
            }

            unitOfWork.Save();
        }


        /// <summary>
        /// Найти элемент в буфере
        /// </summary>
        /// <param name="dbLayer">Экземпляр UnitOfWork</param>
        /// <returns>Элемент в буфере</returns>
        private NotificationBuffer FindBufferItem(IUnitOfWork dbLayer)
        {
            var key = notifyEvent.GetEventKey();

            var buffered =
                dbLayer.NotificationBufferRepository.FirstOrDefault(
                    buff =>
                        buff.Account == notifyEvent.AccountUser.AccountId
                        && buff.NotifyKey == key
                        && buff.ActualPeriod >= DateTime.Now);

            return buffered;
        }
    }
}