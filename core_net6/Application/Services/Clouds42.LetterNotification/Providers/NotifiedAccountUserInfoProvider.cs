﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Провайдер для работы с информацией об уведомляемом пользователе
    /// </summary>
    internal class NotifiedAccountUserInfoProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : INotifiedAccountUserInfoProvider
    {
        /// <summary>
        /// Попытаться получить информацию об уведомляемом пользователе
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="notifiedAccountUserInfo">Информацию об уведомляемом пользователе</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="emailsToCopy">Электронные адреса в копию</param>
        /// <returns>Результат попытки получения</returns>
        public bool TryGet(out NotifiedAccountUserInfoDto notifiedAccountUserInfo, Guid accountId,
            Guid? accountUserId = null, List<string>? emailsToCopy = null)
        {
            notifiedAccountUserInfo = new NotifiedAccountUserInfoDto();

            try
            {
                var accountLocale = accountConfigurationDataProvider.GetAccountLocale(accountId);

                if (!TryGetAccountAdmin(accountId, accountUserId, out var accountAdmins))
                    return false;

                var accountAdmin = accountAdmins.First();
                if (emailsToCopy != null && emailsToCopy.Any())
                    emailsToCopy = emailsToCopy.Where(email => email != accountAdmin.Email)
                        .Distinct()
                        .ToList();

                notifiedAccountUserInfo = new NotifiedAccountUserInfoDto
                {
                    Id = accountAdmin.Id,
                    AccountId = accountId,
                    Email = accountAdmin.Email,
                    Login = accountAdmin.Login,
                    LocaleName = accountLocale.Name,
                    LocaleCurrency = accountLocale.Currency,
                    IsUnsubscribed = accountAdmin.Unsubscribed,
                    EmailsToCopy = emailsToCopy,
                    EmailStatus = accountAdmin.EmailStatus,
                    UserIdsToSend = accountAdmins.Select(x => x.Id).ToList()
                };

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Попытаться получить админа аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="accountAdmins">Админы аккаунта</param>
        /// <returns>Результат поиска админа аккаунта</returns>
        private bool TryGetAccountAdmin(Guid accountId, Guid? accountUserId, out List<AccountUser> accountAdmins)
        {
            accountAdmins = [];

            var accountAdminIds = !accountUserId.IsNullOrEmpty() && accountId != Guid.Empty
                ? [accountUserId!.Value]
                : accessProvider.GetAccountAdmins(accountId);

            if (!accountAdminIds.Any())
                return false;

            accountAdmins = dbLayer.AccountUsersRepository.Where(a => accountAdminIds.Contains(a.Id)).ToList();

            return accountAdmins.Any();
        }
    }
}
