﻿using System.Net;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Contracts;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Провайдер для работы с SendGrid
    /// </summary>
    public class SendGridProvider : ISendGridProvider
    {
        /// <summary>
        /// Отправить письмо
        /// </summary>
        /// <param name="commandExecuteRequestPayload">Полезная нагрузка для выполнения команды</param>
        public ManagerResult SendEmail(CommandExecuteRequestPayloadDto commandExecuteRequestPayload)
        { 
            try
            {
                SendEmailWithTemplate(commandExecuteRequestPayload).Wait();
                return CreateManagerResult(true);
            }
            catch (Exception ex)
            {
                return CreateManagerResult(false, ex.Message);
            }
        }

        public async Task<ManagerResult> SendEmailAsync(CommandExecuteRequestPayloadDto commandExecuteRequestPayload)
        {
            await SendEmailWithTemplate(commandExecuteRequestPayload);

            return CreateManagerResult(true);
        }

        /// <summary>
        /// Отправить письмо асинхронно
        /// </summary>
        /// <param name="commandExecuteRequestPayload">Полезная нагрузка для выполнения команды</param>
        private static async Task SendEmailWithTemplate(CommandExecuteRequestPayloadDto commandExecuteRequestPayload)
        {
            var message = CreateSendGridMessage(commandExecuteRequestPayload);
            var requestHeaders = CreateRequestHeaders(commandExecuteRequestPayload.EmailRequestSettings);

            var client = new SendGridClient(commandExecuteRequestPayload.EmailRequestSettings.SendGridApiKey, requestHeaders: requestHeaders);
            var response = await client.SendEmailAsync(message);
            var responseBody = await response.Body.ReadAsStringAsync();
            if (response.StatusCode != HttpStatusCode.Accepted)
                throw new InvalidOperationException(responseBody);
        }

        /// <summary>
        /// Создать письмо SendGrid
        /// </summary>
        /// <param name="commandExecuteRequestPayload">Полезная нагрузка для выполнения команды</param>
        /// <returns>Письмо SendGrid</returns>
        private static SendGridMessage CreateSendGridMessage(CommandExecuteRequestPayloadDto commandExecuteRequestPayload)
        {
            var emailRequestSettings = commandExecuteRequestPayload.EmailRequestSettings;

            var message = new SendGridMessage();
            var from = new EmailAddress(emailRequestSettings.EmailSender.Email,
                emailRequestSettings.EmailSender.PersonName);

            if (emailRequestSettings.EmailReceivers != null && emailRequestSettings.EmailReceivers.Any())
            {
                var tos = emailRequestSettings.EmailReceivers.Select(x => new EmailAddress(x.Email, x.PersonName))
                    .ToList();

                message = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos,
                    null, null, null);

                message.Categories = emailRequestSettings.Categories;
                message.TemplateId = emailRequestSettings.SendGridTemplateId;
                message.SetFrom(from);
                message.Personalizations ??= [];
                
                foreach (var to in emailRequestSettings.EmailReceivers)
                {
                    commandExecuteRequestPayload.LetterTemplateCommonData.CompanyDetails.UnsubscribeLink = new Uri(
                            $"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/{CloudConfigurationProvider.Cp.GetRouteValueForUnsubscribe()}{SimpleIdHash.GetHashById(to.Id!.Value)}")
                        .AbsoluteUri;

                    var existsPersonalization = message.Personalizations.FirstOrDefault(x =>
                        x.Tos.Any(z => z.Email == to.Email && z.Name == to.PersonName));

                    if (existsPersonalization == null)
                    {
                        message.Personalizations.Add(new Personalization
                        {
                            Tos = [tos.First(x => x.Email == to.Email && x.Name == to.PersonName)],
                            Subject = commandExecuteRequestPayload.LetterTemplateCommonData.LetterTemplateData.Subject,
                            TemplateData = commandExecuteRequestPayload.LetterTemplateCommonData
                        });
                    }

                    else
                    {
                        existsPersonalization.Subject = commandExecuteRequestPayload.LetterTemplateCommonData
                            .LetterTemplateData.Subject;
                        existsPersonalization.TemplateData = commandExecuteRequestPayload.LetterTemplateCommonData;
                    }
                }
            }

            else
            {
                message.SetFrom(from);
                message.Categories = emailRequestSettings.Categories;
                message.TemplateId = emailRequestSettings.SendGridTemplateId;

                message.AddTo(new EmailAddress(emailRequestSettings.EmailReceiver.Email,
                    emailRequestSettings.EmailReceiver.PersonName));

                message.SetTemplateData(commandExecuteRequestPayload.LetterTemplateCommonData);
                AddAttachments(message, emailRequestSettings);

                if (emailRequestSettings.CopyToEmailAddresses == null || !emailRequestSettings.CopyToEmailAddresses.Any())
                {
                    return message;
                }

                if (emailRequestSettings.IsBccRecipients)
                    message.AddBccs(emailRequestSettings.CopyToEmailAddresses.Select(info => new EmailAddress(info.Email, info.PersonName)).ToList());
                else
                    message.AddCcs(emailRequestSettings.CopyToEmailAddresses.Select(info => new EmailAddress(info.Email, info.PersonName)).ToList());
            }
            return message;
        }

        /// <summary>
        /// Добавить вложения к письму
        /// </summary>
        /// <param name="sendGridMessage">Модель письма SendGrid</param>
        /// <param name="emailRequestSettings">Настройки запроса на отправку письма</param>
        private static void AddAttachments(SendGridMessage sendGridMessage, EmailRequestSettingsDto emailRequestSettings)
        {
            if (emailRequestSettings.Attachments == null || !emailRequestSettings.Attachments.Any())
                return;

            emailRequestSettings.Attachments.ForEach(attachment =>
            {
                sendGridMessage.AddAttachment(new Attachment
                {
                    Content = Convert.ToBase64String(attachment.Bytes),
                    Filename = attachment.FileName
                });
            });
        }

        /// <summary>
        /// Создать заголовки запроса
        /// </summary>
        /// <param name="emailRequestSettings">Настройки запроса на отправку письма</param>
        /// <returns>Заголовки запроса</returns>
        private static Dictionary<string, string> CreateRequestHeaders(EmailRequestSettingsDto emailRequestSettings)
            => new()
            {
                { SendGridCustomHeadersKeysConst.AgentUIdHeaderKey, emailRequestSettings.SendGridAgentUniqueId.ToString()},
                { SendGridCustomHeadersKeysConst.AgentNameHeaderKey, emailRequestSettings.SendGridAgentName}
            };

        private static ManagerResult CreateManagerResult(bool isSuccess, string? message = null)
            => new()
            {
                Message = message,
                State = isSuccess ? ManagerResultState.Ok : ManagerResultState.PreconditionFailed
            };
    }
}
