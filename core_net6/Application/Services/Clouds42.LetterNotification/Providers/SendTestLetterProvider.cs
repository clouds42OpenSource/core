﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Configurations.Configurations.MailConfigurations;
using Clouds42.DataContracts.Service.SendGrid;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.Helpers;
using Clouds42.LetterNotification.SendGrid.EmailClient;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Провайдер для отправки тестового письма
    /// </summary>
    internal class SendTestLetterProvider(
        LetterTemplateTestInstanceCreator letterTemplateTestInstanceCreator,
        ILetterTemplateProvider letterTemplateProvider,
        IAccessProvider accessProvider,
        ISendGridEmailClient sendGridEmailClient)
        : ISendTestLetterProvider
    {
        private readonly Lazy<string> _sendGridApiKey = new(CloudConfigurationProvider.SendGrid.GetApiKey);
        private readonly Lazy<string> _sendGridAgentName = new(CloudConfigurationProvider.SendGrid.GetAgentName);
        private readonly Lazy<Guid> _sendGridAgentUniqueId = new(CloudConfigurationProvider.SendGrid.GetAgentUniqueId);
        private readonly Lazy<string> _managerMailLogin = new(MailConfiguration.Efsol.Credentials.Manager.GetLogin);
        private readonly Lazy<string> _senderDescription = new(CloudConfigurationProvider.LetterNotification.GetSenderDescription);
        private readonly Lazy<string> _testLetterCategoryName = new(CloudConfigurationProvider.LetterNotification.GetTestLetterCategoryName);

        /// <summary>
        /// Отправить тестовое письмо
        /// </summary>
        /// <param name="sendTestLetterDto">Модель данных для отправки тестового письма</param>
        public void Send(SendTestLetterDto sendTestLetterDto)
        {
            var letterTemplate =
                letterTemplateProvider.GetLetterTemplate(sendTestLetterDto.LetterTemplateDataForNotification.Id);

            var letterInstance =
                letterTemplateTestInstanceCreator.CreateInstance(letterTemplate.LetterAssemblyClassName);

            var letterTemplateCommonData =
                letterInstance?.GetCommonLetterTemplateTestData(accessProvider.ContextAccountId,
                    sendTestLetterDto.LetterTemplateDataForNotification);

            var emailRequestSettings = CreateEmailRequestSettings(sendTestLetterDto);
            
            sendGridEmailClient.SendEmailViaNewThread(emailRequestSettings, letterTemplateCommonData);
        }

        /// <summary>
        /// Создать настройки запроса для отправки письма
        /// </summary>
        /// <param name="sendTestLetterDto">Модель данных для отправки тестового письма</param>
        /// <returns>Настройки запроса для отправки письма</returns>
        private EmailRequestSettingsDto CreateEmailRequestSettings(SendTestLetterDto sendTestLetterDto)
        {
            var emailRequestSettings = new EmailRequestSettingsDto
            {
                EmailReceiver = new EmailAddressInfoDto(sendTestLetterDto.Email),
                EmailSender = new EmailAddressInfoDto(_managerMailLogin.Value, _senderDescription.Value),
                Categories = [_testLetterCategoryName.Value],
                SendGridApiKey = _sendGridApiKey.Value,
                SendGridTemplateId = sendTestLetterDto.LetterTemplateDataForNotification.SendGridTemplateId,
                SendGridAgentName = _sendGridAgentName.Value,
                SendGridAgentUniqueId = _sendGridAgentUniqueId.Value,
            };

            return emailRequestSettings;
        }
    }
}
