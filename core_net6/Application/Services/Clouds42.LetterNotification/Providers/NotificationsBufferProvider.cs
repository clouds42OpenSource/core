﻿using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Провайдер для работы с буффером уведомлений
    /// </summary>
    public class NotificationsBufferProvider(IUnitOfWork dbLayer) : INotificationsBufferProvider
    {
        /// <summary>
        /// Проверить является ли уведомление буфферизованным
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="eventKey">Ключ уведомления</param>
        /// <param name="allowedNotificationsInPeriodCount">
        /// Допустимое количество уведомлений за период</param>
        /// <returns>Уведомление буфферизованно</returns>
        public bool IsBufferedNotifyEvent(Guid accountId, string eventKey, int allowedNotificationsInPeriodCount)
        {
            var buffer = GetNotificationBuffer(accountId, eventKey);

            if (buffer == null) return false;

            return buffer.Counter >= allowedNotificationsInPeriodCount;
        }

        /// <summary>
        /// Записать уведомление в буффер
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="eventKey">Ключ уведомления</param>
        /// <param name="eventKeyPeriod">
        /// Дата, до которой это уведомление будет актуальным</param>
        public void WriteNotificationToBuffer(Guid accountId, string eventKey, DateTime eventKeyPeriod)
        {
            var buffer = GetNotificationBuffer(accountId, eventKey);

            if (buffer != null)
            {
                IncreaseBufferNotificationCounter(buffer);
                return;
            }

            CreateNotificationBuffer(accountId, eventKey, eventKeyPeriod);
        }

        /// <summary>
        /// Создать буффер уведомлений
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="eventKey">Ключ уведомления</param>
        /// <param name="eventKeyPeriod">
        /// Дата, до которой это уведомление будет актуальным</param>
        private void CreateNotificationBuffer(Guid accountId, string eventKey, DateTime eventKeyPeriod)
        {
            dbLayer.NotificationBufferRepository.Insert(new NotificationBuffer
            {
                Account = accountId,
                ActualPeriod = eventKeyPeriod,
                Counter = 1,
                Id = Guid.NewGuid(),
                NotifyKey = eventKey,
            });

            dbLayer.Save();
        }

        /// <summary>
        /// Увеличить счетчик уведомления буффера
        /// </summary>
        /// <param name="notificationBuffer">Буффер уведомлений</param>
        private void IncreaseBufferNotificationCounter(NotificationBuffer notificationBuffer)
        {
            notificationBuffer.Counter += 1;
            dbLayer.NotificationBufferRepository.Update(notificationBuffer);
            dbLayer.Save();
        }

        /// <summary>
        /// Получить буффер уведомления
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="eventKey">Ключ уведомления</param>
        /// <returns>Буффер уведомлений</returns>
        private NotificationBuffer GetNotificationBuffer(Guid accountId, string eventKey) =>
            dbLayer.NotificationBufferRepository.FirstOrDefault(
                buff =>
                    buff.Account == accountId
                    && buff.NotifyKey == eventKey
                    && buff.ActualPeriod >= DateTime.Now);
    }
}