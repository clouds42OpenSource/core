﻿using Clouds42.LetterNotification.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Провайдер для работы с данными электронных аресов
    /// </summary>
    internal class EmailAddressesDataProvider(IUnitOfWork dbLayer) : IEmailAddressesDataProvider
    {
        /// <summary>
        /// Получить все электронные адреса аккаунта для отправки
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Все электронные адреса аккаунта для отправки</returns>
        public List<string> GetAllAccountEmailsToSend(Guid accountId)
        {
            var accountAdminIds = dbLayer.AccountsRepository.GetAccountAdminIds(accountId);
            var accountEmails = dbLayer.AccountEmailRepository.WhereLazy(au => au.AccountId == accountId)
                .Select(au => au.Email);

            return (from accountUser in dbLayer.AccountUsersRepository.WhereLazy()
                join accountAdminId in accountAdminIds on accountUser.Id equals accountAdminId
                where accountUser.Email != null
                select accountUser.Email).Union(accountEmails).Distinct().ToList();

        }

        /// <summary>
        /// Получить все электронные адреса аккаунта для отправки, в виде строки
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Все электронные адреса аккаунта для отправки, в виде строки</returns>
        public string GetAllAccountEmailsToSendAsString(Guid accountId) =>
            string.Join(", ", GetAllAccountEmailsToSend(accountId));
    }
}
