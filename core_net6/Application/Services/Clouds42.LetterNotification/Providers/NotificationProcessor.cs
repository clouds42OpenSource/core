﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.Helpers;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Обработчик пользовательский уведомлений
    /// </summary>
    public class NotificationProcessor : INotificationProcessor
    {
        private readonly IAccessProvider _accessProvider;
        private readonly IUnitOfWork _dbLayer;
        private readonly IConfiguration _configuration;
        private readonly IAccountConfigurationDataProvider _accountConfProvider;
        private readonly ILogger42 _logger;
        private readonly bool _useNotifycations;
        private readonly bool _sendEmail;
        private readonly IHandlerException _handlerException;
        public NotificationProcessor(
            IAccessProvider accessProvider,
            IUnitOfWork dbLayer,
            ILogger42 logger,
            IConfiguration configuration,
            IAccountConfigurationDataProvider accountConfProvider, IHandlerException handlerException)
        {
            _accessProvider = accessProvider;
            _dbLayer = dbLayer;
            _accountConfProvider = accountConfProvider;
            _logger = logger;
            _configuration = configuration;
            _useNotifycations = _configuration.GetValue("Notifications.Enabled", false);
            _handlerException = handlerException;
            var sendEmailKey = Configurations.Configurations.CloudConfigurationProvider.CoreWorker.GetNotificationsUseEmail();
            if (!string.IsNullOrEmpty(sendEmailKey))
            {
                _sendEmail = sendEmailKey.ToLower() == "true";
            }
        }

        /// <summary>
        /// Уведомить о скорой блокировке сервиса.
        /// </summary>        
        public void NotifyBeforeLockService(BeforeLockInServicefoModelDto beforeLockInServicefoModel)
        {
            Notify(beforeLockInServicefoModel.AccountId,
                (accountUser, locale) =>
                    new BeforeServiceLockNotification(locale, beforeLockInServicefoModel, accountUser));
        }

        /// <summary>
        /// Надо ли слать уведомленние.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns></returns>
        private AccountUser? GetAccountAdmin(Guid accountId)
        {
            var accountAdminId = _accessProvider.GetAccountAdmins(accountId).FirstOrDefault();

            if (accountAdminId == Guid.Empty)
                return null;

            if (!_useNotifycations)
                return null;

            var account = _dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == accountId);

            return account == null ? null : _dbLayer.AccountUsersRepository.FirstOrDefault(a => a.Id == accountAdminId);
        }

        /// <summary>
        /// Уведомить пользователя.
        /// </summary>       
        private void Notify(Guid accountId,
            Func<AccountUser, Locale, IUserNotification> createNotificationContentAction)
        {
            var locale = _dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(_configuration["DefaultLocale"] ?? ""));

            var accountAdmin = GetAccountAdmin(accountId);

            var emailsCopies = _dbLayer.AccountEmailRepository
                .WhereLazy(w => w.AccountId == accountId).Select(w => w.Email).ToList();
            
            Notify(createNotificationContentAction(accountAdmin, locale), emailsCopies);
        }

        /// <summary>
        /// Уведомить пользователя.
        /// </summary>
        /// <param name="notification">Уведомление</param>
        /// <param name="emailsCopies">Дополнительные email-ы для отправки</param>
        private void Notify(IUserNotification notification, List<string> emailsCopies)
        {
            // проверка буфера отправленных сообщений
            var notificationsBuffer = new NotificationsBuffer(notification, _dbLayer);
            if (notificationsBuffer.NotifyEventIsBuffered()) return;

            try
            {
                // формирование тела письма
                var content = new EmailContentBuilder(_accountConfProvider, notification.GetEmailBody())
                    .AddFooterEfsol(notification.AccountUser.Id, notification.AccountUser.Unsubscribed).Build();

                var subject = notification.GetEmailTopic();

                //  отправить
                var emailProvider =
                    new Manager42CloudsMail()
                        .DisplayName(notification.GetFromLable())
                        .To(notification.AccountUser.Email)
                        .Body(content)
                        .Subject(subject);

                var logMessage = $"Письмо \"{subject}\" отправлено на адрес: {notification.AccountUser.Email}";

                if (emailsCopies.Count > 0)
                {
                    emailsCopies.ForEach(email =>
                    {
                        emailProvider.Copy(email);
                    });

                    logMessage =
                        $"Письмо \"{subject}\" отправлено на адреса: {notification.AccountUser.Email}, {string.Join(", ", emailsCopies)}";
                }

                var attachment = notification.GetAttachmentFile();
                if (attachment != null)
                {
                    emailProvider.Attachment(attachment.Bytes, attachment.FileName);
                }

                if (_sendEmail)
                {
                    emailProvider.SendViaNewThread();
                    LogEventHelper.LogEvent(_dbLayer, notification.AccountUser.AccountId, _accessProvider, LogActions.MailNotification, logMessage);
                }

                // запись в буфер 
                notificationsBuffer.BufferNotifyEvent();

                _logger.Trace("Уведомление о событии {0}, login: {1} обработано", notification.GetEventKey(), notification.AccountUser.Login);
            }

            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка уведомления о событии] {notification.GetEventKey()} ");
            }
        }
    }
}
