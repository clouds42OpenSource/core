﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.DataModels.Mailing;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Провайдер для работы с шаблоном письма
    /// </summary>
    internal class LetterTemplateProvider(
        IUnitOfWork dbLayer,
        IAdvertisingBannerTemplateProvider advertisingBannerTemplateProvider)
        : ILetterTemplateProvider
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="id">Id шаблона письма</param>
        /// <returns>Шаблон письма</returns>
        public LetterTemplate GetLetterTemplate(int id) =>
            dbLayer.GetGenericRepository<LetterTemplate>().FirstOrDefault(lt => lt.Id == id) ??
            throw new NotFoundException($"Не удалось получить шаблон письма по Id {id}");

        /// <summary>
        /// Получить модель Dto шаблона письма
        /// </summary>
        /// <param name="id">Id шаблона письма</param>
        /// <returns>Модель Dto шаблона письма</returns>
        public LetterTemplateDetailsDto GetLetterTemplateDto(int id)
        {
            var letterTemplate = GetLetterTemplate(id);

            var letterTemplateDto = new LetterTemplateDetailsDto
            {
                Id = letterTemplate.Id,
                Header = letterTemplate.Header,
                Body = letterTemplate.Body,
                Subject = letterTemplate.Subject,
                Category = letterTemplate.LetterTypeName,
                SendGridTemplateId = letterTemplate.SendGridTemplateId,
                AdvertisingBannerTemplatesShortInfo =
                    advertisingBannerTemplateProvider.GetAdvertisingBannerTemplatesShortInfo()
            };

            if (TryGetAdvertisingBannerTemplate(id, out var advertisingBannerTemplate))
                letterTemplateDto.AdvertisingBannerTemplateId = advertisingBannerTemplate?.Id;

            return letterTemplateDto;
        }


        /// <summary>
        /// Попытаться получить рекламный баннер по Id шаблона письма
        /// </summary>
        /// <param name="letterTemlateId">Id шаблона письма</param>
        /// <param name="advertisingBannerTemplate">Рекламный баннер</param>
        /// <returns>Результат получения рекламного баннера</returns>
        public bool TryGetAdvertisingBannerTemplate(int letterTemlateId,
            out AdvertisingBannerTemplate? advertisingBannerTemplate)
        {
            advertisingBannerTemplate = dbLayer
                .GetGenericRepository<LetterAdvertisingBannerRelation>()
                .FirstOrDefault(relation => relation.LetterTemplateId == letterTemlateId)?.AdvertisingBannerTemplate ;

            return advertisingBannerTemplate != null;
        }
    }
}
