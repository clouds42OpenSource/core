﻿using System.Text;
using Clouds42.Configurations.Configurations;
using Clouds42.Configurations.Configurations.MailConfigurations;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.Helpers.Footers;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Уведомление пользователя о скорой блокировке сервиса
    /// </summary>
    public class BeforeServiceLockNotification(
        Locale locale,
        BeforeLockInServicefoModelDto beforeLockInServicefoModel,
        AccountUser accountUser)
        : IUserNotification
    {
        private readonly StringBuilder _body = new();

        /// <summary>
        /// Тема сообщение уведомления
        /// </summary>
        public string GetEmailTopic()
        {
            const string topic = "Ваши финансы";
            return topic;
        }

        /// <summary>
        /// Ключ уникальности события - для избежания дублей
        /// </summary>   
        public string GetEventKey()
        {
            return $"BeforeServiceLock=>ServiceName={beforeLockInServicefoModel.BillingServiceName}";
        }

        /// <summary>
        /// До какой даты это уведомление будет актуальным (в случае о предупреждениях об оплате)
        /// </summary>
        public DateTime GetEventKeyPeriod()
        {
            return beforeLockInServicefoModel.LockDate;
        }

        /// <summary>
        /// Сколько раз сообщать о событии в данном периоде
        /// </summary>
        public int GetNotifyCount()
        {
            return 1;
        }

        /// <summary>
        /// Уведомляемый пользователь.
        /// </summary>
        public AccountUser AccountUser { get; } = accountUser;

        /// <summary>
        /// Текст сообщения уведомления
        /// </summary>  
        public string GetEmailBody()
        {

            _body.Append("<table style=\"margin: 0pt; padding: 0pt; border-collapse: collapse; vertical-align: top; text-align: left; height: 100% !important; width: 100% !important; background: url('https://42clouds.com/{5}/') 0% 50% repeat scroll #f7f6f4;\" bgcolor=\"#f7f6f4\">");
            _body.Append("<tbody>");
            _body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            _body.Append("<td align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\">");
            _body.Append("<table style=\"padding: 0pt; width: 100%; border-collapse: collapse; vertical-align: top; text-align: left;\">");
            _body.Append("<tbody>");
            _body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            _body.Append("<td height=\"50\" align=\"left\" style=\"padding: 0pt; height: 50px; vertical-align: top; text-align: left; border-collapse: collapse !important;\">&nbsp;</td>");
            _body.Append("</tr>");
            _body.Append("</tbody>");
            _body.Append("</table>");
            _body.Append("<center>");
            _body.Append("<table style=\"border: 1px solid #e2e2e2; padding: 0pt; border-collapse: collapse; vertical-align: top; text-align: left; width: 705px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; height: 200px; background: none 0% 50% repeat scroll #ffffff;\" bgcolor=\"#ffffff\">");
            _body.Append("<tbody>");
            _body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            _body.Append("<td align=\"left\" style=\"padding: 0pt; border-collapse: collapse !important; vertical-align: top; text-align: left;\">");
            _body.Append("<table bgcolor=\"#fed048\" style=\"padding: 0pt; width: 100%; border-collapse: collapse; vertical-align: top; text-align: left; height: 97px; background: #fed048 0% 50%;\">");
            _body.Append("<tbody>");
            _body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            _body.Append($"<td height=\"40\" align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\"><img style=\"margin-top: 20px;\" src=\"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/Content/img/upload/42logo.png\" alt=\"\" width=\"142\" height=\"48\" />&nbsp;</td>");
            _body.Append("<td height=\"40\" align=\"right\" style=\"padding: 0pt; font-size: 22px; white-space: nowrap; text-align: right; vertical-align: middle; border-collapse: collapse !important;\">");
            _body.Append("<p style=\"font-size: 22px; text-align: left;\">Напоминание</p>");
            _body.Append("</td>");
            _body.Append("<td align=\"left\" style=\"padding: 0pt; border-collapse: collapse !important; vertical-align: top; text-align: left;\">");
            _body.Append("<p>&nbsp;</p>");
            _body.Append("<p>&nbsp;</p>");
            _body.Append("</td>");
            _body.Append("</tr>");
            _body.Append("</tbody>");
            _body.Append("</table>");
            _body.Append("</td>");
            _body.Append("</tr>");
            _body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            _body.Append("<td align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\">");
            _body.Append("<table style=\"height: 211px;\" width=\"700\">");
            _body.Append("<tbody>");
            _body.Append("<tr>");
            _body.Append("<td height=\"40\" align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important; width: 40px;\">&nbsp;</td>");
            _body.Append("<td>");
            _body.Append($"<p>Уважаемый пользователь 42Clouds, {AccountUser.Name}!</p>");
            _body.Append("<p>Для продления работы сервиса {3}.</p>");
            _body.Append("<table style=\"height: 60px; width: 300px;\" width=\"211\">");
            _body.Append("<tbody>");
            _body.Append("<tr>");
            _body.Append("<td>Стоимость сервисов:</td>");
            _body.Append("<td style=\"text-align: right;\">{0:0.00}&nbsp;{5}</td>");
            _body.Append("</tr>");
            _body.Append("<tr>");
            _body.Append("<td>Текущий баланс:&nbsp;</td>");
            _body.Append("</tr>");
            _body.Append("<tr>");
            _body.Append("<td><span style=\"padding-left: 1pt;\"> &nbsp; основной счет:&nbsp;</span></td>");
            _body.Append("<td style=\"text-align: right;\">{1:0.00}&nbsp;{5}</td>");
            _body.Append("</tr>");
            _body.Append("<tr>");
            _body.Append("<td><span style=\"padding-left: 1pt;\"> &nbsp; бонусный счет:&nbsp;</span></td>");
            _body.Append("<td style=\"text-align: right;\">{6:0.00}&nbsp;{5}</td>");
            _body.Append("</tr>");
            _body.Append("<tr>");
            _body.Append("<td>Сервис будет недоступен с:&nbsp;</td>");
            _body.Append("<td style=\"text-align: right;\">{2}</td>");
            _body.Append("</tr>");
            _body.Append("</tbody>");
            _body.Append("</table>");
            if (beforeLockInServicefoModel.Invoice != null)
            {
                _body.Append("<p>Во вложении счет на оплату за период {8} мес на сумму {7:0.00}&nbsp;{5}. </p>");
            }
            _body.Append("<br />");
            _body.Append("<p>Благодарим за своевременную оплату!</p>");
            _body.Append("<p>Если у Вас возникнут какие-либо вопросы, обратитесь в службу поддержки: {9}</p>");
            _body.Append("</td>");
            _body.Append("<td align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important; width: 40px;\"></td>");
            _body.Append("</tr>");
            _body.Append("</tbody>");
            _body.Append("</table>");
            _body.Append("</td>");
            _body.Append("</tr>");
            _body.Append("</tbody>");
            _body.Append("</table>");
            _body.Append("</center>");
            _body.Append(MarketFooterGenerator.GenerateFooter(MarketServiceEnum.InformerBarcode, locale));

            var invoicePeriod =
                beforeLockInServicefoModel.InvoicePeriod == null || beforeLockInServicefoModel.InvoicePeriod == (int)PayPeriod.None
                    ? (int)PayPeriod.Month1
                    : beforeLockInServicefoModel.InvoicePeriod.Value;

            var monthlyCost = beforeLockInServicefoModel.Cost / invoicePeriod;

            var managerEmail = MailConfiguration.Efsol.Credentials.Manager.GetLogin();

            return string.Format(_body.ToString(), monthlyCost, beforeLockInServicefoModel.AccountBalance,
                beforeLockInServicefoModel.LockDate.ToString("dd.MM.yyyy"), beforeLockInServicefoModel.ReasonText,
                locale.Name, locale.Currency, beforeLockInServicefoModel.AccountBonusBalance,
                beforeLockInServicefoModel.Cost, invoicePeriod, managerEmail);
        }

        /// <summary>
        /// Получить приложение
        /// </summary>
        public DocumentDataDto? GetAttachmentFile()
        {
            if (beforeLockInServicefoModel.Invoice == null)
                return null;

            return new DocumentDataDto
            {
                FileName = CloudConfigurationProvider.Invoices.GetInvoiceAttachFileName(),
                Bytes = beforeLockInServicefoModel.Invoice.InvoiceDocBytes
            };
        }

        /// <summary>
        /// Получить описание отправителя
        /// </summary>  
        public string GetFromLable()
        {
            return CloudConfigurationProvider.CoreWorker.Get42CloudCommandSender();
        }
    }
}
