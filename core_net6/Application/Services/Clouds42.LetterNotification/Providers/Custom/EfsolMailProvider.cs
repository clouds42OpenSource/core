﻿using Clouds42.Configurations.Configurations.MailConfigurations;
using Clouds42.LetterNotification.Contracts.Mail;

namespace Clouds42.LetterNotification.Providers.Custom
{
    /// <summary>
    ///     Почтовый провайдер бренда Efsol
    /// </summary>
    public class EfsolMailProvider : BaseMailProvider
    {
        /// <summary>
        ///     Порт
        /// </summary>
        protected override int Port => MailConfiguration.Efsol.GetPort();

        /// <summary>
        ///     Хост
        /// </summary>
        protected override string Host => MailConfiguration.Efsol.GetHost();
    }
}
