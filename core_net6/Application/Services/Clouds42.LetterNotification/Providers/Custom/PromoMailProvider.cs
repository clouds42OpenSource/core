﻿using Clouds42.Configurations.Configurations.MailConfigurations;
using Clouds42.LetterNotification.Contracts.Mail;

namespace Clouds42.LetterNotification.Providers.Custom
{
    /// <summary>
    ///     Почтовый провайдер брэнда Promo
    /// </summary>
    public class PromoMailProvider : BaseMailProvider
    {
        /// <summary>
        ///     Порт
        /// </summary>
        protected override int Port => MailConfiguration.Promo.GetPort();

        /// <summary>
        ///     Хост
        /// </summary>
        protected override string Host => MailConfiguration.Promo.GetHost();
    }
}
