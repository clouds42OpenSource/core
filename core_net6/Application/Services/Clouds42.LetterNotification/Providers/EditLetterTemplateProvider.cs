﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.DataModels.Mailing;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Providers
{
    /// <summary>
    /// Провайдер для редактирования шаблона письма
    /// </summary>
    internal class EditLetterTemplateProvider(
        ILetterTemplateProvider letterTemplateProvider,
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IEditLetterTemplateProvider
    {
        /// <summary>
        /// Редактировать шаблон письма
        /// </summary>
        /// <param name="editLetterTemplateDto">Модель редактирования шаблона письма</param>
        public void Edit(EditLetterTemplateDto editLetterTemplateDto)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var letterTemplate = letterTemplateProvider.GetLetterTemplate(editLetterTemplateDto.Id);

                UpdateAdvertisingBannerTemplate(editLetterTemplateDto);
                letterTemplate.Header = editLetterTemplateDto.Header;
                letterTemplate.Body = editLetterTemplateDto.Body;
                letterTemplate.SendGridTemplateId = editLetterTemplateDto.SendGridTemplateId;
                letterTemplate.Subject = editLetterTemplateDto.Subject;
                letterTemplate.NotificationText = editLetterTemplateDto.NotificationText;

                dbLayer.GetGenericRepository<LetterTemplate>().Update(letterTemplate);
                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка редактирования шаблона письма письма] {editLetterTemplateDto.Id}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Обновить шаблон рекламного баннера для пиьма
        /// </summary>
        /// <param name="editLetterTemplateDto">Модель редактирования шаблона письма</param>
        private void UpdateAdvertisingBannerTemplate(EditLetterTemplateDto editLetterTemplateDto)
        {
            var letterAdvertisingBannerRelation = dbLayer.GetGenericRepository<LetterAdvertisingBannerRelation>()
                .FirstOrDefault(relation => relation.LetterTemplateId == editLetterTemplateDto.Id);

            if (editLetterTemplateDto.AdvertisingBannerTemplateId ==
                letterAdvertisingBannerRelation?.AdvertisingBannerTemplateId)
                return;

            DeleteLetterAdvertisingBannerRelation(letterAdvertisingBannerRelation);

            if (!editLetterTemplateDto.AdvertisingBannerTemplateId.HasValue)
                return;

            CreateLetterAdvertisingBannerRelation(editLetterTemplateDto);

            LogEventHelper.LogEvent(dbLayer, accessProvider.ContextAccountId, accessProvider,
                LogActions.SetAdvertisingBannerTemplateInLetter,
                $@"Рекламный баннер “{GetAdvertisingBannerHeader(editLetterTemplateDto.AdvertisingBannerTemplateId.Value)}” 
                        установлен в письме “{editLetterTemplateDto.Subject}”.");
        }

        /// <summary>
        /// Получить заголовок рекламного баннера
        /// </summary>
        /// <param name="bannerId">Id рекламного баннера</param>
        /// <returns>Заголовок рекламного баннера</returns>
        private string GetAdvertisingBannerHeader(int bannerId) =>
            dbLayer.GetGenericRepository<AdvertisingBannerTemplate>()
                .FirstOrDefault(banner => banner.Id == bannerId)?.Header ??
            throw new NotFoundException($"Не удалось получить рекламный баннер {bannerId}");

        /// <summary>
        /// Удалить связь шаблона письма с рекламным баннером
        /// </summary>
        /// <param name="letterAdvertisingBannerRelation">Связь шаблона письма с рекламным баннером</param>
        private void DeleteLetterAdvertisingBannerRelation(
            LetterAdvertisingBannerRelation? letterAdvertisingBannerRelation)
        {
            if (letterAdvertisingBannerRelation == null)
                return;

            dbLayer.GetGenericRepository<LetterAdvertisingBannerRelation>().Delete(letterAdvertisingBannerRelation);
            dbLayer.Save();
        }

        /// <summary>
        /// Создать связь шаблона письма с рекламным баннером
        /// </summary>
        /// <param name="editLetterTemplateDto">Модель редактирования шаблона письма</param>
        private void CreateLetterAdvertisingBannerRelation(EditLetterTemplateDto editLetterTemplateDto)
        {
            if (!editLetterTemplateDto.AdvertisingBannerTemplateId.HasValue)
                return;

            dbLayer.GetGenericRepository<LetterAdvertisingBannerRelation>().Insert(
                new LetterAdvertisingBannerRelation
                {
                    LetterTemplateId = editLetterTemplateDto.Id,
                    AdvertisingBannerTemplateId = editLetterTemplateDto.AdvertisingBannerTemplateId.Value
                });

            dbLayer.Save();
        }
    }
}
