﻿using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;

namespace Clouds42.LetterNotification.Getters
{
    /// <summary>
    /// Класс для получения контактной информации о компании,
    /// для указания в письме 
    /// </summary>
    public static class CompanyContactInfoForLetterGetter
    {
        /// <summary>
        /// Cписок правил маппинга номера телефона к локали
        /// </summary>
        private static readonly List<(Func<string, bool> CompareLocaleName, Lazy<string> PhoneNumber)>
            ListRulesForMappPhoneToLocale =
            [
                (localName => localName == LocaleConst.Russia,
                    new Lazy<string>(CloudConfigurationProvider.CompanyDetails.GetPhoneRu)),

                (localName => localName == LocaleConst.Kazakhstan,
                    new Lazy<string>(CloudConfigurationProvider.CompanyDetails.GetPhoneKz)),

                (localName => localName == LocaleConst.Ukraine,
                    new Lazy<string>(CloudConfigurationProvider.CompanyDetails.GetPhoneUa))
            ];

        /// <summary>
        /// Ссылка на сайт облака
        /// </summary>
        private static readonly Lazy<string> SiteLink = new(CloudConfigurationProvider.CompanyDetails.GetSiteLink);

        /// <summary>
        /// Ссылка на сайт облака для укр. локали
        /// </summary>
        private static readonly Lazy<string> SiteLinkUa = new(CloudConfigurationProvider.CompanyDetails.GetSiteLinkUa);

        /// <summary>
        /// Электронная почта
        /// </summary>
        private static readonly Lazy<string> Email = new(CloudConfigurationProvider.CompanyDetails.GetEmail);

        /// <summary>
        /// Электронная почта для укр. локали
        /// </summary>
        private static readonly Lazy<string> EmailUa = new(CloudConfigurationProvider.CompanyDetails.GetEmailUa);

        /// <summary>
        /// Ссылка на телеграм
        /// </summary>
        private static readonly Lazy<string> TelegramLink =
            new(CloudConfigurationProvider.CompanyDetails.GetTelegramLink);

        /// <summary>
        /// Получить контактную информацию компании для письма
        /// по названию локали аккаунта
        /// </summary>
        /// <param name="localeName">Название локали аккаунта</param>
        /// <returns>Модель контактной информации компании для письма</returns>
        public static CompanyContactInfoForLetterDto Get(string localeName) =>
            new()
            {
                Phone = GetPhoneNumberByLocalName(localeName),
                Email = GetEmailByLocalName(localeName),
                SiteLink = GetSiteLink(localeName),
                TelegramLink = TelegramLink.Value,
                NeedShowSocialMediaBlock = localeName != LocaleConst.Ukraine
            };

        /// <summary>
        /// Получить эл. почту по названию локали
        /// </summary>
        /// <param name="localName">Название локали</param>
        /// <returns>Эл. почта</returns>
        private static string GetEmailByLocalName(string localName) =>
            localName == LocaleConst.Ukraine ? EmailUa.Value : Email.Value;

        /// <summary>
        /// Получить ссылку на сайт по названию локали
        /// </summary>
        /// <param name="localName">Название локали</param>
        /// <returns>Ссылка на сайта</returns>
        private static string GetSiteLink(string localName) =>
            localName == LocaleConst.Ukraine ? SiteLinkUa.Value : SiteLink.Value;

        /// <summary>
        /// Получить номер телефона по названию локали
        /// </summary>
        /// <param name="localName">Название локали</param>
        /// <returns>Номер телефона</returns>
        private static string GetPhoneNumberByLocalName(string localName) =>
            ListRulesForMappPhoneToLocale.FirstOrDefault(tuple => tuple.CompareLocaleName(localName)).PhoneNumber
                ?.Value ?? throw new NotFoundException(
                $"Не удалось получить номер телефона по названию локали {localName}");
    }
}
