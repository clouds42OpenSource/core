﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Helpers
{
    /// <summary>
    /// Хелпер для работы с письмом о регистрации пользователя
    /// </summary>
    public static class RegistrationUserPromoLetterHelper
    {
        /// <summary>
        /// Заменить базовые данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        public static string ReplaceBaseDataForCurrentHtmlLetterBody(this RegistrationUserLetterModelDto model,
            string htmlLetterBody) => htmlLetterBody
            .Replace(LetterKeysConst.AccountUserLogin, model.AccountUserLogin)
            .Replace(LetterKeysConst.AccountUserPwd, model.AccountUserPsd);

        /// <summary>
        /// Получить базовые тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о регистрации пользователя</returns>
        public static RegistrationUserLetterModelDto GetBaseTestDataToCurrentHtmlLetter()
            =>
                new()
                {
                    AccountUserLogin = "bigBoss",
                    AccountUserPsd = "Qwerty_123"
                };
    }
}