﻿using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Helpers
{
    /// <summary>
    /// Класс хелпер для работы с Html шаблоном письма
    /// </summary>
    public static class LetterHtmlTemplateHelper
    {
        /// <summary>
        /// Определить по списку атрибут display
        /// для Html
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="list">Список</param>
        /// <returns>Значение атрибута display</returns>
        public static string DefineByListHtmlDisplayValue<T>(this IEnumerable<T> list) =>
            !list.Any() ? LetterKeysConst.DisplayValueNone : string.Empty;

        /// <summary>
        /// Сформировать Html для списка элементов
        /// </summary>
        /// <typeparam name="T">Тип элемента</typeparam>
        /// <param name="enumerable">Перечисление</param>
        /// <param name="selector">Функция для выбора данных</param>
        /// <returns>Html для списка элементов</returns>
        public static string GenerateHtmlForListElements<T>(this IEnumerable<T> enumerable, Func<T, int, string> selector)
        {
            var list = enumerable.ToList();

            return list.Any()
                ? list.Select(selector).Aggregate((current, next) => current + " " + next)
                : string.Empty;
        }

        /// <summary>
        /// Преобразовать дату в строку(без времени) 
        /// </summary>
        /// <param name="dateTime">Дата</param>
        /// <returns>Строковое значение даты</returns>
        public static string ConvertToDateString(this DateTime dateTime) => $"{dateTime:dd.MM.yyyy}";

        /// <summary>
        /// Сформировать Html для ячейки таблицы
        /// </summary>
        /// <param name="value">Значение ячейки</param>
        /// <returns>Html ячейки таблицы</returns>
        public static string GenerateHtmlForTableCell(object value) =>
            $@"<td style=""border: 1px solid #ddd; padding: 8px;"">
            <span style=""font-size: 9pt; font-family: Arial; color: rgb(0, 0, 0); white-space: pre-wrap;"">{value}</span><br>
            </td>";
    }
}