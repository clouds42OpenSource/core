﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Helpers
{
    /// <summary>
    /// Хелпер для работы с письмом о скором изменении сервиса
    /// </summary>
    public static class BeforeChangeServiceLetterHelper
    {
        /// <summary>
        /// Заменить базовые данные для тела Html письма
        /// об изменениях в сервисе
        /// </summary>
        /// <param name="model">Базовая модель письма о скором изменении сервиса</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML тела письма</returns>
        public static string ReplaceBaseDataForHtmlLetterBody(this BeforeChangeServiceBaseLetterModelDto model,
            string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.ServiceName, model.ServiceName)
                .Replace(LetterKeysConst.UrlForOpenBillingServiceCard, model.UrlForOpenBillingServiceCard)
                .Replace(LetterKeysConst.ServiceOwnerEmail, model.ServiceOwnerEmail)
                .Replace(LetterKeysConst.ServiceOwnerPhoneNumber, model.ServiceOwnerPhoneNumber)
                .Replace(LetterKeysConst.TextOnServiceOwnerPhoneNumberDisplayValue,
                    GetDisplayValueForTextOnServiceOwnerPhoneNumber(model.ServiceOwnerPhoneNumber));

        /// <summary>
        /// Получить значение атрибута display
        /// для текста - номер телефона владельца сервиса
        /// </summary>
        /// <param name="phoneNumber">Номер телефона</param>
        /// <returns>Значение атрибута display</returns>
        public static string GetDisplayValueForTextOnServiceOwnerPhoneNumber(string phoneNumber) =>
            phoneNumber.IsNullOrEmpty()
                ? LetterKeysConst.DisplayValueNone
                : string.Empty;
    }
}
