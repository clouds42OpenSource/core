﻿using Clouds42.DataContracts.Navigation;

namespace Clouds42.LetterNotification.Helpers.Footers
{
    /// <summary>
    /// Создатель footer'a русской локали
    /// </summary>
    public class FooterCreatorUA : FooterCreator
    {

        /// <summary>
        /// Получить footer украинской локали
        /// </summary>
        /// <returns></returns>
        public override FooterDto GetLocalFooter()
        {
            return new FooterUADto();
        }
    }
}
