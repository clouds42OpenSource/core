﻿using Clouds42.DataContracts.Navigation;

namespace Clouds42.LetterNotification.Helpers.Footers;

/// <summary>
/// Создатель footer'a узбекской локали
/// </summary>
public class FooterCreatorUZ : FooterCreator
{

    /// <summary>
    /// Получить footer узбекской локали
    /// </summary>
    /// <returns></returns>
    public override FooterDto GetLocalFooter()
    {
        return new FooterUZDto();
    }
}
