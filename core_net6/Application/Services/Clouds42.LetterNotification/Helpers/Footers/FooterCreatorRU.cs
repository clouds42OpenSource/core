﻿using Clouds42.DataContracts.Navigation;

namespace Clouds42.LetterNotification.Helpers.Footers
{

    /// <summary>
    /// Создатель footer'a русской локали
    /// </summary>
    public class FooterCreatorRU : FooterCreator
    {

        /// <summary>
        /// Получить footer русской локали
        /// </summary>
        /// <returns></returns>
        public override FooterDto GetLocalFooter()
        {
            return new FooterRUDto();
        }
    }
}
