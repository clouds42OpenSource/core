﻿using Clouds42.DataContracts.Navigation;

namespace Clouds42.LetterNotification.Helpers.Footers
{
    /// <summary>
    /// Создатель footer'a казахской локали
    /// </summary>
    public class FooterCreatorKZ : FooterCreator
    {

        /// <summary>
        /// Получить footer казахской локали
        /// </summary>
        /// <returns></returns>
        public override FooterDto GetLocalFooter()
        {
            return new FooterKZDto();
        }
    }
}
