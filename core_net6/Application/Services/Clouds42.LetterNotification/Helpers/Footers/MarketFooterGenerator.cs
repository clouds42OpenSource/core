﻿using System.Text;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.LetterNotification.Helpers.Footers
{
    /// <summary>
    /// Генератор footer с рекламой магазина сервисов
    /// </summary>
    public static class MarketFooterGenerator
    {
        private static readonly List<(Func<MarketServiceEnum, bool>, Func<string>)> GenerateFooterFuncList =
        [
            (marketServiceEnum => marketServiceEnum == MarketServiceEnum.InformerBarcode,
                GenerateInformerBarcodeServiceFooter),
            (marketServiceEnum => marketServiceEnum == MarketServiceEnum.DataControl, GenerateDataControlServiceFooter),
            (marketServiceEnum => marketServiceEnum == MarketServiceEnum.Delivery, GenerateDeliveryServiceFooter),
            (marketServiceEnum => marketServiceEnum == MarketServiceEnum.ChatBots, GenerateChatBotsServiceFooter)
        ];

        /// <summary>
        /// Сгенерировать footer для магазина серверисов
        /// </summary>
        /// <param name="marketServiceEnum">Сервис маркет42</param>
        /// <param name="locale">Локаль</param>
        /// <returns>HTML</returns>
        public static string GenerateFooter(MarketServiceEnum marketServiceEnum, Locale locale)
        {
            if (locale == null || locale.Name != LocaleConst.Russia)
                return string.Empty;

            var generateFooterFunc = GenerateFooterFuncList.FirstOrDefault(l => l.Item1(marketServiceEnum)).Item2 ??
                                     throw new NotFoundException(
                                         $"Не удалось получить фоотер для сервиса {marketServiceEnum}");

            return generateFooterFunc();
        }

        /// <summary>
        /// Сгенерировать footer для сервиса "Штрихкод-информер"
        /// </summary>
        /// <returns>HTML</returns>
        private static string GenerateInformerBarcodeServiceFooter() => GetMarketFooter(
            "Штрихкод-информер в вашем смартфоне!",
            "Сбор заказов, инвентаризация, проверка ценников, просмотр полной информации об остатках и ценах — со смартфона онлайн.",
            "Footer_One.png",
            CloudConfigurationProvider.Market.GetUrlForInformerBarcodeService());


        /// <summary>
        /// Сгенерировать footer для сервиса "Контроль ввода данных в 1С"
        /// </summary>
        /// <returns>HTML</returns>
        private static string GenerateDataControlServiceFooter() => GetMarketFooter(
            "Контроль ввода данных в 1С и доступа к ним!",
            "Сервис помогает разграничить права доступа внутри 1С:Бухгалтерии и исключает ошибки при работе с документами и справочниками.",
            "Footer_Two.png",
            CloudConfigurationProvider.Market.GetUrlForDataControlService());

        /// <summary>
        /// Сгенерировать footer для сервиса "30 служб доставки"
        /// </summary>
        /// <returns>HTML</returns>
        private static string GenerateDeliveryServiceFooter() => GetMarketFooter(
            "30 служб доставки в вашей 1С!",
            @"Экономьте деньги на доставке. Интегрируйте 1С с BoxBerry, СДЭК, DPD и еще 27 курьерскими службами. 
                    Сравнивайте цены курьерских служб и оформляйте доставку прямо из 1С.",
            "Footer_Three.png",
            CloudConfigurationProvider.Market.GetUrlForDeliveryService());

        /// <summary>
        /// Сгенерировать footer для сервиса "Чат-боты"
        /// </summary>
        /// <returns>HTML</returns>
        private static string GenerateChatBotsServiceFooter() => GetMarketFooter(
            "1С чат-боты",
            @"Новый уровень вашей техподдержки и продаж. В 2 клика интегрируйте 1С с Telegram, Viber, ВКонтакте, Skype, Одноклассники, Яндекс.Алиса, WhatsApp.",
            "Footer_Four.png",
            CloudConfigurationProvider.Market.GetUrlForChatBotsService());

        /// <summary>
        /// Получить footer для магазина серверисов
        /// </summary>
        /// <param name="title">Заголовок</param>
        /// <param name="message">Сообщение</param>
        /// <param name="imageName">Название картинки</param>
        /// <param name="url">Ссылка</param>
        /// <returns>HTML</returns>
        private static string GetMarketFooter(string title, string message, string imageName, string url)
        {
            var imageLink = $"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/Content/img/Market/{imageName}";

            var builder = new StringBuilder();
            builder.Append("<center>");
            builder.Append($"<table style=\"padding: 0px; border-collapse: collapse; text-align: left; width: 700px; height: 198px; background-image: url('{imageLink}'); background-position: center;  background-repeat: no-repeat; background-size: contain; vertical-align: top; font-family: Arial, Helvetica, sans-serif; font-size: 15px;\"> ");
            builder.Append("<tbody>");
            builder.Append("<tr style=\"padding: 0pt; text-align: left; vertical-align: top;\" align=\"left\">");
            builder.Append("<td height=\"30\" align=\"left\" style=\"padding: 0pt; text-align: left; vertical-align: top; border-collapse: collapse !important; width: 40px; height: 30px;\">&nbsp;</td>");
            builder.Append("<td height=\"30\" align=\"left\" style=\"padding: 0pt; text-align: left; vertical-align: top; border-collapse: collapse !important; height: 30px;\">&nbsp;</td>");
            builder.Append("<td height=\"30\" align=\"left\" style=\"padding: 0pt; text-align: left; vertical-align: top; border-collapse: collapse !important; width: 40px; height: 30px;\">&nbsp;</td>");
            builder.Append("</tr>");
            builder.Append("<tr style=\"padding: 0pt; text-align: left; vertical-align: top;\" align=\"left\">");
            builder.Append("<td height=\"30\" align=\"left\" style=\"padding: 0pt; text-align: left; vertical-align: top; border-collapse: collapse !important; width: 40px; height: 30px;\">&nbsp;</td>");
            builder.Append("<td align=\"left\" style=\"padding: 0pt; text-align: left; vertical-align: top; border-collapse: collapse !important;\">");
            builder.Append("<table style=\"height: 117px; font-size: 13px; font-family: Arial, Helvetica, sans-serif;\" width=\"614\">");
            builder.Append("<tbody>");
            builder.Append("<tr>");
            builder.Append("<td>");
            builder.Append($"<h1 style=\"font-weight: bold; font-size: 18px; margin-left: 12px;\">{title}</h1>");
            builder.Append($"<p style=\"margin-left: 12px;\">{message}</p>");
            builder.Append($"<p style=\"text-align: right;\"><a style=\"color: #0f6fb3; text-decoration: none; text-align: right; margin-right: 20px; font-weight: bold;\" href=\"{url}\">Узнать подробнее &nbsp;&gt;&gt;</a></p>");
            builder.Append("</td>");
            builder.Append("</tr>");
            builder.Append("</tbody>");
            builder.Append("</table>");
            builder.Append("</td>");
            builder.Append("<td height=\"30\" align=\"left\" style=\"padding: 0pt; text-align: left; vertical-align: top; border-collapse: collapse !important; width: 40px; height: 30px;\">&nbsp;</td>");
            builder.Append("</tr>");
            builder.Append("<tr style=\"padding: 0pt; text-align: left; vertical-align: top;\" align=\"left\">");
            builder.Append("<td height=\"30\" align=\"left\" style=\"padding: 0pt; text-align: left; vertical-align: top; border-collapse: collapse !important;\">&nbsp;</td>");
            builder.Append("<td></td>");
            builder.Append("<td></td>");
            builder.Append("</tr>");
            builder.Append("</tbody>");
            builder.Append("</table>");
            builder.Append("</center>");

            return builder.ToString();
        }
    }
}
