﻿using Clouds42.DataContracts.Navigation;

namespace Clouds42.LetterNotification.Helpers.Footers
{
    /// <summary>
    /// Создаёт footer требуемой локали
    /// </summary>
    public abstract class FooterCreator
    {

        /// <summary>
        /// Создаёт экземпляр класса Footer требуемой локали 
        /// </summary>
        /// <returns></returns>
        public abstract FooterDto GetLocalFooter();
    }
}
