﻿using Clouds42.Domain.DataModels;

namespace Clouds42.LetterNotification.Helpers
{
    /// <summary>
    /// Хелпер генерации заголовка письма
    /// </summary>
    public static class GenerateMailTitleHelper
    {
        /// <summary>
        /// Попытаться получить полное имя пользователя
        /// </summary>
        /// <param name="accountUser">Модель пользоваетля аккаунта</param>
        /// <param name="fullName">Полное имя пользователя</param>
        /// <returns>Результат получения полного имени пользователя</returns>
        public static bool TryGetFullUserName(this AccountUser accountUser, out string fullName)
        {
            fullName = ($"{accountUser.LastName} {accountUser.FirstName} {accountUser.MiddleName}").Trim();
            return !string.IsNullOrEmpty(fullName);
        }
    }
}
