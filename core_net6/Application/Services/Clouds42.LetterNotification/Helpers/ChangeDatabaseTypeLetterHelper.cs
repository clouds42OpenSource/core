﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Helpers
{
    /// <summary>
    /// Хелпер для письма об изменении типа инф. базы
    /// </summary>
    public static class ChangeDatabaseTypeLetterHelper
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма об изменении типа информационной базы(серверная/файловая)</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        public static string ReplaceDataForCurrentHtmlLetterBody(this ChangeDatabaseTypeLetterModelDto model,
            string htmlLetterBody) => htmlLetterBody
            .Replace(LetterKeysConst.AccountInfo, model.AccountInfo)
            .Replace(LetterKeysConst.DatabaseInfo, model.DatabaseInfo)
            .Replace(LetterKeysConst.AutoUpdate, "руб")
            .Replace(LetterKeysConst.LocaleCurrency, "руб")
            .Replace(LetterKeysConst.AccountDatabaseCaption, model.DatabaseInfo)
            .Replace(LetterKeysConst.DisplayValueNone, "none")
            .Replace(LetterKeysConst.InvoiceNumber, "12345678")
            .Replace(LetterKeysConst.InvoiceSum, "100")
            .Replace(LetterKeysConst.AccountDatabasePublishPath, "путь до базы")
            .Replace(LetterKeysConst.AcDbPublishPathDisplayValue, "ссылка до базы")
            .Replace(LetterKeysConst.ServiceName, "Сервис для проверки")
            .Replace(LetterKeysConst.ModerationResult, "Проверка завершена успешно")
            .Replace(LetterKeysConst.ModeratorComment, "Комментарий")
            .Replace(LetterKeysConst.UrlForOpenBillingServiceCard, "Ссылка сервиса")
            .Replace(LetterKeysConst.TextOnEditServiceRequestDisplayValue, "текст")
            .Replace(LetterKeysConst.BalancePageUrl, "ссылка на страницу баланса")
            .Replace(LetterKeysConst.UrlForAccountDatabasesPage, "ссылка на страницу баз")
            .Replace(LetterKeysConst.LifetimeAccountDataInTombDaysCount, "900")
            .Replace(LetterKeysConst.DaysToArchive, "900")
            .Replace(LetterKeysConst.DaysToDelete, "30")
            .Replace(LetterKeysConst.AccountBalance, "3000")
            .Replace(LetterKeysConst.AccountBonusBalance, "3000")
            .Replace(LetterKeysConst.ServiceCost, "300")
            .Replace(LetterKeysConst.InvoicePeriod, "300")
            .Replace(LetterKeysConst.SupportDatabaseList, "42_12212_1")
            .Replace(LetterKeysConst.File1CName, "42_12212_1")
            .Replace(LetterKeysConst.Service1CFileVersionAuditResult, "42_12212_1")
            .Replace(LetterKeysConst.DescriptionOf1CFileChanges, "42_12212_1")
            .Replace(LetterKeysConst.SupportOperation, "Обновление")
            .Replace(LetterKeysConst.JobsWithStatusNewCount, "Обновление")
            .Replace(LetterKeysConst.AuditCoreWorkerTasksQueueResults, "Обновление")
            .Replace(LetterKeysConst.AccountsWithInvalidSegment, "Обновление")
            .Replace(LetterKeysConst.UpdatedFileTemplates, "Обновление")
            .Replace(LetterKeysConst.CreationDate, "Обновление")
            .Replace(LetterKeysConst.ContactPhoneNumber, "Обновление")
            .Replace(LetterKeysConst.Configurations1CNewReleasesInfo, "Обновление")
            .Replace(LetterKeysConst.RemainingNumberOfDaysUntilTheEndOfTheDemoPeriod, "Обновление")
            .Replace(LetterKeysConst.PaymentMethod, "Обновление")
            .Replace(LetterKeysConst.AutoPayDate, "7")
            .Replace(LetterKeysConst.Today, "7.10.2024")
            .Replace(LetterKeysConst.LockServiceDate, "7")
            .Replace(LetterKeysConst.PromisePaymentDays, "7")
            .Replace(LetterKeysConst.LockServiceDate, "7")
            .Replace(LetterKeysConst.ServiceCostChangeDate, "7")
            .Replace(LetterKeysConst.ServiceOwnerEmail, "7")
            .Replace(LetterKeysConst.ServiceOwnerEmail, "7")
            .Replace(LetterKeysConst.ServiceOwnerPhoneNumber, "7")
            .Replace(LetterKeysConst.ServiceTypeCostChanges, "7")
            .Replace(LetterKeysConst.TextOnServiceOwnerPhoneNumberDisplayValue, "7")
            .Replace(LetterKeysConst.ServiceTypesDeleteDate, "7")
            .Replace(LetterKeysConst.ServiceTypesToRemove, "7")
            .Replace(LetterKeysConst.ServiceNames, "7")
            .Replace(LetterKeysConst.LockReason, "7")
            .Replace(LetterKeysConst.TehSupportStartDateTimeStringValue, "7")
            .Replace(LetterKeysConst.AutoUpdateStartDateTimeStringValue, "7")
            .Replace(LetterKeysConst.AutoUpdateDatabasesDisplayValue, "7")
            .Replace(LetterKeysConst.TehSupportDatabaseNamesDisplayValue, "7")
            .Replace(LetterKeysConst.TehSupportFailureDatabasesDisplayValue, "7")
            .Replace(LetterKeysConst.TehSupportSuccessesDatabaseNamesDisplayValue, "7")
            .Replace(LetterKeysConst.TehSupportSuccessesDatabaseNames, "7")
            .Replace(LetterKeysConst.TehSupportFailureDatabases, "7")
            .Replace(LetterKeysConst.AutoUpdateSuccessesDatabases, "7")
            .Replace(LetterKeysConst.AutoUpdateFailureDatabases, "7")
            .Replace(LetterKeysConst.AutoUpdateFailureDatabasesDisplayValue, "7")
            .Replace(LetterKeysConst.AccountUserPwd, "7")
            .Replace(LetterKeysConst.AccountUserEmail, "7")
            .Replace(LetterKeysConst.UrlForVerifyEmail, "7")
            .Replace(LetterKeysConst.ChangesDateFrom, "7")
            .Replace(LetterKeysConst.ChangesDateTo, "7")
            .Replace(LetterKeysConst.MainServiceResourcesChanges, "7")
            .Replace(LetterKeysConst.ModeratorCommentAboutService1CFileVersionDisplayValue, "7")
            .Replace(LetterKeysConst.Version1CFile, "7")
            .Replace(LetterKeysConst.UrlForOpenDelansServicePage, "7")
            .Replace(LetterKeysConst.UrlForResetPassword, "7")
            .Replace(LetterKeysConst.AutoUpdateSuccessesDatabasesDisplayValue, "7")
            .Replace(LetterKeysConst.AccountCaption, model.AccountInfo)
            .Replace(LetterKeysConst.ContactPhoneNumber, model.ContactPhoneNumber);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <param name="hasChangedToServerType">Изменена на серверную</param>
        /// <returns>Модель письма об изменении типа информационной базы(серверная/файловая)</returns>
        public static ChangeDatabaseTypeLetterModelDto GetTestDataToCurrentHtmlLetter(bool hasChangedToServerType = false) =>
            new()
            {
                AccountInfo = "23566 (ООО “ПромСтройМет”)",
                DatabaseInfo = "1c_my_23566_4 (Склад ИЛД)",
                ContactPhoneNumber = "+7 495 516 16 45",
                HasChangedToServerType = hasChangedToServerType
            };
    }
}
