﻿using System.Text;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.LetterNotification.Helpers.Footers;
using Clouds42.Logger;

namespace Clouds42.LetterNotification.Helpers
{
    /// <summary>
    /// Билдер для содержимого письма
    /// </summary>
    public class EmailContentBuilder(IAccountConfigurationDataProvider accountConfProvider, string body)
    {
        private readonly StringBuilder _body = new StringBuilder(body);
        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Добавить нижнюю часть письма по шаблону Efsol
        /// </summary>
        /// <param name="userGuid">ID пользователя</param>
        /// <param name="showUnsubscribe">Признак необходимости показывать ссылку на отказ от рассылки</param>
        /// <returns>Нижняя часть письма по шаблону Efsol</returns>
        public EmailContentBuilder AddFooterEfsol(Guid userGuid, bool? showUnsubscribe)
        {
            var locale = accountConfProvider.GetAccountUserLocale(userGuid);

            FooterCreator footer = locale.Name switch
            {
                LocaleConst.Ukraine => new FooterCreatorUA(),
                LocaleConst.Russia => new FooterCreatorRU(),
                LocaleConst.Kazakhstan => new FooterCreatorKZ(),
                LocaleConst.Uzbekistan => new FooterCreatorUZ()
            };

            _body.AppendLine(footer.GetLocalFooter().GetFooter(userGuid, showUnsubscribe, Configurations.Configurations.CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()));

            return this;
        }

        /// <summary>
        /// Построить тело письма
        /// </summary>
        /// <returns>Тело письма</returns>
        public string Build()
        {
            _logger.Trace("build content");
            return _body.ToString();
        }
    }
}
