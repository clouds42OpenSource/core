﻿using Clouds42.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountUser;

namespace Clouds42.LetterNotification.Helpers
{
    /// <summary>
    /// Класс для отправки письма о подтверждении почты
    /// </summary>
    public class EmailVerificationSender(ILetterNotificationProcessor letterNotificationProcessor)
    {
        /// <summary>
        /// Отправить уведомление
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <returns>Результат отправки</returns>
        public bool SendNotification(AccountUser accountUser) => letterNotificationProcessor
            .TryNotify<ConfirmationUserEmailLetterNotification, ConfirmationUserEmailLetterModelDto>(
                new ConfirmationUserEmailLetterModelDto
                {
                    AccountId = accountUser.AccountId,
                    AccountUserId = accountUser.Id,
                    AccountUserLogin = accountUser.Login,
                    UrlForVerifyEmail = GetVerifyLink(accountUser.Id)
                });

        /// <summary>
        /// Получить урл для верефикации Email адреса 
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <returns>Урл для верефикации Email адреса</returns>
        private string GetVerifyLink(Guid userId)
        {
            var link = ConfigurationHelper.GetConfigurationValue("EmailVerificationLink");
            return link + new string(userId.ToString().Reverse().ToArray());
        }
    }
}