﻿using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.Helpers
{
    /// <summary>
    /// Создатель экземпляра тестового письма
    /// </summary>
    public class LetterTemplateTestInstanceCreator(IServiceProvider serviceProvider)
    {
        /// <summary>
        /// Cоздать новый экземпляр тестового письма
        /// </summary>
        /// <param name="letterAssemblyClassName">Полная информация о классе письма</param>
        /// <returns>Новый экземпляр тестового письма</returns>
        public LetterTemplateTestBase? CreateInstance(string letterAssemblyClassName)
        {
            var type = Type.GetType(letterAssemblyClassName, true);
            return serviceProvider.GetRequiredService(type) as LetterTemplateTestBase;
        }
    }
}
