﻿using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.LetterNotification.IncomingModels
{
    /// <summary>
    /// Модель письма о скорой автооплате перед блокировкой сервиса
    /// </summary>
    public class BeforeServiceLockAutoPayLetterModel : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Информация о платежном способе
        /// </summary>
        public string PaymentMethod { get; set; }

        /// <summary>
        /// Дата блокировки
        /// </summary>
        public DateTime LockServiceDate { set; get; }

        /// <summary>
        /// Дата автосписания
        /// </summary>
        public DateTime AutoPayDate { get; set; }

        /// <summary>
        /// Счет
        /// </summary>
        public InvoiceDataDto Invoice { get; set; }

        /// <summary>
        /// Стоимсоть сервиса
        /// </summary>
        public decimal ServiceCost { set; get; }

        /// <summary>
        /// Сумма счета
        /// </summary>
        public decimal InvoiceSum { get; set; }

        /// <summary>
        /// Урл на страницу баланса
        /// </summary>
        public string BalancePageUrl { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string BillingServiceName { get; set; }
    }
}
