﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Configurations.Configurations;
using Clouds42.Configurations.Configurations.MailConfigurations;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid;
using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.LetterNotification.LetterNotifications.AccountUser;
using Clouds42.LetterNotification.LetterNotifications.Invoice;
using Clouds42.LetterNotification.LetterNotifications.Services;
using Clouds42.LetterNotification.SendGrid.EmailClient;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.NotificationContext.Commands;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.Base
{
    /// <summary>
    /// Базовый класс уведомления
    /// </summary>
    /// <typeparam name="TModel">Входящая модель</typeparam>
    public abstract class LetterNotificationBase<TModel>(IServiceProvider serviceProvider)
        : ILetterNotificationBase<TModel>
        where TModel : ILetterTemplateModel
    {
        protected readonly IServiceProvider ServiceProvider = serviceProvider;

        /// <summary>
        /// Описание отправителя
        /// </summary>
        private readonly Lazy<string> _senderDescription = new(CloudConfigurationProvider.LetterNotification.GetSenderDescription);

        /// <summary>
        /// Дефолтное количество допустимых уведомлений
        /// </summary>
        private readonly Lazy<int> _defaultNotificationsInPeriodCount = new(CloudConfigurationProvider.LetterNotification.GetDefaultNotificationsInPeriodCount);

        /// <summary>
        /// Состояние активности писем
        /// </summary>
        private readonly Lazy<bool> _isEnabledLetters = new(CloudConfigurationProvider.LetterNotification.GetLettersEnableState);

        /// <summary>
        /// Признак, указывающий на необходимость отправлять письма
        /// </summary>
        private readonly Lazy<bool> _needSendLetters = new(CloudConfigurationProvider.LetterNotification.GetNeedSendLetters);

        /// <summary>
        /// Почта менеджера pro облака
        /// </summary>
        private readonly Lazy<string> _managerProEmail = new(MailConfiguration.Efsol.Credentials.ManagerPro.GetLogin);

        /// <summary>
        /// Почта менеджера облака
        /// </summary>
        private readonly Lazy<string> _managerEmail = new(MailConfiguration.Efsol.Credentials.Manager.GetLogin);

        private readonly IAccessProvider _accessProvider = serviceProvider.GetRequiredService<IAccessProvider>();
        private readonly IUnitOfWork _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly INotifiedAccountUserInfoProvider _notifiedAccountUserInfoProvider = serviceProvider.GetRequiredService<INotifiedAccountUserInfoProvider>();
        private readonly ILogger42 _logger = serviceProvider.GetRequiredService<ILogger42>();
        private readonly ISendGridEmailClient _sendGridEmailClient = serviceProvider.GetRequiredService<ISendGridEmailClient>();
        private readonly IHandlerException _handlerException = serviceProvider.GetRequiredService<IHandlerException>();
        private readonly ISender _sender = serviceProvider.GetRequiredService<ISender>();
        private readonly List<Type> _typesForIgnoreUnsubscribeFlag =
        [
            typeof(SendInvoiceToClientLetterNotification),
            typeof(ResetPasswordLetterNotification),
            typeof(RegistrationUserLetterNotification),
            typeof(ConfirmationUserEmailLetterNotification),
            typeof(PublishDatabaseLetterNotification),
            typeof(BeforeChangeServiceCostLetterNotification),
            typeof(BeforeDeleteServiceTypesLetterNotification),
            typeof(BeforeSupportDatabaseLetterNotification)
        ];
        private readonly List<Type> _typesNoIgnoreCheckedFlag =
        [
            typeof(RegistrationAccountUserLetterNotification),
            typeof(RegistrationUserLetterNotification)
        ];

        /// <summary>
        /// Получить шаблон письма
        /// (Фабричный метод)
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected abstract LetterTemplateBase<TModel> GetLetterTemplate(TModel model,
            NotifiedAccountUserInfoDto? notifiedUserInfo);

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Прикрепленный файл</returns>
        protected abstract DocumentDataDto? GetAttachmentFile(TModel model);

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Ключ уникальности события</returns>  
        protected abstract string GetEventKey(TModel model);

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// (в случае о предупреждениях об оплате)
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>  
        protected abstract DateTime GetEventKeyPeriod(TModel model);

        /// <summary>
        /// Получить количество допустимых уведомлений
        /// в данном периоде
        /// </summary>
        /// <returns>Количество допустимых событий
        /// в данном периоде</returns>
        protected virtual int GetAllowedNotificationsInPeriodCount() => _defaultNotificationsInPeriodCount.Value;

        /// <summary>
        /// Получить описание отправителя
        /// </summary>
        /// <returns>Описание отправителя</returns>
        protected virtual string GetSenderDescription() => _senderDescription.Value;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected virtual List<string> GetAccountEmails(TModel model) => _dbLayer.AccountEmailRepository
            .WhereLazy(w => w.AccountId == model.AccountId)
            .Select(w => w.Email)
            .ToList();

        /// <summary>
        /// Получить Id пользователя аккаунта
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Id пользователя аккаунта</returns>
        protected virtual Guid? GetAccountUserId(TModel model) => null;

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        protected virtual Action CreateActionToExecuteAfterSendingEmail(TModel model) => () =>
            _logger.Info($"Письмо для аккаунта '{model.AccountId}' успешно отправленно.");

        /// <summary>
        /// Получить электронный адрес получателя
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Электронный адрес получателя(email)</returns>
        protected virtual string GetReceiverEmail(TModel model, NotifiedAccountUserInfoDto notifiedAccountUserInfo) =>
            notifiedAccountUserInfo.Email;

        /// <summary>
        /// Попытаться выполнить уведомление
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Результат выполнения уведомления</returns>
        public bool TryNotify(TModel model)
        {
            var accountUserId = GetAccountUserId(model);
            var accountEmails = GetAccountEmails(model);

            if (!_notifiedAccountUserInfoProvider.TryGet(out var notifiedAccountUserInfo, model.AccountId, accountUserId, accountEmails))
                return false;

            return NeedNotify(notifiedAccountUserInfo) && TrySendLetter(model, notifiedAccountUserInfo);
        }

        /// <summary>
        /// Попытаться отправить письмо
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Резуьтат отправки псьма</returns>
        private bool TrySendLetter(TModel model, NotifiedAccountUserInfoDto notifiedAccountUserInfo)
        {
            var eventKey = GetEventKey(model);
            var notifyType = GetType();
            _logger.Info($"Значение поля Unsubscribed: {(notifiedAccountUserInfo.IsUnsubscribed?.ToString() ?? "не определено")}");
            _logger.Info($"Тип нотификации: {notifyType}");
            
            try
            {
                var commonLetterTemplateData =
                    GetLetterTemplate(model, notifiedAccountUserInfo)
                        .GetCommonLetterTemplateData(model, notifiedAccountUserInfo);
                
                SendEmail(model, commonLetterTemplateData, notifiedAccountUserInfo, eventKey);

                _sender.Send(new GlobalSendNotificationsCommand
                {
                    UserIds = notifiedAccountUserInfo.UserIdsToSend.Distinct().ToList(),
                    Message = commonLetterTemplateData.LetterTemplateData.NotificationText,
                    IgnoreMailSending = true,
                    NotificationSendType = NotificationSendType.ByUsersIdsList,
                    State = NotificationState.Normal
                }).Wait();
            }

            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка уведомления о событии] {eventKey}");

                return false;
            }

            return true;
        }


        private void SendEmail(TModel model, LetterTemplateCommonDataDto commonLetterTemplateData, NotifiedAccountUserInfoDto notifiedAccountUserInfo, string eventKey)
        {
            var accountUserNotificationSettings = _dbLayer.AccountUserNotificationSettingsRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(x => x.AccountUserId == notifiedAccountUserInfo.Id && x.NotificationSettings.Type == NotificationType.Email && x.IsActive);

            if (accountUserNotificationSettings == null)
            {
                _logger.Info($"У пользователя {notifiedAccountUserInfo.Login} c почтовым ящиком {notifiedAccountUserInfo.Email} отключены уведомления по email. Отправка письма будет произведена по другим включенным каналам");

                return;
            }

            var accountConfig = _dbLayer.AccountConfigurationRepository.FirstOrDefault(x => x.AccountId == model.AccountId);
            _logger.Info($"Значение поля Unsubscribed: {(notifiedAccountUserInfo.IsUnsubscribed.HasValue ? notifiedAccountUserInfo.IsUnsubscribed.Value : "не определено")}");
            _logger.Info($"Тип нотификации: {GetType()}");

            if (notifiedAccountUserInfo.IsUnsubscribed.HasValue && notifiedAccountUserInfo.IsUnsubscribed.Value &&
                !_typesForIgnoreUnsubscribeFlag.Contains(GetType()))
            {
                _logger.Info("Отписан, нотификация не системная(отписка действует)");

                return;
            }

            if (!_typesNoIgnoreCheckedFlag.Contains(GetType()) && notifiedAccountUserInfo.EmailStatus != "Checked")
            {
                _logger.Info("Почта не верифицирована, отклоняем отправку письма");

                return;
            }

            if (accountConfig.Type == "Cloud.ru")
            {
                _logger.Info("Письма пользователям Cloud не отправляем!");

                return;
            }

            var notificationsBufferProvider = ServiceProvider.GetRequiredService<INotificationsBufferProvider>();

            if (notificationsBufferProvider.IsBufferedNotifyEvent(model.AccountId, eventKey,
                GetAllowedNotificationsInPeriodCount()))

                return;

            var subject = commonLetterTemplateData.LetterTemplateData.Subject;
            var receiverEmail = GetReceiverEmail(model, notifiedAccountUserInfo);
            var emailRequestSettings =
                CreateEmailRequestSettings(model, notifiedAccountUserInfo, receiverEmail);

            var logMessage = $"Письмо \"{subject}\" отправлено на адрес: {receiverEmail}";

            if (notifiedAccountUserInfo.EmailsToCopy.Count > 0)
            {
                foreach (var email in notifiedAccountUserInfo.EmailsToCopy.Where(email => !string.Equals(email, receiverEmail, StringComparison.CurrentCultureIgnoreCase)))
                {
                    emailRequestSettings.CopyToEmailAddresses.Add(new EmailAddressInfoDto(email));
                }


                logMessage = $"Письмо \"{subject}\" отправлено на адреса: {receiverEmail}, {string.Join(", ", notifiedAccountUserInfo.EmailsToCopy)}";
            }

            var attachment = GetAttachmentFile(model);

            if (attachment != null)
                emailRequestSettings.Attachments.Add(attachment);

            if (_needSendLetters.Value)
            {
                _sendGridEmailClient
                    .SendEmailViaNewThread(emailRequestSettings, commonLetterTemplateData);

                CreateActionToExecuteAfterSendingEmail(model)();

                LogEventHelper.LogEvent(_dbLayer, model.AccountId, _accessProvider,
                    LogActions.MailNotification, logMessage);
            }

            notificationsBufferProvider.WriteNotificationToBuffer(model.AccountId, eventKey,
                GetEventKeyPeriod(model));
        }

        /// <summary>
        /// Получить информацию об отправителе письма
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Информацию об отправителе письма</returns>
        private EmailAddressInfoDto GetEmailSenderInfo(string localeName) => new(
            localeName == LocaleConst.Ukraine ? _managerProEmail.Value : _managerEmail.Value, GetSenderDescription());

        /// <summary>
        /// Создать настройки запроса для отправки письма
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="receiverEmail">Эл. почта получателя</param>
        /// <param name="notifiedUserInfo"></param>
        /// <returns>Настройки запроса для отправки письма</returns>
        private EmailRequestSettingsDto CreateEmailRequestSettings(TModel model, NotifiedAccountUserInfoDto notifiedUserInfo,
            string receiverEmail)
        {
            var letterTemplate = GetLetterTemplate(model, notifiedUserInfo);
            var emailRequestSettings = new EmailRequestSettingsDto
            {
                EmailReceiver = new EmailAddressInfoDto(receiverEmail),
                EmailSender = GetEmailSenderInfo(notifiedUserInfo.LocaleName),
                Categories = [letterTemplate.GetCurrentLetterTypeName()],
                SendGridApiKey = CloudConfigurationProvider.SendGrid.GetApiKey(),
                SendGridTemplateId = letterTemplate.GetSendGridTemplateId(),
                SendGridAgentName = CloudConfigurationProvider.SendGrid.GetAgentName(),
                SendGridAgentUniqueId = CloudConfigurationProvider.SendGrid.GetAgentUniqueId()
            };

            return emailRequestSettings;
        }

        /// <summary>
        /// Проверить что необходимо уведомление для пользователя
        /// </summary>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Необходимость в уведомлении</returns>
        private bool NeedNotify(NotifiedAccountUserInfoDto notifiedAccountUserInfo)
        {
            return notifiedAccountUserInfo != null && _isEnabledLetters.Value;
        }
    }
}
