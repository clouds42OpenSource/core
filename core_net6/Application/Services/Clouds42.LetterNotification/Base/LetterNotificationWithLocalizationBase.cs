﻿using Clouds42.Common.Constants;
using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.Base
{
    /// <summary>
    /// Базовый класс уведомления с локализацией
    /// </summary>
    /// <typeparam name="TModel">Входящая модель</typeparam>
    /// <typeparam name="TLetterTemplateRu">Тип шаблона письма для русской локали</typeparam>
    /// <typeparam name="TLetterTemplateUa">Тип шаблона письма для украинской локали</typeparam>
    /// <typeparam name="TLetterTemplateKz">Тип шаблона письма для казахской локали</typeparam>
    /// <typeparam name="TLetterTemplateUz">Тип шаблона письма для узбекской локали</typeparam>
    public abstract class LetterNotificationWithLocalizationBase<TModel, TLetterTemplateRu, TLetterTemplateUa,
        TLetterTemplateKz, TLetterTemplateUz>(IServiceProvider serviceProvider) : LetterNotificationBase<TModel>(serviceProvider) where TModel : ILetterTemplateModel
        where TLetterTemplateRu : LetterTemplateBase<TModel>
        where TLetterTemplateUa : LetterTemplateBase<TModel>
        where TLetterTemplateKz : LetterTemplateBase<TModel>
        where TLetterTemplateUz : LetterTemplateBase<TModel>
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<TModel> GetLetterTemplate(TModel model,
            NotifiedAccountUserInfoDto? notifiedUserInfo)
        {
            var mapElement = GenerateMatchingLetterTemplatesToLocalesMap()
                .FirstOrDefault(matching => matching.LocaleName == notifiedUserInfo?.LocaleName);

            if (mapElement == default)
                return ServiceProvider.GetRequiredService<TLetterTemplateRu>();

            return mapElement.CreateLetterTemplate();
        }

        /// <summary>
        /// Сформировать карту сопоставлений шаблонов писем к локалям аккаунта
        /// </summary>
        /// <returns>Карта сопоставлений шаблонов писем к локалям аккаунта</returns>
        protected virtual IList<(string LocaleName, Func<LetterTemplateBase<TModel>> CreateLetterTemplate)>
            GenerateMatchingLetterTemplatesToLocalesMap() =>
            new List<(string LocaleName, Func<LetterTemplateBase<TModel>> CreateLetterTemplate)>
            {
                (LocaleConst.Russia, () => ServiceProvider.GetRequiredService<TLetterTemplateRu>()),
                (LocaleConst.Ukraine, () => ServiceProvider.GetRequiredService<TLetterTemplateUa>()),
                (LocaleConst.Kazakhstan, () => ServiceProvider.GetRequiredService<TLetterTemplateKz>()),
                (LocaleConst.Uzbekistan, () => ServiceProvider.GetRequiredService<TLetterTemplateUz>())
            };
    }
}
