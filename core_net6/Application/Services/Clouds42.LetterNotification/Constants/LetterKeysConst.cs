﻿namespace Clouds42.LetterNotification.Constants
{
    /// <summary>
    /// Общие ключи для письма (для замены значений)
    /// </summary>
    public static class LetterKeysConst
    {
        /// <summary>
        /// Валюта локали
        /// </summary>
        public const string LocaleCurrency = "@LocaleCurrency";

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public const string AccountDatabaseCaption = "@AcDbCaption";

        /// <summary>
        /// Значение атрибута display='none'
        /// </summary>
        public const string DisplayValueNone = "none";

        /// <summary>
        /// Номер счета
        /// </summary>
        public const string InvoiceNumber = "@InvoiceNumber";

        /// <summary>
        /// Сумма счета
        /// </summary>
        public const string InvoiceSum = "@InvoiceSum";

        /// <summary>
        /// Веб ссылка для запуска инф. базы
        /// </summary>
        public const string AccountDatabasePublishPath = "@AcDbpublishPath";

        /// <summary>
        /// Значение CSS свойства Display для отображения веб ссылки для запуска инф. базы
        /// </summary>
        public const string AcDbPublishPathDisplayValue = "@AcDbPublishPathDisplayValue";

        /// <summary>
        /// Название сервиса
        /// </summary>
        public const string ServiceName = "@ServiceName";

        /// <summary>
        /// Результат модерации
        /// </summary>
        public const string ModerationResult = "@ModerationResult";

        /// <summary>
        /// Комментарий модератора
        /// </summary>
        public const string ModeratorComment = "@ModeratorComment";

        /// <summary>
        /// Ссылка для открытия карточки сервиса
        /// </summary>
        public const string UrlForOpenBillingServiceCard = "@UrlForOpenBillingServiceCard";

        /// <summary>
        /// Отображение текста о возможности повтороно изменить
        /// заявку на редактирование сервиса
        /// </summary>
        public const string TextOnEditServiceRequestDisplayValue = "@TextOnEditServiceRequestDisplayValue";

        /// <summary>
        /// Урл страницы баланса
        /// </summary>
        public const string BalancePageUrl = "@BalancePageUrl";

        /// <summary>
        /// Количество дней на обещанный платеж
        /// </summary>
        public const string PromisePaymentDays = "@PromisePaymentDays";

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public const string AccountCaption = "@AccountCaption";

        /// <summary>
        /// Урл на страницу ифн. баз
        /// </summary>
        public const string UrlForAccountDatabasesPage = "@UrlForAccountDatabasesPage";

        /// <summary>
        /// Время жизни данных аккаунта в склепе(количество дней)
        /// </summary>
        public const string LifetimeAccountDataInTombDaysCount = "@LifetimeAccountDataInTombDaysCount";

        /// <summary>
        /// Количество дней до архивации данных аккаунта
        /// </summary>
        public const string DaysToArchive = "@DaysToArchive";

        /// <summary>
        /// Количество дней до удаления данных аккаунта
        /// </summary>
        public const string DaysToDelete = "@DaysToDelete";

        /// <summary>
        /// Баланс аккаунта
        /// </summary>
        public const string AccountBalance = "@AccountBalance";

        /// <summary>
        /// Бонусный баланс аккаунта
        /// </summary>
        public const string AccountBonusBalance = "@AccountBonusBalance";

        /// <summary>
        /// Стоимость сервиса
        /// </summary>
        public const string ServiceCost = "@ServiceCost";

        /// <summary>
        /// Период счета
        /// </summary>
        public const string InvoicePeriod = "@InvoicePeriod";

        /// <summary>
        /// Дата блокировки
        /// </summary>
        public const string LockServiceDate = "@LockServiceDate";

        /// <summary>
        /// Дата изменения стоимости сервиса
        /// </summary>
        public const string ServiceCostChangeDate = "@ServiceCostChangeDate";

        /// <summary>
        /// Электронная почта владельца сервиса
        /// </summary>
        public const string ServiceOwnerEmail = "@ServiceOwnerEmail";

        /// <summary>
        /// Номер телефона владельца сервиса
        /// </summary>
        public const string ServiceOwnerPhoneNumber = "@ServiceOwnerPhoneNumber";

        /// <summary>
        /// Изменения стоимости услуг
        /// </summary>
        public const string ServiceTypeCostChanges = "@ServiceTypeCostChanges";

        /// <summary>
        /// Отображение текста - номер телефона владельца сервиса
        /// </summary>
        public const string TextOnServiceOwnerPhoneNumberDisplayValue = "@TextOnServiceOwnerPhoneNumberDisplayValue";

        /// <summary>
        /// Дата удаления услуг сервиса
        /// </summary>
        public const string ServiceTypesDeleteDate = "@ServiceTypesDeleteDate";

        /// <summary>
        /// Услуги к удалению
        /// </summary>
        public const string ServiceTypesToRemove = "@ServiceTypesToRemove";

        /// <summary>
        /// Название заблокированных сервисов
        /// </summary>
        public const string ServiceNames = "@ServiceNames";

        /// <summary>
        /// Причина блокировки
        /// </summary>
        public const string LockReason = "@LockReason";

        /// <summary>
        /// Строковое значение времени запуска ТиИ для ифн. баз
        /// </summary>
        public const string TehSupportStartDateTimeStringValue = "@TehSupportStartDateTimeStringValue";

        /// <summary>
        /// Строковое значение времени запуска AO для ифн. баз
        /// </summary>
        public const string AutoUpdateStartDateTimeStringValue = "@AutoUpdateStartDateTimeStringValue";

        /// <summary>
        /// Отображение текста - cписок баз, в которых будет проведено AO
        /// </summary>
        public const string AutoUpdateDatabasesDisplayValue = "@AutoUpdateDatabasesDisplayValue";

        /// <summary>
        /// Отображение текста - cписок баз, в которых будет проведено ТиИ
        /// </summary>
        public const string TehSupportDatabaseNamesDisplayValue = "@TehSupportDatabaseNamesDisplayValue";

        /// <summary>
        /// Отображение текста - инф. базы, для которых ТиИ завершилось с ошибкой
        /// </summary>
        public const string TehSupportFailureDatabasesDisplayValue = "@TehSupportFailureDatabasesDisplayValue";

        /// <summary>
        /// Отображение текста - названия инф. баз, которые успешно прошли ТиИ
        /// </summary>
        public const string TehSupportSuccessesDatabaseNamesDisplayValue = "@TehSupportSuccessesDatabaseNamesDisplayValue";

        /// <summary>
        /// Названия инф. баз, которые успешно прошли ТиИ
        /// </summary>
        public const string TehSupportSuccessesDatabaseNames = "@TehSupportSuccessesDatabaseNames";

        /// <summary>
        /// Инф. базы, для которых ТиИ завершилось с ошибкой
        /// </summary>
        public const string TehSupportFailureDatabases = "@TehSupportFailureDatabases";

        /// <summary>
        /// Инф. базы, для которых АО завершилось успешно
        /// </summary>
        public const string AutoUpdateSuccessesDatabases = "@AutoUpdateSuccessesDatabases";

        /// <summary>
        /// Инф. базы, для которых АО завершилось с ошибкой
        /// </summary>
        public const string AutoUpdateFailureDatabases = "@AutoUpdateFailureDatabases";

        /// <summary>
        /// Отображение текста - инф. базы, для которых АО завершилось с ошибкой
        /// </summary>
        public const string AutoUpdateFailureDatabasesDisplayValue = "@AutoUpdateFailureDatabasesDisplayValue";

        /// <summary>
        /// Отображение текста - инф. базы, для которых АО завершилось успешно
        /// </summary>
        public const string AutoUpdateSuccessesDatabasesDisplayValue = "@AutoUpdateSuccessesDatabasesDisplayValue";

        /// <summary>
        /// Урл для сброса/восстановления пароля
        /// </summary>
        public const string UrlForResetPassword = "@UrlForResetPassword";

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public const string AccountUserLogin = "@AccountUserLogin";

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public const string AccountUserPwd = "@AccountUserPwd";

        /// <summary>
        /// Урл страницы сервиса Delans
        /// </summary>
        public const string UrlForOpenDelansServicePage = "@UrlForOpenDelansServicePage";

        /// <summary>
        /// Почта пользователя
        /// </summary>
        public const string AccountUserEmail = "@AccountUserEmail";

        /// <summary>
        /// Урл для верефикации Email адреса
        /// </summary>
        public const string UrlForVerifyEmail = "@UrlForVerifyEmail";

        /// <summary>
        /// Дата изменений с
        /// </summary>
        public const string ChangesDateFrom = "@ChangesDateFrom";

        /// <summary>
        /// Дата изменений по
        /// </summary>
        public const string ChangesDateTo = "@ChangesDateTo";

        /// <summary>
        /// Изменения ресурсов главного сервиса (Аренда1С)
        /// </summary>
        public const string MainServiceResourcesChanges = "<tr><td>@MainServiceResourcesChanges</td></tr>";

        /// <summary>
        /// Отображение комментария модератора о результате аудита версии файла 1С
        /// </summary>
        public const string ModeratorCommentAboutService1CFileVersionDisplayValue = "@ModeratorCommentAboutService1CFileVersionDisplayValue";

        /// <summary>
        /// Версия 1С файла
        /// </summary>
        public const string Version1CFile = "@Version1CFile";

        /// <summary>
        /// Название файла разработки
        /// </summary>
        public const string File1CName = "@File1CName";

        /// <summary>
        /// Результат аудита версии файла разработки 1С
        /// </summary>
        public const string Service1CFileVersionAuditResult = "@Service1CFileVersionAuditResult";

        /// <summary>
        /// Описание изменений файла 1С
        /// </summary>
        public const string DescriptionOf1CFileChanges = "@DescriptionOf1CFileChanges";

        /// <summary>
        /// Количество задач в очереди со статусом "Новая"
        /// </summary>
        public const string JobsWithStatusNewCount = "@JobsWithStatusNewCount";

        /// <summary>
        /// Данные по очереди задач для каждого воркера
        /// </summary>
        public const string AuditCoreWorkerTasksQueueResults = "<tr><td>@AuditCoreWorkerTasksQueueResults</td></tr>";

        /// <summary>
        /// Аккаунты с неверным сегментом после миграции
        /// </summary>
        public const string AccountsWithInvalidSegment = "<tr><td>@AccountsWithInvalidSegment</td></tr>";

        /// <summary>
        /// Список обновлённых файловых шаблонов
        /// </summary>
        public const string UpdatedFileTemplates = "<tr><td>@UpdatedFileTemplates</td></tr>";

        /// <summary>
        /// Дата создания
        /// </summary>
        public const string CreationDate = "@CreationDate";

        /// <summary>
        /// Информация об аккаунте
        /// </summary>
        public const string AccountInfo = "@AccountInfo";

        /// <summary>
        /// Информация об инф. базе
        /// </summary>
        public const string DatabaseInfo = "@DatabaseInfo";

        /// <summary>
        /// Телефон для связи с пользователем
        /// </summary>
        public const string ContactPhoneNumber = "@ContactPhoneNumber";

        /// <summary>
        /// Информация о новых релизах конфигурации 1С
        /// </summary>
        public const string Configurations1CNewReleasesInfo = "@Configurations1CNewReleasesInfo";

        /// <summary>
        /// Количество оставшихся дней до завершения демо периода.
        /// </summary>
        public const string RemainingNumberOfDaysUntilTheEndOfTheDemoPeriod = "@RemainingNumbeOfDays";

        /// <summary>
        /// Информация о платежном способе
        /// </summary>
        public const string PaymentMethod = "@PaymentMethod";

        /// <summary>
        /// Дата автоплатежа
        /// </summary>
        public const string AutoPayDate = "@AutoPayDate";

        /// <summary>
        /// Сегодня
        /// </summary>
        public const string Today = "@Today";

        /// <summary>
        /// Сегодня
        /// </summary>
        public const string SupportOperation = "@SupportOperation";

        /// <summary>
        /// Список баз на обслуживание
        /// </summary>
        public const string SupportDatabaseList = "@SupportDatabaseList";

        /// <summary>
        /// Список баз на обслуживание
        /// </summary>
        public const string AutoUpdate = "@AutoUpdate";
    }
}
