﻿namespace Clouds42.LetterNotification.Constants
{
    /// <summary>
    /// Ключи кастомных заголовков для SendGrid
    /// </summary>
    internal static class SendGridCustomHeadersKeysConst
    {
        /// <summary>
        /// Название заголовка для уникального идентификатора агента SendGrid
        /// </summary>
        public const string AgentUIdHeaderKey = "X-Nep-Ff";

        /// <summary>
        /// Название заголовка для названия агента SendGrid
        /// </summary>
        public const string AgentNameHeaderKey = "X-Nep-AgN";
    }
}
