﻿namespace Clouds42.LetterNotification.Constants
{
    /// <summary>
    /// Текстовые константы для Html шаблона письма
    /// </summary>
    public static class LetterHtmlTemplateTextConst
    {
        /// <summary>
        /// Отказ от рассылок в письме
        /// </summary>
        public const string UnsubscribeText = "Отписаться от рассылки";
    }
}
