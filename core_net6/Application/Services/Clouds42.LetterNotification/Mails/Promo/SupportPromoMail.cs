﻿using Clouds42.Configurations.Configurations.MailConfigurations;
using Clouds42.LetterNotification.Providers.Custom;

namespace Clouds42.LetterNotification.Mails.Promo
{
    /// <summary>
    ///     Почта support@42clouds.com
    /// </summary>
    public class SupportPromoMail : PromoMailProvider
    {
        /// <summary>
        ///     Логин
        /// </summary>
        protected override string Login => MailConfiguration.Promo.Credentials.Support.GetLogin();

        /// <summary>
        ///     Пароль
        /// </summary>
        protected override string Password => MailConfiguration.Promo.Credentials.Support.GetPassword();
    }
}