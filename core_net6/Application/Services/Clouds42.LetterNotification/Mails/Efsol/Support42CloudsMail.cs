﻿using Clouds42.Configurations.Configurations.MailConfigurations;
using Clouds42.LetterNotification.Providers.Custom;

namespace Clouds42.LetterNotification.Mails.Efsol
{
    /// <summary>
    ///     Почта support@42clouds.com
    /// </summary>
    public class Support42CloudsMail : EfsolMailProvider
    {
        /// <summary>
        ///     Логин
        /// </summary>
        protected override string Login => MailConfiguration.Efsol.Credentials.Support.GetLogin();

        /// <summary>
        ///     Пароль
        /// </summary>
        protected override string Password => MailConfiguration.Efsol.Credentials.Support.GetPassword();
    }
}
