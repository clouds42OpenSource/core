﻿using Clouds42.Configurations.Configurations.MailConfigurations;
using Clouds42.LetterNotification.Providers.Custom;

namespace Clouds42.LetterNotification.Mails.Efsol
{
    /// <summary>
    ///     Почта manager@42clouds.com
    /// </summary>
    public class Manager42CloudsMail : EfsolMailProvider
    {
        /// <summary>
        ///     Логин
        /// </summary>
        protected override string Login => MailConfiguration.Efsol.Credentials.Manager.GetLogin();

        /// <summary>
        ///     Пароль
        /// </summary>
        protected override string Password => MailConfiguration.Efsol.Credentials.Manager.GetPassword();
    }
}
