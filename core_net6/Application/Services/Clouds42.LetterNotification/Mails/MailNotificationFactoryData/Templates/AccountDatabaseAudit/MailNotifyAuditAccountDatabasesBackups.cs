﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseAudit
{
    /// <summary>
    /// Уведомление по результатам аудита бэкапов инф. баз в склепе
    /// </summary>
    public class MailNotifyAuditAccountDatabasesBackups : IMailNotificationTemplate<string>
    {
        /// <summary>
        /// Отправить письмо
        /// </summary>
        /// <param name="unitOfWork">Экземпляр IUnitOfWork</param>
        /// <param name="accessProvider">Провайдер доступа</param>
        /// <param name="param">Параметры письма</param>
        /// <param name="provider">Провайдер конфигураций аккаунта</param>
        public void SendMail(IUnitOfWork unitOfWork, IAccessProvider accessProvider, string param, IAccountConfigurationDataProvider provider)
        {
            var cloudServices = ConfigurationHelper.GetConfigurationValue<string>("cloud-services");

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(cloudServices)
                .Subject("Результат проверки бэкапов инф. баз в склепе")
                .Body(param)
                .SendViaNewThread();
        }
    }
}
