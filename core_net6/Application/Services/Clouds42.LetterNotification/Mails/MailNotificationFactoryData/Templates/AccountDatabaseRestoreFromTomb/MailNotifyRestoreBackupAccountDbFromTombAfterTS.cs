﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseRestoreFromTomb
{
    /// <summary>
    ///     Отправить письмо хотлайну о ошибке восстановления информационной базы из склепа при проведении тех-работ (АО или ТиИ)
    /// </summary>
    public class MailNotifyRestoreBackupAccountDbFromTombAfterTS : IMailNotificationTemplate<string>
    {
        public void SendMail(IUnitOfWork unitOfWork, IAccessProvider accessProvider, string param, IAccountConfigurationDataProvider provider)
        {
            var cloudServices = ConfigurationHelper.GetConfigurationValue<string>("cloud-services");
            
            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(cloudServices)
                .Subject("Проблема восстановления базы (АО\\ТиИ)")
                .Body("Не удалось восстановить информационную базу при проведении тех-работ (АО или ТиИ)." +
                      "<br/>Детали:<br/>" + param)
                .SendViaNewThread();
        }
    }
}
