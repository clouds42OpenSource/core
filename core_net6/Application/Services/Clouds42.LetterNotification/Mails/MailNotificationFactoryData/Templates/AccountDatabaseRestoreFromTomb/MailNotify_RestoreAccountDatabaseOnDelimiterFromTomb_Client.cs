﻿using System.Text;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Helpers;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseRestoreFromTomb
{
    /// <summary>
    ///     Уведомление клиенту, в случае успешного восстановления информационной базы на разделителях из склепа
    /// </summary>
    public class MailNotifyRestoreAccountDatabaseOnDelimiterFromTombClient : IMailNotificationTemplate<Guid>
    {   
        public MailNotifyRestoreAccountDatabaseOnDelimiterFromTombClient()
        {
     
        }
        public void SendMail(IUnitOfWork unitOfWork, IAccessProvider accessProvider, Guid param, IAccountConfigurationDataProvider provider)
        {
            var query = (
                from accountDatabase in unitOfWork.DatabasesRepository.WhereLazy()
                join account in unitOfWork.AccountsRepository.WhereLazy()
                    on accountDatabase.AccountId equals account.Id
                where accountDatabase.Id == param
                select new
                {
                    AccountDatabaseCaption = accountDatabase.Caption,
                    Account = account
                }).FirstOrDefault();

            if (query == null)
                return;

            var accAdminIds = unitOfWork.AccountsRepository.GetAccountAdminIds(query.Account.Id);
            var accAdmins = unitOfWork.AccountUsersRepository.Where(w => accAdminIds.Contains(w.Id)).ToList();

            var emails = unitOfWork.AccountsRepository.GetEmails(query.Account.Id);

            if (!emails.Any())
                return;

            var defaultAccountAdmin = accAdmins.FirstOrDefault(acc => emails.Any(e => acc.Email == e));

            if (defaultAccountAdmin == null)
                return;

            emails.ForEach(email =>
            {
                var accAdmin = accAdmins.FirstOrDefault(e => e.Email == email) ?? defaultAccountAdmin;

                var userLogin = accAdmin.TryGetFullUserName(out var fullName) ? fullName : accAdmin.Login;

                var body = CreateMailBody(query.AccountDatabaseCaption, userLogin);

                var content = new EmailContentBuilder(provider, body).AddFooterEfsol(accAdmin.Id, true).Build();

                new Manager42CloudsMail()
                    .DisplayName("Команда 42Clouds")
                    .To(email)
                    .Subject("Восстановление архивной копии")
                    .Body(content)
                    .SendViaNewThread();

                LogEventHelper.LogEvent(unitOfWork, query.Account.Id, accessProvider, LogActions.MailNotification,
                    $"Письмо \"Восстановление архивной копии\" отправлено на адрес {email}");
            });

        }

        private static string CreateMailBody(string dbName, string userLogin)
        {
            var body = new StringBuilder();

            body.Append("<table style=\"margin: 0pt; padding: 0pt; border-collapse: collapse; vertical-align: top; text-align: left; height: 100% !important; width: 100% !important; background: url('https://42clouds.com/ru-ru/') 0% 50% repeat scroll #f7f6f4;\" bgcolor=\"#f7f6f4\">");
            body.Append("<tbody>");
            body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            body.Append("<td align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\">");
            body.Append("<table style=\"padding: 0pt; width: 100%; border-collapse: collapse; vertical-align: top; text-align: left;\">");
            body.Append("<tbody>");
            body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            body.Append("<td height=\"50\" align=\"left\" style=\"padding: 0pt; height: 50px; vertical-align: top; text-align: left; border-collapse: collapse !important;\">&nbsp;</td>");
            body.Append("</tr>");
            body.Append("</tbody>");
            body.Append("</table>");
            body.Append("<center>");
            body.Append("<table style=\"border: 1px solid #e2e2e2; padding: 0pt; border-collapse: collapse; vertical-align: top; text-align: left; width: 705px; font-size: 15px; font-family: Arial, Helvetica, sans-serif; height: 200px; background: none 0% 50% repeat scroll #ffffff;\" bgcolor=\"#ffffff\">");
            body.Append("<tbody>");
            body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            body.Append("<td align=\"left\" style=\"padding: 0pt; border-collapse: collapse !important; vertical-align: top; text-align: left;\">");
            body.Append("<table bgcolor=\"#fed048\" style=\"padding: 0pt; width: 100%; border-collapse: collapse; vertical-align: top; text-align: left; height: 97px; background: #fed048 0% 50%;\">");
            body.Append("<tbody>");
            body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            body.Append($"<td height=\"40\" align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\"><img style=\"margin-top: 20px;\" src=\"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/Content/img/upload/42logo.png\" alt=\"\" width=\"142\" height=\"48\" />&nbsp;</td>");
            body.Append("<td height=\"40\" align=\"right\" style=\"padding: 0pt; font-size: 22px; white-space: nowrap; text-align: right; vertical-align: middle; border-collapse: collapse !important;\">");
            body.Append("<p style=\"font-size: 22px; text-align: left;\">Ваша база восстановлена</p>");
            body.Append("</td>");
            body.Append("<td align=\"left\" style=\"padding: 0pt; border-collapse: collapse !important; vertical-align: top; text-align: left;\">");
            body.Append("<p>&nbsp;</p>");
            body.Append("<p>&nbsp;</p>");
            body.Append("</td>");
            body.Append("</tr>");
            body.Append("</tbody>");
            body.Append("</table>");
            body.Append("</td>");
            body.Append("</tr>");
            body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            body.Append("<td align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\">");
            body.Append("<table style=\"height: 154px;\" width=\"694\">");
            body.Append("<tbody>");
            body.Append("<tr>");
            body.Append("<td height=\"40\" align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important; width: 40px;\">&nbsp;</td>");
            body.Append("<td>");
            body.Append($"<p>Уважаемый пользователь, {userLogin}!</p>");
            body.Append($"<p>Восстановление архивной копии информационной базы &ldquo;{dbName}&rdquo; успешно завершено.</p>");
            body.Append(@"<p>База доступна в Вашем <a href=""https://cp.42clouds.com/AccountDatabases"">Личном кабинете</a>.</p>");
            body.Append("</br>");
            body.Append("<br>");
            body.Append("<p>С уважением, команда 42 Clouds</p>");
            body.Append("</td>");
            body.Append("<td align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important; width: 40px;\"></td>");
            body.Append("</tr>");
            body.Append("</tbody>");
            body.Append("</table>");
            body.Append("</td>");
            body.Append("</tr>");
            body.Append("</tbody>");
            body.Append("</table>");
            body.Append("</center>");
            body.Append("<table style=\"padding: 0pt; border-collapse: collapse; vertical-align: top; text-align: left;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">");
            body.Append("<tbody>");
            body.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            body.Append("<td height=\"15\" align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\">&nbsp;</td>");
            body.Append("</tr>");
            body.Append("</tbody>");
            body.Append("</table>");
            body.Append("<center>");

            return body.ToString();
        }
    }
}