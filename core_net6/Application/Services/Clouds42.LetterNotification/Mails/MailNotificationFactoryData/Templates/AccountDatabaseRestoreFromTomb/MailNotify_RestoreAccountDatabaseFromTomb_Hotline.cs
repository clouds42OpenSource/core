﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseRestoreFromTomb
{
    /// <summary>
    ///     Уведомление хотлайну в случае ошибки восстановления информационной базы из склепа
    /// </summary>
    public class MailNotifyHotlineRestoreAccountDatabaseFromTomb : IMailNotificationTemplate<string>
    {
        public void SendMail(IUnitOfWork unitOfWork, IAccessProvider accessProvider, string param, IAccountConfigurationDataProvider provider)
        {
            var cloudServices = ConfigurationHelper.GetConfigurationValue<string>("cloud-services");

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(cloudServices)
                .Subject("Проблема восстановления базы")
                .Body(param)
                .SendViaNewThread();
        }
    }
}