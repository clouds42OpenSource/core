﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseDeleteToTomb
{
    /// <summary>
    ///     Уведомление хотлайну в случае ошибки удаления информационной базы в склеп
    /// </summary>
    public class MailNotifySendToTombAccountDatabaseJobHotlineError : IMailNotificationTemplate<string>
    {
        public void SendMail(IUnitOfWork unitOfWork, IAccessProvider accessProvider, string param, IAccountConfigurationDataProvider provider)
        {
            var cloudServices = ConfigurationHelper.GetConfigurationValue<string>("cloud-services");

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(cloudServices)
                .Subject("Проблема удаления/архивации базы")
                .Body(param)
                .SendViaNewThread();
        }
    }
}