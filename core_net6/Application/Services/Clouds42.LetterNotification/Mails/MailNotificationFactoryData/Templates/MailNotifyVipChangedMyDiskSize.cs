﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates
{
    /// <summary>
    /// Notification when VIP account changes disk size
    /// </summary>
    public class MailNotifyVipChangedMyDiskSize : IMailNotificationTemplate<VipChangedMyDiskSizeMailDto>
    {
        public void SendMail(IUnitOfWork unitOfWork, IAccessProvider accessProvider, VipChangedMyDiskSizeMailDto param, IAccountConfigurationDataProvider provider)
        {
            var vipSupportEmail = ConfigurationHelper.GetConfigurationValue<string>("Emails.VipSupportEmail");

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(vipSupportEmail)
                .Subject("Изменение дискового пространства для ВИП-аккаунта.")
                .Body(CreateMailBody(param))
                .SendViaNewThread();
        }

        private static string CreateMailBody(VipChangedMyDiskSizeMailDto param) =>
            $"<p>Для аккаунта {param.AccountIndex} “{param.AccountName}” было изменено дисковое пространство с " +
            $"{param.OldSize} Гб на {param.NewSize} Гб.</p>" +
            $"\r\n<p>Необходимо проверить свободное место на VIP сервере.</p>";
    }

    /// <summary>
    /// Parameters for <see cref="MailNotifyVipChangedMyDiskSize"/> notification
    /// </summary>
    public class VipChangedMyDiskSizeMailDto
    {
        /// <summary>
        /// Disk size before change (GB)
        /// </summary>
        public int OldSize { get; set; }

        /// <summary>
        /// Disk size after change (GB)
        /// </summary>
        public int NewSize { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        public int AccountIndex { get; set; }

        /// <summary>
        /// Account name
        /// </summary>
        public string AccountName { get; set; }
    }
}
