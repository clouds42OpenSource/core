﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Mails.MailNotificationFactoryData
{
    /// <summary>
    ///     Фабрика для рассылки уведомлений
    /// </summary>
    public class MailNotificationFactory(
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        ILogger42 logger,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        ///     Отправить письмо
        /// </summary>
        /// <typeparam name="TTemplateMailNotification">Шаблон уведомления</typeparam>
        /// <typeparam name="TModelNotification">Данные для уведомления</typeparam>
        /// <param name="param"></param>
        public void SendMail<TTemplateMailNotification, TModelNotification>(TModelNotification param) 
            where TTemplateMailNotification: IMailNotificationTemplate<TModelNotification>, new ()
        {
            try
            {
                new TTemplateMailNotification().SendMail(unitOfWork, accessProvider, param, accountConfigurationDataProvider);
                logger.Info($"Отправка письма: Шаблон={typeof(TTemplateMailNotification).Name}; Данные={param.ToJson()};");
            }
            catch (Exception ex)
            {
                logger.Info($"Ошибка при отправке письма: {ex.GetFullInfo()}");
            }
        }
    }
}