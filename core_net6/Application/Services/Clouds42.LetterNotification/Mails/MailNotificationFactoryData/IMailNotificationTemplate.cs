﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Mails.MailNotificationFactoryData
{
    public interface IMailNotificationTemplate<in TModelParam>
    {
        void SendMail(IUnitOfWork unitOfWork, IAccessProvider accessProvider, TModelParam param, IAccountConfigurationDataProvider provider);

    }
}
