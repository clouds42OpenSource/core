﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification.Managers
{
    /// <summary>
    /// Менеджер для работы с шаблоном письма
    /// </summary>
    public class LetterTemplateManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ILetterTemplateProvider letterTemplateProvider,
        IEditLetterTemplateProvider editLetterTemplateProvider,
        IHandlerException handlerException,
        ISendTestLetterProvider sendTestLetterProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить модель Dto шаблона письма
        /// </summary>
        /// <param name="id">Id шаблона письма</param>
        /// <returns>Модель Dto шаблона письма</returns>
        public ManagerResult<LetterTemplateDetailsDto> GetLetterTemplateDto(int id)
        {
            var message = $"Получение модели шаблона письма {id}";
            logger.Info(message);
            try
            {
                AccessProvider.HasAccess(ObjectAction.LetterTemplate_Get, () => AccessProvider.ContextAccountId);
                var data = letterTemplateProvider.GetLetterTemplateDto(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось с ошибкой]");
                return PreconditionFailed<LetterTemplateDetailsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать шаблон письма
        /// </summary>
        /// <param name="editLetterTemplateDto">Модель редактирования шаблона письма</param>
        public ManagerResult EditLetterTemplate(EditLetterTemplateDto editLetterTemplateDto)
        {
            var message = $"Редактирование шаблона письма “{editLetterTemplateDto.Header}”";

            try
            {
                AccessProvider.HasAccessBool(ObjectAction.LetterTemplate_Edit);

                if (!editLetterTemplateDto.TryValidateModel(out var errorMessage))
                    return PreconditionFailed(errorMessage);

                editLetterTemplateProvider.Edit(editLetterTemplateDto);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.EditLetterTemplate, message);

                return Ok();
            }
            catch (Exception ex)
            {
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.EditLetterTemplate,
                    $"{message} завершилось с ошибкой. Причина: {ex.Message}");

                handlerException.Handle(ex, $"[{message} завершилось с ошибкой]");

                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Отправить тестовое письмо
        /// </summary>
        /// <param name="sendTestLetterDto">Модель данных для отправки тестового письма</param>
        public ManagerResult SendTestLetter(SendTestLetterDto sendTestLetterDto)
        {
            var message =
                $"Отправка тестового письма '{sendTestLetterDto.LetterTemplateDataForNotification.Header}' на почту '{sendTestLetterDto.Email}'";

            logger.Info(message);

            try
            {
                AccessProvider.HasAccess(ObjectAction.LetterTemplate_SendTestLetter);

                if (!sendTestLetterDto.TryValidateModel(out var errorMessage))
                    return PreconditionFailed(errorMessage);

                sendTestLetterProvider.Send(sendTestLetterDto);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.SendTestLetter, message);

                return Ok();
            }
            catch (Exception ex)
            {
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.SendTestLetter,
                    $"{message} завершилась с ошибкой. Причина: {ex.Message}");

                handlerException.Handle(ex, $"[{message} завершилась с ошибкой]");
                return PreconditionFailed<LetterTemplateDetailsDto>(ex.Message);
            }
        }
    }
}
