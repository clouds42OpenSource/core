﻿using Clouds42.Common.ManagersResults;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.Helpers;
using Clouds42.LetterNotification.LetterCreators.RegistrationUser;
using Clouds42.LetterNotification.LetterNotifications.Account;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.LetterNotification.LetterNotifications.AccountUser;
using Clouds42.LetterNotification.LetterNotifications.Billing;
using Clouds42.LetterNotification.LetterNotifications.Configuration1C;
using Clouds42.LetterNotification.LetterNotifications.CoreWorker;
using Clouds42.LetterNotification.LetterNotifications.Invoice;
using Clouds42.LetterNotification.LetterNotifications.Services;
using Clouds42.LetterNotification.Letters.Account;
using Clouds42.LetterNotification.Letters.AccountDatabase;
using Clouds42.LetterNotification.Letters.AccountUser;
using Clouds42.LetterNotification.Letters.AccountUser.RegistrationUser;
using Clouds42.LetterNotification.Letters.AccountUser.ResetPassword;
using Clouds42.LetterNotification.Letters.Billing;
using Clouds42.LetterNotification.Letters.Configuration1C;
using Clouds42.LetterNotification.Letters.CoreWorker;
using Clouds42.LetterNotification.Letters.Invoice;
using Clouds42.LetterNotification.Letters.Services;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.LetterNotification.Managers;
using Clouds42.LetterNotification.Processors;
using Clouds42.LetterNotification.Providers;
using Clouds42.LetterNotification.SendGrid.EmailClient;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddLetterEmailAddress(this IServiceCollection services)
        {
            services
                .AddTransient<IEmailAddressesDataProvider, EmailAddressesDataProvider>()
                .AddTransient<INotifiedAccountUserInfoProvider, NotifiedAccountUserInfoProvider>()
                .AddTransient<ILetterNotificationProcessor, LetterNotificationProcessor>()
                .AddTransient<ILetterTemplateProvider, LetterTemplateProvider>()
                .AddTransient<IEditLetterTemplateProvider, EditLetterTemplateProvider>()
                .AddTransient<ISendTestLetterProvider, SendTestLetterProvider>()
                .AddTransient<INotificationsBufferProvider, NotificationsBufferProvider>()
                .AddTransient<INotificationProcessor, NotificationProcessor>()
                .AddTransient<ManagerResult>()
                .AddTransient<ISendGridProvider, SendGridProvider>()
                .AddTransient<MailNotificationFactory>()
                .AddTransient<IMessagesManager, MessagesManager>()
                .AddTransient<TransferringAccountDataToTombLetterNotification>()
                .AddTransient<NewUpdateTemplatesLetterNotification>()
                .AddTransient<AuditChangeAccountSegmentOperationsLetterNotification>()
                .AddTransient<BeforeDeleteAccountDataLetterNotification>()
                .AddTransient<BeforeArchiveAccountDataLetterNotification>()
                .AddTransient<LetterTemplateTestInstanceCreator>()
                .AddTransient<BeforeSupportDatabaseLetterNotification>()
                .AddTransient<TehSupportResultLetterNotification>()
                .AddTransient<RestoreAccountDatabaseFromTombLetterNotification>()
                .AddTransient<PublishDatabaseLetterNotification>()
                .AddTransient<ProvidingAccessToDatabaseLetterNotification>()
                .AddTransient<CreateAccountDatabaseLetterNotification>()
                .AddTransient<ChangeDatabaseTypeLetterNotification>()
                .AddTransient<ConfirmationUserEmailLetterNotification>()
                .AddTransient<AutoUpdateResultLetterNotification>()
                .AddTransient<AuthorizationToDatabaseFailedLetterNotification>()
                .AddTransient<AccountDatabaseUsedLetterNotification>()
                .AddTransient<RegistrationUserLetterPromoCreator>()
                .AddTransient<RegistrationUserLetterDelansCreator>()
                .AddTransient<RegistrationUserLetterMcobCreator>()
                .AddTransient<RegistrationUserLetterKladovoyCreator>()
                .AddTransient<RegistrationUserLetterYashchenkoCreator>()
                .AddTransient<RegistrationUserLetterSauriCreator>()
                .AddTransient<RegistrationUserLetterProfitAccountCreator>()
                .AddTransient<RegistrationUserLetterEfsolCreator>()
                .AddTransient<RegistrationUserLetterDostavkaCreator>()
                .AddTransient<RegistrationUserLetterAbonCenterCreator>()
                .AddTransient<RegistrationUserLetterNotification>()
                .AddTransient<RegistrationAccountUserLetterNotification>()
                .AddTransient<BeforeChangeServiceCostLetterNotification>()
                .AddTransient<BeforeDeleteServiceTypesLetterNotification>()
                .AddTransient<ModerationServiceResultLetterNotification>()
                .AddTransient<DemoPeriodIsComingToEndLetterNotification>()
                .AddTransient<DemoPeriodEndedPaymentRequiredLetterNotification>()
                .AddTransient<ResetPasswordLetterNotification>()
                .AddTransient<BeforeServiceLockLetterNotification>()
                .AddTransient<TakingPromisePaymentLetterNotification>()
                .AddTransient<SendInvoiceToClientLetterNotification>()
                .AddTransient<LockServiceLetterNotification>()
                .AddTransient<Configurations1CNewReleasesLetterNotification>()
                .AddTransient<ServiceDemoPeriodExpiredLetterNotification>()
                .AddTransient<MainServiceResourcesChangesLetterNotification>()
                .AddTransient<LetterTemplateManager>()
                .AddTransient<ModerationServiceResultLetter>()
                .AddTransient<DemoPeriodIsComingToEndLetter>()
                .AddTransient<DemoPeriodEndedPaymentRequiredLetter>()
                .AddTransient<BeforeDeleteServiceTypesLetter>()
                .AddTransient<BeforeChangeServiceCostLetter>()
                .AddTransient<AuditCoreWorkerTasksQueueLetterNotification>()
                .AddTransient<RegistrationUserPromoLetter>()
                .AddTransient<RegistrationUserMcobLetter>()
                .AddTransient<RegistrationUserKladovoyLetter>()
                .AddTransient<RegistrationUserDelansLetter>()
                .AddTransient<RegistrationUserAbonCenterLetter>()
                .AddTransient<EmailVerificationSender>()
                .AddTransient<ResetPasswordSauriLetter>()
                .AddTransient<ResetPasswordKladovoyLetter>()
                .AddTransient<ResetPasswordDelansLetter>()
                .AddTransient<ConfirmationUserEmailLetter>()
                .AddTransient<AuditCoreWorkerTasksQueueLetter>()
                .AddTransient<AutoUpdateResultLetter>()
                .AddTransient<AutoUpdateResultUaLetter>()
                .AddTransient<TakingPromisePaymentLetter>()
                .AddTransient<SendInvoiceToClientLetter>()
                .AddTransient<LockServiceLetter>()
                .AddTransient<BeforeServiceLockLetter>()
                .AddTransient<Configurations1CNewReleasesLetter>()
                .AddTransient<MainServiceResourcesChangesLetter>()
                .AddTransient<ServiceDemoPeriodExpiredLetter>()
                .AddTransient<TehSupportResultUaLetter>()
                .AddTransient<TehSupportResultLetter>()
                .AddTransient<RestoreAccountDatabaseFromTombUaLetter>()
                .AddTransient<RestoreAccountDatabaseFromTombLetter>()
                .AddTransient<PublishDatabaseUaLetter>()
                .AddTransient<PublishDatabaseLetter>()
                .AddTransient<ProvidingAccessToDatabaseUaLetter>()
                .AddTransient<ProvidingAccessToDatabaseLetter>()
                .AddTransient<CreateAccountDatabaseUaLetter>()
                .AddTransient<CreateAccountDatabaseLetter>()
                .AddTransient<ChangeDatabaseTypeToServerLetter>()
                .AddTransient<ChangeDatabaseTypeToFileLetter>()
                .AddTransient<BeforeSupportDatabaseUaLetter>()
                .AddTransient<BeforeSupportDatabaseLetter>()
                .AddTransient<AuthorizationToDatabaseFailedUaLetter>()
                .AddTransient<AuthorizationToDatabaseFailedLetter>()
                .AddTransient<AccountDatabaseUsedLetter>()
                .AddTransient<TransferringAccountDataToTombUaLetter>()
                .AddTransient<TransferringAccountDataToTombLetter>()
                .AddTransient<NewUpdateTemplatesLetter>()
                .AddTransient<BeforeDeleteAccountDataUaLetter>()
                .AddTransient<BeforeDeleteAccountDataLetter>()
                .AddTransient<BeforeArchiveAccountDataUaLetter>()
                .AddTransient<BeforeArchiveAccountDataLetter>()
                .AddTransient<AuditChangeAccountSegmentOperationsLetter>()
                .AddTransient<ResetPasswordAbonCenterLetter>()
                .AddTransient<ResetPasswordPromoLetter>()
                .AddTransient<ISendGridEmailClient, SendGridEmailClient>();

            return services;
        }
    }
}
