﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.AdvertisingBanner.Mappers;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.DataModels.Mailing;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Getters;
using Clouds42.LetterNotification.Mappers;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification
{
    /// <summary>
    /// Базовый класс шаблона письма
    /// </summary>
    /// <typeparam name="TModel">Входящая модель</typeparam>
    public abstract class LetterTemplateBase<TModel>(IServiceProvider serviceProvider)
        : LetterTemplateTestBase(serviceProvider)
        where TModel : ILetterTemplateModel
    {
        private readonly IUnitOfWork _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly IAdvertisingBannerAudienceProvider _advertisingBannerAudienceProvider = serviceProvider.GetRequiredService<IAdvertisingBannerAudienceProvider>();
        private readonly IAdvertisingBannerHtmlBuilder _advertisingBannerHtmlBuilder = serviceProvider.GetRequiredService<IAdvertisingBannerHtmlBuilder>();

        /// <summary>
        /// Сайл ControlPanel
        /// </summary>
        private readonly Lazy<string> _cpSite = new(CloudConfigurationProvider.Cp.GetSiteAuthorityUrl);

        /// <summary>
        /// Путь для отказа от подписки уведомлений
        /// </summary>
        private readonly Lazy<string> _routeValueForUnsubscribe = new(CloudConfigurationProvider.Cp.GetRouteValueForUnsubscribe);

        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected abstract string ReplaceDataForCurrentHtmlLetterBody(TModel model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        protected abstract TModel GetTestDataToCurrentHtmlLetter();

        /// <summary>
        /// Признак, указывающий на необходимость
        /// показывать тэг отказа от подписки в письме
        /// </summary>
        /// <returns>Показывать тэг отказа от подписки</returns>
        protected virtual bool NeedShowUnsubscribeTag() => true;

        /// <summary>
        /// Получить дефолный урл сайта облака
        /// </summary>
        protected string GetDefaultCpSiteUrl() => _cpSite.Value;

        /// <summary>
        /// Получить общие данные для шаблона
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        public LetterTemplateCommonDataDto GetCommonLetterTemplateData(TModel model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo)
        {
            var letterTemplate = GetCurrentLetterTemplate();

            var data = GetCommonDataForCurrentLetter(model, notifiedAccountUserInfo,
                letterTemplate.MapToLetterTemplateDataForNotificationDto(GetAdvertisingBannerId(letterTemplate.Id)));

            return data;
        }

        /// <summary>
        /// Получить общие тестовые данные для шаблона
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="letterTemplateDataModel">Модель данных шаблона письма, которая используется для уведомления</param>
        /// <returns>Общие данные для шаблона письма</returns>
        public override LetterTemplateCommonDataDto GetCommonLetterTemplateTestData(Guid accountId,
            ILetterTemplateDataForNotificationModelDto letterTemplateDataModel) => GetCommonDataForCurrentLetter(
            GetTestDataToCurrentHtmlLetter(),
            GetNotifiedAccountUserInfo(accountId), letterTemplateDataModel, true);

        /// <summary>
        /// Получить ID шаблона SendGrid
        /// </summary>
        /// <returns>ID шаблона SendGrid</returns>
        public string GetSendGridTemplateId()
            => GetCurrentLetterTemplate().SendGridTemplateId;

        /// <summary>
        /// Получить тип текущего письма
        /// </summary>
        /// <returns>Тип текущего письма</returns>
        public string GetCurrentLetterTypeName() => GetType().Name;

        /// <summary>
        /// Получить общие данные для текущего письма 
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="letterTemplateDataModel">Модель данных шаблона письма, которая используется для уведомления</param>
        /// <param name="isTestLetter">Данные для тестового письма</param>
        private LetterTemplateCommonDataDto GetCommonDataForCurrentLetter(TModel model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo,
            ILetterTemplateDataForNotificationModelDto letterTemplateDataModel, bool isTestLetter = false)
        {
            var unsubscribeText = NeedShowUnsubscribeTag()
                ? LetterHtmlTemplateTextConst.UnsubscribeText
                : string.Empty;

            var companyContactInfo = CompanyContactInfoForLetterGetter.Get(notifiedAccountUserInfo.LocaleName?? "");

            return new LetterTemplateCommonDataDto
            {
                AdvertisingBannerData = GetAdvertisingBannerData(letterTemplateDataModel, notifiedAccountUserInfo.AccountId, isTestLetter),
                LetterTemplateData = new LetterTemplateDataModelDto
                {
                    Body = ReplaceDataForCurrentHtmlLetterBody(model, notifiedAccountUserInfo,
                        letterTemplateDataModel.Body),
                    Header = letterTemplateDataModel.Header,
                    Subject = letterTemplateDataModel.Subject,
                    NotificationText = string.IsNullOrEmpty(letterTemplateDataModel.NotificationText) ? null : ReplaceDataForCurrentHtmlLetterBody(model, notifiedAccountUserInfo, letterTemplateDataModel.NotificationText)
                },
                CompanyDetails = new CompanyDetailsDto
                {
                    Email = companyContactInfo.Email,
                    SiteLink = companyContactInfo.SiteLink,
                    TelegramLink = companyContactInfo.TelegramLink,
                    Phone = companyContactInfo.Phone,
                    NeedShowSocialMediaBlock = companyContactInfo.NeedShowSocialMediaBlock,
                    UnsubscribeText = unsubscribeText,
                    UnsubscribeLink = GetUrlForUnsubscribe(notifiedAccountUserInfo.Id)
                }
            };
        }

        /// <summary>
        /// Получить шаблон текущего письма
        /// </summary>
        /// <returns>Шаблон текущего письма</returns>
        private LetterTemplate GetCurrentLetterTemplate()
        {
            var letterTypeName = GetCurrentLetterTypeName();

            return _dbLayer.GetGenericRepository<LetterTemplate>()
                       .FirstOrDefault(lt => lt.LetterTypeName == letterTypeName) ??
                   throw new NotFoundException(
                       $"Не удалось получить шаблон для письма {letterTypeName}");
        }

        /// <summary>
        /// Получить данные рекламного баннера
        /// </summary>
        /// <param name="letterTemplateDataModel">Модель данных шаблона письма, которая используется для уведомления</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="isTestLetter">Данные для тестового письма</param>
        /// <returns>Модель данных рекламного баннера</returns>
        private AdvertisingBannerDataModelDto? GetAdvertisingBannerData(
            ILetterTemplateDataForNotificationModelDto letterTemplateDataModel, Guid accountId, bool isTestLetter)
        {
            if (!letterTemplateDataModel.AdvertisingBannerTemplateId.HasValue || !TryGetAdvertisingBannerTemplate(
                    letterTemplateDataModel.AdvertisingBannerTemplateId.Value, out var advertisingBannerTemplate))
                return null;

            if (!isTestLetter && !CanShowAdvertisingBanner(advertisingBannerTemplate, accountId))
                return null;

            return new AdvertisingBannerDataModelDto
            {
                HtmlData = _advertisingBannerHtmlBuilder.Build(advertisingBannerTemplate, accountId)
            };
        }

        /// <summary>
        /// Попытаться получить рекламный баннер
        /// </summary>
        /// <param name="advertisingBannerId">Id рекламного баннера</param>
        /// <param name="advertisingBannerTemplate">Шаблон рекламного баннера</param>
        /// <returns>Результат получения баннера</returns>
        private bool TryGetAdvertisingBannerTemplate(int advertisingBannerId,
            out AdvertisingBannerTemplate advertisingBannerTemplate)
        {
            advertisingBannerTemplate = _dbLayer.GetGenericRepository<AdvertisingBannerTemplate>()
                .FirstOrDefault(banner => banner.Id == advertisingBannerId);

            return advertisingBannerTemplate != null;
        }

        /// <summary>
        /// Можно ли показывать рекламный баннер
        /// </summary>
        /// <param name="advertisingBannerTemplate">Шаблон рекламного баннера</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Показывать рекламный баннер</returns>
        private bool CanShowAdvertisingBanner(AdvertisingBannerTemplate advertisingBannerTemplate,
            Guid accountId)
        {
            var currentDate = DateTime.Now;

            if (advertisingBannerTemplate.DisplayBannerDateFrom.HasValue &&
                currentDate < advertisingBannerTemplate.DisplayBannerDateFrom.Value ||
                advertisingBannerTemplate.DisplayBannerDateTo.HasValue &&
                currentDate > advertisingBannerTemplate.DisplayBannerDateTo.Value)
                return false;

            var dto = advertisingBannerTemplate.AdvertisingBannerAudience
                .MapToAdvertisingBannerAudienceDto();

            return _advertisingBannerAudienceProvider
                .SelectAdvertisingBannerAudience(dto.Locales, dto.AccountType, dto.AvailabilityPayment, dto.RentalServiceState)
                .Any(a => a.Id == accountId);
        }

        /// <summary>
        /// Получить урл для отказа от подписки
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <returns>Урл для отказа от подписки</returns>
        private string GetUrlForUnsubscribe(Guid userId) =>
            new Uri(
                    $"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/{_routeValueForUnsubscribe.Value}{SimpleIdHash.GetHashById(userId)}")
                .AbsoluteUri;

        /// <summary>
        /// Получить Id рекламного баннера
        /// </summary>
        /// <param name="letterId">Id шаблона письма</param>
        /// <returns>Id рекламного баннера</returns>
        private int? GetAdvertisingBannerId(int letterId) => _dbLayer
            .GetGenericRepository<LetterAdvertisingBannerRelation>()
            .FirstOrDefault(letterRelation => letterRelation.LetterTemplateId == letterId)?.AdvertisingBannerTemplateId;
    }
}
