﻿using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.CoreWorker
{
    /// <summary>
    /// Письмо о результатах аудита очереди задач воркеров
    /// </summary>
    public class AuditCoreWorkerTasksQueueLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<AuditCoreWorkerTasksQueueLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита очереди задач воркеров</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(AuditCoreWorkerTasksQueueLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody)
            => htmlLetterBody.Replace(LetterKeysConst.JobsWithStatusNewCount, model.JobsWithStatusNewCount.ToString())
                .Replace(LetterKeysConst.AuditCoreWorkerTasksQueueResults,
                    GenerateHtmlForAuditCoreWorkerTasksQueueResults(model.AuditCoreWorkerTasksQueueResults));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о результатах аудита очереди задач воркеров</returns>
        protected override AuditCoreWorkerTasksQueueLetterModelDto GetTestDataToCurrentHtmlLetter()
            => new()
            {
                JobsWithStatusNewCount = 30,
                AuditCoreWorkerTasksQueueResults = new List<AuditCoreWorkerTasksQueueResultDto>
                {
                    new()
                    {
                        TasksInQueueCount = 10,
                        CoreWorkerId = 1,
                        CoreWorkerName = "core-worker-1"
                    },
                    new()
                    {
                        TasksInQueueCount = 20,
                        CoreWorkerId = 2,
                        CoreWorkerName = "core-worker-2"
                    },
                    new()
                    {
                        TasksInQueueCount = 30,
                        CoreWorkerId = 3,
                        CoreWorkerName = "core-worker-3"
                    },
                    new()
                    {
                        TasksInQueueCount = 40,
                        CoreWorkerId = 4,
                        CoreWorkerName = "core-worker-4"
                    }
                }
            };

        /// <summary>
        /// Сформировать HTML для данных по очереди задач каждого воркера
        /// </summary>
        /// <param name="auditCoreWorkerTasksQueueResults">Данные по очереди задач для каждого воркера</param>
        /// <returns>HTML для данных по очереди задач каждого воркера</returns>
        private static string GenerateHtmlForAuditCoreWorkerTasksQueueResults(
            IEnumerable<AuditCoreWorkerTasksQueueResultDto> auditCoreWorkerTasksQueueResults) =>
            auditCoreWorkerTasksQueueResults.GenerateHtmlForListElements((item, index) =>
                $@"<tr>
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.CoreWorkerId)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.CoreWorkerName)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.TasksInQueueCount)}
                </tr>");
    }
}
