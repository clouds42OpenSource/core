﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.Services
{
    /// <summary>
    /// Письмо о скором удалении услуг сервиса
    /// </summary>
    public class BeforeDeleteServiceTypesLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<BeforeDeleteServiceTypesLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о скором удалении услуг сервиса</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(BeforeDeleteServiceTypesLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            model.ReplaceBaseDataForHtmlLetterBody(htmlLetterBody)
                .Replace(LetterKeysConst.ServiceTypesDeleteDate, $"{model.ServiceTypesDeleteDate:dd.MM.yyyy}")
                .Replace(LetterKeysConst.ServiceTypesToRemove,
                    GenerateHtmlAboutRemoveServiceTypes(model.ServiceTypeNamesToRemove));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о скором удалении услуг сервиса</returns>
        protected override BeforeDeleteServiceTypesLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                ServiceName = "Delans",
                ServiceOwnerEmail = "limpopo@mail.ru",
                ServiceOwnerPhoneNumber = "+79046185729",
                UrlForOpenBillingServiceCard = GetDefaultCpSiteUrl(),
                ServiceTypesDeleteDate = DateTime.Now.AddMonths(3),
                ServiceTypeNamesToRemove =
                [
                    "Delans Маршрутизация",
                    "Delans Telegram-бот",
                    "Delans Приложение Курьера"
                ]
            };

        /// <summary>
        /// Сформировать Html об удаленных услугах сервиса
        /// </summary>
        /// <param name="serviceTypeNamesToRemove">Названия услуг которые будут удалены</param>
        /// <returns>Html об удаленных услугах сервиса</returns>
        private string GenerateHtmlAboutRemoveServiceTypes(List<string> serviceTypeNamesToRemove) =>
            serviceTypeNamesToRemove.GenerateHtmlForListElements((serviceTypeName, index) =>
                $"<br> - Услуга “{serviceTypeName}” больше не будет предоставляться;");
    }
}