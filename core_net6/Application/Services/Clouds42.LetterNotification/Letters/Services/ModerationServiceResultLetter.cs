﻿using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Services
{
    /// <summary>
    /// Письмо для отправки результата модерации сервиса
    /// </summary>
    public class ModerationServiceResultLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<ModerationServiceResultLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма для отправки результата модерации сервиса</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(ModerationServiceResultLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.ServiceName, model.ServiceName)
                .Replace(LetterKeysConst.ModeratorComment,
                    model.ModerationResult.ModeratorComment)
                .Replace(LetterKeysConst.UrlForOpenBillingServiceCard,
                    model.UrlForOpenBillingServiceCard)
                .Replace(LetterKeysConst.ModerationResult,
                    GetModerationResultValue(model.ModerationResult.Status))
                .Replace(LetterKeysConst.TextOnEditServiceRequestDisplayValue,
                    GetDisplayValueForTextOnEditServiceRequest(model.ModerationResult.Status));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма для отправки результата модерации сервиса</returns>
        protected override ModerationServiceResultLetterModelDto GetTestDataToCurrentHtmlLetter() => 
            new()
            {
                ServiceName = "СуперТест",
                UrlForOpenBillingServiceCard = GetDefaultCpSiteUrl(),
                ModerationResult = new ChangeServiceRequestModerationResultDto
                {
                    ModeratorComment = "Укажите корректный файл инструкции",
                    Status = ChangeRequestStatusEnum.Rejected
                }
            };

        /// <summary>
        /// Получить значение результата модерации
        /// </summary>
        /// <param name="changeRequestStatus">Статус заявки на модерацию</param>
        /// <returns>Значение результата модерации</returns>
        private string GetModerationResultValue(ChangeRequestStatusEnum changeRequestStatus) =>
            changeRequestStatus == ChangeRequestStatusEnum.Rejected
                ? "отклонена"
                : "принята";

        /// <summary>
        /// Получить значение атрибута display
        /// для текста о возможности повтороно изменить
        /// заявку на редактирование сервиса
        /// </summary>
        /// <param name="changeRequestStatus">Статус заявки на модерацию</param>
        /// <returns>Значение результата модерации</returns>
        private string GetDisplayValueForTextOnEditServiceRequest(ChangeRequestStatusEnum changeRequestStatus) =>
            changeRequestStatus != ChangeRequestStatusEnum.Rejected
                ? LetterKeysConst.DisplayValueNone
                : string.Empty;
    }
}
