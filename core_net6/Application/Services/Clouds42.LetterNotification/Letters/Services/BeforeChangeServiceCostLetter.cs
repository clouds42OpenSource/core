﻿using Clouds42.DataContracts.Billing.RecalculateServiceCost;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.Services
{
    /// <summary>
    /// Письмо о скором изменении стоимости сервиса
    /// </summary>
    public class BeforeChangeServiceCostLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<BeforeChangeServiceCostLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о скором изменении стоимости сервиса</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(BeforeChangeServiceCostLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            model.ReplaceBaseDataForHtmlLetterBody(htmlLetterBody)
                .Replace(LetterKeysConst.ServiceCostChangeDate, $"{model.ServiceCostChangeDate:dd.MM.yyyy}")
                .Replace(LetterKeysConst.ServiceTypeCostChanges,
                    GenerateServiceTypeCostChangesHtml(model.ServiceTypeCostChanges,
                        notifiedAccountUserInfo.LocaleCurrency));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о скором изменении стоимости сервиса</returns>
        protected override BeforeChangeServiceCostLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                ServiceCostChangeDate = DateTime.Now.AddDays(10),
                ServiceName = "Delans",
                ServiceOwnerEmail = "limpopo@mail.ru",
                ServiceOwnerPhoneNumber = "+79046185729",
                UrlForOpenBillingServiceCard = GetDefaultCpSiteUrl(),
                ServiceTypeCostChanges =
                [
                    new() { Name = "Delans Маршрутизация", OldCost = 700, NewCost = 900 },

                    new() { Name = "Delans Telegram-бот", OldCost = 1300, NewCost = 100 }
                ]
            };

        /// <summary>
        /// Сформировать Html для изменений стоимости услуг
        /// </summary>
        /// <param name="serviceTypeCostChanges">Изменения стоимости услуг</param>
        /// <param name="localeCurrency">Валюта локали</param>
        /// <returns>Html для изменений стоимости услуг</returns>
        private string GenerateServiceTypeCostChangesHtml(List<ServiceTypeCostChangedDto> serviceTypeCostChanges,
            string localeCurrency) =>
            serviceTypeCostChanges.GenerateHtmlForListElements((stc, index) =>
                $"<br> - Стоимость услуги \"{stc.Name}\" изменилась с {stc.OldCost:00} {localeCurrency} на {stc.NewCost:00} {localeCurrency}");
    }
}