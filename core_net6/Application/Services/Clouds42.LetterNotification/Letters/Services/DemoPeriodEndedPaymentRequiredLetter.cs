﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Services
{
    /// <summary>
    /// Письма о уведомлении что демо период завершён, нужна оплата
    /// </summary>
    public class DemoPeriodEndedPaymentRequiredLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<DemoPeriodEndedPaymentRequiredLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        protected override DemoPeriodEndedPaymentRequiredLetterModelDto GetTestDataToCurrentHtmlLetter()
            => new()
            {
                ServiceName = "Тестовый сервис",
                ServiceId = Guid.Empty
            };

        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(DemoPeriodEndedPaymentRequiredLetterModelDto model, NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody)
            => htmlLetterBody
            .Replace(LetterKeysConst.ServiceName, model.ServiceName)
            .Replace(LetterKeysConst.UrlForOpenBillingServiceCard, model.BillingServicePageUrl);
    }
}
