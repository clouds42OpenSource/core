﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Services
{
    /// <summary>
    /// Письмо о уведомлении, что демо период скоро закончится
    /// </summary>
    public class DemoPeriodIsComingToEndLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<DemoPeriodIsComingToEndLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        protected override DemoPeriodIsComingToEndLetterModelDto GetTestDataToCurrentHtmlLetter()
            => new()
            {
                MoneyForService = 500,
                ServiceName = "Тестовый сервис",
                RemainingNumberOfDays = 2,
                ServiceId = Guid.Empty,
                BillingServicePageUrl = GetDefaultCpSiteUrl()
            };

        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(DemoPeriodIsComingToEndLetterModelDto model, NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody)
            => htmlLetterBody
            .Replace(LetterKeysConst.ServiceName, model.ServiceName)
            .Replace(LetterKeysConst.ServiceCost, model.MoneyForService.ToString("0.00"))
            .Replace(LetterKeysConst.LocaleCurrency, notifiedAccountUserInfo.LocaleCurrency)
            .Replace(LetterKeysConst.RemainingNumberOfDaysUntilTheEndOfTheDemoPeriod, model.RemainingNumberOfDays.ToString())
            .Replace(LetterKeysConst.UrlForOpenBillingServiceCard, model.BillingServicePageUrl);
    }
}
