﻿using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.Billing
{
    /// <summary>
    /// Письмо об изменении ресурсов главного сервиса (Аредна 1С)
    /// у вип аккаунтов
    /// </summary>
    public class MainServiceResourcesChangesLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<MainServiceResourcesChangesLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма об изменениях ресурсов главного сервиса (Аренда1С)</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(MainServiceResourcesChangesLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.ChangesDateFrom, model.ChangesDateFrom.ConvertToDateString())
                .Replace(LetterKeysConst.ChangesDateTo, model.ChangesDateTo.ConvertToDateString())
                .Replace(LetterKeysConst.MainServiceResourcesChanges,
                    GenerateHtmlForMainServiceResourcesChanges(model.MainServiceResourcesChanges));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма об изменениях ресурсов главного сервиса (Аренда1С)</returns>
        protected override MainServiceResourcesChangesLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                ChangesDateFrom = DateTime.Now,
                ChangesDateTo = DateTime.Now.AddDays(1),
                MainServiceResourcesChanges = new List<MainServiceResourcesChangeDto>
                {
                    new()
                    {
                        AccountCaption = "TestAccount",
                        AccountNumber = 215748,
                        SaleManagerLogin = "TestLogin",
                        EnabledResourcesCount = 5,
                        DisabledResourcesCount = 6
                    },
                    new()
                    {
                        AccountCaption = "TestAccount2",
                        AccountNumber = 1141,
                        EnabledResourcesCount = 8,
                        DisabledResourcesCount = 6
                    },
                    new()
                    {
                        AccountCaption = "Test",
                        AccountNumber = 147,
                        SaleManagerLogin = "TestLogin",
                        EnabledResourcesCount = 5
                    }
                }
            };

        /// <summary>
        /// Сформировать HTML для изменений ресурсов главного сервиса (Аренда1С)
        /// </summary>
        /// <param name="mainServiceResourcesChanges">Изменения ресурсов главного сервиса (Аренда1С)</param>
        /// <returns>HTML для изменений ресурсов главного сервиса (Аренда1С)</returns>
        private string GenerateHtmlForMainServiceResourcesChanges(
            IEnumerable<MainServiceResourcesChangeDto> mainServiceResourcesChanges) =>
            mainServiceResourcesChanges.GenerateHtmlForListElements((item, index) =>
                $@"<tr>
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.AccountNumber)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.AccountCaption)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.SaleManagerLogin ?? string.Empty)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(ConvertResourcesCountToString(item.DisabledResourcesCount))}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(ConvertResourcesCountToString(item.EnabledResourcesCount))}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(ConvertResourcesCountToString(item.TotalEnabledResourcesCount, true))}              
                </tr>");

        /// <summary>
        /// Перевести количество ресурсов в строковый формат
        /// </summary>
        /// <param name="resourcesCount">Количество ресурсов</param>
        /// <param name="needArithmeticOperator">Необходим арифметический оператор(+-)</param>
        /// <returns>Количество ресурсов в строковом формате</returns>
        private string ConvertResourcesCountToString(int resourcesCount, bool needArithmeticOperator = false)
        {
            var resourcesCountString = resourcesCount == 0 ? string.Empty : resourcesCount.ToString();

            if (!needArithmeticOperator)
                return resourcesCountString;

            return resourcesCount > 0 ? $"+{resourcesCountString}" : resourcesCountString;
        }
    }
}