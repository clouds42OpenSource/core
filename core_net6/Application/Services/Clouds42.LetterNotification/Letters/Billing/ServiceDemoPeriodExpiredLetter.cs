﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Billing
{
    /// <summary>
    /// Письмо о завершении демо периода сервиса
    /// </summary>
    public class ServiceDemoPeriodExpiredLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<ServiceDemoPeriodExpiredLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о завершении демо периода сервиса</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(ServiceDemoPeriodExpiredLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.ServiceName, model.ServiceName)
                .Replace(LetterKeysConst.BalancePageUrl, model.BalancePageUrl);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о завершении демо периода сервиса</returns>
        protected override ServiceDemoPeriodExpiredLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                BalancePageUrl = GetDefaultCpSiteUrl(),
                ServiceName = "СуперТест"
            };
    }
}