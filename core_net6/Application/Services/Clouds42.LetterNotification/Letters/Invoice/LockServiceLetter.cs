﻿using System.Text;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Invoice
{
    /// <summary>
    /// Письмо о блокировке сервиса
    /// </summary>
    public class LockServiceLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<LockServiceLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о блокировке сервиса</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(LockServiceLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) => htmlLetterBody
            .Replace(LetterKeysConst.ServiceNames, model.ServiceNames)
            .Replace(LetterKeysConst.LockReason, model.LockReason);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о блокировке сервиса</returns>
        protected override LockServiceLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                ServiceNames = "Аренда 1С, Delans, Sauri",
                LockReason = GenerateTestMessageAboutLackOfBalance()
            };

        /// <summary>
        /// Сгенерировать тестовое сообщение о недостаточном балансе
        /// </summary>
        /// <returns>Сообщение</returns>
        private string GenerateTestMessageAboutLackOfBalance()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("К сожалению, мы не получили от вас оплату за продление подписки. <br/> <br/>");
            builder.AppendLine(
                $@"Если вы хотите продолжить работу, пожалуйста, пополните баланс в личном кабинете или воспользуйтесь услугой «Обещанный платеж» (услуга доступна всем, кто хоть раз проводил оплату). 
                Активировать услугу можно в личном кабинете во вкладке <a href=""{GetDefaultCpSiteUrl()}"">«Баланс»</a>. <br/> <br/>");
            builder.AppendLine($"Стоимость сервисов: {100000:0.00} руб. <br/> <br/>");
            builder.AppendLine("Текущий баланс <br/>");
            builder.AppendLine($"&emsp;- основной счет: {9000:0.00} руб. <br/>");
            builder.AppendLine($"&emsp;- бонусный счет: {700:0.00} руб. <br/>");

            return builder.ToString();
        }
    }
}