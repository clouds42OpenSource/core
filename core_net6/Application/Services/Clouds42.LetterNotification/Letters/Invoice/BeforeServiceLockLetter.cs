﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Invoice
{
    /// <summary>
    /// Письмо о скорой блокировке сервиса
    /// </summary>
    public class BeforeServiceLockLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<BeforeServiceLockLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(BeforeServiceLockLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.LockServiceDate, $"{model.LockServiceDate:dd.MM.yyyy}")
                .Replace(LetterKeysConst.InvoicePeriod, model.InvoicePeriod.ToString())
                .Replace(LetterKeysConst.AccountBalance, $"{model.AccountBalance:0.00}")
                .Replace(LetterKeysConst.AccountBonusBalance, $"{model.AccountBonusBalance:0.00}")
                .Replace(LetterKeysConst.ServiceCost, $"{model.ServiceCost:0.00}")
                .Replace(LetterKeysConst.InvoiceSum, $"{model.InvoiceSum:0.00}")
                .Replace(LetterKeysConst.LocaleCurrency, notifiedAccountUserInfo.LocaleCurrency)
                .Replace(LetterKeysConst.BalancePageUrl, model.BalancePageUrl);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о скорой блокировке сервиса</returns>
        protected override BeforeServiceLockLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                AccountBalance = 9000.00M,
                AccountBonusBalance = 3567.00M,
                InvoicePeriod = 3,
                InvoiceSum = 6500.00M,
                ServiceCost = 3400.00M,
                LockServiceDate = DateTime.Now.AddDays(7),
                BalancePageUrl = GetDefaultCpSiteUrl()
            };
    }
}