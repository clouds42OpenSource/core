﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Invoice
{
    /// <summary>
    /// Письмо для отправки счета клиенту
    /// </summary>
    public class SendInvoiceToClientLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<SendInvoiceToClientLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма для отправки счета клиенту</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(SendInvoiceToClientLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.LocaleCurrency, notifiedAccountUserInfo.LocaleCurrency)
                .Replace(LetterKeysConst.InvoiceSum, $"{decimal.Round(model.InvoiceSum, 2)}")
                .Replace(LetterKeysConst.InvoiceNumber, model.InvoiceNumber)
                .Replace(LetterKeysConst.BalancePageUrl, model.BalancePageUrl);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма для отправки счета клиенту</returns>
        protected override SendInvoiceToClientLetterModelDto GetTestDataToCurrentHtmlLetter()
            => new()
            {
                InvoiceNumber = "420042",
                InvoiceSum = 0.00M,
                BalancePageUrl = GetDefaultCpSiteUrl()
            };

        /// <summary>
        /// Признак, указывающий на необходимость
        /// показывать тэг отказа от подписки в письме
        /// </summary>
        /// <returns>Показывать тэг отказа от подписки</returns>
        protected override bool NeedShowUnsubscribeTag() => false;
    }
}
