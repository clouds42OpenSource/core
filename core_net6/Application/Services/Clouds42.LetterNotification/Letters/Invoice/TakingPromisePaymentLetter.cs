﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Invoice
{
    /// <summary>
    /// Письмо о взятии обещанного платежа
    /// </summary>
    public class TakingPromisePaymentLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<TakingPromisePaymentLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о взятии обещанного платежа</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(TakingPromisePaymentLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.InvoiceSum, $"{decimal.Round(model.InvoiceSum, 2)}")
                .Replace(LetterKeysConst.InvoiceNumber, model.InvoiceNumber)
                .Replace(LetterKeysConst.LocaleCurrency, notifiedAccountUserInfo.LocaleCurrency)
                .Replace(LetterKeysConst.PromisePaymentDays, model.PromisePaymentDays.ToString());

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о взятии обещанного платежа</returns>
        protected override TakingPromisePaymentLetterModelDto GetTestDataToCurrentHtmlLetter() => 
            new()
            {
                InvoiceNumber = "420042",
                InvoiceSum = 700.00M,
                PromisePaymentDays = 7
            };
    }
}
