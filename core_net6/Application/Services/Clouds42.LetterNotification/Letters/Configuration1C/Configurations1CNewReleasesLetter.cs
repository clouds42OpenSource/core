﻿using Clouds42.DataContracts.Configurations1c;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;
using System.Text;

namespace Clouds42.LetterNotification.Letters.Configuration1C
{
    /// <summary>
    /// Письмо о новых релизах конфигураций
    /// </summary>
    public class Configurations1CNewReleasesLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<Configurations1CNewReleasesLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(Configurations1CNewReleasesLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) => htmlLetterBody
            .Replace(LetterKeysConst.Configurations1CNewReleasesInfo, GenerateHtmlForConfigurationsNewReleases(model.Configurations1CInfo));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма</returns>
        protected override Configurations1CNewReleasesLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                Configurations1CInfo =
                [
                    new()
                    {
                        Configuration1CName = "Test configuration",
                        ReleasesVersions = ["1.0", "1.1"]
                    }
                ]
            };

        /// <summary>
        /// Сгенерировать html для новых релизов конфигураций 1С
        /// </summary>
        /// <param name="configurations1CInfo">Список моделей информации о новых релизах конфигураций</param>
        /// <returns>HTML списка новых релизов</returns>
        private string GenerateHtmlForConfigurationsNewReleases(List<Configuration1CVersionsInfoDto> configurations1CInfo)
        {
            var htmlNewReleasesBuilder = new StringBuilder();
            configurations1CInfo.ForEach(conf => GenerateHtmlForConfigurationNewReleases(conf, htmlNewReleasesBuilder));
            return htmlNewReleasesBuilder.ToString();
        }

        /// <summary>
        /// Сгенерировать html для новых релизов конфигурации 1С
        /// </summary>
        /// <param name="configuration1CInfoDc">Модель информации о конфигурации</param>
        /// <param name="htmlNewReleasesBuilder">Билдер сообщения о новых релизах конфигураций</param>
        private void GenerateHtmlForConfigurationNewReleases(Configuration1CVersionsInfoDto configuration1CInfoDc, StringBuilder htmlNewReleasesBuilder)
        {
            htmlNewReleasesBuilder.AppendLine($@"<p>Новые релизы для конфигурации ""{configuration1CInfoDc.Configuration1CName}"":");
            htmlNewReleasesBuilder.AppendLine("<ul>");
            configuration1CInfoDc.ReleasesVersions.ForEach(version => { htmlNewReleasesBuilder.AppendLine($"<li>Версия: {version}</li>"); });
            htmlNewReleasesBuilder.AppendLine("</ul></p>");
        }

    }
}
