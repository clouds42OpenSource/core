﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;

namespace Clouds42.LetterNotification.Letters
{
    /// <summary>
    /// Тестовое письмо
    /// </summary>
    public class TestLetter(IServiceProvider serviceProvider) : LetterTemplateBase<TestLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель тестового письма</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(TestLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo,
            string htmlLetterBody) => htmlLetterBody;

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель тестового письма</returns>
        protected override TestLetterModelDto GetTestDataToCurrentHtmlLetter() => new();
    }
}
