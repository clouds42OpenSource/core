﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountUser.ResetPassword
{
    /// <summary>
    /// Письмо о восстановлении пароля пользователя "Промо-сайт"
    /// </summary>
    public class ResetPasswordPromoLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<ResetPasswordLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о восстановлении пароля пользователя</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(ResetPasswordLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.UrlForResetPassword, model.UrlForResetPassword);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о восстановлении пароля пользователя</returns>
        protected override ResetPasswordLetterModelDto GetTestDataToCurrentHtmlLetter() => new()
        {
            UrlForResetPassword = GetDefaultCpSiteUrl()
        };
    }
}