﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.AccountUser.RegistrationUser
{
    /// <summary>
    /// Письмо регистрации пользователя с источника "Kladovoy"
    /// </summary>
    public class RegistrationUserKladovoyLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<RegistrationUserLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(RegistrationUserLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            model.ReplaceBaseDataForCurrentHtmlLetterBody(htmlLetterBody);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о регистрации пользователя</returns>
        protected override RegistrationUserLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            RegistrationUserPromoLetterHelper.GetBaseTestDataToCurrentHtmlLetter();
    }
}
