﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountUser.RegistrationUser
{
    /// <summary>
    /// Письмо регистрации пользователя с источника "AbonCenter"
    /// </summary>
    public class RegistrationUserAbonCenterLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<RegistrationUserLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(RegistrationUserLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.AccountUserEmail, model.AccountUserEmail)
                .Replace(LetterKeysConst.AccountUserPwd, model.AccountUserPsd)
                .Replace(LetterKeysConst.UrlForResetPassword, model.UrlForResetPassword);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о регистрации пользователя</returns>
        protected override RegistrationUserLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                AccountUserLogin = "bigBoss",
                AccountUserPsd = "Qwerty_123",
                AccountUserEmail = "bigBoss@mail.ru"
            };
    }
}
