﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountUser
{
    /// <summary>
    /// Письмо о подтверждении email пользователя
    /// </summary>
    public class ConfirmationUserEmailLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<ConfirmationUserEmailLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о подтверждении email пользователя</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(ConfirmationUserEmailLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) => htmlLetterBody
            .Replace(LetterKeysConst.AccountUserLogin, model.AccountUserLogin)
            .Replace(LetterKeysConst.UrlForVerifyEmail, model.UrlForVerifyEmail);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о подтверждении email пользователя</returns>
        protected override ConfirmationUserEmailLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                AccountUserLogin = "MusicLover",
                UrlForVerifyEmail = GetDefaultCpSiteUrl()
            };
    }
}