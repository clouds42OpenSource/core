﻿using Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.Account
{
    /// <summary>
    /// Письмо о переносе данных аккаунта в склеп
    /// </summary>
    public class TransferringAccountDataToTombLetter(IServiceProvider serviceProvider)
        : TransferringAccountDataToTombBaseLetter(serviceProvider);
}