﻿using Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.Account
{
    /// <summary>
    /// Письмо о скорой архивации данных аккаунта для украинской локали
    /// </summary>
    public class BeforeArchiveAccountDataUaLetter(IServiceProvider serviceProvider)
        : BeforeArchiveAccountDataBaseLetter(serviceProvider);
}