﻿using Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.Account
{
    /// <summary>
    /// Письмо о переносе данных аккаунта в склеп для украинской локали
    /// </summary>
    public class TransferringAccountDataToTombUaLetter(IServiceProvider serviceProvider)
        : TransferringAccountDataToTombBaseLetter(serviceProvider);
}