﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о скором удалении данных аккаунта
    /// </summary>
    public abstract class BeforeDeleteAccountDataBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<BeforeDeleteAccountDataLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о скором удалении данных аккаунта</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(BeforeDeleteAccountDataLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.DaysToDelete, model.DaysToDelete.ToString());

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о скором удалении данных аккаунта</returns>
        protected override BeforeDeleteAccountDataLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                DaysToDelete = 2
            };
    }
}
