﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о скорой архивации данных аккаунта
    /// </summary>
    public abstract class BeforeArchiveAccountDataBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<BeforeArchiveAccountDataLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о скорой архивации данных аккаунта</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(BeforeArchiveAccountDataLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.DaysToArchive, model.DaysToArchive.ToString());

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о скорой архивации данных аккаунта</returns>
        protected override BeforeArchiveAccountDataLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                DaysToArchive = 5
            };
    }
}
