﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о переносе данных аккаунта в склеп
    /// </summary>
    public abstract class
        TransferringAccountDataToTombBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<TransferringAccountDataToTombLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о переносе данных аккаунта в склеп</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(TransferringAccountDataToTombLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) => htmlLetterBody.Replace(
            LetterKeysConst.LifetimeAccountDataInTombDaysCount, model.LifetimeAccountDataInTombDaysCount.ToString());

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о переносе данных аккаунта в склеп</returns>
        protected override TransferringAccountDataToTombLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                LifetimeAccountDataInTombDaysCount = 60
            };
    }
}