﻿using Clouds42.DataContracts.Account.AccountAudit;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.Account
{
    /// <summary>
    /// Письмо о результатах аудита операций по смене сегмента у аккаунтов
    /// </summary>
    public class AuditChangeAccountSegmentOperationsLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<AuditChangeAccountSegmentOperationsLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита операций по смене сегмента у аккаунтов</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(
            AuditChangeAccountSegmentOperationsLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody)
            => htmlLetterBody.Replace(LetterKeysConst.AccountsWithInvalidSegment,
                GenerateHtmlForAuditChangeAccountSegmentOperationsResults(
                    model.AccountsWithInvalidSegmentAfterMigration));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о результатах аудита операций по смене сегмента у аккаунтов</returns>
        protected override AuditChangeAccountSegmentOperationsLetterModelDto GetTestDataToCurrentHtmlLetter()
        {
            var auditChangeAccountSegmentOperationsLetterModel = new AuditChangeAccountSegmentOperationsLetterModelDto();
            for (var iterator = 1; iterator < 5; iterator++)
            {
                auditChangeAccountSegmentOperationsLetterModel.AccountsWithInvalidSegmentAfterMigration.Add(
                    new AccountWithInvalidSegmentAfterMigrationDto
                    {
                        AccountIndexNumber = iterator,
                        AccountCaption = $"Тестовый аккаунт №{iterator}",
                        OperationDateTime = DateTime.Now.AddHours(-iterator),
                        CurrentAccountSegmentName = $"Сегмент аккаунта №{iterator}",
                        TargetSegmentName = $"Новый сегмент аккаунта №{iterator}"
                    });
            }

            return auditChangeAccountSegmentOperationsLetterModel;
        }

        /// <summary>
        /// Сформировать HTML для данных аудита операций по смене сегмента у аккаунтов
        /// </summary>
        /// <param name="accountsWithInvalidSegmentAfterMigration">Аккаунты с неверным сегментом после миграции</param>
        /// <returns>HTML для данных по очереди задач каждого воркера</returns>
        private static string GenerateHtmlForAuditChangeAccountSegmentOperationsResults(
            IEnumerable<AccountWithInvalidSegmentAfterMigrationDto> accountsWithInvalidSegmentAfterMigration) =>
            accountsWithInvalidSegmentAfterMigration.GenerateHtmlForListElements((item, index) =>
                $@"<tr>
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell($"{item.AccountCaption} ({item.AccountIndexNumber})")}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.CurrentAccountSegmentName)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.TargetSegmentName)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell($"{item.OperationDateTime:dd.MM.yyyy HH:mm:ss}")}
                </tr>");
    }
}
