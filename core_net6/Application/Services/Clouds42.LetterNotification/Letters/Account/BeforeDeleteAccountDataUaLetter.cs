﻿using Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.Account
{
    /// <summary>
    /// Письмо о скором удалении данных аккаунта для украинской локали
    /// </summary>
    public class BeforeDeleteAccountDataUaLetter(IServiceProvider serviceProvider)
        : BeforeDeleteAccountDataBaseLetter(serviceProvider);
}
