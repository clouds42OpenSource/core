﻿using Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.Account
{
    /// <summary>
    /// Письмо о скорой архивации данных аккаунта
    /// </summary>
    public class BeforeArchiveAccountDataLetter(IServiceProvider serviceProvider)
        : BeforeArchiveAccountDataBaseLetter(serviceProvider);
}