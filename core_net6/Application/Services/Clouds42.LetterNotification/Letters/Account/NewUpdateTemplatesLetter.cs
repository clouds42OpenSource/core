﻿using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums.DbTemplateUpdates;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.Account
{
    /// <summary>
    /// Письмо со списком обновлённых файловых шаблонов
    /// </summary>
    public class NewUpdateTemplatesLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<NewUpdateTemplatesLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма со списком обновлённых файловых шаблонов</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(
            NewUpdateTemplatesLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, 
            string htmlLetterBody)
            => htmlLetterBody.Replace(LetterKeysConst.UpdatedFileTemplates,
                GenerateHtmlTableForNewUpdateTemplatesLetter(
                    model.ListWithUpdatedFileTemplates));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма со списком обновлённых файловых шаблонов</returns>
        protected override NewUpdateTemplatesLetterModelDto GetTestDataToCurrentHtmlLetter()
        {
            var newUpdateTemplatesLetterModel = new NewUpdateTemplatesLetterModelDto();
            for (var iterator = 1; iterator < 5; iterator++)
            {
                newUpdateTemplatesLetterModel.ListWithUpdatedFileTemplates.Add(
                    new UpdateDatabaseTemplateResultDto
                    {
                        UpdateDatabaseTemplateState = UpdateDatabaseTemplateStateEnum.Success,
                        TemplateDefaultCaption = $"Шаблон {iterator}",
                        CurrentVersion = $"Обновленная версия {iterator}",
                        UpdateTemplatePath = $"Путь до обновленного шаблона {iterator}",
                        NewVersion = $"Новая версия шаблона {iterator}"
                    });
            }

            return newUpdateTemplatesLetterModel;
        }

        /// <summary>
        /// Сформировать HTML для списка обновлённых файловых шаблонов
        /// </summary>
        /// <param name="listWithUpdatedFileTemplates">Список обновлённых файловых шаблонов</param>
        /// <returns>HTML для списка обновлённых файловых шаблонов</returns>
        private static string GenerateHtmlTableForNewUpdateTemplatesLetter(
            IEnumerable<UpdateDatabaseTemplateResultDto> listWithUpdatedFileTemplates) =>
            listWithUpdatedFileTemplates.GenerateHtmlForListElements((item, index) =>
                $@"<tr>
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell($"{item.TemplateDefaultCaption}")}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.CurrentVersion)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell(item.UpdateTemplatePath)}
                {LetterHtmlTemplateHelper.GenerateHtmlForTableCell($"{item.NewVersion}")}
                </tr>");
    }
}
