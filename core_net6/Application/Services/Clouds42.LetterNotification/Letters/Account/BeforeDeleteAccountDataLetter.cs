﻿using Clouds42.LetterNotification.Letters.Account.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.Account
{
    /// <summary>
    /// Письмо о скором удалении данных аккаунта
    /// </summary>
    public class BeforeDeleteAccountDataLetter(IServiceProvider serviceProvider)
        : BeforeDeleteAccountDataBaseLetter(serviceProvider);
}