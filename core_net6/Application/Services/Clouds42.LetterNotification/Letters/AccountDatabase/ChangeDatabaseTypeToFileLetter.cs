﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо об изменении типа базы на файловую
    /// </summary>
    public class ChangeDatabaseTypeToFileLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<ChangeDatabaseTypeLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма об изменении типа информационной базы(серверная/файловая)</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(ChangeDatabaseTypeLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            model.ReplaceDataForCurrentHtmlLetterBody(htmlLetterBody);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма об изменении типа информационной базы(серверная/файловая)</returns>
        protected override ChangeDatabaseTypeLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            ChangeDatabaseTypeLetterHelper.GetTestDataToCurrentHtmlLetter();
    }
}