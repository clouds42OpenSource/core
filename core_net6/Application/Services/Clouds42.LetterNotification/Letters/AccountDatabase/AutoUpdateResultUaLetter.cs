﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о результате проведения АО для украинской локали
    /// </summary>
    public class AutoUpdateResultUaLetter(IServiceProvider serviceProvider)
        : AutoUpdateResultBaseLetter(serviceProvider);
}
