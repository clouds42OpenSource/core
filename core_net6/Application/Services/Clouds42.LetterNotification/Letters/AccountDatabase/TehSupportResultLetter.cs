﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    ///  Письмо о результате проведения ТиИ
    /// </summary>
    public class TehSupportResultLetter(IServiceProvider serviceProvider) : TehSupportResultBaseLetter(serviceProvider);
}