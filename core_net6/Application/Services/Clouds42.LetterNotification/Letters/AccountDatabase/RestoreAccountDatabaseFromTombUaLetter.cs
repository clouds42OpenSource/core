﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о востановлении базы из склепа для украинской локали
    /// </summary>
    public class RestoreAccountDatabaseFromTombUaLetter(IServiceProvider serviceProvider)
        : RestoreAccountDatabaseFromTombBaseLetter(serviceProvider);
}
