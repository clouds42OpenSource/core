﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо об ошибочной авторизации в базу для украинской локали
    /// </summary>
    public class AuthorizationToDatabaseFailedUaLetter(IServiceProvider serviceProvider)
        : AuthorizationToDatabaseFailedBaseLetter(serviceProvider);
}
