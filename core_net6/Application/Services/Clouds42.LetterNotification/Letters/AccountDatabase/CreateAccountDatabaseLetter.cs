﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о создании инф. базы
    /// </summary>
    public class CreateAccountDatabaseLetter(IServiceProvider serviceProvider)
        : CreateAccountDatabaseBaseLetter(serviceProvider);
}
