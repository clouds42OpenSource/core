﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о создании инф. базы для украинской локали
    /// </summary>
    public class CreateAccountDatabaseUaLetter(IServiceProvider serviceProvider)
        : CreateAccountDatabaseBaseLetter(serviceProvider);
}
