﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    ///  Письмо о результате проведения ТиИ для украинской локали
    /// </summary>
    public class TehSupportResultUaLetter(IServiceProvider serviceProvider)
        : TehSupportResultBaseLetter(serviceProvider);
}
