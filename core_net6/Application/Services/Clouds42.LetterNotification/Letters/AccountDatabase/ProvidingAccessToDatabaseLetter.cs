﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о предоставлении доступа к базе
    /// </summary>
    public class ProvidingAccessToDatabaseLetter(IServiceProvider serviceProvider)
        : ProvidingAccessToDatabaseBaseLetter(serviceProvider);
}