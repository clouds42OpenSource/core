﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о скорых работах (ТиИ/АО) с базой для украинской локали
    /// </summary>
    public class BeforeSupportDatabaseUaLetter(IServiceProvider serviceProvider)
        : BeforeSupportDatabaseBaseLetter(serviceProvider);
}
