﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о публикации инф. базы
    /// </summary>
    public class PublishDatabaseLetter(IServiceProvider serviceProvider) : PublishDatabaseBaseLetter(serviceProvider);
}