﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о скорых работах (ТиИ/АО) с базой
    /// </summary>
    public class BeforeSupportDatabaseLetter(IServiceProvider serviceProvider)
        : BeforeSupportDatabaseBaseLetter(serviceProvider);
}