﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо уведомления об использовании инф. базы 
    /// </summary>
    public class AccountDatabaseUsedLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<AccountDatabaseUsedLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о предоставлении доступа к базе</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(AccountDatabaseUsedLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.AccountDatabaseCaption, model.DatabaseCaption);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о предоставлении доступа к базе</returns>
        protected override AccountDatabaseUsedLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                DatabaseCaption = "База для тестирования"
            };
    }
}
