﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о предоставлении доступа к базе для украинской локали
    /// </summary>
    public class ProvidingAccessToDatabaseUaLetter(IServiceProvider serviceProvider)
        : ProvidingAccessToDatabaseBaseLetter(serviceProvider);
}
