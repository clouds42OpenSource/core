﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо об ошибочной авторизации в базу
    /// </summary>
    public class AuthorizationToDatabaseFailedLetter(IServiceProvider serviceProvider)
        : AuthorizationToDatabaseFailedBaseLetter(serviceProvider);
}