﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о востановлении базы из склепа
    /// </summary>
    public class RestoreAccountDatabaseFromTombLetter(IServiceProvider serviceProvider)
        : RestoreAccountDatabaseFromTombBaseLetter(serviceProvider);
}