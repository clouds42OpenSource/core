﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о публикации инф. базы для украинской локали
    /// </summary>
    public class PublishDatabaseUaLetter(IServiceProvider serviceProvider) : PublishDatabaseBaseLetter(serviceProvider);
}
