﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма об ошибочной авторизации в базу
    /// </summary>
    public abstract class AuthorizationToDatabaseFailedBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<AuthorizationToDatabaseFailedLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма об ошибочной авторизации в базу</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(AuthorizationToDatabaseFailedLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.AccountDatabaseCaption, model.AccountDatabaseCaption);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма об ошибочной авторизации в базу</returns>
        protected override AuthorizationToDatabaseFailedLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                AccountDatabaseCaption = "Моя база для тестов"
            };
    }
}
