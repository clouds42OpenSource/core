﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о создании инф. базы
    /// </summary>
    public abstract class CreateAccountDatabaseBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<CreateAccountDatabaseLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(CreateAccountDatabaseLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody)
        {
            var currentHtmlLetterBody = htmlLetterBody.Replace(LetterKeysConst.LocaleCurrency, notifiedAccountUserInfo.LocaleCurrency)
                .Replace(LetterKeysConst.AccountDatabaseCaption, model.AccountDatabaseCaption)
                .Replace(LetterKeysConst.AccountDatabasePublishPath, model.AccountDatabasePublishPath);

            if (string.IsNullOrEmpty(model.AccountDatabasePublishPath))
                currentHtmlLetterBody =
                    currentHtmlLetterBody.Replace(LetterKeysConst.AcDbPublishPathDisplayValue,
                        LetterKeysConst.DisplayValueNone);

            return currentHtmlLetterBody;
        }

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма</returns>
        protected override CreateAccountDatabaseLetterModelDto GetTestDataToCurrentHtmlLetter()
            => new()
            {
                AccountDatabaseCaption = "Инф. база для теста",
                AccountDatabasePublishPath = GetDefaultCpSiteUrl()
            };
    }
}
