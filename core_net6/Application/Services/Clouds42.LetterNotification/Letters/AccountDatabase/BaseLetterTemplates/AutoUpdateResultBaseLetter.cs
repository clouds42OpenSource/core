﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о результате проведения АО
    /// </summary>
    public abstract class AutoUpdateResultBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<AutoUpdateResultLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о результате проведения АО</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(AutoUpdateResultLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.UrlForAccountDatabasesPage, model.UrlForAccountDatabasesPage)
                .Replace(LetterKeysConst.AutoUpdateSuccessesDatabasesDisplayValue,
                    model.AutoUpdateSuccessesDatabases.DefineByListHtmlDisplayValue())
                .Replace(LetterKeysConst.AutoUpdateFailureDatabasesDisplayValue,
                    model.AutoUpdateFailureDatabases.DefineByListHtmlDisplayValue())
                .Replace(LetterKeysConst.AutoUpdateFailureDatabases,
                    GenerateHtmlForAutoUpdateFailureDatabases(model.AutoUpdateFailureDatabases))
                .Replace(LetterKeysConst.AutoUpdateSuccessesDatabases,
                    GenerateHtmlForAutoUpdateSuccessesDatabases(model.AutoUpdateSuccessesDatabases));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о результате проведения АО</returns>
        protected override AutoUpdateResultLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                UrlForAccountDatabasesPage = GetDefaultCpSiteUrl(),
                AutoUpdateSuccessesDatabases = new List<AutoUpdateDatabaseSuccessesDto>
                {
                    new()
                    {
                        AccountDatabaseCaption = "Своя база №1",
                        NewVersion = "4.5.2"
                    },
                    new()
                    {
                        AccountDatabaseCaption = "Своя база №2",
                        NewVersion = "4.3.2"
                    }
                },
                AutoUpdateFailureDatabases = new List<AutoUpdateDatabaseFailureDto>
                {
                    new()
                    {
                        AccountDatabaseCaption = "Своя база №3",
                        Message = "наличие активных сессий в базе",
                        Description = "автообновление пройдет в следующие сутки, если не будет активных сессий"
                    },
                    new()
                    {
                        AccountDatabaseCaption = "Своя база №4",
                        Message = "неверные авторизационные данные",
                        Description = "Убедитесь, что: <br>&emsp;&emsp;1) Вы ввели корректные данные при авторизации. <br>&emsp;&emsp;2) Пользователь 1С является администратором в базе 1С."
                    }
                }
            };

        /// <summary>
        /// Сформировать Html для инф. баз, у которых АО завершилось успешно
        /// </summary>
        /// <param name="databases">Инф. базы, для которых АО завершилось успешно</param>
        /// <returns>Html для списка баз, которые успешно прошли АО</returns>
        private string GenerateHtmlForAutoUpdateSuccessesDatabases(
            IEnumerable<AutoUpdateDatabaseSuccessesDto> databases) => databases.GenerateHtmlForListElements(
            (db, index) => $"<br> {index + 1}. \"{db.AccountDatabaseCaption}\" выполнено обновление до версии релиза {db.NewVersion}.");

        /// <summary>
        /// Сформировать Html для инф. баз, у которых АО завершилось с ошибкой
        /// </summary>
        /// <param name="databases">Инф. базы, для которых АО завершилось с ошибкой</param>
        /// <returns>Html для списка баз, которые не прошли АО</returns>
        private string GenerateHtmlForAutoUpdateFailureDatabases(IEnumerable<AutoUpdateDatabaseFailureDto> databases) =>
            databases.GenerateHtmlForListElements((db, index) =>
                $"<br><br> Информационная база {db.AccountDatabaseCaption}. Причина: {db.Message} ({db.Description})");
    }
}
