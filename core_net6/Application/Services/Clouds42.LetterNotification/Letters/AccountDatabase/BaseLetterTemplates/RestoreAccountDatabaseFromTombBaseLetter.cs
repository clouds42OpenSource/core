﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о востановлении базы из склепа
    /// </summary>
    public abstract class RestoreAccountDatabaseFromTombBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<RestoreAccountDatabaseFromTombLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о востановлении базы из склепа</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(RestoreAccountDatabaseFromTombLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.AccountDatabaseCaption, model.DatabaseCaption)
                .Replace(LetterKeysConst.UrlForAccountDatabasesPage, model.UrlForAccountDatabasesPage);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о востановлении базы из склепа</returns>
        protected override RestoreAccountDatabaseFromTombLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                DatabaseCaption = "Тестовая база",
                UrlForAccountDatabasesPage = GetDefaultCpSiteUrl()
            };
    }
}
