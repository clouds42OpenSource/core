﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о скорых работах (ТиИ/АО) с базой
    /// </summary>
    public abstract class BeforeSupportDatabaseBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<BeforeSupportDatabaseLetterModelDto>(serviceProvider)
    {
        private readonly Dictionary<DatabaseSupportOperation, string> NameOfSupportOperations = new()
        {
            {DatabaseSupportOperation.PlanTehSupport, "Планирование тестирования и исправления (ТиИ)"},
            {DatabaseSupportOperation.PlanAutoUpdate, "Планирование автообновление"},
        };

        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о скорых работах (ТиИ/АО) с базой</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(BeforeSupportDatabaseLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody)
        {
            string databaseList = model.SupportOperation == DatabaseSupportOperation.PlanAutoUpdate
                ? GenerateHtmlForAutoUpdateDatabases(model.AutoUpdateDatabases)
                : GenerateHtmlForTehSupportDatabaseNames(model.TehSupportDatabaseNames);

            return htmlLetterBody
                .Replace(LetterKeysConst.Today, $"Сегодня ({DateTime.Now:dd.MM.yyyy})")
                .Replace(LetterKeysConst.SupportOperation, $"{NameOfSupportOperations[model.SupportOperation]})")
                .Replace(LetterKeysConst.SupportDatabaseList, $"{databaseList}");
        }


        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о скорых работах (ТиИ/АО) с базой</returns>
        protected override BeforeSupportDatabaseLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                TehSupportDatabaseNames = new List<string> {"Своя база №1", "Своя база №2", "Своя база №3",},
                AutoUpdateDatabases = new List<DatabaseSupportInfoDto>
                {
                    new() {Caption = "Своя база №1", CurrentVersion = "1.2.6", UpdateVersion = "4.2.6"},
                    new() {Caption = "Своя база №2", CurrentVersion = "2.2.6", UpdateVersion = "4.2.6"}
                }
            };

        /// <summary>
        /// Сформировать Html для списка баз, в которых будет проведено AO
        /// </summary>
        /// <param name="databases">Список баз, в которых будет проведено AO</param>
        /// <returns>Html для списка баз, в которых будет проведено AO</returns>
        private string GenerateHtmlForAutoUpdateDatabases(IEnumerable<DatabaseSupportInfoDto> databases) =>
            databases.GenerateHtmlForListElements((database, index) =>
                $"\n {index + 1}. \"{database.Caption}\"- текущий релиз {database.CurrentVersion}, актуальный - {database.UpdateVersion}. " +
                $" Время обновления : ({(int)database.TimeOfOperation} : 00)");

        /// <summary>
        /// Сформировать Html для списка названий баз, в которых будет проведено ТиИ
        /// </summary>
        /// <param name="databaseNames">Cписок названий баз, в которых будет проведено ТиИ</param>
        /// <returns>Html для списка названий баз, в которых будет проведено ТиИ</returns>
        private string GenerateHtmlForTehSupportDatabaseNames(IEnumerable<string> databaseNames) =>
            databaseNames.GenerateHtmlForListElements((dbName, index) => $"<br> {index + 1}. \"{dbName}\".");
    }
}
