﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о публикации инф. базы
    /// </summary>
    public abstract class PublishDatabaseBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<PublishDatabaseLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о публикации базы</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(PublishDatabaseLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) => htmlLetterBody
            .Replace(LetterKeysConst.AccountDatabaseCaption, model.AccountDatabaseCaption)
            .Replace(LetterKeysConst.AccountDatabasePublishPath, model.AccountDatabasePublishPath);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о публикации базы</returns>
        protected override PublishDatabaseLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                AccountDatabaseCaption = "MyDatabase",
                AccountDatabasePublishPath = GetDefaultCpSiteUrl()
            };
    }
}
