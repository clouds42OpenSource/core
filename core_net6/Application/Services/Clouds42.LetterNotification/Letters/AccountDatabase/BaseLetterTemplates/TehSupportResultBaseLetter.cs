﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Helpers;

namespace Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма о результате проведения ТиИ
    /// </summary>
    public abstract class TehSupportResultBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<TehSupportResultLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о результате проведения ТиИ</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(TehSupportResultLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.UrlForAccountDatabasesPage, model.UrlForAccountDatabasesPage)
                .Replace(LetterKeysConst.TehSupportFailureDatabasesDisplayValue,
                    model.TehSupportFailureDatabases.DefineByListHtmlDisplayValue())
                .Replace(LetterKeysConst.TehSupportSuccessesDatabaseNamesDisplayValue,
                    model.TehSupportSuccessesDatabaseNames.DefineByListHtmlDisplayValue())
                .Replace(LetterKeysConst.TehSupportFailureDatabases,
                    GenerateHtmlForTehSupportFailureDatabases(model.TehSupportFailureDatabases))
                .Replace(LetterKeysConst.TehSupportSuccessesDatabaseNames,
                    GenerateHtmlForTehSupportSuccessesDatabaseNames(model.TehSupportSuccessesDatabaseNames));

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о результате проведения ТиИ</returns>
        protected override TehSupportResultLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                UrlForAccountDatabasesPage = GetDefaultCpSiteUrl(),
                TehSupportSuccessesDatabaseNames = new List<string>
                {
                    "Своя база №1",
                    "Своя база №2",
                    "Своя база №3"
                },
                TehSupportFailureDatabases = new List<TehSupportDatabaseFailureDto>
                {
                    new()
                    {
                        AccountDatabaseCaption = "Своя база №4",
                        Message = "наличие активных сессий в базе",
                        Description = "ТиИ пройдет в следующие сутки, если не будет активных сессий"
                    },
                    new()
                    {
                        AccountDatabaseCaption = "Своя база №5",
                        Message = "неверные авторизационные данные",
                        Description =
                            "Убедитесь, что: <br>&emsp;&emsp;1) Вы ввели корректные данные при авторизации. <br>&emsp;&emsp;2) Пользователь 1С является администратором в базе 1С."
                    }
                }
            };

        /// <summary>
        /// Сформировать Html для названий инф. баз, которые успешно прошли ТиИ
        /// </summary>
        /// <param name="databaseNames">Названия инф. баз, которые успешно прошли ТиИ</param>
        /// <returns>Html для списка названий баз, которые успешно прошли ТиИ</returns>
        private string GenerateHtmlForTehSupportSuccessesDatabaseNames(IEnumerable<string> databaseNames) =>
            databaseNames.GenerateHtmlForListElements((dbName, index) => $"<br> {index + 1}. \"{dbName}\".");

        /// <summary>
        /// Сформировать Html для инф. баз,  для которых ТиИ завершилось с ошибкой
        /// </summary>
        /// <param name="databases">Инф. базы, для которых ТиИ завершилось с ошибкой</param>
        /// <returns>Html для инф. баз,  для которых ТиИ завершилось с ошибкой</returns>
        private string GenerateHtmlForTehSupportFailureDatabases(IEnumerable<TehSupportDatabaseFailureDto> databases) =>
            databases.GenerateHtmlForListElements((db, index) =>
                    $"<br><br> Информационная база \"{db.AccountDatabaseCaption}\". Причина - {db.Message}. ({db.Description})");
    }
}
