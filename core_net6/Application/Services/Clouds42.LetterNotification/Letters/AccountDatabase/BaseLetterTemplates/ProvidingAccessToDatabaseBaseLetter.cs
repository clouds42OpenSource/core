﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Constants;

namespace Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates
{
    /// <summary>
    /// Базовый класс письма письма о предоставлении доступа к базе
    /// </summary>
    public abstract class ProvidingAccessToDatabaseBaseLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<ProvidingAccessToDatabaseLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о предоставлении доступа к базе</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(ProvidingAccessToDatabaseLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody.Replace(LetterKeysConst.UrlForAccountDatabasesPage, model.UrlForAccountDatabasesPage)
                .Replace(LetterKeysConst.AccountDatabaseCaption, model.DatabaseCaption)
                .Replace(LetterKeysConst.AccountCaption, model.AccountCaption);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о предоставлении доступа к базе</returns>
        protected override ProvidingAccessToDatabaseLetterModelDto GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                AccountCaption = "Моя компания",
                DatabaseCaption = "База для тестирования",
                UrlForAccountDatabasesPage = GetDefaultCpSiteUrl()
            };
    }
}
