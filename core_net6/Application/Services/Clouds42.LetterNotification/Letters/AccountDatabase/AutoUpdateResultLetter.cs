﻿using Clouds42.LetterNotification.Letters.AccountDatabase.BaseLetterTemplates;

namespace Clouds42.LetterNotification.Letters.AccountDatabase
{
    /// <summary>
    /// Письмо о результате проведения АО
    /// </summary>
    public class AutoUpdateResultLetter(IServiceProvider serviceProvider) : AutoUpdateResultBaseLetter(serviceProvider);
}