﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.LetterNotification
{
    /// <summary>
    /// Менеджер сообщений
    /// </summary>
    public class MessagesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IMessagesManager
    {
        /// <summary>
        /// Отправить отчет воркера
        /// </summary>
        /// <param name="messageText">Текст сообщений</param>
        /// <param name="topic">Тема сообщения</param>
        /// <param name="eMail">Почта, на которую необходимо отправить письмо</param>
        public void SendWorkerReport(string messageText, string topic = "CoreWorker report",
            string? eMail = null)
        {
            AccessProvider.HasAccess(ObjectAction.Messages_Add, () => AccessProvider.ContextAccountId);
            var email = eMail ?? CloudConfigurationProvider.CoreWorker.GetWorkerReportMail();

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(email)
                .Body(messageText)
                .Subject(topic)
                .SendViaNewThread();
        }

        /// <summary>
        /// Отпарвить письмо воркера
        /// </summary>
        /// <param name="messageText">Текст сообщений</param>
        /// <param name="topiс">Тема сообщения</param>
        /// <param name="email">Почта, на которую необходимо отправить письмо</param>
        public void SendWorkerMail(string messageText, string topiс,
            string email)
        {
            AccessProvider.HasAccess(ObjectAction.Messages_Add, () => AccessProvider.ContextAccountId);
            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(email)
                .Body(messageText)
                .Subject(topiс)
                .SendViaNewThread();
        }
    }

}
