﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.DataModels.Mailing;

namespace Clouds42.LetterNotification.Mappers
{
    /// <summary>
    /// Маппер модели данных шаблона письма, которая используется для уведомления
    /// </summary>
    public static class LetterTemplateDataForNotificationDtoMapper
    {
        /// <summary>
        /// Выполнить маппинг к модели Dto
        /// </summary>
        /// <param name="letterTemplate">Шаблон письма</param>
        /// <param name="bannerTemplateId">Id шаблона рекламного баннера</param>
        /// <returns>Модель данных шаблона письма, которая используется для уведомления</returns>
        public static LetterTemplateDataForNotificationDto MapToLetterTemplateDataForNotificationDto(this LetterTemplate letterTemplate, int? bannerTemplateId) =>
            new()
            {
                Id = letterTemplate.Id,
                Header = letterTemplate.Header,
                Body = letterTemplate.Body,
                Subject = letterTemplate.Subject,
                SendGridTemplateId = letterTemplate.SendGridTemplateId,
                AdvertisingBannerTemplateId = bannerTemplateId,
                NotificationText = letterTemplate.NotificationText
            };
    }
}
