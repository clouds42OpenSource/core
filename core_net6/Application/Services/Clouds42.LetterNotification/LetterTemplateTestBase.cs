﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.LetterNotification.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification
{
    /// <summary>
    /// Базовый класс тестового шаблона письма
    /// </summary>
    public abstract class LetterTemplateTestBase(IServiceProvider serviceProvider)
    {
        private readonly INotifiedAccountUserInfoProvider _notifiedAccountUserInfoProvider = serviceProvider.GetRequiredService<INotifiedAccountUserInfoProvider>();

        /// <summary>
        /// Получить общие тестовые данные для шаблона
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="letterTemplateDataModel">Модель данных шаблона письма, которая используется для уведомления</param>
        /// <returns>Общие данные для шаблона письма</returns>
        public abstract LetterTemplateCommonDataDto GetCommonLetterTemplateTestData(Guid accountId,
            ILetterTemplateDataForNotificationModelDto letterTemplateDataModel);

        /// <summary>
        /// Получить информацию об уведомляемом пользователе
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информацию об уведомляемом пользователе</returns>
        protected NotifiedAccountUserInfoDto GetNotifiedAccountUserInfo(Guid accountId)
        {
            if (!_notifiedAccountUserInfoProvider.TryGet(out var notifiedAccountUserInfo, accountId))
                throw new NotFoundException(
                    $"Не удалось получить информацию об уведомляемом пользователе аккаунта {accountId}");

            return notifiedAccountUserInfo;
        }
    }
}
