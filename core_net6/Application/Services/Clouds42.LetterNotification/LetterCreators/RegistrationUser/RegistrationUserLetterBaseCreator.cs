﻿using System.Text;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Letters.AccountUser.RegistrationUser;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Базовый класс создатель письма регистрации пользователя
    /// </summary>
    public abstract class RegistrationUserLetterBaseCreator(IServiceProvider serviceProvider)
    {
        protected readonly IServiceProvider ServiceProvider = serviceProvider;
        protected readonly Lazy<string> SenderDescription = new(CloudConfigurationProvider.LetterNotification.GetSenderDescription);
        protected readonly Lazy<string> ManagerEmail = new(CloudConfigurationProvider.Emails.Get42CloudsManagerEmail);
        protected readonly Lazy<string> EfsolSaleEmail = new(CloudConfigurationProvider.Emails.GetEfsolSalesEmail);

        /// <summary>
        /// Сформировать сообщение для менеджера
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Сообщение для менеджера</returns>
        protected virtual string GenerateMessageForManager(RegistrationUserLetterModelDto model)
        {
            var builder = new StringBuilder();
            builder.Append("Через личный кабинет зарегистрирован новый клиент.");
            builder.Append("<br>");
            builder.Append($"Почта : {model.AccountUserEmail}");
            builder.Append("<br>");
            builder.Append($"Телефон : {model.AccountUserPhoneNumber}");
            builder.Append("<br>");

            if (!string.IsNullOrEmpty(model.UserSource))
                builder.Append($"Источник посещения пользователя: {model.UserSource}");

            return builder.ToString();
        }

        /// <summary>
        /// Создать шаблон письма
        /// </summary>
        /// <returns>Шаблон письма</returns>
        public virtual LetterTemplateBase<RegistrationUserLetterModelDto> CreateLetter() =>
            ServiceProvider.GetRequiredService<RegistrationUserPromoLetter>();

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public abstract Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model);
    }
}
