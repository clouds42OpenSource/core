﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Letters.AccountUser.RegistrationUser;
using Clouds42.Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "AbonCenter"
    /// </summary>
    public class RegistrationUserLetterAbonCenterCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        private readonly ILogger42 _logger = serviceProvider.GetRequiredService<ILogger42>();

        /// <summary>
        /// Создать шаблон письма
        /// </summary>
        /// <returns>Шаблон письма</returns>
        public override LetterTemplateBase<RegistrationUserLetterModelDto> CreateLetter() =>
            ServiceProvider.GetRequiredService<RegistrationUserAbonCenterLetter>();

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
            () => _logger.Info($"Регистрация нового клиента AbonCenter '{model.AccountUserLogin}'");
    }
}