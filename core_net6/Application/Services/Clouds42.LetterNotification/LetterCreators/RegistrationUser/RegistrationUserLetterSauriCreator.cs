﻿using Clouds42.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Mails.Efsol;
using System.Text;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "Sauri"
    /// </summary>
    public class RegistrationUserLetterSauriCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        private readonly Lazy<string> _sauriEmailManager = new(() =>
            ConfigurationHelper.GetConfigurationValue("SauriEmailManagersNotification"));

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
            () => new Support42CloudsMail()
                .DisplayName("Команда Sauri")
                .To(_sauriEmailManager.Value)
                .Subject("Sauri. Регистрация нового клиента")
                .Body(GenerateMessageForManager(model))
                .SendViaNewThread();

        /// <summary>
        /// Сформировать сообщение для менеджера
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Сообщение для менеджера</returns>
        protected override string GenerateMessageForManager(RegistrationUserLetterModelDto model)
        {
            var builder = new StringBuilder();
            builder.Append("Через личный кабинет зарегистрирован новый клиент.");
            builder.Append("<br>");
            builder.Append($"Почта : {model.AccountUserEmail}");
            builder.Append("<br>");
            builder.Append($"Логин : {model.AccountUserLogin}");
            builder.Append("<br>");
            builder.Append($"Телефон : {model.AccountUserPhoneNumber}");
            builder.Append("<br>");

            if (!string.IsNullOrEmpty(model.UserSource))
                builder.Append($"Источник посещения пользователя: {model.UserSource}");

            return builder.ToString();
        }
    }
}
