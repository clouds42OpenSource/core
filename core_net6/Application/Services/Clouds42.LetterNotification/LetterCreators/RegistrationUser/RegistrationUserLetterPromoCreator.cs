﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Mails.Promo;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "Промо-сайт"
    /// </summary>
    public class RegistrationUserLetterPromoCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
            () => new SupportPromoMail()
                .DisplayName(SenderDescription.Value)
                .To(ManagerEmail.Value)
                .Subject("ЛК. Регистрация нового клиента")
                .Body(GenerateMessageForManager(model))
                .SendViaNewThread();
    }
}
