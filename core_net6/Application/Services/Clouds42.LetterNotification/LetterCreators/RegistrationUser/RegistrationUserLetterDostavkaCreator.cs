﻿using Clouds42.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Mails.Efsol;
using System.Text;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "Dostavka"
    /// </summary>
    public class RegistrationUserLetterDostavkaCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
        () => new Support42CloudsMail()
            .DisplayName(SenderDescription.Value)
            .To(ConfigurationHelper.GetConfigurationValue("ManagerEMailDostavka"))
            .Subject("Мобильное приложение по доставке. Регистрация нового клиента")
            .Body(GenerateMessageForManager(model))
            .SendViaNewThread();

        /// <summary>
        /// Сформировать сообщение для менеджера
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Сообщение для менеджера</returns>
        protected override string GenerateMessageForManager(RegistrationUserLetterModelDto model)
        {
            var builder = new StringBuilder();
            builder.Append("Через мобильное приложение по доставке зарегистрирован новый клиент.");
            builder.Append("<br>");
            builder.Append($"Почта : {model.AccountUserEmail}");
            builder.Append("<br>");
            builder.Append($"Телефон : {model.AccountUserPhoneNumber}");
            return builder.ToString();
        }
    }
}
