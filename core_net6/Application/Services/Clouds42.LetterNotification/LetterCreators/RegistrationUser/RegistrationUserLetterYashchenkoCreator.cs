﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Mails.Promo;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "Yashchenko"
    /// </summary>
    public class RegistrationUserLetterYashchenkoCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        private readonly Lazy<string> _dmitriyYashchenkoEmail = new(CloudConfigurationProvider.Registration.GetDmitriyYashchenkoEmail);

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
            () => new SupportPromoMail()
                .DisplayName(SenderDescription.Value)
                .To(_dmitriyYashchenkoEmail.Value)
                .Copy(ManagerEmail.Value)
                .Subject("ЛК. Регистрация нового клиента Dmitriy Yashchenko")
                .Body(GenerateMessageForManager(model))
                .SendViaNewThread();
    }
}