﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Letters.AccountUser.RegistrationUser;
using Clouds42.LetterNotification.Mails.Efsol;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "Delans"
    /// </summary>
    public class RegistrationUserLetterDelansCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        private readonly Lazy<string> _delansManagerEmail = new(CloudConfigurationProvider.Registration.GetDelansManagerEmail);
        private readonly Lazy<string> _delansInfoEmail = new(CloudConfigurationProvider.Emails.GetDelansInfoEmail);

        /// <summary>
        /// Создать шаблон письма
        /// </summary>
        /// <returns>Шаблон письма</returns>
        public override LetterTemplateBase<RegistrationUserLetterModelDto> CreateLetter() =>
            ServiceProvider.GetRequiredService<RegistrationUserDelansLetter>();

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
        () => new Support42CloudsMail()
            .DisplayName("Команда 42Clouds")
            .To(_delansManagerEmail.Value)
            .Copy(_delansInfoEmail.Value)
            .Subject("ЛК. Регистрация нового клиента Delans")
            .Body(GenerateMessageForManager(model))
            .SendViaNewThread();
    }
}
