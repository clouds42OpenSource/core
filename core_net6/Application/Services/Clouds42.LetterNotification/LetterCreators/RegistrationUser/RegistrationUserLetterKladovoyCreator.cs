﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Letters.AccountUser.RegistrationUser;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Clouds42.LetterNotification.Mails.Promo;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "Kladovoy"
    /// </summary>
    public class RegistrationUserLetterKladovoyCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        private readonly Lazy<string> _kladovoyManagerEmail = new(CloudConfigurationProvider.Registration.GetKladovoyManagerEmail);
        private readonly Lazy<string> _kladovoySiteName = new(CloudConfigurationProvider.KladovoyService.GetKladovoySiteName);
        private readonly Lazy<string> _kladovoySiteUrl = new(CloudConfigurationProvider.KladovoyService.GetKladovoySiteUrl);

        /// <summary>
        /// Создать шаблон письма
        /// </summary>
        /// <returns>Шаблон письма</returns>
        public override LetterTemplateBase<RegistrationUserLetterModelDto> CreateLetter() =>
            ServiceProvider.GetRequiredService<RegistrationUserKladovoyLetter>();

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
            () => new SupportPromoMail()
                .DisplayName(SenderDescription.Value)
                .To(ManagerEmail.Value)
                .Copy(_kladovoyManagerEmail.Value)
                .Copy(EfsolSaleEmail.Value)
                .Subject("Кладовой. Регистрация нового клиента")
                .Body(GenerateMessageForManager(model))
                .SendViaNewThread();

        /// <summary>
        /// Сформировать сообщение для менеджера
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Сообщение для менеджера</returns>
        protected override string GenerateMessageForManager(RegistrationUserLetterModelDto model)
        {
            var builder = new StringBuilder();
            builder.Append(
                $"Регистрация на сервисе Кладовой <a href='{_kladovoySiteUrl.Value}'>{_kladovoySiteName.Value}</a>.");
            builder.Append("<br>");
            builder.Append($"Почта : {model.AccountUserEmail}");
            builder.Append("<br>");
            builder.Append($"Имя пользователя: {model.AccountUserFullName}");
            builder.Append("<br>");
            builder.Append($"Телефон : {model.AccountUserPhoneNumber}");
            return builder.ToString();
        }
    }
}