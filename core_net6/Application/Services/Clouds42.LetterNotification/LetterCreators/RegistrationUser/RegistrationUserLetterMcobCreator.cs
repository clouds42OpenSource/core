﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Letters.AccountUser.RegistrationUser;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Clouds42.LetterNotification.Mails.Promo;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "Mcob"
    /// </summary>
    public class RegistrationUserLetterMcobCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        private readonly Lazy<string> _mcobManagerEmail = new(CloudConfigurationProvider.Registration.GetMcobManagerEmail);

        /// <summary>
        /// Создать шаблон письма
        /// </summary>
        /// <returns>Шаблон письма</returns>
        public override LetterTemplateBase<RegistrationUserLetterModelDto> CreateLetter() =>
            ServiceProvider.GetRequiredService<RegistrationUserMcobLetter>();

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
            () => new SupportPromoMail()
                .DisplayName(SenderDescription.Value)
                .To(_mcobManagerEmail.Value)
                .Subject("ЛК. Регистрация нового клиента")
                .Body(GenerateMessageForManager(model))
                .SendViaNewThread();

        /// <summary>
        /// Сформировать сообщение для менеджера
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Сообщение для менеджера</returns>
        protected override string GenerateMessageForManager(RegistrationUserLetterModelDto model)
        {
            var builder = new StringBuilder();
            builder.Append("Через МЦОБ зарегистрирован новый клиент.");
            builder.Append("<br>");
            builder.Append($"Почта : {model.AccountUserEmail}");
            builder.Append("<br>");
            builder.Append($"Логин : {model.AccountUserLogin}");
            builder.Append("<br>");
            builder.Append($"Телефон : {model.AccountUserPhoneNumber}");
            builder.Append("<br>");

            if (!string.IsNullOrEmpty(model.UserSource))
                builder.Append($"Источник посещения пользователя: {model.UserSource}");

            return builder.ToString();
        }
    }
}