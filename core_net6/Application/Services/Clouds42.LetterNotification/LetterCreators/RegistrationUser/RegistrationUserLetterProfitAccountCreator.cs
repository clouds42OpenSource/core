﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Mails.Efsol;

namespace Clouds42.LetterNotification.LetterCreators.RegistrationUser
{
    /// <summary>
    /// Создатель письма регистрации пользователя с источника "ProfitAccount"
    /// </summary>
    public class RegistrationUserLetterProfitAccountCreator(IServiceProvider serviceProvider)
        : RegistrationUserLetterBaseCreator(serviceProvider)
    {
        private readonly Lazy<string> _profitAccountEmail = new(CloudConfigurationProvider.Registration.GetProfitAccountEmail);

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        public override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
            () => new Support42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(_profitAccountEmail.Value)
                .Copy(ManagerEmail.Value)
                .Subject("ЛК. Регистрация нового клиента Profit-Account")
                .Body(GenerateMessageForManager(model))
                .SendViaNewThread();
    }
}
