﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Services
{
    /// <summary>
    /// Уведомление о скором изменении стоимости сервиса
    /// </summary>
    public class BeforeChangeServiceCostLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<BeforeChangeServiceCostLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о скором изменении стоимости сервиса</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<BeforeChangeServiceCostLetterModelDto> GetLetterTemplate(
            BeforeChangeServiceCostLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<BeforeChangeServiceCostLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о скором изменении стоимости сервиса</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(BeforeChangeServiceCostLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о скором изменении стоимости сервиса</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(BeforeChangeServiceCostLetterModelDto model) =>
            $"BeforeChangeServiceCost=>Account={model.AccountId};ServiceChanges={model.BillingServiceChangesId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о скором изменении стоимости сервиса</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>  
        protected override DateTime GetEventKeyPeriod(BeforeChangeServiceCostLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о скором изменении стоимости сервиса</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(BeforeChangeServiceCostLetterModelDto model) =>
            model.EmailsToCopy;

        /// <summary>
        /// Получить Id пользователя аккаунта
        /// </summary>
        /// <param name="model">Модель письма о скором изменении стоимости сервиса</param>
        /// <returns>Id пользователя аккаунта</returns>
        protected override Guid? GetAccountUserId(BeforeChangeServiceCostLetterModelDto model) => model.AccountUserId;
    }
}
