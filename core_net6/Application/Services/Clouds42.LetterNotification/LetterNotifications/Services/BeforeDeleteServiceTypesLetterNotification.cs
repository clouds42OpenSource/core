﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Services
{
    /// <summary>
    /// Уведомление о скором удалении услуг сервиса
    /// </summary>
    public class
        BeforeDeleteServiceTypesLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<BeforeDeleteServiceTypesLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о скором удалении услуг сервиса</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<BeforeDeleteServiceTypesLetterModelDto> GetLetterTemplate(
            BeforeDeleteServiceTypesLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<BeforeDeleteServiceTypesLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о скором удалении услуг сервиса</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(BeforeDeleteServiceTypesLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о скором удалении услуг сервиса</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(BeforeDeleteServiceTypesLetterModelDto model) =>
            $"BeforeDeleteServiceTypes=>Account={model.AccountId};ServiceChanges={model.BillingServiceChangesId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о скором удалении услуг сервиса</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(BeforeDeleteServiceTypesLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о скором удалении услуг сервиса</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(BeforeDeleteServiceTypesLetterModelDto model) =>
            model.EmailsToCopy;

        /// <summary>
        /// Получить Id пользователя аккаунта
        /// </summary>
        /// <param name="model">Модель письма о скором удалении услуг сервиса</param>
        /// <returns>Id пользователя аккаунта</returns>
        protected override Guid? GetAccountUserId(BeforeDeleteServiceTypesLetterModelDto model) => model.AccountUserId;
    }
}
