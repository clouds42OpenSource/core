﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Services
{
    /// <summary>
    /// Уведомление о результате модерации сервиса
    /// </summary>
    public class ModerationServiceResultLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<ModerationServiceResultLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма для отправки результата модерации сервиса</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<ModerationServiceResultLetterModelDto> GetLetterTemplate(
            ModerationServiceResultLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<ModerationServiceResultLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма для отправки результата модерации сервиса</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(ModerationServiceResultLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма для отправки результата модерации сервиса</param>
        /// <returns>Ключ уникальности события</returns>  
        protected override string GetEventKey(ModerationServiceResultLetterModelDto model) =>
            $"ModerationResultService=>ChangeServiceRequest={model.ModerationResult.Id}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма для отправки результата модерации сервиса</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>  
        protected override DateTime GetEventKeyPeriod(ModerationServiceResultLetterModelDto model) =>
            DateTime.Now.AddMonths(3);
    }
}
