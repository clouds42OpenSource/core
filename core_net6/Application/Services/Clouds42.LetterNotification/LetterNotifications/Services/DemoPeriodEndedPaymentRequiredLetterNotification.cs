﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Services
{
    /// <summary>
    /// Уведомление о том что демо период завершён, нужна оплата
    /// </summary>
    public class DemoPeriodEndedPaymentRequiredLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<DemoPeriodEndedPaymentRequiredLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(DemoPeriodEndedPaymentRequiredLetterModelDto model) 
            => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Ключ уникальности события</returns>  
        protected override string GetEventKey(DemoPeriodEndedPaymentRequiredLetterModelDto model) 
            => $"DemoPeriodEndedPaymentRequiredLetter_{model.AccountId}_{model.ServiceName}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// (в случае о предупреждениях об оплате)
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns> 
        protected override DateTime GetEventKeyPeriod(DemoPeriodEndedPaymentRequiredLetterModelDto model) 
            => DateTime.Now;

        /// <summary>
        /// Получить шаблон письма
        /// (Фабричный метод)
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<DemoPeriodEndedPaymentRequiredLetterModelDto> 
            GetLetterTemplate(DemoPeriodEndedPaymentRequiredLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo)
            => ServiceProvider.GetRequiredService<DemoPeriodEndedPaymentRequiredLetter>();
    }
}
