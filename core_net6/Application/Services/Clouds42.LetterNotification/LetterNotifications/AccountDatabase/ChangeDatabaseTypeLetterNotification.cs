﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountDatabase;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.AccountDatabase
{
    /// <summary>
    /// Уведомление об изменении типа инф. базы
    /// </summary>
    public class ChangeDatabaseTypeLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<ChangeDatabaseTypeLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма об изменении типа информационной базы(серверная/файловая)</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<ChangeDatabaseTypeLetterModelDto> GetLetterTemplate(
            ChangeDatabaseTypeLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo)
        {
            if (model.HasChangedToServerType)
                return ServiceProvider.GetRequiredService<ChangeDatabaseTypeToServerLetter>();

            return ServiceProvider.GetRequiredService<ChangeDatabaseTypeToFileLetter>();
        }

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма об изменении типа информационной базы(серверная/файловая)</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(ChangeDatabaseTypeLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма об изменении типа информационной базы(серверная/файловая)</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(ChangeDatabaseTypeLetterModelDto model) =>
            $"ChangeDatabaseTypeLetterNotification_{model.AccountId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма об изменении типа информационной базы(серверная/файловая)</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(ChangeDatabaseTypeLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма об изменении типа информационной базы(серверная/файловая)</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(ChangeDatabaseTypeLetterModelDto model) =>
            [];

        /// <summary>
        /// Получить электронный адрес получателя
        /// </summary>
        /// <param name="model">Модель письма об изменении типа информационной базы(серверная/файловая)</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Электронный адрес получателя(email)</returns>
        protected override string GetReceiverEmail(ChangeDatabaseTypeLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo) => model.ReceiverEmail;
    }
}
