﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountDatabase;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.AccountDatabase
{
    /// <summary>
    /// Уведомление об использовании инф. базы
    /// </summary>
    public class AccountDatabaseUsedLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<AccountDatabaseUsedLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<AccountDatabaseUsedLetterModelDto> GetLetterTemplate(
            AccountDatabaseUsedLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo)
            => ServiceProvider.GetRequiredService<AccountDatabaseUsedLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(AccountDatabaseUsedLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <returns>Ключ уникальности события</returns> 
        protected override string GetEventKey(AccountDatabaseUsedLetterModelDto model) =>
            $"AccountDatabaseUsedLetterNotification_{model.AccountDatabaseId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о предоставлении доступа к базе</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(AccountDatabaseUsedLetterModelDto model) => DateTime.Now;
    }
}
