﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountDatabase;

namespace Clouds42.LetterNotification.LetterNotifications.AccountDatabase
{
    /// <summary>
    /// Уведомление клиента о создании инф. базы
    /// </summary>
    public class CreateAccountDatabaseLetterNotification(IServiceProvider serviceProvider) : LetterNotificationWithLocalizationBase<
        CreateAccountDatabaseLetterModelDto, CreateAccountDatabaseLetter, CreateAccountDatabaseUaLetter,
        CreateAccountDatabaseLetter, CreateAccountDatabaseLetter>(serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(CreateAccountDatabaseLetterModelDto model)
            => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <returns>Ключ уникальности события</returns>  
        protected override string GetEventKey(CreateAccountDatabaseLetterModelDto model)
            => $"CreateAccountDatabaseNotification=>AccountId=>{model.AccountId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// (в случае о предупреждениях об оплате)
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>  
        protected override DateTime GetEventKeyPeriod(CreateAccountDatabaseLetterModelDto model)
            => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(CreateAccountDatabaseLetterModelDto model) =>
            model.EmailsToCopy;
    }
}
