﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountDatabase;

namespace Clouds42.LetterNotification.LetterNotifications.AccountDatabase
{
    /// <summary>
    /// Уведомление об ошибочной авторизации в базу
    /// </summary>
    public class AuthorizationToDatabaseFailedLetterNotification(IServiceProvider serviceProvider) : LetterNotificationWithLocalizationBase<
        AuthorizationToDatabaseFailedLetterModelDto, AuthorizationToDatabaseFailedLetter,
        AuthorizationToDatabaseFailedUaLetter, AuthorizationToDatabaseFailedLetter, AuthorizationToDatabaseFailedLetter>(serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма об ошибочной авторизации в базу</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(AuthorizationToDatabaseFailedLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма об ошибочной авторизации в базу</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(AuthorizationToDatabaseFailedLetterModelDto model) =>
            $"AdminAuthorizationFailed=>Account={model.AccountId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма об ошибочной авторизации в базу</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(AuthorizationToDatabaseFailedLetterModelDto model) => DateTime.Now;
    }
}
