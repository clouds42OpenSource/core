﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountDatabase;

namespace Clouds42.LetterNotification.LetterNotifications.AccountDatabase
{
    /// <summary>
    /// Уведомление о скорых работах (ТиИ/АО) с базой
    /// </summary>
    public class BeforeSupportDatabaseLetterNotification(IServiceProvider serviceProvider) : LetterNotificationWithLocalizationBase<
        BeforeSupportDatabaseLetterModelDto, BeforeSupportDatabaseLetter, BeforeSupportDatabaseUaLetter,
        BeforeSupportDatabaseLetter, BeforeSupportDatabaseLetter>(serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о скорых работах (ТиИ/АО) с базой</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(BeforeSupportDatabaseLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о скорых работах (ТиИ/АО) с базой</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(BeforeSupportDatabaseLetterModelDto model) =>
            $"BeforeTehSupport=>Account={model.AccountId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о скорых работах (ТиИ/АО) с базой</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(BeforeSupportDatabaseLetterModelDto model) => DateTime.Now;
    }
}
