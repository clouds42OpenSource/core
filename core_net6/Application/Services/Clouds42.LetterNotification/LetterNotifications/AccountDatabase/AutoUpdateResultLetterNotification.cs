﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountDatabase;

namespace Clouds42.LetterNotification.LetterNotifications.AccountDatabase
{
    /// <summary>
    /// Уведомление о результате проведения АО
    /// </summary>
    public class AutoUpdateResultLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationWithLocalizationBase<AutoUpdateResultLetterModelDto,
            AutoUpdateResultLetter, AutoUpdateResultUaLetter, AutoUpdateResultLetter, AutoUpdateResultLetter>(
            serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о результате проведения АО</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(AutoUpdateResultLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о результате проведения АО</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(AutoUpdateResultLetterModelDto model) =>
            $"Update=>Account={model.AccountId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о результате проведения АО</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(AutoUpdateResultLetterModelDto model) => DateTime.Now;
    }
}
