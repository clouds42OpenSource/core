﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountDatabase;

namespace Clouds42.LetterNotification.LetterNotifications.AccountDatabase
{
    /// <summary>
    /// Уведомление о востановлении базы из склепа
    /// </summary>
    public class
        RestoreAccountDatabaseFromTombLetterNotification(IServiceProvider serviceProvider) : LetterNotificationWithLocalizationBase<
            RestoreAccountDatabaseFromTombLetterModelDto, RestoreAccountDatabaseFromTombLetter,
            RestoreAccountDatabaseFromTombUaLetter, RestoreAccountDatabaseFromTombLetter, RestoreAccountDatabaseFromTombLetter>(serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о востановлении базы из склепа</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(RestoreAccountDatabaseFromTombLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о востановлении базы из склепа</param>
        /// <returns>Ключ уникальности события</returns> 
        protected override string GetEventKey(RestoreAccountDatabaseFromTombLetterModelDto model) =>
            $"RestoreAccountDatabaseFromTombLetter_{model.AccountDatabaseId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о востановлении базы из склепа</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(RestoreAccountDatabaseFromTombLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о востановлении базы из склепа</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(RestoreAccountDatabaseFromTombLetterModelDto model) =>
            model.EmailsToCopy;
    }
}
