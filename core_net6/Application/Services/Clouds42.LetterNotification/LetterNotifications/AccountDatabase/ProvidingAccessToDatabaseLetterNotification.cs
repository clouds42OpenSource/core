﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountDatabase;

namespace Clouds42.LetterNotification.LetterNotifications.AccountDatabase
{
    /// <summary>
    /// Уведомление о предоставлении доступа к базе
    /// </summary>
    public class
        ProvidingAccessToDatabaseLetterNotification(IServiceProvider serviceProvider) : LetterNotificationWithLocalizationBase<
            ProvidingAccessToDatabaseLetterModelDto, ProvidingAccessToDatabaseLetter, ProvidingAccessToDatabaseUaLetter,
            ProvidingAccessToDatabaseLetter, ProvidingAccessToDatabaseLetter>(serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о предоставлении доступа к базе</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(ProvidingAccessToDatabaseLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о предоставлении доступа к базе</param>
        /// <returns>Ключ уникальности события</returns> 
        protected override string GetEventKey(ProvidingAccessToDatabaseLetterModelDto model) =>
            $"ProvidingAccessToDatabaseLetterNotification_{model.AccountDatabaseId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о предоставлении доступа к базе</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(ProvidingAccessToDatabaseLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить Id пользователя аккаунта
        /// </summary>
        /// <param name="model">Модель письма о предоставлении доступа к базе</param>
        /// <returns>Id пользователя аккаунта</returns>
        protected override Guid? GetAccountUserId(ProvidingAccessToDatabaseLetterModelDto model) => model.AccountUserId;
    }
}
