﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Account;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Account
{
    /// <summary>
    /// Уведомление о результатах аудита операций по смене сегмента у аккаунтов
    /// </summary>
    public class AuditChangeAccountSegmentOperationsLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<AuditChangeAccountSegmentOperationsLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита операций по смене сегмента у аккаунтов</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<AuditChangeAccountSegmentOperationsLetterModelDto> GetLetterTemplate(
            AuditChangeAccountSegmentOperationsLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo)
            => ServiceProvider.GetRequiredService<AuditChangeAccountSegmentOperationsLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита операций по смене сегмента у аккаунтов</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(AuditChangeAccountSegmentOperationsLetterModelDto model)
            => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита операций по смене сегмента у аккаунтов</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(AuditChangeAccountSegmentOperationsLetterModelDto model)
            => $"AuditChangeAccountSegmentOperationsLetterNotification_{model.ReceiverEmail}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита операций по смене сегмента у аккаунтов</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(AuditChangeAccountSegmentOperationsLetterModelDto model)
            => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита операций по смене сегмента у аккаунтов</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(AuditChangeAccountSegmentOperationsLetterModelDto model) =>
            [];

        /// <summary>
        /// Получить электронный адрес получателя
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита операций по смене сегмента у аккаунтов</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Электронный адрес получателя(email)</returns>
        protected override string GetReceiverEmail(AuditChangeAccountSegmentOperationsLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo) => model.ReceiverEmail;
    }
}
