﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Account;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Account
{
    /// <summary>
    /// Уведомление об обновлёных фаловых шаблонах
    /// </summary>
    public class NewUpdateTemplatesLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<NewUpdateTemplatesLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма со списком обновлённых фаловых шаблонов</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<NewUpdateTemplatesLetterModelDto> GetLetterTemplate(
            NewUpdateTemplatesLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo)
            => ServiceProvider.GetRequiredService<NewUpdateTemplatesLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма со списком обновлённых фаловых шаблонов</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(NewUpdateTemplatesLetterModelDto model)
            => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма со списком обновлённых фаловых шаблонов</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(NewUpdateTemplatesLetterModelDto model)
            => $"NewUpdateTemplatesLetterNotification_{model.ReceiverEmail}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма со списком обновлённых фаловых шаблонов</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(NewUpdateTemplatesLetterModelDto model)
            => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма со списком обновлённых фаловых шаблонов</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(NewUpdateTemplatesLetterModelDto model) =>
            [];

        /// <summary>
        /// Получить электронный адрес получателя
        /// </summary>
        /// <param name="model">Модель письма со списком обновлённых фаловых шаблонов</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Электронный адрес получателя(email)</returns>
        protected override string GetReceiverEmail(NewUpdateTemplatesLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo) => model.ReceiverEmail;
    }
}
