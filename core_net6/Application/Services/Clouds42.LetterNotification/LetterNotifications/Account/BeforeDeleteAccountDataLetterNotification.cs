﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Account;

namespace Clouds42.LetterNotification.LetterNotifications.Account
{
    /// <summary>
    /// Уведомление о скором удалении данных аккаунта
    /// </summary>
    public class BeforeDeleteAccountDataLetterNotification(IServiceProvider serviceProvider) : LetterNotificationWithLocalizationBase<
        BeforeDeleteAccountDataLetterModelDto, BeforeDeleteAccountDataLetter, BeforeDeleteAccountDataUaLetter,
        BeforeDeleteAccountDataLetter, BeforeDeleteAccountDataLetter>(serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о скором удалении данных аккаунта</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(BeforeDeleteAccountDataLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о скором удалении данных аккаунта</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(BeforeDeleteAccountDataLetterModelDto model) =>
            $"BeforeDelete=>Account={model.AccountId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о скором удалении данных аккаунта</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns> 
        protected override DateTime GetEventKeyPeriod(BeforeDeleteAccountDataLetterModelDto model) =>
            DateTime.Now.AddDays(model.DaysToDelete + 1);
    }
}
