﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Account;

namespace Clouds42.LetterNotification.LetterNotifications.Account
{
    /// <summary>
    /// Уведомление о переносе данных аккаунта в склеп
    /// </summary>
    public class
        TransferringAccountDataToTombLetterNotification(IServiceProvider serviceProvider) : LetterNotificationWithLocalizationBase<
            TransferringAccountDataToTombLetterModelDto, TransferringAccountDataToTombLetter,
            TransferringAccountDataToTombUaLetter, TransferringAccountDataToTombLetter, TransferringAccountDataToTombLetter>(serviceProvider)
    {
        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о переносе данных аккаунта в склеп</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(TransferringAccountDataToTombLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о переносе данных аккаунта в склеп</param>
        /// <returns>Ключ уникальности события</returns> 
        protected override string GetEventKey(TransferringAccountDataToTombLetterModelDto model) =>
            $"Archive=>Account={model.AccountId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о переносе данных аккаунта в склеп</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns> 
        protected override DateTime GetEventKeyPeriod(TransferringAccountDataToTombLetterModelDto model) =>
            DateTime.Now.AddDays(30);
    }
}
