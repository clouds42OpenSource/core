﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications
{
    /// <summary>
    /// Уведомление для отправки тестового письма
    /// </summary>
    public class TestLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<TestLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель тестового письма</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<TestLetterModelDto> GetLetterTemplate(TestLetterModelDto model,
            NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<TestLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель тестового письма</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(TestLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель тестового письма</param>
        /// <returns>Ключ уникальности события</returns> 
        protected override string GetEventKey(TestLetterModelDto model) => nameof(TestLetter);

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель тестового письма</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>  
        protected override DateTime GetEventKeyPeriod(TestLetterModelDto model) => DateTime.Now;
    }
}
