﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.IncomingModels;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Invoice
{
    /// <summary>
    /// Уведомление о скорой автооплате перед блокировкой сервиса
    /// </summary>
    public class BeforeServiceLockAutoPayLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<BeforeServiceLockAutoPayLetterModel>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<BeforeServiceLockAutoPayLetterModel> GetLetterTemplate(
            BeforeServiceLockAutoPayLetterModel model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<BeforeServiceLockAutoPayLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(BeforeServiceLockAutoPayLetterModel model) => model.Invoice != null
            ? new DocumentDataDto
            {
                FileName = model.Invoice.InvoiceDocFileName,
                Bytes = model.Invoice.InvoiceDocBytes
            }
            : null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(BeforeServiceLockAutoPayLetterModel model) =>
            $"BeforeServiceLock=>ServiceName={model.BillingServiceName}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns> 
        protected override DateTime GetEventKeyPeriod(BeforeServiceLockAutoPayLetterModel model) => model.LockServiceDate;
    }
}
