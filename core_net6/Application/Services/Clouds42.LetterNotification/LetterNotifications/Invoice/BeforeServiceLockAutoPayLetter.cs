﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.IncomingModels;

namespace Clouds42.LetterNotification.LetterNotifications.Invoice
{
    /// <summary>
    /// Письмо о скорой автооплате перед блокировкой сервиса
    /// </summary>
    public class BeforeServiceLockAutoPayLetter(IServiceProvider serviceProvider)
        : LetterTemplateBase<BeforeServiceLockAutoPayLetterModel>(serviceProvider)
    {
        /// <summary>
        /// Заменить данные для текущего тела Html письма
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <param name="htmlLetterBody">HTML тела письма</param>
        /// <returns>HTML письма</returns>
        protected override string ReplaceDataForCurrentHtmlLetterBody(BeforeServiceLockAutoPayLetterModel model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo, string htmlLetterBody) =>
            htmlLetterBody
                .Replace(LetterKeysConst.LockServiceDate, $"{model.LockServiceDate:dd.MM.yyyy}")
                .Replace(LetterKeysConst.PaymentMethod, model.PaymentMethod)
                .Replace(LetterKeysConst.ServiceName, model.BillingServiceName)
                .Replace(LetterKeysConst.AutoPayDate, $"{model.AutoPayDate:dd.MM.yyyy}")
                .Replace(LetterKeysConst.ServiceCost, $"{model.ServiceCost:0.00}")
                .Replace(LetterKeysConst.InvoiceSum, $"{model.InvoiceSum:0.00}")
                .Replace(LetterKeysConst.LocaleCurrency, "RUB")
                .Replace(LetterKeysConst.BalancePageUrl, model.BalancePageUrl);

        /// <summary>
        /// Получить тестовые данные для текущего HTML письма
        /// </summary>
        /// <returns>Модель письма о скорой блокировке сервиса</returns>
        protected override BeforeServiceLockAutoPayLetterModel GetTestDataToCurrentHtmlLetter() =>
            new()
            {
                InvoiceSum = 6500.00M,
                ServiceCost = 3400.00M,
                LockServiceDate = DateTime.Now.AddDays(7),
                BalancePageUrl = GetDefaultCpSiteUrl()
            };
    }
}
