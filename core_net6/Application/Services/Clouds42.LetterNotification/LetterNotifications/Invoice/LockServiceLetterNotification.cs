﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Invoice;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Invoice
{
    /// <summary>
    /// Уведомление о блокировке сервиса
    /// </summary>
    public class LockServiceLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<LockServiceLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о блокировке сервиса</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<LockServiceLetterModelDto> GetLetterTemplate(LockServiceLetterModelDto model,
            NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<LockServiceLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о блокировке сервиса</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(LockServiceLetterModelDto model) => model.InvoiceDocument;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о блокировке сервиса</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(LockServiceLetterModelDto model) =>
            $"ServiceLock=>ServiceName={model.ServiceNames}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о блокировке сервиса</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(LockServiceLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить письма для копий
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected override List<string> GetAccountEmails(LockServiceLetterModelDto model) => model.EmailsToCopy;
    }
}
