﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Invoice;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Invoice
{
    /// <summary>
    /// Уведомление о скорой блокировке сервиса
    /// </summary>
    public class BeforeServiceLockLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<BeforeServiceLockLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<BeforeServiceLockLetterModelDto> GetLetterTemplate(
            BeforeServiceLockLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<BeforeServiceLockLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(BeforeServiceLockLetterModelDto model) => model.Invoice != null
            ? new DocumentDataDto
            {
                FileName = model.Invoice.InvoiceDocFileName,
                Bytes = model.Invoice.InvoiceDocBytes
            }
            : null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(BeforeServiceLockLetterModelDto model) =>
            $"BeforeServiceLock=>ServiceName={model.BillingServiceName}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о скорой блокировке сервиса</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns> 
        protected override DateTime GetEventKeyPeriod(BeforeServiceLockLetterModelDto model) => model.LockServiceDate;


        /// <summary>
        /// Получить письма для копий
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected override List<string> GetAccountEmails(BeforeServiceLockLetterModelDto model) => model.EmailsToCopy;
    }
}
