﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Invoice;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Invoice
{
    /// <summary>
    /// Уведомление для отправки счета клиенту 
    /// </summary>
    public class SendInvoiceToClientLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<SendInvoiceToClientLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма для отправки счета клиенту</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<SendInvoiceToClientLetterModelDto> GetLetterTemplate(
            SendInvoiceToClientLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<SendInvoiceToClientLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма для отправки счета клиенту</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(SendInvoiceToClientLetterModelDto model) =>
            model.InvoiceDocument;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма для отправки счета клиенту</param>
        /// <returns>Ключ уникальности события</returns>  
        protected override string GetEventKey(SendInvoiceToClientLetterModelDto model) =>
            $"SendInvoiceNotificationToClient=>InvoiceId=>{model.InvoiceId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// (в случае о предупреждениях об оплате)
        /// </summary>
        /// <param name="model">Модель письма для отправки счета клиенту</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>  
        protected override DateTime GetEventKeyPeriod(SendInvoiceToClientLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма для отправки счета клиенту</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(SendInvoiceToClientLetterModelDto model) => model.EmailsToCopy;

        /// <summary>
        /// Получить Id пользователя аккаунта
        /// </summary>
        /// <param name="model">Модель письма для отправки счета клиенту</param>
        /// <returns>Id пользователя аккаунта</returns>
        protected override Guid? GetAccountUserId(SendInvoiceToClientLetterModelDto model) => model.AccountUserId;
    }
}
