﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Invoice;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Invoice
{
    /// <summary>
    /// Уведомление о взятии обещанного платежа
    /// </summary>
    public class TakingPromisePaymentLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<TakingPromisePaymentLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о взятии обещанного платежа</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<TakingPromisePaymentLetterModelDto> GetLetterTemplate(
            TakingPromisePaymentLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<TakingPromisePaymentLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о взятии обещанного платежа</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(TakingPromisePaymentLetterModelDto model) =>
            model.InvoiceDocument;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о взятии обещанного платежа</param>
        /// <returns>Ключ уникальности события</returns> 
        protected override string GetEventKey(TakingPromisePaymentLetterModelDto model) =>
            $"SendPromisePaymentNotificationToClient=>InvoiceId=>{model.InvoiceId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о взятии обещанного платежа</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns> 
        protected override DateTime GetEventKeyPeriod(TakingPromisePaymentLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о взятии обещанного платежа</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(TakingPromisePaymentLetterModelDto model) => model.EmailsToCopy;
    }
}
