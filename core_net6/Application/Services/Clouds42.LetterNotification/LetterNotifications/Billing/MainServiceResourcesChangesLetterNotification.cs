﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Billing;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Billing
{
    /// <summary>
    /// Уведомление об изменении ресурсов главного сервиса (Аредна 1С)
    /// у вип аккаунтов
    /// </summary>
    public class
        MainServiceResourcesChangesLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<MainServiceResourcesChangesLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма об изменениях ресурсов главного сервиса (Аренда1С)</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<MainServiceResourcesChangesLetterModelDto>
            GetLetterTemplate(MainServiceResourcesChangesLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<MainServiceResourcesChangesLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма об изменениях ресурсов главного сервиса (Аренда1С)</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(MainServiceResourcesChangesLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма об изменениях ресурсов главного сервиса (Аренда1С)</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(MainServiceResourcesChangesLetterModelDto model) =>
            $"MainServiceResourcesChangesLetterNotification_{model.ReceiverEmail}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма об изменениях ресурсов главного сервиса (Аренда1С)</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(MainServiceResourcesChangesLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма об изменениях ресурсов главного сервиса (Аренда1С)</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(MainServiceResourcesChangesLetterModelDto model) =>
            [];

        /// <summary>
        /// Получить электронный адрес получателя
        /// </summary>
        /// <param name="model">Модель письма об изменениях ресурсов главного сервиса (Аренда1С)</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Электронный адрес получателя(email)</returns>
        protected override string GetReceiverEmail(MainServiceResourcesChangesLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo) => model.ReceiverEmail;
    }
}
