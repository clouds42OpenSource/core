﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.Billing;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.Billing
{
    /// <summary>
    /// Уведомление о завершении демо периода сервиса
    /// </summary>
    public class
        ServiceDemoPeriodExpiredLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<ServiceDemoPeriodExpiredLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о завершении демо периода сервиса</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<ServiceDemoPeriodExpiredLetterModelDto> GetLetterTemplate(
            ServiceDemoPeriodExpiredLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<ServiceDemoPeriodExpiredLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о завершении демо периода сервиса</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(ServiceDemoPeriodExpiredLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о завершении демо периода сервиса</param>
        /// <returns>Ключ уникальности события</returns>  
        protected override string GetEventKey(ServiceDemoPeriodExpiredLetterModelDto model) =>
            $"ServiceLock=>ServiceId={model.ServiceId}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о завершении демо периода сервиса</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns> 
        protected override DateTime GetEventKeyPeriod(ServiceDemoPeriodExpiredLetterModelDto model) => DateTime.Now;
    }
}
