﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.CoreWorker;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.CoreWorker
{
    /// <summary>
    /// Уведомление о результатах аудита очереди задач воркеров 
    /// </summary>
    public class
        AuditCoreWorkerTasksQueueLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<AuditCoreWorkerTasksQueueLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита очереди задач воркеров</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<AuditCoreWorkerTasksQueueLetterModelDto> GetLetterTemplate(
            AuditCoreWorkerTasksQueueLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo)
            => ServiceProvider.GetRequiredService<AuditCoreWorkerTasksQueueLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита очереди задач воркеров</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(AuditCoreWorkerTasksQueueLetterModelDto model)
            => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита очереди задач воркеров</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(AuditCoreWorkerTasksQueueLetterModelDto model)
            => $"AuditCoreWorkerTasksQueueLetterNotification_{model.ReceiverEmail}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита очереди задач воркеров</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(AuditCoreWorkerTasksQueueLetterModelDto model)
            => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита очереди задач воркеров</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(AuditCoreWorkerTasksQueueLetterModelDto model) =>
            [];

        /// <summary>
        /// Получить электронный адрес получателя
        /// </summary>
        /// <param name="model">Модель письма о результатах аудита очереди задач воркеров</param>
        /// <param name="notifiedAccountUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Электронный адрес получателя(email)</returns>
        protected override string GetReceiverEmail(AuditCoreWorkerTasksQueueLetterModelDto model,
            NotifiedAccountUserInfoDto notifiedAccountUserInfo) => model.ReceiverEmail;
    }
}
