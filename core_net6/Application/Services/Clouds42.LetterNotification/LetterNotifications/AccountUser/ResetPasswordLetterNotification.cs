﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountUser.ResetPassword;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.AccountUser
{
    /// <summary>
    /// Уведомление о восстановлении пароля пользователя
    /// </summary>
    public class ResetPasswordLetterNotification : LetterNotificationBase<ResetPasswordLetterModelDto>
    {
        /// <summary>
        /// Список создателей писем 
        /// </summary>
        private readonly
            List<(Func<ExternalClient, bool> CompareExternalClient, Func<LetterTemplateBase<ResetPasswordLetterModelDto>>
                CreateLetter
                )> _listLetterCreators;

        public ResetPasswordLetterNotification(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _listLetterCreators =
            [
                (externalClient => externalClient == ExternalClient.Promo,
                    () => ServiceProvider.GetRequiredService<ResetPasswordPromoLetter>()),

                (externalClient => externalClient == ExternalClient.Delans,
                    () => ServiceProvider.GetRequiredService<ResetPasswordDelansLetter>()),

                (externalClient => externalClient == ExternalClient.Kladovoy,
                    () => ServiceProvider.GetRequiredService<ResetPasswordKladovoyLetter>()),

                (externalClient => externalClient == ExternalClient.AbonCenter,
                    () => ServiceProvider.GetRequiredService<ResetPasswordAbonCenterLetter>()),

                (externalClient => externalClient == ExternalClient.Sauri,
                    () => ServiceProvider.GetRequiredService<ResetPasswordSauriLetter>())
            ];
        }

        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о восстановлении пароля пользователя</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<ResetPasswordLetterModelDto> GetLetterTemplate(
            ResetPasswordLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo)
        {
            var foundCreator = _listLetterCreators
                .FirstOrDefault(creator => creator.CompareExternalClient(model.ExternalClient));

            return foundCreator != default
                ? foundCreator.CreateLetter()
                : ServiceProvider.GetRequiredService<ResetPasswordPromoLetter>();
        }

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о восстановлении пароля пользователя</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(ResetPasswordLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о восстановлении пароля пользователя</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(ResetPasswordLetterModelDto model) =>
            $"ResetPasswordLetter_{model.AccountUserLogin}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о восстановлении пароля пользователя</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(ResetPasswordLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о восстановлении пароля пользователя</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(ResetPasswordLetterModelDto model) =>
            [];

        /// <summary>
        /// Получить Id пользователя аккаунта
        /// </summary>
        /// <param name="model">Модель письма о восстановлении пароля пользователя</param>
        /// <returns>Id пользователя аккаунта</returns>
        protected override Guid? GetAccountUserId(ResetPasswordLetterModelDto model) => model.AccountUserId;
    }
}
