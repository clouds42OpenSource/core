﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.LetterCreators.RegistrationUser;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.AccountUser
{
    /// <summary>
    /// Уведомление о регистрации пользователя
    /// </summary>
    public class RegistrationUserLetterNotification : LetterNotificationBase<RegistrationUserLetterModelDto>
    {
        /// <summary>
        /// Создатели писем о регистрации пользователя
        /// </summary>
        private readonly
            List<(Func<ExternalClient, bool> CompareExternalClient, Func<RegistrationUserLetterBaseCreator>
                GetLetterCreator)> _letterCreators;

        public RegistrationUserLetterNotification(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _letterCreators =
            [
                (externalClient => externalClient == ExternalClient.Promo,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterPromoCreator>()),

                (externalClient => externalClient == ExternalClient.AbonCenter,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterAbonCenterCreator>()),

                (externalClient => externalClient == ExternalClient.Delans,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterDelansCreator>()),

                (externalClient => externalClient == ExternalClient.Dostavka,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterDostavkaCreator>()),

                (externalClient => externalClient == ExternalClient.Efsol,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterEfsolCreator>()),

                (externalClient => externalClient == ExternalClient.Kladovoy,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterKladovoyCreator>()),

                (externalClient => externalClient == ExternalClient.MCOB,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterMcobCreator>()),

                (externalClient => externalClient == ExternalClient.ProfitAccount,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterProfitAccountCreator>()),

                (externalClient => externalClient == ExternalClient.Sauri,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterSauriCreator>()),

                (externalClient => externalClient == ExternalClient.Yashchenko,
                    () => ServiceProvider.GetRequiredService<RegistrationUserLetterYashchenkoCreator>())
            ];
        }

        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<RegistrationUserLetterModelDto>
            GetLetterTemplate(RegistrationUserLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            GetLetterCreator(model.ExternalClient).CreateLetter();

        /// <summary>
        /// Создать действие для выполнения его после отправки письма
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Действие для выполнения после отправки письма</returns>
        protected override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
            GetLetterCreator(model.ExternalClient).CreateActionToExecuteAfterSendingEmail(model);

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(RegistrationUserLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(RegistrationUserLetterModelDto model) =>
            $"RegistrationUserLetter{model.AccountUserLogin}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(RegistrationUserLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(RegistrationUserLetterModelDto model) =>
            [];

        /// <summary>
        /// Получить Id пользователя аккаунта
        /// </summary>
        /// <param name="model">Модель письма о регистрации пользователя</param>
        /// <returns>Id пользователя аккаунта</returns>
        protected override Guid? GetAccountUserId(RegistrationUserLetterModelDto model) => model.AccountUserId;

        /// <summary>
        /// Получить создателя письма
        /// </summary>
        /// <param name="externalClient">Источник регистрации</param>
        /// <returns>Создатель письма</returns>
        private RegistrationUserLetterBaseCreator GetLetterCreator(ExternalClient externalClient)
        {
            var foundMatch = _letterCreators
                .FirstOrDefault(creator => creator.CompareExternalClient(externalClient));

            return foundMatch != default
                ? foundMatch.GetLetterCreator()
                : ServiceProvider.GetRequiredService<RegistrationUserLetterPromoCreator>();
        }
    }
}
