﻿using System.Text;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Mails.Promo;

namespace Clouds42.LetterNotification.LetterNotifications.AccountUser;

public class RegistrationAccountUserLetterNotification(IServiceProvider serviceProvider)
    : RegistrationUserLetterNotification(serviceProvider)
{
    protected readonly Lazy<string> SenderDescription = new(CloudConfigurationProvider.LetterNotification.GetSenderDescription);
    protected readonly Lazy<string> ManagerEmail = new(CloudConfigurationProvider.Emails.Get42CloudsManagerEmail);

    /// <summary>
    /// Сформировать сообщение для менеджера
    /// </summary>
    /// <param name="model">Модель письма о регистрации пользователя</param>
    /// <returns>Сообщение для менеджера</returns>
    protected virtual string GenerateMessageForManager(RegistrationUserLetterModelDto model)
    {
        var builder = new StringBuilder();
        builder.Append("Через личный кабинет зарегистрирован новый пользователь.");
        builder.Append("<br>");
        builder.Append($"Почта : {model.AccountUserEmail ?? "не указана"}");
        builder.Append("<br>");
        builder.Append($"Телефон : {model.AccountUserPhoneNumber ?? "не указан"}");
        builder.Append("<br>");
        builder.Append($"Номер аккаунта : {model.AccountIndexNumber}  Login : {model.AccountUserLogin}");
        builder.Append("<br>");
        builder.Append($"Сейл менеджер : {model.AccountSaleManagerCaption}");
        builder.Append("<br>");

        return builder.ToString();
    }

    protected override Action CreateActionToExecuteAfterSendingEmail(RegistrationUserLetterModelDto model) =>
        () => new SupportPromoMail()
            .DisplayName(SenderDescription.Value)
            .To(ManagerEmail.Value)
            .Subject("ЛК. Регистрация нового пользователя")
            .Body(GenerateMessageForManager(model))
            .SendViaNewThread();
}
