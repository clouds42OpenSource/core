﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Base;
using Clouds42.LetterNotification.Letters.AccountUser;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.LetterNotifications.AccountUser
{
    /// <summary>
    /// Уведомление о подтверждении email пользователя
    /// </summary>
    public class ConfirmationUserEmailLetterNotification(IServiceProvider serviceProvider)
        : LetterNotificationBase<ConfirmationUserEmailLetterModelDto>(serviceProvider)
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="model">Модель письма о подтверждении email пользователя</param>
        /// <param name="notifiedUserInfo">Информация об уведомляемом пользователе</param>
        /// <returns>Шаблон письма</returns>
        protected override LetterTemplateBase<ConfirmationUserEmailLetterModelDto>
            GetLetterTemplate(ConfirmationUserEmailLetterModelDto model, NotifiedAccountUserInfoDto? notifiedUserInfo) =>
            ServiceProvider.GetRequiredService<ConfirmationUserEmailLetter>();

        /// <summary>
        /// Получить прикрепленный файл
        /// </summary>
        /// <param name="model">Модель письма о подтверждении email пользователя</param>
        /// <returns>Прикрепленный файл</returns>
        protected override DocumentDataDto? GetAttachmentFile(ConfirmationUserEmailLetterModelDto model) => null;

        /// <summary>
        /// Получить ключ уникальности события - для избежания дублей
        /// </summary>
        /// <param name="model">Модель письма о подтверждении email пользователя</param>
        /// <returns>Ключ уникальности события</returns>
        protected override string GetEventKey(ConfirmationUserEmailLetterModelDto model) =>
            $"ConfirmationUserEmailLetter_{model.AccountUserLogin}";

        /// <summary>
        /// Получить дату, до которой это уведомление будет актуальным
        /// </summary>
        /// <param name="model">Модель письма о подтверждении email пользователя</param>
        /// <returns>Дата, до которой это уведомление будет актуальным</returns>
        protected override DateTime GetEventKeyPeriod(ConfirmationUserEmailLetterModelDto model) => DateTime.Now;

        /// <summary>
        /// Получить электронные адреса аккаунта
        /// </summary>
        /// <param name="model">Модель письма о подтверждении email пользователя</param>
        /// <returns>Электронные адреса аккаунта</returns>
        protected override List<string> GetAccountEmails(ConfirmationUserEmailLetterModelDto model) =>
            [];

        /// <summary>
        /// Получить Id пользователя аккаунта
        /// </summary>
        /// <param name="model">Модель письма о подтверждении email пользователя</param>
        /// <returns>Id пользователя аккаунта</returns>
        protected override Guid? GetAccountUserId(ConfirmationUserEmailLetterModelDto model) => model.AccountUserId;
    }
}
