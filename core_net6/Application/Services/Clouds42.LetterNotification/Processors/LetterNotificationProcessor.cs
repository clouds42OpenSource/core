﻿using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.LetterNotification.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.LetterNotification.Processors
{
    /// <summary>
    /// Процессор для работы с уведомлениями по почте
    /// </summary>
    internal class LetterNotificationProcessor(IServiceProvider serviceProvider) : ILetterNotificationProcessor
    {
        /// <summary>
        /// Попытаться выполнить уведомление по почте
        /// </summary>
        /// <typeparam name="TNotification">Тип уведомления</typeparam>
        /// <typeparam name="TModel">Входящая модель для уведомления</typeparam>
        /// <param name="model">Входящая модель</param>
        /// <returns>Результат выполнения уведомления</returns>
        public bool TryNotify<TNotification, TModel>(TModel model) where TNotification : ILetterNotificationBase<TModel>
            where TModel : ILetterTemplateModel =>
            serviceProvider.GetRequiredService<TNotification>().TryNotify(model);
    }
}
