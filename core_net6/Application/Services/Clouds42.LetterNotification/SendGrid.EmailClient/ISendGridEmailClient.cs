﻿using Clouds42.DataContracts.Service.SendGrid;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;

namespace Clouds42.LetterNotification.SendGrid.EmailClient
{
    public interface ISendGridEmailClient
    {
        void SendEmailViaNewThread(EmailRequestSettingsDto emailRequestSettings, LetterTemplateCommonDataDto? letterTemplateCommonData);
    }
}
