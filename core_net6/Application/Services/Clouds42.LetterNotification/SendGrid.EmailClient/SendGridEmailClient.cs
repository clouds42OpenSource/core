﻿using Clouds42.DataContracts.Service.SendGrid;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Logger;

namespace Clouds42.LetterNotification.SendGrid.EmailClient
{
    /// <summary>
    /// Клиент для работы с SendGrid
    /// </summary>
    public class SendGridEmailClient(
        ISendGridProvider sendGridProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISendGridEmailClient
    {
        /// <summary>
        /// Отправить письмо в новом потоке
        /// </summary>
        /// <param name="emailRequestSettings">Настроки для отправки письма</param>
        /// <param name="letterTemplateCommonData">Общие данные для шаблона письма</param>
        public void SendEmailViaNewThread(
            EmailRequestSettingsDto emailRequestSettings, LetterTemplateCommonDataDto? letterTemplateCommonData)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    var commandPayload = new CommandExecuteRequestPayloadDto
                    {
                        EmailRequestSettings = emailRequestSettings,
                        LetterTemplateCommonData = letterTemplateCommonData
                    };

                    logger.Info("Отправляем письмо");

                    var result = sendGridProvider.SendEmail(commandPayload);

                    logger.Info($"Получили результат {result.Message}");

                    if (result.Error)
                        throw new InvalidOperationException(result.Message);
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex,
                        $"[Ошибка отправке письма на адрес] {emailRequestSettings.EmailReceiver.Email} от " +
                        $"{emailRequestSettings.EmailSender.Email} с темой {letterTemplateCommonData?.LetterTemplateData.Subject} " +
                        $"в отдельном потоке ");
                }
            });
        }
    }
}
