﻿using System.DirectoryServices.AccountManagement;
using Clouds42.Configurations.Configurations;

namespace Clouds42.ActiveDirectory.Contracts.Activators
{
    /// <summary>
    /// Активатор контекста в режиме разработки
    /// </summary>
    public class DevelopModeContextActivator : IContextActivator
    {
        private static readonly Lazy<string> AdServer = new(CloudConfigurationProvider.AdManager.AdServer.GetAdServer);
        private static readonly Lazy<string> AdContainerClients = new(CloudConfigurationProvider.AdManager.AdServer.GetAdContainer);
        private static readonly Lazy<string> AdContainerRoot = new(CloudConfigurationProvider.AdManager.AdServer.GetAdContainerRoot);
        private static readonly Lazy<string> AdContainerCompanies = new(CloudConfigurationProvider.AdManager.AdServer.GetAdContainerCompanies);
        private static readonly Lazy<string> DevelopModeUserLogin = new(CloudConfigurationProvider.AdManager.Mode.GetDevelopModeAdUserName);
        private static readonly Lazy<string> DevelopModeUserPassword = new(CloudConfigurationProvider.AdManager.Mode.GetDevelopModeAdUserPassword);

        /// <summary>
        /// Открыть корневой контейнер
        /// </summary>
        /// <returns>Контекст корневого контейнера</returns>
        public PrincipalContext OpenRootUO()
            => new(ContextType.Domain, AdServer.Value, AdContainerRoot.Value, DevelopModeUserLogin.Value, DevelopModeUserPassword.Value);

        /// <summary>
        /// Открыть контейнер клиентов
        /// </summary>
        /// <returns>Контекст контейнера клиентов</returns>
        public PrincipalContext OpenClientsUO()
            => new(ContextType.Domain, AdServer.Value, AdContainerClients.Value, DevelopModeUserLogin.Value, DevelopModeUserPassword.Value);

        /// <summary>
        /// Открыть контейнер компаний
        /// </summary>
        /// <returns>Контекст контейнера компаний</returns>
        public PrincipalContext OpenCompanyUO()
            => new(ContextType.Domain, AdServer.Value, AdContainerCompanies.Value, DevelopModeUserLogin.Value, DevelopModeUserPassword.Value);
    }
}
