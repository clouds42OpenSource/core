﻿using System.DirectoryServices.AccountManagement;
using Clouds42.Configurations.Configurations;

namespace Clouds42.ActiveDirectory.Contracts.Activators
{
    /// <summary>
    /// Активатор контекста в режиме ПРОД
    /// </summary>
    public class EnterpriseModeContextActivator : IContextActivator
    {
        private readonly Lazy<string> _adServer = new(CloudConfigurationProvider.AdManager.AdServer.GetAdServer);
        private readonly Lazy<string> _adContainerClients = new(CloudConfigurationProvider.AdManager.AdServer.GetAdContainer);
        private readonly Lazy<string> _adContainerRoot = new(CloudConfigurationProvider.AdManager.AdServer.GetAdContainerRoot);
        private readonly Lazy<string> _adContainerCompanies = new(CloudConfigurationProvider.AdManager.AdServer.GetAdContainerCompanies);

        /// <summary>
        /// Открыть корневой контейнер
        /// </summary>
        /// <returns>Контекст корневого контейнера</returns>
        public PrincipalContext OpenRootUO()
            => new(ContextType.Domain, _adServer.Value, _adContainerRoot.Value);

        /// <summary>
        /// Открыть контейнер клиентов
        /// </summary>
        /// <returns>Контекст контейнера клиентов</returns>
        public PrincipalContext OpenClientsUO()
            => new(ContextType.Domain, _adServer.Value, _adContainerClients.Value);

        /// <summary>
        /// Открыть контейнер компаний
        /// </summary>
        /// <returns>Контекст контейнера компаний</returns>
        public PrincipalContext OpenCompanyUO()
            => new(ContextType.Domain, _adServer.Value, _adContainerCompanies.Value);
    }
}
