﻿using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.ActiveDirectory.Contracts
{
    public interface IRegisterUserInAccountAdHelper
    {
        void RegisterUser(ICreateNewUserModel userModel);
    }
}