﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Interoperability", "CA1416:Проверка совместимости платформы", Justification = "<Ожидание>", Scope = "member", Target = "~M:Clouds42.ActiveDirectory.Contracts.Functions.ActiveDirectoryCompanyFunctions.GetUserGroups(System.String)~System.Collections.Generic.List{System.String}")]
[assembly: SuppressMessage("Interoperability", "CA1416:Проверка совместимости платформы", Justification = "<Ожидание>", Scope = "member", Target = "~M:Clouds42.ActiveDirectory.Contracts.Functions.ActiveDirectoryCompanyFunctions.AddToGroups(System.DirectoryServices.AccountManagement.PrincipalContext,System.String,System.String[])")]
[assembly: SuppressMessage("Interoperability", "CA1416:Проверка совместимости платформы", Justification = "<Ожидание>", Scope = "member", Target = "~M:Clouds42.ActiveDirectory.Contracts.Functions.ActiveDirectoryCompanyFunctions.RemoveFromGroups(System.DirectoryServices.AccountManagement.PrincipalContext,System.String,System.String[])")]
