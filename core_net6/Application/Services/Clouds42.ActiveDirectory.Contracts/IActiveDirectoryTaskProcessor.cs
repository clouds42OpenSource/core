﻿using System.Linq.Expressions;

namespace Clouds42.ActiveDirectory.Contracts
{
    public interface IActiveDirectoryTaskProcessor
    {
        /// <summary>
        /// Попытаться выполнить таск немедленно.
        /// Пимечание: если контекс выполнения "воркер" и "воркер" может выполнять требуюмую задачу - выполнение произойдет немделенно.
        /// Если выполнить незамедлительно нет возможности, но есть рабочие воркеры - запускается таска и ожидаем результат выполения.
        /// Если нет рабочего воркера - таска ставится в очаредь на выполнение.
        /// </summary>
        /// <param name="actionExpression">Выражение выполняемого действие.</param>
        void TryDoImmediately(Expression<Action<IActiveDirectoryProvider>> actionExpression);

        /// <summary>
        /// Запустить таску.
        /// </summary>
        /// <param name="actionExpression">Выражение выполняемого действие.</param>
        void RunTask(Expression<Action<IActiveDirectoryProvider>> actionExpression);

    }
}