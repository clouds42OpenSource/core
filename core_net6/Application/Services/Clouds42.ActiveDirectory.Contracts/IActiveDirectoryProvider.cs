﻿using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.ActiveDirectory.Contracts
{
    /// <summary>
    /// Провайдер контроллера домена.
    /// </summary>
    public interface IActiveDirectoryProvider
    {

        void RegisterNewAccount(ICreateNewAccountModel model);

        void RegisterNewUser(ICreateNewUserModel userModel);

        /// <summary>
        /// Удалить пользователя.
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя.</param>
        /// <param name="username">логин</param>        
        void DeleteUser(Guid accountUserId, string username);

        /// <summary>
        /// Сменить логин
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя.</param>
        /// <param name="username">Логин пользователя.</param>
        /// <param name="newLogin">Новый логин.</param>
        void ChangeLogin(Guid accountUserId, string username, string newLogin, string email);

        /// <summary>
        /// Сменить пароль
        /// </summary>
        /// <param name="username">логин пользователя</param>
        /// <param name="accountUserId">Идентификатор пользователя.</param>
        /// <param name="newPassword">пароль</param>  
        void ChangePassword(Guid accountUserId, string username, string newPassword);

        void EditUser(Guid accountUserId, string userName, IPrincipalUserAdapterDto userModel);

        void EditUserGroupsAdList(EditUserGroupsAdListModel model);

        /// <summary>
        /// Установка прав на папку.
        /// </summary>
        /// <param name="groupName">Имя группы</param>
        /// <param name="strPath">Путь до папки</param>
        void SetDirectoryAcl(string groupName, string strPath);

        /// <summary>
        /// Уставновка прав на папку для пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="strPath">Путь до папки</param>        
        void SetDirectoryAclForUser(string userName, string strPath);

        /// <summary>
        /// Удаление прав на папку для пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="strPath">Путь до папки</param>
        void RemoveDirectoryAclForUser(string userName, string strPath);
    }

}
