﻿using System.DirectoryServices.AccountManagement;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.ActiveDirectory.Contracts.Functions
{
    public static class ActiveDirectoryCompanyFunctions
    {

        private static readonly ILogger42 Logger = Logger42.GetLogger();
        private static readonly IHandlerException HandlerException = HandlerException42.GetHandler();
        /// <summary>
        /// Создать группы.
        /// </summary>
        /// <param name="groups">Список групп.</param>
        /// <returns>Признак успешности</returns>
        public static void CreateGroups(params string[] groups)
        {
            using var ctx = ContextActivatorFactory.Create().OpenCompanyUO();
            CreateGroups(ctx, groups);
        }

        /// <summary>
        /// Создать группы.
        /// </summary>
        /// <param name="ctx">Контекс контроллера домена.</param>
        /// <param name="groups">Список групп.</param>
        /// <returns>Признак успешности</returns>
        public static void CreateGroups(PrincipalContext ctx, params string[] groups)
        {
            foreach (var groupName in groups)
            {
                Logger.Trace("Проверка существования группы домена {0}", groupName);

                if (GroupPrincipal.FindByIdentity(ctx, groupName) == null)
                {
                    Logger.Trace("Создание группы домена {0}", groupName);

                    using var grp = new GroupPrincipal(ctx);
                    grp.Name = groupName;
                    grp.SamAccountName = groupName;
                    grp.Save();

                    Logger.Info("Создана группа домена {0}", groupName);
                }
                else
                {
                    Logger.Info("Группа домена {0} уже существует", groupName);
                }
            }
        }

        /// <summary>
        /// Добавить пользователя в группу
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groups">Список групп.</param>
        public static void AddToGroups(string username, params string[] groups)
        {
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                AddToGroups(ctx, username, groups);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка добавления пользователя в группу] пользователь: {username}' в группы '{string.Join(";", groups)}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Добавить пользователя в группу
        /// </summary>
        /// <param name="ctx">Контекс контроллера домена.</param>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groups">Список групп.</param>
        public static void AddToGroups(PrincipalContext ctx, string username, params string[] groups)
        {
            var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, username);
            if (user == null)
            {
                Logger.Warn($"Пользователь {username} не найден");
                throw new KeyNotFoundException($"Пользователь не найден: {username}");
            }

            foreach (var groupName in groups)
            {
                Logger.Trace("Добавление пользователя {0} в группу {1}", username, groupName);

                var grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, groupName) ?? throw new KeyNotFoundException($"Группа: '{groupName}' не найдена в домене.");
                var userExist = grp.Members.FirstOrDefault(u => u.Name == username);
                if (userExist != null)
                {
                    Logger.Info("Пользователь {0} уже добавлен в группу {1}", username, groupName);
                    return;
                }

                grp.Members.Add(user);
                grp.Save();

                Logger.Info("Пользователь {0} добавлен в группу {1}", username, groupName);
            }
        }

        /// <summary>
        /// Добавить в группу если еще не состоит в ней
        /// </summary>
        /// <param name="ctx">Контекс контроллера домена.</param>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groupName">Имя группы</param>
        public static void AddToGroupIfNoExist(PrincipalContext ctx, string username, string groupName)
        {
            try
            {
                Logger.Trace("Добавление пользователя {0} в группу {1}", username, groupName);

                var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, username) ??
                           throw new KeyNotFoundException($"Пользователь {username} не найден");
                var grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, groupName) ??
                          throw new KeyNotFoundException($"Группа {groupName} не найдена");
                var userExist = grp.Members.FirstOrDefault(u => u.Name == username);
                if (userExist != null)
                {
                    Logger.Info("Пользователь {0} уже добавлен в группу {1}", username, groupName);
                    return;
                }
                grp.Members.Add(user);
                grp.Save();

                Logger.Info("Пользователь {0} добавлен в группу {1}", username, groupName);
            }

            catch (KeyNotFoundException keyNotFoundException)
            {
                Logger.Warn(keyNotFoundException.Message, $"{nameof(KeyNotFoundException)}. Ошибка поиска пользователя или группы");
            }

            catch (Exception ex)
            {
                var message = $"[Ошибка добавления пользователя в группу] '{username}' в группу '{groupName}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Добавить в группу если еще не состоит в ней
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groupName">Имя группы</param>
        public static void AddToGroupIfNoExist(string username, string groupName)
        {
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                AddToGroupIfNoExist(ctx, username, groupName);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка добавления пользователя в группу] '{username}' в группу '{groupName}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Получить информацию о пользователе, входит он в группу или нет
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groupName">Имя группы</param>
        public static bool ExistUserAtGroup(string username, string groupName)
        {
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, username);
                if (user == null)
                {
                    Logger.Warn("Пользователь {0} не найден", username);
                    return false;
                }

                var grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, groupName);
                if (grp == null)
                {
                    Logger.Warn("Группа {0} не найдена", groupName);
                    return false;
                }

                var userExist = grp.Members.FirstOrDefault(u => u.Name == username);
                if (userExist != null)
                {
                    return true;
                }

                Logger.Info("Пользователь {0} не добавлен в группу {1}", username, groupName);
                return false;
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, $"[Ошибка получение информации о вхождении пользователя] {username} в группу {groupName}");
                return false;
            }
        }

        /// <summary>
        /// Удалить пользователя с группы
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groups">Список групп из которых нужно удалить пользователя.</param>
        public static void RemoveFromGroups(string username, params string[] groups)
        {
            try
            {
                if (groups.Length <= 0)
                {
                    return;
                }

                Logger.Trace("Удаление пользователя {0} из группы {1}", username, groups);
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                RemoveFromGroups(ctx, username, groups);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка удаления пользователя из группы] '{username}' из группы '{groups}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Удалить пользователя с группы
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groups">Список групп из которых нужно удалить пользователя.</param>
        /// <param name="ctx">Контекст контроллера домена.</param>
        public static void RemoveFromGroups(PrincipalContext ctx, string username, params string[] groups)
        {
            var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, username);

            if (user == null)
                return;

            foreach (var group in groups)
            {
                var grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, @group) ?? throw new KeyNotFoundException($"Группа не найдена: {username}");
                
                grp.Members.Remove(user);
                grp.Save();

                Logger.Info("Пользователь {0} удален из группы {1}", username, groups);
            }
        }

        /// <summary>
        /// Удалить пользователя с группы если он в ней состоит
        /// </summary>
        /// <param name="ctx">Контекс контроллера домена.</param>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groupName">Имя группы</param>
        /// <returns>Признак успешности</returns>
        public static void RemoveFromGroupIfExist(PrincipalContext ctx, string username, string groupName)
        {
            try
            {
                Logger.Trace("Удаление пользователя {0} из группы {1}", username, groupName);

                var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, username);
                var grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, groupName);

                if (user == null)
                    return;

                if (grp == null)
                    return;

                var userExist = grp.Members.FirstOrDefault(u => u.Name == username);
                if (userExist == null)
                {
                    Logger.Info("Пользователь {0} не состоит в группе {1}", username, groupName);
                    return;
                }

                grp.Members.Remove(user);
                grp.Save();

                Logger.Info("Пользователь {0} удален из группы {1}", username, groupName);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка удаления пользователя из группы] '{username}' из группы '{groupName}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Удалить пользователя с группы если он в ней состоит
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="groupName">Имя группы</param>
        /// <returns>Признак успешности</returns>
        public static void RemoveFromGroupIfExist(string username, string groupName)
        {
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                RemoveFromGroupIfExist(ctx, username, groupName);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка удаления пользователя из группы] '{username}' из группы '{groupName}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Список групп пользователя кроме главной группы "Пользователи домена"
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <returns>Список групп</returns>
        public static List<string> GetUserGroups(string username)
        {
            using var ctx = ContextActivatorFactory.Create().OpenRootUO();
            var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, username) ?? throw new KeyNotFoundException($"Пользователь {username} не найден в домене");
            
            var usersGroups = user.GetGroups(ctx);

            return usersGroups.Select(group => group.Name).ToList();
        }

        /// <summary>
        /// Удалить группу
        /// </summary>
        /// <param name="groups">Имена групп к удалению.</param>
        /// <returns>Признак успешности</returns>
        public static void DeleteGroups(params string[] groups)
        {
            using var ctx = ContextActivatorFactory.Create().OpenCompanyUO();
            foreach (var groupName in groups)
            {
                try
                {
                    Logger.Trace("Запрос удаления группы домена: {0}", groupName);

                    var group = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, groupName);

                    if (group == null)
                        return;

                    group.Delete();

                    Logger.Info("Удалена группа домена: {0}", groupName);
                }
                catch (Exception ex)
                {
                    var message = $"[Ошибка удаления группы домена] '{groupName}'";
                    HandlerException.Handle(ex, message);
                    throw new InvalidOperationException(message);
                }
            }
        }

        /// <summary>
        /// Проверка на наличие группы в домене
        /// </summary>
        /// <param name="grpName">Имя группы</param>
        /// <returns>Наличие группы в доменне</returns>
        public static bool GroupExists(string grpName)
        {
            return FindGroup(grpName) != null;
        }

        public static GroupPrincipal FindGroup(string grpName)
        {
            using var ctx = ContextActivatorFactory.Create().OpenCompanyUO();
            return GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, grpName);
        }
    }
}
