﻿using System.DirectoryServices.AccountManagement;
using System.Security.AccessControl;
using System.Security.Principal;
using Clouds42.HandlerExeption.Contract;

using Clouds42.Logger;

namespace Clouds42.ActiveDirectory.Contracts.Functions
{
    public static class ActiveDirectoryFolderFunctions
    {

        private static readonly ILogger42 _logger = Logger42.GetLogger();
        private static readonly IHandlerException _handlerException = HandlerException42.GetHandler();
        /// <summary>
        /// Установка прав на папку.
        /// </summary>
        /// <param name="groupName">Имя группы</param>
        /// <param name="strPath">Путь до папки</param>
        /// /// <param name="ctx">Контекс контроллера домена.</param>
        public static void SetDirectoryAcl(PrincipalContext ctx, string groupName, string strPath)
        {

            try
            {
                _logger.Trace($"Установка прав на папку '{strPath}' для группы {groupName}");

                var grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, groupName);
                if (grp == null)
                {
                    _logger.Warn($"Не удалось установить доступ к папке: '{strPath}' по причине: Группа {groupName} не найдена");
                    return;
                }

                var secIdentifierSid = new SecurityIdentifier(grp.Sid.ToString());

                var tmpDirInfo = new DirectoryInfo(strPath);
                if (!tmpDirInfo.Exists)
                    Directory.CreateDirectory(strPath);

                var dirInfo = new DirectoryInfo(strPath);
                dirInfo.Refresh();

                var ds = dirInfo.GetAccessControl();
                var rule = new FileSystemAccessRule(
                    secIdentifierSid,
                    FileSystemRights.Read | FileSystemRights.Write | FileSystemRights.CreateDirectories |
                    FileSystemRights.CreateFiles | FileSystemRights.Delete | FileSystemRights.ExecuteFile |
                    FileSystemRights.Traverse,
                    InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                    PropagationFlags.InheritOnly, AccessControlType.Allow
                );

                ds.AddAccessRule(rule);
                dirInfo.SetAccessControl(ds);
                _logger.Info($"Установлены права на папку '{strPath}' для группы {groupName}");
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка установки прав на папку] '{strPath}' для группы {groupName}";
                _handlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }

        }

        /// <summary>
        /// Установка прав на папку.
        /// </summary>
        /// <param name="groupName">Имя группы</param>
        /// <param name="strPath">Путь до папки</param>
        public static void SetDirectoryAcl(string groupName, string strPath)
        {
            using var ctx = ContextActivatorFactory.Create().OpenCompanyUO();
            SetDirectoryAcl(ctx, groupName, strPath);
        }

        /// <summary>
        /// Уставновка прав на папку для пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="strPath">Путь до папки</param>        
        public static void SetDirectoryAclForUser(string userName, string strPath)
        {
            try
            {
                _logger.Trace($"Установка прав на папку '{strPath}' для пользователя {userName}");

                var tmpDirInfo = new DirectoryInfo(strPath);
                if (!tmpDirInfo.Exists) Directory.CreateDirectory(strPath);

                var dirInfo = new DirectoryInfo(strPath);
                dirInfo.Refresh();

                var ds = dirInfo.GetAccessControl();
                var rule = new FileSystemAccessRule(
                    @"AD\" + userName,
                    FileSystemRights.Read | FileSystemRights.Write | FileSystemRights.CreateDirectories |
                    FileSystemRights.CreateFiles | FileSystemRights.Delete | FileSystemRights.ExecuteFile |
                    FileSystemRights.Traverse,
                    InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                    PropagationFlags.None, AccessControlType.Allow
                );

                ds.AddAccessRule(rule);
                dirInfo.SetAccessControl(ds);
                _logger.Info($"Установлены права на папку '{strPath}' для пользователя {userName}");
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка установки прав на папку] '{strPath}' для пользователя {userName}";
                _handlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Удаление прав на папку для пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="strPath">Путь до папки</param>
        public static void RemoveDirectoryAclForUser(string userName, string strPath)
        {
            try
            {
                _logger.Trace("Удаление прав на папку {0} для пользователя {1}", strPath, userName);

                var tmpDirInfo = new DirectoryInfo(strPath);
                if (!tmpDirInfo.Exists) Directory.CreateDirectory(strPath);

                var dirInfo = new DirectoryInfo(strPath);
                dirInfo.Refresh();

                var ds = dirInfo.GetAccessControl();
                var rule = new FileSystemAccessRule(
                    @"AD\" + userName,
                    FileSystemRights.Read | FileSystemRights.Write | FileSystemRights.CreateDirectories |
                    FileSystemRights.CreateFiles | FileSystemRights.Delete | FileSystemRights.ExecuteFile |
                    FileSystemRights.Traverse,
                    InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                    PropagationFlags.None, AccessControlType.Allow
                );

                ds.RemoveAccessRule(rule);
                dirInfo.SetAccessControl(ds);
                _logger.Info("Установлены права на папку {0} для пользователя {1}", strPath, userName);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка удаления прав на папку] '{strPath}' для пользователя '{userName}'";
                _handlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

    }
}
