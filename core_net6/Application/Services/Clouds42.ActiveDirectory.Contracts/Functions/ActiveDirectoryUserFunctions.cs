﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.ActiveDirectory.Interface;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using Clouds42.ActiveDirectory.Contracts.Extensions;
using Clouds42.Common.Exceptions;

#pragma warning disable CA1416

namespace Clouds42.ActiveDirectory.Contracts.Functions
{
    public static class ActiveDirectoryUserFunctions
    {
        private static readonly ILogger42 Logger = Logger42.GetLogger();
        private static readonly IHandlerException HandlerException = HandlerException42.GetHandler();

        /// <summary>
        /// Создание пользовательской учетки в домене.
        /// </summary>
        /// <param name="principalUser">Модель пользователя.</param>
        public static void CreateUser(IPrincipalUserAdapterDto principalUser)
        {
            if (UsersExistsInDomain(principalUser.GetUserName()))
                return;

            using var ctx = ContextActivatorFactory.Create().OpenClientsUO();
            CreateUser(ctx, principalUser);
        }

        /// <summary>
        /// Создание пользовательской учетки в домене.
        /// </summary>
        /// <param name="principalUser">Модель пользователя.</param>
        /// <param name="ctx">Контекс контроллера домена.</param>
        public static void CreateUser(PrincipalContext ctx, IPrincipalUserAdapterDto principalUser)
        {
            try
            {
                if (UsersExistsInDomain(principalUser.GetUserName()))
                {
                    Logger.Debug("Уже существует такой пользователь домена: {0} пароль: {1} displayName: {2}", principalUser.GetUserName(), principalUser.Password);
                    return;
                }    

                var displayName = principalUser.GetDisplayName();

                Logger.Debug("Запрос создания пользователя домена: {0} пароль: {1} displayName: {2}", principalUser.GetUserName(), principalUser.Password, displayName);

                Logger.Debug("Создание контекста ");
                using (var user = new UserPrincipal(ctx))
                {
                    if (!string.IsNullOrEmpty(principalUser.FirstName))
                    {
                        user.GivenName = principalUser.FirstName;
                    }
                    if (!string.IsNullOrEmpty(principalUser.LastName))
                    {
                        user.Surname = principalUser.LastName;
                    }
                    if (!string.IsNullOrEmpty(principalUser.MiddleName))
                    {
                        user.MiddleName = principalUser.MiddleName[0].ToString().ToUpper();
                    }
                    if (!string.IsNullOrEmpty(principalUser.PhoneNumber))
                    {
                        user.VoiceTelephoneNumber = principalUser.PhoneNumber;
                    }
                    var userName = principalUser.GetUserName();
                    user.UserCannotChangePassword = true;
                    user.SamAccountName = userName;
                    user.DisplayName = string.IsNullOrEmpty(displayName) ? principalUser.GetUserName() : displayName;
                    user.PasswordNeverExpires = true;
                    user.UserPrincipalName = principalUser.Email ?? userName;
                    user.Description = "Клиент 42 Облака";
                    user.Enabled = true;
                    Logger.Debug("DisplayName: {0} SamAccountName: {1} ", user.DisplayName, user.SamAccountName);
                    user.SetPassword(principalUser.Password);
                    user.Save();

                    if (!string.IsNullOrEmpty(principalUser.MiddleName))
                    {
                        SetProperty(user.GetUnderlyingObject() as DirectoryEntry, "initials", principalUser.MiddleName[0].ToString().ToUpper());
                    }
                }

                Logger.Info("Создан пользователь домена: {0}", principalUser.GetUserName());
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка создания пользователя домена] '{principalUser.GetUserName()}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Удалить пользователя.
        /// </summary>
        /// <param name="username">логин</param>  
        public static void DeleteUser(string username)
        {

            using var context = ContextActivatorFactory.Create().OpenRootUO();
            try
            {
                Logger.Trace("Запрос удаления пользователя домена: {0}", username);

                var user = FindUser(username, context);
                if (user == null)
                {
                    Logger.Trace("Пользователь с логином {0} не найден.", username);
                    return;
                }
                user.Delete();
                Logger.Info("Удален пользователь домена: {0}", username);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка удаления пользователя домена] '{username}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Активировать / диактивировать пользователя.
        /// </summary>
        /// <param name="username">Имя пользователя</param>
        /// <param name="enabled">Состояние активности.</param>
        /// <returns></returns>
        public static void SetActivateStatus(string username, bool enabled)
        {
            try
            {
                Logger.Trace("Устанавливаем пользователю {0} Enabled статус: {1}", username, enabled);

                using (var ctx = ContextActivatorFactory.Create().OpenRootUO())
                {
                    var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, username) ?? throw new NotFoundException($"В домене не удалось найти пользователя '{username}'");
                    user.Enabled = enabled;
                    user.Save();

                }

                Logger.Info("Статус успешно изменен: {0}.Enabled -> {1}", username, enabled);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка смены статуса Enabled] пользователю  '{username}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Проверить валидность пользователя в домене
        /// </summary>
        /// <param name="username">логин пользователя</param>
        /// <param name="password">пароль</param>        
        public static bool IsValidUser(string username, string password)
        {
            try
            {
                Logger.Trace("Проверка учётных данных пользователя {0}", username);
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                var validateCredentials = ctx.ValidateCredentials(username, password);
                Logger.Info("Учётные данные пользователя {0} корректны", username);
                return validateCredentials;
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, $"[Ошибка проверки учетный данных пользователя] {username}");
                return false;
            }
        }

        public static void ChangeEmail(string? newEmail, string login)
        {
            var emailOrLogin = newEmail ?? login;


            Logger.Trace("Запрос изменения userPrincipalName пользователя домена: {0}", emailOrLogin);

            try
            {
                using var context = ContextActivatorFactory.Create().OpenRootUO();
                var user = FindUser(login, context) ?? throw new KeyNotFoundException($"Пользователь не найден: {login}");
                using (var entry = (DirectoryEntry)user.GetUnderlyingObject())
                {
                    entry.InvokeSet("userPrincipalName", emailOrLogin);
                    entry.CommitChanges();
                }

                user.Save();

                Logger.Info("Изменен userPrincipalName пользователя домена: {0}.", emailOrLogin);
            }

            catch (Exception ex)
            {
                var message = $"[Ошибка редактирования userPrincipalName у пользователя] '{emailOrLogin}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Сменить логин
        /// </summary>
        /// <param name="username">Логин пользователя.</param>
        /// <param name="newLogin">Новый логин.</param>
        /// <param name="email"></param>
        public static void ChangeLogin(string username, string newLogin, string email)
        {            
            if (username == newLogin)
                return;

            Logger.Trace("Запрос изменения логина пользователя домена: {0}", username);

            try
            {
                using var context = ContextActivatorFactory.Create().OpenRootUO();
                var user = FindUser(username, context) ?? throw new KeyNotFoundException($"Пользователь не найден: {username}");
                using (var entry = (DirectoryEntry)user.GetUnderlyingObject())
                {
                    entry.InvokeSet("uid", newLogin);
                    entry.InvokeSet("sAMAccountName", newLogin);
                    entry.InvokeSet("userPrincipalName", email);
                    entry.CommitChanges();
                    entry.Rename("CN=" + newLogin);
                    entry.CommitChanges();
                }

                user.Save();

                Logger.Info("Изменен логин пользователя домена: {0}.", username);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка редактирования имени входа у пользователя] '{username}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Сменить пароль
        /// </summary>
        /// <param name="username">логин пользователя</param>
        /// <param name="newPassword">пароль</param>
        public static void ChangePassword(string username, string newPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
                return;

            Logger.Trace("Запрос изменения пароля пользователя домена: {0}", username);
            using var context = ContextActivatorFactory.Create().OpenRootUO();
            try
            {
                var user = FindUser(username, context);
                if (user == null)
                {
                    throw new KeyNotFoundException($"Пользователь не найден: {username}");
                }

                user.SetPassword(newPassword);
                user.Save();

                Logger.Info("Изменен пароль пользователя домена: {0}.", username);
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка изменения пароля пользователя домена] '{username}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Сменить отоброжаемое имя.
        /// </summary>
        /// <param name="login">логин пользователя</param>
        /// <param name="displayName">Значение аттрибута DisplayName.</param>
        public static void ChangeDisplayName(string login, string displayName)
        {
            Logger.Trace($"Запрос изменения display name пользователя домена: {login} - {displayName}");
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, login) ?? throw new KeyNotFoundException($"Пользователь не найден: {login}");
                user.DisplayName = displayName;
                user.Save();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка изменения display name  пользователя домена] '{login}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Сменить фамилию
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="lastName">Фамилия</param>
        public static void ChangeLastName(string login, string lastName)
        {
            Logger.Trace($"Запрос изменения фамилии пользователя домена: {login} - {lastName}");
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, login) ?? throw new KeyNotFoundException($"Пользователь не найден: {login}");
                user.Surname = string.IsNullOrEmpty(lastName) ? " " : lastName;
                user.Save();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка изменения фамилии пользователя домена] '{login}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Сменить отчество
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="middleName">Отчество</param>
        public static void ChangeMiddleName(string login, string middleName)
        {
            Logger.Trace($"Запрос изменения отчества пользователя домена: {login} - {middleName}");
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, login);

                if (user != null)
                {
                    SetProperty(user.GetUnderlyingObject() as DirectoryEntry, "initials",
                        !string.IsNullOrEmpty(middleName) ? middleName[0].ToString().ToUpper() : " ");

                    user.MiddleName = string.IsNullOrEmpty(middleName) ? " " : middleName[0].ToString().ToUpper();
                    user.Save();
                }
                else
                {
                    throw new KeyNotFoundException($"Пользователь не найден: {login}");
                }
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка изменения отчества пользователя домена] '{login}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Сменить имя
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="firstName">Имя</param>
        public static void ChangeName(string login, string firstName)
        {
            Logger.Trace($"Запрос изменения имени пользователя домена: {login} - {firstName}");
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, login) ?? throw new KeyNotFoundException($"Пользователь не найден: {login}");
                user.GivenName = string.IsNullOrEmpty(firstName) ? " " : firstName;
                user.Save();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка изменения имени пользователя домена] '{login}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Смена номера телефона
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="phoneNumber">Номер телефона</param>
        public static void ChangePhoneNumber(string login, string phoneNumber)
        {
            Logger.Trace($"Запрос изменения телефона пользователя домена: {login} - {phoneNumber}");
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();
                var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, login);
                if (user == null)
                {
                    Logger.Warn($"Пользователь {login} не найден");
                    throw new KeyNotFoundException($"Пользователь не найден: {login}");
                }

                user.VoiceTelephoneNumber = string.IsNullOrEmpty(phoneNumber) ? " " : phoneNumber;
                user.Save();
                Logger.Trace($"Изменили номер телефона на номер {phoneNumber} для пользователя {login}");
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка изменения телефона пользователя домена] '{login}'";
                HandlerException.Handle(ex, message);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Проверка наличия пользователя в домене
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>наличия пользователя в домене</returns>
        public static bool UsersExistsInDomain(string login)
        {
            using var ctx = ContextActivatorFactory.Create().OpenRootUO();
            var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, login);
            Logger.Trace("UsersExistsInDomain login: {0} isExist: {1}", login, user != null);
            return user != null;
        }

        /// <summary>
        /// Проверка активации/деактивации пользователя 
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>наличие активации/деактивации в домене</returns>
        public static bool UsersActivateInDomain(string login)
        {
            using var ctx = ContextActivatorFactory.Create().OpenRootUO();
            var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, login);

            if (user?.Enabled == null)
                return false;

            Logger.Trace("UsersActivateInDomain login: {0} isActivate: {1}", login, user.Enabled);
            return user.Enabled != null && user.Enabled.Value;
        }

        public static UserPrincipal FindUser(string username)
        {
            using var ctx = ContextActivatorFactory.Create().OpenRootUO();

            return UserPrincipal.FindByIdentity(ctx, IdentityType.Name, username);
        }

        /// <summary>
        ///     Найти пользователя в Active Directory
        /// </summary>
        /// <param name="username">Логин пользователя</param>
        /// <param name="context">Principal Context</param>
        /// <returns></returns>
        private static UserPrincipal FindUser(string username, PrincipalContext context)
        {
            return UserPrincipal.FindByIdentity(context, IdentityType.Name, username);
        }

        /// <summary>
        /// This will test the value of the propertyvalue and if empty will not set the property
        /// as AD is particular about being sent blank values
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="sPropertyName"></param>
        /// <param name="sPropertyValue"></param>
        private static void SetProperty(DirectoryEntry entry, string sPropertyName, string sPropertyValue)
        {
            try
            {
                if (!string.IsNullOrEmpty(sPropertyValue))
                {
                    if (entry.Properties.Contains(sPropertyName))
                    {
                        entry.Properties[sPropertyName].Value = sPropertyValue;
                        entry.CommitChanges();
                        entry.Close();
                    }
                    else
                    {
                        entry.Properties[sPropertyName].Add(sPropertyValue);
                        entry.CommitChanges();
                        entry.Close();
                    }
                }
                else
                {
                    if (entry.Properties.Contains(sPropertyName))
                    {
                        entry.Properties[sPropertyName].Value = " ";
                        entry.CommitChanges();
                        entry.Close();
                    }
                    else
                    {
                        entry.Properties[sPropertyName].Add(" ");
                        entry.CommitChanges();
                        entry.Close();
                    }
                }
            }
            catch (Exception)
            {
                Logger.Error($"Ошибка с выдачей прав {sPropertyValue} в {sPropertyName} ");
            }
            
        }

        public static void SetUserFilesHomeDirectory(string? path,  string? disk, params string[] userNames)
        {
            try
            {
                using var ctx = ContextActivatorFactory.Create().OpenRootUO();

                foreach (var userName in userNames)
                {
                    var user = UserPrincipal.FindByIdentity(ctx, IdentityType.Name, userName) ?? throw new KeyNotFoundException($"Пользователь {userName} не найден в домене");

                    DirectoryEntry directoryEntry = (DirectoryEntry)user.GetUnderlyingObject();

                    if (path.IsNullOrEmpty())
                    {
                        directoryEntry.Properties["homeDirectory"].Clear();
                    }

                    else
                    {
                        directoryEntry.Properties["homeDirectory"].Value = path;
                    }

                    if (disk.IsNullOrEmpty())
                    {
                        directoryEntry.Properties["homeDrive"].Clear();
                    }

                    else
                    {
                        directoryEntry.Properties["homeDrive"].Value = disk;
                    }

                    directoryEntry.CommitChanges();
                }

            }

            catch (Exception ex)
            {
                var message = $"[Ошибка добавления пути пользовательских файлов в AD] {path}";
                HandlerException.Handle(ex, message);

                throw new InvalidOperationException(message);
            }
        }

    }
}
