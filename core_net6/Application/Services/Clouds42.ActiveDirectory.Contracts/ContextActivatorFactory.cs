﻿using Clouds42.ActiveDirectory.Contracts.Activators;
using Clouds42.Configurations.Configurations;

namespace Clouds42.ActiveDirectory.Contracts
{
    /// <summary>
    /// Фабрика для создания контекста подключения к контроллеру домена
    /// </summary>
    public static class ContextActivatorFactory
    {
        private static readonly bool DeveloperMode = CloudConfigurationProvider.AdManager.Mode.GetIsDevelopMode();

        /// <summary>
        /// Создать контекст
        /// </summary>
        /// <returns>Контекст</returns>
        public static IContextActivator Create()
            => DeveloperMode ? new DevelopModeContextActivator() : new EnterpriseModeContextActivator();
    }
}