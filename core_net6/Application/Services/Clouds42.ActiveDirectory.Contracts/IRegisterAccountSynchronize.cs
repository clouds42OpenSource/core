﻿namespace Clouds42.ActiveDirectory.Contracts
{
    /// <summary>
    /// Синхронизатор регистрации аккаунта
    /// </summary>
    public interface IRegisterAccountSynchronize
    {
        /// <summary>
        /// Синхронизировать регистрацию
        /// </summary>
        void Synchronize();
    }
}
