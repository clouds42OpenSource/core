﻿namespace Clouds42.ActiveDirectory.Contracts.Helpers
{
    public static class DomainCoreGroups
    {

        public static string WebCompanyGroupBuild(int accountIndexNumber)
        {
            return $"company_{accountIndexNumber}_web";
        }
        public static string CompanyGroupBuild(int accountIndexNumber)
        {
            return $"company_{accountIndexNumber}";
        }

        public static string GetDatabaseGroupName(int accountIndexNumber, int databaseNumber)
        {
            return $"web_{accountIndexNumber}_{databaseNumber}";
        }

        public const string RemoteDesktopAccess = "Remote Desktop Access";
        public const string MyOfficeClients = "Клиенты Мой Оффис";
        public const string ClientsMk = "Клиенты МК";
        public const string DomainUsers = "Пользователи домена";
    }
}
