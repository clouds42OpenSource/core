﻿using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.ActiveDirectory.Contracts.Extensions
{
    public  static class PrincipalUserAdapterExtensions
    {

        public static string GetDisplayName(this IPrincipalUserAdapterDto model)
        {
            var displayName = string.Join(" ", string.IsNullOrEmpty(model.FirstName) ? "" : model.FirstName,
                string.IsNullOrEmpty(model.LastName) ? "" : model.LastName,
                string.IsNullOrEmpty(model.MiddleName) ? "" : model.MiddleName).Trim();

            return !string.IsNullOrEmpty(displayName) ? displayName : model.GetUserName();
        }
    }
}
