﻿using System.DirectoryServices.AccountManagement;

namespace Clouds42.ActiveDirectory.Contracts
{
    /// <summary>
    /// Активатор контекста подключения к контроллеру домена
    /// </summary>
    public interface IContextActivator
    {
        /// <summary>
        /// Открыть корневой контейнер
        /// </summary>
        /// <returns>Контекст корневого контейнера</returns>
        PrincipalContext OpenRootUO();

        /// <summary>
        /// Открыть контейнер клиентов
        /// </summary>
        /// <returns>Контекст контейнера клиентов</returns>
        PrincipalContext OpenClientsUO();

        /// <summary>
        /// Открыть контейнер компаний
        /// </summary>
        /// <returns>Контекст контейнера компаний</returns>
        PrincipalContext OpenCompanyUO();
    }
}
