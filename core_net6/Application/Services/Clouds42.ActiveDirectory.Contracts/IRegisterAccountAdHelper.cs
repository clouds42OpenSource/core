﻿using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.ActiveDirectory.Contracts
{
    /// <summary>
    /// Хэлпер регистрации аккаунта в ActiveDirectory
    /// </summary>
    public interface IRegisterAccountAdHelper
    {
        /// <summary>
        /// Зарегистрировать аккаунт
        /// </summary>
        /// <param name="model">Модель создания нового аккаунта</param>
        void RegisterAccount(ICreateNewAccountModel model);

        /// <summary>
        /// Проверяет существует ли логин в Active Directory
        /// </summary>
        /// <param name="login">Логин для проверки существования</param>
        /// <returns>Возвращает <c>true</c> если логин существует, иначе <c>false</c></returns>
        bool IsLoginInActiveDirectoryExist(string login);
    }
}