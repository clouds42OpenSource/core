﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.ResourceConfiguration.CSPerformanceCounterModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Repositories;
using Clouds42.SelfPerformance.Contracts;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.SelfPerformance
{
    public interface ICsPerformanceCountersManager
    {
        ManagerResult<List<CsPerformanceCounter>> GetPerformanceValue(string cloudServiceId, string performanceCounterId,
            DateTime dateTimeFrom, DateTime dateTimeTo,
            Guid? accountUserId = null);

        ManagerResult<EmptyResultDto> SetPerformanceValue(SetPerformanceValueModelDto model);
        ManagerResult SetPerformanceDateValueList(SetPerformanceDateValueListModelDto model);
        ManagerResult<EmptyResultDto> SetPerformanceValueInternal(SetPerformanceValueModelDto model);
        void SetPerformanceDateValueListInternal(SetPerformanceDateValueModel model);
    }

    public class CsPerformanceCountersManager : BaseManager, ICsPerformanceCountersManager
    {
        private readonly ISelfPerformanceTestingProvider _selfPerformanceTestingProvider;
        private readonly IConfiguration _configuration;
        private readonly List<string> _listCache = new ();

        public CsPerformanceCountersManager(
            IAccessProvider accessProvider, 
            IUnitOfWork unitOfWork,
            IConfiguration configuration, 
            ISelfPerformanceTestingProvider selfPerformanceTestingProvider,
            IHandlerException handlerException)
            : base(accessProvider, unitOfWork, handlerException)
        {
            _configuration = configuration;
            _selfPerformanceTestingProvider = selfPerformanceTestingProvider;
        }

        public ManagerResult<List<CsPerformanceCounter>> GetPerformanceValue(string cloudServiceId, string performanceCounterId,
                                                     DateTime dateTimeFrom, DateTime dateTimeTo,
                                                     Guid? accountUserId = null)
        {
            AccessProvider.HasAccess(ObjectAction.CSPerformanceCounters_View, null, ()=>accountUserId);

            //получаем данные из БД по заданным условиям
            var cspCounters = DbLayer.CsSPerformanceCounterRepository.GetCsPerformanceCounters(cloudServiceId, accountUserId, performanceCounterId, dateTimeFrom, dateTimeTo);

            // if count > 1000 return 412
            return cspCounters.Count > 1000
                ? PreconditionFailed<List<CsPerformanceCounter>>("Too many matches reduce selection range")
                : Ok(cspCounters);
        }

        public ManagerResult<EmptyResultDto> SetPerformanceValue(SetPerformanceValueModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.CSPerformanceCounters_Add, null, () => model.AccountUserId);
            return SetPerformanceValueInternal(model);
        }

        public ManagerResult SetPerformanceDateValueList(SetPerformanceDateValueListModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.CSPerformanceCounters_Add);

            if (model == null)
                return PreconditionFailed("Invalid model");

            model.ItemList.ForEach(SetPerformanceDateValueListInternal);

            return Ok();
        }

        public ManagerResult<EmptyResultDto> SetPerformanceValueInternal(SetPerformanceValueModelDto model)
        {

            if (model.AccountUserId == null)
                return PreconditionFailed<EmptyResultDto>("AccountUserId not set");

            //получаем сервис чтобы понять существует ли он
            var cloudService = DbLayer.CloudServiceRepository.GetCloudService(model.CloudServiceId);
            if (cloudService == null)
                return NotFound<EmptyResultDto>($"Cloud service with Id: {model.CloudServiceId} not found");

            Logger.Trace("Adding new performance counter.");
            Logger.Info(model.ToString());

            var cspCounter = new CsPerformanceCounter
            {
                Id = Guid.NewGuid(),
                CloudServiceId = model.CloudServiceId,
                AccountUserId = model.AccountUserId.Value,
                PerformanceCounterId = model.PerformanceCounterId,
                PerformanceCounterValue = model.PerformanceCounterValue ?? default,
                FixationDateTime = DateTime.Now

            };

            _selfPerformanceTestingProvider.SaveStatistic(cspCounter, _configuration["AllowWriteCounter"]);

            return Ok(new EmptyResultDto());
        }

        public void SetPerformanceDateValueListInternal(SetPerformanceDateValueModel model)
        {
            if (_listCache.Any(x => x == model.CloudServiceId)) return;
            var cloudService = DbLayer.CloudServiceRepository.GetCloudService(model.CloudServiceId);
            if (cloudService != null)
                return;
            _listCache.Add(model.CloudServiceId);
        }

    }
}
