﻿namespace Clouds42.SelfPerformance.Models
{
    /// <summary>
    /// Модель обновления статистики производительности
    /// </summary>
    public class UpdateStatisticModel
    {
        /// <summary>
        /// Значение счетчика производительности
        /// </summary>
        public double PerformanceCounterValue { get; set; }

        /// <summary>
        /// ID счетчика производительности
        /// </summary>
        public string PerformanceCounterId { get; set; }

        /// <summary>
        /// Пользователь, инициировавший обновление статистики производительности
        /// </summary>
        public string PerformanceUserAgent { get; set; }
    }
}
