﻿using Clouds42.SelfPerformance.Contracts;
using Clouds42.SelfPerformance.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.SelfPerformance
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddSelfPerformance(this IServiceCollection services)
        {
            services.AddTransient<ISelfPerformanceTestingProvider, SelfPerformanceTestingProvider>();

            return services;
        }
    }
}
