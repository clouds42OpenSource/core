﻿using Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces;
using Clouds42.Domain.DataModels.History;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.LocaleChangesHistory.Providers
{
    /// <summary>
    /// Провайдер для создания истории изменения локали у аккаунта
    /// </summary>
    internal class CreateAccountLocaleChangesHistoryProvider(IUnitOfWork dbLayer)
        : ICreateAccountLocaleChangesHistoryProvider
    {
        /// <summary>
        /// Создать историю изменения локали у аккаунта
        /// (Дата изменения будет текущая)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void Create(Guid accountId)
        {
            var accountLocaleChangesHistory = new AccountLocaleChangesHistory
            {
                AccountId = accountId,
            };

            dbLayer.GetGenericRepository<AccountLocaleChangesHistory>().Insert(accountLocaleChangesHistory);
            dbLayer.Save();
        }
    }
}