﻿using Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces;

namespace Clouds42.Accounts.LocaleChangesHistory.Providers
{
    /// <summary>
    /// Провайдер для регистрации изменения локали у аккаунта
    /// </summary>
    internal class RegisterChangeOfLocaleForAccountProvider(
        IUpdateAccountLocaleChangesHistoryProvider updateAccountLocaleChangesHistoryProvider,
        ICreateAccountLocaleChangesHistoryProvider createAccountLocaleChangesHistoryProvider,
        IAccountLocaleChangesHistoryProvider accountLocaleChangesHistoryProvider)
        : IRegisterChangeOfLocaleForAccountProvider
    {
        /// <summary>
        /// Зарегестрировать изменение локали у аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void Register(Guid accountId)
        {
            var accountLocaleChangesHistory =
                accountLocaleChangesHistoryProvider.GetLocaleChangesHistoryForAccount(accountId);

            if (accountLocaleChangesHistory == null)
            {
                createAccountLocaleChangesHistoryProvider.Create(accountId);
                return;
            }

            updateAccountLocaleChangesHistoryProvider.UpdateChangesDate(accountLocaleChangesHistory, DateTime.Now);
        }
    }
}