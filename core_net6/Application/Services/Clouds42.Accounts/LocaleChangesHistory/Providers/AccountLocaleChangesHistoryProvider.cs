﻿using Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces;
using Clouds42.Domain.DataModels.History;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.LocaleChangesHistory.Providers
{
    /// <summary>
    /// Провайдер для работы с данными истории
    /// изменения локали у аккаунта
    /// </summary>
    internal class AccountLocaleChangesHistoryProvider(IUnitOfWork dbLayer) : IAccountLocaleChangesHistoryProvider
    {
        /// <summary>
        /// Получить изменение(историю изменения) локали для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>История изменения локали аккаунта</returns>
        public AccountLocaleChangesHistory GetLocaleChangesHistoryForAccount(Guid accountId) => dbLayer
            .GetGenericRepository<AccountLocaleChangesHistory>()
            .FirstOrDefault(history => history.AccountId == accountId);
    }
}
