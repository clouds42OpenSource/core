﻿using Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces;
using Clouds42.Domain.DataModels.History;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.LocaleChangesHistory.Providers
{
    /// <summary>
    /// Провайдер для обновления истории изменения локали у аккаунта
    /// </summary>
    internal class UpdateAccountLocaleChangesHistoryProvider(IUnitOfWork dbLayer)
        : IUpdateAccountLocaleChangesHistoryProvider
    {
        /// <summary>
        /// Обновить дату изменения у истории изменения локали аккаунта
        /// </summary>
        /// <param name="accountLocaleChangesHistory">История изменения локали у аккаунта</param>
        /// <param name="changesDate">Дата изменения</param>
        public void UpdateChangesDate(AccountLocaleChangesHistory accountLocaleChangesHistory,
            DateTime changesDate)
        {
            accountLocaleChangesHistory.ChangesDate = changesDate;
            dbLayer.GetGenericRepository<AccountLocaleChangesHistory>().Update(accountLocaleChangesHistory);
            dbLayer.Save();
        }
    }
}