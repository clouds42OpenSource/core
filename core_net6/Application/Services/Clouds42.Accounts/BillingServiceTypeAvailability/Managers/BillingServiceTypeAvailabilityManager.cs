﻿using Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.BillingServiceTypeAvailability.Managers
{
    /// <summary>
    /// Менеджер для установки времени доступности услуги сервиса после ее удаления
    /// </summary>
    public class BillingServiceTypeAvailabilityManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IBillingServiceTypeAvailabilityProvider billingServiceTypeAvailabilityProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IBillingServiceTypeAvailabilityManager
    {
        /// <summary>
        /// Удалить время активности для аккаунта по ID сервиса
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult DeleteAvailabilityDateTimeForAccountByBillingServiceId(Guid billingServiceId, Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.RemoveBillingServiceTypeAvailabilityDateTime);

                billingServiceTypeAvailabilityProvider.DeleteAvailabilityDateTimeForAccountByBillingServiceId(billingServiceId, accountId);
                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Обновить время активности для аккаунта
        /// </summary>
        /// <param name="billingServiceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="expireDateTime">Дата окончания активности</param>
        /// <returns>Результат обновления</returns>
        public ManagerResult UpdateAvailabilityDateTimeForAccount(Guid billingServiceTypeId, Guid accountId, DateTime expireDateTime)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.UpdateBillingServiceTypeAvailabilityDateTime);

                billingServiceTypeAvailabilityProvider.UpdateAvailabilityDateTimeForAccount(billingServiceTypeId, accountId, expireDateTime);
                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
