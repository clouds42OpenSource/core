﻿using Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces;
using Clouds42.Configurations.Configurations;

namespace Clouds42.Accounts.BillingServiceTypeAvailability.Providers
{
    /// <summary>
    /// Провайдер для подсчета отложденной даты удаления услуг
    /// </summary>
    internal class CalculateDelayedDateForDeletingServiceTypesProvider : ICalculateDelayedDateForDeletingServiceTypesProvider
    {
        private readonly Lazy<int> _beforeDeletingServiceTypesMonthsCount = new(CloudConfigurationProvider.BillingServices
            .GetBeforeDeletingServiceTypesMonthsCount);

        /// <summary>
        /// Вычислить отложденную дату удаления услуг 
        /// </summary>
        /// <param name="expireDateTime">Дата окончания активности Аренды 1С</param>
        /// <returns>Отложденная дата удаления услуг</returns>
        public DateTime Calculate(DateTime expireDateTime) =>
            expireDateTime.AddMonths(_beforeDeletingServiceTypesMonthsCount.Value);
    }
}
