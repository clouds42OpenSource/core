﻿using System.Text;
using Clouds42.Accounts.BillingServiceTypeAvailability.Helpers;
using Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.BillingServiceTypeAvailability.Providers
{
    /// <summary>
    /// Провайдер для установки времени доступности услуги сервиса после ее удаления
    /// </summary>
    internal class BillingServiceTypeAvailabilityProvider(
        BillingServiceTypeAvailabilityDataHelper billingServiceTypeAvailabilityDataHelper,
        IUnitOfWork dbLayer,
        ICalculateDelayedDateForDeletingServiceTypesProvider calculateDelayedDateForDeletingServiceTypesProvider,
        ILogger42 logger)
        : IBillingServiceTypeAvailabilityProvider
    {
        /// <summary>
        /// Установить время активности для аккаунтов
        /// </summary>
        /// <param name="model">Модель отложенного удаления услуг сервиса для аккаунтов</param>
        public void SetAvailabilityDateTimeForAccounts(
            DelayedServiceTypesDeletionForAccountsDto model)
        {

            if (!model.AccountsForSetAvailabilityDateTime.Any() || !model.ServiceTypeIds.Any())
                return;

            var errorMessageBuilder = new StringBuilder();

            model.AccountsForSetAvailabilityDateTime.ForEach(accountInfo =>
                model.ServiceTypeIds.ForEach(billingServiceTypeId =>
                    CreateServiceTypeActivityForAccount(billingServiceTypeId, accountInfo, errorMessageBuilder)));

            var errorMessage = errorMessageBuilder.ToString();
            if (!string.IsNullOrEmpty(errorMessage))
                throw new InvalidOperationException($"Сбои при установке времени активности услуг сервиса {model.ServiceId}. {errorMessage}");
        }

        /// <summary>
        /// Удалить время активности для аккаунта
        /// </summary>
        /// <param name="billingServiceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public void DeleteAvailabilityDateTimeForAccount(Guid billingServiceTypeId, Guid accountId)
        {
            var billingServiceTypeActivityForAccount =
                billingServiceTypeAvailabilityDataHelper.GetBillingServiceTypeActivityForAccount(billingServiceTypeId,
                    accountId);

            DeleteAvailabilityDateTimeForAccount(billingServiceTypeActivityForAccount);
        }

        /// <summary>
        /// Обновить время активности для аккаунта
        /// </summary>
        /// <param name="billingServiceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="expireDateTime">Дата окончания активности Аренды 1С</param>
        public void UpdateAvailabilityDateTimeForAccount(Guid billingServiceTypeId, Guid accountId,
            DateTime expireDateTime)
        {
            if (expireDateTime < DateTime.Now)
            {
                DeleteAvailabilityDateTimeForAccount(billingServiceTypeId, accountId);
                return;
            }

            var billingServiceTypeActivityForAccount =
                billingServiceTypeAvailabilityDataHelper.GetBillingServiceTypeActivityForAccount(billingServiceTypeId,
                    accountId);

            billingServiceTypeActivityForAccount.AvailabilityDateTime =
                calculateDelayedDateForDeletingServiceTypesProvider.Calculate(expireDateTime);
            dbLayer.GetGenericRepository<BillingServiceTypeActivityForAccount>().Update(billingServiceTypeActivityForAccount);
            dbLayer.Save();
        }

        /// <summary>
        /// Удалить время активности для аккаунта по ID сервиса
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        public void DeleteAvailabilityDateTimeForAccountByBillingServiceId(Guid billingServiceId, Guid accountId)
        {
            var serviceTypesActivityForAccount =
                billingServiceTypeAvailabilityDataHelper.GetBillingServiceTypesActivityForAccount(billingServiceId,
                    accountId);

            logger.Trace($"Удаление времени активности для аккаунта {accountId} по ID сервиса {billingServiceId}");

            serviceTypesActivityForAccount.ForEach(serviceTypeActivity =>
            {
                if (serviceTypeActivity.AvailabilityDateTime.Date > DateTime.Now.Date)
                    return;

                var billingServiceType = serviceTypeActivity.BillingServiceType;
                logger.Trace($"Услуга {billingServiceType.Name} сервиса {billingServiceType.Service.Name} больше не доступна для аккаунта {accountId}");
                DeleteAvailabilityDateTimeForAccount(serviceTypeActivity);
            });
        }

        /// <summary>
        /// Создать объект даты активности услуги сервиса для аккаунта
        /// </summary>
        /// <param name="billingServiceTypeId">ID услуги сервиса</param>
        /// <param name="accountInfo">Модель аккаунта с отложенной датой удаления услуг</param>
        /// <param name="errorMessageBuilder">Билдер сообщений об ошибке</param>
        private void CreateServiceTypeActivityForAccount(Guid billingServiceTypeId,
            AccountWithDelayedDateOfRemoveServiceTypesDto accountInfo, StringBuilder errorMessageBuilder)
        {
            try
            {
                var serviceTypeActivity = new BillingServiceTypeActivityForAccount
                {
                    AccountId = accountInfo.AccountId,
                    AvailabilityDateTime = accountInfo.DelayedDateForDeletingServiceTypes,
                    BillingServiceTypeId = billingServiceTypeId
                };

                dbLayer.GetGenericRepository<BillingServiceTypeActivityForAccount>().Insert(serviceTypeActivity);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                errorMessageBuilder.AppendLine(
                    $" - операция для аккаунта {accountInfo.AccountId} завершена с ошибкой. Причина: {ex.GetFullInfo(false)}");
            }
        }

        /// <summary>
        /// Удалить время активности для аккаунта
        /// </summary>
        /// <param name="serviceTypeActivityForAccount">Активность услуги сервиса для аккаунта</param>
        private void DeleteAvailabilityDateTimeForAccount(BillingServiceTypeActivityForAccount serviceTypeActivityForAccount)
        {
            dbLayer.GetGenericRepository<BillingServiceTypeActivityForAccount>()
                .Delete(serviceTypeActivityForAccount);
            dbLayer.Save();
        }
    }
}
