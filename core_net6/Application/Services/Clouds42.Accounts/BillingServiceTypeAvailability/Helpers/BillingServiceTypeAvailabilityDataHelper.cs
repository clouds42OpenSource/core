﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.BillingServiceTypeAvailability.Helpers
{
    /// <summary>
    /// Хэлпер для получения данных по времени активности услуги сервиса после ее удаления
    /// </summary>
    public class BillingServiceTypeAvailabilityDataHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить список аккаунтов которые используют удаленные услуги сервиса
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <returns>Список аккаунтов</returns>
        public List<AccountWhichUsesDeletedServiceTypesDto> GetAccountsWhichUsesDeletedServiceTypes(Guid serviceId)
        {
            var nowDateTime = DateTime.Now;

            return dbLayer.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .Join(dbLayer.ResourceConfigurationRepository.AsQueryable(),
                    x => new { x.AccountId, BillingServiceId = serviceId },
                    z => new { z.AccountId, z.BillingServiceId },
                    (configuration, resourcesConfiguration) => new { configuration, resourcesConfiguration })
                .Where(x => x.configuration.BillingService.SystemService == Clouds42Service.MyEnterprise)
                .Select(x => new AccountWhichUsesDeletedServiceTypesDto
                {
                    AccountId = x.configuration.AccountId,
                    Rent1CExpireDate = x.configuration.ExpireDate!.Value,
                    NeedDelayedDeletion = x.configuration.Frozen.HasValue && !x.configuration.Frozen.Value &&
                                          x.configuration.ExpireDate > nowDateTime &&
                                          (!x.resourcesConfiguration.IsDemoPeriod ||
                                           (x.resourcesConfiguration.ExpireDate.HasValue &&
                                            x.resourcesConfiguration.ExpireDate > nowDateTime))
                })
                .Distinct()
                .ToList();
        }
        /// <summary>
        /// Получить модель данных активности услуги для аккаунта
        /// </summary>
        /// <param name="serviceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Модель данных активности услуги для аккаунта</returns>
        public BillingServiceTypeActivityForAccount GetBillingServiceTypeActivityForAccount(Guid serviceTypeId,
            Guid accountId)
            => dbLayer.GetGenericRepository<BillingServiceTypeActivityForAccount>().FirstOrThrowException(bs =>
                bs.BillingServiceTypeId == serviceTypeId && bs.AccountId == accountId);

        /// <summary>
        /// Получить модель данных активности услуг сервиса для аккаунта
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Модель данных активности услуг сервиса для аккаунта</returns>
        public List<BillingServiceTypeActivityForAccount> GetBillingServiceTypesActivityForAccount(
            Guid billingServiceId,
            Guid accountId)
            => dbLayer.GetGenericRepository<BillingServiceTypeActivityForAccount>().Where(bs =>
                bs.BillingServiceType.ServiceId == billingServiceId && bs.AccountId == accountId).ToList();
    }
}
