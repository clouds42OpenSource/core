﻿using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.DataContracts.BaseModel;
using PagedList.Core;
using Clouds42.Accounts.ServiceAccounts.Helpers;
using Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.ServiceAccounts.Providers
{
    /// <summary>
    /// Провайдер данных служебных аккаунтов
    /// </summary>
    internal class ServiceAccountProvider(
        IUnitOfWork dbLayer,
        ServiceAccountValidator serviceAccountValidator,
        IHandlerException handlerException)
        : IServiceAccountProvider
    {
        /// <summary>
        /// Получить список служебных аккаунтов
        /// </summary>
        /// <param name="args">Фильтр</param>
        /// <returns>Список служебных аккаунтов</returns>
        public GetServiceResultAccountsDto GetServiceAccounts(GetServiceAccountsDto args)
        {
            const int itemsPerPage = GlobalSettingsConstants.ReferenceDataPages.DefaultDataGridRecordsCount;
            var records = new ServiceAccountsSelector(dbLayer, args?.Filter).SelectWithFilters();
            var recordsCount = records.Count();
            var pageNumber = PaginationHelper.GetFilterPageNumberBasedOnItemsCount(recordsCount, itemsPerPage,
                args?.PageNumber ?? 1);
            var pageRecords = ServiceAccountsSortingUtilityDto.MakeSorting(records, args?.SortingData)
                .ToPagedList(pageNumber, itemsPerPage);

            var resultRecords = pageRecords.Select(item =>
                new ServiceAccountItemDto
                {
                    AccountId = item.Id,
                    AccountCaption = item.Account.AccountCaption,
                    AccountIndexNumber = item.Account.IndexNumber,
                    AccountUserInitiatorName = item.AccountUser.Login,
                    CreationDateTime = item.CreationDateTime
                }).ToArray();

            return new GetServiceResultAccountsDto
            {
                Records = resultRecords,
                Pagination = new PaginationBaseDto(pageNumber, recordsCount, itemsPerPage)
            };
        }

        /// <summary>
        /// Добавить служебный аккаунт
        /// </summary>
        /// <param name="args">Модель создания служебного аккаунта</param>
        /// <returns>Модель результата после создания служебного аккаунта</returns>
        public AddServiceAccountResultDto AddServiceAccountItem(CreateServiceAccountDto args)
        {
            try
            {
                var validationResult = serviceAccountValidator.Validate(args);

                if (!validationResult.Success)
                    throw new InvalidOperationException(validationResult.Message);


                var serviceAccount = new ServiceAccount
                {
                    Id = args.AccountId,
                    AccountUserInitiatorId = args.AccountUserInitiatorId
                };

                dbLayer.ServiceAccountRepository.Insert(serviceAccount);
                dbLayer.Save();

                return new AddServiceAccountResultDto
                {
                    CreationDateTime = DateTime.Now,
                    AccountUserInitiatorName = args.AccountUserInitiatorName
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка добавления служебного аккаунта] {args.AccountId}");
                throw;
            }
        }

        /// <summary>
        /// Удалить служебный аккаунт
        /// </summary>
        /// <param name="serviceAccountId">ID служебного аккаунта</param>
        public void DeleteServiceAccount(Guid serviceAccountId)
        {
            try
            {
                var serviceAccount = dbLayer.ServiceAccountRepository.FirstOrDefault(w => w.Id == serviceAccountId) ??
                                     throw new NotFoundException(
                                         $"Служебный аккаунт по ID {serviceAccountId} не найден");

                dbLayer.ServiceAccountRepository.Delete(serviceAccount);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления служебного аккаунта] {serviceAccountId}");
                throw;
            }
        }
    }
}
