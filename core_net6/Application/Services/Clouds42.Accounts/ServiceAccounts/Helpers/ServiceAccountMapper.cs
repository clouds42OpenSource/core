﻿using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.Domain.DataModels;

namespace Clouds42.Accounts.ServiceAccounts.Helpers
{
    /// <summary>
    /// Маппер для служебных аккаунтов
    /// </summary>
    public static class ServiceAccountMapper
    {
        /// <summary>
        /// Смапить доменную модель к модели контракта
        /// </summary>
        /// <param name="serviceAccount">Доменная модель</param>
        /// <returns>Модель контракта</returns>
        public static ServiceAccountDto MapToServiceAccountDc(this ServiceAccount serviceAccount)
            => new()
            {
                AccountId = serviceAccount.Id,
                AccountCaption = serviceAccount.Account.AccountCaption,
                AccountIndexNumber = serviceAccount.Account.IndexNumber,
                AccountUserInitiatorId = serviceAccount.AccountUserInitiatorId,
                AccountUserInitiatorName = serviceAccount.AccountUser.Login,
                CreationDateTime = serviceAccount.CreationDateTime
            };
    }
}
