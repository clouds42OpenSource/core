﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.ServiceAccounts.Helpers
{
    /// <summary>
    /// Класс для выбора служебных аккаунтов
    /// </summary>
    public sealed class ServiceAccountsSelector
    {
        private readonly IUnitOfWork _dbLayer;

        /// <summary>
        /// Параметры фильтрации записей служебных аккаунтов
        /// </summary>
        private readonly GetServiceAccountsFilterDto _filterParams;

        /// <summary>
        /// Конструктор класса <see cref="ServiceAccountsSelector"/>
        /// </summary>
        /// <param name="dbLayer">Дата контекст</param>
        /// <param name="filterParams">Параметры фильтрации записей служебных аккаунтов</param>
        public ServiceAccountsSelector(IUnitOfWork dbLayer, GetServiceAccountsFilterDto filterParams)
        {
            _dbLayer = dbLayer;
            _filterParams = filterParams;
        }

        /// <summary>
        /// Отфильтровать записи по ведённой строке
        /// </summary>
        /// <param name="records">Записи для фильтрации</param>
        /// <returns>Отфильтрованные записи</returns>
        private IQueryable<ServiceAccount> FilterBySearchLine(IQueryable<ServiceAccount> records)
            => _filterParams == null || string.IsNullOrEmpty(_filterParams.SearchLine)
                ? records
                : records.Where(x => x.Account.IndexNumber.ToString().Contains(_filterParams.SearchLine) ||
                                     x.Account.AccountCaption.Contains(_filterParams.SearchLine));

        /// <summary>
        /// Получить отфильтрованный список служебных аккаунтов
        /// </summary>
        /// <returns>Отфильтрованный список служебных аккаунтов</returns>
        public IQueryable<ServiceAccount> SelectWithFilters()
        {
            return _dbLayer.ServiceAccountRepository.WhereLazy().ComposeFilters(
                FilterBySearchLine);
        }
    }
}