﻿using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.Accounts.ServiceAccounts.Helpers
{
    /// <summary>
    /// Валидатор для модели служебного аккаунта
    /// </summary>
    public class ServiceAccountValidator(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Провалидировать модель создания служебного аккаунта
        /// </summary>
        /// <param name="createServiceAccountDc">Модель создания служебного аккаунта</param>
        /// <returns>Результат валидации</returns>
        public ValidateResult Validate(CreateServiceAccountDto createServiceAccountDc)
        {
            var checkAccountResult = CheckAccount(createServiceAccountDc);

            if (!checkAccountResult.Success)
                return checkAccountResult;

            var checkAccountUserInitiatorResult = CheckAccountUserInitiator(createServiceAccountDc);

            return checkAccountUserInitiatorResult;
        }

        /// <summary>
        /// Проверить создаваемый аккаунт
        /// </summary>
        /// <param name="createServiceAccountDc">Модель создания служебного аккаунта</param>
        /// <returns>Результат валидации</returns>
        private ValidateResult CheckAccount(CreateServiceAccountDto createServiceAccountDc)
        {
            if (createServiceAccountDc.AccountId == Guid.Empty)
                return CreateErrorValidateResult("Для добавления служебного аккаунта необходимо указать его номер.");

            var account = dbLayer.AccountsRepository.GetAccount(createServiceAccountDc.AccountId);

            if (account == null)
                return CreateErrorValidateResult("Указанный аккаунт не существует");

            var alreadyExistServiceAccount = dbLayer.ServiceAccountRepository.FirstOrDefault(w => w.Id == account.Id);

            if (alreadyExistServiceAccount != null)
                return CreateErrorValidateResult("Такой служебный аккаунт уже существует");

            return new ValidateResult { Success = true };
        }

        /// <summary>
        /// Проверить пользователя, инициировавшего создание
        /// </summary>
        /// <param name="createServiceAccountDc">Модель создания служебного аккаунта</param>
        /// <returns>Результат валидации</returns>
        private ValidateResult CheckAccountUserInitiator(CreateServiceAccountDto createServiceAccountDc)
        {
            if (createServiceAccountDc.AccountUserInitiatorId == Guid.Empty)
                return CreateErrorValidateResult("Невозможно определить пользователя, инициировавшего операцию создания.");

            var accountUser =
                dbLayer.AccountUsersRepository.GetAccountUser(createServiceAccountDc.AccountUserInitiatorId);

            if (accountUser == null)
                return CreateErrorValidateResult("Невозможно определить пользователя, инициировавшего операцию создания.");

            return new ValidateResult { Success = true };
        }

        /// <summary>
        /// Создать отрицательный результат валидации
        /// </summary>
        /// <param name="message">Сообщение об ошибке</param>
        /// <returns>Отрицательный результат валидации</returns>
        private ValidateResult CreateErrorValidateResult(string message)
            => new()
            {
                Success = false,
                Message = message
            };
    }
}
