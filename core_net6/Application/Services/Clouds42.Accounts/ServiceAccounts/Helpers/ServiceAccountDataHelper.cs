﻿using Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Accounts.ServiceAccounts.Helpers
{
    /// <summary>
    /// Хэлпер данных служебных аккаунтов
    /// </summary>
    public class ServiceAccountDataHelper(IUnitOfWork dbLayer) : IServiceAccountDataHelper
    {
        /// <summary>
        /// Признак, что аккаунт служебный
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>true - если аккаунт служебный</returns>
        public bool IsServiceAccount(Guid accountId)
        {
            return dbLayer.ServiceAccountRepository
                .AsQueryableNoTracking()
                .Any(x => x.Id == accountId);
        }

        public async Task<bool> IsServiceAccountAsync(Guid accountId, CancellationToken cancellationToken)
        {
            return await dbLayer.ServiceAccountRepository
                .AsQueryableNoTracking()
                .AnyAsync(x => x.Id == accountId, cancellationToken);
        }

        /// <summary>
        /// Проверить возможность управления сервисом
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void CheckAbilityToManageService(Guid accountId)
        {
            if (!IsServiceAccount(accountId))
                return;

            throw new InvalidOperationException("Служебным аккаунтам недоступно управление сервисами");
        }
    }
}
