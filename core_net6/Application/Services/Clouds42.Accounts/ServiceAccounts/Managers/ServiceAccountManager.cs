﻿using Clouds42.BLL.Common;
using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.HandlerExeption.Contract;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.ServiceAccounts.Managers
{
    /// <summary>
    /// Менеджер служебных аккаунтов
    /// </summary>
    public class ServiceAccountManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IServiceAccountProvider serviceAccountProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;


        /// <summary>
        /// Получить список служебных аккаунтов
        /// </summary>
        /// <returns>Список служебных аккаунтов</returns>
        public ManagerResult<GetServiceResultAccountsDto> GetServiceAccounts(GetServiceAccountsDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.GetServiceAccounts, () => AccessProvider.ContextAccountId);

                return Ok(serviceAccountProvider.GetServiceAccounts(args));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения списка служебных аккаунтов]");
                return PreconditionFailed<GetServiceResultAccountsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Добавить служебный аккаунт
        /// </summary>
        /// <param name="args">Модель создания служебного аккаунта</param>
        /// <returns>Результат добавления</returns>
        public ManagerResult<AddServiceAccountResultDto> AddServiceAccountItem(AddServiceAccountDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CreateServiceAccount, () => AccessProvider.ContextAccountId);

                var user = AccessProvider.GetUser();

                var result = serviceAccountProvider.AddServiceAccountItem(CreateServiceAccountDto.CreateWith(args, user.Id, user.Name));

                LogEvent(() => args.AccountId, LogActions.CreateServiceAccount, "Служебный аккаунт создан");
                return Ok(result);
            }
            catch (Exception ex)
            {
                var message = "[Ошибка при добавлении служебного аккаунта]";
                _handlerException.Handle(ex, $"{message} {args.AccountId}");
                LogEvent(() => args.AccountId, LogActions.CreateServiceAccount,
                    $"{message}. Причина: {ex.Message}");
                return PreconditionFailed<AddServiceAccountResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Удалить служебный аккаунт
        /// </summary>
        /// <param name="serviceAccountId">ID служебного аккаунта</param>
        /// <returns>Результат удаления</returns>
        public ManagerResult DeleteServiceAccount(Guid serviceAccountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeleteServiceAccount, () => AccessProvider.ContextAccountId);

                serviceAccountProvider.DeleteServiceAccount(serviceAccountId);

                LogEvent(() => serviceAccountId, LogActions.DeleteServiceAccount, "Служебный аккаунт удален");
                return Ok();
            }
            catch (Exception ex)
            {
                var message = "[Ошибка при удалении служебного аккаунта]";
                _handlerException.Handle(ex, $"{message} {serviceAccountId}");
                LogEvent(() => serviceAccountId, LogActions.DeleteServiceAccount,
                    $"{message}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
