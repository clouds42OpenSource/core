﻿using System.Collections;
using System.DirectoryServices.AccountManagement;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using Clouds42.Accounts.Audit.Helpers;
using Clouds42.Accounts.Contracts.Audit.Interfaces;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.Accounts.Audit.Providers
{
    /// <summary>
    /// Провайдер проверки папок аккаунта
    /// </summary>
    internal class AuditAccountFoldersProvider(AccountDataHelper accountDataHelper, IHandlerException handlerException)
        : IAuditAccountFoldersProvider
    {
        /// <summary>
        /// Проверить права на папки аккаунтов 
        /// </summary>
        public void AuditAccountFoldersNtfsRules()
        {
            var accountAuditInfoList = accountDataHelper.GetAccountAuditInfoList();
            var messageBuilder = new StringBuilder();

            accountAuditInfoList.ForEach(accountInfo =>
            {
                var accountFolder = Path.Combine(accountInfo.SegmentFileStoragePath, $@"company_{accountInfo.AccountIndexNumber}");
                if (!Directory.Exists(accountFolder))
                    return;

                var accountGroupName = DomainCoreGroups.CompanyGroupBuild(accountInfo.AccountIndexNumber);
                if (!CheckAccountFolderAccessControl(accountFolder, accountGroupName))
                    messageBuilder.AppendLine($"- Для папки аккаунта {accountInfo.AccountIndexNumber} по пути {accountFolder} не установлено наследование!");
            });

            var errorMessage = messageBuilder.ToString();
            if (!string.IsNullOrEmpty(errorMessage))
                throw new InvalidOperationException($"Результат проверки папок аккаунтов: {errorMessage}");
        }

        /// <summary>
        /// Проверить права на папку аккаунта
        /// </summary>
        /// <param name="accountFolderPath">Путь к папке аккаунта</param>
        /// <param name="accountGroupName">Название группы аккаунта</param>
        private bool CheckAccountFolderAccessControl(string accountFolderPath, string accountGroupName)
        {
            var accessControlList = new DirectoryInfo(accountFolderPath).GetAccessControl();
            var accessRules = accessControlList.GetAccessRules(true, true,
                typeof(SecurityIdentifier));

            using var ctx = ContextActivatorFactory.Create().OpenCompanyUO();
            var accountGroup = GroupPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, accountGroupName);
            return accountGroup != null && CheckFileSystemAccessRules(accessRules, accountGroup);
        }

        /// <summary>
        /// Проверить права на папку для группы аккаунта
        /// </summary>
        /// <param name="accessRules">Список прав на папку</param>
        /// <param name="accountGroup">Группа аккаунта</param>
        private bool CheckFileSystemAccessRules(IEnumerable accessRules, Principal accountGroup)
        {
            var checkIsSuccess = false;

            foreach (FileSystemAccessRule accessRule in accessRules)
            {
                try
                {
                    if (!accessRule.IdentityReference.Value.Equals(accountGroup.Sid.Value))
                        continue;

                    checkIsSuccess = accessRule.InheritanceFlags == (InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit);
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, "[Ошибка проверки прав на папку аккаунта]");
                }
            }

            return checkIsSuccess;
        }
    }
}
