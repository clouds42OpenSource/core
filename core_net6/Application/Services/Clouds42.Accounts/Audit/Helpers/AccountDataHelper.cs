﻿using Clouds42.DataContracts.Account;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Audit.Helpers
{
    /// <summary>
    /// Хэлпер получения данных по аккаунтам
    /// </summary>
    public class AccountDataHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить список данных по аккаунтам
        /// </summary>
        /// <returns>Список ID аккаунтов</returns>
        public List<AccountAuditInfoDto> GetAccountAuditInfoList()
        {
            return dbLayer.AccountsRepository
                .AsQueryableNoTracking()
                .Where(x => x.Status != AccountStatusEnum.Deleted.ToString() && (!x.Removed.HasValue || !x.Removed.Value))
                .Select(x => new AccountAuditInfoDto
                {
                    AccountIndexNumber = x.IndexNumber,
                    SegmentFileStoragePath = x.AccountConfiguration.Segment.CloudServicesFileStorageServer.ConnectionAddress
                })
                .ToList();
        }
    }
}
