﻿using Clouds42.Accounts.Contracts.Audit.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Audit.Managers
{
    /// <summary>
    /// Менеджер аудита папок аккаунта
    /// </summary>
    public class AuditAccountFoldersManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAuditAccountFoldersProvider auditAccountFoldersProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Проверить права на папки аккаунтов 
        /// </summary>
        /// <returns>Результат проверки</returns>
        public ManagerResult AuditAccountFoldersNtfsRules()
        {
            try
            {
                auditAccountFoldersProvider.AuditAccountFoldersNtfsRules();
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Проверка прав на папки аккаунтов]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
