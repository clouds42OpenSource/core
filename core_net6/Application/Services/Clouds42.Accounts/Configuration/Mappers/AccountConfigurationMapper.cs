﻿using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.Domain.DataModels.AccountProperties;

namespace Clouds42.Accounts.Configuration.Mappers
{
    /// <summary>
    /// Маппер моделей конфигурации аккаунта
    /// </summary>
    public static class AccountConfigurationMapper
    {
        /// <summary>
        /// Выполнить маппинг к конфигурации аккаунта 
        /// </summary>
        /// <param name="accountConfigurationModel">Модель конфигурации аккаунта</param>
        /// <returns>Конфигурация аккаунта</returns>
        public static AccountConfiguration MapToAccountConfiguration(
            this AccountConfigurationDto accountConfigurationModel) =>
            new()
            {
                AccountId = accountConfigurationModel.AccountId,
                FileStorageId = accountConfigurationModel.FileStorageId,
                SegmentId = accountConfigurationModel.SegmentId,
                SupplierId = accountConfigurationModel.SupplierId,
                LocaleId = accountConfigurationModel.LocaleId,
                IsVip = accountConfigurationModel.IsVip,
                Type = accountConfigurationModel.Type,
                CloudStorageWebLink = accountConfigurationModel.CloudStorageWebLink
            };
    }
}