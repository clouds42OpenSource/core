﻿using Clouds42.Accounts.Configuration.Mappers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Configuration.Providers
{
    /// <summary>
    /// Провайдер для создания конфигурации аккаунта
    /// </summary>
    internal class CreateAccountConfigurationProvider(IUnitOfWork dbLayer) : ICreateAccountConfigurationProvider
    {
        /// <summary>
        /// Создать конфигурацию аккаунта
        /// </summary>
        /// <param name="model">Модель создания конфигурации аккаунта</param>
        public void Create(AccountConfigurationDto model)
        {
            var accountConfiguration = model.MapToAccountConfiguration();
            dbLayer.GetGenericRepository<AccountConfiguration>().Insert(accountConfiguration);
            dbLayer.Save();
        }
    }
}
