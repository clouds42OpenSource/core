﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Configuration.Providers
{
    /// <summary>
    /// Провайдер для обновления конфигурации аккаунта
    /// </summary>
    internal class UpdateAccountConfigurationProvider(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IUpdateAccountConfigurationProvider
    {
        private readonly Lazy<string> _cloudLink = new(CloudConfigurationProvider.Tomb.GetCloudLink);
        private readonly Lazy<string> _bucketName = new(CloudConfigurationProvider.Tomb.GetBucketName);

        /// <summary>
        /// Обновить Id локали
        /// (так же обновит поставщика дефолтного по локали)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="localeId">Id локали</param>
        public void UpdateLocaleId(Guid accountId, Guid localeId)
        {
            var supplier = dbLayer.SupplierRepository.FirstOrDefault(s => s.IsDefault && s.LocaleId == localeId) ??
                           throw new NotFoundException(
                               $"Не удалось найти поставщика по умолчанию для локали '{localeId}'");

            PerformUpdateAccountConfiguration(accountId, configuration =>
            {
                configuration.SupplierId = supplier.Id;
                configuration.LocaleId = localeId;
            });
        }

        /// <summary>
        /// Обновить признак указывающий что аккаунт вип
        /// </summary>
        /// <param name="accountId">Id аккаунт</param>
        /// <param name="isVipAccount">Признак указывающий что аккаунт вип</param>
        public void UpdateSignVipAccount(Guid accountId, bool isVipAccount) =>
            PerformUpdateAccountConfiguration(accountId, configuration => { configuration.IsVip = isVipAccount; });

        /// <summary>
        /// Обновить Id файлового хранилища
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        public void UpdateFileStorageId(Guid accountId, Guid fileStorageId) => PerformUpdateAccountConfiguration(
            accountId, configuration => { configuration.FileStorageId = fileStorageId; });

        /// <summary>
        /// Обновить ссылку на склеп аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="folderPrefix">Id папки аккаунта в склепе</param>
        public void UpdateCloudStorageWebLink(Guid accountId, string folderPrefix) => PerformUpdateAccountConfiguration(
            accountId,
            configuration => { configuration.CloudStorageWebLink = string.Format(_cloudLink.Value, _bucketName.Value, folderPrefix); });

        /// <summary>
        /// Выполнить обновление конфигурации аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="updateConfigurationDataAction">Действие для обновления данных конфигурации аккаунта</param>
        private void PerformUpdateAccountConfiguration(Guid accountId,
            Action<AccountConfiguration> updateConfigurationDataAction)
        {
            var accountConfiguration = accountConfigurationDataProvider.GetAccountConfiguration(accountId);
            updateConfigurationDataAction(accountConfiguration);
            dbLayer.GetGenericRepository<AccountConfiguration>().Update(accountConfiguration);
            dbLayer.Save();
        }
    }
}
