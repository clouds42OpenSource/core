﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Accounts.Configuration.Providers
{
    /// <summary>
    /// Провайдер для работы с данными конфигурации аккаунта
    /// </summary>
    public class AccountConfigurationDataProvider(IUnitOfWork dbLayer) : IAccountConfigurationDataProvider
    {
        /// <summary>
        /// Получить конфигурацию аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Конфигурация аккаунта</returns>
        public AccountConfiguration GetAccountConfiguration(Guid accountId) =>
            dbLayer.AccountConfigurationRepository.GetAccountConfiguration(accountId) ??
            throw new NotFoundException($"Не удалось получить конфигурацию аккаунта по Id '{accountId}'");

        /// <summary>
        /// Получить модель конфигурации аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель конфигурации аккаунта</returns>
        public AccountConfigurationDto GetAccountConfigurationDc(Guid accountId) => dbLayer.AccountConfigurationRepository.GetAccountConfigurationDc(accountId);

        /// <summary>
        /// Получить модель аккаунта с конфигурацией
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель аккаунта с конфигурацией</returns>
        public AccountWithConfigurationDto GetAccountWithConfiguration(Guid accountId)
            => dbLayer.AccountConfigurationRepository.GetAccountWithConfiguration(accountId)
            ?? throw new NotFoundException($"Не удалось получить аккаунт и его конфигурацию по Id '{accountId}'");

        /// <summary>
        /// Получить Id файлового хранилища аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id файлового хранилища аккаунта</returns>
        public Guid GetAccountFileStorageId(Guid accountId) => GetAccountConfiguration(accountId).FileStorageId;

        /// <summary>
        /// Получить Id локали аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id локали аккаунта</returns>
        public Guid GetAccountLocaleId(Guid accountId) => GetAccountConfiguration(accountId).LocaleId;

        /// <summary>
        /// Получить локаль аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Локаль аккаунта</returns>
        public Locale GetAccountLocale(Guid accountId) =>
            (Locale)GetAccountConfigurationByLocaleData(accountId).Locale;

        /// <summary>
        /// Получить локаль пользователя аккаунта
        /// </summary>
        /// <param name="accountUserId">Id пользователя аккаунта</param>
        /// <returns>Локаль пользователя аккаунта</returns>
        public Locale GetAccountUserLocale(Guid accountUserId)
            => (Locale)dbLayer.AccountConfigurationRepository.GetAccountUserLocale(accountUserId)
            ?? throw new NotFoundException($"Не удалось получить локаль для пользователя '{accountUserId}'");

        /// <summary>
        /// Получить данные конфигурации аккаунта по локали
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные конфигурации аккаунта по локали</returns>
        public AccountConfigurationByLocaleDataDto GetAccountConfigurationByLocaleData(Guid accountId)
            => dbLayer.AccountConfigurationRepository.GetAccountConfigurationByLocaleData(accountId)
            ?? throw new NotFoundException($"Не удалось получить конфигурацию аккаунта по локали. Id аккаунта '{accountId}'");

        /// <summary>
        /// Получить конфигурации аккаунтов по локали
        /// </summary>
        /// <returns>Конфигурации аккаунтов по сегменту</returns>
        public IQueryable<AccountConfigurationByLocaleDataDto> GetAccountConfigurationsDataByLocale()
            => dbLayer.AccountConfigurationRepository.GetAccountConfigurationsDataByLocale();

        /// <summary>
        /// Проверить что аккаунт является VIP
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Признак указывающий что аккаунт VIP</returns>
        public bool IsVipAccount(Guid accountId) => GetAccountConfiguration(accountId).IsVip;

        public async Task<bool> IsVipAccountAsync(Guid accountId) => await dbLayer.AccountConfigurationRepository.AsQueryableNoTracking().AnyAsync(x => x.AccountId == accountId && x.IsVip);

        /// <summary>
        /// Проверить что аккаунту нужно автосоздание серверной базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Признак указывающий что аккаунту нужно создать серверную базу </returns>
        public bool NeedAutoCreateClusteredDb(Guid accountId) => GetAccountConfiguration(accountId).CreateClusterDatabase;

        /// <summary>
        /// Получить модели аккаунтов с конфигурацией
        /// </summary>
        /// <returns>Модели аккаунтов с конфигурацией</returns>
        public IQueryable<AccountWithConfigurationDto> GetAccountsWithConfiguration()
            => dbLayer.AccountConfigurationRepository.GetAccountsWithConfiguration();

        /// <summary>
        /// Получить конфигурации аккаунтов по сегменту
        /// </summary>
        /// <returns>Конфигурации аккаунтов по сегменту</returns>
        public IQueryable<AccountConfigurationBySegmentDataDto> GetAccountConfigurationsDataBySegment()
            => dbLayer.AccountConfigurationRepository.GetAccountConfigurationsDataBySegment();

        /// <summary>
        /// Получить данные конфигурации аккаунта по сегменту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные конфигурации аккаунта по сегменту</returns>
        public AccountConfigurationBySegmentDataDto GetAccountConfigurationBySegmentData(Guid accountId)
            => dbLayer.AccountConfigurationRepository.GetAccountConfigurationBySegmentData(accountId)
            ?? throw new NotFoundException($"Не удалось получить конфигурацию аккаунта по сегменту. Id аккаунта '{accountId}'");

        /// <summary>
        /// Получить сегмент аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Сегмент аккаунта</returns>
        public CloudServicesSegment GetAccountSegment(Guid accountId) =>
            (CloudServicesSegment)GetAccountConfigurationBySegmentData(accountId).Segment;

        /// <summary>
        /// Получить хранилище бэкапов по сегменту аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Хранилище бэкапов по сегменту аккаунта</returns>
        public async Task<CloudServicesBackupStorage> GetBackupStorageByAccountSegment(Guid accountId)
            => await dbLayer.AccountConfigurationRepository.GetBackupStorageByAccountSegment(accountId)
            ?? throw new NotFoundException($"Не удалось получить хранилище бэкапов по сегменту аккаунта '{accountId}'");

        /// <summary>
        /// Получить конфигурации аккаунтов по поставщику
        /// </summary>
        /// <returns>Конфигурации аккаунтов по поставщику</returns>
        public IQueryable<AccountConfigurationBySupplierDataDto> GetAccountConfigurationsDataBySupplier()
            => dbLayer.AccountConfigurationRepository.GetAccountConfigurationsDataBySupplier();

        /// <summary>
        /// Получить поставщика аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Поставщик аккаунта</returns>
        public Supplier GetAccountSupplier(Guid accountId) =>
            (Supplier)GetAccountConfigurationsDataBySupplier()
                 .FirstOrDefault(accountConfig => accountConfig.Account.Id == accountId)?.Supplier ??
             throw new NotFoundException($"Не удалось получить поставщика аккаунта '{accountId}'");
    }
}
