﻿using Clouds42.AccountDatabase.Processors.SupportProcessors;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.Accounts.Account.Managers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Audit.Helpers;
using Clouds42.Accounts.Audit.Providers;
using Clouds42.Accounts.BillingServiceTypeAvailability.Helpers;
using Clouds42.Accounts.BillingServiceTypeAvailability.Managers;
using Clouds42.Accounts.BillingServiceTypeAvailability.Providers;
using Clouds42.Accounts.CheckAccountData.Providers;
using Clouds42.Accounts.Configuration.Providers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.Audit.Interfaces;
using Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.CorpCloud.Interfaces;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces;
using Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces;
using Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces;
using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces;
using Clouds42.Accounts.Contracts.Stripe.Interfaces;
using Clouds42.Accounts.CorpCloud.Providers;
using Clouds42.Accounts.CreateAccount.Helpers;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Accounts.CreateAccount.Providers;
using Clouds42.Accounts.CreateAccount.Validator;
using Clouds42.Accounts.LocaleChangesHistory.Providers;
using Clouds42.Accounts.ManageAccountServiceTypes.Providers;
using Clouds42.Accounts.MyDiskPropertiesByAccount.Providers;
using Clouds42.Accounts.Requisites.Providers;
using Clouds42.Accounts.SegmentMigration.Helpers;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.Accounts.SegmentMigration.Providers;
using Clouds42.Accounts.ServiceAccounts.Helpers;
using Clouds42.Accounts.ServiceAccounts.Managers;
using Clouds42.Accounts.ServiceAccounts.Providers;
using Clouds42.Accounts.Stripe.Helpers;
using Clouds42.Accounts.Stripe.Managers;
using Clouds42.Accounts.Stripe.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Accounts
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddAccounts(this IServiceCollection services)
        {
            services.AddTransient<ICreateAccountConfigurationProvider, CreateAccountConfigurationProvider>();
            services.AddTransient<IAccountConfigurationDataProvider, AccountConfigurationDataProvider>();
            services.AddTransient<IUpdateAccountConfigurationProvider, UpdateAccountConfigurationProvider>();
            services.AddTransient<ICreateAccountRequisitesProvider, CreateAccountRequisitesProvider>();
            services.AddTransient<IUpdateAccountRequisitesProvider, UpdateAccountRequisitesProvider>();
            services.AddTransient<IAccountRequisitesDataProvider, AccountRequisitesDataProvider>();
            services.AddTransient<IChangeAccountSegmentProvider, ChangeAccountSegmentProvider>();
            services.AddTransient<IChangeAccountBackupServerProvider, ChangeAccountBackupServerProvider>();
            services.AddTransient<IChangeAccountFileStorageProvider, ChangeAccountFileStorageProvider>();
            services.AddTransient<IChangeAccountClientFileStorageProvider, ChangeAccountClientFileStorageProvider>();
            services.AddTransient<IMigrateAccountFileDatabasesProvider, MigrateAccountFileDatabasesProvider>();
            services.AddTransient<IRepublishAccountDatabasesProvider, RepublishAccountDatabasesProvider>();
            services.AddTransient<IAuditAccountFoldersProvider, AuditAccountFoldersProvider>();
            services.AddTransient<IBillingServiceTypeAvailabilityProvider, BillingServiceTypeAvailabilityProvider>();
            services.AddTransient<ICalculateDelayedDateForDeletingServiceTypesProvider, CalculateDelayedDateForDeletingServiceTypesProvider>();
            services.AddTransient<ICheckAccountDataProvider, CheckAccountDataProvider>();
            services.AddTransient<INotifyBeforeDeleteServiceTypesProvider, NotifyBeforeDeleteServiceTypesProvider>();
            services.AddTransient<IProcessDeletedServiceTypesForAccountsProvider, ProcessDeletedServiceTypesForAccountsProvider>();
            services.AddTransient<ICreateMyDiskPropertiesByAccountProvider, CreateMyDiskPropertiesByAccountProvider>();
            services.AddTransient<IMyDiskPropertiesByAccountDataProvider, MyDiskPropertiesByAccountDataProvider>();
            services.AddTransient<IUpdateMyDiskPropertiesByAccountProvider, UpdateMyDiskPropertiesByAccountProvider>();
            services.AddTransient<IAcDbDelimitersManager, AcDbDelimitersManager>();
            services.AddTransient<IAccountDatabaseUserAccessManager, AccountDatabaseUserAccessManager>();
            services.AddTransient<IAccountReportDataProvider, AccountReportDataProvider>();
            services.AddTransient<IAccountSaleManagerProvider, AccountSaleManagerProvider>();
            services.AddTransient<IAccountFilesToTombProvider, AccountFilesToTombProvider>();
            services.AddTransient<IAccountFilesAnalyzeProvider, AccountFilesAnalyzeProvider>();
            services.AddTransient<IAccountSegmentMigrationProvider, AccountSegmentMigrationProvider>();
            services.AddTransient<IAccountProvider, AccountProvider>();
            services.AddTransient<IAccountSettingsDataProvider, AccountSettingsDataProvider>();
            services.AddTransient<IAccountLocaleChangesHistoryProvider, AccountLocaleChangesHistoryProvider>();
            services.AddTransient<IRegisterChangeOfLocaleForAccountProvider, RegisterChangeOfLocaleForAccountProvider>();
            services.AddTransient<IUpdateAccountLocaleChangesHistoryProvider, UpdateAccountLocaleChangesHistoryProvider>();
            services.AddTransient<ICreateAccountLocaleChangesHistoryProvider, CreateAccountLocaleChangesHistoryProvider>();
            services.AddTransient<ICreateAccountProvider, CreateAccountProvider>();
            services.AddTransient<IActivateServiceForRegistrationAccountProvider, ActivateServiceForRegistrationAccountProvider>();
            services.AddTransient<IRegisterAccountDatabaseProvider, RegisterAccountDatabaseProvider>();
            services.AddTransient<IRent1CForCustomServiceActivator, Rent1CForCustomServiceActivator>();
            services.AddTransient<IValidateLoginInActiveDirectoryProvider, ValidateLoginInActiveDirectoryProvider>();
            services.AddTransient<IServiceAccountProvider, ServiceAccountProvider>();
            services.AddTransient<IStripeDataProvider, StripeDataProvider>();
            services.AddTransient<IBillingServiceTypeAvailabilityManager, BillingServiceTypeAvailabilityManager>();
            services.AddTransient<ISegmentHelper, SegmentHelper>();
            services.AddTransient<IAcDbAccessHelper, AcDbAccessHelper>();
            services.AddTransient<IAccountDataProvider, AccountDataProvider>();
            services.AddTransient<IServiceAccountDataHelper, ServiceAccountDataHelper>();
            services.AddTransient<IReCaptchaValidator, ReCaptchaValidator>();
            services.AddTransient<AccountDataProvider>();
            services.AddTransient<BillingServiceTypeAvailabilityDataHelper>();
            services.AddTransient<IAccountsManager, AccountsManager>();
            services.AddTransient<LogDbEventHelper>();
            services.AddTransient<TombTaskCreatorManager>();
            services.AddTransient<AutoUpdateSupportProcessor>();
            services.AddTransient<AccountSaleManager>();
            services.AddTransient<CreateAccountManager>();
            services.AddTransient<IAccountFolderHelper, AccountFolderHelper>();
            services.AddTransient<AccountDatabaseValidationHelper>();
            services.AddTransient<SynchronizeProcessFlowsHelper>();
            services.AddTransient<AccountDataHelper>();
            services.AddTransient<SegmentMigrationResultReportHelper>();
            services.AddTransient<ChangeAccountSegmentManager>();
            services.AddTransient<RegisterDatabaseAfterServiceActivationHelper>();
            services.AddTransient<AccountRegistrationModelAdapter>();
            services.AddTransient<AccountSegmentHelper>();
            services.AddTransient<ServiceAccountValidator>();
            services.AddTransient<AccountLogEventHelper>();
            services.AddTransient<LogAccountEventHelper>();
            services.AddTransient<ReactivateServiceManager>();
            services.AddTransient<AccountDetailsProvider>();
            services.AddTransient<ServiceAccountManager>();
            services.AddTransient<StripeManager>();
            services.AddTransient<EditAccountManager>();
            services.AddTransient<StripeOptionsMapper>();
            services.AddTransient<EditAccountProvider>();
            services.AddTransient<AccountDatabaseUserAccessManager>();
            services.AddTransient<AccountDataManager>();
            services.AddTransient<IAccountCorpCloudProvider, AccountCorpCloudProvider>();

            return services;
        }
    }
}
