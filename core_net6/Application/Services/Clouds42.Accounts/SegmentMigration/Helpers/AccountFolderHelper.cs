﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;

namespace Clouds42.Accounts.SegmentMigration.Helpers
{
    /// <summary>
    /// Хэлпер для работы с папками аккаунта
    /// </summary>
    public class AccountFolderHelper(ILogger42 logger) : IAccountFolderHelper
    {
        /// <summary>
        /// Скопировать директорию
        /// </summary>
        /// <param name="sourcePath">Текущий путь</param>
        /// <param name="targetPath">Путь, куда нужно скопировать</param>
        public void CopyDirectory(string sourcePath, string targetPath)
        {
            logger.Trace($"Копирование папки {sourcePath} в {targetPath}");
            var sourceDirectoryInfo = new DirectoryInfo(sourcePath);

            if (!Directory.Exists(targetPath))
                Directory.CreateDirectory(targetPath);

            sourceDirectoryInfo.GetFiles().ToList().ForEach(fileInfo =>
            {
                var newPath = Path.Combine(targetPath, fileInfo.Name);
                if (File.Exists(newPath))
                    return;

                logger.Debug($"Новый путь для файла {fileInfo.Name} : {newPath}");
                fileInfo.CopyTo(newPath, false);
            });

            sourceDirectoryInfo.GetDirectories().ToList().ForEach(directoryInfo =>
            {
                CopyDirectory(directoryInfo.FullName, Path.Combine(targetPath, directoryInfo.Name));
            });
        }

        /// <summary>
        /// Удалить директорию
        /// </summary>
        /// <param name="folderPath">Путь</param>
        public void DeleteDirectory(string folderPath)
        {
            if (!Directory.Exists(folderPath))
                return;
            logger.Debug($"Удалить файлы по пути: {folderPath}");
            var directoryInfo = new DirectoryInfo(folderPath);

            SetAttributesNormal(directoryInfo);

            Directory.Delete(folderPath, true);

            logger.Debug($"Все файлы удалены по пути: {folderPath}");
        }


        /// <summary>
        /// Убераем флаг "Только для чтения" у папок и файлов
        /// </summary>
        /// <param name="directoryInfo">Путь до директории</param>
        public void SetAttributesNormal(DirectoryInfo directoryInfo)
        {
            directoryInfo.Attributes = FileAttributes.Normal;

            logger.Debug($"Установить атрибут {FileAttributes.Normal} для всех файлов и папок: {directoryInfo.FullName}");

            foreach (var info in directoryInfo.GetFileSystemInfos("*", SearchOption.AllDirectories))
            {
                info.Attributes = FileAttributes.Normal;
            }
        }

        /// <summary>
        /// Получить путь к файлам бэкапов аккаунта
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Путь к файлам бэкапов аккаунта</returns>
        public string GetAccountFilesBackupPath(CloudServicesSegment segment, Domain.DataModels.Account account)
            => Path.Combine(segment.CloudServicesBackupStorage.ConnectionAddress, $"account_{account.IndexNumber}");

        /// <summary>
        /// Получить путь к файлам аккаунта
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Путь к файлам аккаунта</returns>
        public string GetAccountFileStoragePath(CloudServicesSegment segment, Domain.DataModels.Account account)
            => Path.Combine(segment.CloudServicesFileStorageServer.ConnectionAddress,
                $@"company_{account.IndexNumber}\{segment.ClientFileFolder}");

        /// <summary>
        /// Получить путь к клиентским файлам аккаунта
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Путь к файлам аккаунта</returns>
        public string GetAccountClientFileStoragePath(CloudServicesSegment segment, Domain.DataModels.Account account)
            => Path.Combine(segment.CloudServicesFileStorageServer.ConnectionAddress, $"company_{account.IndexNumber}", segment.ClientFileFolder);
    }
}
