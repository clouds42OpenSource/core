﻿using Clouds42.AccountDatabase.Contracts.Check.Interfaces;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.SegmentMigration.Helpers
{
    /// <summary>
    /// Хэлпер для валидации инф. базы
    /// </summary>
    public class AccountDatabaseValidationHelper(
        IAccountDatabasePathHelper accountDatabasePathHelper,
        IAccountDatabaseFileUseCheck accountDatabaseFileUseChecker,
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
    {
        /// <summary>
        /// Признак необходимости мигрировать инф. базу
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetSegment">Целевой сегмент</param>
        /// <returns>true - если целевом сегменте нет текущего хранилища базы</returns>
        public bool NeedMigrateAccountDatabase(Domain.DataModels.AccountDatabase accountDatabase, CloudServicesSegment targetSegment)
        {
            if (!accountDatabase.FileStorageID.HasValue)
                throw new InvalidOperationException(
                    $"Для инф. базы {accountDatabase.V82Name} не указано файловое хранилище");

            var currentAcDbFileStorage =
                dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(fileStorage =>
                    fileStorage.ID == accountDatabase.FileStorageID.Value)
                ?? throw new NotFoundException($"Файловое хранилище по ID {accountDatabase.FileStorageID} не найдено");

            return !targetSegment.CloudServicesSegmentStorages.Select(segmentStorage => segmentStorage.FileStorageID)
                .Contains(currentAcDbFileStorage.ID);
        }

        /// <summary>
        /// Проверить инф. базу перед миграцией
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetSegment">Целевой сегмент</param>
        /// <returns>Результат проверки</returns>
        public AccountDatabaseValidationResultDto ValidateAccountDatabaseBeforeMigration(Domain.DataModels.AccountDatabase accountDatabase,
            CloudServicesSegment targetSegment)
        {
            var accountDatabaseUsedWebServices = CheckAccountDatabaseUsedWebServices(accountDatabase);
            if (!accountDatabaseUsedWebServices.Result)
                return accountDatabaseUsedWebServices;

            var accountDatabaseExist = CheckAccountDatabasePath(accountDatabase);
            return !accountDatabaseExist.Result
                ? accountDatabaseExist
                : CheckTargetSegmentPlatformVersion(accountDatabase, targetSegment);
        }

        /// <summary>
        /// Проверить возможность смигрировать инф. базу
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Результат проверки</returns>
        public AccountDatabaseValidationResultDto CheckAbilityToMigrateDatabase(IAccountDatabase accountDatabase)
        {
            if (!accountDatabase.FileStorageID.HasValue)
                return CreateValidationResult(false,
                    $"Для инф. базы \"{accountDatabase.V82Name}\" не указано файловое хранилище!");

            var accountDatabaseUsing = CheckAccountDatabaseUsage(accountDatabase);
            return !accountDatabaseUsing.Result
                ? accountDatabaseUsing
                : CreateValidationResult(true);
        }

        /// <summary>
        /// Проверить что база использует веб сервисы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Результат проверки</returns>
        private AccountDatabaseValidationResultDto CheckAccountDatabaseUsedWebServices(IAccountDatabase accountDatabase)
        {
            if (accountDatabase.UsedWebServices != true)
                return CreateValidationResult(true);

            var message =
                $@"В базе {accountDatabase.V82Name} опубликованы веб сервисы. 
                    Вам необходимо вручную отменить публикацию базы, предварительно сохранив web.config и default.vrd. 
                    После этого выполнить миграцию аккаунта и снова опубликовать базу. 
                    В новые файлы публикаций  web.config и default.vrd вставить содержимое 
                    сохранненых ранее web.config и default.vrd(ту часть, которая отвечает за веб сервис).";

            logger.Trace(message);
            return CreateValidationResult(false, message);
        }

        /// <summary>
        /// Проверить существование базы по пути
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Результат проверки</returns>
        private AccountDatabaseValidationResultDto CheckAccountDatabasePath(IAccountDatabase accountDatabase)
        {
            var accountDatabasePath = accountDatabasePathHelper.GetPath(accountDatabase);

            if (DirectoryHelper.DirectoryHas1CDatabaseFile(accountDatabasePath))
                return CreateValidationResult(true);

            var message = $"Не удалось найти базу {accountDatabase.V82Name} по пути {accountDatabasePath}.";
            logger.Warn(message);
            return CreateValidationResult(false, message);
        }

        /// <summary>
        /// Проверить использование инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Результат проверки</returns>
        private AccountDatabaseValidationResultDto CheckAccountDatabaseUsage(IAccountDatabase accountDatabase)
        {
            var accountDatabasePath = accountDatabasePathHelper.GetPath(accountDatabase);

            if (!accountDatabaseFileUseChecker.Check(accountDatabasePath))
                return CreateValidationResult(true);

            var message = $"Инф. база {accountDatabase.V82Name} используется.";
            logger.Warn(message);
            return CreateValidationResult(false, message);
        }

        /// <summary>
        /// Проверить версию платформы в целевом сегменте
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetSegment">Целевой сегмент</param>
        /// <returns>Результат проверки</returns>
        private AccountDatabaseValidationResultDto CheckTargetSegmentPlatformVersion(Domain.DataModels.AccountDatabase accountDatabase,
            CloudServicesSegment targetSegment)
        {
            var currentAcDbPlatformVersion = GetCurrentAccountDatabasePlatformVersion(accountDatabase);

            return accountDatabase.PlatformType == PlatformType.V82
                ? CheckV82PlatformVersionInTargetSegment(currentAcDbPlatformVersion, targetSegment)
                : CheckV83PlatformVersionInTargetSegment(accountDatabase, currentAcDbPlatformVersion, targetSegment);
        }

        /// <summary>
        /// Проверить версию платформы 8.2 в целевом сегменте
        /// </summary>
        /// <param name="currentAcDbPlatformVersion">Текущая версия платформы базы</param>
        /// <param name="targetSegment">Целевой сегмент</param>
        /// <returns>Результат проверки</returns>
        private AccountDatabaseValidationResultDto CheckV82PlatformVersionInTargetSegment(
            string currentAcDbPlatformVersion, CloudServicesSegment targetSegment)
        {
            var result = new VersionComparer().Compare(targetSegment.Stable82PlatformVersionReference.Version,
                currentAcDbPlatformVersion);

            if (result >= 0)
                return CreateValidationResult(true);

            var errorMessage =
                $"Версия платформы 8.2 в целевом сегменте {targetSegment.Name} меньше чем текущая версия";
            logger.Trace(errorMessage);
            return CreateValidationResult(false, errorMessage);
        }

        /// <summary>
        /// Проверить версию платформы 8.3 в целевом сегменте
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="currentAcDbPlatformVersion">Текущая версия платформы базы</param>
        /// <param name="targetSegment">Целевой сегмент</param>
        /// <returns>Результат проверки</returns>
        private AccountDatabaseValidationResultDto CheckV83PlatformVersionInTargetSegment(Domain.DataModels.AccountDatabase accountDatabase,
            string currentAcDbPlatformVersion, CloudServicesSegment targetSegment)
        {
            var errorMessage =
                $"Версия платформы 8.3 в целевом сегменте {targetSegment.Name} меньше чем текущая версия";

            if (accountDatabase.DistributionTypeEnum == DistributionType.Alpha)
            {
                if (targetSegment.Alpha83PlatformVersionReference == null)
                    return CreateValidationResult(false,
                        $"В целевом сегменте {targetSegment.Name} не указана альфа версия платформы");

                var checkAlphaVersionResult = new VersionComparer().Compare(
                    targetSegment.Alpha83PlatformVersionReference.Version,
                    currentAcDbPlatformVersion);

                logger.Trace(errorMessage);
                return checkAlphaVersionResult >= 0
                    ? CreateValidationResult(true)
                    : CreateValidationResult(false, errorMessage);
            }

            var checkStableVersionResult = new VersionComparer().Compare(
                targetSegment.Stable83PlatformVersionReference.Version,
                currentAcDbPlatformVersion);

            if (checkStableVersionResult >= 0)
                return CreateValidationResult(true);

            logger.Trace(errorMessage);
            return CreateValidationResult(false, errorMessage);
        }

        /// <summary>
        /// Получить текущую версию платформы для инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Текущая версия платформы для инф. базы</returns>
        private string GetCurrentAccountDatabasePlatformVersion(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var accountDatabaseSegment = accountConfigurationDataProvider.GetAccountSegment(accountDatabase.AccountId);

            if (accountDatabase.PlatformType == PlatformType.V82)
                return accountDatabaseSegment.Stable82PlatformVersionReference.Version;

            return accountDatabase.DistributionTypeEnum == DistributionType.Alpha
                ? accountDatabaseSegment.Alpha83PlatformVersionReference.Version
                : accountDatabaseSegment.Stable83PlatformVersionReference.Version;
        }

        /// <summary>
        /// Создать результат проверки
        /// </summary>
        /// <param name="success">Признак что валидация успешна</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат проверки</returns>
        private static AccountDatabaseValidationResultDto CreateValidationResult(bool success, string errorMessage = null)
            => new()
            {
                Result = success,
                ValidationMessage = errorMessage
            };
    }
}
