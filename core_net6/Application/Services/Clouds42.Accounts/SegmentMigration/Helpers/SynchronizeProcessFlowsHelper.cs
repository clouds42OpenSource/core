﻿using Clouds42.Common.Extensions;

namespace Clouds42.Accounts.SegmentMigration.Helpers
{
    /// <summary>
    /// Хэлпер для синхронизации рабочих процессов
    /// </summary>
    public class SynchronizeProcessFlowsHelper
    {
        /// <summary>
        /// Получить ID инф. базы из имени обрабатываемого объекта
        /// </summary>
        /// <param name="processedObjectName">Имя обрабатываемого объекта</param>
        /// <returns>ID инф. базы</returns>
        public Guid GetDatabaseIdFromProcessedObjectName(string processedObjectName)
        {
            var parameters = processedObjectName.Split(':');
            return parameters[0].ToGuid();
        }

        /// <summary>
        /// Создать ключ основного рабочего процесса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Ключ основного рабочего процесса</returns>
        public string CreateParentProcessFlowId(Guid accountId)
            => $"{Guid.NewGuid()}::{accountId}";

        /// <summary>
        /// Получить имя обрабатываемого объекта для рабочего процесса миграции инф. базы.
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="parentProcessFlowKey">Ключ основного рабочего процесса</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        public string GetProcessedObjectNameForMigrateAcDbProcessFlow(Domain.DataModels.AccountDatabase accountDatabase, string parentProcessFlowKey = null)
        {
            if (accountDatabase == null)
                throw new InvalidOperationException("Модель инф. базы пуста");

            var additionalParam = !string.IsNullOrEmpty(parentProcessFlowKey)
                ? $"::{parentProcessFlowKey}"
                : null;

            return $"{accountDatabase.Id}::{accountDatabase.V82Name}{additionalParam}";
        }
    }
}
