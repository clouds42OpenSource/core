﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;

namespace Clouds42.Accounts.SegmentMigration.Providers
{
    /// <summary>
    /// Провайдер для смены файлового хранилища аккаунта
    /// </summary>
    internal class ChangeAccountFileStorageProvider(
        AccountDataProvider accountDataProvider,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        IAccountFolderHelper accountFolderHelper,
        IMemorySizeCalculator memorySizeCalculator,
        ILogger42 logger)
        : IChangeAccountFileStorageProvider
    {
        /// <summary>
        /// Сменить файловое хранилище для аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        public void ChangeAccountFileStorage(ChangeAccountSegmentParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            var currentAccountSegment = accountDataProvider.GetCloudServicesSegmentById(model.SegmentFromId);
            var targetAccountSegment = accountDataProvider.GetCloudServicesSegmentById(model.SegmentToId);

            if (!NeedChangeFileStorage(currentAccountSegment, targetAccountSegment, account))
            {
                logger.Trace($"Нет необходимости менять файловое хранилище для аккаунта {account.IndexNumber}");
                return;
            }

            var oldClientFileFolder = accountFolderHelper.GetAccountFileStoragePath(currentAccountSegment, account);
            var newClientFileFolder = accountFolderHelper.GetAccountFileStoragePath(targetAccountSegment, account);

            var groupName = DomainCoreGroups.CompanyGroupBuild(account.IndexNumber);
            var newCompanyPath = Path.Combine(targetAccountSegment.CloudServicesFileStorageServer.ConnectionAddress, groupName);

            if (!Directory.Exists(newCompanyPath))
                Directory.CreateDirectory(newCompanyPath);

            activeDirectoryTaskProcessor.TryDoImmediately(provider => provider.SetDirectoryAcl(groupName, newCompanyPath));

            accountFolderHelper.CopyDirectory(oldClientFileFolder, newClientFileFolder);
            accountFolderHelper.DeleteDirectory(oldClientFileFolder);
        }

        /// <summary>
        /// Признак необходимости менять файловое хранилище
        /// </summary>
        /// <param name="currentAccountSegment">Текущий сегмент аккаунта</param>
        /// <param name="targetAccountSegment">Целевой сегмент для миграции</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Результат проверки</returns>
        private bool NeedChangeFileStorage(CloudServicesSegment currentAccountSegment, CloudServicesSegment targetAccountSegment,
            Domain.DataModels.Account account)
        {
            var currentClientFileFolder = accountFolderHelper.GetAccountFileStoragePath(currentAccountSegment, account);
            if (currentAccountSegment.FileStorageServersID == targetAccountSegment.FileStorageServersID)
                return false;

            return memorySizeCalculator.CalculateCatalog(currentClientFileFolder) != 0;
        }
    }
}
