﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Segment.Contracts;
using Clouds42.StateMachine.Contracts.AccountProcessFlows;
using Clouds42.StateMachine.Contracts.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Accounts.SegmentMigration.Providers
{
    /// <summary>
    /// Провайдер для смены сегмента у аккаунта
    /// </summary>
    internal class ChangeAccountSegmentProvider(
        IServiceProvider serviceProvider,
        ISegmentHistoryProvider segmentHistoryProvider,
        AccountDataProvider accountDataProvider,
        IHandlerException handlerException)
        : IChangeAccountSegmentProvider
    {
        /// <summary>
        /// Сменить сегмент для аккаунтов
        /// </summary>
        /// <param name="changeAccountSegmentParams">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат смены сегмента</returns>
        public MigrationResultDto ChangeAccountSegment(ChangeAccountSegmentParamsDto changeAccountSegmentParams)
        {
            try
            {
                var result = serviceProvider.GetRequiredService<IChangeAccountSegmentProcessFlow>().Run(changeAccountSegmentParams);
                return FinalizeMigrationProcess(result, changeAccountSegmentParams);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $@"[Ошибка смены сегмента аккаунта] Во время миграции аккаунта {changeAccountSegmentParams.AccountId} 
                                из сегмента {changeAccountSegmentParams.SegmentFromId} 
                                в сегмент {changeAccountSegmentParams.SegmentToId} произошла ошибка");

                RegisterFailMigrationHistory(changeAccountSegmentParams);
                return CreateMigrationResult(changeAccountSegmentParams, ex.Message, false);
            }
        }

        /// <summary>
        /// Зарегистрировать неуспешный результат миграции
        /// </summary>
        /// <param name="changeAccountSegmentParams">Параметры смены сегмента для аккаунта</param>
        private void RegisterFailMigrationHistory(ChangeAccountSegmentParamsDto changeAccountSegmentParams)
        {
            segmentHistoryProvider.RegisterMigrationHistory(new MigrationAccountHistory
            {
                AccountId = changeAccountSegmentParams.AccountId,
                SourceSegmentId = changeAccountSegmentParams.SegmentFromId,
                TargetSegmentId = changeAccountSegmentParams.SegmentToId,
                MigrationHistory = segmentHistoryProvider.CreateMigrationHistoryObject(changeAccountSegmentParams.InitiatorId, changeAccountSegmentParams.MigrationStartDateTime, false)
            });
        }

        /// <summary>
        /// Завершить процесс миграции
        /// </summary>
        /// <param name="stateMachineResult">Результат выполнения рабочего процесса</param>
        /// <param name="changeAccountSegmentParams">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат смены сегмента</returns>
        private MigrationResultDto FinalizeMigrationProcess(StateMachineResult<MigrationResultDto> stateMachineResult, ChangeAccountSegmentParamsDto changeAccountSegmentParams)
        {
            if (stateMachineResult.Finish)
                return stateMachineResult.ResultModel;

            var account = accountDataProvider.GetAccountOrThrowException(changeAccountSegmentParams.AccountId);
            var linkForOpenProcessFlow = GetLinkForOpenProcessFlow(stateMachineResult.ProcessFlowId);

            var errorMessage =
                $@"Миграция аккаунта {account.IndexNumber} не выполнена. 
                <br>Чтобы завершить процесс переноса перейдите по ссылке {linkForOpenProcessFlow} . 
                <br>Причина: {stateMachineResult.Message}";

            return CreateMigrationResult(changeAccountSegmentParams, errorMessage, false);
        }

        /// <summary>
        /// Создать результат миграции
        /// </summary>
        /// <param name="changeAccountSegmentParams">Параметры смены сегмента для аккаунта</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <param name="migrationSuccess">Признак, что миграция завершена успешно</param>
        /// <returns></returns>
        private static MigrationResultDto CreateMigrationResult(ChangeAccountSegmentParamsDto changeAccountSegmentParams, string errorMessage, bool migrationSuccess)
            => new()
            {
                AccountId = changeAccountSegmentParams.AccountId,
                Successful = migrationSuccess,
                ErrorMessage = errorMessage
            };

        /// <summary>
        /// Получить ссылку для просмотра деталей рабочего процесса
        /// </summary>
        /// <param name="processFlowId"></param>
        /// <returns>Ссылка для просмотра деталей рабочего процесса</returns>
        private static string GetLinkForOpenProcessFlow(Guid processFlowId)
        {
            var siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
            var routeValue = CloudConfigurationProvider.Cp.GetRouteValueForOpenProcessFlowDetails();

            return $"{siteUrl}/{routeValue}{processFlowId}";
        }
    }
}
