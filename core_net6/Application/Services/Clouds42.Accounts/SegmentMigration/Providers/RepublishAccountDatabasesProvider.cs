﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.SegmentMigration.Providers
{
    /// <summary>
    /// Провайдер для перепубликации баз аккаунта
    /// </summary>
    internal class RepublishAccountDatabasesProvider(
        AccountDataProvider accountDataProvider,
        IUnitOfWork dbLayer,
        AccountDatabasePublishManager accountDatabasePublishManager,
        IExecuteDatabasePublishTasksProvider executeDatabasePublishTasksProvider,
        ILogger42 logger)
        : IRepublishAccountDatabasesProvider
    {
        /// <summary>
        /// Переопубликовать инф. базы аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        public void RepublishAccountDatabases(ChangeAccountSegmentParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            var accountFullName = accountDataProvider.GetAccountFullName(account);

            var oldAccountSegment = accountDataProvider.GetCloudServicesSegmentById(model.SegmentFromId);
            var newAccountSegment = accountDataProvider.GetCloudServicesSegmentById(model.SegmentToId);

            var accountPublishedDatabasesIdList = GetPublishedAccountDatabases(account.Id);

            if (oldAccountSegment.ContentServerID.Equals(newAccountSegment.ContentServerID))
            {
                logger.Trace($"Запуск процесса переопубликации инф. баз аккаунта {accountFullName} при одинаковых серверах публикации.");
                RepublishDatabasesOnEqualContentServers(accountPublishedDatabasesIdList);
                logger.Trace($"Процесс переопубликации инф. баз аккаунта {accountFullName} при одинаковых серверах публикации завершен успешно.");
                return;
            }

            logger.Trace($"Запуск процесса переопубликации инф. баз аккаунта {accountFullName} при разных серверах публикации.");
            RepublishDatabasesOnDifferentContentServers(accountPublishedDatabasesIdList, oldAccountSegment.ID);
            logger.Trace($"Процесс переопубликации инф. баз аккаунта {accountFullName} при разных серверах публикации завершен успешно.");
        }

        /// <summary>
        /// Переопубликовать инф. базы аккаунта при одинаковых серверах публикации
        /// </summary>
        /// <param name="accountPublishedDatabasesIdList">Список ID опубликованных баз аккаунта</param>
        private void RepublishDatabasesOnEqualContentServers(List<Guid> accountPublishedDatabasesIdList)
        {
            accountPublishedDatabasesIdList.ForEach(accountDatabaseId =>
            {
                accountDatabasePublishManager.RepublishDatabaseWithWaiting(accountDatabaseId);
            });
        }

        /// <summary>
        /// Переопубликовать инф. базы аккаунта при разных серверах публикации
        /// </summary>
        /// <param name="accountPublishedDatabasesIdList">Список ID опубликованных баз аккаунта</param>
        /// <param name="oldSegmentId">ID старого сегмента</param>
        private void RepublishDatabasesOnDifferentContentServers(List<Guid> accountPublishedDatabasesIdList, Guid oldSegmentId)
        {
            accountPublishedDatabasesIdList.ForEach(accountDatabaseId =>
            {
                executeDatabasePublishTasksProvider.CreateAndStartRemoveOldPublicationJob(accountDatabaseId, oldSegmentId).Wait();
                accountDatabasePublishManager.PublishDatabaseWithWaiting(accountDatabaseId);
            });
        }

        /// <summary>
        /// Получить список ID опубликованных баз аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список ID опубликованных баз аккаунта</returns>
        private List<Guid> GetPublishedAccountDatabases(Guid accountId)
            => dbLayer.DatabasesRepository.WhereLazy(acDb =>
                acDb.AccountId == accountId &&
                acDb.AccountDatabaseOnDelimiter == null &&
                (
                    acDb.State == DatabaseState.Ready.ToString() ||
                    acDb.State == DatabaseState.ProcessingSupport.ToString() ||
                    acDb.State == DatabaseState.TransferDb.ToString()
                ) &&
                (
                    acDb.PublishState == PublishState.Published.ToString() ||
                    acDb.PublishState == PublishState.RestartingPool.ToString()
                )
            ).Select(acDb => acDb.Id).ToList();
    }
}
