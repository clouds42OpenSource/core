﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Accounts.SegmentMigration.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.StateMachine;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Internal;
using Clouds42.StateMachine.Contracts.MigrateAccountDatabaseProcessFlow;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Accounts.SegmentMigration.Providers
{
    /// <summary>
    /// Провайдер для миграции файловых баз аккаунта
    /// </summary>
    internal class MigrateAccountFileDatabasesProvider(
        IServiceProvider serviceProvider,
        AccountDataProvider accountDataProvider,
        IUnitOfWork dbLayer,
        AccountDatabaseValidationHelper accountDatabaseValidationHelper,
        SynchronizeProcessFlowsHelper synchronizeProcessFlowsHelper,
        IProcessFlowRetryProcessor processFlowRetryProcessor,
        IHandlerException handlerException)
        : IMigrateAccountFileDatabasesProvider
    {
        /// <summary>
        /// Перенести файловые базы аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат миграции инф. баз</returns>
        public List<AccountDatabaseMigrationResultModelDto> MigrateAccountDatabases(ChangeAccountSegmentParamsDto model)
        {
            var migrationResultList = new List<AccountDatabaseMigrationResultModelDto>();

            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            var accountFileDatabases = GetAccountFileDatabases(account.Id);
            var targetAccountSegment = accountDataProvider.GetCloudServicesSegmentById(model.SegmentToId);

            var failedProcessFlows = GetFailedProcessFlows(accountFileDatabases, model);
            if (failedProcessFlows.Any())
                return RetryFailedProcessFlows(failedProcessFlows);

            ValidateAccountDatabases(accountFileDatabases, targetAccountSegment);

            accountFileDatabases.ForEach(accountDatabase =>
            {
                var result = MigrateAccountDatabase(accountDatabase, targetAccountSegment, model);
                migrationResultList.Add(result);
            });

            return migrationResultList;
        }

        /// <summary>
        /// Проверить инф. базы перед миграцией
        /// </summary>
        /// <param name="accountDatabases">Инф. базы аккаунта</param>
        /// <param name="targetAccountSegment">Целевой сегмент</param>
        private void ValidateAccountDatabases(List<Domain.DataModels.AccountDatabase> accountDatabases, CloudServicesSegment targetAccountSegment)
        {
            var validationResult = new List<AccountDatabaseValidationResultDto>();
            var errorMessage = string.Empty;

            accountDatabases.ForEach(accountDatabase =>
            {
                validationResult.Add(accountDatabaseValidationHelper.ValidateAccountDatabaseBeforeMigration(accountDatabase,
                    targetAccountSegment));
            });

            validationResult.Where(res => !res.Result).ToList().ForEach(result => { errorMessage += $"<li>{result.ValidationMessage}</li>"; });

            if (!string.IsNullOrEmpty(errorMessage))
                throw new ValidateException($"Ошибка при переносе файловых баз: <ul>{errorMessage}</ul>");
        }

        /// <summary>
        /// Перенести инф. базу аккаунта
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetAccountSegment">Целевой сегмент</param>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат миграции</returns>
        private AccountDatabaseMigrationResultModelDto MigrateAccountDatabase(Domain.DataModels.AccountDatabase accountDatabase, CloudServicesSegment targetAccountSegment, ChangeAccountSegmentParamsDto model)
        {
            try
            {
                if (!accountDatabaseValidationHelper.NeedMigrateAccountDatabase(accountDatabase, targetAccountSegment))
                    return CreateMigrationResult("Нет необходимости мигрировать инф. базу так как текущее хранилище есть в новом сегменте",
                        true, accountDatabase.Id);

                var canMigrateAccountDatabase =
                    accountDatabaseValidationHelper.CheckAbilityToMigrateDatabase(accountDatabase);
                if (!canMigrateAccountDatabase.Result)
                    return CreateMigrationResult(canMigrateAccountDatabase.ValidationMessage,
                        canMigrateAccountDatabase.Result, accountDatabase.Id);

                var result = serviceProvider.GetRequiredService<IMigrateAccountDatabaseProcessFlow>().Run(
                    new MigrateAccountDatabaseParametersDto
                    {
                        AccountDatabaseId = accountDatabase.Id,
                        SourceFileStorageId = accountDatabase.FileStorageID.GetValueOrDefault(),
                        AccountUserInitiatorId = model.InitiatorId,
                        OperationStartDate = model.MigrationStartDateTime,
                        DestinationFileStorageId = GetDestinationFileStorage(targetAccountSegment).ID,
                        ParentProcessFlowKey = model.ParentProcessFlowKey
                    });

                return CreateMigrationResult(result.Message, result.Finish, result.ProcessFlowId, accountDatabase.Id);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка миграции инф. базы] {accountDatabase.V82Name} произошла ошибка");
                throw;
            }
        }

        /// <summary>
        /// Получить целевое хранилище для инф. баз
        /// </summary>
        /// <param name="targetSegment">Целевой сегмент</param>
        /// <returns>Целевое хранилище для инф. баз</returns>
        private static CloudServicesFileStorageServer GetDestinationFileStorage(CloudServicesSegment targetSegment)
        {
            var targetSegmentFileStorageList = targetSegment.CloudServicesSegmentStorages;

            var destinationFileStorage = targetSegmentFileStorageList.FirstOrDefault(s => s.IsDefault)?.CloudServicesFileStorageServer
                                         ?? targetSegmentFileStorageList.FirstOrDefault()?.CloudServicesFileStorageServer;

            if (destinationFileStorage == null)
                throw new InvalidOperationException($"В сегменте {targetSegment.Name} нет файловых хранилищ.");

            return destinationFileStorage;
        }

        /// <summary>
        /// Получить список файловых баз аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список файловых баз аккаунта</returns>
        private List<Domain.DataModels.AccountDatabase> GetAccountFileDatabases(Guid accountId)
            => dbLayer.DatabasesRepository
                .Where(acDb =>
                    acDb.AccountId == accountId &&
                    (acDb.State == DatabaseState.Ready.ToString() || acDb.State == DatabaseState.TransferDb.ToString()) &&
                    acDb.IsFile == true)
                .ToList();

        /// <summary>
        /// Создать результат миграции инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="message">Сообщение</param>
        /// <param name="successful">Признак, что миграция заверешена успешно</param>
        /// <param name="processFlowId">ID рабочего процесса</param>
        /// <returns>Результат миграции инф. базы</returns>
        private static AccountDatabaseMigrationResultModelDto CreateMigrationResult(string message, bool successful, Guid? processFlowId = null, Guid? accountDatabaseId = null)
            => new()
            {
                AccountDatabaseId = accountDatabaseId,
                Message = message,
                Successful = successful,
                ProcessFlowId = processFlowId
            };

        /// <summary>
        /// Перезапустить упавшие рабочие процессы
        /// </summary>
        /// <param name="failedProcessFlows">Список упавшиъ рабочих процессов</param>
        /// <returns>Реузльтат перезапуска рабочих процессов</returns>
        private List<AccountDatabaseMigrationResultModelDto> RetryFailedProcessFlows(List<ProcessFlow> failedProcessFlows)
        {
            var retryResultList = new List<AccountDatabaseMigrationResultModelDto>();

            failedProcessFlows.ForEach(processFlow =>
            {
                var accountDatabaseId =
                    synchronizeProcessFlowsHelper
                        .GetDatabaseIdFromProcessedObjectName(processFlow.ProcessedObjectName);
                try
                {
                    processFlowRetryProcessor.Retry(processFlow.Id);
                    retryResultList.Add(CreateMigrationResult(null, true, processFlow.Id, accountDatabaseId));
                }
                catch (Exception ex)
                {
                    retryResultList.Add(CreateMigrationResult(ex.Message, false, processFlow.Id, accountDatabaseId));
                }
            });
            return retryResultList;
        }

        /// <summary>
        /// Получить список рабочих процессов с ошибкой
        /// </summary>
        /// <param name="accountDatabases">Список инф. баз</param>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Список рабочих процессов с ошибкой</returns>
        private List<ProcessFlow> GetFailedProcessFlows(List<Domain.DataModels.AccountDatabase> accountDatabases, ChangeAccountSegmentParamsDto model)
        {
            var failedProcessFlows = new List<ProcessFlow>();

            accountDatabases.ForEach(accountDatabase =>
            {
                var processedObjectName =
                    synchronizeProcessFlowsHelper.GetProcessedObjectNameForMigrateAcDbProcessFlow(accountDatabase,
                        model.ParentProcessFlowKey);

                var failedProcessFlow =
                    dbLayer.ProcessFlowRepository.FirstOrDefault(flow =>
                        flow.ProcessedObjectName == processedObjectName &&
                        flow.Status == StateMachineComponentStatus.Error);

                if (failedProcessFlow != null)
                    failedProcessFlows.Add(failedProcessFlow);
            });

            return failedProcessFlows;
        }
    }
}
