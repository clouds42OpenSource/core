﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;

namespace Clouds42.Accounts.SegmentMigration.Providers
{
    /// <summary>
    /// Провайдер для смены хранилища бэкапов аккаунта
    /// </summary>
    internal class ChangeAccountBackupServerProvider(
        IMemorySizeCalculator memorySizeCalculator,
        AccountDataProvider accountDataProvider,
        IAccountFolderHelper accountFolderHelper,
        ILogger42 logger)
        : IChangeAccountBackupServerProvider
    {
        /// <summary>
        /// Сменить хранилище бэкапов для аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Реузльтат смены хранилища</returns>
        public void ChangeAccountBackupServer(ChangeAccountSegmentParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            var currentAccountSegment = accountDataProvider.GetCloudServicesSegmentById(model.SegmentFromId);
            var targetAccountSegment = accountDataProvider.GetCloudServicesSegmentById(model.SegmentToId);

            if (!NeedChangeBackupServer(currentAccountSegment, targetAccountSegment, account))
            {
                logger.Trace($"Нет необходимости менять хранилище бэкапов для аккаунта {account.IndexNumber}");
                return;
            }

            var currentBackupPath = accountFolderHelper.GetAccountFilesBackupPath(currentAccountSegment, account);
            var targetBackupPath = accountFolderHelper.GetAccountFilesBackupPath(currentAccountSegment, account);

            accountFolderHelper.CopyDirectory(currentBackupPath, targetBackupPath);
            accountFolderHelper.DeleteDirectory(currentBackupPath);
        }

        /// <summary>
        /// Признак необходимости менять хранилище бэкапов
        /// </summary>
        /// <param name="currentAccountSegment">Текущий сегмент аккаунта</param>
        /// <param name="targetAccountSegment">Целевой сегмент для миграции</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Результат проверки</returns>
        public bool NeedChangeBackupServer(CloudServicesSegment currentAccountSegment, CloudServicesSegment targetAccountSegment,
            Domain.DataModels.Account account)
        {
            var currentBackupPath = accountFolderHelper.GetAccountFilesBackupPath(currentAccountSegment, account);
            if (currentAccountSegment.BackupStorageID == targetAccountSegment.BackupStorageID)
                return false;

            return memorySizeCalculator.CalculateCatalog(currentBackupPath) != 0;
        }
    }
}
