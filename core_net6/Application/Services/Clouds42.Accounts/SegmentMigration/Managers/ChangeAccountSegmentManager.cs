﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.SegmentMigration.Managers
{
    /// <summary>
    /// Менеджер для смены сегмента у аккаунта
    /// </summary>
    public class ChangeAccountSegmentManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IChangeAccountSegmentProvider changeAccountSegmentProvider,
        AccountDataProvider accountDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Сменить сегмент для аккаунтов
        /// </summary>
        /// <param name="changeAccountSegmentParams">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат смены сегмента</returns>
        public ManagerResult<MigrationResultDto> ChangeAccountSegment(ChangeAccountSegmentParamsDto changeAccountSegmentParams)
        {
            try
            {
                AccessProvider.HasAccessForMultiAccounts(ObjectAction.AccountMigration);

                var result = changeAccountSegmentProvider.ChangeAccountSegment(changeAccountSegmentParams);
                CreateLogEvent(result, changeAccountSegmentParams);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<MigrationResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Создать запись в лог для аккаунта
        /// </summary>
        /// <param name="migrationResult">Результат смены сегмента</param>
        /// <param name="changeAccountSegmentParams">Параметры смены сегмента для аккаунта</param>
        private void CreateLogEvent(MigrationResultDto migrationResult, ChangeAccountSegmentParamsDto changeAccountSegmentParams)
        {
            var currentAccountSegment =
                accountDataProvider.GetCloudServicesSegmentById(changeAccountSegmentParams.SegmentFromId);

            var targetAccountSegment =
                accountDataProvider.GetCloudServicesSegmentById(changeAccountSegmentParams.SegmentToId);

            var changeSegmentDescription =
                $"Из сегмента \"{currentAccountSegment.Name}\" в сегмент \"{targetAccountSegment.Name}\".";

            var logEventMessage = !migrationResult.Successful
                ? $"Перенос аккаунта между сегментами не выполнен. {changeSegmentDescription} {migrationResult.ErrorMessage.ClearHtmlTagsFromString()}"
                : $"Выполнен перенос аккаунта между сегментами. {changeSegmentDescription}";

            LogEvent(() => changeAccountSegmentParams.AccountId, LogActions.MovingAccountBetweenSegments, logEventMessage,
                changeAccountSegmentParams.InitiatorId);
        }
    }
}
