﻿using Clouds42.Accounts.Contracts.CorpCloud.Interfaces;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.CorpCloud.Providers
{
    public class AccountCorpCloudProvider(IUnitOfWork unitOfWork) : IAccountCorpCloudProvider
    {
        /// <summary>
        /// Получить информацию по корп облаку аккаунта
        /// </summary>
        /// <returns></returns>
        public ManagerResult<AccountCorpCloudDto> GetAccountCorpCloudInfo(Guid accountId)
        {
            var corpCloudInfo = unitOfWork.GetGenericRepository<AccountCorpCloud>().Where(ac => ac.AccountId == accountId).FirstOrDefault();

            if (corpCloudInfo == null)
            {
                corpCloudInfo = new AccountCorpCloud
                {
                    Id = Guid.NewGuid(),
                    AccountId = accountId,
                    CountOfClicks = 0,
                    LastUseDate = DateTime.Now
                };

                unitOfWork.GetGenericRepository<AccountCorpCloud>().Insert(corpCloudInfo);
                unitOfWork.Save();
            }

            return ManagerResultHelper.Ok(new AccountCorpCloudDto
            {
                AccountId = corpCloudInfo.AccountId, 
                CountOfClicks = corpCloudInfo.CountOfClicks,
                LastUseDate = corpCloudInfo.LastUseDate
            });
        }
        /// <summary>
        /// Обновить информацию по корп облаку аккаунта
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public ManagerResult UpdateAccountCorpCloudInfo(Guid accountId)
        {
            var accountCorpCloud = unitOfWork.GetGenericRepository<AccountCorpCloud>().FirstOrDefault(ac => ac.AccountId == accountId);

            if (accountCorpCloud == null)
            {
                unitOfWork.GetGenericRepository<AccountCorpCloud>().Insert(
                    new AccountCorpCloud
                    {
                        Id = Guid.NewGuid(),
                        AccountId = accountId,
                        CountOfClicks = 1,
                        LastUseDate = DateTime.Now
                    });
                unitOfWork.Save();
                return ManagerResultHelper.Ok();
            }

            accountCorpCloud.CountOfClicks += 1;
            accountCorpCloud.LastUseDate = DateTime.Now;
            unitOfWork.GetGenericRepository<AccountCorpCloud>().Update(accountCorpCloud);

            unitOfWork.Save();
            return ManagerResultHelper.Ok();
        }
    }
}
