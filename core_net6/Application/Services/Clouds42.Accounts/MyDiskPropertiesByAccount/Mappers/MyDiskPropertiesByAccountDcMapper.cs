﻿using Clouds42.DataContracts.MyDisk;

namespace Clouds42.Accounts.MyDiskPropertiesByAccount.Mappers
{
    /// <summary>
    /// Маппер модели свойств сервиса "Мой диск" по аккаунту
    /// </summary>
    public static class MyDiskPropertiesByAccountDcMapper
    {
        /// <summary>
        /// Выполнить маппинг к доменной модели
        /// свойств сервиса "Мой диск" по аккаунту
        /// </summary>
        /// <param name="model">Модель свойств сервиса "Мой диск" по аккаунту</param>
        /// <returns>Свойства сервиса "Мой диск" по аккаунту</returns>
        public static Domain.DataModels.AccountProperties.MyDiskPropertiesByAccount
            MapToMyDiskPropertiesByAccount(this MyDiskPropertiesByAccountDto model) => new()
            {
                AccountId = model.AccountId,
                MyDiskFilesSizeInMb = model.MyDiskFilesSizeInMb,
                MyDiskFilesSizeActualDateTime = model.MyDiskFilesSizeActualDateTime
            };
    }
}