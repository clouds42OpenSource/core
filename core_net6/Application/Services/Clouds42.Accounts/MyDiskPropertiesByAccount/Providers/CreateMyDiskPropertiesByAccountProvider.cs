﻿using Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces;
using Clouds42.Accounts.MyDiskPropertiesByAccount.Mappers;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.MyDiskPropertiesByAccount.Providers
{
    /// <summary>
    /// Провайдер для создания свойств сервиса "Мой диск" по аккаунту
    /// </summary>
    internal class CreateMyDiskPropertiesByAccountProvider(IUnitOfWork dbLayer)
        : ICreateMyDiskPropertiesByAccountProvider
    {
        /// <summary>
        /// Создать свойств сервиса "Мой диск" по аккаунту
        /// </summary>
        /// <param name="model">Модель создания свойств
        /// сервиса "Мой диск" по аккаунту</param>
        /// <returns>Свойства сервиса "Мой диск" по аккаунту</returns>
        public Domain.DataModels.AccountProperties.MyDiskPropertiesByAccount Create(MyDiskPropertiesByAccountDto model)
        {
            var myDiskPropertiesByAccount = model.MapToMyDiskPropertiesByAccount();
            dbLayer.GetGenericRepository<Domain.DataModels.AccountProperties.MyDiskPropertiesByAccount>().Insert(myDiskPropertiesByAccount);
            dbLayer.Save();

            return myDiskPropertiesByAccount;
        }
    }
}
