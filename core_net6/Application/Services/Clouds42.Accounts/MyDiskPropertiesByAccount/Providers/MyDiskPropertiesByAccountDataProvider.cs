﻿using Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.MyDiskPropertiesByAccount.Providers
{
    /// <summary>
    /// Провайдер для работы с данными
    /// свойств сервиса "Мой диск" по аккаунту
    /// </summary>
    internal class MyDiskPropertiesByAccountDataProvider(
        ICreateMyDiskPropertiesByAccountProvider createMyDiskPropertiesByAccountProvider,
        IUnitOfWork dbLayer)
        : IMyDiskPropertiesByAccountDataProvider
    {
        /// <summary>
        /// Получить свойства сервиса "Мой диск" по аккаунту
        /// или создать новые (если записи не существует)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Свойства сервиса "Мой диск" по аккаунту</returns>
        public Domain.DataModels.AccountProperties.MyDiskPropertiesByAccount GetIfExistsOrCreateNew(Guid accountId)
        {
            var myDiskPropertiesByAccount = dbLayer.GetGenericRepository<Domain.DataModels.AccountProperties.MyDiskPropertiesByAccount>()
                .FirstOrDefault(diskProperties => diskProperties.AccountId == accountId);

            if (myDiskPropertiesByAccount == null)
                return createMyDiskPropertiesByAccountProvider.Create(new MyDiskPropertiesByAccountDto
                {
                    AccountId = accountId
                });

            return myDiskPropertiesByAccount;
        }
    }
}
