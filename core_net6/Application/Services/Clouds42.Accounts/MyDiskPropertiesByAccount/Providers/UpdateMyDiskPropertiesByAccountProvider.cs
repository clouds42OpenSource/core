﻿using Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.MyDiskPropertiesByAccount.Providers
{
    /// <summary>
    /// Провайдер для обновления свойств сервиса "Мой диск" по аккаунту
    /// </summary>
    internal class UpdateMyDiskPropertiesByAccountProvider(
        IMyDiskPropertiesByAccountDataProvider myDiskPropertiesByAccountDataProvider,
        IUnitOfWork dbLayer)
        : IUpdateMyDiskPropertiesByAccountProvider
    {
        /// <summary>
        /// Обновить свойства сервиса "Мой диск" по аккаунту
        /// </summary>
        /// <param name="model">Модель свойств сервиса "Мой диск" по аккаунту</param>
        public void Update(MyDiskPropertiesByAccountDto model)
        {
            var myDiskPropertiesByAccount =
                myDiskPropertiesByAccountDataProvider.GetIfExistsOrCreateNew(model.AccountId);

            myDiskPropertiesByAccount.MyDiskFilesSizeInMb = model.MyDiskFilesSizeInMb;
            myDiskPropertiesByAccount.MyDiskFilesSizeActualDateTime = model.MyDiskFilesSizeActualDateTime;

            dbLayer.GetGenericRepository<Domain.DataModels.AccountProperties.MyDiskPropertiesByAccount>().Update(myDiskPropertiesByAccount);
            dbLayer.Save();
        }
    }
}