﻿using Clouds42.DataContracts.Navigation;

namespace Clouds42.Accounts.Stripe.Helpers
{
    /// <summary>
    /// Маппер для параметров страйпа
    /// </summary>
    public class StripeOptionsMapper
    {
        /// <summary>
        /// Замаппить входящие параметры в модель параметров пользователя для страйпа
        /// </summary>
        /// <param name="isUserAuthenticated">Пользовать аутентифицирован</param>
        /// <param name="userLogin">Логин пользователя</param>
        /// <param name="contextAccountId">Id аккаунта из контекста</param>
        /// <param name="stripeOptionsDto">Модель параметров страйпа</param>
        /// <returns>Модель параметров пользователя для страйпа</returns>
        public UserOptionsForStripeModelDto MapToUserParametersForStripeModel(bool isUserAuthenticated,
            string userLogin, Guid? contextAccountId, StripeOptionsDto stripeOptionsDto = null) =>
            new()
            {
                UserLogin = userLogin,
                StripeOptionsDto = stripeOptionsDto ?? new StripeOptionsDto(),
                ContextAccountId = contextAccountId,
                IsUserAuthenticated = isUserAuthenticated
            };
    }
}
