﻿using Clouds42.Accounts.Contracts.Stripe.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Navigation;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Stripe.Providers
{
    /// <summary>
    /// Провайдер для работы с данными для страйпа
    /// </summary>
    internal class StripeDataProvider(
        IUnitOfWork dbLayer,
        ILocaleProvider localeProvider,
        ILogger42 logger)
        : IStripeDataProvider
    {
        private const int CurrentBalanceRoundValue = 2;

        /// <summary>
        /// Получить данные пользователя для страйпа
        /// </summary>
        /// <param name="userLogin">Логин пользователя</param>
        /// <returns>Модель данных пользователя для страйпа</returns>
        public UserDataForStripeDto GetUserDataForStripe(string userLogin) =>
            (from user in dbLayer.AccountUsersRepository.WhereLazy()
             join billingAccount in dbLayer.BillingAccountRepository.WhereLazy() on user.AccountId
                 equals billingAccount.Id
             where user.Login == userLogin
             select new UserDataForStripeDto
             {
                 UserLogin = user.Login,
                 Balance = billingAccount.Balance
             }).FirstOrDefault() ?? throw new NotFoundException(
                $"Не удалось получить баланс аккаунта по логину польззователя {userLogin}");

        /// <summary>
        /// Получить модель страйпа
        /// </summary>
        /// <param name="userOptionsForStripeModel">Модель параметров пользователя для получения страйпа</param>
        /// <returns>Модель страйпа</returns>
        public StripeDto GetStripeData(UserOptionsForStripeModelDto userOptionsForStripeModel)
        {
            if (!userOptionsForStripeModel.IsUserAuthenticated)
                return CreateStripeDtoForNotAuthenticatedUser(
                    localeProvider.GetLocale(userOptionsForStripeModel.StripeOptionsDto.Local),
                    userOptionsForStripeModel.ContextAccountId, userOptionsForStripeModel.StripeOptionsDto);

            var accounts = GetAccountsDataForStripe(userOptionsForStripeModel.UserLogin,
                userOptionsForStripeModel.ContextAccountId);

            if (!accounts.Any())
            {
                logger.Info($"Не найдены данные для пользователя с логином: {userOptionsForStripeModel.UserLogin}");
                return CreateStripeDtoForNotAuthenticatedUser(
                    localeProvider.GetLocale(userOptionsForStripeModel.StripeOptionsDto.Local),
                    userOptionsForStripeModel.ContextAccountId, userOptionsForStripeModel.StripeOptionsDto);
            }

            var stripeDto = CreateStripeDtoWithCommonParameters(userOptionsForStripeModel.StripeOptionsDto,
                userOptionsForStripeModel.UserLogin);
            FillStripeDtoForUserAccounts(stripeDto, accounts, userOptionsForStripeModel.UserLogin,
                userOptionsForStripeModel.ContextAccountId);

            return stripeDto;
        }

        /// <summary>
        /// Заполнить модель страйпа для аккаунтов пользователя
        /// </summary>
        /// <param name="stripeDto">Модель страйпа</param>
        /// <param name="accounts">Аккаунты</param>
        /// <param name="userLogin">Логин пользователя</param>
        /// <param name="contextAccountId">Id аккаунта из контекста</param>
        private void FillStripeDtoForUserAccounts(StripeDto stripeDto, List<AccountDataForStripeDto> accounts,
            string userLogin, Guid? contextAccountId)
        {
            var selfAccount = accounts.FirstOrDefault(accData => accData.UserLogin == userLogin);
            if (selfAccount != null)
                FillStripeDtoForSelfAccount(stripeDto, selfAccount);

            var selectContextAccount = accounts.FirstOrDefault(accData => accData.AccountId == contextAccountId);
            if (selectContextAccount != null)
                FillStripeDtoForContextAccount(stripeDto, selectContextAccount);
        }

        /// <summary>
        /// Получить данные аккаунтов для страйпа
        /// </summary>
        /// <param name="userLogin">Логин пользователя</param>
        /// <param name="contextAccountId">Id аккаунта из контекста</param>
        /// <returns>Список моделей данных аккаунтов для страйпа</returns>
        private List<AccountDataForStripeDto> GetAccountsDataForStripe(string userLogin, Guid? contextAccountId) =>
            (from account in SelectAccountsByCondition(userLogin, contextAccountId)
             join accountConfiguration in dbLayer.GetGenericRepository<AccountConfiguration>().WhereLazy() on
                 account.Id equals accountConfiguration.AccountId
             join locale in dbLayer.LocaleRepository.WhereLazy() on accountConfiguration.LocaleId equals locale.ID
             join billingAccount in dbLayer.BillingAccountRepository.WhereLazy() on account.Id equals billingAccount
                 .Id
             join user in dbLayer.AccountUsersRepository.WhereLazy() on account.Id equals user.AccountId into users
             from user in users.Where(u => u.Login == userLogin).DefaultIfEmpty()
             select new AccountDataForStripeDto
             {
                 Name = account.AccountCaption,
                 Index = account.IndexNumber,
                 LocaleName = locale.Name,
                 LocaleCurrency = locale.Currency,
                 AccountId = account.Id,
                 Balance = billingAccount.Balance,
                 UserLogin = user != null ? user.Login : ""
             }).ToList();

        /// <summary>
        /// Выбрать аккаунты по условию
        /// </summary>
        /// <param name="userLogin">Логин пользователя</param>
        /// <param name="contextAccountId">Id аккаунта из контекста</param>
        /// <returns>Выбранные аккаунты</returns>
        private IQueryable<Domain.DataModels.Account> SelectAccountsByCondition(string userLogin, Guid? contextAccountId) =>
            (from user in dbLayer.AccountUsersRepository.WhereLazy()
             join account in dbLayer.AccountsRepository.WhereLazy() on user.AccountId equals account.Id
             where user.Login == userLogin
             select account).Union(
                from account in dbLayer.AccountsRepository.WhereLazy()
                where account.Id == contextAccountId
                select account
            );

        /// <summary>
        /// Создать модель страйпа с общими параметрами
        /// </summary>
        /// <param name="stripeOptionsDto">Модель параметров страйпа</param>
        /// <param name="userLogin">Логин пользователя</param>
        /// <returns>Модель страйпа</returns>
        private StripeDto CreateStripeDtoWithCommonParameters(StripeOptionsDto stripeOptionsDto, string userLogin) =>
            new()
            {
                ShowFlag = stripeOptionsDto.ShowFlags,
                IsBitrix = stripeOptionsDto.IsBitrix,
                IsLogin = true,
                CurrentUser = userLogin,
                SelectedAccName = "",
                SelectedAccIndex = ""
            };

        /// <summary>
        /// Заполнить модель страйпа для аккаунта из контекста
        /// </summary>
        /// <param name="stripeDto">Модель страйпа</param>
        /// <param name="contextAccountDataForStripeDc">Модель данных аккаунта из контекста</param>
        private void FillStripeDtoForContextAccount(StripeDto stripeDto,
            AccountDataForStripeDto contextAccountDataForStripeDc)
        {
            stripeDto.ContextAccountId = contextAccountDataForStripeDc.AccountId;
            stripeDto.SelectedAccName = contextAccountDataForStripeDc.Name;
            stripeDto.SelectedAccIndex = contextAccountDataForStripeDc.Index.ToString();
        }

        /// <summary>
        /// Заполнить модель страйпа для своего аккаунта
        /// </summary>
        /// <param name="stripeDto">Модель страйпа</param>
        /// <param name="selfAccountDataForStripeDc">Модель данных своего аккаунта</param>
        private void FillStripeDtoForSelfAccount(StripeDto stripeDto, AccountDataForStripeDto selfAccountDataForStripeDc)
        {
            stripeDto.ContextAccountId = selfAccountDataForStripeDc.AccountId;
            stripeDto.SelectedAccName = selfAccountDataForStripeDc.Name;
            stripeDto.SelectedAccIndex = selfAccountDataForStripeDc.Index.ToString();
            stripeDto.CurrentUserBalance = decimal.Round(selfAccountDataForStripeDc.Balance, CurrentBalanceRoundValue);
            stripeDto.LocaleName = selfAccountDataForStripeDc.LocaleName;
            stripeDto.LocaleCurrency = selfAccountDataForStripeDc.LocaleCurrency;
        }

        /// <summary>
        /// Создать модель страйпа для неаутентифицированного пользователя
        /// </summary>
        /// <param name="locale">Локаль</param>
        /// <param name="contextAccountId">Id аккаунта из контекста</param>
        /// <param name="stripeOptionsDto">Модель параметров страйпа</param>
        /// <returns>Модель страйпа</returns>
        private StripeDto CreateStripeDtoForNotAuthenticatedUser(Locale locale, Guid? contextAccountId,
            StripeOptionsDto stripeOptionsDto)
            => new()
            {
                ShowFlag = stripeOptionsDto.ShowFlags,
                IsBitrix = stripeOptionsDto.IsBitrix,
                IsLogin = false,
                LocaleName = locale.Name,
                LocaleCurrency = locale.Currency,
                ContextAccountId = contextAccountId ?? Guid.Empty
            };
    }
}