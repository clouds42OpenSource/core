﻿using Clouds42.Accounts.Contracts.Stripe.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Navigation;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Stripe.Managers
{
    /// <summary>
    /// Менеджер для работы с страйпом
    /// </summary>
    public class StripeManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IStripeDataProvider stripeDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить данные пользователя для страйпа
        /// </summary>
        /// <param name="isUserAuthorized">Признак что пользователь аутентифицирован</param>
        /// <param name="userLogin">Логин пользователя</param>
        /// <returns>Данные пользователя для страйпа</returns>
        public ManagerResult<UserDataForStripeDto> GetUserDataForStripe(bool isUserAuthorized, string userLogin)
        {
            try
            {
                if (!isUserAuthorized)
                    return Unauthorized<UserDataForStripeDto>("Пользователь не авторизирован");

                var data = stripeDataProvider.GetUserDataForStripe(userLogin);
                return Ok(data);
            }
            catch (Exception ex)
            {
                var message = $"Не удалось получить данные пользователя {userLogin} для страйпа";

                _handlerException.Handle(ex, $"[Ошибка получения данных пользователея для страйпа] {userLogin}");
                return PreconditionFailed<UserDataForStripeDto>(message);
            }
        }

        /// <summary>
        /// Получить модель страйпа
        /// </summary>
        /// <param name="userOptionsForStripeModel">Модель параметров пользователя для получения страйпа</param>
        /// <returns>Данные страйпа</returns>
        public ManagerResult<StripeDto> GetStripeData(UserOptionsForStripeModelDto userOptionsForStripeModel)
        {
            try
            {
                var data = stripeDataProvider.GetStripeData(userOptionsForStripeModel);

                return Ok(data);
            }
            catch (Exception ex)
            {
                var message = $"Не удалось получить данные страйпа для пользователя {userOptionsForStripeModel.UserLogin}.";

                _handlerException.Handle(ex, $"[Ошибка получения данных страйпа] {userOptionsForStripeModel.UserLogin}");

                return PreconditionFailed<StripeDto>($"{message}");
            }
        }
    }
}
