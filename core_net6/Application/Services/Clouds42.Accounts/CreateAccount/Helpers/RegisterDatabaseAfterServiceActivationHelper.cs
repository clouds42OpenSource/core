﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.CreateAccount.Helpers
{
    /// <summary>
    /// Хэлпер для регистрации инф. баз после активации сервиса
    /// </summary>
    public class RegisterDatabaseAfterServiceActivationHelper(
        IBillingServiceDataProvider serviceDataProvider,
        IDbTemplateDelimitersDataProvider dbTemplateDelimitersDataProvider,
        IRegisterAccountDatabaseProvider registerAccountDatabaseProvider,
        IUnitOfWork dbLayer,
        IInstallServiceExtensionDatabaseProvider installServiceExtensionDatabaseProvider)
    {
        /// <summary>
        /// Зарегистрировать инф. базу после активации сервиса если это необходимо.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUser">Пользователь облака.</param>
        /// <param name="compatibleConfig1CFromRegistrationConfig">
        /// Код совместимой конфигурации из настроек при регистрации
        /// (Нужен только при регистрации через АПИ)
        /// </param>
        public void RegisterDatabaseIfNeeded(Guid serviceId, IAccountUser accountUser,
            string compatibleConfig1CFromRegistrationConfig = null)
        {
            if (!NeedRegisterDatabase(serviceId, accountUser))
                return;

            RegisterDatabase(serviceId, accountUser, compatibleConfig1CFromRegistrationConfig);
        }

        /// <summary>
        /// Зарегистрировать инф. базу после активации сервиса.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUser">Пользователь облака.</param>
        /// <param name="compatibleConfig1CFromRegistrationConfig">
        /// Код совместимой конфигурации из настроек при регистрации
        /// (Нужен только при регистрации через АПИ)
        /// </param>
        private void RegisterDatabase(Guid serviceId, IAccountUser accountUser,
            string compatibleConfig1CFromRegistrationConfig = null)
        {
            var compatibleConfigurations =
                serviceDataProvider.GetConfigurationsCompatibleWithService(serviceId).ToList();

            if (compatibleConfigurations.Count > 1 && string.IsNullOrEmpty(compatibleConfig1CFromRegistrationConfig))
                return;

            var dbTemplateDelimiter = GetDbTemplateOnDelimitersForRegisterDatabase(compatibleConfigurations,
                compatibleConfig1CFromRegistrationConfig);
            if (dbTemplateDelimiter == null)
                return;

            var accountDatabaseId = registerAccountDatabaseProvider.RegisterByTemplate(accountUser, dbTemplateDelimiter.Template);
            installServiceExtensionDatabaseProvider.TryInstalExtensionForCreatedDatabase(serviceId, accountDatabaseId);
        }

        /// <summary>
        /// Признак необходимости регистрировать инф. базу
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUser">Пользователь облака.</param>
        /// <returns>
        /// true - если сервис не является Тардисом,
        /// у сервиса есть совместимые конфигурации
        /// и у аккаунта нет уже созданных баз с подходящей конфигурацией
        /// </returns>
        private bool NeedRegisterDatabase(Guid serviceId, IAccountUser accountUser)
        {
            var service = serviceDataProvider.GetBillingServiceOrThrowNewException(serviceId);
            if (service.InternalCloudService == InternalCloudServiceEnum.Tardis)
                return false;

            var compatibleConfigurations =
                serviceDataProvider.GetConfigurationsCompatibleWithService(serviceId).ToList();

            if (!compatibleConfigurations.Any())
                return false;

            var databasesWithEqualServiceConfiguration =
                GetAccountDatabasesWithEqualServiceConfiguration(accountUser.AccountId, compatibleConfigurations);

            return !databasesWithEqualServiceConfiguration.Any();
        }

        /// <summary>
        /// Получить шаблон базы на разделителях для регистрации
        /// </summary>
        /// <param name="compatibleConfigurations">Список совместимых конфигураций</param>
        /// <param name="compatibleConfig1CFromRegistrationConfig">
        /// Код совместимой конфигурации из настроек при регистрации
        /// (Нужен только при регистрации через АПИ)
        /// </param>
        /// <returns>Шаблон базы на разделителях для регистрации</returns>
        private DbTemplateDelimiters GetDbTemplateOnDelimitersForRegisterDatabase(List<string> compatibleConfigurations, string compatibleConfig1CFromRegistrationConfig)
        {
            var configurationId = compatibleConfigurations.First();
            if (compatibleConfigurations.Contains(compatibleConfig1CFromRegistrationConfig))
                configurationId = compatibleConfig1CFromRegistrationConfig;

            return dbTemplateDelimitersDataProvider.GetDbTemplateDelimitersByConfigurationId(configurationId);
        }

        /// <summary>
        /// Получить список совместимых с конфигурацией сервиса инф. баз
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="compatibleConfigurations">Список совместимых конфигураций</param>
        /// <returns>Список совместимых с конфигурацией сервиса инф. баз</returns>
        private List<Domain.DataModels.AccountDatabase> GetAccountDatabasesWithEqualServiceConfiguration(Guid accountId,
            List<string> compatibleConfigurations)
            => dbLayer.DatabasesRepository.Where(db =>
                    (db.State == DatabaseState.Ready.ToString() || db.State == DatabaseState.NewItem.ToString()) &&
                    db.AccountId == accountId &&
                    db.AccountDatabaseOnDelimiter != null &&
                    compatibleConfigurations.Contains(
                        db.AccountDatabaseOnDelimiter.DbTemplateDelimiter.ConfigurationId))
                .ToList();
    }
}
