﻿using System.Text;

namespace Clouds42.Accounts.CreateAccount.Helpers
{
    /// <summary>
    /// Генерирует сообщение для логов об создании аккаунта
    /// </summary>
    public class LogAccountEventHelper
    {
        /// <summary>
        /// Генерирует сообщения для логов успешном создании аккаунта
        /// </summary>
        /// <param name="name">Имя аккаунта</param>
        /// <param name="email">Почта аккаунта</param>
        /// <param name="phoneNumber">Телефон аккаунта</param>
        /// <returns>Строка успешном создании</returns>
        public string GenerateMessageAccountCreated(string name, string email, string phoneNumber)
        {
            var message = new StringBuilder("Регистрация аккаунта." + Environment.NewLine);

            if (!string.IsNullOrEmpty(name))
                message.AppendLine($"Логин: \"{name}\".");

            if (!string.IsNullOrEmpty(email))
                message.AppendLine($"Почта: \"{email}\".");

            if (!string.IsNullOrEmpty(phoneNumber))
                message.AppendLine($"Тел: \"{phoneNumber}\".");

            return message.ToString();
        }

        /// <summary>
        /// Генерирует сообщения для логов об ошибке при создании аккаунта
        /// </summary>
        /// <param name="email">Почта аккаунта</param>
        /// <param name="phoneNumber">Телефон аккаунта</param>
        /// <returns>Строка об ошибке</returns>
        public string GenerateMessageAccountCreatedFail(string email, string phoneNumber)
            => $"Не удалось зарегистрировать аккаунт. Почта: \"{email}\",  Тел: \"{phoneNumber}\"";
    }
}
