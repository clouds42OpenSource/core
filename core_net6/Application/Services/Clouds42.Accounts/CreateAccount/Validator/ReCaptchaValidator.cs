﻿using System.Net;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.AccountUsers.ServiceManagerConnector.Commands;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Logger;
using Microsoft.Extensions.Configuration;
using static Clouds42.Configurations.Configurations.CloudConfigurationProvider;

namespace Clouds42.Accounts.CreateAccount.Validator
{
    /// <summary>
    /// Валидатор капчи при регистрации аккаунта
    /// </summary>
    public class ReCaptchaValidator(
        ExecuteRequestCommand executeRequestCommand,
        ILogger42 logger,
        IConfiguration configuration)
        : IReCaptchaValidator
    {
        private readonly double _acceptableScore = CaptchaValidatorConfiguration.GetAcceptableScore();
        private readonly string _fullUrl = CaptchaValidatorConfiguration.GetRemoteAddress();

        public bool IsCaptchaPassed(string token)
        {

            if (string.IsNullOrEmpty(token))
            {
                logger.Debug("Нет токена со страницы регистрации");
                return false;
            }

            var url = string.Format(_fullUrl, token);
            logger.Debug($"Запрос на валидацию токена от пользователя отправлен по адресу {url}");
            var response = executeRequestCommand.ExecuteJsonRequest(url, HttpMethod.Post);
            var responseData = response.Content.ReadAsStringAsync().Result;
            logger.Debug($"Запрос вернулся со статусом кода {response.StatusCode} и содержанием {responseData}");
            if (response.StatusCode != HttpStatusCode.OK)
                return false;

            var result = responseData.DeserializeFromJson<ReCapcaResultDto>();
            logger.Debug($"Результат валидации токена Success = {result.Success}, Score = {result.Score}, выставленный уровень {_acceptableScore}");

            if (result.Success)
                return result.Score >= _acceptableScore;
            var reCaptchaFlag = configuration.GetValue<bool>("ReCaptchaFlag", true);
            return reCaptchaFlag ? result.ErrorCodes.Contains("timeout-or-duplicate") : result.Success;
        }

    }
}
