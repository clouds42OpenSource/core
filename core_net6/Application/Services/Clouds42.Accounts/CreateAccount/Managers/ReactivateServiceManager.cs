﻿using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.BillingServices.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.CreateAccount.Managers
{
    /// <summary>
    /// Менеджер по реактивции сервиса.
    /// </summary>
    public class ReactivateServiceManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloudLocalizer cloudLocalizer,
        IActivateServiceForRegistrationAccountProvider activateServiceForRegistrationAccountProvider,
        IHandlerException handlerException,
        IBillingServiceDataProvider billingServiceDataProvider,
        BillingServiceManager billingServiceManager,
        BillingServiceTypeAccessProvider billingServiceTypeAccessProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Активировать сервис у существующего пользователя.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        public ManagerResult<ServiceActivationResultDto> ActivateServiceForExistingAccount(Guid serviceId,
            Guid accountUserId)
        {
            return InternalActivateServiceForExistingAccount(serviceId, accountUserId, true);
        }


        /// <summary>
        /// Активировать сервис у существующего пользователя.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="serviceId">Номер сервиса.</param>
        public ManagerResult ActivateServiceForExistingAccountWithoutServiceInstallation(
            Guid accountId, Guid serviceId)
        {
            var currentUser = AccessProvider.GetUser();
            AccessProvider.HasAccess(ObjectAction.BillingService_ActivateForExistingAccount, () => accountId);

            var accountAdmins = AccessProvider.GetAccountAdmins(accountId);
            var accountUserId = accountAdmins.Exists(adminUserId => adminUserId.Equals(currentUser.Id))
                           ? currentUser.Id
                           : accountAdmins.FirstOrDefault();

            var result = InternalActivateServiceForExistingAccount(serviceId, accountUserId, false);

            if (result.Error)
            {
                return result;
            }

            if (result.Result.IsAlreadyActivated)
            {
                return EnableAllServiceOptionsIfNoOneSelectedFor(accountId, accountUserId, serviceId);
            }

            return result;
        }

        /// <summary>
        /// Включение всех опций для аккаунта и первого админа аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта для которого включить опцию</param>
        /// <param name="accountAdminUserId">ID пользователя (админ аккаунта) для которого включить опцию</param>
        /// <param name="serviceId">Для какого сервиса включить опции</param>
        /// <returns>Результат выполнения</returns>
        private ManagerResult EnableAllServiceOptionsIfNoOneSelectedFor(Guid accountId, Guid accountAdminUserId, Guid serviceId)
        {
            List<ActiveAccountServiceOptionsResultDto> accountServiceOptionsForAccount;
            List<ActiveAccountServiceOptionsResultDto> accountServiceOptionsForUser;
            List<ServiceOptionsResultDto> billingServiceOptions;

            try
            {
                accountServiceOptionsForAccount = billingServiceTypeAccessProvider.GetActiveAccountServiceOptionsFor(serviceId, accountId, BillingTypeEnum.ForAccount);
                accountServiceOptionsForUser = billingServiceTypeAccessProvider.GetActiveAccountServiceOptionsFor(serviceId, accountId, BillingTypeEnum.ForAccountUser);
                billingServiceOptions = billingServiceTypeAccessProvider.GetServiceOptionsBillingTypeOfService(serviceId);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка активации опций сервиса]");
                return PreconditionFailed(ex.Message);
            }

            if (accountServiceOptionsForAccount.Any() || accountServiceOptionsForUser.Any())
            {
                return Ok();
            }

            var modifyAccountServiceOptionList = new List<ModifyAccountServiceOptionDto>();
            modifyAccountServiceOptionList.AddRange(CreateEnableServiceOptionsForAccount(accountId, billingServiceOptions));
            modifyAccountServiceOptionList.AddRange(CreateEnableServiceOptionsForUser(accountAdminUserId, billingServiceOptions));

            return UpdateServiceOptionsForAccount(accountId, serviceId, modifyAccountServiceOptionList);
        }

        /// <summary>
        /// Создать модели включения опций на аккаунт
        /// </summary>
        /// <param name="accountId">На какой аккаунт создать включения опция</param>
        /// <param name="billingServiceOptions">Опции сервиса</param>
        /// <returns>Модели включения опций на аккаунт</returns>
        private IEnumerable<ModifyAccountServiceOptionDto> CreateEnableServiceOptionsForAccount(
            Guid accountId, List<ServiceOptionsResultDto> billingServiceOptions)
            => billingServiceOptions.Where(item => item.BillingType == BillingTypeEnum.ForAccount).Select(item =>
            {
                return new ModifyAccountServiceOptionDto
                {
                    IsEnabled = true,
                    ServiceOptionId = item.ServiceOptionId,
                    Subject = accountId,
                    Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto()
                };
            });

        /// <summary>
        /// Создать модели включения опций на аккаунт
        /// </summary>
        /// <param name="accountUserId">На какого пользователя создать включения опций</param>
        /// <param name="billingServiceOptions">Опции сервиса</param>
        /// <returns>Модели включения опций на пользователя</returns>
        private IEnumerable<ModifyAccountServiceOptionDto> CreateEnableServiceOptionsForUser(
            Guid accountUserId, List<ServiceOptionsResultDto> billingServiceOptions)
            => billingServiceOptions.Where(item => item.BillingType == BillingTypeEnum.ForAccountUser).Select(item =>
            {
                return new ModifyAccountServiceOptionDto
                {
                    IsEnabled = true,
                    ServiceOptionId = item.ServiceOptionId,
                    Subject = accountUserId,
                    Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto()
                };
            });

        /// <summary>
        /// Отключение всех опций для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта для которого отключить опцию</param>
        /// <param name="serviceId">Для какого сервиса выключить опции</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult DisableAllServiceOptionsFor(Guid accountId, Guid serviceId)
        {
            List<ActiveAccountServiceOptionsResultDto> accountServiceOptionsForAccount;
            List<ActiveAccountServiceOptionsResultDto> accountServiceOptionsForUser;

            try
            {
                accountServiceOptionsForAccount = billingServiceTypeAccessProvider.GetActiveAccountServiceOptionsFor(serviceId, accountId, BillingTypeEnum.ForAccount);
                accountServiceOptionsForUser = billingServiceTypeAccessProvider.GetActiveAccountServiceOptionsFor(serviceId, accountId, BillingTypeEnum.ForAccountUser);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка Деактивации опций сервиса]");
                return PreconditionFailed(ex.Message);
            }

            var disableAccountServiceOptionList = new List<ModifyAccountServiceOptionDto>();

            disableAccountServiceOptionList.AddRange(
                accountServiceOptionsForAccount.Select(item =>
                {
                    return new ModifyAccountServiceOptionDto
                    {
                        IsEnabled = false,
                        ServiceOptionId = item.ServiceoptionId,
                        Subject = item.Subject,
                        Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto()
                    };
                })
           );

            disableAccountServiceOptionList.AddRange(
                accountServiceOptionsForUser.Select(item =>
                {
                    return new ModifyAccountServiceOptionDto
                    {
                        IsEnabled = false,
                        ServiceOptionId = item.ServiceoptionId,
                        Subject = item.Subject,
                        Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto()
                    };
                })
            );

            return UpdateServiceOptionsForAccount(accountId, serviceId, disableAccountServiceOptionList);
        }


        public ManagerResult<BillingServiceTypeApplyOrPayResultDto> UpdateServiceOptionsForAccount(Guid accountId, ModifyAccountServiceOptionDto args)
        {
            var currentUser = AccessProvider.GetUser();
            AccessProvider.HasAccess(ObjectAction.BillingService_ChangeSubscribe, () => accountId, null, () =>
            {
                var accountAdmins = AccessProvider.GetAccountAdmins(accountId);
                return currentUser != null && currentUser.RequestAccountId == accountId && accountAdmins.Exists(adminUserId => adminUserId == currentUser.Id);
            });

            Guid serviceId;
            try
            {
                serviceId = billingServiceTypeAccessProvider.GetServiceIdByServiceOptionId(args.ServiceOptionId);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка обновления опции сервиса]");
                return PreconditionFailed<BillingServiceTypeApplyOrPayResultDto>(ex.Message);
            }

            return UpdateServiceOptionsForAccount(accountId, serviceId, [args]);
        }


        public ManagerResult<BillingServiceTypeApplyOrPayResultDto> UpdateServiceOptionsForAccount(Guid accountId, Guid serviceId,
            List<ModifyAccountServiceOptionDto> args)
        {
            var currentUser = AccessProvider.GetUser();
            AccessProvider.HasAccess(ObjectAction.BillingService_ChangeSubscribe, () => accountId, null, () =>
            {
                var accountAdmins = AccessProvider.GetAccountAdmins(accountId);
                return currentUser != null && currentUser.RequestAccountId == accountId && accountAdmins.Exists(adminUserId => adminUserId == currentUser.Id);
            });

            List<ServiceOptionsResultDto> serviceOptionBillingTypes;
            try
            {
                serviceOptionBillingTypes = billingServiceTypeAccessProvider.GetServiceOptionsBillingTypeOfService(serviceId);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка обновления опции сервиса]");
                return PreconditionFailed<BillingServiceTypeApplyOrPayResultDto>(ex.Message);
            }

            var dataAccount = new List<CalculateBillingServiceTypeDto>();
            var dataUsers = new List<CalculateBillingServiceTypeDto>();

            foreach (var item in args)
            {
                var foundServiceOption = serviceOptionBillingTypes.FirstOrDefault(value => value.ServiceOptionId == item.ServiceOptionId);

                if (foundServiceOption == null)
                {
                    continue;
                }

                var data = new CalculateBillingServiceTypeDto
                {
                    BillingServiceTypeId = item.ServiceOptionId,
                    Subject = item.Subject,
                    Sponsorship = item.Sponsorship ?? new AccountUserBillingServiceTypeSponsorshipDto(),
                    Status = item.IsEnabled
                };

                switch (foundServiceOption.BillingType)
                {
                    case BillingTypeEnum.ForAccountUser:
                        dataUsers.Add(data);
                        break;

                    case BillingTypeEnum.ForAccount:
                        dataAccount.Add(data);
                        break;

                    default:
                        break;
                }
            }

            return billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(accountId, serviceId, false, dataAccount, dataUsers);
        }

        /// <summary>
        /// Активировать сервис у существующего пользователя.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        /// <param name="withInstallServiceIfNeed">С установкой сервиса в базу, если нужно (нету подходящих баз к этому сервису).</param>
        private ManagerResult<ServiceActivationResultDto> InternalActivateServiceForExistingAccount(Guid serviceId,
            Guid accountUserId, bool withInstallServiceIfNeed)
        {
            var accountId = AccountIdByAccountUser(accountUserId);
            var searchServiceIdResult = billingServiceManager.TryGetServiceIdByKey(serviceId);

            if (searchServiceIdResult.Error)
                throw new NotFoundException(searchServiceIdResult.Message);

            serviceId = searchServiceIdResult.Result;

            var checkResult = billingServiceManager.CheckAbilityToActivateService(serviceId, accountId);
            if (checkResult.Error)
                return PreconditionFailed<ServiceActivationResultDto>(checkResult.Message);

            AccessProvider.HasAccess(ObjectAction.BillingService_ActivateForExistingAccount, () => accountId);

            var service = billingServiceDataProvider.GetBillingServiceOrThrowException(serviceId);
            var serviceName = service.GetNameBasedOnLocalization(cloudLocalizer, accountId ?? AccessProvider.ContextAccountId);

            var hasRent = _dbLayer.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .Any(rc =>  rc.AccountId == accountId && rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            try
            {
                var result = withInstallServiceIfNeed && (!service.IsHybridService || hasRent)
                    ? activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(serviceId, accountUserId)
                    : activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccountWithoutServiceInstallation(serviceId, accountUserId);

                var message = $"Активирован демо-период сервиса \"{serviceName}\"";
                Logger.Info(message);

                return Ok(result);
            }
            catch (Exception ex)
            {
                var message = $"Активация демо-периода сервиса \"{serviceName}\" завершилась ошибкой.";
                _handlerException.Handle(ex, "[Ошибка активации демо периода сервиса]");
                LogEvent(() => AccountIdByAccountUser(accountUserId), LogActions.ActivateServiceForExistingAccount,
                    $"{message} Ошибка: {ex.GetMessage()}");
                return PreconditionFailed<ServiceActivationResultDto>(ex.Message);
            }
        }
    }
}
