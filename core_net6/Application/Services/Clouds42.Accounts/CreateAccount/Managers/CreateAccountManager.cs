﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Accounts.CreateAccount.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.CreateAccount.Managers
{
    /// <summary>
    /// Менеджер создания аккаунта.
    /// </summary>
    public class CreateAccountManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateAccountProvider createAccountProvider,
        LogAccountEventHelper logAccountEventHelper,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        ///  Зарегистрировать аккаунт для реферала.
        /// </summary>
        public async Task<ManagerResult<bool>> CreateAccountByReferral(string email, string phoneNumber, Guid referralAccountId)
        {
            try
            {
                var referralAccount = await DbLayer.AccountsRepository.FirstOrDefaultAsync(a => a.Id == referralAccountId);

                if (referralAccount == null)
                    return NotFound<bool>($"Не удалось получить аккаунт рефферала по номеру '{referralAccountId}'");

                var result = await createAccountProvider.CreateAccount(new AccountRegistrationModelDto
                {
                    LocaleName = accountConfigurationDataProvider.GetAccountLocale(referralAccountId).Name,
                    Email = email,
                    FullPhoneNumber = phoneNumber,
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise,
                        ReferralAccountId = referralAccountId
                    }
                });

                var message =
                    logAccountEventHelper.GenerateMessageAccountCreated(result.AccountUserLogin, email, phoneNumber);

                logger.Trace(message);
                LogEvent(() => AccountIdByAccountUser(result.AccountUserLogin), LogActions.CreateAcccount, message);

                return Ok(true);
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"Ошибка регистрации нового аккаунта Почта {email} Телефон {phoneNumber}");
                LogEvent(GetInitiatorAccountId, LogActions.CreateAcccount,
                    logAccountEventHelper.GenerateMessageAccountCreatedFail(email, phoneNumber) +
                    $"Описание ошибки: {ex.Message}");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Создать новый аккаунт.
        /// </summary>
        /// <param name="accountInfo">регистрационная модель</param>
        /// <returns>Созданный аккаунт</returns>
        public async Task<ManagerResult<AccountRegistrationResultDto>> AddAccount(AccountRegistrationModelDto accountInfo)
        {
            try
            {
                var result = await createAccountProvider.CreateAccount(accountInfo);

                var message = logAccountEventHelper.GenerateMessageAccountCreated(result.AccountUserLogin,
                    accountInfo.Email, accountInfo.FullPhoneNumber);
                logger.Trace(message);
                LogEvent(() => AccountIdByAccountUser(result.AccountUserLogin), LogActions.CreateAcccount, message);

                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Warn(ex,
                    $"Ошибка регистрации нового аккаунта Почта {accountInfo.Email} Телефон {accountInfo.FullPhoneNumber}");
                LogEvent(GetInitiatorAccountId, LogActions.CreateAcccount,
                    logAccountEventHelper.GenerateMessageAccountCreatedFail(accountInfo.Email,
                        accountInfo.FullPhoneNumber) +
                    $"Описание ошибки: {ex.GetFullInfo(false)}");

                return PreconditionFailed<AccountRegistrationResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Создать новый аккаунт и сессию для доступа по АПИ.
        /// </summary>
        public async Task<ManagerResult<AccountAuthDto>> AddAccountAndCreateSessionAsync(AccountRegistrationModelDto accountInfo)
        {
            try
            {
                var result = await createAccountProvider.AddAccountAndCreateSession(accountInfo);

                var message = logAccountEventHelper.GenerateMessageAccountCreated(accountInfo.Login, accountInfo.Email,
                    accountInfo.FullPhoneNumber);

                logger.Trace(message);
                LogEvent(() => AccountIdByAccountUser(accountInfo.Login), LogActions.CreateAcccount, message);

                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Warn(ex,
                    $"Ошибка регистрации нового аккаунта Почта {accountInfo.Email} Телефон {accountInfo.FullPhoneNumber}");
                LogEvent(GetInitiatorAccountId, LogActions.CreateAcccount,
                    logAccountEventHelper.GenerateMessageAccountCreatedFail(accountInfo.Email,
                        accountInfo.FullPhoneNumber) +
                    $"Описание ошибки: {ex.GetFullInfo(false)}");

                return PreconditionFailed<AccountAuthDto>(ex.Message);
            }
        }

        /// <summary>
        /// Add an account without creating services and create an auth session
        /// </summary>
        /// <param name="accountInfo"></param>
        /// <returns>Auth info</returns>
        public async Task<ManagerResult<AccountAuthDto>> AddAccountAndSessionWithoutServices(AccountRegistrationModelDto accountInfo)
        {
            try
            {
                var result = await createAccountProvider.AddAccountWithoutServicesAndCreateSession(accountInfo);

                var message = logAccountEventHelper.GenerateMessageAccountCreated(accountInfo.Login, accountInfo.Email,
                    accountInfo.FullPhoneNumber);

                logger.Trace(message);
                LogEvent(() => AccountIdByAccountUser(accountInfo.Login), LogActions.CreateAcccount, message);

                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Warn(ex,
                     $"Ошибка регистрации нового аккаунта. Почта {accountInfo.Email}, Телефон {accountInfo.FullPhoneNumber}");

                LogEvent(GetInitiatorAccountId, LogActions.CreateAcccount,
                    $"{logAccountEventHelper.GenerateMessageAccountCreatedFail(accountInfo.Email, accountInfo.FullPhoneNumber)} " +
                    $"Описание ошибки: {ex.GetFullInfo(false)}");

                return PreconditionFailed<AccountAuthDto>(ex.Message);
            }
        }
    }
}
