﻿namespace Clouds42.Accounts.CreateAccount.Constants
{
    /// <summary>
    /// Константы для создания аккаунта
    /// </summary>
    public static class CreateAccountConst
    {
        /// <summary>
        /// Дефолтный тип аккаунта
        /// </summary>
        public const string DefaultType = "42Clouds";
    }
}
