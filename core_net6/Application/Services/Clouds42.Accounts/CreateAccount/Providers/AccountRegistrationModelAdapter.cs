﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.CreateAccount.Providers
{
    /// <summary>
    /// Адаптер модели регистрации аккаунта
    /// </summary>
    public class AccountRegistrationModelAdapter(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Применить маппинг для конфига регистрации
        /// </summary>
        /// <param name="accountInfo">Регистрационная модель</param>
        public void ApplyMappingForRegistrationConfig(AccountRegistrationModelDto accountInfo)
        {
            var billingService = FindBillingService(accountInfo);
            if (billingService == null)
                return;

            accountInfo.RegistrationConfig.Configuration1C = null;
            accountInfo.RegistrationConfig.CloudServiceId = billingService.Id;

            if (accountInfo.RegistrationConfig?.RegistrationSource != ExternalClient.Market)
                accountInfo.RegistrationConfig.RegistrationSource =
                    GetRegistrationSource(billingService.InternalCloudService);

            if (CheckIsServiceMcob(billingService))
                accountInfo.RegistrationConfig.RegistrationSource = ExternalClient.MCOB;
        }

        /// <summary>
        /// Если есть, то найти биллинг-сервис на который идет регистрация.
        /// </summary>
        /// <param name="accountInfo">Модель регистрации.</param>
        /// <returns>Биллинг сервис, на которыйидет регистрация.</returns>
        private BillingService FindBillingService(AccountRegistrationModelDto accountInfo)
        {
            if (accountInfo.RegistrationConfig == null)
                return null;

            if (accountInfo.RegistrationConfig.CloudServiceId.HasValue)
            {
                return dbLayer.BillingServiceRepository.FirstOrDefault(service =>
                    service.Id == accountInfo.RegistrationConfig.CloudServiceId);
            }

            var internalCloudService =
                SearchInternalCloudService(accountInfo.RegistrationConfig.RegistrationSource);

            if (internalCloudService == null)
                return null;

            return dbLayer.BillingServiceRepository.FirstOrDefault(service =>
                service.InternalCloudService == internalCloudService &&
                service.BillingServiceStatus == BillingServiceStatusEnum.IsActive);
        }

        /// <summary>
        /// Провести соответсвие внутриссиемного сервиса со статическим брендом.
        /// </summary>
        /// <param name="internalCloudService"></param>
        /// <returns></returns>
        private static ExternalClient GetRegistrationSource(InternalCloudServiceEnum? internalCloudService)
        {
            return internalCloudService switch
            {
                InternalCloudServiceEnum.Delans => ExternalClient.Delans,
                InternalCloudServiceEnum.Kladovoy => ExternalClient.Kladovoy,
                InternalCloudServiceEnum.Sauri => ExternalClient.Sauri,
                _ => ExternalClient.Promo
            };
        }

        /// <summary>
        /// Поиск внутреннего сервиса
        /// </summary>
        /// <param name="externalClient">Внешний клиент</param>
        /// <returns>Внутренний облачный сервис</returns>
        public static InternalCloudServiceEnum? SearchInternalCloudService(ExternalClient externalClient)
        {
            foreach (InternalCloudServiceEnum internalCloudService in Enum.GetValues(typeof(InternalCloudServiceEnum)))
            {
                if (internalCloudService.ToString() == externalClient.ToString())
                    return internalCloudService;
            }

            return null;
        }

        /// <summary>
        /// Проверить является ли сервис сервисом МЦОБ
        /// </summary>
        /// <param name="billingService">Сервис облака.</param>
        /// <returns>Результат проверки</returns>
        private static bool CheckIsServiceMcob(BillingService billingService) =>
            billingService.Id == CloudConfigurationProvider.Mcob.GetMcobBillingServiceId();
    }
}
