﻿using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Logger;

namespace Clouds42.Accounts.CreateAccount.Providers
{
    /// <summary>
    /// Провайдер валидации пользователя в АД
    /// </summary>
    internal class ValidateLoginInActiveDirectoryProvider(
        IRegisterAccountAdHelper registerAccountAdHelper,
        ILogger42 logger)
        : IValidateLoginInActiveDirectoryProvider
    {
        /// <summary>
        /// /Выполнить валидацию пользователя в АД
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        public void Validate(string login)
        {
            if (string.IsNullOrEmpty(login) || !registerAccountAdHelper.IsLoginInActiveDirectoryExist(login))
                return;

            logger.Warn($"Логин {login} существует в Active Directory.");
            throw new ValidateException($"Логин {login} уже существует.");
        }
    }
}
