﻿using Clouds42.DataContracts.BillingService;
using CommonLib.Enums;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.CreateAccount.Providers
{
    /// <summary>
    /// Активатор Аренды 1С
    /// </summary>
    internal class Rent1CForCustomServiceActivator : IRent1CForCustomServiceActivator
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly ICloud42ServiceFactory _cloud42ServiceFactory;
        private readonly ICloudLocalizer _cloudLocalizer;

        private readonly Dictionary<Rent1CActivationType, Func<Rent1CActivationParamsDto, List<ResourceType>>>
            _mapParamsToGetRent1CServiceTypes;

        public Rent1CForCustomServiceActivator(IUnitOfWork dbLayer, ICloud42ServiceFactory cloud42ServiceFactory, ICloudLocalizer cloudLocalizer)
        {
            _dbLayer = dbLayer;
            _cloud42ServiceFactory = cloud42ServiceFactory;
            _cloudLocalizer = cloudLocalizer;

            _mapParamsToGetRent1CServiceTypes =
                new Dictionary<Rent1CActivationType, Func<Rent1CActivationParamsDto, List<ResourceType>>>
                {
                    {Rent1CActivationType.ForNewAccount, GetRent1CResourcesTypeForCustomServices},
                    {Rent1CActivationType.ForExistingAccount, GetRent1CResourcesTypes}
                };
        }

        /// <summary>
        /// Активировать Аренду 1С
        /// </summary>
        /// <param name="rent1CActivationParams">Параметры активации Аренды 1С</param>
        public void ActivateRent1C(Rent1CActivationParamsDto rent1CActivationParams)
        {
            if (!_mapParamsToGetRent1CServiceTypes.TryGetValue(rent1CActivationParams.Rent1CActivationType, out var type))
                throw new InvalidOperationException(
                    $"Невозможно определить метод для получения списка услуг сервиса {_cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, rent1CActivationParams.AccountId)} для активации");

            var rent1CServiceTypes =
                type(rent1CActivationParams);
            var cloudService = (IMyEnterprise42CloudService)_cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise,
                rent1CActivationParams.AccountId);
            cloudService.ActivateRent1C(rent1CActivationParams.AccountUserId, rent1CServiceTypes);
        }

        /// <summary>
        /// Получить список услуг аренды от которых зависят кастомные сервисы
        /// </summary>
        /// <param name="rent1CActivationParams">Параметры активации Аренды 1С</param>
        /// <returns>Список услуг аренды от которых зависят кастомные сервисы</returns>
        private List<ResourceType> GetRent1CResourcesTypeForCustomServices(
            Rent1CActivationParamsDto rent1CActivationParams)
        {
            var billingServices = _dbLayer.BillingServiceRepository.Where(service =>
                rent1CActivationParams.BillingServicesIdList.Contains(service.Id) ||
                rent1CActivationParams.BillingServicesIdList.Contains(service.Key)).ToList();

            var rent1CResourcesTypes = new List<ResourceType>();

            billingServices.ForEach(service =>
            {
                var rent1CResourcesType = GetRent1CResourcesTypeForCustomService(service);

                rent1CResourcesType.ForEach(resType =>
                {
                    if (!rent1CResourcesTypes.Contains(resType))
                        rent1CResourcesTypes.Add(resType);
                });
            });

            return rent1CResourcesTypes;
        }

        /// <summary>
        /// Получить список услуг аренды от которых зависит кастомный сервис
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <returns>Список услуг аренды от которых зависит кастомный сервис</returns>
        private List<ResourceType> GetRent1CResourcesTypeForCustomService(
            BillingService billingService)
        {
            var dependServiceTypes = billingService.BillingServiceTypes
                .Where(st => st.DependServiceType?.SystemServiceType != null).Select(st => st.DependServiceType)
                .ToList();

            var childSystemServiceTypes =
                (from dst in dependServiceTypes
                 join relation in _dbLayer.BillingServiceTypeRelationRepository.WhereLazy() on dst.Id equals relation
                     .MainServiceTypeId
                 where relation.ChildServiceType.SystemServiceType != null
                 select relation.ChildServiceType.SystemServiceType).ToList().Select(sst => sst.GetValueOrDefault());


            return dependServiceTypes.Select(dst => dst.SystemServiceType.GetValueOrDefault())
                .Union(childSystemServiceTypes).ToList();
        }

        /// <summary>
        /// Получить список всех услуг аренды
        /// </summary>
        /// <param name="rent1CActivationParams">Параметры активации Аренды 1С</param>
        /// <returns>Список всех услуг аренды</returns>
        private List<ResourceType> GetRent1CResourcesTypes(Rent1CActivationParamsDto rent1CActivationParams)
        {
            var rent1CBillingService =
                _dbLayer.BillingServiceRepository.FirstOrDefault(ser =>
                    ser.SystemService == Clouds42Service.MyEnterprise)
                ?? throw new NotFoundException(
                    $"Не найден системный сервис {_cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, rent1CActivationParams.AccountId)}");

            return rent1CBillingService.BillingServiceTypes.Where(st => st.SystemServiceType.HasValue).ToList()
                .Select(st => st.SystemServiceType.GetValueOrDefault()).ToList();
        }
    }
}
