﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;

namespace Clouds42.Accounts.CreateAccount.Providers
{
    /// <summary>
    /// Провайдер регистрации информационной базы.
    /// </summary>
    internal class RegisterAccountDatabaseProvider(
        ICreateAccountDatabaseProvider createAccountDatabaseProvider,
        IOnRegisterDatabaseAfterServiceActivationTrigger onRegisterDatabaseTrigger,
        CreateLogsAboutDatabaseCreationHelper createLogsHelper)
        : IRegisterAccountDatabaseProvider
    {
        /// <summary>
        /// Зарегистрировать базу по шаблону.
        /// </summary>
        /// <param name="accountUser">Пользователь.</param>
        /// <param name="dbTemplate">Шаблон.</param>
        /// <returns>ID базы, созданной по шаблону</returns>
        public Guid RegisterByTemplate(IAccountUser accountUser, DbTemplate dbTemplate)
        {
            var result = createAccountDatabaseProvider.Process([
                new InfoDatabaseDomainModelDto
                {
                    DataBaseName = dbTemplate.DefaultCaption,
                    TemplateId = dbTemplate.Id,
                    DbTemplateDelimiters = dbTemplate.GetDbTemplateDelimiters != null,
                    IsChecked = true,
                    UsersToGrantAccess = [accountUser.Id]
                }
            ], accountUser.AccountId);

            var accountDatabaseId = result.Ids.FirstOrDefault();
            onRegisterDatabaseTrigger.Execute(accountDatabaseId, accountUser);

            createLogsHelper.Create(accountUser.AccountId, result);
            return accountDatabaseId;
        }


    }
}
