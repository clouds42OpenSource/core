﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account;
using Clouds42.Logger;
using CommonLib.Enums;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Accounts.CreateAccount.Helpers;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Locales.Extensions;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.BillingServices.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.IDataModels;
using Clouds42.Resources.Contracts.Interfaces;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Accounts.CreateAccount.Providers
{
    /// <summary>
    /// Провайдер по активации сервиса для зарегистрированного аккаунта.
    /// </summary>
    public class ActivateServiceForRegistrationAccountProvider(
        IAccessProvider accessProvider,
        IResourcesService resourcesService,
        ICloud42ServiceFactory cloud42ServiceFactory,
        ActivateBillingServiceResourcesProvider activateBillingServiceResourcesProvider,
        IUnitOfWork dbLayer,
        IDemoResourceRegistrator demoResourceRegistrator,
        IBillingServiceInfoProvider billingServiceInfoProvider,
        ICheckAccountDataProvider checkAccountProvider,
        IAccountUserDataProvider accountUserDataProvider,
        Billing.Contracts.Billing.Interfaces.Providers.IBillingServiceDataProvider billingServiceDataProvider,
        IRent1CForCustomServiceActivator rent1CForCustomServiceActivator,
        ITardisBillingServiceProvider tardisBillingServiceProvider,
        IChangeTardisServiceSubscriptionSynchronizator changeTardisServiceSubscriptionSynchronizator,
        RegisterDatabaseAfterServiceActivationHelper registerDatabaseAfterServiceActivationHelper,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
        : IActivateServiceForRegistrationAccountProvider
    {
        /// <summary>
        ///  Попытаться активировать сервисы для нового аккаунта
        /// </summary>
        /// <param name="registrationConfig">Требуемые сервисы.</param>
        /// <param name="accountUser">Аккаунт админ созданного аккаунта.</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        public bool TryActivateServicesForNewAccount(RegistrationConfigDomainModelDto registrationConfig,
            IAccountUser accountUser, out string errorMessage)
        {
            errorMessage = null;

            try
            {
                resourcesService.GetAccountIfExistsOrCreateNew(accountUser.AccountId);
                logger.Info($"Создали аккаунт billing {accountUser.AccountId}");

                ActivateSystemServices(accountUser.AccountId, Clouds42Service.MyDisk, Clouds42Service.Esdl,
                    Clouds42Service.Recognition, Clouds42Service.MyDatabases);

                if (registrationConfig == null)
                {
                    return true;
                }

                if (registrationConfig.CloudService == Clouds42Service.MyEnterprise ||
                    !string.IsNullOrEmpty(registrationConfig.Configuration1C))
                {
                    logger.Info($"Включаем аренду 1С для аккаунта {accountUser.AccountId} без создания базы");

                    ActivateSystemService(accountUser.AccountId, Clouds42Service.MyEnterprise);
                }

                var isActivatedService =
                    TryActivateCustomService(registrationConfig, accountUser, out errorMessage);

                return isActivatedService;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка активации сервисов при регистрации нового аккаунта]");
                throw;
            }

        }

        /// <summary>
        /// Активировать сервис у существующего пользователя.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        /// <param name="rent1CActivationType">Тип активации Аренды 1С</param>
        public ServiceActivationResultDto ActivateServiceForExistingAccount(Guid serviceId, Guid accountUserId,
            Rent1CActivationType rent1CActivationType = Rent1CActivationType.ForExistingAccount)
        {
            return InternalActivateServiceForExistingAccount(serviceId, accountUserId, true, rent1CActivationType);
        }

        /// <summary>
        /// Активировать сервис у существующего пользователя, без его установки в базу.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        /// <param name="rent1CActivationType">Тип активации Аренды 1С</param>
        public ServiceActivationResultDto ActivateServiceForExistingAccountWithoutServiceInstallation(Guid serviceId, Guid accountUserId, 
            Rent1CActivationType rent1CActivationType = Rent1CActivationType.ForExistingAccount)
        {
            return InternalActivateServiceForExistingAccount(serviceId, accountUserId, false, rent1CActivationType);
        }

        /// <summary>
        /// Активировать сервис у существующего пользователя.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        /// <param name="installService"></param>
        /// <param name="rent1CActivationType">Тип активации Аренды 1С</param>
        private ServiceActivationResultDto InternalActivateServiceForExistingAccount(Guid serviceId, Guid accountUserId, bool installService,
            Rent1CActivationType rent1CActivationType = Rent1CActivationType.ForExistingAccount)
        {
                try
                {
                    var accountUser = accountUserDataProvider.GetAccountUserOrThrowException(accountUserId);

                    ValidateLocale(accountConfigurationDataProvider.GetAccountLocale(accountUser.AccountId),
                        serviceId);
                    CheckServiceActivity(serviceId, accountUser.AccountId);

                    var resConfig = GetResourceConfiguration(accountUser.AccountId, serviceId);

                    if (resConfig != null)
                    {
                        return GenerateServiceActivationResult(resConfig, true);
                    }

                    ActivateServiceForExistingAccount(serviceId, accountUser, rent1CActivationType);

                    if (installService)
                    {
                        registerDatabaseAfterServiceActivationHelper.RegisterDatabaseIfNeeded(
                            serviceId, accountUser);
                    }

                    return GenerateServiceActivationResult(
                        GetResourceConfigurationOrThrowException(accountUser.AccountId, serviceId));
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, "[Ошибка активации сервиса у существующего пользователя]");
                    throw;
                }
        }

        /// <summary>
        /// Активировать системный сервис
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceTypes">Список типов системного сервиса</param>
        private void ActivateSystemServices(Guid accountId, params Clouds42Service[] systemServiceTypes)
        {
            foreach (var systemServiceType in systemServiceTypes)
                ActivateSystemService(accountId, systemServiceType);
        }

        /// <summary>
        /// Активировать системный сервис
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системного сервиса</param>
        private void ActivateSystemService(Guid accountId, Clouds42Service systemServiceType)
        {
            logger.Info($"Активируем сервис {systemServiceType} для аккаунта {accountId}");

            var cloudService =
                cloud42ServiceFactory.Get(systemServiceType, accountId);
            cloudService.ActivateService();
        }

        /// <summary>
        /// Попытаться активировать кастомный сервис
        /// </summary>
        /// <param name="registrationConfig">Модель регистрации сервиса</param>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат активации сервиса</returns>
        private bool TryActivateCustomService(RegistrationConfigDomainModelDto registrationConfig,
            IAccountUser accountUser, out string errorMessage)
        {
            errorMessage = null;

            if (!registrationConfig.CloudServiceId.HasValue)
                return true;

            var accountLocale = accountConfigurationDataProvider.GetAccountLocale(accountUser.AccountId);
            ValidateLocale(accountLocale, registrationConfig.CloudServiceId.Value);
            CheckServiceActivity(registrationConfig.CloudServiceId.Value, accountUser.AccountId);

            var needActivateMainService = NeedActivateMainService(registrationConfig.CloudServiceId.Value,
                accountUser.AccountId);

            if (needActivateMainService is { MainServiceActivated: false, MainServiceId: not null })
            {
                logger.Info($"Включаем аренду 1С для аккаунта {accountUser.AccountId}");
                rent1CForCustomServiceActivator.ActivateRent1C(new Rent1CActivationParamsDto
                {
                    AccountId = accountUser.AccountId,
                    BillingServicesIdList = [registrationConfig.CloudServiceId.Value],
                    Rent1CActivationType = Rent1CActivationType.ForNewAccount
                });
            }

            logger.Info(
                $"Активируем сервис {registrationConfig.CloudServiceId} для аккаунта {accountUser.AccountId}");

            var service = dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == registrationConfig.CloudServiceId.Value);

            CreateServiceAndActivateDemo(registrationConfig.CloudServiceId.Value, accountUser, true, service.IsHybridService);

            return true;
        }

        /// <summary>
        /// Сформировать модель результата активации сервиса
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="isAlreadyActivated">Признак что сервис уже активирован</param>
        /// <returns>Результат активации сервиса</returns>
        private ServiceActivationResultDto GenerateServiceActivationResult(
            ResourcesConfiguration resourcesConfiguration, bool isAlreadyActivated = false) =>
            new()
            {
                ServiceName = resourcesConfiguration.GetLocalizedServiceName(cloudLocalizer),
                DemoPeriodEndDate = resourcesConfiguration.ExpireDateValue,
                IsAlreadyActivated = isAlreadyActivated
            };

        /// <summary>
        /// Провалидировать локаль аккаунта на предмет возможности активировать сервис.
        /// </summary>
        /// <param name="locale">Локаль аккаунта.</param>
        /// <param name="serviceId">Идентификатор сервиса.</param>
        private void ValidateLocale(ILocale locale, Guid serviceId)
        {
            var billingService = billingServiceDataProvider.GetBillingServiceOrThrowException(serviceId);

            if (locale.Name == LocaleConst.Ukraine)
                throw new ValidateException(
                    $"Использование сервиса “{billingService.Name}” доступно в конфигурациях 1С для Российской Федерации. " +
                    $"Для получения консультации о возможности использовать сервис “{billingService.Name}” обратитесь, пожалуйста, на круглосуточную линию технической поддержки.");
        }

        /// <summary>
        /// Проверить активность сервиса
        /// </summary>
        /// <param name="serviceId">Id севриса</param>
        /// <param name="accountId">Id аккаунта</param>
        private void CheckServiceActivity(Guid serviceId, Guid accountId)
        {
            var service = GetServiceOrThrowException(serviceId);

            if (!service.IsActive)
                throw new ValidateException(
                    GenerateMessageStatingThatServiceIsBlockedHelper.GenerateMessage(
                        service.GetNameBasedOnLocalization(cloudLocalizer, accountId)));
        }

        /// <summary>
        /// Активировать сервис у существующего пользователя.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUser">Пользователь.</param>
        /// <param name="rent1CActivationType">Тип активации Аренды 1С</param>
        private void ActivateServiceForExistingAccount(Guid serviceId, AccountUser accountUser,
            Rent1CActivationType rent1CActivationType)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            var needActivateMainService = NeedActivateMainService(serviceId, accountUser.AccountId);
            var service = dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId);

            if (needActivateMainService is { MainServiceActivated: false, MainServiceId: not null } && !service.IsHybridService)
            {
                logger.Info($"Включаем аренду 1С для аккаунта {accountUser.AccountId}");
                rent1CForCustomServiceActivator.ActivateRent1C(new Rent1CActivationParamsDto
                {
                    AccountId = accountUser.AccountId,
                    AccountUserId = accountUser.Id,
                    BillingServicesIdList = [serviceId],
                    Rent1CActivationType = rent1CActivationType
                });
            }

            var billingServiceType =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.ServiceId == serviceId);
            var serviceCost = dbLayer.RateRepository.FirstOrDefault(x => x.BillingServiceTypeId == billingServiceType.Id).Cost;

            CreateServiceAndActivateDemo(serviceId, accountUser, serviceCost is not 0, service.IsHybridService);

            var rent1CResConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                 rc.AccountId == accountUser.AccountId &&
                 rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            switch (rent1CResConfig)
            {
                case null when service.IsHybridService:
                    transaction.Commit();
                    return;
                case null:
                    transaction.Rollback();
                    throw new NotFoundException(
                        $"Не удалось получить запись ResourceConfiguration по аренде для аккаунта '{accountUser.AccountId}'");
            }

            if (rent1CResConfig.ExpireDateValue.Date > DateTime.Now.Date)
            {
                transaction.Commit();
                return;
            }

            var enterpriseService =
                (IMyEnterprise42CloudService)cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise,
                    accountUser.AccountId);

            enterpriseService.ManagedRent1C(new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(Clouds42SystemConstants.MyEnterpriseDemoPeriodDays)
            });

            RegisterRent1CDemoResources(accountUser.Account, rent1CResConfig);
            transaction.Commit();
        }

        /// <summary>
        /// Зарегистрировать демо ресурсы для сервиса Аренда 1С.
        /// </summary>
        /// <param name="account">Аккаунт.</param>
        /// <param name="rent1CResConfig">Ресуср конфигурация сервиса Аренды1С.</param>
        private void RegisterRent1CDemoResources(IAccount account, ResourcesConfiguration rent1CResConfig)
        {
            var freeResourceCount = CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod();

            var serviceTypesInfo = billingServiceInfoProvider
                .GetBillingServiceTypesInfo(rent1CResConfig.BillingServiceId, account.Id)
                .Where(st => st.BillingType == BillingTypeEnum.ForAccountUser);

            var accountRent1CResources = dbLayer.ResourceRepository.Where(w =>
                w.BillingServiceType.ServiceId == rent1CResConfig.BillingServiceId &&
                (w.AccountId == account.Id || w.AccountSponsorId == account.Id)).ToList();

            foreach (var serviceTypeInfo in serviceTypesInfo)
            {
                var serviceTypeDomain =
                    dbLayer.BillingServiceTypeRepository.FirstOrDefault(st => st.Id == serviceTypeInfo.Id) ??
                    throw new NotFoundException(
                        $"По номеру '{serviceTypeInfo.Id}' не удалось получить данные о услуге сервиса '{serviceTypeInfo.Name}'");

                var usedLicenses = accountRent1CResources
                    .Count(w => w.BillingServiceTypeId == serviceTypeInfo.Id);

                var resourceCount = Math.Max(0, freeResourceCount - usedLicenses);
                demoResourceRegistrator.Register(serviceTypeDomain, account, resourceCount);
            }
        }

        /// <summary>
        /// Создать сервис и подключить демо лицензии.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUser">Пользователь.</param>
        /// <param name="isDemoPeriod">Активировать как демо.</param>
        /// <param name="hybridService"></param>
        private void CreateServiceAndActivateDemo(Guid serviceId, IAccountUser accountUser, bool isDemoPeriod, bool hybridService)
        {
            activateBillingServiceResourcesProvider.CreateServiceWithDemoResources(serviceId, accountUser.AccountId,
                isDemoPeriod);

            if (hybridService || CanEnableServiceForAccountUser(serviceId, accountUser))
                EnableServiceForAccountUser(serviceId, accountUser);

            EnableServiceForAccount(serviceId, accountUser.AccountId);

            WriteLogMessageAboutActivateDemoService(serviceId, accountUser.AccountId);
        }

        /// <summary>
        /// Записать в лог активирование демо сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceId">ID сервиса</param>
        private void WriteLogMessageAboutActivateDemoService(Guid serviceId, Guid accountId)
        {
            var service = billingServiceDataProvider.GetBillingServiceOrThrowException(serviceId);
            var serviceName = service.GetNameBasedOnLocalization(cloudLocalizer, accountId);
            LogEventHelper.LogEvent(dbLayer, accountId, accessProvider,
                LogActions.ActivateServiceForExistingAccount,
                $"Активирован демо-период {(service.IsHybridService? "гибридного" : "")}сервиса \"{serviceName}\"");
        }

        /// <summary>
        /// Можно активировать сервис 
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUser">Пользователь аккаунта.</param>
        /// <returns>Признак возможности активировать сервис для пользователя.</returns>
        private bool CanEnableServiceForAccountUser(Guid serviceId, IAccountUser accountUser)
        {
            logger.Trace(
                $"Начинаем проверку возможности активации сервиса {serviceId} для пользователя {accountUser.Login}");
            if (checkAccountProvider.HasAccountSponsoredLicences(accountUser.AccountId))
                return false;

            var service = GetServiceOrThrowException(serviceId);

            var needStandart = service.BillingServiceTypes.Any(st =>
                st.DependServiceType is { SystemServiceType: ResourceType.MyEntUser });

            if (needStandart && !AccountUserHasRent1CResource(accountUser, ResourceType.MyEntUser))
            {
                logger.Trace(
                    $"Активация сервиса {service.Name} для пользователя {accountUser.Login} невозможна. Причина: необходимо активировать Аренду 1С Стандарт");
                return false;
            }

            var needWeb = service.BillingServiceTypes.Any(st =>
                st.DependServiceType is { SystemServiceType: ResourceType.MyEntUserWeb });

            if (needWeb && !AccountUserHasRent1CResource(accountUser, ResourceType.MyEntUserWeb))
            {
                logger.Trace(
                    $"Активация сервиса {service.Name} для пользователя {accountUser.Login} невозможна. Причина: необходимо активировать Аренду 1С WEB");
                return false;
            }

            logger.Trace($"Активация сервиса {service.Name} для пользователя {accountUser.Login} возможна.");
            return true;
        }

        /// <summary>
        /// у пользователя есть активное подключение аренды.
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта.</param>
        /// <param name="resourceType">Услуга аренды 1С.</param>
        /// <returns>Признак наличия активной услуги аренды 1С у пользователя.</returns>
        private bool AccountUserHasRent1CResource(IAccountUser accountUser, ResourceType resourceType)
            => dbLayer.ResourceRepository.Any(r =>
                r.AccountId == accountUser.AccountId && r.Subject == accountUser.Id &&
                r.BillingServiceType.SystemServiceType == resourceType);

        /// <summary>
        /// Включить сервис для пользователя.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUser">Пользователь.</param>
        private void EnableServiceForAccountUser(Guid serviceId, IAccountUser accountUser)
        {
            logger.Trace($"Начинаем процесс активации ресурсов сервиса {serviceId} для пользователя {accountUser.Login}");

            var resources = dbLayer.ResourceRepository.Where(r =>
                r.AccountId == accountUser.AccountId && r.BillingServiceType.ServiceId == serviceId).ToList();

            var serviceTypes = dbLayer.BillingServiceTypeRepository
                .Where(t => t.ServiceId == serviceId && t.BillingType == BillingTypeEnum.ForAccountUser).ToList();

            var resourcesForUpdate = new List<Resource>();
            foreach (var billingServiceType in serviceTypes)
            {
                if (resources.Any(r => r.Subject == accountUser.Id && r.BillingServiceTypeId == billingServiceType.Id))
                    continue;

                var freeResource = resources.FirstOrDefault(r =>
                    r.BillingServiceTypeId == billingServiceType.Id && r.Subject == null);

                if (freeResource == null)
                    continue;

                logger.Trace($"Свободный ресурс услуги сервиса {billingServiceType.Name} найден. Назначаем его для пользователя {accountUser.Login}");

                freeResource.Subject = accountUser.Id;
                resourcesForUpdate.Add(freeResource);
            }
           
            dbLayer.BulkUpdate(resourcesForUpdate);
            dbLayer.Save();

            logger.Trace($"Начинаем перерасчет стоимости сервиса {serviceId} для аккаунта {accountUser.AccountId}");
            recalculateResourcesConfigurationCostProvider.Recalculate(accountUser.AccountId, serviceId);

            tardisBillingServiceProvider.ManageAccessToTardisAccountDatabase(accountUser.Id,
                serviceId, UpdateAcDbAccessActionType.Grant);
            changeTardisServiceSubscriptionSynchronizator.SynchronizeWithTardis(accountUser.Id);
        }

        /// <summary>
        /// Включить сервис для аккаунта.
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса.</param>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        private void EnableServiceForAccount(Guid serviceId, Guid accountId)
        {
            logger.Trace($"Начинаем процесс активации ресурсов сервиса {serviceId} для аккаунта {accountId}");

            var resources = dbLayer.ResourceRepository
                .Where(r => r.AccountId == accountId &&  r.BillingServiceType.ServiceId == serviceId &&  r.Subject == null &&  r.BillingServiceType.BillingType == BillingTypeEnum.ForAccount)
                .ToList()
                .Select(x =>
                {
                    x.Subject = accountId;

                    return x;
                })
                .ToList();

            dbLayer.BulkUpdate(resources);
            dbLayer.Save();

            logger.Trace($"Начинаем перерасчет стоимости сервиса {serviceId} для аккаунта {accountId}");

            recalculateResourcesConfigurationCostProvider.Recalculate(accountId, serviceId);
        }

        /// <summary>
        /// Необходимо активировать основной сервис (Аренду 1С).
        /// </summary>
        /// <param name="serviceId">Номер сервиса</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns></returns>
        private NeedActivateResult NeedActivateMainService(Guid serviceId, Guid accountId)
        {
            var mainService = GetMainService(serviceId);

            if (mainService == null)
                return new NeedActivateResult(false);

            var resConfig = GetResourceConfiguration(accountId, mainService.Id);
            var mainServiceActivated = resConfig != null;

            return new NeedActivateResult(mainServiceActivated, mainService.Id);
        }

        /// <summary>
        /// Получить запись о ресурс конфигурации сервиса у аккаунта.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <returns></returns>
        private ResourcesConfiguration GetResourceConfiguration(Guid accountId, Guid serviceId)
            => dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingServiceId == serviceId);

        /// <summary>
        /// Получить запись о ресурс конфигурации сервиса у аккаунта.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <returns></returns>
        private ResourcesConfiguration GetResourceConfigurationOrThrowException(Guid accountId, Guid serviceId)
            => dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                   rc.AccountId == accountId && rc.BillingServiceId == serviceId) ?? throw new NotFoundException(
                   $"Не удалось получить конфигурацию ресурса для сервиса '{serviceId}' и аккаунта '{accountId}'");

        /// <summary>
        /// Получить данные по основному сервису.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <returns></returns>
        private IBillingService GetMainService(Guid serviceId)
            => dbLayer.BillingServiceTypeRepository.AsQueryable().Include(x => x.DependServiceType).ThenInclude(x => x.Service).FirstOrDefault(t => t.ServiceId == serviceId && t.DependServiceTypeId.HasValue)?.DependServiceType.Service;

        /// <summary>
        /// Получить данные по основному сервису.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <returns></returns>
        private BillingService GetServiceOrThrowException(Guid serviceId)
            => dbLayer.BillingServiceRepository
                   .FirstOrDefault(t => t.Id == serviceId && t.IsActive) ??
               throw new NotFoundException($"По номеру '{serviceId}' не удалось получить сервис облака.");

        /// <summary>
        /// Структура указывающая о необходимости активации основного сервиса.
        /// </summary>
        private class NeedActivateResult(bool mainServiceActivated, Guid? mainServiceId = null)
        {
            /// <summary>
            /// Основной сервис активирован.
            /// </summary>
            public bool MainServiceActivated { get; } = mainServiceActivated;

            /// <summary>
            /// Номер основного сервиса.
            /// </summary>
            public Guid? MainServiceId { get; } = mainServiceId;
        }
    }
}
