﻿using System.Text;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces;
using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Accounts.CreateAccount.Constants;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.AlphaNumericsSupport;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Constants;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Segment.Contracts.CloudServicesFileStorageServer.Interfaces;
using Clouds42.Validators.Account.Validators;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using IAccountUserProfileHelper = Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers.IAccountUserProfileHelper;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;
using AccountUser = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.Accounts.CreateAccount.Providers
{
    /// <summary>
    /// Провайдер для создании аккаунта
    /// </summary>
    internal class CreateAccountProvider(
        IUnitOfWork dbLayer,
        IOpenIdProvider openIdProvider,
        DesEncryptionProvider desEncryptionProvider,
        IActivateServiceForRegistrationAccountProvider activateServiceForRegistrationAccountProvider,
        IRegisterAccountDatabaseProvider registerAccountDatabaseProvider,
        AccountRegistrationModelAdapter accountRegistrationModelAdapter,
        IHandlerException handlerException,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        IAccountUserProfileHelper accountUserProfileHelper,
        AccountUserDataValidator accountUserDataValidator,
        IValidateLoginInActiveDirectoryProvider validateLoginInActiveDirectoryProvider,
        ICloudFileStorageServerProvider cloudFileStorageServerProvider,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        AesEncryptionProvider aesEncryptionProvider,
        ICloud42ServiceFactory cloud42ServiceFactory,
        ICreateMyDiskPropertiesByAccountProvider createMyDiskPropertiesByAccountProvider,
        ICreateAccountRequisitesProvider createAccountRequisitesProvider,
        ICreateAccountConfigurationProvider createAccountConfigurationProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        IConfiguration configuration,
        IAccountUserRegistrationNotificationProvider accountUserRegistrationNotificationProvider,
        IAccountFolderHelper accountFolderHelper)
        : ICreateAccountProvider
    {
        private readonly string _localeRuRu = "ru-ru";
        private const int RegistrationReward = 200; // todo(0) als: перенести константу в настройки конфигурации

        /// <inheritdoc/>
        public async Task<AccountAuthDto> AddAccountAndCreateSession(AccountRegistrationModelDto accountInfo)
        {
            var addResult = await CreateAccount(accountInfo);
            var token = CreateSession(addResult.Account.Id,
                accountInfo.RegistrationConfig?.RegistrationSource ?? ExternalClient.Promo);
            return new AccountAuthDto(addResult.Account.Id, token);
        }

        /// <inheritdoc/>
        public async Task<AccountAuthDto> AddAccountWithoutServicesAndCreateSession(
            AccountRegistrationModelDto accountInfo)
        {
            await ValidateRegistrationModel(accountInfo);

            FillEmptyPassword(accountInfo);
            Guid? accountUserId = null;
            string login = string.Empty;
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                CreateAccountAndAccountAdmin(accountInfo, out var account, out var accountUserAdmin);
                accountUserId = accountUserAdmin.Id;
                login = accountUserAdmin.Login;

                var accountWithSegment = await dbLayer.AccountsRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.AccountConfiguration)
                    .ThenInclude(x => x.Segment)
                    .ThenInclude(x => x.CloudServicesFileStorageServer)
                    .FirstOrDefaultAsync(x => x.Id == account.Id);

                var clientFilesPath =
                    accountFolderHelper.GetAccountClientFileStoragePath(
                        accountWithSegment.AccountConfiguration.Segment, account);

                AddAccountToAd(accountUserAdmin, account, accountInfo.Password, clientFilesPath);
                   
                CreateUserInOpenIdProvider(accountInfo, accountUserAdmin, account);

                SendClientNotificationViaNewThread(accountInfo, accountUserAdmin);
                   
                var authToken = CreateSession(account.Id, accountInfo.RegistrationConfig?.RegistrationSource ?? ExternalClient.Promo);
                dbScope.Commit();

                return new AccountAuthDto(account.Id, authToken);
            }
            catch (Exception ex)
            {
                logger.Warn($"При регистрации аккаунта произошла ошибка {ex.GetFullInfo()}");

                try
                {
                    if (accountUserId.HasValue)
                    {
                        openIdProvider.DeleteUser(
                            new SauriOpenIdControlUserModel { AccountUserID = accountUserId.Value });
                        activeDirectoryTaskProcessor.RunTask(provider =>
                            provider.DeleteUser(accountUserId.Value, login));
                    }
                }

                catch (Exception e)
                {
                    handlerException.Handle(e, $"[Ошибка отката пользователя {login} в AD или OpenId при регистрации]");

                    dbScope.Rollback();

                    throw;
                }

                dbScope.Rollback();

                throw;
            }
        }

        /// <inheritdoc/>
        public async Task<AccountRegistrationResultDto> CreateAccount(AccountRegistrationModelDto accountInfo)
        { 
            await ValidateRegistrationModel(accountInfo);
            accountRegistrationModelAdapter.ApplyMappingForRegistrationConfig(accountInfo);
            FillEmptyPassword(accountInfo);
            Guid? accountUserId = null;
            string login = string.Empty;
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                CreateAccountAndAccountAdmin(accountInfo, out var account, out var accountAdmin);
                accountUserId = accountAdmin.Id;
                login = accountAdmin.Login;
                var accountWithSegment = await dbLayer.AccountsRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.AccountConfiguration.Segment)
                    .Include(x => x.AccountConfiguration.Segment.CloudServicesFileStorageServer)
                    .FirstOrDefaultAsync(x => x.Id == account.Id);

                var clientFilesFolder =
                    accountFolderHelper.GetAccountClientFileStoragePath(
                        accountWithSegment.AccountConfiguration.Segment, accountWithSegment);

                AddAccountToAd(accountAdmin, account, accountInfo.Password, clientFilesFolder);

                if(!accountInfo.SkipOpenIdCreating)
                     CreateUserInOpenIdProvider(accountInfo, accountAdmin, account);

                if (!activateServiceForRegistrationAccountProvider.TryActivateServicesForNewAccount(
                        accountInfo.RegistrationConfig, accountAdmin, out var serviceActivationErrorMessage))
                    logger.Trace(
                        $"Не удалось активировать сервис при регистрации аккаунта '{account.Id}'. Причина: {serviceActivationErrorMessage}");

                PerformReferralAccountIdLink(accountInfo, accountAdmin);
                PerformConfiguration1CLink(accountInfo, accountAdmin);
                if (accountInfo.RegistrationConfig == null || (accountInfo.RegistrationConfig != null
                    && accountInfo.RegistrationConfig.RegistrationSource != ExternalClient.Cloud && accountInfo.UserSource != "AlpacaMeet"))
                    SendClientNotificationViaNewThread(accountInfo, accountAdmin);

                if (accountInfo?.UserSource?.Contains(':') ?? false)
                {
                    var splitArray = accountInfo.UserSource.Split(":");
                    if (splitArray.Length > 1)
                    {
                        var articleNumber = splitArray[1];
                        var _ = int.TryParse(articleNumber, out var value);

                        var article = await dbLayer.ArticleRepository.AsQueryable()
                            .FirstOrDefaultAsync(x => x.WpId == articleNumber || x.Id == value);

                        if (article != null)
                        {
                            article.RegistrationCount++;
                            article.RewardAmount += RegistrationReward;
                            ArticleTransaction(article);
                            logger.Debug($"Начислено вознаграждение за регистрацию пользователя {accountAdmin.Login} со статьи номер {article.Id} заголовок статьи {article.Title} на сумму {RegistrationReward}");
                            dbLayer.ArticleRepository.Update(article);
                            await dbLayer.SaveAsync();
                        }

                    }
                }
                dbScope.Commit();

                return new AccountRegistrationResultDto
                {
                    Account = account,
                    AccountUserLogin = accountAdmin.Login,
                    AccountUserPassword = accountInfo.Password,
                    ServiceActivationErrorMessage = serviceActivationErrorMessage,
                    AccountUserId = accountUserId
                };
            }
            catch (Exception ex)
            {
                logger.Warn($"При регистрации аккаунта произошла ошибка {ex.GetFullInfo()}");

                try
                {
                    if (accountUserId.HasValue)
                    {
                        if(!accountInfo.SkipOpenIdCreating)
                             openIdProvider.DeleteUser(
                                     new SauriOpenIdControlUserModel { AccountUserID = accountUserId.Value });

                        activeDirectoryTaskProcessor.RunTask(provider =>
                            provider.DeleteUser(accountUserId.Value, login));
                    }
                }

                catch (Exception e)
                {
                    handlerException.Handle(e, $"[Ошибка отката пользователя {login} в AD или OpenId при регистрации]");

                    dbScope.Rollback();

                    throw;
                }

                dbScope.Rollback();

                throw;
            }
        }

        /// <summary>
        /// Начисление вознаграждения за регистрацию клиента со статьи
        /// </summary>
        /// <param name="article"></param>
        public void ArticleTransaction(Article article)
        {

            if (article.AccountUser.CorpUserSyncStatus == "Deleted" || article.AccountUser.Removed == true)
            {
                logger.Debug($"Вознаграждения за регистрацию по статье {article.Id} не начислено, так как пользователь {article.AccountUser.Id} удален");
                return;
            }

            logger.Debug($"Начисление вознаграждения за регистрацию по статье {article.Id} автору {article.AccountUser.Name}");

            var articleTransaction = new ArticleTransaction
            {
                Id = Guid.NewGuid(),
                ArticleId = article.Id,
                Cause = ArticleTransactionCause.UserRegistration,
                TransactionType = ArticleTransactionType.Inflow,
                Amount = RegistrationReward,
                CreatedOn = DateTime.Now
            };
            dbLayer.ArticleTransactionRepository.Insert(articleTransaction);

            var articleWallets = dbLayer.ArticleWalletsRepository
            .AsQueryable()
            .FirstOrDefault(x => x.AccountUserId == article.AccountUserId);

            articleWallets.AvailableSum = articleWallets.AvailableSum + articleTransaction.Amount;
            articleWallets.TotalEarned = articleWallets.TotalEarned + articleTransaction.Amount;
            dbLayer.ArticleWalletsRepository.Update(articleWallets);

        }

        /// <summary>
        /// Обработать регистрацию информационной базы по ссылке.
        /// </summary>
        /// <param name="accountInfo">Данные регистрации.</param>
        /// <param name="accountAdmin">Объект созданного пользователя.</param>
        private void PerformConfiguration1CLink(AccountRegistrationModelDto accountInfo, AccountUser accountAdmin)
        {
            if (string.IsNullOrEmpty(accountInfo.RegistrationConfig?.Configuration1C))
                return;

            CreateAccountDatabaseByConfiguration1C(accountAdmin, accountInfo.RegistrationConfig.Configuration1C);
        }

        /// <summary>
        /// Обработать регистрацию по рефферальной ссылке.
        /// </summary>
        /// <param name="accountInfo">Данные регистрации.</param>
        /// <param name="accountAdmin">Объект созданного пользователя.</param>
        private void PerformReferralAccountIdLink(AccountRegistrationModelDto accountInfo, AccountUser accountAdmin)
        {
            if (accountInfo.RegistrationConfig?.ReferralAccountId == null ||
                accountInfo.RegistrationConfig.CloudServiceId != null || accountInfo.LocaleName != LocaleConst.Russia)
                return;

            ActivateRent1CForAccountAdmin(accountAdmin);
        }

        /// <summary>
        /// Заполнить гененрированным значением поле пароля.
        /// </summary>
        /// <param name="accountInfo">Данные регистраци.</param>
        private static void FillEmptyPassword(AccountRegistrationModelDto accountInfo)
        {
            if (!string.IsNullOrEmpty(accountInfo.Password)) return;
            var alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
            accountInfo.Password = alphaNumeric42CloudsGenerator.GeneratePassword();
            accountInfo.PasswordWasGenerated = true;
        }

        /// <summary>
        /// Провести валидацию данных регистрации.
        /// </summary>
        /// <param name="accountInfo">Данные регистрации.</param>
        private async Task ValidateRegistrationModel(AccountRegistrationModelDto accountInfo)
        {
            if (accountInfo == null)
                throw new PreconditionException("Форма добавления пользователя пустая");

            var validationResult = await accountUserDataValidator.ValidateAccountData(accountInfo);
            if (validationResult.Any())
                throw new ValidateException(string.Join(" ", validationResult));

            validateLoginInActiveDirectoryProvider.Validate(accountInfo.Login);
        }

        /// <summary>
        /// Зарегистрировать пользователя в OpenIdProvider.
        /// </summary>
        /// <param name="accountInfo">Данные регистрации.</param>
        /// <param name="accountAdmin">Объект созданного пользователя.</param>
        /// <param name="account">объект созданного аккаунта.</param>
        private void CreateUserInOpenIdProvider(AccountRegistrationModelDto accountInfo, AccountUser accountAdmin,
            Domain.DataModels.Account account)
        {
            logger.Info($"Добавляем пользователя '{accountAdmin.Login}' в провайдер {account.Id}");
            var locale = GetLocaleFrom(accountAdmin.Account);
            openIdProvider.AddUser(new SauriOpenIdControlUserModel
            {
                Name = string.IsNullOrWhiteSpace(accountAdmin.Name) ? accountAdmin.Login : accountAdmin.Name,
                AccountID = account.Id,
                Login = accountAdmin.Login,
                Password = accountInfo.Password,
                AccountUserID = accountAdmin.Id,
                Email = accountInfo.Email,
                Phone = accountInfo.FullPhoneNumber,
                FullName = accountAdmin.FullName
            }, locale);
            logger.Trace("Пользователь {0} Id={1} успешно зарегистрирован в провайдере OpenID",
                accountAdmin.Login,
                accountAdmin.Id);
        }

        /// <summary>
        /// Отправить клиенту уведомления о регистрации на облаке.
        /// </summary>
        /// <param name="accountInfo">Данные регистрации.</param>
        /// <param name="accountAdmin">Объект созданного пользователя.</param>
        private void SendClientNotificationViaNewThread(IRegistrationSourceInfoAdapter accountInfo, AccountUser accountAdmin)
        {
            accountUserRegistrationNotificationProvider.SendAccountCreateLetter(accountAdmin, accountInfo, accountInfo.Password);
        }

        /// <summary>
        /// Получить локаль аккаунта
        /// </summary>
        /// <returns>Локаль пользователя</returns>
        private string GetLocaleFrom(Domain.DataModels.Account account)
        {
            if (account.AccountConfiguration == null ||
               string.IsNullOrEmpty(account.AccountConfiguration.Locale?.Name))
            {
                return _localeRuRu;
            }
            return account.AccountConfiguration.Locale.Name;
        }

        /// <summary>
        /// Активировать Аренду 1С для администратора аккаунта
        /// </summary>
        /// <param name="accountAdmin">Администратор аккаунта.</param>
        private void ActivateRent1CForAccountAdmin(AccountUser accountAdmin)
        {
            var cloudService = (IMyEnterprise42CloudService)
                cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, accountAdmin.AccountId);
            cloudService.ActivateService(accountAdmin.Id);
        }

        /// <summary>
        ///  Создать информационную базу по шаблну при регистрации нового пользователя.
        /// </summary>
        private void CreateAccountDatabaseByConfiguration1C(IAccountUser accountUser, string templateName)
        {
            var dbTemplate = dbLayer.DbTemplateRepository.FirstOrDefault(t => t.Name == templateName);
            if (dbTemplate != null)
            {
                registerAccountDatabaseProvider.RegisterByTemplate(accountUser, dbTemplate);
            }
            else
            {
                handlerException.HandleWarning($"Шаблон: '{templateName}' не найден",
                    "Создания ИБ по шаблону при регистрации.");
                logger.Warn(
                    $"При регистрации аккаунта не удалось создать базу по шаблону {templateName}. По причине шаблон не найден");
            }
        }

        /// <summary>
        /// Создать записи аккаунта и аккаунт админа в базе.
        /// </summary>
        /// <param name="regInfo">Данные для регистрации.</param>
        /// <param name="account">Созданная запись аккаунта.</param>
        /// <param name="accountAdmin">Созданная записаь аккаунт админа.</param>
        private void CreateAccountAndAccountAdmin(
            AccountRegistrationModelDto regInfo,
            out Domain.DataModels.Account account,
            out AccountUser accountAdmin)
        {
            logger.Trace("is firstName null {0} is empty  {1}", regInfo.FirstName == null,
                string.IsNullOrEmpty(regInfo.FirstName));

            var accountId = Guid.NewGuid();
            accountAdmin = CreateAccountAdminObject(regInfo, accountId);
            logger.Trace("Manager info: " + accountAdmin);

            account = CreateAccountObject(regInfo, accountId);
            dbLayer.AccountsRepository.InsertAccount(account);

            if (string.IsNullOrEmpty(accountAdmin.Login))
                accountAdmin.Login = account.IndexNumber + "_user_1";

            dbLayer.AccountUsersRepository.InsertAccountUser(accountAdmin);

            if (string.IsNullOrEmpty(account.AccountCaption))
                account.AccountCaption = account.IndexNumber.ToString();

            dbLayer.AccountsRepository.Update(account);

            dbLayer.AccountUserRoleRepository.Insert(new AccountUserRole
            {
                AccountUserGroup = AccountUserGroup.AccountAdmin,
                Id = Guid.NewGuid(),
                AccountUserId = accountAdmin.Id
            });

            CreateAccountWallet(accountId);
            CreateAccountConfiguration(regInfo, accountId);

            createMyDiskPropertiesByAccountProvider.Create(new MyDiskPropertiesByAccountDto
            {
                AccountId = accountId
            });

            var notificationSettings = dbLayer.NotificationSettingsRepository
                .AsQueryableNoTracking()
                .Where(x => x.IsActive)
                .ToList();

            var accountAdminId = accountAdmin.Id;

            var accountUserNotificationSettings = notificationSettings.Select(x => new AccountUserNotificationSettings
            {
                AccountUserId = accountAdminId,
                IsActive = (x.Type == NotificationType.Email && regInfo.IsEmailVerified) 
                || (x.Type == NotificationType.Telegram && regInfo.TelegramChatId.HasValue) 
                || (x.Type == NotificationType.Sms && regInfo is { IsEmailVerified: false, TelegramChatId: null }),
                Code = CreateCode(x.Type, accountAdminId),
                NotificationSettingsId = x.Id,
                ChatId = x.Type == NotificationType.Telegram && regInfo.TelegramChatId.HasValue 
                    ? regInfo.TelegramChatId
                    : null,
            }).ToList();

            dbLayer.AccountUserNotificationSettingsRepository.InsertRange(accountUserNotificationSettings);

            createAccountRequisitesProvider.Create(new AccountRequisitesDto
            {
                AccountId = accountId,
                Inn = regInfo.Inn
            });

            dbLayer.Save();
        }
        
        /// <summary>
        /// Создать код уведомления для заданного типа уведомления и идентификатора.
        /// </summary>
        /// <param name="type">Тип уведомления (Telegram, WhatsApp и т.д.).</param>
        /// <param name="id">Идентификатор уведомления (GUID).</param>
        /// <returns>Код уведомления в зависимости от типа: для Telegram это строка в формате Base64, для WhatsApp — строка, начинающаяся с "/start", для остальных типов — null.</returns>
        private static string? CreateCode(NotificationType type, Guid id) => type switch
        {
            NotificationType.Telegram => Convert.ToBase64String(Encoding.UTF8.GetBytes(id.ToString())),
            NotificationType.WhatsApp => "/start" + $" {id.ToString()}",
            _ => null
        };

        /// <summary>
        /// Создать объект аккаунта.
        /// </summary>
        /// <param name="regInfo">Данные регистрации.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>Новая сущность аккаунта</returns>
        private Domain.DataModels.Account CreateAccountObject(AccountRegistrationModelDto regInfo, Guid accountId) =>
            new()
            {
                Id = accountId,
                AccountCaption = regInfo.AccountCaption ?? PhoneHelper.ClearNonDigits(regInfo.FullPhoneNumber),
                ReferralAccountID = regInfo.RegistrationConfig?.ReferralAccountId,
                RegistrationDate = DateTime.Now,
                UserSource = regInfo.UserSource
            };

        /// <summary>
        /// Создать конфигурацию аккаунта
        /// </summary>
        /// <param name="registrationInfo">Данные о регистрации аккаунта</param>
        /// <param name="accountId">Id аккаунта</param>
        private void CreateAccountConfiguration(AccountRegistrationModelDto registrationInfo, Guid accountId)
        {
            var localeId = GetLocaleId(registrationInfo);
            createAccountConfigurationProvider.Create(new AccountConfigurationDto
            {
                SegmentId = localesConfigurationDataProvider.GetDefaultSegmentId(localeId),
                AccountId = accountId,
                FileStorageId = cloudFileStorageServerProvider.GetDefaultFileStorageId(localeId),
                Type = GetAccountType(registrationInfo),
                SupplierId = DefineSupplierId(registrationInfo, localeId),
                LocaleId = localeId
            });
        }

        /// <summary>
        /// Определить поставщика локали
        /// </summary>
        /// <param name="registrationInfo">Данные о регистрации аккаунта</param>
        /// <param name="localeId">Id локали</param>
        /// <returns>Id поставщика локали</returns>
        private Guid DefineSupplierId(AccountRegistrationModelDto registrationInfo, Guid localeId)
        {
            if(registrationInfo.RegistrationConfig?.CloudService == Clouds42Service.Esdl)
            {
                var supplier = dbLayer.SupplierRepository.FirstOrDefault(w =>
                w.Name.Contains("УчетОнлайн"));

                return supplier?.Id ?? GetDefaultSupplierIdByLocale(localeId);
            }
               

            if (registrationInfo.RegistrationConfig?.ReferralAccountId == null)
                return GetDefaultSupplierIdByLocale(localeId);

            var supplierReferralAccount = dbLayer.SupplierReferralAccountRepository.FirstOrDefault(w =>
                w.ReferralAccountId == registrationInfo.RegistrationConfig.ReferralAccountId);

            return supplierReferralAccount?.SupplierId ?? GetDefaultSupplierIdByLocale(localeId);
        }

        /// <summary>
        /// Получить тип аккаунта
        /// </summary>
        /// <param name="registrationInfo">Данные о регистрации аккаунта</param>
        /// <returns>Тип аккаунта</returns>
        private string GetAccountType(AccountRegistrationModelDto registrationInfo)
        {
            if (registrationInfo.RegistrationConfig == null ||
                registrationInfo.RegistrationConfig.RegistrationSource == ExternalClient.Promo)
                return CreateAccountConst.DefaultType;

            if (registrationInfo.RegistrationConfig == null ||
                registrationInfo.RegistrationConfig.RegistrationSource == ExternalClient.Cloud)
                return "Cloud.ru";

            return registrationInfo.RegistrationConfig.RegistrationSource == ExternalClient.Market
                ? $"42Clouds({ExternalClient.Market})"
                : registrationInfo.RegistrationConfig.RegistrationSource.ToString();
        }

        /// <summary>
        /// Получить Id локали
        /// </summary>
        /// <param name="regInfo">Данные регистрации.</param>
        /// <returns>идентификатор локали</returns>
        private Guid GetLocaleId(AccountRegistrationModelDto regInfo)
        {
            var defaultLocale = GetDefaultLocale();
            var localeId = dbLayer.LocaleRepository.FirstOrDefault(x => x.Name == regInfo.LocaleName)?.ID;
            var registrationSource = new[] { ExternalClient.Sauri, ExternalClient.Kladovoy, ExternalClient.Delans };

            if (regInfo.RegistrationConfig?.ReferralAccountId != null)
                return accountConfigurationDataProvider.GetAccountLocaleId(regInfo.RegistrationConfig.ReferralAccountId
                    .Value);

            if (localeId == null || regInfo.RegistrationConfig?.RegistrationSource != null &&
                                     registrationSource.Any(x => x == regInfo.RegistrationConfig?.RegistrationSource))
                return defaultLocale;

            return localeId.Value;
        }

        /// <summary>
        /// Получить дефолтную локаль для аккаунта
        /// </summary>
        /// <returns>Дефолтная локаль для аккаунта</returns>
        private Guid GetDefaultLocale() => Guid.Parse(configuration["DefaultLocale"]);

        /// <summary>
        /// Создать кошелек для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void CreateAccountWallet(Guid accountId)
        {
            var accountWallet = new AgentWallet
            {
                AccountOwnerId = accountId,
                AvailableSum = 0
            };

            dbLayer.AgentWalletRepository.Insert(accountWallet);
        }

        /// <summary>
        /// Создать объект пользователя.
        /// </summary>
        /// <param name="regInfo">Данные регистрации.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns></returns>
        private AccountUser CreateAccountAdminObject(AccountRegistrationModelDto regInfo, Guid accountId)
        {
            var accountAdmin = new AccountUser
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Login = regInfo.Login ?? string.Empty,
                Password = desEncryptionProvider.Encrypt(regInfo.Password),
                PasswordHash = aesEncryptionProvider.Encrypt(regInfo.Password),
                Email = regInfo.Email,
                EmailStatus = regInfo.IsEmailVerified ? "Checked" : "Unchecked",
                FirstName = regInfo.FirstName?.Trim(),
                MiddleName = regInfo.MiddleName?.Trim(),
                LastName = regInfo.LastName?.Trim(),
                PhoneNumber = PhoneHelper.ClearNonDigits(regInfo.FullPhoneNumber),
                CreatedInAd = true,
                Activated = true,
                CorpUserSyncStatus = "Added",
                CreationDate = DateTime.Now,
                EditDate = DateTime.Now,
                ResetCode = Guid.NewGuid(),
                IsPhoneVerified = regInfo.PhoneNumberVerified

                
            };
            accountAdmin.FullName = $"{accountAdmin.FirstName} {accountAdmin.MiddleName} {accountAdmin.LastName}";

            return accountAdmin;
        }

        /// <summary>
        /// Создать запись сессии.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="client">Тип клиента</param>
        /// <returns>Номер созданной сессии.</returns>
        private Guid CreateSession(Guid accountId, ExternalClient client)
        {
            var newToken = Guid.NewGuid();
            var user = dbLayer.AccountUsersRepository.FirstOrDefault(x => x.AccountId == accountId);
            dbLayer.AccountUserSessionsRepository.Insert(new AccountUserSession
            {
                Id = Guid.NewGuid(),
                AccountUserId = user.Id,
                StaticToken = false,
                Token = newToken,
                TokenCreationTime = DateTime.Now,
                ClientDescription = client.ToString()
            });

            dbLayer.Save();

            return newToken;
        }

        /// <summary>
        /// Создаем пользователя в AD
        /// </summary>
        /// <param name="accountUser">Пользователь облока.</param>
        /// <param name="userPassword">Пароль пользователя.</param>
        /// <param name="account">Аккаунт.</param>
        /// <param name="clientFilesPath">Путь до клиентских файлов.</param>
        private void AddAccountToAd(AccountUser accountUser, Domain.DataModels.Account account, string userPassword, string clientFilesPath)
        {
            logger.Trace($":: Начинаем процедуру создания пользователя {accountUser.Login} и добавления его в группы");

            var createAdModel = new CreateAccountInAdModelDto
            {
                NewLogin = accountUser.Login,
                Password = userPassword,
                FirstName = accountUser.FirstName,
                MiddleName = accountUser.MiddleName,
                LastName = accountUser.LastName,
                AccountId = account.Id,
                AccountUserId = accountUser.Id,
                PhoneNumber = accountUser.PhoneNumber,
                Email = accountUser.Email,
                ClientFilesPath = clientFilesPath
            };

            accountUserProfileHelper.RunTaskCreate1CStartFolder(accountUser.Login);

            activeDirectoryTaskProcessor.RunTask(provider => provider.RegisterNewAccount(createAdModel));
        }

        /// <summary>
        /// Получить поставщика по умолчанию для локали
        /// </summary>
        /// <param name="localeId"></param>
        /// <returns></returns>
        private Guid GetDefaultSupplierIdByLocale(Guid localeId)
        {
            var supplier = dbLayer.SupplierRepository.FirstOrDefault(w => w.IsDefault && w.LocaleId == localeId) ??
                           throw new NotFoundException($"Для локали {localeId} не найден поставщик!");
            return supplier.Id;
        }
    }
}
