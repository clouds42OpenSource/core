﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Accounts.CheckAccountData.Providers
{
    /// <summary>
    /// Провайдер для выполнения проверок над данными аккаунта
    /// </summary>
    public class CheckAccountDataProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ICloudLocalizer cloudLocalizer,
        ILogger42 logger)
        : ICheckAccountDataProvider
    {
        private readonly Lazy<int> _maxCountAccountDbForDemoAccount = new(CloudConfigurationProvider.AccountDatabase.Support.MaxCountAccountDbForDemoAccount);

        /// <summary>
        /// Проверить по количеству возможность создать базу для аккаунта
        /// </summary>
        /// <param name="account">Id аккаунта</param>
        /// <param name="databasesCount">Количество баз</param>
        public CreateCloud42ServiceModelDto CheckAbilityToCreateDatabasesByLimit(Domain.DataModels.Account account,
            int databasesCount)
        {
            logger.Info(
               $"[{account.Id}]: Проверить по количеству возможность создать базу для аккаунта  {databasesCount}");
            var currentUser = accessProvider.GetUser();

            if (currentUser != null && (currentUser.Groups.Contains(AccountUserGroup.CloudAdmin) ||
            currentUser.Groups.Contains(AccountUserGroup.Hotline) ||
            currentUser.Groups.Contains(AccountUserGroup.AccountSaleManager)))
                return new CreateCloud42ServiceModelDto { Complete = true };

            var accountIsDemo = CheckAccountIsDemo(account);
            var maxCountAccountDbForDemoAccount = _maxCountAccountDbForDemoAccount.Value;
            logger.Info( $"[{account.Id}]: ПРОВЕРИТЬ ЭТО АККАУНТ ДЕМО {accountIsDemo}");
            if (!accountIsDemo)
                return new CreateCloud42ServiceModelDto { Complete = true };

            var countOfCurrentBasesInAccount = dbLayer.DatabasesRepository
                .Where(w => w.AccountId == account.Id && w.State == "Ready").Count();
            logger.Info($"[{account.Id}]: Проверить сколько сейчас  баз у аккаунта {countOfCurrentBasesInAccount} плюс сколько надо создать {databasesCount} > максимум {maxCountAccountDbForDemoAccount}");
            if (countOfCurrentBasesInAccount + databasesCount <= maxCountAccountDbForDemoAccount)
            {
                return new CreateCloud42ServiceModelDto { Complete = true };
            }

            var rent1CName = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, account.Id);

            var logMessage = databasesCount > 1
                ? $"Создание более 10 информационных баз будет доступно после оплаты сервиса {rent1CName}"
                : $"Создание информационной базы будет доступно после оплаты сервиса {rent1CName}";

            return new CreateCloud42ServiceModelDto
            {
                Comment = logMessage,
                Complete = false,
                ResourceForRedirect = ResourceType.CountOfDatabasesOverLimit
            };

        }

        /// <summary>
        ///     Проверить что аккаунт спонсируется
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>true - если аккаунт спонсируется</returns>
        public bool HasAccountSponsoredLicences(Domain.DataModels.Account account)
            => HasRent1CSponsoredLicenses(account);

        /// <summary>
        ///     Проверить что аккаунт спонсируется
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>true - если аккаунт спонсируется</returns>
        public bool HasAccountSponsoredLicences(Guid accountId)
        {
            var account = dbLayer.AccountsRepository.GetAccount(accountId);
            return HasRent1CSponsoredLicenses(account);
        }

        /// <summary>
        /// Проверить что аккаунт относится к демо.
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>признак доступности</returns>
        public bool CheckAccountIsDemo(IAccount account)
        {
            var accountHasInflowPayments = AccountHasInflowPayments(account.Id);

            if (!accountHasInflowPayments && !HasActiveSponsoredLicenses(account))
                return true;

            return false;
        }

        /// <summary>
        /// Проверить что аккаунт относится к демо.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Признак демо у аккаунта</returns>
        public bool CheckAccountIsDemo(Guid accountId)
        {
            var account = dbLayer.AccountsRepository.GetAccount(accountId);
            return CheckAccountIsDemo(account);
        }

        /// <summary>
        /// Проверить возможность подключения поддержки.
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Признак демо у аккаунта</returns>
        public bool CheckAvailabilityUseSupportOperation(Domain.DataModels.Account account)
        {
            var resConfig =
                account.BillingAccount.ResourcesConfigurations.FirstOrDefault(r =>
                    r.AccountId == account.Id &&
                    r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var accountHasInflowPayments = AccountHasInflowPayments(account.Id);

            return accountHasInflowPayments && resConfig is { FrozenValue: false } ||
                   HasActiveSponsoredLicenses(account);
        }

        /// <summary>
        /// Возможность сменить локаль для аккаунта
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Возможность сменить локаль</returns>
        public (bool, string) CanChangeLocaleForAccount(Guid accountId)
        {
            var hasPaidInvoicesOrPayments = HasPaidInvoicesOrPayments(accountId);
            var hasInvoicesForEsdlOrCustomServices = HasInvoicesForEsdlOrCustomServices(accountId);
            var hasCustomServicesForAccount = HasCustomServicesForAccount(accountId);
            var hasInvoicesOnSuggestedSum = HasInvoicesOnSuggestedSum(accountId);
            var hasReferralAccount = HasReferralAccount(accountId);
            
            switch (true)
            {
                case true when hasPaidInvoicesOrPayments:
                    return (false, "У аккаунта есть оплаченные счета или платежи.");
                case true when hasInvoicesForEsdlOrCustomServices:
                    return (false, "У аккаунта есть счета за ESDL или кастомные сервисы.");
                case true when hasCustomServicesForAccount:
                    return (false, "У аккаунта есть кастомные сервисы.");
                case true when hasInvoicesOnSuggestedSum:
                    return (false, "У аккаунта есть счета с произвольной суммой.");
                case true when hasReferralAccount:
                    return (false, "У аккаунта есть реферальный аккаунт.");
                default:
                    return (true, "Локаль можно изменить.");
            }
        }

        /// <summary>
        /// Наличие платежей
        /// или оплаченных счетов у аккаунта
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Наличие платежей или счетов</returns>
        public bool HasPaidInvoicesOrPayments(Guid accountId)
        {
            var availabilityOfPayments = dbLayer.PaymentRepository.Any(p => p.AccountId == accountId && p.Sum != 0);
            var availabilityOfPaidsInvoices = dbLayer.InvoiceRepository.Any(i =>
                i.AccountId == accountId && i.State == InvoiceStatus.Processed.ToString());

            logger.Debug(
                $"[{accountId}]: Наличие платежей= {availabilityOfPayments}, наличие оплаченных счетов= {availabilityOfPaidsInvoices}");

            return availabilityOfPayments || availabilityOfPaidsInvoices;
        }

        /// <summary>
        /// Наличие счетов для кастомных сервисов
        /// или сервиса Загрузка документов
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        public bool HasInvoicesForEsdlOrCustomServices(Guid accountId)
        {
            var accountInvoicesGuids = dbLayer.InvoiceRepository.Where(a => a.AccountId == accountId)
                .Select(a => a.Id).ToList();

            return dbLayer.InvoiceProductRepository.Any(p =>
                accountInvoicesGuids.Contains(p.InvoiceId) &&
                (p.ServiceType.Service.SystemService == Clouds42Service.Esdl ||
                 p.ServiceType.Service.SystemService == Clouds42Service.Recognition ||
                 p.ServiceType.Service.SystemService == null) &&
                p.ServiceSum > 0);
        }

        /// <summary>
        /// Наличие счетов на произвольную сумму
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Наличие счетов на произвольную сумму</returns>
        public bool HasInvoicesOnSuggestedSum(Guid accountId) => dbLayer.InvoiceRepository.Any(w =>
            w.AccountId == accountId &&
            (!w.InvoiceProducts.Any() || w.InvoiceProducts.Any(i => i.ServiceTypeId == null)));

        /// <summary>
        /// Наличие кастомных сервисов для аккаунта
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Наличие кастомных сервисов</returns>
        public bool HasCustomServicesForAccount(Guid accountId) =>
            dbLayer.ResourceConfigurationRepository.Any(w =>
                w.AccountId == accountId && w.BillingService.SystemService == null && w.BillingService.Name != "Дополнительные сеансы");

        /// <summary>
        /// Проверить были ли денежные поступления на аккаунт
        /// </summary>
        /// <param name="accountId">Аккаунт</param>
        /// <returns>Признак наличия поступлений</returns>
        public bool AccountHasInflowPayments(Guid accountId)
            => dbLayer.PaymentRepository.Any(p =>
                p.AccountId == accountId && p.Status == PaymentStatus.Done.ToString() &&
                p.OperationType == PaymentType.Inflow.ToString());

        /// <summary>
        /// Проверить аккаунт на наличие спонсируемых другим аккаунтом пользователей.
        /// </summary>
        private bool HasActiveSponsoredLicenses(IAccount account)
        {
            var hasSposoredLicenses =
                dbLayer.ResourceRepository.Any(resource =>
                    resource.AccountId == account.Id && resource.Cost > 0 && resource.AccountSponsorId.HasValue &&
                    resource.AccountSponsor.BillingAccount.ResourcesConfigurations.Any(
                        rc =>
                            rc.Frozen == false &&
                            rc.BillingService.SystemService == Clouds42Service.MyEnterprise));

            return hasSposoredLicenses;
        }

        /// <summary>
        /// Проверить аккаунт на наличие спонсируемых лицензий Аренды 1С
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>true - если пользователи аккаунта спонсируются</returns>
        private bool HasRent1CSponsoredLicenses(Domain.DataModels.Account account)
            => dbLayer.ResourceRepository.Any(l => l.AccountId == account.Id && l.AccountSponsorId.HasValue &&
                                                    l.AccountSponsor.BillingAccount.ResourcesConfigurations.Any(
                                                        rc => rc.BillingService.SystemService ==
                                                              Clouds42Service.MyEnterprise));

        /// <summary>
        /// Аккаунт имеет реферрала
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>true - если аккаунт имеет реферрала</returns>
        private bool HasReferralAccount(Guid accountId)
        {
            var account = dbLayer.AccountsRepository.GetAccount(accountId) ??
                          throw new NotFoundException($"Аккаунт по {accountId} не найден");

            return account.ReferralAccountID != null && account.ReferralAccountID != Guid.Empty;
        }
    }
}
