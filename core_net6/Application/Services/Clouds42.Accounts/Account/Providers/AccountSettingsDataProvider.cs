﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Account settings data provider
    /// </summary>
    internal class AccountSettingsDataProvider(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IAccountSettingsDataProvider
    {
        /// <summary>
        /// Get account settings
        /// </summary>
        /// <param name="accountUserId">User identifier</param>
        /// <returns></returns>
        public AccountSettingsDataDto GetAccountSettings(Guid accountUserId)
        {
            var accountSettings =
                (from accountUser in dbLayer.AccountUsersRepository.WhereLazy()
                 join accountWithConfiguration in accountConfigurationDataProvider.GetAccountsWithConfiguration() on
                     accountUser.AccountId equals accountWithConfiguration.Account.Id
                 join locale in dbLayer.LocaleRepository.WhereLazy() on accountWithConfiguration
                     .AccountConfiguration.LocaleId equals locale.ID
                 join segment in dbLayer.CloudServicesSegmentRepository.WhereLazy() on accountWithConfiguration
                     .AccountConfiguration.SegmentId equals segment.ID
                 join terminalFarms in dbLayer.CloudServicesTerminalFarmRepository.WhereLazy() on
                     segment.ServicesTerminalFarmID equals terminalFarms.ID into segmentTerminalFarms
                 from terminalFarm in segmentTerminalFarms.Take(1).DefaultIfEmpty()
                 join gatewayTerminals in dbLayer.CloudServicesGatewayTerminalRepository.WhereLazy() on
                     segment.GatewayTerminalsID equals gatewayTerminals.ID into segmentGatewayTerminals
                 from gatewayTerminal in segmentGatewayTerminals.Take(1).DefaultIfEmpty()
                 where accountUser.Id == accountUserId
                 select new AccountSettingsDataDto
                 {
                     Login = accountUser.Login,
                     GatewayTerminalsName = gatewayTerminal.ConnectionAddress,
                     ServicesTerminalFarmName = terminalFarm.ConnectionAddress,
                     LocaleName = locale.Name,
                 }).FirstOrDefault() ??
                throw new NotFoundException($"Не удалось получить настройки аккаунта по пользователю {accountUserId}");

            accountSettings.PlatformThinClientVersions = GetPlatformThinClientVersions().AsEnumerable()
                .OrderByDescending(thinClient => thinClient.Version, new VersionComparer()).ToList();

            return accountSettings;
        }

        /// <summary>
        /// Get a list of thin client versions
        /// </summary>
        /// <returns></returns>
        private IQueryable<PlatformThinClientVersionDto> GetPlatformThinClientVersions()
            => dbLayer.PlatformVersionReferencesRepository.WhereLazy(platf =>
                !string.IsNullOrEmpty(platf.MacOsThinClientDownloadLink) &&
                !string.IsNullOrEmpty(platf.WindowsThinClientDownloadLink)).Select(platf =>
                new PlatformThinClientVersionDto
                {
                    Version = platf.Version,
                    MacOsDownloadLink = platf.MacOsThinClientDownloadLink,
                    WindowsDownloadLink = platf.WindowsThinClientDownloadLink
                });
    }
}
