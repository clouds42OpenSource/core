﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Провайдер для работы с данными аккаунта
    /// </summary>
    public class AccountDataProvider(IUnitOfWork dbLayer) : IAccountDataProvider
    {
        /// <summary>
        /// Получить сгруппированных администраторов по аккаунту(по всем аккаунтам)
        /// </summary>
        /// <returns>Сгруппированные администраторы по аккаунтам</returns>
        public List<AccountUser> GetAdministratorsGroupedByAccount(Guid accountId)
        {
            return dbLayer.AccountUsersRepository
                .AsQueryable()
                .AsNoTracking()
                .Where(x => x.AccountUserRoles.Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin) &&
                            x.AccountId == accountId)
                .ToList();
        }

        /// <summary>
        /// Получить номер телефона админа аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Номер телефона админа аккаунта</returns>
        public string? GetAccountAdminPhoneNumber(Guid accountId)
        {
            var accountAdminIds = dbLayer.AccountsRepository
                .GetAccountAdminIds(accountId);

            return dbLayer.AccountUsersRepository
                .AsQueryable()
                .AsNoTracking()
                .FirstOrDefault(accountUser => accountAdminIds.Contains(accountUser.Id) && accountUser.PhoneNumber != null && accountUser.PhoneNumber != "")?.PhoneNumber;
        }

        /// <summary>
        /// Получить тип аккаунта
        /// </summary>
        /// <param name="referralAccountId"></param>
        /// <param name="configurationType"></param>
        /// <returns>Тип аккаунта</returns>
        public string GetAccountType(Guid? referralAccountId, string configurationType)
        {
            if (!referralAccountId.HasValue ||
                referralAccountId == Guid.Empty)
                return configurationType;

            var referralAccount = dbLayer.AccountsRepository.GetAccount(referralAccountId.Value);

            return string.IsNullOrEmpty(referralAccount.AccountCaption)
                ? GenerateCompanyNameHelper.GetCompanyName(referralAccount)
                : referralAccount.AccountCaption;
        }

        /// <summary>
        /// Получить информацию об аккаунте или выбросить исключение
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт облака 42.</returns>
        public Domain.DataModels.Account GetAccountOrThrowException(Guid accountId)
            => GetAccount(accountId)
               ?? throw new NotFoundException($"По указанному номеру '{accountId}' не найден аккаунт");

        /// <summary>
        /// Получить аккаунт
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт</returns>
        public Domain.DataModels.Account GetAccount(Guid accountId)
            => dbLayer.AccountsRepository.GetAccount(accountId);

        /// <summary>
        /// Получить полное название аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Полное название аккаунта</returns>
        public string GetAccountFullName(Guid accountId)
        {
            var account = GetAccountOrThrowException(accountId);
            return GetAccountFullName(account);
        }

        /// <summary>
        /// Получить полное название аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Полное название аккаунта</returns>
        public string GetAccountFullName(Domain.DataModels.Account account)
            => $"\"{account.AccountCaption}\" ({account.IndexNumber})";

        /// <summary>
        /// Необходимость принять агентское соглашение
        /// аккаунту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Необходимость принять агентское соглашение</returns>
        public bool NeedApplyAgencyAgreement(Guid accountId) =>
            dbLayer.AccountsRepository.NeedApplyAgencyAgreement(accountId);

        /// <summary>
        /// Получить сегмент
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Сегмент</returns>
        public CloudServicesSegment GetCloudServicesSegmentById(Guid segmentId)
            => dbLayer.CloudServicesSegmentRepository.GetSegment(segmentId)
               ?? throw new NotFoundException($"Сегмент по ID {segmentId} не найден");

        /// <summary>
        /// Получить информацию об аккаунте для уведомления
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информация аккаунта для уведомления</returns>
        public AccountInfoForNotifyDto GetAccountInfoForNotifyDto(Guid accountId)
        {
            var accountEmails =
                dbLayer.AccountsRepository.GetEmails(accountId);

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => accountEmails.Contains(au.Email)) ??
                              throw new NotFoundException(
                                  $"Не удалось найти пользователя для отправки сообщения. Аккаунт = {accountId}");

            accountEmails.Remove(accountUser.Email);

            return new AccountInfoForNotifyDto
            {
                AccountAdmin = accountUser,
                Emails = accountEmails
            };
        }

        /// <summary>
        /// Получить описание об администраторе аккаунта
        /// (ФИО + Электронный адрес + номер телефона)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Описание об администраторе аккаунта</returns>
        public string GetAccountAdminDescription(Guid accountId)
        {
            var accountAdminId = dbLayer.AccountsRepository
                .GetAccountAdminIds(accountId)
                .FirstOrDefault();

            if (accountAdminId.IsNullOrEmpty())
                return "";

            var accountAdmin = dbLayer.AccountUsersRepository.GetAccountUser(accountAdminId);

            var stingBuilder = new StringBuilder();
            stingBuilder.Append(string.IsNullOrWhiteSpace(accountAdmin.Name) ? "" : $"{accountAdmin.Name}, ");
            stingBuilder.Append(accountAdmin.Email);
            stingBuilder.Append(string.IsNullOrWhiteSpace(accountAdmin.PhoneNumber) ? "" : $", {accountAdmin.Name}");

            return stingBuilder.ToString();
        }

        
    }
}
