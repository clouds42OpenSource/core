﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Account.Validators;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Провайдер для работы с менеджером аккаунта
    /// </summary>
    internal class AccountSaleManagerProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : IAccountSaleManagerProvider
    {
        /// <summary>
        /// Удалить сейл менеджера у аккаунта
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        public void RemoveSaleManagerFromAccount(int accountNumber)
        {
            var account = GetAccountByNumber(accountNumber);

            if (account.AccountSaleManager == null)
            {
                logger.Info($"К аккаунту {account.IndexNumber} сейл менеджер не прикреплен");
                return;
            }

            RemoveAccountSaleManager(account.AccountSaleManager);
        }

        /// <summary>
        /// Прикрепить сейл менеджера к аккаунту
        /// </summary>
        /// <param name="accountSaleManagerDto">Модель менеджера прикрепленного к аккаунту</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        public void AttachSaleManagerToAccount(AccountSaleManagerDto accountSaleManagerDto, out string errorMessage)
        {
            errorMessage = null;
            AccountSaleManagerDtoValidator.Validate(accountSaleManagerDto);

            var account = GetAccountByNumber(accountSaleManagerDto.AccountNumber);

            var accountUser = GetAccountUserByLogin(accountSaleManagerDto.Login);
            if (accountUser == null)
            {
                errorMessage = $@"В Личном кабинете 42Clouds не удалось найти сотрудника по Логину “{accountSaleManagerDto.Login}”. 
                Необходимо убедиться, что пользователь {accountSaleManagerDto.Login} существует в Личном кабинете.";
                return;
            }

            if (account.AccountSaleManager == null)
            {
                logger.Info("Создание нового сейл менеджера прикрепленного к аккаунту");
                CreateAccountSaleManager(account.Id, accountUser.Id, accountSaleManagerDto);
                return;
            }

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                logger.Info("Удаление старого и создание нового сейл менеджера прикрепленного к аккаунту");
                RemoveAccountSaleManager(account.AccountSaleManager);
                CreateAccountSaleManager(account.Id, accountUser.Id, accountSaleManagerDto);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка прикрепления сейл менеджера]");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Получить информацию по сейл менеджеру для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информацию по сейл менеджеру для аккаунта</returns>
        public AccountSaleManagerInfoDto GetAccountSaleManagerInfo(Guid accountId)
        {
            var account = dbLayer.AccountsRepository.GetAccount(accountId);
            return new AccountSaleManagerInfoDto
            {
                Login = account.AccountSaleManager?.AccountUser?.Login,
                Email = account.AccountSaleManager?.AccountUser?.Email
            };
        }

        /// <summary>
        /// Создать менеджера прикрепленного к аккаунту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="saleManagerId">Id сейл менеджера</param>
        /// <param name="accountSaleManagerDto">Модель менеджера прикрепленного к аккаунту</param>
        private void CreateAccountSaleManager(Guid accountId, Guid saleManagerId,
            AccountSaleManagerDto accountSaleManagerDto)
        {
            var accountSaleManager = new AccountSaleManager
            {
                AccountId = accountId,
                SaleManagerId = saleManagerId,
                Division = accountSaleManagerDto.Division
            };

            dbLayer.AccountSaleManagerRepository.Insert(accountSaleManager);
            dbLayer.Save();
        }

        /// <summary>
        /// Удалить менеджера прикрепленного к аккаунту
        /// </summary>
        /// <param name="accountSaleManager">Менеджер прикрепленный к аккаунту</param>
        private void RemoveAccountSaleManager(AccountSaleManager accountSaleManager)
        {
            dbLayer.AccountSaleManagerRepository.Delete(accountSaleManager);
            dbLayer.Save();
        }

        /// <summary>
        /// Получить аккаунт по номеру
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        /// <returns>Аккаунт</returns>
        private Domain.DataModels.Account GetAccountByNumber(int accountNumber) =>
            dbLayer.AccountsRepository.FirstOrDefault(a => a.IndexNumber == accountNumber) ??
            throw new NotFoundException($"Не удалось найти аккаунт по номеру {accountNumber}");

        /// <summary>
        /// Получить пользователя по логину
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Пользователь</returns>
        private AccountUser GetAccountUserByLogin(string login)
            => dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Login == login);
    }
}
