﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Провайдер по анализу файлов аккаунта
    /// </summary>
    internal class AccountFilesAnalyzeProvider(
        IUnitOfWork dbLayer,
        IMemorySizeCalculator memorySizeCalculator,
        ISegmentHelper segmentHelper,
        ILogger42 logger)
        : IAccountFilesAnalyzeProvider
    {
        /// <summary>
        /// Проверка необходимости запуска таски
        /// на архивацию файлов аккаунта в склеп
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Необходимость в архивации</returns>
        public bool NeedArchiveAccountFilesToTomb(Guid accountId)
        {
            var account = dbLayer.AccountsRepository.FirstOrThrowException(a => a.Id == accountId, $"Аккаунт не найден по номеру '{accountId}'");
            var filePath = segmentHelper.GetFullClientFileStoragePath(account);

            if (string.IsNullOrEmpty(filePath) || !Directory.Exists(filePath))
            {
                logger.Info($"Не найдена папка аккаунта по адресу {filePath}");
                return false;
            }

            if (memorySizeCalculator.CalculateCatalog(filePath) == 0)
            {
                logger.Info($"Папка {filePath} пуста.");
                return false;
            }

            return true;
        }
    }
}
