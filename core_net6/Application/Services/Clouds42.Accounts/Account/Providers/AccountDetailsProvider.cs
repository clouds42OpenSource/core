﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.Domain.DataModels;
using Clouds42.Locales.Managers;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Класс для получения подробных сведений о аккаунте.
    /// </summary>
    public class AccountDetailsProvider(
        IUnitOfWork dbLayer,
        AccountDataProvider accountDataProvider,
        ICheckAccountDataProvider checkAccountProvider,
        IServiceProvider serviceProvider,
        IResourcesService resourcesService,
        ICloud42ServiceHelper cloud42ServiceHelper,
        LocaleManager localeManager)
    {
        /// <summary>
        /// Получить детальную информацию по аккаунту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Детальная информация по аккаунту</returns>
        public AccountDetailsModelDto GetAccountDetails(Guid accountId)
        {
           var acc = dbLayer.AccountsRepository.WhereLazy()
                .Include(x => x.AccountConfiguration)
                .Include(x => x.AccountConfiguration.Locale)
                .Include(x => x.AccountRequisites)
                .Include(x => x.BillingAccount)
                .Include(x => x.AccountSaleManager)
                .FirstOrDefault(a => a.Id == accountId)
                     ?? throw new NotFoundException(
               $"Не удалось получить детальную информацию об аккаунте '{accountId}'");

           return  new AccountDetailsModelDto
           {
               AccountId = acc.Id,
               ServiceTotalSum = GetServiceTotalSum(acc.Id),
               SegmentName = serviceProvider.GetRequiredService<ISegmentHelper>()
                   .GetSegmentName(acc),
               LastActivity = GetLastActivity(acc),
               Balance = acc.BillingAccount.Balance,
               AccountSaleManagerCaption = GetAccountSaleManagerCaption(acc.AccountSaleManager),
               Emails = acc.AccountEmails.Select(x => x.Email).ToList(),
               UsedSizeOnDisk = GetUsedSizeOnDisk(acc.Id),
               CanChangeLocale = checkAccountProvider.CanChangeLocaleForAccount(acc.Id).Item1,
               AccountType =
                   accountDataProvider.GetAccountType(acc.ReferralAccountID,
                       acc.AccountConfiguration.Type),
               LocaleId = acc.AccountConfiguration.LocaleId,

               IsVip = acc.AccountConfiguration.IsVip,
               IndexNumber = acc.IndexNumber,
               AccountCaption = acc.AccountCaption,
               AccountInn = acc.AccountRequisites.Inn,
               UserSource = acc.UserSource,
               LocaleCurrency = acc.AccountConfiguration.Locale.Currency,
               AccountDescription = acc.Description,
               SegmentId = acc.AccountConfiguration.SegmentId,
               CloudStorageWebLink = acc.AccountConfiguration.CloudStorageWebLink,
               RegistrationDate = acc.RegistrationDate,
               Locales = GetLocaleForAccountCardData()
           };
           
        }

        /// <summary>
        /// Получает используемое пространство на диске
        /// </summary>
        /// <param name="accountId">Account's id</param>
        /// <returns></returns>
        private long? GetUsedSizeOnDisk(Guid accountId) =>
            cloud42ServiceHelper.GetMyDiskInfo(accountId).UsedSizeOnDisk;

        /// <summary>
        /// Получить название сейл менеджера аккаунта
        /// </summary>
        /// <param name="accountSaleManager">Менеджер прикрепленный к аккаунту</param>
        /// <returns>Название сейл менеджера</returns>
        private static string GetAccountSaleManagerCaption(AccountSaleManager accountSaleManager)
            => accountSaleManager != null
                ? $"{accountSaleManager.AccountUser.Login} ({accountSaleManager.Division})"
                : "-";

        /// <summary>
        /// Получить последнюю активность пользователя
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Дата последней активности пользователя</returns>
        private DateTime? GetLastActivity(Domain.DataModels.Account account)
        {
            return dbLayer.PaymentRepository.WhereLazy(w => w.AccountId == account.Id)
                       .OrderByDescending(x => x.Date).FirstOrDefault()?.Date ?? account.RegistrationDate;
        }

        /// <summary>
        /// Получить общую стоимость сервисов аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Общая стоимость сервисов аккаунта</returns>
        private decimal GetServiceTotalSum(Guid accountId) => resourcesService.GetResourcesConfigurations(accountId)
            .Where(rc => !rc.IsDemoPeriod)
            .Sum(x => x.Cost);

        /// <summary>
        /// Получить доступные локали для карточки аккаунта
        /// </summary>
        /// <returns>Локали для карточки аккаунта</returns>
        private AccountCardKeyValueDataDto<Guid>[] GetLocaleForAccountCardData()
        {
            var localesManageResult = localeManager.GetLocales();
            var locales = localesManageResult.Error
                ? []
                : localesManageResult.Result.Select(item => new AccountCardKeyValueDataDto<Guid>
                {
                    Id = item.Id,
                    Value = item.Country
                }).ToArray();

            return locales;
        }
    }
}
