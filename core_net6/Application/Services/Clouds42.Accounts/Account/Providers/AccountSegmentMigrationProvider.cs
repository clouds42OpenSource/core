﻿using Clouds42.Accounts.Account.Helpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.SegmentMigration.Helpers;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.Segment;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Провайдер миграций аккаунта между сегментами
    /// </summary>
    internal class AccountSegmentMigrationProvider(
        IUnitOfWork dbLayer,
        SegmentMigrationResultReportHelper segmentMigrationResultReportHelper,
        ChangeAccountSegmentManager changeAccountSegmentManager,
        SynchronizeProcessFlowsHelper synchronizeProcessFlowsHelper,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IAccountSegmentMigrationProvider
    {
        /// <summary>
        /// Сменить сегмент для аккаунтов
        /// </summary>
        /// <param name="segmentMigrationParams">Параметры смены сегмента для аккаунта</param>
        public void ChangeAccountsSegment(SegmentMigrationParamsDto segmentMigrationParams)
        {
            try
            {
                var segmentForMigration = GetNewSegmentForMigration(segmentMigrationParams.SegmentToId);
                var migrationsResult = StartChangeSegmentProcess(segmentMigrationParams);

                segmentMigrationResultReportHelper.SendMigrationResultWorkerReport(segmentMigrationParams.AccountIds,
                    migrationsResult, segmentForMigration);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка смены сегмента для аккаунтов]");
                throw;
            }
        }

        /// <summary>
        /// Запустить процесс смены сегмента
        /// </summary>
        /// <param name="segmentMigrationParams">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат смены сегмента</returns>
        private List<MigrationResultDto> StartChangeSegmentProcess(SegmentMigrationParamsDto segmentMigrationParams)
        {
            var migrationsResult = new List<MigrationResultDto>();

            segmentMigrationParams.AccountIds.ForEach(accountId =>
            {
                var accountConfigurationBySegmentData =
                    accountConfigurationDataProvider.GetAccountConfigurationBySegmentData(accountId);

                var migrationResult = changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
                {
                    AccountId = accountId,
                    InitiatorId = segmentMigrationParams.InitiatorId,
                    SegmentToId = segmentMigrationParams.SegmentToId,
                    SegmentFromId = accountConfigurationBySegmentData.Segment.ID,
                    MigrationStartDateTime = DateTime.Now,
                    ParentProcessFlowKey = synchronizeProcessFlowsHelper.CreateParentProcessFlowId(accountId)
                });

                logger.Trace(
                    $"Результат миграции аккаунта {accountConfigurationBySegmentData.Account.IndexNumber} - {migrationResult.Result.Successful}");

                migrationsResult.Add(migrationResult.Result);
            });
            return migrationsResult;
        }

        /// <summary>
        /// Получить новый сегмент для миграции
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Новый сегмент для миграции</returns>
        private CloudServicesSegment GetNewSegmentForMigration(Guid segmentId)
            => dbLayer.CloudServicesSegmentRepository.FirstOrDefault(s => s.ID == segmentId)
               ?? throw new NotFoundException($"Не найден сегмент по ID {segmentId}");
    }
}
