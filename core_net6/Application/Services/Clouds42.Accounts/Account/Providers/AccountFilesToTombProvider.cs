﻿using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Account;
using Clouds42.Logger;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Провайдер по переносу файлов аккаунта в склеп.
    /// </summary>
    internal class AccountFilesToTombProvider(
        TombTaskCreatorManager tombTaskCreatorManager,
        IAccountFilesAnalyzeProvider accountFilesAnalyzeProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILogger42 logger)
        : IAccountFilesToTombProvider
    {
        private readonly Lazy<int> _lifetimeAccountDataInTombDaysCount = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired
            .GetLifetimeAccountDataInTombDaysCount);

        /// <summary>
        /// Заархивировать файлы аккаунта в склеп
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void ArchiveAccountFilesToTomb(Guid accountId)
        {
            if (!accountFilesAnalyzeProvider.NeedArchiveAccountFilesToTomb(accountId))
            {
                SendNotification(accountId);
                return;
            }

            tombTaskCreatorManager.CreateTaskForArchivateAccountFilesToTomb(new DeleteAccountFilesToTombJobParamsDto
            {
                AccountId = accountId
            });
        }

        /// <summary>
        /// Отправить уведомление об архивации данных
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void SendNotification(Guid accountId)
        {
            letterNotificationProcessor
                .TryNotify<TransferringAccountDataToTombLetterNotification, TransferringAccountDataToTombLetterModelDto
                >
                (new TransferringAccountDataToTombLetterModelDto
                {
                    AccountId = accountId,
                    LifetimeAccountDataInTombDaysCount = _lifetimeAccountDataInTombDaysCount.Value
                });

            logger.Trace($"Уведомление об архивации файлов аккаунта {accountId} в склеп отправлено успешно.");
        }
    }
}
