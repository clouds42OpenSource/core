﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.DataContracts.Account;
using Clouds42.Domain;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Провайдер для работы с данными аккаунта для отчета
    /// </summary>
    internal class AccountReportDataProvider(IUnitOfWork dbLayer) : IAccountReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по аккаунтам для отчета
        /// </summary>
        /// <returns>Список моделей данных по аккаунтам для отчета</returns>
        public List<AccountReportDataDto> GetAccountReportDataDcs() =>
            dbLayer.ExecuteSqlQueryList<AccountReportDataDto>(SqlQueries.Reports.Account.GetData);
    }
}
