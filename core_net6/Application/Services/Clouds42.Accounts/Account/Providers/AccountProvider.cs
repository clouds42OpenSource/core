﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Account.Specifications;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using LinqExtensionsNetFramework;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Провайдер для работы с данными аккаунта
    /// </summary>
    internal class AccountProvider(
        IUnitOfWork dbLayer,
        ISegmentHelper segmentHelper,
        ILogger42 logger,
        IHandlerException handlerException)
        : IAccountProvider
    {
        /// <summary>
        /// Получить списка баз аккаунта для миграции
        /// </summary>
        /// <param name="args">Фильр поиска</param>
        /// <returns>Список баз аккаунта для миграции</returns>
        public async Task<AccountDatabasesMigrationResultDto> GetAccountDatabasesForMigrationAsync(GetAccountDatabasesForMigrationQuery args)
        {

            var records = dbLayer
                .DatabasesRepository
                .AsQueryable()
                .Include(d => d.AccountDatabaseOnDelimiter)
                .Where(AccountDatabasesSpecification.ByAccountIndexNumber(args.Filter!.AccountIndexNumber!.Value))
                .Where(AccountDatabasesSpecification.ByIsNotOnDelimiter())
                .Where(AccountDatabasesSpecification.ByReadyState());
               
            var pagedAndFilteredRecords = await records.Where(AccountDatabasesSpecification.BySearchLine(args?.Filter?.SearchLine))
                .Where(AccountDatabasesSpecification.ByIsFile(args?.Filter?.IsTypeStorageFile))
                .AutoSort(args, StringComparison.OrdinalIgnoreCase)
                .ToPagedListAsync(args?.PageNumber ?? 1, args?.PageSize ?? 30);

            var mappedRecords = Enumerable.Select(pagedAndFilteredRecords, db => new AccountDatabaseMigrationItemDto
                {
                    AccountDatabaseId = db.Id,
                    IsFile = db.IsFile.HasValue && db.IsFile.Value,
                    IsPublishDatabase = db.PublishStateEnum == PublishState.Published,
                    V82Name = db.V82Name,
                    Size = db.SizeInMB,
                    IsPublishServices = false,
                    Path = new AccountDatabasePathHelper(dbLayer, logger, segmentHelper, handlerException).GetPath(db)
                })
                .ToList();

            var totalInfoModel = GetFileStorageSummaryModel(records);

            var availableFileStorages = GetAvailableFileStorages(args!.Filter!.AccountIndexNumber!.Value);
            var availableStoragePath = GetAvailableStoragePath(totalInfoModel);
            var numberOfDatabasesToMigrate = ConfigurationHelper.GetConfigurationValue<int>("LimitMigrationDatabases");

            return new AccountDatabasesMigrationResultDto
            {
                Databases = mappedRecords.ToPagedList(args?.PageNumber ?? 1, args?.PageSize ?? 30).ToPagedDto(),
                FileStorageAccountDatabasesSummary = totalInfoModel,
                AvalableFileStorages = availableFileStorages,
                AvailableStoragePath = availableStoragePath,
                NumberOfDatabasesToMigrate = numberOfDatabasesToMigrate
            };
        }

        /// <summary>
        /// Метод для получение всей информации о базах
        /// </summary>
        /// <param name="accountDatabases"> </param>
        /// <returns>Массив информации о базах клиента</returns>
        private FileStorageAccountDatabasesSummaryDto[] GetFileStorageSummaryModel(IQueryable<Domain.DataModels.AccountDatabase> accountDatabases)
        {
            var storages = dbLayer.CloudServicesFileStorageServerRepository.AsQueryableNoTracking().ToList();
            var storageGroupsWithFileDatabases = accountDatabases.Where(x => x.IsFile.HasValue && x.IsFile.Value).GroupBy(x => x.FileStorageID).ToArray();

            var totalInfoModel = storageGroupsWithFileDatabases.Select(group => new FileStorageAccountDatabasesSummaryDto
            {
                FileStorage = storages.FirstOrDefault(x => x.ID == group.Key)?.Name ?? "Не указано",
                DatabaseCount = group.Count(),
                PathStorage = storages.FirstOrDefault(x => x.ID == group.Key)?.ConnectionAddress ?? "",
                TotatlSizeOfDatabases = group.Sum(database => database.SizeInMB)
            }).OrderBy(x => x.FileStorage).ToList();

            var servDatabases = accountDatabases.Where(x => !x.IsFile.HasValue || !x.IsFile.Value).ToList();
            totalInfoModel.Add(new FileStorageAccountDatabasesSummaryDto
            {
                FileStorage = "Серверные базы",
                DatabaseCount = servDatabases.Count,
                TotatlSizeOfDatabases = servDatabases.Sum(x => x.SizeInMB)
            });

            return totalInfoModel.ToArray();
        }

        /// <summary>
        /// Метод получения имени доступных хранилищ и их Id для переносы
        /// </summary>
        /// <param name="accountIndexNumber">Для какого аккаунта получить доступные хранилища</param>
        /// <returns>Доступные хранилища, где Key - ID файлового хранилища, Value - Название файлового хранилища</returns>
        private KeyValuePair<Guid, string>[] GetAvailableFileStorages(int accountIndexNumber)
        {
            var account = dbLayer.AccountsRepository.GetAccountByIndexNumber(accountIndexNumber);
            var storages = segmentHelper.GetEnableStorageList(account);
            return storages.AsQueryable().Select(s => new KeyValuePair<Guid, string>(s.ID, s.Name)).ToArray();
        }

        /// <summary>
        /// Метод получения списка путей доступных хранилищ
        /// </summary>
        /// <param name="databases">Суммарная информация о базах</param>
        /// <returns>Списка путей доступных хранилищ</returns>
        private static string[] GetAvailableStoragePath(FileStorageAccountDatabasesSummaryDto[] databases)
        {
            var list = databases.Select(path => path.PathStorage).ToList();
            return list.OrderByDescending(p => p).ToArray();
        }
    }
}
