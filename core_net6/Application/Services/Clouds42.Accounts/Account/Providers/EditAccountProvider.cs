﻿using System.Text;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.Department;
using Clouds42.DataContracts.Segment;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Contracts;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Accounts.Account.Providers
{
    /// <summary>
    /// Провайдер изменения аккаунта
    /// </summary>
    public class EditAccountProvider(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ChangeLocaleProvider changeLocaleProvider,
        IResourcesService resourcesService,
        ICreateMigrationAccountTaskHelper createMigrationAccountTaskHelper,
        IAccountRequisitesDataProvider accountRequisitesDataProvider,
        IUpdateAccountRequisitesProvider updateAccountRequisitesProvider,
        IMakeMyDatabasesServiceForAccountFreeProvider makeMyDatabasesServiceForAccountFreeProvider,
        IMyDatabasesServiceCostByLocaleCustomizerFactory myDatabasesServiceCostByLocaleCustomizerFactory,
        IUpdateAccountConfigurationProvider updateAccountConfigurationProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        ICloudLocalizer cloudLocalizer,
        ISender sender)
    {
        /// <summary>
        /// Обновить сущность аккаунта в базе
        /// </summary>
        /// <param name="editAccountDc">Измененная модель аккаунта</param>
        /// <returns>Результат обновления аккаунта</returns>
        public bool UpdateAccount(EditAccountDto editAccountDc)
        {
            var currentAccount = dbLayer.AccountsRepository.AsQueryable().Include(x => x.AccountRequisites).FirstOrDefault(x => x.Id == editAccountDc.Id);
            if (currentAccount == null)
                return false;

            logger.Info("UpdateAccount: Редактирование аккаунта " + currentAccount.IndexNumber);

            var currentAccountConfiguration =
                accountConfigurationDataProvider.GetAccountConfiguration(currentAccount.Id);

            var differenceInfo = GetDifferenceAccount(editAccountDc, currentAccount, currentAccountConfiguration);
            var oldAccountCaption = currentAccount.AccountCaption;
            var oldAccountInn = currentAccount.AccountRequisites.Inn;
            using (var transaction = dbLayer.SmartTransaction.Get())
            {
                try
                {
                    currentAccount.AccountCaption = editAccountDc.AccountCaption;
                    currentAccount.Description = editAccountDc.Description;
                    currentAccount.Deployment = editAccountDc.Deployment;

                    updateAccountRequisitesProvider.Update(new AccountRequisitesDto
                    {
                        AccountId = currentAccount.Id,
                        Inn = editAccountDc.Inn
                    });

                    ChangeAutoCreateClusteredDatabase(currentAccountConfiguration, editAccountDc);
                    
                    if (editAccountDc.DepartmentRequests != null && editAccountDc.DepartmentRequests.Any())
                    {
                        ChangeDepartment(editAccountDc.DepartmentRequests, editAccountDc.Id);
                    }

                    if (currentAccountConfiguration.IsVip != editAccountDc.IsVip)
                    {
                        ChangeAccountVipStatus(currentAccountConfiguration, editAccountDc);
                    }

                    if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_SetLocale, () => editAccountDc.Id) &&
                        currentAccountConfiguration.LocaleId != editAccountDc.LocaleId &&
                        editAccountDc.LocaleId.HasValue)
                    {
                        changeLocaleProvider.ChangeLocaleForAccount(currentAccount, editAccountDc.LocaleId.Value);
                    }

                    if (currentAccount.AccountEmails.Any())
                    {
                        dbLayer.AccountEmailRepository.DeleteRange(currentAccount.AccountEmails);
                    }

                    if (editAccountDc.AccountEmails.Any())
                    {
                        dbLayer.AccountEmailRepository.InsertRange(GetAccountEmails(editAccountDc.AccountEmails,
                            editAccountDc.Id));
                    }

                    var lastAccountFile = dbLayer.AccountFileRepository
                        .AsQueryable()
                        .OrderByDescending(x => x.UpdatedOn)
                        .FirstOrDefault(x =>
                            x.Type == DocumentType.TaxAuthorityRegistrationCertificate &&
                            x.AccountId == editAccountDc.Id);

                    if (lastAccountFile != null &&
                        lastAccountFile.Status != VerificationStatus.Cancelled &&
                        (oldAccountCaption != editAccountDc.AccountCaption || oldAccountInn != editAccountDc.Inn))
                    {
                        lastAccountFile.Status = VerificationStatus.Cancelled;

                        LogEventHelper.LogEvent(dbLayer,
                            currentAccount.Id,
                            accessProvider,
                            LogActions.CancelTaxAuthorityRegistrationCertificate,
                            "Пользователем была отредактирована карточка компании. Произведено аннулирование свидетельства о постановке на учет в налоговый орган");

                        dbLayer.AccountFileRepository.Update(lastAccountFile);
                    }

                    dbLayer.AccountsRepository.Update(currentAccount);
                    dbLayer.Save();


                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    logger.Info(
                        $"UpdateAccount: Ошибка редактирования карточки аккаунта {currentAccount.IndexNumber}. Описание ошибки: {ex.GetFullInfo()}");

                    LogEventHelper.LogEvent(dbLayer, currentAccount.Id, accessProvider, LogActions.EditAccountCard,
                        $"Ошибка редактирования карточки аккаунта. Причина: {ex.Message}");
                    throw;
                }
            }

            logger.Info(
                $"UpdateAccount: Карточка аккаунта {currentAccount.IndexNumber} отредактированна. {differenceInfo}");

            LogEventHelper.LogEvent(dbLayer, currentAccount.Id, accessProvider, LogActions.EditAccountCard,
                $"Карточка аккаунта отредактирована. {differenceInfo}");

            return true;
        }

        /// <summary>
        ///     Миграция аккаунта на выбранный сегмент
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="segmentId">ID сегмента</param>
        /// <param name="useNewMechanism">Признак что необходимо использовать новый механизм</param>
        /// <returns>Результат создания задачи</returns>
        public bool MigrateSegmentAccount(Guid accountId, Guid? segmentId, bool useNewMechanism)
        {
            var accountConfigurationData =
                accountConfigurationDataProvider.GetAccountConfigurationBySegmentData(accountId);

            try
            {
                if (!segmentId.HasValue)
                    throw new NotFoundException("Выбранный сегмент не существует!");

                if (accountConfigurationData.Segment.ID == segmentId)
                    throw new InvalidOperationException("Выбранный сегмент идентичен текущему!");

                createMigrationAccountTaskHelper.CreateTask(new SegmentMigrationParamsDto
                {
                    AccountIds = [accountId],
                    SegmentToId = segmentId.Value,
                    InitiatorId = accessProvider.GetUser().Id,
                    UseNewMechanism = useNewMechanism
                });

                return true;
            }
            catch (Exception ex)
            {
                logger.Info(
                    $"UpdateAccount: Ошибка миграции сегмента аккаунта {accountConfigurationData.Account.IndexNumber}. Exception: {ex.GetFullInfo()}");

                return false;
            }
        }

        /// <summary>
        /// Получить подробное описание изменений аккаунта
        /// </summary>
        /// <param name="editAccountDc">Модель изменения аккаунта</param>
        /// <param name="currentAccount">Текущий аккаунт</param>
        /// <param name="accountConfiguration">Конфигурация аккаунта</param>
        /// <returns>Строка с описанием изменений</returns>
        private string GetDifferenceAccount(EditAccountDto editAccountDc, Domain.DataModels.Account currentAccount,
            AccountConfiguration accountConfiguration)
        {
            var accountRequisites = accountRequisitesDataProvider.GetIfExistsOrCreateNew(editAccountDc.Id);

            var builder = new StringBuilder();
            if (editAccountDc.AccountCaption != currentAccount.AccountCaption)
                builder.Append(
                    $"Имя компании изменено с \"{currentAccount.AccountCaption}\" на \"{editAccountDc.AccountCaption}\". ");

            if (editAccountDc.Inn != accountRequisites.Inn)
                builder.Append(
                    $"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Inn, currentAccount.Id)} компании изменен с \"{accountRequisites.Inn}\" на \"{editAccountDc.Inn}\". ");

            if (accountConfiguration.IsVip != editAccountDc.IsVip &&
                accessProvider.HasAccessBool(ObjectAction.Accounts_ViewFullInfo, () => currentAccount.Id))
                builder.Append(
                    $"Изменен VIP-статус клиента с \"{accountConfiguration.IsVip}\" на \"{editAccountDc.IsVip}\". ");

            if (editAccountDc.Description != currentAccount.Description)
                builder.Append(
                    $"Описание изменено с \"{currentAccount.Description}\" на \"{editAccountDc.Description}\". ");

            var newLocale = dbLayer.LocaleRepository.FirstOrDefault(loc => loc.ID == editAccountDc.LocaleId);
            if (newLocale != null && editAccountDc.LocaleId != accountConfiguration.LocaleId &&
                accessProvider.HasAccessBool(ObjectAction.Accounts_ViewFullInfo, () => currentAccount.Id))
                builder.Append(
                    $"Изменена локаль аккаунта с \"{accountConfiguration.Locale.Name}\" на \"{newLocale.Name}\". ");

            var newEmails = string.Join(",", currentAccount.AccountEmails.Select(w => w.Email).ToList());
            var currentEmails = string.Join(",", editAccountDc.AccountEmails);

            if (newEmails != currentEmails)
                builder.Append($"Эл. адреса для отправки писем изменены с \"{newEmails}\" на \"{currentEmails}\". ");

            return builder.ToString();
        }

        private void ChangeDepartment(List<DepartmentRequest> requests, Guid accountId)
        {
            foreach (var request in requests)
            {
                switch (request.ActionType)
                {
                    case DepartmentAction.Create:

                        var newDepartment = new Department { Id = Guid.NewGuid(), Name = request.Name, AccountId = accountId };
                        dbLayer.DepartmentRepository.Insert(newDepartment);

                        break;

                    case DepartmentAction.Update:

                        var existingDepartment = dbLayer.DepartmentRepository.AsQueryable()
                            .FirstOrDefault(d => d.Id == request.DepartmentId);

                        if (existingDepartment != null)
                        {
                            existingDepartment.Name = request.Name;
                            dbLayer.DepartmentRepository.Update(existingDepartment);
                        }

                        else
                        {
                            throw new NotFoundException($"Департамент {request.Name} не найден.");
                        }

                        break;

                    case DepartmentAction.Remove:

                        var departmentToDelete = dbLayer.DepartmentRepository.AsQueryable()
                            .FirstOrDefault(d => d.Id == request.DepartmentId);

                        var userDepartments = dbLayer.
                            AccountUserDepartmentRepository.
                            AsQueryable()
                            .Where(aud => aud.DepartmentId == request.DepartmentId)
                            .ToList();

                        dbLayer.AccountUserDepartmentRepository.DeleteRange(userDepartments);

                        if (departmentToDelete != null)
                        {
                            dbLayer.DepartmentRepository.Delete(departmentToDelete);
                        }

                        else
                        {
                            throw new NotFoundException($"Департамент {request.Name} не найден.");
                        }

                        break;

                    default:
                        throw new ArgumentException($"Неизвестный тип действия с департаментом: {request.ActionType}");
                }

                LogEventHelper.LogEvent(
                    dbLayer,
                    accountId,
                    accessProvider,
                    LogActions.DepartmentAccountAction,
                    $"Действие {nameof(request.ActionType)} для департамента {request.Name} успешно завершено");
            }
        }

        private void ChangeAutoCreateClusteredDatabase(AccountConfiguration accountConfiguration, EditAccountDto editAccountDc)
        {
            accountConfiguration.CreateClusterDatabase = editAccountDc.CreateClusterDatabase;
            dbLayer.AccountConfigurationRepository.Update(accountConfiguration);
        }

        /// <summary>
        /// Изменить ВИП статус аккаунта
        /// </summary>
        /// <param name="accountConfiguration">Конфигурация аккаунта</param>
        /// <param name="editAccountDc">Измененная модель аккаунта</param>
        private void ChangeAccountVipStatus(AccountConfiguration accountConfiguration, EditAccountDto editAccountDc)
        {
            updateAccountConfigurationProvider.UpdateSignVipAccount(accountConfiguration.AccountId,
                editAccountDc.IsVip ?? false);

            var resConfig = resourcesService.GetResourceConfig(editAccountDc.Id, Clouds42Service.MyEnterprise);

            if (resConfig != null)
                sender.Send(new RecalculateResourcesConfigurationCostCommand(new List<Guid> { editAccountDc.Id },
                    [resConfig.BillingService.Id], resConfig.BillingService.SystemService)).Wait();

            ChangeMyDatabasesServiceCost(editAccountDc.Id, editAccountDc.LocaleId ?? accountConfiguration.LocaleId,
                editAccountDc.IsVip ?? false);
        }

        /// <summary>
        /// Изменить стоимость сервиса "Мои информационные базы"
        /// (при изменении флага "Вип статуса аккаунта")
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="localeId">Id локали</param>
        /// <param name="isWillBeVipAccount">Будет являтся вип аккаунтом</param>
        private void ChangeMyDatabasesServiceCost(Guid accountId, Guid? localeId, bool isWillBeVipAccount)
        {
            if (isWillBeVipAccount)
            {
                makeMyDatabasesServiceForAccountFreeProvider.MakeServiceFree(accountId);
                return;
            }

            var myDatabasesServiceCostByLocaleCustomizer = localeId.HasValue
                ? myDatabasesServiceCostByLocaleCustomizerFactory.CreateByLocale(localeId.Value)
                : myDatabasesServiceCostByLocaleCustomizerFactory.CreateByLocale(LocaleConst.Russia);

            myDatabasesServiceCostByLocaleCustomizer.Customize(accountId);
        }

        /// <summary>
        /// Получить список доменных моделей эл. почт аккаунта из списка строк эл. почт
        /// </summary>
        /// <param name="accountEmails">Список эл. почт из модели редактирования аккаунта</param>
        /// <param name="accoundId">Id аккаунта</param>
        /// <returns>Коллекция эл. почт аккаунта</returns>
        private ICollection<AccountEmail> GetAccountEmails(List<string> accountEmails, Guid accoundId)
        {
            return accountEmails.Select(email => new AccountEmail
            {
                Email = email,
                AccountId = accoundId,
                Id = Guid.NewGuid()
            }).ToList();
        }
    }
}
