﻿using Clouds42.Accounts.Contracts.Account.Models;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.SQLNativeModels;

namespace Clouds42.Accounts.Account.Extensions
{
    /// <summary>
    /// Маппер аккаунта
    /// </summary>
    public static class AccountMapper
    {
        /// <summary>
        /// Выполнить маппинг к модели отображения информации об аккаунтах
        /// </summary>
        /// <param name="accountInfoDataRow">Информация об аккаунтах</param>
        /// <param name="pagination">Пагинация</param>
        /// <param name="referrerId">Id ссылки</param>
        /// <returns></returns>
        public static AccountsPaginationListDto MapToAccountsPaginationListDto(this List<AccountInfoDataRowDto> accountInfoDataRow,
            PaginationBaseDto pagination, Guid? referrerId)
        {
            var accountsPaginationListDto = new AccountsPaginationListDto
            {
                AccountsDetails = accountInfoDataRow.Select(a => new AccountDetailsDto
                {
                    AccountId = a.AccountId,
                    AccountCaption = a.AccountCaption,
                    AccountRegistrationDate = a.AccountRegistrationDate,
                    AccountUsersCount = a.AccountUsersCount ?? 0,
                    IndexNumber = a.IndexNumber,
                    AccountAdminId = a.AccountAdminId,
                    AccountAdminName = a.AccountAdminName ?? "",
                    AccountAdminEmail = a.AccountAdminEmail ?? "",
                    AccountAdminLogin = a.AccountAdminLogin ?? "",
                    AccountAdminPhoneNumber = a.AccountAdminPhoneNumber ?? "",
                    Rent1CExpiredDate = a.Rent1CExpiredDate,
                    IsVip = a.IsVip ?? false
                }).ToList(),
                Pagination = pagination,
                ReferrerId = referrerId
            };
            return accountsPaginationListDto;
        }
    }
}
