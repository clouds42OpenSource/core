﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Managers
{
    public class AccountUserRolesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        public async Task<int[]> Get(Guid accountUserId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUserRoles_View,  null, () => accountUserId);
            var roles = await DbLayer.AccountUserRoleRepository.AsQueryable().Where(x=>x.AccountUserId == accountUserId).ToListAsync();
            
            return roles.Select(p => (int)p.AccountUserGroup).ToArray();
        }
    }
}
