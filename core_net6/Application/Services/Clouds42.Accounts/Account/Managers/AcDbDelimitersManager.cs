﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Managers
{
    /// <summary>
    /// Менеджер инф. баз на разделителях
    /// </summary>
    public class AcDbDelimitersManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAcDbDelimitersProvider acDbDelimitersProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IAcDbDelimitersManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить номер информационной базы на разделителях по номеру материнской базы и номеру области.
        /// </summary>
        /// <param name="databaseSourceId">ID материнской базы</param>
        /// <param name="zone">Номер зоны</param>
        /// <returns>Номер информационной базы</returns>        
        public ManagerResult<SimpleResultModelDto<Guid>> GetAccountDatabaseIdByZone(Guid databaseSourceId, int zone)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_ViewDelimitersInfo, () => AccessProvider.ContextAccountId);

                var result = acDbDelimitersProvider.GetAccountDatabaseIdByZone(databaseSourceId, zone);

                return result == Guid.Empty ? NotFound<SimpleResultModelDto<Guid>>($"В материнской базе с номером '{databaseSourceId}' не найдена информация по области '{zone}'") : Ok(new SimpleResultModelDto<Guid> { Value = result });
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка получения инф. базы по номеру материнской базы] {databaseSourceId} и номеру области {zone}");
                return PreconditionFailed<SimpleResultModelDto<Guid>>(ex.Message);
            }
        }

        /// <summary>
        /// Выставление статуса информационной базы и добавление информации о базе на разделителях
        /// </summary>
        /// <param name="model">Модель базы на разделителях</param>
        /// <returns>Ok - если статус выставлен успешно и информация добавлена</returns>        
        public ManagerResult SetStatusAndInsertDataOnDelimiter(AccountDatabaseViaDelimiterDto model)
        {
            var accountId = AccountIdByAccountDatabase(model.AccountDatabaseId);
            var initiatorAccountId = GetInitiatorAccountId() ?? AccessProvider.ContextAccountId;

            var database = accountDatabaseDataProvider.GetAccountDatabase(model.AccountDatabaseId);
            if (database == null)
                return PreconditionFailed($"Не удалось найти информационную базу по Id {model.AccountDatabaseId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.SetStatusAndRunTaskForDbOnDelimiters,
                    () => accountId);

                acDbDelimitersProvider.SetStatusAndInsertDataOnDelimiter(model);

                LogEvent(() => initiatorAccountId, LogActions.EditInfoBase,
                    $"Успешно установлен статус \"{model.DatabaseState}\" для базы на разделителях {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}");
                return Ok();
            }
            catch (InvalidOperationException ex)
            {
                LogEvent(() => initiatorAccountId, LogActions.EditInfoBase,
                    $"Ошибка установки статуса для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} на разделителях");
                Logger.Warn(ex, "Ошибка создания базы на разделителях.");
                return PreconditionFailed($"Ошибка установки статуса для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} на разделителях");
            }
            catch (Exception e)
            {
                LogEvent(() => initiatorAccountId, LogActions.EditInfoBase,
                    $"Ошибка установки статуса для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} на разделителях");
                _handlerException.Handle(e, "[Ошибка создания базы на разделителях]");
                return PreconditionFailed($"Ошибка установки статуса для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)} на разделителях");
            }
            
        }

        /// <summary>
        /// Метод предоставления доступа одному пользователю
        /// </summary>
        /// <param name="model">Модель предоставления доступа</param>
        /// <param name="database">Инф. база</param>
        public ManagerResult AddAccessToDbOnDelimiters(AcDbAccessPostAddModelDto model, Domain.DataModels.AccountDatabase database)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddAccessToDbOnDelimiters,
                    () => AccountIdByAccountDatabase(database.Id));

                acDbDelimitersProvider.AddAccessToDbOnDelimiters(model, database);

                return Ok();
            }
            catch (InvalidOperationException ex)
            {
                Logger.Warn(ex,
                    $"Ошибка предоставления пользователю {model.AccountUserID} доступа к инф. базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}");
                return PreconditionFailed(ex.Message);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка предоставления доступа к инф. базе пользователю] {model.AccountUserID} доступа к инф. базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удаление доступа для одного пользователя
        /// </summary>
        /// <param name="database">Информационная база.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        public ManagerResult DeleteAccessFromDelimiterDatabase(Domain.DataModels.AccountDatabase database, Guid accountUserId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeleteAccessFromDelimiterDatabase, () => AccountIdByAccountDatabase(database.Id));

                acDbDelimitersProvider.DeleteAccessFromDelimiterDatabase(database, accountUserId);

                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Warn($"Ошибка удаления доступа пользователю {accountUserId} к инф. базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(database)}. Описание ошибки: {ex.GetFullInfo()}");
                return PreconditionFailed(ex.Message);
            }
        }

    }
}
