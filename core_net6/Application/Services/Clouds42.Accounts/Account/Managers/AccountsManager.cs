﻿using System.Globalization;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Segment.Internal;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Accounts.Account.Managers
{
    /// <summary>
    /// Менеджер по работе с аккаунтами облака
    /// </summary>
    public class AccountsManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ISegmentHelper segmentHelper,
        IHandlerException handlerException,
        CreateMigrationAccountDatabaseTaskHelper createMigrationAccountDatabaseTaskHelper)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountsManager
    {
        private readonly IUnitOfWork _dbLayer = dbLayer;
        private readonly IHandlerException _handlerException = handlerException;

        public AccountFooterModel GetFooterViewModel()
        {
            AccessProvider.HasAccessForMultiAccounts(ObjectAction.Accounts_View);
            var lastRegDate = DbLayer.AccountsRepository.AsQueryableNoTracking().OrderByDescending(x => x.RegistrationDate).FirstOrDefault()?.RegistrationDate?.Date ?? DateTime.Now.Date;

            var today = DateTime.Today;
            var month = new DateTime(today.Year, today.Month, today.Day);
            var first = month.AddMonths(-1);

            var listComp = DbLayer.AccountsRepository.Get(x => x.RegistrationDate.HasValue && (x.RegistrationDate.Value - today.Date).Days == 0);

            var footerModel = new AccountFooterModel
                {
                    RegisteredCompanyAmount = DbLayer.AccountsRepository.Count(), RegisteredCompanyAmountToday = listComp.Count(),
                    LastRegistration = lastRegDate.ToString("dd.MM") + " ," + lastRegDate.ToString("HH:mm") + " мск",
                    LastRegisteredCompany = DbLayer.AccountsRepository.OrderByDescending(x => x.RegistrationDate).FirstOrDefault()?.AccountCaption ?? "не определено",
                    UsersAmount = DbLayer.AccountUsersRepository.Count(),
                    DatabasesAmount = DbLayer.DatabasesRepository.Count(),

                    TotalPaymentByServices = decimal.Round(DbLayer.PaymentRepository.Select(x => x.Sum).DefaultIfEmpty(0).Sum(x => x), 2) + " руб.",
                    MonthTotalPayment = decimal.Round(DbLayer.PaymentRepository.Get(x => x.Date <= today.Date && x.Date >= first.Date).Sum(x => x.Sum), 2).ToString(CultureInfo.InvariantCulture) + " руб."
                };

            return footerModel;
        }

        /// <summary>
        /// Запуск задачи по миграциям 
        /// </summary>
        /// <returns></returns>
        public ManagerResult<bool> MigrateDatabaseToStorage(Guid storage, List<string> databases)
        {
            try
            {
                var accountDatabases = _dbLayer.DatabasesRepository.Where(d => databases.Contains(d.V82Name)).ToList();
                if (!accountDatabases.Any())
                {
                    throw new NotFoundException("Базы не найдены");
                }

                var accountDatabaseIds = accountDatabases.Select(d => d.Id).ToList();
                createMigrationAccountDatabaseTaskHelper.CreateTask(accountDatabaseIds, storage);
            }
            catch (Exception ex)
            {
                var message = $"Ошибка при создании задачи миграции. {ex.Message}";
                Logger.Warn(message);
                return PreconditionFailed<bool>(message);
            }

            return Ok(true);
        }


        /// <summary>
        /// Получить данные аккаунта.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        public async Task<ManagerResult<AccountInfoQueryDataDto>> GetPropertiesInfoAsync(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.Accounts_View, () => accountId);
            var account = await DbLayer.AccountsRepository.GetAccountInfoByIdAsync(accountId);

            return account == null ? NotFound<AccountInfoQueryDataDto>($"Аккаунт по идентификатору {accountId} не найден.") : Ok(account);
        }

        /// <summary>Get account cloud file storage server id(storage with client files)</summary>
        public async Task<ManagerResult<Guid>> GetFileStorageServerId(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.Accounts_View, () => accountId);

            // find account
            var account = await DbLayer.AccountsRepository.GetByIdAsync(accountId);

            return account == null ? NotFound<Guid>($"Account with id {accountId} not found.") : Ok(segmentHelper.GetClientFileStorage(account).ID);
        }

        public async Task<ManagerResult<List<Domain.DataModels.Account>>> Find(string searchString,
            int maxRecordsCount = 10)
        {
            AccessProvider.HasAccess(ObjectAction.Accounts_View);
            if (string.IsNullOrWhiteSpace(searchString) || maxRecordsCount < 1)
                return Ok(new List<Domain.DataModels.Account>());

            if (maxRecordsCount > 100)
                maxRecordsCount = 100;

            searchString = searchString.ToUpper();

            _ = int.TryParse(searchString, out var indexNumber);

            var accountsResultTable = await DbLayer.AccountsRepository
                .AsQueryableNoTracking()
                .Where(ac => ac.Status != "Deleted" && ((!string.IsNullOrEmpty(ac.AccountCaption) && ac.AccountCaption.ToUpper().Contains(searchString))  || ac.IndexNumber == indexNumber || ac.AccountEmails.Any(x => x.Email.ToLower().Contains(searchString))))
                .Distinct()
                .Take(maxRecordsCount)
                .ToListAsync();

            return Ok(accountsResultTable);
        }

        public ManagerResult<Guid> GetAccountIdByIndexNumber(int indexNumber)
        {
            AccessProvider.HasAccess(ObjectAction.Accounts_View);

            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.IndexNumber == indexNumber);

            return account == null ? NotFound<Guid>($"По номер {0} аккаунт не найден") : Ok(account.Id);
        }

        public ManagerResult<List<Guid>> GetAccountIdByCriteria(AccountFilteredCriteriaRequestDto criteriaRequest)
        {
            AccessProvider.HasAccess(ObjectAction.Accounts_View);

            const string notFoundMessage = "Account not found";
            try
            {

                if (criteriaRequest.Email == null &&
                    criteriaRequest.Inn == null &&
                    criteriaRequest.ManagerPhone == null)
                {
                    return BadRequest<List<Guid>>("Требудется хотя бы один критерий поиска");
                }

                var byEmail = DbLayer.AccountUsersRepository.WhereLazy(a => false);

                if (!string.IsNullOrEmpty(criteriaRequest.Email?.Trim()))
                {
                    byEmail = DbLayer.AccountUsersRepository.WhereLazy(a => a.Email == criteriaRequest.Email);
                }

                var byManagerPhone = DbLayer.AccountUsersRepository.WhereLazy(a => false);
                var phoneOnlyDigit = PhoneHelper.ClearNonDigits(criteriaRequest.ManagerPhone);
                if (criteriaRequest.ManagerPhone != null && !string.IsNullOrEmpty(phoneOnlyDigit))
                {
                    byManagerPhone = DbLayer.AccountUsersRepository.WhereLazy(a => a.PhoneNumber == phoneOnlyDigit);
                }

                var byInn = DbLayer.AccountUsersRepository.WhereLazy(a => false);
                if (!string.IsNullOrEmpty(criteriaRequest.Inn?.Trim()))
                {
                    byInn = from account in DbLayer.AccountsRepository.WhereLazy()
                        join accountUser in DbLayer.AccountUsersRepository.WhereLazy() on account.Id equals accountUser
                            .AccountId
                        join accountRequisites in DbLayer.GetGenericRepository<AccountRequisites>().WhereLazy() on
                            account.Id equals accountRequisites.AccountId
                        where accountRequisites.Inn == criteriaRequest.Inn
                        select accountUser;
                }


                var res = byEmail.Concat(byManagerPhone).Concat(byInn);
                res = res.Where(w => w.AccountUserRoles.Any(r => r.AccountUserGroup == AccountUserGroup.AccountAdmin));

                var users = res.Select(u => u.AccountId).Distinct().ToList();

                return users.Any()
                    ? Ok(users)
                    : NotFound<List<Guid>>(notFoundMessage);
            }
            catch (ArgumentNullException)
            {
                Logger.Warn(notFoundMessage);
                return NotFound<List<Guid>>(notFoundMessage);
            }
            catch (InvalidOperationException ex)
            {
                var msg = "Error while getting accoun. Error msg: " + ex.Message;
                Logger.Warn(msg);
                return Conflict<List<Guid>>(msg);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка поиска аккаунта по критериям]");
                return BadRequest<List<Guid>>("Other error.");
            }
        }
       
    }
}
