﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Accounts.Account.Managers
{
    /// <summary>
    /// Менеджер для работы с данными аккаунта
    /// </summary>
    public class AccountDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAccountProvider accountProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : BaseManager(accessProvider,
            dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Получить списка баз аккаунта для миграции
        /// </summary>
        /// <param name="args">Фильр поиска</param>
        /// <returns>Список баз аккаунта для миграции</returns>
        public async Task<ManagerResult<AccountDatabasesMigrationResultDto>> GetAccountDatabasesForMigrationAsync(GetAccountDatabasesForMigrationQuery args)
        {
            var account = await _dbLayer.AccountsRepository.AsQueryable().FirstOrDefaultAsync(ac => ac.Id == args.Filter!.AccountId);

            try
            {
                if (account == null)
                    return PreconditionFailed<AccountDatabasesMigrationResultDto>($"Аккаунт с id {args.Filter!.AccountId} не найден");

                args.Filter!.AccountIndexNumber = account.IndexNumber;

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_MigrationListView, () => account.Id);
                var accountDatabases = await accountProvider.GetAccountDatabasesForMigrationAsync(args);
                return Ok(accountDatabases);
            }

            catch (Exception ex)
            {
                _handlerException.Handle(ex,"[Произошла ошибка при получении информационных баз аккаунта]");
                return PreconditionFailed<AccountDatabasesMigrationResultDto>(ex.Message);
            }

        }
        
        /// <summary>
        /// Получить сегмент аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Сегмент аккаунта</returns>
        public ManagerResult<CloudServicesSegment> GetAccountSegment(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccessForMultiAccounts(ObjectAction.Segment_View);
                var segment = accountConfigurationDataProvider.GetAccountSegment(accountId);
                return Ok(segment);
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка получения сегмента аккаунта] '{accountId}'.";
                _handlerException.Handle(ex, errorMessage);
                return PreconditionFailed<CloudServicesSegment>($"{errorMessage}. Причина: {ex.Message}");
            }
        }
    }
}
