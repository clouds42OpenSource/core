﻿using Clouds42.BLL.Common;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Managers
{
    /// <summary>
    /// Менеджер для работы с сейл менеджером аккаунта
    /// </summary>
    public class AccountSaleManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountSaleManagerProvider accountSaleManagerProvider,
        IHandlerException handlerException,
        AccountLogEventHelper accountLogEventHelper,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Удалить сейл менеджера у аккаунта
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        public ManagerResult RemoveSaleManagerFromAccount(int accountNumber)
        {
            AccessProvider.HasAccess(ObjectAction.Account_RemoveSaleManager);
            var message = accountLogEventHelper.GenerateMessageForRemoveSaleManagerFromAccount(accountNumber);
            try
            {
                accountSaleManagerProvider.RemoveSaleManagerFromAccount(accountNumber);
                LogEvent(() => AccountIdByIndexNumber(accountNumber), LogActions.ChangeAccountSaleManager, message);
                return Ok();
            }
            catch (Exception ex)
            {
                LogEvent(() => AccountIdByIndexNumber(accountNumber), LogActions.ChangeAccountSaleManager, $"{message} завершилось ошибкой");
                _handlerException.Handle(ex, $"[{message}]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Прикрепить сейл менеджера к аккаунту
        /// </summary>
        /// <param name="accountSaleManagerDto">Модель менеджера прикрепленного к аккаунту</param>
        public ManagerResult AttachSaleManagerToAccount(AccountSaleManagerDto accountSaleManagerDto)
        {
            AccessProvider.HasAccess(ObjectAction.Account_AttachSaleManager);
            try
            {
                accountSaleManagerProvider.AttachSaleManagerToAccount(accountSaleManagerDto, out var errorMessage);
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    logger.Warn(errorMessage);
                    LogEvent(() => AccountIdByIndexNumber(accountSaleManagerDto.AccountNumber),
                        LogActions.ChangeAccountSaleManager, errorMessage);
                    return PreconditionFailed(errorMessage);
                }

                LogEvent(() => AccountIdByIndexNumber(accountSaleManagerDto.AccountNumber), LogActions.ChangeAccountSaleManager,
                    accountLogEventHelper.GenerateMessageForAttachSaleManagerToAccount(accountSaleManagerDto));
                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    "Закрепление менеджера за аккаунтом завершилось ошибкой.";
                LogEvent(() => AccountIdByIndexNumber(accountSaleManagerDto.AccountNumber), LogActions.ChangeAccountSaleManager, message);
                _handlerException.Handle(ex, $"[{message}]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить информацию по сейл менеджеру для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информацию по сейл менеджеру для аккаунта</returns>
        public ManagerResult<AccountSaleManagerInfoDto> GetAccountSaleManagerInfo(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.Account_GetSaleManager);
            try
            {
                var data = accountSaleManagerProvider.GetAccountSaleManagerInfo(accountId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Получение информации по сейл менеджеру для аккаунта завершилось с ошибкой.]");
                return PreconditionFailed<AccountSaleManagerInfoDto>(ex.Message);
            }
        }
    }
}
