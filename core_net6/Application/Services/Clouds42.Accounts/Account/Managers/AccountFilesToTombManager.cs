﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Managers
{
    /// <summary>
    /// Менеджер по переносу файлов аккаунта в склеп.
    /// </summary>
    public class AccountFilesToTombManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountFilesToTombProvider accountFilesToTombProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;


        /// <summary>
        /// Заархивировать файлы аккаунта в склеп
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public ManagerResult<bool> ArchiveAccountFilesToTomb(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountFilesToTomb, () => accountId);

            try
            {
                accountFilesToTombProvider.ArchiveAccountFilesToTomb(accountId);
                logger.Info($"Архивация файлов аккаунта {accountId} в склеп");
                return Ok(true);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка архивации файлов аккаунта в склеп] {accountId}");
                return PreconditionFailed<bool>($"Не удалось перенести файлы аккаунта {accountId} в склеп. Детали: {ex.Message}");
            }
        }

    }
}
