﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Segment;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Managers
{
    /// <summary>
    /// Менеджер миграций аккаунта между сегментами
    /// </summary>
    public class AccountSegmentMigrationManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountSegmentMigrationProvider accountSegmentMigrationProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Сменить сегмент для аккаунтов
        /// </summary>
        /// <param name="segmentMigrationParams">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат смены сегмента</returns>
        public ManagerResult ChangeAccountsSegment(SegmentMigrationParamsDto segmentMigrationParams)
        {
            try
            {
                AccessProvider.HasAccessForMultiAccounts(ObjectAction.AccountMigration);

                accountSegmentMigrationProvider.ChangeAccountsSegment(segmentMigrationParams);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка смены сегмента для аккаунтов]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
