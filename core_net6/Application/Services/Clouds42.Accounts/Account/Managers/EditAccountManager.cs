﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Managers
{
    /// <summary>
    /// Менеджер изменения аккаунта
    /// </summary>
    public class EditAccountManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        EditAccountProvider editAccountProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        ///     Получить сущность аккаунта
        /// </summary>
        public ManagerResult<Domain.DataModels.Account> GetAccount(Guid accountId)
        {
            try
            {
                var message = $"Получение аккаунта {accountId}";
                logger.Info(message);

                AccessProvider.HasAccess(ObjectAction.Accounts_View, () => accountId);
                return Ok(DbLayer.AccountsRepository.GetById(accountId));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка проучения аккаунта] {accountId}");
                return PreconditionFailed<Domain.DataModels.Account>(ex.Message);
            }
        }

        /// <summary>
        /// Обновить сущность аккаунта в базе
        /// </summary>
        /// <param name="editAccountDc">Модель редактирования аккаунта</param>
        /// <returns>Результат обновления аккаунта в бд</returns>
        public ManagerResult<bool> UpdateAccount(EditAccountDto editAccountDc)
        {
            try
            {
                var message = $"Обновление аккаунта в базе {editAccountDc.Id}";
                logger.Info(message);

                AccessProvider.HasAccess(ObjectAction.Accounts_Edit, () => editAccountDc.Id);
                var result = editAccountProvider.UpdateAccount(editAccountDc);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка Обновления аккаунта] {editAccountDc.Id} ");
                return PreconditionFailed<bool>(ex.Message);
            }
        }
        
        
        /// <summary>
        ///     Сменить сегмент у аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="segmentId">ID нового сегмента</param>
        /// <param name="useNewMechanism">Признак что необходимо использовать новый механизм</param>
        /// <returns>Результат создания задачи</returns>
        public ManagerResult<bool> ChangeSegment(Guid accountId, Guid segmentId, bool useNewMechanism)
        {
            try
            {
                var message = $"Смена сегмента для аккаунта \"{accountId}\"";
                logger.Info(message);

                AccessProvider.HasAccess(ObjectAction.Accounts_EditFullInfoWithSegments, () => accountId);
                var result = editAccountProvider.MigrateSegmentAccount(accountId, segmentId, useNewMechanism);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка Смены сегмента для аккаунта] {accountId}");
                return PreconditionFailed<bool>(ex.Message);
            }
        }
    }
}
