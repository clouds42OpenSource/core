﻿using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.SQLNativeModels;

namespace Clouds42.Accounts.Account.Managers;

public interface IAccountsManager
{
    AccountFooterModel GetFooterViewModel();

    /// <summary>
    /// Запуск задачи по миграциям 
    /// </summary>
    /// <returns></returns>
    ManagerResult<bool> MigrateDatabaseToStorage(Guid storage, List<string> databases);

    /// <summary>
    /// Получить данные аккаунта.
    /// </summary>
    /// <param name="accountId">Идентификатор аккаунта.</param>
    Task<ManagerResult<AccountInfoQueryDataDto>> GetPropertiesInfoAsync(Guid accountId);
    
    /// <summary>Get account cloud file storage server id(storage with client files)</summary>
    Task<ManagerResult<Guid>> GetFileStorageServerId(Guid accountId);
    
    Task<ManagerResult<List<Domain.DataModels.Account>>> Find(string searchString, int maxRecordsCount = 10);
    ManagerResult<Guid> GetAccountIdByIndexNumber(int indexNumber);
    ManagerResult<List<Guid>> GetAccountIdByCriteria(AccountFilteredCriteriaRequestDto criteriaRequest);
    
}
