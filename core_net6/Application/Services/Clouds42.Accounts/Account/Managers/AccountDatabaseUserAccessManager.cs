﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.Common.Helpers;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Managers
{
    /// <summary>
    /// Менеджер по предоставлению доступов к базе
    /// </summary>
    public class AccountDatabaseUserAccessManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAcDbAccessHelper acDbAccessHelper,
        LogDbEventHelper logDbEventHelper,
        AccountHelper accountHelper,
        IAccountDatabaseProvider accountDatabaseProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountDatabaseUserAccessManager
    {
        /// <summary>
        /// Предоставление доступов внутренним пользователям
        /// </summary>
        /// <param name="databaseId"> Id информационной базы </param>
        /// <param name="accountUserId"> Id пользователя </param>
        /// <returns></returns>
        public ManageAccessResultDto GrandInternalAccessToDb(Guid databaseId, Guid accountUserId)
        {
            if (!accountHelper.TryGetAccountUserById(accountUserId, out var accountUser))
                return new ManageAccessResultDto { Success = false, Message = $"Доступ не предоставлен! Не найден пользователь по Id {accountUserId}" };

            if (!accountDatabaseProvider.TryGetAccountDatabaseById(databaseId, out var accountDatabase))
                return new ManageAccessResultDto { Success = false, Message = $"Доступ не предоставлен! Не найдена база по Id {databaseId}" };

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_GrantAccessToUser, () => AccountIdByAccountDatabase(databaseId));

                var result = acDbAccessHelper.GrandInternalAccessToDb(accountDatabase, accountUser);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddAccessToInfoBase,
                    result.Success
                        ? $"Предоставлен доступ внутреннему пользователю \"{accountUser.Login}\" к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}"
                        : $"Доступ внутреннего пользователя \"{accountUser.Login}\" к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не добавлен по причине: \"{result.Message.ClearHtmlTagsFromString()}\"");
                return result;
            }
            catch (Exception e)
            {
                var errorMessage =
                    $"Доступ внутреннему пользователю '{accountUser.Login}' к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не предоставлен по причине: {e.Message}";

                logger.Warn(e, errorMessage);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddAccessToInfoBase, errorMessage);

                return new ManageAccessResultDto
                {
                    Success = false,
                    Message = "Доступ не предоставлен!",
                    UserName = accountUser.Login,
                    DatabasesName = accountDatabase.V82Name
                };
            }
        }

        /// <summary>
        /// Предоставление доступов внешним пользователям
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="accountUserEmail"></param>
        /// <returns></returns>
        public ManageAccessResultDto GrandExternalAccessToDb(Guid databaseId, string accountUserEmail)
        {

            if (!accountHelper.TryGetAccountUserByEmail(accountUserEmail, out _))
                return new ManageAccessResultDto { Success = false, Message = $"Доступ не предоставлен! Не найден пользователь по Email {accountUserEmail}" };

            if (!accountDatabaseProvider.TryGetAccountDatabaseById(databaseId, out var accountDatabase))
                return new ManageAccessResultDto { Success = false, Message = $"Доступ не предоставлен! Не найдена база по Id {databaseId}" };

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_GrantAccessToUser, () => AccountIdByAccountDatabase(databaseId));

                var result = acDbAccessHelper.GrandExternalAccessToDb(accountDatabase, accountUserEmail);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddAccessToInfoBase,
                    result.Success
                        ? $"Предоставлен доступ внешнему пользователю '{accountUserEmail}' к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}."
                        : $"Доступ внешнему пользователю '{accountUserEmail}' к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не предоставлен по причине: '{result.Message.ClearHtmlTagsFromString()}'");
                return result;
            }
            catch (Exception e)
            {
                var errorMessage =
                    $"Доступ внешнему пользователю '{accountUserEmail}' к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не предоставлен по причине: {e.Message}";

                logger.Warn(e, errorMessage);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddAccessToInfoBase, errorMessage);

                return new ManageAccessResultDto
                {
                    Success = false,
                    Message = errorMessage
                };
            }

        }

        /// <summary>
        /// Удаление доступов к базе
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="accountUserId"></param>
        /// <returns></returns>
        public ManageAccessResultDto DeleteAccessFromDb(Guid databaseId, Guid accountUserId)
        {

            if (!accountHelper.TryGetAccountUserById(accountUserId, out var accountUser))
                return new ManageAccessResultDto { Success = false, Message = $"Доступ не предоставлен! Не найден пользователь по Id {accountUserId}" };

            if (!accountDatabaseProvider.TryGetAccountDatabaseById(databaseId, out var accountDatabase))
                return new ManageAccessResultDto { Success = false, Message = $"Доступ не предоставлен! Не найдена база по Id {databaseId}" };

            var accountId = Guid.Empty;

            try
            {
                accountId = AccountIdByAccountDatabase(databaseId) ?? accountId;

                AccessProvider.HasAccess(ObjectAction.AccountDatabases_RemoveUserAccess, () => accountId);

                var result = acDbAccessHelper.DeleteAccessFromDb(accountDatabase, accountUser);

                LogEvent(() => AccessProvider.ContextAccountId != Guid.Empty ? AccessProvider.ContextAccountId : accountId,
                    LogActions.RemoveAccessToInfoBase,
                    result.Success
                        ? logDbEventHelper.GenerateMessageAboutSuccessAccessDeletionFromDb(accountDatabase, accountUser)
                        : logDbEventHelper.GenerateMessageAboutFailedDeletionFromDb(accountDatabase, accountUser, result.Message), accountUserId);

                return result;
            }
            catch (Exception e)
            {
                logger.Warn(e, $"Ошибка удаления доступа пользователю {accountUserId} к базе {databaseId}");

                LogEvent(() => AccessProvider.ContextAccountId != Guid.Empty ? AccessProvider.ContextAccountId : accountId
                , LogActions.RemoveAccessToInfoBase,
                    $"Доступ внешнему пользователю '{accountUser.Login}' к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не удален по причине: {e.Message}", accountUserId);

                return new ManageAccessResultDto
                {
                    Success = false,
                    Message = "Ошибка удаления доступа!",
                    UserName = accountUser.Login,
                    DatabasesName = accountDatabase.V82Name
                };
            }

        }

        /// <summary>
        /// Предоставление доступа всем внутренним пользователям
        /// </summary>
        public GrandAccessForAllUsersOfAccountResultDc GrandAccessForAllUsersOfAccount(Guid accountDatabaseId)
        {

            if (!accountDatabaseProvider.TryGetAccountDatabaseById(accountDatabaseId, out var accountDatabase))
                return new GrandAccessForAllUsersOfAccountResultDc { Success = false, Message = $"Доступ не предоставлен! Не найдена база по Id {accountDatabaseId}" };

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_AccessToAllAccountUsers, () => AccountIdByAccountDatabase(accountDatabaseId));

                var result = acDbAccessHelper.GrandAccessForAllUsersOfAccount(accountDatabase);

                if (result.Success)
                {
                    LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddAccessToInfoBase,
                        result.AccountUsers != null
                            ? $"Добавлен доступ пользователям '{string.Join(",", result.AccountUsers.Select(s => s.UserEmail))}' к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}"
                            : $"Добавлена таска на добавление всех пользователей к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} на разделителях");
                }
                else
                {
                    LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddAccessToInfoBase, $"Не удалось добавить доступ к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} всем пользователям аккаунта");
                }

                return result;
            }
            catch (Exception e)
            {
                logger.Warn(e, $"Ошибка предоставления доступа всем внутренним пользователям к базе{accountDatabaseId}");

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.AddAccessToInfoBase,
                    $"Доступ всем внутренним пользователям к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не предоставлен по причине: {e.Message}");

                return new GrandAccessForAllUsersOfAccountResultDc
                {
                    Success = false,
                    Message = "Доступ не предоставлен!"
                };
            }

        }

    }
}
