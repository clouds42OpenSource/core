﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Configuration.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Хэлпер для работы с сегментами
    /// </summary>
    public class SegmentHelper(IUnitOfWork dbLayer, ICloudLocalizer cloudLocalizer) : ISegmentHelper
    {
        private readonly AccountDataProvider _accountDataProvider = new(dbLayer);
        private readonly AccountConfigurationDataProvider _accountConfigurationDataProvider = new(dbLayer);

        /// <summary>
        /// Получить сервер предприятия 8.2
        /// </summary>
        /// <returns>Сервер предприятия 8.2</returns>
        public string GetEnterpriseServer82(Domain.DataModels.Account account)
            => GetEnterpriseServer(segment => segment.EnterpriseServer82ID, account);

        /// <summary>
        /// Получить сервер предприятия 8.3
        /// </summary>
        /// <returns>Сервер предприятия 8.3</returns>
        public string GetEnterpriseServer83(Domain.DataModels.Account account)
            => GetEnterpriseServer(segment => segment.EnterpriseServer83ID, account);

        /// <summary>
        /// Возвращает обьект дефолтного файлового хранилища
        /// </summary>
        /// <returns>Обьект дефолтного файлового хранилища</returns>
        public CloudServicesFileStorageServer GetFileStorageServer(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);

            var segmentStorageList = dbLayer.CloudServicesSegmentStorageRepository
                .Where(s => s.SegmentID == segment.ID).ToList();

            var defSegmentStorage = segmentStorageList.FirstOrDefault(s => s.IsDefault);

            if (defSegmentStorage != null)
                return defSegmentStorage.CloudServicesFileStorageServer;

            if (segmentStorageList.Any())
                return segmentStorageList.First().CloudServicesFileStorageServer;

            throw new InvalidOperationException($"Segment '{segment.ID}' don't have any FileStorageServer");
        }

        /// <summary>
        /// Возвращает обьект файлового хранилища клиентских файлов
        /// </summary>
        /// <returns>Обьект файлового хранилища клиентских файлов</returns>
        public CloudServicesFileStorageServer GetClientFileStorage(Domain.DataModels.Account account) =>
            GetAccountSegment(account).CloudServicesFileStorageServer;

        /// <summary>
        /// Возвращает полный путь к папке клиентских файлов (с учетом CustomFileStoragePath для VIP).
        /// </summary>
        /// <returns></returns>
        public string GetFullClientFileStoragePath(Domain.DataModels.Account account)
            => new AccountSegmentHelper(_accountDataProvider, _accountConfigurationDataProvider)
                .GetFullClientFileStoragePath(account.Id);

        /// <summary>
        /// Получить адрес подключения для шлюза сегмента
        /// </summary>
        /// <returns>Адрес подключения для шлюза сегмента</returns>
        public string GetGatewayTerminals(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);
            var item = dbLayer.CloudServicesGatewayTerminalRepository.FirstOrDefault(x =>
                x.ID == segment.GatewayTerminalsID);
            return item?.ConnectionAddress;
        }

        /// <summary>
        /// Получить адрес подключения к SQL серверу
        /// </summary>
        /// <returns>Адрес подключения к SQL серверу</returns>
        public string GetSQLServer(Domain.DataModels.Account account)
        {
            if (account == null)
                return string.Empty;

            var segment = GetAccountSegment(account);
            var item = dbLayer.CloudServicesSqlServerRepository.FirstOrDefault(x => x.ID == segment.SQLServerID);
            return item.ConnectionAddress;
        }

        /// <summary>
        /// Получить адрес подключения к ферме серверов
        /// </summary>
        /// <returns>Адрес подключения к ферме серверов</returns>
        public string GetTerminalFarm(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);
            var item = dbLayer.CloudServicesTerminalFarmRepository.FirstOrDefault(x =>
                x.ID == segment.ServicesTerminalFarmID);
            return item.ConnectionAddress;
        }

        /// <summary>
        ///  Получить сервер публикаций для аккаунта
        /// </summary>
        /// <returns>Cервер публикаций для аккаунта</returns>
        public CloudServicesContentServer GetContentServer(Domain.DataModels.Account account)
            => GetAccountCloudContentServer(account)
               ?? throw new NotFoundException(
                   $"Не удалось получить сервер публикаций для аккаунта {account.IndexNumber}");

        /// <summary>
        /// Получить имя сайта публикаций
        /// </summary>
        /// <returns>Имя сайта публикаций</returns>
        public string GetPublishSiteName(Domain.DataModels.Account account)
        {
            var item = GetAccountCloudContentServer(account);
            return item.PublishSiteName;
        }

        /// <summary>
        ///     Сформировать путь к опубликованной базе
        /// </summary>
        /// <param name="folder">Папка</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <param name="databaseName">Имя базы в формате V82Name</param>
        /// <returns>Путь к опубликованной базе</returns>
        public string GetPublishedDatabasePhycicalPath(string folder, string accountEncodeId, string databaseName, Domain.DataModels.Account account)
        {
            var item = GetAccountCloudContentServer(account);

            return item.GroupByAccount
                ? Path.Combine(folder, accountEncodeId, databaseName)
                : Path.Combine(folder, databaseName);
        }

        /// <summary>
        ///     Сформировать путь для IIS к опубликованной базе
        /// </summary>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <param name="databaseName">Имя базы в формате V82Name</param>
        /// <returns>Путь для IIS к опубликованной базе</returns>
        public string GetIisWebAddress(string accountEncodeId, string databaseName, Domain.DataModels.Account account)
        {
            var cloudContentServer = GetAccountCloudContentServer(account);
            return GetIisWebAddress(cloudContentServer, accountEncodeId, databaseName);
        }

        /// <summary>
        ///     Сформировать путь для IIS к опубликованной базе
        /// </summary>
        /// <param name="contentServer">Сервер публикаций</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <param name="databaseName">Имя базы в формате V82Name</param>
        /// <returns>Путь для IIS к опубликованной базе</returns>
        public string GetIisWebAddress(CloudServicesContentServer contentServer, string accountEncodeId,
            string databaseName)
            => contentServer.GroupByAccount
                ? $"/{accountEncodeId}/{databaseName}"
                : $"/{databaseName}";

        /// <summary>
        ///     Сформировать путь к секции конфига
        /// </summary>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <param name="databaseName">Имя базы в формате V82Name</param>
        /// <returns>Путь к секции конфига</returns>
        public string CreateSectionName(string accountEncodeId, string databaseName, Domain.DataModels.Account account)
        {
            var item = GetAccountCloudContentServer(account);

            return item.GroupByAccount
                ? $"{item.PublishSiteName}/{accountEncodeId}/{databaseName}"
                : $"{item.PublishSiteName}/{databaseName}";
        }

        /// <summary>
        ///     Сформировать путь где лежат все
        /// опубликованные базы аккаунта
        /// </summary>
        /// <param name="folder">Папка</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <returns>Путь к опубликованным базам аккаунта</returns>
        public string CreateAccountDirectoryPath(string folder, string accountEncodeId, Domain.DataModels.Account account)
        {
            var cloudContentServer = GetAccountCloudContentServer(account);
            return CreateAccountDirectoryPath(cloudContentServer, folder, accountEncodeId);
        }

        /// <summary>
        ///     Сформировать путь где лежат все
        /// опубликованные базы аккаунта
        /// </summary>
        /// <param name="contentServer">Сервер публикаций</param>
        /// <param name="folder">Папка</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <returns>Путь к опубликованным базам аккаунта</returns>
        public string CreateAccountDirectoryPath(CloudServicesContentServer contentServer, string folder,
            string accountEncodeId)
            => contentServer.GroupByAccount
                ? Path.Combine(folder, accountEncodeId)
                : folder;

        /// <summary>
        ///     Создать путь к модулю IIS для стабильной версии платформы 8.2
        /// </summary>
        /// <returns>Путь к модулю IIS для стабильной версии платформы 8.2</returns>
        public string CreatePathToModuleV82Stable(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);
            var pathToModule = CloudConfigurationProvider.Segment.V82ModulePath();
            return string.Format(pathToModule, segment.Stable82PlatformVersionReference.Version);
        }

        /// <summary>
        ///     Создать путь к модулю IIS для стабильной версии платформы 8.3
        /// </summary>
        /// <returns>Путь к модулю IIS для стабильной версии платформы 8.3</returns>
        public string CreatePathToModuleV83Stable(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);
            var pathToModule = CloudConfigurationProvider.Segment.V83ModulePath();
            return string.Format(pathToModule, segment.Stable83PlatformVersionReference.Version);
        }

        /// <summary>
        ///     Создать путь к модулю IIS для альфа версии платформы 8.3
        /// </summary>
        /// <returns>Путь к модулю IIS для альфа версии платформы 8.3</returns>
        public string CreatePathToModuleV83Alpha(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);

            if (segment.Alpha83PlatformVersionReference == null)
                throw new InvalidOperationException(
                    $"Не возможно сгенерировать путь. {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.SegmentDoesNotIndicateAlphaVersionOf1CPlatform, account.Id)}");

            var pathToModule = CloudConfigurationProvider.Segment.V83ModulePath();
            return string.Format(pathToModule, segment.Alpha83PlatformVersionReference.Version);
        }

        /// <summary>
        /// Получить название сегмента
        /// </summary>
        /// <returns>Название сегмента</returns>
        public string GetSegmentName(Domain.DataModels.Account account) => GetAccountSegment(account).Name;

        /// <summary>
        /// Получить файловые хранилища сегмента
        /// </summary>
        /// <returns>Файловые хранилища сегмента</returns>
        public List<CloudServicesFileStorageServer> GetEnableStorageList(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);
            var storageIds = dbLayer.CloudServicesSegmentStorageRepository.Where(x => x.SegmentID == segment.ID).Select(x => x.FileStorageID);
            return dbLayer.CloudServicesFileStorageServerRepository.Where(x => storageIds.Contains(x.ID)).ToList();
        }

        /// <summary>
        /// Получить путь до 1С exe для альфа платформы
        /// </summary>
        /// <returns>Путь до 1С exe для альфа платформы</returns>
        public string GetPath1CExeAlpha83Platform(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);

            if (segment.Alpha83PlatformVersionReference == null)
                throw new ArgumentNullException($"Альфа версия платформы для сегмента {segment.ID} не найдена");

            return segment.Alpha83PlatformVersionReference.PathToPlatform;
        }

        /// <summary>
        /// Получить путь до 1С exe для стабильной версии 8.3
        /// </summary>
        /// <returns>Путь до 1С exe для стабильной версии 8.3</returns>
        public string GetPath1CExeStable83Platform(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);

            if (segment.Stable83PlatformVersionReference == null)
                throw new ArgumentNullException($"Стабильная версия платформы для сегмента {segment.ID} не найдена");

            return segment.Stable83PlatformVersionReference.PathToPlatform;
        }

        /// <summary>
        /// Получить путь до 1С exe для стабильной версии 8.2
        /// </summary>
        /// <returns>Путь до 1С exe для стабильной версии 8.2</returns>
        public string GetPath1CExeStable82Platform(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);

            if (segment.Stable82PlatformVersionReference == null)
                throw new ArgumentNullException($"Стабильная версия платформы для сегмента {segment.ID} не найдена");

            return segment.Stable82PlatformVersionReference.PathToPlatform;
        }

        public string? GetWindowsThinkClientDownloadLink(Domain.DataModels.Account account, Domain.DataModels.AccountDatabase accountDatabase)
        {
            var segment = GetAccountSegment(account);

            return accountDatabase.PlatformType switch
            {
                PlatformType.V82 => segment.Stable82PlatformVersionReference.WindowsThinClientDownloadLink,
                PlatformType.V83 => accountDatabase.IsFile.HasValue && accountDatabase.IsFile.Value
                    ? accountDatabase.DistributionTypeEnum == DistributionType.Alpha
                        ? segment.Alpha83PlatformVersionReference.WindowsThinClientDownloadLink
                        : segment.Stable83PlatformVersionReference.WindowsThinClientDownloadLink
                    : segment.Stable83PlatformVersionReference.WindowsThinClientDownloadLink,
                _ => null
            };
        }

        /// <summary>
        /// Возвращает путь файлового хранилища клиентских файлов.
        /// </summary>
        /// <returns>Путь файлового хранилища клиентских файлов</returns>
        public string GetBackupStorage(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);
            var item = dbLayer.CloudServicesBackupStorageRepository.FirstOrDefault(x => x.ID == segment.BackupStorageID);
            return item.ConnectionAddress;
        }

        #region Private

        /// <summary>
        /// Возвращает объект CloudServicesContentServer по сегменту аккаунта
        /// </summary>
        /// <returns>CloudServicesContentServer по сегменту аккаунта</returns>
        private CloudServicesContentServer GetAccountCloudContentServer(Domain.DataModels.Account account)
        {
            var segment = GetAccountSegment(account);
            return dbLayer.CloudServicesContentServerRepository.FirstOrDefault(x => x.ID == segment.ContentServerID);
        }

        /// <summary>
        /// Получить сегмент текущего аккаунта
        /// </summary>
        /// <returns>Сегмент текущего аккаунта</returns>
        private CloudServicesSegment GetAccountSegment(Domain.DataModels.Account account)
        {
            if (account == null)
                throw new InvalidOperationException("Не определен аккаунт, для получения сегмента");

            return _accountConfigurationDataProvider.GetAccountSegment(account.Id);
        }

        /// <summary>
        /// Получить сервер предприятия
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера предприятия</param>
        /// <returns>Сервер предприятия</returns>
        private CloudServicesEnterpriseServer GetEnterpriseServer(Guid enterpriseServerId) =>
            dbLayer.CloudServicesEnterpriseServerRepository.GetEnterpriseServer(enterpriseServerId);

        /// <summary>
        /// Получить сервер предприятия
        /// </summary>
        /// <param name="func">Функция для получения сервера предприятия</param>
        /// <returns>Сервер предприятия</returns>
        private string GetEnterpriseServer(Func<CloudServicesSegment, Guid> func, Domain.DataModels.Account account)
        {
            if (account == null)
                return string.Empty;

            var segment = GetAccountSegment(account);
            var item = GetEnterpriseServer(func.Invoke(segment));
            return item?.ConnectionAddress ?? string.Empty;
        }

        #endregion
    }
}
