﻿using Clouds42.Common;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Утилита для сортиовки записей списка аккаунтов
    /// </summary>
    public static class AccountDatabasesForMigrationSortingUtility
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateEnumerable<AccountDatabaseMigrationItemDto>> SortingActions = new()
        {
            {AccountDatabasesForMigrationSortFieldNames.V82Name, SortByV82Name},
            {AccountDatabasesForMigrationSortFieldNames.Size, SortBySize},
            {AccountDatabasesForMigrationSortFieldNames.IsFile, SortByIsFile},
            {AccountDatabasesForMigrationSortFieldNames.Path, SortByPath}
        };

        /// <summary>
        /// Сортитровать выбранные записи информационных баз аккаунта
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи информационных баз аккаунта по полю V82Name</returns>
        private static IOrderedQueryable<AccountDatabaseMigrationItemDto> SortByDefault(IQueryable<AccountDatabaseMigrationItemDto> records)
            => records.OrderBy(row => row.V82Name);


        /// <summary>
        /// Сортитровать выбранные записи информационных баз аккаунта
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи информационных баз аккаунта по полю V82Name</returns>
        private static IOrderedQueryable<AccountDatabaseMigrationItemDto> SortByV82Name(IQueryable<AccountDatabaseMigrationItemDto> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.V82Name),
                SortType.Desc => records.OrderByDescending(row => row.V82Name),
                _ => SortByDefault(records)
            };
        }


        /// <summary>
        /// Сортитровать выбранные записи информационных баз аккаунта
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи информационных баз аккаунта по полю Size</returns>
        private static IOrderedQueryable<AccountDatabaseMigrationItemDto> SortBySize(IQueryable<AccountDatabaseMigrationItemDto> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Size),
                SortType.Desc => records.OrderByDescending(row => row.Size),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать выбранные записи информационных баз аккаунта
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи информационных баз аккаунта по полю IsFile</returns>
        private static IOrderedQueryable<AccountDatabaseMigrationItemDto> SortByIsFile(IQueryable<AccountDatabaseMigrationItemDto> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.IsFile),
                SortType.Desc => records.OrderByDescending(row => row.IsFile),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать выбранные записи информационных баз аккаунта
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи информационных баз аккаунта по полю Path</returns>
        private static IOrderedQueryable<AccountDatabaseMigrationItemDto> SortByPath(IQueryable<AccountDatabaseMigrationItemDto> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Path),
                SortType.Desc => records.OrderByDescending(row => row.Path),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Производит сортировку выбранных записей списка информационных баз аккаунта
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи  списка информационных баз аккаунта</returns>
        public static IQueryable<AccountDatabaseMigrationItemDto> MakeSorting(IQueryable<AccountDatabaseMigrationItemDto> records, SortingDataDto sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();

            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate)
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }

    }
}