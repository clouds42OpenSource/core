﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.Accounts.Account.Helpers
{
    public static class ClientFileStoragePathCreator
    {

        /// <summary>
        /// Получить полный путь к файловому хранилищу аккаунта.
        /// </summary>
        public static string Create(IAccount account, IConnection connection)
            => Path.Combine(connection.ConnectionAddress, $@"company_{account.IndexNumber}\filestorage");
    }
}
