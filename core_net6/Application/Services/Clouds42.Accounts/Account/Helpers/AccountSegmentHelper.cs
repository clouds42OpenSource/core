﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;

namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Хелпер для работы с сегментом аккаунта
    /// </summary>
    public class AccountSegmentHelper(
        AccountDataProvider accountDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
    {
        /// <summary>
        /// Возвращает полный путь к папке клиентских файлов (с учетом CustomFileStoragePath для VIP).
        /// </summary>
        /// <returns></returns>
        public string GetFullClientFileStoragePath(Guid accountId)
        {
            var account = accountDataProvider.GetAccountOrThrowException(accountId);
            var segment = accountConfigurationDataProvider.GetAccountSegment(accountId);
            var res = Path.Combine(GetAccountFileStoragePath(account), segment.ClientFileFolder);
            return res;
        }

        /// <summary>
        /// Возвращает путь файлового хранилища клиентских файлов
        /// </summary>
        /// <returns></returns>
        public string GetAccountFileStoragePath(Guid accountId)
        {
            var account = accountDataProvider.GetAccountOrThrowException(accountId);
            return GetAccountFileStoragePath(account);
        }

        /// <summary>
        /// Возвращает путь файлового хранилища клиентских файлов
        /// </summary>
        /// <returns></returns>
        public string GetAccountFileStoragePath(Domain.DataModels.Account account)
        {
            var fileStorage = accountConfigurationDataProvider.GetAccountSegment(account.Id).CloudServicesFileStorageServer;

            if (fileStorage == null)
                throw new InvalidOperationException($"FileStorage in account {account.IndexNumber} is null");

            return Path.Combine(fileStorage.ConnectionAddress, $"company_{account.IndexNumber}");
        }
    }
}