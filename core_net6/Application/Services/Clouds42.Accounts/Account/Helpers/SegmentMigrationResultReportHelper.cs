﻿using System.Text;
using Clouds42.Configurations;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Хэлпер для отчета о миграции аккаунтов между сегментами
    /// </summary>
    public class SegmentMigrationResultReportHelper(
        IUnitOfWork dbLayer,
        IMessagesManager messagesManager,
        ILogger42 logger)
    {
        /// <summary>
        /// Отправить отчет воркера с результатами миграции аккаунтов
        /// </summary>
        /// <param name="accountIdList">Список аккаунтов</param>
        /// <param name="migrationResults">Реузльтаты миграции</param>
        /// <param name="targetSegment">Целевой сегмент для миграции</param>
        public void SendMigrationResultWorkerReport(List<Guid> accountIdList, List<MigrationResultDto> migrationResults,
            CloudServicesSegment targetSegment)
        {
            var accountsIndex = GetAccountList(accountIdList);

            var messageBuilder = new StringBuilder();

            messageBuilder.Append($"<p>Миграция аккаунтов в сегмент {targetSegment.Name}:</p>");

            if (migrationResults.Any(p => !p.Successful))
            {
                messageBuilder.Append("<p>Ошибки при миграции аккаунтов:</p>");
                messageBuilder.Append("<ul>");
                foreach (var migrationResult in migrationResults.Where(p => !p.Successful))
                {
                    messageBuilder.Append(
                        $"<li>{accountsIndex[migrationResult.AccountId]}. Ошибка: <br>{migrationResult.ErrorMessage}</li>");
                }

                messageBuilder.Append("</ul>");
            }

            var serverDbs = GetServerAccountDatabases(accountIdList);

            if (migrationResults.Any(p => p.Successful))
            {
                messageBuilder.Append("<p>Миграция завершена без ошибок:</p>");
                foreach (var migrationResult in migrationResults.Where(p => p.Successful))
                {
                    messageBuilder.Append(serverDbs.Any(d => d.AccountId == migrationResult.AccountId)
                        ? $"<p>{accountsIndex[migrationResult.AccountId]} (Есть серверные базы)</p>"
                        : $"<p>{accountsIndex[migrationResult.AccountId]}</p>");

                    if (!string.IsNullOrEmpty(migrationResult.ErrorMessage))
                        messageBuilder.Append($"<p>{migrationResult.ErrorMessage}</p>");
                }
            }

            var emailSubject = "Миграция завершена";
            if (serverDbs.Any(s =>
                migrationResults.Where(rd => rd.Successful).Select(r => r.AccountId).Contains(s.AccountId)))
            {
                emailSubject += " (Есть серверные базы)";
            }

            SendReport(messageBuilder.ToString(), emailSubject);
        }

        /// <summary>
        /// Отправить письмо
        /// </summary>
        /// <param name="report">Отчет</param>
        /// <param name="emailSubject">Тема письма</param>
        private void SendReport(string report, string emailSubject)
        {
            var email = ConfigurationHelper.GetConfigurationValue("SegmentMigrationReportMail");
            logger.Debug(report);

            messagesManager.SendWorkerReport(report, emailSubject, email);
        }

        /// <summary>
        /// Получить список аккаунтов
        /// </summary>
        /// <param name="accountIdList">Список ID аккаунтов</param>
        /// <returns>Список аккаунтов</returns>
        private Dictionary<Guid, int> GetAccountList(List<Guid> accountIdList)
            => dbLayer.AccountsRepository.Where(a => accountIdList.Contains(a.Id))
                .ToDictionary(k => k.Id, v => v.IndexNumber);

        /// <summary>
        /// Получить список серверных баз
        /// </summary>
        /// <param name="accountIdList">Список ID аккаунтов</param>
        /// <returns>Список серверных баз</returns>
        private List<Domain.DataModels.AccountDatabase> GetServerAccountDatabases(List<Guid> accountIdList)
            => dbLayer.DatabasesRepository.Where(d =>
                accountIdList.Contains(d.AccountId) && (!d.IsFile.HasValue || !d.IsFile.Value)
                                                    && d.AccountDatabaseOnDelimiter == null &&
                                                    d.State == DatabaseState.Ready.ToString()).ToList();
    }
}