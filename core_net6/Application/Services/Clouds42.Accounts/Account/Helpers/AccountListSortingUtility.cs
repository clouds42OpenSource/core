﻿using Clouds42.Common;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Утилита для сортиовки записей списка аккаунтов
    /// </summary>
    public static class AccountListSortingUtility
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateEnumerable<AccountInfoDataRowDto>> SortingActions = new()
        {
            {AccountListSortFieldNames.AccountRegistrationDate, SortByAccountRegistrationDate},
            {AccountListSortFieldNames.Rent1CExpiredDate, SortByRent1CExpiredDate}
        };

        /// <summary>
        /// Сортитровать записи списка аккаунтов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи списка аккаунтов</returns>
        private static IOrderedQueryable<AccountInfoDataRowDto> SortByDefault(IQueryable<AccountInfoDataRowDto> records)
            => records.OrderByDescending(row => row.AccountRegistrationDate);


        /// <summary>
        /// Сортитровать выбранные записи списка аккаунтов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей списка аккаунтов по полю RegistrationDate</returns>
        private static IOrderedQueryable<AccountInfoDataRowDto> SortByAccountRegistrationDate(IQueryable<AccountInfoDataRowDto> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.AccountRegistrationDate),
                SortType.Desc => records.OrderByDescending(row => row.AccountRegistrationDate),
                _ => SortByDefault(records)
            };
        }


        /// <summary>
        /// Сортитровать выбранные записи списка аккаунтов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей списка аккаунтов по полю </returns>
        private static IOrderedQueryable<AccountInfoDataRowDto> SortByRent1CExpiredDate(IQueryable<AccountInfoDataRowDto> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Rent1CExpiredDate),
                SortType.Desc => records.OrderByDescending(row => row.Rent1CExpiredDate),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Производит сортировку выбранных записей списка аккаунтов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи  списка аккаунтов</returns>
        public static IOrderedQueryable<AccountInfoDataRowDto> MakeSorting(IQueryable<AccountInfoDataRowDto> records, SortingDataDto? sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();

            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate)
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }

    }
}
