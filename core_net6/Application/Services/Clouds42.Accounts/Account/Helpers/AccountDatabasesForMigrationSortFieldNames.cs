﻿namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки списка аккаунтов
    /// </summary>
    public static class AccountDatabasesForMigrationSortFieldNames
    {
        /// <summary>
        ///  Номер информационной базы
        /// </summary>
        public const string V82Name = "v82name";

        /// <summary>
        /// Размер базы
        /// </summary>
        public const string Size = "size";

        /// <summary>
        /// Тип хранилища, true - файловое, false - серверное
        /// </summary>
        public const string IsFile = "isfile";

        /// <summary>
        /// Путь по которому располагается ИБ
        /// </summary>
        public const string Path = "path";
    }
}