﻿namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки списка аккаунтов
    /// </summary>
    public static class AccountListSortFieldNames
    {
        /// <summary>
        ///  Дата регистрации
        /// </summary>
        public const string AccountRegistrationDate = "accountregistrationdate";

        /// <summary>
        /// Дата истечение срока действия
        /// </summary>
        public const string Rent1CExpiredDate = "rent1cexpireddate";
    }
}