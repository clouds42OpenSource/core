﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using ResourceType = CommonLib.Enums.ResourceType;

namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Хелпер для менеджера AccountDatabaseUserAccessManager, по предоставлению/удалению доступов к базе
    /// </summary>
    public class AcDbAccessHelper(
        IUnitOfWork dbLayer,
        IAcDbDelimitersManager acDbDelimitersManager,
        SynchronizeAccountDatabaseAccessWithTardisHelper synchronizeAccountDatabaseAccessWithTardisHelper,
        IWebAccessHelper webAccessHelper,
        IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        IAcDbAccessProvider acDbAccessProvider,
        IResourceDataProvider resourceDataProvider,
        ICloudLocalizer cloudLocalizer,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : IAcDbAccessHelper
    {
        private readonly Lazy<string> _routeForOpenRent1CPage = new(Configurations.Configurations.CloudConfigurationProvider.Cp.GetRouteForOpenRent1CPage);

        /// <summary>
        /// Предоставление доступов внутренним пользователям
        /// </summary>
        public ManageAccessResultDto GrandInternalAccessToDb(Domain.DataModels.AccountDatabase accountDatabase, AccountUser accountUser)
        {

            if (!CanAddAccess(accountDatabase, accountUser, out var manageAccessResult))
                return manageAccessResult;

            var acDbAccessPostAddModel = CreateAcDbAccessPostAddModelObj(accountDatabase.Id, accountDatabase.AccountId, accountUser.Id);
            ProcessAcDbAccess(accountDatabase, acDbAccessPostAddModel, accountUser.Id);
            return CreateManageAccessResultObj(true);
        }

        /// <summary>
        /// Предоставление доступов внешним пользователям
        /// </summary>
        public ManageAccessResultDto GrandExternalAccessToDb(Domain.DataModels.AccountDatabase accountDatabase, string accountUserEmail)
        {
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(f => f.Email == accountUserEmail);
            if (accountUser == null)
                return CreateManageAccessResultObj(false, $"Не найден пользователь '{accountUserEmail}'");

            if (accountDatabase.AccountId == accountUser.AccountId)
                return CreateManageAccessResultObj(false,
                    $"Пользователь \"{accountUserEmail}\" является пользователем вашего аккаунта \"{accountUser.Account.AccountCaption}\".");

            if (!CanAddAccess(accountDatabase, accountUser, out var manageAccessResult))
                return manageAccessResult;

            var acDbAccessPostAddModel =
                CreateAcDbAccessPostAddModelObj(accountDatabase.Id, accountUser.AccountId, accountUser.Id);
            ProcessAcDbAccess(accountDatabase, acDbAccessPostAddModel, accountUser.Id);

            return new ManageAccessResultDto
            {
                Success = true,
                UserId = accountUser.Id.ToString(),
                UserEmail = accountUserEmail,
                UserName = accountUser.FullName,
                DatabasesName = accountDatabase.V82Name
            };
        }

        /// <summary>
        /// Предоставление доступа всем внутренним пользователям
        /// </summary>
        /// <param name="accountDatabase"></param>
        /// <returns></returns>
        public GrandAccessForAllUsersOfAccountResultDc GrandAccessForAllUsersOfAccount(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var result = new GrandAccessForAllUsersOfAccountResultDc();

            var usersQuery =
                from user in dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                    .Where(au => au.AccountId == accountDatabase.AccountId)
                join access in
                    dbLayer.AcDbAccessesRepository.WhereLazy(
                        a =>
                            a.AccountDatabaseID == accountDatabase.Id &&
                            a.AccountUserID != null &&
                            a.AccountID == accountDatabase.AccountId)
                    on user.Id equals access.AccountUserID into joinedAccess
                from access in joinedAccess.DefaultIfEmpty()
                where access == null
                select user;

            var usersActivQuery =
                from user in dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                    .Where(au => au.AccountId == accountDatabase.AccountId)
                join access in
                    dbLayer.AcDbAccessesRepository.WhereLazy(
                        a =>
                            a.AccountDatabaseID == accountDatabase.Id &&
                            a.AccountUserID != null &&
                            a.AccountID == accountDatabase.AccountId)
                    on user.Id equals access.AccountUserID into joinedAccess
                from access in joinedAccess.DefaultIfEmpty()
                join res in
                    dbLayer.ResourceRepository.WhereLazy(
                        r =>
                            r.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise &&
                            (r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb ||
                             r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser))
                    on user.Id equals res.Subject into joinedRes
                from res in joinedRes.DefaultIfEmpty()
                where access == null && res != null
                select user;

            var accountUsersActivList = usersActivQuery.Distinct().ToList();
            var accountUsersList = usersQuery.Distinct().ToList();

            if (!accountUsersList.Any())
            {
                result.Success = false;
                result.Message = "Все пользователи аккаунта уже имеют доступ к базе";
                return result;
            }

            if (!accountUsersActivList.Any() && accountUsersList.Any())
            {
                result.Success = false;
                result.Message = "Доступ не у всех";
                return result;
            }

            if (accountDatabase.IsDelimiter())
            {
                throw new NotImplementedException(
                    "Функция предоставления доступа всем пользователя аккаунта для базы на разделителях недоступна");
            }
            try
            {
                foreach (var accountUser in accountUsersActivList)
                {
                    acDbAccessProvider.GetOrCreateAccess(new AcDbAccessPostAddModelDto
                    {
                        AccountDatabaseID = accountDatabase.Id,
                        AccountID = accountDatabase.AccountId,
                        LocalUserID = Guid.Empty,
                        AccountUserID = accountUser.Id,
                        SendNotification = true
                    });
                }

                dbLayer.Save();

                result.Success = true;
                if (accountUsersList.Count > accountUsersActivList.Count)
                    result.Message = "Доступ не у всех";

                result.AccountUsers = accountUsersActivList.Select(au => new AccessUserInfoDc
                {
                    UserEmail = au.Email,
                    UserId = au.Id,
                    UserName = au.Name
                }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread()
                .GrandAccessDatabaseHandle(accountDatabase.Id);

            return result;
            
        }

        /// <summary>
        /// Удаление доступов пользователя в базе.
        /// </summary>
        /// <param name="accountDatabase">Информационная база.</param>
        /// <param name="accountUser">Пользователь.</param>
        public ManageAccessResultDto DeleteAccessFromDb(Domain.DataModels.AccountDatabase accountDatabase, AccountUser accountUser)
        {
            if (accountDatabase.IsDelimiter())
            {
                var result = acDbDelimitersManager.DeleteAccessFromDelimiterDatabase(accountDatabase, accountUser.Id);
                if (result.Error)
                    throw new InvalidOperationException(result.Message);
            }
            else
            {
                ManagePublishedDatabaseAccesses(accountDatabase, [], [accountUser.Id]);
                acDbAccessProvider.DeleteAccessIfExist(accountDatabase.Id, accountUser.Id);
            }

            synchronizeAccountDatabaseAccessWithTardisHelper.SynchronizeRemoveAccessAccountDatabaseViaNewThread(
                accountDatabase.Id, accountUser.Id);
            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().ExecuteInBackgroundThread()
                .DeleteAccessDatabaseHandle(accountDatabase.Id, accountUser.Id);

            return new ManageAccessResultDto
            {
                Success = true
            };
        }

        /// <summary>
        /// Управление доступом к опубликованной базе
        /// </summary>
        /// <param name="accountDatabase">Модель информационной базы</param>
        /// <param name="usersForGrandAccess">Список пользователей для предоставления доступа</param>
        /// <param name="usersForDeleteAccess">Список пользователей для удаления доступа</param>
        private void ManagePublishedDatabaseAccesses(Domain.DataModels.AccountDatabase accountDatabase, List<Guid> usersForGrandAccess,
            List<Guid> usersForDeleteAccess)
        {
            if (accountDatabase.PublishStateEnum != PublishState.Published)
                return;

            webAccessHelper.ManageAccess([accountDatabase], usersForGrandAccess, usersForDeleteAccess);
        }


        /// <summary>
        /// Обработать доступ к инф. базе
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="acDbAccessPostAddModel">Модель параметров добавления доступа</param>
        /// <param name="accountUserId">ID пользователя</param>
        private void ProcessAcDbAccess(Domain.DataModels.AccountDatabase accountDatabase,
            AcDbAccessPostAddModelDto acDbAccessPostAddModel, Guid accountUserId)
        {
            if (accountDatabase.IsDelimiter())
            {
                var result = acDbDelimitersManager.AddAccessToDbOnDelimiters(acDbAccessPostAddModel, accountDatabase);
                if (result.Error)
                    throw new InvalidOperationException(result.Message);
            }
            else
            {
                ManagePublishedDatabaseAccesses(accountDatabase, [accountUserId], []);

                acDbAccessProvider.GetOrCreateAccess(acDbAccessPostAddModel);
            }

            synchronizeAccountDatabaseAccessWithTardisHelper.SynchronizeGrandAccessAccountDatabaseViaNewThread(
                accountDatabase.Id, accountUserId);
            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread()
                .GrandAccessDatabaseHandle(accountDatabase.Id);
        }

        /// <summary>
        /// Признак что доступ может быть добавлен
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="manageAccessResult">Модель результата проверки</param>
        /// <returns>Результат проверки</returns>
        private bool CanAddAccess(Domain.DataModels.AccountDatabase accountDatabase, AccountUser accountUser,
            out ManageAccessResultDto manageAccessResult)
        {
            if (accountDatabase.CanAddAccessToDb())
            {
                var reasonText = accountDatabase.IsDeleted()
                    ? "удалена"
                    : "в архиве";
                manageAccessResult = CreateManageAccessResultObj(false,
                    $"Доступ не добавлен, так как инф. база {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} {reasonText}!");
                return false;
            }

            var dbAccess = dbLayer.AcDbAccessesRepository.FirstOrDefault(a =>
                a.AccountDatabaseID == accountDatabase.Id && a.AccountUserID == accountUser.Id &&
                a.AccountID == accountDatabase.AccountId);
            if (dbAccess != null)
            {
                manageAccessResult = CreateManageAccessResultObj(false, "Доступ уже добавлен");
                return false;
            }

            return CheckAbilityAddAccessByRent1CActivity(accountDatabase.AccountId, accountUser,
                out manageAccessResult);
        }

        /// <summary>
        /// Проверить возможность выдать доступ пользователю в базу
        /// по активности Анреды 1С у пользователя
        /// </summary>
        /// <param name="accountOwnerId">Id аккаунта владельца базы</param>
        /// <param name="accountUser">Пользователь для проверки</param>
        /// <param name="manageAccessResult">Модель результата проверки</param>
        /// <returns>Возможность выдать доступ по активности Анреды 1С</returns>
        private bool CheckAbilityAddAccessByRent1CActivity(Guid accountOwnerId, AccountUser accountUser,
            out ManageAccessResultDto manageAccessResult)
        {
            var isAnyResources = resourceDataProvider.GetResourcesLazy(Clouds42Service.MyEnterprise,
                r => r.Subject == accountUser.Id,
                ResourceType.MyEntUserWeb,
                ResourceType.MyEntUser).Any();

            if (isAnyResources)
            {
                manageAccessResult = CreateManageAccessResultObj(true);
                return true;
            }

            var url = new Uri(
                    $"{localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountOwnerId)}/{_routeForOpenRent1CPage.Value}")
                .AbsoluteUri;

            var rent1CName = $"“{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C,accountOwnerId)}”";
            var rent1CHtmlLink = $"<a href='{url}' target='_blank'>{rent1CName}</a>";
            manageAccessResult = CreateManageAccessResultObj(false,
                $"Для внешнего пользователя  \"({accountUser.Email})\" сервис {rent1CName} неактивен. Вы можете проспонсировать его на вкладке {rent1CHtmlLink}");

            return false;
        }

        /// <summary>
        /// Создать результат добавления доступа
        /// </summary>
        /// <param name="isSuccess">Признак что операция успешна</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Результат добавления доступа</returns>
        private static ManageAccessResultDto CreateManageAccessResultObj(bool isSuccess, string message = null)
            => new()
            {
                Success = isSuccess,
                Message = message ?? string.Empty
            };

        /// <summary>
        /// Создать модель параметров добавления доступа
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Модель параметров добавления доступа</returns>
        private static AcDbAccessPostAddModelDto CreateAcDbAccessPostAddModelObj(Guid accountDatabaseId, Guid accountId,
            Guid accountUserId)
            => new()
            {
                AccountDatabaseID = accountDatabaseId,
                AccountID = accountId,
                LocalUserID = Guid.Empty,
                AccountUserID = accountUserId,
                SendNotification = true
            };
    }
}
