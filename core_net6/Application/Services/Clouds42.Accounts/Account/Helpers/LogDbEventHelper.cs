﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Domain.DataModels;

namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Хелпер для генерации сообщений логов при ддобавлении/удалении доступов к базам 1С
    /// </summary>
    public class LogDbEventHelper
    {
        /// <summary>
        /// Получить сообщение об успешном удалении доступа
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="accountUser">Пользователь</param>
        /// <returns>Сообщение об успешном удалении</returns>
        public string GenerateMessageAboutSuccessAccessDeletionFromDb(Domain.DataModels.AccountDatabase accountDatabase, AccountUser accountUser)
        {
            return accountUser.AccountId == accountDatabase.AccountId ?
                $"Удален доступ внутреннему пользователю \"{accountUser.Login}\" к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}"
                : $"Удален доступ внешнему пользователю \"{accountUser.Email}\" к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}";

        }

        /// <summary>
        /// Получить сообщение об ошибке при удалении доступа
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="accountUser">Пользователь</param>
        /// <returns>Сообщение об ошибке при удалении</returns>
        public string GenerateMessageAboutFailedDeletionFromDb(Domain.DataModels.AccountDatabase accountDatabase, AccountUser accountUser, string errorMessage)
        {
            return accountUser.AccountId == accountDatabase.AccountId ?
                $"Доступ внутреннего пользователя '{accountUser.Login}' к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не удален по причине: '{errorMessage}'"
                : $"Доступ внешнего пользователя '{accountUser.Login}' к базе {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} не удален по причине: '{errorMessage}'";

        }
    }
}
