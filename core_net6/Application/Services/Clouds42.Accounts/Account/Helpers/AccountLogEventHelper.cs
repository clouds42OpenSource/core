﻿using Clouds42.DataContracts.Account;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Account.Helpers
{
    /// <summary>
    /// Хелпер для генерации сообщений логов при прикреплении/откреплении менеджера к аккаунту
    /// </summary>
    public class AccountLogEventHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить сообщение логирования при прикреплении менеджера к аккаунту
        /// </summary>
        /// <param name="accountSaleManagerDto">Модель данных менеджера прикрепленного к аккаунту</param>
        /// <returns>Сообщение о прикреплении менеджера к аккаунту</returns>
        public string GenerateMessageForAttachSaleManagerToAccount(AccountSaleManagerDto accountSaleManagerDto)
        {
            return
                $"Ведущий \"{accountSaleManagerDto.Login}({accountSaleManagerDto.Division})\" закреплен за аккаунтом \"{accountSaleManagerDto.AccountNumber}\".";
        }

        /// <summary>
        /// Получить сообщение логирования при откреплении менеджера от аккаунта
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        /// <returns>Сообщение об откреплении менеджера к аккаунту</returns>
        public string GenerateMessageForRemoveSaleManagerFromAccount(int accountNumber)
        {
            var accountSaleManagerDto = GetAccountSaleManagerDto(accountNumber);

            return accountSaleManagerDto == null ?
                $"Не удалось получить аккаунт или его менеджера по номеру аккаунта {accountNumber}"
                : $"Открепление менеджера {accountSaleManagerDto.Login}({accountSaleManagerDto.Division}) от аккаунта \"{accountSaleManagerDto.AccountNumber}\".";
        }

        /// <summary>
        /// Получить модель AccountSaleManagerDto по номеру аккаунта
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        /// <returns>Модель прикрепленного менеджера </returns>
        private AccountSaleManagerDto GetAccountSaleManagerDto(int accountNumber)
        {
            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.IndexNumber == accountNumber);

            if (account == null)
                return null;

            var accountSaleManager =
                dbLayer.AccountSaleManagerRepository.FirstOrDefault(manager => manager.AccountId == account.Id);

            if (accountSaleManager == null)
                return null;

            return new AccountSaleManagerDto
            {
                AccountNumber = accountNumber,
                Division = accountSaleManager.Division,
                Login = accountSaleManager.AccountUser.Login
            };
        }
    }
}
