﻿using Clouds42.Domain.Enums;
using LinqExtensionsNetFramework;

namespace Clouds42.Accounts.Account.Specifications
{
    public static class AccountDatabasesSpecification
    {
        public static Spec<Domain.DataModels.AccountDatabase> BySearchLine(string? searchLine)
        {
            return new (rec => string.IsNullOrEmpty(searchLine) || rec.V82Name != null && rec.V82Name.Contains(searchLine) || rec.SizeInMB.ToString().Contains(searchLine));
        }

        public static Spec<Domain.DataModels.AccountDatabase> ByIsFile(bool? isTypeStorageFile)
        {
            return new (rec => !isTypeStorageFile.HasValue || rec.IsFile == isTypeStorageFile);
        }

        public static Spec<Domain.DataModels.AccountDatabase> ByAccountIndexNumber(int indexNumber)
        {
            return new (rec => rec.Account.IndexNumber == indexNumber);
        }

        public static Spec<Domain.DataModels.AccountDatabase> ByReadyState()
        {
            return new (rec => rec.State == DatabaseState.Ready.ToString());
        }

        public static Spec<Domain.DataModels.AccountDatabase> ByIsNotOnDelimiter()
        {
            return new (rec => rec.AccountDatabaseOnDelimiter == null);
        }
    }
}
