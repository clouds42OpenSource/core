﻿using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Requisites.Providers
{
    /// <summary>
    /// Провайдер для работы с данными реквизитов аккаунта
    /// </summary>
    internal class AccountRequisitesDataProvider(
        ICreateAccountRequisitesProvider createAccountRequisitesProvider,
        IUnitOfWork dbLayer)
        : IAccountRequisitesDataProvider
    {
        /// <summary>
        /// Получить реквизиты аккаунта
        /// или создать новые (если записи не существует)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Реквизиты аккаунта</returns>
        public AccountRequisites GetIfExistsOrCreateNew(Guid accountId)
        {
            var accountRequisites = dbLayer.GetGenericRepository<AccountRequisites>()
                .FirstOrDefault(requisites => requisites.AccountId == accountId);

            if (accountRequisites == null)
                return createAccountRequisitesProvider.Create(new AccountRequisitesDto
                {
                    AccountId = accountId
                });

            return accountRequisites;
        }
    }
}
