﻿using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Requisites.Providers
{
    /// <summary>
    /// Провайдер для обновления реквизитов аккаунта
    /// </summary>
    internal class UpdateAccountRequisitesProvider(
        IAccountRequisitesDataProvider accountRequisitesDataProvider,
        IUnitOfWork dbLayer)
        : IUpdateAccountRequisitesProvider
    {
        /// <summary>
        /// Обновить реквизиты аккаунта
        /// </summary>
        /// <param name="model">Модель реквизитов аккаунта</param>
        public void Update(AccountRequisitesDto model)
        {
            var accountRequisites = accountRequisitesDataProvider.GetIfExistsOrCreateNew(model.AccountId);
            accountRequisites.Inn = model.Inn;

            dbLayer.GetGenericRepository<AccountRequisites>().Update(accountRequisites);
            dbLayer.Save();
        }
    }
}