﻿using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.Requisites.Providers
{
    /// <summary>
    /// Провайдер для создания реквизитов аккаунта
    /// </summary>
    internal class CreateAccountRequisitesProvider(IUnitOfWork dbLayer) : ICreateAccountRequisitesProvider
    {
        /// <summary>
        /// Создать реквизиты аккаунта
        /// </summary>
        /// <param name="model">Модель создания реквизитов аккаунта</param>
        /// <returns>Реквизиты аккаунта</returns>
        public AccountRequisites Create(AccountRequisitesDto model)
        {
            var accountRequisites = new AccountRequisites
            {
                AccountId = model.AccountId,
                Inn = model.Inn
            };

            dbLayer.GetGenericRepository<AccountRequisites>().Insert(accountRequisites);
            dbLayer.Save();

            return accountRequisites;
        }
    }
}
