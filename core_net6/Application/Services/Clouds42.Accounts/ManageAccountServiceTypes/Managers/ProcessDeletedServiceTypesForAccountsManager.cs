﻿using Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Accounts.ManageAccountServiceTypes.Managers
{
    /// <summary>
    /// Провайдер для обработки удаленных услуг сервиса для аккаунтов
    /// </summary>
    public class ProcessDeletedServiceTypesForAccountsManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IProcessDeletedServiceTypesForAccountsProvider processDeletedServiceTypesProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider,
            dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Обработать удаленные услуги для аккаунтов
        /// </summary>
        /// <param name="processDeletedServiceTypesForAccountsDto">Модель обработки удаленных услуг для аккаунтов</param>
        /// <returns>Результат обработки</returns>
        public ManagerResult Process(ProcessDeletedServiceTypesForAccountsDto processDeletedServiceTypesForAccountsDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ProcessDeletedServiceTypesForAccounts);

                processDeletedServiceTypesProvider.Process(processDeletedServiceTypesForAccountsDto);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка обработки удаленных услуг сервиса '{processDeletedServiceTypesForAccountsDto.ServiceId}' для аккаунтов]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
