﻿using Clouds42.Accounts.BillingServiceTypeAvailability.Helpers;
using Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces;
using Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Accounts.ManageAccountServiceTypes.Providers
{
    /// <summary>
    /// Провайдер для обработки удаленных услуг сервиса для аккаунтов
    /// </summary>
    internal class ProcessDeletedServiceTypesForAccountsProvider(
        IBillingServiceTypeAvailabilityProvider billingServiceTypeAvailabilityProvider,
        BillingServiceTypeAvailabilityDataHelper billingServiceTypeAvailabilityDataHelper,
        IDeleteUnusedResourcesProcessor deleteUnusedResourcesProcessor,
        IResourcesService resourcesService,
        INotifyBeforeDeleteServiceTypesProvider notifyBeforeDeleteServiceTypesProvider,
        ICalculateDelayedDateForDeletingServiceTypesProvider calculateDelayedDateForDeletingServiceTypesProvider,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : IProcessDeletedServiceTypesForAccountsProvider
    {
        /// <summary>
        /// Обработать удаленные услуги для аккаунтов
        /// </summary>
        /// <param name="model">Модель обработки удаленных услуг для аккаунтов</param>
        public void Process(ProcessDeletedServiceTypesForAccountsDto model)
        {
            var message = $"Обработка удаленных услуг сервиса '{model.ServiceId}'";
            logger.Info(message);

            try
            {
                logger.Info($"Установка метки для услуг сервиса '{model.ServiceId} на удаление");
                MarkServiceTypesAsDeleted(model.ServiceId, model.ServiceTypeIds);

                var accountsWhichUsesDeletedServiceTypes =
                    billingServiceTypeAvailabilityDataHelper.GetAccountsWhichUsesDeletedServiceTypes(
                        model.ServiceId);

                logger.Info($"Пересчет стоимости сервиса '{model.ServiceId} для аккаунтов");
                RecalculateServiceCostForAccounts(model.ServiceId,
                    accountsWhichUsesDeletedServiceTypes.Where(account => !account.NeedDelayedDeletion)
                        .Select(account => account.AccountId).ToList());

                var delayedServiceTypesDeletionForAccounts =
                    GenerateDelayedServiceTypesDeletionForAccounts(model, accountsWhichUsesDeletedServiceTypes);

                logger.Info($"Установка времени активности услуг сервиса {model.ServiceId} для аккаунтов");
                billingServiceTypeAvailabilityProvider.SetAvailabilityDateTimeForAccounts(delayedServiceTypesDeletionForAccounts);

                logger.Info($"Уведомление о скором удалении услуг сервиса {model.ServiceId} для аккаунтов");
                notifyBeforeDeleteServiceTypesProvider.NotifyAccounts(delayedServiceTypesDeletionForAccounts);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка обработки удаленных услуг] сервиса '{model.ServiceId}");
                throw;
            }
        }

        /// <summary>
        /// Пометить услуги сервиса как удаленные
        /// </summary>
        /// <param name="billingServiceId">ID сервис биллинга</param>
        /// <param name="billingServiceTypesForRemove">Список Id услуг сервиса для удаления</param>
        protected void MarkServiceTypesAsDeleted(Guid billingServiceId, List<Guid> billingServiceTypesForRemove)
        {
            if (!billingServiceTypesForRemove.Any())
                return;

            var message = $"[{billingServiceId}] - Удаление услуг";
            try
            {
                billingServiceTypesForRemove.ForEach(ChangeServiceTypeAndRelationStatusToDeleted);
                dbLayer.Save();
                logger.Info($"{message} завершилось успешно.");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка удаления услуг сервиса]");
                throw;
            }
        }

        /// <summary>
        /// Сменить статус услуги и ее связи на удаленные
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        private void ChangeServiceTypeAndRelationStatusToDeleted(Guid serviceTypeId)
        {
            dbLayer.BillingServiceTypeRelationRepository
                .AsQueryable()
                .Where(st => st.ChildServiceTypeId == serviceTypeId || st.MainServiceTypeId == serviceTypeId)
                .ExecuteUpdate(x => x.SetProperty(z => z.IsDeleted, true));
            
            var serviceType = dbLayer.BillingServiceTypeRepository.FirstOrDefault(st => st.Id == serviceTypeId) ??
                              throw new NotFoundException($"Не удалось получить услугу по ID = '{serviceTypeId}'");

            serviceType.IsDeleted = true;
            dbLayer.BillingServiceTypeRepository.Update(serviceType);
        }

        /// <summary>
        /// Пересчитать стоимость сервиса для аккаунтов
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountIds">Список Id аккаунтов</param>
        private void RecalculateServiceCostForAccounts(Guid serviceId, List<Guid> accountIds)
        {
            if (!accountIds.Any())
                return;

            accountIds.ForEach(id =>
            {
                var resConf = resourcesService.GetResourcesConfigurationOrThrowException(id, serviceId);
                deleteUnusedResourcesProcessor.Process(resConf);
            });
        }

        /// <summary>
        /// Сформировать модель отложенного удаления
        /// услуг сервиса для аккаунтов
        /// </summary>
        /// <param name="model">Модель обработки удаленных услуг для аккаунтов</param>
        /// <param name="accounts">Список аккаунтов которые используют удаленную услугу</param>
        /// <returns>Модель отложенного удаления услуг сервиса для аккаунтов</returns>
        private DelayedServiceTypesDeletionForAccountsDto GenerateDelayedServiceTypesDeletionForAccounts(
            ProcessDeletedServiceTypesForAccountsDto model, List<AccountWhichUsesDeletedServiceTypesDto> accounts)
            => new()
            {
                ServiceId = model.ServiceId,
                ServiceTypeIds = model.ServiceTypeIds,
                BillingServiceChangesId = model.BillingServiceChangesId,
                AccountsForSetAvailabilityDateTime = accounts.Where(account => account.NeedDelayedDeletion)
                    .Select(account => new AccountWithDelayedDateOfRemoveServiceTypesDto
                    {
                        AccountId = account.AccountId,
                        DelayedDateForDeletingServiceTypes =
                            calculateDelayedDateForDeletingServiceTypesProvider.Calculate(account.Rent1CExpireDate)
                    }).ToList()
            };
    }
}
