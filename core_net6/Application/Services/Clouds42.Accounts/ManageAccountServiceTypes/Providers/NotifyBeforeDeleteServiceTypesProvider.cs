﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Services;
using Clouds42.Logger;

namespace Clouds42.Accounts.ManageAccountServiceTypes.Providers
{
    /// <summary>
    /// Провайдер для уведомления о скором
    /// удалении услуг сервиса
    /// </summary>
    internal class NotifyBeforeDeleteServiceTypesProvider(
        NotifyBeforeServiceChangesHelper notifyBeforeServiceChangesHelper,
        AccountDataProvider accountDataProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        IBillingServiceOwnerDataProvider billingServiceOwnerDataProvider,
        ILogger42 logger)
        : INotifyBeforeDeleteServiceTypesProvider
    {
        /// <summary>
        /// Уведомить аккаунты о скором удалении
        /// услуг сервиса
        /// </summary>
        /// <param name="model">Модель отложенного удаления услуг сервиса для аккаунтов</param>
        public void NotifyAccounts(DelayedServiceTypesDeletionForAccountsDto model)
        {
            if (!model.AccountsForSetAvailabilityDateTime.Any() || !model.ServiceTypeIds.Any())
                return;

            var service = notifyBeforeServiceChangesHelper.GetBillingServiceOrThrowException(model.ServiceId);

            var accountAdminOwnerOfService = billingServiceOwnerDataProvider.GetAccountAdminOwnerOfService(service);

            model.AccountsForSetAvailabilityDateTime.ForEach(accountInfo =>
            {
                var accountInfoForNotify = accountDataProvider.GetAccountInfoForNotifyDto(accountInfo.AccountId);

                var notifyBeforeDeleteServiceTypes = new BeforeDeleteServiceTypesLetterModelDto
                {
                    BillingServiceChangesId = model.BillingServiceChangesId,
                    ServiceName = service.Name,
                    ServiceOwnerEmail = accountAdminOwnerOfService.Email,
                    ServiceOwnerPhoneNumber = accountAdminOwnerOfService.PhoneNumber,
                    ServiceTypeNamesToRemove =
                        notifyBeforeServiceChangesHelper.GetServiceTypeNames(model.ServiceTypeIds),
                    UrlForOpenBillingServiceCard =
                        notifyBeforeServiceChangesHelper.GetUrlForOpenBillingServicePage(service.Id),
                    AccountId = accountInfo.AccountId,
                    ServiceTypesDeleteDate = accountInfo.DelayedDateForDeletingServiceTypes,
                    AccountUserId = accountInfoForNotify.AccountAdmin.Id,
                    EmailsToCopy = accountInfoForNotify.Emails
                };

                NotifyAccount(accountInfo.AccountId, notifyBeforeDeleteServiceTypes);
            });
        }

        /// <summary>
        /// Уведомить аккаунт о скором удалении услуг сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="beforeDeleteServiceTypesLetterModel">Модель письма о скором удалении услуг сервиса</param>
        private void NotifyAccount(Guid accountId,
            BeforeDeleteServiceTypesLetterModelDto beforeDeleteServiceTypesLetterModel)
        {
            logger.Info(
                $"Уведомление аккаунта {accountId} о скором удалении услуг сервиса {beforeDeleteServiceTypesLetterModel.ServiceName}");

            var sendStatus = letterNotificationProcessor
                .TryNotify<BeforeDeleteServiceTypesLetterNotification, BeforeDeleteServiceTypesLetterModelDto>(
                    beforeDeleteServiceTypesLetterModel);

            logger.Info(
                $"Отправили ли сообщение клиенту - {sendStatus}, Аккаунт - {accountId}, Пользователь - {beforeDeleteServiceTypesLetterModel.AccountUserId}");
        }
    }
}