﻿using Clouds42.DataContracts.Service.Market;
using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;

namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Валидатор моделей шаблона письма
    /// </summary>
    public static class LetterTemplateDtoValidator
    {
        /// <summary>
        /// Попытаться выполнить валидацию модели
        /// </summary>
        /// <param name="letterTemplateDataForNotificationModel">Модель данных шаблона письма</param>
        /// <param name="errorMessage">Собщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public static bool TryValidateModel(this ILetterTemplateDataForNotificationModelDto letterTemplateDataForNotificationModel, out string errorMessage)
        {
            errorMessage = "";

            if (string.IsNullOrEmpty(letterTemplateDataForNotificationModel.Header))
            {
                errorMessage = "Укажите поле 'Заголовок письма'";
                return false;
            }

            if (string.IsNullOrEmpty(letterTemplateDataForNotificationModel.Body))
            {
                errorMessage = "Укажите поле 'Тело письма'";
                return false;
            }

            if (string.IsNullOrEmpty(letterTemplateDataForNotificationModel.SendGridTemplateId))
            {
                errorMessage = "Укажите поле 'ID шаблона письма в SendGrid'";
                return false;
            }

            if (string.IsNullOrEmpty(letterTemplateDataForNotificationModel.Subject))
            {
                errorMessage = "Укажите поле 'Тема письма'";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Попытаться выполнить валидацию модели
        /// </summary>
        /// <param name="sendTestLetterDto">Модель данных шаблона письма</param>
        /// <param name="errorMessage">Собщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public static bool TryValidateModel(this SendTestLetterDto sendTestLetterDto, out string errorMessage)
        {
            if (sendTestLetterDto.LetterTemplateDataForNotification == null)
            {
                errorMessage = "Не указана модель данных шаблона письма";
                return false;
            }

            if (string.IsNullOrEmpty(sendTestLetterDto.Email))
            {
                errorMessage = "Укажите поле'Электронная почта'";
                return false;
            }

            return sendTestLetterDto.LetterTemplateDataForNotification.TryValidateModel(out errorMessage);
        }

        public static bool TryValidateModel(EditAdvertisingBannerTemplateDto editAdvertisingBannerTemplateDto, out string errorMessage)
        {
            errorMessage = "";

            if (string.IsNullOrEmpty(editAdvertisingBannerTemplateDto.Header))
            {
                errorMessage = "Укажите поле 'Заголовок баннера'";
                return false;
            }

            if (string.IsNullOrEmpty(editAdvertisingBannerTemplateDto.Body))
            {
                errorMessage = "Укажите поле 'Тело баннера'";
                return false;
            }

            if (string.IsNullOrEmpty(editAdvertisingBannerTemplateDto.Link))
            {
                errorMessage = "Укажите поле 'Ссылка баннера'";
                return false;
            }

            return true;
        }
    }
}
