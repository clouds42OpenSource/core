﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;

namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Провайдер для отправки тестового письма
    /// </summary>
    public interface ISendTestLetterProvider
    {
        /// <summary>
        /// Отправить тестовое письмо
        /// </summary>
        /// <param name="sendTestLetterDto">Модель данных для отправки тестового письма</param>
        void Send(SendTestLetterDto sendTestLetterDto);
    }
}
