﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;

namespace Clouds42.LetterNotification.Contracts
{

    /// <summary>
    /// Уведомление пользователя
    /// </summary>
    public interface IUserNotification
    {

        /// <summary>
        /// Уведомляемый пользователь.
        /// </summary>
        AccountUser AccountUser { get; }

        /// <summary>
        /// Текст сообщения уведомления
        /// </summary>        
        string GetEmailBody();

        /// <summary>
        /// Получить приложение
        /// </summary>        
        DocumentDataDto? GetAttachmentFile();

        /// <summary>
        /// Тема сообщение уведомления
        /// </summary>        
        string GetEmailTopic();

        /// <summary>
        /// Получить описание отправителя
        /// </summary>        
        string GetFromLable();

        /// <summary>
        /// Ключ уникальности события - для избежания дублей
        /// </summary>        
        string GetEventKey();

        /// <summary>
        /// До какой даты это уведомление будет актуальным (в случае о предупреждениях об оплате)
        /// </summary>        
        DateTime GetEventKeyPeriod();

        /// <summary>
        /// Сколько раз сообщать о событии в данном периоде
        /// </summary>        
        int GetNotifyCount();

    }
}
