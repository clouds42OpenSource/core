﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;

namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Провайдер для редактирования шаблона письма
    /// </summary>
    public interface IEditLetterTemplateProvider
    {
        /// <summary>
        /// Редактировать шаблон письма
        /// </summary>
        /// <param name="editLetterTemplateDto">Модель редактирования шаблона письма</param>
        void Edit(EditLetterTemplateDto editLetterTemplateDto);
    }
}
