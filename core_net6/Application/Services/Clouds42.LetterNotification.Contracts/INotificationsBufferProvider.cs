﻿namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Провайдер для работы с буффером уведомлений
    /// </summary>
    public interface INotificationsBufferProvider
    {
        /// <summary>
        /// Проверить является ли уведомление буфферизованным
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="eventKey">Ключ уведомления</param>
        /// <param name="allowedNotificationsInPeriodCount">
        /// Допустимое количество уведомлений за период</param>
        /// <returns>Уведомление буфферизованно</returns>
        bool IsBufferedNotifyEvent(Guid accountId, string eventKey, int allowedNotificationsInPeriodCount);

        /// <summary>
        /// Записать уведомление в буффер
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="eventKey">Ключ уведомления</param>
        /// <param name="eventKeyPeriod">
        /// Дата, до которой это уведомление будет актуальным</param>
        void WriteNotificationToBuffer(Guid accountId, string eventKey, DateTime eventKeyPeriod);
    }
}
