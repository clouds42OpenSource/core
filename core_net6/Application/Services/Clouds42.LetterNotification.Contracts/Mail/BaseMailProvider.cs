﻿using Clouds42.HandlerExeption.Contract; 
using Clouds42.SmtpClient;
using Clouds42.SmtpClient.Infrastructure;


namespace Clouds42.LetterNotification.Contracts.Mail
{
    /// <summary>
    ///     Базовый клиент почтового бренда
    /// </summary>
    public abstract class BaseMailProvider : IMailProvider
    {
        /// <summary>
        /// Провайдер записи логов
        /// </summary>
        private readonly IHandlerException _handlerException = HandlerException42.GetHandler();
        /// <summary>
        ///     Менеджер отправки сообщения
        /// </summary>
        private readonly ISmtpClientManager _smtpClient = new SendGridSmtpClientManager();

        /// <summary>
        ///     Сообщение
        /// </summary>
        private readonly SmtpClientMessage _smtpClientMessage = new();

        /// <summary>
        ///     Данные авторизации
        /// </summary>
        private SmtpClientCredentials SmtpClientCredentials => new()
        {
            Host = Host,
            Port = Port,
            Login = Login,
            Password = Password
        };

        /// <summary>
        ///     Кому
        /// </summary>
        /// <param name="email">Почта</param>
        public IMailProvider To(string email)
        {
            _smtpClientMessage.To = email;
            return this;
        }

        /// <summary>
        ///     Добавить в копию
        /// </summary>
        /// <param name="email">Почта</param>
        public IMailProvider Copy(string email)
        {
            if (!string.IsNullOrEmpty(email))
                _smtpClientMessage.Copies.Add(email);

            return this;
        }

        /// <summary>
        ///     Тема письма
        /// </summary>
        /// <param name="text">Текст</param>
        public IMailProvider Subject(string text)
        {
            _smtpClientMessage.Subject = text;
            return this;
        }

        /// <summary>
        ///     Тема письма сбоя ядра
        /// </summary>
        /// <param name="text">Текст</param>
        /// <returns>Почтовый провайдер</returns>
        public IMailProvider CoreFailSubject(string text)
        {
            _smtpClientMessage.Subject = $"Ядро сбой. {text}";
            return this;
        }

        /// <summary>
        ///     Тело письма
        /// </summary>
        /// <param name="text">Текст</param>
        public IMailProvider Body(string text)
        {
            _smtpClientMessage.Body = text;
            return this;
        }

        /// <summary>
        ///     Отображаемое название отправителя
        /// </summary>
        /// <param name="text">Текст</param>
        public IMailProvider DisplayName(string text)
        {
            _smtpClientMessage.DisplayName = text;
            return this;
        }

        /// <summary>
        ///     Добавить прикрепленный файл
        /// </summary>
        /// <param name="bytes">Файл в байтах</param>
        /// <param name="fileName">Имя файла</param>
        public IMailProvider Attachment(byte[] bytes, string fileName)
        {
            if (!string.IsNullOrEmpty(fileName) && bytes != null && bytes.Any())
                _smtpClientMessage.Attachments.Add(fileName, bytes);

            return this;
        }
        /// <summary>
        /// Отправить письмо в отдельном потоке
        /// </summary>
        public void SendViaNewThread()
        {
            Task.Factory.StartNew(async () =>
            {
                try
                {
                    await _smtpClient.SendMail(SmtpClientCredentials, _smtpClientMessage);
                }
                catch (Exception ex)
                {
                    _handlerException.Handle(ex, $"[Ошибка отправки письма на адрес] {_smtpClientMessage.To} от {SmtpClientCredentials.Login} с темой {_smtpClientMessage.Subject} в отдельном потоке произошла");
                }
            });
        }

        /// <summary>
        ///     Порт
        /// </summary>
        protected abstract int Port { get; }

        /// <summary>
        ///     Хост
        /// </summary>
        protected abstract string Host { get; }

        /// <summary>
        ///     Логин
        /// </summary>
        protected virtual string? Login { get; } = null;

        /// <summary>
        ///     Пароль
        /// </summary>
        protected virtual string? Password { get; } = null;
    }
}
