﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;

namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Провайдер для работы с информацией об уведомляемом пользователе
    /// </summary>
    public interface INotifiedAccountUserInfoProvider
    {
        /// <summary>
        /// Попытаться получить информацию об уведомляемом пользователе
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="notifiedAccountUserInfo">Информацию об уведомляемом пользователе</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="emailsToCopy">Электронные адреса в копию</param>
        /// <returns>Результат попытки получения</returns>
        bool TryGet(out NotifiedAccountUserInfoDto notifiedAccountUserInfo, Guid accountId, Guid? accountUserId = null,
            List<string>? emailsToCopy = null);
    }
}
