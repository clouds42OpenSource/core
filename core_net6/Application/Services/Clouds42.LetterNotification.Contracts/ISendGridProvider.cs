﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.SendGrid;

namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Провайдер для работы с SendGrid
    /// </summary>
    public interface ISendGridProvider
    {
        /// <summary>
        /// Отправить письмо
        /// </summary>
        /// <param name="commandExecuteRequestPayload">Полезная нагрузка для выполнения команды</param>
        ManagerResult SendEmail(CommandExecuteRequestPayloadDto commandExecuteRequestPayload);
        Task<ManagerResult> SendEmailAsync(CommandExecuteRequestPayloadDto commandExecuteRequestPayload);
    }
}
