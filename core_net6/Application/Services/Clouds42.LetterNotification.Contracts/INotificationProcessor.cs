﻿using Clouds42.DataContracts.Cloud42Services;

namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Обработчик пользовательский уведомлений
    /// </summary>
    public interface INotificationProcessor
    {
        /// <summary>
        /// Уведомить о скорой блокировке сервиса.
        /// </summary>        
        void NotifyBeforeLockService(BeforeLockInServicefoModelDto beforeLockInServicefoModel);
    }
}
