﻿using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.LetterNotification.Contracts
{
    public interface ILetterNotificationBase<in TModel> where TModel : ILetterTemplateModel
    {
        /// <summary>
        /// Попытаться выполнить уведомление
        /// </summary>
        /// <param name="model">Входящая модель</param>
        /// <returns>Результат выполнения уведомления</returns>
        bool TryNotify(TModel model);
    }
}

