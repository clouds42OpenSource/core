﻿namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Провайдер для работы с данными электронных аресов
    /// </summary>
    public interface IEmailAddressesDataProvider
    {
        /// <summary>
        /// Получить все электронные адреса аккаунта для отправки
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Все электронные адреса аккаунта для отправки</returns>
        List<string> GetAllAccountEmailsToSend(Guid accountId);

        /// <summary>
        /// Получить все электронные адреса аккаунта для отправки, в виде строки
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Все электронные адреса аккаунта для отправки, в виде строки</returns>
        string GetAllAccountEmailsToSendAsString(Guid accountId);
    }
}
