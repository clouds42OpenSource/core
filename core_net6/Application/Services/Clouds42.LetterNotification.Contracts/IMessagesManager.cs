﻿namespace Clouds42.LetterNotification.Contracts
{

    /// <summary>
    /// Менеджер сообщений
    /// </summary>
    public interface IMessagesManager
    {
        /// <summary>
        /// Отпарвить письмо воркера
        /// </summary>
        /// <param name="messageText">Текст сообщений</param>
        /// <param name="topiс">Тема сообщения</param>
        /// <param name="email">Почта, на которую необходимо отправить письмо</param>
        void SendWorkerMail(string messageText, string topiс, string email);

        /// <summary>
        /// Отправить отчет воркера
        /// </summary>
        /// <param name="messageText">Текст сообщений</param>
        /// <param name="topic">Тема сообщения</param>
        /// <param name="eMail">Почта, на которую необходимо отправить письмо</param>
        void SendWorkerReport(string messageText, string topic = "CoreWokrer report", string? eMail = null);
    }
}
