﻿using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Процессор для работы с уведомлениями по почте
    /// </summary>
    public interface ILetterNotificationProcessor
    {
        /// <summary>
        /// Попытаться выполнить уведомление по почте
        /// </summary>
        /// <typeparam name="TNotification">Тип уведомления</typeparam>
        /// <typeparam name="TModel">Входящая модель для уведомления</typeparam>
        /// <param name="model">Входящая модель</param>
        /// <returns>Результат выполнения уведомления</returns>
        bool TryNotify<TNotification, TModel>(TModel model) where TNotification : ILetterNotificationBase<TModel>
            where TModel : ILetterTemplateModel;
    }
}
