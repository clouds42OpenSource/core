﻿namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    ///     Интерфейс почтового провайдера
    /// </summary>
    public interface IMailProvider
    {
        /// <summary>
        ///     Кому
        /// </summary>
        /// <param name="email">Почта</param>
        IMailProvider To(string email);

        /// <summary>
        ///     Добавить в копию
        /// </summary>
        /// <param name="email">Почта</param>
        IMailProvider Copy(string email);

        /// <summary>
        ///     Тема письма
        /// </summary>
        /// <param name="text">Текст</param>
        IMailProvider Subject(string text);


        /// <summary>
        ///     Тема письма сбоя ядра
        /// </summary>
        /// <param name="text">Текст</param>
        /// <returns>Почтовый провайдер</returns>
        IMailProvider CoreFailSubject(string text);

        /// <summary>
        ///     Тело письма
        /// </summary>
        /// <param name="text">Текст</param>
        IMailProvider Body(string text);

        /// <summary>
        ///     Отображаемое название отправителя
        /// </summary>
        /// <param name="text">Текст</param>
        IMailProvider DisplayName(string text);

        /// <summary>
        ///     Добавить прикрепленный файл
        /// </summary>
        /// <param name="bytes">Файл в байтах</param>
        /// <param name="fileName">Имя файла</param>
        IMailProvider Attachment(byte[] bytes, string fileName);

        /// <summary>
        /// Отправить письмо в отдельном потоке
        /// </summary>
        void SendViaNewThread();
    }
}
