﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.DataModels.Mailing;
using Clouds42.Domain.DataModels.Marketing;

namespace Clouds42.LetterNotification.Contracts
{
    /// <summary>
    /// Провайдер для работы с шаблоном письма
    /// </summary>
    public interface ILetterTemplateProvider
    {
        /// <summary>
        /// Получить шаблон письма
        /// </summary>
        /// <param name="id">Id шаблона письма</param>
        /// <returns>Шаблон письма</returns>
        LetterTemplate GetLetterTemplate(int id);

        /// <summary>
        /// Получить модель Dto шаблона письма
        /// </summary>
        /// <param name="id">Id шаблона письма</param>
        /// <returns>Модель Dto шаблона письма</returns>
        LetterTemplateDetailsDto GetLetterTemplateDto(int id);

        /// <summary>
        /// Попытаться получить рекламный баннер по Id шаблона письма
        /// </summary>
        /// <param name="letterTemlateId">Id шаблона письма</param>
        /// <param name="advertisingBannerTemplate">Рекламный баннер</param>
        /// <returns>Результат получения рекламного баннера</returns>
        bool TryGetAdvertisingBannerTemplate(int letterTemlateId,
            out AdvertisingBannerTemplate? advertisingBannerTemplate);
    }
}
