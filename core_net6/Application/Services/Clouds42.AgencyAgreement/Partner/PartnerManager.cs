﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner
{
    /// <summary>
    /// Менеджер для работы с партнером
    /// </summary>
    public class PartnerManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IPartnerDataProvider partnerDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить сводную информацию для партнера
        /// </summary>
        /// <param name="accountId">Id аккаунта партнера</param>
        /// <returns>Сводная информация для партнера</returns>
        public ManagerResult<SummaryInformationForPartnerDto> GetSummaryInformationForPartner(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Partners_GetInfo, () => accountId);
                var summaryInformation = partnerDataProvider.GetSummaryInformationForPartner(accountId);

                return Ok(summaryInformation);
            }
            catch (Exception exception)
            {
                return PreconditionFailed<SummaryInformationForPartnerDto>(exception.Message);
            }
        }
    }
}
