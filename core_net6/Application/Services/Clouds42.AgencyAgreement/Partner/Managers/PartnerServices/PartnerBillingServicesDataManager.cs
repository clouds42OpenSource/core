﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.ResourceConfiguration;
using Clouds42.DataContracts.Service.Partner;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер данных партерских сервисов.
    /// </summary>
    public class PartnerBillingServicesDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IPartnerBillingServiceDataProvider partnerBillingServiceDataProvider,
        IPartnerBillingServiceTypeDataProvider partnerBillingServiceTypeDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить список сервисов у аккаунта.
        /// Сервисы принадлежащие партнеру.
        /// </summary>
        /// <param name="accountId">Номер аккаунта партнера.</param>
        /// <returns>Список номеров сервисов.</returns>      
        public ManagerResult<GuidListItemDto> GetAccountServicesList(Guid accountId)
        {

            AccessProvider.HasAccess(ObjectAction.BillingService_GetList, () => accountId);

            try
            {
                return Ok(partnerBillingServiceDataProvider.GetAccountServicesList(accountId));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<GuidListItemDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить детальную информацию по партнерскому сервису.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <returns>Информация по сервису.</returns>        
        public ManagerResult<ServiceInfoDto> GetServiceInfo(Guid serviceKey)
        {

            AccessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => AccountIdByServiceKey(serviceKey));

            try
            {
                return Ok(partnerBillingServiceDataProvider.GetServiceInfo(serviceKey));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ServiceInfoDto>(ex.Message);
            }

        }

        /// <summary>
        /// Получить список услуг партнерского сервиса.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <returns>Список услуг сервиса.</returns>        
        public ManagerResult<GuidListItemDto> GetServiceTypesList(Guid serviceKey)
        {

            AccessProvider.HasAccess(ObjectAction.BillingServiceType_GetList, () => AccountIdByServiceKey(serviceKey));

            try
            {
                return Ok(partnerBillingServiceDataProvider.GetServiceTypesList(serviceKey));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<GuidListItemDto>(ex.Message);
            }

        }

        /// <summary>
        /// Получить детальную информацию о услуге сервиса.
        /// </summary>
        /// <param name="serviceTypeKey">Ключ услуги сервиса.</param>
        /// <returns>Информация по услуге сервиса.</returns>
        public ManagerResult<ServiceTypeInfoDto> GetServiceTypeInfo(Guid serviceTypeKey)
        {

            AccessProvider.HasAccess(ObjectAction.BillingServiceType_GetInfo, () => AccountIdByServiceTypeKey(serviceTypeKey));

            try
            {
                return Ok(partnerBillingServiceTypeDataProvider.GetServiceTypeInfo(serviceTypeKey));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ServiceTypeInfoDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <param name="accountUserId">Номер пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        public ManagerResult<ServiceTypeStateForUserDto> GetServiceTypesStateForUserByKey(Guid serviceKey, Guid accountUserId)
        {

            AccessProvider.HasAccess(ObjectAction.BillingServiceType_GetStateForUser,
                () => AccountIdByAccountUser(accountUserId), optionalCheck: () => AccountIsServiceOwner(serviceKey));
            try
            {
                return Ok(partnerBillingServiceDataProvider.GetServiceTypesStateForUserByKey(serviceKey, accountUserId));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ServiceTypeStateForUserDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        public ManagerResult<ServiceTypeStateForUserDto> GetServiceTypeStateForUserByKey(Guid serviceKey, string accountUserLogin)
        {

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Login == accountUserLogin);

            if (accountUser == null)
                return NotFound<ServiceTypeStateForUserDto>($"Пользователь по логину '{accountUserLogin}' не найден");

            AccessProvider.HasAccess(ObjectAction.BillingServiceType_GetStateForUser, () => accountUser.AccountId,
                optionalCheck: () => AccountIsServiceOwner(serviceKey));

            try
            {
                return Ok(partnerBillingServiceDataProvider.GetServiceTypeStateForUserByKey(serviceKey, accountUserLogin));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ServiceTypeStateForUserDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить информацию о сервисе на аккаунт по пользователю
        /// </summary>
        /// <param name="serviceId">Для какого сервиса</param>
        /// <param name="accountId">Для какого аккаунта </param>
        /// <returns>Информацию о сервисе на аккаунт</returns>
        public ManagerResult<AccountServiceDto> GetServiceData(Guid serviceId, Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.BillingServiceType_GetStateForUser, () => accountId);

            try
            {
                return Ok(partnerBillingServiceDataProvider.GetServiceData(serviceId, accountId));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<AccountServiceDto>(ex.Message);
            }
        }


        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceId">Id сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        public ManagerResult<ServiceTypeStateForUserDto> GetServiceTypeStateForUserById(Guid serviceId, string accountUserLogin)
        {
            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Login == accountUserLogin);

            if (accountUser == null)
                return NotFound<ServiceTypeStateForUserDto>($"Пользователь по логину '{accountUserLogin}' не найден");

            AccessProvider.HasAccess(ObjectAction.BillingServiceType_GetStateForUser, () => accountUser.AccountId);

            try
            {
                return Ok(partnerBillingServiceDataProvider.GetServiceTypeStateForUserById(serviceId, accountUserLogin));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ServiceTypeStateForUserDto>(ex.Message);
            }
        }

        /// <summary>
        /// Проверить активность указанной услуги сервиса у клиента.
        /// </summary>
        /// <param name="serviceTypeKey">Ключ услуги сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Признак активности указанной услуги сервиса у клиента.</returns>
        public ManagerResult<ServiceTypeStatusForUserDto> CheckServiceTypeStatusForUser(Guid serviceTypeKey, string accountUserLogin)
        {
            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Login == accountUserLogin);

            if (accountUser == null)
                return NotFound<ServiceTypeStatusForUserDto>($"Пользователь по логину '{accountUserLogin}' не найден");

            AccessProvider.HasAccess(ObjectAction.BillingServiceType_GetStateForUser, () => accountUser.AccountId);

            try
            {
                return Ok(partnerBillingServiceDataProvider.CheckServiceTypeStatusForUser(serviceTypeKey, accountUserLogin));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ServiceTypeStatusForUserDto>(ex.Message);
            }
        }
    }
}
