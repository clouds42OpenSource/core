﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер редактирования заявки на вывод средств
    /// </summary>
    public class EditAgentCashOutRequestManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IEditAgentCashOutRequestProvider editAgentCashOutRequestProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить модель заявки на вывод средств для редактирования
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки на вывод средств</param>
        /// <returns>Модель заявки на вывод средств</returns>
        public ManagerResult<EditAgentCashOutRequestDto> GetAgentCashOutRequestById(Guid agentCashOutRequestId)
        {
            var accountId = AccountIdByAgentCashOutRequestId(agentCashOutRequestId)
                            ?? throw new NotFoundException($"По ID заявки {agentCashOutRequestId} не найден ID аккаунта");
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_GetCashOutRequests, () => accountId);
                var data = editAgentCashOutRequestProvider.GetAgentCashOutRequestById(agentCashOutRequestId);
                logger.Info($"Получение заявки на вывод средств агента {agentCashOutRequestId} завершилось успешно");
                return Ok(data);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Получение заявки на вывод средств агента {accountId} завершилось ошибкой.]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<EditAgentCashOutRequestDto>(ex.Message);
            }
        }

        /// <summary>
        /// Отредактировать заявку на вывод средств
        /// </summary>
        /// <param name="agentCashOutRequestDto">Модель заявки на вывод средств</param>
        public ManagerResult EditAgentCashOutRequest(EditAgentCashOutRequestDto agentCashOutRequestDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_EditCashOutRequest, () => agentCashOutRequestDto.AccountId);
                editAgentCashOutRequestProvider.EditAgentCashOutRequest(agentCashOutRequestDto);

                var message = $"Заявка на вывод вознаграждения №{agentCashOutRequestDto.RequestNumber} отредактирована.";
                LogEvent(() => agentCashOutRequestDto.AccountId, LogActions.EditAgentCashOutRequest, message);
                logger.Info(message);
                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Редактирование заявки на вывод вознаграждения №{agentCashOutRequestDto.RequestNumber} завершилось ошибкой.]";
                LogEvent(() => agentCashOutRequestDto.AccountId, LogActions.EditAgentCashOutRequest, $"{message} Описание ошибки: {ex.GetFullInfo(false)}");
                _handlerException.Handle(ex, message);

                return PreconditionFailed(ex.Message);
            }
        }
    }
}
