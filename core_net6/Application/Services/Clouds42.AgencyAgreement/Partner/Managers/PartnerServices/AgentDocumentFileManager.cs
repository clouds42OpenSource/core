﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер файлов агентского документа
    /// </summary>
    public class AgentDocumentFileManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAgentDocumentFileProvider agentDocumentFileProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить файл документа агента
        /// </summary>
        /// <param name="agentDocumentFileId">ID файла документа агента</param>
        /// <returns>Файл облака</returns>
        public ManagerResult<CloudFile> GetAgentDocumentFile(Guid agentDocumentFileId)
        {
            if (agentDocumentFileId == Guid.Empty)
            {
                logger.Warn($"ID файлого документа [ {agentDocumentFileId} ] равен нулю, передайте корректный ID");
                return PreconditionFailed<CloudFile>($"ID файлого документа [ {agentDocumentFileId} ] равен нулю, передайте корректный ID");
            }

            try
            {
                var data = agentDocumentFileProvider.GetAgentDocumentFile(agentDocumentFileId);
                logger.Info($"Получение файла {agentDocumentFileId} документа агента завершено успешно");
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения файла документа агента] {agentDocumentFileId}");
                return PreconditionFailed<CloudFile>(ex.Message);
            }
        }

        /// <summary>
        /// Получить файл разработки сервиса
        /// </summary>
        /// <param name="billingService1CFileId">ID файла разработки сервиса</param>
        /// <returns>Файл разработки сервиса</returns>
        public ManagerResult<CloudFile> GetBillingService1CFile(Guid billingService1CFileId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_OpenDocumentFile, () => AccountIdByBillingService1CFile(billingService1CFileId));
                var data = agentDocumentFileProvider.GetBillingService1CFile(billingService1CFileId);
                logger.Info($"Получение файла разработки сервиса {billingService1CFileId} завершено успешно");
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения файла разработки сервиса] {billingService1CFileId}");
                return PreconditionFailed<CloudFile>(ex.Message);
            }
        }
    }
}
