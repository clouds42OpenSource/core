﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    public static class AgentCashOutRequestSpecification
    {
        public static Spec<AgentCashOutRequestInfoDto> BySearchLine(string? value)
        {
            var lowerValue = value?.ToLower();

            return new Spec<AgentCashOutRequestInfoDto>(x =>
                string.IsNullOrEmpty(lowerValue) || x.PartnerAccountCaption.ToLower().Contains(lowerValue) ||
                x.PartnerAccountIndexNumber.ToString().ToLower().Contains(lowerValue));
        }

        public static Spec<AgentCashOutRequestInfoDto> ByDate(DateTime? from, DateTime? to)
        {
            return new Spec<AgentCashOutRequestInfoDto>(x =>
                (!from.HasValue || x.CreationDateTime >= from) && (!to.HasValue || x.CreationDateTime <= to));
        }
    }
    public class AgentCashOutRequestQuery: IPagedQuery, ISortedQuery
    {
        public AgentCashOutRequestFilter Filter { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public string OrderBy { get; set; }
    }

    public class AgentCashOutRequestFilter: IQueryFilter, IHasSpecificSearch
    {
        public string? SearchLine { get; set; }
        public string? RequestNumber { get; set; }
        public AgentRequisitesTypeEnum? RequisiteType { get; set; }
        public AgentCashOutRequestStatusEnum? RequestStatus { get; set; }
        public Guid? PartnerAccountId { get; set; }
        public Guid? AccountUserId { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }

    /// <summary>
    /// Провайдер для получения агентских заявок
    /// </summary>
    internal class AgentCashOutRequestProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : IAgentCashOutRequestProvider
    {
        /// <summary>
        /// Получить список агентских заявок
        /// </summary>
        /// <param name="filter">Фильтр получения агентских заявок на вывод средств</param>
        /// <returns>Список агентских заявок</returns>
        public PaginationDataResultDto<AgentCashOutRequestInfoDto> GetAgentCashOutRequests(
            AgentCashOutRequestsFilterDto filter)
        {
            try
            {
                var newFilter = new AgentCashOutRequestQuery
                {
                    Filter = new AgentCashOutRequestFilter
                    {
                        AccountUserId = filter.AccountUserId,
                        From = filter.PeriodFrom,
                        To = filter.PeriodTo,
                        PartnerAccountId = filter.AccountId,
                        RequestNumber = filter.RequestNumber,
                        RequestStatus = filter.RequestStatus,
                        RequisiteType = filter.AgentRequisitesType,
                        SearchLine = filter.PartnerAccountData
                    },
                    OrderBy = !string.IsNullOrEmpty(filter.SortFieldName) && filter.SortType.HasValue
                        ? $"{filter.SortFieldName}.{(filter.SortType == SortType.Asc ? "asc" : "desc")}"
                        : $"{nameof(AgentCashOutRequestInfoDto.CreationDateTime)}.desc",
                    PageNumber = filter.PageNumber,
                    PageSize = filter.PageSize
                };

                var result = dbLayer.AgentCashOutRequestRepository
                    .AsQueryableNoTracking()
                    .Select(x => new AgentCashOutRequestInfoDto
                    {
                        Id = x.Id,
                        AccountUserId = x.AccountUserId,
                        CreationDateTime = x.CreationDateTime,
                        RequestStatus = x.RequestStatus,
                        RequestedSum = x.RequestedSum,
                        PartnerAccountId = x.AgentRequisites.AccountOwnerId,
                        PartnerAccountCaption = x.AgentRequisites.AccountOwner.AccountCaption,
                        PartnerAccountIndexNumber = x.AgentRequisites.AccountOwner.IndexNumber,
                        StatusDateTime = x.StatusDateTime,
                        RequisiteType = x.AgentRequisites.LegalPersonRequisitesId.HasValue
                            ?
                            AgentRequisitesTypeEnum.LegalPersonRequisites
                            : x.AgentRequisites.PhysicalPersonRequisitesId.HasValue
                                ? AgentRequisitesTypeEnum.PhysicalPersonRequisites
                                :
                                AgentRequisitesTypeEnum.SoleProprietorRequisites,
                        RequestNumber = x.RequestNumber
                    })
                    .Where(AgentCashOutRequestSpecification.BySearchLine(newFilter.Filter.SearchLine) &
                           AgentCashOutRequestSpecification.ByDate(newFilter.Filter.From, newFilter.Filter.To))
                    .AutoFilter(newFilter.Filter)
                    .AutoSort(newFilter)
                    .ToPagedList(newFilter.PageNumber ?? 1, newFilter.PageSize ?? 50)
                    .ToPagedDto();


                return new PaginationDataResultDto<AgentCashOutRequestInfoDto>(
                    result.Records,
                    result.Metadata.TotalItemCount,
                    filter.PageSize, 
                    filter.PageNumber);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения агентских заявок для аккаунта] {filter.AccountId}");

                throw;
            }
        }

        /// <summary>
        /// Получить агентскую заявку
        /// </summary>
        /// <param name="cashOutRequestId">Id агентской заявки</param>
        /// <returns>Агентскую заявку</returns>
        public AgentCashOutRequest GetAgentCashOutRequest(Guid cashOutRequestId) =>
            dbLayer.AgentCashOutRequestRepository.FirstOrDefault(req => req.Id == cashOutRequestId);
    }
}
