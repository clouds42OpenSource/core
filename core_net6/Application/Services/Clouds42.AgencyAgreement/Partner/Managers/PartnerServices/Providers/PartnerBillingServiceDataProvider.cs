﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.ResourceConfiguration;
using Clouds42.DataContracts.Service.Partner;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{

    /// <summary>
    /// Провайдер данных партнерских сервисов.
    /// </summary>
    internal class PartnerBillingServiceDataProvider(
        IUnitOfWork dbLayer,
        ICloudServiceAdapter cloudServiceAdapter,
        IPartnerServiceActivityDataSelector partnerServiceActivityDataSelector)
        : IPartnerBillingServiceDataProvider
    {
        /// <summary>
        /// Получить детальную информацию по партнерскому сервису.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <returns>Информация по сервису.</returns>        
        public ServiceInfoDto GetServiceInfo(Guid serviceKey)
        {
            var billingService = dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Key == serviceKey);

            if (billingService == null)
                throw new NotFoundException($"По ключу '{serviceKey}' не найден сервис.");

            return new ServiceInfoDto
            {
                Name = billingService.Name,
                Description = billingService.ShortDescription,
                BillingServiceStatus = billingService.BillingServiceStatus,
                ServiceActivationDate = billingService.ServiceActivationDate,
                IsServiceDisabled = !billingService.IsActive
            };
        }

        /// <summary>
        /// Проверить активность конкретной услуги сервиса у клиента.
        /// </summary>
        /// <param name="serviceTypeKey">Ключ услуги сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Признак активности услуги сервиса.</returns>
        public ServiceTypeStatusForUserDto CheckServiceTypeStatusForUser(Guid serviceTypeKey, string accountUserLogin)
        {
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == accountUserLogin);

            if (accountUser == null)
                throw new NotFoundException($"По логину '{accountUserLogin}' не найден пользователь.");

            return CheckServiceTypeStatusForUser(serviceTypeKey, accountUser);
        }

        /// <summary>
        /// Получить список услуг партнерского сервиса.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <returns>Список услуг сервиса.</returns>     
        public GuidListItemDto GetServiceTypesList(Guid serviceKey)
        {
            var serviceTypes =
                dbLayer.BillingServiceTypeRepository.Where(s => s.Service.Key == serviceKey)
                    .Select(s => s.Key).ToList();

            return new GuidListItemDto { List = serviceTypes };
        }

        /// <summary>
        /// Получить список сервисов у аккаунта.
        /// Сервисы принадлежащие партнеру.
        /// </summary>
        /// <param name="accountId">Номер аккаунта партнера.</param>
        /// <returns>Список ключей сервиса.</returns>    
        public GuidListItemDto GetAccountServicesList(Guid accountId)
        {
            var services =
                dbLayer.BillingServiceRepository.Where(s => s.AccountOwner.Id == accountId)
                    .Select(s => s.Key).ToList();

            return new GuidListItemDto { List = services };
        }

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <param name="accountUserId">Номер пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        public ServiceTypeStateForUserDto GetServiceTypesStateForUserByKey(Guid serviceKey, Guid accountUserId)
        {

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == accountUserId);

            if (accountUser == null)
                throw new NotFoundException($"По номеру '{accountUserId}' не найден пользователь.");

            return GetServiceTypeStateForUser(GetBillingServiceByKeyOrThrowException(serviceKey), accountUser,
                st => st.BillingServiceType.Key);
        }

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        public ServiceTypeStateForUserDto GetServiceTypeStateForUserByKey(Guid serviceKey, string accountUserLogin)
        {

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == accountUserLogin);

            if (accountUser == null)
                throw new NotFoundException($"По логину '{accountUserLogin}' не найден пользователь.");

            return GetServiceTypeStateForUser(GetBillingServiceByKeyOrThrowException(serviceKey), accountUser,
                st => st.BillingServiceType.Key);
        }

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="serviceId">Id сервиса.</param>
        /// <param name="accountUserLogin">Логин пользователя облака 42.</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        public ServiceTypeStateForUserDto GetServiceTypeStateForUserById(Guid serviceId, string accountUserLogin)
        {

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == accountUserLogin);

            if (accountUser == null)
                throw new NotFoundException($"По логину '{accountUserLogin}' не найден пользователь.");

            return GetServiceTypeStateForUser(GetBillingServiceOrThrowException(serviceId), accountUser,
                st => st.BillingServiceTypeId);
        }

        /// <summary>
        /// Получить статус сервиса для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Модель статуса сервиса для аккаунта</returns>
        public ServiceStateForAccountDto GetServiceStatusForAccount(Guid accountId, Guid serviceId)
        {
            var partnerServiceActivityData = partnerServiceActivityDataSelector.SelectData(accountId, serviceId);

            return new ServiceStateForAccountDto
            {
                DatabasesIds = new DatabasesIdsDto(partnerServiceActivityData.DatabasesIds),
                ConnectedServiceTypesToUsers = new ConnectedServiceTypesToUsersDto
                {
                    ConnectedServiceTypesToUsers = GetConnectedServiceTypeToUser(partnerServiceActivityData.Resources)
                },
                BillingServiceStatusForAccount = GetBillingServiceStatusForAccount(partnerServiceActivityData),
                ExpireDate = GetBillingServiceExpireDateForAccount(partnerServiceActivityData),
                IsDemo = CheckServiceIsDemo(partnerServiceActivityData)
            };
        }

        /// <summary>
        /// Смапить ресурсы к списку подсключенных услуг пользователям
        /// </summary>
        /// <param name="resources">Список ресурсов</param>
        /// <returns>Список подсключенных услуг пользователям</returns>
        private static List<ConnectedServiceTypeToUserDto> GetConnectedServiceTypeToUser(List<Resource> resources) =>
            resources.Where(res => res.Subject != null).Select(res => new ConnectedServiceTypeToUserDto
            {
                UserId = res.Subject.Value,
                ServiceTypeId = res.BillingServiceTypeId
            }).ToList();

        /// <summary>
        /// Получить статус сервиса для аккаунта
        /// </summary>
        /// <param name="dataForAccountDc">Модель данных сервиса для аккаунта</param>
        /// <returns>Статус сервиса для аккаунта</returns>
        private BillingServiceStatusForAccountEnum GetBillingServiceStatusForAccount(ServiceDataForAccountDto dataForAccountDc)
        {
            if (!dataForAccountDc.Service.IsActive)
                return BillingServiceStatusForAccountEnum.NotActivated;

            if (dataForAccountDc.ServiceResourcesConfiguration == null ||
                dataForAccountDc.ServiceResourcesConfiguration.IsDemoPeriod)
                return dataForAccountDc.ServiceResourcesConfiguration.DetectActivityForAccount();

            return
                dataForAccountDc.Rent1CResourcesConfiguration.DetectActivityForAccount();
        }

        /// <summary>
        /// Получить дату окончания сервиса
        /// </summary>
        /// <param name="dataForAccountDc">Модель данных сервиса для аккаунта</param>
        /// <returns>Дата окончания сервиса</returns>
        private DateTime? GetBillingServiceExpireDateForAccount(ServiceDataForAccountDto dataForAccountDc)
        {
            if (dataForAccountDc.ServiceResourcesConfiguration?.IsDemoPeriod == false)
                return dataForAccountDc.Rent1CResourcesConfiguration.ExpireDateValue;

            return dataForAccountDc.ServiceResourcesConfiguration?.ExpireDateValue;
        }

        /// <summary>
        /// Проверирить, что сервис на демо периоде
        /// </summary>
        /// <param name="dataForAccountDc">Модель данных сервиса для аккаунта</param>
        /// <returns>Результат проверки, что сервис на демо периоде</returns>
        private bool CheckServiceIsDemo(ServiceDataForAccountDto dataForAccountDc) =>
            dataForAccountDc.ServiceResourcesConfiguration?.IsDemoPeriod ?? false;

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        /// <param name="service">Cервис</param>
        /// <param name="accountUser">Пользователь облака 42.</param>
        /// <param name="selector">Селектор выбора</param>
        /// <returns>Список подключенных услуг сервиса у клиента.</returns>
        private ServiceTypeStateForUserDto GetServiceTypeStateForUser(BillingService service, AccountUser accountUser, Func<Resource, Guid> selector)
        {
            var serviceTypesList = dbLayer.ResourceRepository.Where(r =>
                 r.BillingServiceType.ServiceId == service.Id &&
                 r.AccountId == accountUser.AccountId).ToList();

            ResourcesConfiguration resConfig;

            if (UserResourceSponsored(serviceTypesList, out var sponsorAccount))
            {
                resConfig = GetResourceConfiguration(service.Id, sponsorAccount.Id);

                if (resConfig == null)
                    return new ServiceTypeStateForUserDto
                    {
                        EnabledServiceTypesList = new GuidListItemDto()
                    };

                if (resConfig.IsDemoPeriod)
                    throw new NotFoundException(
                        $"Операция спонсирования сервиса '{service.Name}' недопустима в демо режиме.");
            }
            else
            {
                resConfig = GetResourceConfiguration(service.Id, accountUser.AccountId);

                if (resConfig == null)
                    return new ServiceTypeStateForUserDto
                    {
                        EnabledServiceTypesList = new GuidListItemDto()
                    };

                if (IsDemoExpired(resConfig))
                    throw new NotFoundException(
                        $"У сервиса '{service.Name}' закончился демо период.");
            }

            var mainService = cloudServiceAdapter.GetMainConfigurationOrThrowException(resConfig);

            return new ServiceTypeStateForUserDto
            {
                ServiceIsActive = GetServiceActiveValue(mainService, resConfig, serviceTypesList),
                ServiceExpiredDate = mainService.ExpireDateValue,
                ServiceDemoExpiredDate = resConfig.ExpireDate ?? DateTime.MinValue,
                IsDemoPeriod = resConfig.IsDemoPeriod,
                IsServiceDisabled = !service.IsActive,
                EnabledServiceTypesList = new GuidListItemDto
                {
                    List = serviceTypesList
                        .Where(r => r.Subject == accountUser.Id || r.Subject == accountUser.AccountId)
                        .Select(selector).Distinct().ToList()
                }
            };
        }

        /// <summary>
        /// Получить информацию о сервисе на аккаунт по пользователю
        /// </summary>
        /// <param name="serviceId">Для какого сервиса</param>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <returns>Информацию о сервисе на аккаунт</returns>
        public AccountServiceDto GetServiceData(Guid serviceId, Guid accountId)
        {
            var service = GetBillingServiceByKeyOrThrowException(serviceId);

            var serviceTypesList = dbLayer.ResourceRepository.Where(r =>
                           r.BillingServiceType.ServiceId == serviceId &&
                           r.AccountId == accountId && r.Subject != null).ToList();

            ResourcesConfiguration resConfig;

            if (UserResourceSponsored(serviceTypesList, out var sponsorAccount))
            {
                resConfig = GetResourceConfiguration(serviceId, sponsorAccount.Id);

                if (resConfig == null)
                    return new AccountServiceDto();
            }
            else
            {
                resConfig = GetResourceConfiguration(serviceId, accountId);

                if (resConfig == null)
                    return new AccountServiceDto();
            }

            var mainService = cloudServiceAdapter.GetMainConfigurationOrThrowException(resConfig);

            return new AccountServiceDto
            {
                IsActive = GetServiceActiveValue(mainService, resConfig, serviceTypesList),
                IsMainServiceNeedPayment = IsMainServiceNeedPayment(mainService),
                ExpiresOn = mainService.ExpireDateValue,
                IsDisabled = !service.IsActive,
                Demo = new ServiceDemoInfoDto
                {
                    IsEnabled = resConfig.IsDemoPeriod,
                    IsExpired = IsDemoExpired(resConfig),
                    ExpiresOn = resConfig.ExpireDate ?? DateTime.MinValue
                },
                Options = serviceTypesList
                        .Select(item => new ServiceOptionInfoDto
                        {
                            BillingType = item.BillingServiceType.BillingType,
                            Subject = item.Subject ?? Guid.Empty,
                            Id = item.BillingServiceTypeId
                        }).Distinct().ToList()
            };
        }

        /// <summary>
        ///     Получить ресурс конфигурации сервиса
        /// </summary>
        /// <param name="serviceId">Номер сервиса</param>
        /// <param name="accountId">Номер аккаунта</param>
        /// <returns>Ресурс конфигурации сервиса</returns>
        private ResourcesConfiguration GetResourceConfiguration(Guid serviceId, Guid accountId)
            => dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingServiceId == serviceId && rc.AccountId == accountId);
        /// <summary>
        /// Получить сервис или выкинуть исключение
        /// </summary>
        /// <param name="id">Id сервиса</param>
        /// <returns>Сервис</returns>
        private BillingService GetBillingServiceOrThrowException(Guid id) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(sr => sr.Id == id) ??
            throw new NotFoundException($"По номеру '{id}' не найден сервис.");

        /// <summary>
        /// Получить сервис или выкинуть исключение
        /// </summary>
        /// <param name="key">Ключ сервиса</param>
        /// <returns>Сервис</returns>
        private BillingService GetBillingServiceByKeyOrThrowException(Guid key) =>
            dbLayer.BillingServiceRepository.FirstOrDefault(sr => sr.Key == key) ??
            throw new NotFoundException($"По ключу '{key}' не найден сервис.");

        /// <summary>
        ///     Пользователь спонсируется
        /// </summary>
        /// <param name="serviceTypesList">Список услуг сервиса у пользователя</param>
        /// <param name="sponsorAccount">Аккаунт спонсора</param>
        /// <returns>
        ///     true - пользователь спонсируется и номер аккаунта спонсора
        ///     false - пользователь не спонсируется номер аккаунта спонсора - null
        /// </returns>
        private bool UserResourceSponsored(List<Resource> serviceTypesList, out Account sponsorAccount)
        {
            if (serviceTypesList.Any(w => w.AccountSponsorId.HasValue))
            {
                sponsorAccount = serviceTypesList.FirstOrDefault(w => w.AccountSponsorId.HasValue)?.AccountSponsor;
                return true;
            }

            sponsorAccount = null;
            return false;
        }

        /// <summary>
        /// Ресурс демо и срок действия завершен.
        /// </summary>
        /// <param name="resConfig">Ресурс конфигурация.</param>
        /// <returns>true- если ресурс демо и срок действия завершени, false в противном случае.</returns>
        private bool IsDemoExpired(ResourcesConfiguration resConfig)
            => resConfig is { IsDemoPeriod: true, ExpireDate: not null } && resConfig.ExpireDateValue <= DateTime.Now;

        /// <summary>
        /// Нужна ли оплата за главный сервис
        /// </summary>
        /// <param name="mainServiceConfig">Ресурс конфигурация гланого сервиса.</param>
        /// <returns>true-если требуется оплата за главный сервис.</returns>
        private bool IsMainServiceNeedPayment(ResourcesConfiguration mainServiceConfig)
            => mainServiceConfig.FrozenValue || mainServiceConfig.ExpireDate.HasValue && mainServiceConfig.ExpireDateValue < DateTime.Now;

        /// <summary>
        /// Проверить активность конкретной услуги сервиса у клиента.
        /// </summary>
        /// <param name="serviceTypeKey">Ключ услуги сервиса.</param>
        /// <param name="accountUser">Пользователь облака 42.</param>
        /// <returns>Признак активности услуги сервиса.</returns>
        private ServiceTypeStatusForUserDto CheckServiceTypeStatusForUser(Guid serviceTypeKey, AccountUser accountUser)
        {
            var resource = dbLayer.ResourceRepository.FirstOrDefault(w =>
                (w.Subject == accountUser.Id || w.Subject == accountUser.AccountId)
                && w.BillingServiceType.Key == serviceTypeKey);

            if (resource == null)
                return new ServiceTypeStatusForUserDto();

            return resource.AccountSponsorId.HasValue
                ? ServiceIsActive(resource.AccountSponsorId.Value, resource.BillingServiceType.ServiceId)
                : ServiceIsActive(accountUser.AccountId, resource.BillingServiceType.ServiceId);
        }

        /// <summary>
        /// Проверить активность сервиса у аккаунта
        /// </summary>
        /// <param name="accountId">Номер аккаунта</param>
        /// <param name="serviceId">Номер сервиса</param>
        /// <returns>Признак активности сервиса</returns>
        private ServiceTypeStatusForUserDto ServiceIsActive(Guid accountId, Guid serviceId)
        {
            var resConfig =
                dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.BillingServiceId == serviceId && rc.AccountId == accountId);

            if (resConfig == null)
                return new ServiceTypeStatusForUserDto();

            if (IsDemoExpired(resConfig))
                return new ServiceTypeStatusForUserDto();

            var mainService = cloudServiceAdapter.GetMainConfigurationOrThrowException(resConfig);

            return new ServiceTypeStatusForUserDto
            {
                Result = !mainService.FrozenValue,
                IsDemoPeriod = resConfig.IsDemoPeriod,
                IsServiceDisabled = !resConfig.BillingService.IsActive
            };
        }

        /// <summary>
        /// Получить значение активности сервиса.
        /// </summary>
        /// <param name="mainService">Основной сервис.</param>
        /// <param name="resConfig">Ресурс конфигурация.</param>
        /// <param name="serviceTypesList">Список активных лицензий.</param>
        /// <returns></returns>
        private static bool GetServiceActiveValue(ResourcesConfiguration mainService, ResourcesConfiguration resConfig, List<Resource> serviceTypesList)
        {
            if ((serviceTypesList == null || !serviceTypesList.Any()) && resConfig.BillingServiceId != mainService.BillingServiceId)
                return false;

            return !mainService.FrozenValue;
        }
    }
}