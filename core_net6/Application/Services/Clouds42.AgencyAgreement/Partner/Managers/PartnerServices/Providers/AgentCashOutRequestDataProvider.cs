﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.AgencyAgreement.Partner.Mappers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер для работы с данными агентских заявок
    /// </summary>
    internal class AgentCashOutRequestDataProvider(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IAgencyAgreementDataProvider agencyAgreementDataProvider)
        : IAgentCashOutRequestDataProvider
    {
        /// <summary>
        /// Получить заявки на расходование средств в статусе "Новая"
        /// </summary>
        /// <returns>Заявки на расходование средств в статусе "Новая"</returns>
        public AgentCashOutRequestsInStatusNewDto GetAgentCashOutRequestsInStatusNew()
        {
            return new AgentCashOutRequestsInStatusNewDto
            {
                ActualAgencyAgreementDate = agencyAgreementDataProvider.GetActualAgencyAgreement().EffectiveDate,
                ActualAgencyAgreementLink = new Uri(
                        $"{CloudConfigurationProvider.Cp.GetSiteCoreUrl()}/partners/open-actual-agency-agreement")
                    .AbsoluteUri,
                AgentCashOutRequestDatas = new AgentCashOutRequestDatasDto
                {
                    AgentCashOutRequestDataList = GetAgentCashOutRequestDataList()
                }
            };
        }

        /// <summary>
        /// Получить cписок данных о заявках на вывод средств
        /// </summary>
        /// <param name="agentCashOutRequestStatus">Статус заявки</param>
        /// <returns>Список данных о заявках на вывод средств</returns>
        private List<AgentCashOutRequestDataDto> GetAgentCashOutRequestDataList(
            AgentCashOutRequestStatusEnum agentCashOutRequestStatus = AgentCashOutRequestStatusEnum.New)
        {
            var agentCashOutRequests =
                (from agentCashOutRequest in dbLayer.AgentCashOutRequestRepository.WhereLazy(acr =>
                        acr.RequestStatus == agentCashOutRequestStatus && acr.AccountUserId == null)
                 join agentRequisites in dbLayer.AgentRequisitesRepository.WhereLazy() on agentCashOutRequest
                     .AgentRequisitesId equals agentRequisites.Id
                 join accountConfiguration in accountConfigurationDataProvider
                         .GetAccountConfigurationsDataBySupplier() on agentRequisites.AccountOwnerId equals
                     accountConfiguration.Account.Id
                 select new
                 {
                     AgentCashOutRequest = agentCashOutRequest,
                     accountConfiguration.Account,
                     AgentRequisites = agentRequisites,
                     accountConfiguration.Supplier,
                     AgentRequisitesFiles = agentCashOutRequest.AgentCashOutRequestFiles
                 }).ToList().Select(result => new AgentCashOutRequestDataDto
                 {
                     Sum = result.AgentCashOutRequest.RequestedSum,
                     IsAuthorPayment = result.AgentCashOutRequest.AccountUserId != null,
                     AccountNumber = result.Account.IndexNumber,
                     CreationDateTime = result.AgentCashOutRequest.CreationDateTime,
                     AgentReportLinks = new AgentReportLinksDto
                     {
                         Links = result.AgentRequisitesFiles
                             .Select(arf => new Uri($"{CloudConfigurationProvider.Cp.GetSiteCoreUrl()}/partners/agent-document-file?agentDocumentFileId={arf.CloudFileId}").AbsoluteUri)
                             .ToList()
                     },
                     AgentRequisitesForPayment = GetAgentRequisitesForPayment(result.AgentRequisites),
                     SupplierCode = ((Supplier)result.Supplier).Code,
                     RequestNumber = result.AgentCashOutRequest.RequestNumber,
                 }).ToList();

            return agentCashOutRequests;
        }

        /// <summary>
        /// Получить агентские реквизиты для выплаты
        /// </summary>
        /// <param name="agentRequisites">Агентские реквизиты для выплаты</param>
        /// <returns></returns>
        private static AgentRequisitesForPaymentDto GetAgentRequisitesForPayment(Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites)
        {
            var agentRequisitesForPaymentDto = agentRequisites.LegalPersonRequisites != null
                ? agentRequisites.LegalPersonRequisites.MapToAgentRequisitesForPaymentDto()
                : agentRequisites.SoleProprietorRequisites != null
                    ? agentRequisites.SoleProprietorRequisites.MapToAgentRequisitesForPaymentDto()
                    : agentRequisites.PhysicalPersonRequisites.MapToAgentRequisitesForPaymentDto();

            return agentRequisitesForPaymentDto;
        }
    }
}
