﻿using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.AgencyAgreement.Partner.Helpers;
using Clouds42.AgencyAgreement.Partner.Mappers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер для редактирования заявок на вывод средств агента
    /// </summary>
    internal class EditAgentCashOutRequestProvider(
        IUnitOfWork dbLayer,
        AgencyFileHelper agencyFileHelper,
        IAgentRequisitesProvider agentRequisitesProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IEditAgentCashOutRequestProvider
    {
        /// <summary>
        /// Получить модель заявки для редактирования по ID
        /// </summary>
        /// <param name="cashOutRequestId">ID заявки</param>
        /// <returns>Модель заявки для редактирования</returns>
        public EditAgentCashOutRequestDto GetAgentCashOutRequestById(Guid cashOutRequestId)
        {
            try
            {
                var agentCashOutRequest =
                    dbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == cashOutRequestId)
                    ?? throw new NotFoundException($"Заявка по ID {cashOutRequestId} не найдена!");

                logger.Trace($"Заявка для редактирования по ID {cashOutRequestId} получена.");

                var editAgentCashOutRequestDto = agentCashOutRequest.MapToEditAgentCashOutRequestDto();

                return editAgentCashOutRequestDto;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения модели заявки] {cashOutRequestId} ");
                throw;
            }
        }

        /// <summary>
        /// Изменить заявку на вывод средств
        /// </summary>
        /// <param name="editAgentCashOutRequestDto">Модель изменения заявки</param>
        public void EditAgentCashOutRequest(EditAgentCashOutRequestDto editAgentCashOutRequestDto)
        {
            var agentCashOutRequest =
                dbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == editAgentCashOutRequestDto.Id)
                ?? throw new NotFoundException($"Заявка по ID {editAgentCashOutRequestDto.Id} не найдена");

            var agentRequisite =
                agentRequisitesProvider.GetAgentRequisites(editAgentCashOutRequestDto.AgentRequisitesId);

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                if (agentCashOutRequest.AgentRequisitesId != editAgentCashOutRequestDto.AgentRequisitesId)
                {
                    agentCashOutRequest.Number = GetNextCashOutRequestNumber(editAgentCashOutRequestDto.AgentRequisitesId) + 1;
                    agentCashOutRequest.RequestNumber = $"{agentRequisite.AccountOwner.IndexNumber}-{agentRequisite.Number}-{agentCashOutRequest.Number}";
                }

                agentCashOutRequest.AgentRequisitesId = editAgentCashOutRequestDto.AgentRequisitesId;
                agentCashOutRequest.RequestedSum = editAgentCashOutRequestDto.TotalSum;

                UpdateAgentCashOutRequestFiles(editAgentCashOutRequestDto);

                dbLayer.AgentCashOutRequestRepository.Update(agentCashOutRequest);
                dbLayer.Save();
                transaction.Commit();

                logger.Trace($"Заявка {agentCashOutRequest.Id} за номером {agentCashOutRequest.RequestNumber} успешно отредактирована");
            }
            catch (Exception ex)
            {
                logger.Trace($@"Редактирование заявки с ID {editAgentCashOutRequestDto.Id} для аккаунта {editAgentCashOutRequestDto.AccountId} завершено с ошибкой. 
                                    Причина: {ex.GetFullInfo()}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Обновить файлы заявки на вывод средств
        /// </summary>
        /// <param name="editAgentCashOutRequestDto">Модель редактирования заявки на вывод средств</param>
        private void UpdateAgentCashOutRequestFiles(EditAgentCashOutRequestDto editAgentCashOutRequestDto)
        {
            logger.Info($"Обновление файлов заявки на вывод средств {editAgentCashOutRequestDto.Id}");
            var existCashOutRequestFiles =
                dbLayer.AgentCashOutRequestFileRepository.Where(w =>
                    w.AgentCashOutRequestId == editAgentCashOutRequestDto.Id).ToList();

            if (editAgentCashOutRequestDto.Files == null ||
                !editAgentCashOutRequestDto.Files.Any())
            {
                RemoveCashOutRequestFiles(existCashOutRequestFiles);
                return;
            }

            var newFiles = editAgentCashOutRequestDto.Files.Where(w =>
                !existCashOutRequestFiles.Exists(existFile =>
                    existFile.CloudFile.FileName == w.FileName)).ToList();

            logger.Info($"Количество новых файлов заявки на вывод средств {editAgentCashOutRequestDto.Id} : {newFiles.Count}");

            var deletedFiles = existCashOutRequestFiles.Where(existFile =>
                !editAgentCashOutRequestDto.Files.Exists(w =>
                    w.FileName == existFile.CloudFile.FileName)).ToList();

            logger.Info($"Количество удаляемых файлов заявки на вывод средств {editAgentCashOutRequestDto.Id} : {deletedFiles.Count}");

            CreateAgentCashOutRequestFiles(editAgentCashOutRequestDto.Id, newFiles);
            RemoveCashOutRequestFiles(deletedFiles);

            logger.Info($"Обновление файлов заявки на вывод средств {editAgentCashOutRequestDto.Id} завершено");
        }

        /// <summary>
        /// Создать файлы заявки на вывод средств
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки на вывод средств</param>
        /// <param name="files">Файлы заявки на вывод средств</param>
        private void CreateAgentCashOutRequestFiles(Guid agentCashOutRequestId, List<CloudFileDataDto<byte[]>> files)
        {
            if (files == null || !files.Any())
                return;

            files.ForEach(w =>
            {
                var cloudFileId = agencyFileHelper.CreateAgencyCloudFile(w);

                var agentCashOutRequestFile = new AgentCashOutRequestFile
                {
                    AgentCashOutRequestId = agentCashOutRequestId,
                    CloudFileId = cloudFileId
                };

                dbLayer.AgentCashOutRequestFileRepository.Insert(agentCashOutRequestFile);
            });
        }

        /// <summary>
        /// Удалить файлы заявки на вывод средств
        /// </summary>
        /// <param name="agentCashOutRequestFiles">Файлы заявки на вывод средств</param>
        private void RemoveCashOutRequestFiles(List<AgentCashOutRequestFile> agentCashOutRequestFiles)
        {
            if (!agentCashOutRequestFiles.Any())
                return;

            var cloudFiles = agentCashOutRequestFiles.Select(w => w.CloudFile).ToList();
            dbLayer.AgentCashOutRequestFileRepository.DeleteRange(agentCashOutRequestFiles);
            dbLayer.CloudFileRepository.DeleteRange(cloudFiles);
        }

        /// <summary>
        /// Получить следующий номер заявки на вывод средств
        /// </summary>
        /// <param name="agentRequisiteId">Id реквизитов</param>
        /// <returns>Следующий номер для заявки на вывод средств</returns>
        private int GetNextCashOutRequestNumber(Guid agentRequisiteId) =>
            dbLayer.AgentCashOutRequestRepository.WhereLazy(acr =>
                    acr.AgentRequisitesId == agentRequisiteId).OrderByDescending(acr => acr.Number).FirstOrDefault()
                ?.Number ?? 0;
    }
}
