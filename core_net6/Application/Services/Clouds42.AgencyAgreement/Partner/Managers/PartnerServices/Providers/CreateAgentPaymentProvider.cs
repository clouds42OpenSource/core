﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Partner.Validators;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер создания агентских платежей.
    /// </summary>
    internal class CreateAgentPaymentProvider(
        ILogger42 logger,
        IUnitOfWork dbLayer,
        AgencyPaymentCreationDtoValidator validator,
        IHandlerException handlerException)
        : ICreateAgentPaymentProvider
    {
        /// <summary>
        /// Создать платеж в пользу агента.
        /// </summary>
        /// <param name="agencyPaymentCreation">Модель создания агентского платежа</param>
        public void CreateAgentPayment(AgencyPaymentCreationDto agencyPaymentCreation)
        {
            var validateResult = validator.Validate(agencyPaymentCreation);

            if (!validateResult.Success)
                throw new ValidateException(validateResult.Message);

            var agentCashOutRequest = dbLayer.AgentCashOutRequestRepository
            .FirstOrDefault(x => x.RequestNumber == agencyPaymentCreation.RequestNumber);

            try
            {
                var clientAccountId = agencyPaymentCreation.AccountNumber.HasValue
                    ? GetClientAccountId(agencyPaymentCreation.AccountNumber.Value)
                    : (Guid?)null;



                if (agentCashOutRequest is { AccountUserId: not null } && agencyPaymentCreation.PaymentType == PaymentType.Outflow)
                    CreateArticleTransactionOutflow(agencyPaymentCreation, agentCashOutRequest);
                else
                {
                    dbLayer.AgentWalletRepository.GetAgentWalletOrCreateNew(agencyPaymentCreation.AgentAccountId);
                    dbLayer.AgentPaymentRepository.InsertAgentPaymentAndUpdateAgentWallet(
                        agencyPaymentCreation.AgentAccountId, agencyPaymentCreation.Sum,
                        paymentType: agencyPaymentCreation.PaymentType, date: agencyPaymentCreation.Date,
                        comment: agencyPaymentCreation.Comment,
                        agentPaymentSourceType: agencyPaymentCreation.AgentPaymentSourceType,
                        clientAccountId: clientAccountId,
                        clientPaymentSum: agencyPaymentCreation.ClientPaymentSum);
                }
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка обработки начисления агентского вознаграждения]");
                throw;
            }
        }

        /// <summary>
        /// Получить Id аккаунта клиента
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        /// <returns>Id аккаунта</returns>
        private Guid GetClientAccountId(int accountNumber) =>
            dbLayer.AccountsRepository.FirstOrDefault(a => a.IndexNumber == accountNumber)?.Id ??
            throw new NotFoundException($"Не удалось получить аккаунт по номеру {accountNumber}");

        /// <summary>
        /// Добавлении транзакции на вывод средст для статей
        /// </summary>
        /// <param name="agencyPaymentCreation">Модель создания агентского платежа</param>
        /// <returns>Id аккаунта</returns>
        private void CreateArticleTransactionOutflow(AgencyPaymentCreationDto agencyPaymentCreation, AgentCashOutRequest agentCashOutRequest) 
        {
            logger.Debug($"Списываем денежные средства со счета пользователя {agentCashOutRequest.AccountUserId} и записываем транзакцию при проведении заявки {agencyPaymentCreation.RequestNumber} на выплату");
            var article = dbLayer.ArticleRepository.OrderByDescending(art=>art.PublicationDate).FirstOrDefault(art=> art.AccountUserId == agentCashOutRequest.AccountUserId 
            && art.RewardAmount>0 && art.Status == Domain.DataModels.ArticleStatus.Published);

            if (article is null)
                throw new NotFoundException("Нет ни одной опубликовонной статьи на которую начислено вознагрождение");

            logger.Debug($"Нашли статью пользователя {agentCashOutRequest.AccountUserId} с темой {article.Topic} и номером {article.Id}");
            var articleTransaction = new ArticleTransaction
            {
                Id = Guid.NewGuid(),
                ArticleId = article.Id,
                Cause = ArticleTransactionCause.ArticlePublication,
                TransactionType = ArticleTransactionType.Outflow,
                Amount = agencyPaymentCreation.Sum,
                CreatedOn = DateTime.Now
            };
            dbLayer.ArticleTransactionRepository.Insert(articleTransaction);

            var articleWallets = dbLayer.ArticleWalletsRepository
            .FirstOrDefault(x => x.AccountUserId == agentCashOutRequest.AccountUserId);

            if (articleWallets != null)
            {
                articleWallets.AvailableSum = articleWallets.AvailableSum - articleTransaction.Amount;
                dbLayer.ArticleWalletsRepository.Update(articleWallets);
            }
            dbLayer.Save();

        }
    }
}
