﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.Partner;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер данных партнерских услуг сервиса.
    /// </summary>
    public class PartnerBillingServiceTypeDataProvider(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IPartnerBillingServiceTypeDataProvider
    {
        /// <summary>
        /// Получить детальную информацию о услуге сервиса.
        /// </summary>
        /// <param name="serviceTypeKey">Ключ услуги сервиса.</param>
        /// <returns>Информация по услуге сервиса.</returns>        
        public ServiceTypeInfoDto GetServiceTypeInfo(Guid serviceTypeKey)
        {
            var serviceType = dbLayer.BillingServiceTypeRepository.FirstOrDefault(t => t.Key == serviceTypeKey);

            if (serviceType == null)
                throw new NotFoundException($"По ключу '{serviceTypeKey}' не найдена услуга сервиса.");

            if (serviceType.Service.AccountOwnerId == null)
                throw new NotFoundException($"Сервису '{serviceType.ServiceId}' не задан владелец.");

            var localeId =
                accountConfigurationDataProvider.GetAccountLocaleId(serviceType.Service.AccountOwnerId.Value);

            var rate = dbLayer.RateRepository.FirstOrDefault(r =>
                r.LocaleId == localeId && r.BillingServiceType.Key == serviceTypeKey);

            if (rate == null)
                throw new NotFoundException(
                    $"Для услуги сервиса '{serviceType.Name} -> {serviceType.Id}' не задан тариф в таблице Rate");

            return new ServiceTypeInfoDto
            {
                Name = serviceType.Name,
                Description = serviceType.Description,
                BillingType = serviceType.BillingType,
                Cost = rate.Cost
            };
        }
    }
}
