﻿using Clouds42.Logger;
using Clouds42.AgencyAgreement.Contracts.AgentFileStorage;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using MimeMapping;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер файлов агентского документа
    /// </summary>
    internal class AgentDocumentFileProvider(
        IUnitOfWork dbLayer,
        IAgentFileStorageProvider agentFileStorageProvider,
        ILogger42 logger)
        : IAgentDocumentFileProvider
    {
        /// <summary>
        /// Получить файл документа агента
        /// </summary>
        /// <param name="agentDocumentFileId">ID файла</param>
        /// <returns>Файл облака</returns>
        public CloudFile GetAgentDocumentFile(Guid agentDocumentFileId)
        {
                var cloudFile =
                    dbLayer.CloudFileRepository.FirstOrDefault(w => w.Id == agentDocumentFileId)
                    ?? throw new NotFoundException($"Файл облака по ID {agentDocumentFileId} не найден");

                cloudFile.FileName = cloudFile.FileName.RemoveCommas();
                return cloudFile;
        }

        /// <summary>
        /// Получить файл разработки сервиса
        /// </summary>
        /// <param name="billingService1CFileId">ID файла разработки сервиса</param>
        /// <returns>Файл разработки сервиса</returns>
        public CloudFile GetBillingService1CFile(Guid billingService1CFileId)
        {
            try
            {
                var billingService1CFile =
                    dbLayer.BillingService1CFileRepository.FirstOrDefault(w => w.Id == billingService1CFileId)
                    ?? throw new NotFoundException($"Файл разработки сервиса по ID {billingService1CFileId} не найден.");

                if (billingService1CFile.Service.AccountOwnerId == null)
                    throw new InvalidOperationException(
                        $"Невозможно открыть файл разработки {billingService1CFileId} системного сервиса");

                var accountId = billingService1CFile.Service.AccountOwnerId.Value;
                return GetFileFromAgentFileStorage(accountId, billingService1CFile.FileName);
            }
            catch (Exception ex)
            {
                logger.Trace($"При получении и конвертации файла разработки {billingService1CFileId} возникла ошибка. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Получить файл
        /// из агентского файлового хранилища
        /// </summary>
        /// <returns>Файл облака</returns>
        private CloudFile GetFileFromAgentFileStorage(Guid accountId, string fileName)
        {
            var filePath =
                agentFileStorageProvider.GetPathForFileAgent(accountId, fileName);

            logger.Trace($"Файл {fileName} найден. Подготовка к конвертации.");
            return CreateCloudFileByPath(fileName, filePath);
        }

        /// <summary>
        /// Создать файл облака по пути
        /// </summary>
        /// <param name="fileName">Название файла</param>
        /// <param name="filePath">Путь к файлу</param>
        /// <returns>Файл облака</returns>
        private CloudFile CreateCloudFileByPath(string fileName, string filePath)
        {
            var fileContent = File.ReadAllBytes(filePath);

            var contentType = MimeUtility.GetMimeMapping(fileName);

            logger.Trace($"Файл {fileName} прочитан успешно. Тип контента: {contentType}");

            return new CloudFile
            {
                Content = fileContent,
                FileName = fileName,
                ContentType = contentType
            };
        }
    }
}
