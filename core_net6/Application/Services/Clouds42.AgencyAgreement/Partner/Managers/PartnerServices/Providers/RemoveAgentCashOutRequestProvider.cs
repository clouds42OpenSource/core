﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер для удаления заявок на вывод средств агента
    /// </summary>
    internal class RemoveAgentCashOutRequestProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : IRemoveAgentCashOutRequestProvider
    {
        /// <summary>
        /// Удалить агентскую заявку на вывод средств
        /// </summary>
        /// <param name="cashOutRequestId">ID агентской заявки</param>
        public void RemoveAgentCashOutRequest(Guid cashOutRequestId)
        {
            logger.Trace($"Удаление заявки {cashOutRequestId} на вывод средств агента");
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var cashOutRequest =
                    dbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == cashOutRequestId)
                    ?? throw new NotFoundException($"Заявка по {cashOutRequestId} не найдена");

                if (cashOutRequest.RequestStatus != AgentCashOutRequestStatusEnum.New)
                    throw new InvalidOperationException($"Удаление возможно только для заявок со статусом '{AgentCashOutRequestStatusEnum.New.Description()}'");

                RemoveCashOutRequestFiles(cashOutRequest.AgentCashOutRequestFiles.ToList());
                dbLayer.AgentCashOutRequestRepository.Delete(cashOutRequest);
                dbLayer.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления заявки на вывод средств] {cashOutRequestId}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Удалить файлы заявки на вывод средств
        /// </summary>
        /// <param name="agentCashOutRequestFiles">Список файлов заявки</param>
        private void RemoveCashOutRequestFiles(List<AgentCashOutRequestFile> agentCashOutRequestFiles)
        {
            if (!agentCashOutRequestFiles.Any())
                return;

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var cloudFiles = agentCashOutRequestFiles.Select(w => w.CloudFile).ToList();
                dbLayer.AgentCashOutRequestFileRepository.DeleteRange(agentCashOutRequestFiles);
                dbLayer.CloudFileRepository.DeleteRange(cloudFiles);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, " [Ошибка удаления файлов заявки на вывод средств] ");
                transaction.Rollback();
                throw;
            }
        }
    }
}
