﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.Partner.Validators;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер смены статуса заявки на вывод средств
    /// </summary>
    internal class ChangeAgentCashOutRequestStatusProvider : IChangeAgentCashOutRequestStatusProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly AgentPaymentManager _agentPaymentManager;
        private readonly IAccessProvider _accessProvider;
        private readonly ILogger42 _logger;
        private readonly IHandlerException _handlerException;
        /// <summary>
        /// Карта правил с сопоставлением метода для создания агентского платежа
        /// </summary>
        private readonly List<(Func<AgentCashOutRequestStatusEnum, AgentCashOutRequestStatusEnum, bool>,
            Action<AgentCashOutRequest>)> _list;

        public ChangeAgentCashOutRequestStatusProvider(
            IUnitOfWork dbLayer,
            AgentPaymentManager agentPaymentManager,
            IAccessProvider accessProvider,
            ILogger42 logger, IHandlerException handlerException)
        {
            _dbLayer = dbLayer;
            _agentPaymentManager = agentPaymentManager;
            _accessProvider = accessProvider;
            _logger = logger;
            _handlerException = handlerException;
            _list =
            [
                (
                    (oldStatus, newStatus) => oldStatus is AgentCashOutRequestStatusEnum.New or AgentCashOutRequestStatusEnum.InProcess
                                             && newStatus == AgentCashOutRequestStatusEnum.Paid,
                    CreateOutflowAgentPayment

                ),

                (
                    (oldStatus, newStatus) => oldStatus == AgentCashOutRequestStatusEnum.Paid &&
                                             newStatus is AgentCashOutRequestStatusEnum.InProcess or AgentCashOutRequestStatusEnum.New, CreateInflowAgentPayment
                )
            ];
        }

        /// <summary>
        /// Попытаться сменить статус заявки на вывод средств
        /// </summary>
        /// <param name="changeAgentCashOutRequestStatus">Модель смены статуса заявки
        /// на вывод средств</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Успешность смены статуса</returns>
        public bool TryChangeAgentCashOutRequestStatus(
            ChangeAgentCashOutRequestStatusDto changeAgentCashOutRequestStatus, out string errorMessage)
        {
            errorMessage = null;
            changeAgentCashOutRequestStatus.Validate();

            _logger.Info($"Попытка сменить статус заявки {changeAgentCashOutRequestStatus.RequestNumber} на вывод средств.");
            var cashOutRequest = GetAgentCashOutRequest(changeAgentCashOutRequestStatus.RequestNumber);

            if (cashOutRequest == null)
            {
                errorMessage = $"Заявка по номеру {changeAgentCashOutRequestStatus.RequestNumber} не найдена.";
                _logger.Warn(errorMessage);
                return false;
            }

            if (cashOutRequest.RequestStatus == changeAgentCashOutRequestStatus.AgentCashOutRequestStatus)
                return true;

            if (!CanChangeAgentCashOutRequestStatus(cashOutRequest, changeAgentCashOutRequestStatus, out errorMessage))
                return false;

            ChangeAgentCashOutRequestStatus(cashOutRequest, changeAgentCashOutRequestStatus);

            return true;
        }

        /// <summary>
        /// Проверка возможности сменить статус
        /// заявки на вывод средств
        /// </summary>
        /// <param name="cashOutRequest">Заявка на вывод средств</param>
        /// <param name="changeAgentCashOutRequestStatus">Модель смены статуса заявки
        /// на вывод средств</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns></returns>
        private bool CanChangeAgentCashOutRequestStatus(AgentCashOutRequest cashOutRequest,
            ChangeAgentCashOutRequestStatusDto changeAgentCashOutRequestStatus, out string errorMessage)
        {
            errorMessage = null;

            if (Math.Round(cashOutRequest.RequestedSum,2) != changeAgentCashOutRequestStatus.Sum)
            {
                errorMessage = $"Заявка по номеру {changeAgentCashOutRequestStatus.RequestNumber} найдена, не соответствует сумма.";
                _logger.Warn(errorMessage);
                return false;
            }

            if (cashOutRequest.RequestStatus == AgentCashOutRequestStatusEnum.Paid &&
                AvailabilityOfLaterPayments(cashOutRequest.AgentRequisites.AccountOwnerId,
                    cashOutRequest.CreationDateTime, cashOutRequest.AccountUserId))
            {
                errorMessage = $"Заявка по номеру {changeAgentCashOutRequestStatus.RequestNumber} найдена, но есть более поздние заявки в статусе “Оплачено”.";
                _logger.Warn(errorMessage);
                return false;
            }

            if (changeAgentCashOutRequestStatus.AgentCashOutRequestStatus == AgentCashOutRequestStatusEnum.Paid &&
                AvailabilityOfEarlyUnpaidAgentCashOutRequest(cashOutRequest.AgentRequisites.AccountOwnerId,
                    cashOutRequest.CreationDateTime, cashOutRequest.AccountUserId))
            {
                errorMessage = $"Заявка по номеру {changeAgentCashOutRequestStatus.RequestNumber} найдена, но есть более ранние заявки в статусе “Новая” или “В работе”.";
                _logger.Warn(errorMessage);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Наличие более поздних выплат
        /// по заявке или переводов на баланс
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="paymentDate">Дата выплаты</param>
        /// <returns>Наличие более поздних выплат</returns>
        private bool AvailabilityOfLaterPayments(Guid accountId, DateTime paymentDate, Guid? userId)
        {
            AgentCashOutRequest paidCashOutRequest;

            if(userId is null)
            paidCashOutRequest = _dbLayer.AgentCashOutRequestRepository.FirstOrDefault(acr =>
                acr.AgentRequisites.AccountOwnerId == accountId && acr.CreationDateTime > paymentDate &&
                acr.RequestStatus == AgentCashOutRequestStatusEnum.Paid && acr.AccountUserId == null);
            else
                paidCashOutRequest = _dbLayer.AgentCashOutRequestRepository.FirstOrDefault(acr =>
                acr.AccountUserId == userId && acr.CreationDateTime > paymentDate &&
                acr.RequestStatus == AgentCashOutRequestStatusEnum.Paid);

            var paidAgentTransferBalanseRequest = _dbLayer.AgentTransferBalanceRequestRepository
                .FirstOrDefault(atb => atb.FromAccountId == accountId && atb.CreationDateTime > paymentDate);

            return paidAgentTransferBalanseRequest != null || paidCashOutRequest != null;
        }

        /// <summary>
        /// Наличие ранних не оплаченных заявок на вывод средств
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="сreationDate">Дата создания заявки</param>
        /// <param name="userId">идентификатор пользователя</param>
        /// <returns>Наличие ранних не оплаченных заявок</returns>
        private bool AvailabilityOfEarlyUnpaidAgentCashOutRequest(Guid accountId, DateTime сreationDate, Guid? userId)
        {
            if(userId is null)
            return _dbLayer.AgentCashOutRequestRepository.FirstOrDefault(acr =>
                 acr.AgentRequisites.AccountOwnerId == accountId && acr.CreationDateTime < сreationDate &&
                 (acr.RequestStatus == AgentCashOutRequestStatusEnum.InProcess ||
                  acr.RequestStatus == AgentCashOutRequestStatusEnum.New) && acr.AccountUserId == null) != null;

            return _dbLayer.AgentCashOutRequestRepository.FirstOrDefault(acr =>
                 acr.AccountUserId == userId && acr.CreationDateTime < сreationDate &&
                 (acr.RequestStatus == AgentCashOutRequestStatusEnum.InProcess ||
                  acr.RequestStatus == AgentCashOutRequestStatusEnum.New)) != null; ;
        }

        /// <summary>
        /// Сменить статус заявки на вывод средств
        /// </summary>
        /// <param name="agentCashOutRequest">Заявка на вывод средств</param>
        /// <param name="changeAgentCashOutRequestStatus">Модель смены статуса заявки
        /// на вывод средств</param>
        private void ChangeAgentCashOutRequestStatus(AgentCashOutRequest agentCashOutRequest,
            ChangeAgentCashOutRequestStatusDto changeAgentCashOutRequestStatus)
        {
            var message =
                $@"Смена статуса для заявки на вывод средств {agentCashOutRequest.Id}-{agentCashOutRequest.RequestNumber} 
                                    с {agentCashOutRequest.RequestStatus.Description()} на {changeAgentCashOutRequestStatus.AgentCashOutRequestStatus.Description()}";

            using var dbScope = _dbLayer.SmartTransaction.Get();
            try
            {
                foreach (var (principleFn, createAgentPaymentAction) in _list)
                    if (principleFn(agentCashOutRequest.RequestStatus,
                            changeAgentCashOutRequestStatus.AgentCashOutRequestStatus))
                        createAgentPaymentAction(agentCashOutRequest);

                agentCashOutRequest.RequestStatus = changeAgentCashOutRequestStatus.AgentCashOutRequestStatus;
                agentCashOutRequest.StatusDateTime = DateTime.Now;
                _dbLayer.AgentCashOutRequestRepository.Update(agentCashOutRequest);
                _dbLayer.Save();

                _logger.Info(message);

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка смены статуса для заявки на вывод средств] {message}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Попытаться получить заявку на вывод средств
        /// </summary>
        /// <param name="requestNumber">Номер заявки</param>
        /// <returns>Заявка на вывод средств</returns>
        private AgentCashOutRequest GetAgentCashOutRequest(string requestNumber) =>
            _dbLayer.AgentCashOutRequestRepository.FirstOrDefault(acr => acr.RequestNumber == requestNumber);

        /// <summary>
        /// Создать входящий агентский платеж
        /// по заявке на вывод средст (При отмене статуса "Оплачена")
        /// </summary>
        /// <param name="agentCashOutRequest">Заявка на вывод средств</param>
        private void CreateInflowAgentPayment(AgentCashOutRequest agentCashOutRequest)
        {
            var message = "Отмена выплаты вознаграждения партнеру " +
                          $"\"{agentCashOutRequest.AgentRequisites.GetAgencyFullName(agentCashOutRequest.AgentRequisites.GetAgentRequisitesType())}\" " +
                          $"по заявке №{agentCashOutRequest.RequestNumber}";

            try
            {
                CreateAgentPayment(agentCashOutRequest, PaymentType.Inflow,
                    $"Отмена выплаты агентского вознаграждения по заявке № {agentCashOutRequest.RequestNumber}");
                _logger.Info(message);
                LogEventHelper.LogEvent(_dbLayer, _accessProvider.ContextAccountId, _accessProvider,
                    LogActions.CancelPartnerRewardPayment, message);
            }
            catch (Exception ex)
            {
                LogEventHelper.LogEvent(_dbLayer, _accessProvider.ContextAccountId, _accessProvider,
                    LogActions.CancelPartnerRewardPayment, $"{message} завершилась ошибкой. Описание ошибки: {ex.GetFullInfo(false)}");
                _handlerException.Handle(ex, $"[Ошибка создания входящего агентского платежа] {message}");
                throw;
            }
        }

        /// <summary>
        /// Создать исходящий агентский платеж
        /// по заявке на вывод средст
        /// </summary>
        /// <param name="agentCashOutRequest">Заявка на вывод средств</param>
        private void CreateOutflowAgentPayment(AgentCashOutRequest agentCashOutRequest)
        {
            var message = "Выплата вознаграждения партнеру " +
                          $"\"{agentCashOutRequest.AgentRequisites.GetAgencyFullName(agentCashOutRequest.AgentRequisites.GetAgentRequisitesType())}\" " +
                          $"по заявке №{agentCashOutRequest.RequestNumber}";
            try
            {
                CreateAgentPayment(agentCashOutRequest, PaymentType.Outflow, message);

                _logger.Info(message);
                LogEventHelper.LogEvent(_dbLayer, _accessProvider.ContextAccountId, _accessProvider, LogActions.PartnerRewardPayment, message);
            }
            catch (Exception ex)
            {
                LogEventHelper.LogEvent(_dbLayer, _accessProvider.ContextAccountId, _accessProvider,
                    LogActions.PartnerRewardPayment, $"{message} завершилась ошибкой. Описание ошибки: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Создать агентский платеж
        /// по заявке на вывод средст
        /// </summary>
        /// <param name="agentCashOutRequest">Заявка на вывод средств</param>
        /// <param name="paymentType">Тип платежа</param>
        /// <param name="comment">Комментарий</param>
        private void CreateAgentPayment(AgentCashOutRequest agentCashOutRequest, PaymentType paymentType,
            string comment)
        {
            var result = _agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                AgentAccountId = agentCashOutRequest.AgentRequisites.AccountOwnerId,
                Sum = agentCashOutRequest.RequestedSum,
                PaymentType = paymentType,
                Comment = comment,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.AgentCashOutRequest,
                Date = DateTime.Now,
                RequestNumber = agentCashOutRequest.RequestNumber
            });

            if (result.Error)
                throw new InvalidOperationException(result.Message);
        }
    }
}
