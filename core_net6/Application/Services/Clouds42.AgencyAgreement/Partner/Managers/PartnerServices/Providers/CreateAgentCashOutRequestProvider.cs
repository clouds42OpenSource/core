﻿using System.Text;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.AgencyAgreement.Partner.Helpers;
using Clouds42.AgencyAgreement.Partner.Mappers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Partner.Models;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер для создания заявок на вывод средств агента
    /// </summary>
    internal class CreateAgentCashOutRequestProvider(
        IUnitOfWork dbLayer,
        AgencyFileHelper agencyFileHelper,
        AgentReportBuildHelper agentReportBuildHelper,
        ILogger42 logger,
        IHandlerException handlerException)
        : ICreateAgentCashOutRequestProvider
    {
        /// <summary>
        /// Получить доступную сумму для вывода
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Доступная сумма для вывода</returns>
        public decimal GetAvailableSumForCashOutRequest(Guid accountId)
        {
            try
            {
                var messageBuilder = new StringBuilder();
                if (!CheckAbilityToCreateCashOutRequest(accountId, messageBuilder))
                    throw new InvalidOperationException(messageBuilder.ToString().Replace("</br>", "\r\n"));
                var agentWallet = dbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == accountId)
                                  ?? throw new NotFoundException($"Не удалось найти агентский кошелек по ID аккаунта {accountId}");

                return agentWallet.AvailableSum;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения доступной суммы для вывода] агентом {accountId} ");
                throw;
            }
        }

        /// <summary>
        /// Проверить возможность создания заявки на вывод средств
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="messageBuilder">Сборщик сообщений о результатах проверки возможности создания заявки</param>
        /// <returns>true - если нет других заявок в статусе "В работе" и сумма кошелька >= 1000</returns>
        public bool CheckAbilityToCreateCashOutRequest(Guid accountId, StringBuilder messageBuilder = null)
        {
            var agentAccount = dbLayer.AccountsRepository.GetAccount(accountId)
                               ?? throw new NotFoundException($"Аккаунт агента по номеру {accountId} не найден");

            var agentWallet = dbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == accountId)
                               ?? throw new NotFoundException($"Кошелек агента {accountId} не найден");

            var activeAgentRequisites = dbLayer.AgentRequisitesRepository.Where(w =>
                w.AccountOwnerId == accountId && w.AgentRequisitesStatus == AgentRequisitesStatusEnum.Verified).ToList();

            if (!activeAgentRequisites.Any())
            {
                var siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
                var routeValueForCreateRequisites = CloudConfigurationProvider.Cp.GetRouteValueForCreateAgentRequisites();
                var linkForCreateRequisites = $"<a href='{siteUrl}/{routeValueForCreateRequisites}'>добавить</a>";
                messageBuilder?.AppendLine($"Для создания заявки на вывод необходимо {linkForCreateRequisites} реквизиты для выплаты");
                return false;
            }

            if (agentWallet.AvailableSum < 1000)
            {
                messageBuilder?.AppendLine("- Для создания заявки необходима минимальная сумма - 1000 руб.");
                return false;
            }

            if (TryGetCashOutRequestNumberWhichIsProcessed(agentAccount, out var cashOutRequestNumber))
            {
                messageBuilder?.AppendLine($@"- У Вас уже создана заявка № {cashOutRequestNumber}.
                                                            Создавать новые заявки Вы сможете после выполнения текущей.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Попробовать получить номер заявки на расходование средств
        /// которая находится в обработке
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="cashOutRequestNumber">Номер заявки</param>
        /// <returns>Результат поиска</returns>
        public bool TryGetCashOutRequestNumberWhichIsProcessed(Account account, out string cashOutRequestNumber)
        {
            cashOutRequestNumber = string.Empty;

            var inProcessAgentCashOutRequests = dbLayer.AgentCashOutRequestRepository.Where(w =>
                w.AgentRequisites.AccountOwnerId == account.Id && w.AccountUserId == null &&
                (w.RequestStatus == AgentCashOutRequestStatusEnum.InProcess ||
                 w.RequestStatus == AgentCashOutRequestStatusEnum.New)).ToList();

            if (!inProcessAgentCashOutRequests.Any())
                return false;

            cashOutRequestNumber = inProcessAgentCashOutRequests.Last().RequestNumber;

            return true;
        }

        /// <summary>
        /// Создать заявку на вывод средств агента
        /// </summary>
        /// <param name="createAgentCashOutRequest">Модель создания заявки</param>
        /// <returns>Id созданной заявки</returns>
        public Guid CreateAgentCashOutRequest(CreateAgentCashOutRequestDto createAgentCashOutRequest)
        {
            logger.Trace($"Создание заявки на вывод средств для агента {createAgentCashOutRequest.AgentRequisitesId} на сумму {createAgentCashOutRequest.PaySum}.");

            var validateResult = ValidateAgentCashOutRequest(createAgentCashOutRequest);

            if (validateResult.Any())
            {
                var message = "Ошибки в форме создания заявки :";
                validateResult.ForEach(w => { message += $"</br> {w.Message}"; });
                throw new ValidateException(message);
            }

            var cashOutRequestId = Guid.NewGuid();

            try
            {
                var agentCashOutRequest = createAgentCashOutRequest.MapToAgentCashOutRequest();
                agentCashOutRequest.StatusDateTime = DateTime.Now;
                agentCashOutRequest.Id = cashOutRequestId;
                dbLayer.AgentCashOutRequestRepository.InsertAgentCashOutRequest(agentCashOutRequest);
                CreateAgentCashOutRequestFiles(agentCashOutRequest.Id, createAgentCashOutRequest.Files);
                dbLayer.Save();

                logger.Trace($"Создание заявки на вывод средств для агента {createAgentCashOutRequest.AgentRequisitesId} на сумму {createAgentCashOutRequest.PaySum} завершено.");

            }
            catch (Exception ex)
            {
                logger.Trace($"Создание заявки на вывод средств для агента {createAgentCashOutRequest.AgentRequisitesId} на сумму {createAgentCashOutRequest.PaySum} завершено с ошибкой. Причина: {ex.GetFullInfo()}.");
                throw;
            }

            return cashOutRequestId;
        }

        /// <summary>
        /// Получить список активных агентских договоров
        /// </summary>
        /// <param name="agentId">ID агента</param>
        /// <returns>Список активных агентских договоров</returns>
        public List<AgentRequisitesInfoDto> GetActiveAgentRequisites(Guid agentId)
        {

            return dbLayer.AgentRequisitesRepository.AsQueryableNoTracking().Where(x =>
                    x.AccountOwnerId == agentId && x.AgentRequisitesStatus == AgentRequisitesStatusEnum.Verified)
                .Select(x => new AgentRequisitesInfoDto
                {
                    Id = x.Id,
                    AccountOwnerIndexNumber = x.AccountOwner.IndexNumber,
                    RequisitesNumber = x.Number,
                    CreationDate = x.CreationDate,
                    AgentRequisitesStatus = x.AgentRequisitesStatus,
                    AgentRequisitesType = x.PhysicalPersonRequisitesId.HasValue
                        ? AgentRequisitesTypeEnum.PhysicalPersonRequisites :
                             x.LegalPersonRequisitesId.HasValue
                                ? AgentRequisitesTypeEnum.LegalPersonRequisites
                                : AgentRequisitesTypeEnum.SoleProprietorRequisites
                })
                .OrderByDescending(x => x.CreationDate)
                .ToList();
        }

        /// <summary>
        /// Распечатать отчет агента
        /// </summary>
        /// <param name="editAgentCashOutRequestDto">Модель редактирования заявки на вывод средств</param>
        /// <returns>Pdf файл на печать</returns>
        public DocumentBuilderResult PrintAgentReport(EditAgentCashOutRequestDto editAgentCashOutRequestDto)
        {
            return agentReportBuildHelper.GetAgentReport(editAgentCashOutRequestDto);
        }

        /// <summary>
        /// Распечатать отчет агента
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки агента</param>
        /// <returns>Pdf файл на печать</returns>
        public DocumentBuilderResult PrintAgentReport(Guid agentCashOutRequestId)
        {
            return agentReportBuildHelper.GetAgentReport(agentCashOutRequestId);
        }

        /// <summary>
        /// Провалидировать модель создания заявки
        /// </summary>
        /// <param name="createAgentCashOutRequestDto">Модель создания заявки</param>
        /// <returns>Результат валидации</returns>
        private List<ValidateResult> ValidateAgentCashOutRequest(CreateAgentCashOutRequestDto createAgentCashOutRequestDto)
        {
            var validationResults = new List<ValidateResult>();

            if (createAgentCashOutRequestDto == null)
            {
                validationResults.Add(new ValidateResult { Success = false, Message = "Форма создания заявки пуста!" });
                return validationResults;
            }

            if (!createAgentCashOutRequestDto.Files.Any())
                validationResults.Add(new ValidateResult { Success = false, Message = "Отсутсвуют файлы отчета!" });

            if (createAgentCashOutRequestDto.PaySum <= 0)
                validationResults.Add(new ValidateResult { Success = false, Message = "Сумма для вывода не может равняться 0!" });

            if (createAgentCashOutRequestDto.AgentRequisitesId == Guid.Empty)
                validationResults.Add(new ValidateResult { Success = false, Message = "Не выбран агентский договор!" });

            if (createAgentCashOutRequestDto.RequestStatus == AgentCashOutRequestStatusEnum.Paid)
                validationResults.Add(new ValidateResult { Success = false, Message = $"Заявку со статусом '{AgentCashOutRequestStatusEnum.Paid.Description()} создать невозможно'!" });

            return validationResults;
        }

        /// <summary>
        /// Создать файлы заявки на вывод средств
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки</param>
        /// <param name="files">Файлы заявки</param>
        private void CreateAgentCashOutRequestFiles(Guid agentCashOutRequestId, List<CloudFileDataDto<byte[]>> files)
        {
            if (files == null || !files.Any())
                return;

            files.ForEach(w =>
            {
                var cloudFileId = agencyFileHelper.CreateAgencyCloudFile(w);

                var agentCashOutRequestFile = new AgentCashOutRequestFile
                {
                    AgentCashOutRequestId = agentCashOutRequestId,
                    CloudFileId = cloudFileId
                };

                dbLayer.AgentCashOutRequestFileRepository.Insert(agentCashOutRequestFile);
            });
        }
    }
}
