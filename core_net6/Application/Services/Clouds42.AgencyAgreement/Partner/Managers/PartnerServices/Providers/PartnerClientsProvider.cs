﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Service.Partner.PartnerClients;
using Clouds42.Domain;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Partner.Validators;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers
{
    /// <summary>
    /// Провайдер для работы с клиентами партнера
    /// </summary>
    internal class PartnerClientsProvider(
        IUnitOfWork dbLayer,
        AccountsBindingDtoValidator accountsBindingDtoValidator,
        ILogger42 logger)
        : IPartnerClientsProvider
    {
        /// <summary>
        /// Привязать аккаунт к партнеру
        /// </summary>
        /// <param name="accountsBinding">Модель связи аккаунтов</param>
        public void BindAccountToPartner(AccountsBindingDto accountsBinding)
        {
            logger.Info($"Подключение аккаунта {accountsBinding.ClientAccountId} к патрнеру {accountsBinding.PartnerAccountId}");
            var validateResult = accountsBindingDtoValidator.ValidateBinding(accountsBinding);

            if (!validateResult.Success)
                throw new ValidateException(validateResult.Message);

            logger.Info("Валидация модели связи завершилась успешно");

            var clientAccount = dbLayer.AccountsRepository.GetAccount(accountsBinding.ClientAccountId);
            clientAccount.ReferralAccountID = accountsBinding.PartnerAccountId;
            dbLayer.AccountsRepository.Update(clientAccount);
            dbLayer.Save();

            logger.Info($"Подключение аккаунта {accountsBinding.ClientAccountId} к патрнеру {accountsBinding.PartnerAccountId} завершилось успешно.");
        }

        /// <summary>
        ///     Поиск аккаунтов по строке поиска
        /// </summary>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>Список аккаунтов</returns>
        public List<AccountInfoDto> SearchAccounts(string searchString)
        {
            return string.IsNullOrWhiteSpace(searchString) ? [] : dbLayer.ExecuteSqlQueryList<AccountInfoDto>(string.Format(SqlQueries.Partners.GetAccounts, searchString));
        }
            
    }
}
