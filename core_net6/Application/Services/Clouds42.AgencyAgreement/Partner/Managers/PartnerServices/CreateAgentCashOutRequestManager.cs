﻿using System.Text;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер создания заявок на вывод средст агента
    /// </summary>
    public class CreateAgentCashOutRequestManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICreateAgentCashOutRequestProvider createAgentCashOutRequestProvider,
        IAgentCashOutRequestProvider agentCashOutRequestProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать заявку на вывод средств агента
        /// </summary>
        /// <param name="createAgentCashOutRequest">Модель создания заявки на вывод средств агента</param>
        /// <returns>Id созданной заявки</returns>
        public ManagerResult<Guid> CreateAgentCashOutRequest(CreateAgentCashOutRequestDto createAgentCashOutRequest)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_CreateCashOutRequest, () => createAgentCashOutRequest.AccountId);
                var cashOutRequestId = createAgentCashOutRequestProvider.CreateAgentCashOutRequest(createAgentCashOutRequest);

                var agentCashOutRequest = agentCashOutRequestProvider.GetAgentCashOutRequest(cashOutRequestId)
                                          ?? throw new NotFoundException($"Заявка на вывод средств по ID {cashOutRequestId} не найдена");

                var message = $"Создана заявка на вывод вознаграждения №{agentCashOutRequest.RequestNumber}.";
                logger.Info(message);
                LogEvent(() => createAgentCashOutRequest.AccountId, LogActions.CreateAgentCashOutRequest, message);

                return Ok(cashOutRequestId);
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"Создание заявки на вывод средств на сумму {createAgentCashOutRequest.PaySum:0.00} руб. завершилось ошибкой.";

                _handlerException.Handle(ex, $"[Ошибка создания заявки на вывод средств на сумму] {createAgentCashOutRequest.PaySum:0.00} акк: {createAgentCashOutRequest.AccountId}");
                LogEvent(() => createAgentCashOutRequest.AccountId, LogActions.CreateAgentCashOutRequest, $"{errorMessage} Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Получить доступную сумму для вывода
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Доступная сумма для вывода</returns>
        public ManagerResult<decimal> GetAvailableSumForCashOutRequest(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_CreateCashOutRequest, () => accountId);
                var result = createAgentCashOutRequestProvider.GetAvailableSumForCashOutRequest(accountId);
                logger.Info($"Получение доступной суммы для вывода аккаунтом {accountId} завершено успешно");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка Получения доступной суммы для вывода аккаунтом] {accountId}");
                return PreconditionFailed<decimal>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список активных реквизитов агента
        /// </summary>
        /// <param name="agentId">ID агента</param>
        /// <returns>Список активных реквизитов агента</returns>
        public ManagerResult<List<AgentRequisitesInfoDto>> GetActiveAgentRequisites(Guid agentId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_GetRequisites, () => agentId);
                var result = createAgentCashOutRequestProvider.GetActiveAgentRequisites(agentId);
                var message = $"Получение списка активных реквизитов агента для аккаунта {agentId}";
                logger.Info(message);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения списка активных реквизитов агента для аккаунта] {agentId}");
                return PreconditionFailed<List<AgentRequisitesInfoDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Печать отчета агента
        /// </summary>
        /// <param name="editAgentCashOutRequestDto">Модель редактирования агентской заявки</param>
        /// <returns>Pdf файл на печать</returns>
        public ManagerResult<DocumentBuilderResult> PrintAgentReport(EditAgentCashOutRequestDto editAgentCashOutRequestDto)
        {
            try
            {
                logger.Info("Печать отчета агента");
                AccessProvider.HasAccess(ObjectAction.Agency_PrintAgentReport, () => editAgentCashOutRequestDto.AccountId);
                return Ok(createAgentCashOutRequestProvider.PrintAgentReport(editAgentCashOutRequestDto));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка печати отчета агента]");
                return PreconditionFailed<DocumentBuilderResult>(ex.Message);
            }
        }

        /// <summary>
        /// Печать отчета агента
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки агента</param>
        /// <returns>Pdf файл на печать</returns>
        public ManagerResult<DocumentBuilderResult> PrintAgentReport(Guid agentCashOutRequestId)
        {
            try
            {
                logger.Info("Печать отчета агента");
                AccessProvider.HasAccess(ObjectAction.Agency_PrintAgentReport, () => AccountIdByAgentCashOutRequestId(agentCashOutRequestId));
                return Ok(createAgentCashOutRequestProvider.PrintAgentReport(agentCashOutRequestId));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка печати отчета агента]");
                return PreconditionFailed<DocumentBuilderResult>(ex.Message);
            }
        }


        /// <summary>
        /// Проверить возможность создания заявки на вывод средств
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Результат проверки</returns>
        public ManagerResult<bool> CheckAbilityToCreateCashOutRequest(Guid accountId)
        {
            try
            {
                logger.Info($"Проверка возможности создания заявки на вывод средств для агента {accountId}");
                AccessProvider.HasAccess(ObjectAction.Agency_CreateCashOutRequest, () => accountId);
                var messageBuilder = new StringBuilder();
                var result =
                    createAgentCashOutRequestProvider.CheckAbilityToCreateCashOutRequest(accountId, messageBuilder);

                return Ok(result, messageBuilder.ToString());
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка проверки возможности создания заявки на вывод средств] {accountId}");
                return PreconditionFailed<bool>(ex.Message);
            }
        }
    }
}
