﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер агентских заявок
    /// </summary>
    public class AgentCashOutRequestManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAgentCashOutRequestProvider agentCashOutRequestProvider,
        IRemoveAgentCashOutRequestProvider removeAgentCashOutRequestProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить список агентских заявок на вывод средств
        /// </summary>
        /// <param name="filter">Фильтр получения агентских заявок на вывод средства</param>
        /// <returns>Список агентских заявок на вывод средств</returns>
        public ManagerResult<PaginationDataResultDto<AgentCashOutRequestInfoDto>> GetAgentCashOutRequests(AgentCashOutRequestsFilterDto filter)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_GetCashOutRequests, () => filter.AccountId);
                var data = agentCashOutRequestProvider.GetAgentCashOutRequests(filter);
                return Ok(data);
            }
            catch (Exception ex)
            {
                var message = "Получение заявок на вывод средств агента завершилось ошибкой.";
                logger.Warn($"{message} Exception: {ex.GetFullInfo()}");

                return PreconditionFailed<PaginationDataResultDto<AgentCashOutRequestInfoDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Удалить заявку на вывод средств агента
        /// </summary>
        /// <param name="cashOutRequestId">ID заявки</param>
        public ManagerResult RemoveAgentCashOutRequest(Guid cashOutRequestId)
        {
            var accountId = AccountIdByAgentCashOutRequestId(cashOutRequestId);
            if (accountId == null)
                return PreconditionFailed($"По ID заявки {cashOutRequestId} не найден ID агента");

            var account = DbLayer.AccountsRepository.GetAccount(accountId.Value);
            var cashOutRequest = agentCashOutRequestProvider.GetAgentCashOutRequest(cashOutRequestId);
            if (cashOutRequest == null)
                return PreconditionFailed($"Не удалось найти заявку по ID {cashOutRequestId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_RemoveCashOutRequest, () => accountId);

                removeAgentCashOutRequestProvider.RemoveAgentCashOutRequest(cashOutRequestId);

                var message = $"Заявка на вывод вознаграждения №{cashOutRequest.RequestNumber} удалена.";
                logger.Info(message);
                LogEvent(() => accountId, LogActions.RemoveAgentCashOutRequest, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"Удаление заявки на вывод средств {cashOutRequest.RequestNumber} для аккаунта {account.GetAccountName()} завершилось ошибкой.";
                _handlerException.Handle(ex, $"[Ошибка удаления заявки на вывод средств] акк: {cashOutRequest.RequestNumber}");
                LogEvent(() => accountId, LogActions.RemoveAgentCashOutRequest, $"{message} Причина: {ex.Message}");

                return PreconditionFailed(ex.Message);
            }
        }
    }
}
