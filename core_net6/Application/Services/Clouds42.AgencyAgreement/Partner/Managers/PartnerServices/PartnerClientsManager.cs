﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.PartnerClients;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер для работы с клиентами партнера
    /// </summary>
    public class PartnerClientsManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IPartnerClientsProvider partnerClientsProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IUnitOfWork _dbLayer = dbLayer;
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Привязать аккаунт к партнеру
        /// </summary>
        /// <param name="accountsBinding">Модель связи аккаунтов</param>
        /// <param name="accountId">Id аккаунта(инициатор)</param>
        public ManagerResult BindAccountToPartner(AccountsBindingDto accountsBinding, Guid accountId)
        {
            var clientAccount = DbLayer.AccountsRepository.GetAccount(accountsBinding.ClientAccountId);
            if (clientAccount == null)
                return PreconditionFailed($"Не удалось найти аккаунт клиента по Id {accountsBinding.ClientAccountId}");

            var partnerAccount = DbLayer.AccountsRepository.GetAccount(accountsBinding.PartnerAccountId);
            if (partnerAccount == null)
                return PreconditionFailed($"Не удалось найти аккаунт партнера по Id {accountsBinding.PartnerAccountId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.Partner_BindAccountToPartner, () => accountId);
                partnerClientsProvider.BindAccountToPartner(accountsBinding);

                var message = $"Аккаунт \"{clientAccount.AccountCaption}\" (№{clientAccount.IndexNumber}) подключен " +
                              $"к патрнеру \"{partnerAccount.AccountCaption}\" (№{partnerAccount.IndexNumber})";
                Logger.Info(message);
                LogEvent(() => accountId, LogActions.BindAccountToPartner, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    $"[Привязка аккаунта \"{clientAccount.AccountCaption}\"(№{clientAccount.IndexNumber}) ]" +
                    $"к патрнеру \"{partnerAccount.AccountCaption}\"(№{partnerAccount.IndexNumber}) завершилось ошибкой {ex.GetFullInfo()}.";

                LogEvent(() => accountId, LogActions.BindAccountToPartner, message);
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Отвязать аккаунт от партнерства
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public ManagerResult UnBindAccountFromPartner(Guid clintId, Guid accountId)
        {
            try
            {
                var clientAccount = DbLayer.AccountsRepository.GetAccount(clintId) 
                    ?? throw new NotFoundException($"Не удалось найти аккаунт клиента по Id {clintId}");

                AccessProvider.HasAccess(ObjectAction.Partner_BindAccountToPartner, () => accountId);

                clientAccount.ReferralAccountID = null;
                _dbLayer.AccountsRepository.Update(clientAccount);
                _dbLayer.Save();

                var message = $"Аккаунт \"{clientAccount.AccountCaption}\" ({clientAccount.IndexNumber}) отключен от патрнера";
                Logger.Info(message);
                LogEvent(() => accountId, LogActions.BindAccountToPartner, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    $"[Отвязка аккаунта \"{clintId} от патрнера завершилась с ошибкой]";

                LogEvent(() => accountId, LogActions.BindAccountToPartner, message);
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        ///     Поиск аккаунтов по строке поиска
        /// </summary>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>Список аккаунтов</returns>
        public ManagerResult<List<AccountInfoDto>> SearchAccounts(string searchString)
        {
            try
            {
                var accounts = partnerClientsProvider.SearchAccounts(searchString);
                return Ok(accounts);
            }
            catch (Exception ex)
            {
                Logger.Warn(ex, $"[Поиск аккаунтов по строке поиска {searchString} завершился ошибкой.]");
                return PreconditionFailed<List<AccountInfoDto>>(ex.Message);
            }
        }

    }
}
