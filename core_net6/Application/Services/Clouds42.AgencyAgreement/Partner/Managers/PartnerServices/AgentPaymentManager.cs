﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер для работы с агентскими платежами
    /// </summary>
    public class AgentPaymentManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICreateAgentPaymentProvider createAgentPaymentProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать платеж в пользу агента.
        /// </summary>
        /// <param name="agencyPaymentCreation">Модель создания агентского платежа</param>
        public ManagerResult CreateAgentPayment(AgencyPaymentCreationDto agencyPaymentCreation)
        {
            AccessProvider.HasAccess(ObjectAction.Agency_CreatePayment);

            var paymentInfo =
                $"{agencyPaymentCreation.PaymentType.Description().ToLower()} платеж на сумму {agencyPaymentCreation.Sum} руб.";

            try
            {
                createAgentPaymentProvider.CreateAgentPayment(agencyPaymentCreation);
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CreateAgentPayment, $"Создан {paymentInfo}");
                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"Ошибка при создании платежа, {paymentInfo}";
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CreateAgentPayment, $"{message}");
                _handlerException.Handle(ex, $"[{message}]");
                return PreconditionFailed(ex.Message);
            }
        }

    }
}
