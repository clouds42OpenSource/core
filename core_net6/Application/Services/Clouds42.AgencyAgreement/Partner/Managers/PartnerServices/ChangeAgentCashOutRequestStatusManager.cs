﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер смены статуса заявки на вывод средств
    /// </summary>
    public class ChangeAgentCashOutRequestStatusManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IChangeAgentCashOutRequestStatusProvider changeAgentCashOutRequestStatusProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Сменить статус заявки на вывод средств
        /// </summary>
        /// <param name="changeAgentCashOutRequestStatus">Модель смены статуса заявки
        /// на вывод средств</param>
        /// <returns>Успешность смены статуса</returns>
        public ManagerResult ChangeAgentCashOutRequestStatus(
            ChangeAgentCashOutRequestStatusDto changeAgentCashOutRequestStatus)
        {
            try
            {
                var canChangeAgentCashOutRequestStatus =
                    changeAgentCashOutRequestStatusProvider.TryChangeAgentCashOutRequestStatus(
                        changeAgentCashOutRequestStatus, out var errorMessage);

                if (!canChangeAgentCashOutRequestStatus)
                    return PreconditionFailed(errorMessage);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Смена статуса заявки на вывод средств завершилось ошибкой.]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
