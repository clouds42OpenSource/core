﻿using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Selectors
{
    /// <summary>
    /// Класс для выбора данных активности сервиса
    /// </summary>
    public class PartnerServiceActivityDataSelector(
        IUnitOfWork dbLayer,
        IResourceDataProvider resourceDataProvider,
        IServiceExtensionDatabaseDataProvider extensionDatabaseDataProvider)
        : IPartnerServiceActivityDataSelector
    {
        /// <summary>
        /// Выбрать данные
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Модель данных сервиса для аккаунта</returns>
        public ServiceDataForAccountDto SelectData(Guid accountId, Guid serviceId) =>
            (from service in dbLayer.BillingServiceRepository.WhereLazy(s => s.Id == serviceId)
             join resConf in dbLayer.ResourceConfigurationRepository.WhereLazy() on accountId equals
                 resConf.AccountId into resConfs
             from resConf in resConfs.Where(rc => rc.BillingServiceId == serviceId).Take(1).DefaultIfEmpty()
             from rent1CResConf in resConfs
                 .Where(rc => rc.BillingService.SystemService == Clouds42Service.MyEnterprise).Take(1)
                 .DefaultIfEmpty()
             join groupedResourcesByService in resourceDataProvider.GetGroupedResourcesByService(accountId)
                 on service.Id equals groupedResourcesByService.ServiceId into resources
             join serviceExtensionDatabase in extensionDatabaseDataProvider
                     .GetAccountServiceExtensionDatabases(accountId, serviceId) on service.Id equals
                 serviceExtensionDatabase.ServiceId into
                 serviceExtensionDatabases
             select new ServiceDataForAccountDto
             {
                 Service = service,
                 ServiceResourcesConfiguration = resConf,
                 Rent1CResourcesConfiguration = rent1CResConf,
                 Resources = resources.SelectMany(res => res.Resources).Where(res => res.Subject != null)
                     .ToList(),
                 DatabasesIds = serviceExtensionDatabases.Where(se =>
                         se.ExtensionState ==
                         ServiceExtensionDatabaseStatusEnum.DoneInstall ||
                         se.ExtensionState ==
                         ServiceExtensionDatabaseStatusEnum.ProcessingInstall)
                     .Select(extDb => extDb.Id).ToList()
             }).FirstOrDefault() ??
            throw new NotFoundException($"Не удалось получить данные сервиса '{serviceId}' по аккаунту '{accountId}'");
    }
}
