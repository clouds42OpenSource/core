﻿using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Managers.PartnerServices
{
    /// <summary>
    /// Менеджер для работы с данными агентских заявок
    /// </summary>
    public class AgentCashOutRequestDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAgentCashOutRequestDataProvider agentCashOutRequestDataProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить заявки на расходование средств в статусе "Новая"
        /// </summary>
        /// <returns>Заявки на расходование средств в статусе "Новая"</returns>
        public ManagerResult<AgentCashOutRequestsInStatusNewDto> GetAgentCashOutRequestsInStatusNew()
        {
            var message = "Получение заявок на расходование средств в статусе \"Новая\"";
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_GetAgentCashOutRequestsInStatusNew);
                var data = agentCashOutRequestDataProvider.GetAgentCashOutRequestsInStatusNew();
                logger.Info(message);

                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");
                return PreconditionFailed<AgentCashOutRequestsInStatusNewDto>(ex.Message);
            }
        }
    }
}
