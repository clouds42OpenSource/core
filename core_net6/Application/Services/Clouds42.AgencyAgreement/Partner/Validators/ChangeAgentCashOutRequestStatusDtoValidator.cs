﻿using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;

namespace Clouds42.AgencyAgreement.Partner.Validators
{
    /// <summary>
    /// Валидатор модели смены статуса заявки на раходование средств
    /// </summary>
    public static class ChangeAgentCashOutRequestStatusDtoValidator
    {
        /// <summary>
        /// Выполнить валидацию
        /// </summary>
        /// <param name="changeAgentCashOutRequestStatus">Модель смены статуса заявки на раходование средств</param>
        public static void Validate(
            this ChangeAgentCashOutRequestStatusDto changeAgentCashOutRequestStatus)
        {
            if (changeAgentCashOutRequestStatus == null)
                throw new ArgumentException("Тело запроса отсутствует или заполнено не корректно.");

            if (string.IsNullOrEmpty(changeAgentCashOutRequestStatus.RequestNumber))
                throw new ArgumentException(
                    $"Поле {nameof(changeAgentCashOutRequestStatus.RequestNumber)} отсутствует.");
        }
    }
}
