﻿using System.Globalization;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using PagedListExtensionsNetFramework;

namespace Clouds42.AgencyAgreement.Partner.Providers
{
    public class MonthlyChargeDto
    {
        public decimal MonthlyCharge { get; set; }
    }

    /// <summary>
    /// Провайдер данных партнера
    /// </summary>
    public class PartnerDataProvider(IUnitOfWork dbLayer, IAgencyAgreementDataProvider agencyAgreementDataProvider)
        : IPartnerDataProvider
    {
        private readonly Lazy<string> _siteAuthorityUrl = new(CloudConfigurationProvider.Cp.GetSiteAuthorityUrl);

        /// <summary>
        /// Получить сводную информацию для партнера
        /// </summary>
        /// <param name="accountId">Id аккаунта партнера</param>
        /// <returns>Сводная информация для партнера</returns>
        public SummaryInformationForPartnerDto GetSummaryInformationForPartner(Guid accountId)
        {
            var actualAgencyAgreement = agencyAgreementDataProvider.GetActualAgencyAgreement();

            var account = dbLayer.AccountsRepository.GetAccount(accountId)
                          ?? throw new NotFoundException($"Аккаунт агента по ID {accountId} не найден");

            var locale = account.AccountConfiguration.Locale.Currency;
            var availableToPay = GetAvailableSum(accountId);

            var needApplyAgencyAgreement = dbLayer.AccountsRepository.NeedApplyAgencyAgreement(accountId);

            return new SummaryInformationForPartnerDto
            {
                CompanyName = account.AccountCaption,
                ReferrerLink = GetAbsoluteReferrerLink(accountId),
                CurrencySymbol = locale,
                ActualAgencyAgreement = new AgencyAgreementDetailsDto
                {
                    EffectiveDate = actualAgencyAgreement.EffectiveDate,
                    MyDiskRewardPercent = actualAgencyAgreement.MyDiskRewardPercent,
                    ServiceOwnerRewardPercent = actualAgencyAgreement.ServiceOwnerRewardPercent,
                    Rent1CRewardPercent = actualAgencyAgreement.Rent1CRewardPercent,
                    Rent1CRewardPercentForVipAccounts = actualAgencyAgreement.Rent1CRewardPercentForVipAccounts,
                    MyDiskRewardPercentForVipAccounts = actualAgencyAgreement.MyDiskRewardPercentForVipAccounts,
                    ServiceOwnerRewardPercentForVipAccounts = actualAgencyAgreement.ServiceOwnerRewardPercentForVipAccounts,
                    Name = actualAgencyAgreement.Name,
                    Id = actualAgencyAgreement.Id

                },
                NeedApplyAgencyAgreement = needApplyAgencyAgreement,
                TotalEarned = GetTotalEarnedSum(accountId),
                AvailableToPay = availableToPay,
                NextEffectiveAgencyAgreement = agencyAgreementDataProvider.TryGetNextEffectiveAgencyAgreement()
            };
        }

        public async Task<ManagerResult<decimal>> GetMonthlyCharge(Guid accountId)
        {
            try
            {
                var actualAgencyAgreement = agencyAgreementDataProvider.GetActualAgencyAgreement();

                var rent1CRewardPercent = actualAgencyAgreement.Rent1CRewardPercent / 100;
                var myDiskRewardPercent = actualAgencyAgreement.MyDiskRewardPercent / 100;
                var serviceOwnerRewardPercent = actualAgencyAgreement.ServiceOwnerRewardPercent / 100;
                var rent1CRewardPercentForVipAccounts = actualAgencyAgreement.Rent1CRewardPercentForVipAccounts / 100;
                var myDiskRewardPercentForVipAccounts = actualAgencyAgreement.MyDiskRewardPercentForVipAccounts / 100;

                var formattedQuery = string.Format(CultureInfo.InvariantCulture, SqlQueries.Partners.GetMonthlyCharge, rent1CRewardPercent,
                    rent1CRewardPercentForVipAccounts, myDiskRewardPercent, myDiskRewardPercentForVipAccounts,
                    serviceOwnerRewardPercent, accountId);

                return (await dbLayer.ExecuteSqlQueryAsync<MonthlyChargeDto>(formattedQuery)).MonthlyCharge
                    .ToOkManagerResult();
            }

            catch
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<decimal>(
                     "Ошибка получения ежемесячного уровня дохода");
            }

        }

        /// <summary>
        /// Получить доступную сумму для вывода
        /// </summary>
        /// <param name="accountId">Id аккаунта партнера</param>
        /// <returns>Доступная сумма для вывода</returns>
        private decimal GetAvailableSum(Guid accountId)
        {
            var agentWallet = dbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == accountId)
                              ?? throw new NotFoundException($"Кошелек агента {accountId} не найден");

            return agentWallet.AvailableSum;
        }

        /// <summary>
        /// Получить сумму всех входящих транзакций агента
        /// </summary>
        /// <param name="accountId">Id аккаунта партнера</param>
        /// <returns>Сумма всех входящих транзакций агента</returns>
        private decimal GetTotalEarnedSum(Guid accountId) => dbLayer.AgentPaymentRepository.WhereLazy(payment =>
                payment.AccountOwnerId == accountId && payment.PaymentType == PaymentType.Inflow &&
                (payment.ClientPaymentId != null ||
                 payment.AgentPaymentSourceType == AgentPaymentSourceTypeEnum.ManualInput))
            .Sum(paymentInfo => (decimal?)paymentInfo.Sum) ?? 0;

        /// <summary>
        /// Получить абсолютную рефферальную ссылку.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>Рефферальная ссылка.</returns>
        private string GetAbsoluteReferrerLink(Guid accountId)
            => new Uri($"{_siteAuthorityUrl.Value}/signup?ReferralAccountId={accountId}").AbsoluteUri;
        
    }
}
