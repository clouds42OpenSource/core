﻿using Clouds42.AgencyAgreement.Contracts.AgentFileStorage;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Partner.Helpers
{
    /// <summary>
    /// Хэлпер для работы с агентскими файлами
    /// </summary>
    public class AgencyFileHelper(IUnitOfWork dbLayer, IAgentFileStorageProvider agentFileStorageProvider)
    {
        /// <summary>
        /// Создать агентский файл облака
        /// </summary>
        /// <param name="fileData">Объект файла</param>
        /// <returns>Id файла облака</returns>
        public Guid CreateAgencyCloudFile(CloudFileDataDto<byte[]> fileData)
        {
            var file = new CloudFile
            {
                Id = Guid.NewGuid(),
                FileName = agentFileStorageProvider.GenerateNewFileName(fileData.FileName),
                Content = fileData.Content,
                ContentType = fileData.ContentType
            };

            dbLayer.CloudFileRepository.Insert(file);
            dbLayer.Save();

            return file.Id;
        }
    }
}
