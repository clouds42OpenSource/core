﻿using System.Linq.Expressions;
using Clouds42.Configurations.Configurations;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.DataContracts.BaseModel;
using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData;
using Clouds42.HandlerExeption.Contract;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.PdfDocuments;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AgencyAgreement.Partner.Helpers
{
    /// <summary>
    /// Хэлпер для построения отчета агента
    /// </summary>
    public class AgentReportBuildHelper(
        IUnitOfWork dbLayer,
        PrintedHtmlFormBuilder printedHtmlFormBuilder,
        IHandlerException handlerException,
        IAgencyAgreementDataProvider agencyAgreementDataProvider)
    {
        private readonly Lazy<Guid> _printHtmlFormId = new(CloudConfigurationProvider.AgentRequisites.GetAgentReportPrintHtmlFormId);

        /// <summary>
        /// Получить сгенерированный отчет агента
        /// </summary>
        /// <param name="editAgentCashOutRequestDto">Модель редактирования заявки на вывод стредств</param>
        /// <returns>Сгенерированный отчет агента</returns>
        public DocumentBuilderResult GetAgentReport(EditAgentCashOutRequestDto editAgentCashOutRequestDto)
        {
            var agentReport = GenerateAgentReportDto(editAgentCashOutRequestDto);
            return GetAgentReportPdfDocument(agentReport);
        }

        /// <summary>
        /// Получить сгенерированный отчет агента
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки агента</param>
        /// <returns>Сгенерированный отчет агента</returns>
        public DocumentBuilderResult GetAgentReport(Guid agentCashOutRequestId)
        {
            var agentReport = GenerateAgentReportDto(agentCashOutRequestId);
            return GetAgentReportPdfDocument(agentReport);
        }

        /// <summary>
        /// Получить pdf документ отчета агента
        /// </summary>
        /// <param name="agentReport">Модель отчета агента</param>
        /// <returns>pdf документ</returns>
        private DocumentBuilderResult GetAgentReportPdfDocument(AgentReportDto agentReport)
        {
            try
            {
                var pdfDocumentParams = new PdfDocumentParams
                {
                    MarginTop = PdfDocumentConstants.MarginTopForAgentReport,
                    MarginBottom = PdfDocumentConstants.MarginBottomForAgentReport
                };

                return printedHtmlFormBuilder.Build(agentReport,
                    GetPrintedHtmlForm(), "Отчет Агента", pdfDocumentParams);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, 
                    $"[Ошибка генерации отчета агента по реквизитам] {agentReport.AgentRequisitesNumber}");
                throw;
            }
        }

        /// <summary>
        /// Получить печатную форму Html
        /// </summary>
        /// <returns></returns>
        private PrintedHtmlFormDto GetPrintedHtmlForm()
        {
            var printedHtmlForm =
                dbLayer.PrintedHtmlFormRepository.FirstOrDefault(w => w.Id == _printHtmlFormId.Value);

            return new PrintedHtmlFormDto
            {
                Id = printedHtmlForm.Id,
                Name = printedHtmlForm.Name,
                HtmlData = printedHtmlForm.HtmlData,
                Files = printedHtmlForm.Files.Select(w => new CloudFileDto
                {
                    Id = w.CloudFile.Id,
                    FileName = w.CloudFile.FileName,
                    ContentType = w.CloudFile.ContentType,
                    Bytes = w.CloudFile.Content
                }).ToList()
            };
        }

        /// <summary>
        /// Сгенерировать модель создания отчета агента
        /// </summary>
        /// <param name="editAgentCashOutRequestDto">Модель редактирования заявки на вывод стредств</param>
        /// <returns>Модель создания отчета агента</returns>
        private AgentReportDto GenerateAgentReportDto(EditAgentCashOutRequestDto editAgentCashOutRequestDto)
        {
            try
            {
                var agentAccount = dbLayer.AccountsRepository
                                       .AsQueryableNoTracking()
                                       .Include(x => x.AccountConfiguration)
                                       .ThenInclude(x => x.Supplier)
                                       .FirstOrDefault(x => x.Id == editAgentCashOutRequestDto.AccountId) ??
                                   throw new NotFoundException(
                                       $"Аккаунт агента по номеру {editAgentCashOutRequestDto.AccountId} не найден");

                var agentRequisites = GetAgentRequisitesOrThrowException(editAgentCashOutRequestDto.AgentRequisitesId);

                var agentCashOutRequestDateTime = !editAgentCashOutRequestDto.Id.IsNullOrEmpty()
                    ? GetAgentCashOutRequestOrThrowException(editAgentCashOutRequestDto.Id).CreationDateTime
                    : DateTime.Now;

                Expression<Func<AgentCashOutRequest, bool>> paymentsSearchFilter =
                    w =>
                        w.AgentRequisites.AccountOwnerId == editAgentCashOutRequestDto.AccountId &&
                        w.RequestStatus == AgentCashOutRequestStatusEnum.Paid && w.CreationDateTime < agentCashOutRequestDateTime;

                var agentPayments = GetAgentPayments(editAgentCashOutRequestDto.AccountId, paymentsSearchFilter, agentCashOutRequestDateTime);

                var agentReport = CreateAgentReportDto(agentAccount, agentRequisites, agentPayments,
                    DateTime.Now);

                return agentReport;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания модели отчета агента] для аккаунта {editAgentCashOutRequestDto.AccountId}");
                throw;
            }

        }

        /// <summary>
        /// Сгенерировать модель создания отчета агента
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки агента</param>
        /// <returns>Модель создания отчета агента</returns>
        protected AgentReportDto GenerateAgentReportDto(Guid agentCashOutRequestId)
        {
            try
            {
                var agentCashOutRequest = dbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == agentCashOutRequestId)
                                          ?? throw new NotFoundException($"Заявка на вывод средств по ID {agentCashOutRequestId} не найдена");

                var agentRequisites = GetAgentRequisitesOrThrowException(agentCashOutRequest.AgentRequisitesId);

                var agentAccount = dbLayer.AccountsRepository
                                       .AsQueryableNoTracking()
                                       .Include(x => x.AccountConfiguration)
                                       .ThenInclude(x => x.Supplier)
                                       .FirstOrDefault(x => x.Id == agentRequisites.AccountOwnerId) 
                                   ?? throw new NotFoundException($"Аккаунт агента по номеру {agentRequisites.AccountOwnerId} не найден");

                Expression<Func<AgentCashOutRequest, bool>> paymentsSearchFilter =
                    w =>
                        w.AgentRequisites.AccountOwnerId == agentRequisites.AccountOwnerId &&
                        w.RequestStatus == AgentCashOutRequestStatusEnum.Paid &&
                        w.Id != agentCashOutRequest.Id && w.CreationDateTime < agentCashOutRequest.CreationDateTime;

                var agentPayments = GetAgentPayments(agentRequisites.AccountOwnerId, paymentsSearchFilter, agentCashOutRequest.CreationDateTime);

                var agentReport = CreateAgentReportDto(agentAccount, agentRequisites, agentPayments,
                    agentCashOutRequest.CreationDateTime);

                return agentReport;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка создания модели отчета агента по заявке] {agentCashOutRequestId}");
                throw;
            }
        }

        /// <summary>
        /// Получить реквизиты агента или выкинуть исключение
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов агента</param>
        /// <returns>Реквизиты агента</returns>
        private Domain.DataModels.billing.AgentRequisites.AgentRequisites GetAgentRequisitesOrThrowException(Guid agentRequisitesId)
            => dbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == agentRequisitesId)
                ?? throw new NotFoundException($"Реквизиты по ID {agentRequisitesId} не найдены");

        /// <summary>
        /// Получить заявку или выкинуть исключение
        /// </summary>
        /// <param name="agentCashOutRequestId">Id заявки</param>
        /// <returns>Заявка на расходование средств</returns>
        private AgentCashOutRequest GetAgentCashOutRequestOrThrowException(Guid agentCashOutRequestId)
            => dbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == agentCashOutRequestId)
               ?? throw new NotFoundException($"Заявка по ID {agentCashOutRequestId} не найдена");

        /// <summary>
        /// Сформировать модель отчета агента
        /// </summary>
        /// <param name="agentAccount">Аккаунт агента</param>
        /// <param name="agentRequisites">Реквизиты агента</param>
        /// <param name="agentPayments">Список агентских платежей</param>
        /// <param name="periodTo">Конечный период отчета</param>
        /// <returns>Модель отчета агента</returns>
        private AgentReportDto CreateAgentReportDto(Account agentAccount, Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites,
            List<AgentPayment> agentPayments, DateTime periodTo)
        {
            var agentName = dbLayer.AgentRequisitesRepository
                .AsQueryableNoTracking()
                .Where(x => x.Id == agentRequisites.Id)
                .Select(x => x.LegalPersonRequisitesId.HasValue
                    ?
                    x.LegalPersonRequisites.HeadFullName
                    : x.PhysicalPersonRequisitesId.HasValue
                        ? x.PhysicalPersonRequisites.FullName
                        : x.SoleProprietorRequisites.FullName)
                .FirstOrDefault();
                
            var agentReport = new AgentReportDto
                {
                    PrefixForFullName = agentRequisites.GetPrefixForFullNameInAgentReport(), PeriodTo = periodTo,
                    PeriodFrom = GetPeriodFromForAgentReport(agentPayments, periodTo),
                    AgentPayments = agentPayments,
                    SupplierReferenceName = agentAccount.AccountConfiguration?.Supplier?.Name,
                    AgentRewardSum = agentPayments.Sum(w => w.Sum),
                    AgentRequisitesNumber = StringExtension.ConvertToContractNumber(agentAccount.IndexNumber, agentRequisites.Number),
                    ActualAgencyAgreementEffectiveDate = agencyAgreementDataProvider.GetActualAgencyAgreement().EffectiveDate,
                    AgentRequisites = agentRequisites,
                    AgentName = agentName
            };

            return agentReport;
        }

        /// <summary>
        /// Получить начальный период отчета
        /// </summary>
        /// <param name="agentPayments">Список агентских платежей</param>
        /// <param name="customPeriodFrom">Начальный период (если нет платежей и необходимо указать)</param>
        /// <returns>Начальный период отчета</returns>
        private DateTime GetPeriodFromForAgentReport(List<AgentPayment> agentPayments, DateTime? customPeriodFrom = null)
        {
            if (!agentPayments.Any())
                return customPeriodFrom ?? DateTime.Now;

            return agentPayments.First().PaymentDateTime;
        }

        /// <summary>
        /// Получить начальный период для формирования отчета агента
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="paymentsSearchFilter">Фильтр для поиска платежей</param>
        /// <param name="agentCashOutRequestDateTime">Дата создания агентского кошелька</param>
        /// <returns>Начальный период для формирования отчета агента</returns>
        private List<AgentPayment> GetAgentPayments(Guid accountId,
            Expression<Func<AgentCashOutRequest, bool>> paymentsSearchFilter,
            DateTime? agentCashOutRequestDateTime = null)
        {
            try
            {
                var periodTo = agentCashOutRequestDateTime ?? DateTime.Now;

                var paidAgentCashOutRequests =
                    dbLayer.AgentCashOutRequestRepository.WhereLazy(paymentsSearchFilter).ToList();

                var paidAgentTransferBalanceRequests = dbLayer.AgentTransferBalanceRequestRepository
                    .WhereLazy(atb => atb.FromAccountId == accountId && atb.CreationDateTime < periodTo).ToList();

                List<AgentPayment> agentPayments;
                if (!paidAgentCashOutRequests.Any() && !paidAgentTransferBalanceRequests.Any())
                {
                    agentPayments = dbLayer.AgentPaymentRepository.Where(agentPayment =>
                        agentPayment.AccountOwnerId == accountId && agentPayment.PaymentType == PaymentType.Inflow && agentPayment.Sum > 0 &&
                        agentPayment.AgentPaymentSourceType != AgentPaymentSourceTypeEnum.AgentCashOutRequest &&
                        agentPayment.AgentPaymentSourceType != AgentPaymentSourceTypeEnum.AgentTransferBalanseRequest &&
                        (agentPayment.Payment != null && agentPayment.PaymentDateTime <= periodTo ||
                        agentPayment.AgentPaymentRelation != null && agentPayment.PaymentEntryDateTime != null && agentPayment.PaymentEntryDateTime <= periodTo))
                        .OrderBy(w => w.PaymentDateTime).ToList();

                    return agentPayments;
                }

                var lastPaidAgentCashOutRequests =
                    paidAgentCashOutRequests.MaxBy(cr => cr.CreationDateTime)
                        ?.CreationDateTime ?? DateTime.MinValue;

                var paidAgentTransferBalanceRequest =
                    paidAgentTransferBalanceRequests.MaxBy(cr => cr.CreationDateTime)
                        ?.CreationDateTime ?? DateTime.MinValue;

                var periodFrom = lastPaidAgentCashOutRequests > paidAgentTransferBalanceRequest
                    ? lastPaidAgentCashOutRequests
                    : paidAgentTransferBalanceRequest;

                agentPayments = dbLayer.AgentPaymentRepository
                    .WhereLazy(agentPayment =>
                        agentPayment.AccountOwnerId == accountId && agentPayment.PaymentType == PaymentType.Inflow && agentPayment.Sum > 0 &&
                        agentPayment.AgentPaymentSourceType != AgentPaymentSourceTypeEnum.AgentCashOutRequest &&
                        agentPayment.AgentPaymentSourceType != AgentPaymentSourceTypeEnum.AgentTransferBalanseRequest &&
                        (agentPayment.Payment != null && agentPayment.PaymentDateTime >= periodFrom && agentPayment.PaymentDateTime <= periodTo ||
                         agentPayment.AgentPaymentRelation != null && agentPayment.PaymentEntryDateTime != null &&
                          agentPayment.PaymentEntryDateTime >= periodFrom && agentPayment.PaymentEntryDateTime <= periodTo))
                    .OrderBy(w => w.PaymentDateTime).ToList();

                return agentPayments;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения списка платежей для аккаунта] {accountId}");
                throw;
            }
        }
    }
}
