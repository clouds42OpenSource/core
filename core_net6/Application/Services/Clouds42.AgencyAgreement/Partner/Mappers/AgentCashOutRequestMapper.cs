﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.DataModels.billing.AgentRequisites;

namespace Clouds42.AgencyAgreement.Partner.Mappers
{
    /// <summary>
    /// Маппер моделей агентских заявок на вывод средств
    /// </summary>
    public static class AgentCashOutRequestMapper
    {
        /// <summary>
        /// Смапить Dto модель заявки к доменной модели 
        /// </summary>
        /// <param name="createAgentCashOutRequestDto">Dto модель заявки</param>
        /// <returns>Доменная модель заявки</returns>
        public static AgentCashOutRequest MapToAgentCashOutRequest(this CreateAgentCashOutRequestDto createAgentCashOutRequestDto)
        {
            return new AgentCashOutRequest
            {
                AgentRequisitesId = createAgentCashOutRequestDto.AgentRequisitesId,
                RequestStatus = createAgentCashOutRequestDto.RequestStatus,
                RequestedSum = createAgentCashOutRequestDto.TotalSum
            };
        }

        /// <summary>
        /// Смапить доменную модель заявки к модели редактирования
        /// </summary>
        /// <param name="agentCashOutRequest"></param>
        /// <returns>Модель редактирования заявки</returns>
        public static EditAgentCashOutRequestDto MapToEditAgentCashOutRequestDto(
            this AgentCashOutRequest agentCashOutRequest)
        {
            return new EditAgentCashOutRequestDto
            {
                Id = agentCashOutRequest.Id,
                RequestNumber = agentCashOutRequest.RequestNumber,
                AccountId = agentCashOutRequest.AgentRequisites.AccountOwnerId,
                AgentRequisitesId = agentCashOutRequest.AgentRequisitesId,
                IsEditMode = true,
                RequestStatus = agentCashOutRequest.RequestStatus,
                TotalSum = agentCashOutRequest.RequestedSum,
                Files = agentCashOutRequest.AgentCashOutRequestFiles.Select(w => w.MapToFileDataDto()).ToList(),
            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="agentCashOutRequestFile">Модель файла заявки на вывод средств</param>
        /// <returns>Объект типа FileDataDto</returns>
        public static CloudFileDataDto<byte[]> MapToFileDataDto(this AgentCashOutRequestFile agentCashOutRequestFile)
        {
            return new CloudFileDataDto<byte[]>
            {
                CloudFileId = agentCashOutRequestFile.CloudFileId,
                FileName = agentCashOutRequestFile.CloudFile.FileName,
                ContentType = agentCashOutRequestFile.CloudFile.ContentType,
                Content = agentCashOutRequestFile.CloudFile.Content

            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="legalPersonRequisites">Реквизиты юр лица</param>
        /// <returns>Агентские реквизиты для выплаты</returns>
        public static AgentRequisitesForPaymentDto MapToAgentRequisitesForPaymentDto(
            this LegalPersonRequisites legalPersonRequisites)
        {
            return new AgentRequisitesForPaymentDto
            {
                Inn = legalPersonRequisites.Inn,
                BankName = legalPersonRequisites.BankName,
                SettlementAccount = legalPersonRequisites.SettlementAccount,
                Recipient = legalPersonRequisites.OrganizationName
            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="physicalPersonRequisites">Реквизиты физ лица</param>
        /// <returns>Агентские реквизиты для выплаты</returns>
        public static AgentRequisitesForPaymentDto MapToAgentRequisitesForPaymentDto(
            this PhysicalPersonRequisites physicalPersonRequisites)
        {
            return new AgentRequisitesForPaymentDto
            {
                Inn = physicalPersonRequisites.Inn,
                BankName = physicalPersonRequisites.BankName,
                SettlementAccount = physicalPersonRequisites.SettlementAccount,
                Recipient = physicalPersonRequisites.FullName
            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="soleProprietorRequisites">Реквизиты ИП</param>
        /// <returns>Агентские реквизиты для выплаты</returns>
        public static AgentRequisitesForPaymentDto MapToAgentRequisitesForPaymentDto(
            this SoleProprietorRequisites soleProprietorRequisites)
        {
            return new AgentRequisitesForPaymentDto
            {
                Inn = soleProprietorRequisites.Inn,
                BankName = soleProprietorRequisites.BankName,
                SettlementAccount = soleProprietorRequisites.SettlementAccount,
                Recipient = soleProprietorRequisites.FullName
            };
        }
    }
}
