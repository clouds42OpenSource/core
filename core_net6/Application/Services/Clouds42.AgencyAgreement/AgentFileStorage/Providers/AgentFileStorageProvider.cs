﻿using Clouds42.AccountDatabase.Helpers;
using Clouds42.AgencyAgreement.AgentFileStorage.Constants;
using Clouds42.AgencyAgreement.Contracts.AgentFileStorage;
using Clouds42.Configurations.Configurations;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;

namespace Clouds42.AgencyAgreement.AgentFileStorage.Providers
{
    /// <summary>
    /// Провайдер для работы с файловым хранилищем агента
    /// </summary>
    internal class AgentFileStorageProvider(IUnitOfWork dbLayer) : IAgentFileStorageProvider
    {
        private readonly Lazy<string> _pathToFileStorageAgents = new(CloudConfigurationProvider.AgentConfigurations.PathToFileStorageAgents);

        /// <summary>
        /// Получить путь к файловому хранилищу агента
        /// </summary>
        /// <param name="accountId">Id агента(аккаунта)</param>
        /// <returns>Путь к файловому хранилищу</returns>
        public string GetPathToFileStorageAgent(Guid accountId)
        {
            var pathToFileStorage = _pathToFileStorageAgents.Value;
            var account = dbLayer.AccountsRepository.GetAccount(accountId);
            var companyName = GenerateCompanyNameHelper.GetCompanyName(account);
            var pathToFileStorageAgent = Path.Combine(pathToFileStorage, companyName);

            if (!Directory.Exists(pathToFileStorageAgent))
            {
                Directory.CreateDirectory(pathToFileStorageAgent);
            }

            return pathToFileStorageAgent;
        }

        /// <summary>
        /// Получить путь для файла агента
        /// </summary>
        /// <param name="accountId">Id аккаунта(агента)</param>
        /// <param name="fileName">Название файла</param>
        /// <returns>Путь для файла агента</returns>
        public string GetPathForFileAgent(Guid accountId, string fileName)
        {
            var pathForFileAgent = Path.Combine(GetPathToFileStorageAgent(accountId), fileName);
            return pathForFileAgent;
        }

        /// <summary>
        /// Получить временную папку агентского хранилища
        /// </summary>
        /// <returns>Временная папка агентского хранилища</returns>
        public string GetTemporaryFolderInAgentStorage()
        {
            var pathToFileStorage = _pathToFileStorageAgents.Value;
            var tempFolderPath = Path.Combine(pathToFileStorage, AgentFileStorageConst.TempFolderName);

            if (!Directory.Exists(tempFolderPath))
                Directory.CreateDirectory(tempFolderPath);

            return tempFolderPath;
        }

        /// <summary>
        /// Сохранить файл в файловое хранилище агента
        /// </summary>
        /// <param name="accountId">Id аккаунта(агента)</param>
        /// <param name="file">Файл</param>
        /// <param name="fileName">Название файла</param>
        public void SaveFileInFileStorageAgent(Guid accountId, IFormFile file, string fileName)
        {
            var pathForFileAgent = GetPathForFileAgent(accountId, fileName);
            using Stream fileStream = new FileStream(pathForFileAgent, FileMode.Create);
            file.CopyTo(fileStream);
        }

        /// <summary>
        /// Проверить наличие файла в файловом хранилище агента
        /// </summary>
        /// <param name="accountId">Id агента</param>
        /// <param name="fileName">Название файла</param>
        /// <returns>Наличие файла</returns>
        public bool IsExistFileInAgentStorage(Guid accountId, string fileName)
        {
            var pathForFileAgent = GetPathForFileAgent(accountId, fileName);

            if (!File.Exists(pathForFileAgent))
                return false;

            return true;
        }

        /// <summary>
        /// Сгенерировать новое имя файлу
        /// </summary>
        /// <param name="oldFileName">Старое имя файла</param>
        /// <returns>Новое имя файла</returns>
        public string GenerateNewFileName(string oldFileName) => $"{Guid.NewGuid()}.{oldFileName.Split('.').Last()}";
    }
}
