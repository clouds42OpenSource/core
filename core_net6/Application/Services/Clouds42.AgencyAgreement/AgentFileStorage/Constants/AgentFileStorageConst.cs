﻿namespace Clouds42.AgencyAgreement.AgentFileStorage.Constants
{
    /// <summary>
    /// Константы для агентского файлового хранилища
    /// </summary>
    public static class AgentFileStorageConst
    {
        /// <summary>
        /// Название временной папки
        /// </summary>
        public const string TempFolderName = "TempFiles";
    }
}
