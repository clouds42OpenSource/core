﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Clouds42.AgencyAgreement.AgentRequisites.Resources
{
    using System;


    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class EmailTemplates
    {

        private static global::System.Resources.ResourceManager resourceMan;

        private static global::System.Globalization.CultureInfo resourceCulture;

        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal EmailTemplates()
        {
        }

        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null)) 
                {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Clouds42.AgencyAgreement.AgentRequisites.Resources.EmailTemplates", typeof(EmailTemplates).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }

        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to &lt;body&gt;
        ///    &lt;p&gt;
        ///        Через личный кабинет (&lt;a href=&apos;{3}&apos;&gt;{3}&lt;/a&gt;) была создана заявка на вывод средств.&lt;br&gt;
        ///    &lt;/p&gt;
        ///    &lt;ul&gt;
        ///        {0}
        ///    &lt;/ul&gt;
        ///    &lt;p&gt;Открыть заполненную форму заявки на вывод средств Вы можете по ссылке:&lt;/p&gt;
        ///    {1}
        ///    &lt;p&gt;Ссылки на прикрепленные сканы документов:&lt;/p&gt;
        ///    {2}
        ///&lt;/body&gt;
        ///.
        /// </summary>
        internal static string CreateAgentCashOutRequestNotificationTemplate
        {
            get
            {
                return ResourceManager.GetString("CreateAgentCashOutRequestNotificationTemplate", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to &lt;body&gt;
        ///&lt;p&gt;
        ///    Через личный кабинет (&lt;a href=&apos;{2}&apos;&gt;{2}&lt;/a&gt;) были добавлены реквизиты для выплаты агентского вознаграждения.&lt;br&gt;
        ///&lt;/p&gt;
        ///&lt;ul&gt;
        ///    {0}
        ///&lt;/ul&gt;
        ///&lt;p&gt;Ссылки на прикрепленные сканы документов:&lt;/p&gt;
        ///{1}
        ///
        ///&lt;p&gt;
        ///    Необходимо определить контрагента (при отсутствии создать нового) и добавить договор согласно заполненным реквизитам
        ///&lt;/p&gt;
        ///&lt;p&gt;
        ///    Критерием успешного выполнения задачи является полученный оригинал договора. В результате выполнения задачи необходимо указать номер документа к регистрац [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CreateAgentRequisitesNotitficationTemplate
        {
            get
            {
                return ResourceManager.GetString("CreateAgentRequisitesNotitficationTemplate", resourceCulture);
            }
        }
    }
}
