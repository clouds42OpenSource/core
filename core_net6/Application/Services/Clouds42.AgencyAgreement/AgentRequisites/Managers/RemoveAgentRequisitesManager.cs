﻿using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Managers
{
    /// <summary>
    /// Менеджер удаления реквизитов агента
    /// </summary>
    public class RemoveAgentRequisitesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IRemoveAgentRequisitesProvider removeAgentRequisitesProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Удалить реквизиты агента
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов агента</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult RemoveAgentRequisites(Guid agentRequisitesId)
        {
            var accountId = AccountIdByAgentRequisitesId(agentRequisitesId);
            var agentRequisites = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == agentRequisitesId);
            if (agentRequisites == null)
                return PreconditionFailed($"Реквизиты агента по ID {agentRequisitesId} не найдены");

            var agencyFullName = agentRequisites.GetAgencyFullName(agentRequisites.GetAgentRequisitesType());
            var agentRequisitesRepresentation =
                AgentRequisitesHelper.GetAgentRequisitesRepresentation(agentRequisites.GetAgentRequisitesType());

            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_RemoveRequisites, () => accountId);

                var message = "Реквизиты для выплаты вознаграждения удалены. " +
                              $"Получатель {agencyFullName}, " +
                              $"тип лица {agentRequisitesRepresentation}";

                removeAgentRequisitesProvider.RemoveAgentRequisites(agentRequisitesId);
                logger.Info(message);
                LogEvent(() => accountId, LogActions.RemoveAgentRequisites, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    "[Удаление реквизитов агента завершилось ошибкой.] " +
                    $"Получатель {agencyFullName}, " +
                    $"тип лица {agentRequisitesRepresentation}";

                LogEvent(() => accountId, LogActions.RemoveAgentRequisites, $"{message} Причина: {ex.Message}");
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
