﻿using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Managers
{
    /// <summary>
    /// Менеджер создания реквизитов агента
    /// </summary>
    public class CreateAgentRequisitesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICreateAgentRequisitesProvider createAgentRequisitesProvider,
        IAgentRequisitesDataProvider agentRequisitesDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать реквизиты агента
        /// </summary>
        /// <param name="createAgentRequisites">Модель создания реквизитов агента</param>
        /// <returns>Id созданных реквизитов</returns>
        public ManagerResult<Guid> CreateAgentRequisites(CreateAgentRequisitesDto createAgentRequisites)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_CreateRequisites, () => createAgentRequisites.AccountId);

                var result = createAgentRequisitesProvider.CreateAgentRequisites(createAgentRequisites);

                var agentRequisites = agentRequisitesDataProvider.GetAgentRequisitesOrThrowException(result);

                var message = $"Созданы реквизиты для выплаты вознаграждения. Получатель \"{agentRequisites.GetAgencyFullName(createAgentRequisites.AgentRequisitesType)}\", " +
                              $"тип лица {AgentRequisitesHelper.GetAgentRequisitesRepresentation(createAgentRequisites.AgentRequisitesType)}";
                logger.Info(message);
                LogEvent(() => createAgentRequisites.AccountId, LogActions.CreateAgentRequisites, message);

                return Ok(result);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Создание реквизитов для выплаты вознаграждения для получателя \"{createAgentRequisites.ShortName}\" завершилось ошибкой.]";
                LogEvent(() => createAgentRequisites.AccountId, LogActions.CreateAgentRequisites,
                    $"{message} Причина: {ex.Message}");
                _handlerException.Handle(ex, message);
                return PreconditionFailed<Guid>(ex.Message);
            }
        }
    }
}
