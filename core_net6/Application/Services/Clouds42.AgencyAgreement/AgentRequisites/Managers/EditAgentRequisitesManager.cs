﻿using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Managers
{
    /// <summary>
    /// Менеджер редактирования реквизитов агента
    /// </summary>
    public class EditAgentRequisitesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IEditAgentRequisitesProvider editAgentRequisitesProvider,
        IAgentRequisitesDataProvider agentRequisitesDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Отредактировать реквизиты агента
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult EditAgentRequisites(EditAgentRequisitesDto editAgentRequisites)
        {
            if (!TryGetAgentRequisites(editAgentRequisites.Id, out var oldAgentRequisites))
                return PreconditionFailed($"Не удалось получить реквизиты агента по ID {editAgentRequisites.Id}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_EditRequisites, () => editAgentRequisites.AccountId);

                var message = $"Реквизиты отредактированы. Смена статуса реквизитов с \"{oldAgentRequisites.AgentRequisitesStatus.Description()}\" " +
                              $"на \"{editAgentRequisites.AgentRequisitesStatus.Description()}\"";

                editAgentRequisitesProvider.EditAgentRequisites(editAgentRequisites);

                LogEvent(() => editAgentRequisites.AccountId, LogActions.EditAgentRequisites, message);
                logger.Info(message);

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка при редактировании реквизитов {oldAgentRequisites.GetAgencyFullName(oldAgentRequisites.GetAgentRequisitesType())}.]";
                LogEvent(() => editAgentRequisites.AccountId, LogActions.EditAgentRequisites, $"{message} Причина: {ex.Message}");
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Сменить статус реквизитов агента
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult ChangeAgentRequisitesStatus(EditAgentRequisitesDto editAgentRequisites)
        {
            if (!TryGetAgentRequisites(editAgentRequisites.Id, out var oldAgentRequisites))
                return PreconditionFailed($"Не удалось получить реквизиты агента по ID {editAgentRequisites.Id}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_EditRequisitesStatus, () => editAgentRequisites.AccountId);

                var message = $"Реквизиты отредактированы. Смена статуса реквизитов с \"{oldAgentRequisites.AgentRequisitesStatus.Description()}\" " +
                              $"на \"{editAgentRequisites.AgentRequisitesStatus.Description()}\"";

                editAgentRequisitesProvider.ChangeAgentRequisitesStatus(editAgentRequisites);

                LogEvent(() => editAgentRequisites.AccountId, LogActions.EditAgentRequisites, message);
                logger.Info(message);

                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Ошибка при редактировании реквизитов агента {oldAgentRequisites.GetAgencyFullName(oldAgentRequisites.GetAgentRequisitesType())}.]";
                LogEvent(() => editAgentRequisites.AccountId, LogActions.EditAgentRequisites, $"{message} Причина: {ex.Message}");
                _handlerException.Handle(ex, message);
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Попытаться получить реквизиты агента
        /// </summary>
        /// <param name="agentRequisitesId">ID реквизитов</param>
        /// <param name="agentRequisites">Реквизиты агента</param>
        /// <returns>Результат попытки</returns>
        private bool TryGetAgentRequisites(Guid agentRequisitesId, out Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites)
        {
            try
            {
                agentRequisites = agentRequisitesDataProvider.GetAgentRequisitesOrThrowException(agentRequisitesId);
                return true;
            }
            catch (Exception)
            {
                agentRequisites = null;
                return false;
            }
        }
    }
}
