﻿using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Managers
{
    /// <summary>
    /// Менеджер для работы с реквизитами агента 
    /// </summary>
    public class AgentRequisitesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAgentRequisitesDataProvider agentRequisitesDataProvider,
        IHandlerException handlerException,
        IAgentRequisitesProvider agentRequisitesProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить реквизиты агента
        /// </summary>
        /// <param name="filter">Фильтр</param>
        /// <returns>Реквизиты агента</returns>
        public ManagerResult<PaginationDataResultDto<AgentRequisitesInfoDto>> GetAgentRequisites(PartnersAgentRequisitesFilterDto filter)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Agency_GetRequisites, () => filter.AccountId);
                logger.Info($"Получение реквизитов агента {filter.AccountId} завершилось успешно");
                var data = agentRequisitesDataProvider.GetAgentRequisites(filter);

                return Ok(data);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Получение реквизитов агента {filter.AccountId} завершилось ошибкой.]";
                _handlerException.Handle(ex, message);

                return PreconditionFailed<PaginationDataResultDto<AgentRequisitesInfoDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель реквизитов агента для редактирования
        /// </summary>
        /// <param name="agentRequisitesId">Id агентского договора</param>
        /// <returns>Модель реквизитов агента для редактирования</returns>
        public ManagerResult<EditAgentRequisitesDto> GetAgentRequisitesById(Guid agentRequisitesId)
        {
            var accountId = AccountIdByAgentRequisitesId(agentRequisitesId);
            try
            {
                logger.Info($"Получение реквизитов агента {agentRequisitesId}");
                AccessProvider.HasAccess(ObjectAction.Agency_GetRequisites, () => accountId);
                var data = agentRequisitesProvider.GetEditAgentRequisitesDto(agentRequisitesId);

                return Ok(data);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Получение реквизитов агента {agentRequisitesId} завершилось ошибкой.]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<EditAgentRequisitesDto>(ex.Message);
            }
        }
    }
}
