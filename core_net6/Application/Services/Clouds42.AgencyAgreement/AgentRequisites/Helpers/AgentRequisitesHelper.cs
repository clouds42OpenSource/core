﻿using Clouds42.Domain.Enums;

namespace Clouds42.AgencyAgreement.AgentRequisites.Helpers
{
    /// <summary>
    /// Хелпер для работы с агентскими договорами
    /// </summary>
    public static class AgentRequisitesHelper
    {
        /// <summary>
        /// Карта сопоставления типа агентских реквизитов к ФИО агента
        /// </summary>
        private static readonly IDictionary<AgentRequisitesTypeEnum, Func<Domain.DataModels.billing.AgentRequisites.AgentRequisites, string>>
            FioDictionary = new Dictionary<AgentRequisitesTypeEnum, Func<Domain.DataModels.billing.AgentRequisites.AgentRequisites, string>>
            {
                {AgentRequisitesTypeEnum.LegalPersonRequisites, model => model?.LegalPersonRequisites?.HeadFullName ?? string.Empty},
                {AgentRequisitesTypeEnum.PhysicalPersonRequisites, model => model?.PhysicalPersonRequisites?.FullName ?? string.Empty},
                {AgentRequisitesTypeEnum.SoleProprietorRequisites, model => model?.SoleProprietorRequisites?.FullName ?? string.Empty},
            };

        /// <summary>
        /// Карта сопоставления типа агентских реквизитов к префиксу перед ФИО агента
        /// </summary>
        private static readonly IDictionary<AgentRequisitesTypeEnum, string>
            PrefixDictionary = new Dictionary<AgentRequisitesTypeEnum, string>
            {
                {AgentRequisitesTypeEnum.LegalPersonRequisites, string.Empty},
                {AgentRequisitesTypeEnum.PhysicalPersonRequisites, string.Empty},
                {AgentRequisitesTypeEnum.SoleProprietorRequisites, "ИП"},
            };

        /// <summary>
        /// Карта сопоставления типа агентских реквизитов к строковому представлению
        /// </summary>
        private static readonly IDictionary<AgentRequisitesTypeEnum, string>
            AgentRequisitesRepresentation = new Dictionary<AgentRequisitesTypeEnum, string>
            {
                {AgentRequisitesTypeEnum.LegalPersonRequisites, "\"юр. лицо\""},
                {AgentRequisitesTypeEnum.PhysicalPersonRequisites, "\"физ. лицо\""},
                {AgentRequisitesTypeEnum.SoleProprietorRequisites, "\"ИП\""},
            };


        /// <summary>
        /// Получить ФИО агента
        /// </summary>
        /// <param name="agentRequisites">Агентские реквизиты</param>
        /// <param name="agentRequisitesType">Тип агентских реквизитов</param>
        /// <returns>ФИО агента</returns>
        public static string GetAgencyFullName(this Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites, AgentRequisitesTypeEnum agentRequisitesType) =>
            FioDictionary[agentRequisitesType](agentRequisites);

        /// <summary>
        /// Получить представление типа агентских реквизитов
        /// </summary>
        /// <param name="agentRequisitesType">Тип агентских реквизитов</param>
        /// <returns>Представление типа агентских реквизитов</returns>
        public static string GetAgentRequisitesRepresentation(AgentRequisitesTypeEnum agentRequisitesType) =>
            AgentRequisitesRepresentation[agentRequisitesType];

        /// <summary>
        /// Получить тип агентских реквизитов
        /// </summary>
        /// <param name="agentRequisites">Агентские реквизиты</param>
        /// <returns>Тип агентских реквизитов</returns>
        public static AgentRequisitesTypeEnum GetAgentRequisitesType(this Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites)
        {
            if (agentRequisites.LegalPersonRequisites != null)
                return AgentRequisitesTypeEnum.LegalPersonRequisites;

            if (agentRequisites.PhysicalPersonRequisites != null)
                return AgentRequisitesTypeEnum.PhysicalPersonRequisites;

            return AgentRequisitesTypeEnum.SoleProprietorRequisites;
        }

        /// <summary>
        /// Получить префикс для ФИО в отчете агента
        /// </summary>
        /// <returns>Префикс для ФИО</returns>
        public static string GetPrefixForFullNameInAgentReport(this Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites)
        {
            var agentRequisitesType = agentRequisites.GetAgentRequisitesType();
            return PrefixDictionary[agentRequisitesType];
        }
    }
}
