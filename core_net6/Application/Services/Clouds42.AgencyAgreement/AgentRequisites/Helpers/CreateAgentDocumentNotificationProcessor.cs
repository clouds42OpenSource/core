﻿using System.Text;
using Clouds42.AgencyAgreement.AgentRequisites.Resources;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Mails.Promo;
using Clouds42.Logger;

namespace Clouds42.AgencyAgreement.AgentRequisites.Helpers
{
    /// <summary>
    /// Процессор для уведомления support о создании реквизитов/заявки
    /// </summary>
    public class CreateAgentDocumentNotificationProcessor(ILogger42 logger)
    {
        /// <summary>
        /// Создать письмо и уведомить саппорт
        /// </summary>
        /// <param name="agentRequisites">Реквизиты агента</param>
        /// <param name="agentRequisitesType">Тип агентских реквизитов</param>
        public void CreateEmailAndNotifySupport(
            Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites,
            AgentRequisitesTypeEnum agentRequisitesType)
        {
            try
            {
                logger.Info($"Начало отправки письма менеджерам по реквизитам {agentRequisites.Number}");
                if (agentRequisites == null)
                    throw new ArgumentNullException(nameof(agentRequisites));

                var filesLinks =
                    GetLinksForAgentDocumentFiles(agentRequisites.AgentRequisitesFiles.Select(w => w.CloudFile)
                        .ToList());

                var baseRequisitesInfo = GetBaseRequisitesInfo(agentRequisites, agentRequisitesType);
                var messageBody = GenerateMessageBodyForAgentRequisites(baseRequisitesInfo, filesLinks);
                var messageSubject = "Создание реквизитов для выплаты";

                logger.Info($"Составили тело {messageBody} и тему для письма {messageSubject}");
                SendEmail(messageSubject, messageBody);
                logger.Info($"Письмо отправлено");
            }
            catch (Exception ex)
            {
                logger.Warn($"При отправке сообщения возникла ошибка. Причина : {ex.GetFullInfo()}");
            }
        }

        /// <summary>
        /// Получить базовую информацию о реквизитах
        /// </summary>
        /// <param name="agentRequisites">Реквизиты агента</param>
        /// <param name="agentRequisitesType">Тип агентских реквизитов</param>
        /// <returns>Базовая информация о реквизитах</returns>
        private string GetBaseRequisitesInfo(Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites,
            AgentRequisitesTypeEnum agentRequisitesType)
            => $@"<li>Аккаунт: {agentRequisites.AccountOwner.IndexNumber}</li>
                <li>Получатель: {agentRequisites.GetAgencyFullName(agentRequisitesType)}</li>
                <li>Дата создания: {agentRequisites.CreationDate: dd.MM.yyyy HH:mm:ss}</li>";

        /// <summary>
        /// Получить ссылки на файлы агентского документа
        /// </summary>
        /// <param name="cloudFiles">Список файлов</param>
        /// <returns>Список ссылок</returns>
        private string GetLinksForAgentDocumentFiles(List<CloudFile> cloudFiles)
        {
            if (!cloudFiles.Any())
                return string.Empty;

            var links = new List<string>();
            var siteUrl = CloudConfigurationProvider.Cp.GetSiteCoreUrl();

            cloudFiles.ForEach(w =>
            {
                var fileUrl = $"{siteUrl}/partners/agent-document-file?agentDocumentFileId={w.Id}";
                links.Add($@"<li>{fileUrl}</li>");
            });

            var linksListTagBuilder = new StringBuilder();
            linksListTagBuilder.AppendLine("<p>Прикрепленные сканы документов можно посмотреть по ссылке:</p>");
            linksListTagBuilder.AppendLine("<ul>");
            foreach (var link in links)
            {
                linksListTagBuilder.AppendLine(link);
            }

            linksListTagBuilder.AppendLine("</ul>");
            return linksListTagBuilder.ToString();
        }

        /// <summary>
        /// Отправить письмо
        /// </summary>
        /// <param name="messageSubject">Тема письма</param>
        /// <param name="messageBody">Содержание письма</param>
        private void SendEmail(string messageSubject, string messageBody)
        {
            var supportEmail = CloudConfigurationProvider.Emails.Get42CloudsManagerEmail();
            new SupportPromoMail()
                .DisplayName("Команда 42Clouds")
                .To(supportEmail)
                .Subject(messageSubject)
                .Body(messageBody)
                .SendViaNewThread();
        }

        /// <summary>
        /// Сгенерировать тело письма для реквизитов агента
        /// </summary>
        /// <param name="baseContractInfo">Базовая информация о реквизитах агента</param>
        /// <param name="filesLinksListTag">Ссылки на файлы</param>
        /// <returns>Тело письма</returns>
        private string GenerateMessageBodyForAgentRequisites(string baseContractInfo, string filesLinksListTag)
        {
            var siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
            return EmailTemplates.CreateAgentRequisitesNotitficationTemplate.Replace("{0}", baseContractInfo)
                .Replace("{1}", filesLinksListTag)
                .Replace("{2}", siteUrl);
        }
    }
}
