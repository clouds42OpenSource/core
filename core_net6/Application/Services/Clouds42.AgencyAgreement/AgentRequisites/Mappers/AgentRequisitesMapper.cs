﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.DataModels.billing.AgentRequisites;

namespace Clouds42.AgencyAgreement.AgentRequisites.Mappers
{
    /// <summary>
    /// Маппер моделей агентского договора
    /// </summary>
    public static class AgentRequisitesMapper
    {
        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="createAgentRequisites">Модель создания реквизитов агента</param>
        /// <returns>Реквизиты юридического лица</returns>
        public static LegalPersonRequisites MapToLegalPersonRequisites(this CreateAgentRequisitesDto createAgentRequisites)
        {
            return new LegalPersonRequisites
            {
                Ogrn = createAgentRequisites.LegalPersonRequisites.Ogrn,
                Inn = createAgentRequisites.LegalPersonRequisites.Inn,
                Kpp = createAgentRequisites.LegalPersonRequisites.Kpp,
                OrganizationName = createAgentRequisites.LegalPersonRequisites.OrganizationName,
                HeadFullName = createAgentRequisites.LegalPersonRequisites.HeadFullName,
                HeadPosition = createAgentRequisites.LegalPersonRequisites.HeadPosition,
                LegalAddress = createAgentRequisites.LegalPersonRequisites.LegalAddress,
                PhoneNumber = createAgentRequisites.LegalPersonRequisites.PhoneNumber,
                SettlementAccount = createAgentRequisites.LegalPersonRequisites.SettlementAccount,
                Bik = createAgentRequisites.LegalPersonRequisites.Bik,
                BankName = createAgentRequisites.LegalPersonRequisites.BankName,
                CorrespondentAccount = createAgentRequisites.LegalPersonRequisites.CorrespondentAccount,
                AddressForSendingDocuments = createAgentRequisites.LegalPersonRequisites.AddressForSendingDocuments
            };
        }

        public static void MapToLegalPersonRequisites(this LegalPersonRequisites exists,
            CreateAgentRequisitesDto createAgentRequisites)
        {

            exists.Ogrn = createAgentRequisites.LegalPersonRequisites.Ogrn;
            exists.Inn = createAgentRequisites.LegalPersonRequisites.Inn;
            exists.Kpp = createAgentRequisites.LegalPersonRequisites.Kpp;
            exists.OrganizationName = createAgentRequisites.LegalPersonRequisites.OrganizationName;
            exists.HeadFullName = createAgentRequisites.LegalPersonRequisites.HeadFullName;
            exists.HeadPosition = createAgentRequisites.LegalPersonRequisites.HeadPosition;
            exists.LegalAddress = createAgentRequisites.LegalPersonRequisites.LegalAddress;
            exists.PhoneNumber = createAgentRequisites.LegalPersonRequisites.PhoneNumber;
            exists.SettlementAccount = createAgentRequisites.LegalPersonRequisites.SettlementAccount;
            exists.Bik = createAgentRequisites.LegalPersonRequisites.Bik;
            exists.BankName = createAgentRequisites.LegalPersonRequisites.BankName;
            exists.CorrespondentAccount = createAgentRequisites.LegalPersonRequisites.CorrespondentAccount;
            exists.AddressForSendingDocuments = createAgentRequisites.LegalPersonRequisites.AddressForSendingDocuments;
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="legalPersonRequisites">Реквизиты юр лица</param>
        /// <param name="agentRequisitesFiles">Файлы реквизитов агента</param>
        /// <returns>Модель реквизитов юридического лица</returns>
        public static LegalPersonRequisitesDto MapToLegalPersonRequisitesDto(this LegalPersonRequisites legalPersonRequisites,
            List<CloudFileDataDto<byte[]>> agentRequisitesFiles)
        {
            return new LegalPersonRequisitesDto
            {
                Ogrn = legalPersonRequisites.Ogrn,
                Inn = legalPersonRequisites.Inn,
                Kpp = legalPersonRequisites.Kpp,
                OrganizationName = legalPersonRequisites.OrganizationName?.Replace('"', '\''),
                HeadFullName = legalPersonRequisites.HeadFullName,
                HeadPosition = legalPersonRequisites.HeadPosition,
                LegalAddress = legalPersonRequisites.LegalAddress,
                PhoneNumber = legalPersonRequisites.PhoneNumber,
                SettlementAccount = legalPersonRequisites.SettlementAccount,
                Bik = legalPersonRequisites.Bik,
                BankName = legalPersonRequisites.BankName,
                CorrespondentAccount = legalPersonRequisites.CorrespondentAccount,
                AddressForSendingDocuments = legalPersonRequisites.AddressForSendingDocuments,
                Files = agentRequisitesFiles
            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="createAgentRequisites">Модель создания реквизитов агента</param>
        /// <returns>Реквизиты физ лица</returns>
        public static PhysicalPersonRequisites MapToPhysicalPersonRequisites(this CreateAgentRequisitesDto createAgentRequisites)
        {
            return new PhysicalPersonRequisites
            {
                SettlementAccount = createAgentRequisites.PhysicalPersonRequisites.SettlementAccount,
                Bik = createAgentRequisites.PhysicalPersonRequisites.Bik,
                BankName = createAgentRequisites.PhysicalPersonRequisites.BankName,
                CorrespondentAccount = createAgentRequisites.PhysicalPersonRequisites.CorrespondentAccount,
                AddressForSendingDocuments = createAgentRequisites.PhysicalPersonRequisites.AddressForSendingDocuments,
                DateOfBirth = createAgentRequisites.PhysicalPersonRequisites.DateOfBirth,
                FullName = createAgentRequisites.PhysicalPersonRequisites.FullName,
                Inn = createAgentRequisites.PhysicalPersonRequisites.Inn,
                WhomIssuedPassport = createAgentRequisites.PhysicalPersonRequisites.WhomIssuedPassport,
                PassportDateOfIssue = createAgentRequisites.PhysicalPersonRequisites.PassportDateOfIssue,
                PassportNumber = createAgentRequisites.PhysicalPersonRequisites.PassportNumber,
                PassportSeries = createAgentRequisites.PhysicalPersonRequisites.PassportSeries,
                RegistrationAddress = createAgentRequisites.PhysicalPersonRequisites.RegistrationAddress,
                Snils = createAgentRequisites.PhysicalPersonRequisites.Snils
            };
        }

        public static void MapToPhysicalPersonRequisites(this PhysicalPersonRequisites exists, CreateAgentRequisitesDto createAgentRequisites)
        {
            exists.SettlementAccount = createAgentRequisites.PhysicalPersonRequisites.SettlementAccount;
            exists.Bik = createAgentRequisites.PhysicalPersonRequisites.Bik;
            exists.BankName = createAgentRequisites.PhysicalPersonRequisites.BankName;
            exists.CorrespondentAccount = createAgentRequisites.PhysicalPersonRequisites.CorrespondentAccount;
            exists.AddressForSendingDocuments = createAgentRequisites.PhysicalPersonRequisites.AddressForSendingDocuments;
            exists.DateOfBirth = createAgentRequisites.PhysicalPersonRequisites.DateOfBirth;
            exists.FullName = createAgentRequisites.PhysicalPersonRequisites.FullName;
            exists.Inn = createAgentRequisites.PhysicalPersonRequisites.Inn;
            exists.WhomIssuedPassport = createAgentRequisites.PhysicalPersonRequisites.WhomIssuedPassport;
            exists.PassportDateOfIssue = createAgentRequisites.PhysicalPersonRequisites.PassportDateOfIssue;
            exists.PassportNumber = createAgentRequisites.PhysicalPersonRequisites.PassportNumber;
            exists.PassportSeries = createAgentRequisites.PhysicalPersonRequisites.PassportSeries;
            exists.RegistrationAddress = createAgentRequisites.PhysicalPersonRequisites.RegistrationAddress;
            exists.Snils = createAgentRequisites.PhysicalPersonRequisites.Snils;
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="physicalPersonRequisites">Реквизиты физ лица</param>
        /// <param name="agentRequisitesFiles">Файлы реквизитов агента</param>
        /// <returns>Модель реквизитов физического лица</returns>
        public static PhysicalPersonRequisitesDto MapToPhysicalPersonRequisitesDto(
            this PhysicalPersonRequisites physicalPersonRequisites,
            List<CloudFileDataDto<byte[]>> agentRequisitesFiles)
        {
            return new PhysicalPersonRequisitesDto
            {
                SettlementAccount = physicalPersonRequisites.SettlementAccount,
                Bik = physicalPersonRequisites.Bik,
                BankName = physicalPersonRequisites.BankName,
                CorrespondentAccount = physicalPersonRequisites.CorrespondentAccount,
                AddressForSendingDocuments = physicalPersonRequisites.AddressForSendingDocuments,
                DateOfBirth = physicalPersonRequisites.DateOfBirth.GetValueOrDefault(),
                FullName = physicalPersonRequisites.FullName,
                Inn = physicalPersonRequisites.Inn,
                WhomIssuedPassport = physicalPersonRequisites.WhomIssuedPassport,
                PassportDateOfIssue = physicalPersonRequisites.PassportDateOfIssue.GetValueOrDefault(),
                PassportNumber = physicalPersonRequisites.PassportNumber,
                PassportSeries = physicalPersonRequisites.PassportSeries,
                RegistrationAddress = physicalPersonRequisites.RegistrationAddress,
                Snils = physicalPersonRequisites.Snils,
                Files = agentRequisitesFiles
            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="createAgentRequisitesDto">Модель создания реквизитов</param>
        /// <returns>Реквизиты физ лица</returns>
        public static SoleProprietorRequisites MapToSoleProprietorRequisites(this CreateAgentRequisitesDto createAgentRequisitesDto)
        {
            return new SoleProprietorRequisites
            {
                SettlementAccount = createAgentRequisitesDto.SoleProprietorRequisites.SettlementAccount,
                Bik = createAgentRequisitesDto.SoleProprietorRequisites.Bik,
                BankName = createAgentRequisitesDto.SoleProprietorRequisites.BankName,
                CorrespondentAccount = createAgentRequisitesDto.SoleProprietorRequisites.CorrespondentAccount,
                AddressForSendingDocuments = createAgentRequisitesDto.SoleProprietorRequisites.AddressForSendingDocuments,
                Inn = createAgentRequisitesDto.SoleProprietorRequisites.Inn,
                Ogrn = createAgentRequisitesDto.SoleProprietorRequisites.Ogrn,
                FullName = createAgentRequisitesDto.SoleProprietorRequisites.FullName,
                LegalAddress = createAgentRequisitesDto.SoleProprietorRequisites.LegalAddress,
                PhoneNumber = createAgentRequisitesDto.SoleProprietorRequisites.PhoneNumber
            };
        }

        public static void MapToSoleProprietorRequisites(this SoleProprietorRequisites exists, CreateAgentRequisitesDto createAgentRequisitesDto)
        {
            exists.SettlementAccount = createAgentRequisitesDto.SoleProprietorRequisites.SettlementAccount;
            exists.Bik = createAgentRequisitesDto.SoleProprietorRequisites.Bik;
            exists.BankName = createAgentRequisitesDto.SoleProprietorRequisites.BankName;
            exists.CorrespondentAccount = createAgentRequisitesDto.SoleProprietorRequisites.CorrespondentAccount;
            exists.AddressForSendingDocuments = createAgentRequisitesDto.SoleProprietorRequisites.AddressForSendingDocuments;
            exists.Inn = createAgentRequisitesDto.SoleProprietorRequisites.Inn;
            exists.Ogrn = createAgentRequisitesDto.SoleProprietorRequisites.Ogrn;
            exists.FullName = createAgentRequisitesDto.SoleProprietorRequisites.FullName;
            exists.LegalAddress = createAgentRequisitesDto.SoleProprietorRequisites.LegalAddress;
            exists.PhoneNumber = createAgentRequisitesDto.SoleProprietorRequisites.PhoneNumber;
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="soleProprietorRequisites">Реквизиты ИП</param>
        /// <param name="agentRequisitesFiles">Файлы реквизитов агента</param>
        /// <returns>Модель реквизитов ИП</returns>
        public static SoleProprietorRequisitesDto MapToSoleProprietorRequisitesDto(
            this SoleProprietorRequisites soleProprietorRequisites,
            List<CloudFileDataDto<byte[]>> agentRequisitesFiles)
        {
            return new SoleProprietorRequisitesDto
            {
                SettlementAccount = soleProprietorRequisites.SettlementAccount,
                Bik = soleProprietorRequisites.Bik,
                BankName = soleProprietorRequisites.BankName,
                CorrespondentAccount = soleProprietorRequisites.CorrespondentAccount,
                AddressForSendingDocuments = soleProprietorRequisites.AddressForSendingDocuments,
                Inn = soleProprietorRequisites.Inn,
                Ogrn = soleProprietorRequisites.Ogrn,
                FullName = soleProprietorRequisites.FullName,
                LegalAddress = soleProprietorRequisites.LegalAddress,
                PhoneNumber = soleProprietorRequisites.PhoneNumber,
                Files = agentRequisitesFiles
            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="agentRequisitesFile">Модель файла реквизитов агента</param>
        /// <returns>Объект типа FileDataDto</returns>
        public static CloudFileDataDto<byte[]> MapToFileDataDto(this AgentRequisitesFile agentRequisitesFile)
        {
            return new CloudFileDataDto<byte[]>
            {
                CloudFileId = agentRequisitesFile.CloudFileId,
                FileName = agentRequisitesFile.CloudFile.FileName,
                Content = agentRequisitesFile.CloudFile.Content,
                AgentRequisitesFileType = agentRequisitesFile.AgentRequisitesFileType,
                ContentType = agentRequisitesFile.CloudFile.ContentType,
            };
        }
    }
}
