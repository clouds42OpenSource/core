﻿using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.DataContracts.BaseModel;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Providers
{
    public class PartnersAgentRequisitesQuery: ISortedQuery
    {
        public PartnersAgentRequisitesFilter Filter { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public string OrderBy { get; set; }
    }

    public class PartnersAgentRequisitesFilter : IQueryFilter, IHasSpecificSearch
    {
        public Guid? PartnerAccountId { get; set; }
        public AgentRequisitesStatusEnum? AgentRequisitesStatus { get; set; }
        public AgentRequisitesTypeEnum? AgentRequisitesType { get; set; }
        public string? Number { get; set; }
        public string? SearchLine { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }

    public static class PartnersAgentRequisitesSpecification
    {
        public static Spec<AgentRequisitesInfoDto> BySearchLine(string? value)
        {
            var lowerValue = value?.ToLower();

            return new Spec<AgentRequisitesInfoDto>(x => string.IsNullOrEmpty(lowerValue) || x.PartnerAccountCaption.ToLower().Contains(lowerValue) ||
                                                x.PartnerAccountIndexNumber.ToString().ToLower().Contains(lowerValue));
        }

        public static Spec<AgentRequisitesInfoDto> ByDate(DateTime? from, DateTime? to)
        {
            return new Spec<AgentRequisitesInfoDto>(x => (!from.HasValue || from.Value >= x.CreationDate) && (!to.HasValue || to.Value <= x.CreationDate));
        }
    }

    /// <summary>
    /// Провайдер данных реквизитов агента
    /// </summary>
    internal class AgentRequisitesDataProvider(IUnitOfWork dbLayer) : IAgentRequisitesDataProvider
    {
        /// <summary>
        /// Получить реквизиты агента
        /// </summary>
        /// <param name="filter">Значение фильтра</param>
        /// <returns>Реквизиты агента</returns>
        public PaginationDataResultDto<AgentRequisitesInfoDto> GetAgentRequisites(
            PartnersAgentRequisitesFilterDto filter)
        {
            var newFilter = new PartnersAgentRequisitesQuery
            {
                Filter = new PartnersAgentRequisitesFilter
                {
                    AgentRequisitesStatus = filter.AgentRequisitesStatus,
                    AgentRequisitesType = filter.AgentRequisitesType,
                    From = filter.PeriodFrom,
                    To = filter.PeriodTo,
                    SearchLine = filter.PartnerAccountData,
                    Number = filter.Number,
                    PartnerAccountId = filter.AccountId
                },
                OrderBy = filter.SortType.HasValue && !string.IsNullOrEmpty(filter.SortFieldName)
                    ? $"{filter.SortFieldName}.{(filter.SortType == SortType.Asc ? "asc" : "desc")}"
                    : $"{nameof(AgentRequisitesInfoDto.CreationDate)}.desc",
                PageNumber = filter.PageNumber,
                PageSize = filter.PageSize
            };

            var result =  dbLayer.AgentRequisitesRepository
                .AsQueryableNoTracking()
                .Select(x => new AgentRequisitesInfoDto
                {
                    Id = x.Id,
                    AccountOwnerIndexNumber = x.AccountOwner.IndexNumber,
                    RequisitesNumber = x.Number,
                    CreationDate = x.CreationDate,
                    AgentRequisitesType =
                        x.PhysicalPersonRequisitesId.HasValue ? AgentRequisitesTypeEnum.PhysicalPersonRequisites :
                        x.LegalPersonRequisitesId.HasValue ? AgentRequisitesTypeEnum.LegalPersonRequisites :
                        AgentRequisitesTypeEnum.SoleProprietorRequisites,
                    PartnerAccountId = x.AccountOwnerId,
                    PartnerAccountCaption = x.PhysicalPersonRequisitesId.HasValue
                        ? x.PhysicalPersonRequisites.FullName
                        : x.LegalPersonRequisitesId.HasValue
                            ? x.LegalPersonRequisites.OrganizationName
                            : x.SoleProprietorRequisites.FullName,
                    PartnerAccountIndexNumber = x.AccountOwner.IndexNumber,
                    StatusDateTime = x.StatusDateTime,
                    AgentRequisitesStatus = x.AgentRequisitesStatus
                })
                .Where(PartnersAgentRequisitesSpecification.BySearchLine(newFilter.Filter.SearchLine) & 
                       PartnersAgentRequisitesSpecification.ByDate(newFilter.Filter.From, newFilter.Filter.To))
                .AutoFilter(newFilter.Filter)
                .AutoSort(newFilter)
                .ToPagedList(newFilter.PageNumber ?? 1, newFilter.PageSize ?? 50)
                .ToPagedDto();

            return new PaginationDataResultDto<AgentRequisitesInfoDto>(result.Records.ToList(), result.Metadata.TotalItemCount,
                result.Metadata.PageSize, result.Metadata.PageNumber);
        }

        /// <summary>
        /// Получить реквизиты агента или выкинуть исключение
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов агента</param>
        /// <returns>Реквизиты агента</returns>
        public Domain.DataModels.billing.AgentRequisites.AgentRequisites GetAgentRequisitesOrThrowException(Guid agentRequisitesId)
            => dbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == agentRequisitesId)
               ?? throw new NotFoundException($"Реквизиты по ID {agentRequisitesId} не найдены");
    }
}
