﻿using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.Validators.Partner.Validators;
using Clouds42.Logger;
using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.AgentRequisites.Mappers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Providers
{
    /// <summary>
    /// Провайдер создания реквизитов агента
    /// </summary>
    internal class CreateAgentRequisitesProvider : BaseAgentRequisitesProvider, ICreateAgentRequisitesProvider
    {
        private readonly ILogger42 _logger;
        private readonly CreateAgentDocumentNotificationProcessor _createAgentDocumentNotificationProcessor;

        /// <summary>
        /// Карта маппинга типа договора к методу создания реквизитов
        /// </summary>
        private readonly IDictionary<AgentRequisitesTypeEnum, Func<CreateAgentRequisitesDto, Guid>> _mapAgentRequisitesTypeToRequisitesCreationMethod;
        private readonly IHandlerException _handlerException;
        public CreateAgentRequisitesProvider(IUnitOfWork dbLayer,
            CreateAgentDocumentNotificationProcessor createAgentDocumentNotificationProcessor,
            ILogger42 logger, IHandlerException handlerException) : base(dbLayer)
        {
            _createAgentDocumentNotificationProcessor = createAgentDocumentNotificationProcessor;
            _logger = logger;
            _handlerException = handlerException;

            _mapAgentRequisitesTypeToRequisitesCreationMethod = new Dictionary<AgentRequisitesTypeEnum, Func<CreateAgentRequisitesDto, Guid>>
            {
                {AgentRequisitesTypeEnum.PhysicalPersonRequisites, CreatePhysicalPersonRequisites},
                {AgentRequisitesTypeEnum.LegalPersonRequisites, CreateLegalPersonRequisites},
                {AgentRequisitesTypeEnum.SoleProprietorRequisites, CreateSoleProprietorRequisites},
            };
        }

        /// <summary>
        /// Создать реквизиты агента
        /// </summary>
        /// <param name="createAgentRequisites">Модель создания реквизитов агента</param>
        /// <returns>Id созданных реквизитов</returns>
        public Guid CreateAgentRequisites(CreateAgentRequisitesDto createAgentRequisites)
        {
            _logger.Info($"Создание договора для аккаунта{createAgentRequisites.AccountId}");
            var validateResult = createAgentRequisites.Validate();
            if (!validateResult.Success)
                throw new ValidateException(validateResult.Message);

            var agentRequisitesId =
                _mapAgentRequisitesTypeToRequisitesCreationMethod[createAgentRequisites.AgentRequisitesType](
                    createAgentRequisites);

            var agentRequisites = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == agentRequisitesId) ??
                                 throw new NotFoundException($"Не удалость получить договор по Id {agentRequisitesId}");

            if (agentRequisites.AgentRequisitesStatus != AgentRequisitesStatusEnum.Draft)
                _createAgentDocumentNotificationProcessor.CreateEmailAndNotifySupport(agentRequisites, createAgentRequisites.AgentRequisitesType);

            return agentRequisitesId;
        }

        /// <summary>
        /// Создать реквизиты физического лица
        /// </summary>
        /// <param name="createAgentRequisites">Модель создания реквизитов агента</param>
        /// <returns>Id созданных реквизитов</returns>
        private Guid CreatePhysicalPersonRequisites(CreateAgentRequisitesDto createAgentRequisites)
        {
            _logger.Info("Создание реквизитов для физического лица");
            if (createAgentRequisites.PhysicalPersonRequisites == null)
                throw new NotFoundException("Отсутсвуют реквизиты для физ. лица");

            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                var agentRequisitesId = Guid.NewGuid();
                var agentRequisites = GenerateAgentRequisites(agentRequisitesId, createAgentRequisites.AccountId,
                    createAgentRequisites.AgentRequisitesStatus, physicalPersonRequisitesId: agentRequisitesId);

                var physicalPersonContract = createAgentRequisites.MapToPhysicalPersonRequisites();
                physicalPersonContract.AgentRequisitesId = agentRequisitesId;
                DbLayer.AgentRequisitesRepository.InsertAgentRequisites(agentRequisites);
                DbLayer.PhysicalPersonRequisitesRepository.Insert(physicalPersonContract);

                CreateAgentRequisitesFiles(agentRequisitesId, createAgentRequisites.PhysicalPersonRequisites.Files);

                DbLayer.Save();
                transaction.Commit();

                return agentRequisitesId;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка создания реквизитов]");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать реквизиты ИП
        /// </summary>
        /// <param name="createAgentRequisites">Модель создания реквизитов агента</param>
        /// <returns>Id созданных реквизитов</returns>
        private Guid CreateSoleProprietorRequisites(CreateAgentRequisitesDto createAgentRequisites)
        {
            _logger.Info("Создание реквизитов для физического лица");
            if (createAgentRequisites.SoleProprietorRequisites == null)
                throw new NotFoundException("Отсутсвуют реквизиты для ИП");

            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                var agentRequisitesId = Guid.NewGuid();
                var agentRequisites = GenerateAgentRequisites(agentRequisitesId, createAgentRequisites.AccountId,
                    createAgentRequisites.AgentRequisitesStatus, soleProprietorRequisitesId: agentRequisitesId);

                var soleProprietorContract = createAgentRequisites.MapToSoleProprietorRequisites();
                soleProprietorContract.AgentRequisitesId = agentRequisitesId;
                DbLayer.AgentRequisitesRepository.InsertAgentRequisites(agentRequisites);
                DbLayer.SoleProprietorRequisitesRepository.Insert(soleProprietorContract);

                CreateAgentRequisitesFiles(agentRequisitesId, createAgentRequisites.SoleProprietorRequisites.Files);

                DbLayer.Save();
                transaction.Commit();

                return agentRequisitesId;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка создания реквизитов]");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать реквизиты юридического лица
        /// </summary>
        /// <param name="createAgentRequisites">Модель создания реквизитов агента</param>
        /// <returns>Id созданных реквизитов</returns>
        private Guid CreateLegalPersonRequisites(CreateAgentRequisitesDto createAgentRequisites)
        {
            _logger.Info("Создание реквизитов для физического лица");
            if (createAgentRequisites.LegalPersonRequisites == null)
                throw new NotFoundException("Отсутсвуют реквизиты для юр. лица");

            try
            {
                var agentRequisitesId = Guid.NewGuid();
                var agentRequisites = GenerateAgentRequisites(agentRequisitesId, createAgentRequisites.AccountId,
                    createAgentRequisites.AgentRequisitesStatus, legalPersonRequisitesId: agentRequisitesId);

                var legalPersonContract = createAgentRequisites.MapToLegalPersonRequisites();
                legalPersonContract.AgentRequisitesId = agentRequisitesId;
                DbLayer.AgentRequisitesRepository.InsertAgentRequisites(agentRequisites);
                DbLayer.Save();

                DbLayer.LegalPersonRequisitesRepository.Insert(legalPersonContract);
                CreateAgentRequisitesFiles(agentRequisitesId, createAgentRequisites.LegalPersonRequisites.Files);

                DbLayer.Save();

                return agentRequisitesId;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка создания реквизитов]");
                throw;
            }
        }

        /// <summary>
        /// Сформировать модель реквизитов агента
        /// </summary>
        /// <param name="id">Id реквизитов</param>
        /// <param name="accountOwnerId">Id аккаунта</param>
        /// <param name="agentRequisitesStatus">Статус реквизитов агента</param>
        /// <param name="legalPersonRequisitesId">Id реквизитов юр. лица</param>
        /// <param name="physicalPersonRequisitesId">Id реквизитов физ. лица</param>
        /// <param name="soleProprietorRequisitesId">Id реквизитов ИП</param>
        /// <returns>Агентский договор</returns>
        private Domain.DataModels.billing.AgentRequisites.AgentRequisites GenerateAgentRequisites(Guid id, Guid accountOwnerId,
            AgentRequisitesStatusEnum agentRequisitesStatus, Guid? legalPersonRequisitesId = null,
            Guid? physicalPersonRequisitesId = null, Guid? soleProprietorRequisitesId = null) => new()
            {
                Id = id,
                AccountOwnerId = accountOwnerId,
                AgentRequisitesStatus = agentRequisitesStatus,
                PhysicalPersonRequisitesId = physicalPersonRequisitesId,
                LegalPersonRequisitesId = legalPersonRequisitesId,
                SoleProprietorRequisitesId = soleProprietorRequisitesId,
                StatusDateTime = DateTime.Now
            };
    }
}
