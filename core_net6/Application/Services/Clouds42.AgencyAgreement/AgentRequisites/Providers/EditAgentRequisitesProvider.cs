﻿using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.Validators.Partner.Validators;
using Clouds42.Logger;
using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.AgentRequisites.Mappers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.DataModels.billing.AgentRequisites;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Providers
{
    /// <summary>
    /// Провайдер редактирования реквизитов агента
    /// </summary>
    internal class EditAgentRequisitesProvider : BaseAgentRequisitesProvider, IEditAgentRequisitesProvider
    {
        private  readonly ILogger42 _logger;
        private readonly CreateAgentDocumentNotificationProcessor _createAgentDocumentNotificationProcessor;

        /// <summary>
        /// Карта маппинга типа реквизитов к методу редактирования
        /// </summary>
        private readonly IDictionary<AgentRequisitesTypeEnum, Action<EditAgentRequisitesDto>> _mapAgentRequisitesTypeToRequisitesEditingMethod;

        /// <summary>
        /// Карта маппинга типа реквизитов к методу изменения типа
        /// </summary>
        private readonly IDictionary<AgentRequisitesTypeEnum, Action<EditAgentRequisitesDto>> _mapAgentRequisitesTypeToRequisitesChangingTypeMethod;

        public EditAgentRequisitesProvider(IUnitOfWork dbLayer,
            CreateAgentDocumentNotificationProcessor createAgentDocumentNotificationProcessor,
            ILogger42 logger) : base(dbLayer)
        {
            _logger = logger;
            _createAgentDocumentNotificationProcessor = createAgentDocumentNotificationProcessor;

            _mapAgentRequisitesTypeToRequisitesEditingMethod = new Dictionary<AgentRequisitesTypeEnum, Action<EditAgentRequisitesDto>>
            {
                {AgentRequisitesTypeEnum.PhysicalPersonRequisites, EditPhysicalPersonRequisites},
                {AgentRequisitesTypeEnum.LegalPersonRequisites, EditLegalPersonRequisites},
                {AgentRequisitesTypeEnum.SoleProprietorRequisites, EditSoleProprietorRequisites}
            };

            _mapAgentRequisitesTypeToRequisitesChangingTypeMethod = new Dictionary<AgentRequisitesTypeEnum, Action<EditAgentRequisitesDto>>
            {
                {AgentRequisitesTypeEnum.PhysicalPersonRequisites, ChangeRequisitesTypeToPhysicalPerson},
                {AgentRequisitesTypeEnum.LegalPersonRequisites, ChangeRequisitesTypeToLegalPerson},
                {AgentRequisitesTypeEnum.SoleProprietorRequisites, ChangeRequisitesTypeToSoleProprietor}
            };
        }

        /// <summary>
        /// Отредактировать реквизиты агента
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        public void EditAgentRequisites(EditAgentRequisitesDto editAgentRequisites)
        {
            _logger.Info($"Редактирование реквизитов агента {editAgentRequisites.Id} {editAgentRequisites.AgentRequisitesType.Description()}");
            var validateResult = editAgentRequisites.Validate();
            if (!validateResult.Success)
                throw new ValidateException(validateResult.Message);

            _mapAgentRequisitesTypeToRequisitesEditingMethod[editAgentRequisites.AgentRequisitesType](editAgentRequisites);

            var agentRequisites = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == editAgentRequisites.Id) ??
                                 throw new NotFoundException(
                                     $"Не удалось получить реквизиты агента по Id {editAgentRequisites.Id}");

            if (agentRequisites.AgentRequisitesStatus == AgentRequisitesStatusEnum.Draft &&
                editAgentRequisites.AgentRequisitesStatus == AgentRequisitesStatusEnum.OnCheck)
                _createAgentDocumentNotificationProcessor.CreateEmailAndNotifySupport(agentRequisites, editAgentRequisites.AgentRequisitesType);

            if (agentRequisites.AgentRequisitesStatus != editAgentRequisites.AgentRequisitesStatus)
                agentRequisites.StatusDateTime = DateTime.Now;

            agentRequisites.AgentRequisitesStatus = editAgentRequisites.AgentRequisitesStatus;
            DbLayer.AgentRequisitesRepository.Update(agentRequisites);
            DbLayer.Save();

            _logger.Info(
                $"Редактирование реквизитов агента {editAgentRequisites.Id} {editAgentRequisites.AgentRequisitesType.Description()} успешно завершено");
        }

        /// <summary>
        /// Сменить статус реквизитов агента
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        public void ChangeAgentRequisitesStatus(EditAgentRequisitesDto editAgentRequisites)
        {
            var agentRequisites = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == editAgentRequisites.Id)
                                 ?? throw new NotFoundException($"Реквизиты агента по {editAgentRequisites.Id} не найдены");

            var contractCashOutRequests = DbLayer.AgentCashOutRequestRepository.Where(cor =>
                cor.AgentRequisitesId == editAgentRequisites.Id &&
                cor.RequestStatus != AgentCashOutRequestStatusEnum.Paid).ToList();

            if (contractCashOutRequests.Any())
                throw new InvalidOperationException(
                    $@"Сменить статус реквизитов агента №{agentRequisites.AccountOwner.IndexNumber}-{agentRequisites.Number:000} невозможно. 
                                    По данным реквизитам есть заявки на вывод средств");

            _logger.Trace($@"Смена статуса реквизитов агента {editAgentRequisites.Id}-{agentRequisites.Number} 
                                    с {agentRequisites.AgentRequisitesStatus.Description()} на {editAgentRequisites.AgentRequisitesStatus.Description()}");

            if (agentRequisites.AgentRequisitesStatus != editAgentRequisites.AgentRequisitesStatus)
                agentRequisites.StatusDateTime = DateTime.Now;

            agentRequisites.AgentRequisitesStatus = editAgentRequisites.AgentRequisitesStatus;
            DbLayer.AgentRequisitesRepository.Update(agentRequisites);
            DbLayer.Save();
        }

        /// <summary>
        /// Отредактировать реквизиты физ. лица
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        private void EditPhysicalPersonRequisites(EditAgentRequisitesDto editAgentRequisites)
        {
            _logger.Info($"Редактирование реквизитов агента {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId}");
            var existContractModel = DbLayer.PhysicalPersonRequisitesRepository
                .FirstOrDefault( w => w.AgentRequisitesId == editAgentRequisites.Id);

            if (existContractModel == null)
            {
                _mapAgentRequisitesTypeToRequisitesChangingTypeMethod[editAgentRequisites.AgentRequisitesType](editAgentRequisites);
                return;
            }

            existContractModel.MapToPhysicalPersonRequisites(editAgentRequisites);
            DbLayer.PhysicalPersonRequisitesRepository.Update(existContractModel);

            UpdateAgentRequisitesFiles(editAgentRequisites.Id, editAgentRequisites.PhysicalPersonRequisites);
            DbLayer.Save();
            _logger.Info($"Редактирование реквизитов агента {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId} завершено");
        }

        /// <summary>
        /// Сменить тип реквизитов агента на рекизиты физ. лица
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        private void ChangeRequisitesTypeToPhysicalPerson(EditAgentRequisitesDto editAgentRequisites)
        {
            _logger.Info($"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId}");
            var agentRequisites = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == editAgentRequisites.Id) ??
                                 throw new NotFoundException($"Реквизиты агента по id '{editAgentRequisites.Id}' не найдены");
            using (var transaction = DbLayer.SmartTransaction.Get())
            {
                try
                {
                    var updateModel = editAgentRequisites.MapToPhysicalPersonRequisites();
                    updateModel.AgentRequisitesId = agentRequisites.Id;
                    DbLayer.PhysicalPersonRequisitesRepository.Insert(updateModel);
                    UpdateAgentRequisitesFiles(agentRequisites.Id, editAgentRequisites.PhysicalPersonRequisites);
                    UpdateAgentRequisites(agentRequisites,
                        new UpdateAgentRequisitesDto(null, updateModel.AgentRequisitesId, null));

                    DbLayer.Save();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    _logger.Info(
                        $"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {agentRequisites.AccountOwner.IndexNumber} завершена с ошибкой: {ex.GetFullInfo()}");
                    transaction.Rollback();
                    throw;
                }
            }
            _logger.Info($"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId} завершена");
        }

        /// <summary>
        /// Отредактировать реквизиты юр. лица
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        private void EditLegalPersonRequisites(EditAgentRequisitesDto editAgentRequisites)
        {
            _logger.Info($"Редактирование реквизитов агента {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId}");
            
            var existRequisites =
                DbLayer.LegalPersonRequisitesRepository.FirstOrDefault(w => w.AgentRequisitesId == editAgentRequisites.Id);

            if (existRequisites == null)
            {
                _mapAgentRequisitesTypeToRequisitesChangingTypeMethod[editAgentRequisites.AgentRequisitesType](editAgentRequisites);
                return;
            }

            existRequisites.MapToLegalPersonRequisites(editAgentRequisites);

            DbLayer.LegalPersonRequisitesRepository.Update(existRequisites);

            UpdateAgentRequisitesFiles(editAgentRequisites.Id, editAgentRequisites.LegalPersonRequisites);

            DbLayer.Save();
            _logger.Info($"Редактирование реквизитов агента {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId} завершено");
        }

        /// <summary>
        /// Сменить тип реквизитов агента на реквизиты физ. лица
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        private void ChangeRequisitesTypeToLegalPerson(EditAgentRequisitesDto editAgentRequisites)
        {
            _logger.Info($"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId}");
            var agentRequisites = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == editAgentRequisites.Id) ??
                                 throw new NotFoundException($"Реквизиты агента по id '{editAgentRequisites.Id}' не найдены");

            using (var transaction = DbLayer.SmartTransaction.Get())
            {
                try
                {
                    var updateModel = editAgentRequisites.MapToLegalPersonRequisites();
                    updateModel.AgentRequisitesId = agentRequisites.Id;
                    DbLayer.LegalPersonRequisitesRepository.Insert(updateModel);
                    UpdateAgentRequisitesFiles(agentRequisites.Id, editAgentRequisites.LegalPersonRequisites);
                    UpdateAgentRequisites(agentRequisites,
                        new UpdateAgentRequisitesDto(updateModel.AgentRequisitesId, null, null));

                    DbLayer.Save();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    _logger.Info(
                        $"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {agentRequisites.AccountOwner.IndexNumber} завершена с ошибкой: {ex.GetFullInfo()}");
                    transaction.Rollback();
                    throw;
                }
            }
            _logger.Info($"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId} завершена");
        }

        /// <summary>
        /// Отредактировать реквизиты ИП
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        private void EditSoleProprietorRequisites(EditAgentRequisitesDto editAgentRequisites)
        {
            _logger.Info($"Редактирование реквизитов агента {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId}");
            var existRequisitesModel = DbLayer.SoleProprietorRequisitesRepository.FirstOrDefault(w => w.AgentRequisitesId == editAgentRequisites.Id);

            if (existRequisitesModel == null)
            {
                _mapAgentRequisitesTypeToRequisitesChangingTypeMethod[editAgentRequisites.AgentRequisitesType](editAgentRequisites);
                return;
            }

            existRequisitesModel.MapToSoleProprietorRequisites(editAgentRequisites);

            DbLayer.SoleProprietorRequisitesRepository.Update(existRequisitesModel);

            UpdateAgentRequisitesFiles(editAgentRequisites.Id, editAgentRequisites.SoleProprietorRequisites);
            DbLayer.Save();
            _logger.Info($"Редактирование реквизитов агента {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId} завершено");
        }

        /// <summary>
        /// Сменить тип реквизитов агента на реквизиты ИП
        /// </summary>
        /// <param name="editAgentRequisites">Модель реквизитов агента</param>
        private void ChangeRequisitesTypeToSoleProprietor(EditAgentRequisitesDto editAgentRequisites)
        {
            _logger.Info($"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId}");
            var agentRequisites = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == editAgentRequisites.Id) ??
                                 throw new NotFoundException($"Реквизиты агента по id '{editAgentRequisites.Id}' не найдены");

            using (var transaction = DbLayer.SmartTransaction.Get())
            {
                try
                {
                    var updateModel = editAgentRequisites.MapToSoleProprietorRequisites();
                    updateModel.AgentRequisitesId = agentRequisites.Id;
                    DbLayer.SoleProprietorRequisitesRepository.Insert(updateModel);
                    UpdateAgentRequisitesFiles(agentRequisites.Id, editAgentRequisites.SoleProprietorRequisites);
                    UpdateAgentRequisites(agentRequisites,
                        new UpdateAgentRequisitesDto(null, null, updateModel.AgentRequisitesId));

                    DbLayer.Save();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    _logger.Info(
                        $"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {agentRequisites.AccountOwner.IndexNumber} завершена с ошибкой: {ex.GetFullInfo()}");
                    transaction.Rollback();
                    throw;
                }
            }
            _logger.Info($"Смена типа реквизитов агента на {editAgentRequisites.AgentRequisitesType.Description()} для аккаунта {editAgentRequisites.AccountId} завершена");
        }

        /// <summary>
        /// Обновить модель реквизитов агента
        /// </summary>
        /// <param name="agentRequisites">Модель реквизитов агента</param>
        /// <param name="updateModel">Модель обновления</param>
        private void UpdateAgentRequisites(Domain.DataModels.billing.AgentRequisites.AgentRequisites agentRequisites, UpdateAgentRequisitesDto updateModel)
        {
            _logger.Info($"Обновление модели реквизитов агента для аккаунта {agentRequisites.AccountOwner.IndexNumber}");
            if (updateModel.LegalPersonRequisitesId.HasValue)
            {
                DbLayer.PhysicalPersonRequisitesRepository.Delete(agentRequisites.PhysicalPersonRequisites);
                DbLayer.SoleProprietorRequisitesRepository.Delete(agentRequisites.SoleProprietorRequisites);
            }

            if (updateModel.PhysicalPersonRequisitesId.HasValue)
            {
                DbLayer.LegalPersonRequisitesRepository.Delete(agentRequisites.LegalPersonRequisites);
                DbLayer.SoleProprietorRequisitesRepository.Delete(agentRequisites.SoleProprietorRequisites);
            }

            if (updateModel.SoleProprietorRequisitesId.HasValue)
            {
                DbLayer.LegalPersonRequisitesRepository.Delete(agentRequisites.LegalPersonRequisites);
                DbLayer.PhysicalPersonRequisitesRepository.Delete(agentRequisites.PhysicalPersonRequisites);
            }

            agentRequisites.SoleProprietorRequisitesId = updateModel.SoleProprietorRequisitesId;
            agentRequisites.PhysicalPersonRequisitesId = updateModel.PhysicalPersonRequisitesId;
            agentRequisites.LegalPersonRequisitesId = updateModel.LegalPersonRequisitesId;

            DbLayer.AgentRequisitesRepository.Update(agentRequisites);

            _logger.Info($"Обновление модели реквизитов агента для аккаунта {agentRequisites.AccountOwner.IndexNumber} завершено");
        }

        /// <summary>
        /// Обновить файлы реквизитов агента
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов агента</param>
        /// <param name="personRequisites">Модель реквизитов агента</param>
        private void UpdateAgentRequisitesFiles(Guid agentRequisitesId, BasePersonRequisitesDto personRequisites)
        {
            _logger.Info($"Обновление файлов реквизитов агента {agentRequisitesId}");
            var existRequisitesFiles =
                DbLayer.AgentRequisitesFileRepository.Where(w =>
                    w.AgentRequisitesId == agentRequisitesId).ToList();

            if (personRequisites.Files == null ||
                !personRequisites.Files.Any())
            {
                RemoveAgentRequisitesFiles(existRequisitesFiles);
                return;
            }

            var newFiles = personRequisites.Files.Where(w =>
                !existRequisitesFiles.Exists(existFile =>
                    existFile.AgentRequisitesFileType == w.AgentRequisitesFileType &&
                    existFile.CloudFile.FileName == w.FileName)).ToList();
            _logger.Info($"Количество новых файлов реквизитов агента {agentRequisitesId} : {newFiles.Count}");

            var deletedFiles = existRequisitesFiles.Where(existFile =>
                !personRequisites.Files.Exists(w =>
                    w.AgentRequisitesFileType == existFile.AgentRequisitesFileType &&
                    w.FileName == existFile.CloudFile.FileName)).ToList();
            _logger.Info($"Количество удаляемых файлов реквизитов агента {agentRequisitesId} : {deletedFiles.Count}");

            CreateAgentRequisitesFiles(agentRequisitesId, newFiles);
            RemoveAgentRequisitesFiles(deletedFiles);

            _logger.Info($"Обновление файлов реквизитов агента {agentRequisitesId} завершено");
        }

        /// <summary>
        /// Удалить файлы реквизитов агента
        /// </summary>
        /// <param name="agentRequisitesFiles">Файлы реквизитов агента</param>
        private void RemoveAgentRequisitesFiles(List<AgentRequisitesFile> agentRequisitesFiles)
        {
            if (!agentRequisitesFiles.Any())
                return;

            var cloudFiles = agentRequisitesFiles.Select(w => w.CloudFile).ToList();
            DbLayer.AgentRequisitesFileRepository.DeleteRange(agentRequisitesFiles);
            DbLayer.CloudFileRepository.DeleteRange(cloudFiles);
        }
    }
}
