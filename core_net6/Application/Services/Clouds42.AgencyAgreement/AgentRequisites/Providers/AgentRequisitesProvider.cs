﻿using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.AgentRequisites.Mappers;
using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Providers
{
    /// <summary>
    /// Провайдер для работы с реквизитами агента
    /// </summary>
    internal class AgentRequisitesProvider(IUnitOfWork dbLayer)
        : BaseAgentRequisitesProvider(dbLayer), IAgentRequisitesProvider
    {
        /// <summary>
        /// Получить модель для редактирования реквизитов агента по Id
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов</param>
        /// <returns>Модель для редактирования реквизитов агента</returns>
        public EditAgentRequisitesDto GetEditAgentRequisitesDto(Guid agentRequisitesId)
        {
            var agentRequisites = GetAgentRequisites(agentRequisitesId);

            var agentRequisitesType = agentRequisites.GetAgentRequisitesType();
            var agencyFullName = agentRequisites.GetAgencyFullName(agentRequisitesType);

            var agentRequisitesFiles =
                DbLayer.AgentRequisitesFileRepository.Where(w => w.AgentRequisitesId == agentRequisitesId)
                    .Select(w => w.MapToFileDataDto()).ToList();

            var legalPersonRequisitesDto = agentRequisites.LegalPersonRequisites?.MapToLegalPersonRequisitesDto(agentRequisitesFiles);
            var soleProprietorRequisitesDto = agentRequisites.SoleProprietorRequisites?.MapToSoleProprietorRequisitesDto(agentRequisitesFiles);
            var physicalPersonRequisitesDto = agentRequisites.PhysicalPersonRequisites?.MapToPhysicalPersonRequisitesDto(agentRequisitesFiles);

            var editAgentRequisitesDto = new EditAgentRequisitesDto
            {
                Id = agentRequisites.Id,
                AccountId = agentRequisites.AccountOwnerId,
                AgentRequisitesStatus = agentRequisites.AgentRequisitesStatus,
                AgentRequisitesType = agentRequisitesType,
                LegalPersonRequisites = legalPersonRequisitesDto ?? new LegalPersonRequisitesDto(),
                SoleProprietorRequisites = soleProprietorRequisitesDto ?? new SoleProprietorRequisitesDto(),
                PhysicalPersonRequisites = physicalPersonRequisitesDto ?? new PhysicalPersonRequisitesDto(),
                IsEditMode = true,
                Number = StringExtension.ConvertToContractNumber(agentRequisites.AccountOwner.IndexNumber, agentRequisites.Number),
                ShortName = agencyFullName.GetShortName(),
                CreationDate = agentRequisites.CreationDate
            };

            return editAgentRequisitesDto;
        }

        /// <summary>
        /// Получить реквизиты агента по Id
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов</param>
        /// <returns>Модель реквизитов агента</returns>
        public Domain.DataModels.billing.AgentRequisites.AgentRequisites
            GetAgentRequisites(Guid agentRequisitesId) =>
            DbLayer.AgentRequisitesRepository.FirstOrDefault(ar => ar.Id == agentRequisitesId) ??
            throw new NotFoundException($"Реквизиты агента по ID '{agentRequisitesId}' не найдены");
    }
}
