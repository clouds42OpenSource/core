﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing.AgentRequisites;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Providers
{
    /// <summary>
    /// Базовый провайдер реквизитов агента
    /// </summary>
    internal class BaseAgentRequisitesProvider(IUnitOfWork dbLayer)
    {
        protected readonly IUnitOfWork DbLayer = dbLayer;

        /// <summary>
        /// Создать файлы реквизитов агента
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов</param>
        /// <param name="files">файлы реквизитов агента</param>
        protected void CreateAgentRequisitesFiles(Guid agentRequisitesId, List<CloudFileDataDto<byte[]>> files)
        {
            if (files == null || !files.Any())
                return;

            files.ForEach(w =>
            {
                var cloudFileId = CreateCloudFile(w);

                var agentRequisitesFile = new AgentRequisitesFile
                {
                    AgentRequisitesId = agentRequisitesId,
                    AgentRequisitesFileType = w.AgentRequisitesFileType,
                    CloudFileId = cloudFileId
                };

                DbLayer.AgentRequisitesFileRepository.Insert(agentRequisitesFile);
            });
        }

        /// <summary>
        /// Сгенерировать новое имя файлу
        /// </summary>
        /// <param name="oldFileName">Старое имя файла</param>
        /// <returns>Новое имя файла</returns>
        protected string GenerateNewFileName(string oldFileName)
        {
            string[] fileArr = oldFileName.Split('.');
            string format = fileArr.Last();

            return $"{Guid.NewGuid()}.{format}";
        }

        /// <summary>
        /// Создать файл облака
        /// </summary>
        /// <param name="fileData">Файл</param>
        /// <returns>Id файла облака</returns>
        protected Guid CreateCloudFile(CloudFileDataDto<byte[]> fileData)
        {
            var file = new CloudFile
            {
                Id = Guid.NewGuid(),
                FileName = GenerateNewFileName(fileData.FileName),
                Content = fileData.Content,
                ContentType = fileData.ContentType
            };

            DbLayer.CloudFileRepository.Insert(file);
            DbLayer.Save();

            return file.Id;
        }
    }
}