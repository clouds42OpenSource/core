﻿using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.billing.AgentRequisites;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentRequisites.Providers
{
    /// <summary>
    ///  Провайдер удаления реквизитов агента
    /// </summary>
    internal class RemoveAgentRequisitesProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : IRemoveAgentRequisitesProvider
    {
        /// <summary>
        /// Удалить реквизиты агента
        /// </summary>
        /// <param name="agentRequisitesId">Id реквизитов агента</param>
        public void RemoveAgentRequisites(Guid agentRequisitesId)
        {
            logger.Info($"Удаление реквизиты агента {agentRequisitesId}");
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var agentRequisites = dbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == agentRequisitesId) ??
                                      throw new NotFoundException($"Реквизиты агента не найдены по Id {agentRequisitesId}");

                if (agentRequisites.AgentRequisitesStatus != AgentRequisitesStatusEnum.Draft)
                    throw new InvalidOperationException($"Удаление реквизитов возможно только со статусом '{AgentRequisitesStatusEnum.Draft.Description()}'");

                var contractCashOutRequests = dbLayer.AgentCashOutRequestRepository
                    .Where(w => w.AgentRequisitesId == agentRequisitesId).ToList();

                if (contractCashOutRequests.Any())
                    throw new InvalidOperationException(
                        $@"Удаление реквизитов №{agentRequisites.AccountOwner.IndexNumber}-{agentRequisites.Number:000} невозможно. 
                                    По данным реквизитам есть заявки на вывод средств");

                RemoveAgentRequisitesFiles(agentRequisites.AgentRequisitesFiles.ToList());
                dbLayer.PhysicalPersonRequisitesRepository.Delete(agentRequisites.PhysicalPersonRequisites);
                dbLayer.SoleProprietorRequisitesRepository.Delete(agentRequisites.SoleProprietorRequisites);
                dbLayer.LegalPersonRequisitesRepository.Delete(agentRequisites.LegalPersonRequisites);
                dbLayer.AgentRequisitesRepository.Delete(agentRequisites);

                dbLayer.Save();
                transaction.Commit();

                logger.Info($"Удаление реквизитов {agentRequisitesId} успешно завершено");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления реквизитов] {agentRequisitesId}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Удалить файлы реквизитов агента
        /// </summary>
        /// <param name="agentRequisitesFiles">Файлы агентского договора</param>
        private void RemoveAgentRequisitesFiles(List<AgentRequisitesFile> agentRequisitesFiles)
        {
            if (!agentRequisitesFiles.Any())
                return;

            var cloudFiles = agentRequisitesFiles.Select(w => w.CloudFile).ToList();
            dbLayer.AgentRequisitesFileRepository.DeleteRange(agentRequisitesFiles);
            dbLayer.CloudFileRepository.DeleteRange(cloudFiles);
        }
    }
}
