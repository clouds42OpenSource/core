﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.AgencyAgreement.Contracts.AgentTransferBalanceRequest;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentTransferBalanceRequest.Managers
{
    /// <summary>
    /// Менеджер для создания заявки на перевод баланса агента
    /// </summary>
    public class CreateAgentTransferBalanceRequestManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateAgentTransferBalanceRequestProvider createAgentTransferBalanceRequestProvider,
        IHandlerException handlerException,
        AccountDataProvider accountDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать заявку на перевод баланса агента
        /// </summary>
        /// <param name="createAgentTransferBalanceRequest">Модель создания заявки на перевод баланса</param>
        public ManagerResult Create(CreateAgentTransferBalanseRequestDto createAgentTransferBalanceRequest)
        {
            AccessProvider.HasAccess(ObjectAction.AgentTransferBalanseRequest_Create,
                () => createAgentTransferBalanceRequest.FromAccountId);

            var account = accountDataProvider.GetAccount(createAgentTransferBalanceRequest.FromAccountId);
            if (account == null)
                return PreconditionFailed(
                    $"Аккаунт агента по номеру {createAgentTransferBalanceRequest.FromAccountId} не найден");

            var localeCurrency = accountConfigurationDataProvider.GetAccountLocale(account.Id).Currency;

            var logEventMessage =
                $"Сумма {createAgentTransferBalanceRequest.Sum:0.00} {localeCurrency}. переведена на баланс аккаунта";
            try
            {
                var abilityToCreate =
                    createAgentTransferBalanceRequestProvider.CheckAbilityToCreateAgentTransferBalanceRequest(
                        createAgentTransferBalanceRequest.FromAccountId, out var message,
                        createAgentTransferBalanceRequest.Sum);

                if (!abilityToCreate)
                    return PreconditionFailed(message);

                createAgentTransferBalanceRequestProvider.Create(createAgentTransferBalanceRequest);

                logger.Info(
                    $"[[{createAgentTransferBalanceRequest.FromAccountId}]-[{createAgentTransferBalanceRequest.ToAccountId}]]"
                    + $"- Создание заявки на перевод баланса агента, на сумму {createAgentTransferBalanceRequest.Sum}");
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CreateAgentTransferBalanseRequest,
                    logEventMessage);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"[Не удалось перевести сумму {createAgentTransferBalanceRequest.Sum} на баланс аккаунта]";
                _handlerException.Handle(ex, $"{errorMessage} {createAgentTransferBalanceRequest.FromAccountId}");
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CreateAgentTransferBalanseRequest,
                    $"{errorMessage}. Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Проверить возможность создания заявки
        /// на перевод баланса агента 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="sum">Сумма перевода</param>
        /// <returns>Результат проверки</returns>
        public ManagerResult<bool> CheckAbilityToCreateAgentTransferBalanceRequest(Guid accountId, decimal? sum = null)
        {
            var messageToLog = $"Проверка возможности создания заявки на перевод баланса агента {accountId}";
            try
            {
                logger.Info(messageToLog);
                AccessProvider.HasAccess(ObjectAction.AgentTransferBalanseRequest_Create, () => accountId);
                var result =
                    createAgentTransferBalanceRequestProvider.CheckAbilityToCreateAgentTransferBalanceRequest(
                        accountId, out var message, sum);

                return Ok(result, message);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{messageToLog} завершилась ошибкой.]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Получить доступную сумму для
        /// перевода баланса агента
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Доступная сумма перевода</returns>
        public ManagerResult<decimal> GetAvailableSumForAgentTransferBalanceRequest(Guid accountId)
        {
            var message = $"Получение доступной суммы для перевода баланса агента {accountId}";

            try
            {
                logger.Info(message);
                AccessProvider.HasAccess(ObjectAction.AgentTransferBalanseRequest_Create, () => accountId);
                var result = createAgentTransferBalanceRequestProvider.GetAvailableSumForAgentTransferBalanceRequest(accountId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилась ошибкой.]");
                return PreconditionFailed<decimal>(ex.Message);
            }
        }
    }
}
