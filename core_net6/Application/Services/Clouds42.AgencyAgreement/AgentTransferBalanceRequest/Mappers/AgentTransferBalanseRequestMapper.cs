﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.DataModels.billing.AgentPayments;

namespace Clouds42.AgencyAgreement.AgentTransferBalanceRequest.Mappers
{
    /// <summary>
    /// Маппер моделей заявки на перевод баланса агента
    /// </summary>
    public static class AgentTransferBalanseRequestMapper
    {
        /// <summary>
        /// Выполнить маппинг модели 
        /// </summary>
        /// <param name="model">Модель создания заявки на перевод баланса</param>
        /// <returns>Модель заявки на перевод баланса</returns>
        public static AgentTransferBalanseRequest MapToAgentTransferBalanseRequest(
            this CreateAgentTransferBalanseRequestDto model) =>
        new()
        {
            Id = Guid.NewGuid(),
            Sum = model.Sum,
            Comment = model.Comment,
            FromAccountId = model.FromAccountId,
            ToAccountId = model.ToAccountId,
            InitiatorId = model.InitiatorId
        };
    }
}
