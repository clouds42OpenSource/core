﻿using Clouds42.AgencyAgreement.AgentTransferBalanceRequest.Mappers;
using Clouds42.AgencyAgreement.Contracts.AgentTransferBalanceRequest;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Payments.Internal;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgentTransferBalanceRequest.Providers
{
    /// <summary>
    /// Провайдер для создания заявки на перевод баланса агента
    /// </summary>
    internal class CreateAgentTransferBalanceRequestProvider(
        ICreateAgentPaymentProvider createAgentPaymentProvider,
        IBillingPaymentsProvider billingPaymentsProvider,
        IUnitOfWork dbLayer,
        ICreateAgentCashOutRequestProvider createAgentCashOutRequestProvider,
        IInflowPaymentProcessor inflowPaymentProcessor,
        ILogger42 logger,
        IHandlerException handlerException)
        : ICreateAgentTransferBalanceRequestProvider
    {
        /// <summary>
        /// Создать заявку на перевод баланса агента
        /// </summary>
        /// <param name="createAgentTransferBalanceRequest">Модель создания заявки на перевод баланса</param>
        public void Create(CreateAgentTransferBalanseRequestDto createAgentTransferBalanceRequest)
        {
            const string paymentComment = "Перевод вознаграждения на баланс аккаунта";
            var logPrefix =
                $"[[{createAgentTransferBalanceRequest.FromAccountId}]-[{createAgentTransferBalanceRequest.ToAccountId}]]-";

            logger.Info($"{logPrefix}{paymentComment}.");
            var initiator = GetInitiator(createAgentTransferBalanceRequest.InitiatorId);

           // using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {

                logger.Info($"{logPrefix} Создание заявки на перевод баланса агента");
                CreateAgentTransferBalanceRequest(createAgentTransferBalanceRequest);

                logger.Info($"{logPrefix} Создание платежа");
                CreatePayment(createAgentTransferBalanceRequest, paymentComment, initiator.Login);

                logger.Info($"{logPrefix} Создание агентского платежа");
                CreateAgentPayment(createAgentTransferBalanceRequest, paymentComment);

               // dbScope.Commit();

                logger.Info($"{logPrefix} Создание заявки на перевод баланса агента завершено");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"{logPrefix} [Ошибка создания заявки на перевод баланса агента]");
               // dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Проверить возможность создания заявки
        /// на перевод баланса агента 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <param name="sum">Сумма перевода</param>
        /// <returns>Результат проверки</returns>
        public bool CheckAbilityToCreateAgentTransferBalanceRequest(Guid accountId, out string message,
            decimal? sum = null)
        {
            message = string.Empty;

            var agentAccount = GetAccount(accountId);
            var availableSum = GetAvailableSumForAgentTransferBalanceRequest(accountId);

            if (availableSum <= 0)
            {
                message = $"Перевод баланса агента не возможен. Доступно для перевода {availableSum:0.00} руб.";
                return false;
            }

            if (availableSum < sum)
            {
                message = $"Перевод баланса агента не возможен. Сумма перевода={sum}. Доступно для перевода {availableSum:0.00} руб.";
                return false;
            }

            if (!createAgentCashOutRequestProvider.TryGetCashOutRequestNumberWhichIsProcessed(agentAccount,
                    out var cashOutRequestNumber))
            {
                return true;
            }

            message = $"- У Вас уже создана заявка № {cashOutRequestNumber}. Создавать новые заявки Вы сможете после выполнения текущей.";

            return false;

        }

        /// <summary>
        /// Получить доступную сумму для
        /// перевода баланса агента
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Доступная сумма перевода</returns>
        public decimal GetAvailableSumForAgentTransferBalanceRequest(Guid accountId) =>
            dbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == accountId)?.AvailableSum ??
            throw new NotFoundException($"Кошелек агента {accountId} не найден");

        /// <summary>
        /// Получить аккаунт
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Аккаунт</returns>
        private Account GetAccount(Guid accountId)
            => dbLayer.AccountsRepository.GetAccount(accountId) ??
               throw new NotFoundException(
                   $"Аккаунт агента по номеру {accountId} не найден");

        /// <summary>
        /// Создать агентский платеж
        /// </summary>
        /// <param name="createAgentTransferBalanceRequest">Модель создания заявки на перевод баланса</param>
        /// <param name="paymentComment">Комментарий для платежа</param>
        private void CreateAgentPayment(CreateAgentTransferBalanseRequestDto createAgentTransferBalanceRequest,
            string paymentComment)
        {
            createAgentPaymentProvider.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                AgentAccountId = createAgentTransferBalanceRequest.FromAccountId,
                Sum = createAgentTransferBalanceRequest.Sum,
                PaymentType = PaymentType.Outflow,
                Comment = paymentComment,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.AgentTransferBalanseRequest,
                Date = DateTime.Now
            });
        }

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="createAgentTransferBalanceRequest">Модель создания заявки на перевод баланса</param>
        /// <param name="paymentComment">Комментарий для платежа</param>
        /// <param name="initiatorLogin">Логин инициатора</param>
        private void CreatePayment(CreateAgentTransferBalanseRequestDto createAgentTransferBalanceRequest,
            string paymentComment, string initiatorLogin)
        {
            var result = billingPaymentsProvider.AddPayment(new Payment
            {
                TransactionType = TransactionType.Money,
                AccountId = createAgentTransferBalanceRequest.ToAccountId,
                Date = DateTime.Now,
                Sum = createAgentTransferBalanceRequest.Sum,
                OperationType = PaymentType.Inflow.ToString(),
                Status = PaymentStatus.Done.ToString(),
                PaymentSystem = PaymentSystem.ControlPanel.ToString(),
                Description = paymentComment,
                OriginDetails = initiatorLogin,
                Id = Guid.NewGuid()
            });

            if (result == PaymentOperationResult.Ok)
                inflowPaymentProcessor.Process(createAgentTransferBalanceRequest.ToAccountId);
        }

        /// <summary>
        /// Создать заявку на перевод баланса агента
        /// </summary>
        /// <param name="createAgentTransferBalanceRequest">Модель создания заявки на перевод баланса</param>
        private void CreateAgentTransferBalanceRequest(
            CreateAgentTransferBalanseRequestDto createAgentTransferBalanceRequest)
        {
            var agentTransferBalanceRequest = createAgentTransferBalanceRequest.MapToAgentTransferBalanseRequest();
            dbLayer.AgentTransferBalanceRequestRepository.Insert(agentTransferBalanceRequest);
            dbLayer.Save();
        }

        /// <summary>
        /// Получить инициатора заявки
        /// </summary>
        /// <param name="initiatorId">Id инициатора</param>
        /// <returns>Инифиатор(Пользователь аккаунта)</returns>
        private AccountUser GetInitiator(Guid initiatorId) =>
            dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == initiatorId) ??
            throw new NotFoundException($"Не удалось найти пользователя по Id {initiatorId}");
    }
}
