﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Providers
{
    /// <summary>
    /// Провайдер для печати с агентского соглашения
    /// </summary>
    internal class PrintAgencyAgreementProvider(
        IUnitOfWork dbLayer,
        PrintedHtmlFormBuilder printedHtmlFormBuilder,
        IAccessProvider accessProvider)
        : IPrintAgencyAgreementProvider
    {
        /// <summary>
        /// Печать агентского соглашения
        /// </summary>
        /// <param name="agencyAgreementId">Id агентского соглашения</param>
        /// <returns>Документ</returns>
        public DocumentBuilderResult Print(Guid agencyAgreementId)
        {
            var supplierName = dbLayer.AccountsRepository
                .AsQueryable()
                .Include(x => x.AccountConfiguration)
                .ThenInclude(x => x.Supplier)
                .FirstOrDefault(x => x.Id == accessProvider.ContextAccountId)?.AccountConfiguration?.Supplier?.Name;

            var printedAgencyAgreementDto = new PrintedAgencyAgreementDto
            {
                ActualAgencyAgreementUrl = new Uri($"{CloudConfigurationProvider.Cp.GetSiteCoreUrl()}/partners/open-actual-agency-agreement").AbsoluteUri,
                SupplierReferenceName = supplierName
            };

            return printedHtmlFormBuilder.Build(printedAgencyAgreementDto, GetPrintedHtmlForm(agencyAgreementId), "Агентское соглашение");
        }

        /// <summary>
        /// Получить печатную форму Html
        /// </summary>
        /// <param name="agencyAgreementId">Id агентского соглашения</param>
        /// <returns>Печатная форма Html</returns>
        private PrintedHtmlFormDto GetPrintedHtmlForm(Guid agencyAgreementId)
        {
            var printedHtmlForm = dbLayer.AgencyAgreementRepository.FirstOrDefault(w => w.Id == agencyAgreementId).PrintedHtmlForm;

            return new PrintedHtmlFormDto
            {
                Id = printedHtmlForm.Id,
                Name = printedHtmlForm.Name,
                HtmlData = printedHtmlForm.HtmlData,
                Files = printedHtmlForm.Files.Select(printedHtmlFormFile => new CloudFileDto
                {
                    Id = printedHtmlFormFile.CloudFile.Id,
                    FileName = printedHtmlFormFile.CloudFile.FileName,
                    ContentType = printedHtmlFormFile.CloudFile.ContentType,
                    Base64 = Convert.ToBase64String(printedHtmlFormFile.CloudFile.Content)
                }).ToList()
            };
        }
    }
}
