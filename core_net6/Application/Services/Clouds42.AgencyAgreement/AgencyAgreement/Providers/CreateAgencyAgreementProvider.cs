﻿using Clouds42.AgencyAgreement.AgencyAgreement.Mappers;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.AgencyAgreement.Validators;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Providers
{
    /// <summary>
    /// Провайдер для создания агентских соглашений
    /// </summary>
    internal class CreateAgencyAgreementProvider(IUnitOfWork dbLayer) : ICreateAgencyAgreementProvider
    {
        /// <summary>
        /// Создать агентское соглашение
        /// </summary>
        /// <param name="args">Модель создания агентского соглашения</param>
        /// <returns>Результат создания, Id агентского соглашения</returns>
        public Guid AddAgencyAgreementItem(AddAgencyAgreementItemDto args)
        {
            args.Validate();
            var agencyAgreement = args.MapToAgencyAgreement();
            dbLayer.AgencyAgreementRepository.Insert(agencyAgreement);
            dbLayer.Save();

            return agencyAgreement.Id;
        }
    }
}
