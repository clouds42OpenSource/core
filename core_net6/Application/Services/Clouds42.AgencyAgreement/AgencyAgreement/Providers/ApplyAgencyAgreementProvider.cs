﻿using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Providers
{
    /// <summary>
    /// Провайдер принятия агентского соглашения
    /// </summary>
    internal class ApplyAgencyAgreementProvider(IUnitOfWork dbLayer, IHandlerException handlerException)
        : IApplyAgencyAgreementProvider
    {
        /// <summary>
        /// Принять агентское соглашение
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void Apply(Guid accountId)
        {
            try
            {
                var agentWallet = dbLayer.AgentWalletRepository.GetAgentWalletOrCreateNew(accountId);
                agentWallet.ApplyAgencyAgreementDate = DateTime.Now;
                dbLayer.AgentWalletRepository.Update(agentWallet);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка принятия агентского соглашения для аккаунта] {accountId}");
                throw;
            }
        }
    }
}
