﻿using Clouds42.AgencyAgreement.AgencyAgreement.Mappers;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Providers
{
    /// <summary>
    /// Провайдер для работы с данными агентского соглашения
    /// </summary>
    public class AgencyAgreementDataProvider(IUnitOfWork dbLayer) : IAgencyAgreementDataProvider
    {
        /// <summary>
        /// Получить данные агентских соглашений
        /// </summary>
        /// <returns>Данные агентских соглашений</returns>
        public AgencyAgreementsDataDto GetAgencyAgreementsData() => new()
        {
            AgencyAgreementInfos = dbLayer.AgencyAgreementRepository.WhereLazy().Select(s => new AgencyAgreementInfoDto
            {
                Id = s.Id,
                Name = s.Name,
                EffectiveDate = s.EffectiveDate
            }).ToList()
        };

        /// <summary>
        /// Получить детальную информацию
        /// об агентском соглашении
        /// </summary>
        /// <param name="agencyAgreementId">Id агентского соглашения</param>
        /// <returns>Детальная информация об агентском соглашении</returns>
        public AgencyAgreementDetailsDto GetAgencyAgreementDetails(Guid agencyAgreementId)
        {
            var agencyAgreement = dbLayer.AgencyAgreementRepository
                .AsQueryableNoTracking()
                .Include(p => p.PrintedHtmlForm)
                .FirstOrDefault(aa => aa.Id == agencyAgreementId) ?? throw new NotFoundException($"Не удалось получить соглашение по Id='{agencyAgreementId}'");

            return agencyAgreement.MapToAgencyAgreementDetailsDto();
        }

        /// <summary>
        /// Получить актуальное агентское соглашение
        /// </summary>
        /// <returns>Актуальное агентское соглашение</returns>
        public Domain.DataModels.billing.AgencyAgreement GetActualAgencyAgreement()
        {
            var nowDate = DateTime.Now;

            return dbLayer.AgencyAgreementRepository
                .AsQueryableNoTracking()
                .OrderByDescending(x => x.EffectiveDate)
                .FirstOrDefault(x => x.EffectiveDate < nowDate) ?? throw new NotFoundException("Не найдено актуальное агентское соглашение");
        }

        /// <summary>
        /// Получить актуальное агентское соглашение
        /// </summary>
        /// <returns>Актуальное агентское соглашение</returns>
        public async Task<Domain.DataModels.billing.AgencyAgreement> GetActualAgencyAgreementAsync()
        {
            var nowDate = DateTime.Now;

            return await dbLayer.AgencyAgreementRepository
                .AsQueryableNoTracking()
                .OrderByDescending(x => x.EffectiveDate)
                .FirstOrDefaultAsync(x => x.EffectiveDate < nowDate) ?? throw new NotFoundException("Не найдено актуальное агентское соглашение");
        }


        /// <summary>
        /// Получить следующее агентское соглашение
        /// которое вступит в силу(если такое соглашение существует)
        /// </summary>
        /// <returns>Следующее агентское соглашение</returns>
        public AgencyAgreementDetailsDto TryGetNextEffectiveAgencyAgreement()
        {
            var nowDate = DateTime.Now;
            var agencyAgreement = dbLayer.AgencyAgreementRepository
                    .AsQueryableNoTracking()
                    .Include(p => p.PrintedHtmlForm)
                    .OrderBy(aa => aa.EffectiveDate)
                    .FirstOrDefault(aa => aa.EffectiveDate > nowDate);

            return agencyAgreement?.MapToAgencyAgreementDetailsDto();
        }

    }
}
