﻿using Clouds42.Common;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Helpers
{
    /// <summary>
    /// Утилита для сортиовки записей агентских соглашений
    /// </summary>
    public static class AgencyAgreementSortingUtility
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateQueryable<Domain.DataModels.billing.AgencyAgreement>> SortingActions = new()
        {
            { AgencyAgreementSortFieldNames.AgencyAgreementName, SortByAgencyAgreementName }
        };

        /// <summary>
        /// Сортитровать записи агентских соглашений
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи агентских соглашений по названию</returns>
        private static IOrderedQueryable<Domain.DataModels.billing.AgencyAgreement> SortByDefault(IQueryable<Domain.DataModels.billing.AgencyAgreement> records)
            => records.OrderByDescending(row => row.Name);


        /// <summary>
        /// Сортитровать выбранные записи агентских соглашений
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей агентских соглашений по полю Name</returns>
        private static IOrderedQueryable<Domain.DataModels.billing.AgencyAgreement> SortByAgencyAgreementName(IQueryable<Domain.DataModels.billing.AgencyAgreement> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Name),
                SortType.Desc => records.OrderByDescending(row => row.Name),
                _ => SortByDefault(records)
            };
        }


        /// <summary>
        /// Производит сортировку выбранных записей агентских соглашений
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи агентских соглашений</returns>
        public static IOrderedQueryable<Domain.DataModels.billing.AgencyAgreement> MakeSorting(IQueryable<Domain.DataModels.billing.AgencyAgreement> records, SortingDataDto sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();

            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate)
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }
    }
}