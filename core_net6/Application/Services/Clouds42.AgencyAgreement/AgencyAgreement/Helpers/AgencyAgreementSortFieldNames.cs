﻿namespace Clouds42.AgencyAgreement.AgencyAgreement.Helpers
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки агентских соглашений
    /// </summary>
    public static class AgencyAgreementSortFieldNames
    {
        /// <summary>
        ///  Название агентского соглашения
        /// </summary>
        public const string AgencyAgreementName = "agencyagreementname";
    }
}