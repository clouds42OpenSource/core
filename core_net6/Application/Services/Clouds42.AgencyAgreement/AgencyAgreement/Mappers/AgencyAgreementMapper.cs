﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Mappers
{
    /// <summary>
    /// Маппер агентского соглашения
    /// </summary>
    public static class AgencyAgreementMapper
    {
        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="agencyAgreement">Агентское соглашение</param>
        /// <returns>Детальная информация об агентском соглашении</returns>
        public static AgencyAgreementDetailsDto MapToAgencyAgreementDetailsDto(this Domain.DataModels.billing.AgencyAgreement agencyAgreement) =>
            new()
            {
                EffectiveDate = agencyAgreement.EffectiveDate,
                MyDiskRewardPercent = agencyAgreement.MyDiskRewardPercent,
                ServiceOwnerRewardPercent = agencyAgreement.ServiceOwnerRewardPercent,
                Rent1CRewardPercent = agencyAgreement.Rent1CRewardPercent,
                PrintedHtmlFormId = agencyAgreement.PrintedHtmlFormId,
                PrintedHtmlFormName = agencyAgreement.PrintedHtmlForm.Name,
                Rent1CRewardPercentForVipAccounts = agencyAgreement.Rent1CRewardPercentForVipAccounts,
                MyDiskRewardPercentForVipAccounts = agencyAgreement.MyDiskRewardPercentForVipAccounts,
                ServiceOwnerRewardPercentForVipAccounts = agencyAgreement.ServiceOwnerRewardPercentForVipAccounts,
                Name = agencyAgreement.Name,
                Id = agencyAgreement.Id
            };

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="createAgencyAgreementDto">Модель создания агентского соглашения</param>
        /// <returns>Агентское соглашение</returns>
        public static Domain.DataModels.billing.AgencyAgreement MapToAgencyAgreement(this AddAgencyAgreementItemDto createAgencyAgreementDto) =>
            new()
            {
                Name = createAgencyAgreementDto.Name,
                EffectiveDate = createAgencyAgreementDto.EffectiveDate,
                MyDiskRewardPercent = createAgencyAgreementDto.MyDiskRewardPercent,
                PrintedHtmlFormId = createAgencyAgreementDto.PrintedHtmlFormId,
                Rent1CRewardPercent = createAgencyAgreementDto.Rent1CRewardPercent,
                ServiceOwnerRewardPercent = createAgencyAgreementDto.ServiceOwnerRewardPercent,
                CreationDate = DateTime.Now,
                Rent1CRewardPercentForVipAccounts = createAgencyAgreementDto.Rent1CRewardPercentForVipAccounts,
                MyDiskRewardPercentForVipAccounts = createAgencyAgreementDto.MyDiskRewardPercentForVipAccounts,
                ServiceOwnerRewardPercentForVipAccounts = createAgencyAgreementDto.ServiceOwnerRewardPercentForVipAccounts
            };
    }
}
