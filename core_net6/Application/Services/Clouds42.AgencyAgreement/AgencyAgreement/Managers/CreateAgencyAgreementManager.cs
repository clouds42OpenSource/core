﻿using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Managers
{
    /// <summary>
    /// Менеджер для создания агентских соглашений
    /// </summary>
    public class CreateAgencyAgreementManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateAgencyAgreementProvider createAgencyAgreementProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать агентское соглашение
        /// </summary>
        /// <param name="args">Модель создания агентского соглашения</param>
        /// <returns>Результат создания, Id агентского соглашения</returns>
        public ManagerResult<Guid> AddAgencyAgreementItem(AddAgencyAgreementItemDto args)
        {
            AccessProvider.HasAccess(ObjectAction.AgencyAgreement_CreateAgencyAgreement);
            try
            {
                var message =
                    $"Создание агентского соглашения, которое вступит в силу {args.EffectiveDate}";
                logger.Info(message);
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CreateAgencyAgreement, message);
                var agencyAgreementId = createAgencyAgreementProvider.AddAgencyAgreementItem(args);
                return Ok(agencyAgreementId);
            }
            catch (Exception ex)
            {
                var errorMessage = "[Создание агентского соглашения завершилось ошибкой.]";
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CreateAgencyAgreement, $"{errorMessage} Причина: {ex.Message}");
                _handlerException.Handle(ex, errorMessage);
                return PreconditionFailed<Guid>(ex.Message);
            }
        }
    }
}
