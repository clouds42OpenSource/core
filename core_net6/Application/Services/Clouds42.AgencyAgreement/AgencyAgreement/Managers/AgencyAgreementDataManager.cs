﻿using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Managers
{
    /// <summary>
    /// Менеджер для работы с данными агентских соглашений
    /// </summary>
    public class AgencyAgreementDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAgencyAgreementDataProvider agencyAgreementDataProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить данные агентских соглашений
        /// </summary>
        /// <returns>Данные агентских соглашений</returns>
        public ManagerResult<AgencyAgreementsDataDto> GetAgencyAgreementsData()
        {
            AccessProvider.HasAccess(ObjectAction.AgencyAgreement_GetAgencyAgreementsData);
            try
            {
                logger.Info("Получение данных агентских соглашений");
                return Ok(agencyAgreementDataProvider.GetAgencyAgreementsData());
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Получение данных агентских соглашений завершилось ошибкой.]");
                return PreconditionFailed<AgencyAgreementsDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить детальную информацию
        /// об агентском соглашении
        /// </summary>
        /// <param name="agencyAgreementId">Id агентского соглашения</param>
        /// <returns>Детальная информация об агентском соглашении</returns>
        public ManagerResult<AgencyAgreementDetailsDto> GetAgencyAgreementDetails(Guid agencyAgreementId)
        {
            AccessProvider.HasAccess(ObjectAction.AgencyAgreement_GetAgencyAgreementDetails);
            var message = $"Получение детальной информации об агентском соглашении '{agencyAgreementId}'";
            try
            {
                logger.Info(message);
                return Ok(agencyAgreementDataProvider.GetAgencyAgreementDetails(agencyAgreementId));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");
                return PreconditionFailed<AgencyAgreementDetailsDto>(ex.Message);
            }
        }
    }
}
