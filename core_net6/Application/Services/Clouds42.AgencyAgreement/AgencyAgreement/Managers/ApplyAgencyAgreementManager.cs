﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Managers
{
    /// <summary>
    /// Менеджер принятия агентского соглашения
    /// </summary>
    public class ApplyAgencyAgreementManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IApplyAgencyAgreementProvider applyAgencyAgreementProvider,
        IHandlerException handlerException,
        AccountDataProvider accountDataProvider,
        IAgencyAgreementDataProvider agencyAgreementDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Принять агентское соглашение
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public ManagerResult Apply(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.AgencyAgreement_ApplyAgencyAgreement,
            () => accountId);

            var actualAgencyAgreement = agencyAgreementDataProvider.GetActualAgencyAgreement();

            try
            {
                if (!accountDataProvider.NeedApplyAgencyAgreement(accountId))
                    return Ok();

                applyAgencyAgreementProvider.Apply(accountId);

                var message = $"Принято соглашение “{actualAgencyAgreement.Name}”";

                Logger.Info($"{message} аккаунтом {accountId}");

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.ApplyAgencyAgreement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"[Принятие агентского соглашения “{actualAgencyAgreement.Name}” аккаунтом {accountId} завершилось ошибкой.]";

                _handlerException.Handle(ex, errorMessage);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.ApplyAgencyAgreement,
                    $"{errorMessage} Причина: {ex.Message}");

                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Необходимость принять агентское соглашение
        /// аккаунту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Необходимость принять агентское соглашение</returns>
        public ManagerResult<bool> NeedApplyAgencyAgreement(Guid accountId)
        {
            try
            {
                var needApplyAgencyAgreement = accountDataProvider.NeedApplyAgencyAgreement(accountId);
                return Ok(needApplyAgencyAgreement);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<bool>(ex.Message);
            }
        }
    }
}
