﻿using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.AgencyAgreement.Managers
{
    /// <summary>
    /// Менеджер для печати с агентского соглашения
    /// </summary>
    public class PrintAgencyAgreementManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IPrintAgencyAgreementProvider printAgencyAgreementProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Печать актуального агентского соглашения
        /// </summary>
        /// <returns>Документ</returns>
        public ManagerResult<DocumentBuilderResult> PrintActualAgencyAgreement()
        {
            var message = "Печать актуального агентского соглашения";
            try
            {
                Logger.Info(message);

                var nowDate = DateTime.Now;
                var actualAgencyAgreement = DbLayer.AgencyAgreementRepository
                                                .AsQueryableNoTracking()
                                                .OrderByDescending(x => x.EffectiveDate).FirstOrDefault(x => x.EffectiveDate < nowDate) ?? throw new NotFoundException("Не найдено актуальное агентское соглашение");

                return Ok(printAgencyAgreementProvider.Print(actualAgencyAgreement.Id));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");
                return PreconditionFailed<DocumentBuilderResult>(ex.Message);
            }
        }
    }
}
