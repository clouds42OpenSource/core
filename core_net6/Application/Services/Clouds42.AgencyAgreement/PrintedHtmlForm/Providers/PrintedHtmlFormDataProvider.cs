﻿using Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm;
using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.AgencyAgreement.PrintedHtmlForm.Providers
{
    /// <summary>
    /// Провайдер данных печатных форм HTML
    /// </summary>
    internal class PrintedHtmlFormDataProvider : IPrintedHtmlFormDataProvider
    {

        /// <summary>
        /// Получить модель печатной формы для создания
        /// </summary>
        /// <returns>Модель печатной формы для создания</returns>
        public PrintedHtmlFormDto GetPrintedHtmlFormModelForCreating()
        {
            return new PrintedHtmlFormDto
            {
                ModelTypes = GetModelTypesForPrintedHtmlForms(),
                HtmlData = "",
                Files = []
            };
        }

        /// <summary>
        /// Получить тип моделей для печатных форм
        /// </summary>
        /// <returns>Тип моделей</returns>
        private Dictionary<string, string> GetModelTypesForPrintedHtmlForms()
        {
            var interfaceType = typeof(IPrintedHtmlFormModelDto);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var types = new List<Type>();

            assemblies.ToList().ForEach(assembly =>
            {
                try
                {
                    var assemblyTypes = assembly.GetTypes().Where(p =>
                        interfaceType.IsAssignableFrom(p) && !interfaceType.IsAssignableFrom(p.BaseType)).ToList();

                    types.AddRange(assemblyTypes);
                }
                catch
                {
                    // ignored
                }
            });

            if (!types.Any())
                return new Dictionary<string, string>();

            var modelTypes = new Dictionary<string, string>();
            types.ForEach(w =>
            {
                if (w != null && w != interfaceType)
                    modelTypes.Add(w.FullName ?? "",
                        ((IPrintedHtmlFormModelDto)Activator.CreateInstance(w)).GetModelName());
            });

            return modelTypes;
        }
    }
}
