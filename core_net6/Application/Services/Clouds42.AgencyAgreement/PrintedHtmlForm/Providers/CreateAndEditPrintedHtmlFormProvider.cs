﻿using Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.PrintedHtmlForm.Providers
{
    /// <summary>
    /// Провайдер сохранения/изменения печатной формой HTML
    /// </summary>
    internal class CreateAndEditPrintedHtmlFormProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : ICreateAndEditPrintedHtmlFormProvider
    {
        /// <summary>
        /// Создать/Изменить печатную форму
        /// </summary>
        /// <param name="printedHtmlFormDc">Модель печатной формы</param>
        /// <returns>Id печатной формы</returns>
        public Guid CreateOrChangePrintedHtmlForm(PrintedHtmlFormDto printedHtmlFormDc)
        {
            printedHtmlFormDc.HtmlData = Uri.UnescapeDataString(printedHtmlFormDc.HtmlData);

            if (printedHtmlFormDc.Id.IsNullOrEmpty())
                return CreatePrintedHtmlForm(printedHtmlFormDc);

            return ChangePrintedHtmlForm(printedHtmlFormDc);
        }

        /// <summary>
        /// Изменить печатную форму HTML
        /// </summary>
        /// <param name="printedHtmlFormDc">Модель печатной формы</param>
        /// <returns>Id печатной формы</returns>
        private Guid ChangePrintedHtmlForm(PrintedHtmlFormDto printedHtmlFormDc)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var printedHtmlForm =
                    dbLayer.PrintedHtmlFormRepository.FirstOrDefault(w => w.Id == printedHtmlFormDc.Id) ??
                    throw new NotFoundException($"Не удалось найти печатную фому по Id {printedHtmlFormDc.Id}");

                printedHtmlForm.HtmlData = printedHtmlFormDc.HtmlData;
                printedHtmlForm.Name = printedHtmlFormDc.Name;
                printedHtmlForm.ModelType = printedHtmlFormDc.ModelType;

                dbLayer.PrintedHtmlFormRepository.Update(printedHtmlForm);
                dbLayer.Save();

                RemoveObsoleteFilesAtPrintedHtmlForm(printedHtmlForm.Id, printedHtmlFormDc);
                InsertNewFilesToPrintedHtmlForm(printedHtmlForm.Id, printedHtmlFormDc.Files);

                transaction.Commit();
                return printedHtmlForm.Id;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования печатной формы] {printedHtmlFormDc.Name}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать печатную форму HTML
        /// </summary>
        /// <param name="printedHtmlFormDc">Печатная форма HTML</param>
        /// <returns>Id созданой печатной формы</returns>
        private Guid CreatePrintedHtmlForm(PrintedHtmlFormDto printedHtmlFormDc)
        {
            try
            {
                var printedHtmlForm = new Domain.DataModels.PrintedHtmlForm
                {
                    Name = printedHtmlFormDc.Name,
                    HtmlData = printedHtmlFormDc.HtmlData,
                    ModelType = printedHtmlFormDc.ModelType
                };

                dbLayer.PrintedHtmlFormRepository.Insert(printedHtmlForm);
                dbLayer.Save();

                InsertNewFilesToPrintedHtmlForm(printedHtmlForm.Id, printedHtmlFormDc.Files);

                return printedHtmlForm.Id;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания печатной формы] {printedHtmlFormDc.Name}");
                throw;
            }
        }


        /// <summary>
        /// Добавить новые файлы к печатной форме HTML
        /// </summary>
        /// <param name="printedHtmlFormId">ID печатной формы</param>
        /// <param name="files">Список файлов</param>
        private void InsertNewFilesToPrintedHtmlForm(Guid printedHtmlFormId, List<CloudFileDto> files)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var newFiles = files.Where(w => w.Id.IsNullOrEmpty());

                foreach (var newFile in newFiles)
                {
                    var newCloudFile = new CloudFile
                    {
                        Id = Guid.NewGuid(),
                        FileName = newFile.FileName,
                        ContentType = newFile.ContentType,
                        Content = Convert.FromBase64String(newFile.Base64)
                    };

                    var printedHtmlFormFile = new PrintedHtmlFormFile
                    {
                        CloudFileId = newCloudFile.Id,
                        PrintedHtmlFormId = printedHtmlFormId
                    };

                    dbLayer.CloudFileRepository.Insert(newCloudFile);
                    dbLayer.PrintedHtmlFormFileRepository.Insert(printedHtmlFormFile);
                }

                dbLayer.Save();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошика добавления файлов в печатную форму] {printedHtmlFormId}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Удалить устаревшие файлы у печатной формы
        /// </summary>
        /// <param name="printedHtmlFormId">ID печатной формы</param>
        /// <param name="printedHtmlFormDc">Модель печатной формы</param>
        private void RemoveObsoleteFilesAtPrintedHtmlForm(Guid printedHtmlFormId, PrintedHtmlFormDto printedHtmlFormDc)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var currentIds = printedHtmlFormDc.Files.Select(x => x.Id);
                var printedHtmlFormFiles = dbLayer.PrintedHtmlFormFileRepository.WhereLazy(w => w.PrintedHtmlFormId == printedHtmlFormId).ToList();

                var obsoleteRazorFiles = printedHtmlFormFiles.Where(w => !currentIds.Contains(w.CloudFileId)).ToList();

                var obsoleteCloudFilesIds = obsoleteRazorFiles.Select(w => w.CloudFileId).ToList();
                var obsoleteCloudFiles = dbLayer.CloudFileRepository.WhereLazy(w => obsoleteCloudFilesIds.Contains(w.Id)).ToList();

                dbLayer.PrintedHtmlFormFileRepository.DeleteRange(obsoleteRazorFiles);
                dbLayer.CloudFileRepository.DeleteRange(obsoleteCloudFiles);

                dbLayer.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления файлов из печатной формы] {printedHtmlFormId}");
                transaction.Rollback();
                throw;
            }
        }
    }
}
