﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm;
using Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.PrintedHtmlForm.Providers
{
    /// <summary>
    /// Провайдер для работы с печатной формой HTML
    /// </summary>
    public class PrintedHtmlFormProvider(
        IUnitOfWork dbLayer,
        PrintedHtmlFormBuilder htmlFormBuilder,
        IAccessProvider accessProvider)
        : IPrintedHtmlFormProvider
    {
        /// <summary>
        /// Получить документ с тестовыми данными
        /// для печатной формы HTML
        /// </summary>
        /// <param name="printedHtmlFormDc">Модель печатной формы</param>
        /// <returns>Документ с тестовыми данными</returns>
        public DocumentBuilderResult GetDocumentWithTestDataForPrintedHtmlForm(PrintedHtmlFormDto printedHtmlFormDc)
        {

            var localeName = !printedHtmlFormDc.Id.IsNullOrEmpty()
                ? GetSupplierByPrintedHtmlFormId(printedHtmlFormDc.Id.Value)?.Locale.Name
                : null;
            var supplierName = dbLayer.AccountsRepository.AsQueryable().Include(x => x.AccountConfiguration).ThenInclude(x => x.Supplier).FirstOrDefault(x => x.Id == accessProvider.ContextAccountId)?.AccountConfiguration?.Supplier?.Name;
            var classType = printedHtmlFormDc.ModelType.GetTypeByNameWithNamespace();

            if (classType == null)
                throw new NotFoundException($"Не удалось определить тип модели для печатной формы {printedHtmlFormDc.Name}");

            var instance = (IPrintedHtmlFormModelDto)Activator.CreateInstance(classType)!;
            instance.FillTestData(Configurations.Configurations.CloudConfigurationProvider.Cp.GetSiteCoreUrl(), localeName, supplierName);

            printedHtmlFormDc.HtmlData = Uri.UnescapeDataString(printedHtmlFormDc.HtmlData);
            

            return htmlFormBuilder.Build(instance,
                printedHtmlFormDc, printedHtmlFormDc.Name);
        }

        /// <summary>
        /// Получить поставщика по id печатной формы
        /// </summary>
        /// <param name="printedHtmlFormId">Id печатной формы</param>
        /// <returns>Поставщик</returns>
        private Supplier GetSupplierByPrintedHtmlFormId(Guid printedHtmlFormId) =>
            dbLayer.SupplierRepository.FirstOrDefault(w =>
                w.PrintedHtmlFormInvoiceId == printedHtmlFormId ||
                w.PrintedHtmlFormInvoiceReceiptId == printedHtmlFormId);

    }
}
