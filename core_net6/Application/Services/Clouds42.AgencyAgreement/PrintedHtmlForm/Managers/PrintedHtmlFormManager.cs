﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.PrintedHtmlForm.Managers
{
    /// <summary>
    /// Менеджер для работы с печатными формами
    /// </summary>
    public class PrintedHtmlFormManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IPrintedHtmlFormDataProvider printedHtmlFormDataProvider,
        IHandlerException handlerException,
        IPrintedHtmlFormProvider printedHtmlFormProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить документ с тестовыми данными
        /// для печатной формы HTML
        /// </summary>
        /// <param name="printedHtmlFormDc">Модель печатной формы</param>
        /// <returns>Документ с тестовыми данными</returns>
        public ManagerResult<DocumentBuilderResult> GetDocumentWithTestDataForPrintedHtmlForm(PrintedHtmlFormDto printedHtmlFormDc)
        {
            try
            {
                logger.Info($"Получение документа с тестовыми данными для печатной формы {printedHtmlFormDc.Name}");
                AccessProvider.HasAccess(ObjectAction.GetPrintedHtmlForm, () => AccessProvider.ContextAccountId);
                var data = printedHtmlFormProvider.GetDocumentWithTestDataForPrintedHtmlForm(printedHtmlFormDc);
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения документа с тестовыми данными для печатной формы] {printedHtmlFormDc.Name}");
                return PreconditionFailed<DocumentBuilderResult>(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель печатной формы для создания
        /// </summary>
        /// <returns>Модель печатной формы для создания</returns>
        public ManagerResult<PrintedHtmlFormDto> GetPrintedHtmlFormModelForCreating()
        {
            var accountId = AccessProvider.ContextAccountId;
            try
            {
                logger.Info($"Получение модели печатной формы для создания, для аккаунта {accountId}");
                AccessProvider.HasAccess(ObjectAction.ChangePrintedHtmlForm, () => accountId);
                var data = printedHtmlFormDataProvider.GetPrintedHtmlFormModelForCreating();
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения модели печатной формы для создания] аккаунта {accountId}");
                return PreconditionFailed<PrintedHtmlFormDto>(ex.Message);
            }
        }
    }
}
