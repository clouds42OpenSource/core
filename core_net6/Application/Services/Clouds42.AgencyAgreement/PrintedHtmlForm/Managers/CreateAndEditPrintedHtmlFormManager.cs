﻿using Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.PrintedHtmlForm.Managers
{
    /// <summary>
    /// Менеджер сохранения/изменения печатной формой HTML
    /// </summary>
    public class CreateAndEditPrintedHtmlFormManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateAndEditPrintedHtmlFormProvider createAndEditPrintedHtmlFormProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать/Изменить печатную форму
        /// </summary>
        /// <param name="printedHtmlFormDc">Модель печатной формы</param>
        /// <returns>Id печатной формы</returns>
        public ManagerResult<Guid> CreateOrChangePrintedHtmlForm(PrintedHtmlFormDto printedHtmlFormDc)
        {
            var accountId = AccessProvider.ContextAccountId;
            var action = printedHtmlFormDc.Id.IsNullOrEmpty()
                ? "создана"
                : "изменена";

            try
            {
                var message = $"Печатная форма HTML \"{printedHtmlFormDc.Name}\" {action}";
                logger.Info(message);
                AccessProvider.HasAccess(ObjectAction.ChangePrintedHtmlForm, () => accountId);
                LogEvent(() => accountId, LogActions.ChangePrintedHtmlForm, message);

                var data = createAndEditPrintedHtmlFormProvider.CreateOrChangePrintedHtmlForm(printedHtmlFormDc);

                return Ok(data);
            }
            catch (Exception ex)
            {
                var message =
                    $" [Ошибка Создания/Измения печатной формы HTML] \"{printedHtmlFormDc.Name}\"";
                _handlerException.Handle(ex, message) ;
                LogEvent(() => accountId, LogActions.ChangePrintedHtmlForm, $"{message} Причина: {ex.Message}");


                return PreconditionFailed<Guid>(ex.Message);
            }
        }
    }
}
