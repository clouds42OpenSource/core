﻿using Clouds42.DataContracts.Service.Industry;
using Clouds42.DataContracts.BaseModel;
using PagedList.Core;
using Clouds42.AgencyAgreement.Contracts.Industry;
using Clouds42.AgencyAgreement.Industry.Helpers;
using Clouds42.Common.Constants;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Industry.Providers
{
    /// <summary>
    /// Провайдер для работы с отраслью
    /// </summary>
    public class IndustryProvider(IUnitOfWork dbLayer) : IIndustryProvider
    {
        /// <summary>
        ///     Добавить новую отрасль
        /// </summary>
        /// <param name="newIndustry">Обьект новой отрасли</param>
        /// <returns>Модель свойств после добавления новой отрасли</returns>
        public AddIndustryResultDto AddNewIndustry(AddIndustryDataItemDto newIndustry)
        {
            var oldIndustry = dbLayer.IndustryRepository.FirstOrDefault(w => w.Name == newIndustry.Name);

            if (oldIndustry != null)
            {
                var message = $"Найден дубликат отрасли {newIndustry.Name}";
                throw new InvalidOperationException(message);
            }

            try
            {
                var industry = new Domain.DataModels.Industry
                {
                    Id = Guid.NewGuid(),
                    Name = newIndustry.Name,
                    Description = newIndustry.Description
                };

                dbLayer.IndustryRepository.Insert(industry);
                dbLayer.Save();

                return new AddIndustryResultDto
                {
                    IndustryId = industry.Id
                };
            }
            catch (Exception ex)
            {
                var message = $"Во время добавления отрасли произошла ошибка : {ex.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        ///     Удалить существующую отрасль
        /// </summary>
        /// <param name="id">Id отрасли</param>
        public void DeleteIndustry(Guid id)
        {
            var industry = dbLayer.IndustryRepository.FirstOrDefault(w => w.Id == id);

            if (industry == null)
            {
                var message = $"Для удаления отрасли Id = {id} не найден обьект в базе";
                throw new InvalidOperationException(message);
            }

            try
            {
                dbLayer.IndustryRepository.Delete(industry);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                var message = $"Во время удаления отрасли произошла ошибка : {ex.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        ///     Обновить существующую отрасль
        /// </summary>
        /// <param name="industryToUpdate">Обновленная отрасль</param>
        public void EditExistingIndustry(IIndustry industryToUpdate)
        {
            var industry = dbLayer.IndustryRepository.FirstOrDefault(w => w.Id == industryToUpdate.Id);
            var duplicateIndustryByName = dbLayer.IndustryRepository.FirstOrDefault(w => w.Name == industryToUpdate.Name);
            if (industry == null)
            {
                var message = $"Для редактирования отрасли Id = {industryToUpdate.Id} не найден обьект в базе";
                throw new InvalidOperationException(message);
            }

            if (duplicateIndustryByName != null && industry.Id != duplicateIndustryByName.Id)
            {
                var message = $"Найден дубликат отрасли {industryToUpdate.Name}";
                throw new InvalidOperationException(message);
            }

            try
            {
                industry.Name = industryToUpdate.Name;
                industry.Description = industryToUpdate.Description;

                dbLayer.IndustryRepository.Update(industry);

                dbLayer.Save();
            }
            catch (Exception ex)
            {
                var message = $"Во время редактирования отрасли произошла ошибка : {ex.GetFullInfo()}";
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Получить коллекцию отраслей
        /// </summary>
        /// <param name="args">Параметры поиска получения коллекции отраслей</param>
        /// <returns>Список отраслей с пагинацией</returns>
        public IndustryDataResultDto GetIndustryItems(GetIndustryParamsDto args)
        {
            const int itemsPerPage = GlobalSettingsConstants.ReferenceDataPages.DefaultDataGridRecordsCount;
            var records = new IndustrySelector(dbLayer, args?.Filter).SelectWithFilters();
            var recordsCount = records.Count();
            var pageNumber = PaginationHelper.GetFilterPageNumberBasedOnItemsCount(recordsCount, itemsPerPage,
                    args?.PageNumber ?? 1);
            var pageRecords = IndustrySortingUtility.MakeSorting(records, args?.SortingData)
                .ToPagedList(pageNumber, itemsPerPage);

            var resultRecords = pageRecords.Select(item =>
                new IndustryDataItemDto
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description
                }).ToArray();

            return new IndustryDataResultDto
            {
                Records = resultRecords,
                Pagination = new PaginationBaseDto(pageNumber, recordsCount, itemsPerPage)
            };
        }
    }
}