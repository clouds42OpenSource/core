﻿using Clouds42.AgencyAgreement.Contracts.Industry;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Industry;
using Clouds42.Domain.Access;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AgencyAgreement.Industry
{
    /// <summary>
    /// Менеджер для работы с отраслью
    /// </summary>
    public class IndustryManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IIndustryProvider industryProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Провайдер  для работы с отраслью
        /// </summary>
        private readonly IIndustryProvider _industryProvider = industryProvider;

        /// <summary>
        /// Обработчик ошибок
        /// </summary>
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        ///     Метод добавления новой отрасли
        /// </summary>
        /// <param name="newIndustry">Отрасль</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult<AddIndustryResultDto> AddNewIndustry(AddIndustryDataItemDto newIndustry)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AddNewIndustry, () => AccessProvider.ContextAccountId);
                var result = _industryProvider.AddNewIndustry(newIndustry);
                return Ok(result);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка добавления новой отрасли.]");
                return PreconditionFailed<AddIndustryResultDto>(exception.Message);
            }
        }

        /// <summary>
        ///     Удаление отрасли из базы
        /// </summary>
        /// <param name="id">Id отрасли</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult<bool> DeleteIndustry(Guid id)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DeleteIndustry, () => AccessProvider.ContextAccountId);
                _industryProvider.DeleteIndustry(id);
                return Ok(true);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, $"[Ошибка удаления отрасли {id}.]");
                return PreconditionFailed<bool>(exception.Message);
            }
        }

        /// <summary>
        ///     Метод редактирования отрасли
        /// </summary>
        /// <param name="industry">Обновленная отрасль</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult EditExistingIndustry(IIndustry industry)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.EditIndustry, () => AccessProvider.ContextAccountId);

                _industryProvider.EditExistingIndustry(industry);
                return Ok();
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, $"[Ошибка редактирования отрасли {industry.Id}.]");
                return PreconditionFailed(exception.Message);
            }
        }

        /// <summary>
        ///     Получить пагинированный список отраслей
        /// </summary>
        /// <param name="args">Фильтр</param>
        /// <returns>Пагинированный список отраслей</returns>
        public ManagerResult<IndustryDataResultDto> GetIndustryItems(GetIndustryParamsDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ViewIndustries, () => AccessProvider.ContextAccountId);
                var collection = _industryProvider.GetIndustryItems(args);

                return Ok(collection);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, "[Ошибка получения списка отраслей.]");
                return PreconditionFailed<IndustryDataResultDto>(exception.Message);
            }
        }
    }
}
