﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.Industry;
using Clouds42.Repositories.Interfaces.Common;


namespace Clouds42.AgencyAgreement.Industry.Helpers
{
    /// <summary>
    /// Класс для выбора отраслей
    /// </summary>
    internal sealed class IndustrySelector(IUnitOfWork dbLayer, IndustryFilterParamsDto filter)
    {
        /// <summary>
        /// Параметры фильтрации записей отраслей
        /// </summary>
        private readonly IndustryFilterParamsDto _filter = filter;

        /// <summary>
        /// Дата контекст
        /// </summary>
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Отфильтровать записи по названию отрасли
        /// </summary>
        /// <param name="records">Записи для фильтрации</param>
        /// <returns>Отфильтрованные записи</returns>
        private IQueryable<Domain.DataModels.Industry> FilterByConfigurationName(IQueryable<Domain.DataModels.Industry> records)
            => records.Where(rec =>
                _filter.IndustryName == null ||
                rec.Name != null && rec.Name.Contains(_filter.IndustryName));


        /// <summary>
        /// Получить отфильтрованный список отраслей
        /// </summary>
        /// <returns>Отфильтрованный список отраслей</returns>
        public IQueryable<Domain.DataModels.Industry> SelectWithFilters()
        {
            return _filter == null
            ? _dbLayer.IndustryRepository.WhereLazy()
            : _dbLayer.IndustryRepository.WhereLazy().ComposeFilters(
                FilterByConfigurationName);
        }
    }
}