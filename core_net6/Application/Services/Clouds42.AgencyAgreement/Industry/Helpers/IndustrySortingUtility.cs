﻿using Clouds42.Common;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.AgencyAgreement.Industry.Helpers
{
    /// <summary>
    /// Утилита для сортиовки записей отраслей
    /// </summary>
    public static class IndustrySortingUtility
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateQueryable<Domain.DataModels.Industry>> SortingActions = new()
        {
            { IndustrySortFieldNames.IndustryName, SortByIndustryName }
        };

        /// <summary>
        /// Сортитровать записи отраслей
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи отраслей по названию</returns>
        private static IOrderedQueryable<Domain.DataModels.Industry> SortByDefault(IQueryable<Domain.DataModels.Industry> records)
            => records.OrderByDescending(row => row.Name);


        /// <summary>
        /// Сортитровать выбранные записи отраслей
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записей отраслей по полю Name</returns>
        private static IOrderedQueryable<Domain.DataModels.Industry> SortByIndustryName(IQueryable<Domain.DataModels.Industry> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Name),
                SortType.Desc => records.OrderByDescending(row => row.Name),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Производит сортировку выбранных записей отраслей
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи отраслей</returns>
        public static IOrderedQueryable<Domain.DataModels.Industry> MakeSorting(IQueryable<Domain.DataModels.Industry> records, SortingDataDto sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();

            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate)
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }
    }
}