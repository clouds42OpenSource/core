﻿namespace Clouds42.AgencyAgreement.Industry.Helpers
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки отравслей
    /// </summary>
    public static class IndustrySortFieldNames
    {
        /// <summary>
        ///  Название отрасли
        /// </summary>
        public const string IndustryName = "industryname";
    }
}