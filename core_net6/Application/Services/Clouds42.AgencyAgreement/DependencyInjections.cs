﻿using Clouds42.AgencyAgreement.AgencyAgreement.Managers;
using Clouds42.AgencyAgreement.AgencyAgreement.Providers;
using Clouds42.AgencyAgreement.AgentFileStorage.Providers;
using Clouds42.AgencyAgreement.AgentRequisites.Helpers;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.AgentRequisites.Providers;
using Clouds42.AgencyAgreement.AgentTransferBalanceRequest.Managers;
using Clouds42.AgencyAgreement.AgentTransferBalanceRequest.Providers;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.AgencyAgreement.Contracts.AgentFileStorage;
using Clouds42.AgencyAgreement.Contracts.AgentRequisites;
using Clouds42.AgencyAgreement.Contracts.AgentTransferBalanceRequest;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.Industry;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.AgencyAgreement.Contracts.PrintedHtmlForm;
using Clouds42.AgencyAgreement.DocumentBuilder.Providers;
using Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData;
using Clouds42.AgencyAgreement.Industry;
using Clouds42.AgencyAgreement.Industry.Providers;
using Clouds42.AgencyAgreement.Partner;
using Clouds42.AgencyAgreement.Partner.Helpers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Providers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices.Selectors;
using Clouds42.AgencyAgreement.Partner.Providers;
using Clouds42.AgencyAgreement.PrintedHtmlForm.Managers;
using Clouds42.AgencyAgreement.PrintedHtmlForm.Providers;
using Microsoft.Extensions.DependencyInjection;
using Wkhtmltopdf.NetCore;

namespace Clouds42.AgencyAgreement
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddAgencyAgreement(this IServiceCollection services, bool isWorker = false)
        {
            services.AddTransient<ICreateAgencyAgreementProvider, CreateAgencyAgreementProvider>();
            services.AddTransient<IAgencyAgreementDataProvider, AgencyAgreementDataProvider>();
            services.AddTransient<IPrintAgencyAgreementProvider, PrintAgencyAgreementProvider>();
            services.AddTransient<IApplyAgencyAgreementProvider, ApplyAgencyAgreementProvider>();
            services.AddTransient<IPartnerBillingServiceDataProvider, PartnerBillingServiceDataProvider>();
            services.AddTransient<IPartnerBillingServiceTypeDataProvider, PartnerBillingServiceTypeDataProvider>();
            services.AddTransient<ICreateAgentCashOutRequestProvider, CreateAgentCashOutRequestProvider>();
            services.AddTransient<IEditAgentCashOutRequestProvider, EditAgentCashOutRequestProvider>();
            services.AddTransient<IRemoveAgentCashOutRequestProvider, RemoveAgentCashOutRequestProvider>();
            services.AddTransient<IAgentCashOutRequestProvider, AgentCashOutRequestProvider>();
            services.AddTransient<IPartnerClientsProvider, PartnerClientsProvider>();
            services.AddTransient<ICreateAgentPaymentProvider, CreateAgentPaymentProvider>();
            services.AddTransient<IAgentDocumentFileProvider, AgentDocumentFileProvider>();
            services.AddTransient<IAgentCashOutRequestDataProvider, AgentCashOutRequestDataProvider>();
            services.AddTransient<IChangeAgentCashOutRequestStatusProvider, ChangeAgentCashOutRequestStatusProvider>();
            services.AddTransient<PartnerServiceActivityDataSelector>();
            services.AddTransient<AgencyFileHelper>();
            services.AddTransient<AgentReportBuildHelper>();
            services.AddTransient<PrintedHtmlFormBuilder>();
            services.AddTransient<IAgencyAgreementDataProvider, AgencyAgreementDataProvider>();
            services.AddTransient<IPartnerServiceActivityDataSelector, PartnerServiceActivityDataSelector>();
            services.AddTransient<IPrintedHtmlFormDataProvider, PrintedHtmlFormDataProvider>();
            services.AddTransient<ICreateAndEditPrintedHtmlFormProvider, CreateAndEditPrintedHtmlFormProvider>();
            services.AddTransient<IPrintedHtmlFormProvider, PrintedHtmlFormProvider>();
            services.AddTransient<IAgentFileStorageProvider, AgentFileStorageProvider>();
            services.AddTransient<IAgentRequisitesProvider, AgentRequisitesProvider>();
            services.AddTransient<IAgentRequisitesDataProvider, AgentRequisitesDataProvider>();
            services.AddTransient<IRemoveAgentRequisitesProvider, RemoveAgentRequisitesProvider>();
            services.AddTransient<IEditAgentRequisitesProvider, EditAgentRequisitesProvider>();
            services.AddTransient<ICreateAgentRequisitesProvider, CreateAgentRequisitesProvider>();
            services.AddTransient<IIndustryProvider, IndustryProvider>();

            if (!isWorker)
            {
                services.AddTransient<IHtmlToPdfProvider, WkHtmlToPdfProvider>();
            }
            else
            {
                services.AddTransient<IHtmlToPdfProvider, SelectPdfHtmlToPdfProvider>();
            }

            services.AddTransient<IInvoiceDocumentBuilder, InvoiceDocumentBuilder>();
            services.AddTransient<IPartnerDataProvider, PartnerDataProvider>();
            services.AddTransient<ICreateAgentTransferBalanceRequestProvider, CreateAgentTransferBalanceRequestProvider>();
            services.AddTransient<IInvoiceReceiptBuilderFactory, InvoiceReceiptBuilderFactory>();
            services.AddTransient<AgentPaymentManager>();
            services.AddTransient<CreateAgentDocumentNotificationProcessor>();
            services.AddTransient<AgentCashOutRequestDataManager>();
            services.AddTransient<PartnerBillingServicesDataManager>();
            services.AddTransient<ChangeAgentCashOutRequestStatusManager>();
            services.AddTransient<PrintedHtmlFormManager>();
            services.AddTransient<PartnerManager>();
            services.AddTransient<AgencyAgreementDataManager>();
            services.AddTransient<AgentCashOutRequestManager>();
            services.AddTransient<AgentDocumentFileManager>();
            services.AddTransient<AgentRequisitesManager>();
            services.AddTransient<CreateAgentCashOutRequestManager>();
            services.AddTransient<CreateAgentRequisitesManager>();
            services.AddTransient<CreateAgentTransferBalanceRequestManager>();
            services.AddTransient<IndustryManager>();
            services.AddTransient<CreateAndEditPrintedHtmlFormManager>();
            services.AddTransient<PartnerClientsManager>();
            services.AddTransient<CreateAgencyAgreementManager>();
            services.AddTransient<EditAgentCashOutRequestManager>();
            services.AddTransient<PrintAgencyAgreementManager>();
            services.AddTransient<RemoveAgentRequisitesManager>();
            services.AddTransient<ApplyAgencyAgreementManager>();
            services.AddTransient<EditAgentRequisitesManager>();

            if (!isWorker)
            {
                services.AddWkhtmltopdf();
            }

            return services;
        }
    }
}
