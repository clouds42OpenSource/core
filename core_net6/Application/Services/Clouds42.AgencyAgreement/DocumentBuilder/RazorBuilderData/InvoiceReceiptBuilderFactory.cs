﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder.Enums;
using Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData.Helpers;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData
{
    /// <summary>
    /// Фабрика создания фискальных чеков
    /// </summary>
    public class InvoiceReceiptBuilderFactory  : IInvoiceReceiptBuilderFactory
    {
        private readonly PrintedHtmlFormBuilder _printedHtmlFormBuilder;
        private readonly Dictionary<string, Func<string, string>> _titles;

        public InvoiceReceiptBuilderFactory(PrintedHtmlFormBuilder printedHtmlFormBuilder)
        {
            _printedHtmlFormBuilder = printedHtmlFormBuilder;
            _titles = new Dictionary<string, Func<string, string>>
            {
                {LocaleConst.Russia, uniq => $"Фискальный чек №{uniq}"},
                {LocaleConst.Ukraine, uniq => $"Фіскальний чек №{uniq}"},
                {LocaleConst.Kazakhstan, uniq => $"Фискалдық түбіртек №{uniq}"},
                {LocaleConst.Uzbekistan, uniq => $"Fiskal tekshirish №{uniq}"}
            };
        }

        /// <summary>
        /// Построить чек
        /// </summary>
        /// <param name="receipt">Фискальный чек</param>
        public DocumentBuilderResult Build(InvoiceReceiptDto receipt)
        {
            if (!_titles.ContainsKey(receipt.LocaleName))
                throw new NotFoundException($"Не найден заголовок для постройки фискального чека {receipt.InvoiceId} по локали {receipt.LocaleName}");

            var builderModel = new InvoiceReceiptHelper().ToBuilderModel(receipt);
            var title = _titles[receipt.LocaleName](receipt.FiscalData.FiscalNumber);

            return _printedHtmlFormBuilder.Build(builderModel,
                new PrintedHtmlFormDto
                {
                    HtmlData = receipt.SupplierRazorTemplate.RazorData,
                    Files = receipt.SupplierRazorTemplate.Attachments
                }, title, pdfPageSize: PdfPageSizeEnum.A6);
        }
    }
}
