﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder.Enums;
using Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData.Helpers;
using Clouds42.Common.PdfDocuments;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.Logger;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData
{
    /// <summary>
    /// Билдер печатной формы html
    /// </summary>
    public class PrintedHtmlFormBuilder(IHtmlToPdfProvider htmlToPdfProvider, ILogger42 logger)
    {
        /// <summary>
        /// Сгенерировать PDF документ
        /// </summary>
        /// <param name="model">Модель данных</param>        
        /// <param name="printedHtmlForm">Печатная форма Html</param>
        /// <param name="title">Заголовок документа</param>
        /// <param name="documentParams">Параметры pdf документа</param>
        /// <param name="pdfPageSize">Формат PDF</param>
        public DocumentBuilderResult Build<T>(T model, PrintedHtmlFormDto printedHtmlForm, string title,
            PdfDocumentParams? documentParams = null, PdfPageSizeEnum pdfPageSize = PdfPageSizeEnum.A4) where T : IPrintedHtmlFormModelDto
        {
            try
            {
                var config = new TemplateServiceConfiguration
                {
                    Debug = false,
                    DisableTempFileLocking = true,
                    CachingProvider = new DefaultCachingProvider(DeleteFileWithRetries)
                };

                Engine.Razor = RazorEngineService.Create(config);

                var razorData = RazorPrepareHelper.PrepareData(printedHtmlForm);

                string result = Engine.Razor.RunCompile(razorData, Guid.NewGuid().ToString(), model.GetType(), model);

                return new DocumentBuilderResult(
                    htmlToPdfProvider.GeneratePdf(result, title, pdfPageSize, documentParams), $"{title}.pdf");
            }

            catch (Exception)
            {
                if (printedHtmlForm.HtmlData.Contains("@functions"))
                {
                    printedHtmlForm.HtmlData = printedHtmlForm.HtmlData.Replace("functions ", "");
                    printedHtmlForm.HtmlData = printedHtmlForm.HtmlData.Replace("private", "");
                }
                var razorData = RazorPrepareHelper.PrepareData(printedHtmlForm);

                string result = Engine.Razor.RunCompile(razorData, Guid.NewGuid().ToString(), model.GetType(), model);

                return new DocumentBuilderResult(
                    htmlToPdfProvider.GeneratePdf(result, title, pdfPageSize, documentParams), $"{title}.pdf");
            }
        }

        private void DeleteFileWithRetries(string filePath)
        {
            const int maxRetries = 3;
            const int delayBetweenRetries = 1000;

            for (int retry = 0; retry < maxRetries; retry++)
            {
                try
                {
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }
                    break; // Exit if file is deleted successfully
                }
                catch (UnauthorizedAccessException ex)
                {
                    logger.Error(ex, $"Access denied to file {filePath}: {ex.Message}");
                }
                catch (IOException ex)
                {
                    logger.Error(ex, $"IO Exception when deleting file {filePath}: {ex.Message}");
                }

                Task.Delay(delayBetweenRetries).Wait();
            }
        }
    }
}
