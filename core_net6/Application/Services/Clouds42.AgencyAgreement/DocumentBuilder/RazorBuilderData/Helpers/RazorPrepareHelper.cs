﻿using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData.Helpers
{
    /// <summary>
    /// Хелпер для подготовки шаблона
    /// </summary>
    public static class RazorPrepareHelper
    {
        /// <summary>
        /// Подготовить разметку шаблона
        /// </summary>
        /// <param name="printedHtmlForm">Печатная форма Html</param>
        public static string PrepareData(PrintedHtmlFormDto printedHtmlForm)
        {
            return printedHtmlForm.Files.Aggregate(printedHtmlForm.HtmlData, (current, attachment) => current.Replace($"@@{System.Web.HttpUtility.UrlEncode(attachment.FileName)}@@", $"data:{attachment.ContentType};base64,{attachment.Base64}"));
        }
    }
}
