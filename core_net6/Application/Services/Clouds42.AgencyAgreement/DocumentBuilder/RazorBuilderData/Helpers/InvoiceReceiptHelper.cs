﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData.Helpers
{
    /// <summary>
    ///     Хелпер для чеков счета
    /// </summary>
    internal class InvoiceReceiptHelper
    {
        /// <summary>
        ///     Получить модель для создания PDF
        /// </summary>
        /// <param name="receipt">Фискальный чек для счета</param>
        public InvoiceReceiptDocumentBuilderModel ToBuilderModel(InvoiceReceiptDto receipt)
        {
            var viewModel = new InvoiceReceiptDocumentBuilderModel
            {
                QrFormat = receipt.FiscalData.QrCodeFormat,
                QrData = receipt.FiscalData.QrCodeData,
                Rows = receipt.FiscalData.ReceiptInfo.Replace("\\n", "\n").Split('\n').Select(ParseReceiptLine).ToList()
            };

            return viewModel;
        }

        /// <summary>
        ///     Парсинг чека в понятную модель
        /// </summary>
        /// <param name="line">Строка в чеке</param>
        private InvoiceReceiptDocumentBuilderModelRowDto ParseReceiptLine(string line)
        {
            var splitted = line.Replace("\\t", "\t").Split('\t');
            if (splitted.Length <= 1)
                return new InvoiceReceiptDocumentBuilderModelRowDto(line, string.Empty);

            return new InvoiceReceiptDocumentBuilderModelRowDto(splitted[0], splitted[1]);
        }
    }
}