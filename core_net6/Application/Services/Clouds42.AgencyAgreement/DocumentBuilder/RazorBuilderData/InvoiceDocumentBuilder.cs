﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData
{
    /// <summary>
    /// Фабрика генерации счетов на оплату
    /// </summary>
    public class InvoiceDocumentBuilder : IInvoiceDocumentBuilder
    {
        private readonly PrintedHtmlFormBuilder _printedHtmlFormBuilder;
        private readonly Dictionary<string, Func<string, string>> _titles;

        public InvoiceDocumentBuilder(PrintedHtmlFormBuilder printedHtmlFormBuilder)
        {
            _printedHtmlFormBuilder = printedHtmlFormBuilder;
            _titles = new Dictionary<string, Func<string, string>>
            {
                {LocaleConst.Russia, uniq => $"Счет на оплату №{uniq}"},
                {LocaleConst.Ukraine, uniq => $"Рахунок на оплату №{uniq}"},
                {LocaleConst.Kazakhstan, uniq => $"Счет на оплату №{uniq}"},
                {LocaleConst.Uzbekistan, uniq => $"Счет на оплату №{uniq}"},
            };
        }

        /// <summary>
        /// Сгенерировать PDF документ для счета на оплату
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        public DocumentBuilderResult Build(InvoiceDc invoice)
        {
            if (!_titles.ContainsKey(invoice.Locale.Name))
                throw new NotFoundException($"Не найден заголовок для постройки счета {invoice.Id} по локали {invoice.Locale.Name}");

            var title = _titles[invoice.Locale.Name](invoice.Uniq);

            return _printedHtmlFormBuilder.Build(invoice,
                new PrintedHtmlFormDto
                {
                    HtmlData = invoice.SupplierRazorTemplate.RazorData,
                    Files = invoice.SupplierRazorTemplate.Attachments
                }, title);
        }
    }
}
