﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder.Enums;
using Clouds42.Common.Extensions;
using Clouds42.Common.PdfDocuments;
using Wkhtmltopdf.NetCore;
using Wkhtmltopdf.NetCore.Options;

namespace Clouds42.AgencyAgreement.DocumentBuilder.Providers
{
    public class WkHtmlToPdfProvider(IGeneratePdf generatePdf) : IHtmlToPdfProvider
    {
        /// <summary>
        /// Дикшенари форматов PDF
        /// </summary>
        private readonly Dictionary<PdfPageSizeEnum, Size> _formats = new()
        {
            {PdfPageSizeEnum.A4, Size.A4},
            {PdfPageSizeEnum.A5, Size.A5},
            {PdfPageSizeEnum.A6, Size.A6},
            {PdfPageSizeEnum.A3, Size.A3},
            {PdfPageSizeEnum.Letter, Size.Letter}
        };

        public byte[] GeneratePdf(string htmlContent, string uniqTitle, PdfPageSizeEnum pdfPageSize,
            PdfDocumentParams? documentParams = null)
        {
            try
            {
                if (!_formats.TryGetValue(pdfPageSize, out var format))
                    format = Size.A4;

                generatePdf.SetConvertOptions(new ConvertOptions
                {
                    PageMargins = documentParams != null ?  new Margins(documentParams.MarginTop, documentParams.MarginRight, documentParams.MarginBottom, documentParams.MarginLeft) : null,
                    PageSize = format,
                    Replacements = new Dictionary<string, string> { {"[title]", uniqTitle}, { "[doctitle]", uniqTitle } }
                });
                htmlContent = htmlContent.Replace("<head>", $"<head><title>{uniqTitle}</title>");
                var pdfBytes = generatePdf.GetPDF(htmlContent);

                return pdfBytes;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    $"Ошибка генерации пдф на основание html: '{htmlContent}' :: Error message :: {ex.GetFullInfo(needStackTrace: true)}");
            }
        }
    }
}
