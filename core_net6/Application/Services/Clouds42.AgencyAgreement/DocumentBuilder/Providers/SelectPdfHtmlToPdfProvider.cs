﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder.Enums;
using Clouds42.Common.Extensions;
using Clouds42.Common.PdfDocuments;
namespace Clouds42.AgencyAgreement.DocumentBuilder.Providers
{
    /// <summary>
    /// Провайдер для преобразования Html в Pdf
    /// </summary>
    public class SelectPdfHtmlToPdfProvider : IHtmlToPdfProvider
    {
        /// <summary>
        /// Сгенерировать Pdf документ
        /// </summary>
        /// <param name="htmlContent">Html контент</param>
        /// <param name="uniqTitle">Заголовок</param>
        /// <param name="pdfPageSize">Размер страницы Pdf</param>
        /// <param name="documentParams">Параметры документа</param>
        /// <returns>Pdf документ</returns>
        public byte[] GeneratePdf(string htmlContent, string uniqTitle, PdfPageSizeEnum pdfPageSize, PdfDocumentParams? documentParams = null)
        {
            try
            {
                var doc = new SelectPdf.HtmlToPdf().ConvertHtmlString(htmlContent);
                doc.DocumentInformation.Title = uniqTitle;
                doc.ViewerPreferences.DisplayDocTitle = true;

                using var ms = new MemoryStream();
                doc.Save(ms);
                var array = ms.ToArray();
                doc.Close();
                return array;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    $"Ошибка генерации пдф на основание html: '{htmlContent}' :: Error message :: {ex.GetFullInfo(needStackTrace: false)}");
            }
        }
    }
}
