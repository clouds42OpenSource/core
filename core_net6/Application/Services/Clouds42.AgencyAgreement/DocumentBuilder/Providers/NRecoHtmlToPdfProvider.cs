﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder.Enums;
using Clouds42.Common.Extensions;
using Clouds42.Common.PdfDocuments;
using NReco.PdfGenerator;

namespace Clouds42.AgencyAgreement.DocumentBuilder.Providers
{
    /// <summary>
    /// Провайдер для преобразования Html в Pdf
    /// </summary>
    public class NRecoHtmlToPdfProvider : IHtmlToPdfProvider
    {

        /// <summary>
        /// Дикшенари форматов PDF
        /// </summary>
        private readonly Dictionary<PdfPageSizeEnum, PageSize> _formats = new()
        {
            {PdfPageSizeEnum.A4, PageSize.A4},
            {PdfPageSizeEnum.A3, PageSize.A3},
            {PdfPageSizeEnum.Letter, PageSize.Letter}
        };

        /// <summary>
        /// Сгенерировать Pdf документ
        /// </summary>
        /// <param name="htmlContent">Html контент</param>
        /// <param name="uniqTitle">Заголовок</param>
        /// <param name="pdfPageSize">Размер страницы Pdf</param>
        /// <param name="documentParams">Параметры документа</param>
        /// <returns>Pdf документ</returns>
        public byte[] GeneratePdf(string htmlContent, string uniqTitle, PdfPageSizeEnum pdfPageSize, PdfDocumentParams? documentParams = null)
        {
            try
            {
                if (!_formats.TryGetValue(pdfPageSize, out var format))
                    format = PageSize.Default;

                var htmlToPdf = new HtmlToPdfConverter
                {
                    Size = format,
                    TocHeaderText = uniqTitle
                };
                if (documentParams != null)
                {
                    htmlToPdf.Margins = new PageMargins
                    {
                        Bottom = documentParams.MarginBottom,
                        Right = documentParams.MarginRight,
                        Left = documentParams.MarginLeft,
                        Top = documentParams.MarginTop
                    };
                }

                var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);

                return pdfBytes;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    $"Ошибка генерации пдф на основание html: '{htmlContent}' :: Error message :: {ex.GetFullInfo(needStackTrace: false)}");
            }
        }
    }
}
