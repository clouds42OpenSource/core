﻿using Clouds42.SmtpClient.Infrastructure;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using Clouds42.Configurations.Configurations;

namespace Clouds42.SmtpClient
{
    /// <summary>
    ///  Менеджер отправки сообщений через сервис SendGrid
    /// </summary>
    public class SendGridSmtpClientManager : ISmtpClientManager
    {
        private readonly Lazy<string> _sendGridApiKey = new(CloudConfigurationProvider.SendGrid.GetApiKey);

        /// <summary>
        ///     Отправить сообщение
        /// </summary>
        /// <param name="credentials">Данные авторизации</param>
        /// <param name="mailMessage">Сообщение</param>
        public async Task SendMail(SmtpClientCredentials credentials, SmtpClientMessage mailMessage)
        {
            var emailFrom = !string.IsNullOrEmpty(mailMessage.DisplayName)
                ? new EmailAddress(credentials.Login, mailMessage.DisplayName)
                : new EmailAddress(credentials.Login);

            await SendEmailAsync(emailFrom, mailMessage);
        }

        /// <summary>
        ///     Отправить сообщение асинхронно
        /// </summary>
        /// <param name="emailFrom">Данные отправителя</param>
        /// <param name="mailMessage">Сообщение</param>
        private async Task SendEmailAsync(EmailAddress emailFrom, SmtpClientMessage mailMessage)
        {
            var sendGridMessage = new SendGridMessage
            {
                From = emailFrom,
                Subject = mailMessage.Subject,
                HtmlContent = mailMessage.Body,
                PlainTextContent = GetMessageBodyTextAlternateView(mailMessage)
            };
            sendGridMessage.AddTo(new EmailAddress(mailMessage.To));

            if (mailMessage.Attachments != null)
                foreach (var attachment in mailMessage.Attachments)
                    await sendGridMessage.AddAttachmentAsync(attachment.Key, new MemoryStream(attachment.Value));

            if (mailMessage.Copies != null)
                foreach (var copyAddress in mailMessage.Copies)
                    sendGridMessage.AddCc(new EmailAddress(copyAddress));

            var transportWeb = new SendGridClient(_sendGridApiKey.Value);
            await transportWeb.SendEmailAsync(sendGridMessage);
        }

        /// <summary>
        /// Получить альтернативное тело письма в виде текста
        /// </summary>
        /// <param name="mailMessage">Сообщение</param>
        /// <returns>Альтернативное тело письма в виде текста</returns>
        private static string GetMessageBodyTextAlternateView(SmtpClientMessage mailMessage)
        {
            var textAlternateBody = string.Empty;

            if (string.IsNullOrEmpty(mailMessage.Body))
                return textAlternateBody;

            var textAlternateView = AlternateView.CreateAlternateViewFromString(WebUtility.HtmlDecode(mailMessage.Body),
                null, MediaTypeNames.Text.Plain);

            using var reader = new StreamReader(textAlternateView.ContentStream);
            textAlternateBody = reader.ReadToEnd();

            return textAlternateBody;
        }
    }
}
