﻿using System.Threading.Tasks;
using Clouds42.SmtpClient.Infrastructure;

namespace Clouds42.SmtpClient
{
    /// <summary>
    ///     Интерфейс отправки сообщения
    /// </summary>
    public interface ISmtpClientManager
    {
        /// <summary>
        ///     Отправить сообщение
        /// </summary>
        /// <param name="credentials">Данные авторизации</param>
        /// <param name="mailMessage">Сообщение</param>
        Task SendMail(SmtpClientCredentials credentials, SmtpClientMessage mailMessage);
    }
}
