﻿namespace Clouds42.SmtpClient.Infrastructure
{
    /// <summary>
    ///     Данные авторизации к почтовой службе
    /// </summary>
    public class SmtpClientCredentials
    {
        /// <summary>
        ///     Порт
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        ///     Хост
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        ///     Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        ///     Пароль
        /// </summary>
        public string Password { get; set; }
    }
}