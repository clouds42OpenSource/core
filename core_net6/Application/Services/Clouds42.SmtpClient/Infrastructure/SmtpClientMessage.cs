﻿using System.Collections.Generic;

namespace Clouds42.SmtpClient.Infrastructure
{
    /// <summary>
    ///     Модель для сообщения
    /// </summary>
    public class SmtpClientMessage
    {
        /// <summary>
        ///     Кому
        /// </summary>
        public string To { get; set; }

        /// <summary>
        ///     Копия
        /// </summary>
        public List<string> Copies { get; set; } = [];

        /// <summary>
        ///     Тема письма
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        ///     Тело письма
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        ///     Отображаемое название отправителя
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        ///     Прикрепленные файлы
        /// </summary>
        public Dictionary<string, byte[]> Attachments { get; set; } = new();
    }
}