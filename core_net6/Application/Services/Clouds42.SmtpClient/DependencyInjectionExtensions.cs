﻿using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.SmtpClient
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddSMTP(this IServiceCollection services)
        {
            services
                .AddTransient<ISmtpClientManager, SendGridSmtpClientManager>();

            return services;
        }
    }
}
