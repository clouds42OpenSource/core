﻿using Clouds42.Domain.Enums;

namespace Clouds42.CloudLogging.Contracts.Dtos
{
    public class LogEventDto
    {
        public Guid AccountId { get; set; }
        public LogActions Log  { get; set; }
        public string Description  { get; set; }
    }
}
