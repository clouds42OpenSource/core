﻿namespace Clouds42.CloudLogging
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки логирования запуска баз и RDP
    /// </summary>
    public static class LaunchDbAndRdbLogSortFieldNames
    {
        /// <summary>
        /// Наименование поля номер аккаунта
        /// </summary>
        public const string AccountNumber = "accountnumber";

        /// <summary>
        /// Наименование поля создания записи логирования
        /// </summary>
        public const string ActionCreated = "actioncreated";

        /// <summary>
        /// Наименование поля действия при логировании
        /// </summary>
        public const string Action = "action";

        /// <summary>
        /// Наименование поля логина, кто инициировал запись
        /// </summary>
        public const string Login = "login";

        /// <summary>
        /// Наименование поля о версии Линка
        /// </summary>
        public const string LinkAppVersion = "linkappversion";

        /// <summary>
        /// Наименование поля запуска базы данных
        /// </summary>
        public const string V82Name = "v82name";

        /// <summary>
        /// Наименование поля о типе Линка
        /// </summary>
        public const string LinkAppType = "linkapptype";

        /// <summary>
        /// Наименование поля о типе запуска
        /// </summary>
        public const string LaunchType = "launchtype";

        /// <summary>
        /// Наименование поля о внешнем IP адресе
        /// </summary>
        public const string ExternalIpAddress = "externalipaddress";

        /// <summary>
        /// Наименование поля о внутреннем IP адресе
        /// </summary>
        public const string InternalIpAddress = "internalipaddress";
    }
}