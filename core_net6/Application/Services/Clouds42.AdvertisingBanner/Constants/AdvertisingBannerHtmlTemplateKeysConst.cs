﻿namespace Clouds42.AdvertisingBanner.Constants
{
    /// <summary>
    /// Ключи для Html шаблона рекламного баннера (для замены значений)
    /// </summary>
    public static class AdvertisingBannerHtmlTemplateKeysConst
    {
        /// <summary>
        /// Изображение баннера
        /// </summary>
        public const string Image = "@Image";

        /// <summary>
        /// Заголовок баннера
        /// </summary>
        public const string Header = "@Header";

        /// <summary>
        /// Тело баннера
        /// </summary>
        public const string Body = "@Body";

        /// <summary>
        /// Ссылка баннера
        /// </summary>
        public const string Link = "@Link";

        /// <summary>
        /// Описание ссылки
        /// </summary>
        public const string CaptionLink = "@CaptionLink";
    }
}
