﻿using Clouds42.AdvertisingBanner.Constants;
using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Locales.Contracts.Interfaces;

namespace Clouds42.AdvertisingBanner.Builder
{
    /// <summary>
    /// Билдер Html рекламного баннера
    /// </summary>
    internal class AdvertisingBannerHtmlBuilder(ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : IAdvertisingBannerHtmlBuilder
    {
        private readonly Lazy<string> _routeForGettingAdvertisingBannerImage = new(CloudConfigurationProvider.Cp.GetRouteForGettingAdvertisingBannerImage);

        /// <summary>
        /// Сгенерировать Html для рекламного баннера
        /// </summary>
        /// <param name="template">Шаблон рекламного баннера</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Html рекламного баннера</returns>
        public string Build(AdvertisingBannerTemplate template, Guid accountId) => template
            .HtmlTemplate.HtmlData.Replace(AdvertisingBannerHtmlTemplateKeysConst.Body, template.Body)
            .Replace(AdvertisingBannerHtmlTemplateKeysConst.Header, template.Header)
            .Replace(AdvertisingBannerHtmlTemplateKeysConst.Link, template.Link)
            .Replace(AdvertisingBannerHtmlTemplateKeysConst.CaptionLink, template.CaptionLink)
            .Replace(AdvertisingBannerHtmlTemplateKeysConst.Image,
                GetUrlForGettingAdvertisingBannerImage(template.Id, accountId));

        /// <summary>
        /// Получить ссылку для получения изображения рекламного баннера
        /// </summary>
        /// <param name="bannerId">Id рекламного баннера</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Ссылка для получения изображения рекламного баннера</returns>
        private string GetUrlForGettingAdvertisingBannerImage(int bannerId, Guid accountId) =>
            new Uri(
                    $"{localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountId)}/{_routeForGettingAdvertisingBannerImage.Value}{bannerId}&key={Guid.NewGuid():N}")
                .AbsoluteUri;
    }
}