﻿using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;
using Clouds42.Domain.DataModels.Marketing;

namespace Clouds42.AdvertisingBanner.Mappers
{
    /// <summary>
    /// Маппер моделей аудитории рекламного баннера
    /// </summary>
    public static class AdvertisingBannerAudienceMapper
    {
        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="advertisingBannerAudience">Аудитория рекламного баннера</param>
        /// <returns>Модель аудитории рекламного баннера</returns>
        public static AdvertisingBannerAudienceDto MapToAdvertisingBannerAudienceDto(
            this AdvertisingBannerAudience advertisingBannerAudience) =>
            new()
            {
                AccountType = advertisingBannerAudience.AccountType,
                AvailabilityPayment = advertisingBannerAudience.AvailabilityPayment,
                Locales = advertisingBannerAudience.AdvertisingBannerAudienceLocaleRelations.Select(lr => lr.LocaleId)
                    .ToList(),
                RentalServiceState = advertisingBannerAudience.RentalServiceState
            };
    }
}