﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;

namespace Clouds42.AdvertisingBanner.Mappers
{
    /// <summary>
    /// Маппер моделей изображения
    /// </summary>
    public static class FileImageMapper
    {
        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <typeparam name="T">Тип контента</typeparam>
        /// <param name="cloudFile">Файл облака</param>
        /// <returns>Модель файла облака</returns>
        public static FileDataDto<T> MapToFileDataDto<T>(this CloudFile cloudFile) =>
            new()
            {
                FileName = cloudFile.FileName,
                ContentType = cloudFile.ContentType,
                Base64 = Convert.ToBase64String(cloudFile.Content)
            };
    }
}