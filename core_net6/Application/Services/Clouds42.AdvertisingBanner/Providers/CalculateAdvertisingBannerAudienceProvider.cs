﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для подсчета аудитории рекламного баннера
    /// </summary>
    internal class CalculateAdvertisingBannerAudienceProvider(
        IAdvertisingBannerAudienceProvider advertisingBannerAudienceProvider)
        : ICalculateAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Посчитать аудиторию рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerAudience">Модель аудитории рекламного баннера</param>
        /// <returns>Результат подсчета аудитории рекламного баннера</returns>
        public CalculationAdvertisingBannerAudienceDto Calculate(
            AdvertisingBannerAudienceDto advertisingBannerAudience)
        {
            var data =
                (from account in advertisingBannerAudienceProvider.SelectAdvertisingBannerAudience(
                        advertisingBannerAudience.Locales, advertisingBannerAudience.AccountType, advertisingBannerAudience.AvailabilityPayment, advertisingBannerAudience.RentalServiceState)
                    select new
                    {
                        accountId = account.Id,
                        accountUserCount = account.AccountUsers.Count(au => au.Activated)
                    }).ToList();

            return new CalculationAdvertisingBannerAudienceDto
            {
                AccountsCount = data.Count,
                AccountUsersCount = data.Sum(d => d.accountUserCount)
            };
        }
    }
}
