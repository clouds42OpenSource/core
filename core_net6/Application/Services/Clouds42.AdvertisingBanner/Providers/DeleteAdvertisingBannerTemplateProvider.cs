﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Mailing;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для удаления рекламного баннера
    /// </summary>
    internal class DeleteAdvertisingBannerTemplateProvider(
        IUnitOfWork dbLayer,
        IDeleteAdvertisingBannerAudienceProvider deleteAdvertisingBannerAudienceProvider,
        IAdvertisingBannerTemplateProvider advertisingBannerTemplateProvider,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IDeleteAdvertisingBannerTemplateProvider
    {
        /// <summary>
        /// Удалить рекламный баннер
        /// </summary>
        /// <param name="bannerTemplateId">Id рекламного баннера</param>
        public void Delete(int bannerTemplateId)
        {
            var bannerTemplate = advertisingBannerTemplateProvider.GetAdvertisingBannerTemplate(bannerTemplateId);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                DeleteLetterAdvertisingBannerRelations(bannerTemplateId);

                dbLayer.GetGenericRepository<AdvertisingBannerTemplate>()
                    .Delete(bannerTemplate);

                DeleteAdvertisingBannerImage(bannerTemplate.Image);
                deleteAdvertisingBannerAudienceProvider.Delete(bannerTemplate.AdvertisingBannerAudienceId);
                dbLayer.Save();

                dbScope.Commit();

                LogEventHelper.LogEvent(dbLayer, accessProvider.ContextAccountId, accessProvider,
                    LogActions.DeleteAdvertisingBannerTemplate,
                    $"Рекламный баннер для писем “{bannerTemplate.Header}” удален");
            }
            catch (Exception ex)
            {
                var message =
                    $"Удаление рекламного баннера '{bannerTemplate.Header}' завершилось с ошибкой: {ex.Message}";

                handlerException.Handle(ex, $"[Ошибка удаления рекламного баннера] {bannerTemplate.Header}");

                LogEventHelper.LogEvent(dbLayer, accessProvider.ContextAccountId, accessProvider,
                    LogActions.DeleteAdvertisingBannerTemplate, message);

                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Удалить изображение рекламного баннера
        /// </summary>
        /// <param name="cloudFile">Изображение рекламного баннера</param>
        private void DeleteAdvertisingBannerImage(CloudFile cloudFile)
        {
            dbLayer.CloudFileRepository.Delete(cloudFile);
            dbLayer.Save();
        }

        /// <summary>
        /// Удалить связи шаблонов писем с рекламным баннером
        /// </summary>
        /// <param name="bannerTemplateId">Id рекламного баннера</param>
        private void DeleteLetterAdvertisingBannerRelations(int bannerTemplateId)
        {
            var advertisingBannerRelations = dbLayer.GetGenericRepository<LetterAdvertisingBannerRelation>()
                .WhereLazy(br => br.AdvertisingBannerTemplateId == bannerTemplateId).ToList();

            if (!advertisingBannerRelations.Any())
                return;

            dbLayer.GetGenericRepository<LetterAdvertisingBannerRelation>().DeleteRange(advertisingBannerRelations);
            dbLayer.Save();
        }
    }
}
