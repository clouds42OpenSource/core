﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для создания аудитории рекламного баннера
    /// </summary>
    internal class CreateAdvertisingBannerAudienceProvider(IUnitOfWork dbLayer, ILogger42 logger)
        : BaseOperationAdvertisingBannerAudienceProvider(dbLayer), ICreateAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Создать аудиторию рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerAudienceDto">Модель аудитории рекламного баннера</param>
        /// <returns>Id созданной аудитории</returns>
        public int Create(AdvertisingBannerAudienceDto advertisingBannerAudienceDto)
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var advertisingBannerAudience = new AdvertisingBannerAudience
                {
                    AccountType = advertisingBannerAudienceDto.AccountType,
                    AvailabilityPayment = advertisingBannerAudienceDto.AvailabilityPayment,
                    RentalServiceState = advertisingBannerAudienceDto.RentalServiceState
                };

                DbLayer.GetGenericRepository<AdvertisingBannerAudience>().Insert(advertisingBannerAudience);
                DbLayer.Save();

                CreateAdvertisingBannerAudienceLocaleRelations(advertisingBannerAudience.Id, advertisingBannerAudienceDto.Locales);

                dbScope.Commit();

                return advertisingBannerAudience.Id;
            }
            catch (Exception ex)
            {
                logger.Info($"Создание аудитории рекламного баннера завершилось с ошибкой {ex.GetFullInfo()}");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
