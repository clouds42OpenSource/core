﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для работы с данными шаблона рекламного баннера
    /// </summary>
    internal class AdvertisingBannerTemplateDataProvider(IUnitOfWork dbLayer) : IAdvertisingBannerTemplateDataProvider
    {
        /// <summary>
        /// Получить данные шаблонов рекламных баннеров
        /// </summary>
        /// <returns>Список моделей данных шаблона рекламного баннера</returns>
        public IEnumerable<AdvertisingBannerTemplateDataDto> GetAdvertisingBannerTemplates() =>
            (from advertisingBannerTemplate in dbLayer
                    .GetGenericRepository<AdvertisingBannerTemplate>().WhereLazy()
                    .OrderByDescending(banner => banner.CreationDate)
                select new AdvertisingBannerTemplateDataDto
                {
                    Id = advertisingBannerTemplate.Id,
                    Header = advertisingBannerTemplate.Header,
                    Body = advertisingBannerTemplate.Body,
                    CaptionLink = advertisingBannerTemplate.CaptionLink,
                    DisplayBannerDateFrom = advertisingBannerTemplate.DisplayBannerDateFrom,
                    DisplayBannerDateTo = advertisingBannerTemplate.DisplayBannerDateTo
                }).AsEnumerable();
    }
}