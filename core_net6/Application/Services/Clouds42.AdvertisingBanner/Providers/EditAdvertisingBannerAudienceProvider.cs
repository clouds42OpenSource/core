﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для редактирования аудитории рекламного баннера
    /// </summary>
    internal class EditAdvertisingBannerAudienceProvider(
        IUnitOfWork dbLayer,
        IAdvertisingBannerAudienceProvider advertisingBannerAudienceProvider,
        ILogger42 logger)
        : BaseOperationAdvertisingBannerAudienceProvider(dbLayer),
            IEditAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Редактировать аудиторию рекламного баннера
        /// </summary>
        /// <param name="id">Id аудитории рекламного баннера</param>
        /// <param name="advertisingBannerAudienceDto">Модель аудитории рекламного баннера</param>
        public void Edit(int id, AdvertisingBannerAudienceDto advertisingBannerAudienceDto)
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var advertisingBannerAudience = advertisingBannerAudienceProvider.GetAdvertisingBannerAudience(id);
                EditAdvertisingBannerAudienceData(advertisingBannerAudience, advertisingBannerAudienceDto);
                RemoveAdvertisingBannerAudienceLocaleRelations(advertisingBannerAudience);
                CreateAdvertisingBannerAudienceLocaleRelations(advertisingBannerAudience.Id,
                    advertisingBannerAudienceDto.Locales);

                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                logger.Info(
                    $"Редактирование аудитории рекламного баннера '{id}' завершилось с ошибкой {ex.GetFullInfo()}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Редактировать данные аудитории рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerAudience">Аудитория рекламного баннера</param>
        /// <param name="newData">Модель аудитории рекламного баннера(новые данные)</param>
        private void EditAdvertisingBannerAudienceData(AdvertisingBannerAudience advertisingBannerAudience,
            AdvertisingBannerAudienceDto newData)
        {
            advertisingBannerAudience.AccountType = newData.AccountType;
            advertisingBannerAudience.AvailabilityPayment = newData.AvailabilityPayment;
            advertisingBannerAudience.RentalServiceState = newData.RentalServiceState;

            DbLayer.GetGenericRepository<AdvertisingBannerAudience>()
                .Update(advertisingBannerAudience);

            DbLayer.Save();
        }

        /// <summary>
        /// Удалить связи аудитории рекламного баннера с локалями аккаунта
        /// </summary>
        /// <param name="advertisingBannerAudience">Аудитория рекламного баннера</param>
        private void RemoveAdvertisingBannerAudienceLocaleRelations(
            AdvertisingBannerAudience advertisingBannerAudience)
        {
            DbLayer.GetGenericRepository<AdvertisingBannerAudienceLocaleRelation>()
                .DeleteRange(advertisingBannerAudience.AdvertisingBannerAudienceLocaleRelations);
            DbLayer.Save();
        }
    }
}