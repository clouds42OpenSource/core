﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Marketing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для работы с аудиторией рекламного баннера
    /// </summary>
    internal class AdvertisingBannerAudienceProvider(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Получить аудиторию рекламного баннера
        /// </summary>
        /// <param name="id">Id рекламного баннера</param>
        /// <returns>Аудитория рекламного баннера</returns>
        public AdvertisingBannerAudience GetAdvertisingBannerAudience(int id) => dbLayer
            .GetGenericRepository<AdvertisingBannerAudience>().FirstOrThrowException(aba => aba.Id == id,
                $"Не удалось получить аудиторию рекламного баннера по Id='{id}'");

        /// <summary>
        /// Выбрать/подобрать аудиторию рекламного баннера
        /// </summary>
        /// <param name="locales"></param>
        /// <param name="accountType"></param>
        /// <param name="availabilityPayment"></param>
        /// <param name="rentalServiceState"></param>
        /// <returns>Выбранные аккаунты(аудитория рекламного баннера)</returns>
        public IQueryable<Account> SelectAdvertisingBannerAudience(List<Guid> locales, AccountTypeEnum accountType, AvailabilityPaymentEnum availabilityPayment, RentalServiceStateEnum rentalServiceState)
        {
            var payments = dbLayer.PaymentRepository.WhereLazy(p =>
                p.Status == PaymentStatus.Done.ToString() &&
                p.OperationType == PaymentType.Inflow.ToString()
                && p.Sum > 0);

            var resourceConfigurations = dbLayer.ResourceConfigurationRepository.WhereLazy(rc =>
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var result = from accountWithConfiguration in accountConfigurationDataProvider.GetAccountsWithConfiguration()
                join inflowPayment in payments on accountWithConfiguration.Account.Id equals inflowPayment.AccountId
                    into tempPayments
                from payment in tempPayments.Take(1).DefaultIfEmpty()
                join resConf in resourceConfigurations on accountWithConfiguration.Account.Id equals resConf.AccountId
                join localId in locales on accountWithConfiguration.AccountConfiguration.LocaleId equals localId
                where
                    (accountType == AccountTypeEnum.All ||
                     accountType == AccountTypeEnum.Vip && accountWithConfiguration.AccountConfiguration.IsVip ||
                     accountType == AccountTypeEnum.NotVip &&
                     !accountWithConfiguration.AccountConfiguration.IsVip) &&
                    (availabilityPayment == AvailabilityPaymentEnum.All ||
                     availabilityPayment == AvailabilityPaymentEnum.WithPayment && payment != null ||
                     availabilityPayment == AvailabilityPaymentEnum.WithoutPayment && payment == null) &&
                    (rentalServiceState == RentalServiceStateEnum.All ||
                     rentalServiceState == RentalServiceStateEnum.Active &&
                     (resConf.Frozen == null || resConf.Frozen == false) ||
                     rentalServiceState == RentalServiceStateEnum.Locked && resConf.Frozen == true)

                select accountWithConfiguration.Account;

            return result;
        }
    }
}
