﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для удаления аудитории рекламного баннера
    /// </summary>
    internal class DeleteAdvertisingBannerAudienceProvider(
        IUnitOfWork dbLayer,
        IAdvertisingBannerAudienceProvider advertisingBannerAudienceProvider,
        IHandlerException handlerException)
        : IDeleteAdvertisingBannerAudienceProvider
    {
        /// <summary>
        /// Удалить аудиторию рекламного баннера
        /// </summary>
        /// <param name="bannerAudienceId">Id аудитории рекламного баннера</param>
        public void Delete(int bannerAudienceId)
        {
            var bannerAudience = advertisingBannerAudienceProvider.GetAdvertisingBannerAudience(bannerAudienceId);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                dbLayer.GetGenericRepository<AdvertisingBannerAudienceLocaleRelation>()
                    .DeleteRange(bannerAudience.AdvertisingBannerAudienceLocaleRelations);
                dbLayer.GetGenericRepository<AdvertisingBannerAudience>().Delete(bannerAudience);
                dbLayer.Save();

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка удаления аудитории рекламного баннера] '{bannerAudienceId}'");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
