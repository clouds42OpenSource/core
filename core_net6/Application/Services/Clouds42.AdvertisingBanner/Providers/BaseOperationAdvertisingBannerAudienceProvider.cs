﻿using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер базовых операций над аудиторией рекламного баннера
    /// </summary>
    public abstract class BaseOperationAdvertisingBannerAudienceProvider(IUnitOfWork dbLayer)
    {
        protected readonly IUnitOfWork DbLayer = dbLayer;

        /// <summary>
        /// Создать связи аудитории рекламного баннера с локалями аккаунта
        /// </summary>
        /// <param name="advertisingBannerAudienceId">Id аудитории рекламного баннера</param>
        /// <param name="locales">Локали</param>
        protected void CreateAdvertisingBannerAudienceLocaleRelations(int advertisingBannerAudienceId, List<Guid> locales)
        {
            if (!locales.Any())
                return;

            locales.ForEach(localeId =>
            {
                var advertisingBannerAudienceLocaleRelation = new AdvertisingBannerAudienceLocaleRelation
                {
                    AdvertisingBannerAudienceId = advertisingBannerAudienceId,
                    LocaleId = localeId
                };

                DbLayer.GetGenericRepository<AdvertisingBannerAudienceLocaleRelation>()
                    .Insert(advertisingBannerAudienceLocaleRelation);
            });

            DbLayer.Save();
        }
    }
}
