﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для создания шаблона рекламного баннера
    /// </summary>
    internal class CreateAdvertisingBannerTemplateProvider(
        IUnitOfWork dbLayer,
        ICloudFileProvider cloudFileProvider,
        ICreateAdvertisingBannerAudienceProvider createAdvertisingBannerAudienceProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ICreateAdvertisingBannerTemplateProvider
    {
        private readonly Lazy<int> _advertisingBannerTemplateId = new(CloudConfigurationProvider.HtmlTemplate.GetAdvertisingBannerTemplateId);

        /// <summary>
        /// Создать шаблон рекламного баннера
        /// </summary>
        /// <param name="model">Модель создания шаблона рекламного баннера</param>
        public void Create(CreateAdvertisingBannerTemplateDto model)
        {
            var message = $"Создание шаблона рекламного баннера '{model.Header}'";
            logger.Info(message);

            try
            {
                var bannerImageId = cloudFileProvider.CreateCloudFile(model.Image);

                var advertisingBannerAudienceId =
                    createAdvertisingBannerAudienceProvider.Create(model.AdvertisingBannerAudience);

                var advertisingBannerTemplate = new AdvertisingBannerTemplate
                {
                    Header = model.Header,
                    Body = model.Body,
                    Link = model.Link,
                    CaptionLink = model.CaptionLink,
                    DisplayBannerDateFrom = model.ShowFrom,
                    DisplayBannerDateTo = model.ShowTo,
                    ImageCloudFileId = bannerImageId,
                    AdvertisingBannerAudienceId = advertisingBannerAudienceId,
                    HtmlTemplateId = _advertisingBannerTemplateId.Value,
                    CreationDate = DateTime.Now
                };

                dbLayer.GetGenericRepository<AdvertisingBannerTemplate>()
                    .Insert(advertisingBannerTemplate);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания шаблона рекламного баннера] {model.Header}");
                throw;
            }
        }
    }
}
