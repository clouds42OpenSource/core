﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для редактирования шаблона рекламного баннера
    /// </summary>
    internal class EditAdvertisingBannerTemplateProvider(
        IUnitOfWork dbLayer,
        IAdvertisingBannerTemplateProvider advertisingBannerTemplateProvider,
        IEditAdvertisingBannerAudienceProvider editAdvertisingBannerAudienceProvider,
        ICloudFileProvider cloudFileProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : IEditAdvertisingBannerTemplateProvider
    {
        /// <summary>
        /// Редактировать шаблон рекламного баннера
        /// </summary>
        /// <param name="editAdvertisingBannerTemplateDto">Модель редактирования шаблона рекламного баннера</param>
        public void Edit(EditAdvertisingBannerTemplateDto editAdvertisingBannerTemplateDto)
        {
            var message = $"Редактирование шаблона рекламного баннера '{editAdvertisingBannerTemplateDto.TemplateId}'";
            logger.Info(message);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var advertisingBannerTemplate =
                    advertisingBannerTemplateProvider.GetAdvertisingBannerTemplate(editAdvertisingBannerTemplateDto
                        .TemplateId);

                EditAdvertisingBannerTemplateData(advertisingBannerTemplate, editAdvertisingBannerTemplateDto);

                if (editAdvertisingBannerTemplateDto.Image?.Content != null)
                    cloudFileProvider.EditCloudFile(advertisingBannerTemplate.ImageCloudFileId,
                        editAdvertisingBannerTemplateDto.Image);

                editAdvertisingBannerAudienceProvider.Edit(advertisingBannerTemplate.AdvertisingBannerAudienceId,
                    editAdvertisingBannerTemplateDto.AdvertisingBannerAudience);

                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования шаблона рекламного банера] {editAdvertisingBannerTemplateDto.TemplateId}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Редактировать данные шаблона рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerTemplate">Шаблон рекламного баннера</param>
        /// <param name="newData">Модель редактирования шаблона рекламного баннера</param>
        private void EditAdvertisingBannerTemplateData(AdvertisingBannerTemplate advertisingBannerTemplate,
            EditAdvertisingBannerTemplateDto newData)
        {
            advertisingBannerTemplate.Header = newData.Header;
            advertisingBannerTemplate.Body = newData.Body;
            advertisingBannerTemplate.CaptionLink = newData.CaptionLink;
            advertisingBannerTemplate.Link = newData.Link;
            advertisingBannerTemplate.DisplayBannerDateFrom = newData.ShowFrom;
            advertisingBannerTemplate.DisplayBannerDateTo = newData.ShowTo;

            dbLayer.GetGenericRepository<AdvertisingBannerTemplate>().Update(advertisingBannerTemplate);
            dbLayer.Save();
        }
    }
}
