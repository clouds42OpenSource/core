﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.AdvertisingBanner.Mappers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;

namespace Clouds42.AdvertisingBanner.Providers
{
    /// <summary>
    /// Провайдер для работы с шаблоном рекламного баннера
    /// </summary>
    internal class AdvertisingBannerTemplateProvider(
        IUnitOfWork dbLayer,
        ICalculateAdvertisingBannerAudienceProvider calculateAdvertisingBannerAudienceProvider)
        : IAdvertisingBannerTemplateProvider
    {
        /// <summary>
        /// Получить модель шаблона рекламного баннера
        /// </summary>
        /// <param name="id">Id шаблона рекламного баннера</param>
        /// <returns>Модель шаблона рекламного баннера</returns>
        public AdvertisingBannerTemplateDetailsDto GetAdvertisingBannerTemplateDto(int id)
        {
            var advertisingBannerTemplate = GetAdvertisingBannerTemplate(id);

            var advertisingBannerTemplateDto = new AdvertisingBannerTemplateDetailsDto
            {
                TemplateId = advertisingBannerTemplate.Id,
                Body = advertisingBannerTemplate.Body,
                Header = advertisingBannerTemplate.Header,
                CaptionLink = advertisingBannerTemplate.CaptionLink,
                Link = advertisingBannerTemplate.Link,
                ShowFrom = advertisingBannerTemplate.DisplayBannerDateFrom,
                ShowTo = advertisingBannerTemplate.DisplayBannerDateTo,
                Image = advertisingBannerTemplate.Image.MapToFileDataDto<IFormFile>(),
                AdvertisingBannerAudience = advertisingBannerTemplate
                    .AdvertisingBannerAudience.MapToAdvertisingBannerAudienceDto()
            };

            advertisingBannerTemplateDto.CalculationAdvertisingBannerAudience =
                calculateAdvertisingBannerAudienceProvider.Calculate(advertisingBannerTemplateDto
                    .AdvertisingBannerAudience);

            return advertisingBannerTemplateDto;
        }

        /// <summary>
        /// Получить шаблон рекламного баннера
        /// </summary>
        /// <param name="id">Id шаблона рекламного баннера</param>
        /// <returns>Шаблон рекламного баннера</returns>
        public AdvertisingBannerTemplate GetAdvertisingBannerTemplate(int id) => dbLayer
            .GetGenericRepository<AdvertisingBannerTemplate>().FirstOrThrowException(b => b.Id == id,
                $"Не удалось получить шаблон рекламного баннера по Id ='{id}'");

        /// <summary>
        /// Получить краткую информацию по шаблонам рекламного баннера
        /// </summary>
        /// <returns>Краткая информация по шаблонам рекламного баннера</returns>
        public IEnumerable<AdvertisingBannerTemplateShortInfoDto> GetAdvertisingBannerTemplatesShortInfo() =>
            (from banner in dbLayer.GetGenericRepository<AdvertisingBannerTemplate>().WhereLazy()
                    .OrderByDescending(banner => banner.CreationDate)
                select new AdvertisingBannerTemplateShortInfoDto
                {
                    Id = banner.Id,
                    Header = banner.Header
                }).AsEnumerable();

        /// <summary>
        /// Получить изображение рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerId">Id шаблона рекламного баннера</param>
        /// <returns>Изображение рекламного баннера</returns>
        public FileDataDto<byte[]> GetAdvertisingBannerImage(int advertisingBannerId)
        {
            var cloudFile = GetAdvertisingBannerTemplate(advertisingBannerId)?.Image ??
                            throw new NotFoundException(
                                $"Не удалось получить изображение рекламного баннера {advertisingBannerId}");

            return new FileDataDto<byte[]>
            {
                Content = cloudFile.Content,
                FileName = cloudFile.FileName,
                ContentType = cloudFile.ContentType,
                Base64 = Convert.ToBase64String(cloudFile.Content)
            };
        }
    }
}
