﻿using Clouds42.AdvertisingBanner.Builder;
using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.AdvertisingBanner.Managers;
using Clouds42.AdvertisingBanner.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AdvertisingBanner
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddAdvertisingBannerAudience(this IServiceCollection services)
        {
            services.AddTransient<ICalculateAdvertisingBannerAudienceProvider, CalculateAdvertisingBannerAudienceProvider>();
            services.AddTransient<ICreateAdvertisingBannerAudienceProvider, CreateAdvertisingBannerAudienceProvider>();
            services.AddTransient<IEditAdvertisingBannerAudienceProvider, EditAdvertisingBannerAudienceProvider>();
            services.AddTransient<IAdvertisingBannerAudienceProvider, AdvertisingBannerAudienceProvider>();
            services.AddTransient<IDeleteAdvertisingBannerAudienceProvider, DeleteAdvertisingBannerAudienceProvider>();
            services.AddTransient<IAdvertisingBannerTemplateProvider, AdvertisingBannerTemplateProvider>();
            services.AddTransient<ICreateAdvertisingBannerTemplateProvider, CreateAdvertisingBannerTemplateProvider>();
            services.AddTransient<IEditAdvertisingBannerTemplateProvider, EditAdvertisingBannerTemplateProvider>();
            services.AddTransient<IAdvertisingBannerHtmlBuilder, AdvertisingBannerHtmlBuilder>();
            services.AddTransient<IAdvertisingBannerTemplateDataProvider, AdvertisingBannerTemplateDataProvider>();
            services.AddTransient<IDeleteAdvertisingBannerTemplateProvider, DeleteAdvertisingBannerTemplateProvider>();
            services.AddTransient<CalculateAdvertisingBannerAudienceManager>();
            services.AddTransient<AdvertisingBannerTemplateManager>();

            return services;
        }
    }
}
