﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AdvertisingBanner.Managers
{
    /// <summary>
    /// Менеджер для подсчета аудитории рекламного баннера
    /// </summary>
    public class CalculateAdvertisingBannerAudienceManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICalculateAdvertisingBannerAudienceProvider calculateAdvertisingBannerAudienceProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Посчитать аудиторию рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerAudience">Модель аудитории рекламного баннера</param>
        /// <returns>Результат подсчета аудитории рекламного баннера</returns>
        public ManagerResult<CalculationAdvertisingBannerAudienceDto> Calculate(
            AdvertisingBannerAudienceDto advertisingBannerAudience)
        {
            logger.Info("Подсчет аудитории рекламного баннера");
            try
            {
                AccessProvider.HasAccess(ObjectAction.AdvertisingBannerAudience_Calculate, () => AccessProvider.ContextAccountId);

                var data = calculateAdvertisingBannerAudienceProvider.Calculate(advertisingBannerAudience);
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Подсчет аудитории рекламного баннера завершился с ошибкой]");
                return PreconditionFailed<CalculationAdvertisingBannerAudienceDto>(ex.Message);
            }
        }
    }
}
