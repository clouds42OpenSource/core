﻿using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Partner.Validators;


namespace Clouds42.AdvertisingBanner.Managers
{
    /// <summary>
    /// Менеджер для работы с шаблоном рекламного баннера
    /// </summary>
    public class AdvertisingBannerTemplateManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAdvertisingBannerTemplateProvider advertisingBannerTemplateProvider,
        IHandlerException handlerException,
        ICreateAdvertisingBannerTemplateProvider createAdvertisingBannerTemplateProvider,
        IEditAdvertisingBannerTemplateProvider editAdvertisingBannerTemplateProvider,
        IAdvertisingBannerTemplateDataProvider advertisingBannerTemplateDataProvider,
        IDeleteAdvertisingBannerTemplateProvider deleteAdvertisingBannerTemplateProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить модель шаблона рекламного баннера
        /// </summary>
        /// <param name="id">Id шаблона рекламного баннера</param>
        /// <returns>Модель шаблона рекламного баннера</returns>
        public ManagerResult<AdvertisingBannerTemplateDetailsDto> GetAdvertisingBannerTemplateDto(int id)
        {
            logger.Info("Получение модели шаблона рекламного баннера");
            try
            {
                AccessProvider.HasAccess(ObjectAction.AdvertisingBannerTemplate_Get,
                    () => AccessProvider.ContextAccountId);

                var data = advertisingBannerTemplateProvider.GetAdvertisingBannerTemplateDto(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения модели шаблона рекламного баннера]");
                return PreconditionFailed<AdvertisingBannerTemplateDetailsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить изображение рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerId">Id шаблона рекламного баннера</param>
        /// <returns>Изображение рекламного баннера</returns>
        public ManagerResult<FileDataDto<byte[]>> GetAdvertisingBannerImage(int advertisingBannerId)
        {
            try
            {
                if (advertisingBannerId == 0)
                    return PreconditionFailed<FileDataDto<byte[]>>($"Id шаблона рекламного баннера равен нулю :: {advertisingBannerId}");

                var image = advertisingBannerTemplateProvider.GetAdvertisingBannerImage(advertisingBannerId);
                return Ok(image);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка получения изображения рекламного баннера '{advertisingBannerId}']");
                return PreconditionFailed<FileDataDto<byte[]>>(ex.Message);
            }
        }

        /// <summary>
        /// Создать шаблон рекламного баннера
        /// </summary>
        /// <param name="createAdvertisingBannerTemplateDto">Модель создания шаблона рекламного баннера</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult CreateAdvertisingBannerTemplate(
            CreateAdvertisingBannerTemplateDto createAdvertisingBannerTemplateDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AdvertisingBannerTemplate_Create,
                    () => AccessProvider.ContextAccountId);

                if (!createAdvertisingBannerTemplateDto.TryValidateObj(out var errorMessage))
                    return PreconditionFailed(errorMessage);

                createAdvertisingBannerTemplateProvider.Create(createAdvertisingBannerTemplateDto);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CreateAdvertisingBannerTemplate,
                    $"Создан рекламный баннер “{createAdvertisingBannerTemplateDto.Header}”");

                return Ok();
            }
            catch (Exception ex)
            {
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CreateAdvertisingBannerTemplate,
                    $"Создание рекламного баннера “{createAdvertisingBannerTemplateDto.Header}” завершилось с ошибкой: {ex.Message}");

                _handlerException.Handle(ex, "[Ошибка создания шаблона рекламного баннера]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать шаблон рекламного баннера
        /// </summary>
        /// <param name="editAdvertisingBannerTemplateDto">Модель редактирования шаблона рекламного баннера</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult EditAdvertisingBannerTemplate(
            EditAdvertisingBannerTemplateDto editAdvertisingBannerTemplateDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AdvertisingBannerTemplate_Edit,
                    () => AccessProvider.ContextAccountId);

                if (!editAdvertisingBannerTemplateDto.TryValidateObj(out var errorMessage))
                    return PreconditionFailed(errorMessage);

                editAdvertisingBannerTemplateProvider.Edit(editAdvertisingBannerTemplateDto);

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.EditAdvertisingBannerTemplate,
                    $"Рекламный баннер для писем “{editAdvertisingBannerTemplateDto.Header}” отредактирован");

                return Ok();
            }
            catch (Exception ex)
            {
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.EditAdvertisingBannerTemplate,
                    $"Редактирование рекламного баннера “{editAdvertisingBannerTemplateDto.Header}” для писем завершилось с ошибкой: {ex.Message}");

                _handlerException.Handle(ex,
                    $"[Ошибка редактирования шаблона рекламного баннера '{editAdvertisingBannerTemplateDto.TemplateId}']");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные шаблонов рекламных баннеров
        /// </summary>
        /// <returns>Список моделей данных шаблона рекламного баннера</returns>
        public ManagerResult<IEnumerable<AdvertisingBannerTemplateDataDto>> GetAdvertisingBannerTemplates()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AdvertisingBannerTemplate_Get,
                    () => AccessProvider.ContextAccountId);

                var data = advertisingBannerTemplateDataProvider.GetAdvertisingBannerTemplates();
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    "[Ошибка получения данных рекламных баннеров]");
                return PreconditionFailed<IEnumerable<AdvertisingBannerTemplateDataDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Удалить рекламный баннер
        /// </summary>
        /// <param name="bannerTemplateId">Id рекламного баннера</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult DeleteAdvertisingBannerTemplate(int bannerTemplateId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AdvertisingBannerTemplate_Delete,
                    () => AccessProvider.ContextAccountId);

                deleteAdvertisingBannerTemplateProvider.Delete(bannerTemplateId);
                
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка удаления рекламного баннера '{bannerTemplateId}']");
                return PreconditionFailed<IEnumerable<AdvertisingBannerTemplateDataDto>>(ex.Message);
            }
        }
    }
}
