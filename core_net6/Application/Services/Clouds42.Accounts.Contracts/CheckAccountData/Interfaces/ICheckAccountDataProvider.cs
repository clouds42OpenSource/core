﻿using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Accounts.Contracts.CheckAccountData.Interfaces
{
    /// <summary>
    /// Провайдер для выполнения проверок над данными аккаунта
    /// </summary>
    public interface ICheckAccountDataProvider
    {
        /// <summary>
        /// Проверить по количеству возможность создать базу для аккаунта
        /// </summary>
        /// <param name="account">Id аккаунта</param>
        /// <param name="databasesCount">Количество баз</param>
        CreateCloud42ServiceModelDto CheckAbilityToCreateDatabasesByLimit(Domain.DataModels.Account account, int databasesCount);

        /// <summary>
        ///     Проверить что аккаунт спонсируется
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>true - если аккаунт спонсируется</returns>
        bool HasAccountSponsoredLicences(Domain.DataModels.Account account);

        /// <summary>
        ///     Проверить что аккаунт спонсируется
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>true - если аккаунт спонсируется</returns>
        bool HasAccountSponsoredLicences(Guid accountId);

        /// <summary>
        /// Проверить что аккаунт относится к демо.
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>признак доступности</returns>
        bool CheckAccountIsDemo(IAccount account);

        /// <summary>
        /// Проверить что аккаунт относится к демо.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Признак демо у аккаунта</returns>
        bool CheckAccountIsDemo(Guid accountId);

        /// <summary>
        /// Проверить доступность ТиИ для аккаунта.
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Признак демо у аккаунта</returns>
        bool CheckAvailabilityUseSupportOperation(Domain.DataModels.Account account);

        /// <summary>
        /// Возможность сменить локаль для аккаунта
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Возможность сменить локаль</returns>
        (bool, string) CanChangeLocaleForAccount(Guid accountId);

        /// <summary>
        /// Наличие платежей
        /// или оплаченных счетов у аккаунта
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Наличие платежей или счетов</returns>
        bool HasPaidInvoicesOrPayments(Guid accountId);

        /// <summary>
        /// Наличие счетов для кастомных сервисов
        /// или сервиса Загрузка документов
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        bool HasInvoicesForEsdlOrCustomServices(Guid accountId);

        /// <summary>
        /// Наличие счетов на произвольную сумму
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Наличие счетов на произвольную сумму</returns>
        bool HasInvoicesOnSuggestedSum(Guid accountId);

        /// <summary>
        /// Наличие кастомных сервисов для аккаунта
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Наличие кастомных сервисов</returns>
        bool HasCustomServicesForAccount(Guid accountId);

        /// <summary>
        /// Проверить были ли денежные поступления на аккаунт
        /// </summary>
        /// <param name="accountId">Аккаунт</param>
        /// <returns>Признак наличия поступлений</returns>
        bool AccountHasInflowPayments(Guid accountId);
    }
}
