﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces
{
    /// <summary>
    /// Провайдер для обработки удаленных услуг сервиса для аккаунтов
    /// </summary>
    public interface IProcessDeletedServiceTypesForAccountsProvider
    {
        /// <summary>
        /// Обработать удаленные услуги для аккаунтов
        /// </summary>
        /// <param name="model">Модель обработки удаленных услуг для аккаунтов</param>
        void Process(ProcessDeletedServiceTypesForAccountsDto model);
    }
}
