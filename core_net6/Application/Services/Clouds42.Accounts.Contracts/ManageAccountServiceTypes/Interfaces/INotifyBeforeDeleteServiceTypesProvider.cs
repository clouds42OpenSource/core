﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces
{
    /// <summary>
    /// Провайдер для уведомления о скором
    /// удалении услуг сервиса
    /// </summary>
    public interface INotifyBeforeDeleteServiceTypesProvider
    {
        /// <summary>
        /// Уведомить аккаунты о скором удалении
        /// услуг сервиса
        /// </summary>
        /// <param name="model">Модель отложенного удаления услуг сервиса для аккаунтов</param>
        void NotifyAccounts(DelayedServiceTypesDeletionForAccountsDto model);
    }
}
