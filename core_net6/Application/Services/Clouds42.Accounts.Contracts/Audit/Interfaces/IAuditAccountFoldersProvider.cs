﻿namespace Clouds42.Accounts.Contracts.Audit.Interfaces
{
    /// <summary>
    /// Провайдер проверки папок аккаунта
    /// </summary>
    public interface IAuditAccountFoldersProvider
    {
        /// <summary>
        /// Проверить права на папки аккаунтов 
        /// </summary>
        void AuditAccountFoldersNtfsRules();
    }
}
