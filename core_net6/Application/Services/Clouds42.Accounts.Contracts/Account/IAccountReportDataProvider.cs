﻿using Clouds42.DataContracts.Account;

namespace Clouds42.Accounts.Contracts.Account
{
    /// <summary>
    /// Провайдер для работы с данными аккаунта для отчета
    /// </summary>
    public interface IAccountReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по аккаунтам для отчета
        /// </summary>
        /// <returns>Список моделей данных по аккаунтам для отчета</returns>
        List<AccountReportDataDto> GetAccountReportDataDcs();
    }
}
