﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;

namespace Clouds42.Accounts.Contracts.Account
{
    /// <summary>
    /// Провайдер для работы с данными аккаунта
    /// </summary>
    public interface IAccountProvider
    {

        /// <summary>
        /// Получить списка баз аккаунта для миграции
        /// </summary>
        /// <param name="args">Фильр поиска</param>
        /// <returns>Список баз аккаунта для миграции</returns>
        Task<AccountDatabasesMigrationResultDto> GetAccountDatabasesForMigrationAsync(
            GetAccountDatabasesForMigrationQuery args);
    }
}
