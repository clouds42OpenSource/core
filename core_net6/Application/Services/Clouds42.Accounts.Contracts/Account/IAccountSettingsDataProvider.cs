﻿using Clouds42.DataContracts.Account;

namespace Clouds42.Accounts.Contracts.Account
{
    /// <summary>
    /// Провайдер получения данных настроек аккаунта
    /// </summary>
    public interface IAccountSettingsDataProvider
    {
        /// <summary>
        /// Получить настройки аккаунта
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Настройки аккаунта</returns>
        AccountSettingsDataDto GetAccountSettings(Guid accountUserId);
    }
}
