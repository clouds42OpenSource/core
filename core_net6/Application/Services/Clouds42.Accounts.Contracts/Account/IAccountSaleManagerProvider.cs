﻿using Clouds42.DataContracts.Account;

namespace Clouds42.Accounts.Contracts.Account
{
    /// <summary>
    /// Провайдер для работы с менеджером аккаунта
    /// </summary>
    public interface IAccountSaleManagerProvider
    {
        /// <summary>
        /// Удалить сейл менеджера у аккаунта
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        void RemoveSaleManagerFromAccount(int accountNumber);

        /// <summary>
        /// Прикрепить сейл менеджера к аккаунту
        /// </summary>
        /// <param name="accountSaleManagerDto">Модель менеджера прикрепленного к аккаунту</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        void AttachSaleManagerToAccount(AccountSaleManagerDto accountSaleManagerDto, out string errorMessage);

        /// <summary>
        /// Получить информацию по сейл менеджеру для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информацию по сейл менеджеру для аккаунта</returns>
        AccountSaleManagerInfoDto GetAccountSaleManagerInfo(Guid accountId);
    }
}
