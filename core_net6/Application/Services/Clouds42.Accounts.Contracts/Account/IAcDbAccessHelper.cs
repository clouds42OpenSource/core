﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.Domain.DataModels;

namespace Clouds42.Accounts.Contracts.Account
{
    public interface IAcDbAccessHelper
    {
        ManageAccessResultDto GrandInternalAccessToDb(AccountDatabase accountDatabase, AccountUser accountUser);
        ManageAccessResultDto GrandExternalAccessToDb(AccountDatabase accountDatabase, string accountUserEmail);
        GrandAccessForAllUsersOfAccountResultDc GrandAccessForAllUsersOfAccount(AccountDatabase accountDatabase);
        ManageAccessResultDto DeleteAccessFromDb(AccountDatabase accountDatabase, AccountUser accountUser);
    }
}