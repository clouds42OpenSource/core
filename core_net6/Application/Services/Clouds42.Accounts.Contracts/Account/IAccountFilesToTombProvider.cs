namespace Clouds42.Accounts.Contracts.Account
{
    /// <summary>
    /// Провайдер по переносу файлов аккаунта в склеп.
    /// </summary>
    public interface IAccountFilesToTombProvider
    {
        /// <summary>
        /// Заархивировать файлы аккаунта в склеп
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        void ArchiveAccountFilesToTomb(Guid accountId);
    }
}
