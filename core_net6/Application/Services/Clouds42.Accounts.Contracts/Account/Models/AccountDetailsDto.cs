﻿namespace Clouds42.Accounts.Contracts.Account.Models
{
    /// <summary>
    /// Модель отображения информации об аккаунте
    /// </summary>
    public class AccountDetailsDto
    {
        /// <summary>
        /// Номер индекса
        /// </summary>
        public int IndexNumber { set; get; }

        /// <summary>
        /// Заголовок аккаунта
        /// </summary>
        public string AccountCaption { set; get; }

        /// <summary>
        /// Id Админ аккаунта
        /// </summary>
        public Guid? AccountAdminId { set; get; }

        /// <summary>
        /// Логин Админ аккаунта
        /// </summary>
        public string AccountAdminLogin { set; get; }

        /// <summary>
        ///Название Админ аккаунта
        /// </summary>
        public string AccountAdminName { set; get; }

        /// <summary>
        /// Почта Админ аккаунта
        /// </summary>
        public string AccountAdminEmail { set; get; }

        /// <summary>
        /// Номер телефона Админ аккаунта
        /// </summary>
        public string AccountAdminPhoneNumber { set; get; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Дата регистрации аккаунта
        /// </summary>
        public DateTime? AccountRegistrationDate { set; get; }

        /// <summary>
        /// Количество пользователей
        /// </summary>
        public int AccountUsersCount { set; get; }

        /// <summary>
        /// Истечение срока действия
        /// </summary>
        public DateTime? Rent1CExpiredDate { set; get; }

        /// <summary>
        /// Флаг, показывающий является ли аккаунт VIP
        /// </summary>
        public bool IsVip { set; get; }
    }
}
