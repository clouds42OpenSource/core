namespace Clouds42.Accounts.Contracts.Account
{
    /// <summary>
    /// Провайдер по анализу файлов аккаунта
    /// </summary>
    public interface IAccountFilesAnalyzeProvider
    {
        /// <summary>
        /// Проверка необходимости запуска таски
        /// на архивацию файлов аккаунта в склеп
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Необходимость в архивации</returns>
        bool NeedArchiveAccountFilesToTomb(Guid accountId);
    }
}
