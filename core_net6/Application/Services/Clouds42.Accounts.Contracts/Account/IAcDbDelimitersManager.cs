﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;

namespace Clouds42.Accounts.Contracts.Account
{
    public interface IAcDbDelimitersManager
    {
        ManagerResult AddAccessToDbOnDelimiters(AcDbAccessPostAddModelDto model, AccountDatabase database);
        ManagerResult DeleteAccessFromDelimiterDatabase(AccountDatabase database, Guid accountUserId);
        ManagerResult<SimpleResultModelDto<Guid>> GetAccountDatabaseIdByZone(Guid databaseSourceId, int zone);
        ManagerResult SetStatusAndInsertDataOnDelimiter(AccountDatabaseViaDelimiterDto model);
    }
}
