﻿using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;

namespace Clouds42.Accounts.Contracts.Account
{
    public interface IAccountDataProvider
    {
        List<AccountUser>? GetAdministratorsGroupedByAccount(Guid accountId);
        string? GetAccountAdminPhoneNumber(Guid accountId);
        string GetAccountType(Guid? referralAccountId, string configurationType);
        Domain.DataModels.Account GetAccountOrThrowException(Guid accountId);
        Domain.DataModels.Account GetAccount(Guid accountId);
        string GetAccountFullName(Guid accountId);
        string GetAccountFullName(Domain.DataModels.Account account);
        bool NeedApplyAgencyAgreement(Guid accountId);
        CloudServicesSegment GetCloudServicesSegmentById(Guid segmentId);
        AccountInfoForNotifyDto GetAccountInfoForNotifyDto(Guid accountId);
        string GetAccountAdminDescription(Guid accountId);
    }
}
