﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;

namespace Clouds42.Accounts.Contracts.Account
{
    public interface IAccountDatabaseUserAccessManager
    {
        ManageAccessResultDto DeleteAccessFromDb(Guid databaseId, Guid accountUserId);
        GrandAccessForAllUsersOfAccountResultDc GrandAccessForAllUsersOfAccount(Guid accountDatabaseId);
        ManageAccessResultDto GrandExternalAccessToDb(Guid databaseId, string accountUserEmail);
        ManageAccessResultDto GrandInternalAccessToDb(Guid databaseId, Guid accountUserId);
    }
}