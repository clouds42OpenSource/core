﻿using Clouds42.DataContracts.Segment;

namespace Clouds42.Accounts.Contracts.Account
{
    /// <summary>
    /// Провайдер миграций аккаунта между сегментами
    /// </summary>
    public interface IAccountSegmentMigrationProvider
    {
        /// <summary>
        /// Сменить сегмент для аккаунтов
        /// </summary>
        /// <param name="segmentMigrationParams">Параметры смены сегмента для аккаунтов</param>
        void ChangeAccountsSegment(SegmentMigrationParamsDto segmentMigrationParams);
    }
}
