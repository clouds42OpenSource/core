﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;

namespace Clouds42.Accounts.Contracts.CorpCloud.Interfaces
{
    public interface IAccountCorpCloudProvider
    {
        ManagerResult<AccountCorpCloudDto> GetAccountCorpCloudInfo(Guid accountId);
        ManagerResult UpdateAccountCorpCloudInfo(Guid accountId);
    }
}
