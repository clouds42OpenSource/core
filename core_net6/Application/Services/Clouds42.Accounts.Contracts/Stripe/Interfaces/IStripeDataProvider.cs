﻿using Clouds42.DataContracts.Navigation;

namespace Clouds42.Accounts.Contracts.Stripe.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными для страйпа
    /// </summary>
    public interface IStripeDataProvider
    {
        /// <summary>
        /// Получить данные пользователя для страйпа
        /// </summary>
        /// <param name="userLogin">Логин пользователя</param>
        /// <returns>Модель данных пользователя для страйпа</returns>
        UserDataForStripeDto GetUserDataForStripe(string userLogin);

        /// <summary>
        /// Получить модель страйпа
        /// </summary>
        /// <param name="userOptionsForStripeModel">Модель параметров пользователя для получения страйпа</param>
        /// <returns>Модель страйпа</returns>
        StripeDto GetStripeData(UserOptionsForStripeModelDto userOptionsForStripeModel);
    }
}