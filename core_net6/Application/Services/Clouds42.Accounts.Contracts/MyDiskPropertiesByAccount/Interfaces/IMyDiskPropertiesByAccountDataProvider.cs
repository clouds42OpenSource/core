﻿namespace Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными
    /// свойств сервиса "Мой диск" по аккаунту
    /// </summary>
    public interface IMyDiskPropertiesByAccountDataProvider
    {
        /// <summary>
        /// Получить свойства сервиса "Мой диск" по аккаунту
        /// или создать новые (если записи не существует)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Свойства сервиса "Мой диск" по аккаунту</returns>
        Domain.DataModels.AccountProperties.MyDiskPropertiesByAccount GetIfExistsOrCreateNew(Guid accountId);
    }
}
