﻿using Clouds42.DataContracts.MyDisk;

namespace Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces
{
    /// <summary>
    /// Провайдер для создания свойств сервиса "Мой диск" по аккаунту
    /// </summary>
    public interface ICreateMyDiskPropertiesByAccountProvider
    {
        /// <summary>
        /// Создать свойств сервиса "Мой диск" по аккаунту
        /// </summary>
        /// <param name="model">Модель создания свойств
        /// сервиса "Мой диск" по аккаунту</param>
        /// <returns>Свойства сервиса "Мой диск" по аккаунту</returns>
        Domain.DataModels.AccountProperties.MyDiskPropertiesByAccount Create(MyDiskPropertiesByAccountDto model);
    }
}
