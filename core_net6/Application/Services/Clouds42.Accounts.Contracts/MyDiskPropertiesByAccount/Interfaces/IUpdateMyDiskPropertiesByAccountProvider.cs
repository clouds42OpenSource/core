﻿using Clouds42.DataContracts.MyDisk;

namespace Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces
{
    /// <summary>
    /// Провайдер для обновления свойств сервиса "Мой диск" по аккаунту
    /// </summary>
    public interface IUpdateMyDiskPropertiesByAccountProvider
    {
        /// <summary>
        /// Обновить свойства сервиса "Мой диск" по аккаунту
        /// </summary>
        /// <param name="model">Модель свойств сервиса "Мой диск" по аккаунту</param>
        void Update(MyDiskPropertiesByAccountDto model);
    }
}
