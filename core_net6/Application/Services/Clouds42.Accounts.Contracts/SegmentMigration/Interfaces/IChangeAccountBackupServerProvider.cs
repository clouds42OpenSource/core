﻿using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;

namespace Clouds42.Accounts.Contracts.SegmentMigration.Interfaces
{
    /// <summary>
    /// Провайдер для смены хранилища бэкапов аккаунта
    /// </summary>
    public interface IChangeAccountBackupServerProvider
    {
        /// <summary>
        /// Сменить хранилище бэкапов для аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        void ChangeAccountBackupServer(ChangeAccountSegmentParamsDto model);

        /// <summary>
        /// Признак необходимости менять хранилище бэкапов
        /// </summary>
        /// <param name="currentAccountSegment">Текущий сегмент аккаунта</param>
        /// <param name="targetAccountSegment">Целевой сегмент для миграции</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Результат проверки</returns>
        bool NeedChangeBackupServer(CloudServicesSegment currentAccountSegment, CloudServicesSegment targetAccountSegment, Domain.DataModels.Account account);
    }
}
