﻿using Clouds42.Domain.DataModels;

namespace Clouds42.Accounts.Contracts.SegmentMigration.Interfaces
{
    public interface IAccountFolderHelper
    {
        /// <summary>
        /// Скопировать директорию
        /// </summary>
        /// <param name="sourcePath">Текущий путь</param>
        /// <param name="targetPath">Путь, куда нужно скопировать</param>
        void CopyDirectory(string sourcePath, string targetPath);

        /// <summary>
        /// Удалить директорию
        /// </summary>
        /// <param name="folderPath">Путь</param>
        void DeleteDirectory(string folderPath);

        /// <summary>
        /// Убераем флаг "Только для чтения" у папок и файлов
        /// </summary>
        /// <param name="directoryInfo">Путь до директории</param>
        void SetAttributesNormal(DirectoryInfo directoryInfo);

        /// <summary>
        /// Получить путь к файлам бэкапов аккаунта
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Путь к файлам бэкапов аккаунта</returns>
        string GetAccountFilesBackupPath(CloudServicesSegment segment, Domain.DataModels.Account account);

        /// <summary>
        /// Получить путь к файлам аккаунта
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Путь к файлам аккаунта</returns>
        string GetAccountFileStoragePath(CloudServicesSegment segment, Domain.DataModels.Account account);

        /// <summary>
        /// Получить путь к клиентским файлам аккаунта
        /// </summary>
        /// <param name="segment">Сегмент</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Путь к файлам аккаунта</returns>
        string GetAccountClientFileStoragePath(CloudServicesSegment segment,Domain.DataModels.Account account);
    }
}
