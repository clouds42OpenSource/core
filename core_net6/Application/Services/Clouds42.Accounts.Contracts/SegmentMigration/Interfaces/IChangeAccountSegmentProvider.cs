﻿using Clouds42.DataContracts.Account;
using MigrationResultDto = Clouds42.DataContracts.Account.MigrationResultDto;

namespace Clouds42.Accounts.Contracts.SegmentMigration.Interfaces
{
    /// <summary>
    /// Провайдер для смены сегмента у аккаунта
    /// </summary>
    public interface IChangeAccountSegmentProvider
    {
        /// <summary>
        /// Сменить сегмент для аккаунтов
        /// </summary>
        /// <param name="changeAccountSegmentParams">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат смены сегмента</returns>
        MigrationResultDto ChangeAccountSegment(ChangeAccountSegmentParamsDto changeAccountSegmentParams);
    }
}
