﻿using Clouds42.DataContracts.Account;

namespace Clouds42.Accounts.Contracts.SegmentMigration.Interfaces
{
    /// <summary>
    /// Провайдер для смены клиентских файлов аккаунта
    /// </summary>
    public interface IChangeAccountClientFileStorageProvider
    {
        /// <summary>
        /// Сменить клиентских файлов для аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        void ChangeAccountClientFileStorage(ChangeAccountSegmentParamsDto model);
    }
}
