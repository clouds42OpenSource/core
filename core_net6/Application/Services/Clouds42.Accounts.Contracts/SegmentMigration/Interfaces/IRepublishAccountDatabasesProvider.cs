﻿using Clouds42.DataContracts.Account;

namespace Clouds42.Accounts.Contracts.SegmentMigration.Interfaces
{
    /// <summary>
    /// Провайдер для перепубликации баз аккаунта
    /// </summary>
    public interface IRepublishAccountDatabasesProvider
    {
        /// <summary>
        /// Переопубликовать инф. базы аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        void RepublishAccountDatabases(ChangeAccountSegmentParamsDto model);
    }
}
