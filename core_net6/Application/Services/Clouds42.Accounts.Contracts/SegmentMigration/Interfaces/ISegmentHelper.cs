﻿using Clouds42.Domain.DataModels;

namespace Clouds42.Accounts.Contracts.SegmentMigration.Interfaces
{
    public interface ISegmentHelper
    {
        /// <summary>
        /// Получить сервер предприятия 8.2
        /// </summary>
        /// <returns>Сервер предприятия 8.2</returns>
        string GetEnterpriseServer82(Domain.DataModels.Account account);

        /// <summary>
        /// Получить сервер предприятия 8.3
        /// </summary>
        /// <returns>Сервер предприятия 8.3</returns>
        string GetEnterpriseServer83(Domain.DataModels.Account account);

        /// <summary>
        /// Возвращает обьект дефолтного файлового хранилища
        /// </summary>
        /// <returns>Обьект дефолтного файлового хранилища</returns>
        CloudServicesFileStorageServer GetFileStorageServer(Domain.DataModels.Account account);

        /// <summary>
        /// Возвращает обьект файлового хранилища клиентских файлов
        /// </summary>
        /// <returns>Обьект файлового хранилища клиентских файлов</returns>
        CloudServicesFileStorageServer GetClientFileStorage(Domain.DataModels.Account account);

        /// <summary>
        /// Возвращает полный путь к папке клиентских файлов (с учетом CustomFileStoragePath для VIP).
        /// </summary>
        /// <returns></returns>
        string GetFullClientFileStoragePath(Domain.DataModels.Account account);

        /// <summary>
        /// Получить адрес подключения для шлюза сегмента
        /// </summary>
        /// <returns>Адрес подключения для шлюза сегмента</returns>
        string GetGatewayTerminals(Domain.DataModels.Account account);

        /// <summary>
        /// Получить адрес подключения к SQL серверу
        /// </summary>
        /// <returns>Адрес подключения к SQL серверу</returns>
        string GetSQLServer(Domain.DataModels.Account account);

        /// <summary>
        /// Получить адрес подключения к ферме серверов
        /// </summary>
        /// <returns>Адрес подключения к ферме серверов</returns>
        string GetTerminalFarm(Domain.DataModels.Account account);

        /// <summary>
        ///  Получить сервер публикаций для аккаунта
        /// </summary>
        /// <returns>Cервер публикаций для аккаунта</returns>
        CloudServicesContentServer GetContentServer(Domain.DataModels.Account account);

        /// <summary>
        /// Получить имя сайта публикаций
        /// </summary>
        /// <returns>Имя сайта публикаций</returns>
        string GetPublishSiteName(Domain.DataModels.Account account);

        /// <summary>
        ///     Сформировать путь к опубликованной базе
        /// </summary>
        /// <param name="folder">Папка</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <param name="databaseName">Имя базы в формате V82Name</param>
        /// <returns>Путь к опубликованной базе</returns>
        string GetPublishedDatabasePhycicalPath(string folder, string accountEncodeId, string databaseName, Domain.DataModels.Account account);

        /// <summary>
        ///     Сформировать путь для IIS к опубликованной базе
        /// </summary>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <param name="databaseName">Имя базы в формате V82Name</param>
        /// <returns>Путь для IIS к опубликованной базе</returns>
        string GetIisWebAddress(string accountEncodeId, string databaseName, Domain.DataModels.Account account);

        /// <summary>
        ///     Сформировать путь для IIS к опубликованной базе
        /// </summary>
        /// <param name="contentServer">Сервер публикаций</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <param name="databaseName">Имя базы в формате V82Name</param>
        /// <returns>Путь для IIS к опубликованной базе</returns>
        string GetIisWebAddress(CloudServicesContentServer contentServer, string accountEncodeId,
            string databaseName);

        /// <summary>
        ///     Сформировать путь к секции конфига
        /// </summary>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <param name="databaseName">Имя базы в формате V82Name</param>
        /// <returns>Путь к секции конфига</returns>
        string CreateSectionName(string accountEncodeId, string databaseName, Domain.DataModels.Account account);

        /// <summary>
        ///     Сформировать путь где лежат все
        /// опубликованные базы аккаунта
        /// </summary>
        /// <param name="folder">Папка</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <returns>Путь к опубликованным базам аккаунта</returns>
        string CreateAccountDirectoryPath(string folder, string accountEncodeId, Domain.DataModels.Account account);

        /// <summary>
        ///     Сформировать путь где лежат все
        /// опубликованные базы аккаунта
        /// </summary>
        /// <param name="contentServer">Сервер публикаций</param>
        /// <param name="folder">Папка</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        /// <returns>Путь к опубликованным базам аккаунта</returns>
        string CreateAccountDirectoryPath(CloudServicesContentServer contentServer, string folder,
            string accountEncodeId);

        /// <summary>
        ///     Создать путь к модулю IIS для стабильной версии платформы 8.2
        /// </summary>
        /// <returns>Путь к модулю IIS для стабильной версии платформы 8.2</returns>
        string CreatePathToModuleV82Stable(Domain.DataModels.Account account);

        /// <summary>
        ///     Создать путь к модулю IIS для стабильной версии платформы 8.3
        /// </summary>
        /// <returns>Путь к модулю IIS для стабильной версии платформы 8.3</returns>
        string CreatePathToModuleV83Stable(Domain.DataModels.Account account);

        /// <summary>
        ///     Создать путь к модулю IIS для альфа версии платформы 8.3
        /// </summary>
        /// <returns>Путь к модулю IIS для альфа версии платформы 8.3</returns>
        string CreatePathToModuleV83Alpha(Domain.DataModels.Account account);

        /// <summary>
        /// Получить название сегмента
        /// </summary>
        /// <returns>Название сегмента</returns>
        string GetSegmentName(Domain.DataModels.Account account);

        /// <summary>
        /// Получить файловые хранилища сегмента
        /// </summary>
        /// <returns>Файловые хранилища сегмента</returns>
        List<CloudServicesFileStorageServer> GetEnableStorageList(Domain.DataModels.Account account);

        /// <summary>
        /// Получить путь до 1С exe для альфа платформы
        /// </summary>
        /// <returns>Путь до 1С exe для альфа платформы</returns>
        string GetPath1CExeAlpha83Platform(Domain.DataModels.Account account);

        /// <summary>
        /// Получить путь до 1С exe для стабильной версии 8.3
        /// </summary>
        /// <returns>Путь до 1С exe для стабильной версии 8.3</returns>
        string GetPath1CExeStable83Platform(Domain.DataModels.Account account);

        /// <summary>
        /// Получить путь до 1С exe для стабильной версии 8.2
        /// </summary>
        /// <returns>Путь до 1С exe для стабильной версии 8.2</returns>
        string GetPath1CExeStable82Platform(Domain.DataModels.Account account);

        /// <summary>
        /// Возвращает путь файлового хранилища клиентских файлов.
        /// </summary>
        /// <returns>Путь файлового хранилища клиентских файлов</returns>
        string GetBackupStorage(Domain.DataModels.Account account);

        string? GetWindowsThinkClientDownloadLink(Domain.DataModels.Account account,
            AccountDatabase accountDatabase);
    }
}
