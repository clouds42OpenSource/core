﻿using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;

namespace Clouds42.Accounts.Contracts.SegmentMigration.Interfaces
{
    /// <summary>
    /// Провайдер для миграции файловых баз аккаунта
    /// </summary>
    public interface IMigrateAccountFileDatabasesProvider
    {
        /// <summary>
        /// Перенести файловые базы аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат миграции инф. баз</returns>
        List<AccountDatabaseMigrationResultModelDto> MigrateAccountDatabases(ChangeAccountSegmentParamsDto model);
    }
}
