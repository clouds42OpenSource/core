﻿using Clouds42.DataContracts.Account;

namespace Clouds42.Accounts.Contracts.SegmentMigration.Interfaces
{
    /// <summary>
    /// Провайдер для смены файлового хранилища аккаунта
    /// </summary>
    public interface IChangeAccountFileStorageProvider
    {
        /// <summary>
        /// Сменить файловое хранилище для аккаунта
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        void ChangeAccountFileStorage(ChangeAccountSegmentParamsDto model);
    }
}
