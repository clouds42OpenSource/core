﻿using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.Supplier;

namespace Clouds42.Accounts.Contracts.Configuration.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными конфигурации аккаунта
    /// </summary>
    public interface IAccountConfigurationDataProvider
    {
        /// <summary>
        /// Получить конфигурацию аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Конфигурация аккаунта</returns>
        AccountConfiguration GetAccountConfiguration(Guid accountId);

        /// <summary>
        /// Получить модель конфигурации аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель конфигурации аккаунта</returns>
        AccountConfigurationDto GetAccountConfigurationDc(Guid accountId);

        /// <summary>
        /// Получить Id файлового хранилища аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id файлового хранилища аккаунта</returns>
        Guid GetAccountFileStorageId(Guid accountId);

        /// <summary>
        /// Получить Id локали аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id локали аккаунта</returns>
        Guid GetAccountLocaleId(Guid accountId);

        /// <summary>
        /// Получить локаль аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Локаль аккаунта</returns>
        Locale GetAccountLocale(Guid accountId);

        /// <summary>
        /// Получить локаль пользователя аккаунта
        /// </summary>
        /// <param name="accountUserId">Id пользователя аккаунта</param>
        /// <returns>Локаль пользователя аккаунта</returns>
        Locale GetAccountUserLocale(Guid accountUserId);

        /// <summary>
        /// Получить данные конфигурации аккаунта по локали
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные конфигурации аккаунта по локали</returns>
        AccountConfigurationByLocaleDataDto GetAccountConfigurationByLocaleData(Guid accountId);

        /// <summary>
        /// Получить конфигурации аккаунтов по локали
        /// </summary>
        /// <returns>Конфигурации аккаунтов по сегменту</returns>
        IQueryable<AccountConfigurationByLocaleDataDto> GetAccountConfigurationsDataByLocale();

        /// <summary>
        /// Проверить что аккаунт является VIP
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Признак указывающий что аккаунт VIP</returns>
        bool IsVipAccount(Guid accountId);

        Task<bool> IsVipAccountAsync(Guid accountId);

        /// <summary>
        /// Проверить что аккаунту нужно автосоздание серверной базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Признак указывающий что аккаунту нужно создать серверную базу </returns>
        bool NeedAutoCreateClusteredDb(Guid accountId);


        /// <summary>
        /// Получить модели аккаунтов с конфигурацией
        /// </summary>
        /// <returns>Модели аккаунтов с конфигурацией</returns>
        IQueryable<AccountWithConfigurationDto> GetAccountsWithConfiguration();

        /// <summary>
        /// Получить модель аккаунта с конфигурацией
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель аккаунта с конфигурацией</returns>
        AccountWithConfigurationDto GetAccountWithConfiguration(Guid accountId);

        /// <summary>
        /// Получить конфигурации аккаунтов по сегменту
        /// </summary>
        /// <returns>Конфигурации аккаунтов по сегменту</returns>
        IQueryable<AccountConfigurationBySegmentDataDto> GetAccountConfigurationsDataBySegment();

        /// <summary>
        /// Получить сегмент аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Сегмент аккаунта</returns>
        CloudServicesSegment GetAccountSegment(Guid accountId);

        /// <summary>
        /// Получить хранилище бэкапов по сегменту аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Хранилище бэкапов по сегменту аккаунта</returns>
        Task<CloudServicesBackupStorage> GetBackupStorageByAccountSegment(Guid accountId);

        /// <summary>
        /// Получить конфигурации аккаунтов по поставщику
        /// </summary>
        /// <returns>Конфигурации аккаунтов по поставщику</returns>
        IQueryable<AccountConfigurationBySupplierDataDto> GetAccountConfigurationsDataBySupplier();

        /// <summary>
        /// Получить поставщика аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Поставщик аккаунта</returns>
        Supplier GetAccountSupplier(Guid accountId);

        /// <summary>
        /// Получить данные конфигурации аккаунта по сегменту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные конфигурации аккаунта по сегменту</returns>
        AccountConfigurationBySegmentDataDto GetAccountConfigurationBySegmentData(Guid accountId);
    }
}
