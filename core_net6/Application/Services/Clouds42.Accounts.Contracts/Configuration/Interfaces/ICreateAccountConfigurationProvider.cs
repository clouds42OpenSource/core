﻿using Clouds42.DataContracts.AccountConfiguration;

namespace Clouds42.Accounts.Contracts.Configuration.Interfaces
{
    /// <summary>
    /// Провайдер для создания конфигурации аккаунта
    /// </summary>
    public interface ICreateAccountConfigurationProvider
    {
        /// <summary>
        /// Создать конфигурацию аккаунта
        /// </summary>
        /// <param name="model">Модель создания конфигурации аккаунта</param>
        void Create(AccountConfigurationDto model);
    }
}
