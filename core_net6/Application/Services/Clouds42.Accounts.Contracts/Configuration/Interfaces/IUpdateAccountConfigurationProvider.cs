﻿namespace Clouds42.Accounts.Contracts.Configuration.Interfaces
{
    /// <summary>
    /// Провайдер для обновления конфигурации аккаунта
    /// </summary>
    public interface IUpdateAccountConfigurationProvider
    {
        /// <summary>
        /// Обновить Id локали
        /// (так же обновит поставщика дефолтного по локали)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="localeId">Id локали</param>
        void UpdateLocaleId(Guid accountId, Guid localeId);

        /// <summary>
        /// Обновить признак указывающий что аккаунт вип
        /// </summary>
        /// <param name="accountId">Id аккаунт</param>
        /// <param name="isVipAccount">Признак указывающий что аккаунт вип</param>
        void UpdateSignVipAccount(Guid accountId, bool isVipAccount);

        /// <summary>
        /// Обновить Id файлового хранилища
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="fileStorageId">Id файлового хранилища</param>
        void UpdateFileStorageId(Guid accountId, Guid fileStorageId);

        /// <summary>
        /// Обновить ссылку на склеп аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="folderPrefix">Id папки аккаунта в склепе</param>
        void UpdateCloudStorageWebLink(Guid accountId, string folderPrefix);
    }
}
