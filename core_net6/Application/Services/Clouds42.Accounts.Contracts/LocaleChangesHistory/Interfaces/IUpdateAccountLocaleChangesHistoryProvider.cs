﻿using Clouds42.Domain.DataModels.History;

namespace Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces
{
    /// <summary>
    /// Провайдер для обновления истории изменения локали у аккаунта
    /// </summary>
    public interface IUpdateAccountLocaleChangesHistoryProvider
    {
        /// <summary>
        /// Обновить дату изменения у истории изменения локали аккаунта
        /// </summary>
        /// <param name="accountLocaleChangesHistory">История изменения локали у аккаунта</param>
        /// <param name="changesDate">Дата изменения</param>
        void UpdateChangesDate(AccountLocaleChangesHistory accountLocaleChangesHistory, DateTime changesDate);
    }
}