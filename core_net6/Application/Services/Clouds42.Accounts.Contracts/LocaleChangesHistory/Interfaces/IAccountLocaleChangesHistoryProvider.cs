﻿using Clouds42.Domain.DataModels.History;

namespace Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными истории
    /// изменения локали у аккаунта
    /// </summary>
    public interface IAccountLocaleChangesHistoryProvider
    {
        /// <summary>
        /// Получить изменение(историю изменения) локали для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>История изменения локали аккаунта</returns>
        AccountLocaleChangesHistory GetLocaleChangesHistoryForAccount(Guid accountId);
    }
}
