﻿namespace Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces
{
    /// <summary>
    /// Провайдер для создания истории изменения локали у аккаунта
    /// </summary>
    public interface ICreateAccountLocaleChangesHistoryProvider
    {
        /// <summary>
        /// Создать историю изменения локали у аккаунта
        /// (Дата изменения будет текущая)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void Create(Guid accountId);
    }
}
