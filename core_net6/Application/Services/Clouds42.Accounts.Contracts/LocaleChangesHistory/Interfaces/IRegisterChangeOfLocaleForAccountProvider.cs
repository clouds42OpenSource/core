﻿namespace Clouds42.Accounts.Contracts.LocaleChangesHistory.Interfaces
{
    /// <summary>
    /// Провайдер для регистрации изменения локали у аккаунта
    /// </summary>
    public interface IRegisterChangeOfLocaleForAccountProvider
    {
        /// <summary>
        /// Зарегестрировать изменение локали у аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void Register(Guid accountId);
    }
}
