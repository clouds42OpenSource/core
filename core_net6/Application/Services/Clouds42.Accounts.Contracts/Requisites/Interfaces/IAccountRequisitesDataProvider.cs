﻿using Clouds42.Domain.DataModels.AccountProperties;

namespace Clouds42.Accounts.Contracts.Requisites.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными реквизитов аккаунта
    /// </summary>
    public interface IAccountRequisitesDataProvider
    {
        /// <summary>
        /// Получить реквизиты аккаунта
        /// или создать новые (если записи не существует)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Реквизиты аккаунта</returns>
        AccountRequisites GetIfExistsOrCreateNew(Guid accountId);
    }
}
