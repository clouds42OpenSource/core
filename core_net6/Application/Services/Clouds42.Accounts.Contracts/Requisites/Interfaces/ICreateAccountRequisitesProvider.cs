﻿using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.AccountProperties;

namespace Clouds42.Accounts.Contracts.Requisites.Interfaces
{
    /// <summary>
    /// Провайдер для создания реквизитов аккаунта
    /// </summary>
    public interface ICreateAccountRequisitesProvider
    {
        /// <summary>
        /// Создать реквизиты аккаунта
        /// </summary>
        /// <param name="model">Модель создания реквизитов аккаунта</param>
        /// <returns>Реквизиты аккаунта</returns>
        AccountRequisites Create(AccountRequisitesDto model);
    }
}
