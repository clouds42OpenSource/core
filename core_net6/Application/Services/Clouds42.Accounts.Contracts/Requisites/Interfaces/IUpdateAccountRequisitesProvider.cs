﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Accounts.Contracts.Requisites.Interfaces
{
    /// <summary>
    /// Провайдер для обновления реквизитов аккаунта
    /// </summary>
    public interface IUpdateAccountRequisitesProvider
    {
        /// <summary>
        /// Обновить реквизиты аккаунта
        /// </summary>
        /// <param name="model">Модель реквизитов аккаунта</param>
        void Update(AccountRequisitesDto model);
    }
}
