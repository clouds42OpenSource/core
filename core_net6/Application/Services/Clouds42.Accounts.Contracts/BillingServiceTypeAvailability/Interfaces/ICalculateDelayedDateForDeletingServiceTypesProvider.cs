﻿namespace Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces
{
    /// <summary>
    /// Провайдер для подсчета отложденной даты удаления услуг
    /// </summary>
    public interface ICalculateDelayedDateForDeletingServiceTypesProvider
    {
        /// <summary>
        /// Вычислить отложденную дату удаления услуг 
        /// </summary>
        /// <param name="expireDateTime">Дата окончания активности Аренды 1С</param>
        /// <returns>Отложденная дата удаления услуг</returns>
        DateTime Calculate(DateTime expireDateTime);
    }
}
