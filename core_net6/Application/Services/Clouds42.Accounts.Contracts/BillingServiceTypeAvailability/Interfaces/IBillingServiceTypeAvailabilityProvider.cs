﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces
{
    /// <summary>
    /// Провайдер для установки времени доступности услуги сервиса после ее удаления
    /// </summary>
    public interface IBillingServiceTypeAvailabilityProvider
    {
        /// <summary>
        /// Установить время активности для аккаунтов
        /// </summary>
        /// <param name="model">Модель отложенного удаления услуг сервиса для аккаунтов</param>
        void SetAvailabilityDateTimeForAccounts(DelayedServiceTypesDeletionForAccountsDto model);

        /// <summary>
        /// Удалить время активности для аккаунта
        /// </summary>
        /// <param name="billingServiceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        void DeleteAvailabilityDateTimeForAccount(Guid billingServiceTypeId, Guid accountId);

        /// <summary>
        /// Обновить время активности для аккаунта
        /// </summary>
        /// <param name="billingServiceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="expireDateTime">Дата окончания активности</param>
        void UpdateAvailabilityDateTimeForAccount(Guid billingServiceTypeId, Guid accountId, DateTime expireDateTime);

        /// <summary>
        /// Удалить время активности для аккаунта по ID сервиса
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        void DeleteAvailabilityDateTimeForAccountByBillingServiceId(Guid billingServiceId, Guid accountId);
    }
}
