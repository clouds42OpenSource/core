﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces
{
    public interface IBillingServiceTypeAvailabilityManager
    {
        ManagerResult DeleteAvailabilityDateTimeForAccountByBillingServiceId(Guid billingServiceId, Guid accountId);
        ManagerResult UpdateAvailabilityDateTimeForAccount(Guid billingServiceTypeId, Guid accountId, DateTime expireDateTime);
    }
}