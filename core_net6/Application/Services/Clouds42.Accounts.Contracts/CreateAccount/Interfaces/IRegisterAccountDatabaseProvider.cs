﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Accounts.Contracts.CreateAccount.Interfaces
{
    /// <summary>
    /// Провайдер регистрации информационной базы.
    /// </summary>
    public interface IRegisterAccountDatabaseProvider
    {
        /// <summary>
        /// Зарегистрировать базу по шаблону.
        /// </summary>
        /// <param name="accountUser">Пользователь.</param>
        /// <param name="dbTemplate">Шаблон.</param>
        /// <returns>ID базы, созданной по шаблону</returns>
        Guid RegisterByTemplate(IAccountUser accountUser, DbTemplate dbTemplate);
    }
}