﻿using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.Account.Registration;

namespace Clouds42.Accounts.Contracts.CreateAccount.Interfaces
{
    public interface ICreateAccountProvider
    {
        /// <summary>
        /// Создать новый аккаунт и сессию для доступа по АПИ.
        /// </summary>
        Task<AccountAuthDto> AddAccountAndCreateSession(AccountRegistrationModelDto accountInfo);

        /// <summary>
        /// Create a new account without registration of services and create session for API access
        /// </summary>
        /// <param name="accountInfo">Registration model</param>
        /// <returns>Auth info</returns>
        Task<AccountAuthDto> AddAccountWithoutServicesAndCreateSession(AccountRegistrationModelDto accountInfo);

        /// <summary>
        /// Создать новый аккаунт.
        /// </summary>
        /// <param name="accountInfo">регистрационная модель</param>
        /// <returns>Созданный аккаунт</returns>
        Task<AccountRegistrationResultDto> CreateAccount(AccountRegistrationModelDto accountInfo);

    }
}
