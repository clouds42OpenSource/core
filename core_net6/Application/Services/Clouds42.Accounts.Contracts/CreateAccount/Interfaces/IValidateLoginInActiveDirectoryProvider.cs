﻿namespace Clouds42.Accounts.Contracts.CreateAccount.Interfaces
{
    /// <summary>
    /// Провайдер валидации пользователя в АД
    /// </summary>
    public interface IValidateLoginInActiveDirectoryProvider
    {
        /// <summary>
        /// /Выполнить валидацию пользователя в АД
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        void Validate(string login);
    }
}
