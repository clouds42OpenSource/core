﻿namespace Clouds42.Accounts.Contracts.CreateAccount.Interfaces
{
    public interface IReCaptchaValidator
    {
        bool IsCaptchaPassed(string token);
    }
}
