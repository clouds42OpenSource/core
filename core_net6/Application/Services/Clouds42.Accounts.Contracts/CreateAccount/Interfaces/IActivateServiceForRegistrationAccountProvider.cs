﻿using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Accounts.Contracts.CreateAccount.Interfaces
{
    /// <summary>
    /// Провайдер по активации сервиса для зарегистрированного аккаунта.
    /// </summary>
    public interface IActivateServiceForRegistrationAccountProvider
    {
        /// <summary>
        ///  Попытаться активировать сервисы для нового аккаунта
        /// </summary>
        /// <param name="registrationConfig">Требуемые сервисы.</param>
        /// <param name="accountUser">Аккаунт админ созданного аккаунта.</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        bool TryActivateServicesForNewAccount(RegistrationConfigDomainModelDto registrationConfig,
            IAccountUser accountUser, out string errorMessage);

        /// <summary>
        /// Активировать сервис у существуещего пользователя.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        /// <param name="rent1CActivationType">Тип активации Аренды 1С</param>
        ServiceActivationResultDto ActivateServiceForExistingAccount(Guid serviceId, Guid accountUserId,
            Rent1CActivationType rent1CActivationType = Rent1CActivationType.ForExistingAccount);

        /// <summary>
        /// Активировать сервис у существуещего пользователя, без его установки в базу.
        /// </summary>
        /// <param name="serviceId">Номер сервиса.</param>
        /// <param name="accountUserId">Номер пользователя.</param>
        /// <param name="rent1CActivationType">Тип активации Аренды 1С</param>
        ServiceActivationResultDto ActivateServiceForExistingAccountWithoutServiceInstallation(Guid serviceId, Guid accountUserId,
            Rent1CActivationType rent1CActivationType = Rent1CActivationType.ForExistingAccount);
    }
}