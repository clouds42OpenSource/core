﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Accounts.Contracts.CreateAccount.Interfaces
{
    /// <summary>
    /// Активатор Аренды 1С
    /// </summary>
    public interface IRent1CForCustomServiceActivator
    {
        /// <summary>
        /// Активировать Аренду 1С
        /// </summary>
        /// <param name="rent1CActivationParams">Параметры активации Аренды 1С</param>
        void ActivateRent1C(Rent1CActivationParamsDto rent1CActivationParams);
    }
}
