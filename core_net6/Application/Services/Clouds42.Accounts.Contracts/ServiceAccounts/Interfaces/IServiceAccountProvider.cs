﻿using Clouds42.DataContracts.Account.AccountsService;

namespace Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces
{
    /// <summary>
    /// Провайдер данных служебных аккаунтов
    /// </summary>
    public interface IServiceAccountProvider
    {
        /// <summary>
        /// Удалить служебный аккаунт
        /// </summary>
        /// <param name="serviceAccountId">ID служебного аккаунта</param>
        void DeleteServiceAccount(Guid serviceAccountId);

        /// <summary>
        /// Получить список служебных аккаунтов
        /// </summary>
        /// <param name="args">Фильтр</param>
        /// <returns>Список служебных аккаунтов</returns>
        GetServiceResultAccountsDto GetServiceAccounts(GetServiceAccountsDto args);


        /// <summary>
        /// Добавить служебный аккаунт
        /// </summary>
        /// <param name="args">Модель создания служебного аккаунта</param>
        /// <returns>Модель результата после создания служебного аккаунта</returns>
        AddServiceAccountResultDto AddServiceAccountItem(CreateServiceAccountDto args);
    }
}
