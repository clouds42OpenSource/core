﻿namespace Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces
{
    public interface IServiceAccountDataHelper
    {
        bool IsServiceAccount(Guid accountId);
        Task<bool> IsServiceAccountAsync(Guid accountId, CancellationToken cancellationToken);
        void CheckAbilityToManageService(Guid accountId);
    }
}
