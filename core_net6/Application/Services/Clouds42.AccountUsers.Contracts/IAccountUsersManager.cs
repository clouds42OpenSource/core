﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.DataContracts.AccountUser.AccountUserBitrixChatInfo;

namespace Clouds42.AccountUsers.Contracts
{
    public class ResultDto<T>
    {
        public T Data { get; set; }
        public string Head { get; set; }
    }

    public interface IAccountUsersManager
    {
        ManagerResult<List<AccountUserBitrixChatInfoItemDto>> GetUserInfoForBitrix(Guid currentUserId, string currentUrl);
        ManagerResult<bool> IsLastAdminAccount(Guid accountId);
        ManagerResult<AccountUserDto> Get(Guid id);
        ManagerResult<List<AccountUserPropertiesDto>> GetAccountUsersList(Guid accountId);
        Domain.DataModels.AccountUser GetCurrent();
        ManagerResult<AccountUserIdAndVerifireStatusDTO> GetIdByEmail(string email, out string headString);
        ManagerResult<Guid> GetIdByLogin(string login, out string headString);
        ManagerResult<AccountUserIdAndVerifireStatusDTO> GetIdByPhoneNumber(string phoneNumber);
        ManagerResult<ResultDto<List<Guid>>> GetIDs(Guid accountId);
        ManagerResult<string> GetLockServiceInformation(Guid accountId);
        ManagerResult<Domain.DataModels.AccountUser> GetProperties(Guid accountUserId);
        ManagerResult<ResultDto<bool>> IsAccountAdmin(Guid accountUserId);
        bool IsManager(Guid accountUserId);
        ManagerResult SetActivated(AccountUserActivatedModel model);
        ManagerResult SetFirstName(AccountUserFirstNameModelDto model);
        ManagerResult SetLastName(AccountUserLastNameModelDto model);
        ManagerResult SetMiddleName(AccountUserMiddleNameModelDto model);
        Task<ManagerResult> SetPhoneNumber(PhoneNumberModelDto model);
        AccountUserDto Sync(Guid accountUserId, out bool isUpdatedByCorp);
        Task<ManagerResult> Unsubscribed(string key, string type);
    }
}
