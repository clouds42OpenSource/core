﻿namespace Clouds42.AccountUsers.Contracts.ActivateAccountUser.Interfaces
{
    /// <summary>
    /// Провайдер активации пользователей
    /// </summary>
    public interface IActivateAccountUserProvider
    {
        /// <summary>
        /// Активировать по номеру телефона
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="confirmationCode">Код подтверждения</param>
        /// <returns>Результат активации</returns>
        bool ActivateUserByPhone(Guid accountUserId, string confirmationCode);
    }
}
