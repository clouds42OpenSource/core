﻿using System.Xml.Serialization;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces.Models;

namespace Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models
{
    [XmlRoot(ElementName = "Request")]
    public class SauriOpenIdChangeUserPropertiesModel : IOpenIdChangeUserPropertiesModel
    {
        [XmlElement]
        public Guid AccountUserID { get; set; }

        [XmlElement]
        public string Password { get; set; }

        [XmlElement]
        public string Login { get; set; }

        [XmlElement]
        public string Email { get; set; }

        [XmlElement]
        public string Phone { get; set; }

        [XmlElement]
        public string FullName { get; set; }

    }
}