﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models
{

    public class SauriOpenIdControlUserModel
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [JsonIgnore]
        public Guid AccountUserID { get; set; }

        /// <summary>
        /// Идентификатор аккаунта
        /// </summary>
        [JsonProperty("account-id")]
        public Guid AccountID { get; set; }

        /// <summary>
        /// Пароль 
        /// </summary>
        [DefaultValue("")]
        [JsonProperty("password", NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Password { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [DefaultValue("")]
        [JsonProperty("login", NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Login { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        [DefaultValue("")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Name { get; set; }

        /// <summary>
        /// Почта
        /// </summary>
        [DefaultValue("")]
        [JsonProperty("email", NullValueHandling = NullValueHandling.Ignore)]
        public string? Email { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        [DefaultValue("")]
        [JsonProperty("phone", NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Phone { get; set; }

        /// <summary>
        /// Полное имя пользователя
        /// </summary>
        [DefaultValue("")]
        [JsonProperty("full-name", NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string FullName { get; set; }

        /// <summary>
        /// призднак админ или нет
        /// </summary>
        [DefaultValue("null")]
        [JsonProperty("is-admin", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsAdmin { get; set; }
    }
}
