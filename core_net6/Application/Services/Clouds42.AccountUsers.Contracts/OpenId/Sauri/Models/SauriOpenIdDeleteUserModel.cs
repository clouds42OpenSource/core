﻿using System.Xml.Serialization;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces.Models;

namespace Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models
{
    [XmlRoot(ElementName = "Request")]
    public class SauriOpenIdDeleteUserModel : IOpenIdDeleteUserModel
    {

        [XmlElement]
        public Guid AccountUserID { get; set; } = Guid.Empty;

    }
}

