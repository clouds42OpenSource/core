﻿using System.Xml.Serialization;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces.Models;

namespace Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models
{
    [XmlRoot(ElementName = "Request")]
    public class SauriOpenIdAddUserModel : IOpenIdAddUserModel
    {
        [XmlElement]
        public Guid AccountUserID { get; set; } = Guid.Empty;

        [XmlElement]
        public Guid AccountID { get; set; } = Guid.Empty;

        [XmlElement]
        public string Login { get; set; } = string.Empty;

        [XmlElement]
        public string Name { get; set; } = string.Empty;

        [XmlElement]
        public string Password { get; set; } = string.Empty;

        [XmlElement]
        public string Email { get; set; } = string.Empty;

        [XmlElement]
        public string Phone { get; set; } = string.Empty;

        [XmlIgnore]
        public string UserSource { get; set; }
    }
}