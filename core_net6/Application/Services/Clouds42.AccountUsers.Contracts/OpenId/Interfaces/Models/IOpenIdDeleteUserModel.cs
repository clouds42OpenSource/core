﻿namespace Clouds42.AccountUsers.Contracts.OpenId.Interfaces.Models
{
    /// <summary>
    /// Модель данных для удаления пользователя
    /// </summary>
    public interface IOpenIdDeleteUserModel
    {
    }
}