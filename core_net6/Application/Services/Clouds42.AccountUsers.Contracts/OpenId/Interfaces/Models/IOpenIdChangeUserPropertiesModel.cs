﻿namespace Clouds42.AccountUsers.Contracts.OpenId.Interfaces.Models
{
    /// <summary>
    ///     Модель данных для смены свойств у пользователя
    /// </summary>
    public interface IOpenIdChangeUserPropertiesModel
    {
    }
}