﻿using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;

namespace Clouds42.AccountUsers.Contracts.OpenId.Interfaces
{
    /// <summary>
    ///     OpenId провайдер
    /// </summary>
    public interface IOpenIdProvider
    {
        /// <summary>
        /// Создать пользователя в OpenId
        /// </summary>
        /// <param name="model">Модель данных</param>
        /// <param name="locale">Локаль пользователя</param>
        void AddUser(SauriOpenIdControlUserModel model, string locale);

        /// <summary>
        /// Удалить пользователя в OpenId
        /// </summary>
        /// <param name="model">Модель данных</param>
        void DeleteUser(SauriOpenIdControlUserModel model);

        /// <summary>
        /// Изменить свойства для пользователя в OpenId
        /// </summary>
        /// <param name="model">Модель данных</param>
        /// <param name="locale">Локаль пользователя</param>
        void ChangeUserProperties(SauriOpenIdControlUserModel model, string locale);

    }
}
