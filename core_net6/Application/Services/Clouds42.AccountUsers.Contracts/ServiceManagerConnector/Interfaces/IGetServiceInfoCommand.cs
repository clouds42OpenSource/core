﻿
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// <summary>
    /// Комманда для получения информации о сервисе из МС
    /// </summary>
    public interface IGetServiceInfoCommand
    {
        /// <summary>
        /// Получить информации о сервисе из МС
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Статус расширения</returns>
        InformationForServiceDto? Execute(Guid serviceId);
    }
}
