﻿
namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для получения списка конфигураций информационных баз
    /// </summary>
    public interface IGetConfigurationDatabaseCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для получения списка конфигураций информационных баз для сервиса
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        List<String> Execute(Guid serviceId);

        Task<List<string>> ExecuteAsync(Guid serviceId, CancellationToken cancellationToken);
    }
}
