﻿
namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    public interface IGetFullPathForDtFileCommand
    {
        string Execute(Guid fileId, Guid accountId);
    }
}
