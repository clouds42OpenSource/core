﻿
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для получения статуса установкисервиса в информационную базу
    /// </summary>
    public interface IGetServiceExtensionDatabaseStatusCommand
    {
        /// <summary>
        /// Получить статус расширения сервиса для информационной базы
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        /// <returns>Статус расширения</returns>
        ServiceExtensionDatabaseStatusDto Execute(Guid serviceId, Guid accountDatabaseId);
    }
}
