﻿using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Команда для запрос по завершению всех сеансов в базе на разделителях
    /// </summary>
    public interface ICloseSessionInDbOnDelimitersCommand
    {
        /// <summary>
        /// Выполнить запрос по завершению всех сеансов в базе на разделителях
        /// </summary>
        /// <param name="accountDatabase">информационные базы.</param>
        /// <param name="model">модель</param>
        TerminateSessionsInDatabaseJobParamsDto Execute(AccountDatabase accountDatabase,
            TerminateSessionsInDatabaseJobParamsDto model);
    }
}
