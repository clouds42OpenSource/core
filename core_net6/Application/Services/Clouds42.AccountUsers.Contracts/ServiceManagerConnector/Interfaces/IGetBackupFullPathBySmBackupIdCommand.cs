﻿
namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    public interface IGetBackupFullPathBySmBackupIdCommand
    {
        string Execute(Guid accountDatabaseBackupId);
    }
}