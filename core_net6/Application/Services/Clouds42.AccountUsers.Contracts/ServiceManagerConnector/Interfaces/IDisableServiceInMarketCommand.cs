﻿
namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для отключения сервиса в маркете
    /// </summary>
    public interface IDisableServiceInMarketCommand
    {
        /// <summary>
        /// Отключить сервис от маркета
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="isActive">флаг активности сервиса</param>
        void Execute(Guid serviceId, bool isActive);
    }
}
