﻿using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для установки расширения сервиса в информационную базу
    /// </summary>
    public interface IInstallServiceExtensionDatabaseCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для установки расширения в базу
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        ManageServiceExtensionDatabaseResultDto Execute(ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState);
    }
}
