﻿
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для получения списка информационных баз доступных для установки сервиса
    /// </summary>
    public interface IGetDatabasesForServicesExtensionCommand
    {
        /// <summary>
        /// Получить список информационных баз доступных для установки сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список ИБ для установки расширения</returns>
        List<DatabaseForServiceExtensionDto>? Execute(Guid accountId, Guid serviceId);
        Task<List<DatabaseForServiceExtensionDto>> ExecuteAsync(Guid accountId, Guid serviceId);
    }
}
