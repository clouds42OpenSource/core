﻿using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для установки статуса активации
    /// расширения сервиса для инф. базы 
    /// </summary>
    public interface ISetServiceExtensionDatabaseActivationStatusCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для установки статуса активации
        /// расширения сервиса для инф. базы
        /// </summary>
        /// <param name="serviceExtensionDatabaseActivationStatusDto">Модель для установки статуса активации расширения сервиса
        /// для инф. базы</param>
        string Execute(SetServiceExtensionDatabaseActivationStatusDto serviceExtensionDatabaseActivationStatusDto);
    }
}
