﻿using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для удаления расширения сервиса из информационной базы
    /// </summary>
    public interface IDeleteServiceExtensionDatabaseCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для удаления расширения в базе
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        ManageServiceExtensionDatabaseResultDto Execute(ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState);
    }
}
