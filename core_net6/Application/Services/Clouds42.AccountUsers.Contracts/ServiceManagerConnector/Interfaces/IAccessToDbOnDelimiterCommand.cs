﻿using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Добавление/Удаление доступов пользователя к инф. базе
    /// </summary>
    public interface IAccessToDbOnDelimiterCommand
    {
        /// <summary>
        /// Выполнить запрос на предоставление/удаление доступа пользователя в инф. базу
        /// </summary>
        /// <param name="database">Информационная база.</param>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <param name="url">Адрес СМ.</param>
        /// <param name="corpParameters">Опционально. Список ролей пользователя.</param>
        /// <param name="isInstall">Операция установки доступов</param>
        ManageAcDbAccessResultDto Execute(AccountDatabase database, Guid userId, string url, string corpParameters, bool isInstall = true);
    }
}
