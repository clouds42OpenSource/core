﻿namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для начисления доп сеансов аккаунту
    /// </summary>
    public interface ICreateAccountUsersSessionsInMsCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для начисления доп сеансов
        /// </summary>
        /// <param name="commonSessions">кол-во общих сеансов
        /// <param name="userSessions">кол-во по пользователям сеансов
        /// <param name="accountId">идентификатор аккаунта
        public void CreateAccountUsersSessionsInMs (int commonSessions, List<DataContracts.Cloud42Services.AccountUserSession>? userSessions, Guid accountId);
    }
}
