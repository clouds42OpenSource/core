﻿namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    public interface IUploadFileCommand
    {
        string GetUploadedFileId(string uploadedFileName);
        void StopUploadedFile(string uploadedFileName);
        Guid UploadChunkFileBytes(List<KeyValuePair<string, object>> requestHeaders, byte[] chunkData, string uploadUrl);
    }
}
