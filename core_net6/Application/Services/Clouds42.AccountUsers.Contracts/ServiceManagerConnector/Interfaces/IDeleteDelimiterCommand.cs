﻿namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Удаление баз на разделителях из МС
    /// </summary>
    public interface IDeleteDelimiterCommand
    {
        /// <summary>
        /// Выполнения запроса на удаление базы на разделителях в МС
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор инф. база</param>
        /// <param name="locale">Локаль аккаунта</param>
        string Execute(Guid accountDatabaseId, string locale, string type = "manual");
    }
}
