﻿
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces
{
    /// <summary>
    /// Комманда для получения списка сервисов доступных для установки в информационную базу
    /// </summary>
    public interface IGetServicesExtensionForDatabaseCommand
    {
        /// <summary>
        /// Получить списка сервисов доступных для установки в информационную базу
        /// </summary>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        /// <returns>Статус расширения</returns>
        Task<List<AvailableServiceExtensionForDatabaseDto>> Execute(Guid accountDatabaseId);
    }
}
