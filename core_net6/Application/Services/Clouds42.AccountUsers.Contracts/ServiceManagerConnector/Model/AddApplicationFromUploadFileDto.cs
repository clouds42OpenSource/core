﻿using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Newtonsoft.Json;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса для создания
    /// базы на разделителях при загрузке файла из МС
    /// </summary>
    public class AddApplicationFromUploadFileDto
    {

        /// <summary>
        /// Список соответствия пользователей 1С из зип пакета с пользователями ЛК
        /// </summary>
        [JsonProperty("users")]
        public List<InfoAboutUserFromZipPackageDto> UsersList { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        [JsonProperty("account-id")]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id конфигуратора
        /// </summary>
        [JsonProperty("configuration")]
        public string ConfigurationId { get; set; }

        /// <summary>
        /// Имя созданной ИБ
        /// </summary>
        [JsonProperty("name")]
        public string ApplicationName { get; set; }

        /// <summary>
        /// ID загруженного файла от МС
        /// </summary>
        [JsonProperty("file-id")]
        public Guid? SmUploadedFileId { get; set; }

        /// <summary>
        /// Список сервисов
        /// </summary>
        [JsonProperty("extensions")]
        public List<ExtensionIds> ExtensionsList { get; set; }

    }

}
