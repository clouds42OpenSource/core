﻿using System.Xml.Serialization;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса для предоставления прав доступа в базе на разделителях.
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class AddApplicationUserDto
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        [XmlElement]
        public Guid UserID { get; set; }

        /// <summary>
        /// Номер зоны
        /// </summary>
        [XmlElement]
        public int Zone { get; set; }

        /// <summary>
        /// Роли при предоставлении доступа пользователям  МЦОБ
        /// </summary>
        [XmlElement]
        public string Role { get; set; }
    }
}
