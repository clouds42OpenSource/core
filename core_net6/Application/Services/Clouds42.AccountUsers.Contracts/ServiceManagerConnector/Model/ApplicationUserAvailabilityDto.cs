﻿using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса.
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ApplicationUserAvailability
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        [XmlElement]
        public Guid UserID { get; set; }
        /// <summary>
        /// Флаг блокировки, разблокировки
        /// </summary>
        [XmlElement]
        public bool Availability { get; set; }

    }
}