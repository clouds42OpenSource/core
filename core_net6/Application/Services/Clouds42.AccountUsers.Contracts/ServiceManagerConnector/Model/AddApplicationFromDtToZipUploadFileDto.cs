﻿using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Newtonsoft.Json;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса для создания
    /// базы на разделителях при загрузке dt в zip 
    /// </summary>
    public class AddApplicationFromDtToZipUploadFileDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        [JsonProperty("account-id")]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Имя созданной ИБ
        /// </summary>
        [JsonProperty("name")]
        public string ApplicationName { get; set; }

        /// <summary>
        /// ID загруженного файла от МС
        /// </summary>
        [JsonProperty("file-id")]
        public Guid? SmUploadedFileId { get; set; }

        /// <summary>
        /// Id конфигуратора
        /// </summary>
        [JsonProperty("configuration")]
        public string ConfigurationId { get; set; }

        /// <summary>
        /// тип загружаемого файла
        /// </summary>
        [JsonProperty("file-type")]
        public string FileType { get; set; }

        /// <summary>
        /// Администратор от базы в МС
        /// </summary>
        [JsonProperty("administrator")]
        public AdministrationInMs AdministrationInMs { get; set; } = new();

        /// <summary>
        /// Список соответствия пользователей 1С из зип пакета с пользователями ЛК
        /// </summary>
        [JsonProperty("users")]
        public List<InfoAboutUserFromZipPackageDto> UsersList { get; set; } = [];
    }

    public class AdministrationInMs
    {

        /// <summary>
        /// Логин администратора в базе
        /// </summary>
        [JsonProperty("login")]
        public string LoginAdmin { get; set; }

        /// <summary>
        /// Пароль администратора в базе
        /// </summary>
        [JsonProperty("password")]
        public string PasswordAdmin { get; set; }
    }

}
