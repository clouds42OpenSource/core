﻿using Newtonsoft.Json;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса.
    /// </summary>
    public class AddApplicationDto
    {
        /// <summary>
        /// Номера пользователей которым нужно предоставить доступ
        [JsonProperty("users")]
        public List<Guid> UsersList { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        [JsonProperty("account-id")]
        public Guid AccountId { get; set; }


        /// <summary>
        /// Id конфигуратора
        /// </summary>
        [JsonProperty("configuration")]
        public string ConfigurationId { get; set; }

        /// <summary>
        /// Имя созданной ИБ
        /// </summary>
        [JsonProperty("name")]
        public string ApplicationName { get; set; }

        /// <summary>
        /// Признак база с демо данными или нет(не обязательный параметр, по умолчанию false)
        /// </summary>
        [JsonProperty("demo")]
        public bool Demo { get; set; }

    }
}