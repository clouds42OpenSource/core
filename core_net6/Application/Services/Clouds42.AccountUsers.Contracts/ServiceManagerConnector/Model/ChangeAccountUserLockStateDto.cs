﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Параметры смены состояния блокировки пользователя
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class ChangeAccountUserLockStateDto
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        [XmlElement(ElementName = "UserID")]
        [DataMember(Name = "UserID")]
        public Guid UserId { get; set; }

        /// <summary>
        /// Флаг блокировки, разблокировки
        /// </summary>
        [XmlElement(ElementName = "Availability")]
        [DataMember(Name = "Availability")]
        public bool IsAvailable { get; set; }

        /// <summary>
        /// Список зон для блокирования
        /// </summary>
        [XmlElement(ElementName = "Zones")]
        [DataMember(Name = "Zones")]
        public string ZoneList { get; set; }
    }
}
