﻿using System.Xml.Serialization;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса для редактирования имени базы на разделителях.
    /// </summary>
    public class RenameApplicationDto
    {
        /// <summary>
        /// Id базы данных
        /// </summary>
        [XmlElement]
        public Guid AccountDatabaseID { get; set; }

        /// <summary>
        /// Имя базы данных
        /// </summary>
        [XmlElement]
        public string ApplicationName { get; set; }

    }
}