﻿using Newtonsoft.Json;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса для редактирования имени базы на разделителях.
    /// </summary>
    public class RenameApplicationApiModelDto
    {

        /// <summary>
        /// Имя базы данных
        /// </summary>
        [JsonProperty("name")]
        public string ApplicationName { get; set; }

    }
}