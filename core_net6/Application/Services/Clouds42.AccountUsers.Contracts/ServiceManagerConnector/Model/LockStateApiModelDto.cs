﻿using Newtonsoft.Json;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса для блокировки/разблокировки пользователя.
    /// </summary>
    public class LockStateApiModelDto
    {

        /// <summary>
        /// Состояние блокировки
        /// </summary>
        [JsonProperty("locked")]
        public bool LockedState { get; set; }

    }
}