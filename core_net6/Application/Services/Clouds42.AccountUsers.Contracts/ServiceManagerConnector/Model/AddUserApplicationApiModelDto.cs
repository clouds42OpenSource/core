﻿using Newtonsoft.Json;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса для добавления ролей пользователям 
    /// в базы на разделителях.
    /// </summary>
    public class AddUserApplicationApiModelDto
    {

        /// <summary>
        /// Имя базы данных
        /// </summary>
        [JsonProperty("roles")]
        public List<string> CorpParameters { get; set; }

    }
}