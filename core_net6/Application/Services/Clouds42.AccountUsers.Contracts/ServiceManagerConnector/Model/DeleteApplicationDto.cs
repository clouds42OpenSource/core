﻿using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model
{
    /// <summary>
    /// Модель отправляемого запроса для удаления базы на разделителях.
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class DeleteApplicationDto
    {
        /// <summary>
        /// Id базы данных
        /// </summary>
        [XmlElement]
        public Guid AccountDatabaseID { get; set; }

    }
}
