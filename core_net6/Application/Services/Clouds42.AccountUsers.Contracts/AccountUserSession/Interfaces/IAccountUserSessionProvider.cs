﻿using Clouds42.DataContracts.AccountUser.AccountUser.Promo;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;

namespace Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces
{
    /// <summary>
    /// Провайдер сессий пользователей
    /// </summary>
    public interface IAccountUserSessionProvider
    {
        /// <summary>
        /// Проверить пароль пользователя
        /// </summary>
        /// <param name="loginModel">Модель данных для логина</param>
        /// <returns>true - если хэш введенного пароля совпадает со значением в базе</returns>
        bool ValidateAccountUserPassword(AccountLoginModelDto loginModel);

        /// <summary>
        /// Создать сессию
        /// </summary>
        /// <param name="loginModel">Модель данных для логина</param>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <returns>Токен пользователя</returns>
        Guid CreateSession(LoginModelDto loginModel, Guid accountUserId);
    }
}
