﻿using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;

namespace Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces
{
    /// <summary>
    /// Провайдер входа пользователя через сессию
    /// </summary>
    public interface IAccountUserSessionLoginProvider
    {
        /// <summary>
        /// Войти с помощью почты
        /// </summary>
        /// <param name="email">Почта</param>
        /// <param name="pass">Пароль</param>
        /// <returns>ID сессии</returns>
        Guid LoginByEmail(string email, string pass);

        /// <summary>
        /// Выполнить вход в облако по средствам логин/пароля. Создается запись с новой сессией пользователя.
        /// </summary>
        /// <param name="model">Параметры входа.</param>
        /// <returns>Идентификатор созданной сессии. SessionToken.</returns>
        /// <remarks>
        /// Сопоставление логина и почты необходимо, тк есть возможность авторизоваться в Линк посредством почты,
        /// которая передается через логин (a.Email == model.AccountUserLogin)
        /// </remarks>
        Guid Login(LoginModelDto model);

        /// <summary>
        /// Выполнить вход в облако по средствам логин для межсервисного взаимодействия. Создается запись с новой сессией пользователя.
        /// </summary>
        /// <param name="model">Параметры входа.</param>
        /// <returns>Идентификатор созданной сессии. SessionToken.</returns>
        Guid CrossServiceLogin(CrossServiceLoginModelDto model);
    }
}
