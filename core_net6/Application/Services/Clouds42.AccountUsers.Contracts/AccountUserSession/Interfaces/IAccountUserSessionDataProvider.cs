﻿namespace Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces
{
    /// <summary>
    /// Провайдер для получения данных по сессиям пользователя
    /// </summary>
    public interface IAccountUserSessionDataProvider
    {
        /// <summary>
        /// Получить сессию пользователя
        /// </summary>
        /// <param name="sessionId">ID сессии</param>
        /// <returns>Сессия пользователя</returns>
        Domain.DataModels.AccountUserSession GetSessionsById(Guid sessionId);

        /// <summary>
        /// Получить все сессии пользователя
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <returns>Список сессий пользователя</returns>
        List<Domain.DataModels.AccountUserSession> GetAllSessionsByUserId(Guid userId);

        /// <summary>
        /// Получить логин пользователя по токену
        /// </summary>
        /// <param name="token">Токен сессии</param>
        /// <returns>Логин пользователя</returns>
        string GetLoginByToken(Guid token);
    }
}
