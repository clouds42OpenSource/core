﻿using Clouds42.DataContracts.AccountUser;

namespace Clouds42.AccountUsers.Contracts.ManageAccountUserLockState.Interfaces
{
    /// <summary>
    /// Провайдер управления блокировкой пользователей 
    /// </summary>
    public interface IAccountUserLockStateProvider
    {
        /// <summary>
        /// Сменить состояние блокировки
        /// </summary>
        /// <param name="manageAcUsrLockStateParams">Параметры управления состоянием блокировки пользователя в МС</param> 
        void ChangeLockState(ManageAccountUserLockStateParamsDto manageAcUsrLockStateParams);
    }
}
