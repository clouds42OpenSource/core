﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Access;
using Microsoft.AspNetCore.Mvc;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Models
{
    /// <summary>
    /// Account user view model
    /// </summary>
    public class AccountUserViewModel : IAccountUserViewModel
    {
        /// <summary>
        /// User identifier
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Is user activated
        /// </summary>
        [DisplayName("Активирован")]
        public bool IsActivated { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        [DisplayName("Логин")]
        [Required(ErrorMessage = "Это поле должно быть заполнено")]
        [Remote("IsLoginAvailable", "AccountUsersValidation")]
        [StringLength(20, ErrorMessage = "Допустимое количество символов: 3–20", MinimumLength = 3)]
        public string? Login { get; set; }

        /// <summary>
        /// Authorization token
        /// </summary>
        [DisplayName("Токен доступа")]
        [RegularExpression(@"[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}",
            ErrorMessage = "Недопустимое значение")]
        public Guid? AuthToken { get; set; }

        /// <summary>
        /// User roles
        /// </summary>
        [DisplayName("Роли пользователя")]
        public List<AccountUserGroup> Roles { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        [DisplayName("Фамилия")]
        [StringLength(30, ErrorMessage = "Допустимое количество символов не более 30")]
        [RegularExpression(@"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z ]+$", ErrorMessage = "Недопустимое значение")]
        public string? LastName { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        [DisplayName("Имя")]
        [StringLength(30, ErrorMessage = "Допустимое количество символов не более 30")]
        [RegularExpression(@"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z ]+$", ErrorMessage = "Недопустимое значение")]
        public string? FirstName { get; set; }

        /// <summary>
        /// User middle name
        /// </summary>
        [DisplayName("Отчеcтвo")]
        [StringLength(30, ErrorMessage = "Допустимое количество символов не более 30")]
        [RegularExpression(@"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z ]+$", ErrorMessage = "Недопустимое значение")]
        public string? MidName { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Current password
        /// </summary>
        [DisplayName("Текущий пароль")]
        [StringLength(25)]
        public string? CurrentPassword { get; set; }

        /// <summary>
        /// New password
        /// </summary>
        [DisplayName("Новый пароль")]
        [StringLength(25, ErrorMessage = "Допустимо 7 - 25 символов")]
        [RegularExpression(@"^[0-9A-Za-z_\\/\,.\-:;|!@#$]*$", ErrorMessage = "Недопустимое значение")]
        public string? Password { get; set; }

        /// <summary>
        /// Password confirmation
        /// </summary>
        [DisplayName("Подтверждение пароля")]
        [Compare(nameof(Password), ErrorMessage = "Пароль и его подтверждение должны совпадать")]
        public string? ConfirmPassword { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>
        [DisplayName("Телефон")]
        [Remote("IsPhoneAvailable", "AccountUsersValidation", AdditionalFields = nameof(Id))]
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// Is user account admin
        /// </summary>
        public bool IsAccountAdmin { set; get; }

        /// <summary>
        /// Is profile mine
        /// </summary>
        public bool IsMyProfile { get; set; }

        /// <summary>
        /// Ability to delete users
        /// </summary>
        public bool CanAccountUsersDelete { get; set; }

        /// <summary>
        /// Need to confirm current password
        /// </summary>
        public bool NeedInputCurrentPassword { get; set; }

        /// <summary>
        /// Has user active connections
        /// </summary>
        public bool HasActiveConnections { get; set; }
    }
}
