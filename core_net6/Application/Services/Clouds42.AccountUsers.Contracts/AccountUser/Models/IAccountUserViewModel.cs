﻿using Clouds42.Domain.Access;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Models
{
    public interface IAccountUserViewModel
    {
        Guid? Id { get; set; }

        bool IsActivated { get; set; }

        string Login { get; set; }
        
        Guid? AuthToken { get; set; }
        
        List<AccountUserGroup> Roles { get; set; }
       
        string LastName { get; set; }
       
        string FirstName { get; set; }
       
        string MidName { get; set; }
    
        string Email { get; set; }
        
        string CurrentPassword { get; set; }
        
        string Password { get; set; }

        string ConfirmPassword { get; set; }
        
        string PhoneNumber { get; set; }
    }
}
