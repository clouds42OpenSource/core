﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.DataValidations;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.Access;
using Microsoft.AspNetCore.Mvc;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Models
{
    public class AccountUserEditModel
    {
        /// <summary>
        /// Account user identifier
        /// </summary>
        [Required(ErrorMessage = $"The field {nameof(Id)} is required")]
        public Guid Id { get; set; }

        /// <summary>
        /// User account identifier
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Is user activated
        /// </summary>
        public bool? IsActivated { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        [Required(ErrorMessage = $"The field {nameof(Login)} is required")]
        [Remote("IsLoginAvailable", "AccountUsersValidation")]
        [StringLength(20, ErrorMessage = "Allowed number of characters: 3-20", MinimumLength = 3)]
        public string Login { get; set; }

        /// <summary>
        /// Authorization token
        /// </summary>
        [RegularExpression(@"[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}", ErrorMessage = "Invalid value")]
        public Guid? AuthToken { get; set; }

        /// <summary>
        /// User roles
        /// </summary>
        public List<AccountUserGroup> Roles { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        [StringLength(30, ErrorMessage = "Allowed number of characters is no more than 30")]
        [RegularExpression(@"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z ]+$", ErrorMessage = "Invalid value")]
        public string? LastName { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        [StringLength(30, ErrorMessage = "Allowed number of characters no more than 30")]
        [RegularExpression(@"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z ]+$", ErrorMessage = "Invalid value")]
        public string? FirstName { get; set; }

        /// <summary>
        /// User middle name
        /// </summary>
        [StringLength(30, ErrorMessage = "Allowed number of characters no more than 30")]
        [RegularExpression(@"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z ]+$", ErrorMessage = "Invalid value")]
        public string? MidName { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        [Required(ErrorMessage = $"The field {nameof(Email)} is required")]
        [Remote("IsEmailAvailable", "AccountUsersValidation", AdditionalFields = nameof(Id))]
        [RegularExpression(EmailValidation.Pattern, ErrorMessage = "Invalid value")]
        public string? Email { get; set; }

        /// <summary>
        /// Current password
        /// </summary>
        [StringLength(25)]
        public string? CurrentPassword { get; set; }

        /// <summary>
        /// New password
        /// </summary>
        [StringLength(25)]
        [RegularExpression(@"^[0-9A-Za-z_\\/\,.\-:;|!@#$]*$", ErrorMessage = "Invalid value")]
        public string? Password { get; set; }

        /// <summary>
        /// Password confirmation
        /// </summary>
        [Compare(nameof(Password), ErrorMessage = "'{1}' and '{0}' must match")]
        public string? ConfirmPassword { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>
        [Remote("IsPhoneAvailable", "AccountUsersValidation", AdditionalFields = nameof(Id))]
        public string? PhoneNumber { get; set; }

        public AccountUserDto ToAccountUserDto(Guid accountId)
        {
            var model = new AccountUserDto
            {
                Id = this.Id,
                Roles = this.Roles,
                AccountId = accountId,
                AuthToken = this.AuthToken,
                Activated = this.IsActivated ?? false,
                Password = this.Password,
                Email = this.Email,
                PhoneNumber = string.IsNullOrWhiteSpace(this.PhoneNumber) ? null : this.PhoneNumber,
                Login = this.Login,
                FirstName = string.IsNullOrWhiteSpace(this.FirstName) ? null : this.FirstName.TrimStart().TrimEnd(),
                LastName = string.IsNullOrWhiteSpace(this.LastName) ? null : this.LastName.TrimStart().TrimEnd(),
                MiddleName = string.IsNullOrWhiteSpace(this.MidName) ? null : this.MidName.TrimStart().TrimEnd()
            };

            return model;
        }
    }
}
