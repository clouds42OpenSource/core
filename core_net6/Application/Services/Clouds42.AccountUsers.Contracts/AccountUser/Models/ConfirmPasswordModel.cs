﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Models
{
    /// <summary>
    /// Модель подтверждения пароля
    /// </summary>
    public class ConfirmPasswordModel
    {
        /// <summary>
        /// Пароль
        /// </summary>
        [Required(ErrorMessage = "Поле пароль не должно быть пустым")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^[\S]+$", ErrorMessage = "Пароль не должен содержать пробелов")]
        [StringLength(25, MinimumLength = 6, ErrorMessage = "Допустимо 6 - 25 символов")]
        public string Password { set; get; }

        /// <summary>
        /// Подтверждение пароля
        /// </summary>
        [DataType(DataType.Password)]
        [Compare(nameof(Password), ErrorMessage = "Пароль и его подтверждение должны совпадать")]
        public string ControlPassword { set; get; }

        /// <summary>
        /// Токен пользователя
        /// </summary>
        public Guid LoginToken { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login { get; set; }
    }
}
