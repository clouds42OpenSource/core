﻿namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers
{
    public interface ISynchronizationCorpHelper
    {
        Task<byte[]> GetAct(Guid actId);
    }
}
