﻿namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers
{
    /// <summary>
    /// Хелпер для профиля пользователя
    /// </summary>
    public interface IAccountUserProfileHelper
    {
        /// <summary>
        /// Запустить задачу по созданию директории 1CeStart.
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        void RunTaskCreate1CStartFolder(string login);
    }
}
