﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.Filters;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Managers
{
    /// <summary>
    /// Менеджер для работы с пользователем
    /// </summary>
    public interface IAccountUserManager
    {
        /// <summary>
        /// Проверить текущий пароль
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="password">Текущий пароль</param>
        /// <returns>Результат проверки</returns>
        ManagerResult<bool> CheckCurrentPassword(Guid userId, string password);


        /// <summary>
        /// Получить пагинацию пользователей аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="request">Детали запроса пагинации</param>
        /// <returns>Результат запроса</returns>
        ManagerResult<SelectDataResultCommonDto<AccountUserDto>> GetPaginatedUsersForAccount(Guid accountId,
            PagniationRequestDto<AccountUsersFilterDto> request);
    }
}
