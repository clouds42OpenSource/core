﻿namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    /// <summary>
    /// Провадйер управления авторизационными даными пользователя.
    /// </summary>
    public interface IAccountUserAccessDataProvider
    {

        /// <summary>
        /// Сменить логин у пользователя.
        /// </summary>      
        void SetLogin(Guid accountUserId, string newLogin);

        /// <summary>
        /// Установить новый пароль пользователю.
        /// </summary>        
        void SetPassword(Guid accountUserId, string newPassword, string oldPassword);
    }
}