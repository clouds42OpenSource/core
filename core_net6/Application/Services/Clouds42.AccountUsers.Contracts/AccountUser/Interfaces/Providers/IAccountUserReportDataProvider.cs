﻿using Clouds42.DataContracts.AccountUser;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными пользователя для отчета
    /// </summary>
    public interface IAccountUserReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по пользователям для отчета
        /// </summary>
        /// <returns>Список моделей данных по пользователям для отчета</returns>
        List<AccountUserReportDataDto> GetAccountUserReportDataDcs();
    }
}
