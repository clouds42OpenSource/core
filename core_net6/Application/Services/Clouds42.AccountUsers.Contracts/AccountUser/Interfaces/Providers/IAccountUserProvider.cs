﻿using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.Filters;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с пользователем
    /// </summary>
    public interface IAccountUserProvider
    {
        /// <summary>
        /// Проверить текущий пароль
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="password">Текущий пароль</param>
        /// <returns>Результат проверки</returns>
        bool CheckCurrentPassword(Guid userId, string password);


        /// <summary>
        /// Запросить пагинированный список пользователей аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="request">Детали запроса</param>
        /// <returns>Пагинированный список пользователей</returns>
        SelectDataResultCommonDto<AccountUserDto> GetPaginatedUsersForAccount(
            Guid accountId, PagniationRequestDto<AccountUsersFilterDto> request);

        /// <summary>
        /// Получить первого админ-аккаунта
        /// </summary>
        /// <param name="accountId">Для какого акконта получить админа</param>
        /// <returns>Если найдент, то админ аакаунта иначе null</returns>
        AccountUserDto? GetFirstAccountAdminFor(Guid accountId);
    }
}
