﻿using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountUser;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для обновления пользователя аккаунта
    /// </summary>
    public interface IAccountUserUpdateProvider
    {
        /// <summary>
        /// Обновить пользователя за исключением его ролей
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        UpdateObjectDataInfo UpdateExceptRoles(AccountUserDm model, AccountUserDto updates);

        /// <summary>
        /// Обновить только роли пользователя
        /// </summary>
        /// <param name="updates">Обновляемая модель</param>
        /// <param name="model">Модель обновлений</param>
        /// <returns>Результат обновления ролей</returns>
        UpdateObjectDataInfo UpdateOnlyRoles(AccountUserDm model, AccountUserDto updates);
    }
}
