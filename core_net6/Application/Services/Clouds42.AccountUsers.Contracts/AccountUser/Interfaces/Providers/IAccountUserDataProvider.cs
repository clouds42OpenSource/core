﻿namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    public interface IAccountUserDataProvider
    {
        string GetPasswordHashByLogin(string login);
        Domain.DataModels.AccountUser GetAccountUserOrThrowException(Guid accountUserId);
        Guid GetAccountIdByUserLogin(string userLogin);
    }
}
