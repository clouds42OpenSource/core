﻿using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountUser;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    /// <summary>
    /// Класс для обновления модели пользователя
    /// </summary>
    public interface IAccountUserUpdater
    {
        /// <summary>
        /// Обновить модель пользователя аккаунта
        /// </summary>
        UpdateObjectDataInfo Update(AccountUserDm model, AccountUserDto updates);
    }
}
