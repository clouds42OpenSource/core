﻿using Clouds42.Domain.Access;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    /// <summary>
    /// Класс для работы с разрешениями пользователя
    /// </summary>
    public interface IAccountUserPermissionsProvider
    {
        /// <summary>
        /// Получить список разрешений юзера для контекстных значений
        /// </summary>
        /// <param name="userId">ID юзера разрешения которого запрашиваются</param>
        /// <param name="targetAccountId">ID аккаунта для операций над которым запрашиваются разрешения</param>
        /// <param name="targetUserId">ID юзера для операций над которым запрашиваются разрешения</param>
        /// <param name="actionsFilter">ID фильтр запрашиваемых разрешений. Если null - будут возвращены все доступные разрешения</param>
        /// <returns>Список разрешений пользователя</returns>
        List<ObjectAction> GetUserPermissionsForTargets(
            Guid userId,
            Guid? targetAccountId,
            Guid? targetUserId,
            IEnumerable<ObjectAction> actionsFilter = null);
    }
}