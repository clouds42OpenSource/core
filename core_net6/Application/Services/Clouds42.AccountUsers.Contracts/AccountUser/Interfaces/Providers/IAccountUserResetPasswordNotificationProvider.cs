﻿namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    /// <summary>
    ///     Провайдер управления нотификациями по сбросу пароля для аккаунт юзера
    /// </summary>
    public interface IAccountUserResetPasswordNotificationProvider
    {
        /// <summary>
        ///     Сформировать ResetToken и отправить брендированное письмо
        /// </summary>
        /// <param name="email">Почтовый адрес пользователя</param>
        void SendPasswordChangeRequest(string email);
    }
}
