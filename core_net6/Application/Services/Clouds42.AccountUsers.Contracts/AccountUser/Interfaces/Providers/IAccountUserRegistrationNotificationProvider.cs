﻿using Clouds42.DataContracts.Account.Registration;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    public interface IAccountUserRegistrationNotificationProvider
    {
        /// <summary>
        /// Уведомить пользователя о регистрации на сервисе.
        /// </summary>        
        /// <param name="accountUser">Зарегистрированный пользователь.</param>
        /// <param name="registrationSource">Источник регистрации.</param>
        /// <param name="generatedPassword">Автосгенирированный пароль</param>
        void NotifyAccountUser(Domain.DataModels.AccountUser accountUser, IRegistrationSourceInfoAdapter registrationSource, string generatedPassword);

        /// <summary>
        ///     Генерируем новый код активации и
        /// отправляем смс
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        void SendNewActivationSms(string login);

        /// <summary>
        /// Письмо о создании аккаунта
        /// </summary>
        /// <param name="accountUser"></param>
        /// <param name="registrationSource"></param>
        /// <param name="generatedPassword"></param>
        void SendAccountCreateLetter(Domain.DataModels.AccountUser accountUser,
            IRegistrationSourceInfoAdapter registrationSource,
            string generatedPassword);

    }
}
