﻿using Clouds42.BLL.Common.Access;
using Clouds42.Domain.Access;

namespace Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers
{
    /// <summary>
    /// Класс для работы с маппингом разрешений к ролям
    /// </summary>
    public interface IAccessMappingsProvider
    {
        /// <summary>
        /// Получает доступы для действий <see cref="objectActions"/> и переданных групп <see cref="groups"/>
        /// </summary>        
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        /// /// <param name="objectActions">действия для которых нужно получить доступ</param>
        IEnumerable<AccessModel> GetAccess(IEnumerable<AccountUserGroup> groups, IEnumerable<ObjectAction> objectActions);

        /// <summary>
        /// Получает доступы для действия <see cref="objectAction"/> и переданных групп <see cref="groups"/>
        /// </summary>
        /// <param name="objectAction">действие над которым получить доступ</param>
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        IEnumerable<AccessModel> GetAccess(ObjectAction objectAction, IEnumerable<AccountUserGroup> groups);

        /// <summary>
        /// Получает доступы для действий по переданным группам <see cref="groups"/>
        /// </summary>                
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        IEnumerable<AccessModel> GetAccess(IEnumerable<AccountUserGroup> groups);
    }
}