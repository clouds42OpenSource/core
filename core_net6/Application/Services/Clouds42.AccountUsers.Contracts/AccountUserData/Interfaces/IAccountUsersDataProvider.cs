﻿using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountUsers.Contracts.AccountUserData.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными пользователей аккаунта
    /// </summary>
    public interface IAccountUsersDataProvider
    {
        /// <summary>
        /// Получить данные пользователей аккаунта по фильтру
        /// </summary>
        /// <param name="args">Параметры фильтра для поиска пользователей аккаунта</param>
        /// <returns>Модель данных пользователей аккаунта</returns>
        AccountUsersDataDto GetAccountUsersDataByFilter(AccountUsersFilterParamsDto args);

        /// <summary>
        /// Найти пользователя
        /// </summary>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>Пользователь</returns>
        IAccountUser? SearchAccountUser(string searchString);

        /// <summary>
        /// Получить детальную информацию о пользователе аккаунта
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="contextAccountId">ID аккаунта, в контексте которого находится текущий пользователь</param>
        /// <returns>Детальная информацию о пользователе аккаунта</returns>
        AccountUserDetailedInfoDto? GetAccountUserDetailedInfo(Guid accountUserId, Guid contextAccountId);
    }
}
