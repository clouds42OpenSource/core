﻿using Clouds42.DataContracts.AccountUser.AccountUser;

namespace Clouds42.AccountUsers.Contracts.AccountUserData.Interfaces
{
    /// <summary>
    /// Провайдер для работы с общими/базовыми
    /// данными пользователей аккаунта
    /// </summary>
    public interface IAccountUsersCommonDataProvider
    {
        /// <summary>
        /// Получить общие/базовые данные для работы с пользователями аккаунта
        /// </summary>
        /// <returns>Общие/базовые данные для работы с пользователями аккаунта</returns>
        CommonDataForWorkingWithUsersDto GetCommonDataForWorkingWithUsers();
    }
}
