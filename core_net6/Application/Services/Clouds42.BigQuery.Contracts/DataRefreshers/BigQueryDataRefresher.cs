﻿namespace Clouds42.BigQuery.Contracts.DataRefreshers
{
    /// <summary>
    /// Интерфейс для рефреша/перезаписи
    /// данных в таблице BigQuery
    /// </summary>
    public interface IBigQueryDataRefresher
    {
        /// <summary>
        /// Перезаписать данные в таблице BigQuery
        /// </summary>
        void Refresh();
    }
}
