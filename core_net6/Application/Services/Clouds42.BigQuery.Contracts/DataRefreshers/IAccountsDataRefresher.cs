﻿namespace Clouds42.BigQuery.Contracts.DataRefreshers
{
    /// <summary>
    /// Интерфейс для рефреша/перезаписи
    /// данных в таблице аккаунтов(BigQuery)
    /// </summary>
    public interface IAccountsDataRefresher : IBigQueryDataRefresher
    {
    }
}
