﻿namespace Clouds42.BigQuery.Contracts.DataRefreshers
{
    /// <summary>
    /// Интерфейс для рефреша/перезаписи
    /// данных в таблице пользователей(BigQuery)
    /// </summary>
    public interface IAccountUsersDataRefresher : IBigQueryDataRefresher
    {
    }
}
