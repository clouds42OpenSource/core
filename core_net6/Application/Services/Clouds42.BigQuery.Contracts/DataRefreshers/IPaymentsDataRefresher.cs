﻿namespace Clouds42.BigQuery.Contracts.DataRefreshers
{
    /// <summary>
    /// Интерфейс для рефреша/перезаписи
    /// данных в таблице платежей(BigQuery)
    /// </summary>
    public interface IPaymentsDataRefresher : IBigQueryDataRefresher
    {
    }
}
