﻿namespace Clouds42.BigQuery.Contracts.DataRefreshers
{
    /// <summary>
    /// Интерфейс для рефреша/перезаписи
    /// данных в таблице ресурсов(BigQuery)
    /// </summary>
    public interface IResourcesDataRefresher : IBigQueryDataRefresher
    {
    }
}
