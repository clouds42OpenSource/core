﻿namespace Clouds42.BigQuery.Contracts.DataRefreshers
{
    /// <summary>
    /// Интерфейс для рефреша/перезаписи
    /// данных в таблице счетов на оплату(BigQuery)
    /// </summary>
    public interface IInvoicesDataRefresher : IBigQueryDataRefresher
    {
    }
}
