﻿namespace Clouds42.BigQuery.Contracts.DataRefreshers
{
    /// <summary>
    /// Интерфейс для рефреша/перезаписи
    /// данных в таблице информационных баз(BigQuery)
    /// </summary>
    public interface IDatabasesDataRefresher : IBigQueryDataRefresher
    {
    }
}
