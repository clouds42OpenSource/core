﻿namespace Clouds42.BigQuery.Contracts.DataRefreshers
{
    /// <summary>
    /// Процессор для перезаписи данных в BigQuery
    /// </summary>
    public interface IRefreshDataInBigQueryProcessor
    {
        /// <summary>
        /// Выполнить перезапись данных в BigQuery
        /// </summary>
        /// <typeparam name="TRefresher">Тип интерфейса класса
        /// для перезаписи данных</typeparam>
        void Refresh<TRefresher>() where TRefresher : IBigQueryDataRefresher;
    }
}
