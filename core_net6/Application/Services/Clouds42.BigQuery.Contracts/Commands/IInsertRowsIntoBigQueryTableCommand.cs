﻿using Clouds42.DataContracts.BaseModel.BiqQuery;

namespace Clouds42.BigQuery.Contracts.Commands
{
    /// <summary>
    /// Команда для записи/вставки строк
    /// в таблицу BigQuery 
    /// </summary>
    public interface IInsertRowsIntoBigQueryTableCommand
    {
        /// <summary>
        /// Выполнить команду по записи/вставки строк
        /// в таблицу BigQuery 
        /// </summary>
        /// <param name="model">Модель записи/вставки строк
        /// в таблицу BigQuery</param>
        void Execute(InsertRowsIntoBigQueryTableDto model);
    }
}
