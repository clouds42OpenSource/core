﻿using Clouds42.DataContracts.BaseModel.BiqQuery;

namespace Clouds42.BigQuery.Contracts.Commands
{
    /// <summary>
    /// Команда для очистки таблицы
    /// в БД BigQuery 
    /// </summary>
    public interface IClearBigQueryTableCommand
    {
        /// <summary>
        /// Выполнить команду по очистке таблицы BigQuery
        /// </summary>
        /// <param name="model">Модель для очистки таблицы
        /// в БД BigQuery</param>
        void Execute(ClearBigQueryTableDto model);
    }
}
