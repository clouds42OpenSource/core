﻿using Clouds42.DataContracts.Billing.Inovice;

namespace Clouds42.BigQuery.Contracts.Tables
{
    /// <summary>
    /// Интерфейс таблицы BigQuery для счетов на оплату
    /// </summary>
    public interface IInvoicesBigQueryTable : IBigQueryTable<InvoiceReportDataDto>
    {
    }
}
