﻿using Clouds42.DataContracts.Account;

namespace Clouds42.BigQuery.Contracts.Tables
{
    /// <summary>
    /// Интерфейс таблицы BigQuery для аккаунтов
    /// </summary>
    public interface IAccountsBigQueryTable : IBigQueryTable<AccountReportDataDto>
    {
    }
}
