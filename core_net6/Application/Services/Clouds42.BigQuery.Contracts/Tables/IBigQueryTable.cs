﻿namespace Clouds42.BigQuery.Contracts.Tables
{
    /// <summary>
    /// Интерфейс таблицы BigQuery 
    /// </summary>
    /// <typeparam name="TModel">Тип модели для записи/вставки</typeparam>
    public interface IBigQueryTable<TModel> where TModel : class
    {
        /// <summary>
        /// Очистить таблицу
        /// (удалить все записи в таблице)
        /// </summary>
        void ClearTable();

        /// <summary>
        /// Записать/вставить строки в таблицу
        /// </summary>
        /// <param name="models">Модели для записи/вставки</param>
        void InsertRows(List<TModel> models);
    }
}
