﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.BigQuery.Contracts.Tables
{
    /// <summary>
    /// Интерфейс таблицы BigQuery для ресурсов
    /// </summary>
    public interface IResourcesBigQueryTable : IBigQueryTable<ResourceReportDataDto>
    {
    }
}
