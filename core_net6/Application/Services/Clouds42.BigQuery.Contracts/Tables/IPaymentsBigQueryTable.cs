﻿using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.BigQuery.Contracts.Tables
{
    /// <summary>
    /// Интерфейс таблицы BigQuery для платежей
    /// </summary>
    public interface IPaymentsBigQueryTable : IBigQueryTable<PaymentReportDataDto>
    {
    }
}
