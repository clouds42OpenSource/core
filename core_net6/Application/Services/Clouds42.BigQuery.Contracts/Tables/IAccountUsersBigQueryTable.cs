﻿using Clouds42.DataContracts.AccountUser;

namespace Clouds42.BigQuery.Contracts.Tables
{
    /// <summary>
    /// Интерфейс таблицы BigQuery для пользователей
    /// </summary>
    public interface IAccountUsersBigQueryTable : IBigQueryTable<AccountUserReportDataDto>
    {
    }
}
