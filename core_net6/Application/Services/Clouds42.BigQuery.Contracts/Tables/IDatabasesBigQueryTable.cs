﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.BigQuery.Contracts.Tables
{
    public interface IDatabasesBigQueryTable : IBigQueryTable<AccountDatabaseReportDataDto>
    {
    }
}
