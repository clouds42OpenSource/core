﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.BigQuery.Contracts.ExportDataFromCore
{
    public interface IExportDataToBigQueryManager
    {
        /// <summary>
        /// Экспортировать данные аккаунта в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        ManagerResult ExportAccountsData();

        /// <summary>
        /// Экспортировать данные пользователей в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        ManagerResult ExportAccountUsersData();

        /// <summary>
        /// Экспортировать данные информационных баз в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        ManagerResult ExportDatabasesData();

        /// <summary>
        /// Экспортировать данные счетов на оплату в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        ManagerResult ExportInvoicesData();

        /// <summary>
        /// Экспортировать данные платежей в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        ManagerResult ExportPaymentsData();

        /// <summary>
        /// Экспортировать данные ресурсов в BigQuery
        /// </summary>
        /// <returns>Результат выполнения</returns>
        ManagerResult ExportResourcesData();
    }
}