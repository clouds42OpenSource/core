﻿namespace Clouds42.BigQuery.Contracts.ExportDataFromCore
{
    /// <summary>
    /// Провайдер для экспорта данных в BigQuery
    /// </summary>
    public interface IExportDataToBigQueryProvider
    {
        /// <summary>
        /// Экспортировать данные аккаунта в BigQuery
        /// </summary>
        void ExportAccountsData();

        /// <summary>
        /// Экспортировать данные пользователей в BigQuery
        /// </summary>
        void ExportAccountUsersData();

        /// <summary>
        /// Экспортировать данные ресурсов в BigQuery
        /// </summary>
        void ExportResourcesData();

        /// <summary>
        /// Экспортировать данные счетов на оплату в BigQuery
        /// </summary>
        void ExportInvoicesData();

        /// <summary>
        /// Экспортировать данные информационных баз в BigQuery
        /// </summary>
        void ExportDatabasesData();

        /// <summary>
        /// Экспортировать данные платежей в BigQuery
        /// </summary>
        void ExportPaymentsData();
    }
}
