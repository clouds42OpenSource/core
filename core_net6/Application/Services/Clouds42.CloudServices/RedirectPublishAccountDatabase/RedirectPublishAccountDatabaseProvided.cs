﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.AccountDatabase.Jobs.ManagePublishAcDbRedirect;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.CloudServices.RedirectPublishAccountDatabase
{
    /// <summary>
    /// Провайдер блокировки и разблокировки опубликованных баз
    /// </summary>
    public class RedirectPublishAccountDatabaseProvided(
        IUnitOfWork dbLayer,
        ManagePublishedAccountDatabaseRedirectJobWrapper managePublishedAccountDatabaseRedirectJobWrapper,
        IDatabasePlatformVersionHelper databasePlatformVersionHelper,
        IAccessProvider accessProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IRedirectPublishAccountDatabase
    {
        /// <summary>
		/// Установка редиректа при заблокированной аренде 1С
		/// Происходит замена web.config на другой в котором прописана страничка для редиректа
		/// </summary>
		/// <param name="accountId">ID аккаунта</param>
		public void SetRedirectNeedMoneyToAccount(Guid accountId)
		{
			var publishedAccountDatabases = GetPublishedAccountDatabase(accountId);
			if (publishedAccountDatabases.Count == 0)
			{
				logger.Trace($"Опубликованных баз для аккаунта {accountId} не найдено");
				return;
			}

			logger.Trace($"Найдено {publishedAccountDatabases.Count} опубликованных баз для аккаунта {accountId} ");

			var accountInfo = dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == accountId);
			if (accountInfo == null)
			{
				logger.Trace($"Аккаунт {accountId} не найден ");
				throw new NotFoundException($"Аккаунт {accountId} не найден ");
			}

            StartTasksForPublishedDatabases(publishedAccountDatabases, ManageAcDbRedirectOperationType.Install);
        }

        /// <summary>
        /// Снятие редиректа при разблокировании Аренды 1С
        /// Происходит замена web.config на прежний который был при публикации
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void DeleteRedirectionNeedMoneyForAccount(Guid accountId)
		{
			var publishedAccountDatabases = GetPublishedAccountDatabase(accountId);
			
            if (publishedAccountDatabases.Count == 0)
			{
				logger.Trace($"Опубликованных баз для пользователя {accountId} не найдено");
				return;
			}

			logger.Trace($"Найдено {publishedAccountDatabases.Count} опубликованных баз для пользователя {accountId}");

			var accountInfo = dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == accountId);
			if (accountInfo == null)
			{
				logger.Trace($"Аккаунт {accountId} не найден ");
				throw new NotFoundException($"Аккаунт {accountId} не найден ");
			}

            StartTasksForPublishedDatabases(publishedAccountDatabases, ManageAcDbRedirectOperationType.Remove);
		}

        /// <summary>
        /// Получить список опубликованных инф. баз для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список опубликованных инф. баз для аккаунта</returns>
        private List<Domain.DataModels.AccountDatabase> GetPublishedAccountDatabase(Guid accountId)
		{
		    return dbLayer.DatabasesRepository.AsQueryableNoTracking().Include(a=>a.AccountDatabaseOnDelimiter)
		        .Where(a => a.AccountId == accountId && a.PublishState == PublishState.Published.ToString() 
                && a.State == DatabaseState.Ready.ToString()).ToList();
		}

        /// <summary>
        /// Запустить задачи по управлению редиректом для инф. баз
        /// </summary>
        /// <param name="accountDatabases">Список инф. баз</param>
        /// <param name="operationType">Тип операции</param>
        private void StartTasksForPublishedDatabases(List<Domain.DataModels.AccountDatabase> accountDatabases, ManageAcDbRedirectOperationType operationType)
        {
            foreach (var dbInfo in accountDatabases)
            {
                try
                {
                    if (dbInfo.AccountDatabaseOnDelimiter != null)
                        continue;

                    var pathTo1CModule = dbInfo.IsFile == true
                        ? databasePlatformVersionHelper.CreatePathTo1CModuleForFileDb(dbInfo.AccountId, dbInfo.PlatformType, dbInfo.DistributionTypeEnum)
                        : databasePlatformVersionHelper.CreatePathTo1CModuleForServerDb(dbInfo.AccountId, dbInfo.PlatformType);

                    managePublishedAccountDatabaseRedirectJobWrapper.Start(new ManagePublishedAcDbRedirectParamsDto
                    {
                        AccountDatabaseV82Name = dbInfo.V82Name,
                        AccountDatabaseId = dbInfo.Id,
                        AccountId = dbInfo.AccountId,
                        OperationType = operationType,
                        Module1CPath = pathTo1CModule
                    });

                    logger.Debug($"Задача по управлению редиректом для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(dbInfo)} запущена. Тип операции: {operationType.Description()}");
                }
                catch (Exception ex)
                {
                    var message = $"[Ошибка при запуске задачи по управлению редиректом для базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(dbInfo)}. " +
                                  $"Тип операции: {operationType.Description()}. Причина: {ex.Message}";

                    LogEventHelper.LogEvent(dbLayer, dbInfo.AccountId, accessProvider,
                        LogActions.ManagePublishedAccountDatabaseRedirect, message);

                    handlerException.Handle(ex, $"[Ошибка при запуске задачи по управлению редиректом для базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(dbInfo)}. " +
                                  $"Тип операции: {operationType.Description()}");
                    throw;
                }
            }
        }
	}
}
