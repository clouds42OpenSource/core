﻿using Clouds42.AccountUsers.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.MyDisk;

namespace Clouds42.CloudServices;

public interface ICloudServiceManager
{

    /// <summary>
    /// Check service token validity
    /// </summary>
    /// <returns></returns>                
    ManagerResult<bool>
        CheckServiceTokenValidity(Guid serviceToken,
            out string headString);
    

    /// <summary>
    /// Add cloud service
    /// </summary>
    /// <returns></returns>                       
    ManagerResult<string> Add(AddCloudServiceModelDto model);

    /// <summary>
    /// Получить название службы по токену
    /// </summary>
    /// <param name="token">Токен службы</param>
    /// <param name="headString">Строка заголовка запроса</param>
    /// <returns>Название службы</returns>
    ManagerResult<string> GetServiceByToken(Guid token, out string headString);

    /// <summary>
    /// Получить информацию по сервису "Мой диск" для аккаунта
    /// </summary>
    /// <param name="accountId">Id аккаунта</param>
    /// <param name="headString">Строка для заголовка запроса</param>
    /// <returns>Информация по сервису "Мой диск" для аккаунта</returns>
    ManagerResult<ResultDto<MyDiskInfoModelDto>> GetMyDiscByAccount(Guid accountId);
    

    /// <summary>
    /// Обновить данные в таблице CloudService
    /// </summary>
    /// <param name="model">Модель для обновления</param>
    ManagerResult<bool> UpdateCloudService(CloudServiceDto model);

    /// <summary>
    /// Добавить новую запись в таблице CloudService
    /// </summary>
    /// <param name="model">Модель для добавления</param>
    /// <returns>Модель ответа добавленного CloudService</returns>
    ManagerResult<AddCloudServiceResponseDto> AddCloudService(CloudServiceDto model);

    /// <summary>
    /// Подтверждение подписки для интеграции с Cloud
    /// </summary>
    /// <param name="model">Модель для добавления</param>
    /// <returns></returns>
    ManagerResult SubscriptionConfirmationForCloud(CloudServiceIntegrationDto model, string login);

    /// <summary>
    /// Получить информацио о сервисе для Cloud
    /// </summary>
    /// <param name="model">Модель для кого запрашивается информация</param>
    /// <returns></returns>
    ManagerResult<CloudServiceInfoDto> GetServiceInfoForCloud(string email, string login);
}
