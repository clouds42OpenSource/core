﻿using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.CloudServices.Mappings
{
    internal static class KorpNomenclatureMapping
    {        

        internal class Item
        {
            public Clouds42Service Service { get; set; }
            public ResourceType? ResourceType { get; set; }
            public string ServiceName { get; set; }
            public string UniquaKey { get; set; }
        }

        public static readonly List<Item> Items =
        [
            new Item
            {
                Service = Clouds42Service.MyEnterprise,
                ResourceType = ResourceType.MyEntUser,
                ServiceName = NomenclaturesNameConstants.Renst1CStanart,
                UniquaKey = "0D8A3FC3-E8B2-40C1-A91D-7B99C35FB945"
            },

            new Item
            {
                Service = Clouds42Service.MyEnterprise,
                ResourceType = ResourceType.MyEntUserWeb,
                ServiceName = NomenclaturesNameConstants.Renst1CWeb,
                UniquaKey = "FB67E9A0-B2D9-4854-9B06-4968622940CE"
            },

            new Item
            {
                Service = Clouds42Service.MyDisk,
                ResourceType = null,
                ServiceName = NomenclaturesNameConstants.MyDisk,
                UniquaKey = "F558004F-54ED-4B64-924B-E660E75C998F"
            },

            new Item
            {
                Service = Clouds42Service.Recognition,
                ResourceType = null,
                ServiceName = NomenclaturesNameConstants.Recognition,
                UniquaKey = "FEEBC3EC-2A97-426A-B878-0A680E3C9988"
            },

            new Item
            {
                Service = Clouds42Service.Esdl,
                ResourceType = null,
                ServiceName = NomenclaturesNameConstants.Esdl,
                UniquaKey = "659D3B63-5D08-4372-B58B-CB5DDD4D4CC4"
            }
        ];


    }
}
