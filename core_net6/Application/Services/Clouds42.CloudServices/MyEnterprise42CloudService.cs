﻿using System.Text;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Helpers;
using Clouds42.CloudServices.Helpers;
using Clouds42.CloudServices.Providers;
using Clouds42.Common.Constants;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CloudServices
{
    /// <summary>
    /// Сервис обработки биллинга системной услуги Аренда 1С.
    /// </summary>
    public class MyEnterprise42CloudService(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        Rent1CAccountUserProvider rent1CAccountUserProvider,
        IResourcesService resourcesService,
        Rent1CRateHelper rent1CRateHelper,
        IRent1CManageHelper rent1CManageHelper,
        IPromisePaymentProvider promisePaymentProvider,
        IRent1CSponsorAccountProvider rent1CSponsorAccountProvider,
        IServiceProvider serviceProvider,
        BillingServiceDataProvider billingServiceDataProvider,
        ICreatePaymentHelper createPaymentHelper,
        IIncreaseAgentPaymentProcessor increaseAgentPaymentProcessor,
        IOnDisabledRent1CForUsersTrigger onDisabledRent1CForUsersTrigger,
        IConfigurateRent1CAccessesDataProvider configureRent1CAccessesDataProvider,
        IConfiguration configuration,
        INotificationProcessor notificationProcessor,
        ICheckAccountDataProvider checkAccountDataProvider,
        ICloudLocalizer cloudLocalizer,
        IProvidedServiceHelper providedServiceHelper)
        : Cloud42ServiceBase(accessProvider, dbLayer, configuration, notificationProcessor, createPaymentHelper,
            checkAccountDataProvider), IMyEnterprise42CloudService
    {
        private readonly ICreatePaymentHelper _createPaymentHelper = createPaymentHelper;
        private readonly IRent1CAccessHelper _rent1CAccessHelper = serviceProvider.GetRequiredService<IRent1CAccessHelper>();
        private readonly IConfiguration _configuration = configuration;

        public override bool CanProlong(out string? reason)
        {
            bool canProlong = true;
            reason = null;

            var hasActiveResources = DbLayer.ResourceRepository.Any(r =>
                (r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser ||
                 r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb) &&
                (r.AccountId == AccountId || r.AccountSponsorId == AccountId) &&
                r.Subject.HasValue);

            if (hasActiveResources)
            {
                return canProlong;
            }

            var resConfig = ResourceConfiguration;
            Logger.Trace($"Нету пользователей Аренды. Пропускаем продление. Аккаунт {resConfig.AccountId}");
            canProlong = false;
            reason = $"Нет подключенных пользователей к сервису {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accessProvider.ContextAccountId)}.";

            return canProlong;
        }

        public override Clouds42Service CloudServiceType => Clouds42Service.MyEnterprise;

        protected override void BeforeLock()
        {
        }

        /// <summary>
        ///    1. Блокировка Аренды 1С.
        ///    2. Удаление пользователей из групп AD.
        /// </summary>
        public override void Lock()
        {
            rent1CManageHelper.Lock(AccountId);
        }

        /// <summary>
        ///    1. Разблокировка Аренды 1С.
        ///    2. Восстановление пользователей в группы AD.
        /// </summary>
        public override void UnLock()
        {
            rent1CManageHelper.UnLock(AccountId);
        }

        public override void ActivateService()
        {
            ActivateService(null);
        }

        public void ActivateService(Guid? accountUserId)
        {
            Logger.Info($"[{AccountId}] :: Поиск ресурса {Clouds42Service.MyEnterprise}");
            var resourcesConfiguration = resourcesService.GetResourceConfig(AccountId, Clouds42Service.MyEnterprise);

            if (resourcesConfiguration != null)
            {
                Logger.Warn(
                    $"Найден ресурс {Clouds42Service.MyEnterprise} у аккаунта : {AccountId} активация не нужна");
                return;
            }

            var billingService = billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            resourcesConfiguration = resourcesService.CreateNewResourcesConfiguration(AccountId, billingService,
                resConfExpireDate: DateTime.Now.AddDays(Clouds42SystemConstants.MyEnterpriseDemoPeriodDays));
            
            var accountUser = GetAccountUser(accountUserId);
            var freeResourceCount = CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod();

            rent1CManageHelper.CreateDemoResources(freeResourceCount,
                freeResourceCount,
                resourcesConfiguration);

            var rent1CRateInfo = rent1CRateHelper.GetRent1CRateInfo(AccountId);

            if (accountUser != null)
            {
                resourcesService.AddOrResignResource(resourcesConfiguration.AccountId, resourcesConfiguration,
                    rent1CRateInfo.Rent1CStandartRate.Cost, accountUser.Id, ResourceType.MyEntUser);
                resourcesService.AddOrResignResource(resourcesConfiguration.AccountId, resourcesConfiguration,
                    rent1CRateInfo.Rent1CWebRate.Cost, accountUser.Id, ResourceType.MyEntUserWeb);

                activeDirectoryTaskProcessor.RunTask(provider =>
                    provider.EditUserGroupsAdList(CreateEditUserGroupsAdModelHelper.CreateAdEnableRent1CModel(AccountId, accountUser)));
            }

            providedServiceHelper.RegisterServiceActivation(AccountId,
                Clouds42Service.MyEnterprise, resourcesConfiguration.ExpireDateValue, ResourceType.MyEntUser,
                KindOfServiceProvisionEnum.Periodic, freeResourceCount);
            var locale = DbLayer.AccountsRepository.GetLocale(AccountId, Guid.Parse(_configuration["DefaultLocale"] ?? ""));
            if (locale.Name != LocaleConst.Russia)
            {
                return;
            }

            var additionalSessionsService = billingServiceDataProvider.GetBillingService(Guid.Parse(_configuration["DopSeansServiceId"] ?? ""));
            resourcesService.CreateNewResourcesConfiguration(AccountId, additionalSessionsService);

        }

        /// <summary>
        /// Активировать Аренду 1С
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="rent1CServiceTypes">Список услуг аренды для подключения</param>
        public void ActivateRent1C(Guid? accountUserId, List<ResourceType> rent1CServiceTypes)
        {
            var resourcesService = serviceProvider.GetRequiredService<IResourcesService>();

            Logger.Info($"[{AccountId}] :: Поиск ресурса {Clouds42Service.MyEnterprise}");
            var resourcesConfiguration = resourcesService.GetResourceConfig(AccountId, Clouds42Service.MyEnterprise);

            if (resourcesConfiguration != null)
            {
                Logger.Warn(
                    $"Найден ресурс {Clouds42Service.MyEnterprise} у аккаунта : {AccountId} активация не нужна");
                return;
            }

            var billingService = billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            resourcesConfiguration = resourcesService.CreateNewResourcesConfiguration(AccountId, billingService,
                resConfExpireDate: DateTime.Now.AddDays(Clouds42SystemConstants.MyEnterpriseDemoPeriodDays));

            var accountUser = GetAccountUser(accountUserId);

            var freeResourceCount = CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod();

            rent1CManageHelper.CreateDemoResources(freeResourceCount,
                freeResourceCount,
                resourcesConfiguration);

            if (accountUser == null)
                return;

            ActivateRent1CForAccountUser(accountUser, resourcesConfiguration, rent1CServiceTypes);
        }

        /// <summary>
        /// Получить пользователя аккаунта
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Пользователь аккаунта</returns>
        private AccountUser? GetAccountUser(Guid? accountUserId)
        {
            AccountUser? accountUser;
            if (accountUserId.HasValue)
            {
                accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == accountUserId.Value);
                CheckBelongingAccountUserToAccount(accountUser);
            }
            else
            {
                accountUser = DbLayer.AccountUsersRepository.AsQueryableNoTracking().Include(i => i.AccountUserRoles).FirstOrDefault(u =>
                    u.AccountId == AccountId &&
                    u.AccountUserRoles.Any(r => r.AccountUserGroup == AccountUserGroup.AccountAdmin));
            }

            return accountUser;
        }

        /// <summary>
        /// Активировать аренду для пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="resourcesConfiguration">Ресурс конфигурация аренды</param>
        /// <param name="rent1CServiceTypes">Типы услуг для подключения</param>
        private void ActivateRent1CForAccountUser(AccountUser accountUser,
            ResourcesConfiguration resourcesConfiguration, List<ResourceType> rent1CServiceTypes)
        {
            var freeResourceCount = CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod();

            rent1CServiceTypes.ForEach(resType =>
            {
                var serviceTypeRate = rent1CRateHelper.GetRateInfoForServiceType(AccountId, resType);
                resourcesService.AddOrResignResource(resourcesConfiguration.AccountId, resourcesConfiguration,
                    serviceTypeRate.Cost, accountUser.Id, resType);

                if (rent1CServiceTypes.Contains(ResourceType.MyEntUser) && resType == ResourceType.MyEntUserWeb)
                    return;

                providedServiceHelper.RegisterServiceActivation(AccountId,
                    Clouds42Service.MyEnterprise, resourcesConfiguration.ExpireDateValue, resType,
                    KindOfServiceProvisionEnum.Periodic, freeResourceCount);
            });

            var accountUserResources = DbLayer.ResourceRepository.Where(res =>
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise &&
                res.Subject == accountUser.Id).ToList();

            var accountUserDbAccess = DbLayer.AcDbAccessesRepository
                .AsQueryableNoTracking()
                .Where(x => x.AccountUserID == accountUser.Id)
                .Select(x => new AccountIndexNumberDbNumberDto
                {
                    AccountIndexNumber = x.Account.IndexNumber, DbNumber = x.Database.DbNumber, Login = x.AccountUser.Login
                })
                .ToList();

            var editUserInAdModel = CreateEditUserGroupsAdModelHelper
                    .CreateEditUserGroupsAdModel(accountUserResources, accountUser, EditUserGroupsAdModelDto.OperationEnum.Include)
                    .AppendAcDbAccesses(accountUserDbAccess, accountUser, EditUserGroupsAdModelDto.OperationEnum.Include);

            activeDirectoryTaskProcessor.RunTask(provider =>
                provider.EditUserGroupsAdList(
                    CreateEditUserGroupsAdModelHelper.CreateAdModel(AccountId, editUserInAdModel.Items.ToArray())));
        }

        /// <summary>
        /// Проверить принадлежность пользователя к аккаунту
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        private void CheckBelongingAccountUserToAccount(AccountUser accountUser)
        {
            if (accountUser.AccountId == AccountId)
                return;

            var errorMessage = $"Пользователь {accountUser.Login} не принадлежит аккаунту {AccountId}";
            Logger.Warn(errorMessage);
            throw new InvalidOperationException(errorMessage);
        }

        public override void AcceptVisitor(ISystemCloudServiceProcessorVisitor visitor)
        {
            visitor.Visit(this);
        }

        /// <summary>
        /// Управление менеджером сервисом "Аренда 1С"
        /// </summary>
        /// <param name="model"></param>
        public void ManagedRent1C(Rent1CServiceManagerDto model)
        {
            var result = rent1CManageHelper.ManageRent1C(model, AccountId);
            var accountInfo = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == AccountId);

            var log =
                $"Конфигурация сервисa \"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accessProvider.ContextAccountId)}\" изменена. {result}";
            LogEventHelper.LogEvent(DbLayer, accountInfo.Id, AccessProvider, LogActions.EditServiceCost, log);
        }

        /// <summary>
        /// Покупка сервиса "Аренда 1С" за счет обещанного платежа
        /// </summary>
        /// <param name="accessList"></param>
        /// <returns></returns>
        public bool TryPayAccessByPromisePayment(List<UpdaterAccessRent1CRequestDto> accessList)
        {
            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == AccountId);
            var resourceConfig = resourcesService.GetResourceConfig(account.Id, Clouds42Service.MyEnterprise);
            var billingInfoAllAccess = new List<ChangeRent1CAccessDto>();

            foreach (var accountUser in accessList)
            {
                AccountBillingDataForBuyRent1CDto billingInfo;
                if (account.AccountUsers.All(acUs => acUs.Id != accountUser.AccountUserId))
                {
                    Logger.Debug($"Подключение спонсирования для аккаунта {accountUser.AccountUserId}");
                    accountUser.SponsorAccountUser = true;
                    billingInfo =
                        _rent1CAccessHelper.GetAccountBillingDataForBuyRent1C(accountUser.AccountUserId);
                }
                else
                    billingInfo =
                        _rent1CAccessHelper.GetAccountBillingDataForBuyRent1C(account, resourceConfig!.ExpireDateValue);

                billingInfoAllAccess.Add(new ChangeRent1CAccessDto
                {
                    BuyRent1CData = billingInfo,
                    AccessData = accountUser
                });
            }

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                Logger.Debug(
                    $"[{account.Id}] :: Проверка возможности оплатить ресурс при помощи обещанного платежа");

                if (!CanPayRent1CAccesses(account.Id, billingInfoAllAccess, out var result))
                {
                    Logger.Debug($"[{account.Id}] :: Ошибка покупки ресурсов за счет ОП");
                    dbScope.Rollback();
                    return false;
                }

                if (result.EnoughMoney > 0)
                {
                    Logger.Debug($"[{account.Id}] :: Добавление ОП на сумму {result.EnoughMoney}");

                    promisePaymentProvider.CreatePromisePayment(
                        AccountId,
                        new CalculationOfInvoiceRequestModel
                            {SuggestedPayment = new SuggestedPaymentModelDto {PaymentSum = result.EnoughMoney}},
                        prolongServices: false);
                }

                Logger.Debug($"[{account.Id}] :: Начало конфигурирования доступов к аренде для пользователей.");
                result = ConfigureAccesses(accessList);
                if (!result.Complete)
                {
                    Logger.Debug($"[{account.Id}] :: Ошибка конфигурирования доступов к аренде для пользователей.");
                    dbScope.Rollback();
                    return false;
                }

                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                var rent1CName = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accessProvider.ContextAccountId);
                HandlerException.Handle(ex, $"[Ошибка активации сервиса] {rent1CName} через обещанный платеж {ex.GetFullInfo()}");
                dbScope.Rollback();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверить возможность оплатить лицензии сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changeRent1CAccess">Изменения доступов Аренды 1С</param>
        /// <param name="result">Результат обновления доступов</param>
        /// <returns>Возможность оплатить лицензии сервиса Аренда 1С</returns>
        private bool CanPayRent1CAccesses(Guid accountId, List<ChangeRent1CAccessDto> changeRent1CAccess,
            out UpdateAccessRent1CResultDto result)
        {
            if (_rent1CAccessHelper.CanPayRent1CAccesses(accountId, changeRent1CAccess, out result))
                return true;

            return result is { CanGetPromisePayment: true, CostForPay: >= 0 };
        }

        /// <summary>
        /// Сконфигурировать доступы к аренде для пользователей.
        /// </summary>
        /// <param name="accesses">Список доступов.</param>        
        /// <returns>Результат изменения доступов к аренде.</returns>
        public UpdateAccessRent1CResultDto ConfigureAccesses(List<UpdaterAccessRent1CRequestDto> accesses)
        {
            var configureAccessData = configureRent1CAccessesDataProvider.GetData(AccountId);

            var billingDataForBuyRent1C = _rent1CAccessHelper.GetAccountBillingDataForBuyRent1C(
                configureAccessData.Account,
                configureAccessData.ResourcesConfiguration.ExpireDateValue);

            var changeRent1CAccesses = GenerateChangeRent1CAccesses(accesses, configureAccessData);

            Logger.Debug($"[{configureAccessData.Account.Id}] :: Проверка возможности оплатить ресурс");

            if (!_rent1CAccessHelper.CanPayRent1CAccesses(configureAccessData.Account.Id, changeRent1CAccesses,
                out var result))
                return result;

            var editUserGroupsAdListModel = new EditUserGroupsAdListModel
            {
                AccountId = AccountId
            };
            try
            {
                Logger.Debug($"[{configureAccessData.Account.Id}] :: Изменение списка ресурсов");

                editUserGroupsAdListModel.EditUserGroupsModel.AddRange(
                    _rent1CAccessHelper.ChangeRen1CResourcesOfAccount(configureAccessData.Account,
                        changeRent1CAccesses));

                if (!TryPayRent1CAccesses(configureAccessData.Account.Id,
                    configureAccessData.ResourcesConfiguration, result.CostForPay,
                    GetBuyRent1CPaymentDescription(accesses)))
                {
                    return new UpdateAccessRent1CResultDto
                    {
                        Complete = false,
                        ErrorMessage = "Ошибка проведения платежа."
                    };
                }

                editUserGroupsAdListModel.EditUserGroupsModel.AddRange(ProcessAccesses(accesses));
                ProcessSponsoredAccounts(accesses);

                providedServiceHelper.RegisterRent1CChanges(AccountId, configureAccessData.ResourcesConfiguration, billingDataForBuyRent1C);

                ExecuteOnDisabledRent1CForUsersTrigger(accesses);

                DbLayer.Save();
            }

            catch (Exception ex)
            {
                HandlerException.Handle(ex, $"[Ошибка подключения аренды 1С] для аккаунта {configureAccessData.Account.AccountCaption}");
               
                return new UpdateAccessRent1CResultDto
                {
                    Complete = false,
                    ErrorMessage = ex.Message
                };
            }
            activeDirectoryTaskProcessor.RunTask(provider => provider.EditUserGroupsAdList(editUserGroupsAdListModel));

            return new UpdateAccessRent1CResultDto
            {
                Complete = true
            };
        }

        /// <summary>
        /// Попытаться оплатить лицензии Аренды 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="resourcesConfiguration">Конфигурация ресурсов Аренды 1С</param>
        /// <param name="costToPay">Сумма к оплате</param>
        /// <param name="paymentDescription">Описание платежа</param>
        /// <returns>Результат оплаты</returns>
        private bool TryPayRent1CAccesses(Guid accountId, ResourcesConfiguration resourcesConfiguration,
            decimal costToPay, string paymentDescription)
        {
            if (costToPay <= 0)
                return true;

            Logger.Debug(
                $"[{accountId}] :: Добавление платежа для покупка сервиса Аренда 1С на сумму {costToPay}");

            var paymentResult = _createPaymentHelper.MakePayment(
                resourcesConfiguration,
                paymentDescription, costToPay, PaymentSystem.ControlPanel,
                PaymentType.Outflow, "core-cp");

            if (paymentResult.OperationStatus == PaymentOperationResult.Ok)
            {
                increaseAgentPaymentProcessor.Process(paymentResult.PaymentIds);
                return true;
            }

            Logger.Debug(
                $"[{accountId}] :: Ошибка проведения платежа для покупка сервиса Аренда 1С на сумму {costToPay}");

            return false;
        }

        /// <summary>
        /// Сформировать изменения доступов Аренды 1С
        /// </summary>
        /// <param name="accesses">Доступы для обновления</param>
        /// <param name="configurateAccessesData">Данные для конфигурации лицензий Аренды 1С</param>
        /// <returns>Изменения доступов Аренды 1С</returns>
        private List<ChangeRent1CAccessDto> GenerateChangeRent1CAccesses(List<UpdaterAccessRent1CRequestDto> accesses,
            Rent1CAccessesForConfigurationDataDto configurateAccessesData) => accesses
            .Select(access => GenerateChangeRent1CAccess(access, configurateAccessesData)).ToList();

        /// <summary>
        /// Сформировать изменения доступов Аренды 1С
        /// </summary>
        /// <param name="access">Доступ для обновления</param>
        /// <param name="configurateAccessesData">Данные для конфигурации лицензий Аренды 1С</param>
        /// <returns>Изменение доступа к Аренде 1С</returns>
        private ChangeRent1CAccessDto GenerateChangeRent1CAccess(UpdaterAccessRent1CRequestDto access,
            Rent1CAccessesForConfigurationDataDto configurateAccessesData)
        {
            var isSponsorship = configurateAccessesData.UsersIdList.All(id => id != access.AccountUserId);

            if (isSponsorship)
                access.SponsorAccountUser = true;

            TraceAccessChangesToLog(access, isSponsorship);

            return new ChangeRent1CAccessDto
            {
                BuyRent1CData = access.SponsorAccountUser
                    ? _rent1CAccessHelper.GetAccountBillingDataForBuyRent1C(access.AccountUserId)
                    : _rent1CAccessHelper.GetAccountBillingDataForBuyRent1C(configurateAccessesData.Account,
                        configurateAccessesData.ResourcesConfiguration.ExpireDateValue.Date),
                AccessData = access
            };
        }

        /// <summary>
        /// Записать изменения доступа в лог
        /// </summary>
        /// <param name="access">Доступ для обновления</param>
        /// <param name="isSponsorship">Признак спонсирования</param>
        private void TraceAccessChangesToLog(UpdaterAccessRent1CRequestDto access, bool isSponsorship)
        {
            var userDescription = isSponsorship ? "спонсируемого пользователя" : "пользователя";

            if (access.StandartResource || access.WebResource)
                Logger.Debug($"Подключение {userDescription} {access.AccountUserId} к аренде");
            else
                Logger.Debug($"Отключение {userDescription} {access.AccountUserId} от аренды");
        }

        /// <summary>
        /// Получить описание транзакции покупки сервиса Аренда 1С
        /// </summary>
        /// <param name="accesses">Список доступов</param>
        /// <returns>Описание транзакции покупки сервиса Аренда 1С</returns>
        private string GetBuyRent1CPaymentDescription(List<UpdaterAccessRent1CRequestDto> accesses)
        {
            var serviceTypesDefinitionsBuilder = new StringBuilder();

            var standartAccessesCount = accesses.Count(acc => acc.StandartResource);
            if (standartAccessesCount > 0)
                serviceTypesDefinitionsBuilder.Append($" пользователи Стандарт +{standartAccessesCount};");

            var webAccessesCount = accesses.Count(acc => acc.WebResource);
            if (webAccessesCount > 0)
                serviceTypesDefinitionsBuilder.Append($" пользователи WEB +{webAccessesCount};");

            var serviceTypeDefinitions = serviceTypesDefinitionsBuilder.ToString();
            if (!serviceTypeDefinitions.IsNullOrEmpty())
                serviceTypeDefinitions = $" ({serviceTypeDefinitions.TrimStart().TrimEnd(';')})";

            return
                $"{PaymentActionType.PayService.Description()} \"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accessProvider.ContextAccountId)}\"{serviceTypeDefinitions}";
        }

        /// <summary>
        /// Выполнить триггер на отключение Аренды 1С у пользователей
        /// </summary>
        /// <param name="updaterAccessRent1C">Модели данных по изменениям лицензий</param>
        private void ExecuteOnDisabledRent1CForUsersTrigger(List<UpdaterAccessRent1CRequestDto> updaterAccessRent1C)
        {
            var usersWhoHaveDisabledRent = updaterAccessRent1C.Where(ua => ua is { StandartResource: false, WebResource: false })
                .Select(ua => ua.AccountUserId).ToList();

            if (!usersWhoHaveDisabledRent.Any())
                return;

            onDisabledRent1CForUsersTrigger.Execute(new OnDisabledRent1CForUsersTriggerDto
            {
                AccountUserIds = usersWhoHaveDisabledRent
            });
        }

        /// <summary>
        /// Обработать настройки доступов пользователей Аренды 1С.
        /// Если доступ отключен, заблокировать и разблокировать если подключен.
        /// </summary>
        /// <param name="accessList">Список доступов.</param>
        private List<EditUserGroupsAdModelDto> ProcessAccesses(List<UpdaterAccessRent1CRequestDto> accessList)
        {
            Logger.Debug("Начало обработки настройки доступа пользователей Аренды 1С.");
            var editUserGroupsAdModelList = new List<EditUserGroupsAdModelDto>();

            accessList.ForEach(accessItem =>
            {
                var accountUser = DbLayer.AccountUsersRepository.GetAccountUser(accessItem.AccountUserId);

                if (IsSponsoredAccessForAccountUser(accountUser.AccountId, accessItem))
                    return;

                var changeAccessStateLogData = GetDataForWriteLogAboutChangeAccessState(accessItem, accountUser);

                if (ResourceConfiguration.FrozenValue)
                {
                    WriteLogAboutChangeAccessStateWhenServiceIsBlocked(changeAccessStateLogData);
                    return;
                }

                editUserGroupsAdModelList.Add(ExecuteAccessStateChangeAction(accountUser,
                    changeAccessStateLogData.IsConnectToService));

                WriteLogAboutChangeAccessState(changeAccessStateLogData);
            });

            return editUserGroupsAdModelList;
        }

        /// <summary>
        /// Получить данные для записи лога об изменении состоянии
        /// лицензии сервиса Аренда 1С для пользователя
        /// </summary>
        /// <param name="accessItem">Изменения доступа пользователя</param>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <returns>Данные для записи лога об изменении состоянии
        /// лицензии сервиса Аренда 1С для пользователя</returns>
        private static (string UserDescription, string ResourceType, bool IsConnectToService, string UserLogin)
            GetDataForWriteLogAboutChangeAccessState(UpdaterAccessRent1CRequestDto accessItem,
                AccountUser accountUser) => (
            UserDescription: accessItem.SponsorAccountUser ? " спонсируемого пользователя " : " пользователя ",
            ResourceType: accessItem.WebResource ? " веб " : " стандарт ",
            IsConnectToService: accessItem.WebResource || accessItem.StandartResource, UserLogin: accountUser.Login);

        /// <summary>
        /// Выполнить действие изменение состояния лицензии
        /// сервиса Аренда 1С для пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <param name="isConnectToService">Признак подключения пользователя к сервису</param>
        /// <returns>Модель изменения групп пользователя в AD</returns>
        private EditUserGroupsAdModelDto ExecuteAccessStateChangeAction(AccountUser accountUser,
            bool isConnectToService) => isConnectToService
            ? rent1CAccountUserProvider.UnLock(accountUser)
            : rent1CAccountUserProvider.Lock(accountUser);

        /// <summary>
        /// Сделать запись лог об изменении состоянии
        /// лицензии сервиса Аренда 1С для пользователя
        /// </summary>
        /// <param name="changeAccessStateLogData">Данные для записи лога</param>
        private void WriteLogAboutChangeAccessState(
            (string UserDescription, string ResourceType, bool IsConnectToService, string UserLogin)
                changeAccessStateLogData)
        {
            var message = changeAccessStateLogData.IsConnectToService
                ? $"Подключение {changeAccessStateLogData.UserDescription} \"{changeAccessStateLogData.UserLogin}\" к {changeAccessStateLogData.ResourceType} ресурсам"
                : $"Отключение {changeAccessStateLogData.UserDescription} \"{changeAccessStateLogData.UserLogin}\"  от сервиса '{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accessProvider.ContextAccountId)}'";

            LogEventHelper.LogEvent(DbLayer, ResourceConfiguration.AccountId, AccessProvider,
                DefineChangeAccessStateForUserLogAction(changeAccessStateLogData.IsConnectToService), message);
        }

        /// <summary>
        /// Сделать запись лог об изменении состоянии
        /// лицензии сервиса Аренда 1С для пользователя,
        /// при заблокированном сервисе
        /// </summary>
        /// <param name="changeAccessStateLogData">Данные для записи лога</param>
        private void WriteLogAboutChangeAccessStateWhenServiceIsBlocked(
            (string UserDescription, string ResourceType, bool IsConnectToService, string UserLogin)
                changeAccessStateLogData)
        {
            var message = changeAccessStateLogData.IsConnectToService
                ? $"Подключение {changeAccessStateLogData.UserDescription} \"{changeAccessStateLogData.UserLogin}\" к {changeAccessStateLogData.ResourceType} ресурсам при заблокированном сервисе"
                : $"Отключение {changeAccessStateLogData.UserDescription} \"{changeAccessStateLogData.UserLogin}\" от сервиса '{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accessProvider.ContextAccountId)}' при заблокированном сервисе";

            LogEventHelper.LogEvent(DbLayer, ResourceConfiguration.AccountId, AccessProvider,
                DefineChangeAccessStateForUserLogAction(changeAccessStateLogData.IsConnectToService), message);

            Logger.Debug(message);
        }

        /// <summary>
        /// Определить действие/событие логирования для изменения
        /// состояния лицензии сервиса Аренда 1С для пользователя  
        /// </summary>
        /// <param name="isConnectToService">Признак подключения пользователя к сервису</param>
        /// <returns>Действие/событие логирования для изменения
        /// состояния лицензии сервиса Аренда 1С</returns>
        private static LogActions DefineChangeAccessStateForUserLogAction(bool isConnectToService) =>
            isConnectToService ? LogActions.ConnectToService : LogActions.DisconnectFromService;

        /// <summary>
        /// Проверить что лицензия спонсорская для пользователя
        /// </summary>
        /// <param name="accountId">Id аккаунта пользователя</param>
        /// <param name="accessItem">Модель данных по изменению лицензии</param>
        /// <returns>Лицензия является спонсорской для пользователя</returns>
        private bool IsSponsoredAccessForAccountUser(Guid accountId, UpdaterAccessRent1CRequestDto accessItem)
        {
            if (accountId != ResourceConfiguration.AccountId || !accessItem.SponsorAccountUser)
            {
                return false;
            }

            Logger.Debug($"Этот аккаунт {accessItem.AccountUserId} спонсируется, доступы не трогаем.");
            return true;

        }

        /// <summary>
        /// Обработать спонсирование внешних пользователей.
        /// </summary>
        private void ProcessSponsoredAccounts(List<UpdaterAccessRent1CRequestDto> accesses)
        {
            var sponsoredAccountUserIds = accesses.Where(a => a.SponsorAccountUser).Select(s => s.AccountUserId);

            var sponsoredAccountIds = DbLayer.AccountUsersRepository
                .WhereLazy(au => sponsoredAccountUserIds.Contains(au.Id)).Select(a => a.AccountId).Distinct().ToList();

            if (!sponsoredAccountIds.Any())
            {
                Logger.Trace($"Аккаунт '{AccountId}' : нет спонсируемых лицензий.");
                return;
            }

            if (!CheckAccountProvider.AccountHasInflowPayments(AccountId))
            {
                Logger.Trace(
                    $"Аккаунт '{AccountId}' не имеет ни одной входящей платежки. Спонсируемые не будет разблокироваться.");
                return;
            }

            foreach (var accountId in sponsoredAccountIds)
            {
                rent1CSponsorAccountProvider.ProcessSponsoredAccount(accountId);
            }
        }

        public IMyEnterprise42CloudService SetAccountId(Guid accountId)
        {
            AccountId = accountId;
            return this;
        }

    }
}
