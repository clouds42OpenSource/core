﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsTerminalServers.Providers
{
    /// <summary>
    /// Провайдер для работы с терминальным сервером
    /// </summary>
    internal class CloudTerminalServerProvider(IUnitOfWork dbLayer) : ICloudTerminalServerProvider
    {
        /// <summary>
        /// Получить терминальный сервер
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <returns>Терминальный сервер</returns>
        public CloudTerminalServer GetCloudTerminalServer(Guid terminalServerId) =>
            dbLayer.CloudTerminalServerRepository.FirstOrDefault(ts => ts.ID == terminalServerId) ??
            throw new NotFoundException($"Не удалось получить терминальный сервер по Id {terminalServerId}");

        /// <summary>
        /// Получить адреса терминальных серверов для инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель адресов терминальных серверов для информационной базы</returns>
        public TerminalServerAddressesForDatabaseDto GetTerminalServerAddressesForDatabase(Guid databaseId) =>
            (from database in dbLayer.DatabasesRepository.WhereLazy()
             join groupedTerminalAddressesByDatabase in GetGroupedTerminalServerAddressesByDatabase() on database.Id
                 equals
                 groupedTerminalAddressesByDatabase.DatabaseId into groupedTerminalAddressesByDatabases
             from groupedTerminalAddressesByDatabase in groupedTerminalAddressesByDatabases.Take(1).DefaultIfEmpty()
             where database.Id == databaseId
             select new TerminalServerAddressesForDatabaseDto
             {
                 V82Name = database.V82Name,
                 AccountId = database.AccountId,
                 TerminalServerAddresses = groupedTerminalAddressesByDatabase.ConnectionAddresses
             }).FirstOrDefault() ??
            throw new NotFoundException($"Не удалось получить данные для инф. базы '{databaseId}'");

        /// <summary>
        /// Получить сгруппированные по инф. базе адреса
        /// терминальных серверов
        /// </summary>
        /// <returns>Сгруппированные по инф. базе адреса
        /// терминальных серверов</returns>
        private IQueryable<GroupedTerminalServerAddressesByDatabaseDto> GetGroupedTerminalServerAddressesByDatabase() =>
            (from database in dbLayer.DatabasesRepository.WhereLazy()
             join accountConfiguration in dbLayer.GetGenericRepository<AccountConfiguration>().WhereLazy()
                 on database.AccountId equals accountConfiguration.AccountId
             join segmentTerminalServer in dbLayer.CloudServicesSegmentTerminalServerRepository.WhereLazy() on
                 accountConfiguration.SegmentId equals segmentTerminalServer.SegmentId
             join terminalServer in dbLayer.CloudTerminalServerRepository.WhereLazy() on segmentTerminalServer
                 .TerminalServerId equals terminalServer.ID
             select new { database.Id, terminalServer.ConnectionAddress }).Distinct().GroupBy(data => data.Id)
            .Select(groupedData => new GroupedTerminalServerAddressesByDatabaseDto
            {
                DatabaseId = groupedData.Key,
                ConnectionAddresses = groupedData.Select(gp => gp.ConnectionAddress).ToList()
            });
        

        /// <summary>
        /// Получить Id терминальных серверов
        /// </summary>
        /// <returns>Список Id терминальных серверов</returns>
        public List<Guid> GetTerminalServersIdList()
            => dbLayer.CloudTerminalServerRepository.Where().Select(x => x.ID).ToList();

        /// <summary>
        /// Получить количество терминальных серверов
        /// </summary>
        /// <returns>Количество терминальных серверов</returns>
        public int GetTerminalServersCount()
            => dbLayer.CloudTerminalServerRepository.All().Count();
    }
}
