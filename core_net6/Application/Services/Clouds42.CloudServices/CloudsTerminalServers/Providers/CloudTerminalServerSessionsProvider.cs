﻿using System.Collections.Concurrent;
using Microsoft.EntityFrameworkCore;
using Cassia;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.CloudsTerminalServers.Mappers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsTerminalServers.Providers
{
    /// <summary>
    /// Провайдер для облачных терминальных серверов
    /// </summary>
    internal class CloudTerminalServerSessionsProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ILogger42 logger)
        : ICloudTerminalServerSessionsProvider
    {
        #region public

        /// <summary>
        /// Поиск сессий пользователей для текущего аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Сессии пользователей</returns>
        public async Task<List<UserRdpSessionsDto>> FindUserRdpSessionsByAccountIdAsync(Guid accountId,
            Guid? accountUserId = null)
        {
            var userName = string.Empty;
            var account = await dbLayer.AccountsRepository.AsQueryable()
                .Include(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .ThenInclude(x => x.CloudServicesSegmentTerminalServers)
                .ThenInclude(x => x.CloudTerminalServer)
                .FirstOrDefaultAsync(x => x.Id == accountId) ??
                          throw new NotFoundException($"Не удалось найти аккаунт {accountId}");

            if (accountUserId.HasValue)
            {
                var user = await dbLayer.AccountUsersRepository.FirstOrDefaultAsync(au => au.Id == accountUserId) ??
                           throw new NotFoundException($"Не удалось найти пользователя {accountUserId}");

                userName = user.Login;
            }

            logger.Debug($"Найдено {account.AccountConfiguration.Segment.CloudServicesSegmentTerminalServers.Count} терминальных серверов");

           var terminalServerDict = account.AccountConfiguration.Segment.CloudServicesSegmentTerminalServers
                .Select(x => x.CloudTerminalServer)
                .Where(x => !string.IsNullOrEmpty(x.ConnectionAddress))
                .DistinctBy(x => x.ConnectionAddress)
                .ToDictionary(x => x.ConnectionAddress, z => z.ID);

            var accountSessionList = await FindSessionsForAccountAsync(terminalServerDict, account);

            var userSessionList = (!accountUserId.HasValue
                ? accountSessionList.ToList()
                : accountSessionList.Where(asl => asl.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase))).ToList();

            return FilterByPermissionSessionsList(userSessionList);
        }

        /// <summary>
        /// Получить список пользователей с активными сессиями
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>Список пользователей с активными сессиями</returns>
        public async Task<List<AccountUserDto>> GetAccountUsersConnectionsInfoList(Guid accountId)
        {
            var accountSessions = await FindUserRdpSessionsByAccountIdAsync(accountId);

            var accountUsers = GetAccountUsersListByAccount(accountId);
            return accountUsers.Where(au => accountSessions.Any(x => x.UserName == au.Login)).OrderBy(z => z.Login)
                .ToList();
        }

        
        /// <summary>
        /// Завершить сессию пользователя
        /// </summary>
        /// <param name="model">Данные сессии</param>>
        public void DropSession(CloudTerminalServerPostRequestCloseDto model)
        {
            var serverName = GetTerminalServerNameById(model.TerminalServerID);

            logger.Debug($"Id сервера {model.TerminalServerID}");
            logger.Debug($"Имя сервера {serverName}");
            logger.Debug($"Id сессии {model.UserRdpSessionID}");

            logger.Debug($"Попытка заверешения сесии {model.UserRdpSessionID} на сервере {serverName}");
            ITerminalServicesManager m = new TerminalServicesManager();
            using var server = m.GetRemoteServer(serverName);
            server.Open();
            var sessionToDrop = server.GetSession(model.UserRdpSessionID);
            sessionToDrop.Logoff();

            logger.Debug($"Сессия {model.UserRdpSessionID} на сервере {serverName} завершена");
        }

        /// <summary>
        /// Получить список сессий аккаунта для терминального сервера
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список сессий аккаунта</returns>
        public List<UserRdpSessionsDto> GetSessions(Guid terminalServerId, Guid accountId)
        {
            try
            {
                var logins = GetUsersLoginByAccountId(accountId);

                var serverName = GetTerminalServerNameById(terminalServerId);

                var sessionList = GetSessionListRequestDto(serverName).ToList();
                if (accountId != Guid.Empty)
                {
                    sessionList = sessionList.Where(sl => logins.Contains(sl.UserName)).ToList();
                    logger.Trace(
                        $"Количество сессий терминального сервера {terminalServerId} по пользователям аккаунта {accountId} содержит {sessionList.Count} элементов");
                }
                else
                {
                    logger.Trace(
                        $"Список сессий для терминального сервера {terminalServerId} содержит {sessionList.Count} элементов");
                }

                return sessionList;
            }
            catch (Exception e)
            {
                logger.Warn(
                    $"Ошибка получения сессий для терминального сервера {terminalServerId} :: {e.GetMessage()}");
                throw;
            }
        }

        public async Task<List<UserRdpSessionsDto>> GetSessionsAsync(Dictionary<string, Guid> terminalServersDict, Guid accountId)
        {
            var logins = await dbLayer.AccountUsersRepository
                .AsQueryable()
                .Where(
                    au =>
                        au.AccountId == accountId && au.CorpUserSyncStatus != "Deleted" &&
                        au.CorpUserSyncStatus != "SyncDeleted")
                .Select(y => y.Login)
                .ToListAsync();

            var result = new ConcurrentBag<UserRdpSessionsDto>();

            await Parallel.ForEachAsync(terminalServersDict, (dict, _) =>
            {
                logger.Debug($"Адрес терминального сервера — {dict.Key}");
                ITerminalServicesManager terminalServicesManager = new TerminalServicesManager();

                try
                {
                    using var server = terminalServicesManager.GetRemoteServer(dict.Key);
                    server.Open();
                    List<ITerminalServicesSession> sessionList;

                    sessionList = server.GetSessions().ToList();
                    logger.Info($"Список сессий содержит {sessionList.Count} элементов");
                        

                    sessionList.MapToUserRdpSessionsDtoList(dict.Value).AsParallel().ForAll(x => result.Add(x));
                }

                catch (Exception ex)
                {
                    logger.Warn($"Произошла ошибка при получении сессий адрес {dict.Key} :: {ex.Message}");
                }

                logger.Info($"Получено {result.Count} сессий");

                return ValueTask.CompletedTask;
            });

            if (accountId != Guid.Empty)
            {
                var filtered =  result.ToList().Where(sl => logins.Contains(sl.UserName, StringComparer.InvariantCultureIgnoreCase)).ToList();

                logger.Info(
                    $"Количество сессий терминальных серверов по пользователям аккаунта {accountId} содержит {filtered.Count} элементов");

                return filtered;
            }
            logger.Info(
                $"Список сессий для терминальных серверов содержит {result.Count} элементов");

            return result.ToList();
        }

        /// <summary>
        /// Дропнуть сессии пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        public async Task DropUserRdpSessionsAsync(Guid userId)
        {
            try
            {
                var accountId = (await dbLayer.AccountUsersRepository.FirstOrDefaultAsync(au => au.Id == userId)).AccountId;
                var userRdpSessions = await FindUserRdpSessionsByAccountIdAsync(accountId, userId);

                logger.Debug($"Получено {userRdpSessions.Count} сессий пользователя {userId}");

                foreach (var userRdpSession in userRdpSessions)
                {
                    var sessionToClose = userRdpSession.MapToCmsPostRequestCloseDto();
                    DropSession(sessionToClose);
                    logger.Debug(
                        $"Сессия пользователя {userId} на терминальном сервере {sessionToClose.TerminalServerID} завершена");
                }

                logger.Debug($"Rdp сессии пользователя {userId} завершены");
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка завершения Сессий пользователя] {userId}");
                throw;
            }
        }

        /// <summary>
        /// Найти и закрыть все RDP сессии пользователей
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="userIds">Id's пользователей</param>
        /// <returns>Результат заверешения сессий</returns>
        public async Task<CloseRdpSessionsDto> FindAndCloseUsersRdpSessionsAsync(Guid accountId, Guid[] userIds)
        {
            foreach (var userId in userIds)
            {
                try
                {
                    var accountSessions = await FindUserRdpSessionsByAccountIdAsync(accountId, userId);
                    foreach (var sessions in accountSessions)
                    {
                        DropSession(new CloudTerminalServerPostRequestCloseDto
                        {
                            TerminalServerID = sessions.TerminalServerID,
                            UserRdpSessionID = sessions.UserRdpSessionID
                        });
                    }

                    logger.Trace("Сеансы завершены для пользователя = {0}", userId);
                }
                catch (Exception ex)
                {
                    logger.Trace("Ошибка завершения сеанса пользователя. {0}", ex.GetFullInfo());
                    return new CloseRdpSessionsDto
                    {
                        IsSuccess = false,
                        Message = "Ошибка при завершении сеанса"
                    };
                }
            }

            return new CloseRdpSessionsDto
            {
                IsSuccess = true,
                Message = "Сеанс(ы) успешно завершен(ы)"
            };
        }

        #endregion

        #region private

        /// <summary>
        /// Получить список сессий терминального сервера
        /// </summary>
        /// <param name="serverAddress">Адрес сервера</param>
        /// <returns>Список сессий</returns>
        private IEnumerable<UserRdpSessionsDto> GetSessionListRequestDto(string serverAddress)
        {
            var result = new List<UserRdpSessionsDto>();
            if (string.IsNullOrEmpty(serverAddress))
            {
                logger.Trace("Имя терминального сервера пусто");
                return result;
            }

            logger.Debug($"Адрес терминального сервера — {serverAddress}");
            ITerminalServicesManager terminalServicesManager = new TerminalServicesManager();
            using (var server = terminalServicesManager.GetRemoteServer(serverAddress))
            {
                server.Open();
                List<ITerminalServicesSession> sessionList;
                try
                {
                    sessionList = server.GetSessions().ToList();
                    logger.Trace($"Список сессий содержит {sessionList.Count} элементов");
                }
                catch (Exception ex)
                {
                    logger.Warn($"Произошла ошибка при получении сессий :: {ex.GetMessage()}");
                    throw;
                }

                if (!sessionList.Any())
                {
                    logger.Info("Список сессий пуст");
                    return result;
                }

                var serverId = GetTerminalServerIdByAddress(serverAddress);

                result.AddRange(sessionList.MapToUserRdpSessionsDtoList(serverId));
            }

            logger.Trace($"Получено {result.Count} сессий");
            return result;
        }

        /// <summary>
        /// Отфильтровать список сессий согласно правам доступа
        /// </summary>
        /// <param name="rows">Список сессий</param>
        /// <returns>Отфильтрованный список сессий</returns>
        private List<UserRdpSessionsDto> FilterByPermissionSessionsList(List<UserRdpSessionsDto> rows)
        {
            var userPrincipal = accessProvider.GetUser();
            if (userPrincipal == null)
                return [];

            return userPrincipal.Groups.Any(g =>
                g is AccountUserGroup.AccountAdmin or AccountUserGroup.Hotline or AccountUserGroup.CloudAdmin)
                ? rows
                : rows.Where(r => r.UserName == userPrincipal.Name).ToList();
        }

        /// <summary>
        /// Находит все RDP сессии аккаунта
        /// </summary>
        /// <param name="terminalServers">Список терминальных серверов</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Список сессий</returns>
        private async Task<IEnumerable<UserRdpSessionsDto>> FindSessionsForAccountAsync(
            Dictionary<string, Guid> terminalServers, Account account)
        {
            try
            {
                var sessionList = await GetSessionsAsync(terminalServers, account.Id);

                return sessionList;
            }

            catch (Exception ex)
            {
                logger.Warn(ex.Message,
                    $"Ошибка получения сессий для аккаунта {account.Id}");

                throw;
            }
        }

        /// <summary>
        /// Получить адрес терминального сервера
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <returns>Адрес терминального сервера</returns>
        private string GetTerminalServerNameById(Guid terminalServerId) => dbLayer.CloudTerminalServerRepository
                                                                               .FirstOrDefault(cts =>
                                                                                   cts.ID == terminalServerId)
                                                                               ?.ConnectionAddress ??
                                                                           throw new NotFoundException(
                                                                               $"Не удалось найти имя сервера с id {terminalServerId}");
        
        /// <summary>
        /// Получить логины пользователей аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Логины пользователей</returns>
        private List<string> GetUsersLoginByAccountId(Guid accountId) => dbLayer.AccountUsersRepository.Where(
                au =>
                    au.AccountId == accountId && au.CorpUserSyncStatus != "Deleted" &&
                    au.CorpUserSyncStatus != "SyncDeleted")
            .Select(y => y.Login).ToList();

        /// <summary>
        /// Получить Id сервера по его адресу
        /// </summary>
        /// <param name="serverAddress">Адрес сервера</param>
        /// <returns>Id сервера</returns>
        private Guid GetTerminalServerIdByAddress(string serverAddress) =>
            dbLayer.CloudTerminalServerRepository.FirstOrDefault(cts => cts.ConnectionAddress == serverAddress)?.ID ??
            throw new NotFoundException($"Не удалось найти Id сервера по адресу {serverAddress}");

        /// <summary>
        /// Получить список пользователей для текущего аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список пользователей для текущего аккаунта</returns>
        private List<AccountUserDto> GetAccountUsersListByAccount(Guid accountId)
        {
            var currentUser = accessProvider.GetUser();

            var accountUsers = dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                .Where(au => au.AccountId == accountId).ToList();

            var res = new List<AccountUserDto>();
            var userInitator = dbLayer.AccountUsersRepository.GetAccountUserByLogin(currentUser.Name);
            if (currentUser.Groups.Any(g => g == AccountUserGroup.AccountUser) && userInitator.AccountId == accountId)
            {
                var user = accountUsers.FirstOrDefault(x => x.Login == currentUser.Name);
                res.Add(user.MapToAccountUserDc());
                return res;
            }

            res.AddRange(accountUsers.Select(accountUser => accountUser.MapToAccountUserDc()));
            return res;
        }

        #endregion
    }
}
