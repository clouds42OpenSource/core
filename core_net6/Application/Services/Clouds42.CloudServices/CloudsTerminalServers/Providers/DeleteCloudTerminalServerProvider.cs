﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsTerminalServers.Providers
{
    /// <summary>
    /// Провайдер удаления терминального сервера
    /// </summary>
    internal class DeleteCloudTerminalServerProvider(
        ICloudTerminalServerProvider cloudTerminalServerProvider,
        IUnitOfWork dbLayer)
        : IDeleteCloudTerminalServerProvider
    {
        /// <summary>
        /// Удалить терминальный сервер
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        public void Delete(Guid terminalServerId)
        {
            var cloudTerminalServer = cloudTerminalServerProvider.GetCloudTerminalServer(terminalServerId);

            if (!AbilityDeleteTerminalServer(terminalServerId))
                throw new ValidateException(
                    $"Удаление терминального сервера \'{cloudTerminalServer.Name}\" не возможно так он используется в сегменте");

            dbLayer.CloudTerminalServerRepository.Delete(cloudTerminalServer);
            dbLayer.Save();
        }

        /// <summary>
        /// Возможность удалить терминальный сервер
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        private bool AbilityDeleteTerminalServer(Guid terminalServerId) =>
            dbLayer.CloudServicesSegmentTerminalServerRepository.FirstOrDefault(sts =>
                sts.TerminalServerId == terminalServerId) == null;
    }
}
