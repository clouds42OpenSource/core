﻿using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.CloudServer;

namespace Clouds42.CloudServices.CloudsTerminalServers.Providers
{
    /// <summary>
    /// Провайдер для создания терминального сервера
    /// </summary>
    internal class CreateCloudTerminalServerProvider(IUnitOfWork dbLayer) : ICreateCloudTerminalServerProvider
    {
        /// <summary>
        /// Создать терминальный сервер
        /// </summary>
        /// <param name="createCloudTerminalServer">Модель создания
        /// терминального сервера</param>
        /// <returns>Id созданного сервера</returns>
        public Guid Create(CreateCloudTerminalServerDto createCloudTerminalServer)
        {
            createCloudTerminalServer.Validate();
            var terminalServer = new CloudTerminalServer
            {
                ID = Guid.NewGuid(),
                Name = createCloudTerminalServer.Name,
                Description = createCloudTerminalServer.Description,
                ConnectionAddress = createCloudTerminalServer.ConnectionAddress
            };
            dbLayer.CloudTerminalServerRepository.Insert(terminalServer);
            dbLayer.Save();

            return terminalServer.ID;
        }
    }
}
