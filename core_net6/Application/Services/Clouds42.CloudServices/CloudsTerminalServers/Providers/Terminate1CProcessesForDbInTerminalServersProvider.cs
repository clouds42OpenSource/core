﻿using Cassia;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.CloudsTerminalServers.Constants;
using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Extensions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.CloudsTerminalServers.Providers
{
    /// <summary>
    /// Провайдер завершения 1С процессов для инфомационной базы
    /// на терминальных серверах
    /// </summary>
    internal class
        Terminate1CProcessesForDbInTerminalServersProvider(ICloudTerminalServerProvider cloudTerminalServerProvider, ICloudLocalizer cloudLocalizer)
        : ITerminate1CProcessesForDbInTerminalServersProvider
    {
        /// <summary>
        /// Завершить 1С процессы для инфомационной базы
        /// на терминальных серверах
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        public void Terminate(Guid databaseId)
        {
            var terminalServerAddressesForDatabaseData =
                cloudTerminalServerProvider.GetTerminalServerAddressesForDatabase(databaseId);

            if (!terminalServerAddressesForDatabaseData.TerminalServerAddresses.Any())
                return;

            if (terminalServerAddressesForDatabaseData.V82Name.IsNullOrEmpty())
                return;

            terminalServerAddressesForDatabaseData.TerminalServerAddresses.ForEach(terminalServerAddress =>
                Terminate1CProcessesInTerminalServer(terminalServerAddressesForDatabaseData.AccountId,
                    terminalServerAddressesForDatabaseData.V82Name,
                    terminalServerAddress));
        }

        /// <summary>
        /// Завершить 1С процессы для инфомационной базы
        /// на терминальном сервере
        /// </summary>
        /// <param name="accountId">Id аккаунта владельца базы</param>
        /// <param name="v82Name">Номер инф. базы</param>
        /// <param name="terminalServerAddress">Адрес терминального сервера</param>
        private void Terminate1CProcessesInTerminalServer(Guid accountId, string v82Name, string terminalServerAddress)
        {
            var terminalServicesManager = new TerminalServicesManager();
            using var server = terminalServicesManager.GetRemoteServer(terminalServerAddress);
            try
            {
                server.Open();
                var terminalServicesProcesses = Get1CProcessesByDatabase(server, v82Name, terminalServerAddress);
                terminalServicesProcesses.ForEach(process => process.Kill());
            }
            catch (Exception ex)
            {
                var completionOfProcesses1CForDatabasePhrase =
                    cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.CompletionOfProcesses1CForDatabase, accountId);

                throw new InvalidOperationException(
                    $"{completionOfProcesses1CForDatabasePhrase} '{v82Name}' на терминальном сервере '{terminalServerAddress}' завершилось с ошибкой: {ex.GetFullInfo()}");
            }
        }

        /// <summary>
        /// Получить 1С процессы для инф. базы на терминальном сервере
        /// </summary>
        /// <param name="terminalServer">Терминальный сервер</param>
        /// <param name="v82Name">Номер инф. базы</param>
        /// <param name="terminalServerAddress">Адрес терминального сервера</param>
        /// <returns>1С процессы инф. базы</returns>
        private static List<ITerminalServicesProcess> Get1CProcessesByDatabase(ITerminalServer terminalServer,
            string v82Name, string terminalServerAddress) =>
            terminalServer.GetProcesses().Where(process =>
                    process.ProcessName is TerminalServerConst.Process1CName or TerminalServerConst.DesignerProcess1CName)
                .Select(process => new
                {
                    process,
                    commandLine = process.GetCommandLine(terminalServerAddress)
                })
                .Where(processData =>
                    !processData.commandLine.IsNullOrEmpty() && processData.commandLine.Contains(v82Name))
                .Select(pr => pr.process).ToList();
    }
}
