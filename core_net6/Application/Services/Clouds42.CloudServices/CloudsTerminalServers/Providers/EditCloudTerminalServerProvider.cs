﻿using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.CloudServer;

namespace Clouds42.CloudServices.CloudsTerminalServers.Providers
{
    /// <summary>
    /// Провайдер для редактирования терминального сервера
    /// </summary>
    internal class EditCloudTerminalServerProvider(
        ICloudTerminalServerProvider cloudTerminalServerProvider,
        IUnitOfWork dbLayer)
        : IEditCloudTerminalServerProvider
    {
        /// <summary>
        /// Редактировать терминальный сервер
        /// </summary>
        /// <param name="cloudTerminalServerDto">Модель терминального сервера</param>
        public void Edit(CloudTerminalServerDto cloudTerminalServerDto)
        {
            cloudTerminalServerDto.Validate();
            var terminalServer = cloudTerminalServerProvider.GetCloudTerminalServer(cloudTerminalServerDto.Id);
            terminalServer.Name = cloudTerminalServerDto.Name;
            terminalServer.Description = cloudTerminalServerDto.Description;
            terminalServer.ConnectionAddress = cloudTerminalServerDto.ConnectionAddress;
            dbLayer.CloudTerminalServerRepository.Update(terminalServer);
            dbLayer.Save();
        }
    }
}
