﻿using Cassia;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;

namespace Clouds42.CloudServices.CloudsTerminalServers.Mappers
{
    /// <summary>
    /// Маппер сессий сервера
    /// </summary>
    public static class CloudTerminalServerSessionsMapper
    {
        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="session">Сессия пользователя</param>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <returns>Результат получения сессий пользователя</returns>
        public static UserRdpSessionsDto MatToUserRdpSessionsDto(this ITerminalServicesSession session, Guid terminalServerId) => new()
        {
            Downtime = session.IdleTime.Seconds,
            EntryTime = session.LoginTime ?? DateTime.Now,
            SessionName = session.ClientName,
            SessionStatus = session.ConnectionState.ToString(),
            TerminalServerID = terminalServerId,
            UserName = session.UserName,
            UserRdpSessionID = session.SessionId
        };

        /// <summary>
        /// Выполнить маппинг списка моделей
        /// </summary>
        /// <param name="sessionList">Сессии пользователей</param>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <returns>Результат получения сессий пользователей</returns>
        public static List<UserRdpSessionsDto> MapToUserRdpSessionsDtoList(
            this List<ITerminalServicesSession> sessionList, Guid terminalServerId) => sessionList
            .Select(terminalServicesSession => terminalServicesSession.MatToUserRdpSessionsDto(terminalServerId))
            .ToList();


        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="session">Сессия пользователя</param>
        /// <returns>Упрощенная модель сессии</returns>
        public static CloudTerminalServerPostRequestCloseDto MapToCmsPostRequestCloseDto(this UserRdpSessionsDto session) =>
            new()
            {
                TerminalServerID = session.TerminalServerID,
                UserRdpSessionID = session.UserRdpSessionID
            };
    }
}
