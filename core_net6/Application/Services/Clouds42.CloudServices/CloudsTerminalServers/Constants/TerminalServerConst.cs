﻿namespace Clouds42.CloudServices.CloudsTerminalServers.Constants
{
    /// <summary>
    /// Константы для работы с терминальным сервером
    /// </summary>
    public static class TerminalServerConst
    {
        /// <summary>
        /// Полномочия на аутентификацию по COM соединению
        /// </summary>
        public const string Authority = "ntlmdomain:AD";

        /// <summary>
        /// Коммандная строка процесса
        /// </summary>
        public const string CommandLine = nameof(CommandLine);

        /// <summary>
        /// Название процесса 1С
        /// </summary>
        public const string Process1CName = "1cv8c.exe";

        /// <summary>
        /// Название процесса 1С в режиме конфигуратора
        /// </summary>
        public const string DesignerProcess1CName = "1cv8.exe";
    }
}
