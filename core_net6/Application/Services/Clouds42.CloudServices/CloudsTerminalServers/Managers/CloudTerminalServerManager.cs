﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsTerminalServers.Managers
{
    /// <summary>
    /// Менеджер для работы с терминальными серверами 
    /// </summary>
    public class CloudTerminalServerManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IDeleteCloudTerminalServerProvider deleteCloudTerminalServerProvider,
        IEditCloudTerminalServerProvider editCloudTerminalServerProvider,
        ICloudTerminalServerProvider cloudTerminalServerProvider,
        ICreateCloudTerminalServerProvider cloudTerminalServer,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать терминальный сервер
        /// </summary>
        /// <param name="createCloudTerminalServer">Модель создания
        /// терминального сервера</param>
        /// <returns>Id созданного сервера</returns>
        public ManagerResult<Guid> CreateCloudTerminalServer(CreateCloudTerminalServerDto createCloudTerminalServer)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                var terminalServerId = cloudTerminalServer.Create(createCloudTerminalServer);

                var message = $"Создан терминальный сервер \"{createCloudTerminalServer.Name}\".";

                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement, message);

                return Ok(terminalServerId);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка добавления нового терминального сервера \"{createCloudTerminalServer.Name}\"]");
                LogEvent(GetInitiatorAccountId, LogActions.AddSegmentOrElement,
                    $"Ошибка добавления нового терминального сервера \"{createCloudTerminalServer.Name}\". Причина: {ex.Message}");
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать терминальный сервер
        /// </summary>
        /// <param name="cloudTerminalServerDto">Модель терминального сервера</param>
        public ManagerResult EditCloudTerminalServer(CloudTerminalServerDto cloudTerminalServerDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);
                editCloudTerminalServerProvider.Edit(cloudTerminalServerDto);

                var message = $"Терминальный сервер \"{cloudTerminalServerDto.Name}\" отредактирован.";

                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка редактирования терминального сервера \"{cloudTerminalServerDto.Name}\"]");
                LogEvent(GetInitiatorAccountId, LogActions.EditSegmentOrElement,
                    $"Ошибка редактирования терминального сервера \"{cloudTerminalServerDto.Name}\". Причина: {ex.Message}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить терминальный сервер
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        public ManagerResult DeleteCloudTerminalServer(Guid terminalServerId)
        {
            var terminalServerName = string.Empty;
            try
            {
                AccessProvider.HasAccess(ObjectAction.Segment_Edit, () => AccessProvider.ContextAccountId);

                var terminalServer = cloudTerminalServerProvider.GetCloudTerminalServer(terminalServerId);
                terminalServerName = terminalServer.Name;
                deleteCloudTerminalServerProvider.Delete(terminalServerId);

                var message = $"Удален терминальный сервер \"{terminalServerName}\"";

                logger.Trace(message + $" пользователем {AccessProvider.Name}");
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, message);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления терминального сервера \"{terminalServerName}\"]";
                _handlerException.Handle(ex, errorMessage);
                LogEvent(GetInitiatorAccountId, LogActions.DeleteSegmentOrElement, $"{errorMessage}. Причина: {ex.Message}");

                return PreconditionFailed(ex.Message);
            }
        }
    }
}
