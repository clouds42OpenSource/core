﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.DataModels;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsTerminalServers.Managers
{
    /// <summary>
    /// Менеджер для работы с сессиями терминального сервера
    /// </summary>
    public class CloudTerminalServerSessionsManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloudTerminalServerSessionsProvider cloudTerminalServerSessionsProvider,
        ICloudTerminalServerProvider cloudTerminalServerProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Поиск сеансов пользователей для текущего аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Список RDP сессий аккаунта</returns>
        public async Task<ManagerResult<List<UserRdpSessionsDto>>> FindUserRdpSessionsByAccountIdAsync(Guid accountId, Guid? accountUserId = null)
        {
            AccessProvider.HasAccess(ObjectAction.CloudTerminalServers_View, () => accountId);

            logger.Info($"Получение RDP сессий пользователей терминальных серверов для аккаунта {accountId}");

            try
            {
                var usersRdpSessionList = await cloudTerminalServerSessionsProvider.FindUserRdpSessionsByAccountIdAsync(accountId, accountUserId);

                return Ok(usersRdpSessionList);
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"При попытке найти RDP сессии для аккаунта {accountId} пользователь {accountUserId} произошла ошибка");
                return PreconditionFailed<List<UserRdpSessionsDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Найти и закрыть все RDP сессии пользователей
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="userIds">Id's пользователей</param>
        /// <returns>Результат заверешения сессий</returns>
        public async Task<ManagerResult<CloseRdpSessionsDto>> FindAndCloseUsersRdpSessions(Guid accountId, Guid[] userIds)
        {
            try
            {
                var accountUserId = (await AccessProvider.GetUserAsync()).Id;

                AccessProvider.HasAccess(ObjectAction.CloudTerminalServers_UsersRdpSessions,
                    () => AccountIdByAccountUser(accountUserId), () => accountUserId);

                var result = await cloudTerminalServerSessionsProvider.FindAndCloseUsersRdpSessionsAsync(accountId, userIds);

                if (result.IsSuccess)
                {
                    var usersLogins = DbLayer.AccountUsersRepository.AsQueryable()
                        .Join(userIds,
                           user => user.Id,
                           id => id,
                        (user, id) => user.Login)
                        .AsEnumerable();

                    LogEvent(() => accountId, LogActions.TerminateSessionsInDatabase,
                        $"Завершение сеансов на сервере для пользователей {String.Join(", ", usersLogins)}");
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<CloseRdpSessionsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Завершить сессию пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ManagerResult CloseUserRdpSession(CloudTerminalServerPostRequestCloseDto model)
        {
            AccessProvider.HasAccess(ObjectAction.CloudTerminalServers_CloseUserRdpSession, () => AccountIdByAccountUser(AccessProvider.GetUser().Id));

            if (!model.IsValid) return PreconditionFailed("Неверная модель");

            try
            {
                cloudTerminalServerSessionsProvider.DropSession(model);

                logger.Debug($"Сессия {model.UserRdpSessionID} на сервере {model.TerminalServerID} заверешена");
                return Ok();
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"Не удалось завершить сессию {model.UserRdpSessionID} на сервер {model.TerminalServerID}");
                return Conflict(ex.Message);
            }
        }

        /// <summary>
        /// Получить RDP сессии пользователей терминального сервера по Id аккаунта
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список RDP сессий</returns>
        public async Task<ManagerResult<List<UserRdpSessionsDto>>> GetUserRdpSessionsAsync(Guid terminalServerId, Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.CloudTerminalServers_View, () => accountId);
            try
            {
                var usersRdpSessionList = await cloudTerminalServerSessionsProvider.GetSessionsAsync(new Dictionary<string, Guid> {{string.Empty, terminalServerId}}, accountId);

                return Ok(usersRdpSessionList);
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"Не удалось получить список RDP сессий терминального сервера {terminalServerId} для аккаунта {accountId}");
                return Conflict<List<UserRdpSessionsDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Дропнуть сессии пользователя
        /// </summary>
        /// <returns>Результат завершения сессий</returns>
        public async Task<ManagerResult> DropUserRdpSessionsAsync()
        {
            IUserPrincipalDto user;

            try
            {
                user = await AccessProvider.GetUserAsync();
            }
            catch (Exception)
            {
                return PreconditionFailed("Произошла ошибка при определении текущего пользователя.");
            }

            if (user == null)
                return PreconditionFailed("Текущий пользователь не определён.");

            var accountUserId = user.Id;

            try
            {
                AccessProvider.HasAccess(ObjectAction.CloudTerminalServers_UsersRdpSessions, () => AccountIdByAccountUser(accountUserId), () => accountUserId);

                await cloudTerminalServerSessionsProvider.DropUserRdpSessionsAsync(accountUserId);
                return Ok();
            }
            catch (Exception ex)
            {
                var capturedAccountUserId = accountUserId;
                logger.Warn(ex, $"Ошибка заверешения сессий пользователя {capturedAccountUserId}");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить Id терминальных серверов
        /// </summary>
        /// <returns>Список Id терминальных серверов</returns>
        public ManagerResult<List<Guid>> GetTerminalServersIdList()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CloudTerminalServers_View);
                return Ok(cloudTerminalServerProvider.GetTerminalServersIdList());
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"При получении списка ID терминальных серверов возникла ошибка. Причина: {ex.GetFullInfo()}");
                return PreconditionFailed<List<Guid>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить количество терминальных серверов
        /// </summary>
        /// <returns>Количество терминальных серверов</returns>
        public ManagerResult<int> GetTerminalServersCount()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CloudTerminalServers_View);
                return Ok(cloudTerminalServerProvider.GetTerminalServersCount());
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"При получении количества терминальных серверов возникла ошибка. Причина: {ex.GetFullInfo()}");
                return PreconditionFailed<int>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список пользователей с активными сессиями
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>Список пользователей с активными сессиями</returns>
        public async Task<ManagerResult<List<AccountUserDto>>> GetAccountUsersConnectionsInfoListAsync(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUsers_View, () => accountId);

                var result = await cloudTerminalServerSessionsProvider.GetAccountUsersConnectionsInfoList(accountId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"При получении списока пользователей аккаунта {accountId} с активными сессиями возникла ошибка. Причина: {ex.GetFullInfo()}");
                return PreconditionFailed<List<AccountUserDto>>(ex.Message);
            }
        }
    }
}
