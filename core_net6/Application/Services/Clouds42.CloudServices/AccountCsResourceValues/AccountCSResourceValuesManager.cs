﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.CloudServices.AccountCsResourceValues
{
    public class AccountCsResourceValuesManager(
        IResourcesService resourcesService,
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        IHandlerException handlerException)
        : BaseManager(accessProvider, unitOfWork, handlerException), IAccountCsResourceValuesManager
    {
        /// <summary>
        /// Отображение идентификаторов некоторых идентификаторов ресурсов, требующих особой обработки, на названия
        /// сервисов
        /// </summary>
        private static readonly Dictionary<Guid, string>  _mapSpecialResIdToCSEnumStrings = new()
        {
            {
                CloudConfigurationProvider.BillingServices.GetEsdl42ServiceId(),
                Clouds42Service.Esdl.ToString()
            },
            {
                CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId(),
                Clouds42Service.Recognition.ToString()
            }
        };

        /// <summary>
        /// Получает имя сервиса по заданному идентификатору ресурса облачного сервиса (cs - cloud service)
        /// для некоторых сервисов, требующих особой обработки.
        /// </summary>
        /// <param name="csResourceId">Идентификатор ресурса облачного сервиса (cs - cloud service)</param>
        /// <returns>Имя сервиса или пустая строка, если для заданного идентификатора ресурса не будет найдено имя сервиса</returns>
        /// <seealso cref="_mapSpecialResIdToCSEnumStrings"/>
        private static string GetSpecialServiceNameByResourceId(Guid csResourceId)
        {
            return _mapSpecialResIdToCSEnumStrings.TryGetValue(csResourceId, out var value) ? value : string.Empty;
        }

        /// <summary>
        /// Получить количество дней до срока истечения срока действия сервисов.
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="accountId">Идентификатор аккаунта, для которого осуществляется проверка</param>
        /// <param name="dateTime">Дата время, на до которой нужно получить дату-время истечения сервисов</param>
        /// <returns>Количество дней, до окончания срока истечения сервиса, или 0, если дата была в прошлом.
        /// Если дата не существует в конфигурации ресурсов, то будет возвращен null.
        /// </returns>
        private async Task<ManagerResult<int>> GetDaysRemainingBeforeTheExpiryOfTheServiceAsync(string serviceName, Guid accountId, DateTime dateTime)
        {

            if (serviceName == Clouds42Service.Recognition.ToString())
                return null;

            if (!Enum.TryParse(serviceName, out Clouds42Service serviceEnum))
                return PreconditionFailed<int>($"Недопустимый тип системного сервиса {serviceName}.");

            var serviceExpirationDate = await DbLayer.ResourceConfigurationRepository.GetServiceExpirationDate(accountId, serviceEnum, ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays"));

            if (!serviceExpirationDate.HasValue)
                return null;

            var days = (serviceExpirationDate.Value.Date - dateTime.Date).Days;

            return Ok(Math.Max(days,0));
        }

        /// <summary>
        /// Получает суммарное значение ресурсов облачного сервиса для указанного пользователя,
        /// ресурса и аккаунта администратора пользователя, если таковой указан.
        /// </summary>
        /// <param name="accountId">Идентификатор администоратора пользователя</param>
        /// <param name="csResourceId">Идентификатор ресурса</param>
        /// <param name="accountUserId">Необязательный идентификатор пользователя</param>
        /// <param name="dateTime">Дата-время на которое нужно получить значение ресурсов</param>
        /// <returns>Количество дней, до истечения срока действия сервисов</returns>
        public async Task<ManagerResult<int>> GetValueAsync(Guid accountId, Guid csResourceId, Guid? accountUserId = null, DateTime? dateTime = null)
        {
            var onTheDateTime = dateTime ?? DateTime.Now;

            ManagerResult<int> result;

            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => accountId);

            var serviceName = GetSpecialServiceNameByResourceId(csResourceId);

            var res = await GetDaysRemainingBeforeTheExpiryOfTheServiceAsync(serviceName, accountId, onTheDateTime);

            if (res != null)
            {
                return res;
            }

            if (accountUserId.HasValue)
            {
                logger.Trace($"result = await GetResourcesValueAsync(accountId={accountId}, csResourceId={csResourceId}, accountUserId.Value={accountUserId.Value});");
                result = await GetResourcesValueAsync(accountId, csResourceId, accountUserId.Value, onTheDateTime);
            }
            else
            {
                var count = await DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValuesModifyCountAsync(accountId, csResourceId, onTheDateTime);
                    result = Ok(count);
            }

            return result;
        }

        /// <summary>
        /// Find Ids by AccountId or cloud service (CS) ResourceId
        /// </summary>
        /// <returns></returns>
        public ManagerResult<List<Guid>> FindIDs(Guid accountId = default, Guid csResourceId = default)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => accountId);

            var accountCsResourceValues =
                DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValues(accountId, csResourceId).ToList();

            return !accountCsResourceValues.Any()
                ? NotFound<List<Guid>>("no resources were found")
                : Ok(accountCsResourceValues.Select(x => x.Id).ToList());
        }

        /// <summary>
        /// Get account cloud service resource value properties
        /// </summary>
        /// <returns></returns>
        public ManagerResult<AccountCSResourceValue> GetProperties(Guid accountCsResourceValueId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => AccountIdByAccountCSResourceValue(accountCsResourceValueId));

            var accountCsResourceValue = DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValue(accountCsResourceValueId);//получаем запись
            if (accountCsResourceValue == null)
                return NotFound<AccountCSResourceValue>(
                    $"Account cloud service resource value with Id: {accountCsResourceValueId} not found");

            return Ok(accountCsResourceValue);
        }

        /// <summary>
        /// Get account cloud service resource value account id
        /// </summary>
        /// <returns></returns>
        public ManagerResult<Guid> GetAccountId(Guid accountCsResourceValueId)
        {

            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => AccountIdByAccountCSResourceValue(accountCsResourceValueId));

            var accountCsResourceValue = DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValue(accountCsResourceValueId);  //получаем запись
            if (accountCsResourceValue == null)
                return NotFound<Guid>(
                    $"Account cloud service resource value with Id: {accountCsResourceValueId} not found");

            return Ok(accountCsResourceValue.AccountId);
        }

        /// <summary>
        /// Get account cloud service resource value cloud service resource id
        /// </summary>
        /// <returns></returns>
        public ManagerResult<Guid> GetCsResourceId(Guid accountCsResourceValueId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => AccountIdByAccountCSResourceValue(accountCsResourceValueId));

            var accountCsResourceValue =
                DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValue(accountCsResourceValueId);//получаем запись
            if (accountCsResourceValue == null)
                return NotFound<Guid>(
                    $"Account cloud service resource value with Id: {accountCsResourceValueId} not found");

            return Ok(accountCsResourceValue.CSResourceId);
        }

        /// <summary>
        /// Get account cloud service resource value initiator cloud service id
        /// </summary>
        /// <returns></returns>
        public ManagerResult<Guid> GetInitiatorCloudServiceId(Guid accountCsResourceValueId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => AccountIdByAccountCSResourceValue(accountCsResourceValueId));

            var accountCsResourceValue =
                DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValue(accountCsResourceValueId);//получаем запись
            if (accountCsResourceValue == null)
                return NotFound<Guid>(
                    $"Account cloud service resource value with Id: {accountCsResourceValueId} not found");

            return Ok(accountCsResourceValue.InitiatorCloudService);
        }

        /// <summary>
        /// Get account cloud service resource value modify date time
        /// </summary>
        /// <returns></returns>
        public ManagerResult<DateTime> GetModifyResourceDateTime(Guid accountCsResourceValueId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => AccountIdByAccountCSResourceValue(accountCsResourceValueId));

            AccountCSResourceValue accountCsResourceValue =
                DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValue(accountCsResourceValueId);//получаем запись
            if (accountCsResourceValue == null)
                return NotFound<DateTime>(
                    $"Account cloud service resource value with Id: {accountCsResourceValueId} not found");

            return Ok(accountCsResourceValue.ModifyResourceDateTime);
        }

        /// <summary>
        /// Get account cloud service resource value modify resource value
        /// </summary>
        /// <returns></returns>
        public ManagerResult<int> GetModifyResourceValue(Guid accountCsResourceValueId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => AccountIdByAccountCSResourceValue(accountCsResourceValueId));

            var accountCsResourceValue =
                DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValue(accountCsResourceValueId);//получаем запись
            if (accountCsResourceValue == null)
                return NotFound<int>(
                    $"Account cloud service resource value with Id: {accountCsResourceValueId} not found");

            return Ok(accountCsResourceValue.ModifyResourceValue);
        }

        /// <summary>
        /// Get account cloud service resource value modify resource comment
        /// </summary>
        /// <returns></returns>
        public ManagerResult<string> GetModifyResourceComment(Guid accountCsResourceValueId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => AccountIdByAccountCSResourceValue(accountCsResourceValueId));
            var accountCsResourceValue =
                DbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValue(accountCsResourceValueId);//получаем запись
            if (accountCsResourceValue == null)
                return NotFound<string>(
                    $"Account cloud service resource value with Id: {accountCsResourceValueId} not found");

            return Ok(accountCsResourceValue.ModifyResourceComment ?? string.Empty);
        }

        public ManagerResult<bool> IsAssignedResource(Guid accountId, Guid accountUserId, string resourcesTypeStr)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => accountId);

            if (!Enum.TryParse(resourcesTypeStr, out ResourceType resourceType))
                return PreconditionFailed<bool>($"Не известный тип услуги '{resourcesTypeStr}'");

            var res = DbLayer.ResourceRepository.FirstOrDefault(x => x.AccountId == accountId
                                                                     && x.Subject == accountUserId
                                                                     && x.BillingServiceType.SystemServiceType == resourceType);
            if (res == null)
                return Ok(false);

            return res.DemoExpirePeriod.HasValue ? Ok(res.DemoExpirePeriod.Value <= DateTime.Now) : Ok(true);
        }

        /// <summary>
        /// Проверяем активен сервис или нет
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="subject">Id пользователя</param>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="type">Тип ресурса</param>
        /// <returns>Активность сервиса</returns>
        public ManagerResult<bool> IsActiveService(Guid accountId, Guid subject, string serviceName, string type = null)
        {
            AccessProvider.HasAccess(ObjectAction.AccountCSResource_PaidService, () => accountId);

            if (!Enum.TryParse(serviceName, out Clouds42Service service))
                return NotFound<bool>("Error occurred. Service name isn't valid");

            logger.Debug($"accountId {accountId}; Subject {subject}; serviceName {serviceName}");

            var accountIdPeid = accountId;
            if (service == Clouds42Service.MyEnterprise)
            {
                var res = DbLayer.ResourceRepository.FirstOrDefault(x => x.Subject == subject && x.BillingServiceType.Service.SystemService == service);
                accountIdPeid = res == null
                    ? accountId
                    : (res.AccountSponsorId ?? accountId);
            }
            var resConfig = resourcesService.GetResourceConfig(accountIdPeid, service);
            var isActive = resConfig is { FrozenValue: false };

            if (string.IsNullOrEmpty(type))
                return Ok(isActive);

            if (!Enum.TryParse(type, out ResourceType resType))
                return NotFound<bool>("Error occurred. Resource Type isn't valid");

            var userResource = resourcesService.GetResources(accountIdPeid, service, resType).FirstOrDefault(r => r.Subject == subject);

            if (userResource == null)
                isActive = false;

            return Ok(isActive);
        }

		#region internalCode

        /// <summary>
        /// Асинхронная реализация метода GetResourcesValue с параметром accountUserId.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="csResourceId"></param>
        /// <param name="accountUserId"></param>
        /// <returns></returns>
        private async Task<ManagerResult<int>> GetResourcesValueAsync(Guid accountId, Guid csResourceId, Guid accountUserId, DateTime onTheDateTime)
        {
            var csres = await (
                from   csResource in DbLayer.CsResourceRepository.AsQueryable()
                where  csResource.Id == csResourceId
                select csResource
            ).FirstOrDefaultAsync();

            if (csres == null)
                return NotFound<int>($"[GetValue] Resource with id {csResourceId} was not found");

            var resourceName = csres.ResourcesName;
            logger.Trace($"[GetValue] Resource name is {resourceName}");
            switch (resourceName)
            {
                case "MyEntUser":
                    var res = await (
                        from   aresource in DbLayer.ResourceRepository.AsQueryable()
                        where  aresource.Subject == accountUserId
                        select aresource
                    ).ToListAsync();

                    logger.Trace($"[GetValue_MyEntUser] {res.Count} resources were found for the user with id={accountUserId}");

                    var myEntConf = await (
                        from   aresource in res.AsQueryable()
                        where  aresource.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise
                        select aresource
                    ).ToListAsync();

                    if (myEntConf.Any())
                        return Ok(true.ToInt());

                    logger.Warn($"[GetValue_MyEntUser] Resource MyEnterprise was not found for the account user with id={accountUserId}");

                    return Ok(false.ToInt());
                case "MyEntFrozen":
                    var resourceConf = await (
                        from   resourcesConfiguration in DbLayer.ResourceConfigurationRepository.AsQueryable()
                        where  resourcesConfiguration.AccountId  == accountId
                        &&     resourcesConfiguration.CreateDate <= onTheDateTime
                        select resourcesConfiguration
                    ).ToListAsync();

                    logger.Trace($"[GetValue_MyEntFrozen] {resourceConf.Count} resource configs were found for the user with id={accountId}");
                    var myEntResConf = await (
                        from   conf in resourceConf.AsQueryable()
                        where  conf.BillingService.SystemService == Clouds42Service.MyEnterprise
                        select conf
                    ).ToListAsync();

                    if (!myEntResConf.Any())
                    {
                        var err = $"[GetValue_MyEntFrozen] Resource MyEnterprise wasn't found for the account with id={accountId}";
                        logger.Warn(err);
                        return Ok(true.ToInt());
                    }

                    var isFrozen = await (
                        from   conf in myEntResConf.AsQueryable()
                        where  conf.Frozen == true
                        select conf
                    ).AnyAsync();

                    var not = isFrozen ? "" : "not ";
                    logger.Trace($"[GetValue_MyEntFrozen] Service for the account with id={accountId} is {not} frozen");
                    return Ok(isFrozen.ToInt());
                default:
                    return Ok(0);
            }
        }

        #endregion

    }
}
