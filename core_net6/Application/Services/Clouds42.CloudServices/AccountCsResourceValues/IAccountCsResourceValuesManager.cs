﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;

namespace Clouds42.CloudServices.AccountCsResourceValues;

public interface IAccountCsResourceValuesManager
{
    /// <summary>
    /// Получает суммарное значение ресурсов облачного сервиса для указанного пользователя,
    /// ресурса и аккаунта администратора пользователя, если таковой указан.
    /// </summary>
    /// <param name="accountId">Идентификатор администоратора пользователя</param>
    /// <param name="csResourceId">Идентификатор ресурса</param>
    /// <param name="accountUserId">Необязательный идентификатор пользователя</param>
    /// <param name="dateTime">Дата-время на которое нужно получить значение ресурсов</param>
    /// <returns>Количество дней, до истечения срока действия сервисов</returns>
    Task<ManagerResult<int>> GetValueAsync(Guid accountId, Guid csResourceId, Guid? accountUserId = null, DateTime? dateTime = null);

    /// <summary>
    /// Find Ids by AccountId or cloud service (CS) ResourceId
    /// </summary>
    /// <returns></returns>
    ManagerResult<List<Guid>> FindIDs(Guid accountId = default, Guid csResourceId = default);

    /// <summary>
    /// Get account cloud service resource value properties
    /// </summary>
    /// <returns></returns>
    ManagerResult<AccountCSResourceValue> GetProperties(Guid accountCsResourceValueId);

    /// <summary>
    /// Get account cloud service resource value account id
    /// </summary>
    /// <returns></returns>
    ManagerResult<Guid> GetAccountId(Guid accountCsResourceValueId);

    /// <summary>
    /// Get account cloud service resource value cloud service resource id
    /// </summary>
    /// <returns></returns>
    ManagerResult<Guid> GetCsResourceId(Guid accountCsResourceValueId);

    /// <summary>
    /// Get account cloud service resource value initiator cloud service id
    /// </summary>
    /// <returns></returns>
    ManagerResult<Guid> GetInitiatorCloudServiceId(Guid accountCsResourceValueId);

    /// <summary>
    /// Get account cloud service resource value modify date time
    /// </summary>
    /// <returns></returns>
    ManagerResult<DateTime> GetModifyResourceDateTime(Guid accountCsResourceValueId);

    /// <summary>
    /// Get account cloud service resource value modify resource value
    /// </summary>
    /// <returns></returns>
    ManagerResult<int> GetModifyResourceValue(Guid accountCsResourceValueId);

    /// <summary>
    /// Get account cloud service resource value modify resource comment
    /// </summary>
    /// <returns></returns>
    ManagerResult<string> GetModifyResourceComment(Guid accountCsResourceValueId);

    ManagerResult<bool> IsAssignedResource(Guid accountId, Guid accountUserId, string resourcesTypeStr);

    /// <summary>
    /// Проверяем активен сервис или нет
    /// </summary>
    /// <param name="accountId">Id аккаунта</param>
    /// <param name="subject">Id пользователя</param>
    /// <param name="serviceName">Название сервиса</param>
    /// <param name="type">Тип ресурса</param>
    /// <returns>Активность сервиса</returns>
    ManagerResult<bool> IsActiveService(Guid accountId, Guid subject, string serviceName, string type = null);
}
