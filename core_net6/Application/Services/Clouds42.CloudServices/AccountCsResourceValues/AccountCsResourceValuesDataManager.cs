﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountCSResourceValues;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.AccountCsResourceValues
{
    /// <summary>
    /// Менеджер управления данными движемых ресурсов.
    /// </summary>
    public class AccountCsResourceValuesDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IIncreaseDecreaseResourceProvider increaseDecreaseResourceProvider,
        IFlowResourcesScopeDataProvider flowResourcesScopeDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountCsResourceValuesDataManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получает суммарное значение ресурсов облачного сервиса для указанного пользователя, 
        /// ресурса и аккаунта администратора пользователя, если таковой указан.
        /// </summary>
        /// <param name="accountId">Идентификатор администоратора пользователя</param>
        /// <param name="csResourceId">Идентификатор ресурса</param>
        /// <param name="dateTime">Дата, на момент которой нужно получить вычисления</param>
        /// <returns>Сумарное значение ресурсов заданного сервсиа у аккаунта.</returns>
        public async Task<ManagerResult<int>> GetValueAsync(Guid accountId, Guid csResourceId, DateTime? dateTime = null)
        {
            try
            {
                ProcessWithTimingFixation("Auth", ()=> AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_View, () => accountId));
                var scopeValue = await ProcessWithTimingFixation("GetScopeValue", () => flowResourcesScopeDataProvider.GetScopeValue(accountId, csResourceId, dateTime));
                return Ok(scopeValue);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения ресурса у аккаунта.]", () => accountId, () => csResourceId, () => dateTime);
                return PreconditionFailed<int>(ex.Message);
            }
        }

        /// <summary>
        /// Начислить ресурс.
        /// </summary>
        /// <param name="model">Входящая модель данных.</param>
        public ManagerResult IncreaseResource(IncreaseDecreaseValueModelDto model)
        {
            try
            {

                ProcessWithTimingFixation("Auth", () => AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_Add, () => model?.AccountId));

                if (model.AccountIdValue == Guid.Empty)
                    throw new ValidateException($"Поле {nameof(model.AccountIdValue)} не заполнено");

                if (!model.CSResourceId.HasValue || model.CSResourceId.Value == Guid.Empty)
                    throw new ValidateException($"Поле {nameof(model.CSResourceId)} не заполнено");

                ProcessWithTimingFixation("ChangeValue",
                    () => increaseDecreaseResourceProvider.ChangeValue(model.AccountIdValue, model.CSResourceIdValue, model.ModifyResourceValue ?? 0, model.ModifyResourceComment));

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка начисления ресурсов аккаунту.]", () => model.ToString());
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Списание ресурса.
        /// </summary>
        /// <param name="model">Входящая модель данных.</param>
        /// <returns></returns>
        public ManagerResult DecreaseResource(IncreaseDecreaseValueModelDto model)
        {
            try
            {
                ProcessWithTimingFixation("Auth", ()=>AccessProvider.HasAccess(ObjectAction.AccountCSResourceValues_Add, () => model?.AccountId));

                if (model.AccountIdValue == Guid.Empty)
                    throw new ValidateException($"Поле {nameof(model.AccountIdValue)} не заполнено");

                ProcessWithTimingFixation("ChangeValue", 
                    ()=> increaseDecreaseResourceProvider.ChangeValue(model.AccountIdValue, model.CSResourceIdValue, -Math.Abs(model.ModifyResourceValue ?? 0), model.ModifyResourceComment));

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка списания ресурсов у аккаунта.]", () => model.ToString());
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
