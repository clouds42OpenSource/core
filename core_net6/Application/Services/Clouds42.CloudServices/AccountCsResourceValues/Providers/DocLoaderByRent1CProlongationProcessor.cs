﻿using System.Linq.Expressions;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.AccountCsResourceValues.Providers
{
    /// <summary>
    /// Обработчик по начислению лицензий в рамках активного сервиса Аренда 1С.
    /// </summary>
    internal class DocLoaderByRent1CProlongationProcessor(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IFlowResourcesScopeDataProvider flowResourcesScopeDataProvider,
        IIncreaseDecreaseResourceProvider increaseDecreaseResourceProvider,
        ICloudLocalizer cloudLocalizer,
        IAllowIncreaseDocLoaderByStandardRent1CChecker allowIncreaseDocLoaderByStandardRent1CChecker)
        : IDocLoaderByRent1CProlongationProcessor
    {
        private readonly Lazy<Guid> _recognition42ServiceId = new(CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId);
        private readonly Lazy<int> _optimalLimitRc42ResourceForActiveRent1C = new(CloudConfigurationProvider.BillingServices
            .GetOptimalLimitRc42ResourceForActiveRent1C);

        /// <summary>
        /// Выполнить обработку по начислению лицензий в рамках активного сервиса Аренда 1С.
        /// </summary>
        public void ProcessIncreaseForAccounts(Expression<Func<Account, bool>> filter = null)
        {
            var query =
                from resConfRent1C in dbLayer.ResourceConfigurationRepository.WhereLazy(rc =>
                    rc.BillingService.SystemService == Clouds42Service.MyEnterprise)
                join resConfEsdl in dbLayer.ResourceConfigurationRepository.WhereLazy(rc =>
                        rc.BillingService.SystemService == Clouds42Service.Esdl) on resConfRent1C.AccountId equals
                    resConfEsdl.AccountId
                join inflowPayment in dbLayer.PaymentRepository.WhereLazy(p =>
                        p.Status == PaymentStatus.Done.ToString() && p.OperationType == PaymentType.Inflow.ToString() &&
                        p.Sum > 0) on
                    resConfRent1C.AccountId equals inflowPayment.AccountId
                join scopeRes in dbLayer.FlowResourcesScopeRepository.WhereLazy(s =>
                    s.FlowResourceId == _recognition42ServiceId.Value) on resConfRent1C.AccountId equals scopeRes
                    .AccountId
                where scopeRes.ScopeValue < _optimalLimitRc42ResourceForActiveRent1C.Value ||
                      resConfRent1C.ExpireDate > resConfEsdl.ExpireDate
                select resConfRent1C.BillingAccounts.Account;

            var accounts = query.Distinct();

            if (filter != null)
            {
                accounts = accounts.Where(filter);
            }

            foreach (var account in accounts.ToList())
            {
                ProcessIncrease(account.Id);
            }
        }

        /// <summary>
        /// Выполнить обработку по начислению лицензий в рамках активного сервиса Аренда 1С.
        /// </summary>
        /// <param name="accountId"></param>
        public void ProcessIncrease(Guid accountId)
        {
            if (!allowIncreaseDocLoaderByStandardRent1CChecker.CheckAllow(accountId))
                return;

            Rec42Process(accountId);
            EsdlProcess(accountId);
        }

        /// <summary>
        /// Обработать лицензии ЕСДЛ.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        private void EsdlProcess(Guid accountId)
        {
            var esdlResConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingService.SystemService == Clouds42Service.Esdl);

            if (esdlResConfig == null)
                return;

            var rent1CResConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            if (rent1CResConfig == null)
                return;

            if (!(rent1CResConfig.ExpireDate > esdlResConfig.ExpireDate))
            {
                return;
            }

            esdlResConfig.ExpireDate = rent1CResConfig.ExpireDate;
            dbLayer.ResourceConfigurationRepository.Update(esdlResConfig);
            dbLayer.Save();
        }

        /// <summary>
        /// Обработать лицензии рекогнишен42.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        private void Rec42Process(Guid accountId)
        {
            var scopeValue = flowResourcesScopeDataProvider.GetScopeValue(accountId, _recognition42ServiceId.Value)
                .Result;
            if (scopeValue >= _optimalLimitRc42ResourceForActiveRent1C.Value)
                return;

            var diff = Math.Max(0, _optimalLimitRc42ResourceForActiveRent1C.Value - scopeValue);

            var message =
                $"Добавлено {diff} страниц для распознавания в рамках услуги \"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C,accountId)} Стандарт\"";
            increaseDecreaseResourceProvider.ChangeValue(accountId, _recognition42ServiceId.Value, diff, message);

            LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.PagesAmountChanged, message);
        }
    }
}
