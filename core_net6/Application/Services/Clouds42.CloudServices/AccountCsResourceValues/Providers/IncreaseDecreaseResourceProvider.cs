﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.AccountCsResourceValues.Providers
{
    /// <summary>
    /// Провайдер зменения значения ресурсов аккаунта.
    /// </summary>
    internal class IncreaseDecreaseResourceProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IIncreaseDecreaseResourceProvider
    {
        private readonly string _initiatorNumber = CloudConfigurationProvider.CoreWorker.GetOperationInitiatorNumber();

        /// <summary>
        /// Изменить значение ресурса.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="flowResourceId">Номер сервсиа.</param>
        /// <param name="value">Значение списания/начсиления ресурса.</param>
        /// <param name="comment">Коментарий к изменению.</param>
        public void ChangeValue(Guid accountId, Guid flowResourceId, int value, string comment)
        {

            if (flowResourceId == CloudConfigurationProvider.BillingServices.GetEsdl42ServiceId())
            {
                ChangeEsdl42ResourceValue(accountId, value);
                return;
            }

            if (flowResourceId == CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId())
            {
                ChangeRecognition42ResourceValue(accountId, flowResourceId, value, comment);
                return;
            }

            throw new NotSupportedException($"Недопустимый номер сервиса {flowResourceId}");
        }

        /// <summary>
        /// Изменить значение ресурса сервиса Recognition42.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="flowResourceId">Номер сервсиа.</param>
        /// <param name="value">Значение списания/начсиления ресурса.</param>
        /// <param name="comment">Коментарий к изменению.</param>
        private void ChangeRecognition42ResourceValue(Guid accountId, Guid flowResourceId, int value, string comment)
        {
            var initiatorId = GetInitiatorId();
            
            try
            {
                dbLayer.AccountCsResourceValueRepository
                    .Insert(new AccountCSResourceValue
                    {
                        Id = Guid.NewGuid(),
                        CSResourceId = flowResourceId,
                        InitiatorCloudService = initiatorId,
                        ModifyResourceValue = value,
                        ModifyResourceComment = comment,
                        ModifyResourceDateTime = DateTime.Now,
                        AccountId = accountId
                    });

                var flowResources = dbLayer.FlowResourcesScopeRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.AccountId == accountId && x.FlowResourceId == flowResourceId)
                    .ToList();

                foreach (var flowResource in flowResources)
                {
                    flowResource.ScopeValue = flowResource.ScopeValue + value < 0 ? 0 : flowResource.ScopeValue + value;
                }

                dbLayer.BulkUpdate(flowResources);

                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка изменения значения реcурса] '{flowResourceId}' у аккаунта '{accountId}' на '{value}'");

                throw;
            }

        }

        /// <summary>
        /// Изменить значение ресурса сервиса Esdl42.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="value">Значение списания/начсиления ресурса.</param>
        private void ChangeEsdl42ResourceValue(Guid accountId, int value)
        {
            var rc = dbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.AccountId == accountId && x.BillingService.SystemService == Clouds42Service.Esdl)
                .ToList();

            foreach (var item in rc)
            {
                item.ExpireDate = item.ExpireDate.Value.AddDays(value);
                item.Frozen = item.ExpireDate < DateTime.Now;
            }

            dbLayer.BulkUpdate(rc);
            dbLayer.Save();
        }

        /// <summary>
        /// Получинь номер инициатора операции.
        /// </summary>
        private Guid GetInitiatorId()
        {
            var currentUser = accessProvider.GetUser();

            Guid initiatorId;
            if (currentUser != null)
            {
                initiatorId = currentUser.Id;
            }
            else
            {
                if (!Guid.TryParse(_initiatorNumber, out initiatorId))
                    throw new InvalidDataException("Core42 guid id key not a guid!");
            }

            return initiatorId;
        }
    }
}
