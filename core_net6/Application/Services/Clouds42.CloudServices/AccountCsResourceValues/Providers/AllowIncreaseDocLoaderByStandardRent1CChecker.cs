﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.CloudServices.AccountCsResourceValues.Providers
{
    /// <summary>
    /// Класс проверки наличия возможности начислить ЗД в рмках активной аренды 1С.
    /// </summary>
    internal class AllowIncreaseDocLoaderByStandardRent1CChecker(IUnitOfWork dbLayer)
        : IAllowIncreaseDocLoaderByStandardRent1CChecker
    {
        /// <summary>
        /// Проверить возможность бесплатного начисления лицензий ЗД.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>true - если начислить можно и false в противном случае.</returns>
        public bool CheckAllow(Guid accountId)
        {
            var resConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            if (resConfig == null || resConfig.Frozen == true)
                return false;

            var hasActiveStandardResources = dbLayer.ResourceRepository.Any(r => r.AccountId == accountId && r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);

            if (!hasActiveStandardResources)
                return false;

            return dbLayer.PaymentRepository.Any(p => p.AccountId == accountId && p.Status == PaymentStatus.Done.ToString() && p.OperationType == PaymentType.Inflow.ToString() && p.Sum > 0);
        }
    }
}