﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.CloudServices.AccountCsResourceValues.Providers
{
    /// <summary>
    /// Провайдер данных сводной таблицы движемых ресурсов.
    /// </summary>
    internal class FlowResourcesScopeDataProvider(IUnitOfWork dbLayer) : IFlowResourcesScopeDataProvider
    {
        /// <summary>
        /// Добавить новую запись.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="flowResourceId">Номер ресурса.</param>
        /// <param name="scopeValue">Опционально. Сводное значение ресурса.</param>
        public void Insert(Guid accountId, Guid flowResourceId, int scopeValue = 0)
        {
            dbLayer.FlowResourcesScopeRepository.Insert(new FlowResourcesScope
            {
                AccountId = accountId,
                FlowResourceId = flowResourceId,
                ScopeValue = scopeValue
            });
            dbLayer.Save();
        }

        /// <summary>
        /// Получить сумарное значение ресурсов заданного сервсиа у аккаунта.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="flowResourceId">Номер сервиса.</param>
        /// <param name="dateTime">Дата, на момент которой нужно получить вычисления.</param>
        /// <returns>Сумарное значение ресурсов заданного сервсиа у аккаунта.</returns>
        public async Task<int> GetScopeValue(Guid accountId, Guid flowResourceId, DateTime? dateTime = null)
        {

            if (flowResourceId == CloudConfigurationProvider.BillingServices.GetEsdl42ServiceId())
                return await GetEsdl42ResourceScopeValue(accountId);

            if (flowResourceId == CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId())
                return await GetRecognition42ResourceScopeValue(accountId, flowResourceId, dateTime);

            throw new NotSupportedException($"Недопустимый номер сервиса {flowResourceId}");
        }

        /// <summary>
        /// Получить сумарное значение ресурсов сервиса recognition42 у аккаунта.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="flowResourceId">Номер сервиса.</param>
        /// <param name="dateTime">Дата, на момент которой нужно получить вычисления.</param>
        /// <returns>Сумарное значение ресурсов сервиса recognition42 у аккаунта.</returns>
        private async Task<int> GetRecognition42ResourceScopeValue(Guid accountId, Guid flowResourceId, DateTime? dateTime)
        {

            if (dateTime.HasValue)
                return await dbLayer.AccountCsResourceValueRepository.GetAccountCsResourceValuesModifyCountAsync(
                    accountId, flowResourceId, dateTime.Value);

            var flowResource = await dbLayer.FlowResourcesScopeRepository.AsQueryableNoTracking().FirstOrDefaultAsync(r =>
                r.AccountId == accountId && r.FlowResourceId == flowResourceId) ?? throw new NotFoundException($"Не найдена запись сумм в сводной таблице по сервису '{flowResourceId}' для аккауунта '{accountId}'");

            return flowResource.ScopeValue;
        }

        /// <summary>
        /// Получить сумарное значение ресурсов сервиса ЕСДЛ42 у аккаунта.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>Cумарное значение ресурсов сервиса ЕСДЛ42 у аккаунта.</returns>
        private async Task<int> GetEsdl42ResourceScopeValue(Guid accountId)
        {
            var serviceExpirationDate = await dbLayer.ResourceConfigurationRepository.GetServiceExpirationDate(accountId, Clouds42Service.Esdl, ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays"));

            if (!serviceExpirationDate.HasValue)
                return 0;

            var days = (serviceExpirationDate.Value.Date - DateTime.Now.Date).Days;

            return Math.Max(days, 0);
        }

    }
}
