﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountCSResourceValues;

namespace Clouds42.CloudServices.AccountCsResourceValues;

public interface IAccountCsResourceValuesDataManager
{
    /// <summary>
    /// Получает суммарное значение ресурсов облачного сервиса для указанного пользователя, 
    /// ресурса и аккаунта администратора пользователя, если таковой указан.
    /// </summary>
    /// <param name="accountId">Идентификатор администоратора пользователя</param>
    /// <param name="csResourceId">Идентификатор ресурса</param>
    /// <param name="dateTime">Дата, на момент которой нужно получить вычисления</param>
    /// <returns>Сумарное значение ресурсов заданного сервсиа у аккаунта.</returns>
    Task<ManagerResult<int>> GetValueAsync(Guid accountId, Guid csResourceId, DateTime? dateTime = null);

    /// <summary>
    /// Начислить ресурс.
    /// </summary>
    /// <param name="model">Входящая модель данных.</param>
    ManagerResult IncreaseResource(IncreaseDecreaseValueModelDto model);

    /// <summary>
    /// Списание ресурса.
    /// </summary>
    /// <param name="model">Входящая модель данных.</param>
    /// <returns></returns>
    ManagerResult DecreaseResource(IncreaseDecreaseValueModelDto model);
}
