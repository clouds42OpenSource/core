﻿using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;
using Clouds42.StateMachine.Contracts.Models;
using CommonLib.Enums;

namespace Clouds42.CloudServices.Providers
{
    /// <summary>
    /// Провайдер блокировки и разблокировки аренды для юзеров
    /// </summary>
    public class Rent1CAccountUserProvider(
        IResourceDataProvider resourceDataProvider,
        ICloudLocalizer cloudLocalizer,
        IUnitOfWork unitOfWork,
        ILockUnlockAcDbAccessesForAccountUserProcessFlow flow,
        ILogger42 logger)
    {
        /// <summary>
        /// Заблокировать доступы пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <returns>Модель для блокировки пользователя в AD</returns>
        public EditUserGroupsAdModelDto Lock(AccountUser accountUser)
            => Lock(accountUser, GetResources(accountUser.Id));

        /// <summary>
        /// Разблокировать доступы пользователя. 
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <returns>Модель для блокировки пользователя в AD</returns>
        public EditUserGroupsAdModelDto UnLock(AccountUser accountUser)
            => UnLock(accountUser, GetResources(accountUser.Id));

        /// <summary>
        /// Заблокировать доступы пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="accountUseResources">Список ресурсов</param>
        /// <returns>Модель для блокировки пользователя в AD</returns>
        public EditUserGroupsAdModelDto Lock(AccountUser accountUser, List<Resource> accountUseResources)
        {
            var lockResult = flow.Run(
                new LockUnlockAcDbAccessesParamsDto
                {
                    AccountUserId = accountUser.Id,
                    IsAvailable = false
                }, false);

            return ProcessLockUnlockResult(lockResult, accountUser, accountUseResources,
                EditUserGroupsAdModelDto.OperationEnum.Exclude);
        }

        /// <summary>
        /// Разблокировать доступы пользователя. 
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="accountUseResources">Список ресурсов</param>
        /// <returns>Модель для разблокировки пользователя в AD</returns>
        public EditUserGroupsAdModelDto UnLock(AccountUser accountUser, List<Resource> accountUseResources)
        {
            var unlockResult = flow.Run(
                new LockUnlockAcDbAccessesParamsDto
                {
                    AccountUserId = accountUser.Id,
                    IsAvailable = true
                }, false);

            return ProcessLockUnlockResult(unlockResult, accountUser, accountUseResources,
                EditUserGroupsAdModelDto.OperationEnum.Include);
        }

        /// <summary>
        /// Получить список ресурсов пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Список ресурсов пользователя</returns>
        private List<Resource> GetResources(Guid accountUserId)
        {
            return resourceDataProvider.GetResources(
                Clouds42Service.MyEnterprise,
                r => r.Subject != null && r.Subject == accountUserId,
                ResourceType.MyEntUser, ResourceType.MyEntUserWeb);
        }

        /// <summary>
        /// Обработать результат блокировки/разблокировки доступов пользователя
        /// </summary>
        /// <param name="lockUnlockResult">Результат блокировки/разблокировки доступов пользователя</param>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="accountUserResources">Список ресурсов</param>
        /// <param name="operationType">Тип операции</param>
        /// <returns>Модель для блокировки/разблокировки пользователя в AD</returns>
        private EditUserGroupsAdModelDto ProcessLockUnlockResult(StateMachineResult<bool> lockUnlockResult,
            AccountUser accountUser, List<Resource> accountUserResources,
            EditUserGroupsAdModelDto.OperationEnum operationType)
        {
            if (lockUnlockResult.Finish)
            {
                var accountUserAcDbAccesses = unitOfWork.AcDbAccessesRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.AccountUserID == accountUser.Id)
                    .Select(x => new AccountIndexNumberDbNumberDto
                        {
                            AccountIndexNumber = x.Account.IndexNumber, DbNumber = x.Database.DbNumber
                        })
                    .ToList();

                return CreateEditUserGroupsAdModelHelper.CreateEditUserGroupsAdModel(accountUserResources, accountUser,
                    operationType).AppendAcDbAccesses(accountUserAcDbAccesses, accountUser, operationType);
            }
              

            var operationTypeDescription = operationType == EditUserGroupsAdModelDto.OperationEnum.Exclude
                ? cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.ErrorBlockingRent1CForUser, accountUser.AccountId)
                : cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.ErrorUnlockingRent1CForUser,accountUser.AccountId);

            var errorMessage =
                $"{operationTypeDescription} [{accountUser.Id}]:{accountUser.Login}. Причина: {lockUnlockResult.Message}";

            logger.Warn(errorMessage);
            throw new InvalidOperationException(errorMessage);
        }
    }
}
