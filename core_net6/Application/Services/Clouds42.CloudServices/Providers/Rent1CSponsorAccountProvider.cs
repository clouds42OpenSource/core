﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Providers
{
    /// <summary>
    /// Обработчик спонсирования аккаунта.
    /// </summary>
    public interface IRent1CSponsorAccountProvider
    {
        /// <summary>
        /// Обработать активацию спонсорской лицензии в аккаунте.
        /// </summary>
        /// <param name="accountId">Номер спонсируемого аккаунта.</param>
        void ProcessSponsoredAccount(Guid accountId);
    }

    /// <summary>
    /// Обработчик спонсирования аккаунта.
    /// </summary>
    internal class Rent1CSponsorAccountProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ICloudLocalizer cloudLocalizer,
        ICloudServiceLockUnlockProvider cloudServiceLockUnlockProvider)
        : IRent1CSponsorAccountProvider
    {
        /// <summary>
        /// Обработать активацию спонсорской лицензии в аккаунте.
        /// </summary>
        /// <param name="accountId">Номер спонсируемого аккаунта.</param>
        public void ProcessSponsoredAccount(Guid accountId)
        {
            var rent1CConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(c =>
                c.AccountId == accountId && c.BillingService.SystemService == Clouds42Service.MyEnterprise);

            if (rent1CConfig == null)
                throw new InvalidOperationException(
                    $"У аккаунта '{accountId}' не активирован сервис {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)}");

            if (rent1CConfig is { FrozenValue: true, Cost: 0 })
            {
                UnlockService(rent1CConfig);
            }
        }

        /// <summary>
        /// Разблокировать Аренду 1С у спонсируемого аккаунта.
        /// </summary>        
        private void UnlockService(ResourcesConfiguration configuration)
        {
            cloudServiceLockUnlockProvider.Unlock(configuration);

            configuration.ExpireDate = DateTime.Now.AddMonths(1);
            dbLayer.ResourceConfigurationRepository.Update(configuration);

            dbLayer.Save();

            LogEventHelper.LogEvent(dbLayer, configuration.AccountId, accessProvider,
                LogActions.ProlongationService,
                $"Сервис \"{configuration.GetLocalizedServiceName(cloudLocalizer)}\" продлен до \"{configuration.ExpireDate}\".");
        }
    }
}
