﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountCSResourceValues;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.Fasta;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices
{
    /// <summary>
    /// Менеджер для управления ресурсами сервисов облака
    /// </summary>
    public class ResourcesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IResourcesProvider resourcesProvider,
        IHandlerException handlerException,
        IResourcesService resourcesService,
        ICheckAccountDataProvider checkAccountProvider,
        IAccountCsResourceValuesDataManager accountCsResourceValuesDataManager,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IFastaResourcesService fastaResourcesService,
        ICloudLocalizer cloudLocalizer,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IAccessProvider _accessProvider = accessProvider;
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Change resources for "Fasta" service
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ManagerResult> ChangeFastaResources(FastaResourcesDto dto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Resources_SetResConf, () => dto.AccountId);
                var currentResources = await GetPages(dto.AccountId);

                if (dto.ExpireDate.HasValue && currentResources.CurrentExpireDate.Date != dto.ExpireDate.Value.Date)
                {
                    var resousrceConf = await DbLayer.ResourceConfigurationRepository.AsQueryable()
                        .FirstOrDefaultAsync(r => r.AccountId == dto.AccountId && r.BillingService.SystemService == Clouds42Service.Esdl)
                        ?? throw new NotFoundException($"Аккаунт {dto.AccountId} не имеет конфигурации ресурсов для сервиса \"Fasta\"");

                    resousrceConf.ExpireDate = dto.ExpireDate.Value;
                    DbLayer.ResourceConfigurationRepository.Update(resousrceConf);
                    await DbLayer.SaveAsync();

                    LogEvent(() => dto.AccountId, LogActions.ProlongationService,
                        $"Дата окончания лицензии сервиса \"Fasta\" изменена с {currentResources.CurrentExpireDate.Date} на {dto.ExpireDate.Value.Date}");
                }

                if (dto.PagesAmount.HasValue)
                {
                    int pagesDifference = currentResources.PagesLeft - dto.PagesAmount.Value;

                    var changeModel = new IncreaseDecreaseValueModelDto
                    {
                        AccountId = dto.AccountId,
                        CSResourceId = dto.PagesResourceId,
                        ModifyResourceComment = dto.Comment,
                        ModifyResourceValue = Math.Abs(pagesDifference)
                    };

                    if (pagesDifference < 0)
                        accountCsResourceValuesDataManager.IncreaseResource(changeModel);

                    if (pagesDifference > 0)
                        accountCsResourceValuesDataManager.DecreaseResource(changeModel);

                    LogEvent(() => dto.AccountId, LogActions.PagesAmountChanged,
                        $"Количество страниц изменено с {currentResources.PagesAmount} на {dto.PagesAmount.Value}");
                }

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Не удалось изменить ресурсы сервиса \"Fasta\" для аккаунта {dto.AccountId}]");
                return PreconditionFailed(ex.Message);
            }
        }

        private async Task<Recognition42LicensesModelDto> GetPages(Guid accountId)
        {
            var recognition42LicensesModel = new Recognition42LicensesModelDto();
            try
            {
                var pResId = CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId();

                var pagesResourceValue = await accountCsResourceValuesDataManager.GetValueAsync(accountId, pResId);

                recognition42LicensesModel.AccountId = accountId;
                recognition42LicensesModel.PagesResourceId = pResId;

                var esdlResConf = resourcesService.GetResourceConfig(accountId, Clouds42Service.Esdl);

                if (esdlResConf == null)
                    throw new NotFoundException($"Esld resource not found. accountId = {accountId}");
                if (!esdlResConf.ExpireDate.HasValue)
                    throw new NotFoundException($"Esld resource doesn't have value. accountId = {accountId}");
                recognition42LicensesModel.ExpireDate = esdlResConf.ExpireDate.Value;

                recognition42LicensesModel.PagesLeft = pagesResourceValue.Result;
                recognition42LicensesModel.CurrentExpireDate = esdlResConf.ExpireDate.Value;
                recognition42LicensesModel.IsDemoPeriod = checkAccountProvider.CheckAccountIsDemo(accountId);

                //frozen
                recognition42LicensesModel.IsFrozenEsdl = esdlResConf.FrozenValue;
                recognition42LicensesModel.IsFrozenRec42 = false;
                var rec42ResConf = resourcesService.GetResourceConfig(accountId, Clouds42Service.Recognition);
                if (rec42ResConf != null)
                {
                    recognition42LicensesModel.IsFrozenRec42 = rec42ResConf.FrozenValue;
                }
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения Esdl страницы для аккаунта] {accountId}.");
            }

            return recognition42LicensesModel;
        }

        /// <summary>
        /// Изменить демо период Аренды 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель измененения аренды</param>
        /// <returns>Ok - если демо период изменен</returns>
        public ManagerResult ChangeRent1CDemoPeriod(Guid accountId, Rent1CServiceManagerDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_EditResources_ChangeRent1CDemoPeriod,
                    () => accountId);
                resourcesProvider.ChangeRent1CDemoPeriodExpireDateAndRegisterDemoResources(accountId, model);
                logger.Trace($@"Успешное изменение даты окончания демо периода Аренды 1С для аккаунта {accountId}. 
                                    Новая дата завершения демо периода {model.ExpireDate:dd.MM.yyyy HH:mm:ss}");
                LogEvent(() => accountId, LogActions.EditRent1CDemoExpireDate,
                    $"Дата окончания демо периода сервиса " +
                    $"\"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)}\" {model.ExpireDate:dd.MM.yyyy HH:mm:ss}");

                return Ok();
            }
            catch (Exception ex)
            {
                var rent1CValue = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId);
                var message =
                    $"Не удалось изменить дату окончания демо периода сервиса {rent1CValue} для аккаунта {accountId}.";
                LogEvent(() => accountId, LogActions.EditRent1CDemoExpireDate,
                    $"{message} Описание ошибки: {ex.Message}");
                _handlerException.Handle(ex, $"[Ошибка изменения даты окончания демо периода сервиса] {rent1CValue} для аккаунта {accountId}.");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить модель управления ресурсом
        /// </summary>
        /// <param name="resourceId">Id ресурса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель управления ресурсом</returns>
        public ManagerResult<Rent1CUserManagerDto> GetRent1CUserManagerDc(Guid resourceId, Guid accountId)
        {
            try
            {
                var message = $"Получение модели управления ресурсом {resourceId} для редактирования";
                logger.Info(message);

                AccessProvider.HasAccess(ObjectAction.ControlPanel_EditResources_ChangeRent1CResource, () => accountId);
                var model = resourcesProvider.GetRent1CUserManagerDc(resourceId);
                return Ok(model);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка получения модели управления ресурсом] {resourceId} для редактирования");
                return PreconditionFailed<Rent1CUserManagerDto>(ex.Message);
            }
        }

        /// <summary>
        /// Изменить стоимость ресурса
        /// </summary>
        /// <param name="rent1CUserManagerDc">Модель управления ресурсом</param>
        /// <param name="accountId">Id аккаунта</param>
        public ManagerResult EditResourceCost(Rent1CUserManagerDto rent1CUserManagerDc, Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_EditResources_Rent1C, () => accountId);

                var resourceOld = resourcesProvider.GetResource(rent1CUserManagerDc.ResourceId);
                var accountLocale = accountConfigurationDataProvider.GetAccountLocale(accountId);
                var message =
                    $"Изменена стоимость ресурса \"{resourceOld.BillingServiceType.Name}\" с {resourceOld.Cost:0.00} на {rent1CUserManagerDc.Cost:0.00} {accountLocale.Currency}.";

                resourcesProvider.EditResourceCost(rent1CUserManagerDc, accountId);

                logger.Info($"[{accountId}]:{message}");
                LogEvent(() => accountId, LogActions.EditResourceCost, message);
                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    $"Не удалось изменить стоимость ресурса \"{rent1CUserManagerDc.ServiceTypeName}\" на сумму {rent1CUserManagerDc.Cost:0.00}.";

                _handlerException.Handle(ex, $"[Ошибка измения стоимость ресурса] \"{rent1CUserManagerDc.ServiceTypeName}\" на сумму {rent1CUserManagerDc.Cost:0.00}.");
                LogEvent(() => accountId, LogActions.EditResourceCost, $"{message} Описание ошибки: {ex.Message}");

                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить информацию о демо периоде аренды 1С
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Информация о демо периоде аренды 1С</returns>
        public ManagerResult<Rent1CServiceManagerDto> GetRent1DemoPeriodInfo(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_EditResources_ChangeRent1CDemoPeriod,
                    () => accountId);
                var data = resourcesProvider.GetRent1DemoPeriodInfo(accountId);
                logger.Trace($"Получение информации о демо периоде аренды 1С для аккаунта {accountId} завершено успешно");
                return Ok(data);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка получения информации о демо периоде аренды 1С] для аккаунта {accountId}");
                return PreconditionFailed<Rent1CServiceManagerDto>(ex.Message);
            }
        }

        /// <summary>
        /// Покупка лицензий и страниц для сервиса Загрузка документов
        /// </summary>
        /// <param name="pagesAmount">количество страниц</param>
        /// <param name="yearsAmount">количество лицензий</param>
        /// <returns></returns>
        public BuyingEsdlResultModelDto BuyEsdlAndRec42(int pagesAmount, int yearsAmount)
        {
            var userPrincipal = _accessProvider.GetUser();
            AccessProvider.HasAccess(ObjectAction.ManageResources, () => userPrincipal.ContextAccountId);
            //проверка на валидность введенных данных
            var validCount = ValidateCountLicenseAndPage(pagesAmount, yearsAmount);
            if (validCount)
            {
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    Comment = "Выберите количество страниц или лицензий для покупки"
                };
            }

            if (pagesAmount > 0 && yearsAmount <= 0)
                //покупка только страниц
                return fastaResourcesService.BuyRecognition42(userPrincipal.ContextAccountId, pagesAmount);

            if (yearsAmount > 0 && pagesAmount <= 0)
                //покупка только лицензии
                return fastaResourcesService.BuyEsdl(userPrincipal.ContextAccountId, yearsAmount);

            //покупка и страниц и лицензий
            return fastaResourcesService.BuyEsdlAndRecognition42(userPrincipal.ContextAccountId, yearsAmount,
                pagesAmount);
        }

        private bool ValidateCountLicenseAndPage(int pagesAmount, int yearsAmount)
        {
            return pagesAmount == 0 && yearsAmount == 0;
        }


        public enum ResourceManagerResultState
        {
            PartialView,
            JsonError,
            View,
            RedirectToAction,
            Redirect
        }

        public class ControllerManagerResult(ResourceManagerResultState state, string url, object model)
        {
            public ResourceManagerResultState State { get; set; } = state;

            public string Url { get; set; } = url;

            public ControllerManagerResult(ResourceManagerResultState state, string url)
                : this(state, url, null)
            {
            }

            public object Model { get; set; } = model;
            public string TempData { get; set; }
            public string Message { get; set; }
            public Dictionary<string, string> ModelState { get; set; }
        }
    }
}
