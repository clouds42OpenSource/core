﻿using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Http;
using Clouds42.DataContracts.BaseModel;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsFiles.Providers
{
    /// <summary>
    /// Провайдер для работы с файлом облака
    /// </summary>
    internal class CloudFileProvider(IUnitOfWork dbLayer) : ICloudFileProvider
    {
        /// <summary>
        /// Создать файл облака
        /// </summary>
        /// <param name="fileData">Файл</param>
        /// <returns>Id файла</returns>
        public Guid CreateCloudFile(FileDataDto<IFormFile> fileData)
        {
            var file = new CloudFile
            {
                Id = Guid.NewGuid(),
                FileName = fileData.FileName,
                Content = ConvertToByteArray(fileData.Content),
                ContentType = GetContentType(fileData)
            };

            dbLayer.CloudFileRepository.Insert(file);
            dbLayer.Save();
            return file.Id;
        }

        /// <summary>
        /// Попытаться создать файл облака
        /// </summary>
        /// <param name="uploadedFile">Загруженный файл</param>
        /// <returns>Id созданного файла облака или null, если загруженный файл отсутсвует</returns>
        public Guid? TryCreateCloudFile(FileDataDto<IFormFile> uploadedFile)
        {
            if (uploadedFile?.Content != null)
                return CreateCloudFile(uploadedFile);

            return null;
        }

        /// <summary>
        /// Изменить файл
        /// </summary>
        /// <param name="fileId">Id файла</param>
        /// <param name="fileData">Файл для замены</param>
        public void EditCloudFile(Guid fileId, FileDataDto<IFormFile> fileData)
        {
            var file = dbLayer.CloudFileRepository.FirstOrDefault(cloudFile => cloudFile.Id == fileId)
                       ?? throw new NotFoundException($"Файл облака по ID {fileId} не найден");

            file.FileName = fileData.FileName;
            file.ContentType = GetContentType(fileData);
            file.Content = ConvertToByteArray(fileData.Content);

            dbLayer.CloudFileRepository.Update(file);
            dbLayer.Save();
        }

        /// <summary>
        /// Сравнить содержимое файлов
        /// </summary>
        /// <param name="newContent">Новое содержимое</param>
        /// <param name="existContent">Существующее содержимое</param>
        /// <returns>true - если содержимое не отличается</returns>
        public bool CompareCloudFilesContent(byte[] newContent, byte[] existContent)
        {
            if (newContent.Length != existContent.Length)
                return false;

            var newContentLength = (int)Math.Floor(newContent.Length / 8.0);
            var newContentLongArray = Unsafe.As<long[]>(newContent);
            var existContentLongArray = Unsafe.As<long[]>(existContent);

            for (var iterator = 0; iterator < newContentLength; iterator++)
            {
                if (newContentLongArray[iterator] != existContentLongArray[iterator])
                    return false;
            }

            for (var iterator = newContentLength * 8; iterator < newContent.Length; iterator++)
            {
                if (newContent[iterator] != existContent[iterator])
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Получить тип контента для файла
        /// </summary>
        /// <param name="fileData">Файл сервиса</param>
        /// <returns>Тип контента</returns>
        private string GetContentType(FileDataDto<IFormFile> fileData)
        {
            if (fileData.ContentType.IsNullOrEmpty())
                return fileData.Content.ContentType;

            return fileData.ContentType;

        } 

        private byte[] ConvertToByteArray(IFormFile fileBase)
        {
            using MemoryStream target = new MemoryStream();
            fileBase.OpenReadStream().CopyTo(target);
            return target.ToArray();
        }
    }
}
