﻿using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Service;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Fasta.Providers
{
    /// <summary>
    /// Провайдер для изменения демо периода Fasta сервиса
    /// </summary>
    internal class ChangeFastaDemoPeriodProvider(IFastaServiceProvider fastaServiceProvider, IUnitOfWork dbLayer)
        : IChangeFastaDemoPeriodProvider
    {
        /// <summary>
        /// Изменить демо период для Fasta сервиса
        /// </summary>
        /// <param name="manageFastaDemoPeriodDto">Модель управления
        /// демо периодом сервиса Fasta</param>
        public void Change(ManageFastaDemoPeriodDto manageFastaDemoPeriodDto)
        {
            var resConf = fastaServiceProvider.GetResourcesConfiguration(manageFastaDemoPeriodDto.AccountId);
            resConf.ExpireDate = manageFastaDemoPeriodDto.DemoExpiredDate;
            dbLayer.ResourceConfigurationRepository.Update(resConf);
            dbLayer.Save();
        }
    }
}
