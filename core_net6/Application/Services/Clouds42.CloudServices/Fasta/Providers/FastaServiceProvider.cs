﻿using Clouds42.DataContracts.Service;
using Clouds42.CloudServices.Fasta.Helpers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Fasta.Providers
{
    /// <summary>
    /// Провайдер для работы с сервисом Fasta
    /// </summary>
    internal class FastaServiceProvider(
        IUnitOfWork dbLayer,
        IAccountDataProvider accountDataProvider,
        FastaServiceHelper fastaServiceHelper)
        : IFastaServiceProvider
    {
        /// <summary>
        /// Получить модель управления
        /// демо периодом сервиса Fasta
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель управления сервисом Fasta</returns>
        public ManageFastaDemoPeriodDto GetManageFastaDemoPeriodDto(Guid accountId)
        {
            var resConf = GetResourcesConfiguration(accountId);
            var account = accountDataProvider.GetAccountOrThrowException(accountId);

            return new ManageFastaDemoPeriodDto
            {
                AccountId = accountId,
                DemoExpiredDate = resConf.ExpireDateValue,
                MaxDemoExpiredDate = fastaServiceHelper.GetMaxDemoExpiredDate(account)
            };
        }

        /// <summary>
        /// Получить конфигурацию ресурса для сервиса Fasta
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Конфигурация ресурса</returns>
        public ResourcesConfiguration GetResourcesConfiguration(Guid accountId) =>
            dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingService.SystemService == Clouds42Service.Esdl && rc.AccountId == accountId) ??
            throw new NotFoundException($"Не удалось получить конфигурацию ресурса для сервиса {Clouds42Service.Esdl}");
    }
}
