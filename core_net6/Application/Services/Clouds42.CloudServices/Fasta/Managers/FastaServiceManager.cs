﻿using Clouds42.DataContracts.Service;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.CloudServices.Fasta.Validators;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Fasta.Managers
{
    /// <summary>
    /// Менеджер для работы с сервисом Fasta
    /// </summary>
    public class FastaServiceManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IFastaServiceProvider fastaServiceProvider,
        IHandlerException handlerException,
        IChangeFastaDemoPeriodProvider changeFastaDemoPeriodProvider,
        ManageFastaDemoPeriodDtoValidator manageFastaDemoPeriodDtoValidator,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить модель управления
        /// демо периодом сервиса Fasta
        /// </summary>
        /// <returns>Модель управления сервисом Fasta</returns>
        public ManagerResult<ManageFastaDemoPeriodDto> GetManageFastaDemoPeriodDto()
        {
            AccessProvider.HasAccess(ObjectAction.ControlPanel_Fasta_ChangeDemoPeriod,
                () => AccessProvider.ContextAccountId);

            try
            {
                var result = fastaServiceProvider.GetManageFastaDemoPeriodDto(AccessProvider.ContextAccountId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения модели управления демо периодом сервиса Fasta]");
                return PreconditionFailed<ManageFastaDemoPeriodDto>(ex.Message);
            }
        }

        /// <summary>
        /// Изменить демо период для Fasta сервиса
        /// </summary>
        /// <param name="model">Модель управления
        /// демо периодом сервиса Fasta</param>
        public ManagerResult ChangeFastaDemoPeriod(ManageFastaDemoPeriodDto model)
        {
            AccessProvider.HasAccess(ObjectAction.ControlPanel_Fasta_ChangeDemoPeriod,
                () => AccessProvider.ContextAccountId);

            try
            {
                if (!manageFastaDemoPeriodDtoValidator.Validate(model, out string errorMessage))
                    return PreconditionFailed(errorMessage);

                changeFastaDemoPeriodProvider.Change(model);

                logger.Trace($@"Успешное изменение даты окончания демо периода Fasta для аккаунта {model.AccountId}. 
                                    Новая дата завершения демо периода {model.DemoExpiredDate:dd.MM.yyyy HH:mm:ss}");

                LogEvent(() => model.AccountId, LogActions.EditFastaDemoExpireDate,
                    $"Дата окончания демо периода для сервиса \"Fasta\" {model.DemoExpiredDate:dd.MM.yyyy HH:mm:ss}");

                return Ok();
            }
            catch (Exception ex)
            {
                var message =
                    "Ошибка изменения даты окончания демо периода для сервиса \"Fasta\".";

                LogEvent(() => model.AccountId, LogActions.EditFastaDemoExpireDate, $"{message} Причина: {ex.Message}");
                _handlerException.Handle(ex, $"[{message}] Описание ошибки: {ex.GetFullInfo()}");

                return PreconditionFailed(ex.Message);
            }
        }
    }
}
