﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;

namespace Clouds42.CloudServices.Fasta.Helpers
{
    /// <summary>
    /// Хелпер для работы с Fasta сервисом
    /// </summary>
    public class FastaServiceHelper(
        IAccessProvider accessProvider,
        IAccountDataProvider accountDataProvider)
    {
        /// <summary>
        /// Получить максимально возможный срок действия демо периода
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Максимально возможный срок действия демо периода</returns>
        public DateTime? GetMaxDemoExpiredDate(Guid accountId) =>
            GetMaxDemoExpiredDate(accountDataProvider.GetAccountOrThrowException(accountId));


        /// <summary>
        /// Получить максимально возможный срок действия демо периода
        /// </summary>
        /// <param name="account">Аккаунт облака</param>
        /// <returns>Максимально возможный срок действия демо периода</returns>
        public DateTime? GetMaxDemoExpiredDate(Account account)
        {
            var user = accessProvider.GetUser();

            if (user?.Groups != null && user.Groups.Contains(AccountUserGroup.CloudAdmin))
                return DateTime.MaxValue;

            return account.RegistrationDate?.AddMonths(1);
        }
    }
}
