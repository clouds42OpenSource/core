﻿using Clouds42.DataContracts.Service;
using Clouds42.CloudServices.Fasta.Helpers;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;

namespace Clouds42.CloudServices.Fasta.Validators
{
    /// <summary>
    /// Валидатор модели управления демо периодом сервиса Fasta
    /// </summary>
    public class ManageFastaDemoPeriodDtoValidator(
        FastaServiceHelper fastaServiceHelper,
        ICheckAccountDataProvider checkAccountProvider)
    {
        /// <summary>
        /// Выполнить валидацию модели
        /// управления демо периодом сервиса Fasta
        /// </summary>
        /// <param name="model">Модель управления демо
        /// периодом сервиса Fasta</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        public bool Validate(ManageFastaDemoPeriodDto model, out string errorMessage)
        {
            errorMessage = null;
            var isDemoAccount = checkAccountProvider.CheckAccountIsDemo(model.AccountId);

            if (!isDemoAccount)
            {
                errorMessage = "Нельзя изменить демо период, аккаунт не является демо.";
                return false;
            }

            var maxDemoExpiredDate = fastaServiceHelper.GetMaxDemoExpiredDate(model.AccountId);
            var minDemoExpiredDate = DateTime.Now;

            if (maxDemoExpiredDate.HasValue && model.DemoExpiredDate.Date > maxDemoExpiredDate.Value.Date)
            {
                errorMessage = $"Нельзя изменить демо период, максимально допустимая дата {maxDemoExpiredDate:dd.MM.yyyy}";
                return false;
            }

            if (model.DemoExpiredDate.Date < minDemoExpiredDate.Date)
            {
                errorMessage = $"Нельзя изменить демо период, минимально допустимая дата {minDemoExpiredDate:dd.MM.yyyy}";
                return false;
            }

            return true;
        }
    }
}
