﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.ServiceExtensions.Interfaces;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.Contracts.BillingServiceTypeAvailability.Interfaces;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceManagement;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Management.Interfaces;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.CloudServices.Contracts.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Helpers
{
    /// <summary>
    /// Хэлпер для управления Арендой 1С
    /// </summary>
    public class Rent1CManageHelper(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IRedirectPublishAccountDatabase redirectPublishAccountDatabase,
        ICheckAccountDataProvider checkAccountProvider,
        IAccountDataProvider accountDataProvider,
        IResourcesService resourcesService,
        Rent1CRateHelper rent1CRateHelper,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        IDeleteUnusedResourcesProcessor deleteUnusedResourcesProcessor,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IBillingServiceTypeAvailabilityManager billingServiceTypeAvailabilityManager,
        IServiceExtensionDatabaseActivationStatusProvider serviceExtensionDatabaseActivationStatusProvider,
        IResourceDataProvider resourceDataProvider,
        IRecalculateResourcesConfigurationCostProvider recalculateResourcesConfigurationCostProvider,
        IManageMyDatabasesServiceProvider manageMyDatabasesServiceProvider,
        IUpdateAdditionalResourcesForAccountProvider updateAdditionalResourcesForAccountProvider,
        IAdditionalResourcesDataProvider additionalResourcesDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        IRateProvider rateProvider,
        IProvidedServiceHelper providedServiceHelper,
        IHandlerException handlerException)
        : IRent1CManageHelper
    {
        /// <summary>
        /// Изменить параметры Аренды 1С для аккаунта
        /// </summary>
        /// <param name="model">Модель изменения Аренды 1С</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Сообщение об изменениях Аренды 1С для аккаунта</returns>
        public string ManageRent1C(Rent1CServiceManagerDto model, Guid accountId)
        {
            var accountInfo = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);
            var resConfig = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var changes = new StringBuilder();

                ChangeRent1CExpireDate(model, accountId, resConfig, changes);

                if (accountConfigurationDataProvider.IsVipAccount(accountId))
                {
                    UpdateVipAccountAdditionalResources(model, accountId, changes);
                }

                UpdateAccountRates(model, accountInfo, resConfig, changes);

                RegisterRent1CDemoResources(accountId, resConfig, changes);

                manageMyDatabasesServiceProvider.Manage(new ManageMyDatabasesServiceDto
                {
                    AccountId = accountId,
                    LimitOnFreeCreationDb = model.LimitOnFreeCreationDb,
                    CostOfCreatingDbOverFreeLimit = model.CostOfCreatingDbOverFreeLimit,
                    ServerDatabasePlacementCost = model.ServerDatabasePlacementCost
                }, changes);

                logger.Info(
                    $"Конфигурация сервисa \"Аренда 1С\" компании {accountInfo.AccountCaption} ({accountInfo.IndexNumber}) изменена пользователем {accessProvider.Name}. {changes}");
                dbScope.Commit();

                return changes.ToString();
            }
            catch (Exception ex)
            {
                    
                handlerException.Handle(ex, $"[Ошибка изменения параметров аренд 1с] для аккаунта {accountId}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        ///    1. Блокировка Аренды 1С.
        ///    2. Удаление пользователей из групп AD.
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void Lock(Guid accountId)
        {
            try
            {
                logger.Info($"[{accountId}] :: Процесс блокировки начат");

                var account = dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == accountId);
                if (account == null)
                {
                    logger.Info($"[{accountId}] :: Аккаунт не найден");
                    return;
                }

                logger.Info($"[{accountId}] :: Процесс редиректа для опубликованных баз начат");
                redirectPublishAccountDatabase.SetRedirectNeedMoneyToAccount(accountId);
                logger.Info($"[{accountId}] :: Процесс редиректа для опубликованных баз закончен");

                LockAccountUsers(accountId);
                serviceExtensionDatabaseActivationStatusProvider.SetActivationStatus(accountId);
            }
            catch (Exception e)
            {
                logger.Info($"[{accountId}] :: Exception: {e.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        ///    1. Разблокировка Аренды 1С.
        ///    2. Восстановление пользователей в группы AD.
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void UnLock(Guid accountId)
        {
            try
            {
                logger.Info($"[{accountId}] :: Процесс разблокировки начат");

                var account = dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == accountId);
                if (account == null)
                {
                    logger.Info($"[{accountId}] :: Аккаунт не найден");
                    return;
                }

                logger.Info($"[{accountId}] :: Процесс редиректа для опубликованных баз начат");
                redirectPublishAccountDatabase.DeleteRedirectionNeedMoneyForAccount(accountId);
                logger.Info($"[{accountId}] :: Процесс редиректа для опубликованных баз закончен");

                UnlockAccountUsers(accountId);
                serviceExtensionDatabaseActivationStatusProvider.SetActivationStatus(accountId, true);
            }
            catch (Exception e)
            {
                logger.Info($"[{accountId}] :: Exception: {e.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Создать демо ресурсы для Аренды 1С
        /// </summary>
        /// <param name="countWebResources">Количество web ресурсов</param>
        /// <param name="countStandartResources">Количество стандарт ресурсов</param>
        /// <param name="resourcesConfiguration">Ресурс конфигурация Аренды 1С</param>
        public void CreateDemoResources(int countWebResources, int countStandartResources,
            IResourcesConfiguration resourcesConfiguration)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var billingService =
                    new BillingServiceDataProvider(dbLayer).GetSystemService(Clouds42Service.MyEnterprise);

                var rent1CRateInfo = rent1CRateHelper.GetRent1CRateInfo(resourcesConfiguration.AccountId);

                for (var i = 0; i < countWebResources; i++)
                {
                    InsertWebResourceRecord(resourcesConfiguration, rent1CRateInfo);
                }

                for (var i = 0; i < countStandartResources; i++)
                {
                    InsertStandartResourceRecord(resourcesConfiguration, rent1CRateInfo);
                }

                recalculateResourcesConfigurationCostProvider.Recalculate(resourcesConfiguration.AccountId,
                    billingService);

                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка создания демо ресурсов для аренды 1С]");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Вставить записи ресурсов WEB
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация Аренды 1С</param>
        /// <param name="rent1CRateInfo">Информация о тарифах Аренды 1С</param>
        private void InsertWebResourceRecord(IResourcesConfiguration resourcesConfiguration,
            Rent1CRateInfoDto rent1CRateInfo)
        {
            resourcesService.CreateResource(
                new Resource
                {
                    AccountId = resourcesConfiguration.AccountId,
                    Cost = rent1CRateInfo.Rent1CWebRate.Cost,
                    Subject = null,
                    BillingServiceTypeId = rent1CRateInfo.Rent1CWebServiceType.Id,
                    Id = Guid.NewGuid(),
                    DemoExpirePeriod = null,
                    IsFree = false,
                    Tag = null,
                });
        }

        /// <summary>
        /// Вставить записи ресурсов Стандарт
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация Аренды 1С</param>
        /// <param name="rent1CRateInfo">Информация о тарифах Аренды 1С</param>
        private void InsertStandartResourceRecord(IResourcesConfiguration resourcesConfiguration,
            Rent1CRateInfoDto rent1CRateInfo)
        {
            resourcesService.CreateResource(
                new Resource
                {
                    AccountId = resourcesConfiguration.AccountId,
                    Cost = rent1CRateInfo.Rent1CStandartRate.Cost,
                    Subject = null,
                    BillingServiceTypeId = rent1CRateInfo.Rent1CStandartServiceType.Id,
                    Id = Guid.NewGuid(),
                    DemoExpirePeriod = null,
                    IsFree = false,
                    Tag = null,
                });
        }

        /// <summary>
        /// Доначислить свободные ресурсы для Аренды 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="rent1CResConfig">Ресурс конфигурация Аренды 1С</param>
        /// <param name="changeMessageBuilder">Конструктор сообщений об изменениях</param> 
        private void RegisterRent1CDemoResources(Guid accountId, ResourcesConfiguration rent1CResConfig,
            StringBuilder changeMessageBuilder)
        {
            if (!checkAccountProvider.CheckAccountIsDemo(accountId))
                return;

            var account = accountDataProvider.GetAccountOrThrowException(accountId);

            var freeResourceCount = CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod();

            var accountRent1CResources = dbLayer.ResourceRepository.Where(w =>
                w.BillingServiceType.ServiceId == rent1CResConfig.BillingServiceId &&
                (w.AccountId == account.Id || w.AccountSponsorId == account.Id)).ToList();

            var usedWebLicenses = accountRent1CResources
                .Count(w => w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb);

            var usedStandartLicenses = accountRent1CResources
                .Count(w => w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);

            var webResourceCount = Math.Max(0, freeResourceCount - usedWebLicenses);
            var standartResourceCount = Math.Max(0, freeResourceCount - usedStandartLicenses);

            if (webResourceCount != 0)
                changeMessageBuilder.Append($"Начислено {webResourceCount} WEB ресурсов.");

            if (standartResourceCount != 0)
                changeMessageBuilder.Append($"Начислено {standartResourceCount} Стандарт ресурсов.");

            CreateDemoResources(webResourceCount, standartResourceCount, rent1CResConfig);
        }

        /// <summary>
        /// Изменить дату завершения Аренды 1С
        /// </summary>
        /// <param name="model">Модель изменений Аренды 1С</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resConfig">Ресурс конфигурация Аренды 1С</param>
        /// <param name="changeMessageBuilder">Конструктор сообщений об изменениях</param>
        private void ChangeRent1CExpireDate(Rent1CServiceManagerDto model, Guid accountId,
            ResourcesConfiguration resConfig, StringBuilder changeMessageBuilder)
        {
            if (model.ExpireDate != resConfig.ExpireDate)
                changeMessageBuilder.Append(
                    $"Старая дата завершения услуг {resConfig.ExpireDate:dd.MM.yyyy HH:mm:ss} , установленная {model.ExpireDate:dd.MM.yyyy HH:mm:ss}. ");

            providedServiceHelper.ChangeRent1CExpireDate(resConfig, model.ExpireDate);

            resConfig.ExpireDate = model.ExpireDate;
            var needFrozen = model.ExpireDate <= DateTime.Now;

            UpdateServiceTypesAvailabilityDateForAccount(resConfig, accountId, model.ExpireDate);

            if (needFrozen && !resConfig.FrozenValue)
            {
                Lock(accountId);
                deleteUnusedResourcesProcessor.Process(resConfig);
            }
            else if (!needFrozen && resConfig.FrozenValue)
            {
                UnLock(accountId);
            }

            resConfig.Frozen = needFrozen;
            dbLayer.ResourceConfigurationRepository.Update(resConfig);

            dbLayer.Save();
        }

        /// <summary>
        /// Обновить время активности удаленных услуг для аккаунта
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурсов Аренды 1С</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="availabilityDate">Дата, до которой услуга будет доступно</param>
        private void UpdateServiceTypesAvailabilityDateForAccount(ResourcesConfiguration resourcesConfiguration,
            Guid accountId, DateTime availabilityDate)
        {
            var resConfModel =
                resourceConfigurationDataProvider.GetConfigWithDependenciesIncludingDemo(resourcesConfiguration);

            var serviceTypes = GetDeletedServiceTypesForAccount(
                resConfModel.DependedResourceConfigurations.Select(rc => rc.BillingService.Id).ToList(), accountId);

            foreach (var serviceType in serviceTypes)
                billingServiceTypeAvailabilityManager.UpdateAvailabilityDateTimeForAccount(serviceType.Id, accountId,
                    availabilityDate);
        }

        /// <summary>
        /// Получить список удаленных услуг для аккаунта
        /// </summary>
        /// <param name="services">Список Id сервисов</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список удаленных услуг</returns>
        public IEnumerable<BillingServiceType> GetDeletedServiceTypesForAccount(List<Guid> services, Guid accountId) =>
            (from serviceId in services
             join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on serviceId equals serviceType
                 .ServiceId
             join deletedServiceType in dbLayer.GetGenericRepository<BillingServiceTypeActivityForAccount>()
                 .WhereLazy(bst => bst.AccountId == accountId) on serviceType.Id equals deletedServiceType
                 .BillingServiceTypeId
             select serviceType).ToList();

        /// <summary>
        /// Обновить дополнительные ресурсы у вип аккаунта
        /// </summary>
        /// <param name="model">Модель изменений Аренды 1С</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changeMessageBuilder">Конструктор сообщений об изменениях</param>
        private void UpdateVipAccountAdditionalResources(Rent1CServiceManagerDto model, Guid accountId,
            StringBuilder changeMessageBuilder)
        {
            var additionalResourcesData = additionalResourcesDataProvider.GetAdditionalResourcesData(accountId);

            if (model.AdditionalResourceCost != additionalResourcesData.AdditionalResourceCost)
                changeMessageBuilder.Append(
                    "Изменена стоимость ресурса \"Дополнительные услуги\" " +
                    $"с {additionalResourcesData.AdditionalResourceCost:0.00} " +
                    $"на {model.AdditionalResourceCost:0.00} {accountConfigurationDataProvider.GetAccountLocale(accountId).Currency}.");

            if (model.AdditionalResourceName != additionalResourcesData.AdditionalResourceName)
                changeMessageBuilder.Append(
                    "Изменены дополнительные ресурсы " +
                    $"с \"{additionalResourcesData.AdditionalResourceName ?? "---"}\" " +
                    $"на \"{model.AdditionalResourceName ?? "---"}\".");

            updateAdditionalResourcesForAccountProvider.Update(new UpdateAdditionalResourcesForAccountDto
            {
                AccountId = accountId,
                AdditionalResourceName = model.AdditionalResourceName,
                AdditionalResourceCost = model.AdditionalResourceCost
            });
        }

        /// <summary>
        /// Обновить стоимость лицензий Аренды 1С для аккаунта
        /// </summary>
        /// <param name="model">Модель изменений Аренды 1С</param>
        /// <param name="accountInfo">Аккаунт</param>
        /// <param name="resConfig">Ресурс конфигурация Аренды 1С</param>
        /// <param name="changeMessageBuilder">Конструктор сообщений об изменениях</param>
        private void UpdateAccountRates(Rent1CServiceManagerDto model, Account accountInfo,
            ResourcesConfiguration resConfig, StringBuilder changeMessageBuilder)
        {
            var ratesForChange = GetModelsForUpdateLicenseCost(model, accountInfo, changeMessageBuilder);
            rateProvider.UpdateAccountRates(accountInfo.BillingAccount.Id, resConfig.BillingService, ratesForChange.ToArray());
            dbLayer.Save();
        }

        /// <summary>
        /// Получить модели для обновления стоимости лицензий
        /// </summary>
        /// <param name="model">Модель изменений Аренды 1С</param>
        /// <param name="accountInfo">Аккаунт</param>
        /// <param name="changeMessageBuilder">Конструктор сообщений об изменениях</param>
        /// <returns>Модели для обновления стоимости лицензий</returns>
        private List<RateChangeItem> GetModelsForUpdateLicenseCost(Rent1CServiceManagerDto model,
            Account accountInfo, StringBuilder changeMessageBuilder)
        {
            var ratesForChange = new List<RateChangeItem>();

            var rent1CRateInfo = rent1CRateHelper.GetRent1CRateInfo(accountInfo.Id);

            if (model.CostOfRdpLicense.HasValue &&
                model.CostOfRdpLicense.Value != rent1CRateInfo.Rent1CStandartRate.Cost)
            {
                changeMessageBuilder.Append(
                    $"Изменена стоимость ресурса \"RDP\" с {rent1CRateInfo.Rent1CStandartRate.Cost:0.00} на {model.CostOfRdpLicense.Value:0.00} руб.");
                ratesForChange.Add(new RateChangeItem
                {
                    BillingServiceTypeId = rent1CRateInfo.Rent1CStandartServiceType.Id,
                    Cost = model.CostOfRdpLicense.Value
                });
            }

            if (model.CostOfWebLicense.HasValue && model.CostOfWebLicense.Value != rent1CRateInfo.Rent1CWebRate.Cost)
            {
                changeMessageBuilder.Append(
                    $"Изменена стоимость ресурса \"WEB\" с {rent1CRateInfo.Rent1CWebRate.Cost:0.00} на {model.CostOfWebLicense.Value:0.00} руб.");
                ratesForChange.Add(new RateChangeItem
                {
                    BillingServiceTypeId = rent1CRateInfo.Rent1CWebServiceType.Id,
                    Cost = model.CostOfWebLicense.Value
                });
            }

            return ratesForChange;
        }

        /// <summary>
        /// Разблокировать пользователей в AD
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void UnlockAccountUsers(Guid accountId)
        {
            var resourcesForUnlock = GetAccountResources(accountId);

            if (!resourcesForUnlock.Any())
            {
                logger.Info($"[{accountId}] :: Ресурсов для разблокировки нет");
                return;
            }

            logger.Info($"[{accountId}] :: Процесс разблокировки в Active Directory начат");

            var accountUsersForUnlock = resourcesForUnlock.Where(x => x.Subject.HasValue).Select(s => s.Subject.Value).ToList();

            var accountUsers = dbLayer.AccountUsersRepository.WhereLazy(a => accountUsersForUnlock.Contains(a.Id))
                .ToList();

            var editUserGroupsAdListModel = new EditUserGroupsAdListModel
            {
                AccountId = accountId
            };


            var accountUsersAcDbAccesses = dbLayer.AcDbAccessesRepository
                .AsQueryableNoTracking()
                .Where(x => accountUsersForUnlock.Contains(x.AccountUserID.Value))
                .Select(x => new AccountIndexNumberDbNumberDto
                {
                    AccountIndexNumber = x.Account.IndexNumber, DbNumber = x.Database.DbNumber, Login = x.AccountUser.Login
                })
                .ToList();

            foreach (var accountUser in accountUsers)
            {
                var resourcesGroup = resourcesForUnlock.GroupBy(g => g.Subject)
                    .FirstOrDefault(k => k.Key == accountUser.Id) ?? throw new NotFoundException($"Отсутсвуют группа ресурсов для юзера {accountUser.Id}.");

                var resources = resourcesGroup.ToList();

                editUserGroupsAdListModel.EditUserGroupsModel.Add(CreateEditUserGroupsAdModelHelper.CreateEditUserGroupsAdModel(resources, accountUser,
                    EditUserGroupsAdModelDto.OperationEnum.Include));

                LockUnlockAcDbAccesses(accountUser.Id, false);
            }

            editUserGroupsAdListModel.EditUserGroupsModel.AddRange(accountUsersAcDbAccesses.ConvertToEditUserGroupAdModels(EditUserGroupsAdModelDto.OperationEnum.Include));

            activeDirectoryTaskProcessor.RunTask(provider => provider.EditUserGroupsAdList(editUserGroupsAdListModel));

            logger.Info($"[{accountId}] :: Процесс разблокировки в Active Directory закончен");
        }

        /// <summary>
        /// Заблокировать пользователей
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void LockAccountUsers(Guid accountId)
        {
            var resourcesForLock = GetAccountResources(accountId);

            if (!resourcesForLock.Any())
            {
                logger.Info($"[{accountId}] :: Ресурсов для блокировки нет");
                return;
            }

            var accountUsersForLock = resourcesForLock.Select(s => s.Subject).ToList();
            logger.Info($"[{accountId}] :: Блокируем Аренду для юзеров {string.Join(", ", accountUsersForLock)}");
            var accountUsers = dbLayer.AccountUsersRepository.WhereLazy(a => accountUsersForLock.Contains(a.Id))
                .ToList();

            logger.Info($"[{accountId}] :: Процесс блокировки в Active Directory начат");

            var editUserGroupsAdListModel = new EditUserGroupsAdListModel
            {
                AccountId = accountId
            };

            var accountUsersAcDbAccesses = dbLayer.AcDbAccessesRepository
                .AsQueryableNoTracking()
                .Where(x => accountUsersForLock.Contains(x.AccountUserID))
                .Select(x => new AccountIndexNumberDbNumberDto
                {
                    AccountIndexNumber = x.Account.IndexNumber,
                    DbNumber = x.Database.DbNumber,
                    Login = x.AccountUser.Login
                })
                .ToList();

            foreach (var accountUser in accountUsers)
            {
                var resourcesGroup = resourcesForLock.GroupBy(g => g.Subject)
                    .FirstOrDefault(k => k.Key == accountUser.Id) ?? throw new NotFoundException($"Отсутсвуют группа ресурсов для юзера {accountUser.Id}.");
                editUserGroupsAdListModel.EditUserGroupsModel.Add(
                    CreateEditUserGroupsAdModelHelper.CreateEditUserGroupsAdModel(resourcesGroup.ToList(), accountUser,
                    EditUserGroupsAdModelDto.OperationEnum.Exclude));
                LockUnlockAcDbAccesses(accountUser.Id, true);
            }

            editUserGroupsAdListModel.EditUserGroupsModel.AddRange(accountUsersAcDbAccesses.ConvertToEditUserGroupAdModels(EditUserGroupsAdModelDto.OperationEnum.Exclude));

            activeDirectoryTaskProcessor.RunTask(provider => provider.EditUserGroupsAdList(editUserGroupsAdListModel));

            logger.Info($"[{accountId}] :: Процесс блокировки в Active Directory закончен");
        }

        private void LockUnlockAcDbAccesses(Guid accountUserId, bool isLock)
        {
            try
            {
                var accessList = dbLayer.AcDbAccessesRepository
                    .Where(access => access.AccountUserID == accountUserId).ToList();

                accessList.ForEach(access =>
                {
                    access.IsLock = isLock;
                    dbLayer.AcDbAccessesRepository.Update(access);
                });

                dbLayer.Save();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка смены состояния блокировки пользователя] {accountUserId} в БД.");
                throw;
            }
        }
        /// <summary>
        /// Получить список ресурсов для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список ресурсов для аккаунта</returns>
        private List<Resource> GetAccountResources(Guid accountId)
            => resourceDataProvider.GetResources(
                Clouds42Service.MyEnterprise,
                r => r.Subject != null && (r.AccountId == accountId && r.AccountSponsorId == null ||
                                           r.AccountSponsorId == accountId),
                ResourceType.MyEntUser, ResourceType.MyEntUserWeb);
    }
}
