﻿using Clouds42.Billing.Contracts.Billing.ModelProcessors;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.BLL.Common.Extensions;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Configurations;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CloudServices.Helpers
{
    /// <summary>
    /// Класс для получения статуса сервиса заблокирован он или нет
    /// </summary>
    public class ServiceStatusHelper : IServiceStatusHelper
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IUnitOfWork _dbLayer;
        private readonly IConfiguration _configuration;
        private readonly IResourcesService _resourcesService;
        private readonly ILogger42 _logger;
        private readonly Lazy<int> _promisePaymentDateLazy;
        private readonly ICloudServiceAdapter _cloudServiceAdapter;
        private readonly ICloudLocalizer _localizer;

        public ServiceStatusHelper(IServiceProvider serviceProvider, ILogger42 logger, IConfiguration configuration, ICloudLocalizer localizer)
        {
            _serviceProvider = serviceProvider;
            _dbLayer = _serviceProvider.GetRequiredService<IUnitOfWork>();
            _resourcesService = _serviceProvider.GetRequiredService<IResourcesService>();
            _cloudServiceAdapter = _serviceProvider.GetRequiredService<ICloudServiceAdapter>();
            _logger = logger;
            _promisePaymentDateLazy =
                new Lazy<int>(() => ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays"));
            _configuration = configuration;
            _localizer = localizer;
        }

        /// <summary>
        /// Получить информацию о причине блокировки сервиса если он заблокирован
        /// </summary>
        public ServiceStatusModelDto CreateServiceStatusModel(Guid accountId, ResourcesConfiguration resConfig)
        {
            var result = new ServiceStatusModelDto();

            var billingAccount = _dbLayer.BillingAccountRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(x => x.Id == accountId);

            var locale = _dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(_configuration["DefaultLocale"]!));

            var mainServiceResConfig = _cloudServiceAdapter.GetMainConfigurationOrThrowException(resConfig);

            result.ServiceIsLocked = mainServiceResConfig.FrozenValue || resConfig.FrozenValue;
            result.PromisedPaymentIsActive = billingAccount.HasPromisedPayment();

            if (result.PromisedPaymentIsActive)
            {
                result.PromisedPaymentSum = $"{billingAccount.PromisePaymentSum!.Value:0.00} {locale.Currency}.";
                result.PromisedPaymentExpireDate =
                    billingAccount.PromisePaymentDate!.Value.AddDays(_promisePaymentDateLazy.Value);
            }

            if (billingAccount.IsPromisedPaymentExpired())
            {
                result.ServiceIsLocked = true;
                result.ServiceLockReasonType = ServiceLockReasonType.OverduePromisedPayment;
                result.ServiceLockReason =
                    $"Сервис '{resConfig.GetLocalizedServiceName(_localizer)}' заблокирован по причине наличия обещанного платежа на сумму {result.PromisedPaymentSum}";
                _logger.Info(
                    $"У аккаунта {accountId} сервис '{resConfig.BillingService.Name}' заблокирован по причине ОП на сумму {result.PromisedPaymentSum}");
                return result;
            }

            if (!result.ServiceIsLocked)
            {
                return result;
            }

            result.ServiceLockReasonType = ServiceLockReasonType.ServiceNotPaid;
            result.ServiceLockReason = GetServiceLockReason(resConfig, mainServiceResConfig);
            _logger.Info($"У аккаунта {accountId} сервис {resConfig.BillingService.Name} заблокирован.");

            return result;
        }

        /// <summary>
        /// Получить информацию о причине блокировки двух сервисов MyEnterprise и MyDisk если они заблокированы
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        public ServiceStatusModelDto CreateModelForRentAndMyDisk(Guid accountId)
        {
            ServiceStatusModelDto result = new ServiceStatusModelDto();

            //получаем конфигурацию ресурсов для сервиса Аренда 1С
            var resConfig = _resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);

            //если конфигурации ресурсов для сервиса Аренда 1С не найдена сервис считается разблокированым
            if (resConfig == null)
            {
                _logger.Info($"У аккаунта {accountId} сервис Аренда 1С не найдена.");
                return result;
            }

            //получаем информацио о состоянии сервиса Аренда 1С
            result = CreateServiceStatusModel(accountId, resConfig);

            if (result.ServiceIsLocked)
                return result;

            //получаем конфигурацию ресурсов для сервиса Мой Диск
            resConfig = _resourcesService.GetResourceConfig(accountId, Clouds42Service.MyDisk);

            //получаем информацио о состоянии сервиса Аренда 1С MyDisk
            result = CreateServiceStatusModel(accountId, resConfig);

            var myDiskInfo = _serviceProvider.GetRequiredService<IServiceMyDiskDomainModelProcessor>().GetModel(accountId);

            if (myDiskInfo.MyDiskVolumeInfo.UsedSizeOnDisk <= myDiskInfo.MyDiskVolumeInfo.AvailableSize)
            {
                return result;
            }

            var diskInfo =
                $"Доступно: {myDiskInfo.MyDiskVolumeInfo.AvailableSize.MegaByteToClassicFormat(2)}. Используется: {myDiskInfo.MyDiskVolumeInfo.UsedSizeOnDisk.MegaByteToClassicFormat(2)}.";

            _logger.Info($"У аккаунта {accountId} нет свободного дискового пространства. {diskInfo}");

            return new ServiceStatusModelDto
            {
                ServiceLockReason = $"Нет свободного дискового пространства. {diskInfo}",
                ServiceLockReasonType = ServiceLockReasonType.NoDiskSpace,
                ServiceIsLocked = true
            };
        }

        /// <summary>
        /// Получить текст причины блокировки сервиса.
        /// </summary>
        /// <param name="resConfig">Ресурс конфигурация сервиса.</param>
        /// <param name="mainServiceResConfig">Ресурс конфигурация основного сервиса.</param>
        /// <returns>Текст причины блокировки.</returns>
        private string GetServiceLockReason(ResourcesConfiguration resConfig,
            ResourcesConfiguration mainServiceResConfig)
        {
            var rent1CName = resConfig.GetLocalizedServiceName(_localizer);

            if (mainServiceResConfig.BillingServiceId != resConfig.BillingServiceId && mainServiceResConfig.FrozenValue)
                return $"Работа с сервисом “{rent1CName}” возможна " +
                       $"при активном сервисе “{_localizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, resConfig.AccountId)}”.";

            return resConfig.IsDemoPeriod ? $"У сервиса '{rent1CName}' завершился демо период" : $"Сервис '{rent1CName}' заблокирован.";
        }
    }
}
