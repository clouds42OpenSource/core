﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.CloudServices.Helpers
{
    /// <summary>
    /// Хелпер для работы с ревисом облака
    /// </summary>
    public class Cloud42ServiceHelper(
        IUnitOfWork dbLayer,
        IMyDiskPropertiesByAccountDataProvider myDiskPropertiesByAccountDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IResourcesService resourcesService)
        : ICloud42ServiceHelper
    {
        /// <summary>Список пользователей аккаунта, для которых включен сервис</summary>        
        public List<string> GetEnabledAccountUserLogins(Account account, Clouds42Service service,
            ResourceType resourceType)
        {
            var users = new List<AccountUser>(account.AccountUsers);
            var userResList = resourcesService.GetResources(account.Id, service, resourceType).ToList();
            return users.Where(u => userResList.Any(ur => ur.Subject == u.Id)).Select(u => u.Login).ToList();
        }

        /// <summary>
        /// Получить доступный объем дискового пространства.
        /// </summary>        
        /// <returns>объем в Мб </returns>
        public long GetAvailableSpaceOnDisk(Guid accountId)
        {
            var payedSpaceOnDisk = GetPayedSpaceOnDisk(accountId);
            return payedSpaceOnDisk > 0 ? payedSpaceOnDisk : Clouds42SystemConstants.DefaultAvailableMemorySize;
        }

        /// <summary>
        /// Получить объем купленного дискового пространства.
        /// </summary>        
        /// <returns>объем в Мб </returns>
        public long GetPayedSpaceOnDisk(Guid accountId)
        {
            var companyInfo = dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == accountId);

            if (companyInfo == null)
                return 0;

            var resource = dbLayer.DiskResourceRepository.FirstOrDefault(r => r.Resource.AccountId == accountId
                                                                               && r.Resource.Subject == accountId);

            if (resource == null)
                return 0;

            return resource.VolumeGb * 1024;
        }

        /// <summary>
        /// Информация о состояние дискового пространства.
        /// </summary>        
        public MyDiskInfoModelDto GetMyDiskInfo(Guid accountId, long availableSizeOnDisk = 0)
        {
            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);

            if (account == null)
                return null;

            var dataBases = dbLayer.DatabasesRepository.Where(d =>
                d.AccountId == accountId && d.State != DatabaseState.DeletedToTomb.ToString()
                                         && d.State != DatabaseState.DelitingToTomb.ToString()
                                         && d.State != DatabaseState.DetachingToTomb.ToString()
                                         && d.State != DatabaseState.DeletedFromCloud.ToString()).ToList();

            long payedSizeOnDisk = 0, serverDataBaseSize = 0, fileDataBaseSize = 0;

            if (availableSizeOnDisk == 0)
                availableSizeOnDisk += GetAvailableSpaceOnDisk(accountId);
            payedSizeOnDisk += GetPayedSpaceOnDisk(accountId);

            var serverDataBases = dataBases.Where(d => d.IsFile == null || d.IsFile == false).ToList();
            foreach (var serverDb in serverDataBases)
            {
                serverDataBaseSize += serverDb.SizeInMB;
            }

            var fileDataBases = dataBases.Where(d => d.IsFile == true).ToList();
            foreach (var fileDb in fileDataBases)
            {
                fileDataBaseSize += fileDb.SizeInMB;
            }

            var myDiskPropertiesByAccount = myDiskPropertiesByAccountDataProvider.GetIfExistsOrCreateNew(accountId);
            var clientFileSize = myDiskPropertiesByAccount.MyDiskFilesSizeInMb;
            var usedSizeOnDisk = clientFileSize + serverDataBaseSize + fileDataBaseSize;
            var freeSizeOnDisk = availableSizeOnDisk - usedSizeOnDisk;

            if (freeSizeOnDisk < 0)
                freeSizeOnDisk = 0;

            var myDiskInfoModelDto = new MyDiskInfoModelDto
            {
                ClinetFilesCalculateDate = myDiskPropertiesByAccount.MyDiskFilesSizeActualDateTime,
                AvailableSize = availableSizeOnDisk,
                PayedSize = payedSizeOnDisk,
                FreeSizeOnDisk = freeSizeOnDisk,
                ClientFilesSize = clientFileSize,
                ServerDataBaseSize = serverDataBaseSize,
                FileDataBaseSize = fileDataBaseSize,
                UsedSizeOnDisk = usedSizeOnDisk,
                BillingInfo = GetDiskBillingInfo(accountId)
            };
            return myDiskInfoModelDto;
        }

        private MyDiskBillingModelDto GetDiskBillingInfo(Guid accountId)
        {
            var resConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == accountId && r.BillingService.SystemService == Clouds42Service.MyDisk);

            if (resConfig == null)
                throw new NotFoundException(
                    $"Ну удалось получить запись ResourceConfiguration по сервису Мой диск для аккаунта '{accountId}'");

            var currentTariff =
                dbLayer.DiskResourceRepository.AsQueryableNoTracking().Include(w => w.Resource).FirstOrDefault(r =>
                    r.Resource.AccountId == accountId && r.Resource.Subject == accountId)?.VolumeGb ??
                CloudConfigurationProvider.BillingServices.MyDisk.GetMyDiskFreeTariffSize();

            return new MyDiskBillingModelDto
            {
                MonthlyCost = resConfig.Cost,
                CurrentTariffInGb = currentTariff
            };
        }

        public decimal CalculateActualCostOfMyDiskService(int size, Account account)
        {
            var diskLicense = GetDiskLicenseMax(account);

            if (!diskLicense.HasValue)
                return CalculateDiskCost(size, account);

            //стоимость подключенного диска согласно тарификации.
            var realCostByRate = CalculateDiskCost(diskLicense.Value, account);
            var diskResConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == account.Id && r.BillingService.Name == Clouds42Service.MyDisk.ToString());

            //Если реальная стоимость сервиса меньше тарификации
            if (diskResConfig != null && diskResConfig.Cost < realCostByRate && size <= diskLicense.Value)
            {
                return diskResConfig.Cost;
            }

            return CalculateDiskCost(size, account);
        }

        /// <summary>
        /// Посчитать стоимость сервиса "Мой диск"
        /// </summary>
        /// <param name="size">Размер диска</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Стоимость сервиса "Мой диск"</returns>
        public decimal CalculateDiskCost(int size, Account account)
        {
            var localeId = accountConfigurationDataProvider.GetAccountLocaleId(account.Id);

            var rates = dbLayer.RateRepository.Where(r =>
                    r.BillingServiceType.Service.SystemService == Clouds42Service.MyDisk &&
                    r.LocaleId == localeId &&
                    (size >= r.UpperBound || r.LowerBound <= size && size <= r.UpperBound)).OrderBy(o => o.LowerBound)
                .ToList();

            if (!rates.Any())
                throw new InvalidOperationException($"Не определен тариф для дискового объема в {size} гб");

            decimal cost = 0;
            foreach (var rateMyDisk in rates)
            {
                if (!rateMyDisk.UpperBound.HasValue || !rateMyDisk.LowerBound.HasValue)
                    throw new InvalidOperationException($"Не определен дисковый диапозон тарифа {rateMyDisk.Id}");

                if (rateMyDisk.UpperBound <= size)
                {
                    cost += (rateMyDisk.UpperBound.Value - rateMyDisk.LowerBound.Value + 1) * rateMyDisk.Cost;
                }
                else if (rateMyDisk.UpperBound > size && size >= rateMyDisk.LowerBound)
                {
                    cost += (size - rateMyDisk.LowerBound.Value + 1) * rateMyDisk.Cost;
                }
            }

            return cost;
        }

        /// <summary>
        /// Получить сумму максимальный д
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        private int? GetDiskLicenseMax(Account account)
            => dbLayer.DiskResourceRepository.FirstOrDefault(
                    r => r.Resource.AccountId == account.Id && r.Resource.Subject != null)?
                .VolumeGb;

        public MyDiskChangeTariffPreviewDto ChangeSizeOfServicePreview(Guid accountId, int size)
        {
            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);
            var myDiskInfo = GetMyDiskInfo(accountId, size * 1024);

            return new MyDiskChangeTariffPreviewDto
            {
                MyDiskVolumeInfo = new MyDiskVolumeInfoDto
                {
                    ClientFilesSize = myDiskInfo.ClientFilesSize,
                    FileDataBaseSize = myDiskInfo.FileDataBaseSize,
                    ServerDataBaseSize = myDiskInfo.ServerDataBaseSize,
                    FreeSizeOnDisk = myDiskInfo.FreeSizeOnDisk,
                    AvailableSize = myDiskInfo.AvailableSize
                },
                Cost = CalculateActualCostOfMyDiskService(size, account)
            };
        }

        /// <summary>
        /// Получить текущую дату окончания сервиса или новую дату для рассчета стоимости сервиса
        /// </summary>        
        /// <returns>дату окончания для рассчета стоимости сервиса </returns>
        public DateTime GetCurrentExpireDateOrNewDateToCalculateCostOfService(DateTime? expireDate)
        {
            return expireDate.HasValue && expireDate.Value > DateTime.Now ? expireDate.Value : DateTime.Now.AddMonths(1);
        }
    }
}
