﻿using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.DataContracts.Billing.Rate;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.CloudServices.Helpers
{
    /// <summary>
    /// Хэлпер для получения информации о тарифах Аренды 1С
    /// </summary>
    public class Rent1CRateHelper(
        IUnitOfWork dbLayer,
        IResourcesService resourcesService,
        IRateProvider rateProvider,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Получить информацию о тарифах Аренды 1С для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Информация о тарифах Аренды 1С</returns>
        public Rent1CRateInfoDto GetRent1CRateInfo(Guid accountId)
        {
            try
            {
                var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(accountId);

                var myEntUser = dbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser);
                var myEntUserWeb = dbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUserWeb);
                var myEntRate = rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
                var myEntRateWeb = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

                return new Rent1CRateInfoDto
                {
                    Rent1CStandartServiceType = myEntUser,
                    Rent1CWebServiceType = myEntUserWeb,
                    Rent1CWebRate = myEntRateWeb,
                    Rent1CStandartRate = myEntRate
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения тарифов Аренды 1С для аккаунта] {accountId}");
                throw;
            }
        }

        /// <summary>
        /// Получить тариф услуги Аренды 1С для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="resourceType">Тип услуги</param>
        /// <returns>Тариф услуги Аренды 1С для аккаунта</returns>
        public RateInfoBaseDc GetRateInfoForServiceType(Guid accountId, ResourceType resourceType)
        {
            var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(accountId);
            var serviceType = dbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == resourceType);
            return rateProvider.GetOptimalRate(billingAccount.Id, serviceType.Id);
        }
    }
}
