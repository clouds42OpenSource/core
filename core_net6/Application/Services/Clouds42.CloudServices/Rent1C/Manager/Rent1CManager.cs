﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CloudServices.Rent1C.Manager
{
    /// <summary>
    /// Входная точка для управлением сервисом Аренда 1С
    /// </summary>
    public class Rent1CManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IServiceProvider serviceProvider,
        IRent1CAccessHelper rent1CAccessHelper,
        ICloudLocalizer cloudLocalizer,
        IRent1CServiceInfoDmProcessor rent1CServiceInfoDmProcessor)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Изменить параметры ресуср конфигурации Аренды 1С
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="model">Модель изменения Аренды 1С</param>
        public void ManageRent1C(Guid accountId, Rent1CServiceManagerDto model)
        {
            AccessProvider.HasAccess(ObjectAction.ControlPanel_EditResources_ChangeRent1CData, () => accountId);
            serviceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountId)
                .ManagedRent1C(model);
        }

        /// <summary>
        ///     Метод получения информации об Аренде 1С
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns></returns>
        public ManagerResult<Rent1CServiceInfoDto> GetRent1CInfo(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => accountId);

                var model = rent1CServiceInfoDmProcessor.GetModel(accountId);
                return Ok(model);
            }
            catch (Exception exception)
            {
                var errorGettingDataAboutRent1CPhrase = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.ErrorGettingDataAboutRent1C, accountId);
              
                _handlerException.Handle(exception, $"[{errorGettingDataAboutRent1CPhrase} для компании {accountId}.]");

                return PreconditionFailed<Rent1CServiceInfoDto>(
                    $"{errorGettingDataAboutRent1CPhrase} : {exception.GetFullInfo()}");
            }
        }

        /// <summary>
        ///     Поиск внешнего пользователя для подлючения аренды по средсвам Спонсорства.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="email">e-mail искомого пользователя</param>
        /// <returns></returns>
        public ManagerResult<SearchExternalUserRent1CResultDto> SearchExternalUserRent1C(Guid accountId, string email)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ManageResources, () => accountId);
                var result = rent1CAccessHelper.SearchExternalUser(accountId, email);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка поиска внешнего пользователя для подключения аренды 1С] {accountId}");					
                return PreconditionFailed<SearchExternalUserRent1CResultDto>(
                    $"Ошибка поиска внешнего пользователя для подключения сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C,accountId)}");
            }
        }
    }
}
