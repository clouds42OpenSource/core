﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Rent1C.Manager
{
    /// <summary>
    /// Менеджер для работы с данными упраления Арендой 1С
    /// </summary>
    public class Rent1CDataManagementDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IRent1CDataManagementDataProvider rent1CDataManagementDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить данные для управления Арендой 1С
        /// аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные для управления Арендой 1С</returns>
        public ManagerResult<Rent1CManagerDto> GetManagementData(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_EditResources_Rent1CManagement, () => accountId);
                var data = rent1CDataManagementDataProvider.GetManagementData(accountId);
                return Ok(data);
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения данных по управлению Арендой 1С для аккаунта '{accountId}'";
                _handlerException.Handle(ex, $"[{errorMessage}], {ex.Message}.");
                return PreconditionFailed<Rent1CManagerDto>(errorMessage);
            }
        }
    }
}
