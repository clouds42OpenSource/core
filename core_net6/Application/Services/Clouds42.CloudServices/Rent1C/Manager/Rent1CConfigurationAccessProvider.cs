﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CloudServices.Rent1C.Manager
{
    /// <summary>
    /// Провайдер управления активации/диактивации сервиса Аренда 1С у ползователй
    /// </summary>
    public class Rent1CConfigurationAccessProvider(
        IResourcesService resourcesService,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IServiceUnlocker serviceUnlocker,
        IDocLoaderByRent1CProlongationProcessor docLoaderByRent1CProlongationProcessor,
        IServiceProvider serviceProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IRent1CConfigurationAccessProvider
    {
        /// <summary>
        ///     Подключение/отключение аренды для своих и спонсируемых пользователей
        /// с использованием услуги Обещаный платеж
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accessList">Список из изменяемых пользователей</param>
        /// <returns></returns>
        public bool TryPayAccessByPromisePaymentForRent1C(Guid accountId,
            List<UpdaterAccessRent1CRequestDto> accessList)
        {
            var needIncreaseRec42Resources = NeedIncreaseRec42Resources(accessList);

            var cloudService = serviceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountId);
            var result = cloudService.TryPayAccessByPromisePayment(accessList);

            if (result && needIncreaseRec42Resources)
            {
                docLoaderByRent1CProlongationProcessor.ProcessIncrease(accountId);
            }

            return result;
        }

        /// <summary>
        ///     Подключение и отключение аренды для своих и спонсируемых пользователей
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accessList">Список из изменяемых пользователей</param>
        /// <returns></returns>
        public UpdateAccessRent1CResultDto ConfigureAccesses(Guid accountId, List<UpdaterAccessRent1CRequestDto> accessList)
        {

            var needIncreaseRec42Resources = NeedIncreaseRec42Resources(accessList);
            try
            {
                var myEnterpriseResourceConfiguration = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);
                if (myEnterpriseResourceConfiguration is null)
                {
                    logger.Info($"У пользователя {accountId} не подключена аренда");
                    return new UpdateAccessRent1CResultDto{ Complete = false, ErrorMessage = $"У пользователя {accountId} не подключена аренда"};
                }
                var oldCostOfMyEnterprise = resourceConfigurationDataProvider.GetTotalServiceCost(myEnterpriseResourceConfiguration);

                logger.Info($"Старая стоимость сервиса : {oldCostOfMyEnterprise}, начало подключения/отключения пользователей в количестве {accessList.Count}");

                var cloudService = serviceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountId);
                var result = cloudService.ConfigureAccesses(accessList);

                if (result.Complete && needIncreaseRec42Resources)
                {
                    docLoaderByRent1CProlongationProcessor.ProcessIncrease(accountId);
                }

                if (!string.IsNullOrEmpty(result.ErrorMessage))
                {
                    return result;
                }

                logger.Trace("Подключение/отключение пользователей завершилось без ошибки. Покупка заблокированного сервиса если его стоимость изменилась и денег на балансе достаточно для продления");
                serviceUnlocker.ProlongServiceIfTheCostIsLess(accountId, oldCostOfMyEnterprise, cloudService.ResourceConfiguration);

                return result;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка конфигурации подключаемых лицензий у Аренды 1С]");
                throw;
            }
        }

        /// <summary>
        /// Метод активирования Аренды 1С
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <returns>Результат активации аренды 1С</returns>
        public void TurnOnRent1C(Guid accountId, Guid? accountUserId = null)
        {
            var myEnterprise42CloudService = serviceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountId);
            myEnterprise42CloudService.ActivateService(accountUserId);
        }

        /// <summary>
        /// Проверить необходимость запуска процессора по начислению страниц ЗД.
        /// </summary>
        /// <param name="accessList">Состояние тумлеров с клиента.</param>
        private bool NeedIncreaseRec42Resources(List<UpdaterAccessRent1CRequestDto> accessList)
            => accessList.Any(res => res is { StandartResource: true, RdpResourceId: null });
    }
}
