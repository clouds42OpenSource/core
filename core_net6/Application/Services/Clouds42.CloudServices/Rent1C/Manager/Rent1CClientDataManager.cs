﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Rent1C.Manager
{
    /// <summary>
    /// Менеджер для работы с данными Аренды 1С клиента
    /// </summary>
    public class Rent1CClientDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IRent1CClientDataProvider rent1CClientDataProvider,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IRent1CClientDataManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить виды разрешений на аренду 1С
        /// для клиента 
        /// </summary> 
        /// <param name="accountId">Id аккаунта пользователя</param>
        /// <param name="requestedUserId">Id пользователя который запрашивает информацию</param>
        /// <returns>Разрешения на аренду 1С для клиента</returns>
        public ManagerResult<Rent1CClientPermissionsDto> GetRent1CClientPermissions(Guid accountId, Guid requestedUserId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => accountId);
                var accountDatabasesListInfo = rent1CClientDataProvider.GetRent1CClientPermissions(accountId, requestedUserId);
                return Ok(accountDatabasesListInfo);
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения видов разрешения для сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)}";
                _handlerException.Handle(ex, $"[{errorMessage}], {ex.Message}.");
                return PreconditionFailed<Rent1CClientPermissionsDto>(errorMessage);
            }
        }
    }
}
