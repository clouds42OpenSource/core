﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Rent1C.Manager
{

    /// <summary>
    /// Менеджер управления активации/диактивации сервиса Аренда 1С у ползователй
    /// </summary>
    public class Rent1CConfigurationAccessManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloudLocalizer cloudLocalizer,
        IRent1CConfigurationAccessProvider rent1CConfigurationAccessProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;
        private readonly IUnitOfWork _dbLayer = dbLayer;


        /// <summary>
        ///     Подключение/отключение аренды для своих и спонсируемых пользователей
        /// с использованием услуги Обещаный платеж
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accessList">Список из изменяемых пользователей</param>
        /// <returns></returns>
        public ManagerResult<bool> TryPayAccessByPromisePaymentForRent1C(Guid accountId,
            List<UpdaterAccessRent1CRequestDto> accessList)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_ChangeSubscribe, () => accountId);

            try
            {
                var result = rent1CConfigurationAccessProvider.TryPayAccessByPromisePaymentForRent1C(accountId, accessList);
                return Ok(result);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, $"[Ошибка Подключение/отключение пользователей с ОП] {string.Join(", ", accessList)} от аренды 1С.");
                return PreconditionFailed<bool>("Подключение/отключение пользователей с ОП завершилось c ошибкой");
            }
        }

        /// <summary>
        ///     Подключение и отключение аренды для своих и спонсируемых пользователей
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accessList">Список из изменяемых пользователей</param>
        /// <returns></returns>
        public ManagerResult<UpdateAccessRent1CResultDto> ConfigureAccesses(Guid accountId, List<UpdaterAccessRent1CRequestDto> accessList)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_ChangeSubscribe, () => accountId);
                var result = rent1CConfigurationAccessProvider.ConfigureAccesses(accountId, accessList);
                return Ok(result);
            }
            catch (Exception exception)
            {
                Logger.Warn(exception, $"[Ошибка Подключение/отключение пользователей] {string.Join(",", accessList.Select(u => u.AccountUserId))} от аренды 1С.");
                return PreconditionFailed<UpdateAccessRent1CResultDto>("Подключение/отключение пользователей завершилось c ошибкой", exception);
            }
        }

        /// <summary>
        ///     Метод активирования Аренды 1С
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <returns>Результат активации аренды 1С</returns>
        public ManagerResult TurnOnRent1C(Guid accountId, Guid? accountUserId = null)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_ActivateRent1C, () => accountId);

            try
            {
                rent1CConfigurationAccessProvider.TurnOnRent1C(accountId, accountUserId);
                return Ok();
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception,
                    $"Ошибка включения аренды 1С у компании {accountId} для пользователя {accountUserId}.");
                return PreconditionFailed<bool>($"Ошибка включения сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)}");
            }
        }

        public ManagerResult GetUserWithRent1C(Guid userId) 
        {
            var containsAny = _dbLayer.ResourceRepository
                .AsQueryable()
                .Include(r => r.BillingServiceType)
                .Any(r => (r.BillingServiceType.Name == "WEB" || r.BillingServiceType.Name == "Стандарт") && r.Subject == userId);

            return containsAny ? Ok() : PreconditionFailed("Доступ запрещен!");
        }

    }
}
