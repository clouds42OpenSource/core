﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Helpers;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.ResourceConfiguration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;
using Microsoft.Extensions.Configuration;

namespace Clouds42.CloudServices.Rent1C
{
    /// <summary>
    /// Процессор для работы с информацией сервиса Аренда 1С
    /// </summary>
    public class Rent1CServiceInfoDmProcessor(
        IUnitOfWork dbLayer,
        IResourceDataProvider resourceDataProvider,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IAdditionalResourcesDataProvider additionalResourcesDataProvider,
        ICloud42ServiceHelper cloud42ServiceHelper,
        IServiceStatusHelper serviceStatusHelper,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IResourcesService resourcesService,
        IServiceAccountDataHelper serviceAccountDataHelper,
        IRateProvider rateProvider,
        IConfiguration configuration)
        : IRent1CServiceInfoDmProcessor
    {
        /// <summary>
        ///     Получить модель которая содержит информацию
        /// о Аренде 1С
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns>Модель с информацией об Аренде 1С</returns>
        public Rent1CServiceInfoDto GetModel(Guid accountId)
        {
            serviceAccountDataHelper.CheckAbilityToManageService(accountId);

            var model = new Rent1CServiceInfoDto();
            var resConfig = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);

            var accountWithConfiguration = accountConfigurationDataProvider.GetAccountWithConfiguration(accountId);

            if (resConfig == null)
            {
                model.ServiceStatus = null;
            }
            else
            {
                model.ExpireDate = resConfig.ExpireDateValue.Date;
                model.MonthlyCostTotal = resConfig.Cost;
                model.ServiceStatus = serviceStatusHelper.CreateServiceStatusModel(accountId, resConfig);
            }

            var locale = dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(configuration["DefaultLocale"] ?? ""));
            model.Locale = new LocaleDto { Name = locale.Name, Currency = locale.Currency };

            InitResources(accountId, model);

            model.AccountIsVip = accountWithConfiguration.AccountConfiguration.IsVip;
            model.MyDatabasesServiceData = myDatabasesServiceDataProvider.GetMyDatabasesServiceShortData(accountId);
            if (!accountWithConfiguration.AccountConfiguration.IsVip)
                model.MonthlyCostTotal += model.MyDatabasesServiceData.TotalAmountForDatabases;

            model.AdditionalAccountResources = InitAdditionalResources(accountWithConfiguration.Account);

            return model;
        }

        /// <summary>
        /// Инициилизировать дополнительные ресурсы для аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Модель дополнительных ресурсов аккаунта</returns>
        private AdditionalAccountResourcesModelDto InitAdditionalResources(Account account)
        {
            var additionalResourcesData = additionalResourcesDataProvider.GetAdditionalResourcesData(account.Id);

            if (!additionalResourcesData.IsAvailabilityAdditionalResources)
                return new AdditionalAccountResourcesModelDto();

            return new AdditionalAccountResourcesModelDto
            {
                AdditionalResourcesCost = additionalResourcesData.AdditionalResourceCost ?? 0,
                AdditionalResourcesName = additionalResourcesData.AdditionalResourceName,
                NeedShowAdditionalResources = true
            };
        }

        /// <summary>
        /// Подсчет стоимости ресурса для спонсируемого аккаунта
        /// </summary>
        /// <param name="accountUser"></param>
        /// <param name="partialRateWebCost"></param>
        /// <param name="partialRateRdpCost"></param>
        /// <param name="partialUserCostStandart"></param>
        public void CalculatePartialCostForSponsorAccount(AccountUser accountUser, out decimal partialRateWebCost,
            out decimal partialRateRdpCost, out decimal partialUserCostStandart)
        {
            var resourceConfig = resourcesService.GetResourceConfig(accountUser.AccountId, Clouds42Service.MyEnterprise);

            GetOptimalRates(accountUser.AccountId, out var rateWebCost, out var rateRdpCost);

            var expireDateOfService =
                cloud42ServiceHelper.GetCurrentExpireDateOrNewDateToCalculateCostOfService(resourceConfig?
                    .ExpireDateValue);

            var accountLocaleName = accountConfigurationDataProvider.GetAccountLocale(accountUser.AccountId).Name;

            partialRateWebCost = BillingServicePartialCostCalculator.CalculateUntilExpireDate(rateWebCost,
                expireDateOfService, accountLocaleName);
            partialRateRdpCost = BillingServicePartialCostCalculator.CalculateUntilExpireDate(rateRdpCost,
                expireDateOfService, accountLocaleName);
            partialUserCostStandart =
                BillingServicePartialCostCalculator.CalculateUntilExpireDate(rateRdpCost + rateWebCost,
                    expireDateOfService, accountLocaleName);
        }

        private LicenseOfRent1CDm GetLicenseOfRent1CDm(Guid accountId, AccountUser accountUser, Account account,
            List<Resource> userResources)
        {
            var webResource =
                userResources.FirstOrDefault(f => f.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb);
            var rdpResource =
                userResources.FirstOrDefault(f => f.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);

            return new LicenseOfRent1CDm
            {
                Id = accountUser.Id,
                AccountId = account.Id,
                FullUserName = $"{accountUser.LastName} {accountUser.FirstName} {accountUser.MiddleName}".Trim(),
                Login = accountUser.Login,
                IsActive = accountUser.Activated,
                Email = accountUser.Email,
                Phone = accountUser.PhoneNumber,
                RdpResourceId = rdpResource?.Id,
                WebResourceId = webResource?.Id,
                SponsoredLicenseAccountName = GetSponsoredLicenseAccountName(userResources, accountId),
                SponsorshipLicenAccountName = GetSponsorshipLicenAccountName(userResources, accountId),
                NamesOfDependentActiveServices = GetNamesOfDependentActiveServices(accountUser.Id)
            };
        }

        private void InitResources(Guid accountId, Rent1CServiceInfoDto model)
        {
            var resources = resourceDataProvider.GetResourcesLazy(
                Clouds42Service.MyEnterprise,
                r => (r.AccountId == accountId || r.AccountSponsorId == accountId) && r.Subject != null,
                ResourceType.MyEntUser,
                ResourceType.MyEntUserWeb
            ).Include(r => r.AccountSponsor);

            InitUsers(accountId, model, resources);
            var resourcesList = resources.ToList();

            GetOptimalRates(accountId, out var rateWebCost, out var rateRdpCost);

            model.RateWeb = rateWebCost;
            model.RateRdp = rateRdpCost;

            model.UserList.ForEach(w =>
            {
                GetOptimalRates(w.AccountId, out rateWebCost, out rateRdpCost);

                w.PartialUserCostWeb =
                    BillingServicePartialCostCalculator.CalculateUntilExpireDate(rateWebCost, model.ExpireDate,
                        model.Locale.Name);
                w.PartialUserCostRdp =
                    BillingServicePartialCostCalculator.CalculateUntilExpireDate(rateRdpCost, model.ExpireDate,
                        model.Locale.Name);
                w.PartialUserCostStandart =
                    BillingServicePartialCostCalculator.CalculateUntilExpireDate(rateRdpCost + rateWebCost,
                        model.ExpireDate, model.Locale.Name);
            });

            model.FreeWebResources = resourceDataProvider.GetResourcesLazy(
                Clouds42Service.MyEnterprise,
                r => r.AccountId == accountId && r.Subject == null,
                ResourceType.MyEntUserWeb
            ).Select(s => s.Id).ToList();

            model.FreeRdpResources = resourceDataProvider.GetResourcesLazy(
                Clouds42Service.MyEnterprise,
                r => r.AccountId == accountId && r.Subject == null,
                ResourceType.MyEntUser
            ).Select(s => s.Id).ToList();

            //все рдп ресурсы за которые платит текущий аккаунт
            var paydResRdp = resourcesList.Where(r => (r.AccountSponsorId == null || r.AccountSponsorId == accountId) &&
                                                      r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser)
                .ToList();

            var allResWeb = resourcesList.Where(r =>
                (r.AccountSponsorId == null || r.AccountSponsorId == accountId) &&
                r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb).ToList();

            //все веб ресурсы за которые платит текущий аккаунт
            var paydResWeb = allResWeb.Where(r => !paydResRdp.Select(rdp => rdp.Subject).Contains(r.Subject)).ToList();

            model.MonthlyCostWeb = paydResWeb.Sum(r => r.Cost);

            model.MonthlyCostStandart =
                paydResRdp.Sum(r => r.Cost) +
                allResWeb.Where(r => paydResRdp.Select(rdp => rdp.Subject).Contains(r.Subject)).Sum(r => r.Cost);

            model.SponsoredStandartCount = paydResRdp.Count(r => r.AccountSponsorId == accountId);
            model.SponsoredWebCount = paydResWeb.Count(r => r.AccountSponsorId == accountId);

            model.SponsorshipStandartCount = resourcesList.Count(r => r.AccountSponsorId != null &&
                                                                      r.AccountSponsorId != accountId &&
                                                                      r.BillingServiceType.SystemServiceType ==
                                                                      ResourceType.MyEntUser);

            model.SponsorshipWebCount = Math.Max(0, resourcesList.Count(r => r.AccountSponsorId != null &&
                                                                             r.AccountSponsorId != accountId &&
                                                                             r.BillingServiceType.SystemServiceType ==
                                                                             ResourceType.MyEntUserWeb) -
                                                    model.SponsorshipStandartCount);

            model.StandartResourcesCount =
                resourcesList.Count(r => r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);
            model.WebResourcesCount = Math.Max(0,
                resourcesList.Count(r => r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb) -
                model.StandartResourcesCount);
        }

        private void GetOptimalRates(Guid accountId, out decimal rateWebCost, out decimal rateRdpCost)
        {
            var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(accountId);

            var myEntUser =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUser);
            var myEntUserWeb =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUserWeb);
            rateRdpCost = rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id)?.Cost ?? 0;
            rateWebCost = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id)?.Cost ?? 0;
        }

        private void InitUsers(Guid accountId, Rent1CServiceInfoDto model, IQueryable<Resource> resources)
        {
            var notDeletedUsers = dbLayer.AccountUsersRepository.WhereLazy(x => x.CorpUserSyncStatus != "Deleted" &&
                                                                                 x.CorpUserSyncStatus != "SyncDeleted");
            var queryable = (
                    from user in notDeletedUsers.Where(w => w.AccountId == accountId)
                    join account in dbLayer.AccountsRepository.WhereLazy()
                        on user.AccountId equals account.Id
                    select new { user, account })
                .Concat(
                    from user in notDeletedUsers.Where(w => w.AccountId != accountId)
                    join res in resources.Where(r => r.AccountSponsorId == accountId)
                        on user.Id equals res.Subject
                    join account in dbLayer.AccountsRepository.WhereLazy()
                        on user.AccountId equals account.Id
                    select new { user, account }
                ).Distinct();

            var accountUsersList = queryable.Select(w => w.user).ToList();
            var accountsList = queryable.Select(w => w.account).ToList();
            var resourcesList = resources.ToList();

            foreach (var accountUser in accountUsersList)
            {
                var userResources = resourcesList.Where(r => r.Subject == accountUser.Id).ToList();

                if (!userResources.Any() && accountUser.AccountId != accountId)
                    continue;

                var account = accountsList.FirstOrDefault(w => w.Id == accountUser.AccountId);

                model.UserList.Add(GetLicenseOfRent1CDm(accountId, accountUser, account, userResources));
            }

            model.UserList = model.UserList.OrderBy(o => o.SponsoredLicenseAccountName + o.SponsorshipLicenAccountName)
                .ToList();
        }

        private string GetSponsorshipLicenAccountName(List<Resource> userResources, Guid accountId)
        {
            var sponsorResource = userResources.FirstOrDefault(r =>
                r.AccountId == accountId && r.AccountSponsorId != null && r.AccountSponsorId != accountId) ;
            return sponsorResource == null ? null : GetAccountName(sponsorResource.AccountSponsor);
        }

        private string GetSponsoredLicenseAccountName(List<Resource> userResources, Guid accountId)
        {
            Resource spondoredResource;
            if ((spondoredResource =
                    userResources.FirstOrDefault(r => r.AccountId != accountId && r.AccountSponsorId == accountId)) ==
                null)
                return null;

            var accountOfSponsoredLicense =
                dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == spondoredResource.AccountId);

            if (accountOfSponsoredLicense == null)
                return null;

            return GetAccountName(accountOfSponsoredLicense);
        }

        private string GetAccountName(Account account)
        {
            if (!string.IsNullOrEmpty(account.AccountCaption))
                return account.AccountCaption;

            return account.IndexNumber.ToString();
        }

        /// <summary>
        /// Получить названия всех услуг что зависимы от аренды для данного пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        private List<KeyValuePair<ResourceType, List<string>>> GetNamesOfDependentActiveServices(Guid accountUserId)
        {
            return
            [
                new(ResourceType.MyEntUser,
                    GetNamesOfDependentActiveService(accountUserId, ResourceType.MyEntUser).ToList()),


                new(ResourceType.MyEntUserWeb,
                    GetNamesOfDependentActiveService(accountUserId, ResourceType.MyEntUserWeb).ToList())
            ];
        }

        /// <summary>
        /// Получить названия всех активных сервисов(наличие ресурсов)
        /// что зависимы от услуги Аренды для пользователя
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="systemServiceType">Тип системной услуги(услуги Аренды 1С)</param>
        /// <returns>Названия всех активных сервисов(наличие ресурсов)
        /// что зависимы от услуги Аренды для пользователя</returns>
        private IEnumerable<string> GetNamesOfDependentActiveService(Guid accountUserId,
            ResourceType systemServiceType) =>
            (from resource in dbLayer.ResourceRepository.WhereLazy(r => r.Subject == accountUserId)
             join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource.BillingServiceTypeId
                 equals serviceType.Id
             join service in dbLayer.BillingServiceRepository.WhereLazy() on serviceType.ServiceId equals service.Id
             join dependServiceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on serviceType
                 .DependServiceTypeId equals dependServiceType.Id
             where dependServiceType.SystemServiceType == systemServiceType &&
                   service.SystemService != Clouds42Service.MyDatabases &&
                   service.SystemService != Clouds42Service.MyEnterprise
             select service.Name).Distinct().AsEnumerable();
    }
}
