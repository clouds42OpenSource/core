﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Rent1C.Providers
{
    /// <summary>
    /// Провайдер для работы с данными
    /// для конфигурации лицензий сервиса Аренда 1С
    /// </summary>
    public class ConfigurateRent1CAccessesDataProvider(IUnitOfWork dbLayer) : IConfigurateRent1CAccessesDataProvider
    {
        /// <summary>
        /// Получить данные для конфигурации лицензий сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные для конфигурации лицензий сервиса Аренда 1С</returns>
        public Rent1CAccessesForConfigurationDataDto GetData(Guid accountId) =>
            (from account in dbLayer.AccountsRepository.WhereLazy(a => a.Id == accountId)
             join resourcesConfiguration in dbLayer.ResourceConfigurationRepository.WhereLazy() on
                 account.Id equals resourcesConfiguration.AccountId
             join service in dbLayer.BillingServiceRepository.WhereLazy(s =>
                     s.SystemService == Clouds42Service.MyEnterprise) on resourcesConfiguration
                     .BillingServiceId
                 equals service.Id
             join accountUser in dbLayer.AccountUsersRepository.WhereLazy() on account.Id equals
                 accountUser.AccountId into accountUsers
             select new Rent1CAccessesForConfigurationDataDto
             {
                 Account = account,
                 ResourcesConfiguration = resourcesConfiguration,
                 UsersIdList = accountUsers.Select(accountUser => accountUser.Id).ToList()
             }).FirstOrDefault() ?? throw new NotFoundException(
                $"Не удалось получить данные для конфигурации лицензий сервиса Аренда 1С. Аккаунт '{accountId}'");
    }
}