﻿using CommonLib.Enums;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Resources.Contracts.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Management.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Rent1C.Providers
{
    /// <summary>
    /// Провайдер для работы с данными упраления Арендой 1С
    /// </summary>
    internal class Rent1CDataManagementDataProvider(
        IUnitOfWork dbLayer,
        IResourcesConfigurationDataProvider resourcesConfigurationDataProvider,
        IOptimalRateProvider optimalRateProvider,
        ICheckAccountDataProvider checkAccountDataProvider,
        IMyDatabasesServiceManagementDataProvider myDatabasesServiceManagementDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IRent1CDataManagementDataProvider
    {
        /// <summary>
        /// Получить данные для управления Арендой 1С
        /// аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные для управления Арендой 1С</returns>
        public Rent1CManagerDto GetManagementData(Guid accountId)
        {
            var accountWithConfiguration = accountConfigurationDataProvider.GetAccountWithConfiguration(accountId);

            var resourcesConfiguration =
                resourcesConfigurationDataProvider.GetResourcesConfiguration(accountId, Clouds42Service.MyEnterprise);

            if ((accountWithConfiguration.Account).BillingAccount == null || resourcesConfiguration == null)
                return new Rent1CManagerDto();

            return new Rent1CManagerDto
            {
                Service = GetDataForManagingRent1CService(accountWithConfiguration,
                    resourcesConfiguration.ExpireDateValue),
                Users = GetDataForManagingRent1CUsers(accountId).ToList()
            };
        }

        /// <summary>
        /// Получить данные для управления сервисом Арендой 1С
        /// </summary>
        /// <param name="accountWithConfiguration">Модель аккаунта с конфигурацией</param>
        /// <param name="expireDateRent1C">Дата пролонгации Аренды 1С</param>
        /// <returns>Данные для управления сервисом Арендой 1С</returns>
        private Rent1CServiceManagerDto GetDataForManagingRent1CService(
            AccountWithConfigurationDto accountWithConfiguration, DateTime expireDateRent1C)
        {
            var billingAccount = (accountWithConfiguration.Account).BillingAccount;

            var myDatabasesServiceManagementData =
                myDatabasesServiceManagementDataProvider.GetManagementData(billingAccount);

            return new Rent1CServiceManagerDto
            {
                ExpireDate = expireDateRent1C,
                AccountIsVip = accountWithConfiguration.AccountConfiguration.IsVip,
                AdditionalResourceName = billingAccount.AdditionalResourceName,
                AdditionalResourceCost = billingAccount.AdditionalResourceCost,
                CostOfRdpLicense = optimalRateProvider.GetOptimalRate(billingAccount, ResourceType.MyEntUser).Cost,
                CostOfWebLicense = optimalRateProvider.GetOptimalRate(billingAccount, ResourceType.MyEntUserWeb).Cost,
                IsDemoPeriod = checkAccountDataProvider.CheckAccountIsDemo(accountWithConfiguration.Account),
                MaxDemoExpiredDate = accountWithConfiguration.Account.RegistrationDate?.AddMonths(1),
                LimitOnFreeCreationDb = myDatabasesServiceManagementData.LimitOnFreeCreationDb,
                ServerDatabasePlacementCost = myDatabasesServiceManagementData.ServerDatabasePlacementCost,
                CostOfCreatingDbOverFreeLimit = myDatabasesServiceManagementData.CostOfCreatingDbOverFreeLimit
            };
        }

        /// <summary>
        /// Получить данные для управления ресурсами Арендой 1С
        /// (для пользователей) 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель управления ресурсом Аренды 1С</returns>
        private IEnumerable<Rent1CUserManagerDto> GetDataForManagingRent1CUsers(Guid accountId) =>
            (from resource in dbLayer.ResourceRepository.WhereLazy(r => r.AccountId == accountId)
             join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy(st =>
                     st.SystemServiceType == ResourceType.MyEntUser ||
                     st.SystemServiceType == ResourceType.MyEntUserWeb)
                 on resource.BillingServiceTypeId equals serviceType.Id
             join accountUserByResource in dbLayer.AccountUsersRepository.WhereLazy(au => au.AccountId == accountId)
                 on resource.Subject equals accountUserByResource.Id into accountUserByResources
             from accountUser in accountUserByResources.DefaultIfEmpty()
             orderby resource.IsFree
             select new
             {
                 accountUser,
                 resource,
                 serviceTypeName = serviceType.Name
             }).AsEnumerable().Select(data => new Rent1CUserManagerDto
             {
                 Name = data.accountUser != null ? data.accountUser.Name : string.Empty,
                 Login = data.accountUser != null ? data.accountUser.Login : string.Empty,
                 ResourceId = data.resource.Id,
                 Cost = data.resource.Cost,
                 ServiceTypeName = data.serviceTypeName
             });
    }
}
