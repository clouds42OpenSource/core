﻿using System.Text;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Services;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Locales.Extensions;

namespace Clouds42.CloudServices.Processors
{
    /// <summary>
    /// Обработчик уведомлений о приближении конца демо периода
    /// </summary>
    internal class DemoPeriodIsComingToEndNotificationProcessor(
        ILetterNotificationProcessor letterNotificationProcessor,
        ICloudLocalizer cloudLocalizer,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        IApplyDemoServiceSubscribeProcessor applyDemoServiceSubscribeProcessor)
        : IDemoPeriodIsComingToEndNotifacotionProcessor
    {
        private readonly Lazy<string> _routeValueForOpenBillingServicePage = new(CloudConfigurationProvider.Cp.GetRouteValueForOpenBillingServicePage);

        /// <summary>
        /// Обработать уведомление о приближении конца демо периода
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурации</param>
        public void Process(ResourcesConfiguration resourcesConfiguration, StringBuilder stringBuilder)
        {
            try
            {
                BillingServiceAmountDataModelDto billingServiceAmountData = applyDemoServiceSubscribeProcessor.GetBillingServiceAmountData(resourcesConfiguration.BillingServiceId, resourcesConfiguration.AccountId);

                if (CheckNoNeedToSendTheLetter(resourcesConfiguration, billingServiceAmountData))
                {
                    return;
                }

                letterNotificationProcessor
                    .TryNotify<DemoPeriodIsComingToEndLetterNotification, DemoPeriodIsComingToEndLetterModelDto>(
                    new DemoPeriodIsComingToEndLetterModelDto
                    {
                        AccountId = resourcesConfiguration.AccountId,
                        ServiceId = resourcesConfiguration.BillingServiceId,
                        ServiceName = resourcesConfiguration.BillingService?.GetNameBasedOnLocalization(cloudLocalizer, resourcesConfiguration.AccountId),
                        BillingServicePageUrl = GenerateBillingServicePageUrl(resourcesConfiguration.AccountId, resourcesConfiguration.BillingServiceId),
                        MoneyForService = CalculateMoneyForService(billingServiceAmountData),
                        RemainingNumberOfDays = IDemoPeriodIsComingToEndNotifacotionProcessor.RemainingNumberOfDays
                    });
            }
            catch (Exception ex)
            {
                var message =
                    $"Ошибка уведомления аккаунта '{resourcesConfiguration.AccountId}' о скором завершении демо периода. " +
                    $"сервиса '{resourcesConfiguration.GetLocalizedServiceName(cloudLocalizer)}' :: {ex.GetFullInfo()}";
                stringBuilder.Append(message);
            }
        }

        /// <summary>
        /// Посчитать сумму на оплату сервиса после демо
        /// </summary>
        /// <param name="billingServiceAmountData">Данные стоимости конфигурации сервиса</param>
        /// <returns>Сумму на оплату сервиса после демо</returns>
        private decimal CalculateMoneyForService(BillingServiceAmountDataModelDto billingServiceAmountData)
            => Math.Max(0, (billingServiceAmountData.ServicePartialAmount ?? 0) - (IDemoPeriodIsComingToEndNotifacotionProcessor.RemainingNumberOfDays * billingServiceAmountData.AmountForOneDay));

        /// <summary>
        /// Проверить, нужно ли отправлять сообщение о том, что демо период заканчивается или нет.
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация сервиса, которую нужно проверить</param>
        /// <param name="billingServiceAmountData">Данные стоимости этой конфигурации сервиса</param>
        /// <returns><c>true</c>, если не требуется отправлять сообщение, иначе <c>false</c> - требуется.</returns>
        private bool CheckNoNeedToSendTheLetter(ResourcesConfiguration resourcesConfiguration, BillingServiceAmountDataModelDto billingServiceAmountData)
            => resourcesConfiguration.Cost == 0 ||
               billingServiceAmountData.ServicePartialAmount == null ||
               billingServiceAmountData.ServicePartialAmount == 0;

        /// <summary>
        /// Сгенерировать URL для открытия страницы сервиса
        /// </summary>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <param name="serviceId">Для какого сервиса</param>
        /// <returns></returns>
        private string GenerateBillingServicePageUrl(Guid accountId, Guid serviceId) 
            => new Uri($"{localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountId)}/{_routeValueForOpenBillingServicePage.Value}{serviceId}")
               .AbsoluteUri;
    }
}
