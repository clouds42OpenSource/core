﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Processors
{
    /// <summary>
    /// Провайдер блокирования/ разблокирования сервиса.
    /// </summary>
    internal class CloudServiceLockUnlockProvider(
        IUnitOfWork dbLayer,
        ICloudServiceProcessor cloudServiceProcessor)
        : ICloudServiceLockUnlockProvider
    {
        /// <summary>
        /// Заблокировать сервис.
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        public void Lock(ResourcesConfiguration resourcesConfiguration)
        {
            cloudServiceProcessor.Process(resourcesConfiguration,
                (systemService) =>
                {
                    systemService.Lock();

                    if (systemService.CloudServiceType != Clouds42Service.Recognition)
                    {
                        SetResConfigFrozen(resourcesConfiguration, true);
                    }
                },
                () =>
                {
                    SetResConfigFrozen(resourcesConfiguration, true);
                });
        }

        /// <summary>
        /// Разблокировать сервис.
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        public void Unlock(ResourcesConfiguration resourcesConfiguration)
        {          
            cloudServiceProcessor.Process(resourcesConfiguration,
                (systemService) =>
                {
                    systemService.UnLock();
                    SetResConfigFrozen(resourcesConfiguration, false);
                },
                () =>
                {
                    SetResConfigFrozen(resourcesConfiguration, false);
                });
        }

        private void SetResConfigFrozen(ResourcesConfiguration resourcesConfiguration, bool frozenValue)
        {
            resourcesConfiguration.Frozen = frozenValue;
            dbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            dbLayer.Save();
        }        

    }
}
