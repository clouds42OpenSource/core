﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.CloudServices.Processors
{
    /// <summary>
    /// Процессор для обработки ОП
    /// </summary>
    public class PromisedPaymentProcessor(
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ICloudServiceLockUnlockProvider cloudServiceLockUnlockProvider,
        IServiceUnlocker serviceUnlocker,
        IBillingPaymentsProvider billingPaymentsProvider,
        ILogger42 logger,
        ICloudLocalizer cloudLocalizer,
        IConfiguration configuration)
        : IPromisedPaymentProcessor
    {
        /// <summary>
        /// Обработка обещанного платежа воркером
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// </summary>
        public void ProcessRegularOperation(BillingAccount billingAccount)
        {
            if (billingAccount.GetAvailableBalance() < billingAccount.GetPromisePaymentSum())
            {
                BlockAllPaidServices(billingAccount.Id);
            }
            else
            {
                RepayPromisedPayment(billingAccount, billingAccount.GetPromisePaymentSum());
            }
        }

        /// <summary>
        /// Преждeвременное погасить обещанный платеж
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <returns>true - если ОП погашен</returns>
        public bool EarlyRepay(BillingAccount billingAccount)
        {
            if (billingAccount.GetAvailableBalance() < billingAccount.GetPromisePaymentSum())
                return false;
            RepayPromisedPayment(billingAccount, billingAccount.GetPromisePaymentSum());
            return true;
        }

        /// <summary>
        /// Продлевает обещанный платеж на 1 день.
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// </summary>
        public bool ProlongPromisePayment(BillingAccount billingAccount)
        {
            try
            {
                if (!billingAccount.PromisePaymentDate.HasValue)
                    return false;

                var ppDays = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");

                var date = billingAccount.PromisePaymentDate.Value;

                var ppDateForToday = DateTime.Today.AddDays(-ppDays);

                if (date < ppDateForToday)
                    date = ppDateForToday;

                billingAccount.PromisePaymentDate = date.AddDays(1);

                unitOfWork.BillingAccountRepository.Update(billingAccount);
                unitOfWork.Save();

                serviceUnlocker.UnlockExpiredOrPayLockedServicesAccount(billingAccount.Id);
                logger.Trace(
                    $"Обещанный платёж аккаунта {billingAccount.Id} успешно продлён на один день. Исполнитель:{accessProvider?.GetUser()?.Id} , дата: {DateTime.Now}");
                return true;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка продления обещанного платежа для аккаунта] {billingAccount.Id}. Исполнитель:{accessProvider?.GetUser()?.Id}.");
                return false;
            }
        }

        #region Private          

        /// <summary>
        /// Погашаем обещанный платеж  
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="repaytDiff">Сумма оплаты</param>
        public void RepayPromisedPayment(BillingAccount billingAccount, decimal repaytDiff)
        {
            var date = DateTime.Now;

            var paymentResult = billingPaymentsProvider.MakePayment(new PaymentDefinitionDto
            {
                System = PaymentSystem.ControlPanel,
                Account = billingAccount.Id,
                Date = date,
                Description = "Погашение обещанного платежа",
                Status = PaymentStatus.Done,
                Total = repaytDiff,
                OperationType = PaymentType.Outflow,
                OriginDetails = "PromisePayment"
            });

            if (paymentResult.OperationStatus != PaymentOperationResult.Ok)
            {
                logger.Warn($"Ошибка при погашении обещанного платежа на {repaytDiff} аккаунта {billingAccount.Id}");
                LogEventHelper.LogEvent(unitOfWork, billingAccount.Id, accessProvider, LogActions.RepayPromisePayment,
                    $"Ошибка погашения обещанного платежа '{paymentResult.OperationStatus}'");
            }
            else
            {
                billingAccount.PromisePaymentSum = billingAccount.GetPromisePaymentSum() - repaytDiff;
                unitOfWork.BillingAccountRepository.Update(billingAccount);
                unitOfWork.Save();
                logger.Trace($"Погашен обещанный платеж на {repaytDiff} аккаунта {billingAccount.Id}");
                var currency = unitOfWork.AccountsRepository.GetLocale(billingAccount.Id, Guid.Parse(configuration["DefaultLocale"])).Currency;
                LogEventHelper.LogEvent(unitOfWork, billingAccount.Id, accessProvider, LogActions.RepayPromisePayment,
                    $"Погашен обещанный платеж на сумму {repaytDiff:0} {currency}.");
            }
        }

        /// <summary>
        /// Блокируем все платные сервисы
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void BlockAllPaidServices(Guid accountId)
        {
            var resourceList = unitOfWork.ResourceConfigurationRepository.GetAllPaidActiveMainServices(accountId);

            if (!resourceList.Any())
            {
                return;
            }

            foreach (var resource in resourceList)
            {
                cloudServiceLockUnlockProvider.Lock(resource);
            }

            LogEventHelper.LogEvent(unitOfWork, accountId, accessProvider, LogActions.LockService,
                $"Сервисы {string.Join(",", resourceList.Select(x => x.BillingService.GetNameBasedOnLocalization(cloudLocalizer, accountId)))} заблокированы." +
                "Причина: не погашен обещанный платеж");
        }

        #endregion
    }
}
