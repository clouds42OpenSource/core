﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.CloudServices.Processors
{
    public class CloudServiceAdapter(IUnitOfWork unitOfWork) : ICloudServiceAdapter
    {
        /// <summary>
        /// Получить запись конфигурации основного сервиса
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="cloudService">Тип системного сервиса.</param>
        /// <returns>Ресурс конфигурация основного сервиса.</returns>
        public ResourcesConfiguration GetMainConfigurationOrThrowException(Guid accountId, Clouds42Service cloudService)
        {
            var resConfig = unitOfWork.ResourceConfigurationRepository
                .AsQueryable()
                .Include(r => r.BillingService)
                .ThenInclude(x => x.BillingServiceTypes).FirstOrDefault(r =>
                    r.AccountId == accountId && r.BillingService.SystemService == cloudService);

            if (resConfig == null)
                throw new NotFoundException(
                    $"Не удалось получить запсь ResourceConfiguration аккаунта '{accountId}' для системной услуги '{cloudService}'");

            return resConfig.BillingService.BillingServiceTypes.Any(w => w.DependServiceTypeId != null) ? 
                GetMainConfigurationOrThrowException(resConfig) : 
                resConfig;
        }

        /// <summary>
        /// Получить запись конфигурации основного сервиса.
        /// </summary>
        /// <param name="resourceConfiguration">Исходная ресурс конфгурация.</param>        
        /// <returns>Ресурс конфигурация основного сервиса.</returns>
        public ResourcesConfiguration GetMainConfigurationOrThrowException(ResourcesConfiguration resourceConfiguration)
        {
            var mainServiceId = resourceConfiguration.BillingService.BillingServiceTypes.FirstOrDefault(w =>
                    w.DependServiceTypeId != null)?.DependServiceType.ServiceId;

            var mainResConfig = unitOfWork
                .ResourceConfigurationRepository
                .AsQueryable()
                .Include(x => x.BillingService)
                .ThenInclude(x => x.BillingServiceTypes)
                .FirstOrDefault(r => r.AccountId == resourceConfiguration.AccountId && r.BillingService.Id == mainServiceId);

            if (mainResConfig == null && mainServiceId.HasValue && !resourceConfiguration.BillingService.IsHybridService)
                throw new NotFoundException(
                    $"Не удалось получить запись ResourceConfiguration основной услуги для аккаунта '{resourceConfiguration.AccountId}' по услуге '{resourceConfiguration.BillingService.Name}'");

            return mainResConfig ?? resourceConfiguration;
        }

        /// <summary>
        /// Получить запись конфигурации основного сервиса.
        /// </summary>
        /// <param name="resourceConfiguration">Исходная ресурс конфгурация.</param>        
        /// <returns>Ресурс конфигурация основного сервиса.</returns>
        public ResourcesConfiguration GetMainConfiguration(ResourcesConfiguration resourceConfiguration)
        {
            var mainServiceId =
                resourceConfiguration.BillingService.BillingServiceTypes.FirstOrDefault(w =>
                    w.DependServiceTypeId != null)?.DependServiceType.ServiceId;

            var mainResConfig = unitOfWork.ResourceConfigurationRepository
                .AsQueryable()
                .Include(x => x.BillingService)
                .ThenInclude(x => x.BillingServiceTypes)
                .FirstOrDefault(r => r.AccountId == resourceConfiguration.AccountId && r.BillingService.Id == mainServiceId);

            if (mainResConfig == null && mainServiceId.HasValue)
                return null;

            return mainResConfig ?? resourceConfiguration;
        }
    }
}
