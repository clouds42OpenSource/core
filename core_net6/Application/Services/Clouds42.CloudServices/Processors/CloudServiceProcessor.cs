﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Processors
{
    /// <summary>
    /// Обработчик сервиса.
    /// </summary>
    public class CloudServiceProcessor(ICloud42ServiceFactory cloud42ServiceFactory) : ICloudServiceProcessor
    {
        /// <summary>
        /// Выполнить обработку.
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация сервиса.</param>
        /// <param name="systemServiceProcess">Обработка если сервис системный.</param>
        /// <param name="simpleServiceProcess">Обработка если сервис не системный.</param>
        public void Process(ResourcesConfiguration resourcesConfiguration, Action<Cloud42ServiceBase> systemServiceProcess, Action simpleServiceProcess)
        {
            if (resourcesConfiguration.BillingService.SystemService != null)
            {
                var systemService = GetSystemCloudServiceProvider(resourcesConfiguration.AccountId, resourcesConfiguration.BillingService.SystemService.Value);
                systemServiceProcess(systemService);
            }
            else
            {
                simpleServiceProcess();
            }
        }

        /// <summary>
        /// Выполнить обработку.
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация сервиса.</param>
        /// <param name="systemServiceProcess">Обработка если сервис системный.</param>
        /// <param name="simpleServiceProcess">Обработка если сервис не системный.</param>
        public TResult Process<TResult>(ResourcesConfiguration resourcesConfiguration, Func<Cloud42ServiceBase, TResult> systemServiceProcess, Func<TResult> simpleServiceProcess)
        {
            if (resourcesConfiguration.BillingService.SystemService == null)
                return simpleServiceProcess();

            var systemService = GetSystemCloudServiceProvider(resourcesConfiguration.AccountId, resourcesConfiguration.BillingService.SystemService.Value);
            return systemServiceProcess(systemService);

        }

        /// <summary>
        /// Получить обертку для управления систменым сервисом.
        /// </summary>
        /// <param name="accountId">Аккаунт.</param>
        /// <param name="systemClouds42Service">Тип системного сервиса.</param>
        /// <returns></returns>
        private Cloud42ServiceBase GetSystemCloudServiceProvider(Guid accountId, Clouds42Service systemClouds42Service)
            => cloud42ServiceFactory.Get(systemClouds42Service, accountId);

    }
}