﻿using System.Text;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Services;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Locales.Extensions;

namespace Clouds42.CloudServices.Processors
{
    /// <summary>
    /// Обработчик уведомлений о том что демо период завершён, нужна оплата
    /// </summary>
    public class DemoPeriodEndedPaymentRequiredNotificationProcessor(
        ILetterNotificationProcessor letterNotificationProcessor,
        ICloudLocalizer cloudLocalizer,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : IDemoPeriodEndedPaymentRequiredNotifacotionProcessor
    {
        private readonly Lazy<string> _routeValueForOpenBillingServicePage = new(CloudConfigurationProvider.Cp.GetRouteValueForOpenBillingServicePage);

        /// <summary>
        /// Обработать уведомление отом что демо период завершён, нужна оплата
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурации</param>
        public void Process(ResourcesConfiguration resourcesConfiguration, StringBuilder stringBuilder)
        {
            try
            {
                letterNotificationProcessor
                    .TryNotify<DemoPeriodEndedPaymentRequiredLetterNotification, DemoPeriodEndedPaymentRequiredLetterModelDto>(
                    new DemoPeriodEndedPaymentRequiredLetterModelDto
                    {
                        AccountId = resourcesConfiguration.AccountId,
                        ServiceId = resourcesConfiguration.BillingServiceId,
                        ServiceName = resourcesConfiguration.BillingService?.GetNameBasedOnLocalization(cloudLocalizer, resourcesConfiguration.AccountId),
                        BillingServicePageUrl = GenerateBillingServicePageUrl(resourcesConfiguration.AccountId, resourcesConfiguration.BillingServiceId)
                    });
            }
            catch (Exception ex)
            {
                var message =
                    $"Ошибка уведомления аккаунта '{resourcesConfiguration.AccountId}' о том что демо период завершён, нужна оплата." +
                    $"сервиса '{resourcesConfiguration.GetLocalizedServiceName(cloudLocalizer)}' :: {ex.GetFullInfo()}";
                stringBuilder.Append(message);
            }
        }

        /// <summary>
        /// Сгенерировать URL для открытия страницы сервиса
        /// </summary>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <param name="serviceId">Для какого сервиса</param>
        /// <returns></returns>
        private string GenerateBillingServicePageUrl(Guid accountId, Guid serviceId) 
            => new Uri($"{localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountId)}/{_routeValueForOpenBillingServicePage.Value}{serviceId}")
               .AbsoluteUri;
    }
}
