﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Managers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Helpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Invoice;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Processors
{
    /// <summary>
    /// Процессор нотификаций за 5 дней до блокировки сервиса
    /// </summary>
    public class Before5DayNotificationProcessor(
        IInvoiceProvider invoiceProvider,
        IBillingAccountManager billingAccountManager,
        IInvoiceDocumentBuilder razorBuilder,
        IUnitOfWork unitOfWork,
        ICloudLocalizer cloudLocalizer,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        IResourcesService resourcesService,
        ILogger42 logger)
        : ISystemCloudServiceProcessorVisitor
    {
        /// <summary>
        /// Выполнить базовое уведомление
        /// </summary>
        /// <param name="service">Сервис облака</param>
        private void BaseNotification(Cloud42ServiceBase service)
        {
            var configuration = service.ResourceConfiguration;
            if (configuration.Cost == 0m)
            {
                return;
            }
            var account = resourcesService.GetAccountIfExistsOrCreateNew(service.AccountId);
            var availableBalance = account.GetAvailableBalance();

            if (availableBalance < configuration.Cost)
            {
                BeforeLockNotificationAndCreateInvoice(service, account);
            }
        }

        /// <summary>
        /// Посетить
        /// </summary>
        /// <param name="service">Сервис облака</param>
        public void Visit(IMyDisk42CloudService service)
            => WriteNotImplementedMessageForCloudService((Cloud42ServiceBase)service);

        /// <summary>
        /// Посетить
        /// </summary>
        /// <param name="service">Сервис Аренда 1С</param>
        public void Visit(IMyEnterprise42CloudService service)
        {
            BaseNotification((Cloud42ServiceBase)service);
        }

        /// <summary>
        /// Посетить
        /// </summary>
        /// <param name="service">Сервис ЕСДЛ</param>
        public void Visit(IEsdlCloud42Service service)
            => WriteNotImplementedMessageForCloudService((Cloud42ServiceBase)service);

        /// <summary>
        /// Посетить
        /// </summary>
        /// <param name="service">Сервис рекогнишен</param>
        public void Visit(IRecognition42CloudService service)
            => WriteNotImplementedMessageForCloudService((Cloud42ServiceBase)service);

        /// <summary>
        /// Посетить
        /// </summary>
        /// <param name="service">Класс для управления сервисом "Мои информационные базы"</param>
        public void Visit(IMyDatabases42CloudService service)
            => WriteNotImplementedMessageForCloudService((Cloud42ServiceBase)service);

        /// <summary>
        /// Выполнить уведомление и создать счет на оплату
        /// </summary>
        /// <param name="service">Сервис облака</param>
        /// <param name="account">Аккаунт облака</param>
        private void BeforeLockNotificationAndCreateInvoice(Cloud42ServiceBase service, BillingAccount account)
        {
            var invoice =
                invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(service.ResourceConfiguration, account);

            var invoiceDc = billingAccountManager.GetInvoiceDc(invoice.Id).Result;
            var invoicePdf = razorBuilder.Build(invoiceDc);

            var siteAuthorityUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(account.Id);

            letterNotificationProcessor.TryNotify<BeforeServiceLockLetterNotification, BeforeServiceLockLetterModelDto>(
                PrepareBeforeServiceLockLetterModelHelper.PrepareBeforeServiceLockLetterModel(unitOfWork,
                    service.ResourceConfiguration,
                    invoiceDc, invoicePdf, siteAuthorityUrl, invoice.Period, cloudLocalizer));
        }

        /// <summary>
        /// Записать в логи сообщение о нереализованном функционале для сервиса облака
        /// </summary>
        /// <param name="service">Сервис</param>
        private void WriteNotImplementedMessageForCloudService(Cloud42ServiceBase service)
            => logger.Trace(
                $"Уведомление за 5 дней до блокировки не предусмотрено для сервиса {service.CloudServiceType.GetDisplayName()}");

        /// <summary>
        /// Выполнить обработку
        /// </summary>
        public void Process(Cloud42ServiceBase service)
        {
            service.AcceptVisitor(this);
        }
    }
}
