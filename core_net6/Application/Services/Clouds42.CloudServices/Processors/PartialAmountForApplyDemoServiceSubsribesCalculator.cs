﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Processors
{

    /// <summary>
    /// Калькулятор расчета суммы которую нужно оплатить что бы применить подписку демо сервиса.
    /// </summary>
    public class PartialAmountForApplyDemoServiceSubsribesCalculator : IPartialAmountForApplyDemoServiceSubsribesCalculator
    {

        /// <summary>
        /// Посчитат сумму.
        /// </summary>
        /// <param name="resourceConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="mainResourceConfiguration">Ресурс конфигурация основного сервиса.</param>
        /// <returns>Сумма которая нужна что бы применить подписку демо сервиса.</returns>
        public decimal? Calculate(ResourcesConfiguration resourceConfiguration, ResourcesConfiguration mainResourceConfiguration)
        {
            var countDays = Convert.ToInt32(Math.Max(0, (mainResourceConfiguration.ExpireDateValue - CalculateFreeUsageServiceDate(resourceConfiguration)).TotalDays));
            return CalculateInternal(resourceConfiguration, mainResourceConfiguration, countDays);
        }


        /// <summary>
        /// Посчитат сумму.
        /// </summary>
        /// <param name="resourceConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="mainResourceConfiguration">Ресурс конфигурация основного сервиса.</param>
        /// <param name="countDays">За сколько дней посчитать.</param>
        /// <returns>Сумма которая нужна что бы применить подписку демо сервиса.</returns>
        public decimal? CalculateForDays(ResourcesConfiguration resourceConfiguration, ResourcesConfiguration mainResourceConfiguration, int countDays) 
            => CalculateInternal(resourceConfiguration, mainResourceConfiguration, countDays);


        /// <summary>
        /// Посчитат сумму.
        /// </summary>
        /// <param name="resourceConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="mainResourceConfiguration">Ресурс конфигурация основного сервиса.</param>
        /// <param name="countDays">За сколько дней посчитать.</param>
        /// <returns>Сумма которая нужна что бы применить подписку демо сервиса.</returns>
        private static decimal? CalculateInternal(ResourcesConfiguration resourceConfiguration, ResourcesConfiguration mainResourceConfiguration, int countDays)
        {
            if (!resourceConfiguration.IsDemoPeriod)
                return null;

            return resourceConfiguration.Id == mainResourceConfiguration.Id ? resourceConfiguration.Cost : Math.Round(Math.Min((countDays * resourceConfiguration.Cost) / 30, resourceConfiguration.Cost), 0);
        }


        /// <summary>
        /// Расчитать дату с которой не нужно платить.
        /// </summary>
        /// <param name="resourceConfiguration"></param>
        /// <returns></returns>
        private DateTime CalculateFreeUsageServiceDate(ResourcesConfiguration resourceConfiguration)
            => DateTime.Now > resourceConfiguration.ExpireDateValue
                ? DateTime.Now
                : resourceConfiguration.ExpireDateValue;

    }
}
