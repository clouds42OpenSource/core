//using Clouds42.Billing.Billing.Internal;
//using Clouds42.CloudServices.Contracts;
//using Clouds42.CloudServices.Helpers;
//using Clouds42.DataContracts.Cloud42Services;
//using Clouds42.DomainContext.DataModels.Billing;
//using CommonLib.Enums;
//using NLog;

//namespace Clouds42.CloudServices.Processors
//{

//    /// <summary>
//    /// ��������� ��������/������������� �������� 
//    /// </summary>
//    public class CheckPaymentsServiceProcessor : I42CloudServiceProcessorVisitor
//    {

//        private readonly NLog.Logger _log;
        
//        private readonly ResourcesService _billing;

//        /// <summary>
//        /// ������� ��� ������� ����� �������������� ������.
//        /// </summary>
//        /// <returns></returns>
//        private delegate bool CanUnLockServiceCondition(ResourcesConfiguration configuration);

//        private CheckPaymentsServiceProcessor(ResourcesService resourcesService) {            
//            _billing = resourcesService;
//            _log = LogManager.GetCurrentClassLogger();
//        }

//        private void UnlockService(Cloud42ServiceBase service, CanUnLockServiceCondition canUnLockServiceCondition) {

//            var configuration = service.GetResourcesConfiguration();
//            if ( configuration.FrozenValue == false ) {
//                _log.Warn("������� ����������� ��� �������������� ������ {0} �������� '{1}'", (object) service.CloudServiceType, (object) service.AccountId);
//                return;
//            }

//            if (configuration.Cost == 0m || canUnLockServiceCondition(configuration))
//            {
//                _log.Trace("��������������� ������� {0}. Id �������� {1}", configuration.Service, service.AccountId);

//                try
//                {
//                    var expireDate = DateTime.Now.AddMonths( 1 );
//                    _billing.UpdateResourceConfig( service.AccountId, configuration.Service, configuration.Cost,
//                        configuration.CostIsFixed.HasValue && configuration.CostIsFixed.Value,
//                        expireDate, false, configuration.DiscountGroup );
//                    service.UnLock(new UnLockInServicefoModel { ExpireDate = expireDate });
//                    _log.Info("������ {0} �������������. Id �������� {1}. �������� {2}�.", configuration.Service,
//                            service.AccountId, configuration.Cost);                    
//                }
//                catch (Exception ex)
//                {
//                    _log.Error("������ ��� ��������������� ������� {0}. Id �������� {1}. �������� ������: {2}", configuration.Service, service.AccountId, ex.Message);
//                }
//            }
//        }

//        private void BaseCheck( Cloud42ServiceBase service ) {            
//            UnlockService(service,
//                (configuration) =>
//                {

//                    if (configuration.Cost > _billing.GetAccount(service.AccountId).Balance)
//                        return false;

//                    var res = service.MakePayment( service.CloudServiceType, "��������������� �������",
//                        configuration.Cost, PaymentSystem.ControlPanel, PaymentType.Outflow, "core-scheduler");

//                    if (res == PaymentOperationResult.Ok)
//                    {
//                        //_log.Info("������ �������.");
//                        return true;
//                    }

//                   _log.Error("������ ���������� ������� ��� ������ {0} , ������� : '{1}', ������: '{2}'", res, service.AccountId, service.CloudServiceType);

//                    return false;                   
//                }                
//                );
//        }

//        public void Visit(MyDisk42CloudService service)
//        {                       
//            UnlockService( service,

//                (configuration) =>
//                {

//                    if (_billing.GetAccount(service.AccountId).Balance < 0)
//                        return false;

//                    var myDiskInfo = Cloud42ServiceHelper.GetMyDiskInfo(service.AccountId);
//                    //���� ����� ������ ������ ��� �������� ������ ������������ ������.
//                    if (myDiskInfo.AvailableSize < myDiskInfo.UsedSizeOnDisk)
//                    {
//                        _log.Info("� �������� {0} ��� ���������� ����� �� �����", service.AccountId);
//                        return false;
//                    }

//                    var res = service.MakePayment(service.CloudServiceType, "��������������� �������",
//                       configuration.Cost, PaymentSystem.ControlPanel, PaymentType.Outflow, "core-scheduler", true);

//                    if (res == PaymentOperationResult.Ok)
//                    {
//                        //_log.Info("������ �������.");
//                        return true;
//                    }

//                    _log.Error("������ ���������� ������� ��� ������ {0} , ������� : '{1}', ������: '{2}'", (object) res, (object) service.AccountId, (object) service.CloudServiceType);
//                    return false;
//                }
//                );
//        }

//        public void Visit(MyEnterprise42CloudService service) {
//            BaseCheck( service );
//        }

//        public void Visit(MyOffice42CloudService service)
//        {
//            BaseCheck(service);
//        }

//        public void Visit(Crm42CloudService service)
//        {
//            BaseCheck(service);
//        }

//        public void Visit(MyStaffCloudService service)
//        {
//            BaseCheck(service);
//        }

//        public void Visit(MyTaxExpertCloud42Service service)
//        {
//            BaseCheck(service);
//        }

//        private void InternalProcess(Cloud42ServiceBase serviceBase)
//        {
//            serviceBase.AcceptVisitor(this);
//        }


//        public static void Process( Cloud42ServiceBase serviceBase )
//        {
//            new CheckPaymentsServiceProcessor().InternalProcess(serviceBase);
//        }

//    }
//}