﻿using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Billing;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Locales.Extensions;

namespace Clouds42.CloudServices.Processors
{
    /// <summary>
    /// Обработчик уведомлений блокировки демо сервиса.
    /// </summary>
    public class LockDemoServiceNotificationProcessor(
        ILetterNotificationProcessor letterNotificationProcessor,
        ICloudLocalizer cloudLocalizer,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : ILockDemoServiceNotifacotionProcessor
    {
        private readonly Lazy<string> _routeForOpenBalancePage = new(CloudConfigurationProvider.Cp.GetRouteForOpenBalancePage);

        /// <summary>
        /// Обработать уведомление о блокировке сервиса.
        /// </summary>
        /// <param name="resourcesConfiguration"></param>
        public void Process(ResourcesConfiguration resourcesConfiguration) =>
            letterNotificationProcessor
                .TryNotify<ServiceDemoPeriodExpiredLetterNotification, ServiceDemoPeriodExpiredLetterModelDto>(
                    new ServiceDemoPeriodExpiredLetterModelDto
                    {
                        AccountId = resourcesConfiguration.AccountId,
                        ServiceId = resourcesConfiguration.BillingServiceId,
                        ServiceName =
                            resourcesConfiguration.BillingService?.GetNameBasedOnLocalization(cloudLocalizer, resourcesConfiguration
                                .AccountId),
                        BalancePageUrl =
                            new Uri(
                                    $"{localesConfigurationDataProvider.GetCpSiteUrlForAccount(resourcesConfiguration.AccountId)}/{_routeForOpenBalancePage.Value}")
                                .AbsoluteUri
                    });
    }
}
