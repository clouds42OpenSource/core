﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.CloudServices
{
    /// <summary>
    /// Класс для управления сервисом "Мои информационные базы"
    /// </summary>
    public class MyDatabases42CloudService(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        BillingServiceDataProvider billingServiceDataProvider,
        IResourcesService resourcesService,
        IHandlerException handlerException,
        IConfiguration configuration,
        INotificationProcessor notificationProcessor,
        ICreatePaymentHelper createPaymentHelper,
        ICheckAccountDataProvider checkAccountDataProvider,
        IChargeFreeMyDatabasesResourcesProvider chargeFreeMyDatabasesResourcesProvider,
        IMyDatabasesServiceCostByLocaleCustomizerFactory myDatabasesServiceCostByLocaleCustomizerFactory)
        : Cloud42ServiceBase(accessProvider, dbLayer, configuration, notificationProcessor, createPaymentHelper,
            checkAccountDataProvider), IMyDatabases42CloudService
    {
        private readonly IConfiguration _configuration = configuration;

        /// <summary>
        /// Установка параметра идентификатора аккаунта
        /// </summary>
        /// <param name="accountId">идентификатор аккаунта</param>
        /// <returns></returns>
        public IMyDatabases42CloudService SetAccountId(Guid accountId)
        {
            AccountId = accountId;
            return this;
        }
        /// <summary>
        /// Тип системного сервиса
        /// </summary>
        public override Clouds42Service CloudServiceType => Clouds42Service.MyDatabases;

        /// <summary>
        /// Действие для выполнения до блокировки сервиса
        /// </summary>
        protected override void BeforeLock()
        {
            Logger.Trace($"Скорая блокировка сервиса “{CloudServiceType.Description()}”, компании {AccountId}");
        }

        /// <summary>
        /// Действие блокировки сервиса
        /// </summary>
        public override void Lock()
        {
            Logger.Trace($"Блокировка сервиса “{CloudServiceType.Description()}”, компании {AccountId}");
        }

        /// <summary>
        /// Действие разблокировки сервиса
        /// </summary>
        public override void UnLock()
        {
            Logger.Trace($"Разблокировка сервиса “{CloudServiceType.Description()}”, компании {AccountId}");
        }

        /// <summary>
        /// Активировать сервис "Мои информационные базы"
        /// </summary>
        public override void ActivateService()
        {
            var message = $"Активация сервиса “{CloudServiceType.Description()}”";

            var billingService = billingServiceDataProvider.GetSystemService(Clouds42Service.MyDatabases);
            var resourceConfig = resourcesService.GetResourceConfig(AccountId, billingService);
            var locale = DbLayer.AccountsRepository.GetLocale(AccountId, Guid.Parse(_configuration["DefaultLocale"] ?? ""));

            if (resourceConfig != null)
            {
                Logger.Warn(
                    $"Найден ресурс {Clouds42Service.MyDatabases} у аккаунта : {AccountId} активация не нужна");
                return;
            }

            Logger.Trace($"Добавление нового ресурса {billingService.Name} для аккаунта {AccountId}.");

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                resourcesService.CreateNewResourcesConfigurationForDependentService(AccountId, billingService);
                chargeFreeMyDatabasesResourcesProvider.ChargeFreeResourcesForAccount(AccountId);
                myDatabasesServiceCostByLocaleCustomizerFactory.CreateByLocale(locale.Name).Customize(AccountId, true);

                dbScope.Commit();
                Logger.Trace($"{message} завершилась успешно");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка активации сервиса 'мои инф базы']");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Принять поситителя
        /// </summary>
        /// <param name="visitor">Посититель</param>
        public override void AcceptVisitor(ISystemCloudServiceProcessorVisitor visitor)
        {
            visitor.Visit(this);
        }


    }
}
