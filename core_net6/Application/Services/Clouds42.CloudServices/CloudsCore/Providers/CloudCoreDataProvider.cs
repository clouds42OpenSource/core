﻿using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsCore.Providers
{
    /// <summary>
    /// Провайдер для работы с данными по настройке облака
    /// </summary>
    public class CloudCoreDataProvider(IUnitOfWork dbLayer) : ICloudCoreDataProvider
    {
        /// <summary>
        /// Получить тип платежного агрегатора для русской локали
        /// </summary>
        /// <returns>Платежный агрегатор русской локали</returns>
        public RussianLocalePaymentAggregatorEnum GetRussianLocalePaymentAggregator() =>
            dbLayer.CloudCoreRepository.FirstOrDefault()?.RussianLocalePaymentAggregator ??
            default;

        /// <summary>
        /// Получить дефолтные конфигурации
        /// </summary>
        /// <returns>Обьект дефолтных конфигураций</returns>
        public CloudCoreDefConfigurationDto GetDefaultConfiguration()
        {
            var cloudCore = dbLayer.CloudCoreRepository.FirstOrDefault();

            if (cloudCore == null)
                return null;

            var defSegment = dbLayer
                .CloudServicesSegmentRepository
                .FirstOrDefault(x => x.IsDefault);

            var segments = dbLayer.CloudServicesSegmentRepository
                .Select(s => new CloudCoreSegmentConfigDto
                {
                    DefaultSegmentName = s.Name,
                    SegmentId = s.ID
                })
                .OrderBy(segment => segment.DefaultSegmentName)
                .ToList();

            return new CloudCoreDefConfigurationDto
            {
                DefaultSegment = new CloudCoreSegmentConfigDto
                {
                    DefaultSegmentName = defSegment?.Name ?? "...",
                    SegmentId = defSegment?.ID ?? Guid.Empty
                },
                HoursDifferenceOfUkraineAndMoscow = cloudCore.HourseDifferenceOfUkraineAndMoscow ?? 0,
                Segments = segments,
                MainPageNotification = cloudCore.MainPageNotification,
                RussianLocalePaymentAggregator = cloudCore.RussianLocalePaymentAggregator
            };
        }

        /// <summary>
        /// Получить настройки облака
        /// </summary>
        /// <returns>Обьект таблицы для настройки облака</returns>
        public CloudCore GetCloudCore(out bool needInsert)
        {
            var cloudCore = dbLayer.CloudCoreRepository.FirstOrDefault();
            needInsert = false;

            if (cloudCore != null) return cloudCore;

            cloudCore = new CloudCore { PrimaryKey = Guid.NewGuid() };
            needInsert = true;

            return cloudCore;
        }
    }
}
