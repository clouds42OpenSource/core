﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsCore.Providers
{
    /// <summary>
    /// Провайдер для редактирования настройки облака
    /// </summary>
    public class EditCloudCoreProvider(
        IUnitOfWork dbLayer,
        ICloudCoreDataProvider cloudCoreDataProvider)
        : IEditCloudCoreProvider
    {
        /// <summary>
        /// Для редактирования контента страницы уведомления
        /// </summary>
        /// <param name="mainPageNotification">Отредактированный контент страницы уведомления</param>
        public void EditMainPageNotification(string mainPageNotification)
        {
            var cloudCore = cloudCoreDataProvider.GetCloudCore(out bool needInsert);
            cloudCore.MainPageNotification = mainPageNotification;

            if (needInsert)
                dbLayer.CloudCoreRepository.Insert(cloudCore);
            else
                dbLayer.CloudCoreRepository.Update(cloudCore);

            dbLayer.Save();
        }

        /// <summary>
        /// Для редактирования дефолтного сегмента
        /// </summary>
        /// <param name="defaultSegmentId">ID дефолтного сегмента</param>
        public void EditDefaultSegment(Guid defaultSegmentId)
        {
            var oldDefault = dbLayer.CloudServicesSegmentRepository.FirstOrDefault(x => x.IsDefault);

            if (oldDefault != null)
                oldDefault.IsDefault = false;

            var newDefault = dbLayer.CloudServicesSegmentRepository.FirstOrDefault(x => x.ID == defaultSegmentId);

            if (newDefault != null)
                newDefault.IsDefault = true;

            dbLayer.CloudServicesSegmentRepository.Update(oldDefault);
            dbLayer.CloudServicesSegmentRepository.Update(newDefault);
            dbLayer.Save();
        }

        /// <summary>
        /// Для редактирования разницы часов между Украиной и Москвой
        /// </summary>
        /// <param name="hoursDifferenceOfUkraineAndMoscow">Разница часов между Украиной и Москвой</param>
        public void EditHoursDifferenceOfUkraineAndMoscow(int hoursDifferenceOfUkraineAndMoscow)
        {
            var cloudCore = cloudCoreDataProvider.GetCloudCore(out bool needInsert);
            cloudCore.HourseDifferenceOfUkraineAndMoscow = hoursDifferenceOfUkraineAndMoscow;

            if (needInsert)
                dbLayer.CloudCoreRepository.Insert(cloudCore);
            else
                dbLayer.CloudCoreRepository.Update(cloudCore);

            dbLayer.Save();
        }

        /// <summary>
        /// Для переключения между Юкассой и Робокассой
        /// </summary>
        /// <param name="aggregator">Вид аггрегатора для переключения</param>
        public void EditRussianLocalePaymentAggregator(RussianLocalePaymentAggregatorEnum aggregator)
        {
            var cloudCore = cloudCoreDataProvider.GetCloudCore(out bool needInsert);
            cloudCore.RussianLocalePaymentAggregator = aggregator;

            if (needInsert)
                dbLayer.CloudCoreRepository.Insert(cloudCore);
            else
                dbLayer.CloudCoreRepository.Update(cloudCore);

            dbLayer.Save();
        }
    }
}