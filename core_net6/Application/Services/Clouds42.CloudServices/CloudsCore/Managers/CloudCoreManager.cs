﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.HandlerExeption.Contract;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.CloudsCore.Managers
{
    /// <summary>
    /// Менеджер настройки облака
    /// </summary>
    public class CloudCoreManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICloudCoreDataProvider cloudCoreDataProvider,
        IEditCloudCoreProvider editCloudCoreProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получение дефолтных конфигураций настройки облака
        /// </summary>
        /// <returns>Обьект дефолтных конфигураций</returns>
        public ManagerResult<CloudCoreDefConfigurationDto> GetDefaultConfiguration()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                var defaultConfiguration = cloudCoreDataProvider.GetDefaultConfiguration();

                return Ok(defaultConfiguration);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения дефолтных конфигураций]");
                return PreconditionFailed<CloudCoreDefConfigurationDto>(ex.Message);
            }
        }

        /// <summary>
        /// Для редактирования контента страницы уведомления
        /// </summary>
        /// <param name="mainPageNotification">Отредактированный контент страницы уведомления</param>
        /// <returns>Возвращает буулевское выражение True успех, false ошибка</returns>
        public ManagerResult<bool> EditMainPageNotification(string mainPageNotification)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                editCloudCoreProvider.EditMainPageNotification(mainPageNotification);

                return Ok(true);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка редактирования главной страницы уведомления]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Для редактирования дефолтного сегмента
        /// </summary>
        /// <param name="defaultSegmentId">ID дефолтного сегмента</param>
        /// <returns>Возвращает буулевское выражение True успех, false ошибка</returns>
        public ManagerResult<bool> EditDefaultSegment(Guid defaultSegmentId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                editCloudCoreProvider.EditDefaultSegment(defaultSegmentId);

                return Ok(true);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка редактирования дефолтного сегмента ID:{defaultSegmentId}]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Для редактирования разницы часов между Украиной и Москвой
        /// </summary>
        /// <param name="hoursDifferenceOfUkraineAndMoscow">Разница часов между Украиной и Москвой</param>
        /// <returns>Возвращает буулевское выражение True успех, false ошибка</returns>
        public ManagerResult<bool> EditHoursDifferenceOfUkraineAndMoscow(int hoursDifferenceOfUkraineAndMoscow)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                editCloudCoreProvider.EditHoursDifferenceOfUkraineAndMoscow(hoursDifferenceOfUkraineAndMoscow);

                return Ok(true);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка редактирования разницы часов между Украиной и Москвой]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Для переключения между Юкассой и Робокассой
        /// </summary>
        /// <param name="aggregator">Вид аггрегатора для переключения</param>
        /// <returns>Возвращает буулевское выражение True успех, false ошибка</returns>
        public ManagerResult<bool> EditRussianLocalePaymentAggregator(RussianLocalePaymentAggregatorEnum aggregator)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                editCloudCoreProvider.EditRussianLocalePaymentAggregator(aggregator);

                return Ok(true);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка редактирования вида аггрегатора между Юкассой и Робокассой]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }
    }
}
