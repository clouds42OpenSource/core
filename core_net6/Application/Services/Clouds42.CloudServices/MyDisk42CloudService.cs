﻿using System.Text;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.Helpers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.MyDisk;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;
using Microsoft.Extensions.Configuration;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates;
using Microsoft.EntityFrameworkCore;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices
{
    /// <summary>
    /// Класс для управления сервисом "Мой диск"
    /// </summary>
    public class MyDisk42CloudService(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICloud42ServiceHelper cloud42ServiceHelper,
        IProvidedServiceHelper providedServiceHelper,
        IPromisePaymentProvider promisePaymentProvider,
        IBillingPaymentsDataProvider billingPaymentsDataProvider,
        IIncreaseAgentPaymentProcessor increaseAgentPaymentProcessor,
        IResourcesConfigurationDataProvider resourceConfigurationDataProvider,
        IResourcesService resourcesService,
        IConfiguration configuration,
        INotificationProcessor notificationProcessor,
        ICreatePaymentHelper createPaymentHelper,
        ICheckAccountDataProvider checkAccountDataProvider,
        MailNotificationFactory mailNotificationFactory)
        : Cloud42ServiceBase(accessProvider,
            dbLayer,
            configuration,
            notificationProcessor,
            createPaymentHelper,
            checkAccountDataProvider), IMyDisk42CloudService
    {
        private readonly int _freeMyDiskTariffSize = CloudConfigurationProvider.BillingServices.MyDisk.GetMyDiskFreeTariffSize();

        /// <summary>
        /// Установка параметра идентификатора аккаунта
        /// </summary>
        /// <param name="accountId">идентификатор аккаунта</param>
        /// <returns></returns>
        public IMyDisk42CloudService SetAccountId(Guid accountId)
        {
            AccountId = accountId;
            return this;
        }

        public override bool CanProlong(out string? reason)
        {
            var myDiskInfo = cloud42ServiceHelper.GetMyDiskInfo(AccountId);

            if (myDiskInfo.UsedSizeOnDisk >= myDiskInfo.AvailableSize)
            {
                var diskInfo =
                    $"Доступно: {myDiskInfo.AvailableSize.MegaByteToClassicFormat(2)}. Используется: {myDiskInfo.UsedSizeOnDisk.MegaByteToClassicFormat(2)}.";

                reason = $"Диск полностью заполнен. {diskInfo}";
                return false;
            }

            reason = null;
            return true;
        }

        public TryChangeTariffResultDto TryChangeTariff(int size)
        {
            var sizeMb = size * 1024;
            var myDiskInfo = cloud42ServiceHelper.GetMyDiskInfo(AccountId);
            var expireDate = resourceConfigurationDataProvider.GetExpireDateOrThrowException(ResourceConfiguration);

            if (myDiskInfo.UsedSizeOnDisk > sizeMb)
            {
                return new TryChangeTariffResultDto
                {
                    Complete = false,
                    Error = $"Вами используется: '{myDiskInfo.UsedSizeOnDisk / 1024} Гб'. Пожалуйста освободите место."
                };
            }

            var accountInfo = DbLayer.AccountsRepository.FirstOrDefault(c => c.Id == AccountId);
            if (accountInfo == null)
                return new TryChangeTariffResultDto
                {
                    Complete = false,
                    Error = "Не удалось получить информацию о компании. Обратитесь в техподдержку."
                };

            var maxAvailableResource = DbLayer.DiskResourceRepository.OrderByDescending(r => r.VolumeGb)
                .FirstOrDefault(r => r.Resource.AccountId == AccountId);

            //ежемесячный платеж
            var monthlyCost = cloud42ServiceHelper.CalculateActualCostOfMyDiskService(size, accountInfo);


            //стоимость покупки.
            var costOfPay = monthlyCost;
            int freeMaxAvailableSize = 0;
            if (maxAvailableResource != null)
            {
                costOfPay = Math.Max(0, costOfPay - maxAvailableResource.Resource.Cost);
                freeMaxAvailableSize = maxAvailableResource.VolumeGb;
            }

            //Стоимость покупки до конца месяца
            var partialCost = decimal.Round(
                BillingServicePartialCostCalculator.CalculateUntilExpireDateWithoutRound(costOfPay, expireDate),
                0,
                MidpointRounding.AwayFromZero);

            if (freeMaxAvailableSize >= size || partialCost <= 0)
            {
                SetTariffPlan(size, monthlyCost);

                ResourceConfiguration.Cost = monthlyCost;
                DbLayer.ResourceConfigurationRepository.Update(ResourceConfiguration);
                DbLayer.Save();

                LogEvent(() => accountInfo.Id, LogActions.DickRateEdit,
                    $"Тариф диска изменен c {myDiskInfo.AvailableSize / 1024} Гб на {size} Гб.");

                if (accountInfo.AccountConfiguration.IsVip)
                    mailNotificationFactory.SendMail<MailNotifyVipChangedMyDiskSize, VipChangedMyDiskSizeMailDto>(new VipChangedMyDiskSizeMailDto
                    {
                        AccountIndex = accountInfo.IndexNumber,
                        AccountName = accountInfo.AccountCaption,
                        OldSize = Convert.ToInt32(myDiskInfo.AvailableSize / 1024),
                        NewSize = size
                    });

                return new TryChangeTariffResultDto
                {
                    Complete = true
                };
            }

            if (partialCost <= accountInfo.BillingAccount.GetAvailableBalance())
            {
                return new TryChangeTariffResultDto
                {
                    Complete = false,
                    EnoughMoney = 0,
                    CostOftariff = partialCost,
                    MonthlyCost = monthlyCost
                };
            }


            return new TryChangeTariffResultDto
            {
                Complete = false,
                CanGetPromisePayment = billingPaymentsDataProvider.CanGetPromisePayment(AccountId),
                CostOftariff = partialCost,
                EnoughMoney = partialCost - accountInfo.BillingAccount.GetAvailableBalance(),
                MonthlyCost = monthlyCost
            };
        }

        public bool SetTariffByPromisePayment(int size)
        {
            var changeTariff = TryChangeTariff(size);
            if (!changeTariff.CanGetPromisePayment)
            {
                return false;
            }

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                promisePaymentProvider.CreatePromisePayment(
                    AccountId,
                    new CalculationOfInvoiceRequestModel
                        {SuggestedPayment = new SuggestedPaymentModelDto {PaymentSum = changeTariff.EnoughMoney}},
                    prolongServices: false);

                TryPayDisk(size, changeTariff);

                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }

            return true;
        }

        /// <summary>
        /// Купить диск.
        /// </summary>
        /// <param name="size">покупаемый объем.</param>
        /// <param name="changeTariff">Состояние клиентского кошелька.</param>
        private bool TryPayDisk(int size, TryChangeTariffResultDto changeTariff)
        {
            var paymentResult = MakePayment(changeTariff.CostOftariff);

            if (paymentResult.OperationStatus != PaymentOperationResult.Ok || !paymentResult.PaymentIds.Any())
                return false;

            SetTariffPlan(size, changeTariff.MonthlyCost);

            ResourceConfiguration.Cost = changeTariff.MonthlyCost;
            DbLayer.ResourceConfigurationRepository.Update(ResourceConfiguration);
            DbLayer.Save();

            if (changeTariff.CostOftariff > 0)
            {
                increaseAgentPaymentProcessor.Process(paymentResult.PaymentIds);
                providedServiceHelper.RegisterPayDisk(AccountId, ResourceConfiguration, changeTariff.MonthlyCost);
            }

            var accountInfo = DbLayer.AccountsRepository.AsQueryable()
                .Include(acc => acc.AccountConfiguration)
                .FirstOrDefault(acc => acc.Id == AccountId);

            if (accountInfo?.AccountConfiguration?.IsVip == true)
                mailNotificationFactory.SendMail<MailNotifyVipChangedMyDiskSize, VipChangedMyDiskSizeMailDto>(new VipChangedMyDiskSizeMailDto
                {
                    AccountIndex = accountInfo.IndexNumber,
                    AccountName = accountInfo.AccountCaption,
                    OldSize = 0,
                    NewSize = size
                });

            return true;
        }

        /// <summary>
        /// Переход на новый тариф "Мой диск"
        /// </summary>
        /// <param name="size">Новый обьём памяти в гигабайтах</param>
        /// <returns></returns>
        public bool PayTariff(int size)
        {
            var oldTariffDigit = cloud42ServiceHelper.GetPayedSpaceOnDisk(AccountId) / 1024;

            if (oldTariffDigit == 0)
            {
                oldTariffDigit = _freeMyDiskTariffSize;
            }

            var changeTariff = TryChangeTariff(size);
            var costOfNewTariff = changeTariff.CostOftariff;

            var accountInfo = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == AccountId);
            if (accountInfo == null)
            {
                Logger.Warn($"Не удалось получить информацию по аккаунту '{AccountId}'");
            }

            var currentCurrency = accountInfo?.AccountConfiguration?.Locale?.Currency;

            if (currentCurrency == null)
            {
                Logger.Warn($"Не удалось получить валюту локали по аккаунту '{AccountId}'");
            }

            if (changeTariff.Complete)
            {
                return true;
            }

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                if (TryPayDisk(size, changeTariff))
                {
                    Logger.Info(
                        $"Переход на тариф 'Мой Диск {size} Гб'. Оплачено {costOfNewTariff}. Аккаунт '{AccountId}'");

                    LogEvent(() => AccountId, LogActions.BuyingDiskSpace,
                        $"Конфигурация сервиса \"Мой Диск\" изменена. Изменен размер диска с {oldTariffDigit} Гб, установлено {size} Гб. Оплачено {costOfNewTariff} {currentCurrency}.");
                }

                dbScope.Commit();

                return true;
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Управление сервисом администратором облака 
        /// </summary>
        /// <param name="model">Модель управления сервисом "Мой диск"</param>
        public void ManagedMyDisk(ServiceMyDiskManagerDto model)
        {
            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == AccountId);
            var diskResource = GetDiskResource();

            if (model.Tariff < _freeMyDiskTariffSize)
                model.Tariff = _freeMyDiskTariffSize;

            var monthlyCost = GetMonthlyCost(model, account);

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var changes = GetLogsAfterManagedService(model, diskResource);

                ResourceConfiguration.DiscountGroup = model.DiscountGroup;
                ResourceConfiguration.Cost = monthlyCost;

                DbLayer.ResourceConfigurationRepository.Update(ResourceConfiguration);
                DbLayer.Save();

                SetTariffPlan(model.Tariff, monthlyCost);

                LogEventHelper.LogEvent(DbLayer, account.Id, AccessProvider, LogActions.EditServiceCost,
                    $"Конфигурация сервисa \"Мой диск\" изменена. {changes}");

                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, "[Ошибка управления сервисом администратором облака]");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Получить месячную стоимость сервиса
        /// </summary>
        /// <param name="model">Модель управления сервисом "Мой диск"</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Месячную стоимость сервиса</returns>
        private decimal GetMonthlyCost(ServiceMyDiskManagerDto model, Account account)
        {
            if (model is { AccountIsVip: true, Cost: not null })
                return model.Cost.Value;

            return cloud42ServiceHelper.CalculateActualCostOfMyDiskService(model.Tariff, account);
        }

        /// <summary>
        /// Получить ресурс диска
        /// </summary>
        /// <returns>Ресурс диска</returns>
        private DiskResource GetDiskResource() => DbLayer.DiskResourceRepository.FirstOrDefault(r =>
            r.Resource.AccountId == AccountId && r.Resource.Subject == AccountId);

        /// <summary>
        /// Получить лог изменений после модификации сервиса.
        /// </summary>
        /// <param name="model">Модель с изменеными аттрибутами сервиса.</param>
        /// <param name="beforeChangeResource">Ресурс до изменений.</param>
        /// <returns></returns>
        private StringBuilder GetLogsAfterManagedService(ServiceMyDiskManagerDto model,
            DiskResource beforeChangeResource)
        {
            var changes = new StringBuilder();

            if (model is { AccountIsVip: true, Cost: not null } && model.Cost.Value != ResourceConfiguration.Cost)
                changes.Append($"Вип клиент. Старая стоимость {ResourceConfiguration.Cost} , установленная {model.Cost.Value}.");

            if (model.DiscountGroup != ResourceConfiguration.DiscountGroup)
                changes.Append(
                    $"Старая дисконт группа {ResourceConfiguration.DiscountGroup ?? 0} , установленная {model.DiscountGroup ?? 0}. ");

            if (beforeChangeResource != null && beforeChangeResource.VolumeGb != model.Tariff)
                changes.Append(
                    $"Изменен размер диска с  {beforeChangeResource.VolumeGb} , установленно {model.Tariff}. ");

            return changes;
        }


        /// <summary>
        /// Совершить платеж  покупки доступа для сервиса "Мой диск"
        /// </summary>
        /// <param name="partialCost">Необходимая сумма для активации сервиса</param>
        /// <returns>Результат проведения платежа</returns>
        private CreatePaymentResultDto MakePayment(decimal partialCost)
        {
            var paymentDescription = "Покупка доступa для сервиса \"Мой диск\"";

            var result = CreatePaymentHelper.MakePayment(ResourceConfiguration, paymentDescription, partialCost,
                PaymentSystem.ControlPanel,
                PaymentType.Outflow, "core-cp", true);

            Logger.Info(@"Платеж на сумму '{0}' для  ""{1}"" был создан: {2} ", partialCost, paymentDescription,
                result.OperationStatus);

            return result;
        }

        private void SetTariffPlan(int diskSize, decimal cost)
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var resources = DbLayer.DiskResourceRepository.Where(r => r.Resource.AccountId == AccountId)
                    .ToList();

                foreach (var diskResource in resources)
                {
                    diskResource.Resource.Subject = null;
                    DbLayer.ResourceRepository.Update(diskResource.Resource);
                }

                DbLayer.Save();

                var dstResource = resources.FirstOrDefault(r => r.VolumeGb == diskSize);
                if (dstResource == null)
                {
                    var serviceType =
                        new BillingServiceDataProvider(DbLayer).GetSystemServiceTypeOrThrowException(ResourceType
                            .DiskSpace);
                    var newResource = new Resource
                    {
                        Id = Guid.NewGuid(),
                        AccountId = AccountId,
                        Cost = cost,
                        Subject = AccountId,
                        BillingServiceTypeId = serviceType.Id
                    };

                    DbLayer.ResourceRepository.Insert(newResource);
                    dstResource = new DiskResource
                    {
                        VolumeGb = diskSize,
                        ResourceId = newResource.Id
                    };

                    DbLayer.DiskResourceRepository.Insert(dstResource);
                }
                else
                {
                    dstResource.Resource.Subject = AccountId;
                    dstResource.Resource.Cost = cost;
                    DbLayer.ResourceRepository.Update(dstResource.Resource);
                }

                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }

        protected override void BeforeLock()
        {
        }

        public sealed override Clouds42Service CloudServiceType
        {
            get { return Clouds42Service.MyDisk; }
        }

        /// <summary></summary>
        public override void Lock()
        {
            Logger.Trace("Блокировка сервиса МойДиск, компании {0}", AccountId);
        }


        /// <summary></summary>
        public override void UnLock()
        {
            Logger.Trace("Разблокировка сервиса МойДиск, компании {0}", AccountId);
        }

        /// <summary>
        /// Активировать сервис
        /// </summary>
        public override void ActivateService()
        {
            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyDisk);
            var myEntConfig = resourcesService.GetResourceConfig(AccountId, billingService);

            if (myEntConfig != null)
                return;

            Logger.Trace($"Добавление нового ресурса {billingService.Name} для аккаунта {AccountId}.");

            resourcesService.CreateNewResourcesConfigurationForDependentService(AccountId, billingService);
        }

        /// <summary>
        /// Принять поситителя
        /// </summary>
        /// <param name="visitor">Посититель</param>
        public override void AcceptVisitor(ISystemCloudServiceProcessorVisitor visitor)
        {
            visitor.Visit(this);
        }

    }
}
