﻿using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using Quartz;

namespace Clouds42.CloudServices
{
    /// <summary>
    /// Провайдер для работы с CloudService
    /// </summary>
    public sealed class CloudServiceProvider : ICloudServiceProvider
    {
        /// <summary>
        /// Признак того, что пользователь удалён
        /// </summary>
        private const string AddedUserStatus = "Added";
        
        /// <summary>
        /// Признак того, что почта не проверена
        /// </summary>
        private const string UncheckedEmailStatus = "Unchecked";

        /// <summary>
        /// Контекст базы
        /// </summary>
        private readonly IUnitOfWork _dbLayer;
        
        /// <summary>
        /// Провайдер шифрования
        /// </summary>
        private readonly IEncryptionProvider _encryptionProvider;
        private readonly IHandlerException _handlerException;
        private readonly AesEncryptionProvider _aesEncryptionProvider;
        private readonly IOpenIdProvider _openIdProvider;
        private readonly ICreateInflowPaymentHelper _createInflowPaymentHelper;
        private readonly IApplyDemoServiceSubscribeProcessor _applyDemoServiceSubscribeProcessor;
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger42 _logger;

        /// <summary>
        /// Конструктор класса <see cref="CloudServiceProvider"/>
        /// </summary>
        /// <param name="dbLayer">Контекст базы</param>
        /// <param name="encryptionProvider"></param>
        /// <param name="aesEncryptionProvider"></param>
        /// <param name="handlerException"></param>
        /// <param name="openIdProvider"></param>
        public CloudServiceProvider(IUnitOfWork dbLayer, 
            DesEncryptionProvider encryptionProvider, 
            AesEncryptionProvider aesEncryptionProvider,
            IApplyDemoServiceSubscribeProcessor applyDemoServiceSubscribeProcessor,
            ILogger42 logger, IHandlerException handlerException, IOpenIdProvider openIdProvider, ICreateInflowPaymentHelper createInflowPaymentHelper)
        {
            _dbLayer = dbLayer;
            _encryptionProvider = encryptionProvider;
            _aesEncryptionProvider = aesEncryptionProvider;
            _logger = logger;
            _handlerException = handlerException;
            _openIdProvider = openIdProvider;
            _createInflowPaymentHelper = createInflowPaymentHelper;
            _applyDemoServiceSubscribeProcessor = applyDemoServiceSubscribeProcessor;
        }

        /// <summary>
        /// Обновить данные в таблице CloudService
        /// </summary>
        /// <param name="model">Модель для обновления</param>
        public void UpdateCloudService(CloudServiceDto model)
        {
            var cloudService = _dbLayer.CloudServiceRepository.GetCloudService(model.CloudServiceId);

            if (cloudService == null)
            {
                _logger.Warn($"Запись с id:{model.CloudServiceId} не найдена");
                throw new NotFoundException($"Запись с id:{model.CloudServiceId} не найдена");
            }

            if (!string.IsNullOrEmpty(model.ServiceCaption))
            {
                cloudService.ServiceCaption = model.ServiceCaption;
            }

            try
            {
                _dbLayer.CloudServiceRepository.Update(cloudService);
                _dbLayer.Save();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка сохранения изменений в CloudService]");
                throw;
            }
        }

        /// <summary>
        /// Добавить новую запись в таблице CloudService
        /// </summary>
        /// <param name="model">Модель для добавления</param>
        /// <returns>Модель ответа добавленного CloudService</returns>
        public AddCloudServiceResponseDto AddCloudService(CloudServiceDto model)
        {
            var foundCloudService = _dbLayer.CloudServiceRepository.GetCloudService(model.CloudServiceId);

            if (foundCloudService != null)
            {
                throw new ObjectAlreadyExistsException($"Служба с ID:{model.CloudServiceId} уже зарегестрирована.");
            }


            using var tran = _dbLayer.SmartTransaction.Get();
            try
            {
                var accountUserId = AddAccountUserForService(model);

                var addedCloudServiceResult = AddNewCloudService(model, accountUserId);

                tran.Commit();

                return addedCloudServiceResult;
            }
            catch (Exception ex)
            {
                tran.Rollback();
 
                _handlerException.Handle(ex,
                    "[Ошибка добавления записи в таблицу CloudService]");
                throw;
            }
        }


        /// <summary>
        /// Подтверждение подписки для интеграции с Cloud
        /// </summary>
        /// <param name="model">Модель для добавления</param>
        /// <param name="login"></param>
        /// <returns></returns>
        public void SubscriptionConfirmationForCloud(CloudServiceIntegrationDto model, string login)
        {
            ValidateCurrentUserRole(login);
            var user = GetUser(model.Email);

            try
            {
                _logger.Info($"Подтверждаем окончание демо периода всем включенным сервисам");
                var listService = _dbLayer.ResourceConfigurationRepository.Where(conf => conf.AccountId == user.AccountId && conf.IsDemoPeriod).ToList();
                foreach ( var service in listService)
                {
                    _logger.Info($"Принятие демо периода для сервиса {service.BillingService.Name}");
                     _applyDemoServiceSubscribeProcessor.Apply(service.BillingService.Id, user.AccountId, false);
                }
                _logger.Info($"Пользователь идентифицирован с логином {user.Login} и почтой {model.Email}, начисляем бонусные средства за подтверждение подписки");
                var newPayment = new PaymentDefinitionDto
                {
                    Account = user.AccountId,
                    Date = DateTime.Now,
                    OriginDetails = "cloud-cp",
                    Id = Guid.NewGuid(),
                    OperationType = PaymentType.Inflow,
                    System = PaymentSystem.Admin,
                    Status = PaymentStatus.Done,
                    Description = "Начисление бонусных средств за подтверждение подписки",
                    Total = 5000,
                    TransactionType = TransactionType.Bonus
                };

                var paymentResult = _createInflowPaymentHelper.CreatePaymentAndProcessInflowPayment(newPayment);
                if (paymentResult != PaymentOperationResult.Ok)
                    throw new InvalidOperationException($"Ошибка внесения бонусных средств для пользователя {model.Email}");

                _logger.Info($"Бонусные средства пользователю {user.Login} успешно зачислены ");
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка начисление бонусных средств за подтверждение подписки: {ex.Message}]");
                throw;
            }
        }

        private void ValidateCurrentUserRole(string login)
        {
            var userRequest = _dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == login) ?? throw new NotFoundException($"Недостаточно прав для получения информации.");

            if (userRequest.AccountUserRoles.All(r => r.AccountUserGroup != AccountUserGroup.AccountSaleManager))
            {
                throw new NotFoundException($"Недостаточно прав для получения информации.");
            }
        }

        private AccountUser GetUser(string email)
        {
            var user = _dbLayer.AccountUsersRepository.AsQueryableNoTracking().Include(v => v.Account).FirstOrDefault(u =>
                u.Email == email && u.CorpUserSyncStatus != "Deleted" && u.CorpUserSyncStatus != "SyncDeleted") 
                ?? throw new NotFoundException($"Пользователь с такой почтой {email} не зарегестрирован.");

            if (user.AccountUserRoles.All(r => r.AccountUserGroup != AccountUserGroup.AccountAdmin))
            {
                throw new NotFoundException($"Пользователь с почтой {user.Email} не является администратором аккаунта.");
            }

            if (user.Account.UserSource != "Cloud.ru")
            {
                throw new NotFoundException($"Пользователь с почтой {user.Email} не является партнером Cloud.");
            }

            return user;
        }

        /// <summary>
        /// Получить информацио о сервисе для Cloud
        /// </summary>
        /// <returns></returns>
        public CloudServiceInfoDto GetServiceInfoForCloud(string email, string login)
        {
            ValidateCurrentUserRole(login);

            var user = GetUser(email);

            var serviceInfo = new List<ServiceInfoDto>();
            
            try
            {
                _logger.Info($"Получаем список всех сервисов");
                var listService = _dbLayer.ResourceConfigurationRepository.Where(res =>
                res.AccountId == user.AccountId &&
                res.Frozen == false && res.BillingService.SystemService != Clouds42Service.Recognition && res.BillingService.SystemService != Clouds42Service.Esdl).ToList();

                serviceInfo.AddRange(listService.Select(service => new ServiceInfoDto
                {
                    Cost = service.Cost,
                    ExpireDate = service.ExpireDate,
                    Frozen = service.FrozenValue,
                    IsDemoPeriod = service.IsDemoPeriod,
                    ServiceName = service.BillingService.Name
                }));
                _logger.Info($"Получаем баланс");
                var balance = _dbLayer.BillingAccountRepository.FirstOrDefault(b=>b.Id == user.AccountId);
                _logger.Info($"Всю информацию получили для пользоватля с почтой {email}");

                return new CloudServiceInfoDto
                { 
                    Balance = balance.Balance + balance.BonusBalance,
                    Services = serviceInfo
                };
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка получения данных о сервисе: {ex.Message}]");
                throw;
            }
        }

        /// <summary>
        /// Добавить новую службу
        /// </summary>
        /// <param name="model">Модель для добавления новой службы</param>
        /// <param name="accountUserId">С каким пользователем связать службу</param>
        /// <returns>Модель ответа добавленного CloudService</returns>
        private AddCloudServiceResponseDto AddNewCloudService(CloudServiceDto model, Guid accountUserId)
        {
            var cloudService = new CloudService
            {
                Id = Guid.NewGuid(),
                ServiceCaption = model.ServiceCaption,
                CloudServiceId = model.CloudServiceId,
                ServiceToken = Guid.NewGuid(),
                AccountUserId = accountUserId
            };

            _dbLayer.CloudServiceRepository.InsertCloudService(cloudService);

            return new AddCloudServiceResponseDto
            {
                Id = cloudService.Id
            };
        }

        /// <summary>
        /// Добавить нового пользователя для службы
        /// </summary>
        /// <param name="model">Модель для добавления новой службы</param>
        /// <returns>ID нового пользователя</returns>
        private Guid AddAccountUserForService(CloudServiceDto model)
        {
            var newUserPwd = Guid.NewGuid().ToString("N");
            var accountId = CloudConfigurationProvider.Common.SystemAccount.GetSystemAccountId();
            var accountUser = new AccountUser
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Login = $"{model.CloudServiceId}_{Guid.NewGuid().ToString("D").Split(new []{'-'}, StringSplitOptions.RemoveEmptyEntries)[4]}",
                FirstName = model.ServiceCaption,
                LastName = string.Empty,
                MiddleName = string.Empty,
                Password = _encryptionProvider.Encrypt(newUserPwd),
                PasswordHash = _aesEncryptionProvider.Encrypt(newUserPwd),
                Email = $"{model.CloudServiceId}@temporary.com",
                Activated = true,
                CreatedInAd = false,
                CorpUserSyncStatus = AddedUserStatus,
                CreationDate = DateTime.Now,
                EditDate = DateTime.Now,
                EmailStatus = UncheckedEmailStatus
            };

            var accountUserRole = new AccountUserRole
            {
                Id = Guid.NewGuid(),
                AccountUserId = accountUser.Id,
                AccountUserGroup = AccountUserGroup.Cloud42Service
            };

            _dbLayer.AccountUsersRepository.InsertAccountUser(accountUser);
            _dbLayer.AccountUserRoleRepository.Insert(accountUserRole);

            _openIdProvider.AddUser(new SauriOpenIdControlUserModel
            { 
                AccountID = accountId, 
                AccountUserID = accountUser.Id, 
                Email = accountUser.Email, 
                FullName = accountUser.FullName, 
                Login = accountUser.Login, 
                Name = accountUser.Name, 
                Password = newUserPwd, 
                Phone = accountUser.PhoneNumber
            }, "ru-ru");

            return accountUser.Id;
        }
    }
}
