﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.MainServiceResourcesChangesHistory.Managers
{
    /// <summary>
    /// Менеджер для работы с изменениями ресурсов главного сервиса(Аренда 1С)
    /// </summary>
    public class MainServiceResourcesChangesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        INotifyAboutMainServiceResourcesChangesProvider notifyAboutMainServiceResourcesChangesProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Уведомить об об изменении ресурсов главного сервиса (Аредна 1С)
        /// </summary>
        /// <param name="changesDate">Дата изменений</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult NotifyAboutMainServiceResourcesChanges(DateTime changesDate)
        {
            AccessProvider.HasAccess(ObjectAction.AboutMainServiceResourcesChanges_Notify,
                () => AccessProvider.ContextAccountId);

            try
            {
                notifyAboutMainServiceResourcesChangesProvider.Notify(changesDate);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    "[Ошибка уведомления об об изменении ресурсов главного сервиса (Аредна 1С)]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
