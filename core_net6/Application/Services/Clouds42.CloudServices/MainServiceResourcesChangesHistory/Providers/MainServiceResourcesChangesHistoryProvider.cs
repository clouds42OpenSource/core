﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.MainServiceResourcesChangesHistory.Providers
{
    /// <summary>
    /// Провайдер для работы с историей изменений ресурсов главного сервиса(Аренда 1С)
    /// </summary>
    internal class MainServiceResourcesChangesHistoryProvider(IUnitOfWork dbLayer)
        : IMainServiceResourcesChangesHistoryProvider
    {
        /// <summary>
        /// Получить историю изменений ресурсов главного сервиса
        /// для Аккаунта, по дате изменений
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changesDate">Дата изменений</param>
        /// <returns>История изменений ресурсов главного сервиса(Аренда 1С) для аккаунта</returns>
        public Domain.DataModels.History.MainServiceResourcesChangesHistory GetMainServiceResourcesChangesHistory(Guid accountId,
            DateTime changesDate) => dbLayer.GetGenericRepository<Domain.DataModels.History.MainServiceResourcesChangesHistory>()
            .FirstOrDefault(history => history.AccountId == accountId && history.ChangesDate == changesDate.Date);
    }
}