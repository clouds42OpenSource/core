﻿using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.MainServiceResourcesChangesHistory.Providers
{
    /// <summary>
    /// Провайдер для фиксирования истории изменений ресурсов главного сервиса(Аренда1С)
    /// </summary>
    internal class CommitMainServiceResourcesChangesHistoryProvider(
        IMainServiceResourcesChangesHistoryProvider mainServiceResourcesChangesHistoryProvider,
        IUnitOfWork dbLayer,
        ICreateMainServiceResourcesChangesHistoryProvider createMainServiceResourcesChangesHistoryProvider)
        : ICommitMainServiceResourcesChangesHistoryProvider
    {
        /// <summary>
        /// Зафиксировать историю изменений ресурсов главного сервиса(Аренда1С)
        /// </summary>
        /// <param name="model">Модель для фиксирования истории изменений ресурсов</param>
        public void Commit(CommitMainServiceResourcesChangesHistoryDto model)
        {
            var mainServiceResourcesChangesHistory =
                mainServiceResourcesChangesHistoryProvider.GetMainServiceResourcesChangesHistory(model.AccountId,
                    DateTime.Now);

            if (mainServiceResourcesChangesHistory == null)
            {
                createMainServiceResourcesChangesHistoryProvider.Create(model);
                return;
            }

            mainServiceResourcesChangesHistory.EnabledResourcesCount += model.EnabledResourcesCount;
            mainServiceResourcesChangesHistory.DisabledResourcesCount += model.DisabledResourcesCount;

            dbLayer.GetGenericRepository<Domain.DataModels.History.MainServiceResourcesChangesHistory>()
                .Update(mainServiceResourcesChangesHistory);
            dbLayer.Save();
        }
    }
}