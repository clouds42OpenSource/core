﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.MainServiceResourcesChangesHistory.Providers
{
    /// <summary>
    /// Провайдер для уведомления об об изменении ресурсов главного сервиса (Аредна 1С)
    /// у вип аккаунтов
    /// </summary>
    internal class NotifyAboutMainServiceResourcesChangesProvider(
        IUnitOfWork dbLayer,
        ILetterNotificationProcessor letterNotificationProcessor)
        : INotifyAboutMainServiceResourcesChangesProvider
    {
        private readonly Lazy<Guid> _systemAccountId = new(CloudConfigurationProvider.Common.SystemAccount.GetSystemAccountId);
        private readonly Lazy<string> _vipSupportEmail = new(CloudConfigurationProvider.Emails.GetVipSupportEmail);

        /// <summary>
        /// Уведомить об об изменении ресурсов главного сервиса (Аредна 1С)
        /// у вип аккаунтов
        /// </summary>
        /// <param name="changesDate">Дата изменений</param>
        public void Notify(DateTime changesDate)
        {
            foreach (var letterModel in GetMainServiceResourcesChangesLetterModels(changesDate))
                letterNotificationProcessor
                    .TryNotify<MainServiceResourcesChangesLetterNotification, MainServiceResourcesChangesLetterModelDto>(
                        letterModel);
        }

        /// <summary>
        /// Получить модели для писем об изменении ресурсов
        /// главного сервиса (Аренда1С)
        /// </summary>
        /// <param name="changesDate">Дата изменений</param>
        /// <returns>Модели для писем об изменении ресурсов
        /// главного сервиса (Аренда1С)</returns>
        private IEnumerable<MainServiceResourcesChangesLetterModelDto> GetMainServiceResourcesChangesLetterModels(
            DateTime changesDate)
        {
            var mainServiceResourcesChanges = GetMainServiceResourcesChanges(changesDate).ToList();

            if (!mainServiceResourcesChanges.Any())
                yield break;

            var dateNextDay = changesDate.AddDays(1);

            yield return GenerateMainServiceResourcesChangesLetterModel(changesDate, dateNextDay,
                _vipSupportEmail.Value, mainServiceResourcesChanges);

            var groupedDataBySaleManagers = GroupedDataBySaleManager(mainServiceResourcesChanges);

            foreach (var groupedDataBySaleManager in groupedDataBySaleManagers)
                yield return GenerateMainServiceResourcesChangesLetterModel(changesDate, dateNextDay,
                    groupedDataBySaleManager.Key, groupedDataBySaleManager);
        }

        /// <summary>
        /// Сгруппировать данные изменений ресурсов главного сервиса по сейлс менеджеру
        /// </summary>
        /// <param name="mainServiceResourcesChanges">Изменения ресурсов главного сервиса (Аренда1С)</param>
        /// <returns>Сгруппированные данные изменений ресурсов главного сервиса</returns>
        private IEnumerable<IGrouping<string, MainServiceResourcesChangeDto>> GroupedDataBySaleManager(
            IEnumerable<MainServiceResourcesChangeDto> mainServiceResourcesChanges) => mainServiceResourcesChanges
            .Where(change => !change.SaleManagerEmail.IsNullOrEmpty())
            .GroupBy(change => change.SaleManagerEmail);

        /// <summary>
        /// Сформировать модель письма
        /// об изменениях ресурсов главного сервиса (Аренда1С)
        /// </summary>
        /// <param name="changesDateFrom">Дата изменений с</param>
        /// <param name="changesDateTo">Дата изменений по</param>
        /// <param name="receiverEmail">Электронный адрес получателя</param>
        /// <param name="mainServiceResourcesChanges">Изменения ресурсов главного сервиса (Аренда1С)</param>
        /// <returns>Модель письма  об изменениях ресурсов главного сервиса (Аренда1С)</returns>
        private MainServiceResourcesChangesLetterModelDto GenerateMainServiceResourcesChangesLetterModel(
            DateTime changesDateFrom, DateTime changesDateTo, string receiverEmail,
            IEnumerable<MainServiceResourcesChangeDto> mainServiceResourcesChanges) =>
            new()
            {
                AccountId = _systemAccountId.Value,
                ChangesDateFrom = changesDateFrom,
                ChangesDateTo = changesDateTo,
                ReceiverEmail = receiverEmail,
                MainServiceResourcesChanges = mainServiceResourcesChanges
            };

        /// <summary>
        /// Получить изменения ресурсов главного сервиса (Аренда1С)
        /// </summary>
        /// <param name="changesDate">Дата изменений</param>
        /// <returns>Изменения ресурсов главного сервиса (Аренда1С)</returns>
        private IEnumerable<MainServiceResourcesChangeDto> GetMainServiceResourcesChanges(DateTime changesDate) =>
            (from history in dbLayer.GetGenericRepository<Domain.DataModels.History.MainServiceResourcesChangesHistory>()
                    .WhereLazy(history => history.ChangesDate == changesDate.Date)
             join accountSaleManager in dbLayer.GetGenericRepository<AccountSaleManager>().WhereLazy() on
                 history.AccountId equals accountSaleManager.AccountId into saleManager
             from accountSaleManager in saleManager.Take(1).DefaultIfEmpty()
             select new MainServiceResourcesChangeDto
             {
                 AccountNumber = history.Account.IndexNumber,
                 AccountCaption = history.Account.AccountCaption,
                 SaleManagerLogin = accountSaleManager.AccountUser.Login,
                 SaleManagerEmail = accountSaleManager.AccountUser.Email,
                 DisabledResourcesCount = history.DisabledResourcesCount,
                 EnabledResourcesCount = history.EnabledResourcesCount
             }).AsEnumerable();
    }
}