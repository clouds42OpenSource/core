﻿using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.MainServiceResourcesChangesHistory.Providers
{
    /// <summary>
    /// Провайдер для создания истории изменений ресурсов главного сервиса(Аренда1С)
    /// для аккаунта
    /// </summary>
    internal class CreateMainServiceResourcesChangesHistoryProvider(IUnitOfWork dbLayer)
        : ICreateMainServiceResourcesChangesHistoryProvider
    {
        /// <summary>
        /// Создать историю изменений ресурсов главного сервиса(Аренда1С)
        /// </summary>
        /// <param name="model">Модель для фиксирования истории изменений ресурсов</param>
        public void Create(CommitMainServiceResourcesChangesHistoryDto model)
        {
            var mainServiceResourcesChangesHistory = new Domain.DataModels.History.MainServiceResourcesChangesHistory
            {
                AccountId = model.AccountId,
                EnabledResourcesCount = model.EnabledResourcesCount,
                DisabledResourcesCount = model.DisabledResourcesCount,
                ChangesDate = DateTime.Now.Date
            };

            dbLayer.GetGenericRepository<Domain.DataModels.History.MainServiceResourcesChangesHistory>()
                .Insert(mainServiceResourcesChangesHistory);
            dbLayer.Save();
        }
    }
}
