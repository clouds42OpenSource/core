﻿using System.Reflection;
using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.CloudServices.AccountCsResourceValues.Providers;
using Clouds42.CloudServices.CloudsCore.Managers;
using Clouds42.CloudServices.CloudsCore.Providers;
using Clouds42.CloudServices.CloudsFiles.Providers;
using Clouds42.CloudServices.CloudsTerminalServers.Managers;
using Clouds42.CloudServices.CloudsTerminalServers.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.CloudServices.Fasta.Helpers;
using Clouds42.CloudServices.Fasta.Managers;
using Clouds42.CloudServices.Fasta.Providers;
using Clouds42.CloudServices.Fasta.Validators;
using Clouds42.CloudServices.Helpers;
using Clouds42.CloudServices.MainServiceResourcesChangesHistory.Providers;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.CloudServices.MyDisk.Providers;
using Clouds42.CloudServices.Processors;
using Clouds42.CloudServices.Providers;
using Clouds42.CloudServices.RedirectPublishAccountDatabase;
using Clouds42.CloudServices.Rent1C;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.CloudServices.Rent1C.Providers;
using Clouds42.CloudServices.Tardis;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CloudServices
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddCloudServices(this IServiceCollection services)
        {
            var assembly = Assembly.GetExecutingAssembly();
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(assembly));
            services.AddAutoMapper(assembly);

            services.AddTransient<ICloudCoreDataProvider, CloudCoreDataProvider>();
            services.AddTransient<IEditCloudCoreProvider, EditCloudCoreProvider>();
            services.AddTransient<ICloudFileProvider, CloudFileProvider>();
            services.AddTransient<ICloudTerminalServerSessionsProvider, CloudTerminalServerSessionsProvider>();
            services.AddTransient<ICreateCloudTerminalServerProvider, CreateCloudTerminalServerProvider>();
            services.AddTransient<ICloudTerminalServerProvider, CloudTerminalServerProvider>();
            services.AddTransient<IEditCloudTerminalServerProvider, EditCloudTerminalServerProvider>();
            services.AddTransient<IDeleteCloudTerminalServerProvider, DeleteCloudTerminalServerProvider>();
            services.AddTransient<ITerminate1CProcessesForDbInTerminalServersProvider, Terminate1CProcessesForDbInTerminalServersProvider>();
            services.AddTransient<IFastaServiceProvider, FastaServiceProvider>();
            services.AddTransient<IChangeFastaDemoPeriodProvider, ChangeFastaDemoPeriodProvider>();
            services.AddTransient<ICommitMainServiceResourcesChangesHistoryProvider, CommitMainServiceResourcesChangesHistoryProvider>();
            services.AddTransient<IMainServiceResourcesChangesHistoryProvider, MainServiceResourcesChangesHistoryProvider>();
            services.AddTransient<ICreateMainServiceResourcesChangesHistoryProvider, CreateMainServiceResourcesChangesHistoryProvider>();
            services.AddTransient<INotifyAboutMainServiceResourcesChangesProvider, NotifyAboutMainServiceResourcesChangesProvider>();
            services.AddTransient<IRecalculateMyDiskUsedSizeProvider, RecalculateMyDiskUsedSizeProvider>();
            services.AddTransient<IMyDiskAnalyzeProvider, MyDiskAnalyzeProvider>();
            services.AddTransient<IRent1CClientDataProvider, Rent1CClientDataProvider>();
            services.AddTransient<IRent1CDataManagementDataProvider, Rent1CDataManagementDataProvider>();
            services.AddTransient<IConfigurateRent1CAccessesDataProvider, ConfigurateRent1CAccessesDataProvider>();
            services.AddTransient<ITardisSynchronizerHelper, TardisSynchronizerHelper>();
            services.AddTransient<ICloudServiceAdapter, CloudServiceAdapter>();
            services.AddTransient<IRedirectPublishAccountDatabase, RedirectPublishAccountDatabaseProvided>();
            services.AddTransient<IRent1CConfigurationAccessProvider, Rent1CConfigurationAccessProvider>();
            services.AddTransient<ICloudServiceProvider, CloudServiceProvider>();
            services.AddTransient<IMyEnterprise42CloudService,MyEnterprise42CloudService>();
            services.AddTransient<IMyDisk42CloudService, MyDisk42CloudService> ();
            services.AddTransient<IMyDatabases42CloudService, MyDatabases42CloudService>();
            services.AddTransient<ICloudServiceLockUnlockProvider, CloudServiceLockUnlockProvider>();
            services.AddTransient<IFlowResourcesScopeDataProvider, FlowResourcesScopeDataProvider>();
            services.AddTransient<IIncreaseDecreaseResourceProvider, IncreaseDecreaseResourceProvider>();
            services.AddTransient<IDocLoaderByRent1CProlongationProcessor, DocLoaderByRent1CProlongationProcessor>();
            services.AddTransient<IAllowIncreaseDocLoaderByStandardRent1CChecker, AllowIncreaseDocLoaderByStandardRent1CChecker>();
            services.AddTransient<IRent1CSponsorAccountProvider, Rent1CSponsorAccountProvider>();
            services.AddTransient<IEsdlCloud42Service, EsdlCloud42Service>();
            services.AddTransient<ICloud42ServiceHelper, Cloud42ServiceHelper>();
            services.AddTransient<IRent1CManageHelper, Rent1CManageHelper>();
            services.AddTransient<IRent1CServiceInfoDmProcessor, Rent1CServiceInfoDmProcessor>();
            services.AddTransient<IRecalculateMyDiskUsedSizeManager, RecalculateMyDiskUsedSizeManager>();
            services.AddTransient<ICloudServiceProcessor, CloudServiceProcessor>();
            services.AddTransient<ICloud42ServiceFactory, Cloud42ServiceFactory>();
            services.AddTransient<ITardisSynchronizer, TardisSynchronizer>();
            services.AddTransient<IServiceStatusHelper, ServiceStatusHelper>();
            services.AddTransient<IPartialAmountForApplyDemoServiceSubsribesCalculator, PartialAmountForApplyDemoServiceSubsribesCalculator>();
            services.AddTransient<IDemoPeriodEndedPaymentRequiredNotifacotionProcessor, DemoPeriodEndedPaymentRequiredNotificationProcessor>();
            services.AddTransient<IPromisedPaymentProcessor, PromisedPaymentProcessor>();
            services.AddTransient<IDemoPeriodIsComingToEndNotifacotionProcessor, DemoPeriodIsComingToEndNotificationProcessor>();
            services.AddTransient<ILockDemoServiceNotifacotionProcessor, LockDemoServiceNotificationProcessor>();
            services.AddTransient<IRecognition42CloudService, Recognition42CloudService>();
            services.AddTransient<IRent1CManageHelper, Rent1CManageHelper>();
            services.AddTransient<UpdateAccountDatabaseAccessTrigger>();
            services.AddTransient<BillingServiceDataProvider>();
            services.AddTransient<IAccountCsResourceValuesDataManager, AccountCsResourceValuesDataManager>();
            services.AddTransient<IAccountCsResourceValuesManager, AccountCsResourceValuesManager>();
            services.AddTransient<FastaServiceHelper>();
            services.AddTransient<Rent1CAccountUserProvider>();
            services.AddTransient<Rent1CRateHelper>();
            services.AddTransient<ICloudServiceManager, CloudServiceManager>();
            services.AddTransient<CloudTerminalServerSessionsManager>();
            services.AddTransient<CloudCoreManager>();
            services.AddTransient<CloudTerminalServerManager>();
            services.AddTransient<FastaServiceManager>();
            services.AddTransient<ResourcesManager>();
            services.AddTransient<Rent1CManager>();
            services.AddTransient<ManageFastaDemoPeriodDtoValidator>();
            services.AddTransient<Rent1CConfigurationAccessManager>();
            services.AddTransient<UpdateAccountDatabaseTrigger>();
            services.AddTransient<MyDiskManager>();
            services.AddTransient<Rent1CDataManagementDataManager>();
            services.AddTransient<CreateDatabaseOnDelimitersTrigger>();
            services.AddTransient<IRent1CClientDataManager, Rent1CClientDataManager>();
            services.AddTransient<UpdateAccountUserTrigger>();
            services.AddTransient<DeleteAccountUserTrigger>();
            services.AddTransient<AccountCsResourceValuesDataManager>();

            return services;
        }
    }
}
