﻿using System.Configuration;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.CloudServices
{
    /// <summary>
    /// Провайдер для получения данных по сервисам биллинга
    /// </summary>
    public class BillingServiceDataProvider(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить данные о системном сервисе.
        /// </summary>
        /// <param name="systemServiceType">Тип системного сервиса.</param>        
        public IBillingService GetSystemService(Clouds42Service systemServiceType)
        {
            var billingService =
                dbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == systemServiceType) ?? throw new ConfigurationErrorsException($"В справочнике сервисы биллинга, не сконфигурирован сервис '{systemServiceType.GetDisplayName()}'."); ;
            
            return billingService;
        }

        /// <summary>
        /// Получить данные о сервисе.
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса.</param>        
        public IBillingService GetBillingServiceOrThrowException(Guid serviceId)
        {
            var billingService =
                dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId);

            if (billingService == null)
                throw new NotFoundException($"В справочнике сервисы биллинга, не найден сервис по идентификатору'{serviceId}'.");

            return billingService;
        }

        /// <summary>
        /// Получить данные о системной услуги сервиса.
        /// </summary>
        /// <param name="resourceType">Тип услуги.</param>
        /// <returns></returns>
        public IBillingServiceType GetSystemServiceTypeOrThrowException(ResourceType resourceType)
        {
            var billingServiceType =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(s => s.SystemServiceType == resourceType);

            if (billingServiceType == null)
                throw new ConfigurationErrorsException($"В справочнике услуги сервиса биллинга, не сконфигурированна услуга '{resourceType.GetDisplayName()}'.");

            return billingServiceType;
        }

        /// <summary>
        /// Получить данные о системной услуги сервиса.
        /// </summary>
        /// <param name="billingServiceTypeId">ID услуги.</param>
        /// <returns></returns>
        public IBillingServiceType GetSystemServiceTypeOrThrowException(Guid billingServiceTypeId)
        {
            var billingServiceType =
                dbLayer.BillingServiceTypeRepository.FirstOrDefault(s => s.Id == billingServiceTypeId);

            if (billingServiceType == null)
                throw new ConfigurationErrorsException($"В справочнике услуги сервиса биллинга, не сконфигурированна услуга '{billingServiceTypeId}'.");

            return billingServiceType;
        }

        /// <summary>
        /// Получить данные о сервисе.
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса.</param>
        /// <returns>Модель сервиса биллига</returns> 
        public IBillingService GetBillingService(Guid serviceId) => dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId);
        
    }
}
