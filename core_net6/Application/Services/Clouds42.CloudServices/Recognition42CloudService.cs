﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.AccountCsResourceValues.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.Extensions.Configuration;

namespace Clouds42.CloudServices
{
    public class Recognition42CloudService : Cloud42ServiceBase, IRecognition42CloudService
    {
        private readonly Lazy<Guid> _recognition42ServiceId;
        private readonly ILogger42 _logger;
        private readonly IIncreaseDecreaseResourceProvider _increaseDecreaseResourceProvider;
        private readonly FlowResourcesScopeDataProvider _flowResourcesScopeDataProvider;
        private readonly IEsdlCloud42Service _esdlCloud42Service;
        private readonly IResourcesService _resourcesService;
        private readonly IProvidedServiceHelper _providedServiceHelper;
        private readonly IRateProvider _rateProvider;
        public Recognition42CloudService(
            IResourcesService resourcesService,
            IRateProvider rateProvider,
            IAccessProvider accessProvider,
            IUnitOfWork dbLayer,
            ILogger42 logger,
            IIncreaseDecreaseResourceProvider increaseDecreaseResourceProvider,
            IConfiguration configuration,
            INotificationProcessor notificationProcessor,
            ICreatePaymentHelper createPaymentHelper,
            ICheckAccountDataProvider checkAccountDataProvider,
            IProvidedServiceHelper providedServiceHelper,
            IEsdlCloud42Service esdlCloud42Service) : base(accessProvider, dbLayer, configuration, notificationProcessor, createPaymentHelper, checkAccountDataProvider)
        {
            _logger = logger;
            _resourcesService = resourcesService;
            _increaseDecreaseResourceProvider = increaseDecreaseResourceProvider;
            _flowResourcesScopeDataProvider = new FlowResourcesScopeDataProvider(DbLayer);
            _recognition42ServiceId = new Lazy<Guid>(CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId);
            _providedServiceHelper = providedServiceHelper;
            _rateProvider = rateProvider;
            _esdlCloud42Service = esdlCloud42Service;
        }

        public override Clouds42Service CloudServiceType => Clouds42Service.Recognition;

        protected override void BeforeLock()
        {
        }

        public override void Lock()
        {
        }

        public override void UnLock()
        {
        }

        /// <summary>
        /// Установка параметра идентификатора аккаунта
        /// </summary>
        /// <param name="accountId">идентификатор аккаунта</param>
        /// <returns></returns>
        public IRecognition42CloudService SetAccountId(Guid accountId)
        {
            AccountId = accountId;
            _esdlCloud42Service.SetAccountId(accountId);
            return this;
        }

        /// <summary>
        /// Начисляет лицензии на распознование документов(страницы).
        /// </summary>
        public override void ActivateService()
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {

                var licenseCount = ConfigurationHelper.GetConfigurationValue<int>("Recognition42DemoLicenseCount");

                var licenseResource = DbLayer.AccountCsResourceValueRepository.FirstOrDefault(f =>
                    f.AccountId == AccountId &&
                    f.CSResourceId == _recognition42ServiceId.Value);

                if (licenseResource != null)
                    throw new InvalidOperationException($"Попытка активации уже активного сервиса '{ResourceConfiguration.BillingService.Name}' у аккаунта '{AccountId}'.");

                _flowResourcesScopeDataProvider.Insert(AccountId, _recognition42ServiceId.Value);
                _increaseDecreaseResourceProvider.ChangeValue(AccountId, _recognition42ServiceId.Value, licenseCount, "Initial configuration");

                dbScope.Commit();

                _providedServiceHelper.RegisterServiceActivation(AccountId, Clouds42Service.Recognition,
                    null, ResourceType.Rec10, KindOfServiceProvisionEnum.Single);
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, "[Ошибка активации сервиса Рекогнишен]");
                dbScope.Rollback();
                throw;
            }
        }

        public override void AcceptVisitor(ISystemCloudServiceProcessorVisitor visitor)
        {
            visitor.Visit(this);
        }

        public decimal GetCost(int pagesAmount)
        {
            if (pagesAmount == 0)
                return 0m;

            if (!Enum.TryParse($"Rec{pagesAmount}", out ResourceType resType))
            {
                _logger.Warn($"Нету тарифа на заданное количество страниц: {pagesAmount}");
                return 0;
            }

            var serviceType = DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == resType);
            var billingAccount = _resourcesService.GetAccountIfExistsOrCreateNew(AccountId);
            var rate = _rateProvider.GetOptimalRate(billingAccount.Id, serviceType.Id);

            return rate.Cost;
        }

        /// <summary>
        /// Получить стоимость недостающую для покупки сервиса загрузки документов
        /// </summary>
        /// <param name="pagesAmount">количество страниц</param>
        /// <param name="years">срок лицензии</param>
        /// <returns></returns>
        public decimal? GetNeedModeyForBuyServices(int pagesAmount, int years)
        {
            var account = _resourcesService.GetAccountIfExistsOrCreateNew(AccountId);
            _logger.Trace("Начало подсчета стоимости сервиса");
            //подсчет стоимости
            var costPages = GetCost(pagesAmount);
            _logger.Trace($"Cтоимости страниц {costPages}");
            var costYears = _esdlCloud42Service.GetCost(years);
            _logger.Trace($"Cтоимости лицензии {costYears} ");
            var cost = costPages + costYears;
            if (account.GetAvailableBalance() < cost)
            {
                _logger.Debug($"Cтоимости сервиса {cost} больше чем сумма баланса {account.Balance}");
                return cost;
            }
            _logger.Debug($"Cтоимости сервиса {cost}, баланс {account.Balance} покупка сервиса возможна");
            return null;
        }

        /// <summary>
        /// Покупка страниц для сервиса Загрузки данных
        /// </summary>
        /// <param name="pagesAmount">количество страниц</param>
        /// <returns></returns>
        public BuyingEsdlResultModelDto BuyRecognition42(int pagesAmount)
        {
            if (!Enum.TryParse($"Rec{pagesAmount}", out ResourceType resType))
            {
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    Comment = "Нет тарифа на заданное количество страниц."
                };
            }

            var account = _resourcesService.GetAccountIfExistsOrCreateNew(AccountId);
            _logger.Trace("Начало подсчета стоимости сервиса");

            var cost = GetCost(pagesAmount);
            _logger.Trace($"Cтоимости сервиса {cost}");
            if (account.GetAvailableBalance() < cost)
            {
                _logger.Debug($"Cтоимости сервиса {cost} больше чем сумма баланса {account.Balance}");
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    NeedModey = cost,
                    Comment = "Недостаточно денег для покупки сервиса."
                };
            }

            _logger.Trace("Начало создание платежа");
            var res = MakePayment(cost);
            if (res != PaymentOperationResult.Ok)
            {
                _logger.Warn($"Ошибка создания платежа для аккаунта {account.Id}");
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    Comment = "Ошибка проведения платежа. Обратитесь в тех. поддержку."
                };
            }

            AddRecLicense(resType);

            _logger.Trace("Начало обновления ресурса или добавление нового в случае его отсутствия");
            AddOrUpdateResource(cost, resType);

            var resConf =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                    r => r.AccountId == AccountId && r.BillingService.SystemService == CloudServiceType);

            _providedServiceHelper.RegisterSinglePay(AccountId, resConf, resType, cost);

            return new BuyingEsdlResultModelDto
            {
                Complete = true,
                Comment = $"Куплен пакет страниц \"{pagesAmount}\""
            };

        }


        #region Private Methods
        /// <summary>
        /// Обновление существующих или добавление новых ресурсов и регистрация движения средств
        /// </summary>
        /// <param name="cost">стоимость</param>        
        /// <param name="resType">тип ресурса</param>
        private void AddOrUpdateResource(decimal cost, ResourceType resType)
        {

            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.Recognition);
            var resConf = _resourcesService.GetResourceConfig(AccountId, billingService);

            if (resConf != null)
            {
                _logger.Trace($"Обновление ресурса в ResourceConfiguration для аккаунта{AccountId} и сервиса {CloudServiceType.GetDisplayName()}");
                resConf.Cost = cost;
                resConf.CostIsFixed = true;
                resConf.Frozen = false;
                DbLayer.ResourceConfigurationRepository.Update(resConf);
            }
            else
            {
                _logger.Trace($"Добавление ресурса в ResourceConfiguration для аккаунта{AccountId} и сервиса {CloudServiceType.GetDisplayName()}");
                resConf = new ResourcesConfiguration
                {
                    Id = Guid.NewGuid(),
                    AccountId = AccountId,
                    Cost = cost,
                    CostIsFixed = true,
                    ExpireDate = DateTime.Now.AddYears(1),
                    CreateDate = DateTime.Now,
                    BillingServiceId = billingService.Id,
                    Frozen = false
                };
                DbLayer.ResourceConfigurationRepository.Insert(resConf);
            }
            DbLayer.Save();

            var resource =
                DbLayer.ResourceRepository.FirstOrDefault(
                    r => r.AccountId == AccountId && r.BillingServiceType.Service.SystemService == CloudServiceType);

            var serviceType = new BillingServiceDataProvider(DbLayer).GetSystemServiceTypeOrThrowException(resType);

            if (resource != null)
            {
                _logger.Trace($"Обновление ресурса {resType.ToString()} в ResourceConfiguration для аккаунта{AccountId} и сервиса {CloudServiceType.GetDisplayName()}");

                resource.BillingServiceTypeId = serviceType.Id;
                DbLayer.ResourceRepository.Update(resource);
            }
            else
            {
                _logger.Trace($"Добавление ресурса {resType.ToString()} в ResourceConfiguration для аккаунта{AccountId} и сервиса {CloudServiceType.GetDisplayName()}");

                DbLayer.ResourceRepository.Insert(new Resource
                {
                    Id = Guid.NewGuid(),
                    AccountId = AccountId,
                    Cost = 0,
                    BillingServiceTypeId = serviceType.Id,
                    IsFree = false
                });
            }
            DbLayer.Save();
        }

        private void AddRecLicense(ResourceType resourceType)
        {
            var pagesCount = resourceType.GetDataValue();
            _increaseDecreaseResourceProvider.ChangeValue(AccountId, _recognition42ServiceId.Value, pagesCount, $"Покупка {pagesCount} страниц");
        }


        /// <summary>
        /// Совершить платеж для покупки лицензий сервиса "Recognition"
        /// </summary>
        /// <param name="cost">Сумма платежа</param>
        /// <returns>Результат платежной операции</returns>
        private PaymentOperationResult MakePayment(decimal cost)
        {
            var paymentDescription = "Покупка лицензий для сервиса \"Recognition\"";
            var resourceConfiguration = ResourceConfiguration;
            if (resourceConfiguration == null)
            {
                resourceConfiguration = new ResourcesConfiguration
                {
                    AccountId = AccessProvider.ContextAccountId,
                    BillingServiceId = DbLayer.BillingServiceRepository
                        .FirstOrDefault(s => s.SystemService == CloudServiceType).Id
                };
            }

            var result = CreatePaymentHelper.MakePayment(resourceConfiguration, paymentDescription, cost, PaymentSystem.ControlPanel,
                PaymentType.Outflow, "core-cp", true);

            Logger.Info(@"Платеж на сумму '{0}' для  ""{1}"" был создан: {2} ", cost, paymentDescription, result.OperationStatus);
            return result.OperationStatus;
        }
        #endregion


    }
}
