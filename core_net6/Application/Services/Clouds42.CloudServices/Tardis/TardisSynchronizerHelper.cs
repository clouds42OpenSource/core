﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Tardis
{
    /// <summary>
    /// Синхронизатор с Тардис.
    /// </summary>
    public class TardisSynchronizerHelper(
        ITardisSynchronizer tardisSynchronizer,
        IUnitOfWork dbLayer)
        : ITardisSynchronizerHelper
    {
        /// <summary>
        /// Выполнить синхронизацию пользователя с Тардис.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        public void SynchronizeAccountUser<TTrigger, TTriggerModel>(TTriggerModel model)
            where TTrigger : SynchTriggerBase<TTriggerModel>
            where TTriggerModel : class, IAccountUserIdDto
        {
            if (!CheckActivation(model.AccountUserId))
                return;

            tardisSynchronizer.Synchronize<TTrigger, TTriggerModel>(model);
        }

        /// <summary>
        /// Выполнить синхронизацию базы с Тардис.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        public void SynchronizeAccountDatabase<TTrigger, TTriggerModel>(TTriggerModel model)
            where TTrigger : SynchTriggerBase<TTriggerModel>
            where TTriggerModel : class, IAccountDatabaseIdDto
        {
            tardisSynchronizer.Synchronize<TTrigger, TTriggerModel>(model);
        }

        /// <summary>
        /// Выполнить синхронизацию с Тардис при изменение доступа пользователя в информационной базе.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        public void SynchronizeAccountDatabaseAccess<TTrigger, TTriggerModel>(TTriggerModel model)
            where TTrigger : SynchTriggerBase<TTriggerModel>
            where TTriggerModel : class, IAccountDatabaseAccessDto
        {
            tardisSynchronizer.Synchronize<TTrigger, TTriggerModel>(model);
        }

        /// <summary>
        /// Проверить активность услуги сервиса у пользователя.
        /// </summary>
        /// <param name="accountUserId">Идентификато пользователя облака.</param>
        /// <returns>Признак активности услуги сервиса у пользователя.</returns>
        private bool CheckActivation(Guid accountUserId)
        {
            return dbLayer.ResourceRepository.Any(resource =>
                    resource.BillingServiceType.Service.InternalCloudService == InternalCloudServiceEnum.Tardis
                    && resource.Subject == accountUserId);
        }
    }
}
