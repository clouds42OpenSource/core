﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CloudServices.Tardis
{

    /// <summary>
    /// Синхронизатор ядра облака с Тардис.
    /// </summary>
    public class TardisSynchronizer(IServiceProvider serviceProvider) : ITardisSynchronizer
    {
        /// <summary>
        /// Выполнить синхронизацию.
        /// </summary>
        /// <typeparam name="TTrigger">Триггер синхронизации.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели триггера.</typeparam>
        /// <param name="triggerModel">Модель триггера.</param>
        public void Synchronize<TTrigger, TTriggerModel>(TTriggerModel triggerModel)
            where TTrigger : SynchTriggerBase<TTriggerModel> where TTriggerModel : class
        {
            var trigger = serviceProvider.GetRequiredService<TTrigger>();
            trigger.RiseEvent(triggerModel);
        }
    }
}
