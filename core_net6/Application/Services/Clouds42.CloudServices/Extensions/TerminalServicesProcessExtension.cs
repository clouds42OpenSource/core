﻿using System.Management;
using Cassia;
using Clouds42.CloudServices.CloudsTerminalServers.Constants;

namespace Clouds42.CloudServices.Extensions
{
    /// <summary>
    /// Расширение процесса терминального сервера
    /// </summary>
    public static class TerminalServicesProcessExtension
    {
        /// <summary>
        /// Получить коммандную строку процесса терминального сервера
        /// </summary>
        /// <param name="terminalServicesProcesss">Процесс терминального сервера</param>
        /// <param name="terminalServerAddress">Адрес терминального сервера</param>
        /// <returns>Командная строка процесса 1С</returns>
        public static string GetCommandLine(this ITerminalServicesProcess terminalServicesProcesss, string terminalServerAddress)
        {
            if (terminalServicesProcesss.UnderlyingProcess == null)
                return string.Empty;

            var scope = CreateManagementScope(terminalServerAddress);
            scope.Connect();

            var query = CreateObjectQueryForGettingCommandLine(terminalServicesProcesss.UnderlyingProcess.Id);
            var searcher = new ManagementObjectSearcher(scope, query);
            var collection = searcher.Get();

            return collection.Cast<ManagementBaseObject>().SingleOrDefault()?[TerminalServerConst.CommandLine]
                ?.ToString();
        }

        /// <summary>
        /// Создать объект области управления для подключения к терминальному серверу
        /// </summary>
        /// <param name="terminalServerAddress">Адрес терминального сервера</param>
        /// <returns>Объект области управления</returns>
        private static ManagementScope CreateManagementScope(string terminalServerAddress) =>
            new($@"\\{terminalServerAddress}\root\cimv2", new ConnectionOptions
            {
                Authority = TerminalServerConst.Authority
            });

        /// <summary>
        /// Создать объект запроса для получения коммандной строки процесса
        /// </summary>
        /// <param name="processId">Id процесса</param>
        /// <returns>Объект запроса для получения коммандной строки процесса</returns>
        private static ObjectQuery CreateObjectQueryForGettingCommandLine(int processId) =>
            new($"SELECT CommandLine FROM Win32_Process WHERE ProcessId = {processId}");
    }
}
