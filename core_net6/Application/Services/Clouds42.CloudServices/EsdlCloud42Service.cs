﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.Extensions.Configuration;

namespace Clouds42.CloudServices
{
    public class EsdlCloud42Service(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IResourcesService resourcesService,
        IRateProvider rateProvider,
        IConfiguration configuration,
        INotificationProcessor notificationProcessor,
        ICreatePaymentHelper createPaymentHelper,
        ICheckAccountDataProvider checkAccountDataProvider,
        IProvidedServiceHelper providedServiceHelper)
        : Cloud42ServiceBase(accessProvider, dbLayer, configuration, notificationProcessor, createPaymentHelper,
            checkAccountDataProvider), IEsdlCloud42Service
    {
        public override Clouds42Service CloudServiceType => Clouds42Service.Esdl;

        public override bool CanProlong(out string reason)
        {
            reason = "Расчетный период закончен.";
            return false;
        }
        protected override void BeforeLock() { }
        public override void Lock()
        {
            Logger.Trace($"Блокировка сервиса {NomenclaturesNameConstants.Esdl} компании {AccountId}");
        }
        public override void UnLock() { }

        /// <summary>
        /// Установка параметра идентификатора аккаунта
        /// </summary>
        /// <param name="accountId">идентификатор аккаунта</param>
        /// <returns></returns>
        public IEsdlCloud42Service SetAccountId(Guid accountId)
        {
            AccountId = accountId;
            return this;
        }

        /// <summary>
        /// Активирует тестовый период на использование ЗД.
        /// </summary>
        public override void ActivateService()
        {
            try
            {
                var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.Esdl);
                var resConfig = resourcesService.GetResourceConfig(AccountId, billingService);
                if (resConfig == null)
                {
                    resConfig = new ResourcesConfiguration
                    {
                        Id = Guid.NewGuid(),
                        AccountId = AccountId,
                        Cost = 0,
                        ExpireDate = DateTime.Now.AddDays(
                            CloudConfigurationProvider.FastaService.GetDemoDaysCount()),
                        CreateDate = DateTime.Now,
                        BillingServiceId = billingService.Id,
                        Frozen = false
                    };
                    DbLayer.ResourceConfigurationRepository.Insert(resConfig);
                }
                else
                {
                    Logger.Warn($"Esdl42 resourceConfiguration already exist for acc: {AccountId}");
                }

                var resource = DbLayer.ResourceRepository.FirstOrDefault(r =>
                    r.AccountId == AccountId &&
                    r.BillingServiceType.ServiceId == billingService.Id);

                if (resource == null)
                {
                    var serviceType =
                        new BillingServiceDataProvider(DbLayer).GetSystemServiceTypeOrThrowException(ResourceType.Esdl);

                    var res = new Resource
                    {
                        Id = Guid.NewGuid(),
                        AccountId = AccountId,
                        Cost = 0,
                        BillingServiceTypeId = serviceType.Id,
                        IsFree = false
                    };
                    DbLayer.ResourceRepository.Insert(res);
                }
                else
                {
                    Logger.Warn($"Esdl42 resource already exist for acc: {AccountId}");
                }
                DbLayer.Save();

                providedServiceHelper.RegisterServiceActivation(AccountId, Clouds42Service.Esdl,
                    resConfig.ExpireDateValue, ResourceType.Esdl, KindOfServiceProvisionEnum.Single);
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex,
                    $"[Ошибка активации сервиса] {Clouds42Service.Esdl.GetDisplayName()} для аккаунта {AccountId}");
                throw;
            }
        }

        public decimal GetCost(int pagesAmount)
        {
            if (pagesAmount == 0)
                return 0m;

            var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(AccountId);
            var serviceType = DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.Esdl);
            var rate = rateProvider.GetOptimalRate(billingAccount.Id, serviceType.Id);
            return rate.Cost * pagesAmount;
        }

        public BuyingEsdlResultModelDto BuyEsdl(int years)
        {
            var account = resourcesService.GetAccountIfExistsOrCreateNew(AccountId);

            Logger.Trace("Начало подсчета стоимости сервиса");
            var cost = GetCost(years);
            Logger.Trace($"Cтоимости сервиса {cost}");
            if (account.GetAvailableBalance() < cost)
            {
                Logger.Debug($"Cтоимости сервиса {cost} больше чем сумма баланса {account.Balance}");
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    NeedModey = cost,
                    Comment = "Не достаточно денег для покупки сервиса."
                };
            }
            Logger.Trace("Начало создание платежа");
            var res = MakePayment(cost);
            if (res != PaymentOperationResult.Ok)
            {
                Logger.Warn($"Ошибка создания платежа для аккаунта {account.Id}");
                return new BuyingEsdlResultModelDto
                {
                    Complete = false,
                    Comment = "Ошибка проведения платежа. Обратитесь в тех поддержку."
                };
            }

            Logger.Trace("Начало добавления лицензий или создание новой записи с ресурсами");
            AddEsdlLicense(years, cost);

            var resConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == AccountId &&
                rc.BillingService.SystemService == CloudServiceType
            );

            providedServiceHelper.RegisterSinglePay(AccountId, resConfig, ResourceType.Esdl, cost);

            return new BuyingEsdlResultModelDto
            {
                Complete = true,
                Comment = $"Куплена лицензия \"{years} год(ов)\""
            };
        }

        public override void AcceptVisitor(ISystemCloudServiceProcessorVisitor visitor)
        {
            visitor.Visit(this);
        }

        #region Private Methods

        private void AddEsdlLicense(int years, decimal cost)
        {
            var resourceConfiguration = ResourceConfiguration;

            if (resourceConfiguration != null)
            {
                resourceConfiguration.Cost = cost;
                resourceConfiguration.CostIsFixed = true;

                var expireDate = resourceConfiguration.ExpireDate ?? DateTime.Now;
                Logger.Debug($"Обновление данных сервиса: использование сервиса возможно до {expireDate}");
                if (expireDate < DateTime.Now)
                    expireDate = DateTime.Now;

                expireDate = expireDate.AddYears(years);

                resourceConfiguration.ExpireDate = expireDate;
                Logger.Debug($"Установленная дата использование сервиса {expireDate}");
                resourceConfiguration.Frozen = false;

                DbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
                DbLayer.Save();
            }
            else
            {

                var resource = DbLayer.ResourceRepository.FirstOrDefault(r =>
                    r.AccountId == AccountId &&
                    r.BillingServiceType.Service.SystemService == CloudServiceType);
                if (resource == null)
                {
                    var serviceType =
                        new BillingServiceDataProvider(DbLayer).GetSystemServiceTypeOrThrowException(ResourceType.Esdl);

                    Logger.Debug($"Добавление данных о сервисе {ResourceType.Esdl.ToString()} в  ResourceRepository");
                    var res = new Resource
                    {
                        Id = Guid.NewGuid(),
                        AccountId = AccountId,
                        Cost = 0,
                        BillingServiceTypeId = serviceType.Id,
                        IsFree = false
                    };
                    DbLayer.ResourceRepository.Insert(res);
                }

                Logger.Debug($"Добавление данных о сервисе {CloudServiceType.ToString()} в  ResourcesConfiguration");

                var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.Esdl);

                var resConf = new ResourcesConfiguration
                {
                    Id = Guid.NewGuid(),
                    AccountId = AccountId,
                    Cost = cost,
                    CostIsFixed = true,
                    ExpireDate = DateTime.Now.AddYears(years),
                    CreateDate = DateTime.Now,
                    BillingServiceId = billingService.Id,
                    Frozen = false
                };
                DbLayer.ResourceConfigurationRepository.Insert(resConf);
            }

            DbLayer.Save();

        }


        /// <summary>
        /// Совершить платеж для покупки лицензий сервиса "Fasta"
        /// </summary>
        /// <param name="cost">Сумма платежа</param>
        /// <returns>Результат платежной операции</returns>
        private PaymentOperationResult MakePayment(decimal cost)
        {
            var paymentDescription = $"Покупка лицензий для сервиса \"{NomenclaturesNameConstants.Esdl}\"";

            var result = CreatePaymentHelper.MakePayment(ResourceConfiguration, paymentDescription, cost, PaymentSystem.ControlPanel,
                PaymentType.Outflow, "core-cp", true);
            Logger.Info(@"Платеж на сумму '{0}' для  ""{1}"" был создан: {2} ", cost, paymentDescription, result.OperationStatus);
            return result.OperationStatus;
        }

        #endregion
    }
}
