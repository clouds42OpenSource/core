﻿using Clouds42.DataContracts.MyDisk;
using Clouds42.Logger;
using Clouds42.CloudServices.Contracts;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.MyDisk.Providers
{
    /// <summary>
    /// Провайдер выполнения расчета диска.
    /// </summary>
    internal class MyDiskAnalyzeProvider(
        IUnitOfWork dbLayer,
        IAccountDatabaseAnalyzeProvider accountDatabaseAnalyzeProvider,
        IMemorySizeCalculator memorySizeCalculator,
        IUpdateMyDiskPropertiesByAccountProvider updateMyDiskPropertiesByAccountProvider,
        ILogger42 logger)
        : IMyDiskAnalyzeProvider
    {
        /// <summary>
        ///     Пересчет занятого место на диске для клиентских файлов
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void RecalculateClientFilesInMyDisk(Guid accountId)
        {

            var account = dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == accountId);
            if (account == null)
                return;

            var serviceAccounts = dbLayer.ServiceAccountRepository.FirstOrDefault(a => a.Id == accountId);
            if (serviceAccounts != null)
                return;

            var myDiskPropertiesByAccount = dbLayer.GetGenericRepository<MyDiskPropertiesByAccount>()
                .FirstOrDefault(diskProperties => diskProperties.AccountId == accountId);

            logger.Info($"[{accountId}] :: Начинаем пересчет занятого место на диске для клиентских файлов");
            try
            {
                var size = memorySizeCalculator.CalculateClintFilesSize(account, myDiskPropertiesByAccount?.MyDiskFilesSizeInMb);

                logger.Info($"[{accountId}] :: Подсчитан размер клиентских файлов: {size}");

                updateMyDiskPropertiesByAccountProvider.Update(new MyDiskPropertiesByAccountDto
                {
                    AccountId = accountId,
                    MyDiskFilesSizeInMb = size,
                    MyDiskFilesSizeActualDateTime = DateTime.Now
                });
            }
            catch (Exception ex)
            {
                logger.Info($"[{accountId}] :: Ошибка: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Пересчитать используемое место на диске аккаунтом
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public void RecalculateAccountSize(Guid accountId)
        {
            RecalculateAccountDatabasesSize(accountId);
            RecalculateClientFilesInMyDisk(accountId);
        }

        /// <summary>
        /// Проверить выполнена ли задача по пересчету диска
        /// </summary>
        /// <param name="taskId">Id задачи</param>
        /// <returns>Результат проверки</returns>
        public bool CheckMyDiskRecalculationJobFinished(Guid taskId) => dbLayer.CoreWorkerTasksQueueRepository.Any(
            task => task.Id == taskId && (task.Status == CloudTaskQueueStatus.Ready.ToString() ||
                                          task.Status == CloudTaskQueueStatus.Error.ToString()));

        /// <summary>
        /// Пересчитать размер баз аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        private void RecalculateAccountDatabasesSize(Guid accountId)
        {
            var accountDatabases = dbLayer.DatabasesRepository.WhereLazy(db =>
                db.AccountId == accountId && db.State == DatabaseState.Ready.ToString() &&
                db.AccountDatabaseOnDelimiter == null).ToList();

            foreach (var accountDatabase in accountDatabases)
            {
                accountDatabaseAnalyzeProvider.RecalculateSizeOfAccountDatabase(accountDatabase);
            }
        }
    }
}
