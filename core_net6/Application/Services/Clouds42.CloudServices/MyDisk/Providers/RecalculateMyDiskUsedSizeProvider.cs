﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CoreWorker.BillingJobs.MyDiskSizeRecalculate;
using Clouds42.DataContracts.BillingService;

namespace Clouds42.CloudServices.MyDisk.Providers
{
    /// <summary>
    /// Провайдер пересчета занимаемого места на диске
    /// </summary>
    public class RecalculateMyDiskUsedSizeProvider(
        MyDiskUsedSizeRecalculationJobWrapper myDiskUsedSizeRecalculationJobWrapper)
        : IRecalculateMyDiskUsedSizeProvider
    {
        /// <summary>
        /// Запустить задачу пересчета занимаемого места на диске
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void RunTaskToRecalculateAccountSizeAndTryProlong(Guid accountId)
        {
            myDiskUsedSizeRecalculationJobWrapper.Start(new RecalculateAccountMyDiskSizeDto
            {
                AccountId = accountId
            });
        }
    }
}
