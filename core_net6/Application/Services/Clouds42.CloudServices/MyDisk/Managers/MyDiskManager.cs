﻿using Clouds42.BLL.Common;
using Clouds42.DataContracts.MyDisk;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Contracts;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using static Clouds42.Configurations.Configurations.CloudConfigurationProvider;

namespace Clouds42.CloudServices.MyDisk.Managers
{
    /// <summary>
    /// Менеджер для работы с сервисом Мой диск
    /// </summary>
    public class MyDiskManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IServiceUnlocker serviceUnlocker,
        IServiceProvider serviceProvider,
        IResourcesService resourcesService,
        ICloudServiceAdapter cloudServiceAdapter,
        IRecalculateMyDiskUsedSizeProvider recalculateMyDiskUsedSizeProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Пересчитать используемое место на диске аккаунтом
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public ManagerResult<Guid?> RecalculateAccountSizeAndTryProlong(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Resources_RecalculateMyDisk, () => accountId);
                recalculateMyDiskUsedSizeProvider.RunTaskToRecalculateAccountSizeAndTryProlong(accountId);

                return Ok(GetRecalculateTaskId(accountId));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<Guid?>(ex.Message);
            }
        }

        /// <summary>
        /// Get get the task identifier for recalculating the disk space
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        private Guid? GetRecalculateTaskId(Guid accountId)
        {
            var taskId = MyDiskRecalculation.GetCoreWorkerTaskId();
            return DbLayer.CoreWorkerTasksQueueRepository.AsQueryable().
                Where(t => t.CoreWorkerTaskId == taskId
                    && t.Status != CloudTaskQueueStatus.Ready.ToString()
                    && t.Status != CloudTaskQueueStatus.Error.ToString())
                .Join(DbLayer.GetGenericRepository<CoreWorkerTaskParameter>().AsQueryable(),
                    t => t.Id,
                    tp => tp.TaskId,
                    (t, tp) => new { t.Id, tp.TaskParams })
                .AsEnumerable()
                .FirstOrDefault(x => x.TaskParams.Contains(accountId.ToString("D")))?.Id;
        }

        /// <summary>
        /// Try to change "My Disk" service tariff (size) with a payment
        /// </summary>
        /// <param name="accountId">Account identifier for which the rate is being changed</param>
        /// <param name="size">New disk size (GB)</param>
        /// <returns></returns>
        public ManagerResult<TryChangeTariffResultDto> TryChangeMyDiskTariff(Guid accountId, int size)
        {
            AccessProvider.HasAccess(ObjectAction.BillingService_ChangeSubscribe, () => accountId);

            var myDiskResourceConfiguration = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyDisk);
            var oldCostOfMyDisk = myDiskResourceConfiguration.Cost;

            var result = serviceProvider.GetRequiredService<IMyDisk42CloudService>().SetAccountId(accountId).TryChangeTariff(size);

            if (string.IsNullOrEmpty(result.Error))
            {
                var resourceConfiguration = cloudServiceAdapter.GetMainConfigurationOrThrowException(accountId, Clouds42Service.MyDisk);
                serviceUnlocker.ProlongServiceIfTheCostIsLess(accountId, oldCostOfMyDisk,
                    resourceConfiguration);
            }

            return Ok(result);
        }

        /// <summary>
        /// Change "My Disk" tariff (size) with a payment
        /// </summary>
        /// <param name="dto">Parameters model dto</param>
        /// <returns></returns>
        public ManagerResult<bool> ChangeMyDiskTariff(MyDiskChangeTariffDto dto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_ChangeSubscribe, () => dto.AccountId);
                var service = serviceProvider.GetRequiredService<IMyDisk42CloudService>().SetAccountId(dto.AccountId);
                var result = dto.ByPromisedPayment ? service.SetTariffByPromisePayment(dto.SizeGb) : service.PayTariff(dto.SizeGb);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Управление сервисом администратором облака 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель управления сервисом "Мой диск"</param>
        public ManagerResult<bool> ManageMyDisk(Guid accountId, ServiceMyDiskManagerDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_EditResources_MyDisk, () => accountId);
                serviceProvider.GetRequiredService<IMyDisk42CloudService>().SetAccountId(accountId).ManagedMyDisk(model);
                return Ok(true);
            }
            catch (Exception ex) 
            {
                return PreconditionFailed<bool>(ex.Message);
            }
        }
    }
}
