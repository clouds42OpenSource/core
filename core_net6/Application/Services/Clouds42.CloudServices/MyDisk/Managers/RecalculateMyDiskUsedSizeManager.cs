﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.HandlerExeption.Contract;
using Clouds42.CloudServices.Contracts;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.MyDisk.Managers
{
    /// <summary>
    /// Менеджер для пересчета занимаемого на диске аккаунта
    /// </summary>
    public class RecalculateMyDiskUsedSizeManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IMyDiskAnalyzeProvider myDiskAnalyzeProvider,
        IProlongServiceProcessor prolongServiceProcessor,
        ICloudServiceAdapter cloudServiceAdapter,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IRecalculateMyDiskUsedSizeManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Пересчитать занимаемое место на диске аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Разультат перестчета</returns>
        public ManagerResult RecalculateMyDiskUsedSize(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.Resources_RecalculateMyDisk, () => accountId);
            try
            {
                myDiskAnalyzeProvider.RecalculateAccountSize(accountId);
                prolongServiceProcessor.ProlongService(
                    cloudServiceAdapter.GetMainConfigurationOrThrowException(accountId, Clouds42Service.MyDisk),
                    rc => rc.FrozenValue);

                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[При пересчете занимаемого места на диске аккаутна - {accountId} произошла ошибка]";

                _handlerException.Handle(ex, errorMessage);

                return PreconditionFailed(
                    $"{errorMessage} {ex.GetMessage()}");
            }
        }
    }
}
