﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CloudServices
{

    /// <summary>
    /// Фабрика для создания классов по управлению системными сервисами
    /// </summary>
    public class Cloud42ServiceFactory : ICloud42ServiceFactory
    {

        /// <summary>
        /// Сопоставление типа системного сервиса с
        /// функцией создания класса по управлению системным сервисом
        /// </summary>
        private readonly Dictionary<Clouds42Service, Func<Guid, Cloud42ServiceBase>> _42CloudServices;
        private readonly Dictionary<Type, Func<Guid, ICloud42ServiceBase>> _42CloudsServicesByType;

        public Cloud42ServiceFactory(IServiceProvider serviceProvider)
        {
            _42CloudServices = new Dictionary<Clouds42Service, Func<Guid, Cloud42ServiceBase>> {
                {Clouds42Service.MyDisk, accountId=> (MyDisk42CloudService)serviceProvider.GetRequiredService<IMyDisk42CloudService>().SetAccountId(accountId)},
                {Clouds42Service.MyEnterprise, accountId=> (MyEnterprise42CloudService)serviceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountId)},
                {Clouds42Service.Esdl, accountId=> (EsdlCloud42Service)serviceProvider.GetRequiredService<IEsdlCloud42Service>().SetAccountId(accountId)},
                {Clouds42Service.Recognition, accountId=> (Recognition42CloudService)serviceProvider.GetRequiredService<IRecognition42CloudService>().SetAccountId(accountId)},
                {Clouds42Service.MyDatabases, accountId=> (MyDatabases42CloudService)serviceProvider.GetRequiredService<IMyDatabases42CloudService>().SetAccountId(accountId)},
            };
            _42CloudsServicesByType = new Dictionary<Type, Func<Guid, ICloud42ServiceBase>>
            {
                {typeof(IMyDisk42CloudService), accountId=> (MyDisk42CloudService)serviceProvider.GetRequiredService<IMyDisk42CloudService>().SetAccountId(accountId)},
                {typeof(IMyEnterprise42CloudService), accountId=> (MyEnterprise42CloudService)serviceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountId)},
                {typeof(IEsdlCloud42Service), accountId=> (EsdlCloud42Service)serviceProvider.GetRequiredService<IEsdlCloud42Service>().SetAccountId(accountId)},
                {typeof(IRecognition42CloudService), accountId=> (Recognition42CloudService)serviceProvider.GetRequiredService<IRecognition42CloudService>().SetAccountId(accountId)},
                {typeof(IMyDatabases42CloudService), accountId=> (MyDatabases42CloudService)serviceProvider.GetRequiredService<IMyDatabases42CloudService>().SetAccountId(accountId)},
            };
        }

        /// <summary>
        /// Получить класс по управлению системным сервисом
        /// </summary>
        /// <param name="clouds42Service">Тип системного сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Класс по управлению системным сервисом</returns>
        public Cloud42ServiceBase Get(Clouds42Service clouds42Service, Guid accountId)
        {
            return !_42CloudServices.TryGetValue(clouds42Service, out var service) ? null : service(accountId);
        }

        /// <summary>
        /// Получить класс по управлению системным сервисом по типу
        /// </summary>
        /// <typeparam name="TService">Тип класса по управлению системным сервисом</typeparam>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Класс по управлению системным сервисом</returns>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        public TService? Get<TService>(Guid accountId) 
            where TService : ICloud42ServiceBase
        {
            var type = typeof(TService);
            if (!_42CloudsServicesByType.ContainsKey(typeof(TService)))
                throw new ArgumentException($"Factory for {type.FullName} is not registered");
            var service = _42CloudsServicesByType[type].Invoke(accountId);
            if (service == null )
                throw new InvalidCastException($"Factory for {type.FullName} does not return correctly typed instance");
            return (TService?)service;
        }

    }
}
