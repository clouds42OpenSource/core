﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.MyDisk;
using Clouds42.HandlerExeption.Contract;
using Quartz;
using System.Diagnostics;
using Clouds42.AccountUsers.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices
{
    /// <summary>
    /// Менеджер для работы с сервисом облака
    /// </summary>
    public class CloudServiceManager(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        ICloud42ServiceHelper cloud42ServiceHelper,
        ICloudServiceProvider cloudServiceProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), ICloudServiceManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Check service token validity
        /// </summary>
        /// <returns></returns>                
        public ManagerResult<bool>
            CheckServiceTokenValidity(Guid serviceToken,
                out string headString) 
        {
            var timer = new Stopwatch();
            timer.Start();
            AccessProvider.HasAccess(ObjectAction.CloudFileStorageServers_View);
            timer.Stop();
            headString = "Auth:" + timer.ElapsedMilliseconds;

            timer.Restart();
            var cloudService = DbLayer.CloudServiceRepository.FirstOrDefault(c => c.ServiceToken == serviceToken);
            timer.Stop();
            headString += "&ReadDB:" + timer.ElapsedMilliseconds;

            var validity = cloudService != null;
            return Ok(validity);
        }

        /// <summary>
        /// Add cloud service
        /// </summary>
        /// <returns></returns>                       
        public ManagerResult<string> Add(AddCloudServiceModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.CloudFileStorageServers_View);
            if (string.IsNullOrWhiteSpace(model.CloudServiceId))
                return PreconditionFailed<string>("CloudServiceID is empty");
            if (string.IsNullOrWhiteSpace(model.ServiceCaption))
                return PreconditionFailed<string>("ServiceCaption is empty");

            CloudService
                cloudService =
                    DbLayer.CloudServiceRepository
                        .GetCloudService(model.CloudServiceId); 
            if (cloudService != null)
                return Conflict<string>($"Cloud service resource with Id: {model.CloudServiceId} already exist!");
            cloudService = new CloudService
            {
                CloudServiceId = model.CloudServiceId,
                ServiceCaption = model.ServiceCaption,
                Id = Guid.NewGuid()
            };
            DbLayer.CloudServiceRepository.InsertCloudService(cloudService); 

            return Ok(cloudService.CloudServiceId ?? string.Empty);
        }

        /// <summary>
        /// Получить название службы по токену
        /// </summary>
        /// <param name="token">Токен службы</param>
        /// <param name="headString">Строка заголовка запроса</param>
        /// <returns>Название службы</returns>
        public ManagerResult<string> GetServiceByToken(Guid token, out string headString)
        {
            var timer = new Stopwatch();
            timer.Start();
            AccessProvider.HasAccess(ObjectAction.AccountUserSessions_View);
            timer.Stop();

            headString = "Auth:" + timer.ElapsedMilliseconds;

            timer.Restart();
            var serviceUser = DbLayer.CloudServiceRepository.FirstOrDefault(x => x.ServiceToken == token);
            timer.Stop();
            headString += "&ReadDB:" + timer.ElapsedMilliseconds;

            if (serviceUser == null)
                return NotFound<string>("Service not found");

            return Ok(serviceUser.CloudServiceId);
        }

        /// <summary>
        /// Получить информацию по сервису "Мой диск" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информация по сервису "Мой диск" для аккаунта</returns>
        public ManagerResult<ResultDto<MyDiskInfoModelDto>> GetMyDiscByAccount(Guid accountId)
        {
            var timer = new Stopwatch();
            timer.Start();

            AccessProvider.HasAccess(ObjectAction.CloudServices_ViewMyDisc,
                () => AccessProvider.GetUser().RequestAccountId);
            timer.Stop();

            var headString = "Auth:" + timer.ElapsedMilliseconds;

            timer.Restart();

            var myDisk = cloud42ServiceHelper.GetMyDiskInfo(accountId);
            timer.Stop();
            headString += "&ReadDB:" + timer.ElapsedMilliseconds;


            return Ok(new ResultDto<MyDiskInfoModelDto>{Data = myDisk, Head = headString});
        }

        /// <summary>
        /// Обновить данные в таблице CloudService
        /// </summary>
        /// <param name="model">Модель для обновления</param>
        public ManagerResult<bool> UpdateCloudService(CloudServiceDto model)
        {
            AccessProvider.HasAccess(ObjectAction.CloudServices_Edit);

            try
            {
                cloudServiceProvider.UpdateCloudService(model);
            }
            catch (NotFoundException nfe)
            {
                return NotFound<bool>(nfe.Message);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка сохранения изменений в CloudService]");
            }

            return Ok(true);
        }

        /// <summary>
        /// Добавить новую запись в таблице CloudService
        /// </summary>
        /// <param name="model">Модель для добавления</param>
        /// <returns>Модель ответа добавленного CloudService</returns>
        public ManagerResult<AddCloudServiceResponseDto> AddCloudService(CloudServiceDto model)
        {
            AccessProvider.HasAccess(ObjectAction.CloudServices_Edit);

            try
            {
                return Ok(cloudServiceProvider.AddCloudService(model));
            }

            catch (ObjectAlreadyExistsException ex)
            {
                return Conflict<AddCloudServiceResponseDto>(ex.Message);
            }

            catch (Exception ex)
            {
                const string message = "[Ошибка добавления новой записи в CloudService]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<AddCloudServiceResponseDto>(message);
            }
        }

        /// <summary>
        /// Подтверждение подписки для интеграции с Cloud
        /// </summary>
        /// <param name="model">Модель для добавления</param>
        /// <returns></returns>
        public ManagerResult SubscriptionConfirmationForCloud(CloudServiceIntegrationDto model, string login)
        {
            try
            {
                cloudServiceProvider.SubscriptionConfirmationForCloud(model, login);
                return Ok();
            }
            catch (Exception ex)
            {
                string message = $"[Ошибка начисления бонусных средств при интеграции Cloud {ex.Message}]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed(message);
            }
        }

        /// <summary>
        /// Получить информацио о сервисе для Cloud
        /// </summary>
        /// <param name="model">Модель для кого запрашивается информация</param>
        /// <returns></returns>
        public ManagerResult<CloudServiceInfoDto> GetServiceInfoForCloud(string email, string login)
        {
            try
            {
                var result = cloudServiceProvider.GetServiceInfoForCloud(email, login);
                return Ok(result);
            }
            catch (Exception ex)
            {
                string message = $"[Ошибка получения информации для Cloud {ex.Message}]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<CloudServiceInfoDto>(message);
            }
        }

    }
}
