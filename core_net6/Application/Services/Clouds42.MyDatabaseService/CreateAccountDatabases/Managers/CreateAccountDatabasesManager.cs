﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.CreateAccountDatabases.Managers
{
    /// <summary>
    /// Менеджер создания инф. баз
    /// </summary>
    public class CreateAccountDatabasesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateAccountDatabasesProvider createAccountDatabasesProvider,
        IHandlerException handlerException,
        ICloudLocalizer cloudLocalizer,
        IActivateRent1CAtCreationDatabaeProvider activateRent1CAtCreationDatabaseProvider)
        : BaseManager(accessProvider,
            dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать инф. базы из шаблона
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAccountDatabasesFromTemplateDto">Модель создания инф. баз из шаблона</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<CreateAccountDatabasesResultDto> CreateAccountDatabasesFromTemplate(Guid accountId,
            CreateAccountDatabasesFromTemplateDto createAccountDatabasesFromTemplateDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);

                return Ok(createAccountDatabasesProvider.CreateAccountDatabasesFromTemplate(accountId,
                    createAccountDatabasesFromTemplateDto));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка создания инф. баз из шаблона для аккаунта {accountId}]");
                return PreconditionFailed<CreateAccountDatabasesResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Активировать сервис Аренда 1С если это необходимо
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult ActivateRent1CIfNeeded(Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);
                activateRent1CAtCreationDatabaseProvider.ActivateIfNeeded(accountId);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка активации сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)} для аккаунта {accountId}]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Создать инф. базу из ZIP файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromZipDto">Модель создания инф. баз из ZIP файла</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<CreateAccountDatabasesResultDto> CreateAccountDatabaseFromZip(Guid accountId,
            CreateAcDbFromZipDto createAcDbFromZipDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);
                return Ok(createAccountDatabasesProvider.CreateAccountDatabaseFromZip(accountId,
                    createAcDbFromZipDto));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка создания инф. базы из ZIP файла для аккаунта] {accountId}");
                return PreconditionFailed<CreateAccountDatabasesResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Создать инф. базу на разделителях из ZIP файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromZipDto">Модель создания инф. баз из ZIP файла</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<CreateAccountDatabasesResultDto> CreateDatabaseOnDelimitersFromZip(Guid accountId,
            CreateAcDbFromZipForMSDto createAcDbFromZipDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);
                return Ok(createAccountDatabasesProvider.CreateDatabaseOnDelimiterFromZip(accountId,
                    createAcDbFromZipDto));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка создания инф. базы на разделителях из ZIP файла для аккаунта] {accountId}");
                return PreconditionFailed<CreateAccountDatabasesResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Создать инф. базу на разделителях из Dt в ZIP файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromDtToZipDto">Модель создания инф. баз из Dt в ZIP файла</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<CreateAccountDatabasesResultDto> CreateDatabaseOnDelimitersFromDtToZip(Guid accountId,
            CreateAcDbFromDtToZipForMSDto createAcDbFromDtToZipDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);
                return Ok(createAccountDatabasesProvider.CreateDatabaseOnDelimiterFromDtToZip(accountId,
                    createAcDbFromDtToZipDto));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка создания инф. базы на разделителях из ZIP файла для аккаунта] {accountId}");
                return PreconditionFailed<CreateAccountDatabasesResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Создать инф. базу из DT файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromDtDto">Модель создания инф. баз из DT файла</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<CreateAccountDatabasesResultDto> CreateAccountDatabaseFromDtFile(Guid accountId,
            CreateAcDbFromDtDto createAcDbFromDtDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);
                var result = createAccountDatabasesProvider.CreateAccountDatabaseFromDtFile(accountId,
                    createAcDbFromDtDto);

                if (result.IsComplete)
                    return Ok(result);

                return PreconditionFailedError(result, result.ErrorMessage);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка создания инф. базы из DT файла для аккаунта {accountId}]");
                return PreconditionFailed<CreateAccountDatabasesResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Создать инф. базу на разделителях из бекапа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbOnDelimitersFromBackupDto">Модель создания инф. базы на разделителях из бекапа</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<CreateAccountDatabasesResultDto> CreateAcDbOnDelimitersFromBackup(Guid accountId,
            CreateAcDbOnDelimitersFromBackupDto createAcDbOnDelimitersFromBackupDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);

                return Ok(createAccountDatabasesProvider.CreateAcDbOnDelimitersFromBackup(accountId,
                    createAcDbOnDelimitersFromBackupDto));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка создания инф. базы на разделителях из бекапа для аккаунта {accountId}]");
                return PreconditionFailed<CreateAccountDatabasesResultDto>(ex.Message);
            }
        }
    }
}
