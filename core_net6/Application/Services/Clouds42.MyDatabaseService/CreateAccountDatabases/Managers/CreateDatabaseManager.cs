﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.CreateAccountDatabases.Managers
{
    /// <summary>
    /// Менеджер создания баз данных
    /// </summary>
    public class CreateDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICreateAccountDatabaseProvider createAccountDatabaseProvider,
        ICreateDatabaseFromDtCreator createDatabaseFromDtCreator,
        IHandlerException handlerException,
        CreateLogsAboutDatabaseCreationHelper createLogsAboutDatabaseCreationHelper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Создать информационные базы из шаблонов
        /// </summary>
        /// <param name="accountId">Id аккаунта </param>
        /// <param name="databases">Информация по создаваемым базам</param>
        /// <returns>Результат создания инф. баз</returns>
        public CreateCloud42ServiceModelDto CreateDataBasesFromTemplate(Guid accountId,
            List<InfoDatabaseDomainModelDto> databases)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);

            var res = createAccountDatabaseProvider.Process(databases, accountId);

            createLogsAboutDatabaseCreationHelper.Create(accountId, res);

            return res;
        }

        /// <summary>
        /// Создание информационной базы при загрузки zip
        /// </summary>
        /// <param name="accountId">Id аккаунта </param>
        /// <param name="templateId">Идентификатор шаблона загружаемой базы.</param>
        /// <param name="dbCaption">название загруженненной иб</param>
        /// <param name="listInfoUser">Список информации о пользователе полученного из зип пакета</param>
        /// <param name="uploadedFileId">Идентификатор загруженного файла базы.</param>
        /// <returns></returns>
        public CreateCloud42ServiceModelDto CreateDataBasesFromZip(
            Guid accountId,
            Guid uploadedFileId,
            Guid templateId,
            string dbCaption,
            List<InfoAboutUserFromZipPackageDto> listInfoUser,
            List<ExtensionIds> extensionsList)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);

                var databases = new List<InfoDatabaseDomainModelDto>
                {
                    new()
                    {
                        DbTemplateDelimiters = true,
                        DataBaseName = dbCaption,
                        IsChecked = true,
                        IsFile = false,
                        UploadedFileId = uploadedFileId,
                        TemplateId = templateId,
                        ListInfoAboutUserFromZipPackage = listInfoUser,
                        ExtensionsList = extensionsList
                    }
                };

                return CreateDataBasesFromTemplate(accountId, databases);
            }
            catch (Exception e)
            {
                _handlerException.Handle(e, $"[Ошибка создания ифо базы '{dbCaption}' из zip аккаунту '{accountId}']");
                return GenerateCreationDbErrorResultModel($"Загрузка zip файла завершилось с ошибкой :: {e.Message}");
            }
        }

        /// <summary>
        /// Создание информационной базы при загрузки Dt в zip
        /// </summary>
        /// <param name="accountId">Id аккаунта </param>
        /// <param name="dbCaption">название загруженненной иб</param>
        /// <param name="listInfoUser">Список информации о пользователе полученного из зип пакета</param>
        /// <param name="templateId">Идентификатор шаблона загружаемой базы.</param>
        /// <param name="uploadedFileId">Идентификатор загруженного файла базы.</param>
        /// <param name="loginAdmin">Логин администратора базы.</param>
        /// <param name="passwordAdmin">Пароль администратора базы.</param>
        /// <returns></returns>
        public CreateCloud42ServiceModelDto CreateDataBasesFromDtToZip(
            Guid accountId, Guid uploadedFileId, Guid templateId, string dbCaption,
            List<InfoAboutUserFromZipPackageDto> listInfoUser, string loginAdmin, string passwordAdmin)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);

                var databases = new List<InfoDatabaseDomainModelDto>
                {
                    new()
                    {
                        DbTemplateDelimiters = true,
                        DataBaseName = dbCaption,
                        IsChecked = true,
                        IsFile = false,
                        UploadedFileId = uploadedFileId,
                        TemplateId = templateId,
                        ListInfoAboutUserFromZipPackage = listInfoUser,
                        LoginAdmin = loginAdmin,
                        PasswordAdmin = passwordAdmin
                    }
                };

                return CreateDataBasesFromTemplate(accountId, databases);
            }
            catch (Exception e)
            {
                _handlerException.Handle(e, $"[Ошибка создания ифо базы '{dbCaption}' из zip аккаунту '{accountId}']");
                return GenerateCreationDbErrorResultModel($"Загрузка zip файла завершилось с ошибкой :: {e.Message}");
            }
        }



        /// <summary>
        /// Создать информационную базу из бэкапа
        /// </summary>
        /// <param name="infoDatabaseModel">Модель инф. базы</param>
        /// <returns>Результат создания базы</returns>
        public CreateCloud42ServiceModelDto CreateDbOnDelimitersFromBackup(InfoDatabaseDomainModelDto infoDatabaseModel)
        {
            if (!infoDatabaseModel.AccountDatabaseBackupId.HasValue)
                return GenerateCreationDbErrorResultModel(
                    $"Для создаваемой базы {infoDatabaseModel.AccountId}:{infoDatabaseModel.AccountDatabaseBackupId} не указан ID бэкапа");

            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseByBackupIdOrThrowException(infoDatabaseModel
                    .AccountDatabaseBackupId
                    .Value);

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => infoDatabaseModel.AccountId);

                var result = createAccountDatabaseProvider.CreateDbOnDelimitersFromBackup(infoDatabaseModel);


                LogEvent(() => infoDatabaseModel.AccountId, LogActions.RestoreAccountDatabase,
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} поставлена в очередь на восстановление.");

                return result;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"Ошибка создания инф. базы '{infoDatabaseModel.DataBaseName}' из бэкапа аккаунту '{infoDatabaseModel.AccountId}'");

                LogEvent(() => accountDatabase.AccountId, LogActions.RestoreAccountDatabase,
                    $"Не удалось восстановить базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}. Описание ошибки: {ex.Message}");

                return GenerateCreationDbErrorResultModel($"Загрузка zip файла завершилось с ошибкой :: {ex.Message}");
            }
        }

        /// <summary>
        /// Создать инф. базу из DT файла и предоставить указанные доступы
        /// </summary>
        /// <param name="uploadedFileId">Идентификатор загруженного файла.</param>
        /// <param name="dbCaption">Название базы данных</param>
        /// <param name="usersIdForAddAccess">Список ID пользователей для предоставления доступа</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<CreateCloud42ServiceModelDto> CreateDatabaseFromDtAndGrantAccesses(Guid accountId, CreateAcDbFromDtDto createAcDbFromDtDto)
        {
            try
            {

                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => AccessProvider.GetUser().ContextAccountId);
                var result =
                    createDatabaseFromDtCreator.CreateFromDt(accountId, createAcDbFromDtDto);
                createLogsAboutDatabaseCreationHelper.Create(accountId, result, true);

                if(result.Complete)
                    return Ok(result);

                return PreconditionFailedError(result, result.Comment);
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"При создании инф. базы {createAcDbFromDtDto.DbCaption} из DT файла произошла ошибка. Причина: {ex.GetFullInfo(false)}";
                Logger.Error(errorMessage, ex);
                return PreconditionFailed<CreateCloud42ServiceModelDto>(errorMessage);
            }
        }


        /// <summary>
        /// Создать инф. базу из DT файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromDtDto">Модель создания инф. баз из DT файла</param>
        /// <returns>Результат создания</returns>
        public ManagerResult<CreateCloud42ServiceModelDto> LoadAccountDatabaseАfterСonversion(CreateAcDbDtDto createAcDbDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => AccessProvider.GetUser().ContextAccountId);
                var result = createDatabaseFromDtCreator.LoadFromDtАfterСonversion(createAcDbDto);

                if (result.Complete)
                    return Ok(result);

                return PreconditionFailedError(result, result.Comment);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка загрузки инф. базы из DT после конвертации для базы {createAcDbDto.DatabaseId}]");
                return PreconditionFailed<CreateCloud42ServiceModelDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить Uri службы загрузки файлов баз.
        /// </summary>
        /// <param name="accountId">Id аккаунта </param>
        public ManagerResult<string> GetUploadApiUri(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);

            var accountSegment = accountConfigurationDataProvider.GetAccountSegment(accountId);
            return Ok(accountSegment.CoreHosting.UploadApiUrl);
        }

        /// <summary>
        ///  Сформировать модель ошибочного результата создания базы
        /// </summary>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Модель ошибочного результата создания базы</returns>
        private static CreateCloud42ServiceModelDto GenerateCreationDbErrorResultModel(string errorMessage) =>
            new()
            {
                Complete = false,
                Comment = errorMessage
            };
    }
}
