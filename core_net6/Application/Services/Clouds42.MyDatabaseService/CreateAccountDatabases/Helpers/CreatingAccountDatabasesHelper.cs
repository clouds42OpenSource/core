﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;

namespace Clouds42.MyDatabaseService.CreateAccountDatabases.Helpers
{
    /// <summary>
    /// Хэлпер для работы с создаваемыми инф. базами
    /// </summary>
    public class CreatingAccountDatabasesHelper(IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider)
    {
        /// <summary>
        /// Получить список доступов пользователей для создаваемых инф. баз
        /// </summary>
        /// <param name="createDatabasesModelDc">Список моделей создания инф. баз</param>
        /// <returns>Список доступов пользователей</returns>
        public List<AccountUserAccessDto> GetAccountUserAccessesForCreatedDatabases(List<InfoDatabaseDomainModelDto> createDatabasesModelDc)
        {
            var accountUserIdsList = new List<Guid>();
            createDatabasesModelDc.ForEach(model =>
            {
                accountUserIdsList.AddRange(model.UsersToGrantAccess);
            });
            return accountUserIdsList.Distinct().Select(userId => new AccountUserAccessDto
            {
                HasAccess = true,
                UserId = userId
            }).ToList();
        }

        /// <summary>
        /// Получить список доступов пользователей для создаваемых инф. баз
        /// </summary>
        /// <param name="userFromZipPackageList">Список моделей пользователей из зип файла</param>
        /// <returns>Список доступов пользователей</returns>
        public List<AccountUserAccessDto> GetAccountUserAccessesForCreatedDatabases(
            List<InfoAboutUserFromZipPackageDto> userFromZipPackageList)
            => userFromZipPackageList.Select(user => new AccountUserAccessDto
            {
                HasAccess = true,
                UserId = user.AccountUserId
            }).ToList();

        /// <summary>
        /// Получить список доступов пользователей для восстанавливаемой инф. базы
        /// </summary>
        /// <param name="usersIdForAddAccess">Список пользователей для предоставления доступа</param>
        /// <returns>Список доступов пользователей</returns>
        public List<AccountUserAccessDto> GetAccountUserAccessesForRestoreAcDb(List<Guid> usersIdForAddAccess)
            => usersIdForAddAccess.Select(userId => new AccountUserAccessDto
            {
                HasAccess = true,
                UserId = userId
            }).ToList();

        /// <summary>
        /// Получить список ID услуг сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="createDatabasesModelDc">Список моделей создания инф. баз</param>
        /// <returns>Список ID услуг сервиса "Мои инф. базы"</returns>
        public List<Guid> GetMyDatabasesServiceTypesIdList(List<InfoDatabaseDomainModelDto> createDatabasesModelDc)
        {
            var myDatabasesServiceTypesIdList = new List<Guid>();
            createDatabasesModelDc.ForEach(model =>
            {
                var serviceTypeId =
                    myDatabasesServiceTypeDataProvider.GetServiceTypeIdByDbTemplateId(model.TemplateId);
                if (!serviceTypeId.HasValue)
                    return;
                myDatabasesServiceTypesIdList.Add(serviceTypeId.Value);
            });
            return myDatabasesServiceTypesIdList;
        }
    }
}
