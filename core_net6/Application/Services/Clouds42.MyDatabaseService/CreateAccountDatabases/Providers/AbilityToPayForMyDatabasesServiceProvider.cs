﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces;
using Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.CreateAccountDatabases.Helpers;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.CreateAccountDatabases.Providers
{
    /// <summary>
    /// Провайдер проверки возможности оплаты за сервис "Мои инф. базы"
    /// </summary>
    internal class AbilityToPayForMyDatabasesServiceProvider(
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        ICalculateCostOfAcDbAccessesProvider calculateCostOfAcDbAccessesProvider,
        ICalculateCostOfPlacementDatabaseProvider calculateCostOfPlacementDatabaseProvider,
        CreatingAccountDatabasesHelper creatingAccountDatabasesHelper,
        IAbilityToCreatePaymentProvider abilityToCreatePaymentProvider)
        : IAbilityToPayForMyDatabasesServiceProvider
    {
        /// <summary>
        /// Проверить возможность
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак что для оплаты будет использован ОП</param>
        /// <param name="createDatabasesModelDc">Список моделей создания инф. баз</param>
        /// <returns>Результат проверки</returns>
        public CreateAccountDatabasesResultDto CheckAbility(Guid accountId, bool isPromisePayment,
            List<InfoDatabaseDomainModelDto> createDatabasesModelDc)
        {
            var amount = GetCreateAccountDatabasesFromTemplateAmount(accountId, createDatabasesModelDc);
            return CheckAbilityToCreatePayment(accountId, amount, isPromisePayment);
        }

        /// <summary>
        /// Проверить возможность
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак что для оплаты будет использован ОП</param>
        /// <returns>Результат проверки</returns>
        public CreateAccountDatabasesResultDto CheckAbility(Guid accountId, bool isPromisePayment)
        {
            var amount = CalculateCostOfCreatingDatabases(accountId, 1).PartialCost;
            return CheckAbilityToCreatePayment(accountId, amount, isPromisePayment);
        }

        /// <summary>
        /// Проверить возможность
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак что для оплаты будет использован ОП</param>
        /// <param name="userFromZipPackageList">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <returns>Результат проверки</returns>
        public CreateAccountDatabasesResultDto CheckAbility(Guid accountId, bool isPromisePayment,
            List<InfoAboutUserFromZipPackageDto> userFromZipPackageList, Guid? myDatabasesServiceTypeId)
        {
            var amount =
                GetCreateAccountDatabaseFromZipAmount(accountId, userFromZipPackageList, myDatabasesServiceTypeId);

            return CheckAbilityToCreatePayment(accountId, amount, isPromisePayment);
        }

        /// <summary>
        /// Проверить возможность
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак что для оплаты будет использован ОП</param>
        /// <param name="usersIdForAddAccess">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <returns>Результат проверки</returns>
        public CreateAccountDatabasesResultDto CheckAbility(Guid accountId, bool isPromisePayment,
            List<Guid> usersIdForAddAccess, Guid? myDatabasesServiceTypeId)
        {
            var amount =
                GetRestoreAccountDatabaseFromBackupAmount(accountId, usersIdForAddAccess, myDatabasesServiceTypeId);

            return CheckAbilityToCreatePayment(accountId, amount, isPromisePayment);
        }

        /// <summary>
        /// Проверить возможность создать платеж на указанную сумму
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="amount">Сумма</param>
        /// <param name="isPromisePayment">Признак что это ОП</param>
        /// <returns>Результат проверки</returns>
        private CreateAccountDatabasesResultDto CheckAbilityToCreatePayment(Guid accountId, decimal amount,
            bool isPromisePayment)
        {
            var myDatabasesServiceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId();

            var abilityToPayForServiceTypeResult =
                abilityToCreatePaymentProvider.CheckAbilityAndGetPromisePaymentIfNeeded(accountId,
                    myDatabasesServiceId, amount,
                    isPromisePayment);

            return abilityToPayForServiceTypeResult.MapToCreateAccountDatabasesResult();
        }

        /// <summary>
        /// Получить стоимость создаваемых инф. баз
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createDatabasesModelDc">Список моделей создания инф. баз</param>
        /// <returns>Стоимость создаваемых инф. баз</returns>
        private decimal GetCreateAccountDatabasesFromTemplateAmount(Guid accountId,
            List<InfoDatabaseDomainModelDto> createDatabasesModelDc)
        {
            var costOfCreatingDatabases = CalculateCostOfCreatingDatabases(accountId, createDatabasesModelDc.Count);

            var accountUsersAccesses = creatingAccountDatabasesHelper.GetAccountUserAccessesForCreatedDatabases(createDatabasesModelDc);

            var myDatabasesServiceTypesIdList = creatingAccountDatabasesHelper.GetMyDatabasesServiceTypesIdList(createDatabasesModelDc);

            var totalAccessesCost = calculateCostOfAcDbAccessesProvider.GetTotalAccessesCostForConfigurations(
                accountId, myDatabasesServiceTypesIdList,accountUsersAccesses);

            return costOfCreatingDatabases.PartialCost + totalAccessesCost;
        }

        /// <summary>
        /// Получить стоимость создания инф. базы из ZIP файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="userFromZipPackageList">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <returns>Стоимость создаваемых инф. баз</returns>
        private decimal GetCreateAccountDatabaseFromZipAmount(Guid accountId,
            List<InfoAboutUserFromZipPackageDto> userFromZipPackageList, Guid? myDatabasesServiceTypeId)
        {
            var costOfCreatingDatabases = CalculateCostOfCreatingDatabases(accountId, 1);
            if (!myDatabasesServiceTypeId.HasValue)
                return costOfCreatingDatabases.PartialCost;

            var accountUsersAccesses =
                creatingAccountDatabasesHelper.GetAccountUserAccessesForCreatedDatabases(userFromZipPackageList);
            var totalAccessesCost = calculateCostOfAcDbAccessesProvider.GetTotalAccessesCostForConfigurations(
                accountId, [myDatabasesServiceTypeId.Value],
                accountUsersAccesses);
            return costOfCreatingDatabases.PartialCost + totalAccessesCost;
        }

        /// <summary>
        /// Получить стоимость восстанавливаемой базы из бекапа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="usersIdForAddAccess">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <returns></returns>
        private decimal GetRestoreAccountDatabaseFromBackupAmount(Guid accountId, List<Guid> usersIdForAddAccess,
            Guid? myDatabasesServiceTypeId)
        {
            var costOfCreatingDatabases = CalculateCostOfCreatingDatabases(accountId, 1);
            if (!myDatabasesServiceTypeId.HasValue)
                return costOfCreatingDatabases.PartialCost;

            var accountUsersAccesses =
                creatingAccountDatabasesHelper.GetAccountUserAccessesForRestoreAcDb(usersIdForAddAccess);
            var totalAccessesCost = calculateCostOfAcDbAccessesProvider.GetTotalAccessesCostForConfigurations(
                accountId, [myDatabasesServiceTypeId.Value],
                accountUsersAccesses);
            return costOfCreatingDatabases.PartialCost + totalAccessesCost;
        }

        /// <summary>
        /// Посчитать стоимость создаваемых баз по количеству
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="databasesCount">Кол-во баз</param>
        /// <returns>Стоимость создания инф. баз по кол-ву</returns>
        private CalculationCostOfPlacementDatabasesResultDto CalculateCostOfCreatingDatabases(Guid accountId,
            int databasesCount)
            => calculateCostOfPlacementDatabaseProvider.CalculateCostOfPlacementDatabases(
                new CalculateCostOfPlacementDatabasesDto
                {
                    AccountId = accountId,
                    DatabasesForPlacementCount = databasesCount,
                    SystemServiceType = ResourceType.CountOfDatabasesOverLimit
                });
    }
}
