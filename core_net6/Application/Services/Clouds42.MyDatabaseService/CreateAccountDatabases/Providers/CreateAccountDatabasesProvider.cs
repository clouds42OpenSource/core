﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces;
using CreateDatabaseManager = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers.CreateDatabaseManager;

namespace Clouds42.MyDatabaseService.CreateAccountDatabases.Providers
{
    /// <summary>
    /// Провайдер создания инф. баз
    /// </summary>
    internal class CreateAccountDatabasesProvider(
        IAbilityToPayForMyDatabasesServiceProvider abilityToPayForMyDatabasesServiceProvider,
        CreateDatabaseManager createDatabaseManager,
        AccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        IApplyOrPayForAllMyDatabasesServiceChanges applyOrPayForAllMyDatabasesServiceChanges)
        : ICreateAccountDatabasesProvider
    {
        /// <summary>
        /// Создать инф. базы из шаблона
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAccountDatabasesFromTemplateDto">Модель создания инф. баз из шаблона</param>
        /// <returns>Результат создания</returns>
        public CreateAccountDatabasesResultDto CreateAccountDatabasesFromTemplate(Guid accountId,
            CreateAccountDatabasesFromTemplateDto createAccountDatabasesFromTemplateDto)
        {
            var abilityToPayForDatabasesResult =
                abilityToPayForMyDatabasesServiceProvider.CheckAbility(accountId,
                    createAccountDatabasesFromTemplateDto.NeedUsePromisePayment,
                    createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc);

            if (!abilityToPayForDatabasesResult.IsComplete)
                return abilityToPayForDatabasesResult;

            applyOrPayForAllMyDatabasesServiceChanges.ApplyOrPay(accountId,
                createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc);

            var createAcDbResult = createDatabaseManager.CreateDataBasesFromTemplate(accountId,
                createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc);

            if (createAcDbResult.Complete)
                return new CreateAccountDatabasesResultDto
                {
                    IsComplete = true
                };

            return new CreateAccountDatabasesResultDto
            {
                ErrorMessage = createAcDbResult.Comment,
                IsComplete = false
            };
        }

        /// <summary>
        /// Создать инф. базу из ZIP файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromZipDto">Модель создания инф. баз из ZIP файла</param>
        /// <returns>Результат создания</returns>
        public CreateAccountDatabasesResultDto CreateAccountDatabaseFromZip(Guid accountId, CreateAcDbFromZipDto createAcDbFromZipDto)
        {
            var abilityToPayForDatabasesResult =
                abilityToPayForMyDatabasesServiceProvider.CheckAbility(accountId,
                    createAcDbFromZipDto.NeedUsePromisePayment,
                    createAcDbFromZipDto.UserFromZipPackageList,
                    createAcDbFromZipDto.MyDatabasesServiceTypeId);

            if (!abilityToPayForDatabasesResult.IsComplete)
                return abilityToPayForDatabasesResult;

            applyOrPayForAllMyDatabasesServiceChanges.ApplyOrPay(accountId,
                createAcDbFromZipDto.UserFromZipPackageList, createAcDbFromZipDto.MyDatabasesServiceTypeId);

            var createAcDbResult = createDatabaseManager.CreateDataBasesFromZip(accountId,
                createAcDbFromZipDto.UploadedFileId, createAcDbFromZipDto.TemplateId, createAcDbFromZipDto.DbCaption,
                createAcDbFromZipDto.UserFromZipPackageList, []);

            if (createAcDbResult.Complete)
                return new CreateAccountDatabasesResultDto
                {
                    IsComplete = true
                };

            return new CreateAccountDatabasesResultDto
            {
                ErrorMessage = createAcDbResult.Comment,
                IsComplete = false
            };
        }

        /// <summary>
        /// Создать инф. базу на разделителях из ZIP файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromZipDto">Модель создания инф. баз из ZIP файла</param>
        /// <returns>Результат создания</returns>
        public CreateAccountDatabasesResultDto CreateDatabaseOnDelimiterFromZip(Guid accountId, CreateAcDbFromZipForMSDto createAcDbFromZipDto)
        {
            var abilityToPayForDatabasesResult =
                abilityToPayForMyDatabasesServiceProvider.CheckAbility(accountId,
                    createAcDbFromZipDto.NeedUsePromisePayment,
                    createAcDbFromZipDto.UserFromZipPackageList,
                    createAcDbFromZipDto.MyDatabasesServiceTypeId);

            if (!abilityToPayForDatabasesResult.IsComplete)
                return abilityToPayForDatabasesResult;

            applyOrPayForAllMyDatabasesServiceChanges.ApplyOrPay(accountId,
                createAcDbFromZipDto.UserFromZipPackageList, createAcDbFromZipDto.MyDatabasesServiceTypeId);

            var template = accountDatabaseWebPublishPathHelper.GetDbTemplateDelimiters(createAcDbFromZipDto.ConfigurationName);

            var createAcDbResult = createDatabaseManager.CreateDataBasesFromZip(accountId,
            createAcDbFromZipDto.UploadedFileId, template.TemplateId, createAcDbFromZipDto.DbCaption,
            createAcDbFromZipDto.UserFromZipPackageList, createAcDbFromZipDto.ExtensionsList);

            if (createAcDbResult.Complete)
                return new CreateAccountDatabasesResultDto
                {
                    IsComplete = true
                };

            return new CreateAccountDatabasesResultDto
            {
                ErrorMessage = createAcDbResult.Comment,
                IsComplete = false
            };
        }

        /// <summary>
        /// Создать инф. базу на разделителях из DT в ZIP
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromZipDto">Модель создания инф. баз из DT в ZIP</param>
        /// <returns>Результат создания</returns>
        public CreateAccountDatabasesResultDto CreateDatabaseOnDelimiterFromDtToZip(Guid accountId, CreateAcDbFromDtToZipForMSDto createAcDbFromDtToZipDto)
        {
            var abilityToPayForDatabasesResult =
                abilityToPayForMyDatabasesServiceProvider.CheckAbility(accountId,
                    createAcDbFromDtToZipDto.NeedUsePromisePayment,
                    createAcDbFromDtToZipDto.UserFromZipPackageList,
                    createAcDbFromDtToZipDto.MyDatabasesServiceTypeId);

            if (!abilityToPayForDatabasesResult.IsComplete)
                return abilityToPayForDatabasesResult;

            applyOrPayForAllMyDatabasesServiceChanges.ApplyOrPay(accountId,
                createAcDbFromDtToZipDto.UserFromZipPackageList, createAcDbFromDtToZipDto.MyDatabasesServiceTypeId);

            var template = accountDatabaseWebPublishPathHelper.GetDbTemplateDelimiters(createAcDbFromDtToZipDto.ConfigurationName);

            var createAcDbResult = createDatabaseManager.CreateDataBasesFromDtToZip(accountId,
            createAcDbFromDtToZipDto.UploadedFileId, template.TemplateId, createAcDbFromDtToZipDto.DbCaption,
            createAcDbFromDtToZipDto.UserFromZipPackageList, createAcDbFromDtToZipDto.LoginAdmin, createAcDbFromDtToZipDto.PasswordAdmin);

            if (createAcDbResult.Complete)
                return new CreateAccountDatabasesResultDto
                {
                    IsComplete = true
                };

            return new CreateAccountDatabasesResultDto
            {
                ErrorMessage = createAcDbResult.Comment,
                IsComplete = false
            };
        }

        /// <summary>
        /// Создать инф. базу из DT файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromDtDto">Модель создания инф. баз из DT файла</param>
        /// <returns>Результат создания</returns>
        public CreateAccountDatabasesResultDto CreateAccountDatabaseFromDtFile(Guid accountId, CreateAcDbFromDtDto createAcDbFromDtDto)
        {
            var abilityToPayForDatabasesResult =
                abilityToPayForMyDatabasesServiceProvider.CheckAbility(accountId,
                    createAcDbFromDtDto.NeedUsePromisePayment);

            if (!abilityToPayForDatabasesResult.IsComplete)
                return abilityToPayForDatabasesResult;

            applyOrPayForAllMyDatabasesServiceChanges.PayForDatabasePlacement(accountId, 1);

            var createAcDbResult =
                createDatabaseManager.CreateDatabaseFromDtAndGrantAccesses(accountId, createAcDbFromDtDto);

            if (!createAcDbResult.Error)
                return new CreateAccountDatabasesResultDto
                {
                    IsComplete = true
                };

            return new CreateAccountDatabasesResultDto
            {
                ErrorMessage = createAcDbResult.Message,
                IsComplete = false,
                ResourceForRedirect = createAcDbResult.Result.ResourceForRedirect
            };
        }

        /// <summary>
        /// Создать инф. базу на разделителях из бекапа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbOnDelimitersFromBackupDto">Модель создания инф. базы на разделителях из бекапа</param>
        /// <returns>Результат создания</returns>
        public CreateAccountDatabasesResultDto CreateAcDbOnDelimitersFromBackup(Guid accountId,
            CreateAcDbOnDelimitersFromBackupDto createAcDbOnDelimitersFromBackupDto)
        {
            var abilityToPayForDatabasesResult =
                abilityToPayForMyDatabasesServiceProvider.CheckAbility(accountId,
                    createAcDbOnDelimitersFromBackupDto.NeedUsePromisePayment,
                    createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.ListInfoAboutUserFromZipPackage,
                    createAcDbOnDelimitersFromBackupDto.MyDatabasesServiceTypeId);

            if (!abilityToPayForDatabasesResult.IsComplete)
                return abilityToPayForDatabasesResult;

            applyOrPayForAllMyDatabasesServiceChanges.ApplyOrPay(accountId,
                createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.ListInfoAboutUserFromZipPackage,
                createAcDbOnDelimitersFromBackupDto.MyDatabasesServiceTypeId);

            var createAcDbResult =
                createDatabaseManager.CreateDbOnDelimitersFromBackup(createAcDbOnDelimitersFromBackupDto
                    .CreateAcDbModel);

            if (createAcDbResult.Complete)
                return new CreateAccountDatabasesResultDto
                {
                    IsComplete = true
                };

            return new CreateAccountDatabasesResultDto
            {
                ErrorMessage = createAcDbResult.Comment,
                IsComplete = false
            };
        }
    }
}
