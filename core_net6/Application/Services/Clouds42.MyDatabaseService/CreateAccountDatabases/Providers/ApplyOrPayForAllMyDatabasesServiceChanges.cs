﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.DatabasesPlacement;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.MyDatabaseService.Contracts.BuyDatabasePlacement.Interfaces;
using Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.CreateAccountDatabases.Helpers;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.CreateAccountDatabases.Providers
{
    /// <summary>
    /// Провайдер для принятия/оплаты изменений сервиса "Мои инф. базы"
    /// </summary>
    internal class ApplyOrPayForAllMyDatabasesServiceChanges(
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IBillingServiceTypePayProvider billingServiceTypePayProvider,
        CreatingAccountDatabasesHelper creatingAccountDatabasesHelper,
        IBuyDatabasesPlacementProvider buyDatabasesPlacementProvider,
        IUnitOfWork dbLayer)
        : IApplyOrPayForAllMyDatabasesServiceChanges
    {
        /// <summary>
        /// Принять/оплатить изменения
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createDatabasesModelDc">Список моделей создания инф. баз</param>
        public void ApplyOrPay(Guid accountId, List<InfoDatabaseDomainModelDto> createDatabasesModelDc)
            => ApplyOrPay(accountId, GetAccountUsersResourcesChanges(createDatabasesModelDc),
                createDatabasesModelDc.Count);

        /// <summary>
        /// Принять/оплатить изменения
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="userFromZipPackageList">Список моделей создания инф. баз</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        public void ApplyOrPay(Guid accountId, List<InfoAboutUserFromZipPackageDto> userFromZipPackageList, Guid? myDatabasesServiceTypeId)
            => ApplyOrPay(accountId, GetAccountUsersResourcesChanges(userFromZipPackageList, myDatabasesServiceTypeId), 1);

        /// <summary>
        /// Принять/оплатить изменения
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="usersIdForAddAccess">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        public void ApplyOrPay(Guid accountId, List<Guid> usersIdForAddAccess, Guid? myDatabasesServiceTypeId)
            => ApplyOrPay(accountId, GetAccountUsersResourcesChanges(usersIdForAddAccess, myDatabasesServiceTypeId), 1);

        /// <summary>
        /// Принять/оплатить изменения
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUsersResourcesChanges">Список изменений ресурсов пользователей</param>
        /// <param name="databasesCount">Кол-во инф. базы</param>
        private void ApplyOrPay(Guid accountId, List<CalculateBillingServiceTypeDto> accountUsersResourcesChanges,
            int databasesCount)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var errorMessage =
                    $"Не удалось оплатить покупку создания {databasesCount} инф. баз для аккаунта {accountId}";

                ApplyOrPayForAccesses(accountId, accountUsersResourcesChanges, errorMessage);
                PayForDatabasePlacement(accountId, databasesCount);

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Принять/оплатить за доступы
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUsersResourcesChanges">Список изменений ресурсов пользователей</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        private void ApplyOrPayForAccesses(Guid accountId, List<CalculateBillingServiceTypeDto> accountUsersResourcesChanges, string errorMessage)
        {
            if (!accountUsersResourcesChanges.Any())
                return;

            var applyOrPayResult = billingServiceTypePayProvider.ApplyOrPayForAllChangesAccountServiceTypes(accountId,
                GetMyDatabasesServiceId(), false, null, accountUsersResourcesChanges);
            if (!applyOrPayResult.IsComplete)
                throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Заплатить за размещение инф. баз
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="databasesCount">Кол-во баз</param>
        public void PayForDatabasePlacement(Guid accountId, int databasesCount)
        {
            var errorMessage =
                $"Не удалось оплатить покупку создания {databasesCount} инф. баз для аккаунта {accountId}";
            var databasesPlacementResult = buyDatabasesPlacementProvider.BuyDatabasesPlacement(new BuyDatabasesPlacementDto
            {
                AccountId = accountId,
                SystemServiceType = ResourceType.CountOfDatabasesOverLimit,
                IsPromisePayment = false,
                DatabasesForPlacementCount = databasesCount
            });
            if (!databasesPlacementResult.IsComplete)
                throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Получить список изменений ресурсов пользователей
        /// </summary>
        /// <param name="createDatabasesModelDc">Список моделей создания инф. баз</param>
        /// <returns>Список изменений ресурсов пользователей</returns>
        private List<CalculateBillingServiceTypeDto> GetAccountUsersResourcesChanges(List<InfoDatabaseDomainModelDto> createDatabasesModelDc)
        {
            var resourcesChanges = new List<CalculateBillingServiceTypeDto>();
            var myDatabasesServiceTypeIdList =
                creatingAccountDatabasesHelper.GetMyDatabasesServiceTypesIdList(createDatabasesModelDc).Distinct()
                    .ToList();

            var accountUserAccessesModels =
                creatingAccountDatabasesHelper.GetAccountUserAccessesForCreatedDatabases(createDatabasesModelDc);

            myDatabasesServiceTypeIdList.ForEach(serviceTypeId =>
            {
                resourcesChanges.AddRange(CreateAcUsResourcesChangesModels(serviceTypeId, accountUserAccessesModels));
            });
            return resourcesChanges;
        }

        /// <summary>
        /// Получить список изменений ресурсов пользователей
        /// </summary>
        /// <param name="userFromZipPackageList">Список моделей создания инф. баз</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <returns>Список изменений ресурсов пользователей</returns>
        private List<CalculateBillingServiceTypeDto> GetAccountUsersResourcesChanges(List<InfoAboutUserFromZipPackageDto> userFromZipPackageList, Guid? myDatabasesServiceTypeId)
        {
            var accountUserAccessesModels =
                creatingAccountDatabasesHelper.GetAccountUserAccessesForCreatedDatabases(userFromZipPackageList);

            return !myDatabasesServiceTypeId.HasValue
                ? []
                : CreateAcUsResourcesChangesModels(myDatabasesServiceTypeId.Value, accountUserAccessesModels);
        }

        /// <summary>
        /// Получить список изменений ресурсов пользователей
        /// </summary>
        /// <param name="usersIdForAddAccess">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <returns>Список изменений ресурсов пользователей</returns>
        private List<CalculateBillingServiceTypeDto> GetAccountUsersResourcesChanges(List<Guid> usersIdForAddAccess,
            Guid? myDatabasesServiceTypeId)
        {
            var accountUserAccessesModels =
                creatingAccountDatabasesHelper.GetAccountUserAccessesForRestoreAcDb(usersIdForAddAccess);

            return !myDatabasesServiceTypeId.HasValue
                ? []
                : CreateAcUsResourcesChangesModels(myDatabasesServiceTypeId.Value, accountUserAccessesModels);
        }

        /// <summary>
        /// Создать список моделей изменений ресурсов пользователей
        /// </summary>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <param name="accountUserAccesses">Список доступов пользователей</param>
        /// <returns>Список моделей изменений ресурсов пользователей</returns>
        private List<CalculateBillingServiceTypeDto> CreateAcUsResourcesChangesModels(Guid myDatabasesServiceTypeId,
            List<AccountUserAccessDto> accountUserAccesses)
            => accountUserAccesses.Select(access => new CalculateBillingServiceTypeDto
                {
                    BillingServiceTypeId = myDatabasesServiceTypeId,
                    Status = true,
                    Subject = access.UserId,
                    Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                    {
                        Me = false,
                        I = false
                    }
                }).ToList();
        

        /// <summary>
        /// Получить ID услуги сервиса "Мои инф. базы"
        /// </summary>
        /// <returns>ID услуги сервиса "Мои инф. базы"</returns>
        private Guid GetMyDatabasesServiceId()
            => myDatabasesServiceDataProvider.GetMyDatabasesServiceId();
    }
}
