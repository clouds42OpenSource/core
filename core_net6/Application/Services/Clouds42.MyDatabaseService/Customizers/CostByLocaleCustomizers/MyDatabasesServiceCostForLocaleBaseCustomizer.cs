﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.MyDatabaseService.Customizers.CostByLocaleCustomizers
{
    /// <summary>
    /// Базовый класс кастомизатора стоимости
    /// сервиса "Мои инф. базы" для локали
    /// </summary>
    public abstract class MyDatabasesServiceCostForLocaleBaseCustomizer(IServiceProvider serviceProvider)
    {
        private readonly ILogger42 _logger = serviceProvider.GetRequiredService<ILogger42>();
        private readonly ICreateAccountRateProvider _createAccountRateProvider = serviceProvider.GetRequiredService<ICreateAccountRateProvider>();
        private readonly IAccountRateDataProvider _accountRateDataProvider = serviceProvider.GetRequiredService<IAccountRateDataProvider>();
        private readonly IMyDatabasesServiceDataProvider _myDatabasesServiceDataProvider = serviceProvider.GetRequiredService<IMyDatabasesServiceDataProvider>();
        private readonly IRecalculateMyDatabasesServiceResourcesProvider _recalculateMyDatabasesServiceResourcesProvider = serviceProvider.GetRequiredService<IRecalculateMyDatabasesServiceResourcesProvider>();
        private readonly IUpdateAccountRateProvider _updateAccountRateProvider = serviceProvider.GetRequiredService<IUpdateAccountRateProvider>();
        private readonly IRemoveAccountRateProvider _removeAccountRateProvider = serviceProvider.GetRequiredService<IRemoveAccountRateProvider>();
        private readonly IUnitOfWork _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly IHandlerException _handlerException = serviceProvider.GetRequiredService<IHandlerException>();
        private readonly ISender _sender = serviceProvider.GetRequiredService<ISender>();

        /// <summary>
        /// Определить кастомную стоимость услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Кастомная стоимость услуги</returns>
        protected abstract decimal DefineCustomServiceTypeCost(ResourceType systemServiceType);

        /// <summary>
        /// Выполнить дополнительную кастомизацию
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        protected virtual void ExecuteAdditionalCustomization(Guid accountId) =>
            _logger.Trace($"Дополнительная кастомизация для аккаунта '{accountId}' не требуется");

        /// <summary>
        /// Кастомизировать стоимость сервиса
        /// "Мои инф. базы" для локали аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="isNewAccount">Признак, указывающий что аккаунт является новым
        /// (нет дополнительных тарифов и настроек на аккаунт)</param>
        public virtual void Customize(Guid accountId, bool isNewAccount = false)
        {
            var serviceId = _myDatabasesServiceDataProvider.GetMyDatabasesServiceId();

            var message =
                $"Выполнение кастомизации стоимости сервиса 'Мои информационные базы' для аккаунта '{accountId}'";

            using var dbScope = _dbLayer.SmartTransaction.Get();
            try
            {
                if (!isNewAccount)
                    _removeAccountRateProvider.RemoveAccountRatesByService(accountId, serviceId);

                MakeCustomAccountRates(accountId);
                ExecuteAdditionalCustomization(accountId);
                _recalculateMyDatabasesServiceResourcesProvider.RecalculateResources(accountId);
                _sender.Send(new RecalculateResourcesConfigurationCostCommand([accountId],
                    [serviceId], Clouds42Service.MyDatabases)).Wait();


                dbScope.Commit();

                _logger.Trace($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка выполнения кастомизации стоимости сервиса 'Мои информационные базы' для аккаунта] '{accountId}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Сделать кастомные тарифы услуг на аккаунт
        /// для сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        private void MakeCustomAccountRates(Guid accountId) => _myDatabasesServiceDataProvider
            .GetShortDataByMyDatabasesServiceTypes().ToList().ForEach(serviceTypeData =>
            {
                var newCost = DefineCustomServiceTypeCost(serviceTypeData.SystemServiceType);
                MakeCustomAccountRateForServiceType(accountId, serviceTypeData.Id, newCost);
            });

        /// <summary>
        /// Сделать кастомный тариф услуги на аккаунт
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="rateCost">Стоимость тарифа</param>
        private void MakeCustomAccountRateForServiceType(Guid accountId, Guid serviceTypeId, decimal rateCost)
        {
            var accountRate = _accountRateDataProvider.GetAccountRate(accountId, serviceTypeId);

            if (accountRate != null)
            {
                _updateAccountRateProvider.Update(accountRate, rateCost);
                return;
            }

            _createAccountRateProvider.Create(new CreateAccountRateDto
            {
                AccountId = accountId,
                BillingServiceTypeId = serviceTypeId,
                Cost = rateCost
            });
        }
    }
}
