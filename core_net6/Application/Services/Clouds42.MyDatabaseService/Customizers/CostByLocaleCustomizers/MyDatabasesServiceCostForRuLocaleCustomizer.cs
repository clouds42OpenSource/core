﻿using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.MyDatabaseService.Customizers.CostByLocaleCustomizers
{
    /// <summary>
    /// Кастомизатор стоимости сервиса "Мои инф. базы" для русской локали
    /// </summary>
    internal class MyDatabasesServiceCostForRuLocaleCustomizer(IServiceProvider serviceProvider)
        : MyDatabasesServiceCostForLocaleBaseCustomizer(serviceProvider),
            IMyDatabasesServiceCostForLocaleCustomizer
    {
        private readonly ILogger42 _logger = serviceProvider.GetRequiredService<ILogger42>();

        private readonly IRecalculateMyDatabasesServiceForAccountProvider
            _recalculateMyDatabasesServiceForAccountProvider = serviceProvider.GetRequiredService<IRecalculateMyDatabasesServiceForAccountProvider>();

        /// <summary>
        /// Определить кастомную стоимость услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Кастомная стоимость услуги</returns>
        protected override decimal DefineCustomServiceTypeCost(ResourceType systemServiceType) =>
            throw new NotImplementedException();

        /// <summary>
        /// Кастомизировать стоимость сервиса
        /// "Мои инф. базы" для локали аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="isNewAccount">Признак, указывающий что аккаунт является новым</param>
        public override void Customize(Guid accountId, bool isNewAccount = false)
        {
            if (!isNewAccount)
            {
                _recalculateMyDatabasesServiceForAccountProvider.RecalculateServiceCost(accountId);
                return;
            }

            _logger.Trace(
                $"Кастомизация стоимости сервиса 'Мои инф. базы' для аккаунта '{accountId}' с русской локалью не требуется");
        }
    }
}