﻿using Clouds42.Configurations.Configurations;
using Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.MyDatabaseService.Customizers.CostByLocaleCustomizers;

internal class MyDatabasesServiceCostForUzLocaleCustomizer : MyDatabasesServiceCostForLocaleBaseCustomizer,
    IMyDatabasesServiceCostForLocaleCustomizer
{
    private readonly IUpdateLimitOnFreeCreationDbForAccountProvider _updateLimitOnFreeCreationDbForAccountProvider;

    public MyDatabasesServiceCostForUzLocaleCustomizer(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        _updateLimitOnFreeCreationDbForAccountProvider =
            serviceProvider.GetRequiredService<IUpdateLimitOnFreeCreationDbForAccountProvider>();
    }

    /// <summary>
    /// Определить кастомную стоимость услуги
    /// </summary>
    /// <param name="systemServiceType">Тип системной услуги</param>
    /// <returns>Кастомная стоимость услуги</returns>
    protected override decimal DefineCustomServiceTypeCost(ResourceType systemServiceType) =>
        systemServiceType == ResourceType.CountOfDatabasesOverLimit
            ? CloudConfigurationProvider
                .MyDatabasesServiceCustomization
                .CustomizationForUzLocale.GetCostOfCreatingDbOverFreeLimit()
            : decimal.Zero;

    /// <summary>
    /// Выполнить дополнительную кастомизацию
    /// </summary>
    /// <param name="accountId">Id аккаунта</param>
    protected override void ExecuteAdditionalCustomization(Guid accountId) =>
        _updateLimitOnFreeCreationDbForAccountProvider.UpdateIfAvailableOtherwiseCreate(accountId, CloudConfigurationProvider.MyDatabasesServiceCustomization
            .CustomizationForUzLocale.GetLimitOnFreeCreationDb());
}
