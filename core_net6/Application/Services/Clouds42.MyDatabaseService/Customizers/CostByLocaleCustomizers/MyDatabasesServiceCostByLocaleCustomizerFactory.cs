﻿using Clouds42.Common.Exceptions;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.MyDatabaseService.Customizers.CostByLocaleCustomizers
{
    /// <summary>
    /// Фабрика для создания кастомизатора стоимости сервиса "Мои инф. базы"
    /// по локали аккаунта
    /// </summary>
    internal class MyDatabasesServiceCostByLocaleCustomizerFactory(IServiceProvider serviceProvider)
        : IMyDatabasesServiceCostByLocaleCustomizerFactory
    {
        /// <summary>
        /// Создать кастомизатор стоимости сервиса "Мои инф. базы"
        /// по локали аккаунта
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Кастомизатор стоимости сервиса "Мои инф. базы"</returns>
        public IMyDatabasesServiceCostForLocaleCustomizer CreateByLocale(string localeName) =>
            // 27.04.2022 ToDo: придумать как получать сервисы в зависимости от локали
            serviceProvider.GetRequiredService<IMyDatabasesServiceCostForLocaleCustomizer>() ?? throw new NotFoundException(
                $"Не удалось создать кастомизатор стоимости сервиса 'Мои инф. базы' для локали '{localeName}'");

        /// <summary>
        /// Создать кастомизатор стоимости сервиса "Мои инф. базы"
        /// по локали аккаунта
        /// </summary>
        /// <param name="localeId">Id локали</param>
        /// <returns>Кастомизатор стоимости сервиса "Мои инф. базы"</returns>
        public IMyDatabasesServiceCostForLocaleCustomizer CreateByLocale(Guid localeId) =>
            CreateByLocale(serviceProvider.GetRequiredService<ILocaleProvider>().GetById(localeId).Name);

    }
}