﻿using Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Customizers.CostByLocaleCustomizers
{
    /// <summary>
    /// Кастомизатор стоимости сервиса "Мои инф. базы" для украинской локали
    /// </summary>
    internal class MyDatabasesServiceCostForUaLocaleCustomizer(IServiceProvider serviceProvider)
        : MyDatabasesServiceCostForLocaleBaseCustomizer(serviceProvider),
            IMyDatabasesServiceCostForLocaleCustomizer
    {
        /// <summary>
        /// Определить кастомную стоимость услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Кастомная стоимость услуги</returns>
        protected override decimal DefineCustomServiceTypeCost(ResourceType systemServiceType) => decimal.Zero;
    }
}