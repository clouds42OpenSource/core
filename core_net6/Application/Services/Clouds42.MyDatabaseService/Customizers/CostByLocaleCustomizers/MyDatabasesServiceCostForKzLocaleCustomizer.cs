﻿using Clouds42.Configurations.Configurations;
using Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.MyDatabaseService.Customizers.CostByLocaleCustomizers
{
    /// <summary>
    /// Кастомизатор стоимости сервиса "Мои инф. базы" для казахской локали
    /// </summary>
    internal class MyDatabasesServiceCostForKzLocaleCustomizer : MyDatabasesServiceCostForLocaleBaseCustomizer,
        IMyDatabasesServiceCostForLocaleCustomizer
    {
        private readonly IUpdateLimitOnFreeCreationDbForAccountProvider _updateLimitOnFreeCreationDbForAccountProvider;
        private readonly Lazy<int> _limitOnFreeCreationDb;
        private readonly Lazy<decimal> _costOfCreatingDbOverFreeLimit;

        public MyDatabasesServiceCostForKzLocaleCustomizer(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _updateLimitOnFreeCreationDbForAccountProvider =
                serviceProvider.GetRequiredService<IUpdateLimitOnFreeCreationDbForAccountProvider>();

            _limitOnFreeCreationDb = new Lazy<int>(CloudConfigurationProvider.MyDatabasesServiceCustomization
                .CustomizationForKzLocale.GetLimitOnFreeCreationDb);

            _costOfCreatingDbOverFreeLimit = new Lazy<decimal>(CloudConfigurationProvider
                .MyDatabasesServiceCustomization
                .CustomizationForKzLocale.GetCostOfCreatingDbOverFreeLimit);
        }

        /// <summary>
        /// Определить кастомную стоимость услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Кастомная стоимость услуги</returns>
        protected override decimal DefineCustomServiceTypeCost(ResourceType systemServiceType) =>
            systemServiceType == ResourceType.CountOfDatabasesOverLimit
                ? _costOfCreatingDbOverFreeLimit.Value
                : decimal.Zero;

        /// <summary>
        /// Выполнить дополнительную кастомизацию
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        protected override void ExecuteAdditionalCustomization(Guid accountId) =>
            _updateLimitOnFreeCreationDbForAccountProvider.UpdateIfAvailableOtherwiseCreate(accountId,
                _limitOnFreeCreationDb.Value);
    }
}
