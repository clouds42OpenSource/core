﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Constants;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.DatabasesPlacement;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.BuyDatabasePlacement.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;

namespace Clouds42.MyDatabaseService.BuyDatabasePlacement.Providers
{
    /// <summary>
    /// Провайдер для покупки лицензий на размещение инф. баз
    /// </summary>
    internal class BuyDatabasesPlacementProvider(
        ICalculateCostOfPlacementDatabaseProvider calculateCostOfPlacementDatabaseProvider,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IUpdateMyDatabasesResourceProvider updateMyDatabasesResourceProvider,
        IAbilityToCreatePaymentProvider abilityToCreatePaymentProvider,
        ICreatePaymentHelper createPaymentHelper,
        IIncreaseAgentPaymentProcessor increaseAgentPaymentProcessor,
        IResourcesConfigurationDataProvider resourcesConfigurationDataProvider,
        IRecalculateMyDatabasesServiceResourcesProvider recalculateMyDatabasesServiceResourcesProvider,
        IHandlerException handlerException,
        ICloudLocalizer cloudLocalizer,
        ISender sender)
        : IBuyDatabasesPlacementProvider
    {
        /// <summary>
        /// Совершить покупку(применение) лицензий на размещение инф. баз
        /// </summary>
        /// <param name="model">Модель для покупки лицензий на размещение инф. баз</param>
        /// <returns>Модель результата покупки лицензий на размещение инф. баз</returns>
        public BuyDatabasesPlacementResultDto BuyDatabasesPlacement(BuyDatabasesPlacementDto model)
        {
            var calculationOfPlacementResult = CalculateCostOfPlacementDatabases(model);
            var myDatabasesServiceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId();
            try
            {
                var buyDatabasesPlacementResult = TryCreatePayments(model, myDatabasesServiceId,
                    calculationOfPlacementResult.PartialCost);

                if (!buyDatabasesPlacementResult.IsComplete)
                {
                    return buyDatabasesPlacementResult;
                }

                if (buyDatabasesPlacementResult.PaymentIds.Any())
                    increaseAgentPaymentProcessor.Process(buyDatabasesPlacementResult.PaymentIds);

                EditResourceForServiceType(model, calculationOfPlacementResult);

                sender.Send(new RecalculateResourcesConfigurationCostCommand([model.AccountId],
                    [myDatabasesServiceId], Clouds42Service.MyDatabases)).Wait();

                return buyDatabasesPlacementResult;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка Покупки(применения) лицензий на размещение инф. баз для аккаунта] '{model.AccountId}'");
                throw;
            }
        }

        /// <summary>
        /// Изменить ресурс для услуги сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="model">Модель для покупки лицензий на размещение инф. баз</param>
        /// <param name="calculationOfPlacementResult">Модель результата подсчета стоимости
        /// размещения инф. баз для аккаунта</param>
        private void EditResourceForServiceType(BuyDatabasesPlacementDto model,
            CalculationCostOfPlacementDatabasesResultDto calculationOfPlacementResult)
        {
            updateMyDatabasesResourceProvider.UpdateActualDatabasesCount(model.AccountId, model.SystemServiceType,
                calculationOfPlacementResult.ActualDatabasesCount);

            updateMyDatabasesResourceProvider.IncreasePaidDatabasesCount(model.AccountId, model.SystemServiceType,
                calculationOfPlacementResult.DatabasesForWhichCostCalculatedCount);

            recalculateMyDatabasesServiceResourcesProvider.RecalculateResourcesForServiceType(model.AccountId,
                model.SystemServiceType);
        }

        /// <summary>
        /// Попытаться создать платежи на указанную сумму 
        /// </summary>
        /// <param name="model">Модель для покупки лицензий на размещение инф. баз</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="cost">Сумма предпологаемого платежа</param>
        /// <returns>Модель результата покупки лицензий на размещение инф. баз</returns>
        private BuyDatabasesPlacementResultDto TryCreatePayments(BuyDatabasesPlacementDto model, Guid serviceId,
            decimal cost)
        {
            if (cost <= 0)
                return new BuyDatabasesPlacementResultDto {IsComplete = true};

            var abilityToCreatePaymentResult = abilityToCreatePaymentProvider.CheckAbilityAndGetPromisePaymentIfNeeded(
                model.AccountId, serviceId, cost,
                model.IsPromisePayment);

            var buyDatabasesPlacementResultDc = abilityToCreatePaymentResult.MapToBuyDatabasesPlacementResultDc();

            if (abilityToCreatePaymentResult.IsComplete)
                buyDatabasesPlacementResultDc.PaymentIds = CreatePayments(model, serviceId, cost);

            return buyDatabasesPlacementResultDc;
        }

        /// <summary>
        /// Создать платежи на указанную сумму 
        /// </summary>
        /// <param name="model">Модель для покупки лицензий на размещение инф. баз</param>
        /// <param name="serviceId">Id сервиса, по которому совершается оплата</param>
        /// <param name="amount">Сумма платежа</param>
        /// <returns>Список совершенных платежей</returns>
        private List<Guid> CreatePayments(BuyDatabasesPlacementDto model, Guid serviceId, decimal amount)
        {
            var serviceResConfig =
                resourcesConfigurationDataProvider.GetResourcesConfiguration(model.AccountId, serviceId);

            var paidServiceDescription =
                CreatePaidServiceDescription(model.AccountId, model.SystemServiceType,
                    model.DatabasesForPlacementCount);

            var operationResult = createPaymentHelper.MakePayment(serviceResConfig,
                $"Покупка сервиса {paidServiceDescription}", amount, PaymentSystem.ControlPanel, PaymentType.Outflow,
                PaymentConst.OriginDetails);

            if (operationResult.OperationStatus != PaymentOperationResult.Ok || !operationResult.PaymentIds.Any())
                throw new InvalidOperationException(
                    $"Ошибка проведения платежа для покупки сервиса {paidServiceDescription} на сумму {amount}");

            return operationResult.PaymentIds;
        }

        /// <summary>
        /// Создать описание оплачиваемого сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="resourceType">Тип ресурса системной услуги</param>
        /// <param name="databasesCount">Кол-во баз для покупки</param>
        /// <returns>Описание оплачиваемого сервиса</returns>
        private string CreatePaidServiceDescription(Guid accountId, ResourceType resourceType, int databasesCount)
        {
            var paymentDescription = $"\"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)}\"";

            var serviceTypeName = resourceType.Description().ToLower();
            var serviceTypeDefinition = resourceType == ResourceType.ServerDatabasePlacement
                ? $"({serviceTypeName} - {databasesCount})"
                : $"(добавлено баз: +{databasesCount})";

            return $"{paymentDescription} {serviceTypeDefinition}";
        }

        /// <summary>
        /// Посчитать стоимость размещения инф. баз для аккаунта
        /// </summary>
        /// <param name="model">Модель для покупки лицензий на размещение инф. баз</param>
        /// <returns>Модель результата подсчета стоимости
        /// размещения инф. баз для аккаунта</returns>
        private CalculationCostOfPlacementDatabasesResultDto CalculateCostOfPlacementDatabases(
            BuyDatabasesPlacementDto model) =>
            calculateCostOfPlacementDatabaseProvider.CalculateCostOfPlacementDatabases(
                new CalculateCostOfPlacementDatabasesDto
                {
                    AccountId = model.AccountId,
                    SystemServiceType = model.SystemServiceType,
                    DatabasesForPlacementCount = model.DatabasesForPlacementCount,
                    PaidDate = model.PaidDate,
                });
    }
}
