﻿using Clouds42.MyDatabaseService.BuyDatabasePlacement.Providers;
using Clouds42.MyDatabaseService.Calculator.Managers;
using Clouds42.MyDatabaseService.Calculator.Providers;
using Clouds42.MyDatabaseService.CardData.Managers;
using Clouds42.MyDatabaseService.CardData.Providers;
using Clouds42.MyDatabaseService.Contracts.BuyDatabasePlacement.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces;
using Clouds42.MyDatabaseService.Contracts.CardData.Interfaces;
using Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Management.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.MyDatabaseService.CreateAccountDatabases.Helpers;
using Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using Clouds42.MyDatabaseService.CreateAccountDatabases.Providers;
using Clouds42.MyDatabaseService.Customizers.CostByLocaleCustomizers;
using Clouds42.MyDatabaseService.Data.Providers;
using Clouds42.MyDatabaseService.LimitOnFreeCreationDbForAccount.Providers;
using Clouds42.MyDatabaseService.Management.Providers;
using Clouds42.MyDatabaseService.Operations.Providers;
using Clouds42.MyDatabaseService.Operations.Selectors;
using Clouds42.MyDatabaseService.Triggers;
using Clouds42.MyDatabaseService.Triggers.Selectors;
using Clouds42.MyDatabaseService.TypeData.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.MyDatabaseService
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddMyDatabase(this IServiceCollection services)
        {
            services.AddTransient<ILimitOnFreeCreationDbForAccountDataProvider, LimitOnFreeCreationDbForAccountDataProvider>();
            services.AddTransient<IUpdateLimitOnFreeCreationDbForAccountProvider, UpdateLimitOnFreeCreationDbForAccountProvider>();
            services.AddTransient<ICreateLimitOnFreeCreationDbForAccount, CreateLimitOnFreeCreationDbForAccount>();
            services.AddTransient<IBuyDatabasesPlacementProvider, BuyDatabasesPlacementProvider>();
            services.AddTransient<IAbilityToPayForMyDatabasesServiceProvider, AbilityToPayForMyDatabasesServiceProvider>();
            services.AddTransient<ICreateAccountDatabasesProvider, CreateAccountDatabasesProvider>();
            services.AddTransient<IApplyOrPayForAllMyDatabasesServiceChanges, ApplyOrPayForAllMyDatabasesServiceChanges>();
            services.AddTransient<IMyDatabasesCalculationCostDataProvider, MyDatabasesCalculationCostDataProvider>();
            services.AddTransient<ICalculateCostOfAcDbAccessesProvider, CalculateCostOfAcDbAccessesProvider>();
            services.AddTransient<ICalculateCostOfPlacementDatabaseProvider, CalculateCostOfPlacementDatabaseProvider>();
            services.AddTransient<IMyDatabasesServiceCardDataProvider, MyDatabasesServiceCardDataProvider>();
            services.AddTransient<IMyDatabasesServiceCostByLocaleCustomizerFactory, MyDatabasesServiceCostByLocaleCustomizerFactory>();
            services.AddTransient<IMyDatabasesServiceCostForLocaleCustomizer, MyDatabasesServiceCostForUzLocaleCustomizer>();
            services.AddTransient<IMyDatabasesServiceCostForLocaleCustomizer, MyDatabasesServiceCostForKzLocaleCustomizer>();
            services.AddTransient<IMyDatabasesServiceCostForLocaleCustomizer, MyDatabasesServiceCostForUaLocaleCustomizer>();
            services.AddTransient<IMyDatabasesServiceCostForLocaleCustomizer, MyDatabasesServiceCostForRuLocaleCustomizer>();
            services.AddTransient<IMyDatabasesServiceDataProvider, MyDatabasesServiceDataProvider>();
            services.AddTransient<IMyDatabasesServiceManagementDataProvider, MyDatabasesServiceManagementDataProvider>();
            services.AddTransient<IManageMyDatabasesServiceProvider, ManageMyDatabasesServiceProvider>();
            services.AddTransient<IRecalculateMyDatabasesServiceForAccountProvider, RecalculateMyDatabasesServiceForAccountProvider>();
            services.AddTransient<IMakeMyDatabasesServiceForAccountFreeProvider, MakeMyDatabasesServiceForAccountFreeProvider>();
            services.AddTransient<IRecalculateMyDatabasesServiceResourcesProvider, RecalculateMyDatabasesServiceResourcesProvider>();
            services.AddTransient<IActualizeMyDatabasesResourcesProvider, ActualizeMyDatabasesResourcesProvider>();
            services.AddTransient<IChargeFreeMyDatabasesResourcesProvider, ChargeFreeMyDatabasesResourcesProvider>();
            services.AddTransient<IOnDisabledMyDatabasesServiceTypeForUsersTrigger, OnDisabledMyDatabasesServiceTypeForUsersTrigger>();
            services.AddTransient<IOnDisabledRent1CForUsersTrigger, OnDisabledRent1CForUsersTrigger>();
            services.AddTransient<IOnChangeDatabaseTemplateTrigger, OnChangeDatabaseTemplateTrigger>();
            services.AddTransient<IOnChangeDatabaseTypeTrigger, OnChangeDatabaseTypeTrigger>();
            services.AddTransient<IOnChangeDatabaseStateTrigger, OnChangeDatabaseStateTrigger>();
            services.AddTransient<IOnRegisterDatabaseAfterServiceActivationTrigger, OnRegisterDatabaseAfterServiceActivationTrigger>();
            services.AddTransient<IMyDatabasesServiceTypeDataProvider, MyDatabasesServiceTypeDataProvider>();
            services.AddTransient<IMyDatabasesServiceTypeBillingDataProvider, MyDatabasesServiceTypeBillingDataProvider>();
            services.AddTransient<IMyDatabasesServiceTypeResourcesDataProvider, MyDatabasesServiceTypeResourcesDataProvider>();
            services.AddTransient<MyDatabasesServiceStateDataSelector>();
            services.AddTransient<RecalculateMyDatabasesServiceResourcesDataSelector>();
            services.AddTransient<MyDatabasesServiceResourcesToDisabledSelector>();
            services.AddTransient<CreatingAccountDatabasesHelper>();
            services.AddTransient<AcDbAccessesToRemoveForDisabledServiceTypeDataSelector>();
            services.AddTransient<CreateDatabaseManager>();
            services.AddTransient<UsersDataWhoHaveAccessToDbButNoResourcesSelector>();
            services.AddTransient<MyDatabasesServiceCardDataManager>();
            services.AddTransient<CreateAccountDatabasesManager>();
            services.AddTransient<CalculateCostOfPlacementDatabaseManager>();
            services.AddTransient<CalculateCostOfAcDbAccessesManager>();

            return services;
        }
    }
}
