﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.MyDatabaseService.Data.Providers
{
    /// <summary>
    /// Провайдер для работы с данными сервиса "Мои информационные базы"
    /// </summary>
    internal class MyDatabasesServiceDataProvider : IMyDatabasesServiceDataProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly IMyDatabasesServiceTypeBillingDataProvider _myDatabasesServiceTypeBillingDataProvider;
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider;
        private readonly Lazy<Guid> _serviceId;

        public MyDatabasesServiceDataProvider(IUnitOfWork dbLayer,
            IMyDatabasesServiceTypeBillingDataProvider myDatabasesServiceTypeBillingDataProvider,
            IAccountConfigurationDataProvider accountConfigurationDataProvider)
        {
            _dbLayer = dbLayer;
            _myDatabasesServiceTypeBillingDataProvider = myDatabasesServiceTypeBillingDataProvider;
            _accountConfigurationDataProvider = accountConfigurationDataProvider;
            _serviceId = new Lazy<Guid>(GetServiceId);
        }

        /// <summary>
        /// Получить Id сервиса "Мои информационные базы"
        /// </summary>
        /// <returns>Id сервиса "Мои информационные базы"</returns>
        public Guid GetMyDatabasesServiceId() => _serviceId.Value;

        /// <summary>
        /// Получить Id услуги от которой зависит сервис
        /// (услуга сервиса) "Мои информационные базы"
        /// </summary> 
        /// <returns>Id услуги от которой зависит сервис</returns>
        public Guid GetServiceTypeIdOnWhichServiceDepends() =>
            _dbLayer.BillingServiceTypeRepository
                .FirstOrDefault(st => st.SystemServiceType == ResourceType.MyEntUserWeb)?.Id ??
            throw new NotFoundException("Не удалось получить web услугу сервиса Аренда 1С");

        /// <summary>
        /// Получить список Id услуг сервиса "Мои информационные базы"
        /// </summary>
        /// <returns>Список Id услуг</returns>
        public IEnumerable<Guid> GetMyDatabasesServiceTypeIds() => _dbLayer.BillingServiceTypeRepository
            .WhereLazy(st => st.ServiceId == _serviceId.Value).Select(st => st.Id).AsEnumerable();

        /// <summary>
        /// Получить данные с краткой информацией
        /// по услугам сервиса "Мои инф. базы"
        /// </summary>
        /// <returns>Список моделей данных с краткой инфомацией
        /// по услугам сервиса "Мои инф. базы"</returns>
        public IEnumerable<MyDatabasesServiceTypeShortDataDto> GetShortDataByMyDatabasesServiceTypes() => _dbLayer
            .BillingServiceTypeRepository
            .WhereLazy(st => st.ServiceId == _serviceId.Value && st.SystemServiceType != null).Select(st =>
                new MyDatabasesServiceTypeShortDataDto
                {
                    Id = st.Id,
                    SystemServiceType = st.SystemServiceType.Value
                }).AsEnumerable();

        /// <summary>
        /// Получить список пар ID-Название активных услуг сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="locale"></param>
        /// <returns>Список пар ID-Название активных услуг сервиса "Мои информационные базы"</returns>
        public async Task<List<KeyValueDto<Guid>>> GetAccountActivePaidServiceTypesIdNamePairs(Guid accountId,
            string locale)
        {
            if (await _accountConfigurationDataProvider.IsVipAccountAsync(accountId) ||
                locale != LocaleConst.Russia)
                return [];

            return (await _dbLayer.ResourceRepository
                .AsQueryableNoTracking()
                .Join(_dbLayer.RateRepository.AsQueryable(), x => x.BillingServiceTypeId, z => z.BillingServiceTypeId,
                        (resource, rate) => new { resource, rate })
                .Where(x =>
                    (x.resource.BillingServiceType.SystemServiceType == ResourceType.AccessToConfiguration1C || x.resource.BillingServiceType.SystemServiceType == ResourceType.AccessToServerDatabase) &&
                    (x.resource.AccountId == accountId || x.resource.AccountSponsorId == accountId) &&
                     x.resource.Subject.HasValue && x.rate.Cost>0 &&
                    (x.resource.Cost > 0 || x.resource.AccountSponsorId.HasValue))
                .Select(x => new { ServiceTypeId = x.resource.BillingServiceType.Id, x.resource.BillingServiceType.SystemServiceType, x.resource.BillingServiceType.Id, x.resource.BillingServiceType.Name })
                .ToListAsync())
                .DistinctBy(x => x.ServiceTypeId)
                .OrderBy(x => x.SystemServiceType)
                .Select(x => new KeyValueDto<Guid>(x.Id, x.Name))
                .ToList();
        }

        /// <summary>
        /// Получить краткие данные по сервису "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Краткие данные по сервису "Мои инф. базы"</returns>
        public MyDatabasesServiceShortDataDto GetMyDatabasesServiceShortData(Guid accountId)
        {
            var serviceTypesBillingData =
                _myDatabasesServiceTypeBillingDataProvider.GetBillingDataForMyDatabasesServiceTypes(accountId);

            var databasePlacementServiceTypeData =
                SelectServiceTypeBillingData(serviceTypesBillingData, ResourceType.CountOfDatabasesOverLimit);

            var serverDatabasePlacementServiceTypeData =
                SelectServiceTypeBillingData(serviceTypesBillingData, ResourceType.ServerDatabasePlacement);

            return new MyDatabasesServiceShortDataDto
            {
                ServerDatabasePlacementCost = serverDatabasePlacementServiceTypeData.CostPerOneLicense,
                DatabasePlacementCost = databasePlacementServiceTypeData.CostPerOneLicense,
                LimitOnFreeCreationDb = databasePlacementServiceTypeData.LimitOnFreeLicenses,
                CountDatabasesOverLimit = databasePlacementServiceTypeData.VolumeInQuantity,
                ServerDatabasesCount = serverDatabasePlacementServiceTypeData.VolumeInQuantity,
                TotalAmountForDatabases = databasePlacementServiceTypeData.TotalAmount,
                TotalAmountForServerDatabases = serverDatabasePlacementServiceTypeData.TotalAmount
            };
        }

        /// <summary>
        /// Выбрать данные биллинга услуги
        /// по типу системной услуги
        /// </summary>
        /// <param name="serviceTypesBillingData">Данные биллинга по услугам сервиса "Мои инф. базы"</param>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Данные биллинга по услуге</returns>
        private static MyDatabasesServiceTypeBillingDataDto SelectServiceTypeBillingData(
            List<MyDatabasesServiceTypeBillingDataDto> serviceTypesBillingData, ResourceType systemServiceType) =>
            serviceTypesBillingData.FirstOrDefault(st => st.SystemServiceType == systemServiceType) ??
            throw new NotFoundException(
                $"Не удалось получить данные биллинга для услуги {systemServiceType.Description()}");

        /// <summary>
        /// Получить Id сервиса
        /// </summary>
        /// <returns>Id сервиса "Мои информационные базы"</returns>
        private Guid GetServiceId() =>
            _dbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == Clouds42Service.MyDatabases)?.Id ??
            throw new NotFoundException("Не удалось получить сервис - Мои информационные базы");
    }
}
