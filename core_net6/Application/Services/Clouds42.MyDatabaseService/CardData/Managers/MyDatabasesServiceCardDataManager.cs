﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.CardData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.CardData.Managers
{
    /// <summary>
    /// Менеджер получения данных для карточки сервиса "Мои инф. базы"
    /// </summary>
    [Obsolete("Удалить после полного перехода на React")]
    public class MyDatabasesServiceCardDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IMyDatabasesServiceCardDataProvider myDatabasesServiceCardDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить данные карточки по критериям
        /// </summary>
        /// <param name="serviceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Данные карточки</returns>
        [Obsolete("Использовать GetMyDatabasesServiceInfoQueryHandler. Удалить после полного перехода на React")]
        public ManagerResult<MyDatabasesServiceTypeCardDto> GetCardDataByCriteria(Guid serviceTypeId, Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => accountId);
                var result = myDatabasesServiceCardDataProvider.GetCardDataByCriteria(serviceTypeId, accountId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var errorMessage = $"[При получении данных для карточки услуги сервиса {serviceTypeId} Мои инф. базы произошла ошибка.]";
                _handlerException.Handle(ex, errorMessage);
                return PreconditionFailed<MyDatabasesServiceTypeCardDto>($"{errorMessage} Причина: {ex.Message}");
            }
        }
    }
}
