﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.CardData.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.CardData.Providers
{
    /// <summary>
    /// Провайдер получения данных карточки сервиса "Мои инф. базы"
    /// </summary>
    internal class MyDatabasesServiceCardDataProvider(
        IServiceAccountDataHelper serviceAccountDataHelper,
        IUnitOfWork dbLayer,
        IServiceStatusHelper serviceStatusHelper,
        ICloudLocalizer cloudLocalizer,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IMyDatabasesServiceCardDataProvider
    {
        /// <summary>
        /// Получить данные карточки по критериям
        /// </summary>
        /// <param name="serviceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Данные карточки</returns>
        public MyDatabasesServiceTypeCardDto GetCardDataByCriteria(Guid serviceTypeId, Guid accountId)
        {
            serviceAccountDataHelper.CheckAbilityToManageService(accountId);

            var myDatabasesServiceTypeCard =
            (
                from serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy()
                join configurationRelation in dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>()
                        .WhereLazy() on serviceType.Id equals configurationRelation.ServiceTypeId into
                    configurationRelations
                from configRelation in configurationRelations.Take(1).DefaultIfEmpty()
                join dbTemplateRelation in dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().WhereLazy()
                    on configRelation.Configuration1CName equals dbTemplateRelation.Configuration1CName into
                    dbTemplateRelations
                from dbTemplateRelation in dbTemplateRelations.Take(1).DefaultIfEmpty()
                join dbTemplate in dbLayer.DbTemplateRepository.WhereLazy() on dbTemplateRelation.DbTemplateId equals
                    dbTemplate.Id into dbTemplates
                from dbTemplate in dbTemplates.Take(1).DefaultIfEmpty()
                where serviceType.Id == serviceTypeId
                select new MyDatabasesServiceTypeCardDto
                {
                    Id = serviceType.Id,
                    BillingServiceId = serviceType.ServiceId,
                    Description = serviceType.Description,
                    DisplayedName = serviceType.Name,
                    SystemServiceType = serviceType.SystemServiceType
                }
            ).FirstOrDefault();

            InitServiceTypeStatusModel(myDatabasesServiceTypeCard, accountId);
            InitDatabasesCountData(accountId, myDatabasesServiceTypeCard);
            return myDatabasesServiceTypeCard;
        }

        /// <summary>
        /// Инициализировать модель статуса услуги сервиса
        /// </summary>
        /// <param name="myDatabasesServiceTypeCardDto">Модель данных карточки</param>
        /// <param name="accountId">Id аккаунта</param>
        private void InitServiceTypeStatusModel(MyDatabasesServiceTypeCardDto myDatabasesServiceTypeCardDto,
            Guid accountId)
        {

            var rent1CResConfig =
                dbLayer.ResourceConfigurationRepository.FirstOrDefault(x =>
                    x.AccountId == accountId && x.BillingService.SystemService == Clouds42Service.MyEnterprise) ??
                throw new NotFoundException(
                    $"У аккаунта '{accountId}' не найдена ресурс конфигурация сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId)}.");

            var serviceTypeStatus = serviceStatusHelper.CreateServiceStatusModel(accountId, rent1CResConfig);

            myDatabasesServiceTypeCardDto.ServiceTypeExpireDate = rent1CResConfig.ExpireDateValue;
            myDatabasesServiceTypeCardDto.ServiceTypeStatus = serviceTypeStatus;

            var displayedExpireDate = serviceTypeStatus.PromisedPaymentIsActive &&
                                      serviceTypeStatus.PromisedPaymentExpireDate <= rent1CResConfig.ExpireDate
                ? serviceTypeStatus.PromisedPaymentExpireDate
                : rent1CResConfig.ExpireDate;

            if (!displayedExpireDate.HasValue)
                throw new InvalidOperationException(
                    $"Не удалось получить дату окончания сервиса для аккаунта {accountId}");

            myDatabasesServiceTypeCardDto.DisplayedExpireDate = displayedExpireDate.Value;
            myDatabasesServiceTypeCardDto.CurrencyCode =
                accountConfigurationDataProvider.GetAccountLocale(accountId).Currency;
        }

        /// <summary>
        /// Инициализировать данные по кол-ву инф. баз
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="myDatabasesServiceTypeCardDto">Модель данных карточки</param>
        private void InitDatabasesCountData(Guid accountId, MyDatabasesServiceTypeCardDto myDatabasesServiceTypeCardDto)
        {
            var myDatabasesServiceShortData = myDatabasesServiceDataProvider.GetMyDatabasesServiceShortData(accountId);
            var isAccessToServerDbServiceType =
                myDatabasesServiceTypeCardDto.SystemServiceType == ResourceType.AccessToServerDatabase;

            myDatabasesServiceTypeCardDto.DatabasesCount = isAccessToServerDbServiceType
                ? myDatabasesServiceShortData.ServerDatabasesCount
                : myDatabasesServiceShortData.CountDatabasesOverLimit;
            myDatabasesServiceTypeCardDto.CostPerOneDatabase = isAccessToServerDbServiceType
                ? myDatabasesServiceShortData.ServerDatabasePlacementCost
                : myDatabasesServiceShortData.DatabasePlacementCost;
            myDatabasesServiceTypeCardDto.TotalDatabasesCost = isAccessToServerDbServiceType
                ? myDatabasesServiceShortData.TotalAmountForServerDatabases
                : myDatabasesServiceShortData.TotalAmountForDatabases;
        }
    }
}
