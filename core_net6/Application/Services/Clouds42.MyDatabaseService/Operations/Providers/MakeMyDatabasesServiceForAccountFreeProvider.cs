﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;

namespace Clouds42.MyDatabaseService.Operations.Providers
{
    /// <summary>
    /// Провайдер, делающий сервис "Мои информационные базы" бесплатным для аккаунта
    /// </summary>
    internal class MakeMyDatabasesServiceForAccountFreeProvider(
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IUpdateAccountRateProvider updateAccountRateProvider,
        IAccountRateDataProvider accountRateDataProvider,
        IResourceDataProvider resourceDataProvider,
        ICreateAccountRateProvider createAccountRateProvider,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException,
        ISender sender)
        : IMakeMyDatabasesServiceForAccountFreeProvider

    {
        /// <summary>
        /// Сделать сервис "Мои информационные базы" бесплатным для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void MakeServiceFree(Guid accountId)
        {
            var serviceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId();

            var message =
                $"Выполнение операции: Сделать сервис 'Мои информационные базы' бесплатным для аккаунта '{accountId}'";

            logger.Info(message);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                MakeResourcesFreeForAccount(accountId, serviceId);
                MakeAccountRatesFree(accountId);

                sender.Send(new RecalculateResourcesConfigurationCostCommand([accountId],
                    [serviceId], Clouds42Service.MyDatabases)).Wait();

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка cделать сервис 'Мои информационные базы' бесплатным для аккаунта] '{accountId}'");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Сделать ресурсы сервиса "Мои информационные базы" бесплатными для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        private void MakeResourcesFreeForAccount(Guid accountId, Guid serviceId)
        {
            var resources = resourceDataProvider.GetAllAccountResourcesForService(accountId, serviceId).ToList();

            var toUpdate = resources.Select(x =>
            {
                x.Cost = 0;

                return x;

            }).ToList();

            dbLayer.BulkUpdate(toUpdate);
            dbLayer.Save();
        }

        /// <summary>
        /// Сделать бесплатные тарифы услуг на аккаунт
        /// для сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        private void MakeAccountRatesFree(Guid accountId) => myDatabasesServiceDataProvider
            .GetMyDatabasesServiceTypeIds().ToList().ForEach(id => MakeAccountRateFreeForServiceType(accountId, id));

        /// <summary>
        /// Сделать бесплатный тарифы услуги на аккаунт
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        private void MakeAccountRateFreeForServiceType(Guid accountId, Guid serviceTypeId)
        {
            var accountRate = accountRateDataProvider.GetAccountRate(accountId, serviceTypeId);

            if (accountRate != null)
            {
                updateAccountRateProvider.Update(accountRate, 0);
                return;
            }

            createAccountRateProvider.Create(new CreateAccountRateDto
            {
                AccountId = accountId,
                BillingServiceTypeId = serviceTypeId,
                Cost = 0
            });
        }
    }
}
