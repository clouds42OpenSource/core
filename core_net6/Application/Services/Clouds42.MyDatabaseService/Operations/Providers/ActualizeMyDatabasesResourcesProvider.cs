﻿using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Operations.Providers
{
    /// <summary>
    /// Актуализировать ресурсы сервиса "Мои информационные базы"
    /// </summary>
    internal class ActualizeMyDatabasesResourcesProvider(
        IUnitOfWork dbLayer,
        IMyDatabasesResourceDataProvider myDatabasesResourceDataProvider,
        IUpdateMyDatabasesResourceProvider updateMyDatabasesResourceProvider,
        IRecalculateMyDatabasesServiceResourcesProvider recalculateMyDatabasesServiceResourcesProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : IActualizeMyDatabasesResourcesProvider
    {
        /// <summary>
        /// Актуализировать ресурсы сервиса "Мои информационные базы"
        /// c пересчетом значений количества баз
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void ActualizeForAccount(Guid accountId)
        {
            var message = $"Актуализация ресурсов сервиса 'Мои информационные базы' для аккаунта '{accountId}'";

            var actualValuesForMyDatabasesResources =
                myDatabasesResourceDataProvider.GetActualValuesForMyDatabasesResourcesForAccount(accountId);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                updateMyDatabasesResourceProvider.SetSingleValueForDatabasesCount(accountId,
                    ResourceType.CountOfDatabasesOverLimit, actualValuesForMyDatabasesResources.DatabasesCount);

                updateMyDatabasesResourceProvider.SetSingleValueForDatabasesCount(accountId,
                    ResourceType.ServerDatabasePlacement, actualValuesForMyDatabasesResources.ServerDatabasesCount);

                recalculateMyDatabasesServiceResourcesProvider.RecalculateResources(accountId);

                dbScope.Commit();
                logger.Trace($"{message} завершилась успешно");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка актуализации ресурсов сервиса 'Мои информационные базы' для аккаунта] '{accountId}");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
