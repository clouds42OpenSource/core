﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;

namespace Clouds42.MyDatabaseService.Operations.Providers
{
    /// <summary>
    /// Провайдер по пересчету стоимости сервиса "Мои информационные базы" для аккаунта
    /// Удаляет тарифы на аккаунт, обновляет стоимость ресурсов согласно тарифу
    /// и пересчитывает стоимость сервиса
    /// </summary>
    internal class RecalculateMyDatabasesServiceForAccountProvider(
        IUnitOfWork dbLayer,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IRemoveAccountRateProvider removeAccountRateProvider,
        IRecalculateMyDatabasesServiceResourcesProvider recalculateMyDatabasesServiceResourcesProvider,
        ILogger42 logger,
        IHandlerException handlerException,
        ISender sender)
        : IRecalculateMyDatabasesServiceForAccountProvider
    {
        /// <summary>
        /// Пересчитать стоимость сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void RecalculateServiceCost(Guid accountId)
        {
            var message =
                $"Выполнение операции: Пересчитать стоимость сервиса 'Мои информационные базы' для аккаунта '{accountId}'";

            var serviceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId();

            logger.Info(message);

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                removeAccountRateProvider.RemoveAccountRatesByService(accountId, serviceId);
                recalculateMyDatabasesServiceResourcesProvider.RecalculateResources(accountId);

                sender.Send(new RecalculateResourcesConfigurationCostCommand([accountId],
                    [serviceId], Clouds42Service.MyDatabases)).Wait();

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка  Пересчитать стоимость сервиса 'Мои информационные базы' для аккаунта] '{accountId}'");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
