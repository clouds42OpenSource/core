﻿using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Models;
using Clouds42.MyDatabaseService.Operations.Selectors;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Operations.Providers
{
    /// <summary>
    /// Провайдер для пересчета стоимости ресурсов сервиса "Мои информационные базы"
    /// </summary>
    internal class RecalculateMyDatabasesServiceResourcesProvider(
        IUnitOfWork dbLayer,
        RecalculateMyDatabasesServiceResourcesDataSelector recalculateMyDatabasesServiceResourcesDataSelector,
        ILimitOnFreeCreationDbForAccountDataProvider limitOnFreeCreationDbForAccountDataProvider,
        IHandlerException handlerException)
        : IRecalculateMyDatabasesServiceResourcesProvider
    {
        /// <summary>
        /// Пересчитать стоимость ресурсов сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void RecalculateResources(Guid accountId)
        {
            var resourcesDataForRecalculation =
                recalculateMyDatabasesServiceResourcesDataSelector.SelectData(accountId).ToList();

            if (!resourcesDataForRecalculation.Any())
                return;

            try
            {
                var resourcesForUpdate = resourcesDataForRecalculation.Select(data =>
                {
                     RecalculateResource(data, accountId);

                     return data.Resource;
                }).ToList();

                dbLayer.BulkUpdate(resourcesForUpdate);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка пересчета стоимости ресурсов сервиса 'Мои информационные базы']");
                throw;
            }
        }

        /// <summary>
        /// Пересчитать стоимость ресурсов услуги сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        public void RecalculateResourcesForServiceType(Guid accountId, Guid serviceTypeId) =>
            RecalculateResourcesForServiceType(accountId,
                GetRecalculationResourcesDataForServiceType(accountId, serviceTypeId).ToList());

        /// <summary>
        /// Пересчитать стоимость ресурсов услуги сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги</param>
        public void RecalculateResourcesForServiceType(Guid accountId, ResourceType systemServiceType) =>
            RecalculateResourcesForServiceType(accountId,
                GetRecalculationResourcesDataForServiceType(accountId, systemServiceType).ToList());

        /// <summary>
        /// Пересчитать стоимость ресурсов услуги сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="recalculationResourcesData">Данные для пересчета стоимости ресурсов услуги</param>
        private void RecalculateResourcesForServiceType(Guid accountId,
            List<RecalculateMyDatabasesServiceResourceDataModel> recalculationResourcesData)
        {

            if (!recalculationResourcesData.Any())
                return;

            try
            {
                var resourcesForUpdate = recalculationResourcesData
                    .Select(data =>{ RecalculateResource(data, accountId); return data.Resource; })
                    .ToList();

                dbLayer.BulkUpdate(resourcesForUpdate);
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка пересчета стоимости ресурсов сервиса 'Мои информационные базы']");

                throw;
            }
        }

        /// <summary>
        /// Получить данные для пересчета стоимости ресурсов услуги
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Данные для пересчета стоимости ресурсов услуги</returns>
        private IEnumerable<RecalculateMyDatabasesServiceResourceDataModel> GetRecalculationResourcesDataForServiceType(
            Guid accountId,
            Guid serviceTypeId) =>
            recalculateMyDatabasesServiceResourcesDataSelector.SelectData(accountId)
                .Where(r => r.Resource.BillingServiceTypeId == serviceTypeId);

        /// <summary>
        /// Получить данные для пересчета стоимости ресурсов услуги
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Данные для пересчета стоимости ресурсов услуги</returns>
        private IEnumerable<RecalculateMyDatabasesServiceResourceDataModel> GetRecalculationResourcesDataForServiceType(
            Guid accountId, ResourceType systemServiceType) =>
            recalculateMyDatabasesServiceResourcesDataSelector.SelectData(accountId)
                .Where(r => r.ResourceType == systemServiceType);

        /// <summary>
        /// Пересчитать стоимость ресурса
        /// </summary>
        /// <param name="model">Модель данных для пересчета стоимости ресурса
        /// сервиса "Мои информационные базы"</param>
        /// <param name="accountId">Id аккаунта</param>
        private void RecalculateResource(RecalculateMyDatabasesServiceResourceDataModel model, Guid accountId)
        {
            var newCost = GetResourceCost(model, accountId);
            model.Resource.Cost = newCost;
        }

        /// <summary>
        /// Получить стоимость ресурса
        /// </summary>
        /// <param name="model">Модель данных для пересчета стоимости ресурса
        /// сервиса "Мои информационные базы"</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Стоимость ресурса</returns>
        private decimal GetResourceCost(RecalculateMyDatabasesServiceResourceDataModel model, Guid accountId)
        {
            if (!model.ActualDatabasesCount.HasValue)
                return model.ActualCost;

            if (model.ResourceType != ResourceType.CountOfDatabasesOverLimit)
                return model.ActualDatabasesCount.Value * model.ActualCost;

            return limitOnFreeCreationDbForAccountDataProvider.GetDatabasesCountForWhichYouNeedToPay(accountId,
                       model.ActualDatabasesCount.Value) * model.ActualCost;
        }
    }
}
