﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;

namespace Clouds42.MyDatabaseService.Operations.Providers
{
    /// <summary>
    /// Провайдер для начисления бесплатных ресурсов
    /// по сервису "Мои информационные базы"
    /// </summary>
    internal class ChargeFreeMyDatabasesResourcesProvider(
        IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider,
        ICreateMyDatabasesResourceProvider createMyDatabasesResourceProvider,
        IUnitOfWork dbLayer,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        IHandlerException handlerException,
        ISender sender)
        : IChargeFreeMyDatabasesResourcesProvider
    {
        private readonly Lazy<int> _freeResourceCountAtDemoPeriod = new(CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod);

        /// <summary>
        /// Начислить бесплатные ресурсы по сервису
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void ChargeFreeResourcesForAccount(Guid accountId)
        {
            var myDatabasesServiceTypesData =
                myDatabasesServiceTypeDataProvider.GetMyDatabasesServiceTypesData(accountId).ToList();

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var serviceTypesResources = SelectServiceTypesByAccountUser(myDatabasesServiceTypesData)
                    .SelectMany(data => GenerateAccountResourcesForServiceType(accountId, data)).ToList();

                InsertServiceTypesResources(serviceTypesResources);

                CreateFreeMyDatabasesResource(accountId, ResourceType.CountOfDatabasesOverLimit);

                CreateFreeMyDatabasesResource(accountId, ResourceType.ServerDatabasePlacement);

                sender.Send(new RecalculateResourcesConfigurationCostCommand([accountId],
                    [myDatabasesServiceDataProvider.GetMyDatabasesServiceId()], Clouds42Service.MyDatabases)).Wait();

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка начисления бесплатных ресурсов по сервису 'Мои инф. базы' для аккаунта] '{accountId}'");

                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать бесплатный/пустой ресурс услуги
        /// сервиса "Мои информационные базы" 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги</param>
        private void CreateFreeMyDatabasesResource(Guid accountId, ResourceType systemServiceType) =>
            createMyDatabasesResourceProvider.Create(new CreateMyDatabasesResourceDto
            {
                AccountId = accountId,
                SystemServiceType = systemServiceType,
                ResourceCost = decimal.Zero,
                ActualDatabasesCount = 0,
                PaidDatabasesCount = 0
            });

        /// <summary>
        /// Сгенерировать список ресурсов аккаунта для услуги сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="myDatabasesServiceTypeData">Модель данных услуги сервиса "Мои инф. базы"</param>
        /// <returns>Список ресурсов аккаунта для услуги сервиса</returns>
        private List<Resource> GenerateAccountResourcesForServiceType(Guid accountId, MyDatabasesServiceTypeDataDto myDatabasesServiceTypeData)
        {
            var serviceTypeResources = new List<Resource>();
            for (var index = 0; index < _freeResourceCountAtDemoPeriod.Value; index++)
            {
                serviceTypeResources.Add(new CreateResourceDto
                {
                    AccountId = accountId,
                    BillingServiceTypeId = myDatabasesServiceTypeData.ServiceTypeId,
                    IsFree = false,
                    Cost = myDatabasesServiceTypeData.ServiceTypeCost
                }.MapToResource());
            }

            return serviceTypeResources;
        }

        /// <summary>
        /// Записать в БД ресурсы услуг
        /// </summary>
        /// <param name="serviceTypesResources">Список ресурсов по услугам сервиса</param>
        private void InsertServiceTypesResources(IEnumerable<Resource> serviceTypesResources)
        {
            dbLayer.BulkInsert(serviceTypesResources);
            dbLayer.Save();
        }

        /// <summary>
        /// Выбрать данные услуг с типом биллинга "По пользователю"
        /// </summary>
        /// <param name="myDatabasesServiceTypesData">Список данных услуг
        /// сервиса "Мои инф. базы"</param>
        /// <returns>Список данных услуг сервиса "Мои инф. базы"
        /// c типом биллинга "По пользователю"</returns>
        private static IEnumerable<MyDatabasesServiceTypeDataDto>
            SelectServiceTypesByAccountUser(List<MyDatabasesServiceTypeDataDto> myDatabasesServiceTypesData) =>
            myDatabasesServiceTypesData.Where(data => data.BillingType == BillingTypeEnum.ForAccountUser);
    }
}
