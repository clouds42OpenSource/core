﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Models;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Operations.Selectors
{
    /// <summary>
    /// Класс для выбора данных, для пересчета стоимости ресурсов
    /// сервиса "Мои информационные базы"
    /// </summary>
    public class RecalculateMyDatabasesServiceResourcesDataSelector(
        IUnitOfWork dbLayer,
        IResourceDataProvider resourceDataProvider,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider)
    {
        /// <summary>
        /// Выбрать данные для пересчета стоимости ресурсов
        /// сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные для пересчета стоимости ресурсов
        /// сервиса "Мои информационные базы"</returns>
        public IQueryable<RecalculateMyDatabasesServiceResourceDataModel> SelectData(Guid accountId)
        {
            var serviceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId();

            return from resource in resourceDataProvider.GetAllAccountResourcesForService(accountId, serviceId)
                join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource.BillingServiceTypeId
                    equals serviceType.Id
                join rate in dbLayer.RateRepository.WhereLazy() on resource.BillingServiceTypeId equals rate
                    .BillingServiceTypeId
                join myDatabasesResource in dbLayer.GetGenericRepository<MyDatabasesResource>().WhereLazy() on resource
                        .Id
                    equals myDatabasesResource.ResourceId into myDatabasesResources
                from dbResource in myDatabasesResources.DefaultIfEmpty()
                join serviceTypeAccountRate in dbLayer.AccountRateRepository.WhereLazy(ar => ar.AccountId == accountId)
                    on resource.BillingServiceTypeId equals serviceTypeAccountRate.BillingServiceTypeId into
                    serviceTypeAccountRates
                from accountRate in serviceTypeAccountRates.Take(1).DefaultIfEmpty()
                select new RecalculateMyDatabasesServiceResourceDataModel
                {
                    Resource = resource,
                    ActualCost = accountRate == null ? rate.Cost : accountRate.Cost,
                    ActualDatabasesCount = dbResource.ActualDatabasesCount,
                    ResourceType = serviceType.SystemServiceType ?? ResourceType.AccessToConfiguration1C
                };
        }
    }
}