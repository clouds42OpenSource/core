﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.AcDbAccessesJobs.JobWrappers;
using Clouds42.CoreWorker.AcDbAccessesJobs.Params;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.MyDatabaseService.Triggers.Mappers;
using Clouds42.MyDatabaseService.Triggers.Selectors;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Triggers
{
    /// <summary>
    /// Триггер на отключение лицензии по услуге сервиса
    /// "Мои информационные базы" у пользователей
    /// </summary>
    internal class OnDisabledMyDatabasesServiceTypeForUsersTrigger(
        IUnitOfWork dbLayer,
        RemoveAccessesFromDatabaseJobWrapper removeAccessesFromDatabaseJobWrapper,
        AcDbAccessesToRemoveForDisabledServiceTypeDataSelector accessesToRemoveForDisabledServiceTypeDataSelector,
        ILogger42 logger)
        : IOnDisabledMyDatabasesServiceTypeForUsersTrigger
    {
        /// <summary>
        /// Выполнить триггер на отключение
        /// лицензии по услуге у пользователей
        /// </summary>
        /// <param name="model">Модель триггера на отключение лицензии
        /// по услуге сервиса "Мои информационные базы" у пользователей</param>
        public void Execute(OnDisabledMyDatabasesServiceTypeForUsersTriggerDto model)
        {
            var message =
                $"Выполнение триггера на отключение лицензии по услуге '{model.ServiceTypeId}' у пользователей";
            
                try
                {
                    RunTasksForRemoveAcDbAccessesByDisabledServiceType(model);

                    logger.Trace($"{message} завершилось успешно");
                }
                catch (Exception ex)
                {
                    logger.Trace($"{message} завершилось с ошибкой. : {ex.GetFullInfo()}");
                    throw;
                }
        }

        /// <summary>
        /// Запустить задачи для удаления доступов из инф. базы
        /// по отключенной услуге у пользователей
        /// </summary>
        /// <param name="model">Модель триггера на отключение лицензии
        /// по услуге у пользователей</param>
        private void RunTasksForRemoveAcDbAccessesByDisabledServiceType(
            OnDisabledMyDatabasesServiceTypeForUsersTriggerDto model)
        {
            var taskParameters = GetTaskParametersForRemoveAcDbAccesses(model).ToList();

            if (!taskParameters.Any())
                return;

            taskParameters.ForEach(taskParameter => removeAccessesFromDatabaseJobWrapper.Start(taskParameter));
        }

        /// <summary>
        /// Получить параметры задач для удаления доступов из инф. базы
        /// </summary>
        /// <param name="model">Модель триггера на отключение лицензии
        /// по услуге у пользователей</param>
        /// <returns>Параметры задач по удалению доступов из инф. базы</returns>
        private IEnumerable<ManageAcDbAccessesJobParams> GetTaskParametersForRemoveAcDbAccesses(
            OnDisabledMyDatabasesServiceTypeForUsersTriggerDto model)
        {
            if (GetSystemServiceType(model.ServiceTypeId) == ResourceType.AccessToServerDatabase)
                return accessesToRemoveForDisabledServiceTypeDataSelector
                    .SelectUsersWithAccessToServerDatabases(model.AccountUserIds)
                    .Select(data => data.MapToManageAcDbAccessesJobParams());

            return accessesToRemoveForDisabledServiceTypeDataSelector
                .SelectUsersWithAccessToDatabasesByServiceType(model.ServiceTypeId, model.AccountUserIds)
                .Select(data => data.MapToManageAcDbAccessesJobParams());
        }

        /// <summary>
        /// Получить тип системной услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тип системной услуги</returns>
        private ResourceType GetSystemServiceType(Guid serviceTypeId) =>
            dbLayer.BillingServiceTypeRepository.FirstOrDefault(st => st.Id == serviceTypeId)?.SystemServiceType ??
            throw new NotFoundException($"Не удалось определить тип системной услуги '{serviceTypeId}'");
    }
}
