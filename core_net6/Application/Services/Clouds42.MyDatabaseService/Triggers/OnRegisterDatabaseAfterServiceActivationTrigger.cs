﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Triggers
{
    /// <summary>
    /// Триггер на регистрацию инф. базы после активации сервиса
    /// </summary>
    internal class OnRegisterDatabaseAfterServiceActivationTrigger(IServiceProvider serviceProvider, ILogger42 logger)
        : OnChangeDatabaseBaseTrigger(serviceProvider), IOnRegisterDatabaseAfterServiceActivationTrigger
    {
        /// <summary>
        /// Выполнить триггер на регистрацию инф. базы после активации сервиса
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountUser">Пользователь</param>
        public void Execute(Guid accountDatabaseId, IAccountUser accountUser)
        {
            var serviceTypeCostDataForDatabase =
                MyDatabasesServiceTypeDataProvider.GetServiceTypeCostDataForDatabase(accountDatabaseId);

            var message = $"Выполнение триггера на регистрацию инф. базы '{accountDatabaseId}' после активации сервиса ";

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                if (NeedAttachResourceToUser(accountUser.Id, serviceTypeCostDataForDatabase.ServiceTypeId))
                    AttachResourcesToUsers(accountUser, serviceTypeCostDataForDatabase);

                IncreaseDatabasesCountWithRecalculatingCostResource(accountUser.AccountId,
                    ResourceType.CountOfDatabasesOverLimit);
                RecalculateMyDatabasesServiceCost(serviceTypeCostDataForDatabase.AccountId);
                dbScope.Commit();

                logger.Trace($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                logger.Trace($"{message} завершилось с ошибкой. : {ex.GetFullInfo()}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Признак необходимости прикреплять ресурс к пользователю
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="serviceTypeId">ID услуги</param>
        /// <returns>Признак необходимости прикреплять ресурс к пользователю</returns>
        private bool NeedAttachResourceToUser(Guid accountUserId, Guid serviceTypeId)
            => DbLayer.ResourceRepository.FirstOrDefault(res =>
                res.Subject == accountUserId && res.BillingServiceTypeId == serviceTypeId) == null;

        /// <summary>
        /// Прикрепить к пользователю ресурс
        /// Если есть свободные, взять свободные иначе создать
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <param name="serviceTypeCostData">Модель данных стоимости услуги
        /// для инф. базы(по конфигурации базы)</param>
        private void AttachResourcesToUsers(IAccountUser accountUser, ServiceTypeCostDataForDatabaseDto serviceTypeCostData)
        {
            var freeResource = ResourceDataProvider.GetFreeResourcesByServiceType(serviceTypeCostData.AccountId,
                serviceTypeCostData.ServiceTypeId).FirstOrDefault();

            if (freeResource != null)
            {
                freeResource.Subject = accountUser.Id;
                DbLayer.ResourceRepository.Update(freeResource);
                DbLayer.Save();

                return;
            }

            CreateResourceProvider.Create(new CreateResourceDto
            {
                AccountId = accountUser.AccountId,
                AccountSponsorId = accountUser.AccountId != serviceTypeCostData.AccountId
                    ? serviceTypeCostData.AccountId
                    : (Guid?)null,
                BillingServiceTypeId = serviceTypeCostData.ServiceTypeId,
                IsFree = false,
                SubjectId = accountUser.Id,
                Cost = serviceTypeCostData.ServiceTypeCost
            });
        }
    }
}
