﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Triggers.Models;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.MyDatabaseService.Triggers.Selectors;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.MyDatabaseService.Triggers
{
    /// <summary>
    /// Базовый класс триггера на изменение инф. базы
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    public abstract class OnChangeDatabaseBaseTrigger(IServiceProvider serviceProvider)
    {
        private readonly IRecalculateMyDatabasesServiceResourcesProvider
            _recalculateMyDatabasesServiceResourcesProvider = serviceProvider.GetRequiredService<IRecalculateMyDatabasesServiceResourcesProvider>();

        private readonly IMyDatabasesServiceDataProvider _myDatabasesServiceDataProvider = serviceProvider.GetRequiredService<IMyDatabasesServiceDataProvider>();
        private readonly IUpdateMyDatabasesResourceProvider _updateMyDatabasesResourceProvider = serviceProvider.GetRequiredService<IUpdateMyDatabasesResourceProvider>();
        private readonly IMyDatabasesResourceDataProvider _myDatabasesResourceDataProvider = serviceProvider.GetRequiredService<IMyDatabasesResourceDataProvider>();
        protected readonly IMyDatabasesResourceDataProvider MyDatabasesResourceDataProvider = serviceProvider.GetRequiredService<IMyDatabasesResourceDataProvider>();
        protected readonly IMyDatabasesServiceTypeDataProvider MyDatabasesServiceTypeDataProvider = serviceProvider.GetRequiredService<IMyDatabasesServiceTypeDataProvider>();
        protected readonly ICreateResourceProvider CreateResourceProvider = serviceProvider.GetRequiredService<ICreateResourceProvider>();
        protected readonly IResourceDataProvider ResourceDataProvider = serviceProvider.GetRequiredService<IResourceDataProvider>();
        protected readonly UsersDataWhoHaveAccessToDbButNoResourcesSelector
            UsersDataWhoHaveAccessToDbButNoResourcesSelector = serviceProvider.GetRequiredService<UsersDataWhoHaveAccessToDbButNoResourcesSelector>();
        protected readonly IUnitOfWork DbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly ISender _sender = serviceProvider.GetRequiredService<ISender>();

        /// <summary>
        /// Прикрепить к пользователям ресурсы
        /// Если есть свободные, взять свободные иначе создать
        /// </summary>
        /// <param name="usersWithoutResources">Данные пользователей
        /// у которых нет ресурсов</param>
        /// <param name="serviceTypeCostData">Модель данных стоимости услуги
        /// для инф. базы</param>
        protected void AttachResourcesToUsers(List<AccountUserDataByResourceModel> usersWithoutResources,
            ServiceTypeCostDataForDatabaseDto serviceTypeCostData)
        {
            if (!usersWithoutResources.Any())
                return;

            var freeResourcesList = ResourceDataProvider.GetFreeResourcesByServiceType(serviceTypeCostData.AccountId,
                serviceTypeCostData.ServiceTypeId).ToList();
            
            var freeResources = new Queue<Resource>(freeResourcesList);
            if (!freeResources.Any())
            {
                var resourcesForCreate = usersWithoutResources.Select(x => new Resource
                {
                    Id = Guid.NewGuid(),
                    AccountId = x.AccountId,
                    AccountSponsorId = x.AccountId != serviceTypeCostData.AccountId
                        ? serviceTypeCostData.AccountId
                        : (Guid?)null,
                    BillingServiceTypeId = serviceTypeCostData.ServiceTypeId,
                    IsFree = false,
                    Subject = x.AccountUserId,
                    Cost = serviceTypeCostData.ServiceTypeCost
                }).ToList();

                DbLayer.BulkInsert(resourcesForCreate);
                DbLayer.Save();

                return;
            }

            var resourcesForUpdate = new List<Resource>();
            var resourcesForAdd = new List<Resource>();

            foreach (var userData in usersWithoutResources)
            {
                if (freeResources.Any())
                {
                    var freeResource = freeResources.Dequeue();
                    freeResource.Subject = userData.AccountUserId;
                    resourcesForUpdate.Add(freeResource);
                }
                else
                {
                   resourcesForAdd.Add(new Resource
                   {
                       Id = Guid.NewGuid(),
                       AccountId = userData.AccountId,
                       AccountSponsorId = userData.AccountId != serviceTypeCostData.AccountId
                           ? serviceTypeCostData.AccountId
                           : (Guid?)null,
                       BillingServiceTypeId = serviceTypeCostData.ServiceTypeId,
                       IsFree = false,
                       Subject = userData.AccountUserId,
                       Cost = serviceTypeCostData.ServiceTypeCost
                   });
                }
            }
          
            DbLayer.BulkUpdate(resourcesForUpdate);
            DbLayer.BulkInsert(resourcesForAdd);
            DbLayer.Save();
        }

        /// <summary>
        /// Создать ресурс для пользователя
        /// </summary>
        /// <param name="serviceTypeCostData">Модель данных стоимости услуги для инф. базы</param>
        /// <param name="userData">Данные пользователя</param>
        protected void CreateResource(ServiceTypeCostDataForDatabaseDto serviceTypeCostData,
            AccountUserDataByResourceModel userData) => CreateResourceProvider.Create(new CreateResourceDto
            {
                AccountId = userData.AccountId,
                AccountSponsorId = userData.AccountId != serviceTypeCostData.AccountId
                    ? serviceTypeCostData.AccountId
                    : (Guid?) null,
                BillingServiceTypeId = serviceTypeCostData.ServiceTypeId,
                IsFree = false,
                SubjectId = userData.AccountUserId,
                Cost = serviceTypeCostData.ServiceTypeCost
            });

        /// <summary>
        /// Пересчитать стоимость сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        protected void RecalculateMyDatabasesServiceCost(Guid accountId) => _sender.Send(new RecalculateResourcesConfigurationCostCommand(
            [accountId],
            [_myDatabasesServiceDataProvider.GetMyDatabasesServiceId()], Clouds42Service.MyDatabases)).GetAwaiter().GetResult();
        
        /// <summary>
        /// Увеличить количество баз для ресурса сервиса "Мои инф. базы"
        /// с пересчетом ресурса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса "Мои инф. базы"</param>
        protected void IncreaseDatabasesCountWithRecalculatingCostResource(Guid accountId,
            ResourceType systemServiceType)
        {
            var myDatabasesResourcesData =
                MyDatabasesResourceDataProvider.GetMyDatabasesResourcesDataByAccount(accountId).ToList();

            var databasesCountByResources = SelectMyDatabasesResourceData(myDatabasesResourcesData, systemServiceType);

            if (databasesCountByResources.ActualDatabasesCount >= databasesCountByResources.PaidDatabasesCount)
                _updateMyDatabasesResourceProvider.IncreasePaidDatabasesCount(accountId, systemServiceType, 1);

            _updateMyDatabasesResourceProvider.IncreaseActualDatabasesCount(accountId, systemServiceType, 1);
            
            _recalculateMyDatabasesServiceResourcesProvider.RecalculateResourcesForServiceType(accountId,
                systemServiceType);
        }

        /// <summary>
        /// Актуализировать ресурсы сервиса "Мои инф. базы"(для ресурсов с услугой по аккаунту)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        protected void ActualizeMyDatabasesResources(Guid accountId)
        {
            var actualValuesForMyDatabasesResources =
                _myDatabasesResourceDataProvider.GetActualValuesForMyDatabasesResourcesForAccount(accountId);

            ActualizeActualDatabasesCountInResource(accountId,
                actualValuesForMyDatabasesResources.DatabasesCount, ResourceType.CountOfDatabasesOverLimit);

            ActualizeActualDatabasesCountInResource(accountId,
                actualValuesForMyDatabasesResources.ServerDatabasesCount, ResourceType.ServerDatabasePlacement);
        }

        /// <summary>
        /// Актуализировать актуальное количество инф. баз в ресурсе
        /// (с пересчетом стоимости ресурса)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="actualDatabasesCount">Актуальное количество инф. баз</param>
        /// <param name="systemServiceType">Тип системной услуги сервиса</param>
        private void ActualizeActualDatabasesCountInResource(Guid accountId, int actualDatabasesCount,
            ResourceType systemServiceType)
        {
            _updateMyDatabasesResourceProvider.UpdateActualDatabasesCount(accountId, systemServiceType, actualDatabasesCount);
            _recalculateMyDatabasesServiceResourcesProvider.RecalculateResourcesForServiceType(accountId,
                systemServiceType);
        }

        /// <summary>
        /// Выбрать данные ресурса сервиса "Мои инф. базы"
        /// по типу системной услуги
        /// </summary>
        /// <param name="myDatabasesResourcesData">Данные ресурсов сервиса "Мои инф. базы"</param>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Данные ресурса сервиса "Мои инф. базы"</returns>
        private static MyDatabasesResourceDataDto SelectMyDatabasesResourceData(
            List<MyDatabasesResourceDataDto> myDatabasesResourcesData, ResourceType systemServiceType) =>
            myDatabasesResourcesData.FirstOrDefault(rd =>
                rd.SystemServiceType == systemServiceType) ??
            throw new NotFoundException($"Не удалось получить данные ресурса по услуге '{systemServiceType}'");
    }
}
