﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.MyDatabaseService.Triggers.Selectors;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;

namespace Clouds42.MyDatabaseService.Triggers
{
    /// <summary>
    /// Триггер на отключение Аренды 1С у пользователей
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    internal class OnDisabledRent1CForUsersTrigger(
        IUnitOfWork dbLayer,
        IOnDisabledMyDatabasesServiceTypeForUsersTrigger onDisabledMyDatabasesServiceTypeForUsersTrigger,
        MyDatabasesServiceResourcesToDisabledSelector myDatabasesServiceResourcesToDisabledSelector,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
        ILogger42 logger,
        ISender sender)
        : IOnDisabledRent1CForUsersTrigger
    {
        /// <summary>
        /// Выполнить триггер на отключение
        /// Аренды 1С у пользователей
        /// </summary>
        /// <param name="model">Модель триггера на отключение
        /// Аренды 1С у пользователей</param>
        public void Execute(OnDisabledRent1CForUsersTriggerDto model)
        {
            var resourcesToDisabled = myDatabasesServiceResourcesToDisabledSelector
                .SelectResourcesToDisabledForUsers(model.AccountUserIds)
                .Select(x =>
                {
                    x.Subject = null;

                    return x;

                }).ToList();

            if (!resourcesToDisabled.Any())
                return;

            var myDatabasesServiceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId();
            var dataForTriggers = SelectDataForOnDisabledServiceTypeTriggers(resourcesToDisabled).ToList();

            const string message = "Выполнение триггера на отключение Аренды 1С у пользователей";
            
            try
            {
                dbLayer.BulkUpdate(resourcesToDisabled);
                dbLayer.Save();

                var accountOwnerIds = SelectAccountOwnerIds(resourcesToDisabled).ToList();

                sender.Send(new RecalculateResourcesConfigurationCostCommand(accountOwnerIds,
                    [myDatabasesServiceId], Clouds42Service.MyDatabases)).Wait();

                dataForTriggers.ForEach(onDisabledMyDatabasesServiceTypeForUsersTrigger.Execute);
            

                logger.Info($"{message} завершилось успешно");
            }

            catch (Exception ex)
            {
                logger.Info($"{message} завершилось с ошибкой. : {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Выбрать данные для триггеров на отключение лицензии по услуге сервиса
        /// "Мои информационные базы" у пользователей
        /// </summary>
        /// <param name="resources">Список ресурсов к отключению</param>
        /// <returns>Данные для триггеров на отключение лицензии</returns>
        private static IEnumerable<OnDisabledMyDatabasesServiceTypeForUsersTriggerDto>
            SelectDataForOnDisabledServiceTypeTriggers(List<Resource> resources) =>
            resources
                .GroupBy(resource => resource.BillingServiceTypeId)
                .Select(resource =>
                    new OnDisabledMyDatabasesServiceTypeForUsersTriggerDto
                    {
                        ServiceTypeId = resource.Key,
                        AccountUserIds = resource.Where(r => r.Subject != null).Select(r => r.Subject.Value).Distinct()
                            .ToList()
                    });

        /// <summary>
        /// Выбрать Id владельцев ресурсов
        /// </summary>
        /// <param name="resources">Ресурсы по услугам</param>
        /// <returns>Id владельцев ресурсов</returns>
        private static IEnumerable<Guid> SelectAccountOwnerIds(
            List<Resource> resources) => resources.Select(r => r.AccountSponsorId ?? r.AccountId).Distinct();
    }
}
