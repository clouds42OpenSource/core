﻿using Clouds42.CoreWorker.AcDbAccessesJobs.Params;
using Clouds42.MyDatabaseService.Contracts.Triggers.Models;

namespace Clouds42.MyDatabaseService.Triggers.Mappers
{
    /// <summary>
    /// Маппер модели данных пользователей с доступом к инф. базе
    /// </summary>
    public static class UsersWithAccessToDatabaseDataModelMapper
    {
        /// <summary>
        /// Выполнить маппинг к модели параметров задачи
        /// по выдаче/удалению доступов в инф. базу
        /// </summary>
        /// <param name="model">Модель данных пользователей с доступом к инф. базе</param>
        /// <returns>Модели параметров задачи
        /// по выдаче/удалению доступов в инф. базу</returns>
        public static ManageAcDbAccessesJobParams MapToManageAcDbAccessesJobParams(
            this UsersWithAccessToDatabaseDataModel model) =>
            new()
            {
                DatabaseId = model.DatabaseId,
                UsersId = model.UserIds
            };
    }
}