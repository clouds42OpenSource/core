﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Triggers.Models;
using Clouds42.MyDatabaseService.Triggers.Selectors;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;

namespace Clouds42.MyDatabaseService.Triggers
{
    /// <summary>
    /// Триггер на изменение состояния инф. базы
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    internal class OnChangeDatabaseStateTrigger(
        MyDatabasesServiceStateDataSelector myDatabasesServiceStateDataSelector,
        IResourcesConfigurationDataProvider resourcesConfigurationDataProvider,
        IActualizeMyDatabasesResourcesProvider actualizeMyDatabasesResourcesProvider,
        IServiceUnlocker serviceUnlocker,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider,
        IServiceProvider serviceProvider,
        IUnitOfWork dbLayer,
        MyDatabasesServiceResourcesToDisabledSelector myDatabasesServiceResourcesToDisabledSelector,
        ILogger42 logger)
        : OnChangeDatabaseBaseTrigger(serviceProvider), IOnChangeDatabaseStateTrigger
    {
        /// <summary>
        /// Выполнить триггер на изменение состояния инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        public void Execute(Guid databaseId)
        {
            var myDatabasesServiceStateData =
                myDatabasesServiceStateDataSelector.SelectMyDatabasesServiceStateByDbContextData(databaseId);

            var message = $"Выполнение триггера на изменение состояния инф. базы '{databaseId}'";

            GetTriggerAction(myDatabasesServiceStateData).Invoke(myDatabasesServiceStateData);

            logger.Trace($"{message} завершилось успешно");
        }

        /// <summary>
        /// Получить действие триггера на изменение состояния инф. базы
        /// </summary>
        /// <param name="myDatabasesServiceStateByDbContextData">Данные по состоянию сервиса "Мои инф. базы"</param>
        /// <returns>Действие триггера на изменение состояния инф. базы</returns>
        private Action<MyDatabasesServiceStateByDbContextDataModel> GetTriggerAction(
            MyDatabasesServiceStateByDbContextDataModel myDatabasesServiceStateByDbContextData)
        {
            if (myDatabasesServiceStateByDbContextData.IsFrozen)
                return ExecuteForBlockedService;

            return ExecuteForActiveService;
        }

        /// <summary>
        /// Выполнить триггер на изменение состояния инф. базы
        /// для заблокированного сервиса
        /// </summary>
        /// <param name="myDatabasesServiceStateByDbContextData">Данные по состоянию сервиса "Мои инф. базы"</param>
        private void ExecuteForBlockedService(
            MyDatabasesServiceStateByDbContextDataModel myDatabasesServiceStateByDbContextData)
        {
            var oldServicesCost = GetServicesCost(myDatabasesServiceStateByDbContextData);
            UpdateMyDatabasesService(actualizeMyDatabasesResourcesProvider.ActualizeForAccount,
                myDatabasesServiceStateByDbContextData);
            ProlongServiceIfTheCostIsLess(myDatabasesServiceStateByDbContextData, oldServicesCost);
        }

        /// <summary>
        /// Выполнить триггер на изменение состояния инф. базы
        /// для активного сервиса
        /// </summary>
        /// <param name="myDatabasesServiceStateByDbContextData">Данные по состоянию сервиса "Мои инф. базы"</param>
        private void ExecuteForActiveService(
            MyDatabasesServiceStateByDbContextDataModel myDatabasesServiceStateByDbContextData) =>
            UpdateMyDatabasesService(ActualizeMyDatabasesResources, myDatabasesServiceStateByDbContextData);

        /// <summary>
        /// Обновить сервис "Мои инф. базы"
        /// 1) Актуализирует ресурсы
        /// 2) Пересчитает стоимость ресурсов
        /// 3) Пересчитает стоимость сервиса
        /// </summary>
        /// <param name="actualizeResourcesAction">Действие по актуализации ресурсов сервиса "Мои инф. базы"</param>
        /// <param name="myDatabasesServiceStateByDbContextData">Данные по состоянию сервиса "Мои инф. базы"</param>
        private void UpdateMyDatabasesService(Action<Guid> actualizeResourcesAction,
            MyDatabasesServiceStateByDbContextDataModel myDatabasesServiceStateByDbContextData)
        {
            actualizeResourcesAction(myDatabasesServiceStateByDbContextData.AccountId);
            DisableUserLicensesByServiceTypes(myDatabasesServiceStateByDbContextData.AccountId,
                myDatabasesServiceStateByDbContextData.ServiceTypeIdListCanBeDisabled);
            RecalculateMyDatabasesServiceCost(myDatabasesServiceStateByDbContextData.AccountId);
        }

        /// <summary>
        /// Купить/продлить заблокированый сервис если его стоимость 
        /// изменилась и денег на балансе достаточно для продления. 
        /// </summary>
        /// <param name="myDatabasesServiceStateByDbContextData">Данные по состоянию сервиса "Мои инф. базы"
        /// для аккаунта</param>
        /// <param name="oldServicesCost">Старая стоимость сервисов</param>
        private void ProlongServiceIfTheCostIsLess(
            MyDatabasesServiceStateByDbContextDataModel myDatabasesServiceStateByDbContextData,
            decimal oldServicesCost)
        {
            var resourcesConfiguration =
                GetMyDatabasesServiceResourcesConfiguration(myDatabasesServiceStateByDbContextData);
            dbLayer.ResourceConfigurationRepository.Reload(resourcesConfiguration);
            serviceUnlocker.ProlongServiceIfTheCostIsLess(myDatabasesServiceStateByDbContextData.AccountId,
                oldServicesCost,
                resourcesConfiguration);
        }

        /// <summary>
        /// Получить стоимость сервисов аккаунта
        /// </summary>
        /// <param name="myDatabasesServiceStateByDbContextData">Данные по состоянию сервиса "Мои инф. базы"
        /// для аккаунта</param>
        /// <returns>Данные для пролонгации сервиса "Мои инф. базы"</returns>
        private decimal GetServicesCost(
            MyDatabasesServiceStateByDbContextDataModel myDatabasesServiceStateByDbContextData) =>
            resourceConfigurationDataProvider.GetTotalServiceCost(
                GetMyDatabasesServiceResourcesConfiguration(myDatabasesServiceStateByDbContextData));

        /// <summary>
        /// Получить конфигурацию ресурсов сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="myDatabasesServiceStateByDbContextData">Данные по состоянию сервиса "Мои инф. базы"</param>
        /// <returns>Конфигурацию ресурсов сервиса "Мои инф. базы"</returns>
        private ResourcesConfiguration GetMyDatabasesServiceResourcesConfiguration(
            MyDatabasesServiceStateByDbContextDataModel myDatabasesServiceStateByDbContextData) =>
            resourcesConfigurationDataProvider.GetResourcesConfigurationOrThrowException(
                myDatabasesServiceStateByDbContextData.AccountId,
                myDatabasesServiceStateByDbContextData.ServiceId);

        /// <summary>
        /// Отключить лицензии у пользователей по услугам
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeIdList">Список Id услуг</param>
        private void DisableUserLicensesByServiceTypes(Guid accountId, List<Guid> serviceTypeIdList)
        {
            if (!serviceTypeIdList.Any())
                return;

            var toDisable =myDatabasesServiceResourcesToDisabledSelector.SelectResourcesToDisabledByServiceTypes(accountId,
                serviceTypeIdList)
                .Select(x =>
                {
                    x.Subject = null;

                    return x;

                }).ToList();

            dbLayer.BulkUpdate(toDisable);
            dbLayer.Save();
        }
    }
}
