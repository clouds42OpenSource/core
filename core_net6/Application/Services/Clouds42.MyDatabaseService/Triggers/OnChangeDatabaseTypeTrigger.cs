﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;

namespace Clouds42.MyDatabaseService.Triggers
{
    /// <summary>
    /// Триггер на изменение типа базы
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    internal class OnChangeDatabaseTypeTrigger(IServiceProvider serviceProvider, ILogger42 logger)
        : OnChangeDatabaseBaseTrigger(serviceProvider), IOnChangeDatabaseTypeTrigger
    {
        /// <summary>
        /// Выполнить триггер на изменение типа базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        public void Execute(Guid databaseId)
        {
            var accessToServerDatabaseCostData =
                MyDatabasesServiceTypeDataProvider.GetAccessToServerDatabaseCostData(databaseId);
          
            var message = $"Выполнение триггера на изменение типа базы, для инф. базы '{databaseId}' ";

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                UpdateResourcesForAccessToDatabase(databaseId, accessToServerDatabaseCostData);
                ActualizeMyDatabasesResources(accessToServerDatabaseCostData.AccountId);
                RecalculateMyDatabasesServiceCost(accessToServerDatabaseCostData.AccountId);
                dbScope.Commit();

                logger.Trace($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                logger.Trace($"{message} завершилось с ошибкой. : {ex.GetFullInfo()}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Обновить ресурсы на доступ в базу для пользователей
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="accessToServerDatabaseCostData">Модель данных стоимости услуги для инф. базы(по конфигурации базы)</param>
        private void UpdateResourcesForAccessToDatabase(Guid databaseId,  ServiceTypeCostDataForDatabaseDto accessToServerDatabaseCostData)
        {
            if (accessToServerDatabaseCostData.IsFileDatabase)
                return;

            var usersWithoutResources =
                UsersDataWhoHaveAccessToDbButNoResourcesSelector.Select(databaseId,
                    accessToServerDatabaseCostData.ServiceTypeId).ToList();

            AttachResourcesToUsers(usersWithoutResources, accessToServerDatabaseCostData);
        }
    }
}