﻿using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.MyDatabaseService.Contracts.Triggers.Models;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.MyDatabaseService.Triggers.Selectors
{
    /// <summary>
    /// Класс для выбора данных доступов к удалению,
    /// для отключенной услуги сервиса "Мои инф. базы"
    /// </summary>
    public class AcDbAccessesToRemoveForDisabledServiceTypeDataSelector(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Выбрать данные пользователей с доступами к инф. базам
        /// по услуге
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="accountUsersId">Id пользователя</param>
        /// <returns>Данные пользователей с доступами к инф. базам
        /// по услуге</returns>
        public IEnumerable<UsersWithAccessToDatabaseDataModel> SelectUsersWithAccessToDatabasesByServiceType(
            Guid serviceTypeId, List<Guid> accountUsersId) =>
            (from usersWithAccessToDatabase in SelectUsersWithAccessToDatabasesData(accountUsersId)
            join database in dbLayer.DatabasesRepository.WhereLazy() on usersWithAccessToDatabase.DatabaseId equals
                database.Id
            join templateRelation in dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().WhereLazy() on
                database.TemplateId equals templateRelation.DbTemplateId
            join configurationRelation in dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>()
                .WhereLazy() on templateRelation.Configuration1CName equals configurationRelation
                .Configuration1CName
            where configurationRelation.ServiceTypeId == serviceTypeId
            select usersWithAccessToDatabase).AsEnumerable();

        /// <summary>
        /// Выбрать данные пользователей с доступами к серверным инф. базам
        /// </summary>
        /// <param name="accountUsersId">Id пользователя</param>
        /// <returns>Данные пользователей с доступами к серверным инф. базам</returns>
        public IEnumerable<UsersWithAccessToDatabaseDataModel> SelectUsersWithAccessToServerDatabases(
            List<Guid> accountUsersId) =>
            (from usersWithAccessToDatabase in SelectUsersWithAccessToDatabasesData(accountUsersId)
            join database in dbLayer.DatabasesRepository.WhereLazy() on usersWithAccessToDatabase.DatabaseId equals
                database.Id
            join dbOnDelimiter in dbLayer.AccountDatabaseDelimitersRepository.WhereLazy() on database.Id equals dbOnDelimiter.AccountDatabaseId into dbOnDelimiters
            from dbOnDelimiter in dbOnDelimiters.DefaultIfEmpty()
            where dbOnDelimiter == null && (database.IsFile == null || database.IsFile == false)
            select usersWithAccessToDatabase).AsEnumerable();

        /// <summary>
        /// Выбрать сгруппированные данные пользователей с доступом к инф. базе
        /// (или доступ в процессе выдачи)
        /// </summary>
        /// <param name="accountUsersId">Список пользователей</param>
        /// <returns>Сгруппированные данные пользователей с доступом к инф. базе</returns>
        private IQueryable<UsersWithAccessToDatabaseDataModel> SelectUsersWithAccessToDatabasesData(
            List<Guid> accountUsersId)
        {
            return dbLayer.ManageDbOnDelimitersAccessRepository
                .AsQueryableNoTracking()
                .Include(x => x.AcDbAccess)
                .ThenInclude(x => x.AccountUser)
                .Where(x => x.AccessState == AccountDatabaseAccessState.Done ||
                            x.AccessState == AccountDatabaseAccessState.ProcessingGrant && x.AcDbAccess.AccountUserID.HasValue && accountUsersId.Contains(x.AcDbAccess.AccountUserID.Value))
                .Select(x => new { x.AcDbAccess.AccountUserID, x.AcDbAccess.AccountDatabaseID })
                .GroupBy(x => x.AccountDatabaseID)
                .Select(x => new UsersWithAccessToDatabaseDataModel
                {
                    DatabaseId = x.Key,
                    UserIds = x.Select(z => z.AccountUserID.Value).Distinct().ToList()
                });
        }
    }
}
