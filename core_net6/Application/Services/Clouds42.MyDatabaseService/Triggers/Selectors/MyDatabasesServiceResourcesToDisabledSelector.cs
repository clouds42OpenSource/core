﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Triggers.Selectors
{
    /// <summary>
    /// Класс для выбора данных ресурсов
    /// по сервису "Мои инф. базы" к отключению
    /// </summary>
    public class MyDatabasesServiceResourcesToDisabledSelector(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Выбрать данные ресурсов по сервису "Мои инф. базы" к отключению
        /// для пользователей
        /// </summary>
        /// <param name="accountUserIds">Список Id пользователей</param>
        /// <returns>Данные ресурсов по сервису "Мои инф. базы" к отключению
        /// для пользователей</returns>
        public List<Resource> SelectResourcesToDisabledForUsers(List<Guid> accountUserIds)
        {
            return dbLayer.ResourceRepository
                .AsQueryable()
                .Where(x => x.Subject.HasValue && accountUserIds.Contains(x.Subject.Value) &&
                            (x.BillingServiceType.SystemServiceType == ResourceType.AccessToServerDatabase ||
                             x.BillingServiceType.SystemServiceType == ResourceType.AccessToConfiguration1C))
                .ToList();
        }

        /// <summary>
        /// Выбрать ресурсы к отключению по услугам для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeIdList">Список Id услуг по которым нужно выбрать ресурсы</param>
        /// <returns>Ресурсы к отключению по услугам для аккаунта</returns>
        public List<Resource> SelectResourcesToDisabledByServiceTypes(Guid accountId,
            List<Guid> serviceTypeIdList)
        {
            return dbLayer.ResourceRepository
                .AsQueryable()
                .Where(x => serviceTypeIdList.Contains(x.BillingServiceTypeId) && x.AccountId == accountId && x.Subject.HasValue && !x.AccountSponsorId.HasValue)
                .ToList();
        }
    }
}
