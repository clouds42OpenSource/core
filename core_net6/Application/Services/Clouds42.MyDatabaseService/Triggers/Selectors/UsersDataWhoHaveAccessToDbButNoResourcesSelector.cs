﻿using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.MyDatabaseService.Contracts.Triggers.Models;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.MyDatabaseService.Triggers.Selectors
{
    /// <summary>
    /// Класс для выбора данных пользователей у которых
    /// есть доступ к базе но нет ресурсов по услуге
    /// </summary>
    public class UsersDataWhoHaveAccessToDbButNoResourcesSelector(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Выбрать данные пользователей у которых
        /// есть доступ к базе но нет ресурсов по услуге
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Данные пользователей у которых
        /// есть доступ к базе но нет ресурсов по услуге</returns>
        public IEnumerable<AccountUserDataByResourceModel> Select(Guid databaseId, Guid serviceTypeId)
        {
            return dbLayer.AcDbAccessesRepository
                .AsQueryableNoTracking()
                .Include(x => x.AccountUser)
                .ThenInclude(x => x.Account)
                .ThenInclude(x => x.BillingAccount)
                .ThenInclude(x => x.Resources.Where(z => z.BillingServiceTypeId == serviceTypeId))
                .Include(x => x.ManageDbOnDelimitersAccess)
                .Where(x => x.AccountUserID.HasValue && x.ManageDbOnDelimitersAccess == null ||
                            x.ManageDbOnDelimitersAccess.AccessState == AccountDatabaseAccessState.Done ||
                            x.ManageDbOnDelimitersAccess.AccessState == AccountDatabaseAccessState.ProcessingGrant &&
                            x.AccountUser.Account.BillingAccount.Resources.All(z => z.Subject != x.AccountUserID))
                .Select(x => new AccountUserDataByResourceModel
                {
                    AccountId = x.AccountUser.AccountId, AccountUserId = x.AccountUserID.Value
                })
                .GroupBy(x => x.AccountUserId)
                .Select(x => x.First());
        }
    }
}
