﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.Triggers.Models;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.MyDatabaseService.Triggers.Selectors
{
    /// <summary>
    /// Класс для выбора данных по состоянию сервиса "Мои инф. базы"
    /// </summary>
    public class MyDatabasesServiceStateDataSelector(
        IUnitOfWork dbLayer,
        IMyDatabasesResourceDataProvider myDatabasesResourceDataProvider,
        IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider)
    {
        /// <summary>
        /// Выбрать данные по состоянию сервиса "Мои инф. базы"
        /// в разрезе инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Данные по состоянию сервиса "Мои инф. базы"</returns>
        public MyDatabasesServiceStateByDbContextDataModel SelectMyDatabasesServiceStateByDbContextData(Guid databaseId)
        {
            var accountDatabase = dbLayer.DatabasesRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(x => x.Id == databaseId) ?? throw new NotFoundException($"Не удалось получить данные по состоянию сервиса \"Мои инф. базы\", для базы {databaseId}");

            var rent1C = dbLayer.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(x => x.BillingService.SystemService == Clouds42Service.MyEnterprise &&  x.AccountId == accountDatabase.AccountId) ??  throw new NotFoundException($"Не удалось получить данные по состоянию сервиса \"Мои инф. базы\", для базы {databaseId} - rent1c");


            var serviceTypeRelations = myDatabasesServiceTypeDataProvider
                .GetDbTemplateServiceTypeRelationsData()
                .AsNoTracking()
                .FirstOrDefault(x => x.DbTemplateId == accountDatabase.TemplateId) 
                                       ?? throw new NotFoundException($"Не удалось получить данные по состоянию сервиса \"Мои инф. базы\", для базы {databaseId} - serviceTypeRelations");

            var activeDbForBilling = myDatabasesResourceDataProvider
                .GetActiveDatabasesForBilling()
                .AsNoTracking()
                .Where(x => x.AccountId == accountDatabase.AccountId).ToList();

            var serviceId = dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(serviceType => serviceType.SystemServiceType == ResourceType.AccessToServerDatabase)?
                .ServiceId ?? throw new NotFoundException($"Не удалось получить данные по состоянию сервиса \"Мои инф. базы\", для базы {databaseId} - serviceId");

            var accessToServerDatabaseServiceTypeId = dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(serviceType => serviceType.SystemServiceType == ResourceType.AccessToServerDatabase)?
                .Id ?? throw new NotFoundException($"Не удалось получить данные по состоянию сервиса \"Мои инф. базы\", для базы {databaseId} - accessToServerDatabaseServiceTypeId");

            var myDatabasesServiceStateByDbContextDataModel =  new MyDatabasesServiceStateByDbContextDataModel
            {
                AccountId = accountDatabase.AccountId,
                IsFrozen = rent1C.Frozen ?? false,
                IsServerDatabase =
                    accountDatabase.AccountDatabaseOnDelimiter == null && accountDatabase.IsFile is null or false,
                MatchingDatabasesByTemplateCount = activeDbForBilling
                    .Count(x => x.TemplateId == accountDatabase.TemplateId),
                ServerDatabasesCount = activeDbForBilling
                    .Count(db => db.AccountDatabaseOnDelimiter == null && db.IsFile is null or false),
                AccessToConfiguration1CServiceTypeId = serviceTypeRelations.ServiceType.Id,
                ServiceId = serviceId,
                AccessToServerDatabaseServiceTypeId = accessToServerDatabaseServiceTypeId
            };

            myDatabasesServiceStateByDbContextDataModel.ServiceTypeIdListCanBeDisabled =
                CreateServiceTypesListCanBeDisabled(
                [
                    (myDatabasesServiceStateByDbContextDataModel.AccessToConfiguration1CServiceTypeId,
                        myDatabasesServiceStateByDbContextDataModel.MatchingDatabasesByTemplateCount == 0),
                    (myDatabasesServiceStateByDbContextDataModel.AccessToServerDatabaseServiceTypeId,
                        myDatabasesServiceStateByDbContextDataModel is
                            { IsServerDatabase: true, ServerDatabasesCount: 0 })
                ]);

            return myDatabasesServiceStateByDbContextDataModel;

        }

        /// <summary>
        /// Создать список Id услуг, которые можно отключить(по условиям)
        /// </summary>
        /// <param name="elementsWithConditions">Id услуг с условиями их добавления в список</param>
        /// <returns>Cписок Id услуг, которые можно отключить</returns>
        private static List<Guid> CreateServiceTypesListCanBeDisabled(
            List<(Guid ServiceTypeId, bool Condition)> elementsWithConditions) =>
            ListExtension.CreateListUnderConditions(elementsWithConditions);
    }
}
