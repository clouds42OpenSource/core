﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces;

namespace Clouds42.MyDatabaseService.Triggers
{
    /// <summary>
    /// Триггер на изменение шаблона у инф. базы
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    internal class OnChangeDatabaseTemplateTrigger(IServiceProvider serviceProvider, ILogger42 logger)
        : OnChangeDatabaseBaseTrigger(serviceProvider), IOnChangeDatabaseTemplateTrigger
    {
        /// <summary>
        /// Выполнить триггер на изменение шаблона у инф. базы
        /// </summary>
        /// <param name="model">Модель триггера на изменение
        /// шаблона у инф. базы</param>
        public void Execute(OnChangeDatabaseTemplateTriggerDto model)
        {
            var serviceTypeCostDataForDatabase =
                MyDatabasesServiceTypeDataProvider.GetServiceTypeCostDataForDatabase(model.DatabaseId);

            var usersWithoutResources =
                UsersDataWhoHaveAccessToDbButNoResourcesSelector.Select(model.DatabaseId,
                    serviceTypeCostDataForDatabase.ServiceTypeId).ToList();

            if (!usersWithoutResources.Any())
                return;

            var message = $"Выполнение триггера на изменение шаблона у инф. базы '{model.DatabaseId}'";

            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                AttachResourcesToUsers(usersWithoutResources, serviceTypeCostDataForDatabase);
                RecalculateMyDatabasesServiceCost(serviceTypeCostDataForDatabase.AccountId);
                dbScope.Commit();

                logger.Trace($"{message} завершилось успешно");
            }
            catch (Exception ex)
            {
                logger.Trace($"{message} завершилось с ошибкой. : {ex.GetFullInfo()}");
                dbScope.Rollback();
                throw;
            }
        }
    }
}