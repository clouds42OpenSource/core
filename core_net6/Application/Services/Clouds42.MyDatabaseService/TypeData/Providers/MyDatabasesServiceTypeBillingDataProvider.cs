﻿using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.TypeData.Providers
{
    /// <summary>
    /// Провайдер для работы с данными билинга услуги сервиса "Мои инф. базы"
    /// </summary>
    internal class MyDatabasesServiceTypeBillingDataProvider(
        ILimitOnFreeCreationDbForAccountDataProvider limitOnFreeCreationDbForAccountDataProvider,
        IUnitOfWork dbLayer,
        ICloudLocalizer cloudLocalizer,
        IResourceDataProvider resourceDataProvider)
        : IMyDatabasesServiceTypeBillingDataProvider
    {
        /// <summary>
        /// Получить данные биллинга для услуг сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные биллинга для услуг сервиса "Мои инф. базы"</returns>
        public List<MyDatabasesServiceTypeBillingDataDto> GetBillingDataForMyDatabasesServiceTypes(Guid accountId)
        {
            var limitOnFreeCreationDbForAccount =
                limitOnFreeCreationDbForAccountDataProvider.GetLimitOnFreeCreationDbValue(accountId);

            var serviceName = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountId);

            return (from serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy()
                join service in dbLayer.BillingServiceRepository.WhereLazy() on serviceType.ServiceId equals
                    service.Id
                join rate in dbLayer.RateRepository.WhereLazy() on serviceType.Id equals rate
                    .BillingServiceTypeId
                join serviceTypeAccountRate in dbLayer.AccountRateRepository.WhereLazy(ar =>
                        ar.AccountId == accountId)
                    on serviceType.Id equals serviceTypeAccountRate.BillingServiceTypeId into
                    serviceTypeAccountRates
                from accountRate in serviceTypeAccountRates.Take(1).DefaultIfEmpty()
                join resource in resourceDataProvider.GetUsedPaidResourcesDataByServiceTypes(accountId) on
                    serviceType.Id equals resource.ServiceTypeId
                where service.SystemService == Clouds42Service.MyDatabases && serviceType.SystemServiceType != null
                select new MyDatabasesServiceTypeBillingDataDto
                {
                    ServiceId = service.Id,
                    ServiceName = serviceName,
                    ServiceTypeId = serviceType.Id,
                    ServiceTypeName = serviceType.Name,
                    ServiceTypeDescription = serviceType.Description,
                    SystemServiceType = serviceType.SystemServiceType.Value,
                    Clouds42ServiceType = service.SystemService,
                    BillingType = serviceType.BillingType,
                    IsActiveService = service.IsActive,
                    LimitOnFreeLicenses = serviceType.SystemServiceType != ResourceType.CountOfDatabasesOverLimit
                        ? 0
                        : limitOnFreeCreationDbForAccount,
                    CostPerOneLicense = accountRate == null ? rate.Cost : accountRate.Cost,
                    VolumeInQuantity = resource.VolumeInQuantity,
                    TotalAmount = resource.TotalAmount
                }).AsEnumerable().Select(UpdateVolumeInQuantity).ToList();
        }

        /// <summary>
        /// Обновить количественное значение ресурса(Объем) 
        /// </summary>
        /// <param name="billingData">Данные биллинга по услуге</param>
        /// <returns>Данные биллинга по услуге</returns>
        private static MyDatabasesServiceTypeBillingDataDto UpdateVolumeInQuantity(
            MyDatabasesServiceTypeBillingDataDto billingData)
        {
            billingData.VolumeInQuantity = DetermineLicensesCount(billingData);
            return billingData;
        }

        /// <summary>
        /// Определить количество лицензий по услуге
        /// </summary>
        /// <param name="billingData">Данные биллинга по услуге</param>
        /// <returns>Количество лицензий по услуге</returns>
        private static int DetermineLicensesCount(MyDatabasesServiceTypeBillingDataDto billingData) =>
            billingData.LimitOnFreeLicenses > billingData.VolumeInQuantity
                ? 0
                : billingData.VolumeInQuantity - billingData.LimitOnFreeLicenses;
    }
}
