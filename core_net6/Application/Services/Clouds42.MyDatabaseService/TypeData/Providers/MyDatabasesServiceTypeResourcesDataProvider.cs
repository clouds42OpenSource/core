﻿using Clouds42.DataContracts.Billing;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.TypeData.Providers
{
    /// <summary>
    /// Провайдер для работы с данными ресурсов услуги сервиса "Мои инф. базы"
    /// </summary>
    internal class MyDatabasesServiceTypeResourcesDataProvider(
        IMyDatabasesServiceTypeBillingDataProvider myDatabasesServiceTypeBillingDataProvider)
        : IMyDatabasesServiceTypeResourcesDataProvider
    {
        /// <summary>
        /// Получить данные по ресурсам услуг сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="payPeriod">Период оплаты</param>
        /// <returns>Данные по ресурсам услуг</returns>
        public List<BillingServiceTypeResourceDto> GetResourcesDataForMyDatabasesServiceTypes(Guid accountId,
            int payPeriod)
        {
            var serviceTypesBillingData =
                myDatabasesServiceTypeBillingDataProvider.GetBillingDataForMyDatabasesServiceTypes(accountId);

            return serviceTypesBillingData.Select(billingData =>
                    GenerateBillingServiceTypeResource(billingData, payPeriod))
                .Where(resData => resData.Amount > 0).ToList();
        }

        /// <summary>
        /// Сформировать модель данных ресурсов услуги
        /// </summary>
        /// <param name="billingData">Данные биллинга по услуге</param>
        /// <param name="payPeriod">Период оплаты</param>
        /// <returns>Модель данных ресурсов услуги</returns>
        private static BillingServiceTypeResourceDto GenerateBillingServiceTypeResource(
            MyDatabasesServiceTypeBillingDataDto billingData, int payPeriod)
        {
            var serviceTypeName = billingData.SystemServiceType == ResourceType.CountOfDatabasesOverLimit
                ? $"{billingData.ServiceTypeDescription}, (свыше {billingData.LimitOnFreeLicenses} шт)"
                : billingData.ServiceTypeDescription;

            return new BillingServiceTypeResourceDto
            {
                Id = billingData.ServiceTypeId,
                ServiceName = billingData.ServiceName,
                ServiceTypeName = serviceTypeName,
                CountLicenses = billingData.VolumeInQuantity,
                BillingType = billingData.BillingType,
                Amount = billingData.TotalAmount * payPeriod,
                CostPerOneLicense = billingData.CostPerOneLicense,
                SystemServiceType = billingData.SystemServiceType,
                IsActiveService = billingData.IsActiveService
            };
        }
    }
}