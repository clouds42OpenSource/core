﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.MyDatabaseService.TypeData.Providers
{
    /// <summary>
    /// Провайдер для работы с данными услуги сервиса "Мои информационные базы"
    /// </summary>
    internal class MyDatabasesServiceTypeDataProvider(IUnitOfWork dbLayer) : IMyDatabasesServiceTypeDataProvider
    {
        /// <summary>
        /// Получить услугу по Id базы
        /// </summary>
        /// <param name="databaseId">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        public BillingServiceType GetServiceTypeByDatabaseId(Guid databaseId) =>
            (from database in dbLayer.DatabasesRepository.WhereLazy(db => db.Id == databaseId)
                join relation in GetDbTemplateServiceTypeRelationsData() on database.TemplateId equals
                    relation.DbTemplateId
                select relation).FirstOrDefault()?.ServiceType;

        /// <summary>
        /// Получить услугу по Id шаблона базы
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        public BillingServiceType GetServiceTypeByDbTemplateId(Guid dbTemplateId) =>
            GetDbTemplateServiceTypeRelationsData().FirstOrDefault(relation => relation.DbTemplateId == dbTemplateId)
                ?.ServiceType;

        /// <summary>
        /// Получить услугу по имени конфигурации
        /// </summary>
        /// <param name="configurationName">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        public BillingServiceType GetServiceTypeByConfigurationName(string configurationName) =>
            GetConfigurationServiceTypeRelationsData().FirstOrDefault(relation => relation.Configuration1C.Name == configurationName || relation.ConfigurationVariations.Contains(configurationName))
                ?.ServiceType;

        /// <summary>
        /// Получить Id услуги по Id базы
        /// </summary>
        /// <param name="databaseId">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        public Guid? GetServiceTypeIdByDatabaseId(Guid databaseId) =>
            GetServiceTypeByDatabaseId(databaseId)?.Id;

        /// <summary>
        /// Получить ID услуги "Доступ в серверную базу"
        /// </summary>
        /// <returns>ID услуги "Доступ в серверную базу"</returns>
        public Guid GetAccessToServerDatabaseServiceTypeId()
            => dbLayer.BillingServiceTypeRepository.FirstOrDefault(servType =>
                   servType.SystemServiceType == ResourceType.AccessToServerDatabase)?.Id
               ?? throw new NotFoundException(
                   $"По типу системной услуги {ResourceType.AccessToServerDatabase.Description()} не найдена услуга");

        /// <summary>
        /// Получить Id услуги по Id шаблона базы
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        public Guid? GetServiceTypeIdByDbTemplateId(Guid dbTemplateId) =>
            GetServiceTypeByDbTemplateId(dbTemplateId)?.Id;

        /// <summary>
        /// Получить Id услуги по имени конфигурации базы
        /// </summary>
        /// <param name="configurationName">имя конфигурации информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        public Guid? GetServiceTypeIdByConfigurationName(string configurationName) =>
            GetServiceTypeByConfigurationName(configurationName)?.Id;

        

        /// <summary>
        /// Получить данные стоимости услуги для инф. базы(по конфигурации базы)
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель данных стоимости услуги для инф. базы(по конфигурации базы)</returns>
        public ServiceTypeCostDataForDatabaseDto GetServiceTypeCostDataForDatabase(Guid databaseId) =>
            (from database in dbLayer.DatabasesRepository.WhereLazy(db => db.Id == databaseId)
                join relation in GetDbTemplateServiceTypeRelationsData() on database.TemplateId equals
                    relation.DbTemplateId
                join serviceTypeRate in dbLayer.RateRepository.WhereLazy() on relation.ServiceType.Id equals
                    serviceTypeRate.BillingServiceTypeId
                join accountRate in dbLayer.AccountRateRepository.WhereLazy() on relation.ServiceType.Id
                    equals accountRate.BillingServiceTypeId into accountRates
                from accountRate in accountRates.Where(ar => ar.AccountId == database.AccountId).Take(1)
                    .DefaultIfEmpty()
                select new ServiceTypeCostDataForDatabaseDto
                {
                    AccountId = database.AccountId,
                    ServiceTypeId = relation.ServiceType.Id,
                    ServiceTypeCost = accountRate == null ? serviceTypeRate.Cost : accountRate.Cost
                }).FirstOrDefault() ??
            throw new NotFoundException($"Не удалось получить данные стоимости услуги для инф. базы '{databaseId}'");

        /// <summary>
        /// Получить данные стоимости услуги "Доступ в серверную базу" по инф. базе
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель данных стоимости услуги "Доступ в серверную базу" по инф. базе</returns>
        public ServiceTypeCostDataForDatabaseDto GetAccessToServerDatabaseCostData(Guid databaseId) =>
            (from serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy(st =>
                    st.SystemServiceType == ResourceType.AccessToServerDatabase)
                join database in dbLayer.DatabasesRepository.WhereLazy() on databaseId equals database.Id
                join serviceTypeRate in dbLayer.RateRepository.WhereLazy() on serviceType.Id equals
                    serviceTypeRate.BillingServiceTypeId
                join accountRate in dbLayer.AccountRateRepository.WhereLazy() on serviceType.Id
                    equals accountRate.BillingServiceTypeId into accountRates
                from accountRate in accountRates.Where(ar => ar.AccountId == database.AccountId).Take(1)
                    .DefaultIfEmpty()
                select new ServiceTypeCostDataForDatabaseDto
                {
                    AccountId = database.AccountId,
                    ServiceTypeId = serviceType.Id,
                    ServiceTypeCost = accountRate == null ? serviceTypeRate.Cost : accountRate.Cost,
                    IsFileDatabase = database.IsFile ?? false
                }).FirstOrDefault() ??
            throw new NotFoundException(
                $"Не удалось получить данные стоимости услуги \"Доступ в серверную базу\" по инф. базе '{databaseId}'");

        /// <summary>
        /// Получить данные услуг сервиса "Мои инф. базы"
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные услуг сервиса "Мои инф. базы"
        /// для аккаунта</returns>
        public IQueryable<MyDatabasesServiceTypeDataDto> GetMyDatabasesServiceTypesData(Guid accountId) =>
            from service in dbLayer.BillingServiceRepository.WhereLazy()
            join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on service.Id equals serviceType
                .ServiceId
            join serviceTypeRate in dbLayer.RateRepository.WhereLazy() on serviceType.Id equals
                serviceTypeRate.BillingServiceTypeId
            join accountRate in dbLayer.AccountRateRepository.WhereLazy(ar => ar.AccountId == accountId) on
                serviceType.Id
                equals accountRate.BillingServiceTypeId into accountRates
            from accountRate in accountRates.Take(1).DefaultIfEmpty()
            where service.SystemService == Clouds42Service.MyDatabases
            select new MyDatabasesServiceTypeDataDto
            {
                ServiceTypeId = serviceType.Id,
                SystemServiceType = serviceType.SystemServiceType,
                BillingType = serviceType.BillingType,
                ServiceTypeCost = accountRate == null ? serviceTypeRate.Cost : accountRate.Cost
            };

        /// <summary>
        /// Получить данные связей услуги сервиса "Мои информационные базы"
        /// с шаблоном инф. базы
        /// </summary>
        /// <returns>Данные связей услуги сервиса "Мои информационные базы"
        /// с шаблоном инф. базы</returns>
        public IQueryable<DbTemplateServiceTypeRelationDataDto> GetDbTemplateServiceTypeRelationsData()
        {
            return dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                .AsQueryableNoTracking()
                .Include(x => x.Configuration1C)
                .ThenInclude(x => x.ConfigurationServiceTypeRelation)
                .ThenInclude(x => x.ServiceType)
                .Select(x => new DbTemplateServiceTypeRelationDataDto
                {
                    DbTemplateId = x.DbTemplateId,
                    ServiceType = x.Configuration1C.ConfigurationServiceTypeRelation.ServiceType
                });
        }

        /// <summary>
        /// Получить данные связей услуги сервиса "Мои информационные базы"
        /// с шаблоном инф. базы
        /// </summary>
        /// <returns>Данные связей услуги сервиса "Мои информационные базы"
        /// с шаблоном инф. базы</returns>
        public IQueryable<ConfigurationServiceTypeRelationDataDto> GetConfigurationServiceTypeRelationsData() =>
            from configurations1C in dbLayer.Configurations1CRepository.WhereLazy()
            join relation in dbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>().WhereLazy() on
                configurations1C.Name equals relation.Configuration1CName
            join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on relation
                .ServiceTypeId equals serviceType.Id
            select new ConfigurationServiceTypeRelationDataDto
            {
                Configuration1C = configurations1C,
                ConfigurationVariations = configurations1C.ConfigurationNameVariations.Select(w => w.VariationName)
                    .ToList(),
                ServiceType = serviceType
            };

    }
}
