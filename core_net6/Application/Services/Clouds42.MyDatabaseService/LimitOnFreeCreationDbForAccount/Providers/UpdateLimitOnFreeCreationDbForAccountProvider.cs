﻿using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.LimitOnFreeCreationDbForAccount.Providers
{
    /// <summary>
    /// Провайдер для обновления лимита на
    /// бесплатное количество баз для создания(для аккаунта)
    /// </summary>
    internal class UpdateLimitOnFreeCreationDbForAccountProvider(
        IUnitOfWork dbLayer,
        ICreateLimitOnFreeCreationDbForAccount createLimitOnFreeCreationDbForAccount,
        ILimitOnFreeCreationDbForAccountDataProvider limitOnFreeCreationDbForAccountDataProvider)
        : IUpdateLimitOnFreeCreationDbForAccountProvider
    {
        /// <summary>
        ///  Обновить лимита на бесплатное количество баз для
        ///  создания если он существует, иначе создать
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="limitValue">Новый лимит</param>
        public void UpdateIfAvailableOtherwiseCreate(Guid accountId, int limitValue)
        {
            var limitOnFreeCreationDbForAccount =
                limitOnFreeCreationDbForAccountDataProvider.GetLimitOnFreeCreationDbForAccount(accountId);

            if (limitOnFreeCreationDbForAccount == null)
            {
                createLimitOnFreeCreationDbForAccount.Create(accountId, limitValue);
                return;
            }

            limitOnFreeCreationDbForAccount.LimitedQuantity = limitValue;
            dbLayer.GetGenericRepository<Domain.DataModels.billing.LimitOnFreeCreationDbForAccount>()
                .Update(limitOnFreeCreationDbForAccount);

            dbLayer.Save();
        }
    }
}