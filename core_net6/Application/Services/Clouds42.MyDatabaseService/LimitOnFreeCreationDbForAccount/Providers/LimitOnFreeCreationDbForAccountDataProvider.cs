﻿using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.LimitOnFreeCreationDbForAccount.Providers
{
    /// <summary>
    /// Провайдер по работе с данными лимита на
    /// бесплатное количество баз для создания(для аккаунта)
    /// </summary>
    public class LimitOnFreeCreationDbForAccountDataProvider(IUnitOfWork dbLayer)
        : ILimitOnFreeCreationDbForAccountDataProvider
    {
        private readonly Lazy<int> _defaultLimitOnFreeCreationDb = new(CloudConfigurationProvider.MyDatabasesService.GetDefaultLimitOnFreeCreationDb);

        /// <summary>
        /// Получить значение лимита на бесплатное количество баз для создания
        /// Возьмет заначение по аккаунту или дефолтное значение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Лимит на бесплатное количество баз для создания</returns>
        public int GetLimitOnFreeCreationDbValue(Guid accountId) =>
            GetLimitOnFreeCreationDbForAccount(accountId)?.LimitedQuantity ??
            _defaultLimitOnFreeCreationDb.Value;

        /// <summary>
        /// Получить информацию об аккаунте
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Валюта аккаунта</returns>
        public Account GetAccountInfo(Guid accountId) =>
            dbLayer.AccountsRepository.FirstOrDefault(c => c.Id == accountId);


        /// <summary>
        /// Получить лимит на бесплатное количество баз для создания (для аккаунта)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Лимит на бесплатное количество баз для создани</returns>
        public Domain.DataModels.billing.LimitOnFreeCreationDbForAccount GetLimitOnFreeCreationDbForAccount(Guid accountId) =>
            dbLayer.GetGenericRepository<Domain.DataModels.billing.LimitOnFreeCreationDbForAccount>().AsQueryable()
                .FirstOrDefault(limit => limit.AccountId == accountId);

        /// <summary>
        /// Получить количество инф. баз
        /// для которых требуется оплата(сверх лимита)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="databasesCount">Количество баз</param>
        /// <returns>Количество инф. баз
        /// для которых требуется оплата</returns>
        public int GetDatabasesCountForWhichYouNeedToPay(Guid accountId, int databasesCount)
        {
            var limitOnFreeCreationDb =
                GetLimitOnFreeCreationDbValue(accountId);

            return GetDatabasesCountForWhichYouNeedToPay(accountId, databasesCount, limitOnFreeCreationDb);
        }

        /// <summary>
        /// Получить количество инф. баз
        /// для которых требуется оплата(сверх лимита)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="databasesCount">Количество баз</param>
        /// <param name="limitOnFreeCreationDb">Лимит на бесплатное количество создания баз</param>
        /// <returns>Количество инф. баз
        /// для которых требуется оплата</returns>
        public int GetDatabasesCountForWhichYouNeedToPay(Guid accountId, int databasesCount, int limitOnFreeCreationDb)
        {
            if (limitOnFreeCreationDb >= databasesCount)
                return 0;

            return databasesCount - limitOnFreeCreationDb;
        }

        /// <summary>
        /// Получить количество баз для которых необходима оплата
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="countOfDatabasesToCreate">Количество баз, которые необходимо создать</param>
        /// <param name="countOfDatabasesCreated">Количество созданных баз</param>
        /// <returns>Количество баз, за которые нужно платить</returns>
        public int GetDatabasesCountForWhichYouNeedPayment(Guid accountId, int countOfDatabasesToCreate,
            int countOfDatabasesCreated)
        {
            var limitOnFreeCreationDbs = GetLimitOnFreeCreationDbValue(accountId);

            if (countOfDatabasesCreated >= limitOnFreeCreationDbs)
                return countOfDatabasesToCreate;

            var freeCountOfCreationDbs = limitOnFreeCreationDbs - countOfDatabasesCreated;

            if (freeCountOfCreationDbs >= countOfDatabasesToCreate)
                return 0;

            return countOfDatabasesToCreate - freeCountOfCreationDbs;
        }
    }
}
