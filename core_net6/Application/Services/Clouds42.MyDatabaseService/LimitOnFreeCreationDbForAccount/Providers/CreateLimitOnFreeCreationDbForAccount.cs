﻿using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.LimitOnFreeCreationDbForAccount.Providers
{
    /// <summary>
    /// Провайдер создания лимита на
    /// бесплатное количество баз для создания(для аккаунта)
    /// </summary>
    internal class CreateLimitOnFreeCreationDbForAccount(IUnitOfWork dbLayer) : ICreateLimitOnFreeCreationDbForAccount
    {
        /// <summary>
        /// Создать лимит на
        /// бесплатное количество баз для создания
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="limitValue">Лимит</param>
        public void Create(Guid accountId, int limitValue)
        {
           var limitOnFreeCreationDbForAccount = new Domain.DataModels.billing.LimitOnFreeCreationDbForAccount
           {
               AccountId = accountId,
               LimitedQuantity = limitValue
           };

           dbLayer.GetGenericRepository<Domain.DataModels.billing.LimitOnFreeCreationDbForAccount>()
               .Insert(limitOnFreeCreationDbForAccount);

           dbLayer.Save();
        }
    }
}
