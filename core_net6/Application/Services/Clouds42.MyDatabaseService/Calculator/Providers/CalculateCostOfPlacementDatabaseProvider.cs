﻿using Clouds42.BLL.Common.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountBilling.Extensions;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.MyDatabaseService.Calculator.Providers
{
    /// <summary>
    /// Провайдер для подсчета стоимости размещения инф. базы
    /// </summary>
    internal class CalculateCostOfPlacementDatabaseProvider(
        IMyDatabasesCalculationCostDataProvider myDatabasesCalculationCostDataProvider,
        ILimitOnFreeCreationDbForAccountDataProvider limitOnFreeCreationDbForAccountDataProvider,
        IUnitOfWork unitOfWork)
        : ICalculateCostOfPlacementDatabaseProvider
    {
        /// <summary>
        /// Посчитать стоимость размещения инф. баз для аккаунта
        /// </summary>
        /// <param name="model">Модель подсчета стоимости размещения инф. баз для аккаунта</param>
        /// <returns>Модель результата подсчета стоимости
        /// размещения инф. баз для аккаунта</returns>
        public CalculationCostOfPlacementDatabasesResultDto CalculateCostOfPlacementDatabases(
            CalculateCostOfPlacementDatabasesDto model)
        {
            var calculationCostData =
                myDatabasesCalculationCostDataProvider.GetMyDatabasesCalculationCostForAccountServiceTypeData(
                    model.AccountId,
                    model.SystemServiceType);

            var actualDatabasesCount = calculationCostData.PlacementDatabasesCount + model.DatabasesForPlacementCount;

            var databasesCountForWhichYouNeedPayment =  GetDatabasesCountForWhichYouNeedPayment(calculationCostData, actualDatabasesCount, model.SystemServiceType);

            if (databasesCountForWhichYouNeedPayment == 0)
                return new CalculationCostOfPlacementDatabasesResultDto
                {
                    ActualDatabasesCount = actualDatabasesCount
                };

            var cost = CalculateCost(model, calculationCostData, databasesCountForWhichYouNeedPayment);

            return new CalculationCostOfPlacementDatabasesResultDto
            {
                Cost = cost,
                DatabasesForWhichCostCalculatedCount = databasesCountForWhichYouNeedPayment,
                PartialCost = BillingServicePartialCostCalculator.CalculateUntilExpireDateWithRoundedUp(cost,
                    calculationCostData.ExpireDate, model.PaidDate),
                DatabasesCountForWhichYouNeedPayment =
                    limitOnFreeCreationDbForAccountDataProvider.GetDatabasesCountForWhichYouNeedPayment(
                        model.AccountId,
                        databasesCountForWhichYouNeedPayment, calculationCostData.PaidDatabasesCount),
                ActualDatabasesCount = actualDatabasesCount
            };
        }

        /// <summary>
        /// Посчитать стоимость размещения инф. баз для аккаунта
        /// </summary>
        /// <param name="model">Модель подсчета стоимости размещения инф. баз для аккаунта</param>
        /// <returns>Модель результата подсчета стоимости
        /// размещения инф. баз для аккаунта</returns>
        public async Task<TryChangeTariffResultDto> CalculateCostDatabases(CalculateCostOfPlacementDatabasesDto model)
        {
            var accountInfo = await unitOfWork.AccountsRepository
                .AsQueryableNoTracking()
                .Include(x => x.BillingAccount)
                .FirstOrDefaultAsync(x => x.Id == model.AccountId) ?? throw new NotFoundException($"Не удалсь получись данные об аккаунте '{model.AccountId}'");

            var billingAccount = await unitOfWork.BillingAccountRepository
                                     .AsQueryableNoTracking()
                                     .FirstOrDefaultAsync(a => a.Id == model.AccountId) ??
                                 throw new NotFoundException($"Не удалсь получись данные о состояние баланса аккаунта '{model.AccountId}'");

            var canGetPromisePayment = await unitOfWork.PaymentRepository
                .AsQueryableNoTracking()
                .AnyAsync(p => p.AccountId == model.AccountId && 
                               p.OperationType == PaymentType.Inflow.ToString() && 
                               p.Sum > 0 &&
                               p.Status == PaymentStatus.Done.ToString()) && billingAccount.PromisePaymentSum is null or <= 0;

            var balance = accountInfo!.BillingAccount.GetAvailableBalance();

            TryChangeTariffResultDto result = new TryChangeTariffResultDto
            {
                Complete = true,
                Currency = accountInfo.AccountConfiguration.Locale.Currency,
                CanGetPromisePayment = canGetPromisePayment,
                Balance = balance,
            };

            var existsResourceConfiguration = await unitOfWork.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .AnyAsync(x => x.AccountId == model.AccountId && x.BillingService.SystemService == Clouds42Service.MyEnterprise);
           
            if (!existsResourceConfiguration) 
                return result;

            var calculationCostData = await myDatabasesCalculationCostDataProvider.GetMyDatabasesCalculationCostForAccountServiceTypeDataAsync(
                    model.AccountId,
                    model.SystemServiceType);

            var actualDatabasesCount = calculationCostData.PlacementDatabasesCount + model.DatabasesForPlacementCount;

            var databasesCountForWhichYouNeedPayment = GetDatabasesCountForWhichYouNeedPayment(calculationCostData, actualDatabasesCount, model.SystemServiceType);

            if (databasesCountForWhichYouNeedPayment == 0)
                return result;

            var cost = CalculateCost(model, calculationCostData, databasesCountForWhichYouNeedPayment);

            var partialCost = BillingServicePartialCostCalculator.CalculateUntilExpireDateWithRoundedUp(cost, calculationCostData.ExpireDate);

            if (partialCost == 0)
                return result;


            if (partialCost <= balance)
            {
                return new TryChangeTariffResultDto
                {
                    CanGetPromisePayment = canGetPromisePayment,
                    Currency = accountInfo.AccountConfiguration.Locale.Currency,
                    Complete = false,
                    EnoughMoney = 0,
                    CostOftariff = partialCost,
                    MonthlyCost = cost,
                    Balance = balance,
                };
            }


            return new TryChangeTariffResultDto
            {
                Currency = accountInfo.AccountConfiguration.Locale.Currency,
                Complete = false,
                CanGetPromisePayment = canGetPromisePayment,
                CostOftariff = partialCost,
                EnoughMoney = partialCost - balance,
                MonthlyCost = cost,
                Balance = balance
            };

        }

        /// <summary>
        /// Посчитать стоимость размещения инф. баз для аккаунта
        /// </summary>
        /// <param name="model">Модель подсчета стоимости размещения инф. баз для аккаунта</param>
        /// <param name="calculationCostData">Модель данных для калькуляции
        /// услуги сервиса "Мои информационные базы"</param>
        /// <param name="databasesCountForWhichYouNeedPayment">Количество ифн. баз для которых необходима оплата</param>
        /// <returns>Стоимость размещения инф. баз для аккаунта</returns>
        private decimal CalculateCost(CalculateCostOfPlacementDatabasesDto model,
            MyDatabasesCalculationCostDataDto calculationCostData,
            int databasesCountForWhichYouNeedPayment)
        {
            if (model.SystemServiceType == ResourceType.ServerDatabasePlacement)
                return databasesCountForWhichYouNeedPayment * calculationCostData.ActualCost;

            return limitOnFreeCreationDbForAccountDataProvider.GetDatabasesCountForWhichYouNeedPayment(model.AccountId,
                       databasesCountForWhichYouNeedPayment, calculationCostData.PaidDatabasesCount) *
                   calculationCostData.ActualCost;
        }

        /// <summary>
        /// Получить количество ифн. баз для которых необходима оплата
        /// </summary>
        /// <param name="calculationCostData">Модель данных для калькуляции
        /// услуги сервиса "Мои информационные базы"</param>
        /// <param name="actualDatabasesCount">Актуальное количество инф. баз,
        /// с учетом новых(которые нужно разместить)</param>
        /// <param name="resourceType"></param>
        /// <returns>Количество ифн. баз для которых необходима оплата</returns>
        private static int GetDatabasesCountForWhichYouNeedPayment(MyDatabasesCalculationCostDataDto calculationCostData,
            int actualDatabasesCount, ResourceType? resourceType)
        {
            if (resourceType == ResourceType.ServerDatabasePlacement)
                return 1;

            if (calculationCostData.PaidDatabasesCount >= actualDatabasesCount)
                return 0;

            return actualDatabasesCount - calculationCostData.PaidDatabasesCount;
        }
    }
}
