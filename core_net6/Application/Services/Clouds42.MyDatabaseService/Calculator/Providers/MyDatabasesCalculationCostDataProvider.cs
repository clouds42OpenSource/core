﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;

#pragma warning disable CS8603

namespace Clouds42.MyDatabaseService.Calculator.Providers
{
    /// <summary>
    /// Провайдер по работе с данными для калькуляции
    /// услуги сервиса "Мои информационные базы"
    /// </summary>
    internal class MyDatabasesCalculationCostDataProvider(
        IUnitOfWork dbLayer)
        : IMyDatabasesCalculationCostDataProvider
    {
        /// <summary>
        /// Получить данные для калькуляции
        /// услуги сервиса "Мои информационные базы" (услуги по аккаунту)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги(услуги сервиса "Мои информационные базы" по аккаунту)</param>
        /// <returns>Данные для калькуляции
        /// услуги сервиса "Мои информационные базы"</returns>
        public MyDatabasesCalculationCostDataDto GetMyDatabasesCalculationCostForAccountServiceTypeData(Guid accountId,
            ResourceType systemServiceType)
        {
            var accountLocale = dbLayer.AccountConfigurationRepository
                .AsQueryableNoTracking()
                .Include(x => x.Locale)
                .Where(x => x.AccountId == accountId)
                .Select(x => x.Locale.Name)
                .FirstOrDefault();

            var accountDatabasesResources = dbLayer.DatabasesRepository
                .AsQueryableNoTracking()
                .Where(database =>
                    database.State == DatabaseState.Ready.ToString() ||
                    database.State == DatabaseState.NewItem.ToString() ||
                    database.State == DatabaseState.ProcessingSupport.ToString() ||
                    database.State == DatabaseState.TransferDb.ToString() ||
                    database.State == DatabaseState.RestoringFromTomb.ToString())
                .GroupBy(x => x.AccountId)
                .Select(x => new MyDatabasesResourcesActualValuesForAccountDto
                {
                    AccountId = x.Key,
                    DatabasesCount = x.Count(),
                    ServerDatabasesCount = x.Count(db =>
                        db.AccountDatabaseOnDelimiter == null && (db.IsFile == null || db.IsFile == false))
                })
                .FirstOrDefault(x => x.AccountId == accountId);


            var data = dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .Include(x => x.DependServiceType)
                .ThenInclude(x => x.Service)
                .ThenInclude(x => x.ResourcesConfigurations.Where(z => z.AccountId == accountId))
                .Include(x => x.Resources.Where(z => z.AccountId == accountId))
                .ThenInclude(x => x.MyDatabasesResource)
                .Include(x => x.AccountRates.Where(z => z.AccountId == accountId))
                .Where(x => x.SystemServiceType == systemServiceType &&
                            x.DependServiceType.Service.ResourcesConfigurations.Any(z => z.ExpireDate.HasValue))
                .Include(x => x.Rates)
                .Select(x => new MyDatabasesCalculationCostDataDto
                {
                    ActualCost = x.AccountRates.Any() ? x.AccountRates.FirstOrDefault()!.Cost : x.Rates.FirstOrDefault()!.Cost,
                    PaidDatabasesCount = x.Resources.FirstOrDefault() != null ? x.Resources.FirstOrDefault()!.MyDatabasesResource != null ? x.Resources.FirstOrDefault()!.MyDatabasesResource.PaidDatabasesCount : 0 : 0,
                    ExpireDate = x.DependServiceType.Service.ResourcesConfigurations.FirstOrDefault()!.ExpireDate!.Value
                })
                .FirstOrDefault() ?? throw new NotFoundException(
                $"Не удалось получить данные для калькуляции услуги '{systemServiceType.Description()}'"); ;

            data.LocaleName = accountLocale;
            data.PlacementDatabasesCount = accountDatabasesResources != null ? systemServiceType == ResourceType.ServerDatabasePlacement ? accountDatabasesResources.ServerDatabasesCount : accountDatabasesResources.DatabasesCount : 0;

            return data;
        }

        /// <summary>
        /// Получить данные для калькуляции
        /// услуги сервиса "Мои информационные базы" (услуги по аккаунту)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги(услуги сервиса "Мои информационные базы" по аккаунту)</param>
        /// <returns>Данные для калькуляции
        /// услуги сервиса "Мои информационные базы"</returns>
        public async Task<MyDatabasesCalculationCostDataDto> GetMyDatabasesCalculationCostForAccountServiceTypeDataAsync(Guid accountId,
            ResourceType systemServiceType)
        {
            var accountLocale = await dbLayer.AccountConfigurationRepository
                .AsQueryableNoTracking()
                .Include(x => x.Locale)
                .Where(x => x.AccountId == accountId)
                .Select(x => x.Locale.Name)
                .FirstOrDefaultAsync();

            var accountDatabasesResources = await dbLayer.DatabasesRepository
                .AsQueryableNoTracking()
                .Where(database =>
                    database.State == DatabaseState.Ready.ToString() ||
                    database.State == DatabaseState.NewItem.ToString() ||
                    database.State == DatabaseState.ProcessingSupport.ToString() ||
                    database.State == DatabaseState.TransferDb.ToString() ||
                    database.State == DatabaseState.RestoringFromTomb.ToString())
                .GroupBy(x => x.AccountId)
                .Select(x => new MyDatabasesResourcesActualValuesForAccountDto
                {
                    AccountId = x.Key,
                    DatabasesCount = x.Count(),
                    ServerDatabasesCount = x.Count(db =>
                        db.AccountDatabaseOnDelimiter == null && (!db.IsFile.HasValue || !db.IsFile.Value))
                })
                .FirstOrDefaultAsync(x => x.AccountId == accountId);

            var data = await dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .AsSplitQuery()
                .Include(x => x.DependServiceType)
                .ThenInclude(x => x.Service)
                .ThenInclude(x => x.ResourcesConfigurations)
                .Include(x => x.Resources)
                .ThenInclude(x => x.MyDatabasesResource)
                .Include(x => x.AccountRates)
                .Where(x => x.SystemServiceType == systemServiceType &&
                            x.DependServiceType.Service.ResourcesConfigurations.Any(z => z.ExpireDate.HasValue))
                .Include(x => x.Rates)
                .Select(x => new MyDatabasesCalculationCostDataDto
                {
                    ActualCost = x.AccountRates.Any(z => z.AccountId == accountId) ? x.AccountRates.FirstOrDefault(z => z.AccountId == accountId)!.Cost : x.Rates.FirstOrDefault()!.Cost,
                    PaidDatabasesCount = x.Resources.FirstOrDefault(z => z.AccountId == accountId) != null ? x.Resources.FirstOrDefault(z => z.AccountId == accountId)!.MyDatabasesResource != null ? x.Resources.FirstOrDefault(z => z.AccountId == accountId)!.MyDatabasesResource.PaidDatabasesCount : 0 : 0,
                    ExpireDate = x.DependServiceType.Service.ResourcesConfigurations.FirstOrDefault(z => z.AccountId == accountId)!.ExpireDate!.Value
                })
                .FirstOrDefaultAsync() ?? throw new NotFoundException(
                $"Не удалось получить данные для калькуляции услуги '{systemServiceType.Description()}'"); ;

            data.LocaleName = accountLocale;
            data.PlacementDatabasesCount = accountDatabasesResources != null ? systemServiceType == ResourceType.ServerDatabasePlacement ? accountDatabasesResources.ServerDatabasesCount : accountDatabasesResources.DatabasesCount : 0;

            return data;
        }

        /// <summary>
        /// Получить данные для рассчета стоимости доступа в конфигурацию
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceTypeId">ID услуги</param>
        /// <returns>Данные для рассчета стоимости доступа в конфигурацию</returns>
        public CalculationDataForAccessToConfiguration1CDto GetCalculationDataForAccessToConfiguration1C(Guid accountId,
            Guid serviceTypeId)
        {
            var accountLocale = dbLayer.AccountConfigurationRepository
                .AsQueryableNoTracking()
                .Include(x => x.Locale)
                .Where(x => x.AccountId == accountId)
                .Select(x => x.Locale.Name)
                .FirstOrDefault();

            var freeAccountResourcesCount = dbLayer.ResourceRepository
                .AsQueryableNoTracking()
                .Where(x => x.BillingServiceTypeId == serviceTypeId)
                .Select(x => new 
                { 
                    resource = x, 
                    resourceAccountId = x.AccountSponsorId ?? x.AccountId
                })
                .Where(x => x.resourceAccountId == accountId)
                .Select(x => x.resource)
                .Count(x => !x.Subject.HasValue);


            var data = dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .Include(x => x.DependServiceType)
                .ThenInclude(x => x.Service)
                .ThenInclude(x => x.ResourcesConfigurations.Where(z => z.AccountId == accountId))
                .Include(x => x.Rates)
                .Include(x => x.AccountRates.Where(z => z.AccountId == accountId))
                .Select(x => new CalculationDataForAccessToConfiguration1CDto
                {
                    MyDatabasesServiceExpireDate = x.DependServiceType.Service.ResourcesConfigurations
                        .FirstOrDefault().ExpireDate.Value,
                    Cost = x.AccountRates.FirstOrDefault() != null ? x.AccountRates.FirstOrDefault()!.Cost : x.Rates.FirstOrDefault().Cost
                })
                .FirstOrDefault();

            data.LocaleName = accountLocale;
            data.FreeResourcesCount = freeAccountResourcesCount;

            return data;

        }
    }
}
