﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.Domain.Enums;
using Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.Calculator.Providers
{
    /// <summary>
    /// Провайдер для расчета стоимости доступов
    /// </summary>
    internal class CalculateCostOfAcDbAccessesProvider(
        IUnitOfWork dbLayer,
        IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider,
        IMyDatabasesCalculationCostDataProvider myDatabasesCalculationCostDataProvider,
        ICreateAccountDatabaseDataProvider createAccountDatabaseDataProvider,
        IResourcesService resourcesService)
        : ICalculateCostOfAcDbAccessesProvider
    {
        /// <summary>
        /// Получить общую стоимость доступов в конфигурации
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceTypesIdsList">Список ID услуг сервиса</param>
        /// <param name="accountUserDataModelsList">Список моделей пользователей</param>
        /// <returns>Общая стоимость доступов в конфигурации</returns>
        public decimal GetTotalAccessesCostForConfigurations(Guid accountId, List<Guid> serviceTypesIdsList,
            List<AccountUserAccessDto> accountUserDataModelsList)
        {
            var accessesCostForConfigurations =
                CalculateAccessesCostForConfigurations(accountId, serviceTypesIdsList, accountUserDataModelsList);

            return accessesCostForConfigurations.Sum(access => access.AccessCost);
        }

        /// <summary>
        /// Рассчитать стоимость доступов в конфигурации
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceTypesIdsList">Список ID услуг сервиса</param>
        /// <param name="accountUserDataModelsList">Список моделей пользователей</param>
        /// <returns>Стоимость доступов в конфигурации</returns>
        public List<AccountUserLicenceForConfigurationDto> CalculateAccessesCostForConfigurations(Guid accountId, List<Guid> serviceTypesIdsList, 
            List<AccountUserAccessDto> accountUserDataModelsList, DateTime? paidDate = null)
        {
            var calculateResult = new List<AccountUserLicenceForConfigurationDto>();

            var resConfig = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);
            if (resConfig == null)
                return calculateResult;

            var selectedServiceTypesCost = GetSelectedServiceTypesCost(accountId, serviceTypesIdsList, paidDate);

            var calculateResultGroupedByServiceType =
                GetCalculateResultGroupedByServiceType(selectedServiceTypesCost, accountUserDataModelsList);


            var groupedByAccountUserResult = calculateResultGroupedByServiceType.GroupBy(res => res.AccountUserId).ToList();
            groupedByAccountUserResult.ForEach(result =>
            {
                var accessCost = result.Where(item => !item.HasLicense).Sum(item => item.AccessCost);
                var hasLicense = result.All(item => item.HasLicense);
                calculateResult.Add(new AccountUserLicenceForConfigurationDto
                {
                    AccountUserId = result.Key,
                    AccessCost = accessCost,
                    HasLicense = hasLicense
                });
            });

            return calculateResult;
        }

        /// <summary>
        /// Рассчитать стоимость доступов для загруженного файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="configirationId">ID конфигурации</param>
        /// <param name="configurationName">имя конфигурации</param>
        /// <returns>Стоимость доступов в конфигурации</returns>
        public AccountUserLicenceForUploudFileDto CalculateAccessesCostForUploadFile(Guid accountId, string configirationId, string configurationName)
        {

            AccountUserLicenceForUploudFileDto result = new AccountUserLicenceForUploudFileDto();
            Guid? serviceTypesId = null;
            var resConfig = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);
            if (resConfig == null)
                return result;

            if(string.IsNullOrEmpty(configirationId) && string.IsNullOrEmpty(configurationName))
                throw new NotFoundException($"Не найден шаблон  конфигурации по коду '{configirationId}' и имени '{configurationName}'.");

            if (!string.IsNullOrEmpty(configirationId))
            {
                var templateDelimiters = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(s => s.ConfigurationId == configirationId) ??
                                                    throw new NotFoundException($"Не найден шаблон  конфигурации по коду '{configirationId}'.");

                serviceTypesId = myDatabasesServiceTypeDataProvider.GetServiceTypeIdByDbTemplateId(templateDelimiters.TemplateId);
            }
            else
            {
                serviceTypesId = myDatabasesServiceTypeDataProvider.GetServiceTypeIdByConfigurationName(configurationName);
            }

            if (!serviceTypesId.HasValue)
                return result;

            result.ServiceTypesId = serviceTypesId.Value;

            List<Guid> serviceTypesIdsList = [serviceTypesId.Value];
            var selectedServiceTypesCost = GetSelectedServiceTypesCost(accountId, serviceTypesIdsList);
            var usersForGrantAccess = createAccountDatabaseDataProvider.GetAvailableUsersForGrantAccess(accountId);
            var accountUserDataModelsList = usersForGrantAccess.Select(user => new AccountUserAccessDto
            {
                HasAccess = true,
                UserId = user.UserId
            }).ToList();
            var calculateResultGroupedByServiceType =
                GetCalculateResultGroupedByServiceType(selectedServiceTypesCost, accountUserDataModelsList);

            
            var groupedByAccountUserResult = calculateResultGroupedByServiceType.GroupBy(res => res.AccountUserId).ToList();
            groupedByAccountUserResult.ForEach(r =>
            {
                var accessCost = r.Where(item => !item.HasLicense).Sum(item => item.AccessCost);
                var hasLicense = r.All(item => item.HasLicense);
                result.AccountUserLicence.Add(new AccountUserLicenceForConfigurationDto
                {
                    AccountUserId = r.Key,
                    AccessCost = accessCost,
                    HasLicense = hasLicense
                });
            });

            return result;
        }

        /// <summary>
        /// Получить список стоимостей выбранных услуг сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceTypesIdsList">Список выбранных ID услуг</param>
        /// <returns>Список стоимостей выбранных услуг сервиса "Мои инф. базы"</returns>
        private List<AccessForServiceTypeCostDto> GetSelectedServiceTypesCost(Guid accountId, List<Guid> serviceTypesIdsList, DateTime? paidDate = null)
        {
            var selectedServiceTypesCost = new List<AccessForServiceTypeCostDto>();
            serviceTypesIdsList = serviceTypesIdsList.Distinct().ToList();

            serviceTypesIdsList.ForEach(serviceTypeId =>
            {
                var calculationData =
                    myDatabasesCalculationCostDataProvider.GetCalculationDataForAccessToConfiguration1C(accountId,
                        serviceTypeId);
                var partialCost = BillingServicePartialCostCalculator.CalculateUntilExpireDateWithRoundedUp(calculationData.Cost,
                    calculationData.MyDatabasesServiceExpireDate, paidDate);

                selectedServiceTypesCost.Add(new AccessForServiceTypeCostDto
                {
                    ServiceTypeId = serviceTypeId,
                    PartialCost = partialCost,
                    FreeResourcesCount = calculationData.FreeResourcesCount
                });
            });

            return selectedServiceTypesCost;
        }

        /// <summary>
        /// Получить результаты расчета сгруппированный по услуге
        /// </summary>
        /// <param name="selectedServiceTypesCost">Список стоимостей выбранных услуг сервиса "Мои инф. базы"</param>
        /// <param name="accountUserDataModelsList">Список моделей пользователей</param>
        /// <returns>Результаты расчета сгруппированный по услуге</returns>
        private IEnumerable<AccountUserLicenceForConfigurationDto> GetCalculateResultGroupedByServiceType(
            List<AccessForServiceTypeCostDto> selectedServiceTypesCost, List<AccountUserAccessDto> accountUserDataModelsList)
        {
            var calculateResult = new List<AccountUserLicenceForConfigurationDto>();

            if (accountUserDataModelsList == null || !accountUserDataModelsList.Any())
                return calculateResult;

            selectedServiceTypesCost.ForEach(serviceTypeCost =>
            {
                accountUserDataModelsList.ForEach(accountUser =>
                {
                    var accountUserResource = dbLayer.ResourceRepository.FirstOrDefault(res =>
                        res.BillingServiceTypeId == serviceTypeCost.ServiceTypeId && res.Subject == accountUser.UserId);

                    var hasLicense = accountUserResource != null;
                    if (accountUserResource == null && serviceTypeCost.FreeResourcesCount > 0 && accountUser.HasAccess)
                    {
                        hasLicense = true;
                        serviceTypeCost.FreeResourcesCount -= 1;
                    }

                    var model = new AccountUserLicenceForConfigurationDto
                    {
                        AccountUserId = accountUser.UserId,
                        AccessCost = serviceTypeCost.PartialCost,
                        HasLicense = hasLicense
                    };

                    calculateResult.Add(model);
                });
            });
            return calculateResult;
        }
    }
}
