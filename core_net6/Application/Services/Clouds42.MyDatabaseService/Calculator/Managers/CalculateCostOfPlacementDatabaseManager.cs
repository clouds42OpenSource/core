﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.Calculator.Managers
{
    /// <summary>
    /// Менеджер для подсчета стоимости размещения инф. базы для аккаунта 
    /// </summary>
    public class CalculateCostOfPlacementDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICalculateCostOfPlacementDatabaseProvider calculateCostOfPlacementDatabaseProvider)
        : BaseManager(accessProvider,
            dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Посчитать стоимость размещения инф. баз для аккаунта
        /// </summary>
        /// <param name="model">Модель подсчета стоимости размещения инф. баз для аккаунта</param>
        /// <returns>Модель результата подсчета стоимости
        /// размещения инф. баз для аккаунта</returns>
        public ManagerResult<CalculationCostOfPlacementDatabasesResultDto> CalculateCostOfPlacementDatabases(
            CalculateCostOfPlacementDatabasesDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Database_CalculateCostOfCreating, () => model.AccountId);
                var result = calculateCostOfPlacementDatabaseProvider.CalculateCostOfPlacementDatabases(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $@"[Ошибка подсчета стоимости размещения инф. баз для аккаунта] '{model.AccountId}' 
                                    по количеству '{model.DatabasesForPlacementCount}'");

                return PreconditionFailed<CalculationCostOfPlacementDatabasesResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Посчитать стоимость размещения инф. баз для аккаунта
        /// </summary>
        /// <param name="model">Модель подсчета стоимости размещения инф. баз для аккаунта</param>
        /// <returns>Модель результата подсчета стоимости
        /// размещения инф. баз для аккаунта</returns>
        public async Task<ManagerResult<TryChangeTariffResultDto>> CalculateCostDatabases(
            CalculateCostOfPlacementDatabasesDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Database_CalculateCostOfCreating, () => model.AccountId);
                var result = await calculateCostOfPlacementDatabaseProvider.CalculateCostDatabases(model);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"""
                     [Подсчет стоимости размещения инф. баз для аккаунта '{model.AccountId}]' 
                                                         по количеству '{model.DatabasesForPlacementCount}' завершился с ошибкой : {ex.Message}
                     """);

                return PreconditionFailed<TryChangeTariffResultDto>(ex.Message);
            }
        }
    }
}
