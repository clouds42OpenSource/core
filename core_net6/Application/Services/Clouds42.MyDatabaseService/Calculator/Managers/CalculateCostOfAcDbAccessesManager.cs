﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.MyDatabaseService.Calculator.Managers
{
    /// <summary>
    /// Менеджер для расчета стоимости доступов
    /// </summary>
    public class CalculateCostOfAcDbAccessesManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICalculateCostOfAcDbAccessesProvider calculateCostOfAcDbAccessesProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Рассчитать стоимость доступов в конфигурации
        /// </summary>
        /// <param name="serviceTypesIdsList">Список ID услуг сервиса</param>
        /// <param name="accountUserDataModelsList">Список моделей пользователей</param>
        /// <returns>Стоимость доступов в конфигурации</returns>
        public ManagerResult<List<AccountUserLicenceForConfigurationDto>> CalculateAccessesCostForConfigurations(
            List<Guid> serviceTypesIdsList, List<AccountUserAccessDto> accountUserDataModelsList, Guid? accountId = null, DateTime? paidDate = null)
        {
            if(accountId == null || accountId == Guid.Empty) 
                accountId = AccessProvider.ContextAccountId;
            
            try
            {
                AccessProvider.HasAccess(ObjectAction.Database_CalculateCostOfCreating, () => accountId);

                if (paidDate is not null && (DateTime.Now > paidDate || paidDate.Value.Day > DateTime.Now.AddDays(1).Day))
                    return PreconditionFailed<List<AccountUserLicenceForConfigurationDto>>("Дата оплаты сервиса может быть выбрана только в течение двух дней");

                return Ok(calculateCostOfAcDbAccessesProvider.CalculateAccessesCostForConfigurations(accountId.Value,
                    serviceTypesIdsList, accountUserDataModelsList, paidDate));
            }
            catch (Exception ex)
            {
                var message =
                    $"[Ошибка рассчета стоимости доступов в конфигурации для аккаунта] {accountId}";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<List<AccountUserLicenceForConfigurationDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Рассчитать стоимость доступов для загруженного файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="configirationId">ID конфигурации</param>
        /// /// <param name="configurationName">имя конфигурации</param>
        /// <returns>Стоимость доступов в конфигурации</returns>
        public ManagerResult<AccountUserLicenceForUploudFileDto> CalculateAccessesCostForUploadFile(
            Guid accountId, string configirationId, string configurationName)
        {

            try
            {
                AccessProvider.HasAccess(ObjectAction.Database_CalculateCostOfCreating, () => accountId);

                return Ok(calculateCostOfAcDbAccessesProvider.CalculateAccessesCostForUploadFile(accountId, configirationId, configurationName));
            }
            catch (Exception ex)
            {
                var message =
                    $"[Ошибка рассчета стоимости доступов в конфигурации для аккаунта] {accountId}";
                _handlerException.Handle(ex, message);
                return PreconditionFailed<AccountUserLicenceForUploudFileDto>(ex.Message);
            }
        }
    }
}
