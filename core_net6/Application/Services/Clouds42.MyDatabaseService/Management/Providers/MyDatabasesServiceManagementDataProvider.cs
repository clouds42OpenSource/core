﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceManagement;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Management.Interfaces;
using CommonLib.Enums;
using BillingAccountEntity = Clouds42.Domain.DataModels.billing.BillingAccount;

namespace Clouds42.MyDatabaseService.Management.Providers
{
    /// <summary>
    /// Провайдер для работы с данными управления
    /// сервиса "Мои информационные базы"
    /// </summary>
    internal class MyDatabasesServiceManagementDataProvider(
        IOptimalRateProvider optimalRateProvider,
        ILimitOnFreeCreationDbForAccountDataProvider limitOnFreeCreationDbForAccountDataProvider)
        : IMyDatabasesServiceManagementDataProvider
    {
        /// <summary>
        /// Получить данные по управлению
        /// сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <returns>Данные по управлению
        /// сервиса "Мои информационные базы"</returns>
        public MyDatabasesServiceManagementDataDto GetManagementData(BillingAccountEntity billingAccount)
        {
            return new MyDatabasesServiceManagementDataDto
            {
                CostOfCreatingDbOverFreeLimit = optimalRateProvider
                    .GetOptimalRate(billingAccount, ResourceType.CountOfDatabasesOverLimit).Cost,

                ServerDatabasePlacementCost = optimalRateProvider
                    .GetOptimalRate(billingAccount, ResourceType.ServerDatabasePlacement).Cost,

                LimitOnFreeCreationDb =
                    limitOnFreeCreationDbForAccountDataProvider.GetLimitOnFreeCreationDbValue(billingAccount.Id)
            };
        }
    }
}