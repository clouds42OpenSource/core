﻿using System.Text;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceManagement;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Management.Interfaces;
using Clouds42.MyDatabaseService.Contracts.Management.Models;
using Clouds42.MyDatabaseService.Contracts.Operations.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;
using BillingAccountsEntity = Clouds42.Domain.DataModels.billing.BillingAccount;

namespace Clouds42.MyDatabaseService.Management.Providers
{
    /// <summary>
    /// Провайдер для управления сервисом "Мои информационные базы"
    /// </summary>
    internal class ManageMyDatabasesServiceProvider : IManageMyDatabasesServiceProvider
    {
        private readonly IRecalculateMyDatabasesServiceResourcesProvider
            _recalculateMyDatabasesServiceResourcesProvider;

        private readonly ISystemServiceTypeDataProvider _serviceTypeDataProvider;
        private readonly IOptimalRateProvider _optimalRateProvider;
        private readonly IUnitOfWork _dbLayer;
        private readonly IUpdateAccountRateProvider _updateAccountRateProvider;
        private readonly IMyDatabasesServiceDataProvider _myDatabasesServiceDataProvider;
        private readonly IUpdateLimitOnFreeCreationDbForAccountProvider _updateLimitOnFreeCreationDbForAccountProvider;
        private readonly ILimitOnFreeCreationDbForAccountDataProvider _limitOnFreeCreationDbForAccountDataProvider;
        private readonly IHandlerException _handlerException;
        private readonly ISender _sender;
        /// <summary>
        /// Функции для применения изменений для сервиса "Мои информационные базы"
        /// </summary>
        private readonly IList<Func<ManageMyDatabasesServiceDto, BillingAccountsEntity, StringBuilder, bool>>
            _applyingChangesFunctions;

        public ManageMyDatabasesServiceProvider(
            IRecalculateMyDatabasesServiceResourcesProvider recalculateMyDatabasesServiceResourcesProvider,
            ISystemServiceTypeDataProvider serviceTypeDataProvider,
            IOptimalRateProvider optimalRateProvider,
            IUnitOfWork dbLayer,
            IUpdateAccountRateProvider updateAccountRateProvider,
            IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider,
            IUpdateLimitOnFreeCreationDbForAccountProvider updateLimitOnFreeCreationDbForAccountProvider,
            ILimitOnFreeCreationDbForAccountDataProvider limitOnFreeCreationDbForAccountDataProvider,
            IHandlerException handlerException, ISender sender)
        {
            _recalculateMyDatabasesServiceResourcesProvider = recalculateMyDatabasesServiceResourcesProvider;
            _serviceTypeDataProvider = serviceTypeDataProvider;
            _optimalRateProvider = optimalRateProvider;
            _dbLayer = dbLayer;
            _updateAccountRateProvider = updateAccountRateProvider;
            _myDatabasesServiceDataProvider = myDatabasesServiceDataProvider;
            _updateLimitOnFreeCreationDbForAccountProvider = updateLimitOnFreeCreationDbForAccountProvider;
            _limitOnFreeCreationDbForAccountDataProvider = limitOnFreeCreationDbForAccountDataProvider;
            _handlerException = handlerException;
            _sender = sender;

            _applyingChangesFunctions = new List<Func<ManageMyDatabasesServiceDto, BillingAccountsEntity, StringBuilder, bool>>
            {
                TryChangeLimitOnFreeCreationDb,
                TryChangeCostOfCreatingDbOverFreeLimit,
                TryChangeServerDatabasePlacementCost
            };

        }

        /// <summary>
        /// Выполнить управление сервисом "Мои информационные базы"
        /// </summary>
        /// <param name="model">Модель для управления сервисом "Мои информационные базы"</param>
        /// <param name="changeMessageBuilder">Билдер сообщений об изменениях</param>
        public void Manage(ManageMyDatabasesServiceDto model, StringBuilder changeMessageBuilder)
        {
            var billingAccount = _dbLayer.BillingAccountRepository.GetBillingAccount(model.AccountId);

            if (billingAccount == null)
                return;

            using var dbScope = _dbLayer.SmartTransaction.Get();
            try
            {
                var needRecalculateServiceCost = _applyingChangesFunctions
                    .Select(func => func(model, billingAccount, changeMessageBuilder)).ToList()
                    .Any(result => result);

                if (needRecalculateServiceCost)
                    _sender.Send(new RecalculateResourcesConfigurationCostCommand([model.AccountId],
                        [_myDatabasesServiceDataProvider.GetMyDatabasesServiceId()], Clouds42Service.MyDatabases)).Wait();

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка управления сервисом \"Мои информационные базы\" для аккаунта] '{model.AccountId}'");

                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Попытаться изменить лимит на бесплатное создание баз
        /// </summary>
        /// <param name="model">Модель для управления сервисом "Мои информационные базы"</param>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="changeMessageBuilder">Билдер сообщений об изменениях</param>
        /// <returns>Была ли изменена стоимость</returns>
        private bool TryChangeLimitOnFreeCreationDb(ManageMyDatabasesServiceDto model,
            BillingAccountsEntity billingAccount, StringBuilder changeMessageBuilder)
        {
            if (!model.LimitOnFreeCreationDb.HasValue)
                return false;

            var limit = _limitOnFreeCreationDbForAccountDataProvider.GetLimitOnFreeCreationDbValue(billingAccount.Id);

            if (limit == model.LimitOnFreeCreationDb)
                return false;

            _updateLimitOnFreeCreationDbForAccountProvider.UpdateIfAvailableOtherwiseCreate(billingAccount.Id,
                model.LimitOnFreeCreationDb.Value);

            _recalculateMyDatabasesServiceResourcesProvider.RecalculateResourcesForServiceType(billingAccount.Id,
                _serviceTypeDataProvider.GetServiceTypeBySystemServiceType(ResourceType.CountOfDatabasesOverLimit).Id);

            changeMessageBuilder.Append(
                $"Изменено количество бесплатных баз с \"{limit}\" на {model.LimitOnFreeCreationDb.Value} шт.");

            return true;
        }

        /// <summary>
        /// Попытаться изменить стоимость создания базы, свыше бесплатного лимита
        /// </summary>
        /// <param name="model">Модель для управления сервисом "Мои информационные базы"</param>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="changeMessageBuilder">Билдер сообщений об изменениях</param>
        /// <returns>Была ли изменена стоимость</returns>
        private bool TryChangeCostOfCreatingDbOverFreeLimit(ManageMyDatabasesServiceDto model,
            BillingAccountsEntity billingAccount, StringBuilder changeMessageBuilder) => TryChangeServiceTypeCost(
            billingAccount, ResourceType.CountOfDatabasesOverLimit, changeMessageBuilder,
            model.CostOfCreatingDbOverFreeLimit);

        /// <summary>
        /// Попытаться изменить стоимость размещения серверной базы
        /// </summary>
        /// <param name="model">Модель для управления сервисом "Мои информационные базы"</param>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="changeMessageBuilder">Билдер сообщений об изменениях</param>
        /// <returns>Была ли изменена стоимость</returns>
        private bool TryChangeServerDatabasePlacementCost(ManageMyDatabasesServiceDto model,
            BillingAccountsEntity billingAccount, StringBuilder changeMessageBuilder) => TryChangeServiceTypeCost(
            billingAccount, ResourceType.ServerDatabasePlacement, changeMessageBuilder,
            model.ServerDatabasePlacementCost);

        /// <summary>
        /// Попытаться изменить стоимость услуги сервиса
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="resourceType">Тип системной услуги</param>
        /// <param name="changeMessageBuilder">Билдер сообщений об изменениях</param>
        /// <param name="newCost">Новая стоимсть</param>
        /// <returns>Была ли изменена стоимость</returns>
        private bool TryChangeServiceTypeCost(BillingAccountsEntity billingAccount, ResourceType resourceType,
            StringBuilder changeMessageBuilder, decimal? newCost)
        {
            if (!newCost.HasValue)
                return false;

            var serviceTypeRateData = GetServiceTypeRateData(billingAccount, resourceType);

            if (serviceTypeRateData.Cost == newCost)
                return false;

            ChangeServiceTypeCost(serviceTypeRateData, newCost.Value);

            changeMessageBuilder.Append(
                $"Изменена стоимость ресурса \"{serviceTypeRateData.ServiceTypeName}\" с {serviceTypeRateData.Cost:0.00} на {newCost.Value:0.00}.");

            return true;
        }

        /// <summary>
        /// Изменить стоимость услуги
        /// </summary>
        /// <param name="serviceTypeRateData">Модель данных тарифа по услуге</param>
        /// <param name="newCost">Новая стоимость услуги</param>
        private void ChangeServiceTypeCost(ServiceTypeRateDataModel serviceTypeRateData, decimal newCost)
        {
            _updateAccountRateProvider.UpdateIfAvailableOtherwiseCreate(new UpdateAccountRateDto
            {
                AccountId = serviceTypeRateData.AccountId,
                BillingServiceTypeId = serviceTypeRateData.ServiceTypeId,
                Cost = newCost
            });

            _recalculateMyDatabasesServiceResourcesProvider.RecalculateResourcesForServiceType(
                serviceTypeRateData.AccountId, serviceTypeRateData.ServiceTypeId);
        }

        /// <summary>
        /// Получить данные тарифа по услуге
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="resourceType">Тип системной услуги</param>
        /// <returns>Модель данных тарифа по услуге</returns>
        public ServiceTypeRateDataModel GetServiceTypeRateData(BillingAccountsEntity billingAccount, ResourceType resourceType)
        {
            var serviceType =
                _serviceTypeDataProvider.GetServiceTypeBySystemServiceType(resourceType);

            var rate = _optimalRateProvider.GetOptimalRate(billingAccount, serviceType.Id);

            return new ServiceTypeRateDataModel
            {
                Cost = rate.Cost,
                ServiceTypeId = serviceType.Id,
                AccountId = billingAccount.Id,
                ServiceTypeName = serviceType.Name
            };
        }
    }
}
