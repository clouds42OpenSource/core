﻿using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Провайдер на удаление поставщика
    /// </summary>
    internal class DeleteSupplierReferralAccountProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : SupplierReferenceBaseProvider(dbLayer, logger, handlerException), IDeleteSupplierReferralAccountProvider
    {
        /// <summary>
        /// Удалить реферальный аккаунт у поставщика
        /// </summary>
        /// <param name="supplierId">ID Поставщика</param>
        /// <param name="accountId">ID Аккаунта</param>
        public void DeleteSupplierReferralAccount(Guid supplierId, Guid accountId)
        {
            var supplierReferralAccount = DbLayer.SupplierReferralAccountRepository
                .FirstOrDefault(w => w.SupplierId == supplierId && w.ReferralAccountId == accountId);

            if (supplierReferralAccount == null)
                throw new InvalidOperationException($"Не найден реферальный аккаунт в привязках у поставщика. Поставщик: {supplierId}, Аккаунт: {accountId}");

            DbLayer.SupplierReferralAccountRepository.Delete(supplierReferralAccount);
            DbLayer.Save();
        }
    }
}
