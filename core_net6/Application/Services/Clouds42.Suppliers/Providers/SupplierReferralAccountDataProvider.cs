﻿using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Провайдер для получения реферальных аккаунтов поставщика
    /// </summary>
    internal class SupplierReferralAccountDataProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : SupplierReferenceBaseProvider(dbLayer, logger, handlerException),
            ISupplierReferralAccountDataProvider
    {
        /// <summary>
        /// Получить список рефералов для поставщика
        /// Key - ID реферального аккаунта
        /// Value - Название реферального аккаунта
        /// </summary>
        /// <param name="id">ID поставщика</param>
        /// <returns>Cписок словарей рефералов для поставщика</returns>
        public KeyValuePair<Guid, string>[] GetReferralAccountsBySupplierId(Guid id) => DbLayer
            .SupplierReferralAccountRepository
            .WhereLazy(s => s.SupplierId == id)
            .ToDictionary(s => s.ReferralAccountId, s => s.ReferralAccount.AccountCaption.Replace("\"", "'"))
            .ToArray();

        /// <summary>
        /// Найти аккаунт по значению поисковика
        /// </summary>
        /// <param name="searchValue">Значения поисковика</param>
        /// <returns>Cписок словарей рефералов для поставщика</returns>
        public KeyValuePair<Guid, string>[] GetReferralAccountsBySearchValue(string searchValue)
        {
            if (string.IsNullOrEmpty(searchValue))
                return [];

            return DbLayer.AccountsRepository
                .WhereLazy(a =>
                    a.AccountCaption.ToLower().Contains(searchValue.ToLower()) ||
                    a.IndexNumber.ToString().Contains(searchValue))
                .ToDictionary(a => a.Id, a => a.AccountCaption)
                .ToArray();
        }
    }
}
