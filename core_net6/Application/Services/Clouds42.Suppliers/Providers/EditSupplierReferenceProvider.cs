﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Провайдер редактирования поставщика
    /// </summary>
    internal class EditSupplierReferenceProvider(
        IUnitOfWork dbLayer,
        ICloudFileProvider cloudFileProvider,
        ISupplierReferenceDataProvider supplierReferenceDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : SupplierReferenceBaseProvider(dbLayer, logger, handlerException), IEditSupplierReferenceProvider
    {
        /// <summary>
        /// Редактировать поставщика
        /// </summary>
        /// <param name="supplier">Данные поставщика на редактирование</param>
        public void EditSupplier(SupplierDto supplier)
        {
            var currentSupplier = supplierReferenceDataProvider.GetDomainSupplierById(supplier.Id);

            var duplicateSupplier = DbLayer.SupplierRepository.FirstOrDefault(w =>
                (w.Name == supplier.Name || w.Code == supplier.Code) && w.Id != supplier.Id);

            if (duplicateSupplier != null)
                throw new InvalidOperationException(
                    $"Найден дубликат при редактировании поставщика. Название: {duplicateSupplier.Name}, Код поставщика: {duplicateSupplier.Code}");

            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                if (!string.IsNullOrEmpty(supplier.CloudFile?.FileName) &&
                    supplier.CloudFile?.FileName != currentSupplier.Agreement?.FileName)
                    supplier.AgreementId = cloudFileProvider.CreateCloudFile(new FileDataDto<IFormFile>
                    {
                        ContentType = supplier.CloudFile.ContentType,
                        FileName = supplier.CloudFile.FileName,
                        Content = supplier.CloudFile
                    });

                AgreementCheckForDefaultSupplier(supplier.AgreementId.IsNullOrEmpty(), supplier.IsDefault);

                var supplierDomainModel = MappingSupplier(currentSupplier, supplier);

                DbLayer.SupplierRepository.Update(supplierDomainModel);
                DbLayer.Save();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка редактирования поставщика]");
                transaction.Rollback();
                throw;
            }
        }
    }
}
