﻿using Clouds42.Domain.DataModels.Supplier;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Провайдер для создания реферальных аккаунтов поставщика
    /// </summary>
    internal class CreateSupplierReferralAccountProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : SupplierReferenceBaseProvider(dbLayer, logger, handlerException), ICreateSupplierReferralAccountProvider
    {
        /// <summary>
        /// Добавить реферальный аккаунт для поставщика
        /// </summary>
        /// <param name="supplierId">ID Поставщика</param>
        /// <param name="accountId">ID Аккаунта</param>
        public void CreateSupplierReferralAccount(Guid supplierId, Guid accountId)
        {
            var isCurrentSupplierReferralAccountExist = DbLayer.SupplierReferralAccountRepository.Any(s => s.SupplierId == supplierId && s.ReferralAccountId == accountId);

            if (isCurrentSupplierReferralAccountExist)
                throw new InvalidOperationException("Данный аккаунт уже привязан к поставщику");

            DbLayer.SupplierReferralAccountRepository.Insert(new SupplierReferralAccount { SupplierId = supplierId, ReferralAccountId = accountId });
            DbLayer.Save();
        }

        /// <summary>
        /// Сменить поставщика для аккаунта
        /// </summary>
        /// <param name="supplierId">ID Поставщика</param>
        /// <param name="accountId">ID Аккаунта</param>
        public void EditSupplierForAccount(Guid supplierId, Guid accountId)
        {
            var isEditSupplier = DbLayer.SupplierRepository.FirstOrDefault(s => s.Id == supplierId);

            if(isEditSupplier == null)
                throw new InvalidOperationException("Поставщик не найден");

            var accountConfig = DbLayer.AccountConfigurationRepository.FirstOrDefault(a => a.AccountId == accountId);

            if(accountConfig == null)
                throw new InvalidOperationException("Аккаунт не найден");

            if (isEditSupplier.Id == accountConfig.SupplierId)
                throw new InvalidOperationException("Данный аккаунт уже привязан к этому поставщику");

            Logger.Info($"Смена поставщика с {accountConfig.Supplier.Name} на {isEditSupplier.Name} для аккаунта {accountConfig.Account.IndexNumber}");

            accountConfig.SupplierId = isEditSupplier.Id;
            DbLayer.AccountConfigurationRepository.Update(accountConfig);
            DbLayer.Save();

        }
    }
}
