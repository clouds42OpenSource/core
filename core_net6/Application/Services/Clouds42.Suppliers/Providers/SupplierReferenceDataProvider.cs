﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Constants;
using Clouds42.Suppliers.Contracts.Interfaces;
using PagedList.Core;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Провайдер получения поставщиков
    /// </summary>
    internal class SupplierReferenceDataProvider(
        IUnitOfWork dbLayer,
        ILocaleProvider localeProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : SupplierReferenceBaseProvider(dbLayer, logger, handlerException), ISupplierReferenceDataProvider
    {
        /// <summary>
        /// Получить всех поставщиков с пагинацией
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <returns>Всех поставщиков с пагинацией</returns>
        public SuppliersPaginationDto GetSuppliersPagedList(int page)
        {
            var suppliers = DbLayer.SupplierRepository
                .WhereLazy()
                .OrderBy(s => s.Name)
                .Select(s => new SupplierTableDto
                {
                    Id = s.Id,
                    IsDefault = s.IsDefault,
                    LocaleName = s.Locale.Name,
                    Name = s.Name,
                    Code = s.Code
                });

            return GetPagedTableSuppliers(page, suppliers);
        }

        /// <summary>
        /// Получить поставшика по ID
        /// </summary>
        /// <param name="id">ID поставщика</param>
        /// <returns>Модель поставщика</returns>
        public SupplierDto GetSupplierById(Guid id)
        {
            var currentSupplier = GetDomainSupplierById(id);

            return new SupplierDto
            {
                Id = currentSupplier.Id,
                IsDefault = currentSupplier.IsDefault,
                AgreementId = currentSupplier.AgreementId,
                Name = currentSupplier.Name,
                LocaleId = currentSupplier.LocaleId,
                AgreementFileName = currentSupplier.Agreement?.FileName,
                Code = currentSupplier.Code,
                PrintedHtmlFormInvoiceId = currentSupplier.PrintedHtmlFormInvoiceId,
                PrintedHtmlFormInvoiceReceiptId = currentSupplier.PrintedHtmlFormInvoiceReceiptId
            };
        }

        /// <summary>
        /// Получить доменный модель поставщика по ID
        /// </summary>
        /// <param name="id">ID поставщика</param>
        /// <returns>Доменный модель поставщика</returns>
        public Supplier GetDomainSupplierById(Guid id) => DbLayer.SupplierRepository.FirstOrDefault(s => s.Id == id);

        /// <summary>
        /// Получить файл оферты поставщика
        /// </summary>
        /// <param name="id">ID поставщика</param>
        /// <returns>Модель файла оферты поставщика</returns>
        public CloudFileDto GetAgreementCloudFileBySupplierId(Guid id)
        {
            var currentSupplier = GetDomainSupplierById(id);

            return new CloudFileDto
            {
                Id = currentSupplier.AgreementId,
                FileName = currentSupplier.Agreement?.FileName,
                ContentType = currentSupplier.Agreement?.ContentType,
                Bytes = currentSupplier.Agreement?.Content
            };
        }

        /// <summary>
        /// Получить элементы комбобокса
        /// при инциализации страниц создания/редактирования поставщика
        /// </summary>
        /// <returns>Все локали, все печатные формы</returns>
        public InitSelectListItemsDto GetInitCreateEditPageItems()
        {
            var printedHtmlForms = DbLayer.PrintedHtmlFormRepository.WhereLazy()
                .Select(f => new ComboboxItemDto<Guid> {Text = f.Name, Value = f.Id})
                .ToList();

            printedHtmlForms.Insert(0,
                new ComboboxItemDto<Guid> {Text = "Выберите печатную форму", Value = Guid.Empty});

            var locales = localeProvider.GetLocales()
                .Select(l => new ComboboxItemDto<Guid> {Text = l.Country, Value = l.Id})
                .ToList();

            return new InitSelectListItemsDto
            {
                Locales = locales,
                PrintedHtmlForms = printedHtmlForms
            };
        }

        /// <summary>
        /// Приватный метод для пагинации
        /// </summary>
        /// <param name="page">номер страницы</param>
        /// <param name="suppliers">коллекция поставщиков</param>
        /// <returns>Коллекция поставщиков с пагинацией</returns>
        private SuppliersPaginationDto GetPagedTableSuppliers(int page, IQueryable<SupplierTableDto> suppliers)
        {
            var tableCount = suppliers.Count();

            var tablePagedList = suppliers
                .ToPagedList(page, SupplierReferenceConstants.ItemsPerPage)
                .ToArray();

            return new SuppliersPaginationDto
            {
                Records = tablePagedList,
                Pagination = new PaginationBaseDto(page, tableCount, SupplierReferenceConstants.ItemsPerPage)
            };
        }
    }
}
