﻿using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Базовый провайдер для поставщика
    /// </summary>
    internal abstract class SupplierReferenceBaseProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        protected readonly IUnitOfWork DbLayer = dbLayer;
        protected readonly ILogger42 Logger = logger;
        protected readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Маппинг модели поставщика с сущностью поставщика
        /// </summary>
        /// <param name="supplierDomainModel">Сущность поставщика</param>
        /// <param name="supplierModel">Модель поставщика</param>
        /// <returns>Сущность поставщика</returns>
        protected Supplier MappingSupplier(Supplier supplierDomainModel, SupplierDto supplierModel)
        {
            supplierDomainModel.Name = supplierModel.Name;
            supplierDomainModel.LocaleId = supplierModel.LocaleId;
            supplierDomainModel.IsDefault = supplierModel.IsDefault;
            supplierDomainModel.AgreementId = supplierModel.AgreementId ??
                                              GetDefaultSupplierAgreementIdByLocaleId(supplierModel.LocaleId);
            supplierDomainModel.Code = supplierModel.Code;
            supplierDomainModel.PrintedHtmlFormInvoiceId = supplierModel.PrintedHtmlFormInvoiceId == Guid.Empty
                ? null
                : supplierModel.PrintedHtmlFormInvoiceId;
            supplierDomainModel.PrintedHtmlFormInvoiceReceiptId = supplierModel.PrintedHtmlFormInvoiceReceiptId == Guid.Empty
                ? null
                : supplierModel.PrintedHtmlFormInvoiceReceiptId;

            return supplierDomainModel;
        }

        /// <summary>
        /// Получить ID договора оферты у дефолтного поставщика по ID локали
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>ID договора оферты</returns>
        protected Guid GetDefaultSupplierAgreementIdByLocaleId(Guid localeId) =>
            DbLayer.SupplierRepository.FirstOrDefault(sp => sp.Locale.ID == localeId && sp.IsDefault)?.AgreementId ??
            throw new NotFoundException($"Не найден стандартный поставщик по ID локали: {localeId}");

        /// <summary>
        /// Проверка файла договора оферты
        /// </summary>
        /// <param name="isAgreementEmpty">Проверка файла договора оферты</param>
        /// <param name="isDefault">Стандартный поставщик на локаль</param>
        protected void AgreementCheckForDefaultSupplier(bool isAgreementEmpty, bool isDefault)
        {
            if (isAgreementEmpty && isDefault)
                throw new ArgumentException("Договор оферты обязателен, если поставщик стандартный на локаль.");
        }
    }
}
