﻿using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Провайдер на удаление поставщика
    /// </summary>
    internal class DeleteSupplierReferenceProvider(
        IUnitOfWork dbLayer,
        ISupplierReferenceDataProvider supplierReferenceDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : SupplierReferenceBaseProvider(dbLayer, logger, handlerException), IDeleteSupplierReferenceProvider
    {
        /// <summary>
        /// Удалить поставщика
        /// </summary>
        /// <param name="id">ID поставщика</param>
        public void DeleteSupplier(Guid id)
        {
            var currentSupplier = supplierReferenceDataProvider.GetDomainSupplierById(id);
            var hasSupplierReferralAccount = DbLayer.SupplierReferralAccountRepository.Any(r => r.SupplierId == id);

            if (hasSupplierReferralAccount)
                throw new InvalidOperationException($"Нельзя удалить поставщика {currentSupplier.Name}. Причина: У поставщика есть реферальные аккаунты.");

            DbLayer.SupplierRepository.Delete(currentSupplier);
            DbLayer.Save();
        }
    }
}
