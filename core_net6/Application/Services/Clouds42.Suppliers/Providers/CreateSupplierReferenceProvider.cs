﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Провайдер создания поставщика
    /// </summary>
    internal class CreateSupplierReferenceProvider(
        IUnitOfWork dbLayer,
        ICloudFileProvider cloudFileProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : SupplierReferenceBaseProvider(dbLayer, logger, handlerException), ICreateSupplierReferenceProvider
    {
        /// <summary>
        /// Создавть нового поставщика
        /// </summary>
        /// <param name="newSupplier">Новый поставщик</param>
        public void CreateSupplier(SupplierDto newSupplier)
        {
            var duplicateSupplier =
                DbLayer.SupplierRepository.FirstOrDefault(
                    w => w.Name == newSupplier.Name || w.Code == newSupplier.Code);

            if (duplicateSupplier != null)
                throw new InvalidOperationException(
                    $"Найден дубликат при создании поставщика. Название: {duplicateSupplier.Name}, Код поставщика: {duplicateSupplier.Code}");

            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                if (!string.IsNullOrEmpty(newSupplier.CloudFile?.FileName))
                    newSupplier.AgreementId = cloudFileProvider.CreateCloudFile(new FileDataDto<IFormFile>
                    {
                        Content = newSupplier.CloudFile,
                        ContentType = newSupplier.CloudFile.ContentType,
                        FileName = newSupplier.CloudFile.FileName
                    });

                AgreementCheckForDefaultSupplier(newSupplier.AgreementId.IsNullOrEmpty(), newSupplier.IsDefault);

                var supplierDomainModel = MappingSupplier(new Supplier(), newSupplier);

                DbLayer.SupplierRepository.Insert(supplierDomainModel);
                DbLayer.Save();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при создании поставщика]");
                transaction.Rollback();
                throw;
            }
        }
    }
}
