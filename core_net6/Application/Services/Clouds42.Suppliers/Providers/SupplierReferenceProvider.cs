﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;
using PagedList.Core;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Управление справочником поставщиков
    /// </summary>
    internal class SupplierReferenceProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISupplierReferenceProvider
    {
        /// <summary>
        /// Получить список поставщиков по фильтру
        /// </summary>
        /// <param name="filter">Фильтр</param>
        public IPagedList<SupplierModelDto> GetSuppliersPagedList(PagedListFilter? filter = null)
        {
            try
            {
                filter ??= new PagedListFilter();

                var collection = dbLayer.SupplierRepository.WhereLazy().OrderBy(x => x.Name).Select(w => new SupplierModelDto
                {
                    Id = w.Id,
                    IsDefault = w.IsDefault,
                    AgreementId = w.AgreementId,
                    LocaleId = w.LocaleId,
                    Locale = new LocaleDto
                    {
                        Name = w.Locale.Name,
                        Currency = w.Locale.Currency
                    },
                    Name = w.Name,
                    Code = w.Code
                }).ToPagedList(filter.CurrentPage, filter.PageSize);

                return collection;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при получении поставщиков]");
                throw;
            }
        }

        /// <summary>
        /// Создать нового поставщика
        /// </summary>
        /// <param name="supplier">Модель данных</param>
        public void CreateNewSupplier(SupplierModelDto supplier)
        {
            logger.Info($"Добавление поставщика {supplier.Name}");

            var finded = dbLayer.SupplierRepository.FirstOrDefault(w => w.Name == supplier.Name || w.Code == supplier.Code);
            if (finded != null)
            {
                var message = $"Найден дубликат при создании поставщика. Название: {finded.Name}, Код поставщика: {finded.Code}";
                logger.Info(message);
                throw new InvalidOperationException(message);
            }

            try
            {
                var currentSupplier = new Supplier();
                using var transaction = dbLayer.SmartTransaction.Get();
                try
                {
                    if (!string.IsNullOrEmpty(supplier.Agreement?.FileName))
                    {
                        var cloudFile = new CloudFile
                        {
                            FileName = supplier.Agreement.FileName,
                            ContentType = supplier.Agreement.ContentType,
                            Content = supplier.Agreement.Bytes,
                            Id = Guid.NewGuid()
                        };
                        dbLayer.CloudFileRepository.Insert(cloudFile);
                        dbLayer.Save();

                        supplier.AgreementId = cloudFile.Id;
                    }

                    var supplierDomainModel = ApplyChangesInModel(currentSupplier, supplier);

                    dbLayer.SupplierRepository.Insert(supplierDomainModel);
                    dbLayer.Save();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка добавления поставщика]");
                throw;
            }
        }

        /// <summary>
        /// Изменить поставщика
        /// </summary>
        /// <param name="supplier">Модель данных</param>
        public void ChangeSupplier(SupplierModelDto supplier)
        {
            var currentSupplier = dbLayer.SupplierRepository.FirstOrDefault(w => w.Id == supplier.Id);

            logger.Info($"Изменение поставщика {currentSupplier.Name}");

            var finded = dbLayer.SupplierRepository.FirstOrDefault(w => (w.Name == supplier.Name || w.Code == supplier.Code) && w.Id != supplier.Id);
            if (finded != null)
            {
                var message = $"Найден дубликат при изменении поставщика. Название: {finded.Name}, Код поставщика: {finded.Code}";
                logger.Info(message);
                throw new InvalidOperationException(message);
            }

            try
            {
                using var transaction = dbLayer.SmartTransaction.Get();
                try
                {
                    if (!string.IsNullOrEmpty(supplier.Agreement?.FileName) && supplier.Agreement?.FileName != currentSupplier.Agreement?.FileName)
                    {
                        var cloudFile = new CloudFile
                        {
                            FileName = supplier.Agreement.FileName,
                            ContentType = supplier.Agreement.ContentType,
                            Content = supplier.Agreement.Bytes,
                            Id = Guid.NewGuid()
                        };
                        dbLayer.CloudFileRepository.Insert(cloudFile);
                        dbLayer.Save();

                        supplier.AgreementId = cloudFile.Id;
                    }

                    var supplierDomainModel = ApplyChangesInModel(currentSupplier, supplier);

                    dbLayer.SupplierRepository.Update(supplierDomainModel);
                    dbLayer.Save();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка изменения поставщика]");
                throw;
            }
        }

        /// <summary>
        /// Удалить поставщика
        /// </summary>
        /// <param name="supplierId">ID поставщика</param>
        public void DeleteSupplier(Guid supplierId)
        {
            try
            {
                var currentSupplier = dbLayer.SupplierRepository.FirstOrDefault(w => w.Id == supplierId);

                logger.Info($"Удаление поставщика {currentSupplier.Name}");

                dbLayer.SupplierRepository.Delete(currentSupplier);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при удалении поставщика] '{supplierId}'");
                throw;
            }
        }

        /// <summary>
        /// Получить поставщика по ID
        /// </summary>
        /// <param name="id">ID поставщика</param>
        public SupplierModelDto GetSupplierById(Guid id)
        {
            try
            {
                logger.Info($"Получить поставщика по ID {id}");

                var finded = dbLayer.SupplierRepository.FirstOrDefault(w => w.Id == id);

                logger.Info($"Поставщик по ID {id} найден!");

                return new SupplierModelDto
                {
                    Id = finded.Id,
                    IsDefault = finded.IsDefault,
                    AgreementId = finded.AgreementId,
                    Name = finded.Name,
                    LocaleId = finded.LocaleId,
                    Locale = new LocaleDto
                    {
                        Name = finded.Locale?.Name,
                        Currency = finded.Locale?.Currency
                    },
                    Agreement = new CloudFileDto
                    {
                        FileName = finded.Agreement?.FileName,
                        ContentType = finded.Agreement?.ContentType,
                        Bytes = finded.Agreement?.Content
                    },
                    Code = finded.Code,
                    PrintedHtmlFormInvoiceId = finded.PrintedHtmlFormInvoiceId,
                    PrintedHtmlFormInvoiceReceiptId = finded.PrintedHtmlFormInvoiceReceiptId
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при получении поставщика]");
                throw;
            }
        }

        /// <summary>
        /// Применить к текущей модели изменения измененной модели
        /// </summary>
        /// <param name="currentModel">Текущая модель</param>
        /// <param name="changedModel">Измененная модель</param>
        private Supplier ApplyChangesInModel(
            Supplier currentModel, SupplierModelDto changedModel)
        {
            if (changedModel.AgreementId.IsNullOrEmpty() && changedModel.IsDefault)
                throw new ArgumentException("Договор оферты обязателен, если поставщик стандартный на локаль.");

            currentModel.Name = changedModel.Name;
            currentModel.LocaleId = changedModel.LocaleId;
            currentModel.IsDefault = changedModel.IsDefault;
            currentModel.AgreementId = changedModel.AgreementId ??
                                       GetDefaultSupplierByLocale(changedModel.LocaleId).AgreementId;
            currentModel.Code = changedModel.Code;
            currentModel.PrintedHtmlFormInvoiceId = changedModel.PrintedHtmlFormInvoiceId;
            currentModel.PrintedHtmlFormInvoiceReceiptId = changedModel.PrintedHtmlFormInvoiceReceiptId;

            return currentModel;
        }

        /// <summary>
        /// Получить стандартного поставщика по локали
        /// </summary>
        /// <param name="localeId">Id локали</param>
        /// <returns>Поставщик</returns>
        private Supplier GetDefaultSupplierByLocale(Guid localeId) =>
            dbLayer.SupplierRepository.FirstOrDefault(sp => sp.Locale.ID == localeId && sp.IsDefault) ??
            throw new NotFoundException($"Не найден стандартный поставщик на локаль {localeId}");
    }
}
