﻿using Clouds42.Domain.DataModels.Supplier;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;

namespace Clouds42.Suppliers.Providers
{
    /// <summary>
    /// Управление рефералами поставщика
    /// </summary>
    internal class SupplierReferralProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISupplierReferralProvider
    {
        /// <summary>
        /// Получить список рефералов для поставщика
        /// </summary>
        /// <param name="id">ID поставщика</param>
        public Dictionary<Guid, string> GetReferralAccounts(Guid id)
        {
            try
            {
                logger.Info("Получение реферальных аккаунтов для поставщика " + id);

                var accounts = dbLayer.SupplierReferralAccountRepository
                    .WhereLazy(w => w.SupplierId == id)
                    .Select(w => new { w.ReferralAccountId, w.ReferralAccount.AccountCaption })
                    .ToList();

                return accounts.ToDictionary(w => w.ReferralAccountId, w => w.AccountCaption.Replace("\"", "'"));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения реферальных аккаунтов для поставщика] '{id}'");
                throw;
            }
        }

        /// <summary>
        /// Удалить реферальный аккаунт
        /// </summary>
        /// <param name="supplierId">Поставщик</param>
        /// <param name="accountId">Аккаунт</param>
        public void DeleteReferralAccount(Guid supplierId, Guid accountId)
        {
            try
            {
                var supplierReferralAccount = dbLayer.SupplierReferralAccountRepository.FirstOrDefault(w => w.SupplierId == supplierId && w.ReferralAccountId == accountId);
                if (supplierReferralAccount == null)
                    throw new InvalidOperationException($"Не найден реферальный аккаунт в привязках у поставщика. Поставщик: {supplierId}, Аккаунт: {accountId}");

                logger.Info($"Удаление реферального аккаунт в привязках у поставщика. Поставщик: {supplierId}, Аккаунт: {accountId}");

                dbLayer.SupplierReferralAccountRepository.Delete(supplierReferralAccount);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления реферального аккаунта] '{accountId}' для поставщика '{supplierId}'");
                throw;
            }
        }

        /// <summary>
        /// Добавить реферальный аккаунт
        /// </summary>
        /// <param name="supplierId">Поставщик</param>
        /// <param name="accountId">Аккаунт</param>
        public IAccount AddReferralAccount(Guid supplierId, Guid accountId)
        {
            try
            {
                var supplierReferralAccount = dbLayer.SupplierReferralAccountRepository.FirstOrDefault(w => w.SupplierId == supplierId && w.ReferralAccountId == accountId);
                if (supplierReferralAccount != null)
                    throw new InvalidOperationException("Данный аккаунт уже привязан к поставщику");

                logger.Info($"Добавление реферального аккаунта к поставщику. Поставщик: {supplierId}, Аккаунт: {accountId}");

                dbLayer.SupplierReferralAccountRepository.Insert(new SupplierReferralAccount { SupplierId = supplierId, ReferralAccountId = accountId });
                dbLayer.Save();

                var account = dbLayer.AccountsRepository.FirstOrDefault(w => w.Id == accountId);
                return account;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при добавлении реферального аккаунта к поставщику]. Поставщик: {supplierId}, Аккаунт: {accountId}");
                throw;
            }
        }

        /// <summary>
        /// Найти аккаунт по искомой строке
        /// </summary>
        /// <param name="searchValue">Искомая строка</param>
        public Dictionary<string, string> SearchAccountByText(string searchValue)
        {
            try
            {
                if (string.IsNullOrEmpty(searchValue))
                    return new Dictionary<string, string>();

                logger.Info("Поиск аккаунта по искомой строке: " + searchValue);

                var accounts = dbLayer.AccountsRepository
                    .WhereLazy(w => w.AccountCaption.ToLower().Contains(searchValue.ToLower()) || searchValue.ToLower().Contains(w.AccountCaption.ToLower()))
                    .Select(w => new { w.Id, w.AccountCaption })
                    .ToList().ToDictionary(w => w.Id.ToString(), w => w.AccountCaption);

                return accounts;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при поиске аккаунта по искомому слову] " + searchValue);
                throw;
            }
        }
    }
}
