﻿using Clouds42.Suppliers.Contracts.Interfaces;
using Clouds42.Suppliers.Managers;
using Clouds42.Suppliers.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Suppliers
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddSupplierReference(this IServiceCollection services)
        {
            services.AddTransient<ICreateSupplierReferenceProvider, CreateSupplierReferenceProvider>();
            services.AddTransient<IEditSupplierReferenceProvider, EditSupplierReferenceProvider>();
            services.AddTransient<IDeleteSupplierReferenceProvider, DeleteSupplierReferenceProvider>();
            services.AddTransient<ISupplierReferenceDataProvider, SupplierReferenceDataProvider>();
            services.AddTransient<ICreateSupplierReferralAccountProvider, CreateSupplierReferralAccountProvider>();
            services.AddTransient<IDeleteSupplierReferralAccountProvider, DeleteSupplierReferralAccountProvider>();
            services.AddTransient<ISupplierReferralAccountDataProvider, SupplierReferralAccountDataProvider>();
            services.AddTransient<ISupplierReferenceProvider, SupplierReferenceProvider>();
            services.AddTransient<ISupplierReferralProvider, SupplierReferralProvider>();
            services.AddTransient<SupplierReferenceManager>();
            services.AddTransient<SupplierReferralAccountManager>();

            return services;
        }
    }
}
