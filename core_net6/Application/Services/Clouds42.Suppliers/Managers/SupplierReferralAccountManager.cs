﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.SupplierReference;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;

namespace Clouds42.Suppliers.Managers
{
    /// <summary>
    /// Менеджер для реферальных аккаунтов поставщика
    /// </summary>
    public class SupplierReferralAccountManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ICreateSupplierReferralAccountProvider createSupplierReferralAccountProvider,
        IDeleteSupplierReferralAccountProvider deleteSupplierReferralAccountProvider)
        : BaseManager(accessProvider,
            dbLayer, handlerException)
    {

        /// <summary>
        /// Удалить реферальный аккаунт у поставщика
        /// </summary>
        /// <param name="supplierId">ID Поставщика</param>
        /// <param name="accountId">ID Аккаунта</param>
        public ManagerResult DeleteSupplierReferralAccount(Guid supplierId, Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_Delete, () => AccessProvider.ContextAccountId);
                deleteSupplierReferralAccountProvider.DeleteSupplierReferralAccount(supplierId, accountId);
                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при удалении реферального аккаунта поставщика.]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Добавить реферальный аккаунт для поставщика
        /// </summary>
        /// <param name="supplierId">ID Поставщика</param>
        /// <param name="accountId">ID Аккаунта</param>
        public ManagerResult CreateSupplierReferralAccount(Guid supplierId, Guid accountId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_Create, () => AccessProvider.ContextAccountId);
                createSupplierReferralAccountProvider.CreateSupplierReferralAccount(supplierId, accountId);
                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при добавлении реферального аккаунта к поставщику.]");
                return PreconditionFailed(ex.Message);
            }
        }

        public ManagerResult CreateSupplierReferralAccount(SupplierReferenceAddRefferalAccountDto dto) =>
            CreateSupplierReferralAccount(dto.SupplierId, dto.AccountId);

        public ManagerResult EditSupplierForAccount(SupplierReferenceAddRefferalAccountDto dto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_Create, () => AccessProvider.ContextAccountId);
                createSupplierReferralAccountProvider.EditSupplierForAccount(dto.SupplierId, dto.AccountId);

                LogEventHelper.LogEvent(DbLayer, dto.AccountId, AccessProvider, LogActions.EditSupplairForAccount,
                   $"Изменен поставщик для аккаунта");
                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при смене поставщика для аккаунта.]");
                return PreconditionFailed(ex.Message);
            }
        }

    }
}
