﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Suppliers.Contracts.Interfaces;

namespace Clouds42.Suppliers.Managers
{
    /// <summary>
    /// Менеджер для справочника поставщиков
    /// </summary>
    public class SupplierReferenceManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ISupplierReferenceDataProvider supplierReferenceDataProvider,
        ICreateSupplierReferenceProvider createSupplierReferenceProvider,
        IEditSupplierReferenceProvider editSupplierReferenceProvider,
        IDeleteSupplierReferenceProvider deleteSupplierReferenceProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить список поставщиков по фильтру
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <returns>Список поставщиков с пагинацией</returns>
        public ManagerResult<SuppliersPaginationDto> GetSuppliersPagedList(int page)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_View, () => AccessProvider.ContextAccountId);
                var suppliersPagedList = supplierReferenceDataProvider.GetSuppliersPagedList(page);
                return Ok(suppliersPagedList);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при получении списка поставщиков по фильтру.]");
                return PreconditionFailed<SuppliersPaginationDto>(ex.Message);
            }
        }

        /// <summary>
        /// Создать нового поставщика
        /// </summary>
        /// <param name="supplier">Модель данных</param>
        public ManagerResult CreateSupplier(SupplierDto supplier)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_Create, () => AccessProvider.ContextAccountId);
                createSupplierReferenceProvider.CreateSupplier(supplier);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при создании поставщика.]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать поставщика
        /// </summary>
        /// <param name="supplier">Модель данных</param>
        public ManagerResult EditSupplier(SupplierDto supplier)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_Edit, () => AccessProvider.ContextAccountId);
                editSupplierReferenceProvider.EditSupplier(supplier);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при редактировании поставщика.]");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить элементы комбобокса
        /// при инциализации страниц создания/редактирования поставщика
        /// </summary>
        /// <returns>Все локали, все печатные формы</returns>
        public ManagerResult<InitSelectListItemsDto> GetInitCreateEditPageItems()
        {
            try
            {
                var result = supplierReferenceDataProvider.GetInitCreateEditPageItems();
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    "[Ошибка при получении элементов для инициализации страницы создания/редактирования поставщика.]");
                return PreconditionFailed<InitSelectListItemsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить поставщика по ID
        /// </summary>
        /// <param name="supplierId">ID поставщика</param>
        /// <returns>Модель поставщика</returns>
        public ManagerResult<SupplierDto> GetSupplierById(Guid supplierId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_View, () => AccessProvider.ContextAccountId);
                var result = supplierReferenceDataProvider.GetSupplierById(supplierId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при получении поставщика.]");
                return PreconditionFailed<SupplierDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить файл оферты поставщика
        /// </summary>
        /// <param name="supplierId">ID поставщика</param>
        /// <returns>Модель файла оферты поставщика</returns>
        public ManagerResult<CloudFileDto> GetAgreementCloudFileBySupplierId(Guid supplierId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_View, () => AccessProvider.ContextAccountId);
                var result = supplierReferenceDataProvider.GetAgreementCloudFileBySupplierId(supplierId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при получении файла оферты поставщика.]");
                return PreconditionFailed<CloudFileDto>(ex.Message);
            }
        }

        /// <summary>
        /// Редактировать поставщика
        /// </summary>
        /// <param name="supplierId">ID поставщика</param>
        public ManagerResult DeleteSupplier(Guid supplierId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.SupplierReference_Delete, () => AccessProvider.ContextAccountId);
                deleteSupplierReferenceProvider.DeleteSupplier(supplierId);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка при удалении поставщика.]");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
