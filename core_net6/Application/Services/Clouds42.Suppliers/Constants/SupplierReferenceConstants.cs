﻿namespace Clouds42.Suppliers.Constants
{
    /// <summary>
    /// Константы поставщика
    /// </summary>
    public static class SupplierReferenceConstants
    {
        /// <summary>
        /// Число элементов на одну страницу
        /// </summary>
        public const int ItemsPerPage = 10;
    }
}