﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Domain.DataModels.Supplier;

namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер получения поставщиков
    /// </summary>
    public interface ISupplierReferenceDataProvider
    {
        /// <summary>
        /// Получить всех поставщиков с пагинацией
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <returns>Всех поставщиков с пагинацией</returns>
        SuppliersPaginationDto GetSuppliersPagedList(int page);

        /// <summary>
        /// Получить поставшика по ID
        /// </summary>
        /// <param name="id">ID поставщика</param>
        /// <returns>Модель поставщика</returns>
        SupplierDto GetSupplierById(Guid id);

        /// <summary>
        /// Получить доменный модель поставщика по ID
        /// </summary>
        /// <param name="id">ID поставщика</param>
        /// <returns>Доменный модель поставщика</returns>
        Supplier GetDomainSupplierById(Guid id);

        /// <summary>
        /// Получить файл оферты поставщика
        /// </summary>
        /// <param name="id">ID поставщика</param>
        /// <returns>Модель файла оферты поставщика</returns>
        CloudFileDto GetAgreementCloudFileBySupplierId(Guid id);

        /// <summary>
        /// Получить элементы комбобокса
        /// при инциализации страниц создания/редактирования поставщика
        /// </summary>
        /// <returns>Все локали, все печатные формы</returns>
        InitSelectListItemsDto GetInitCreateEditPageItems();
    }
}