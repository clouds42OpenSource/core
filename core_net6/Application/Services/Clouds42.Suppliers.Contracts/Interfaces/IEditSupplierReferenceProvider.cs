﻿using Clouds42.DataContracts.Billing.Supplier;

namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер редактирования поставщика
    /// </summary>
    public interface IEditSupplierReferenceProvider
    {
        /// <summary>
        /// Редактировать поставщика
        /// </summary>
        /// <param name="supplier">Данные поставщика на редактирование</param>
        void EditSupplier(SupplierDto supplier);
    }
}