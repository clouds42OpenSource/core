﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Supplier;
using PagedList.Core;

namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Управление справочником поставщиков
    /// </summary>
    public interface ISupplierReferenceProvider
    {
        /// <summary>
        /// Получить список поставщиков по фильтру
        /// </summary>
        /// <param name="filter">Фильтр</param>
        IPagedList<SupplierModelDto> GetSuppliersPagedList(PagedListFilter? filter = null);

        /// <summary>
        /// Создать нового поставщика
        /// </summary>
        /// <param name="supplier">Модель данных</param>
        void CreateNewSupplier(SupplierModelDto supplier);

        /// <summary>
        /// Изменить поставщика
        /// </summary>
        /// <param name="supplier">Модель данных</param>
        void ChangeSupplier(SupplierModelDto supplier);

        /// <summary>
        /// Удалить поставщика
        /// </summary>
        /// <param name="supplierId">ID поставщика</param>
        void DeleteSupplier(Guid supplierId);

        /// <summary>
        /// Получить поставщика по ID
        /// </summary>
        /// <param name="id">ID поставщика</param>
        SupplierModelDto GetSupplierById(Guid id);
    }
}