﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Управление рефералами поставщика
    /// </summary>
    public interface ISupplierReferralProvider
    {
        /// <summary>
        /// Получить список рефералов для поставщика
        /// </summary>
        /// <param name="id">ID поставщика</param>
        Dictionary<Guid, string> GetReferralAccounts(Guid id);

        /// <summary>
        /// Удалить реферальный аккаунт
        /// </summary>
        /// <param name="supplierId">Поставщик</param>
        /// <param name="accountId">Аккаунт</param>
        void DeleteReferralAccount(Guid supplierId, Guid accountId);

        /// <summary>
        /// Добавить реферальный аккаунт
        /// </summary>
        /// <param name="supplierId">Поставщик</param>
        /// <param name="accountId">Аккаунт</param>
        IAccount AddReferralAccount(Guid supplierId, Guid accountId);

        /// <summary>
        /// Найти аккаунт по искомой строке
        /// </summary>
        /// <param name="searchValue">Искомая строка</param>
        Dictionary<string, string> SearchAccountByText(string searchValue);
    }
}