﻿namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для создания реферальных аккаунтов поставщика
    /// </summary>
    public interface ICreateSupplierReferralAccountProvider
    {
        /// <summary>
        /// Добавить реферальный аккаунт для поставщика
        /// </summary>
        /// <param name="supplierId">ID Поставщика</param>
        /// <param name="accountId">ID Аккаунта</param>
        void CreateSupplierReferralAccount(Guid supplierId, Guid accountId);

        /// <summary>
        /// Сменить поставщика для аккаунта
        /// </summary>
        /// <param name="supplierId">ID Поставщика</param>
        /// <param name="accountId">ID Аккаунта</param>
        void EditSupplierForAccount(Guid supplierId, Guid accountId);
    }
}
