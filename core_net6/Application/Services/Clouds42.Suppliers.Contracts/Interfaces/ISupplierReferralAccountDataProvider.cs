﻿namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для получения реферальных аккаунтов поставщика
    /// </summary>
    public interface ISupplierReferralAccountDataProvider
    {
        /// <summary>
        /// Получить список рефералов для поставщика
        /// Key - ID реферального аккаунта
        /// Value - Название реферального аккаунта
        /// </summary>
        /// <param name="id">ID поставщика</param>
        /// <returns>Cписок словарей рефералов для поставщика</returns>
        KeyValuePair<Guid, string>[] GetReferralAccountsBySupplierId(Guid id);

        /// <summary>
        /// Найти аккаунт по значению поисковика
        /// </summary>
        /// <param name="searchValue">Значения поисковика</param>
        /// <returns>Cписок словарей рефералов для поставщика</returns>
        KeyValuePair<Guid, string>[] GetReferralAccountsBySearchValue(string searchValue);
    }
}