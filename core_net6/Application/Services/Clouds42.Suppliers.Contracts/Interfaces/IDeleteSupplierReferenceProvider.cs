﻿namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер на удаление поставщика
    /// </summary>
    public interface IDeleteSupplierReferenceProvider
    {
        /// <summary>
        /// Удалить поставщика
        /// </summary>
        /// <param name="id">ID поставщика</param>
        void DeleteSupplier(Guid id);
    }
}