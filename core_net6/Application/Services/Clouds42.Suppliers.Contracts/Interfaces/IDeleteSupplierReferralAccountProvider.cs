﻿namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер на удаление реферальных аккаунтов поставщика
    /// </summary>
    public interface IDeleteSupplierReferralAccountProvider
    {
        /// <summary>
        /// Удалить реферальный аккаунт у поставщика
        /// </summary>
        /// <param name="supplierId">ID Поставщика</param>
        /// <param name="accountId">ID Аккаунта</param>
        void DeleteSupplierReferralAccount(Guid supplierId, Guid accountId);
    }
}