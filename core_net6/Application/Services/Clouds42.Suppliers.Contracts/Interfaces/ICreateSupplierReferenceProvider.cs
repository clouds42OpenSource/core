﻿using Clouds42.DataContracts.Billing.Supplier;

namespace Clouds42.Suppliers.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер создания поставщика
    /// </summary>
    public interface ICreateSupplierReferenceProvider
    {
        /// <summary>
        /// Создавть нового поставщика
        /// </summary>
        /// <param name="newSupplier">Новый поставщик</param>
        void CreateSupplier(SupplierDto newSupplier);
    }
}