﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using RestSharp;

namespace Clouds42.RestClient
{
    /// <summary>
    /// Обертка Rest клиента.
    /// </summary>
    public class RestClientWrapper : RestSharp.RestClient, IRestClientWrapper
    {
        private string _baseUrl;
        public RestClientWrapper()
        {}

        public RestClientWrapper(string baseUrl) : base(GetHttpClientWithBaseAddress(baseUrl))
        {
        }

        private static HttpClient GetHttpClientWithBaseAddress(string baseUrl)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(baseUrl)
            };

            return client;
        }

        /// <summary>
        /// Выполнить Rest запрос
        /// </summary>
        public new RestResponse Execute(RestRequest request)
        {
            if (string.IsNullOrEmpty(_baseUrl))
                throw new InvalidOperationException("Не указан параметр BaseUrl, требуется перед вызовом использовать метод SetBaseUrl");

            var client = new RestSharp.RestClient(GetHttpClientWithBaseAddress(_baseUrl))
            {
                Authenticator = base.Authenticator
            };

            return client.ExecuteAsync(request).Result;
        }

        /// <summary>
        /// Выполнить Rest запрос
        /// </summary>
        public async Task<RestResponse> ExecuteAsync(RestRequest request)
        {
            if (string.IsNullOrEmpty(_baseUrl))
                throw new InvalidOperationException("Не указан параметр BaseUrl, требуется перед вызовом использовать метод SetBaseUrl");

            var client = new RestSharp.RestClient(GetHttpClientWithBaseAddress(_baseUrl))
            {
                Authenticator = Authenticator
            };

            return await client.ExecuteAsync(request);
        }

        /// <summary>
        /// Задать адрес сервера подключения.
        /// </summary>
        /// <param name="baseUrl">полный путь до подключаемого сервера Rest</param>
        public void SetBaseUrl(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        /// <summary>
        /// Построить URL
        /// </summary>
        /// <param name="request">REST запрос</param>
        public void BuildUrl(RestRequest request)
        {
            BuildUri(request);
        }

        public string GetBaseUri()
        {
            return _baseUrl;
        }

    }
}
