﻿using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;

namespace Clouds42.RestClient
{

    /// <summary>
    /// Обертка Rest клиента.
    /// </summary>
    public interface IRestClientWrapper
    {

        /// <summary>
        /// Выполнить Rest запрос
        /// </summary>
        RestResponse  Execute(RestRequest request);

        Task<RestResponse> ExecuteAsync(RestRequest request);

        /// <summary>
        /// Авторизационные данные.
        /// </summary>
        IAuthenticator Authenticator { get; set; }

        /// <summary>
        /// Задать адрес сервера подключения.
        /// </summary>
        /// <param name="baseUrl">полный путь до подключаемого сервера Rest</param>
        void SetBaseUrl(string baseUrl);

        /// <summary>
        /// Построить URL
        /// </summary>
        /// <param name="request">REST запрос</param>
        void BuildUrl(RestRequest request);

        /// <summary>
        /// Вернуть базовый уровень
        /// </summary>
        /// <returns></returns>
        string GetBaseUri();

    }
}
