﻿using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.RestClient
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddRestClient(this IServiceCollection services)
        {
            services.AddTransient<IRestClientWrapper, RestClientWrapper>();
            return services;
        }
    }
}