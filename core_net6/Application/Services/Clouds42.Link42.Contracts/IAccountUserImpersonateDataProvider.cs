﻿using Clouds42.DataContracts.Service.Link;

namespace Clouds42.Link42.Contracts
{
    /// <summary>
    /// Провайдер данных имперсонации пользователя облака.
    /// </summary>
    public interface IAccountUserImpersonateDataProvider
    {

        /// <summary>
        /// Получить данные имперсонации пользователя по ИД.
        /// </summary>
        /// <param name="accountUserId">ИД пользователя облака. AccountUser.Login</param>
        Task<AccountUserImpersonateDataDto> GetByAccountUserIdAsync(Guid accountUserId);
    }
}