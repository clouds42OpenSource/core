﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.CloudServices.Contracts
{
    public interface IRecalculateMyDiskUsedSizeManager
    {
        ManagerResult RecalculateMyDiskUsedSize(Guid accountId);
    }
}