﻿using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Contracts
{
    public interface IMyDatabases42CloudService : ICloud42ServiceBase
    {
        Guid AccountId { get; }
        ResourcesConfiguration ResourceConfiguration { get; }
        void BeforeLock(BeforeLockInServicefoModelDto beforeLockInServicefoModel);
        bool CanProlong(out string? reason);
        void LogEvent(Func<Guid?> getAccountId, LogActions log, string description);

        IMyDatabases42CloudService SetAccountId(Guid accountId);
    }
}

