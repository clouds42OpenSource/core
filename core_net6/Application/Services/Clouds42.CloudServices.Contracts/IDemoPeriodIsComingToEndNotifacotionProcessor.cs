﻿using System.Text;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    public interface IDemoPeriodIsComingToEndNotifacotionProcessor
    {
        /// <summary>
        /// Оставшееся количество дней до зовершения демо
        /// </summary>
        const byte RemainingNumberOfDays = 2;
        void Process(ResourcesConfiguration resourcesConfiguration, StringBuilder stringBuilder);
    }
}