﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Contracts.Triggers
{
    /// <summary>
    /// Триггер синхронизации с Тардис при добавлении базы на разделителях.
    /// </summary>
    public class CreateDatabaseOnDelimitersTrigger(IUnitOfWork dbLayer)
        : SynchTriggerBase<CreateDatabaseOnDelimitersDto>(dbLayer)
    {
        private readonly Lazy<string> _routeValueForAddAccountDatabase = new(CloudConfigurationProvider.Tardis.GetRouteValueForAddAccountDatabase);

        /// <summary>
        /// Web путь к методу синхронизации.
        /// </summary>
        protected override string MethodPath => _routeValueForAddAccountDatabase.Value;
    }
}
