﻿using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Tardis.Response;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Contracts.Triggers
{

    /// <summary>
    /// Базовый триггер синхронизации с Тардис.
    /// </summary>
    /// <typeparam name="TModel">Тип модели триггера.</typeparam>
    public abstract class SynchTriggerBase<TModel>(IUnitOfWork dbLayer)
        where TModel : class
    {
        /// <summary>
        /// Адрес АПИ Тардис.
        /// </summary>
        private readonly Lazy<Uri> _tardisApiUri = new(CloudConfigurationProvider.Tardis.GetTardisApiUrl);

        /// <summary>
        /// HTTP клиент
        /// </summary>
        protected HttpClient HttpClient => new()
        {
            BaseAddress = _tardisApiUri.Value
        };

        /// <summary>
        /// Вызвать триггер.
        /// </summary>
        /// <param name="model">Входящая модель триггера.</param>
        public void RiseEvent(TModel model)
        {
            var bodyXml = model.SerializeToCleanXml();
            RiseEventAsync(bodyXml, GetCore42CloudService()).Wait();
        }

        /// <summary>
        /// Вызвать триггер асинхронно
        /// </summary>
        /// <param name="bodyXml">Тело запроса</param>
        /// <param name="core42CloudService">Модель системного сервиса CORE42</param>
        /// <returns>Результат вызова</returns>
        private async Task RiseEventAsync(string bodyXml, CloudService core42CloudService)
        {
            TardisResonseDto response;
            try
            {
                if (core42CloudService?.ServiceToken == null)
                    throw new InvalidOperationException("Токен службы core42 не найден");

                var headersList = new List<KeyValuePair<string, string>>
                {
                    new("Token", core42CloudService.ServiceToken.ToString()),
                    new("UserAgent", core42CloudService.CloudServiceId)
                };

                response = HttpClient.PostAsStringAsync(MethodPath, bodyXml, headersList).Result
                    .Deserialize<TardisResonseDto>();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"Ошибка синхронизации :: '{ex.GetFullInfo(needStackTrace: false)}'. Метод '{MethodPath}', Тело запроса '{bodyXml}'");
            }

            if (!response.IsOk)
                throw new InvalidOperationException($"Ошибка синхронизации :: '{response.Status} {response.Description}'. Метод '{MethodPath}', Тело запроса '{bodyXml}'");

            await Task.FromResult(0);
        }

        /// <summary>
        /// Web путь к методу синхронизации.
        /// </summary>
        protected abstract string MethodPath { get; }

        /// <summary>
        /// Получить модель системного сервиса CORE42
        /// </summary>
        /// <returns>Модель системного сервиса CORE42</returns>
        private CloudService GetCore42CloudService()
            => dbLayer.CloudServiceRepository.GetCloudService("CORE42");
    }
}