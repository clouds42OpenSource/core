﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Contracts.Triggers
{
    /// <summary>
    /// Триггер синхронизации с Тардис при редактирование пользователя облака.
    /// </summary>
    public class UpdateAccountUserTrigger(IUnitOfWork dbLayer) : SynchTriggerBase<AccountUserIdDto>(dbLayer)
    {
        private readonly Lazy<string> _routeValueForSetUserProperties = new(CloudConfigurationProvider.Tardis.GetRouteValueForSetUserProperties);

        /// <summary>
        /// Web путь к методу синхронизации.
        /// </summary>
        protected override string MethodPath => _routeValueForSetUserProperties.Value;
    }
}