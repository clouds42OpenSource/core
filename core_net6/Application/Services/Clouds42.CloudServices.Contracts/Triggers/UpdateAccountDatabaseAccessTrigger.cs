﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Contracts.Triggers
{
    /// <summary>
    /// Триггер синхронизации с Тардис при измненени доступа пользоватя в информационной базы.
    /// </summary>
    public class UpdateAccountDatabaseAccessTrigger(IUnitOfWork dbLayer)
        : SynchTriggerBase<AccountDatabaseAccessDto>(dbLayer)
    {
        private readonly Lazy<string> _routeValueForSetAccess = new(CloudConfigurationProvider.Tardis.GetRouteValueForSetAccess);

        /// <summary>
        /// Web путь к методу синхронизации.
        /// </summary>
        protected override string MethodPath => _routeValueForSetAccess.Value;
    }
}