﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Contracts.Triggers
{
    /// <summary>
    /// Триггер синхронизации с Тардис при удаление пользователя облака.
    /// </summary>
    public class DeleteAccountUserTrigger(IUnitOfWork dbLayer) : SynchTriggerBase<AccountUserIdDto>(dbLayer)
    {
        private readonly Lazy<string> _routeValueForDeleteUser = new(CloudConfigurationProvider.Tardis.GetRouteValueForDeleteUser);

        /// <summary>
        /// Web путь к методу синхронизации.
        /// </summary>
        protected override string MethodPath => _routeValueForDeleteUser.Value;
    }
}