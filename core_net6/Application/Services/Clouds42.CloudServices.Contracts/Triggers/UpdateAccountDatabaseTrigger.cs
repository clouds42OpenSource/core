﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Contracts.Triggers
{
    /// <summary>
    /// Триггер синхронизации с Тардис при редактирование информационной базы.
    /// </summary>
    public class UpdateAccountDatabaseTrigger(IUnitOfWork dbLayer) : SynchTriggerBase<AccountDatabaseIdDto>(dbLayer)
    {
        private readonly Lazy<string> _routeValueForSetBaseProperties = new(CloudConfigurationProvider.Tardis.GetRouteValueForSetBaseProperties);

        /// <summary>
        /// Web путь к методу синхронизации.
        /// </summary>
        protected override string MethodPath => _routeValueForSetBaseProperties.Value;
    }
}