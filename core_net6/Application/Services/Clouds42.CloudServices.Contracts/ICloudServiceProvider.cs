﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с CloudService
    /// </summary>
    public interface ICloudServiceProvider
    {
        /// <summary>
        /// Обновить данные в таблице CloudService
        /// </summary>
        /// <param name="model">Модель для обновления</param>
        void UpdateCloudService(CloudServiceDto model);
        
        /// <summary>
        /// Добавить новую запись в таблице CloudService
        /// </summary>
        /// <param name="model">Модель для добавления</param>
        /// <returns>Модель ответа добавленного CloudService</returns>
        AddCloudServiceResponseDto AddCloudService(CloudServiceDto model);

        /// <summary>
        /// Подтверждение подписки для интеграции с Cloud
        /// </summary>
        /// <param name="model">Модель для добавления</param>
        /// <returns></returns>
        void SubscriptionConfirmationForCloud(CloudServiceIntegrationDto model, string login);

        /// <summary>
        /// Получить информацио о сервисе для Cloud
        /// </summary>
        /// <param name="model">Модель для кого запрашивается информация</param>
        /// <returns></returns>
        CloudServiceInfoDto GetServiceInfoForCloud(string email, string login);
    }
}
