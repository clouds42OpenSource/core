﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.CloudServices.Contracts
{
    public interface IEsdlCloud42Service : ICloud42ServiceBase
    {
        BuyingEsdlResultModelDto BuyEsdl(int years);
        bool CanProlong(out string reason);

        decimal GetCost(int pagesAmount);

        IEsdlCloud42Service SetAccountId(Guid accountId);
    }
}