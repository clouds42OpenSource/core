﻿using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для редактирования терминального сервера
    /// </summary>
    public interface IEditCloudTerminalServerProvider
    {
        /// <summary>
        /// Редактировать терминальный сервер
        /// </summary>
        /// <param name="cloudTerminalServerDto">Модель терминального сервера</param>
        void Edit(CloudTerminalServerDto cloudTerminalServerDto);
    }
}
