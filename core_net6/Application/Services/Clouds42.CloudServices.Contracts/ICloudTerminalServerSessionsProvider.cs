﻿using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для облачных терминальных серверов
    /// </summary>
    public interface ICloudTerminalServerSessionsProvider
    {
        /// <summary>
        /// Поиск сессий пользователей для текущего аккаунта
        /// </summary>
        /// <param name="accountId">Id акканта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Сессии пользователей</returns>
        Task<List<UserRdpSessionsDto>> FindUserRdpSessionsByAccountIdAsync(Guid accountId, Guid? accountUserId = null);

        /// <summary>
        /// Получить список пользователей с активными сессиями
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>Список пользователей с активными сессиями</returns>
        Task<List<AccountUserDto>> GetAccountUsersConnectionsInfoList(Guid accountId);

        /// <summary>
        /// Завершить сессию пользователя
        /// </summary>
        /// <param name="model">Данные сессии</param>>
        void DropSession(CloudTerminalServerPostRequestCloseDto model);

        /// <summary>
        /// Получить список сессий аккаунта для терминального сервера
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список сессий аккаунта</returns>
        List<UserRdpSessionsDto> GetSessions(Guid terminalServerId, Guid accountId);

        Task<List<UserRdpSessionsDto>> GetSessionsAsync(Dictionary<string, Guid> terminalServersDict, Guid accountId);

        /// <summary>
        /// Дропнуть сессии пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        Task DropUserRdpSessionsAsync(Guid userId);

        /// <summary>
        /// Найти и закрыть все RDP сессии пользователей
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="userIds">Id's пользователей</param>
        /// <returns>Результат заверешения сессий</returns>
        Task<CloseRdpSessionsDto> FindAndCloseUsersRdpSessionsAsync(Guid accountId, Guid[] userIds);
    }
}
