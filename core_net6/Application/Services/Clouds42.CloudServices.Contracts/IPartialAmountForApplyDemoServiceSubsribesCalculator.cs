﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    public interface IPartialAmountForApplyDemoServiceSubsribesCalculator
    {
        decimal? Calculate(ResourcesConfiguration resourceConfiguration, ResourcesConfiguration mainResourceConfiguration);
        decimal? CalculateForDays(ResourcesConfiguration resourceConfiguration, ResourcesConfiguration mainResourceConfiguration, int countDays);
    }
}