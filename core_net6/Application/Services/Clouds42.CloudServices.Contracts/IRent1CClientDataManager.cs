﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Contracts
{
    public interface IRent1CClientDataManager
    {
        /// <summary>
        /// Получить виды разрешений на аренду 1С
        /// для клиента 
        /// </summary> 
        /// <param name="accountId">Id аккаунта пользователя</param>
        /// <param name="requestedUserId">Id пользователя который запрашивает информацию</param>
        /// <returns>Разрешения на аренду 1С для клиента</returns>
        ManagerResult<Rent1CClientPermissionsDto> GetRent1CClientPermissions(Guid accountId, Guid requestedUserId);

        /// <summary>
        ///     Получить информационную базу
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <returns></returns>
        AccountDatabase GetAccountDatabase(Guid accountDatabaseId);

        /// <summary>
        /// Версия метода для логирования роботы корворкера когда в инициаторах нужно записывать
        /// пользователя, поставившего задачу на выполнение
        /// </summary>
        /// <param name="getAccountId">Функтор получения идентификатора аккаунта</param>
        /// <param name="log">Действие для записи в лог</param>
        /// <param name="description">Описание события</param>
        /// <param name="accountUserInitiatorId">Идентификатор пользователя, инициатора записи в лог</param>
        void LogEvent(Func<Guid?> getAccountId, LogActions log, string description, Guid accountUserInitiatorId);

        /// <summary>
        /// Метод для логирования роботы корворкера
        /// </summary>
        /// <param name="getAccountId">Функтор получения идентификатора аккаунта</param>
        /// <param name="log">Действие для записи в лог</param>
        /// <param name="description">Описание события</param>
        void LogEvent(Func<Guid?> getAccountId, LogActions log, string description);
    }
}
