﻿using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels;

namespace Clouds42.CloudServices.Contracts
{
    public interface IRent1CServiceInfoDmProcessor
    {
        void CalculatePartialCostForSponsorAccount(AccountUser accountUser, out decimal partialRateWebCost, out decimal partialRateRdpCost, out decimal partialUserCostStandart);
        Rent1CServiceInfoDto GetModel(Guid accountId);
    }
}