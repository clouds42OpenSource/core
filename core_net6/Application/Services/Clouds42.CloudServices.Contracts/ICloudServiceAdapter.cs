﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Contracts
{
    public interface ICloudServiceAdapter
    {

        /// <summary>
        /// Получить запись конфигурации основного сервиса
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="cloudService">Тип системного сервиса.</param>
        /// <returns>Ресурс конфигурация основного сервиса.</returns>
        ResourcesConfiguration GetMainConfigurationOrThrowException(Guid accountId, Clouds42Service cloudService);

        /// <summary>
        /// Получить запись конфигурации основного сервиса.
        /// </summary>
        /// <param name="resourceConfiguration">Исходная ресурс конфгурация.</param>        
        /// <returns>Ресурс конфигурация основного сервиса.</returns>
        ResourcesConfiguration GetMainConfigurationOrThrowException(ResourcesConfiguration resourceConfiguration);

        /// <summary>
        /// Получить запись конфигурации основного сервиса.
        /// </summary>
        /// <param name="resourceConfiguration">Исходная ресурс конфгурация.</param>        
        /// <returns>Ресурс конфигурация основного сервиса.</returns>
        ResourcesConfiguration GetMainConfiguration(ResourcesConfiguration resourceConfiguration);
    }
}