﻿using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Contracts
{
    public interface ICloud42ServiceBase
    {
        Clouds42Service CloudServiceType { get; }
        void AcceptVisitor(ISystemCloudServiceProcessorVisitor visitor);
        void ActivateService();
        void Lock();
        void UnLock();
    }
}
