﻿using Clouds42.Domain.DataModels.History;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с Историей изменений ресурсов главного сервиса(Аренда 1С)
    /// </summary>
    public interface IMainServiceResourcesChangesHistoryProvider
    {
        /// <summary>
        /// Получить историю изменений ресурсов главного сервиса
        /// для Аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changesDate">Дата изменений</param>
        /// <returns>История изменений ресурсов главного сервиса(Аренда 1С) для аккаунта</returns>
        MainServiceResourcesChangesHistory GetMainServiceResourcesChangesHistory(Guid accountId,
            DateTime changesDate);
    }
}
