﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для уведомления об об изменении ресурсов главного сервиса (Аредна 1С)
    /// </summary>
    public interface INotifyAboutMainServiceResourcesChangesProvider
    {
        /// <summary>
        /// Уведомить об об изменении ресурсов главного сервиса (Аредна 1С)
        /// </summary>
        /// <param name="changesDate">Дата изменений</param>
        void Notify(DateTime changesDate);
    }
}
