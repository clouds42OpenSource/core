﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер пересчета занимаемого места на диске
    /// </summary>
    public interface IRecalculateMyDiskUsedSizeProvider
    {
        /// <summary>
        /// Запустить задачу пересчета занимаемого места на диске
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void RunTaskToRecalculateAccountSizeAndTryProlong(Guid accountId);
    }
}
