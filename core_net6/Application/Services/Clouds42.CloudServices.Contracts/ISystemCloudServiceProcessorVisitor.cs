﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Посетитель процессора системных облачных сервисов
    /// </summary>
    public interface ISystemCloudServiceProcessorVisitor
    {
        /// <summary>
        /// Поситить сервис МойДиск
        /// </summary>
        /// <param name="service">Класс для управления сервисом "Мой диск"</param>
        void Visit(IMyDisk42CloudService service);

        /// <summary>
        /// Поситить сервис Аренда 1С
        /// </summary>
        /// <param name="service">Класс для управления сервисом "Аренда 1С"</param>
        void Visit(IMyEnterprise42CloudService service);

        /// <summary>
        /// Поситить сервис Fasta
        /// </summary>
        /// <param name="service">Класс для управления сервисом "Fasta"</param>
        void Visit(IEsdlCloud42Service service);

        /// <summary>
        /// Поситить сервис Распознование документов
        /// </summary>
        /// <param name="service">Класс для управления сервисом "Распознование документов"</param>
        void Visit(IRecognition42CloudService service);

        /// <summary>
        /// Поситить сервис Распознование документов
        /// </summary>
        /// <param name="service">Класс для управления сервисом "Мои информационные базы"</param>
        void Visit(IMyDatabases42CloudService service);
    }
}
