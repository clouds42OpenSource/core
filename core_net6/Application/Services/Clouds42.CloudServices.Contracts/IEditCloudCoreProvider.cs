﻿using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для редактирования настройки облака
    /// </summary>
    public interface IEditCloudCoreProvider
    {
        /// <summary>
        /// Для редактирования контента страницы уведомления
        /// </summary>
        /// <param name="mainPageNotification">Отредактированный контент страницы уведомления</param>
        void EditMainPageNotification(string mainPageNotification);

        /// <summary>
        /// Для редактирования дефолтного сегмента
        /// </summary>
        /// <param name="defaultSegmentId">ID дефолтного сегмента</param>
        void EditDefaultSegment(Guid defaultSegmentId);

        /// <summary>
        /// Для редактирования разницы часов между Украиной и Москвой
        /// </summary>
        /// <param name="hoursDifferenceOfUkraineAndMoscow">Разница часов между Украиной и Москвой</param>
        void EditHoursDifferenceOfUkraineAndMoscow(int hoursDifferenceOfUkraineAndMoscow);

        /// <summary>
        /// Для переключения между Юкассой и Робокассой
        /// </summary>
        /// <param name="aggregator">Вид аггрегатора для переключения</param>
        void EditRussianLocalePaymentAggregator(RussianLocalePaymentAggregatorEnum aggregator);
    }
}