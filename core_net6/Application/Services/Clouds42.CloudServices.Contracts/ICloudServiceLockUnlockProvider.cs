﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер блокирования/ разблокирования сервиса.
    /// </summary>
    public interface ICloudServiceLockUnlockProvider
    {

        /// <summary>
        /// Заблокировать сервис.
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        void Lock(ResourcesConfiguration resourcesConfiguration);

        /// <summary>
        /// Разблокировать сервис.
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        void Unlock(ResourcesConfiguration resourcesConfiguration);
    }
}