﻿using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для создания терминального сервера
    /// </summary>
    public interface ICreateCloudTerminalServerProvider
    {
        /// <summary>
        /// Создать терминальный сервер
        /// </summary>
        /// <param name="createCloudTerminalServer">Модель создания
        /// терминального сервера</param>
        /// <returns>Id созданного сервера</returns>
        Guid Create(CreateCloudTerminalServerDto createCloudTerminalServer);
    }
}
