﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Класс проверки наличия возможности начислить ЗД в рмках активной аренды 1С.
    /// </summary>
    public interface IAllowIncreaseDocLoaderByStandardRent1CChecker
    {
        /// <summary>
        /// Проверить возможность бесплатного начисления лицензий ЗД.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>true - если начислить можно и false в противном случае.</returns>
        bool CheckAllow(Guid accountId);

    }
}