﻿using Clouds42.DataContracts.Cloud42Services.Rent1C;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с данными упраления Арендой 1С
    /// </summary>
    public interface IRent1CDataManagementDataProvider
    {
        /// <summary>
        /// Получить данные для управления Арендой 1С
        /// аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные для управления Арендой 1С</returns>
        Rent1CManagerDto GetManagementData(Guid accountId);
    }
}
