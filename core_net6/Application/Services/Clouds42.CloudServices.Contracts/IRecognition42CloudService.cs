﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.CloudServices.Contracts
{
    public interface IRecognition42CloudService : ICloud42ServiceBase
    {
        BuyingEsdlResultModelDto BuyRecognition42(int pagesAmount);

        decimal? GetNeedModeyForBuyServices(int pagesAmount, int years);

        IRecognition42CloudService SetAccountId(Guid accountId);

        decimal GetCost(int pagesAmount);
    }
}