﻿using Clouds42.DataContracts.Cloud42Services.Rent1C;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер управления активации/диактивации сервиса Аренда 1С у ползователй
    /// </summary>
    public interface IRent1CConfigurationAccessProvider
    {

        /// <summary>
        ///     Подключение/отключение аренды для своих и спонсируемых пользователей
        /// с использованием услуги Обещаный платеж
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accessList">Список из изменяемых пользователей</param>
        /// <returns></returns>
        bool TryPayAccessByPromisePaymentForRent1C(Guid accountId, List<UpdaterAccessRent1CRequestDto> accessList);

        /// <summary>
        ///     Подключение и отключение аренды для своих и спонсируемых пользователей
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accessList">Список из изменяемых пользователей</param>
        /// <returns></returns>
        UpdateAccessRent1CResultDto ConfigureAccesses(Guid accountId, List<UpdaterAccessRent1CRequestDto> accessList);

        /// <summary>
        /// Метод активирования Аренды 1С
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <returns>Результат активации аренды 1С</returns>
        void TurnOnRent1C(Guid accountId, Guid? accountUserId = null);

    }
}
