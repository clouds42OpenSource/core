﻿using Clouds42.DataContracts.Service;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с сервисом Fasta
    /// </summary>
    public interface IFastaServiceProvider
    {
        /// <summary>
        /// Получить модель управления
        /// демо периодом сервиса Fasta
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель управления сервисом Fasta</returns>
        ManageFastaDemoPeriodDto GetManageFastaDemoPeriodDto(Guid accountId);

        /// <summary>
        /// Получить конфигурацию ресурса для сервиса Fasta
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Конфигурация ресурса</returns>
        ResourcesConfiguration GetResourcesConfiguration(Guid accountId);
    }
}
