﻿using Clouds42.DataContracts.Cloud42Services.Rent1C;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с данными Аренды 1С клиента
    /// </summary>
    public interface IRent1CClientDataProvider
    {
        /// <summary>
        /// Получить виды разрешений на аренду 1С
        /// для клиента 
        /// </summary> 
        /// <param name="accountId">Id аккаунта пользователя</param>
        /// <param name="requestedUserId">Id пользователя который запрашивает информацию</param>
        /// <returns>Разрешения на аренду 1С для клиента</returns>
        Rent1CClientPermissionsDto GetRent1CClientPermissions(Guid accountId, Guid requestedUserId);

        /// <summary>
        /// Получить дату окончания срока действия
        /// Аренды 1С для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Дата окончания срока действия
        /// Аренды 1С</returns>
        DateTime GetRent1CExpireDateForAccount(Guid accountId);
    }
}
