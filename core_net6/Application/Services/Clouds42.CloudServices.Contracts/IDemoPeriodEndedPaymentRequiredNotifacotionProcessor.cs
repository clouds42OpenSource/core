﻿using System.Text;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    public interface IDemoPeriodEndedPaymentRequiredNotifacotionProcessor
    {
        void Process(ResourcesConfiguration resourcesConfiguration, StringBuilder stringBuilder);
    }
}