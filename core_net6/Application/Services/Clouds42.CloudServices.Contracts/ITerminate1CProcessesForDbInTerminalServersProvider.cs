﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер завершения 1С процессов для инфомационной базы
    /// на терминальных серверах
    /// </summary>
    public interface ITerminate1CProcessesForDbInTerminalServersProvider
    {
        /// <summary>
        /// Завершить 1С процессы для инфомационной базы
        /// на терминальных серверах
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        void Terminate(Guid databaseId);
    }
}
