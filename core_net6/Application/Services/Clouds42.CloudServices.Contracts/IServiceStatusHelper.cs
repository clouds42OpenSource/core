﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    public interface IServiceStatusHelper
    {
        ServiceStatusModelDto CreateServiceStatusModel(Guid accountId, ResourcesConfiguration resConfig);
        ServiceStatusModelDto CreateModelForRentAndMyDisk(Guid accountId);
    }
}
