﻿using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    public interface IRent1CManageHelper
    {
        void CreateDemoResources(int countWebResources, int countStandartResources, IResourcesConfiguration resourcesConfiguration);
        IEnumerable<BillingServiceType> GetDeletedServiceTypesForAccount(List<Guid> services, Guid accountId);
        void Lock(Guid accountId);
        string ManageRent1C(Rent1CServiceManagerDto model, Guid accountId);
        void UnLock(Guid accountId);
    }
}