﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    public interface IPromisedPaymentProcessor
    {
        void ProcessRegularOperation(BillingAccount billingAccount);
        bool EarlyRepay(BillingAccount billingAccount);
        bool ProlongPromisePayment(BillingAccount billingAccount);
        void RepayPromisedPayment(BillingAccount billingAccount, decimal repaytDiff);
    }
}
