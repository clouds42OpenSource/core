﻿using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.DataContracts.Service.Tardis.Requests;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Синхронизатор с Тардис.
    /// </summary>
    public interface ITardisSynchronizerHelper
    {
        /// <summary>
        /// Выполнить синхронизацию пользователя с Тардис.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        void SynchronizeAccountUser<TTrigger, TTriggerModel>(TTriggerModel model) where TTrigger : SynchTriggerBase<TTriggerModel>
            where TTriggerModel : class, IAccountUserIdDto;

        /// <summary>
        /// Выполнить синхронизацию базы с Тардис.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        void SynchronizeAccountDatabase<TTrigger, TTriggerModel>(TTriggerModel model) where TTrigger : SynchTriggerBase<TTriggerModel>
            where TTriggerModel : class, IAccountDatabaseIdDto;

        /// <summary>
        /// Выполнить синхронизацию с Тардис при изменение доступа пользователя в информационной базе.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        void SynchronizeAccountDatabaseAccess<TTrigger, TTriggerModel>(TTriggerModel model) where TTrigger : SynchTriggerBase<TTriggerModel>
            where TTriggerModel : class, IAccountDatabaseAccessDto;
    }
}
