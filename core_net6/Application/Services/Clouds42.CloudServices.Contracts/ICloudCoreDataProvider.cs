﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с данными по настройке облака
    /// </summary>
    public interface ICloudCoreDataProvider
    {
        /// <summary>
        /// Получить тип платежного агрегатора для русской локали
        /// </summary>
        /// <returns>Платежный агрегатор русской локали</returns>
        RussianLocalePaymentAggregatorEnum GetRussianLocalePaymentAggregator();

        /// <summary>
        /// Получить дефолтные конфигурации
        /// </summary>
        /// <returns>Обьект дефолтных конфигураций</returns>
        CloudCoreDefConfigurationDto GetDefaultConfiguration();

        /// <summary>
        /// Получить настройки облака
        /// </summary>
        /// <returns></returns>
        CloudCore GetCloudCore(out bool needInsert);
    }
}
