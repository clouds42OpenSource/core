﻿using Clouds42.DataContracts.BaseModel;
using Microsoft.AspNetCore.Http;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с файлом облака
    /// </summary>
    public interface ICloudFileProvider
    {
        /// <summary>
        /// Создать файл облака
        /// </summary>
        /// <param name="fileData">Файл</param>
        /// <returns>Id файла</returns>
        Guid CreateCloudFile(FileDataDto<IFormFile> fileData);

        /// <summary>
        /// Изменить файл
        /// </summary>
        /// <param name="fileId">Id файла</param>
        /// <param name="fileData">Файл для замены</param>
        void EditCloudFile(Guid fileId, FileDataDto<IFormFile> fileData);

        /// <summary>
        /// Попытаться создать файл облака
        /// </summary>
        /// <param name="uploadedFile">Загруженный файл</param>
        /// <returns>Id созданного файла облака или null, если загруженный файл отсутсвует</returns>
        Guid? TryCreateCloudFile(FileDataDto<IFormFile> uploadedFile);

        /// <summary>
        /// Сравнить содержимое файлов
        /// </summary>
        /// <param name="newContent">Новое содержимое</param>
        /// <param name="existContent">Существующее содержимое</param>
        /// <returns>true - если содержимое не отличается</returns>
        bool CompareCloudFilesContent(byte[] newContent, byte[] existContent);
    }
}
