﻿using Clouds42.DataContracts.Service;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для изменения демо периода Fasta сервиса
    /// </summary>
    public interface IChangeFastaDemoPeriodProvider
    {
        /// <summary>
        /// Изменить демо период для Fasta сервиса
        /// </summary>
        /// <param name="manageFastaDemoPeriodDto">Модель управления демо периодом сервиса Fasta</param>
        void Change(ManageFastaDemoPeriodDto manageFastaDemoPeriodDto);
    }
}
