﻿using System.Linq.Expressions;
using Clouds42.Domain.DataModels;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Обработчик по начислению лицензий в рамках активного сервиса Аренда 1С.
    /// </summary>
    public interface IDocLoaderByRent1CProlongationProcessor
    {

        /// <summary>
        /// Выполнить обработку по начислению лицензий в рамках активного сервиса Аренда 1С.
        /// </summary>
        void ProcessIncreaseForAccounts(Expression<Func<Account, bool>> filter = null);

        /// <summary>
        /// Выполнить обработку по начислению лицензий в рамках активного сервиса Аренда 1С.
        /// </summary>
        /// <param name="accountId"></param>
        void ProcessIncrease(Guid accountId);

    }
}
