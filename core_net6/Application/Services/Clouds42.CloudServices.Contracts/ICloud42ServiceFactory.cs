﻿using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Contracts
{
    public interface ICloud42ServiceFactory
    {
        Cloud42ServiceBase Get(Clouds42Service clouds42Service, Guid accountId);
        TService Get<TService>(Guid accountId) where TService : ICloud42ServiceBase;
    }
}