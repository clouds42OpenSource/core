﻿using Clouds42.CloudServices.Contracts.Triggers;

namespace Clouds42.CloudServices.Contracts
{
    public interface ITardisSynchronizer
    {
        void Synchronize<TTrigger, TTriggerModel>(TTriggerModel triggerModel) where TTrigger : SynchTriggerBase<TTriggerModel> where TTriggerModel : class;
    }
}