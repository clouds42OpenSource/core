﻿using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.CloudServices.Contracts
{
    public interface ICloud42ServiceHelper
    {
        decimal CalculateActualCostOfMyDiskService(int size, Account account);
        decimal CalculateDiskCost(int size, Account account);
        MyDiskChangeTariffPreviewDto ChangeSizeOfServicePreview(Guid accountId, int size);
        long GetAvailableSpaceOnDisk(Guid accountId);
        DateTime GetCurrentExpireDateOrNewDateToCalculateCostOfService(DateTime? expireDate);
        List<string> GetEnabledAccountUserLogins(Account account, Clouds42Service service, ResourceType resourceType);
        MyDiskInfoModelDto GetMyDiskInfo(Guid accountId, long availableSizeOnDisk = 0);
        long GetPayedSpaceOnDisk(Guid accountId);
    }
}
