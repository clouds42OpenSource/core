﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Интерфейс для снятия и установки редиректа при заблокированной аренде 1С
    /// </summary>
    public interface IRedirectPublishAccountDatabase
    {
        //Установка редиректа при заблокированной аренде 1С
        // Происходит замена web.config на другой в котором прописана страничка для редиректа
        void SetRedirectNeedMoneyToAccount(Guid accountId);

        // Снятие редиректа при разблокировании Аренды 1С
        // Происходит замена web.config на прежний который был при публикации
        void DeleteRedirectionNeedMoneyForAccount(Guid accountId);
    }
}
