﻿using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.CloudServices.Contracts
{
    public interface IMyDisk42CloudService : ICloud42ServiceBase
    {
        /// <summary>
        /// Установка параметра идентификатора аккаунта
        /// </summary>
        /// <param name="accountId">идентификатор аккаунта</param>
        /// <returns></returns>

        bool CanProlong(out string? reason);
        TryChangeTariffResultDto TryChangeTariff(int size);
        bool SetTariffByPromisePayment(int size);

        /// <summary>
        /// Переход на новый тариф "Мой диск"
        /// </summary>
        /// <param name="size">Новый обьём памяти в гигабайтах</param>
        /// <returns></returns>
        bool PayTariff(int size);

        /// <summary>
        /// Управление сервисом администратором облака 
        /// </summary>
        /// <param name="model">Модель управления сервисом "Мой диск"</param>
        void ManagedMyDisk(ServiceMyDiskManagerDto model);

        void BeforeLock(BeforeLockInServicefoModelDto beforeLockInServicefoModel);
        Guid AccountId { get; }
        ResourcesConfiguration ResourceConfiguration { get; }
        void LogEvent(Func<Guid?> getAccountId, LogActions log, string description);

        IMyDisk42CloudService SetAccountId(Guid accountId);
    }
}

