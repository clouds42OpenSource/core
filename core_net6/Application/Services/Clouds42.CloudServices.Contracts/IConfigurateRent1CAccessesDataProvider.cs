﻿using Clouds42.DataContracts.Cloud42Services.Rent1C;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с данными
    /// для конфигурации лицензий сервиса Аренда 1С
    /// </summary>
    public interface IConfigurateRent1CAccessesDataProvider
    {
        /// <summary>
        /// Получить данные для конфигурации лицензий сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные для конфигурации лицензий сервиса Аренда 1С</returns>
        Rent1CAccessesForConfigurationDataDto GetData(Guid accountId);
    }
}
