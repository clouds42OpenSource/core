﻿using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
 
using Clouds42.LetterNotification.Contracts;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Базовый класс для работы с сервисом облака
    /// </summary>
    public abstract class Cloud42ServiceBase(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IConfiguration configuration,
        INotificationProcessor notificationProcessor,
        ICreatePaymentHelper createPaymentHelper,
        ICheckAccountDataProvider checkAccountDataProvider)
    {
        public readonly IAccessProvider AccessProvider = accessProvider;
        public readonly IUnitOfWork DbLayer = dbLayer;
        protected readonly ICreatePaymentHelper CreatePaymentHelper = createPaymentHelper;
        protected readonly ICheckAccountDataProvider CheckAccountProvider = checkAccountDataProvider;
        protected readonly ILogger42 Logger = Logger42.GetLogger();
        protected readonly IHandlerException HandlerException = HandlerException42.GetHandler();
        public readonly IConfiguration Сonfiguration = configuration;

        private Guid _accountId;
        public Guid AccountId
        {
            get => _accountId;
            protected set
            {
                if (_accountId != value)
                    _resourcesConfiguration = null;
                _accountId = value;
            }
        }

        private ResourcesConfiguration? _resourcesConfiguration;
        public ResourcesConfiguration ResourceConfiguration
        {
            get
            {
                if (_resourcesConfiguration != null)
                    return _resourcesConfiguration;

                return _resourcesConfiguration = GetResourceConfigurationForAccount(AccountId);
            }
        }
        protected virtual ResourcesConfiguration GetResourceConfigurationForAccount(Guid accountId)
        {
            return DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.AccountId == accountId && rc.BillingService.SystemService == CloudServiceType);
        }

        public virtual void BeforeLock(BeforeLockInServicefoModelDto beforeLockInServicefoModel)
        {
            BeforeLock();
            notificationProcessor.NotifyBeforeLockService(beforeLockInServicefoModel);
        }

        public virtual bool CanProlong(out string? reason)
        {
            reason = null;
            return true;
        }

        public abstract Clouds42Service CloudServiceType { get; }
        protected abstract void BeforeLock();
        public abstract void Lock();
        public abstract void UnLock();
        public abstract void ActivateService();
        public abstract void AcceptVisitor(ISystemCloudServiceProcessorVisitor visitor);

        public void LogEvent(Func<Guid?> getAccountId, LogActions log, string description)
        {
            try
            {
                var accountId = getAccountId.Invoke();

                if (accountId == null)
                {
                    Logger.Warn($"анонимная операция: '{description}' ");
                    return;
                }

                LogEventHelper.LogEvent(DbLayer, accountId.Value, AccessProvider, log, description);
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, "[Ошибка логирования]");
            }
        }
    }
}
