﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер зменения значения ресурсов аккаунта.
    /// </summary>
    public interface IIncreaseDecreaseResourceProvider
    {
        /// <summary>
        /// Изменить значение ресурса.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="flowResourceId">Номер сервсиа.</param>
        /// <param name="value">Значение списания/начсиления ресурса.</param>
        /// <param name="comment">Коментарий к изменению.</param>
        void ChangeValue(Guid accountId, Guid flowResourceId, int value, string comment);
    }
}