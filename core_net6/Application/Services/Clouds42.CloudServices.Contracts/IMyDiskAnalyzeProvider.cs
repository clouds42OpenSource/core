﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер выполнения расчета диска.
    /// </summary>
    public interface IMyDiskAnalyzeProvider
    {
        /// <summary>
        ///     Пересчет занятого место на диске для клиентских файлов
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        void RecalculateClientFilesInMyDisk(Guid accountId);

        /// <summary>
        /// Пересчитать используемое место на диске аккаунтом
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        void RecalculateAccountSize(Guid accountId);

        /// <summary>
        /// Проверить выполнена ли задача по пересчету диска
        /// </summary>
        /// <param name="taskId">Id задачи</param>
        /// <returns>Результат проверки</returns>
        bool CheckMyDiskRecalculationJobFinished(Guid taskId);
    }
}
