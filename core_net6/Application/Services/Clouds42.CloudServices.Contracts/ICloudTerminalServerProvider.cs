﻿using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.Domain.DataModels;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для работы с терминальным сервером
    /// </summary>
    public interface ICloudTerminalServerProvider
    {
        /// <summary>
        /// Получить терминальный сервер
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <returns>Терминальный сервер</returns>
        CloudTerminalServer GetCloudTerminalServer(Guid terminalServerId);

        /// <summary>
        /// Получить Id терминальных серверов
        /// </summary>
        /// <returns>Список Id терминальных серверов</returns>
        List<Guid> GetTerminalServersIdList();

        /// <summary>
        /// Получить количество терминальных серверов
        /// </summary>
        /// <returns>Количество терминальных серверов</returns>
        int GetTerminalServersCount();

        /// <summary>
        /// Получить адреса терминальных серверов для инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель адресов терминальных серверов для информационной базы</returns>
        TerminalServerAddressesForDatabaseDto GetTerminalServerAddressesForDatabase(Guid databaseId);
    }
}
