﻿using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для фиксирования истории изменений ресурсов главного сервиса(Аренда1С)
    /// </summary>
    public interface ICommitMainServiceResourcesChangesHistoryProvider
    {
        /// <summary>
        /// Зафиксировать историю изменений ресурсов главного сервиса(Аренда1С)
        /// </summary>
        /// <param name="model">Модель для фиксирования истории изменений ресурсов</param>
        void Commit(CommitMainServiceResourcesChangesHistoryDto model);
    }
}