﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер удаления терминального сервера
    /// </summary>
    public interface IDeleteCloudTerminalServerProvider
    {
        /// <summary>
        /// Удалить терминальный сервер
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        void Delete(Guid terminalServerId);
    }
}
