﻿namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер данных сводной таблицы движемых ресурсов.
    /// </summary>
    public interface IFlowResourcesScopeDataProvider
    {
        /// <summary>
        /// Добавить новую запись.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="flowResourceId">Номер ресурса.</param>
        /// <param name="scopeValue">Опционально. Сводное значение ресурса.</param>
        void Insert(Guid accountId, Guid flowResourceId, int scopeValue = 0);


        /// <summary>
        /// Получить сумарное значение ресурсов заданного сервсиа у аккаунта.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="flowResourceId">Номер сервиса.</param>
        /// <param name="dateTime">Дата, на момент которой нужно получить вычисления.</param>
        /// <returns>Сумарное значение ресурсов заданного сервсиа у аккаунта.</returns>
        Task<int> GetScopeValue(Guid accountId, Guid flowResourceId, DateTime? dateTime = null);

    }
}