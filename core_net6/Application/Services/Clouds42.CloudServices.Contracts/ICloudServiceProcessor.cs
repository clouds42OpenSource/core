﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    public interface ICloudServiceProcessor
    {
        void Process(ResourcesConfiguration resourcesConfiguration, Action<Cloud42ServiceBase> systemServiceProcess, Action simpleServiceProcess);
        TResult Process<TResult>(ResourcesConfiguration resourcesConfiguration, Func<Cloud42ServiceBase, TResult> systemServiceProcess, Func<TResult> simpleServiceProcess);
    }
}
