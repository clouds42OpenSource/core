﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.CloudServices.Contracts
{
    public interface ILockDemoServiceNotifacotionProcessor
    {
        void Process(ResourcesConfiguration resourcesConfiguration);
    }
}