﻿using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.CloudServices.Contracts
{
    public interface IMyEnterprise42CloudService : ICloud42ServiceBase
    {
        bool CanProlong(out string? reason);

        new Clouds42Service CloudServiceType { get; }
        Guid AccountId { get; }
        ResourcesConfiguration ResourceConfiguration { get; }
        void BeforeLock(BeforeLockInServicefoModelDto beforeLockInServicefoModel);

        /// <summary>
        ///    1. Блокировка Аренды 1С.
        ///    2. Удаление пользователей из групп AD.
        /// </summary>
        new void Lock();

        /// <summary>
        ///    1. Разблокировка Аренды 1С.
        ///    2. Восстановление пользователей в группы AD.
        /// </summary>
        new void UnLock();

        new void ActivateService();
        void ActivateService(Guid? accountUserId);

        /// <summary>
        /// Активировать Аренду 1С
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="rent1CServiceTypes">Список услуг аренды для подключения</param>
        void ActivateRent1C(Guid? accountUserId, List<ResourceType> rent1CServiceTypes);

        new void AcceptVisitor(ISystemCloudServiceProcessorVisitor visitor);

        /// <summary>
        /// Управление менеджером сервисом "Аренда 1С"
        /// </summary>
        /// <param name="model"></param>
        void ManagedRent1C(Rent1CServiceManagerDto model);

        /// <summary>
        /// Покупка сервиса "Аренда 1С" за счет обещанного платежа
        /// </summary>
        /// <param name="accessList"></param>
        /// <returns></returns>
        bool TryPayAccessByPromisePayment(List<UpdaterAccessRent1CRequestDto> accessList);

        /// <summary>
        /// Сконфигурировать доступы к аренде для пользователей.
        /// </summary>
        /// <param name="accesses">Список доступов.</param>        
        /// <returns>Результат изменения доступов к аренде.</returns>
        UpdateAccessRent1CResultDto ConfigureAccesses(List<UpdaterAccessRent1CRequestDto> accesses);

        IMyEnterprise42CloudService SetAccountId(Guid accountId);
        void LogEvent(Func<Guid?> getAccountId, LogActions log, string description);
    }
}

