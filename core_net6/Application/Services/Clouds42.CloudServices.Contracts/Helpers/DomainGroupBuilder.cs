
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.Domain.DataModels;

namespace Clouds42.CloudServices.Contracts.Helpers
{
    public static class DomainGroupBuilder
    {
        public static string WebCompanyGroupBuild(Account account)
            => DomainCoreGroups.WebCompanyGroupBuild(account.IndexNumber);
    }
}
