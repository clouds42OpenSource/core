﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Locales.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CloudServices.Contracts.Helpers
{
    /// <summary>
    /// Хелпер для подготовки модели письма о скорой блокировке сервиса
    /// </summary>
    public static class PrepareBeforeServiceLockLetterModelHelper
    {
        private static readonly string RouteForOpenBalancePage =
            CloudConfigurationProvider.Cp.GetRouteForOpenBalancePage();

        /// <summary>
        /// Подготовить модель письма о скорой блокировке сервиса
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="invoiceDc">Модель счета</param>
        /// <param name="documentBuilderResult">Результат билда документа</param>
        /// <param name="siteAuthorityUrl">Урл сайта ЛК</param>
        /// <param name="invoicePeriod">Период счета</param>
        /// <param name="cloudLocalizer"></param>
        /// <returns>Модель письма о скорой блокировке сервиса</returns>
        public static BeforeServiceLockLetterModelDto PrepareBeforeServiceLockLetterModel(
            IUnitOfWork unitOfWork,
            ResourcesConfiguration resourcesConfiguration, InvoiceDc invoiceDc,
            DocumentBuilderResult documentBuilderResult, string siteAuthorityUrl, int? invoicePeriod, ICloudLocalizer cloudLocalizer)
        {
            var invoicePeriodValue = invoicePeriod is null or (int)PayPeriod.None
                ? (int)PayPeriod.Month1
                : invoicePeriod.Value;

            var accountEmails = unitOfWork.AccountsRepository.GetEmails(resourcesConfiguration.AccountId);

            return new BeforeServiceLockLetterModelDto
            {
                AccountId = resourcesConfiguration.AccountId,
                BillingServiceName = resourcesConfiguration.GetLocalizedServiceName(cloudLocalizer),
                AccountBalance = resourcesConfiguration.BillingAccounts.Balance,
                AccountBonusBalance = resourcesConfiguration.BillingAccounts.BonusBalance,
                LockServiceDate = resourcesConfiguration.ExpireDate ?? DateTime.Now,
                Invoice = new InvoiceDataDto
                {
                    InvoiceNumber = invoiceDc.Uniq,
                    InvoiceDocBytes = documentBuilderResult.Bytes,
                    InvoiceDocFileName = CloudConfigurationProvider.Invoices.GetInvoiceAttachFileName()
                },
                ServiceCost = invoiceDc.InvoiceSum / invoicePeriodValue,
                InvoiceSum = invoiceDc.InvoiceSum,
                InvoicePeriod = invoicePeriodValue,
                BalancePageUrl =
                    new Uri(
                            $"{siteAuthorityUrl}/{RouteForOpenBalancePage}")
                        .AbsoluteUri,
                EmailsToCopy = accountEmails
            };
        }
    }
}
