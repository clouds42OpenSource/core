﻿using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;

namespace Clouds42.CloudServices.Contracts.Helpers
{
    public static class CreateEditUserGroupsAdModelHelper
    {
        public static EditUserGroupsAdModelDto CreateEditUserGroupsAdModel(
            List<Resource> resources,
            AccountUser accountUser,
            EditUserGroupsAdModelDto.OperationEnum operation)
        {
            var webGroupName = DomainGroupBuilder.WebCompanyGroupBuild(accountUser.Account);
            
            const string rdpGroupName = DomainCoreGroups.RemoteDesktopAccess;

            return new EditUserGroupsAdModelDto()
                .Append(accountUser.Login, webGroupName, operation)
                .Append(accountUser.Login, rdpGroupName, resources.Any(r => r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser) ? operation : EditUserGroupsAdModelDto.OperationEnum.Exclude);
        }

        public static EditUserGroupsAdModelDto AppendAcDbAccesses(this EditUserGroupsAdModelDto editUserGroupsAdModelDto,
            List<AccountIndexNumberDbNumberDto> dbNumbers, AccountUser accountUser, EditUserGroupsAdModelDto.OperationEnum operation)
        {
            var items = dbNumbers.Select(x => new EditUserGroupsAdModelDto.Item
            {
                GroupName = DomainCoreGroups.GetDatabaseGroupName(x.AccountIndexNumber, x.DbNumber),
                Operation = operation,
                UserName = accountUser.Login
            }).ToList();

            editUserGroupsAdModelDto.AppendRange(items);

            return editUserGroupsAdModelDto;
        }

        public static List<EditUserGroupsAdModelDto> ConvertToEditUserGroupAdModels(this List<AccountIndexNumberDbNumberDto> dbNumbers, EditUserGroupsAdModelDto.OperationEnum operation)
        {
            var items = dbNumbers.Select(x => new EditUserGroupsAdModelDto
            {
                Items =
                [
                    new()
                    {
                        GroupName = DomainCoreGroups.GetDatabaseGroupName(x.AccountIndexNumber, x.DbNumber),
                        Operation = operation,
                        UserName = x.Login
                    }
                ]
            }).ToList();

            return items;
        }


        public static EditUserGroupsAdListModel CreateAdEnableRent1CModel(Guid accountId, AccountUser accountUser)
        {
            return CreateAdModel(accountId,
                new EditUserGroupsAdModelDto.Item
                {
                    UserName = accountUser.Login,
                    Operation = EditUserGroupsAdModelDto.OperationEnum.Include,
                    GroupName = DomainCoreGroups.RemoteDesktopAccess
                },
                new EditUserGroupsAdModelDto.Item
                {
                    UserName = accountUser.Login,
                    Operation = EditUserGroupsAdModelDto.OperationEnum.Include,
                    GroupName = DomainGroupBuilder.WebCompanyGroupBuild(accountUser.Account)
                });
        }

        public static EditUserGroupsAdListModel CreateAdModel(Guid accountId, params EditUserGroupsAdModelDto.Item[] items)
        {
            return new EditUserGroupsAdListModel
            {
                AccountId = accountId,
                EditUserGroupsModel =
                [
                    new() { Items = items.ToList() }
                ]
            };
        }
    }
}
