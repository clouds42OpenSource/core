﻿using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;

namespace Clouds42.CloudServices.Contracts
{
    /// <summary>
    /// Провайдер для создания истории изменений ресурсов главного сервиса(Аренда1С)
    /// для аккаунта
    /// </summary>
    public interface ICreateMainServiceResourcesChangesHistoryProvider
    {
        /// <summary>
        /// Создать историю изменений ресурсов главного сервиса(Аренда1С)
        /// </summary>
        /// <param name="model">Модель для фиксирования истории изменений ресурсов</param>
        void Create(CommitMainServiceResourcesChangesHistoryDto model);
    }
}
