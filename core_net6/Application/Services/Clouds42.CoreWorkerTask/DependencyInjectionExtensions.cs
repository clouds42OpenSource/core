﻿using System.Data;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorkerTask.Managers;
using Clouds42.CoreWorkerTask.Providers;
using Medallion.Threading;
using Medallion.Threading.Postgres;
using Medallion.Threading.SqlServer;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorkerTask
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddAuditCoreWorkerTasksQueue(this IServiceCollection services)
        {
            services.AddTransient<IAuditCoreWorkerTasksQueueProvider, AuditCoreWorkerTasksQueueProvider>();
            services.AddTransient<ICoreWorkersDataProvider, CoreWorkersDataProvider>();
            services.AddTransient<IRegisterTaskInQueueProvider, RegisterTaskInQueueProvider>();
            services.AddTransient<ICoreWorkerTaskDataProvider, CoreWorkerTaskDataProvider>();
            services.AddTransient<IEditCoreWorkerTaskProvider, EditCoreWorkerTaskProvider>();
            services.AddTransient<ISearchCoreWorkerTaskProvider, SearchCoreWorkerTaskProvider>();
            services.AddTransient<ICoreWorkerAvailableTasksProvider, CoreWorkerAvailableTasksProvider>();
            services.AddTransient<ICoreWorkerTasksBagsPriorityProvider, CoreWorkerTasksBagsPriorityProvider>();
            services.AddTransient<ICoreWorkerTasksQueueDataProvider, CoreWorkerTasksQueueDataProvider>();
            services.AddTransient<ICoreWorkerTasksQueueProvider, CoreWorkerTasksQueueProvider>();
            services.AddTransient<ICoreWorkerTaskManager, CoreWorkerTaskManager>();
            services.AddTransient<ICoreWorkersDataManager, CoreWorkersDataManager>();
            services.AddTransient<ICoreWorkerTasksQueueManager, CoreWorkerTasksQueueManager>();
            services.AddTransient<ICoreWorkerTasksQueueDataManager, CoreWorkerTasksQueueDataManager>();
            services.AddTransient<IReportCoreWorkerTaskProvider, ReportCoreWorkerTaskProvider>();
            
            return services;
        }

        public static IServiceCollection AddDistributedIsolatedExecutor(this IServiceCollection services, DbObjectLockProvider provider)
        {
            services.AddSingleton<IDbObjectLockProvider>(provider);
            services.AddSingleton(provider.DistributedLockProvider);

            return services;
        }

    }

    public class DbObjectLockProvider : IDbObjectLockProvider
    {
        public IDistributedLockProvider DistributedLockProvider { get; }

        public DbObjectLockProvider(string connectionString, string dbType)
        {
            DistributedLockProvider = dbType == "mssql" ? new SqlDistributedSynchronizationProvider(connectionString) : new PostgresDistributedSynchronizationProvider(connectionString);
        }

        public DbObjectLockProvider(IDbConnection dbConnection, string dbType)
        {
            DistributedLockProvider =  dbType == "mssql" ? new SqlDistributedSynchronizationProvider(dbConnection) : new PostgresDistributedSynchronizationProvider(dbConnection);
        }

        public async ValueTask<IDisposable> AcquireLockAsync(string lockName,  TimeSpan? timeout = null)
        {
            return await DistributedLockProvider.AcquireLockAsync(lockName, timeout ?? Timeout.InfiniteTimeSpan);
        }

        public IDisposable AcquireLock(string lockName, TimeSpan? timeout = null)
        {
            return DistributedLockProvider.AcquireLock(lockName, timeout ?? Timeout.InfiniteTimeSpan);
        }
    }

    public interface IDbObjectLockProvider
    {
        ValueTask<IDisposable> AcquireLockAsync(string lockName, TimeSpan? timeout = null);

        IDisposable AcquireLock(string lockName, TimeSpan? timeout = null);
    }
}
