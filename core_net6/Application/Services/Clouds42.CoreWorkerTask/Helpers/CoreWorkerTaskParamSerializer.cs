﻿using System.Runtime.Serialization.Json;
using System.Text;
using Clouds42.HandlerExeption.Contract;
 

namespace Clouds42.CoreWorkerTask.Helpers
{
    public class CoreWorkerTaskParamSerializer<T> 
    {
        private readonly IHandlerException _handlerException = HandlerException42.GetHandler();
        public T DeSerialize(string param)
        {
            T res;
            using var srm = new MemoryStream(Encoding.UTF8.GetBytes(param));
            var ser = new DataContractJsonSerializer(typeof(T));

            try
            {
                res = (T)ser.ReadObject(srm);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Неправильный формат параметров] : {param}");
                throw;
            }

            return res;
        }

        public string Serialize(T contract)
        {
            var ser = new DataContractJsonSerializer(typeof(T));

            using var str = new MemoryStream();
            ser.WriteObject(str, contract);

            str.Position = 0;
            using var srm = new StreamReader(str);
            var s = srm.ReadToEnd();
            return s;
        }
    }
}

