﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorkerTask.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Managers
{
    /// <summary>
    /// Менеджер очереди задач воркера
    /// </summary>
    public class CoreWorkerTasksQueueManager(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        ICoreWorkerTasksQueueProvider coreWorkerTasksQueueProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, unitOfWork, handlerException), ICoreWorkerTasksQueueManager
    {
        /// <summary>
        /// Добавить задачу
        /// </summary>
        /// <param name="taskInfo">Модель параметров задачи</param>
        /// <returns>ID созданной задачи</returns>
        public ManagerResult<Guid> Add(CoreWorkerTasksDto taskInfo)
        {
            return AddTaskQueue(taskInfo);
        }

        /// <summary>
        /// Установить комментарий для задачи
        /// </summary>
        /// <param name="taskInfo">Модель параметров задачи</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult SetComment(CoreWorkerTasksQueueSetCommentDto taskInfo)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_Edit);

                var result = coreWorkerTasksQueueProvider.SetComment(taskInfo);
                return result
                    ? Ok()
                    : ConflictError("Core worker task queue not found");
            }
            catch (Exception ex)
            {
                return PreconditionFailed<IEnumerable<CoreWorkerTasksQueue>>(
                    $"Ошибка установки комментария для задачи {taskInfo.Id}. Причина {ex.Message}");
            }
        }

        /// <summary>
        /// Получить список ID задач
        /// </summary>
        /// <param name="status">Статус задач</param>
        /// <param name="maxRecordsCount">Максимально количество записей</param>
        /// <returns>Список ID задач</returns>
        public ManagerResult<IEnumerable<CoreWorkerTasksQueue>> GetIds(string status, int maxRecordsCount)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_View);

                var coreWorkerTasksQueues = coreWorkerTasksQueueProvider.GetTasksQueuesId(status, maxRecordsCount);
                return Ok(coreWorkerTasksQueues);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<IEnumerable<CoreWorkerTasksQueue>>(
                    $"Ошибка получения списка ID задач. Причина {ex.Message}");
            }
        }

        /// <summary>
        /// Получить ID задачи воркера в БД
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <returns>ID задачи воркера в БД</returns>
        public ManagerResult<Guid> GetCoreWorkerTaskId(Guid taskId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_View);

                var task = coreWorkerTasksQueueProvider.GetTasksInQueue(taskId);
                return task == null ? Conflict<Guid>("Core worker task queue not found") : Ok(task.CoreWorkerTaskId);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<Guid>(
                    $"Ошибка получения ID задачи {taskId}. Причина {ex.Message}");
            }
        }

        /// <summary>
        /// Получить статус задачи
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Статус задачи</returns>
        public ManagerResult<string> GetTaskStatus(Guid taskId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_View);

                var task = coreWorkerTasksQueueProvider.GetTasksInQueue(taskId);
                return task == null ? Conflict<string>("Core worker task queue not found") : Ok(task.Status);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<string>(
                    $"Ошибка получения статуса задачи {taskId}. Причина {ex.Message}");
            }
        }

        /// <summary>
        /// Получить дату создания задачи
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Дата создания задачи</returns>
        public ManagerResult<DateTime> GetDate(Guid taskId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_View);

                var task = coreWorkerTasksQueueProvider.GetTasksInQueue(taskId);
                return task == null ? Conflict<DateTime>("Core worker task queue not found") : Ok(task.CreateDate);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<DateTime>(
                    $"Ошибка получения даты создания задачи {taskId}. Причина {ex.Message}");
            }
        }

        /// <summary>
        /// Получить комментарий к задаче
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Комментарий к задаче</returns>
        public ManagerResult<string> GetComment(Guid taskId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_View);

                var task = coreWorkerTasksQueueProvider.GetTasksInQueue(taskId);
                return task == null ? Conflict<string>("Core worker task queue not found") : Ok(task.Comment);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<string>(
                    $"Ошибка получения комментария к задаче {taskId}. Причина {ex.Message}");
            }
        }

        /// <summary>
        /// Получить ID воркера, который обработал задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <returns>ID воркера, который обработал задачу</returns>
        public ManagerResult<short?> GetCoreWorkerId(Guid taskId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_View);

                var task = coreWorkerTasksQueueProvider.GetTasksInQueue(taskId);
                return task == null ? Conflict<short?>("Core worker task queue not found") : Ok(task.CapturedWorkerId);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<short?>(
                    $"Ошибка получения ID воркера, который обработал задачу {taskId}. Причина {ex.Message}");
            }
        }

        /// <summary>
        /// Получить задачу 
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Модель задачи</returns>
        public ManagerResult<CoreWorkerTasksQueue> GetProperties(Guid taskId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_View);

                var task = coreWorkerTasksQueueProvider.GetTasksInQueue(taskId);
                return task == null ? Conflict<CoreWorkerTasksQueue>("Core worker task queue not found") : Ok(task);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<CoreWorkerTasksQueue>(
                    $"Ошибка получения задачи по ID {taskId}. Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Добавить задачу
        /// </summary>
        /// <param name="taskType">Тип задачи</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>ID задачи</returns>
        public ManagerResult<Guid> Add(CoreWorkerTaskType taskType, string comment, object taskParams = null)
            => Add(taskType, comment, null, taskParams);

        /// <summary>
        /// Добавить задачу в очередь
        /// </summary>
        /// <param name="taskInfo">Модель параметров задачи</param>
        /// <returns>ID созданной задачи</returns>
        public ManagerResult<Guid> AddTaskQueue(CoreWorkerTasksDto taskInfo)
        {
            var coreWorkerTask = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(t => t.ID == taskInfo.CloudTaskId);
            return coreWorkerTask == null
                ? Conflict<Guid>("Core worker task not found")
                : Add(coreWorkerTask, taskInfo.Comment, taskInfo.DateTimeDelayOperation, taskInfo.TaskParams);
        }

        /// <summary>
        /// Добавить задачу
        /// </summary>
        /// <param name="taskType">Тип задачи</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="dateTimeDelayOperation">Задержка выполнения задачи</param>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>ID задачи</returns>
        private ManagerResult<Guid> Add(CoreWorkerTaskType taskType, string comment, DateTime? dateTimeDelayOperation, object taskParams = null)
            => Add(taskType.ToString(), comment, dateTimeDelayOperation, taskParams);

        /// <summary>
        /// Добавить задачу
        /// </summary>
        /// <param name="taskName">Название задачи</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="dateTimeDelayOperation">Задержка выполнения задачи</param>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>ID задачи</returns>
        private ManagerResult<Guid> Add(string taskName, string comment, DateTime? dateTimeDelayOperation, object taskParams = null)
        {
            var coreWorkerTask = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(t => t.TaskName == taskName);
            return coreWorkerTask == null
                ? Conflict<Guid>("Core worker task not found")
                : Add(coreWorkerTask, comment, dateTimeDelayOperation, taskParams);
        }

        /// <summary>
        /// Добавить задачу
        /// </summary>
        /// <param name="coreWorkerTask">Задача</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="dateTimeDelayOperation">Задержка выполнения задачи</param>
        /// <param name="taskParams">Параметры задачи</param>
        /// <returns>ID задачи</returns>
        private ManagerResult<Guid> Add(Domain.DataModels.CoreWorkerTask coreWorkerTask, string comment, DateTime? dateTimeDelayOperation, object taskParams = null)
        {
            return Ok(new RegisterTaskInQueueProvider(DbLayer, logger).RegisterTask(coreWorkerTask.ID, comment,
                dateTimeDelayOperation,
                taskParams != null ? new ParametrizationModelDto(taskParams) : null
            ));
        }

        /// <summary>
        /// Захватить задачу из очереди
        /// </summary>
        /// <param name="captureTaskRequestDto">Тело запроса на захват задачи</param>
        /// <returns>ID задачи из очереди</returns>
        public async Task<ManagerResult<Guid?>> CaptureTaskFromQueueAsync(CaptureTaskRequestDto captureTaskRequestDto)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTaskQueue_View);

                var taskIdFromQueue = await coreWorkerTasksQueueProvider.CaptureTaskFromQueueAsync(captureTaskRequestDto);

                return Ok(taskIdFromQueue);
            }

            catch (Exception ex)
            {
                return PreconditionFailed<Guid?>(
                    $"Ошибка получения ID задачи из очереди для воркера {captureTaskRequestDto.CoreWorkerId}. Причина {ex.Message}");
            }
        }

        /// <summary>
        /// Отменить запущенную задачу
        /// </summary>
        /// <param name="id">Id запущенной задачи</param>
        /// <param name="reasonCancellation">Причина отмены задачи</param>
        public async Task<ManagerResult> CancelCoreWorkerTasksQueue(Guid id, string reasonCancellation)
        {
            var logRecordDescription = string.Empty;
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTasksQueue_Cancel);

                await coreWorkerTasksQueueProvider.CancelCoreWorkerTasksQueue(id, reasonCancellation);

                logRecordDescription = GetCancelTaskInQueueLogRecordDescription(id, reasonCancellation);
                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CancelCoreWorkerTasksQueue, logRecordDescription);
                return Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"{logRecordDescription} завершено с ошибкой. Причина: {ex.Message}";

                LogEvent(() => AccessProvider.ContextAccountId, LogActions.CancelCoreWorkerTasksQueue, errorMessage);

                return PreconditionFailed<Guid?>(errorMessage);
            }
        }

        /// <summary>
        /// Получить сообщение для записи в лог при отмене запущенной задачи
        /// </summary>
        /// <param name="taskInQueueId">Id запущенной задачи</param>
        /// <param name="reasonCancellation">Причина отмены задачи</param>
        /// <returns>Сообщение для записи в лог при отмене запущенной задачи</returns>
        private string GetCancelTaskInQueueLogRecordDescription(Guid taskInQueueId, string reasonCancellation)
        {
            var coreWorkerTaskInQueue = coreWorkerTasksQueueProvider.GetTasksInQueue(taskInQueueId);
            if (coreWorkerTaskInQueue == null)
                return $"Завершение запущенной задачи \"{taskInQueueId}\"";

            return
                $"Завершение запущенной задачи \"{coreWorkerTaskInQueue.CoreWorkerTask.TaskName}\". " +
                $"Дата создания: {coreWorkerTaskInQueue.CreateDate:dd.MM.yyyy HH:mm:ss}. " +
                $"Причина отмены: \"{reasonCancellation}\"";
        }
    }
}
