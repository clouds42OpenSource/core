﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Managers
{
    /// <summary>
    /// Менеджер для получения данных по воркерам
    /// </summary>
    public class CoreWorkersDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICoreWorkersDataProvider coreWorkersDataProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), ICoreWorkersDataManager
    {
        /// <summary>
        /// Получить весь список воркеров
        /// </summary>
        /// <returns>Весь список воркеров</returns>
        public ManagerResult<List<CoreWorkerDescriptionDto>> GetAllWorkers()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                return Ok(coreWorkersDataProvider.GetAllWorkers());
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения списка воркеров в системе. Причина: {ex.Message}";
                handlerException.Handle(ex, "[Ошибка получения списка воркеров в системе].");
                return PreconditionFailed<List<CoreWorkerDescriptionDto>>(errorMessage);
            }
        }

        /// <summary>
        /// Получить настрйоки воркера
        /// </summary>
        public ManagerResult<CoreWorkerSettingsDto> GetWorkerConfiguration()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkers_View);
                logger.Info("Получаем креды для воркера...");
                return Ok(coreWorkersDataProvider.GetWorkerConfiguration());
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения настроек воркера. Причина: {ex.Message}";
                handlerException.Handle(ex, "[Ошибка получения настроек воркера]");
                return PreconditionFailed<CoreWorkerSettingsDto>(errorMessage);
            }
        }
    }
}
