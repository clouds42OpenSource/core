﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Managers
{
    /// <summary>
    /// Менеджер данных очереди задачи воркера
    /// </summary>
    public class CoreWorkerTasksQueueDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICoreWorkerTasksQueueDataProvider coreWorkerTasksQueueDataProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), ICoreWorkerTasksQueueDataManager
    {
        /// <summary>
        /// Получить данные по очереди задач вокреров
        /// </summary>
        /// <param name="args">Фильтр поиска</param>
        /// <returns>Данные по очереди задач вокреров</returns>
        public ManagerResult<CoreWorkerTasksInQueueDataDto> GetCoreWorkerTasksQueueData(CoreWorkerTasksQueueFilterParamsDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                return Ok(coreWorkerTasksQueueDataProvider.GetCoreWorkerTasksQueueData(args));
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения списка задач из очереди воркеров. Причина: {ex.Message}";
                logger.Warn(errorMessage);
                return PreconditionFailed<CoreWorkerTasksInQueueDataDto>(errorMessage);
            }
        }

        /// <summary>
        /// Получить информацию по задаче из очереди задач воркера
        /// </summary>
        /// <param name="taskInQueueItemId">ID задачи из очереди</param>
        /// <returns>Информация по задаче из очереди задач воркера</returns>
        public ManagerResult<TaskInQueueItemInfoDto?> GetTaskInQueueItemInfo(Guid taskInQueueItemId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                return Ok(coreWorkerTasksQueueDataProvider.GetTaskInQueueItemInfo(taskInQueueItemId));
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения информации по задаче из очереди задач воркера. Причина: {ex.Message}";
                logger.Warn(errorMessage);
                return PreconditionFailed<TaskInQueueItemInfoDto?>(errorMessage);
            }
        }
    }
}
