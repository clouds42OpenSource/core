﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasks;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Managers
{
    /// <summary>
    /// Менеджер для работы с задачами воркера
    /// </summary>
    public class CoreWorkerTaskManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        ICoreWorkerTaskDataProvider coreWorkerTaskDataProvider,
        IEditCoreWorkerTaskProvider editCoreWorkerTaskProvider,
        ISearchCoreWorkerTaskProvider searchCoreWorkerTaskProvider,
        ICoreWorkerAvailableTasksProvider coreWorkerAvailableTasksProvider,
        ICoreWorkerTasksBagsPriorityProvider coreWorkerTasksBagsPriorityProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), ICoreWorkerTaskManager
    {

        /// <summary>
        /// Получить задачи воркера
        /// </summary>
        /// <param name="args">Модель фильтра задач воркера</param>
        /// <returns>Задачи воркера</returns>
        public ManagerResult<CoreWorkerTasksControlDataDto> GetCoreWorkerTasks(
            CoreWorkerTaskFilterDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTask_GetAll);
                var data = coreWorkerTaskDataProvider.GetCoreWorkerTasks(args);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<CoreWorkerTasksControlDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные по доступным задачам воркеров
        /// </summary>
        /// <param name="args">Фильтр для поиска записей</param>
        /// <returns>Данные по доступным задачам воркеров</returns>
        public ManagerResult<CoreWorkersAvailableTasksBagsDataDto> GetCoreWorkerAvailableTasksBag(
            CoreWorkersAvailableTasksBagsFilterDto args)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);
                return Ok(coreWorkerTaskDataProvider.GetCoreWorkerAvailableTasksBag(args));
            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения данных по доступным задачам воркеров. Причина: {ex.Message}";
                handlerException.Handle(ex, "[Ошибка получения данных по доступным задачам воркеров]");
                return PreconditionFailed<CoreWorkersAvailableTasksBagsDataDto>(errorMessage);
            }
        }

        /// <summary>
        /// Выполнить редактирование задачи воркера
        /// </summary>
        /// <param name="model">Модель редактирования задачи воркера</param>
        public ManagerResult EditCoreWorkerTask(EditCoreWorkerTaskDto model)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTask_Edit);
                editCoreWorkerTaskProvider.Edit(model);

                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить весь список задач воркера
        /// </summary>
        /// <returns>Весь список задач воркера</returns>
        public ManagerResult<List<KeyValuePair<Guid, string>>> GetAllCoreWorkerTasks()
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTask_Search);
                var data = searchCoreWorkerTaskProvider.GetAllCoreWorkerTasks();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<List<KeyValuePair<Guid, string>>>(ex.Message);
            }
        }

        /// <summary>
        /// Получить список доступных задач воркера
        /// </summary>
        /// <param name="coreWorkerId">ID воркера</param>
        /// <returns>Список доступных задач воркера</returns>
        public ManagerResult<CoreWorkerTasksBagsDataDto> GetWorkerAvailableTasksBags(short coreWorkerId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTask_Edit);
                return Ok(coreWorkerAvailableTasksProvider.GetWorkerAvailableTasksBags(coreWorkerId));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<CoreWorkerTasksBagsDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Изменить доступные задачи воркера
        /// </summary>
        /// <param name="changeWorkerAvailableTasksBags">Модель изменения доступных задач воркера</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult ChangeWorkerAvailableTasksBags(ChangeWorkerAvailableTasksBagsDto changeWorkerAvailableTasksBags)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTask_Edit);
                coreWorkerAvailableTasksProvider.ChangeWorkerAvailableTasksBags(changeWorkerAvailableTasksBags);
                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить приоритет доступной задачи воркера
        /// </summary>
        /// <param name="workerId">ID воркера</param>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Приоритет доступной задачи воркера</returns>
        public ManagerResult<CoreWorkerTaskBagPriorityDataDto> GetWorkerTaskBagPriority(short workerId, Guid taskId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTask_Edit);
                return Ok(coreWorkerTasksBagsPriorityProvider.GetWorkerTaskBagPriority(workerId, taskId));
            }
            catch (Exception ex)
            {
                return PreconditionFailed<CoreWorkerTaskBagPriorityDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Сменить приоритет доступной задачи воркера
        /// </summary>
        /// <param name="changeCoreWorkerTaskBagPriority">Модель для изменения приоритета доступной задачи воркера</param>
        /// <returns>Результат изменения</returns>
        public ManagerResult ChangeWorkerTaskBagPriority(ChangeCoreWorkerTaskBagPriorityDto changeCoreWorkerTaskBagPriority)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.CoreWorkerTask_Edit);
                coreWorkerTasksBagsPriorityProvider.ChangeWorkerTaskBagPriority(changeCoreWorkerTaskBagPriority);
                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed<CoreWorkerTaskBagPriorityDataDto>(ex.Message);
            }
        }
    }
}
