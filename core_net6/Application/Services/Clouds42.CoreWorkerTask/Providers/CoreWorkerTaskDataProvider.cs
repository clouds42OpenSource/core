﻿using Clouds42.Common.Exceptions;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasks;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using PagedList.Core;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер для работы с данными задач воркера
    /// </summary>
    internal class CoreWorkerTaskDataProvider(IUnitOfWork dbLayer) : ICoreWorkerTaskDataProvider
    {
        /// <summary>
        /// Получить задачи воркера
        /// </summary>
        /// <param name="filter">Модель фильтра задач воркера</param>
        /// <returns>Задачи воркера</returns>
        public CoreWorkerTasksControlDataDto GetCoreWorkerTasks(CoreWorkerTaskFilterDto filter)
        {
            const int pageSize = 10;
            var taskId = filter.Filter?.TaskId;
            var coreWorkerTasksData = (
                from coreWorkerTask in dbLayer.CoreWorkerTaskRepository.WhereLazy()
                where taskId == null || coreWorkerTask.ID == taskId
                select new CoreWorkerTaskDataDto
                {
                    Id = coreWorkerTask.ID,
                    Name = coreWorkerTask.TaskName,
                    TaskExecutionLifetimeInMinutes = coreWorkerTask.TaskExecutionLifetimeInMinutes
                }
            ).OrderBy(crt => crt.Name);

            return new CoreWorkerTasksControlDataDto
            {
                Records = coreWorkerTasksData.ToPagedList(filter.PageNumber ?? 1, pageSize).ToArray(),
                Pagination = new PaginationBaseDto(filter.PageNumber ?? 1, coreWorkerTasksData.Count(), pageSize)
            };
        }

        /// <summary>
        /// Получить данные по доступным задачам воркеров
        /// </summary>
        /// <param name="args">Фильтр для поиска записей</param>
        /// <returns>Данные по доступным задачам воркеров</returns>
        public CoreWorkersAvailableTasksBagsDataDto GetCoreWorkerAvailableTasksBag(
            CoreWorkersAvailableTasksBagsFilterDto args)
        {
            const int itemsPerPage = 50;
            var filter = args.Filter;

            var workerId = filter?.WorkerId;
            var taskId = filter?.TaskId;
            var taskPriority = filter?.TaskPriority;

            var data =
            (
                from availableTaskBag in dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>().WhereLazy()
                where
                    (workerId == null || availableTaskBag.CoreWorkerId == workerId) &&
                    (taskId == null || availableTaskBag.CoreWorkerTaskId == taskId) &&
                    (taskPriority == null || availableTaskBag.Priority == taskPriority)
                select new CoreWorkerAvailableTaskBagItemDto
                {
                    WorkerId = availableTaskBag.CoreWorkerId,
                    WorkerName = availableTaskBag.CoreWorker.CoreWorkerAddress,
                    TaskId = availableTaskBag.CoreWorkerTaskId,
                    TaskName = availableTaskBag.CoreWorkerTask.TaskName,
                    TaskPriority = availableTaskBag.Priority
                }
            ).OrderByDescending(taskInQueue => taskInQueue.WorkerId);

            return new CoreWorkersAvailableTasksBagsDataDto
            {
                Records = data.MakeSorting(args.SortingData).ToPagedList(args.PageNumber ?? 1, itemsPerPage).ToArray(),
                Pagination = new PaginationBaseDto(args.PageNumber ?? 1, data.Count(), itemsPerPage)
            };
        }

        /// <summary>
        /// Получить задачу воркера
        /// </summary>
        /// <param name="id">Id задачи воркера</param>
        /// <returns>Задача воркера</returns>
        public Domain.DataModels.CoreWorkerTask GetCoreWorkerTask(Guid id) =>
            dbLayer.CoreWorkerTaskRepository.FirstOrDefault(cwt => cwt.ID == id) ??
            throw new NotFoundException($"Не удалось получить задачу воркера по Id '{id}'");
    }
}
