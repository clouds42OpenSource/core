﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using System.Linq.Expressions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер поиска задач воркера 
    /// </summary>
    internal class SearchCoreWorkerTaskProvider(IUnitOfWork dbLayer) : ISearchCoreWorkerTaskProvider
    {
        /// <summary>
        /// Получить весь список задач воркера
        /// </summary>
        /// <returns>Весь список задач воркера</returns>
        public List<KeyValuePair<Guid, string>> GetAllCoreWorkerTasks()
            => ExecuteGetTasksQuery();

        /// <summary>
        /// Выполнить запрос на получение задач воркера
        /// </summary>
        /// <param name="filterFunc">Функция фильтра записей</param>
        /// <returns>Список задач воркера</returns>
        private List<KeyValuePair<Guid, string>> ExecuteGetTasksQuery(Expression<Func<Domain.DataModels.CoreWorkerTask,
            bool>>? filterFunc = null)
        {
            var coreWorkerTasks = filterFunc == null
                ? dbLayer.CoreWorkerTaskRepository.WhereLazy()
                : dbLayer.CoreWorkerTaskRepository.WhereLazy(filterFunc);

            return coreWorkerTasks
                .Select(wt => new
                {
                    Key = wt.ID,
                    Value = wt.TaskName
                }).AsEnumerable()
                .Select(item => new KeyValuePair<Guid, string>(item.Key, item.Value))
                .ToList();
        }
    }
}
