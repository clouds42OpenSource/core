﻿using Clouds42.Common.Extensions;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using PagedList.Core;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер данных очереди задачи воркера
    /// </summary>
    internal class CoreWorkerTasksQueueDataProvider(IUnitOfWork dbLayer) : ICoreWorkerTasksQueueDataProvider
    {
        /// <summary>
        /// Получить информацию по задаче из очереди задач воркера
        /// </summary>
        /// <param name="taskInQueueItemId">ID задачи из очереди</param>
        /// <returns>Информация по задаче из очереди задач воркера</returns>
        public TaskInQueueItemInfoDto? GetTaskInQueueItemInfo(Guid taskInQueueItemId)
        {
            return dbLayer.CoreWorkerTasksQueueRepository
                .AsQueryableNoTracking()
                .Include(x => x.CoreWorkerTask)
                .ThenInclude(x => x.CoreWorkerAvailableTasksBags)
                .Include(x => x.TaskParameter)
                .Where(x => x.Id == taskInQueueItemId)
                .Select(x => new TaskInQueueItemInfoDto
                {
                    TaskName = x.CoreWorkerTask.TaskName,
                    TaskParams = x.TaskParameter.TaskParams,
                    Comment = x.Comment,
                    StartDateTime = x.StartDate,
                    TaskType = x.CoreWorkerTask.WorkerTaskType,
                    Priority = x.CoreWorkerTask.CoreWorkerAvailableTasksBags.FirstOrDefault(y => y.CoreWorkerId == (x.CapturedWorkerId ?? -1)) != null ? x.CoreWorkerTask.CoreWorkerAvailableTasksBags.FirstOrDefault(y => y.CoreWorkerId == (x.CapturedWorkerId ?? -1))!.Priority : null,
                    Status = x.Status,
                    TaskInQueueItemId = x.Id
                })
                .FirstOrDefault();
        }

        /// <summary>
        /// Получить данные по очереди задач вокреров
        /// </summary>
        /// <param name="args">Фильтр поиска</param>
        /// <returns>Данные по очереди задач вокреров</returns>
        public CoreWorkerTasksInQueueDataDto GetCoreWorkerTasksQueueData(CoreWorkerTasksQueueFilterParamsDto args)
        {
            try
            {
                const int itemsPerPage = 50;
                var data = GetCoreWorkerTasksQueueDataBy(args.Filter);
                return new CoreWorkerTasksInQueueDataDto
                {
                    Records = data.MakeSorting(args.SortingData).ToPagedList(args.PageNumber ?? 1, itemsPerPage).ToArray(),
                    Pagination = new PaginationBaseDto(args.PageNumber ?? 1, data.Count(), itemsPerPage)
                };
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    $"При получении данных по очереди задач воркера произошла ошибка. Причина: {ex.GetFullInfo(false)}");
            }
        }

        /// <summary>
        /// Получить данные из очереди задач по фильтру
        /// </summary>
        /// <param name="filter">Фильтр поиска</param>
        /// <returns>Данные из очереди задач по фильтру</returns>   
        public IQueryable<CoreWorkerTaskInQueueItemDto> GetCoreWorkerTasksQueueDataBy(
            CoreWorkerTasksQueueFilterDto? filter)
        {
            var coreWorkerTaskInQueueStatus = filter is { Status: not null }
                ? filter.Status.Value.ToString()
                : "";

            var statusFilter = string.IsNullOrEmpty(coreWorkerTaskInQueueStatus) ? default : coreWorkerTaskInQueueStatus;
            var searchString = string.IsNullOrEmpty(filter?.SearchString) ? default : filter.SearchString;
            var workerIdFilter = filter?.WorkerId;
            var taskIdFilter = filter?.TaskId;
            var periodFromFilter = filter?.PeriodFrom;
            var periodToFilter = filter?.PeriodTo;


            return (
                from taskInQueue in dbLayer.CoreWorkerTasksQueueRepository.WhereLazy()
                join workerTask in dbLayer.CoreWorkerTaskRepository.WhereLazy() on taskInQueue.CoreWorkerTaskId equals workerTask.ID
                join taskParam in dbLayer.GetGenericRepository<CoreWorkerTaskParameter>().WhereLazy() on taskInQueue.Id equals taskParam.TaskId
                into taskParams
                from taskParam in taskParams.Take(1).DefaultIfEmpty()
                where
                    (statusFilter == null || taskInQueue.Status == statusFilter) &&
                    (searchString == null || taskParam.TaskParams.Contains(searchString)) &&
                    (!workerIdFilter.HasValue || taskInQueue.CapturedWorkerId == workerIdFilter) &&
                    (!taskIdFilter.HasValue || taskInQueue.CoreWorkerTaskId == taskIdFilter) &&
                    (!periodFromFilter.HasValue || taskInQueue.CreateDate >= periodFromFilter) &&
                    (!periodToFilter.HasValue || taskInQueue.CreateDate <= periodToFilter)
                select new CoreWorkerTaskInQueueItemDto
                {
                    Id = taskInQueue.Id,
                    Name = workerTask.TaskName,
                    Status = taskInQueue.Status,
                    WorkerId = taskInQueue.CapturedWorkerId,
                    FinishDateTime = taskInQueue.EditDate,
                    CreateDateTime = taskInQueue.CreateDate,
                    Comment = taskInQueue.Comment
                }
            ).OrderByDescending(taskInQueue => taskInQueue.CreateDateTime).AsQueryable();
        }
    }
}
