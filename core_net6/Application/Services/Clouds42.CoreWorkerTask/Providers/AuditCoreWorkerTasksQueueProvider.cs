﻿using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.CoreWorker;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер проверки очереди задач воркеров
    /// </summary>
    internal class AuditCoreWorkerTasksQueueProvider(
        IUnitOfWork dbLayer,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILogger42 logger)
        : IAuditCoreWorkerTasksQueueProvider
    {
        private readonly Lazy<Guid> _systemAccountId =
            new(CloudConfigurationProvider.Common.SystemAccount.GetSystemAccountId);
        private readonly Lazy<string> _coreExceptionMail = new(CloudConfigurationProvider.Emails.GetCoreExceptionEmail);
        private readonly Lazy<int> _maxAllowableTasksInQueueCount =
            new(CloudConfigurationProvider.CoreWorker.GetMaxAllowableTasksInQueueCount);

        /// <summary>
        /// Выполнить проверку очереди задач воркеров
        /// </summary>
        public void AuditTasksQueue()
        {
            var jobsWithStatusNewCount = GetJobsWithStatusNewCount();
            var coreWorkersTasksQueueData = GetCoreWorkersTasksQueueData().ToList();
            if (!NeedSendReport(jobsWithStatusNewCount, coreWorkersTasksQueueData))
            {
                logger.Trace($"Количество задач в очереди со статусом '{CloudTaskQueueStatus.New.Description()}' - {jobsWithStatusNewCount}");
                return;
            }
            letterNotificationProcessor
                .TryNotify<AuditCoreWorkerTasksQueueLetterNotification, AuditCoreWorkerTasksQueueLetterModelDto>(
                    CreateLetterModel(jobsWithStatusNewCount, coreWorkersTasksQueueData));
        }

        /// <summary>
        /// Создать модель данных для письма
        /// </summary>
        /// <param name="jobsWithStatusNewCount">Количество задач в очереди со статусом "Новая"</param>
        /// <param name="coreWorkersTasksQueueData">Данные по очереди задач для каждого воркера</param>
        /// <returns>Модель данных для письма</returns>
        private AuditCoreWorkerTasksQueueLetterModelDto CreateLetterModel(int jobsWithStatusNewCount,
            IEnumerable<AuditCoreWorkerTasksQueueResultDto> coreWorkersTasksQueueData)
            => new()
            {
                AccountId = _systemAccountId.Value,
                AuditCoreWorkerTasksQueueResults = coreWorkersTasksQueueData,
                JobsWithStatusNewCount = jobsWithStatusNewCount,
                ReceiverEmail = _coreExceptionMail.Value
            };

        /// <summary>
        /// Признак необходимости отправлять отчет по аудиту
        /// </summary>
        /// <param name="jobsWithStatusNewCount">Количество задач в очереди со статусом "Новая"</param>
        /// <param name="coreWorkersTasksQueueData">Данные по очереди задач для каждого воркера</param>
        /// <returns>
        /// true - если количество новых задач или
        /// задач в очереди хотя бы одного воркера больше заданного значения
        /// </returns>
        private bool NeedSendReport(int jobsWithStatusNewCount,
            IEnumerable<AuditCoreWorkerTasksQueueResultDto> coreWorkersTasksQueueData)
            => jobsWithStatusNewCount > _maxAllowableTasksInQueueCount.Value ||
               coreWorkersTasksQueueData.Any(queueData =>
                   queueData.TasksInQueueCount > _maxAllowableTasksInQueueCount.Value);

        /// <summary>
        /// Получить количество задач в очереди со статусом "Новая"
        /// </summary>
        /// <returns>Количество задач в очереди со статусом "Новая"</returns>
        private int GetJobsWithStatusNewCount()
        {
            var newQueueStatus = CloudTaskQueueStatus.New.ToString();
            return dbLayer.CoreWorkerTasksQueueRepository
                .WhereLazy(taskQueue =>
                    taskQueue.Status == newQueueStatus &&
                    (taskQueue.DateTimeDelayOperation == null || taskQueue.DateTimeDelayOperation < DateTime.Now))
                .Count();
        }

        /// <summary>
        /// Получить данные по очереди задач для каждого воркера
        /// </summary>
        /// <returns>Данные по очереди задач для каждого воркера</returns>
        private IEnumerable<AuditCoreWorkerTasksQueueResultDto> GetCoreWorkersTasksQueueData()
        {
            var taskQueueStatuses = new List<string>
            {
                CloudTaskQueueStatus.Captured.ToString(),
                CloudTaskQueueStatus.Processing.ToString(),
                CloudTaskQueueStatus.NeedRetry.ToString()
            };
            return
                (from worker in dbLayer.CoreWorkerRepository.WhereLazy()
                 join queue in dbLayer.CoreWorkerTasksQueueRepository.WhereLazy(queue => taskQueueStatuses.Contains(queue.Status)
                     && (queue.DateTimeDelayOperation == null || queue.DateTimeDelayOperation < DateTime.Now)) on
                     worker.CoreWorkerId equals queue.CapturedWorkerId
                 into tasksQueue
                 where worker.Status == CoreWorkerStatus.ON
                 select new AuditCoreWorkerTasksQueueResultDto
                 {
                     CoreWorkerId = worker.CoreWorkerId,
                     CoreWorkerName = worker.CoreWorkerAddress,
                     TasksInQueueCount = tasksQueue.Count()
                 }).AsEnumerable();
        }
    }
}