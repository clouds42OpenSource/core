﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер очереди задач воркера
    /// </summary>
    public class CoreWorkerTasksQueueProvider(
        ILogger42 logger,
        IDbObjectLockProvider dbObjectLockProvider,
        IServiceProvider serviceProvider,
        IUnitOfWork unitOfWork)
        : ICoreWorkerTasksQueueProvider
    {

        /// <summary>
        /// Получить задачу в очереди
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Модель задачи</returns>
        public CoreWorkerTasksQueue? GetTasksInQueue(Guid taskId)
            => unitOfWork.CoreWorkerTasksQueueRepository.AsQueryable().Include(x => x.CoreWorkerTask).FirstOrDefault(tasksQueue => tasksQueue.Id == taskId);

        /// <summary>
        /// Получить список ID задач
        /// </summary>
        /// <param name="status">Статус задач</param>
        /// <param name="maxRecordsCount">Максимально количество записей</param>
        /// <returns>Список ID задач</returns>
        public IEnumerable<CoreWorkerTasksQueue> GetTasksQueuesId(string status, int maxRecordsCount)
            => unitOfWork.CoreWorkerTasksQueueRepository.Where(x => x.Status == status).Take(maxRecordsCount);

        /// <summary>
        /// Установить комментарий для задачи
        /// </summary>
        /// <param name="taskInfo">Модель параметров задачи</param>
        /// <returns>Результат выполнения</returns>
        public bool SetComment(CoreWorkerTasksQueueSetCommentDto taskInfo)
        {
            var task = GetTasksInQueue(taskInfo.Id);
            if (task == null)
                return false;

            task.Comment = taskInfo.Comment;

            unitOfWork.CoreWorkerTasksQueueRepository.Update(task);
            unitOfWork.Save();

            return true;
        }

        /// <summary>
        /// Захватить задачу из очереди
        /// </summary>
        /// <param name="captureTaskRequestDto">Параметры для захвата задачи из очереди</param>
        /// <returns>ID задачи из очереди</returns>
        public async Task<Guid?> CaptureTaskFromQueueAsync(CaptureTaskRequestDto captureTaskRequestDto)
        {
            using (await dbObjectLockProvider.AcquireLockAsync("CaptureTask"))
            {
                await using var scope = serviceProvider.CreateAsyncScope();
                var dbLayer = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                if (!await NeedGiveTaskToWorkerAsync(captureTaskRequestDto, dbLayer))
                    return null;

                try
                {
                    var nowDate = DateTime.Now;
                  

                    var capturedTaskId = await dbLayer.CoreWorkerTasksQueueLiveRepository
                        .AsQueryable()
                        .Join(dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>().AsQueryable(), z => new { z.CoreWorkerTaskId, captureTaskRequestDto.CoreWorkerId }, y => new { y.CoreWorkerTaskId, y.CoreWorkerId }, (coreWorkerTasksQueueLive, coreWorkerAvailableTasksBag) => new { coreWorkerTasksQueueLive, coreWorkerAvailableTasksBag })
                        .GroupJoin(dbLayer.CoreWorkerTasksQueueLiveRepository.AsQueryable(), z => z.coreWorkerTasksQueueLive.DependTaskId, x => x.CoreWorkerTasksQueueId, (arg1, lives) => new {live1 = arg1.coreWorkerTasksQueueLive, bag = arg1.coreWorkerAvailableTasksBag, lives})
                        .SelectMany(x => x.lives.DefaultIfEmpty(), (arg1, live2) => new {arg1.live1, arg1.bag, live2})
                        .GroupJoin(dbLayer.CoreWorkerTaskRepository.AsQueryable(), z => z.live2.CoreWorkerTaskId, x => x.ID, (arg1, tasks) => new {arg1.live1, arg1.bag, arg1.live2, tasks})
                        .SelectMany(x => x.tasks.DefaultIfEmpty(), (arg1, task) => new {arg1.live1, arg1.live2, arg1.bag, task})
                        .OrderBy(x => x.live1.Status == CloudTaskQueueStatus.NeedRetry ? 0 : 1)
                        .ThenBy(x => x.bag.Priority)
                        .ThenBy(x => x.live1.CreateDate)
                        .Where(x =>
                            x.bag.CoreWorkerId ==
                            captureTaskRequestDto.CoreWorkerId &&
                            (x.live1.Status ==
                             CloudTaskQueueStatus.New ||
                             x.live1.Status ==
                             CloudTaskQueueStatus.NeedRetry) &&
                            (x.live1
                                 .DateTimeDelayOperation == null ||
                             x.live1
                                 .DateTimeDelayOperation <= nowDate) &&
                             (x.live2 == null ||
                             (x.live2.StartDate.HasValue &&
                              x.task != null &&
                              x.task.TaskExecutionLifetimeInMinutes > 0 &&
                              x.live2.StartDate.Value.Minute - nowDate.Minute >=
                              x.task.TaskExecutionLifetimeInMinutes) ||
                              x.live2.Status == CloudTaskQueueStatus.Ready ||
                              x.live2.Status == CloudTaskQueueStatus.Error))
                        .Select(x => x.live1)
                        .FirstOrDefaultAsync();


                    if (capturedTaskId == null)
                    {
                        return null;
                    }

                    capturedTaskId.CapturedWorkerId = captureTaskRequestDto.CoreWorkerId;
                    capturedTaskId.EditDate = nowDate;
                    capturedTaskId.Status = CloudTaskQueueStatus.Captured;

                    dbLayer.CoreWorkerTasksQueueLiveRepository.Update(capturedTaskId);

                    var coreWorkerTasksQueue = dbLayer.CoreWorkerTasksQueueRepository
                        .AsQueryable()
                        .FirstOrDefault(x => x.Id == capturedTaskId.CoreWorkerTasksQueueId);

                    if (coreWorkerTasksQueue != null)
                    {
                        coreWorkerTasksQueue.EditDate = nowDate;
                        coreWorkerTasksQueue.Status = CloudTaskQueueStatus.Captured.ToString();
                        coreWorkerTasksQueue.CapturedWorkerId = captureTaskRequestDto.CoreWorkerId;

                        dbLayer.CoreWorkerTasksQueueRepository.Update(coreWorkerTasksQueue);
                    }

                    await dbLayer.SaveAsync();

                    return capturedTaskId.CoreWorkerTasksQueueId;
                }

                catch (Exception ex)
                {
                    logger.Error(ex, $"Ошибка захвата задачи воркером {captureTaskRequestDto.CoreWorkerId}");

                    return null;
                }
            }
        }

        public async Task UpdateTaskStatusAsync(UpdateTaskStatusRequestDto updateTaskStatusRequestDto)
        {
            try
            {
                using (await dbObjectLockProvider.AcquireLockAsync("UpdateTaskStatus"))
                {
                    await using var scope = serviceProvider.CreateAsyncScope();
                    var dbLayer = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                    var task = await dbLayer.CoreWorkerTasksQueueRepository
                        .AsQueryable()
                        .FirstOrDefaultAsync(x => x.Id == updateTaskStatusRequestDto.TaskId);

                    if (task != null)
                    {
                        if (!string.IsNullOrEmpty(updateTaskStatusRequestDto.Comment))
                            task.Comment = updateTaskStatusRequestDto.Comment;

                        task.EditDate = DateTime.Now;
                        task.Status = updateTaskStatusRequestDto.ExecutionResult.TaskQueueStatus.ToString();

                        if (updateTaskStatusRequestDto.ExecutionResult.TaskQueueStatus == CloudTaskQueueStatus.NeedRetry)
                            task.DateTimeDelayOperation =
                                DateTime.Now.AddSeconds(updateTaskStatusRequestDto.ExecutionResult.RetryJobParams.RetryDelayInSeconds);

                        dbLayer.CoreWorkerTasksQueueRepository.Update(task);

                        var liveTask = await dbLayer.CoreWorkerTasksQueueLiveRepository
                            .AsQueryable()
                            .FirstOrDefaultAsync(x => x.CoreWorkerTasksQueueId == task.Id);

                        if (liveTask != null)
                        {
                            liveTask.EditDate = task.EditDate;
                            liveTask.Status = task.QueueStatus;
                            liveTask.DateTimeDelayOperation = task.DateTimeDelayOperation;

                            dbLayer.CoreWorkerTasksQueueLiveRepository.Update(liveTask);
                        }

                        await dbLayer.SaveAsync();

                        await dbLayer.CoreWorkerTasksQueueLiveRepository.DeleteAllMultiTasksWhenCompleted(task.Id);

                        await dbLayer.SaveAsync();
                    }
                   
                }

            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка обновления статуса задачи или связанных задач");
            }
        }

        /// <summary>
        /// Отменить запущенную задачу
        /// </summary>
        /// <param name="id">Id запущенной задачи</param>
        /// <param name="reasonCancellation">Причина отмены задачи</param>
        public async Task CancelCoreWorkerTasksQueue(Guid id, string reasonCancellation)
        {
            unitOfWork.SetCommandTimeout(15);

            var task = GetTasksInQueue(id) ?? throw new NotFoundException($"Не удалось получить задачу по id='{id}'");

            task.Status = CloudTaskQueueStatus.Error.ToString();

            if (!string.IsNullOrEmpty(reasonCancellation))
                task.Comment = reasonCancellation;

            unitOfWork.CoreWorkerTasksQueueRepository.Update(task);

            SetErrorStatusForTaskQueueLive(id);

            await unitOfWork.SaveAsync();

            await DeleteCompletedTasksQueueLiveAsync(id);
        }


        /// <summary>
        /// Удалить Live задачу 
        /// </summary>
        /// <param name="coreWorkerTasksQueueId">ID задачи</param>
        private void SetErrorStatusForTaskQueueLive(Guid coreWorkerTasksQueueId)
        {
            var taskLive = unitOfWork.CoreWorkerTasksQueueLiveRepository.GetById(coreWorkerTasksQueueId);

            if (taskLive == null)
                return;

            taskLive.Status = CloudTaskQueueStatus.Error;

            unitOfWork.CoreWorkerTasksQueueLiveRepository.Update(taskLive);
        }

        /// <summary>
        /// Удалить задачи из активных задач если все заимосвязанные задачи являются завершёнными 
        /// </summary>
        /// <param name="coreWorkerTasksQueueId">Задача в очереди от которой найти все взаимосвязанные</param>
        private async Task DeleteCompletedTasksQueueLiveAsync(Guid coreWorkerTasksQueueId)
        {
            await unitOfWork.CoreWorkerTasksQueueLiveRepository.DeleteAllMultiTasksWhenCompleted(coreWorkerTasksQueueId);
            await unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Признак необходимости отдавать задачу воркеру
        /// </summary>
        /// <param name="captureTaskRequestDto">Параметры для захвата задачи из очереди</param>
        /// <param name="uow"></param>
        /// <returns>Признак необходимости отдавать задачу воркеру</returns>
        private async Task<bool> NeedGiveTaskToWorkerAsync(CaptureTaskRequestDto captureTaskRequestDto, IUnitOfWork uow)
        {
            var listInternalTask = captureTaskRequestDto.InternalTasksQueue.List.Any()
                ? captureTaskRequestDto.InternalTasksQueue.List
                : [Guid.Empty];

            var date = DateTime.Now.AddDays(CloudConfigurationProvider.CoreWorker.GetProcessingTaskStartPeriodInDays());

            return !await uow.CoreWorkerTasksQueueLiveRepository
                .AsQueryableNoTracking()
                .Include(x => x.CoreWorkerTask)
                .GroupJoin(uow.CoreWorkerTasksQueueLiveRepository
                        .AsQueryable()
                        .Where(x => listInternalTask.Contains(x.CoreWorkerTasksQueueId) &&
                                    x.CoreWorkerTask.TaskAllowsMultithreading == false), z => z.CoreWorkerTasksQueueId,
                    x => x.CoreWorkerTasksQueueId, (live, lives) => new { live, lives })
                .SelectMany(x => x.lives.DefaultIfEmpty(), (x, lives) => new { x.live, lives })
                .AnyAsync(x =>
                    ((x.live.Status == CloudTaskQueueStatus.Captured ||
                      x.live.Status == CloudTaskQueueStatus.Processing)
                     && x.live.CapturedWorkerId == captureTaskRequestDto.CoreWorkerId
                     && x.live.CoreWorkerTask.TaskAllowsMultithreading == false
                     && x.live.CreateDate > date) || x.lives != null);
        }
    }
}
