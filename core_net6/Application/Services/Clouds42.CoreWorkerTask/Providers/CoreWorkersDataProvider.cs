﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер для получения данных по воркерам
    /// </summary>
    internal class CoreWorkersDataProvider(
        IUnitOfWork dbLayer,
        IConfiguration configuration) : ICoreWorkersDataProvider
    {
        /// <summary>
        /// Получить весь список воркеров
        /// </summary>
        /// <returns>Весь список воркеров</returns>
        public List<CoreWorkerDescriptionDto> GetAllWorkers()
            => (
                from worker in dbLayer.CoreWorkerRepository
                select new CoreWorkerDescriptionDto
                {
                    Id = worker.CoreWorkerId,
                    Name = worker.CoreWorkerAddress
                }
            ).OrderBy(worker => worker.Id).ToList();

        /// <summary>
        /// Получить настройки воркера
        /// </summary>
        /// <returns></returns>
        public CoreWorkerSettingsDto GetWorkerConfiguration()
        {
            var configuration1 = new CoreWorkerSettingsDto();

            configuration1.Secrets.Add("ElasticUri", configuration["Elastic:Uri"]);
            configuration1.Secrets.Add("ElasticUserName", configuration["Elastic:UserName"]);
            configuration1.Secrets.Add("ElasticPassword", configuration["Elastic:Password"]);
            configuration1.Secrets.Add("ElasticCoreWorkerIndex", configuration["Elastic:CoreWorkerIndex"]);
            configuration1.Secrets.Add("AppName", configuration["Logs:AppName"]);
            configuration1.Secrets.Add("TelegramBot:ChatId", configuration["TelegramBot:ChatId"]);
            configuration1.Secrets.Add("TelegramBot:ApiKey", configuration["TelegramBot:ApiKey"]);

            return configuration1;
        }
    }
}
