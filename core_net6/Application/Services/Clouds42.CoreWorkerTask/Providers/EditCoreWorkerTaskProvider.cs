﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasks;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер для редактирования задачи воркера
    /// </summary>
    internal class EditCoreWorkerTaskProvider(
        IUnitOfWork dbLayer,
        ICoreWorkerTaskDataProvider coreWorkerTaskDataProvider)
        : IEditCoreWorkerTaskProvider
    {
        /// <summary>
        /// Выполнить редактирование задачи воркера
        /// </summary>
        /// <param name="model">Модель редактирования задачи воркера</param>
        public void Edit(EditCoreWorkerTaskDto model)
        {
            var task = coreWorkerTaskDataProvider.GetCoreWorkerTask(model.Id);
            task.TaskExecutionLifetimeInMinutes = model.TaskExecutionLifetimeInMinutes;

            dbLayer.CoreWorkerTaskRepository.Update(task);
            dbLayer.Save();
        }
    }
}