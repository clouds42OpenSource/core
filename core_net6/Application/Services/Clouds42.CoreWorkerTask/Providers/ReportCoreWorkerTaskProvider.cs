﻿using System.Text;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using Clouds42.TelegramBot.Interfaces;

namespace Clouds42.CoreWorkerTask.Providers
{
    public class ReportCoreWorkerTaskProvider : IReportCoreWorkerTaskProvider
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly ITelegramBot42 _telegramBot42;
        private readonly IHandlerException _handlerException;
        private readonly ILogger42 _logger;

        public ReportCoreWorkerTaskProvider(
            IUnitOfWork dbLayer,
            IEnumerable<ITelegramBot42> telegramBots42, 
            IHandlerException handlerException, 
            ILogger42 logger)
        {
            _dbLayer = dbLayer;
            _telegramBot42 = telegramBots42.FirstOrDefault(x => x.Type == BotType.Alert)!;
            _handlerException = handlerException;
            _logger = logger;
        }
         
        public void SendDailyReportToTelegram()
        {
            _logger.Info("Начинаю отправлять отчёты");
            try
            {
                RegularJobReport();

                DailyErrorJobReport();

                DailyStuckJobReport();

                IncidentReport();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка отправки ежедневного отчёта]");
            }
            
        }


        /// <summary>
        /// Отчёт по инцидентам
        /// </summary>
        public void IncidentReport()
        {
            var reportDate = DateTime.Now.AddHours(-24);
            var allDeilyIncidents = _dbLayer.IncidentRepository.Where(i => i.DateOfLastDiscover > reportDate).ToList();

            var reportBuilder = new StringBuilder().AppendLine("Отчёт по инцидентам:");

            if (!allDeilyIncidents.Any())
            {
                reportBuilder.AppendLine("Странно, инцидентов сегодня не было....\nОочень подозрительно \U0001F440");
                _telegramBot42.SendAlert(reportBuilder.ToString());
                return;
            }

            reportBuilder.AppendLine("Отчет по инцидентам за прошедшие сутки: \u2705");
            allDeilyIncidents.ForEach(i =>
            {
                reportBuilder.AppendLine($"|{i.Name}| \t |{i.CountOfIncedentToday}|");
            });

            _telegramBot42.SendAlert(reportBuilder.ToString());
        }

        /// <summary>
        /// Отчёт по регулярным задачам
        /// </summary>
        private void RegularJobReport()
        {
            var reportDate = DateTime.Now.AddHours(-24);

            var regular42CloudServiceProlongationJob = _dbLayer.CoreWorkerTasksQueueRepository
                .FirstOrDefault(t => t.CreateDate > reportDate && t.CoreWorkerTask.TaskName == "Regular42CloudServiceProlongationJob");

            if (regular42CloudServiceProlongationJob == null || regular42CloudServiceProlongationJob.Status != "Ready")
                _telegramBot42.SendAlert("‼Regular42CloudServiceProlongationJob неправильно отработала или не запускалась\U0000203C");


            var reportBuilder = new StringBuilder().AppendLine("Отчёт по регуляркам:");

            var dailyRegularJob = _dbLayer.CoreWorkerTasksQueueRepository
                .Where(t => t.CreateDate > reportDate && 
                (t.CoreWorkerTask.TaskName == "Regular42CloudServiceProlongationJob"
                || t.CoreWorkerTask.TaskName == "AccountDatabasesAuditAndChangeStateJob"
                || t.CoreWorkerTask.TaskName == "PlanSupportAccountDatabaseJob"
                || t.CoreWorkerTask.TaskName == "TehSupportAccountDatabasesJob"))
                .ToList();

            if (!dailyRegularJob.Any())
            {
                reportBuilder.AppendLine("Регулярок сегодня не было \U0001F440");
                _telegramBot42.SendAlert(reportBuilder.ToString());
                return;
            }

            dailyRegularJob.ForEach(t =>
            {
                reportBuilder.AppendLine($"|{t.CoreWorkerTask.TaskName}|\t|{t.Status}|");
            });

            _telegramBot42.SendAlert(reportBuilder.ToString());
        }

        /// <summary>
        /// Отчёт по задачам с ошибками
        /// </summary>
        private void DailyErrorJobReport()
        {
            var reportDate = DateTime.Now.AddHours(-24);

            var dailyErrorTasks = _dbLayer.CoreWorkerTasksQueueRepository
                .Where(t => t.Status == "Error" && t.CreateDate > reportDate)
                .GroupBy(t => t.CoreWorkerTask.TaskName)
                .Select(t => new
                {
                    taskName = t.Key,
                    count = t.Count()
                }).ToList();

            var reportBuilder = new StringBuilder().AppendLine("Отчёт по задачам с ошибкой:");
            if (!dailyErrorTasks.Any())
            {
                reportBuilder.AppendLine("Странно, сегодня нет задач заверешнных с ошибкой \nОочень подозрительно \U0001F440");
                _telegramBot42.SendAlert(reportBuilder.ToString());
                return;
            }

            reportBuilder.AppendLine("Отчет по таскам с ошибкой за прошедшие сутки: \u2705");
            dailyErrorTasks.ForEach(t =>
            {
                reportBuilder.AppendLine($"|{t.taskName}| \t |{t.count}| \n");
            });

            _telegramBot42.SendAlert(reportBuilder.ToString());
        }

        /// <summary>
        /// Отчёт по зависшим задачам
        /// </summary>
        private void DailyStuckJobReport()
        {
            var reportDate = DateTime.Now.AddHours(-36);

            var stuckTime = DateTime.Now.AddHours(-12);

            var dailyStuckTasks = _dbLayer.CoreWorkerTasksQueueRepository
                .Where(t => t.Status != "Error" && t.Status != "Ready" && t.Status != "New" && t.CreateDate > reportDate && t.CreateDate < stuckTime)
                .ToList();

            var reportBuilder = new StringBuilder().AppendLine("Отчёт по зависшим задачам:");
            if (!dailyStuckTasks.Any())
            {
                reportBuilder.AppendLine("Cегодня нет зависших задач. \U0001F440");
                _telegramBot42.SendAlert(reportBuilder.ToString());
                return;
            }

            reportBuilder.AppendLine("Отчет по зависшим задачам: \u23f0 \nЗависли:");
            dailyStuckTasks.ForEach(t =>
            {
                reportBuilder.AppendLine($"|{t.CoreWorkerTask.TaskName}|\t|{t.CreateDate}|\t|{t.Status}|\n");
            });

            _telegramBot42.SendAlert(reportBuilder.ToString());
        }
    }
}
