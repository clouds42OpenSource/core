﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.HandlerExeption.Contract;
using System.Linq.Expressions;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер для работы с доступными задачами воркера
    /// </summary>
    internal class CoreWorkerAvailableTasksProvider(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : ICoreWorkerAvailableTasksProvider
    {
        /// <summary>
        /// Получить список доступных задач воркера
        /// </summary>
        /// <param name="coreWorkerId">ID воркера</param>
        /// <returns>Список доступных задач воркера</returns>
        public CoreWorkerTasksBagsDataDto GetWorkerAvailableTasksBags(short coreWorkerId)
        {
            var coreWorker = GetCoreWorker(coreWorkerId);
            var data =
            (
                from coreWorkerTasks in dbLayer.CoreWorkerTaskRepository.WhereLazy()
                join availableTasksBag in dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>().WhereLazy()
                    on new { TaskId = coreWorkerTasks.ID, WorkerId = coreWorkerId }
                    equals new { TaskId = availableTasksBag.CoreWorkerTaskId, WorkerId = availableTasksBag.CoreWorkerId }
                    into tasksBags
                from availableTasksBag in tasksBags.DefaultIfEmpty()
                select new
                {
                    Id = coreWorkerTasks.ID,
                    Name = coreWorkerTasks.TaskName,
                    CanBeAdded = availableTasksBag == null
                }
            ).OrderBy(task => task.Name);

            return new CoreWorkerTasksBagsDataDto
            {
                WorkerId = coreWorker.CoreWorkerId,
                WorkerName = coreWorker.CoreWorkerAddress,
                AvailableTasksForAdd = GetTasksListByCriteria(data, task => task.CanBeAdded,
                    task => new KeyValuePair<Guid, string>(task.Id, task.Name)),
                WorkerTasksBags = GetTasksListByCriteria(data, task => !task.CanBeAdded,
                    task => new KeyValuePair<Guid, string>(task.Id, task.Name))
            };
        }

        /// <summary>
        /// Изменить доступные задачи воркера
        /// </summary>
        /// <param name="changeWorkerAvailableTasksBags">Модель изменения доступных задач воркера</param>
        public void ChangeWorkerAvailableTasksBags(ChangeWorkerAvailableTasksBagsDto changeWorkerAvailableTasksBags)
        {
            if (!changeWorkerAvailableTasksBags.WorkerTasksBagsIds.Any())
                throw new InvalidOperationException($"Не возможно удалить все доступные задачи воркера № {changeWorkerAvailableTasksBags.CoreWorkerId}");

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var coreWorkerId = changeWorkerAvailableTasksBags.CoreWorkerId;
                var workerAvailableTasksBags =
                    GetWorkerAvailableTasksBagsIds(coreWorkerId);
                var tasksForAdd = changeWorkerAvailableTasksBags.WorkerTasksBagsIds.Except(workerAvailableTasksBags)
                    .ToList();
                var tasksForRemove = workerAvailableTasksBags
                    .Except(changeWorkerAvailableTasksBags.WorkerTasksBagsIds).ToList();

                ProcessChangedTasksBags(tasksForRemove, coreWorkerId, true);
                ProcessChangedTasksBags(tasksForAdd, coreWorkerId, false);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка изменения доступных задач воркера] {changeWorkerAvailableTasksBags.CoreWorkerId}");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Получить список задач по признаку
        /// </summary>
        /// <typeparam name="TModel">Тип входящей модели</typeparam>
        /// <param name="data">Список данных</param>
        /// <param name="criteria">Критерий поиска</param>
        /// <param name="selector">Селектор записей</param>
        /// <returns>Список задач по признаку</returns>
        private static List<KeyValuePair<Guid, string>> GetTasksListByCriteria<TModel>(IQueryable<TModel> data,
            Expression<Func<TModel, bool>> criteria, Func<TModel, KeyValuePair<Guid, string>> selector)
            => data.Where(criteria).AsEnumerable().Select(selector).ToList();

        /// <summary>
        /// Получить воркер
        /// </summary>
        /// <param name="coreWorkerId">ID воркера</param>
        /// <returns>Модель воркера</returns>
        private CoreWorker GetCoreWorker(short coreWorkerId)
            => dbLayer.CoreWorkerRepository.FirstOrThrowException(worker => worker.CoreWorkerId == coreWorkerId,
                $"Воркер по ID {coreWorkerId} не найден.");

        /// <summary>
        /// Получить список ID доступных задач для воркера
        /// </summary>
        /// <param name="coreWorkerId">ID воркера</param>
        /// <returns>Список ID доступных задач для воркера</returns>
        private List<Guid> GetWorkerAvailableTasksBagsIds(short coreWorkerId)
            => dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>()
                .WhereLazy(task => task.CoreWorkerId == coreWorkerId).Select(task => task.CoreWorkerTaskId).ToList();

        /// <summary>
        /// Обработать измененнные доступные задачи
        /// </summary>
        /// <param name="tasksIds">Список ID доступных задач</param>
        /// <param name="coreWorkerId">ID воркера</param>
        /// <param name="isDeletion">Признак что задача удаляется из доступных</param>
        private void ProcessChangedTasksBags(List<Guid> tasksIds, short coreWorkerId, bool isDeletion)
        {
            tasksIds.ForEach(taskId =>
            {
                if (isDeletion)
                {
                    DeleteTasksBag(taskId, coreWorkerId);
                    return;
                }
                InsertTasksBag(taskId, coreWorkerId);
            });
        }

        /// <summary>
        /// Удалить доступную задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="coreWorkerId">ID воркера</param>
        private void DeleteTasksBag(Guid taskId, short coreWorkerId)
        {
            var tasksBag = dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>().FirstOrThrowException(
                taskBag => taskBag.CoreWorkerTaskId == taskId && taskBag.CoreWorkerId == coreWorkerId,
                $"Доступная задача по ID {taskId} для воркера {coreWorkerId} не найдена");

            dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>().Delete(tasksBag);
            dbLayer.Save();
        }

        /// <summary>
        /// Добавить доступную задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="coreWorkerId">ID воркера</param>
        private void InsertTasksBag(Guid taskId, short coreWorkerId)
        {
            var tasksBag = new CoreWorkerAvailableTasksBag
            {
                CoreWorkerId = coreWorkerId,
                CoreWorkerTaskId = taskId,
                Priority = GetLowestTaskPriorityForWorker(coreWorkerId)
            };

            dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>().Insert(tasksBag);
            dbLayer.Save();
        }

        /// <summary>
        /// Получить самый низкий приоритет для текущего воркера
        /// </summary>
        /// <param name="coreWorkerId">ID воркера</param>
        /// <returns>Самый низкий приоритет для текущего воркера</returns>
        private int GetLowestTaskPriorityForWorker(short coreWorkerId)
        {
            var taskBagWithLowestPriority = dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>()
                .Where(taskBag => taskBag.CoreWorkerId == coreWorkerId).OrderByDescending(taskBag => taskBag.Priority)
                .FirstOrDefault();

            return taskBagWithLowestPriority?.Priority + 1 ?? 0;
        }
    }
}
