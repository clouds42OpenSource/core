﻿using Clouds42.Common.Exceptions;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер для работы с приоритетом доступных задач воркера
    /// </summary>
    internal class CoreWorkerTasksBagsPriorityProvider(IUnitOfWork dbLayer, IHandlerException handlerException)
        : ICoreWorkerTasksBagsPriorityProvider
    {
        /// <summary>
        /// Получить приоритет доступной задачи воркера
        /// </summary>
        /// <param name="workerId">ID воркера</param>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Приоритет доступной задачи воркера</returns>
        public CoreWorkerTaskBagPriorityDataDto GetWorkerTaskBagPriority(short workerId, Guid taskId)
        {
            var workerTaskBagPriorityData =
            (
                from availableTasksBag in dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>().WhereLazy()
                where availableTasksBag.CoreWorkerId == workerId && availableTasksBag.CoreWorkerTaskId == taskId
                select new CoreWorkerTaskBagPriorityDataDto
                {
                    TaskId = availableTasksBag.CoreWorkerTaskId,
                    WorkerId = availableTasksBag.CoreWorkerId,
                    WorkerName = availableTasksBag.CoreWorker.CoreWorkerAddress,
                    TaskName = availableTasksBag.CoreWorkerTask.TaskName,
                    TaskPriority = availableTasksBag.Priority
                }
            ).FirstOrDefault();
            if (workerTaskBagPriorityData == null)
                throw new NotFoundException($"Доступная задача по ID {taskId} для воркера {workerId} не найдена");
            return workerTaskBagPriorityData;
        }

        /// <summary>
        /// Сменить приоритет доступной задачи воркера
        /// </summary>
        /// <param name="changeCoreWorkerTaskBagPriority">Модель для изменения приоритета доступной задачи воркера</param>
        public void ChangeWorkerTaskBagPriority(ChangeCoreWorkerTaskBagPriorityDto changeCoreWorkerTaskBagPriority)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var workerAvailableTasksBag = dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>()
                    .FirstOrThrowException(
                        taskBag => taskBag.CoreWorkerId == changeCoreWorkerTaskBagPriority.WorkerId &&
                                   taskBag.CoreWorkerTaskId == changeCoreWorkerTaskBagPriority.TaskId,
                        $"Доступная задача по ID {changeCoreWorkerTaskBagPriority.TaskId} для воркера {changeCoreWorkerTaskBagPriority.WorkerId} не найдена");

                workerAvailableTasksBag.Priority = changeCoreWorkerTaskBagPriority.TaskPriority;

                dbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>().Update(workerAvailableTasksBag);
                dbLayer.Save();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка смены приоритета задачи для воркера] задача: ${changeCoreWorkerTaskBagPriority.TaskId} воркер: {changeCoreWorkerTaskBagPriority.WorkerId}");
                transaction.Rollback();
                throw;
            }
        }
    }
}
