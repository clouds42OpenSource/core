﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorkerTask.Providers
{
    /// <summary>
    /// Провайдер регистрации задачи в очереди
    /// </summary>
    public class RegisterTaskInQueueProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger) : IRegisterTaskInQueueProvider
    {
        /// <inheritdoc/>
        public Guid RegisterTask(CoreWorkerTaskType taskType,
            ParametrizationModelDto? parametrizationModel = null,
            string? comment = null,
            DateTime? dateTimeDelayOperation = null,
            CloudTaskQueueStatus queueStatus = CloudTaskQueueStatus.New)
        {
            return RegisterTask(taskType.ToString(), parametrizationModel, comment, dateTimeDelayOperation, queueStatus);
        }

        /// <inheritdoc/>
        public Guid RegisterTask(string taskName,
            ParametrizationModelDto? parametrizationModel = null,
            string? comment = null,
            DateTime? dateTimeDelayOperation = null,
            CloudTaskQueueStatus queueStatus = CloudTaskQueueStatus.New)
        {
            var coreWorkerTask = dbLayer.CoreWorkerTaskRepository.FirstOrDefault(t => t.TaskName == taskName);
            if (coreWorkerTask == null)
                throw new NotFoundException($"Core worker task {taskName} is not found");
            return RegisterTaskAux(coreWorkerTask.ID, comment, dateTimeDelayOperation, parametrizationModel, queueStatus);
        }


        /// <inheritdoc/>
        public Guid RegisterTask(Guid coreWorkerTaskId, string comment, DateTime? dateTimeDelayOperation,
            ParametrizationModelDto parametrizationModel, CloudTaskQueueStatus queueStatus = CloudTaskQueueStatus.New)
        {
            var coreWorkerTask = dbLayer.CoreWorkerTaskRepository.FirstOrDefault(t => t.ID == coreWorkerTaskId);
            if (coreWorkerTask == null)
                throw new NotFoundException($"Core worker task {coreWorkerTaskId} is not found");

            return RegisterTaskAux(coreWorkerTaskId, comment, dateTimeDelayOperation, parametrizationModel, queueStatus);
        }

        private Guid RegisterTaskAux(Guid coreWorkerTaskId, string? comment, DateTime? dateTimeDelayOperation,
            ParametrizationModelDto? parametrizationModel, CloudTaskQueueStatus queueStatus)
        {
            try
            {
                var task = CreateCoreWorkerTasksQueueObject(coreWorkerTaskId, dateTimeDelayOperation, comment,
                    queueStatus);

                dbLayer.GetGenericRepository<CoreWorkerTasksQueue>().Insert(task);
                CoreWorkerTaskParameter? taskParam = null;
                if (parametrizationModel?.TaskParams != null)
                {
                    taskParam = CreateCoreWorkerTaskParameterObject(task.Id, parametrizationModel);
                    dbLayer.GetGenericRepository<CoreWorkerTaskParameter>().Insert(taskParam);
                }

                var taskLive = CreateCoreWorkerTasksQueueLiveObject(task, taskParam);
                dbLayer.CoreWorkerTasksQueueLiveRepository.Insert(taskLive);

                dbLayer.Save();

                return task.Id;
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка регистрации задачи на выполнение] {coreWorkerTaskId}");
                throw;
            }
        }

        /// <summary>
        /// Получить ID зависимой задачи
        /// </summary>
        /// <param name="parametrizationModel">Параметры задачи</param>
        /// <param name="currentTaskId">ID создаваемой задачи в очереди</param>
        /// <returns>ID зависимой задачи</returns>
        private Guid? GetDependTaskId(ParametrizationModelDto parametrizationModel, Guid currentTaskId)
        {
            if (string.IsNullOrEmpty(parametrizationModel.SynchronizationKey))
                return null;

            return dbLayer.GetGenericRepository<CoreWorkerTaskParameter>()
                .WhereLazy(taskParam =>
                    taskParam.SynchronizationKey == parametrizationModel.SynchronizationKey &&
                    taskParam.TaskId != currentTaskId &&
                    taskParam.Task.Status != CloudTaskQueueStatus.Error.ToString() &&
                    taskParam.Task.Status != CloudTaskQueueStatus.Ready.ToString())
                .OrderByDescending(t => t.Task.CreateDate)
                .FirstOrDefault()?.TaskId;
        }

        /// <summary>
        /// Создать объект задачи в очереди
        /// </summary>
        /// <param name="coreWorkerTaskId">ID задачи</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="dateTimeDelayOperation">Задержка выполнения</param>
        /// <param name="queueStatus">Статус задачи в очереди</param>
        /// <returns>Объект задачи в очереди</returns>
        private static CoreWorkerTasksQueue CreateCoreWorkerTasksQueueObject(Guid coreWorkerTaskId, DateTime? dateTimeDelayOperation, string? comment, CloudTaskQueueStatus queueStatus)
            => new()
            {
                Id = Guid.NewGuid(),
                CoreWorkerTaskId = coreWorkerTaskId,
                Comment = comment,
                CreateDate = DateTime.Now,
                Status = queueStatus.ToString(),
                DateTimeDelayOperation = dateTimeDelayOperation
            };


        /// <summary>
        /// Создать объект задачи в очереди на текущий момент
        /// </summary>
        /// <param name="coreWorkerTaskQueue">Модель задачи в очереди</param>
        /// <param name="taskParam">параметры задачи</param>
        /// <returns>Объект задачи в очереди в текущий момент</returns>
        private static CoreWorkerTasksQueueLive CreateCoreWorkerTasksQueueLiveObject(CoreWorkerTasksQueue coreWorkerTaskQueue,
            CoreWorkerTaskParameter? taskParam)
            => new()
            {
                CoreWorkerTasksQueueId = coreWorkerTaskQueue.Id,
                CoreWorkerTaskId = coreWorkerTaskQueue.CoreWorkerTaskId,
                CreateDate = coreWorkerTaskQueue.CreateDate,
                Status = coreWorkerTaskQueue.QueueStatus,
                DateTimeDelayOperation = coreWorkerTaskQueue.DateTimeDelayOperation,
                EditDate = coreWorkerTaskQueue.EditDate,
                StartDate = coreWorkerTaskQueue.StartDate,
                CapturedWorkerId = coreWorkerTaskQueue.CapturedWorkerId,

                DependTaskId = taskParam?.DependTaskId,
                TaskParams = taskParam?.TaskParams,
                SynchronizationKey = taskParam?.SynchronizationKey
            };

        /// <summary>
        /// Создать объект параметров задачи
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="parametrizationModel">Параметры</param>
        /// <returns>Объект параметров задачи</returns>
        private CoreWorkerTaskParameter CreateCoreWorkerTaskParameterObject(Guid taskId,
            ParametrizationModelDto parametrizationModel)
            => new()
            {
                TaskId = taskId,
                TaskParams = parametrizationModel.TaskParams.ToJson(),
                SynchronizationKey = parametrizationModel.SynchronizationKey,
                DependTaskId = GetDependTaskId(parametrizationModel, taskId)
            };
    }
}
