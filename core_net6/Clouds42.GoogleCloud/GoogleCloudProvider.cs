﻿using System.Net.Http.Headers;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Docs.v1;
using Google.Apis.Docs.v1.Data;
using Google.Apis.Download;
using Google.Apis.Drive.v2;
using Google.Apis.Drive.v2.Data;
using Google.Apis.Services;
using Google.Apis.Storage.v1;
using Google.Apis.Upload;
using Google.Cloud.Storage.V1;
using File = System.IO.File;
using Object = Google.Apis.Storage.v1.Data.Object;

namespace Clouds42.GoogleCloud
{
    public class GoogleCloudProvider(ILogger42 logger) : IGoogleCloudProvider
    {
        private readonly Guid _session = Guid.NewGuid();
        private readonly string _bucketName = CloudConfigurationProvider.Tomb.GetBucketName();
        private readonly StorageClient _client = StorageClient.Create(GoogleCredential.FromStream(File.OpenRead(CloudConfigurationProvider.Tomb.GetCredentialsPath())));

        /// <summary>
        /// Upload file to google cloud
        /// </summary>
        /// <param name="filePath">Path to local file</param>
        /// <param name="folderPrefix">Fully google cloud folders prefix</param>
        /// <param name="forceUpload">If file exists in cloud, rewrite it</param>
        /// <param name="databaseName">Info base name</param>
        /// <returns></returns>
        public string UploadFile(string filePath, string folderPrefix, bool forceUpload, string databaseName)
        {
            try
            {
                logger.Info($"[{_session}] :: Загрузка файла на Google cloud: FilePath={filePath}; FolderPrefix={folderPrefix}");

                var fileName = GetTitleDownloadFileName(Path.GetFileName(filePath), databaseName);
                var contentType = MimeTypes.GetMimeType(Path.GetExtension(filePath));
                var fullFilePath = $"{folderPrefix}/{fileName}";

                var objects = _client
                    .ListObjects(_bucketName, folderPrefix)
                    .ReadPage(100);

                var existsFile = objects.FirstOrDefault(w => w.Name == fileName);
                if (existsFile != null)
                {
                    logger.Info($"[{_session}] :: На Google cloud, в папке {folderPrefix} уже найден файл {existsFile.Name}");

                    if (!forceUpload)
                    {
                        logger.Info($"forceUpload={forceUpload}, пропускаем файл");

                        return GetSubstringFilePath(existsFile.Id);
                    }
                }

                ObjectsResource.InsertMediaUpload mediaUpload;

                using (var fileStream = new FileStream(filePath, FileMode.Open))
                {
                    mediaUpload = _client.CreateObjectUploader(_bucketName, fullFilePath, contentType, fileStream, new UploadObjectOptions{PredefinedAcl = PredefinedObjectAcl.PublicRead});

                    Exception? uploadException = null;
                    mediaUpload.ProgressChanged += progress =>
                    {
                        switch (progress.Status)
                        {
                            case UploadStatus.NotStarted:
                                logger.Info($"[{_session}] :: Ожидание отправки файла...");
                                break;
                            case UploadStatus.Starting:
                                logger.Info($"[{_session}] :: Отправка файла начата");
                                break;
                            case UploadStatus.Uploading:
                                logger.Info($"[{_session}] :: Прогресс: отправлено {progress.BytesSent} байт ...");
                                break;
                            case UploadStatus.Completed:
                                logger.Info($"[{_session}] :: Файл успешно отправлен на Google cloud");
                                break;
                            case UploadStatus.Failed:
                                logger.Warn($"[{_session}] :: Ошибка отправки файла. {progress.Exception.GetFullInfo()}");
                                uploadException = progress.Exception;
                                throw progress.Exception;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    };

                    mediaUpload.Upload();

                    if (uploadException != null)
                        throw uploadException;
                }

                var fileId = mediaUpload.ResponseBody.Id;
                var onlyPath = GetSubstringFilePath(fileId);
                logger.Info($"[{_session}] :: Загрузка файла завершена: FileId={onlyPath}");

                return onlyPath;
            }

            catch (Exception ex)
            {
                logger.Warn(ex, $"{_session} [Ошибка при загрузке файла]");

                throw;
            }
           
        }

        /// <summary>
        ///  Get download file title
        /// </summary>
        /// <param name="fileName">file name to upload</param>
        /// <param name="databaseName">info base name</param>
        private string GetTitleDownloadFileName(string fileName, string databaseName)
        {
            if (string.IsNullOrEmpty(databaseName))
            {
                logger.Info($"[{_session}] :: Имя ИБ пустое возвращаем прежнее название файла {fileName}");

                return fileName;
            }

            if (fileName.Contains(databaseName))
            {
                logger.Info($"[{_session}] :: Имя ИБ {databaseName} содержится в названии файла {fileName}, ничего менять не надо");

                return fileName;
            }

            var titleDownloadFileName = $"{databaseName}_{DateTime.Now:yyyy-MM-dd-HH-mm}.zip";
            logger.Info($"[{_session}] :: Имя ИБ {databaseName} НЕ содержится в названии файла {fileName}, меняем название файла на {titleDownloadFileName}");

            return titleDownloadFileName;
        }

        /// <summary>
        /// Get google object file by file path
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public Object GetFileById(string fileId)
        {
            return _client.GetObject(_bucketName, fileId);
        }

        /// <summary>
        /// Download file from google cloud
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="outputFilePath"></param>
        /// <param name="forceDownload"></param>
        /// <returns></returns>
        public bool DownloadFile(string fileId, string outputFilePath, bool forceDownload)
        {
            try
            {
                logger.Info($"[{_session}] :: Скачивание файла с Google cloud: FileId={fileId}; OutputFilePath={outputFilePath}");

                var file = _client.GetObject(_bucketName, fileId);

                if (File.Exists(outputFilePath))
                {
                    logger.Info($"[{_session}] :: В директории {outputFilePath} найден файл {file.Name}");

                    if (!forceDownload)
                        return false;

                    logger.Info($"[{_session}] :: Указан флаг ForceDownload. Перезапишем найденый файл {outputFilePath}");
                }

                var fileSize = file.Size ?? throw new InvalidOperationException("FileSize не имеет данных");

                var ranges = GetByteFileRanges(fileSize, 10000000);

                foreach (var range in ranges)
                {
                    var partialStream = DownloadPartial(file, range);
                    using var stream = new FileStream(outputFilePath, FileMode.Append);
                    stream.Write(partialStream, 0, partialStream.Length);
                }

                logger.Info($"[{_session}] :: Скачивание файла завершено: FileId={fileId}");

                return true;
               
            }
            catch (Exception ex)
            {
                logger.Info($"[{_session}] :: Ошибка при скачивании файла: {ex.GetFullInfo()}");
                return false;
            }
        }

        public bool DeleteFile(string fileId)
        {
            try
            {
                logger.Info($"[{_session}] :: Удаление файла на Google cloud: FileId={fileId}");
                _client.DeleteObject(_bucketName, fileId);
                logger.Info($"[{_session}] :: Удаление файла на Google cloud завершено");

                return true;
            }

            catch (Exception ex)
            {
                logger.Info($"[{_session}] :: Ошибка при удалении файла: {ex.GetFullInfo()}");
                return false;
            }
        }

        /// <summary>
        /// Recursive and parallel delete all files in google cloud by accountFolderId
        /// </summary>
        /// <param name="accountFolderId"></param>
        /// <param name="nextPageToken"></param>
        public void DeleteAllInFolder(string accountFolderId, string? nextPageToken = null)
        {
            try
            {
                var options = string.IsNullOrEmpty(nextPageToken)
                    ? null
                    : new ListObjectsOptions { PageToken = nextPageToken };

                logger.Info($"[{_session}] :: Удаление  на Google cloud всех файлов в папке: accountFolderId={accountFolderId} nextPageToken={nextPageToken}");
                var objects = _client.ListObjects(_bucketName, accountFolderId, options).ReadPage(100);

                Parallel.ForEach(objects, x =>
                {
                    _client.DeleteObject(x);
                });

                logger.Info($"[{_session}] :: Удаление на Google cloud всех файлов в папке: accountFolderId={accountFolderId} nextPageToken={nextPageToken} завершено");

                if (!string.IsNullOrEmpty(objects.NextPageToken))
                {
                    DeleteAllInFolder(accountFolderId, objects.NextPageToken);
                }
            }

            catch (Exception ex)
            {
                logger.Info($"[{_session}] :: Ошибка при удалении файла: {ex.GetFullInfo()}");

                 throw;
            }
        }

        public async Task<string> CreateDocumentAsync(string title)
        {
            var googleCredentials = GoogleCredential.FromStream(File.OpenRead(CloudConfigurationProvider.Tomb.GetCredentialsPath()))
                .CreateScoped(DocsService.Scope.Documents)
                .CreateScoped(DocsService.Scope.Drive)
                .CreateScoped(DocsService.Scope.DriveFile);

            var initializer = new BaseClientService.Initializer { HttpClientInitializer = googleCredentials };
            var service = new DocsService(initializer);

           var doc =  await service.Documents.Create(new Document { Title = title }).ExecuteAsync();

           DriveService driveService = new DriveService(initializer);

           var permission = new Permission
           {
               Type = "anyone",
               Role = "writer"
           };

           var request = driveService.Permissions.Insert(permission, doc.DocumentId);

           request.Fields = "id";

           await request.ExecuteAsync();

           return doc.DocumentId;
        }

        public async Task DeleteDocumentAsync(string documentId)
        {
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = GoogleCredential.FromStream(File.OpenRead(CloudConfigurationProvider.Tomb.GetCredentialsPath()))
                    .CreateScoped(DocsService.Scope.Documents)
                    .CreateScoped(DocsService.Scope.Drive)
                    .CreateScoped(DocsService.Scope.DriveFile)
            });

            await service.Files.Delete(documentId).ExecuteAsync();
        }

        /// <summary>
        /// Get file bytes range
        /// </summary>
        /// <param name="fileSize">Размер файла</param>
        /// <param name="portion">Порция</param>
        private IEnumerable<RangeHeaderValue> GetByteFileRanges(ulong fileSize, long portion)
        {
            var ranges = new List<RangeHeaderValue>();
            long from = 0;
            while (fileSize > (ulong)from)
            {
                var to = from + portion;
                if ((ulong)to > fileSize)
                    to = from + (long)(fileSize - (ulong)from);

                ranges.Add(new RangeHeaderValue(from, to));
                from = to + 1;
            }

            return ranges;
        }

        /// <summary>
        ///     Download file partial by range
        /// </summary>
        /// <param name="file">Файл Google cloud</param>
        /// <param name="range">Промежуток для скачивания</param>
        private byte[] DownloadPartial(Object file, RangeHeaderValue range)
        {
            using var stream = new MemoryStream();
            _client.DownloadObject(file, stream, new DownloadObjectOptions { Range = range }, new Progress<IDownloadProgress>(
                progress =>
                {
                    switch (progress.Status)
                    {
                        case DownloadStatus.NotStarted:
                            logger.Info($"[{_session}] :: Ожидание копирования...");
                            break;
                        case DownloadStatus.Downloading:
                            logger.Info($"[{_session}] :: Прогресс: скопировано {progress.BytesDownloaded} байт ...");
                            break;
                        case DownloadStatus.Completed:
                            logger.Info($"[{_session}] :: Копирование завершено!");
                            break;
                        case DownloadStatus.Failed:
                            logger.Warn($"[{_session}] :: Ошибка копирования. {progress.Exception}");
                            throw progress.Exception;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }));

            return stream.GetBuffer();
        }

        private static string GetSubstringFilePath(string filePath)
        {
            return filePath.Remove(filePath.LastIndexOf("/", StringComparison.Ordinal))[(filePath.IndexOf('/') + 1)..];
        }
    }
}
