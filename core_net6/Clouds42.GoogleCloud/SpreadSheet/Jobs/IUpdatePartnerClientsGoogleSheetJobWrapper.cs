﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.GoogleCloud.SpreadSheet.Jobs;

public interface IUpdatePartnerClientsGoogleSheetJobWrapper : ITypingJobWrapper<UpdatePartnerClientsGoogleSheetJob>
{
    
}
