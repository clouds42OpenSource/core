﻿using AutoMapper;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels.GoogleSheets;
using Clouds42.GoogleCloud.SpreadSheet.Interfaces;
using Clouds42.GoogleCloud.SpreadSheet.Models;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.GoogleCloud.SpreadSheet.Jobs;

[CoreWorkerJobName(CoreWorkerTasksCatalog.UpdatePartnerClientsGoogleSheetJob)]
public class UpdatePartnerClientsGoogleSheetJob : CoreWorkerJob
{
    private readonly IGoogleSheetsProvider _googleSheetsProvider;
    private readonly BillingServiceManager _billingServiceManager;
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    

    public UpdatePartnerClientsGoogleSheetJob(IUnitOfWork dbLayer,
        IGoogleSheetsProvider googleSheetsProvider,
        BillingServiceManager billingServiceManager,
        IMapper mapper,
        IUnitOfWork unitOfWork) {
        _googleSheetsProvider = googleSheetsProvider;
        _billingServiceManager = billingServiceManager;
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }
    
    public override async void Execute(Guid taskId, Guid taskQueueId)
    {
        var spreadSheets = await _unitOfWork.GoogleSheetRepository
            .AsQueryableNoTracking()
            .Where(s => s.SheetType == SheetType.PartnerClients)
            .ToArrayAsync();

        foreach (GoogleSheet sheet in spreadSheets)
        {
            PartnersClientFilterDto filter = new PartnersClientFilterDto
            {
                PeriodFrom = sheet.PeriodFrom,
                PeriodTo = sheet.PeriodTo,
                AccountId = sheet.AccountId,
                PageNumber = 1,
                PageSize = int.MaxValue,
            };
            var partnerClients = _billingServiceManager.GetPartnerClientsConsiderAdditionalMonths(filter)
                .Result.ChunkDataOfPagination;
            var partnerClientsSheetDtos = _mapper.Map<List<PartnerClientSheetDto>>(partnerClients);
            await _googleSheetsProvider
                .UpdateSpreadsheetAsync(partnerClientsSheetDtos, sheet.DocumentId, PartnerClientGoogleSheetInfo.ColumnNames);
        }
    }
}
