﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.GoogleCloud.SpreadSheet.Jobs;

public class UpdatePartnerClientsGoogleSheetJobWrapper(
    IRegisterTaskInQueueProvider registerTaskInQueueProvider,
    IUnitOfWork dbLayer)
    : TypingJobWrapperBase<UpdatePartnerClientsGoogleSheetJob>(registerTaskInQueueProvider, dbLayer),
        IUpdatePartnerClientsGoogleSheetJobWrapper
{
    public override TypingJobWrapperBase<UpdatePartnerClientsGoogleSheetJob> Start()
    {
        StartTask("Обновление Google Sheet клиентов партнеров");
        return this;
    }
}
