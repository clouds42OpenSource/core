﻿using System.Net;
using Clouds42.GoogleCloud.SpreadSheet.Interfaces;
using Clouds42.Logger;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Polly;
using Polly.Retry;

namespace Clouds42.GoogleCloud.SpreadSheet;

public class SheetsServiceWrapper : ISheetsServiceWrapper
{
    private readonly SheetsService _sheetsService;
    private readonly AsyncRetryPolicy _retryPolicy;

    public SheetsServiceWrapper(SheetsService sheetsService, ILogger42 logger)
    {
        _sheetsService = sheetsService;
        _retryPolicy = Policy
            .Handle<HttpRequestException>(ex => ex.StatusCode == (HttpStatusCode)429) 
            .WaitAndRetryAsync(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), 
                (exception, timeSpan, retryCount, context) => {
                    logger.Info($"Retry {retryCount} after {timeSpan.Seconds} seconds due to {exception.Message}");
                });
    }
    
    public async Task<Spreadsheet> CreateSpreadsheetAsync(Spreadsheet spreadsheet)
    {
        return await _retryPolicy.ExecuteAsync(async () =>
        {
            var request = _sheetsService.Spreadsheets.Create(spreadsheet);
            return await request.ExecuteAsync();
        });
    }

    public async Task<ClearValuesResponse> ClearValuesAsync(ClearValuesRequest requestBody, string spreadsheetId, string range)
    {
        return await _retryPolicy.ExecuteAsync(async () =>
        {
            var request = _sheetsService.Spreadsheets.Values.Clear(requestBody, spreadsheetId, range);
            return await request.ExecuteAsync();
        });
    }

    public async Task<UpdateValuesResponse> UpdateValuesAsync(ValueRange body, string spreadsheetId, string range)
    {
        return await _retryPolicy.ExecuteAsync(async () =>
        {
            var request = _sheetsService.Spreadsheets.Values.Update(body, spreadsheetId, range);
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
            return await request.ExecuteAsync();
        });
    }

    public async Task<BatchUpdateSpreadsheetResponse> BatchUpdateAsync(BatchUpdateSpreadsheetRequest requestBody, string spreadsheetId)
    {
        return await _retryPolicy.ExecuteAsync(async () =>
        {
            var request = _sheetsService.Spreadsheets.BatchUpdate(requestBody, spreadsheetId);
            return await request.ExecuteAsync();
        });
    }  
}
