﻿using Google.Apis.Sheets.v4.Data;

namespace Clouds42.GoogleCloud.SpreadSheet.Interfaces;

public interface ISheetsServiceWrapper
{
    /// <summary>
    /// Инкапсулирует логику ретраев и отправки запроса на создание таблицы.
    /// </summary>
    /// <param name="spreadsheet">Данные фильтра.</param>
    /// <returns>Контекс сгенерированного файла.</returns>
    Task<Spreadsheet> CreateSpreadsheetAsync(Spreadsheet spreadsheet);
    
    /// <summary>
    /// Инкапсулирует логику ретраев и отправки запроса на очистку таблицы.
    /// </summary>
    /// <param name="requestBody">Тело запроса.</param>
    /// <param name="spreadsheetId">Идентификатор документа.</param>
    /// <param name="range">Диапазон очистки.</param>
    /// <returns>Ответ запроса.</returns>
    Task<ClearValuesResponse> ClearValuesAsync(ClearValuesRequest requestBody, string spreadsheetId, string range);
    
    /// <summary>
    /// Инкапсулирует логику ретраев и отправки запроса на обновление таблицы.
    /// </summary>
    /// <param name="body">Тело запроса.</param>
    /// <param name="spreadsheetId">Идентификатор документа.</param>
    /// <param name="range">Диапазон очистки.</param>
    /// <returns>Ответ запроса.</returns>
    Task<UpdateValuesResponse> UpdateValuesAsync(ValueRange body, string spreadsheetId, string range);
    
    /// <summary>
    /// Инкапсулирует логику ретраев и отправки запроса на модификацию элементов таблицы.
    /// </summary>
    /// <param name="requestBody">Тело запроса.</param>
    /// <param name="spreadsheetId">Идентификатор документа.</param>
    /// <returns>Ответ запроса.</returns>
    Task<BatchUpdateSpreadsheetResponse> BatchUpdateAsync(BatchUpdateSpreadsheetRequest requestBody, string spreadsheetId);
}
