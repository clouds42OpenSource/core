﻿using Clouds42.Common.ManagersResults;
using Clouds42.GoogleCloud.SpreadSheet.Models;

namespace Clouds42.GoogleCloud.SpreadSheet.Interfaces;

public interface IGoogleSheetsProvider
{
    
    /// <summary>
    /// Получает или создаёт Google Sheet по указанному email и параметрам листа.
    /// </summary>
    /// <param name="email">Email пользователя.</param>
    /// <param name="sheet">Параметры Google Sheet.</param>
    /// <returns>Результат операции, содержащий идентификатор Google Sheet.</returns>
  Task<ManagerResult<string>> GetOrCreateGoogleSheetAsync(string email, GoogleSheetDto sheet);
    
    /// <summary>
    /// Обновляет данные в Google Sheet.
    /// </summary>
    /// <typeparam name="T">Тип данных для обновления.</typeparam>
    /// <param name="data">Список данных для обновления.</param>
    /// <param name="documentId">Идентификатор документа Google Sheet.</param>
    /// <param name="columnNames">Список имён столбцов для обновления.</param>
    /// <param name="sheetNumber">Номер листа в документе (по умолчанию 1).</param>
    /// <returns>Task.</returns>
    Task UpdateSpreadsheetAsync<T>(
        IList<T> data,
        string documentId,
        IList<string> columnNames,
        int sheetNumber = 1);
}
