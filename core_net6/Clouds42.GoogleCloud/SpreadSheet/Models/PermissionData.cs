namespace Clouds42.GoogleCloud.SpreadSheet.Models;

public record PermissionData(string Type, string Role, string? Email);
