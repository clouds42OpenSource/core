﻿using System.Globalization;
using Clouds42.Domain.Enums;

namespace Clouds42.GoogleCloud.SpreadSheet.Models;


    /// <summary>
    /// Модель, представляющая данные партнёрских клиентов в Google Sheets.
    /// </summary>
    public class PartnerClientSheetDto
    {
        /// <summary>
        /// Индексный номер аккаунта.
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Описание аккаунта или его заголовок.
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Тип партнёрства.
        /// </summary>
        public string TypeOfPartnership { get; set; }

        /// <summary>
        /// Название сервиса.
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Дата активации клиента.
        /// </summary>
        public string ClientActivationDate { get; set; }

        /// <summary>
        /// Дата окончания действия сервиса.
        /// </summary>
        public string ServiceExpireDate { get; set; }

        /// <summary>
        /// Статус активности сервиса для клиента.
        /// </summary>
        public string ServiceIsActiveForClient { get; set; }

        /// <summary>
        /// Статус пробного периода.
        /// </summary>
        public string IsDemoPeriod { get; set; }

        /// <summary>
        /// Ежемесячный бонус.
        /// </summary>
        public decimal MonthlyBonus { get; set; }
    }

    public static class PartnerClientSheetDtoExtensions
    {
        /// <summary>
        /// Преобразует дату в строку формата "yyyy-MM-dd".
        /// </summary>
        /// <param name="date">Дата для преобразования.</param>
        /// <returns>Строковое представление даты в формате "yyyy-MM-dd".</returns>
        public static string? ToDateOnlyString(this DateTime? date)
        {
            return date?.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Преобразует статус активности сервиса в строку.
        /// </summary>
        /// <param name="isActive">Статус активности.</param>
        /// <returns>"Активно" если сервис активен, иначе "Неактивно".</returns>
        public static string MapServiceIsActive(bool isActive)
        {
            return isActive ? "Активно" : "Неактивно";
        }

        /// <summary>
        /// Преобразует статус пробного периода в строку.
        /// </summary>
        /// <param name="isDemo">Статус пробного периода.</param>
        /// <returns>"Демо" если это пробный период, иначе null.</returns>
        public static string? MapDemoPeriod(bool isDemo)
        {
            return isDemo ? "Демо" : null;
        }

        /// <summary>
        /// Преобразует тип партнёрства в строку.
        /// </summary>
        /// <param name="typeOfPartnership">Тип партнёрства.</param>
        /// <returns>"Клиент" если это агент, "Клиент моего сервиса" если это владелец сервиса, иначе null.</returns>
        public static string? MapClientType(TypeOfPartnership typeOfPartnership)
        {
            return typeOfPartnership switch
            {
                TypeOfPartnership.Agent => "Клиент",
                TypeOfPartnership.SoftOwner => "Клиент моего сервиса",
                _ => null
            };
        }
    }
