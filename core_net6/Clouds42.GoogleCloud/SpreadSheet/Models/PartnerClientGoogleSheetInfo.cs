namespace Clouds42.GoogleCloud.SpreadSheet.Models;

public class PartnerClientGoogleSheetInfo : GoogleSheetDto
{
    public static readonly string[] ColumnNames =
    [
        "Номер аккаунта", "Название компании", "Тип партнерства", "Сервис", "Дата подключения",
        "Дата окончания сервиса", "Активность", "Демо период", "Ежемесячное вознаграждение, руб"
    ];

}
