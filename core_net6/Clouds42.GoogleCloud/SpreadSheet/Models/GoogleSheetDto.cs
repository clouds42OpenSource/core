using Clouds42.Domain.DataModels.GoogleSheets;

namespace Clouds42.GoogleCloud.SpreadSheet.Models;

public  class GoogleSheetDto
{
    public Guid AccountId { get; set; }
    public DateTime? PeriodFrom { get; set; }
    public DateTime? PeriodTo { get; set; }
    public SheetType SheetType { get; set; }
};

