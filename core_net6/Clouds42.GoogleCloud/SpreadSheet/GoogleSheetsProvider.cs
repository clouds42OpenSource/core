﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.GoogleSheets;
using Clouds42.GoogleCloud.SpreadSheet.Interfaces;
using Clouds42.GoogleCloud.SpreadSheet.Models;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Google.Apis.Sheets.v4.Data;

namespace Clouds42.GoogleCloud.SpreadSheet;

public class GoogleSheetsProvider(
    ILogger42 logger,
    IGoogleDriveWithAccessProvider driveService,
    IUnitOfWork unitOfWork,
    IAccessProvider accessProvider,
    IHandlerException handlerException,
    ISheetsServiceWrapper sheetsServiceWrapper)
    : BaseManager(accessProvider, unitOfWork, handlerException), IGoogleSheetsProvider
{
    private const string Type = "anyone";
    private const string Role = "reader";


    public async Task<ManagerResult<string>> GetOrCreateGoogleSheetAsync(string email, GoogleSheetDto spreadSheetDto)
    {
        try
        {
            GoogleSheet? spreadsheet = await GetSpreadsheetByAccountIdAsync(spreadSheetDto.AccountId);
            string spreadsheetId;
            if (spreadsheet == null)
            {
                spreadsheetId = await CreateSpreadsheet(spreadSheetDto.AccountId, email);
                unitOfWork.GoogleSheetRepository
                    .Insert(new GoogleSheet {
                        Id = Guid.NewGuid(),
                        AccountId = spreadSheetDto.AccountId,
                        DocumentId = spreadsheetId,
                        PeriodTo = spreadSheetDto.PeriodTo,
                        PeriodFrom = spreadSheetDto.PeriodFrom,
                        SheetType = spreadSheetDto.SheetType
                    });
                await unitOfWork.SaveAsync();
                logger.Info($"Spreadsheet created with ID: {spreadsheetId} for account with ID: {spreadSheetDto.AccountId}");
            }
            else
            {
                if (ChangeDate(spreadSheetDto, spreadsheet))
                {
                    unitOfWork.GoogleSheetRepository.Update(spreadsheet);
                    await unitOfWork.SaveAsync();
                }
                spreadsheetId = spreadsheet.DocumentId;
                logger.Info($"Spreadsheet found with ID: {spreadsheet.DocumentId}");
            }
            return Ok(spreadsheetId);
        }
        catch (Exception e)
        {
            handlerException.Handle(e, $"[Ошибка получения или создания гугл таблицы партнера] {spreadSheetDto.AccountId}");
            return PreconditionFailed<string>(e.Message);
        }
    }

    /// <summary>
    /// Обновляет даты периода в объекте GoogleSheet, если они отличаются от дат в GoogleSheetDto.
    /// </summary>
    /// <param name="googleSheetDto">Объект DTO, содержащий новые даты.</param>
    /// <param name="googleSheet">Объект GoogleSheet для обновления.</param>
    /// <returns>
    /// true, если были внесены изменения в даты периода; иначе false.
    /// </returns>
    private static bool ChangeDate(GoogleSheetDto googleSheetDto, GoogleSheet googleSheet)
    {
        bool isChanged = false;

        if (googleSheet.PeriodFrom != googleSheetDto.PeriodFrom)
        {
            googleSheet.PeriodFrom = googleSheetDto.PeriodFrom;
            isChanged = true;
        }

        if (googleSheet.PeriodTo == googleSheetDto.PeriodTo)
        {
            return isChanged;
        }

        googleSheet.PeriodTo = googleSheetDto.PeriodTo;
        isChanged = true;

        return isChanged;
    }

    public async Task UpdateSpreadsheetAsync<T>(
        IList<T> data,
        string documentId,
        IList<string> columnNames,
        int sheetNumber = 1)
    {
        try
        {
            await ClearSpreadsheet(documentId, sheetNumber);
            await WriteToSheet(data, documentId, columnNames);
        }
        catch (Exception e)
        {
            handlerException.Handle(e, $"[Ошибка обновления гугл таблицы] {documentId}");
        }
    }

    private async Task ClearSpreadsheet(string spreadSheetId, int spreadSheetNum = 1)
    {
        ClearValuesRequest requestBody = new ClearValuesRequest();
        await sheetsServiceWrapper.ClearValuesAsync(requestBody, spreadSheetId, $"Sheet{spreadSheetNum}");

    }
    private async Task<GoogleSheet?> GetSpreadsheetByAccountIdAsync(Guid userId) =>
        (await unitOfWork.GoogleSheetRepository.FirstOrDefaultAsync(s => s.AccountId == userId));
    
    
    private async Task<string> CreateSpreadsheet(Guid accountId, string email)
    {
        var spreadsheet = new Spreadsheet
        {
            Properties = new SpreadsheetProperties
            {
                Title = $"Partners {accountId}"
            }
        };
        var response = await sheetsServiceWrapper.CreateSpreadsheetAsync(spreadsheet);
        var data = new PermissionData(Type, Role, email);
        await driveService.GrantAccessToUser(response.SpreadsheetId, data);
        return response.SpreadsheetId;
    }

    private  async Task WriteToSheet<T>(IList<T> data, string spreadSheetId, IList<string> columnNames, int sheetNumber = 1)
    {
        int numberOfRows = data.Count + 1;
        string range = $"Sheet{sheetNumber}!A1:{GetExcelColumnName(columnNames.Count)}{numberOfRows}";
        var valueRange = new ValueRange();
        var objectList = new List<IList<object>> { columnNames.Cast<object>().ToList() };

        var properties = typeof(T).GetProperties();
        objectList.AddRange(data.Select(item => properties.Select(prop => prop.GetValue(item)).Select(value => value ?? "").ToList()));

        valueRange.Values = objectList;

        await sheetsServiceWrapper.UpdateValuesAsync(valueRange, spreadSheetId, range);
        await AutoResizeColumns(spreadSheetId, columnNames.Count, sheetNumber);
    }
    
    private async Task AutoResizeColumns(string spreadSheetId, int columnCount, int sheetNumber = 1)
    {
        var autoResizeRequest = new Request
        {
            AutoResizeDimensions = new AutoResizeDimensionsRequest
            {
                Dimensions = new DimensionRange
                {
                    SheetId = sheetNumber - 1,
                    Dimension = "COLUMNS",
                    StartIndex = 0,
                    EndIndex = columnCount
                }
            }
        };

        var batchUpdateRequest = new BatchUpdateSpreadsheetRequest
        {
            Requests = new List<Request> { autoResizeRequest }
        };
        
        await sheetsServiceWrapper.BatchUpdateAsync(batchUpdateRequest, spreadSheetId);
    }

    
    private static string GetExcelColumnName(int columnNumber)
    {
        string columnName = "";
        while (columnNumber > 0)
        {
            int modulo = (columnNumber - 1) % 26;
            columnName = Convert.ToChar('A' + modulo) + columnName;
            columnNumber = (columnNumber - modulo) / 26;
        }
        return columnName;
    }
    
}


