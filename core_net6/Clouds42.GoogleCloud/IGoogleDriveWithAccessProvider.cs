using Clouds42.GoogleCloud.SpreadSheet.Models;
using Google.Apis.Drive.v3.Data;

namespace Clouds42.GoogleCloud;

public interface IGoogleDriveWithAccessProvider : IGoogleDriveProvider
{
    Task<Permission?> GrantAccessToUser(string spreadsheetId, PermissionData data);
}
