﻿using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.GoogleCloud.SignedLinks;
using Clouds42.GoogleCloud.SpreadSheet;
using Clouds42.GoogleCloud.SpreadSheet.Interfaces;
using Clouds42.GoogleCloud.SpreadSheet.Jobs;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.GoogleCloud
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddGoogleCloud(this IServiceCollection services, IConfiguration config)
        {
            ConfigurationHelper.SetConfiguration(config);
            // Google service providers
            services.AddTransient<IGoogleCloudProvider, GoogleCloudProvider>();
            services.AddTransient<IGoogleDriveProvider, GoogleDriveProvider>();
            services.AddTransient<IGoogleDriveAuthProvider, GoogleDriveAuthProvider>();
            services.AddTransient<IGoogleDriveWithAccessProvider, GoogleDriveWithAccessProvider>();
            services.AddScoped<ISheetsServiceWrapper, SheetsServiceWrapper>();
            services.AddScoped<IGoogleSheetsProvider, GoogleSheetsProvider>();
            services.AddScoped<ITemporaryUrlService, TemporaryUrlService>();


            // job
            services
                .AddTransient<IUpdatePartnerClientsGoogleSheetJobWrapper, UpdatePartnerClientsGoogleSheetJobWrapper>();
            // Google service instances
            GoogleCredential credential = GoogleCredential.FromStream(File.OpenRead(CloudConfigurationProvider.Tomb.GetCredentialsPath()))
                .CreateScoped(SheetsService.Scope.Spreadsheets, DriveService.Scope.DriveFile);
            services.AddScoped(_ => new SheetsService(new BaseClientService.Initializer
            {
                
                HttpClientInitializer = credential,
                ApplicationName = CloudConfigurationProvider.Tomb.GetApplicationName(),
            }));
            services.AddScoped(_ => new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = CloudConfigurationProvider.Tomb.GetApplicationName(),
            }));
            return services;
        }
    }
}
