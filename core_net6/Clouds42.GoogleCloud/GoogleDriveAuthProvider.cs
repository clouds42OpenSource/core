﻿using System.Text;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using Newtonsoft.Json;
#pragma warning disable CS8602

namespace Clouds42.GoogleCloud
{
    public interface IGoogleDriveAuthProvider
    {
        /// <summary>
        ///     Авторизация в GoogleDrive
        /// </summary>
        DriveService Authorize();
    }

    public class GoogleDriveAuthProvider : IGoogleDriveAuthProvider
    {
        private readonly ILogger42 _logger;
        /// <summary>
        ///     Секретный ключ (json)
        ///     Примечание: Скачать ключ: https://console.cloud.google.com/
        /// </summary>
        private readonly string _secretKey;

        /// <summary>
        ///     Данные авторизации (json)
        ///     Примечание: Генерируется при первичной авторизации через использование метода GoogleWebAuthorizationBroker.AuthorizeAsync
        /// </summary>
        private readonly string _authTokenData;

        /// <summary>
        ///     Название приложения (Учетные данные в GoogleDrive API).
        ///     Примечание: Узнать: https://console.cloud.google.com/
        /// </summary>
        private readonly string _appName;

        /// <summary>
        ///     Области применения
        /// </summary>
        private readonly string[] _scopes;

        public GoogleDriveAuthProvider(ILogger42 logger)
        {
            _logger = logger;
            var secretKey = CloudConfigurationProvider.Tomb.GetSecretKey();
            var appName = CloudConfigurationProvider.Tomb.GetApplicationName();
            var authTokenData = CloudConfigurationProvider.Tomb.GetAuthTokenData();

            _secretKey = secretKey;
            _appName = appName;
            _authTokenData = authTokenData;
            _scopes = [DriveService.Scope.Drive];
        }

        /// <summary>
        ///     Авторизация в GoogleDrive
        /// </summary>
        public DriveService Authorize()
        { 
            _logger.Info("Выполняется авторизация на Google Drive");

            return new DriveService(ServiceInitializer);
        }

        private BaseClientService.Initializer ServiceInitializer => new()
        {
            HttpClientInitializer = new UserCredential(AuthorizationCodeFlow, "user", TokenResponse),
            ApplicationName = _appName
        };

        private GoogleAuthorizationCodeFlow AuthorizationCodeFlow
        {
            get
            {
                var stream = new MemoryStream(Encoding.UTF8.GetBytes(_secretKey));

                return new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
                {
                    ClientSecrets = GoogleClientSecrets.FromStream(stream).Secrets,
                    Scopes = _scopes
                });
            }
        }

        private TokenResponse TokenResponse
        {
            get
            {
                var authTokenData = JsonConvert.DeserializeObject<AuthTokenData>(_authTokenData);

                return new TokenResponse
                {
                    AccessToken = authTokenData.AccessToken,
                    RefreshToken = authTokenData.RefreshToken,
                    TokenType = authTokenData.TokenType,
                    ExpiresInSeconds = authTokenData.ExpiresInSeconds
                };
            }
        }
    }
}
