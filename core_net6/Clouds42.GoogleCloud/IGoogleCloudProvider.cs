﻿using Object = Google.Apis.Storage.v1.Data.Object;

namespace Clouds42.GoogleCloud;

public interface IGoogleCloudProvider
{
    string UploadFile(string filePath, string folderPrefix, bool forceUpload, string databaseName);
    Object? GetFileById(string fileId);
    bool DownloadFile(string fileId, string outputFilePath, bool forceDownload);
    bool DeleteFile(string fileId);
    void DeleteAllInFolder(string accountFolderId, string? nextPageToken = null);
    Task<string> CreateDocumentAsync(string title);
    Task DeleteDocumentAsync(string documentId);
}
