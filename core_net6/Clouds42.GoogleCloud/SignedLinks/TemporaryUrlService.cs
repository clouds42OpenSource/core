﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using PagedListExtensionsNetFramework;

namespace Clouds42.GoogleCloud.SignedLinks
{
    public class TemporaryUrlService : ITemporaryUrlService
    {
        private readonly IHandlerException _handlerException; 
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccessProvider _accessProvider;

        private readonly UrlSigner _urlSigner;
        
        private static readonly string BucketName = CloudConfigurationProvider.Tomb.GetBucketName();
        private static readonly string CredentialsPath = CloudConfigurationProvider.Tomb.GetCredentialsPath();

        public TemporaryUrlService(IHandlerException handlerException, IUnitOfWork unitOfWork, IAccessProvider accessProvider)
        {
            _handlerException = handlerException;
            _unitOfWork = unitOfWork;
            _accessProvider = accessProvider;
            var credential = GoogleCredential.FromFile(CredentialsPath);
            _urlSigner = UrlSigner.FromCredential(credential);
        }


        public async Task<ManagerResult<string>> GetSignedUrl(Guid backupId)
        {
            try
            {
                var databaseInfo = _unitOfWork.AccountDatabaseBackupRepository.GetAccountDatabaseBackupPath(backupId);
                if (databaseInfo == null)
                    return PagedListExtensions.ToPreconditionFailedManagerResult<string>("Путь к бекапу не найден");

                const string marker = "tomb42/";
                int markerIndex = databaseInfo.IndexOf(marker, StringComparison.OrdinalIgnoreCase);

                if (markerIndex == -1)
                    return PagedListExtensions.ToPreconditionFailedManagerResult<string>("Неверный формат пути к бекапу");

                string objectPath = databaseInfo.Substring(markerIndex + marker.Length);

                return await GenerateSignedUrlAsync(objectPath, TimeSpan.FromMinutes(5));
            }
            catch (Exception ex)
            {
                string message = $"Ошибка при получении подписанной ссылки: {ex.Message}";
                _handlerException.Handle(ex, message);
                return PagedListExtensions.ToPreconditionFailedManagerResult<string>(message);
            }
        } 
        
        private async Task<ManagerResult<string>> GenerateSignedUrlAsync(string objectPath, TimeSpan validity)
        {
            try
            { 
                await LogGeneratingUrl(objectPath); 
                string url = await _urlSigner.SignAsync(BucketName, objectPath, validity);
                return url.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                string message = $"Ошибка генерации временной ссылки: {ex.Message}";
                _handlerException.Handle(ex, message);
                return PagedListExtensions.ToPreconditionFailedManagerResult<string>(message);
            }
        }


        private async Task LogGeneratingUrl(string backupName)
        {
            var currentUser = await _accessProvider.GetUserAsync();
            LogEventHelper.LogEvent(_unitOfWork, currentUser.ContextAccountId, _accessProvider,LogActions.SignedUrlGenerating, 
                $"Пользователь {currentUser.Name} с почтой {currentUser.Email} начал генерацию временной ссылки к {backupName} источник пользователя {currentUser.UserSource}" );
        }
        
    }
}
