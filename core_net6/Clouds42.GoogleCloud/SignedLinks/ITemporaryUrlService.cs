﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.GoogleCloud.SignedLinks;

public interface ITemporaryUrlService
{
    Task<ManagerResult<string>> GetSignedUrl(Guid accountDatabaseId);
}
