using Clouds42.GoogleCloud.SpreadSheet.Models;
using Clouds42.Logger;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;

namespace Clouds42.GoogleCloud;

public class GoogleDriveWithAccessProvider(
    ILogger42 logger42,
    IGoogleDriveAuthProvider authProvider,
    DriveService driveService)
    : GoogleDriveProvider(logger42, authProvider), IGoogleDriveWithAccessProvider
{
    public async Task<Permission?> GrantAccessToUser(string documentId, PermissionData data)
    {
        var permission = new Permission
        { 
            Type = data.Type,
            Role = data.Role,
            EmailAddress = data.Type == "user" ? data.Email : null
        };
      return await driveService.Permissions.Create(permission, documentId).ExecuteAsync();
    }
    
}
