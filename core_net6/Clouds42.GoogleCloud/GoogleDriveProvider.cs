﻿using System.Net.Http.Headers;
using Clouds42.Common.Exceptions;
using Clouds42.Logger;
using Google.Apis.Download;
using Google.Apis.Drive.v2;
using GoogleFile = Google.Apis.Drive.v2.Data.File;

namespace Clouds42.GoogleCloud
{
    public interface IGoogleDriveProvider
    {
        void PartialDownloadFile(string url, string outputFilePath);
    }

    public class GoogleDriveProvider(ILogger42 logger42, IGoogleDriveAuthProvider authProvider) : IGoogleDriveProvider
    {
        private readonly DriveService _driveService = authProvider.Authorize();
        private readonly Guid _session = Guid.NewGuid();

        public void PartialDownloadFile(string url, string outputFilePath)
        {
            string fileId = url.Split('/')[5];

            var file = _driveService.Files.Get(fileId).Execute();

            if (File.Exists(outputFilePath))
            {
                logger42.Info($"[{_session}] :: В директории {outputFilePath} найден файл {file.Title}");

                logger42.Info($"[{_session}] :: Указан флаг ForceDownload. Перезапишем найденый файл {outputFilePath}");
            }

            var fileSize = file.FileSize ?? throw new NotFoundException("FileSize не имеет данных");
            
            var ranges = GetByteFileRanges(fileSize, 10000000);

            foreach (var range in ranges)
            {
                var partialStream = DownloadPartial(file, range);
                using var stream = new FileStream(outputFilePath, FileMode.Append);
                stream.Write(partialStream, 0, partialStream.Length);
            }
        }
        /// <summary>
        ///     Скачать часть файла по выбранным байтам
        /// </summary>
        /// <param name="file">Файл GoogleDrive</param>
        /// <param name="range">Промежуток для скачивания</param>
        private byte[] DownloadPartial(GoogleFile file, RangeHeaderValue range)
        {
            var request = _driveService.Files.Get(file.Id);
            using var stream = new MemoryStream();
            request.MediaDownloader.ProgressChanged += progress =>
            {
                switch (progress.Status)
                {
                    case DownloadStatus.NotStarted:
                        logger42.Info($"[{_session}] :: Ожидание копирования...");
                        break;
                    case DownloadStatus.Downloading:
                        logger42.Info($"[{_session}] :: Прогресс: скопировано {progress.BytesDownloaded} байт ...");
                        break;
                    case DownloadStatus.Completed:
                        logger42.Info($"[{_session}] :: Копирование завершено!");
                        break;
                    case DownloadStatus.Failed:
                        logger42.Warn($"[{_session}] :: Ошибка копирования. {progress.Exception}");
                        throw progress.Exception;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            };

            request.DownloadRange(stream, range);

            return stream.GetBuffer();
        }

        /// <summary>
        /// Get file bytes range
        /// </summary>
        /// <param name="fileSize">Размер файла</param>
        /// <param name="portion">Порция</param>
        private IEnumerable<RangeHeaderValue> GetByteFileRanges(long fileSize, long portion)
        {
            var ranges = new List<RangeHeaderValue>();
            long from = 0;
            while (fileSize > from)
            {
                var to = from + portion;
                if (to > fileSize)
                    to = from + (fileSize - from);

                ranges.Add(new RangeHeaderValue(from, to));
                from = to + 1;
            }

            return ranges;
        }

    }
}
