﻿using Clouds42.OnlineCashier.Builders.ChequeOnline;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Providers;
using Clouds42.OnlineCashier.Infrastructure;
using Clouds42.WebKit;
using Clouds42.WebKit.RequestProviders;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.OnlineCashier
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddOnlineCashierServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IChequeOnlineRequestProvider, ChequeOnlineRequestProvider>();
            serviceCollection.AddTransient<IHttpWebRequestProvider, HttpWebRequestProvider>();
            serviceCollection.AddTransient<IOnlineCashBuilder, ChequeOnlineBuilder>();

            return serviceCollection;
        }
    }
}
