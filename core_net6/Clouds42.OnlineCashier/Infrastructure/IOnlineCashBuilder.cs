﻿using Clouds42.OnlineCashier.Infrastructure.IModels;

namespace Clouds42.OnlineCashier.Infrastructure
{
    /// <summary>
    ///     Интерфейс для формирования чека в онлайн кассе
    /// </summary>
    public interface IOnlineCashBuilder
    {
        /// <summary>
        ///     Получение чека
        /// </summary>
        /// <param name="authData">Данные авторизации</param>
        /// <param name="request">Модель на создание чека</param>
        IOnlineCashierResponse BuildCheck(IOnlineCashierAuth authData, IOnlineCashierRequest request);

        /// <summary>
        ///     Валидация модели
        /// </summary>
        /// <param name="authData">Данные авторизации</param>
        /// <param name="request">Модель на создание чека</param>
        /// <param name="errors">Ошибки</param>
        bool ValidateRequest(IOnlineCashierAuth authData, IOnlineCashierRequest request, out string errors);
    }
}
