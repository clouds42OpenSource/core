﻿using Clouds42.OnlineCashier.Infrastructure.IModels;

namespace Clouds42.OnlineCashier.Infrastructure.Models
{
    /// <summary>
    ///     Базовый ответ онлайн кассы
    /// </summary>
    public class BaseOnlineCashierResponse : IOnlineCashierResponse
    {
        public bool IsSuccess { get; set; }
        public string Errors { get; set; }
    }
}