﻿using System;
using System.Linq;
using System.Text;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Providers;
using Clouds42.OnlineCashier.Infrastructure;
using Clouds42.OnlineCashier.Infrastructure.IModels;
using Clouds42.OnlineCashier.Infrastructure.Models;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline
{
    /// <summary>
    ///     ЧЕК онлайн.
    ///     URL: http://chekonline.ru
    /// </summary>
    public class ChequeOnlineBuilder(IChequeOnlineRequestProvider chequeOnlineRequest) : IOnlineCashBuilder
    {
        private readonly ChequeOnlineTransportRequestProvider _transportRequestProvider = new();
        private readonly ChequeOnlineTransportResponseProvider _transportResponseProvider = new();

        /// <summary>
        ///     Получение чека
        /// </summary>
        /// <param name="authData">Данные авторизации</param>
        /// <param name="request">Модель выставленного счета</param>
        public IOnlineCashierResponse BuildCheck(IOnlineCashierAuth authData, IOnlineCashierRequest request)
        {
            var transportRequest = _transportRequestProvider.ToTransportRequest((ChequeOnlineRequest) request);

            var responseTransport = chequeOnlineRequest.SendRequest((ChequeOnlineAuth) authData, transportRequest);
            if (!string.IsNullOrEmpty(responseTransport.RequestError))
                return new BaseOnlineCashierResponse {Errors = responseTransport.RequestError};

            var businessModel = _transportResponseProvider.ToBusinessModel(responseTransport);
            return businessModel;
        }

        /// <summary>
        ///     Валидация запроса
        /// </summary>
        /// <param name="authData">Данные авторизации</param>
        /// <param name="request">Модель выставленного счета</param>
        /// <param name="errors">Ошибки</param>
        public bool ValidateRequest(IOnlineCashierAuth authData, IOnlineCashierRequest request, out string errors)
        {
            if (authData is not ChequeOnlineAuth authDataModel)
            {
                errors = "Неверная авторизационная модель запроса. Поддерживается только " + nameof(ChequeOnlineAuth);
                return false;
            }

            if (request is not ChequeOnlineRequest requestModel)
            {
                errors = "Неверная бизнес модель запроса. Поддерживается только " + nameof(ChequeOnlineRequest);
                return false;
            }

            var errorMessageBuilder = new StringBuilder();

            ValidateRequestProperties(requestModel, authDataModel, errorMessageBuilder);
            ValidateRequestProducts(requestModel, errorMessageBuilder);

            errors = errorMessageBuilder.ToString();
            return string.IsNullOrEmpty(errors);
        }

        /// <summary>
        /// Провалидировать параметры запроса
        /// </summary>
        /// <param name="requestModel">Модель запроса</param>
        /// <param name="authDataModel">Модель аутентификации</param>
        /// <param name="errorMessageBuilder">Билдер сообщений об ошибке</param>
        private static void ValidateRequestProperties(ChequeOnlineRequest requestModel, ChequeOnlineAuth authDataModel, StringBuilder errorMessageBuilder)
        {
            if (string.IsNullOrEmpty(authDataModel.CertificateData))
                errorMessageBuilder.AppendLine("Данные о сертификате отсутствуют");

            if (string.IsNullOrEmpty(authDataModel.CertificatePassword))
                errorMessageBuilder.AppendLine("Пароль сертификата отсутствует");

            if (string.IsNullOrEmpty(authDataModel.Url))
                errorMessageBuilder.AppendLine("Адрес сервиса chekonline отсутствует");

            if (string.IsNullOrEmpty(requestModel.ClientEmail))
                errorMessageBuilder.AppendLine("Email клиента отсутствует");

            if (requestModel.InvoiceId == Guid.Empty)
                errorMessageBuilder.AppendLine("Неверный ID выплаты");
        }

        /// <summary>
        /// Провалидировать список товаров/услуг в модели запроса
        /// </summary>
        /// <param name="requestModel">Модель запроса</param>
        /// <param name="errorMessageBuilder">Билдер сообщений об ошибке</param>
        private static void ValidateRequestProducts(ChequeOnlineRequest requestModel, StringBuilder errorMessageBuilder)
        {
            if (requestModel.Products == null || !requestModel.Products.Any())
                errorMessageBuilder.AppendLine("Нет ни одного товара/услуги");
            else
            {
                var count = 1;
                requestModel.Products.ForEach(product =>
                {
                    if (string.IsNullOrEmpty(product.Name))
                        errorMessageBuilder.AppendLine($"[{count}] Описание товара/услуги отсутствует");

                    if (product.Amount < 0)
                        errorMessageBuilder.AppendLine($"[{count}] Сумма должна быть положительной");

                    if (product.Count <= 0)
                        errorMessageBuilder.AppendLine($"[{count}] Количество должно быть больше нуля");

                    count++;
                });
            }
        }
    }
}
