﻿using System;
using Clouds42.OnlineCashier.Infrastructure.IModels;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business
{
    /// <summary>
    ///     Информация о чеке
    /// </summary>
    public class ChequeOnlineResponse : IOnlineCashierResponse
    {
        /// <inheritdoc />
        /// <summary>
        ///     Успешный результат
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <inheritdoc />
        /// <summary>
        ///     Ошибки
        /// </summary>
        public string Errors { get; set; }

        /// <summary>
        ///     Дата и время формирования документа
        /// </summary>
        public DateTime? DocumentDateTime { get; set; }

        /// <summary>
        ///     Текст чека
        /// </summary>
        public string ReceiptInfo { get; set; }

        /// <summary>
        ///     QR - код (Base64)
        /// </summary>
        public string QrCodeData { get; set; }

        /// <summary>
        ///     Формат QR кода
        /// </summary>
        public string QrCodeFormat { get; set; }

        /// <summary>
        ///     Фискальная информация
        /// </summary>
        public ChequeOnlineResponseFiscalData FiscalData { get; set; } = new();
    }
}
