﻿using Clouds42.OnlineCashier.Infrastructure.IModels;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business
{
    /// <summary>
    ///     Данные авторизации для ЧЕК кассы
    /// </summary>
    public class ChequeOnlineAuth : IOnlineCashierAuth
    {
        /// <summary>
        ///     Адрес API
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        ///     Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        ///     Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     Данные сертификата в Base64
        /// </summary>
        public string CertificateData { get; set; }

        /// <summary>
        ///     Пароль от сертификата
        /// </summary>
        public string CertificatePassword { get; set; }
    }
}
