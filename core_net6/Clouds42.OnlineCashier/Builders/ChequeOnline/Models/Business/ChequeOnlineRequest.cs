﻿using System;
using System.Collections.Generic;
using Clouds42.OnlineCashier.Infrastructure.IModels;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business
{
    public class ChequeOnlineRequest : IOnlineCashierRequest
    {
        /// <summary>
        ///     ID выставленного счета
        /// </summary>
        public Guid InvoiceId { get; set; }

        /// <summary>
        ///     Электронный адрес покупателя
        /// </summary>
        public string ClientEmail { get; set; }

        /// <summary>
        ///     Продукты
        /// </summary>
        public List<IOnlineCashierProduct> Products { get; set; }

        /// <summary>
        ///     Код налога
        /// </summary>
        public int TaxCode { get; set; }
    }
}
