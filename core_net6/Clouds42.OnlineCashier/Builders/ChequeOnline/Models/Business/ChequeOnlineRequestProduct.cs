﻿using Clouds42.OnlineCashier.Infrastructure.IModels;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business
{
    /// <summary>
    ///     Продукт
    /// </summary>
    public class ChequeOnlineRequestProduct : IOnlineCashierProduct
    {
        /// <summary>
        ///     Наименование продукта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Сумма
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        ///     Количество
        /// </summary>
        public int Count { get; set; }
    }
}
