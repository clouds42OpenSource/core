﻿using Newtonsoft.Json;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport
{
    public class ChequeOnlineResponseTransportDateTime
    {
        [JsonProperty(nameof(Date))]
        public ChequeOnlineResponseTransportDate Date { get; set; }

        [JsonProperty(nameof(Time))]
        public ChequeOnlineResponseTransportTime Time { get; set; }
    }
}
