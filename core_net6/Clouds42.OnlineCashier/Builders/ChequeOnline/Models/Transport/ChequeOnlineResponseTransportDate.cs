﻿using Newtonsoft.Json;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport
{
    public class ChequeOnlineResponseTransportDate
    {
        [JsonProperty(nameof(Day))]
        public int Day { get; set; }

        [JsonProperty(nameof(Month))]
        public int Month { get; set; }

        [JsonProperty(nameof(Year))]
        public int Year { get; set; }
    }
}
