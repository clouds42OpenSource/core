﻿using Newtonsoft.Json;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport
{
    public class ChequeOnlineResponseTransportResponse
    {
        /// <summary>
        ///      Код ошибки
        /// </summary>
        [JsonProperty(nameof(Error))]
        public int Error { get; set; }

        /// <summary>
        ///     Список сообщений, сформированных устройством при обработке запроса
        /// </summary>
        [JsonProperty(nameof(ErrorMessages))]
        public string[] ErrorMessages { get; set; }

        /// <summary>
        ///     Полный текст чека
        /// </summary>
        [JsonProperty("Text")]
        public string ReceiptFullText { get; set; }
    }
}
