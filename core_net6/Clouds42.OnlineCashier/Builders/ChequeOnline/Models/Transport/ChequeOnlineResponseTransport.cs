﻿using Newtonsoft.Json;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport
{
    /// <summary>
    ///     Транспортная модель ответа chekonline
    /// </summary>
    public class ChequeOnlineResponseTransport
    {
        /// <summary>
        ///     Ошибка запроса
        /// </summary>
        public string RequestError { get; set; }

        /// <summary>
        ///     Идентификатор клиента
        /// </summary>
        [JsonProperty(nameof(ClientId))]
        public string ClientId { get; set; }

        /// <summary>
        ///     Дата и время формирований документа
        /// </summary>
        [JsonProperty(nameof(Date))]
        public ChequeOnlineResponseTransportDateTime Date { get; set; }

        /// <summary>
        ///     Идентификация устройства, обработавшего запрос
        /// </summary>
        [JsonProperty(nameof(Device))]
        public ChequeOnlineResponseTransportDevice Device { get; set; }

        /// <summary>
        ///     Регистрационный номер устройства
        /// </summary>
        [JsonProperty(nameof(DeviceRegistrationNumber))]
        public string DeviceRegistrationNumber { get; set; }

        /// <summary>
        ///      Заводской номер устройства
        /// </summary>
        [JsonProperty(nameof(DeviceSerialNumber))]
        public string DeviceSerialNumber { get; set; }

        /// <summary>
        ///     Номер чека
        /// </summary>
        [JsonProperty(nameof(DocNumber))]
        public int DocNumber { get; set; }

        /// <summary>
        ///     Тип документа
        /// </summary>
        [JsonProperty(nameof(DocumentType))]
        public int DocumentType { get; set; }

        /// <summary>
        ///     Номер фискального накопителя, в котором сформирован документ
        /// </summary>
        [JsonProperty("FNSerialNumber")]
        public string FnSerialNumber { get; set; }

        /// <summary>
        ///     Номер фискального документа
        /// </summary>
        [JsonProperty(nameof(FiscalDocNumber))]
        public long FiscalDocNumber { get; set; }

        /// <summary>
        ///      Фискальный признак документа
        /// </summary>
        [JsonProperty(nameof(FiscalSign))]
        public long FiscalSign { get; set; }

        /// <summary>
        ///      Итог чека
        /// </summary>
        [JsonProperty(nameof(GrandTotal))]
        public long GrandTotal { get; set; }

        /// <summary>
        ///     Операция
        /// </summary>
        [JsonProperty(nameof(Path))]
        public string Path { get; set; }

        /// <summary>
        ///     QR-код чека
        /// </summary>
        [JsonProperty("QR")]
        public string QrCode { get; set; }
        
        /// <summary>
        ///     Уникальный ID запроса
        /// </summary>
        [JsonProperty(nameof(RequestId))]
        public string RequestId { get; set; }
        
        /// <summary>
        ///     Ответ
        /// </summary>
        [JsonProperty(nameof(Response))]
        public ChequeOnlineResponseTransportResponse Response { get; set; }

        /// <summary>
        ///     Результат
        /// </summary>
        [JsonProperty(nameof(Responses))]
        public ChequeOnlineResponseTransportResponses[] Responses { get; set; }
    }
}
