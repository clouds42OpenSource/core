﻿using Newtonsoft.Json;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport
{
    public class ChequeOnlineResponseTransportResponses
    {
        /// <summary>
        ///     Операция
        /// </summary>
        [JsonProperty(nameof(Path))]
        public string Path { get; set; }

        /// <summary>
        ///     Ответ
        /// </summary>
        [JsonProperty(nameof(Response))]
        public ChequeOnlineResponseTransportResponse Response { get; set; }
    }
}
