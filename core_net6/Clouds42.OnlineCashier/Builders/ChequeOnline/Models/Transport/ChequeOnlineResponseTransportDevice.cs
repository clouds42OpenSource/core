﻿using Newtonsoft.Json;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport
{
    public class ChequeOnlineResponseTransportDevice
    {
        /// <summary>
        ///     Заводской номер устройства
        /// </summary>
        [JsonProperty(nameof(Name))]
        public string Name { get; set; }

        /// <summary>
        ///     Адрес устройства
        /// </summary>
        [JsonProperty(nameof(Address))]
        public string Address { get; set; }
    }
}
