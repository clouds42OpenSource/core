﻿using Newtonsoft.Json;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport
{
    public class ChequeOnlineResponseTransportTime
    {
        [JsonProperty(nameof(Hour))]
        public int Hour { get; set; }

        [JsonProperty(nameof(Minute))]
        public int Minute { get; set; }

        [JsonProperty(nameof(Second))]
        public int Second { get; set; }
    }
}
