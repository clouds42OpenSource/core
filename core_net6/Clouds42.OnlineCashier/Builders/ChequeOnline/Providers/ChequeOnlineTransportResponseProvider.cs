﻿using System;
using System.Linq;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport;
using QRCoder;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Providers
{
    /// <summary>
    ///     Конвертер транспортного ответа в бизнес
    /// </summary>
    internal class ChequeOnlineTransportResponseProvider
    {
        /// <summary>
        ///     Путь к объекту о закрытии документа
        /// </summary>
        private const string PathCloseDocument = "/fr/api/v2/CloseDocument";

        /// <summary>
        ///     Формат QR кода
        /// </summary>
        private const string QrCodeFormat = "Jpg";

        /// <summary>
        ///     Конвертация транспортной модели в бизнес модель
        /// </summary>
        /// <param name="transportModel">Транспортная модель</param>
        public ChequeOnlineResponse ToBusinessModel(ChequeOnlineResponseTransport transportModel)
        {
            if (transportModel == null)
                return new ChequeOnlineResponse { Errors = "ChequeOnlineResponseTransport null" };

            if (transportModel.Response == null)
                return new ChequeOnlineResponse { Errors = "ChequeOnlineResponseTransport.Response null" };

            if (transportModel.Response.Error != 0)
                return new ChequeOnlineResponse { Errors = string.Join("\r\n", transportModel.Response.ErrorMessages) };

            if (transportModel.Responses == null)
                return new ChequeOnlineResponse { Errors = "ChequeOnlineResponseTransport.Responses null" };

            var response = transportModel.Responses.FirstOrDefault(w => w.Path == PathCloseDocument)?.Response;
            if (string.IsNullOrEmpty(response?.ReceiptFullText))
                return new ChequeOnlineResponse { Errors = "Отсутствует информация о закрытии чека" };

            var qrCodeData = BuildQrCodeByParams(transportModel.QrCode);
            if (string.IsNullOrEmpty(qrCodeData))
                return new ChequeOnlineResponse { Errors = "QR код пустой" };

            return new ChequeOnlineResponse
            {
                DocumentDateTime = new DateTime(
                    transportModel.Date.Date.Year, transportModel.Date.Date.Month, transportModel.Date.Date.Day,
                    transportModel.Date.Time.Hour, transportModel.Date.Time.Minute, transportModel.Date.Time.Second),
                ReceiptInfo = response.ReceiptFullText,
                QrCodeData = qrCodeData,
                QrCodeFormat = QrCodeFormat,
                FiscalData = new ChequeOnlineResponseFiscalData
                {
                    Number = transportModel.FiscalDocNumber.ToString(),
                    Serial = transportModel.FnSerialNumber,
                    Sign = transportModel.FiscalSign.ToString()
                },
                IsSuccess = true
            };
        }

        /// <summary>
        ///     Получить QR код по параметрам
        /// </summary>
        /// <param name="qrInfo">Параметры QR кода</param>
        private static string BuildQrCodeByParams(string qrInfo)
        {
            using var qrGenerator = new QRCodeGenerator();
            using var qrCodeData = qrGenerator.CreateQrCode(qrInfo, QRCodeGenerator.ECCLevel.Q);
            using var qrCode = new PngByteQRCode(qrCodeData);
            var qrCodeImage = qrCode.GetGraphic(20);

            return Convert.ToBase64String(qrCodeImage);
        }
    }
}
