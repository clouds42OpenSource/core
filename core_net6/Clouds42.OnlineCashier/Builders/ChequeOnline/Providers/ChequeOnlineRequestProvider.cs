﻿using System;
using System.Security.Cryptography.X509Certificates;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport;
using Clouds42.WebKit;
using Newtonsoft.Json;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Providers
{
    /// <summary>
    ///     Провайдер для http запросов
    /// </summary>
    public class ChequeOnlineRequestProvider(IHttpWebRequestProvider httpWebRequest) : IChequeOnlineRequestProvider
    {
        /// <summary>
        ///     Отправить запрос в chekonline
        /// </summary>
        /// <param name="authData">Данные авторизации</param>
        /// <param name="requestData">Модель запроса</param>
        public ChequeOnlineResponseTransport SendRequest(ChequeOnlineAuth authData, ChequeOnlineRequestTransport requestData)
        {
            var requestJson = JsonConvert.SerializeObject(requestData);
            var certificateData = Convert.FromBase64String(authData.CertificateData);

            var certificate = new X509Certificate2(certificateData, authData.CertificatePassword);

            var result = httpWebRequest.SendPostJson(authData.Url, requestJson, certificate, null, out var success);
            return !success ? new ChequeOnlineResponseTransport {RequestError = result} : JsonConvert.DeserializeObject<ChequeOnlineResponseTransport>(result);
        }
    }
}
