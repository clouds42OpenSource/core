﻿using System;
using System.Linq;
using System.Text;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Providers
{
    /// <summary>
    ///     Конвертер бизнес запроса в транспортный
    /// </summary>
    internal class ChequeOnlineTransportRequestProvider
    {
        /// <summary>
        ///     Тип документа: Приход
        /// </summary>
        private const int DocumentType = 0;

        /// <summary>
        ///     Признак получения полного ответа
        /// </summary>
        private const bool FullResponse = true;

        /// <summary>
        ///     Признак способа расчёта: Полная предварительная оплата до момента передачи предмета расчёта
        /// </summary>
        private const int MethodCalculation = 1;

        /// <summary>
        ///     Признак предмета расчёта: об оказываемой услуге
        /// </summary>
        private const int SubjectCalculation = 4;

        /// <summary>
        ///     Конвертация суммы в копейки
        /// </summary>
        /// <param name="amount">Сумма</param>
        private static long ConvertAmount(decimal amount) => Convert.ToInt64(amount * 100);

        /// <summary>
        ///     Конвертация количества в QTY
        /// </summary>
        /// <param name="count">Количество</param>
        private static long ConvertCountToQty(int count) => count * 1000;

        /// <summary>
        ///     Конвертация в кодировку CP866
        /// </summary>
        /// <param name="string">Входящая строка</param>
        private static string EncodeToCp866(string @string)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding866 = Encoding.GetEncoding(866);
            var bytes = Encoding.UTF8.GetBytes(@string);
            var convertBytes = Encoding.Convert(Encoding.UTF8, encoding866, bytes);
            return encoding866.GetString(convertBytes);
        }

        /// <summary>
        ///     Конвертация бизнес модели в транспортную модель
        /// </summary>
        /// <param name="request">Бизнес модель</param>
        internal ChequeOnlineRequestTransport ToTransportRequest(ChequeOnlineRequest request)
        {
            var lines = request.Products.Select(product => new ChequeOnlineRequestTransportLines
            {
                Description = EncodeToCp866(product.Name),
                Qty = ConvertCountToQty(1),
                Price = ConvertAmount(product.Amount),
                TaxId = request.TaxCode,
                LineAttribute = SubjectCalculation,
                PayAttribute = MethodCalculation
            }).ToList();

            return new ChequeOnlineRequestTransport
            {
                Device = "auto",
                DocumentType = DocumentType,
                FullResponse = FullResponse,
                PhoneOrEmail = request.ClientEmail,
                RequestId = request.InvoiceId.ToString(),
                NonCash = [lines.Sum(w => w.Price)],
                Lines = lines
            };
        }
    }
}
