﻿using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport;

namespace Clouds42.OnlineCashier.Builders.ChequeOnline.Providers
{
    /// <summary>
    ///     Провайдер для http запросов
    /// </summary>
    public interface IChequeOnlineRequestProvider
    {
        /// <summary>
        ///     Отправить запрос в chekonline
        /// </summary>
        /// <param name="authData">Данные авторизации</param>
        /// <param name="requestData">Модель запроса</param>
        ChequeOnlineResponseTransport SendRequest(ChequeOnlineAuth authData, ChequeOnlineRequestTransport requestData);
    }
}
