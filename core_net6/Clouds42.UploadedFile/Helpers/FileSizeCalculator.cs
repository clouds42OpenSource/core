﻿using System;
using System.IO;

namespace Clouds42.UploadedFiles.Helpers
{
    /// <summary>
    /// Калькулятор размера файлов
    /// </summary>
    public class FileSizeCalculator
    {
        /// <summary>
        /// Получить размер файла в мегабайтах
        /// </summary>
        /// <param name="sourceFileName">Путь к файлу</param>
        /// <returns>Размер файла в мегабайтах</returns>
        public long GetFileSizeInMegabytes(string sourceFileName)
            => Convert.ToInt64(GetFileSize(sourceFileName) / 1024f / 1024f);

        /// <summary>
        /// Получить размер файла
        /// </summary>
        /// <param name="sourceFileName">Путь к файлу</param>
        /// <returns>Размер файла</returns>
        private static long GetFileSize(string sourceFileName)
        {
            var fileInfo = new FileInfo(sourceFileName);
            try
            {
                return fileInfo.Length;
            }

            catch (Exception)
            {
                if (Path.GetExtension(fileInfo.FullName) == ".1CD")
                    throw;
            }

            return 0;
        }
    }
}
