﻿using System;
using System.IO;
using System.Threading.Tasks;
using Clouds42.Logger;

namespace Clouds42.UploadedFiles.Helpers
{
    /// <summary>
    /// Хэлпер для проверки использования файла
    /// </summary>
    public static class FileUseChecker
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Проверить использование файла
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        /// <returns>true - если файл используется</returns>
        public static bool CheckFileUsage(string filePath)
        {
            try
            {
                var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Write);
                fileStream.Dispose();
                return false;
            }
            catch (Exception ex)
            {
                _logger.Warn($"При проверке активных сессий обнаружена ошибка {ex.Message}");
                return true;
            }
        }

        /// <summary>
        /// Ожидать пока файл используется
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        /// <param name="sleepTimeInSeconds">Время задержки перед след. проверкой в секундах</param>
        /// <param name="waitTimeInSeconds">Время ожидания выполнения проверки в секундах</param>
        public static void WaitWhileFileUsing(string filePath, int sleepTimeInSeconds = 10, int waitTimeInSeconds = 900)
        {
            var endTime = DateTime.Now.AddSeconds(waitTimeInSeconds);

            while (true)
            {
                if (!File.Exists(filePath))
                    return;

                if (!CheckFileUsage(filePath))
                    return;

                Task.Delay(TimeSpan.FromSeconds(sleepTimeInSeconds)).Wait();

                if (DateTime.Now < endTime)
                {
                    continue;
                }

                var errorMessage = $"Файл {filePath} используется в течении {waitTimeInSeconds} секунд.";
                _logger.Warn(errorMessage);
                throw new InvalidOperationException(errorMessage);
            }
        }
    }
}
