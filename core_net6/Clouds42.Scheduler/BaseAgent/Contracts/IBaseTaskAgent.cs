﻿using Clouds42.Scheduler.Agent.Contracts;

namespace Clouds42.Scheduler.BaseAgent.Contracts
{
    public interface IBaseTaskAgent
    {
        void Start(TasksAgentOptions options);
        void Stop();
    }
}
