﻿using Clouds42.Scheduler.Agent.Contracts;
using Clouds42.Scheduler.BaseAgent.Contracts;

namespace Clouds42.Scheduler.BaseAgent
{
    public abstract class BaseTaskAgent : IBaseTaskAgent
    {
        protected readonly ITasksAgent TasksAgent;

        protected BaseTaskAgent(ITasksAgent tasksAgent)
        {
            if (tasksAgent == null)
                throw new ArgumentNullException(nameof(tasksAgent));

            TasksAgent = tasksAgent;
        }

        public abstract void Start(TasksAgentOptions options);

        public void Stop()
        {
            var taskName = TaskName;
            TasksAgent.UnscheduledTask(taskName);

            if (TasksAgent.GetSchedules().Any(s => s.Name == taskName))
                TasksAgent.UnscheduledTask(taskName);
        }

        protected string TaskName => $"{GetType().Name}";

    }
}
