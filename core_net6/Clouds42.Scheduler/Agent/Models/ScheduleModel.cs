﻿namespace Clouds42.Scheduler.Agent.Models
{
    public class ScheduleModel
    {
        public DateTimeOffset NextRun { get; set; }

        public string Name { get; set; }
    }
}
