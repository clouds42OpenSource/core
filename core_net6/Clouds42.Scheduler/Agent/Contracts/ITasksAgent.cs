﻿using Clouds42.Scheduler.Agent.Models;

namespace Clouds42.Scheduler.Agent.Contracts
{
    public interface ITasksAgent
    {
        void StartInfiniteTaskImmediately(string taskName, TimeSpan updatePeriod, Type type);
        void StartInfiniteAsyncTaskImmediately(string taskName, TimeSpan updatePeriod, Type type);
        void StartInfiniteAsyncTaskAfter(string taskName, TimeSpan delay, TimeSpan updatePeriod, Type type);
        void StartInfiniteTaskAfter(string taskName, TimeSpan delay, TimeSpan updatePeriod, Type type);
        void StartInfiniteTaskSeveralTimes(string taskName, IEnumerable<TimeSpan> severalStartsTime, TimeSpan updatePeriod, Type type);
        void StartInfiniteTaskAtTime(string taskName, TimeSpan startTime, TimeSpan updatePeriod, Type type);
        void UnscheduledTask(string taskName);

        IEnumerable<ScheduleModel> GetSchedules();
    }
}
