﻿using Clouds42.Scheduler.Agent.Contracts;
using Clouds42.Scheduler.Agent.Models;
using Quartz;
using Quartz.Impl.Matchers;

namespace Clouds42.Scheduler.Agent
{
    public class TasksAgent : ITasksAgent
    {
        private readonly object _lockObject = new();
        private readonly ISchedulerFactory _schedulerFactory;
        private const string TriggerNamePattern = "{0}-Trigger";

        public TasksAgent(ISchedulerFactory schedulerFactory)
        {
            if (schedulerFactory == null)
                throw new ArgumentNullException(nameof(schedulerFactory));

            _schedulerFactory = schedulerFactory;
        }

        public void StartInfiniteTaskImmediately(string taskName, TimeSpan updatePeriod, Type type)
            => StartInfiniteAction(taskName, TimeSpan.Zero, updatePeriod, type);

        public void StartInfiniteAsyncTaskImmediately(string taskName, TimeSpan updatePeriod, Type type)
            => StartInfiniteFunction(taskName, TimeSpan.Zero, updatePeriod, type);

        public void StartInfiniteTaskAtTime(string taskName, TimeSpan startTime, TimeSpan updatePeriod, Type type)
            => StartInfiniteFunction(taskName, CalculateBeforeStartDelta(startTime), updatePeriod, type);

        public void StartInfiniteTaskSeveralTimes(string taskName, IEnumerable<TimeSpan> severalStartsTime, TimeSpan updatePeriod,
            Type type)
        {
            foreach (var startTime in severalStartsTime)
            {
                var beforeStartDelta = CalculateBeforeStartDelta(startTime);
                taskName = $"{taskName} at {startTime}";
                StartInfiniteFunction(taskName, beforeStartDelta, updatePeriod, type);
            }
        }

        private ITrigger CreateTrigger(JobKey jobKey, string triggerName, TimeSpan delay, TimeSpan updatePeriod)
        {
            return TriggerBuilder.Create().WithIdentity(triggerName)
                .ForJob(jobKey)
                .StartAt(DateBuilder.FutureDate(GetMilliseconds(delay), IntervalUnit.Millisecond))
                .WithSimpleSchedule(x => x
                    .WithInterval(updatePeriod)
                    .RepeatForever())
                .Build();
        }
        public void StartInfiniteTaskAfter(
            string taskName,
            TimeSpan delay,
            TimeSpan updatePeriod,
            Type type)
        {
            lock (_lockObject)
            {
                var triggerName = string.Format(TriggerNamePattern, taskName);
                var jobKey = new JobKey(taskName);

                var scheduler = _schedulerFactory.GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();

                var result = CheckAndUpdateTrigger(scheduler, jobKey, triggerName, delay, updatePeriod).GetAwaiter().GetResult();

                if (result)
                    return;

                var job = JobBuilder.Create(type)
                    .WithIdentity(jobKey)
                    .Build();

                var trigger = CreateTrigger(jobKey, triggerName, delay, updatePeriod);

                scheduler.ScheduleJob(job, trigger).GetAwaiter().GetResult();
            }
        }

        public void StartInfiniteAsyncTaskAfter(string taskName, TimeSpan delay, TimeSpan updatePeriod, Type type)
        {
            StartInfiniteFunction(taskName, delay, updatePeriod, type);
        }

        public void UnscheduledTask(string taskName)
        {
            var scheduler = _schedulerFactory.GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();

            scheduler.UnscheduleJob(new TriggerKey(string.Format(TriggerNamePattern, taskName))).ConfigureAwait(false)
                .GetAwaiter().GetResult();
        }

        public IEnumerable<ScheduleModel> GetSchedules()
        {
            var scheduler = _schedulerFactory.GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();

            var schedules = new List<ScheduleModel>();

            var allTriggerKeys = scheduler.GetTriggerKeys(GroupMatcher<TriggerKey>.AnyGroup()).ConfigureAwait(false).GetAwaiter().GetResult();
            foreach (var triggerKey in allTriggerKeys)
            {
                var triggerDetails = scheduler.GetTrigger(triggerKey).ConfigureAwait(false).GetAwaiter().GetResult();

                schedules.Add(new ScheduleModel
                {
                    Name = triggerDetails?.JobKey.Name,
                    NextRun = triggerDetails?.GetNextFireTimeUtc().GetValueOrDefault() ?? DateTimeOffset.UtcNow,
                });
            }

            return schedules;
        }

        private void StartInfiniteAction(string taskName, TimeSpan startTime, TimeSpan updatePeriod, Type type)
        {
            StartInfiniteTaskAfter(taskName, startTime, updatePeriod, type);
        }

        private void StartInfiniteFunction(string taskName, TimeSpan delay, TimeSpan updatePeriod, Type type)
        {
            StartInfiniteTaskAfter(taskName, delay, updatePeriod, type);
        }

        private static int GetMilliseconds(TimeSpan timeSpan)
        {
            return (int)timeSpan.TotalMilliseconds;
        }

        private async Task<bool> CheckAndUpdateTrigger(IScheduler scheduler, JobKey jobKey, string triggerName, TimeSpan delay, TimeSpan updatePeriod)
        {
            if (await scheduler.CheckExists(jobKey))
            {
                var oldTrigger = await scheduler.GetTrigger(new TriggerKey(triggerName));

                var oldUpdatePeriod = oldTrigger?.GetNextFireTimeUtc() - oldTrigger?.GetPreviousFireTimeUtc();

                if (oldUpdatePeriod == updatePeriod)
                    return true;

                var newTrigger = CreateTrigger(jobKey, triggerName, delay, updatePeriod);
                await scheduler.RescheduleJob(newTrigger.Key, newTrigger);

                return true;
            }

            return false;
        }

        private static TimeSpan CalculateBeforeStartDelta(TimeSpan startTime)
        {
            var nowInUtc = DateTime.UtcNow;
            var nextPromotionDateTime = nowInUtc.Date.Add(startTime);

            if (nowInUtc > nextPromotionDateTime)
                nextPromotionDateTime = nextPromotionDateTime.AddDays(1);

            return nextPromotionDateTime.Subtract(nowInUtc);
        }
    }
}
