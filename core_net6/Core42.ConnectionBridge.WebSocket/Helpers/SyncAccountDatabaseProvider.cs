﻿using System;
#if !DEBUG
using System.Net.WebSockets;
#endif
using System.Threading.Tasks;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Repositories.Interfaces.Common;
using Core42.ConnectionBridge.WebSocketServer.WebSockets;
using Core42.WebSocketCommands.Implementation;

namespace Core42.ConnectionBridge.WebSocketServer.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="socketHandler"></param>
    /// <param name="unitOfWork"></param>
    public class SyncAccountDatabaseProvider(ChangeDatabaseHandler socketHandler, IUnitOfWork unitOfWork)
    {
        /// <summary>
        /// Создать задачу уведомления открытых веб сокетных соединений о создание новой базы.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        public Task CreateDatabaseHandleTask(Guid accountDatabaseId)
        {
            return CreateTaskProcessOpenConnections(accountDatabaseId, async socketContainer =>
            {
                var accountDatabase = await unitOfWork.DatabasesRepository.GetAccountDatabasesForUserById(accountDatabaseId, socketContainer.AccountUserId);
                
                if (accountDatabase == null)
                    return;

                await socketHandler.ReceiveCommandAsync(socketContainer.WebSocket,
                    new CreateDatabaseCommand {AccountDatabase = accountDatabase.ConvertToDto()});
            });
        }

        /// <summary>
        /// Создать задачу уведомления открытых веб сокетных соединений о удаление базы.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        public Task CreateDeleteDatabaseHandleTask(Guid accountDatabaseId)
        {
            return Task.Factory.StartNew(async () =>
            {
                var webSocketConnectionsId = await unitOfWork.WebSocketConnectionRepository.GetAllOpenConnections();

                foreach (var connectionId in webSocketConnectionsId)
                {
                    var socketContainer = socketHandler.ConnectionsStorage.GetSocketById(connectionId);

                    if (socketContainer == null)
                    {
                        CloseConnectionInDb(connectionId);
                        continue;
                    }

                    await socketHandler.ReceiveCommandAsync(socketContainer.WebSocket, new DeleteDatabaseCommand { AccountDatabaseId = accountDatabaseId });
                }
            });
        }

        /// <summary>
        /// Создать задачу уведомления открытых веб сокетных соединений об обновление базы.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        public Task CreateUpdateDatabaseHandleTask(Guid accountDatabaseId)
        {
            return CreateTaskProcessOpenConnections(accountDatabaseId, async socketContainer =>
            {
                var accountDatabase = await unitOfWork.DatabasesRepository.GetAccountDatabasesForUserById(accountDatabaseId, socketContainer.AccountUserId);
                
                if (accountDatabase == null)
                    return;

                await socketHandler.ReceiveCommandAsync(socketContainer.WebSocket, new UpdateDatabaseCommand { AccountDatabase = accountDatabase.ConvertToDto() });
            });
        }

        /// <summary>
        /// Создать задачу уведомления открытых веб сокетных соединений о придоставление доступа к базе.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        public Task CreateGrandAccessDatabaseHandleTask(Guid accountDatabaseId)
        {
            return CreateTaskProcessOpenConnections(accountDatabaseId, async socketContainer =>
            {
                var accountDatabase = await unitOfWork.DatabasesRepository.GetAccountDatabasesForUserById(accountDatabaseId, socketContainer.AccountUserId);
                
                if (accountDatabase == null)
                    return;

                await socketHandler.ReceiveCommandAsync(socketContainer.WebSocket, new GrandAccessDatabaseCommand { AccountDatabase = accountDatabase.ConvertToDto() });
            });
        }

        /// <summary>
        /// Создать задачу уведомления открытых веб сокетных соединений о удаление пользовтаельского доступа у базы.
        /// </summary>
        /// <param name="model">Номера информационной базы и пользователя.</param>
        public Task CreateDeleteAccessDatabaseHandleTask(DeleteAccessDatabaseRequestDto model)
        {
            return Task.Factory.StartNew(async () =>
            {
                var webSocketConnectionsId = await unitOfWork.WebSocketConnectionRepository.GetOpenUserConnectionsWhichAvailableDatabase(model.AccountUserId);

                foreach (var connectionId in webSocketConnectionsId)
                {
                    var socket = socketHandler.ConnectionsStorage.GetSocketById(connectionId);

                    if (socket == null)
                    {
                        CloseConnectionInDb(connectionId);
                        continue;
                    }

                    await socketHandler.ReceiveCommandAsync(socket.WebSocket, new DeleteAccessDatabaseCommand { AccountDatabaseId = model.AccountDatabaseId });
                }
            });
        }

        /// <summary>
        /// Создать задачу уведомления откытых веб сокет соединений.
        /// </summary>
        /// <param name="accountDatabaseId">Номер информационной базы.</param>
        /// <param name="receiveCommandAction">Функций передечи выполнения команды открытым соединениям.</param>
        /// <returns></returns>
        private Task CreateTaskProcessOpenConnections(Guid accountDatabaseId, Action<WebSocketContainer> receiveCommandAction)
        {
            return Task.Factory.StartNew(async () =>
            {
                var webSocketConnectionsId = await unitOfWork.WebSocketConnectionRepository.GetOpenConnectionsWhichAvailableDatabase(accountDatabaseId);

                foreach (var connectionId in webSocketConnectionsId)
                {
                    var socket = socketHandler.ConnectionsStorage.GetSocketById(connectionId);

                    if (socket == null)
                    {
                        CloseConnectionInDb(connectionId);
                        continue;
                    }

                    receiveCommandAction(socket);
                }
            });
        }

        private void CloseConnectionInDb(string connectionId)
        {
            var connection = unitOfWork.WebSocketConnectionRepository.GetFirst(x => x.WebSocketId == connectionId);

            if (connection == null)
                return;

            connection.ConnectionCloseeDateTime = DateTime.Now;
            unitOfWork.WebSocketConnectionRepository.Update(connection);
            unitOfWork.Save();
        }

    }
}
