﻿using System;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Core42.ConnectionBridge.WebSocketServer.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Core42.ConnectionBridge.WebSocketServer.Controllers
{

    /// <summary>
    /// Контроллер обрабатывающий запросы на изменение объектов ифнормационной базы.
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ChangesAccountDatabaseController(
        [FromServices] SyncAccountDatabaseProvider syncAccountDatabaseProvider)
        : ControllerBase
    {
        /// <summary>
        /// Обаботать команду содание новой базы в облаке.
        /// </summary>
        [HttpPost]
        public IActionResult CreateDatabaseHandle(SimpleRequestModelDto<Guid> accountDatabaseModel)
        {
            syncAccountDatabaseProvider.CreateDatabaseHandleTask(accountDatabaseModel.Value);
            return Ok();
        }

        /// <summary>
        /// Обаботать команду обновления базы в облаке.
        /// </summary>
        [HttpPost]
        public IActionResult UpdateDatabaseHandle(SimpleRequestModelDto<Guid> accountDatabaseModel)
        {
            syncAccountDatabaseProvider.CreateUpdateDatabaseHandleTask(accountDatabaseModel.Value);
            return Ok();
        }

        /// <summary>
        /// Обаботать команду удаления базы с облака.
        /// </summary>
        [HttpPost]
        public IActionResult DeleteDatabaseHandle(SimpleRequestModelDto<Guid> accountDatabaseModel)
        {
            syncAccountDatabaseProvider.CreateDeleteDatabaseHandleTask(accountDatabaseModel.Value);
            return Ok();
        }

        /// <summary>
        /// Обаботать команду предоставления доступа к базе.
        /// </summary>
        [HttpPost]
        public IActionResult GrandAccessDatabaseHandle(SimpleRequestModelDto<Guid> accountDatabaseModel)
        {
            syncAccountDatabaseProvider.CreateGrandAccessDatabaseHandleTask(accountDatabaseModel.Value);
            return Ok();
        }

        /// <summary>
        /// Обаботать команду удаления пользовательского доступа у базы.
        /// </summary>
        [HttpPost]
        public IActionResult DeleteAccessDatabaseHandle(DeleteAccessDatabaseRequestDto model)
        {
            syncAccountDatabaseProvider.CreateDeleteAccessDatabaseHandleTask(model);
            return Ok();
        }
    }
}