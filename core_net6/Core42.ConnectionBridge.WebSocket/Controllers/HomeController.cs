﻿using Microsoft.AspNetCore.Mvc;

namespace Core42.ConnectionBridge.WebSocketServer.Controllers
{
    /// <summary>
    /// Основной контроллер бриджа
    /// </summary>
    [Route("")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private const string ServiceIsRunningMessage = "NET 6 Connection Bridge Running!";

        /// <summary>
        /// Главная страница
        /// </summary>
        /// <returns>Сообщение об успешной работе приложения</returns>
        [HttpGet]
        public string Index()
            => ServiceIsRunningMessage;
    }
}
