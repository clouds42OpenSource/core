﻿using Core42.ConnectionBridge.WebSocketServer.WebSockets;
using Microsoft.AspNetCore.Mvc;

namespace Core42.ConnectionBridge.WebSocketServer.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DiagnosticController : ControllerBase
    {
        private readonly WebSocketConnectionStorage _webSocketConnectionStorage;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="webSocketConnectionStorage"></param>
        public DiagnosticController(WebSocketConnectionStorage webSocketConnectionStorage)
        {
            _webSocketConnectionStorage = webSocketConnectionStorage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new
            {
                Connections = _webSocketConnectionStorage.GetAllConnectionsIds()
            });
        }
    }
}
