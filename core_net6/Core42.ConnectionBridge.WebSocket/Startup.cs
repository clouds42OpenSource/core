﻿using System;
using System.IO;
using System.Reflection;
using Clouds42.BLL.Common.Access;
using Clouds42.Configurations;
using Clouds42.DomainContext;
using Clouds42.HandlerException;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Jwt;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using Core42.ConnectionBridge.WebSocketServer.Extensions;
using Core42.ConnectionBridge.WebSocketServer.Helpers;
using Core42.ConnectionBridge.WebSocketServer.Internal;
using Core42.ConnectionBridge.WebSocketServer.WebSockets;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.OpenApi.Models;
using Repositories;

namespace Core42.ConnectionBridge.WebSocketServer
{

    public class Startup(IWebHostEnvironment env)
    {
        public IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .AddGcpKeyValueSecrets(env.EnvironmentName)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
            .AddEnvironmentVariables()
            .Build();

        public void ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;
            services.AddClouds42DbContext(Configuration);
            services.AddWebSocketManager();
            services.AddControllers();
            services.AddHttpContextAccessor();
            services.AddTransient(_ => services);
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IUserPrincipalHelper, UserPrincipalHelper>();
            services.AddTransient<SyncAccountDatabaseProvider>();
            services.AddSingleton<ILogger42, SerilogLogger42>();
            services.AddSerilogWithElastic(Configuration);
            services.AddHandlerException();
            services.AddTelegramBots(Configuration);

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = nameof(ConnectionBridge)
                });

                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });

            services.AddMemoryCache();
            services.AddHttpClient();
            services.AddJwt();
            services.AddSingleton<BridgeAccessProvider>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            HandlerException42.Init(app.ApplicationServices);
            ConfigurationHelper.SetConfiguration(Configuration);

            var wsOptions = new WebSocketOptions
            {
                KeepAliveInterval = TimeSpan.FromSeconds(60)
            };

            app.ApplicationServices.GetRequiredService<ILogger42>().Info("App starting");

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseWebSockets(wsOptions);
            app.MapWebSocketManager("/changesAccountDatabase", serviceProvider.GetService<ChangeDatabaseHandler>());

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
