﻿using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Core42.ConnectionBridge.WebSocketServer.WebSockets
{
    public class WebSocketManagerMiddleware(
        RequestDelegate requestDelegate,
        WebSocketHandler webSocketHandler,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
    {
        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
                return;

            var socket = await context.WebSockets.AcceptWebSocketAsync();

            await webSocketHandler.OnConnected(socket);

            await Receive(socket, async (result, buffer) =>
            {
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    logger.Info("Состояние web-socket'a - 'Закрытый'. Производим закрытие соединения (удаляем веб-сокет из хранилища)");
                    await webSocketHandler.OnDisconnected(socket);
                }
            });
            await requestDelegate(context);
        }

        private async Task Receive(WebSocket socket, Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            try
            {
                logger.Info(
                    $"Receive. Производим рассылку сообщения web-socket'у - {JsonConvert.SerializeObject(socket)}");
                var buffer = new byte[1024 * 4];

                while (socket.State == WebSocketState.Open)
                {
                    logger.Info("Receive. Состояние web-socket'a - 'Открытый'. Производим отправку сообщения");
                    var result = await socket.ReceiveAsync(buffer: new ArraySegment<byte>(buffer),
                        cancellationToken: CancellationToken.None);
                    logger.Info($"Receive. Получили ответ {JsonConvert.SerializeObject(result)}");
                    handleMessage(result, buffer);
                }
            }

            catch (WebSocketException wEx)
            {
                logger.Warn(wEx, $"Receive. Нештатное завершение соединения. Web-socket - {JsonConvert.SerializeObject(socket)}");
            }

            catch (Exception ex)
            {
               
                handlerException.Handle(ex, "[Receive. Необработанное исключение]");
            }

            finally
            {
                ReleaseSocket(socket);
            }
        }

        private void ReleaseSocket(WebSocket webSocket)
        {
            try
            {
                if (webSocket.State is not WebSocketState.Aborted)
                    return;

                logger.Info("ReleaseSocket. Состояние web-socket'a - 'Aborted'. Производим удаление из базы и хранилища");
                CloseConnectionInDb(webSocketHandler.ConnectionsStorage.GetId(webSocket));
                webSocketHandler.ConnectionsStorage.RemoveSocket(webSocket);

            }

            catch (WebSocketException wEx)
            {
                logger.Warn(wEx, $"ReleaseSocket. Нештатное завершение соединения. Web-socket - {JsonConvert.SerializeObject(webSocket)}");
            }

            catch (Exception ex)
            {
                logger.Warn(ex, "[ReleaseSocket. Необработанное исключение]");
            }
            
        }
        private void CloseConnectionInDb(string connectionId)
        {
            var connection = unitOfWork.WebSocketConnectionRepository.GetFirst(x => x.WebSocketId == connectionId);

            if (connection == null)
                return;

            connection.ConnectionCloseeDateTime = DateTime.Now;
            unitOfWork.WebSocketConnectionRepository.Update(connection);
            unitOfWork.Save();
        }
    }

}
