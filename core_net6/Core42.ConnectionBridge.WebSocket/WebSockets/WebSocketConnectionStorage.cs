﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.WebSockets;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.ConnectionBridge.WebSocketServer.Internal;

namespace Core42.ConnectionBridge.WebSocketServer.WebSockets
{
    /// <summary>
    /// Хранилище веб сокетных подключений
    /// </summary>
    public class WebSocketConnectionStorage(
        BridgeAccessProvider accessProvider,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
    {
        private readonly ConcurrentDictionary<string, WebSocketContainer> _sockets = new();

        /// <summary>
        /// Получить список всех идентификаторов подключенных соединений.
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllConnectionsIds()
            => _sockets.Select(s => s.Key).ToList();
        
        /// <summary>
        /// Получить сокет по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор сокета.</param>
        public WebSocketContainer GetSocketById(string id)
        {
            return _sockets.All(p => p.Key != id) ? null : _sockets.First(p => p.Key == id).Value;
        }        

        /// <summary>
        /// Получить номер сокетного подключения
        /// </summary>
        /// <param name="socket">Сокет.</param>
        public string GetId(WebSocket socket)
        {
            return _sockets.First(p => p.Value.WebSocket == socket).Key;
        }

        /// <summary>
        /// Дбавить сокер в хранилище подключений.
        /// </summary>
        /// <param name="socket">Подключаемый сокет.</param>
        /// <returns>Идентификатор сокета в хранилще.</returns>
        public string AddSocket(WebSocket socket)
        {
            var sId = CreateConnectionId();
            while (!_sockets.TryAdd(sId, new  WebSocketContainer(socket, accessProvider.GetUserIdOrThrowException())))
            {
                sId = CreateConnectionId();
            }

            unitOfWork.WebSocketConnectionRepository.Insert(new WebSocketConnection
            {
                Id = Guid.NewGuid(),
                AccountUserId = accessProvider.GetUserIdOrThrowException(),
                WebSocketId = sId
            });
            unitOfWork.Save();

            return sId;
        }

        /// <summary>
        /// Удалить сокет из хранилища.
        /// </summary>
        /// <param name="id">Идентификатор сокета в хранилще.</param>
        public async Task RemoveSocket(string id)
        {
            try
            {
                _sockets.TryRemove(id, out var socket);
                await socket.WebSocket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure,
                    "Штатное завершение сеанса.", CancellationToken.None);
                CloseConnectionInDb(id);
            }
            catch (WebSocketException wEx)
            {
                logger.Warn(wEx, "RemoveSocket(string id). Ошибка закрытия соединения");
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Необработанное исключение ConnectionBridge] RemoveSocket(string id).");
            }

        }

        /// <summary>
        /// Удалить сокент из хранилища без закрытия соединения
        /// </summary>
        /// <param name="socket"></param>
        public void RemoveSocket(WebSocket socket)
        {
            try
            {
                var key = _sockets.FirstOrDefault(x => x.Value.WebSocket == socket).Key;

                if (string.IsNullOrEmpty(key))
                {
                    return;
                }

                logger.Info($"RemoveSocket(WebSocket socket). Удаляем сокет по ключу {key}");
                _sockets.TryRemove(key, out _);
            }

            catch(Exception ex)
            {
                handlerException.Handle(ex, " [Ошибка удаления сокета из хранилища] RemoveSocket(WebSocket socket).");
            }
            
           
        }

        private void CloseConnectionInDb(string connectionId)
        {
            var connection = unitOfWork.WebSocketConnectionRepository.GetFirst(x => x.WebSocketId == connectionId);

            if (connection == null)
                return;

            connection.ConnectionCloseeDateTime = DateTime.Now;
            unitOfWork.WebSocketConnectionRepository.Update(connection);
            unitOfWork.Save();
        }

        /// <summary>
        /// Сгенирировать новый идентификатор сокета.
        /// </summary>
        private static string CreateConnectionId()
        {                        
            return Guid.NewGuid().ToString("N");
        }
    }
}
