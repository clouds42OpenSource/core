﻿using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Clouds42.Common.Helpers;
using Core42.ConnectionBridge.WebSocketServer.Internal;
using Core42.WebSocketCommands.Implementation;

namespace Core42.ConnectionBridge.WebSocketServer.WebSockets
{
    /// <summary>
    /// Базовый обработчик веб сокетных соединений.
    /// </summary>
    public abstract class WebSocketHandler(
        WebSocketConnectionStorage connectionsStorage,
        BridgeAccessProvider accessProvider)
    {
        public readonly BridgeAccessProvider AccessProvider = accessProvider;
        public readonly WebSocketConnectionStorage ConnectionsStorage = connectionsStorage;

        /// <summary>
        /// Обработчик открытия нового соединения.
        /// </summary>
        /// <param name="socket">Новое соединение.</param>
        /// <returns>Внутренный номер соединения.</returns>
        public virtual async Task<string> OnConnected(WebSocket socket)
        {
            return await Task.FromResult(ConnectionsStorage.AddSocket(socket));            
        }

        /// <summary>
        /// Обработчик закрытия соединения.
        /// </summary>
        /// <param name="socket">Закрываемое соединение.</param>
        public virtual async Task OnDisconnected(WebSocket socket)
        {
            await ConnectionsStorage.RemoveSocket(ConnectionsStorage.GetId(socket));
        }

        /// <summary>
        /// Передать команду целевому соединению.
        /// </summary>
        /// <param name="socket">Целевое соединение.</param>
        /// <param name="command">Передаваемая команда.</param>
        public async Task ReceiveCommandAsync(WebSocket socket, WebSocketCommand command)
        {
            if (socket.State != WebSocketState.Open)
                return;

            var objBytes = command.ToByteArray();
            await socket.SendAsync(buffer: new ArraySegment<byte>(array: objBytes,
                    offset: 0,
                    count: objBytes.Length),
                messageType: WebSocketMessageType.Binary,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
        }

    }

}
