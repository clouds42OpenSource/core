﻿using System;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Core42.ConnectionBridge.WebSocketServer.WebSockets
{
    public static class WebSocketExtensions
    {

        /// <summary>
        /// Заммапить путь к обработчику.
        /// </summary>
        /// <param name="app">Билдер приложения.</param>
        /// <param name="path">Путь подключения.</param>
        /// <param name="handler">Обработчик поступающих запросов.</param>
        /// <returns></returns>
        public static IApplicationBuilder MapWebSocketManager(this IApplicationBuilder app,
            PathString path,
            WebSocketHandler handler)
        {
            return app.Map(path, (ws) => ws.UseMiddleware<WebSocketManagerMiddleware>(handler));
        }

        /// <summary>
        /// Зарегистрировать все производные от WebSocketHandler как синглтон.
        /// </summary>
        public static IServiceCollection AddWebSocketManager(this IServiceCollection services)
        {
            services.AddSingleton<WebSocketConnectionStorage>();

            var exportedTypes = Assembly.GetEntryAssembly()?.ExportedTypes;
            if (exportedTypes == null)
            {
                throw new InvalidOperationException("Не удалось получить экспортируемые типа текущей сборки.");
            }
                
            foreach (var type in exportedTypes)
            {
                if (type.GetTypeInfo().BaseType == typeof(WebSocketHandler))
                {
                    services.AddSingleton(type);
                }
            }

            return services;
        }
    }

}
