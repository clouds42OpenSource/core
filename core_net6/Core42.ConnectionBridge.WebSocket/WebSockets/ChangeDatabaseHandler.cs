﻿using Core42.ConnectionBridge.WebSocketServer.Internal;

namespace Core42.ConnectionBridge.WebSocketServer.WebSockets
{

    /// <summary>
    /// Обрабтчик службы веб сокетных соединений по изменениям информационных баз.
    /// </summary>
    public class ChangeDatabaseHandler(
        WebSocketConnectionStorage connectionsManager,
        BridgeAccessProvider accessProvider)
        : WebSocketHandler(connectionsManager, accessProvider);    

}
