﻿using System;
using System.Net.WebSockets;

namespace Core42.ConnectionBridge.WebSocketServer.WebSockets
{
    /// <summary>
    /// Контейнер веб сокетного подключения.
    /// </summary>
    public class WebSocketContainer(WebSocket webSocket, Guid accountUserId)
    {
        /// <summary>
        /// Сокет подключения.
        /// </summary>
        public WebSocket WebSocket { get; } = webSocket;

        /// <summary>
        /// Идентификатор пользователя подключения.
        /// </summary>
        public Guid AccountUserId { get; } = accountUserId;
    }
}