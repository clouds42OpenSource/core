﻿using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;
using Core42.WebSocketCommands;
using Microsoft.AspNetCore.Http;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace Core42.ConnectionBridge.WebSocketServer.Internal
{

    /// <summary>
    /// Провайдер безопасности.
    /// </summary>
    public class BridgeAccessProvider(
        IHttpContextAccessor httpContextAccessor,
        IUnitOfWork unitOfWork)
    {
        /// <summary>
        /// Получить идентификато пользователя по токену.
        /// </summary>
        public Guid GetUserIdOrThrowException()
        {
            var token = GetTokenOrThrowException();

            var accountUserId = GetAccountUserIdViaToken(token);
            if (accountUserId == null || accountUserId.Equals(Guid.Empty))
            {
                throw new InvalidOperationException();
            }

            return accountUserId.Value;
        }

        /// <summary>
        /// Получает AccountUserId по токену
        /// </summary>
        /// <param name="token">Токен по которому получить AccountUserId</param>
        /// <returns>Возвращает AccountUserId</returns>
        private Guid? GetAccountUserIdViaToken(string token)
        {
            var accountUserId =
                Guid.TryParse(token, out var sessionToken)
                    ? GetAccountUserIdFromSessionsRepository(sessionToken)
                    : GetAccountUserIdFromInternalSystemWithLogin(token);

            return accountUserId ?? GetAccountUserIdFromJsonWebToken(token);
        
        }

        /// <summary>
        /// Получает AccountUserId по переданному логину через внутренюю систему
        /// </summary>
        /// <param name="token">Токен, из которого вытащить логин</param>
        /// <returns>Возвращает AccountUserId</returns>
        private Guid? GetAccountUserIdFromInternalSystemWithLogin(string token)
        {
            var login = token?.StartsWith(WebSocketConstants.InternalSystemTokenWithLoginPrefix) ?? false
                ? token[WebSocketConstants.InternalSystemTokenWithLoginPrefix.Length..]
                : null;

            return string.IsNullOrEmpty(login)
                ? null
                : GetAccountUserIdByLogin(login);
        }

        /// <summary>
        /// Получает AccountUserID по логину
        /// </summary>
        /// <param name="login">Логин по которому найти AccountUserId</param>
        /// <returns>Возвращает AccountUserID</returns>
        private Guid? GetAccountUserIdByLogin(string login)
        {
            var accountUserId = unitOfWork.AccountUsersRepository.FirstOrDefault(user => user.Login == login)?.Id;
            return accountUserId;
        }

        /// <summary>
        /// Получает AccountUserId по токену активной сессии 
        /// </summary>
        /// <param name="sessionToken">Токен активной сессии по которому получить AccountUserId</param>
        /// <returns>Возвращает AccountUserID</returns>
        private Guid? GetAccountUserIdFromSessionsRepository(Guid sessionToken)
        {
            var accountUserId = unitOfWork.AccountUserSessionsRepository.WhereLazy(s => s.Token == sessionToken).Select(s => s.AccountUserId).FirstOrDefault();
            return accountUserId.Equals(Guid.Empty)
                ? null
                : (Guid?)accountUserId;
        }

        /// <summary>
        /// Получает AccountUserId по токену JsonWebToken
        /// </summary>
        /// <param name="jwtToken">JsonWebToken из которого получить AccountUserId</param>
        /// <returns>Возвращает AccountUserID</returns>
        private static Guid? GetAccountUserIdFromJsonWebToken(string jwtToken)
        {
            var accountUserIdStr = GetAccountUserIdFromTokenClaims(jwtToken);
            
            return Guid.TryParse(accountUserIdStr, out var accountUserId)
                ? accountUserId
                : null;
        }

        /// <summary>
        /// Получить из контекста запроса токен авторизации.
        /// </summary>
        private string GetTokenOrThrowException()
        {
            if ((httpContextAccessor?.HttpContext?.Request.Cookies.TryGetValue("Token", out var tokenStr) ?? false) && !string.IsNullOrEmpty(tokenStr))
                return tokenStr;

            throw new WebSocketAuthenticationException("Не удалось получить bearer token. Параметр Cookies['Token']=null");
        }

        /// <summary>
        /// Получить claims из JsonWebToken
        /// </summary>
        /// <param name="token">Json Web Token</param>
        private static string GetAccountUserIdFromTokenClaims(string token)
        {
            var claims = new JwtSecurityToken(token);

            return claims.Claims?.FirstOrDefault(x => x.Type == "sub")?.Value ?? claims.Claims?.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
        }
    }
}
