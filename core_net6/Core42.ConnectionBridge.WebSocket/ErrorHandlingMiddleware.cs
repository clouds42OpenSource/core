﻿using System;
using System.Net;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Clouds42.Logger;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Core42.ConnectionBridge.WebSocketServer
{
    public class ErrorHandlingMiddleware(RequestDelegate next)
    {
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }

            catch (WebSocketException wEx)
            {
                context.RequestServices.GetRequiredService<ILogger42>().Warn(wEx, "Ошибка веб-сокета");
            }
            catch (InvalidOperationException iEx)
            {
                context.RequestServices.GetRequiredService<ILogger42>().Warn(iEx, "Ошибка выполнения операции");
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            if (!context.WebSockets.IsWebSocketRequest && !context.Response.HasStarted)
            {
                var result = JsonConvert.SerializeObject(new { error = ex.Message });
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await context.Response.WriteAsync(result);
            }

            context.RequestServices.GetRequiredService<ILogger42>().Error(ex, "Необработанное исключение приложения");
        }
    }
}
