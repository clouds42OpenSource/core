﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;
using Clouds42.Tools1C.Helpers;
using Newtonsoft.Json;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Starter1C.Helpers
{
    /// <summary>
    /// Хэлпер для стартера 1С
    /// </summary>
    public class Starter1CHelper(
        LogUpdate1CParser logUpdate1CParser,
        ILogger42 logger)
    {
        /// <summary>
        /// Справочник кодов выхода приложения
        /// </summary>
        private readonly IDictionary<int, bool> _processExitCodeDictionary = new Dictionary<int, bool>
        {
            { 0, true },
            { 1, false }
        };

        /// <summary>
        /// Справочник для сопоставления результата распознавания лога 1с и результата подключения по COM
        /// </summary>
        private readonly IDictionary<LogUpdate1CParser.LogState, ConnectCodeDto> _mapLogStateToConnectCode = new Dictionary<LogUpdate1CParser.LogState, ConnectCodeDto>
        {
            { LogUpdate1CParser.LogState.Success, ConnectCodeDto.Success },
            { LogUpdate1CParser.LogState.AcDbHasActiveSessions, ConnectCodeDto.ConnectError },
            { LogUpdate1CParser.LogState.CfuDamaged, ConnectCodeDto.DatabaseDamaged },
            { LogUpdate1CParser.LogState.DbHasModification, ConnectCodeDto.ConnectError },
            { LogUpdate1CParser.LogState.PasswordOrLoginIncorrect, ConnectCodeDto.PasswordOrLoginIncorrect },
            { LogUpdate1CParser.LogState.Error, ConnectCodeDto.ConnectError }
        };

        /// <summary>
        /// Получить параметры запуска 1С Предприятие
        /// </summary>
        /// <param name="isEnterprise">Признак запуска в режиме предприятия</param>
        /// <param name="updateDatabase">Модель обновления базы</param>
        /// <returns>Параметры запуска 1С Предприятие</returns>
        public string GetStartLine(bool isEnterprise, IUpdateDatabaseDto updateDatabase)
        {
            var commandLine = isEnterprise ? "ENTERPRISE" : "DESIGNER";

            if (updateDatabase.IsFile)
                commandLine += $" /F\"{updateDatabase.ConnectionAddress}\"";
            else
                commandLine += $" /S\"{updateDatabase.ConnectionAddress.Split(';').FirstOrDefault()}\\{updateDatabase.UpdateDatabaseName}\"";

            commandLine += $" /N \"{updateDatabase.AdminLogin}\"";

            if (!string.IsNullOrEmpty(updateDatabase.AdminPassword))
                commandLine += $" /P \"{updateDatabase.AdminPassword}\"";

            return commandLine;
        }

        /// <summary>
        /// Получить путь до exe 1С предприятия на сервере.
        /// </summary>
        /// <param name="updateDatabase">Модель обновления базы</param>
        /// <returns>Путь до exe 1С предприятия на сервере</returns>
        public string Get1CExePath(IUpdateDatabaseDto updateDatabase)
        {

            var path1C = Path.Combine(
                updateDatabase.PlatformType == PlatformType.V82 ? CloudConfigurationProvider.Enterprise1C.Get1C82ExecutionPath() : updateDatabase.FolderPath1C,
                Client1CConstants.ThickNameWithExe
                );

            if (!File.Exists(path1C))
                throw new NotFoundException($"Отсутствует файл 1С {path1C} на сервере");

            logger.Trace($"{updateDatabase.UpdateDatabaseName} :: {path1C}");

            return path1C;
        }

        /// <summary>
        /// Обработать результат выполнения процесса принятия обновления
        /// </summary>
        /// <param name="applyUpdateProcess">Процесс принятия обновления</param>
        /// <param name="processRunningTime">Время выполнения процесса</param>
        /// <param name="updateDatabase">Модель обновления базы</param>
        /// <returns>Результат выхода процесса</returns>
        public bool FinalizeApplyUpdateProcess(Process applyUpdateProcess, long processRunningTime, IUpdateDatabaseDto updateDatabase)
        {
            var logAdditionalParams =
                $"Время выполнения: {processRunningTime} секунд. Код выхода: {(applyUpdateProcess.HasExited ? applyUpdateProcess.ExitCode.ToString() : "[Время ожидания истекло]")}";

            if (!applyUpdateProcess.HasExited)
            {
                applyUpdateProcess.Kill();
                throw new InvalidOperationException(
                    $@"Ошибка принятия обновления в инф. базе {updateDatabase.UpdateDatabaseName}. {logAdditionalParams}");
            }

            var processExitResult = GetProcessExitResult(applyUpdateProcess.ExitCode);

            if (!processExitResult)
                throw new InvalidOperationException($@"Ошибка принятия обновлений в инф. базе {updateDatabase.UpdateDatabaseName}. {logAdditionalParams}");

            logger.Trace(
                $@"В инф. базе {updateDatabase.UpdateDatabaseName} успешно приняты обновления. {logAdditionalParams}");

            return true;
        }

        /// <summary>
        /// Создать процесс для получения метаданных
        /// </summary>
        /// <param name="updateDatabase">Модель обновления базы</param>
        /// <param name="metadataFilePath">Путь к файлу метаданных инф. базы</param>
        /// <param name="logFilePath">Путь к файлу лога 1С</param>
        /// <returns>Процесс для получения метаданных</returns>
        public Process CreateProcessForGetMetadata(IUpdateDatabaseDto updateDatabase, string metadataFilePath, string logFilePath)
        {
            var commandLine = $@"{GetStartLine(true, updateDatabase)} 
                                        /Execute ""{CloudConfigurationProvider.AccountDatabase.Support.GetReceiveMetadataHandlingPath()}""
                                        {Start1CCommandLineConstants.DisableStartupDialogs} 
                                        {Start1CCommandLineConstants.DisableStartupMessages} 
                                        /C ""{metadataFilePath}""  
                                        /Out ""{logFilePath}"" 
                                        {Start1CCommandLineConstants.UsePrivilegedMode}
                                    ";

            logger.Debug($"Параметры запуска для получения метаданных инф. базы {updateDatabase.UpdateDatabaseName}: {commandLine}");

            return new Process
            {
                StartInfo =
                {
                    FileName = Get1CExePath(updateDatabase),
                    Arguments = commandLine,
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    ErrorDialog = false
                }
            };
        }

        /// <summary>
        /// Обработать результат выполнения процесса получения метаданных
        /// </summary>
        /// <param name="updateDatabase">Модель обновления базы</param>
        /// <param name="metadataFilePath">Путь к файлу метаданных инф. базы</param>
        /// <param name="logFilePath">Путь к файлу лога 1С</param>
        /// <returns>Метаданные инф. базы</returns>
        public ConnectorResultDto<MetadataResultDto> FinalizeGetMetadataProcess(IUpdateDatabaseDto updateDatabase, string metadataFilePath, string logFilePath)
        {
            try
            {
                var logParseResult = logUpdate1CParser.Parse(logFilePath);
                if (logParseResult.LogState != LogUpdate1CParser.LogState.Success)
                    return CreateConnectorResult(logParseResult.LogState, null,
                        $"Ошибка получения метаданных инф. базы {updateDatabase.UpdateDatabaseName}. Причина: {logParseResult}");

                var metadataInfo = TryGetMetadataFromFile(metadataFilePath);

                if (string.IsNullOrEmpty(metadataInfo.ReleaseVersion) || string.IsNullOrEmpty(metadataInfo.ConfigurationName))
                    return CreateConnectorResult(LogUpdate1CParser.LogState.Error, metadataInfo,
                        $"Модель метаданных для инф. базы {updateDatabase.UpdateDatabaseName} заполнена не корректно");

                return CreateConnectorResult(logParseResult.LogState, metadataInfo);
            }
            catch (Exception ex)
            {
                return CreateConnectorResult(LogUpdate1CParser.LogState.Error, null, ex.GetFullInfo(false));
            }
        }

        /// <summary>
        /// Получить результат выхода процесса
        /// </summary>
        /// <param name="processExitCode">Код выхода процесса</param>
        /// <returns>Результат выхода процесса</returns>
        private bool GetProcessExitResult(int processExitCode)
        {
            if (!_processExitCodeDictionary.ContainsKey(processExitCode))
                return false;

            return _processExitCodeDictionary[processExitCode];
        }

        /// <summary>
        /// Получить объект метаданных из файла
        /// </summary>
        /// <param name="metadataFilePath">Путь к файлу метаданных</param>
        /// <returns>Объект метаданных из файла</returns>
        private static MetadataInfoFrom1CDto TryGetMetadataFromFile(string metadataFilePath)
        {
            var metadataContent = File.ReadAllText(metadataFilePath);
            return JsonConvert.DeserializeObject<MetadataInfoFrom1CDto>(metadataContent);
        }

        /// <summary>
        /// Создать результат получения метаданных
        /// </summary>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <param name="logState">Результат распознавания логов</param>
        /// <param name="metadataInfo">Метаданные</param>
        /// <returns>Результат получения метаданных</returns>
        private ConnectorResultDto<MetadataResultDto> CreateConnectorResult(LogUpdate1CParser.LogState logState, MetadataInfoFrom1CDto metadataInfo = null, string errorMessage = null)
        {
            var connectCode = _mapLogStateToConnectCode[logState];

            return new ConnectorResultDto<MetadataResultDto>
            {
                Result = new MetadataResultDto(metadataInfo?.ReleaseVersion, metadataInfo?.ConfigurationName),
                ConnectCode = connectCode,
                Message = errorMessage
            };
        }
    }
}
