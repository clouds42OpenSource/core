﻿using Clouds42.Tools1C.Helpers;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;

namespace Clouds42.Starter1C.Helpers.Interfaces
{
    /// <summary>
    /// Хэлпер для запуска 1С.
    /// </summary>
    public interface IStarter1C
    {
        /// <summary>
        /// Инициализация параметра updateDatabase
        /// </summary>
        /// <param name="updateDatabase">данные обновления базы</param>
        /// <returns></returns>
        IStarter1C SetUpdateDatabase(IUpdateDatabaseDto updateDatabase);

        /// <summary>
        /// Запустить 1С с параметрами.
        /// </summary>
        /// <param name="param">параметр запуска.</param>
        /// <param name="isEnterprise">Признак Enterprise</param>
        /// <param name="logOut">Логировать процесс.</param>
        /// <returns>Признак успешности.</returns>
        bool StartApp(string param, bool isEnterprise, bool logOut = false );

        /// <summary>
        /// Обновить информационную базу.
        /// </summary>
        /// <param name="cfuPath">Путь к пакету обновления</param>
        /// <param name="logResult">Результат чтения лога 1С</param>
        /// <returns>Результат обновления</returns> 
        bool UpdateVersion(string cfuPath, out LogUpdate1CParser.Result logResult);

        /// <summary>
        /// Принять обновления в инф. базе
        /// </summary>
        /// <returns>Результат выполнения</returns>
        bool ApplyUpdate();

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>
        /// <returns>Метаданные информационной базы</returns>
        ConnectorResultDto<MetadataResultDto> GetMetadata();
    }
}
