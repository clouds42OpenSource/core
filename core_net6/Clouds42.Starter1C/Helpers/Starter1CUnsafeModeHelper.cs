﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Clouds42.Configurations.Configurations;

namespace Clouds42.Starter1C.Helpers
{
    /// <summary>
    /// Хэлпер для запуска 1С с отключенной защитой от опасных действий
    /// </summary>
    public class Starter1CUnsafeModeHelper
    {
        private readonly Lazy<string> _1CEnterpriseConfigFilePath;
        private readonly Lazy<string> _1CEnterpriseUnsafeModeParamName;
        private readonly List<(Func<string, bool, bool>, Action<ICollection<string>, string>)> _mapConditionToUpdateAcDbAction;

        public Starter1CUnsafeModeHelper()
        {
            _1CEnterpriseConfigFilePath =
                new Lazy<string>(CloudConfigurationProvider.AccountDatabase.Support.Get1CEnterpiseConfigFilePath);

            _1CEnterpriseUnsafeModeParamName =
                new Lazy<string>(CloudConfigurationProvider.AccountDatabase.Support.Get1CEnterpriseUnsafeModeParamName);

            _mapConditionToUpdateAcDbAction =
            [
                ((acDbInUnsafeMode, isEnabled) => acDbInUnsafeMode == null && !isEnabled,
                    (unsafeModeDatabases, acDbV82Name) => { }),
                ((acDbInUnsafeMode, isEnabled) => acDbInUnsafeMode != null && isEnabled,
                    (unsafeModeDatabases, acDbV82Name) => { }),
                ((acDbInUnsafeMode, isEnabled) => acDbInUnsafeMode != null && !isEnabled, RemoveAcDbFromUnsafeMode),
                ((acDbInUnsafeMode, isEnabled) => acDbInUnsafeMode == null && isEnabled, AddAcDbInUnsafeMode)
            ];
        }

        /// <summary>
        /// Изменить признак работы 1С Предприятие в небезопасном режиме
        /// </summary>
        /// <param name="isEnabled">Признак включения небезопасного режима</param>
        /// <param name="acDbV82Name">Номер инф. базы</param>
        public void Manage1CEnterpriseUnsafeMode(bool isEnabled, string acDbV82Name)
        {
            var configPath = _1CEnterpriseConfigFilePath.Value;
            var configFileContent = File.ReadAllLines(configPath).ToList();

            var unsafeModeLine = GetUnsafeModeLine(configFileContent);
            if (unsafeModeLine == null)
                AddUsnafeModeLine(configFileContent);

            ChangeUnsafeModeLine(configFileContent, isEnabled, acDbV82Name);
            File.WriteAllLines(configPath, configFileContent);
        }

        /// <summary>
        /// Добавить строку, отвечающую за небезопасный режим работы
        /// </summary>
        /// <param name="configFileContent">Содержимое файла конфигурации</param>
        private void AddUsnafeModeLine(ICollection<string> configFileContent)
        {
            configFileContent.Add(_1CEnterpriseUnsafeModeParamName.Value);
        }

        /// <summary>
        /// Поменять строку, отвечающую за небезопасный режим работы
        /// </summary>
        /// <param name="configFileContent">Содержимое файла конфигурации</param>
        /// <param name="isEnabled">Признак включения небезопасного режима</param>
        /// <param name="acDbV82Name">Номер инф. базы</param>
        private void ChangeUnsafeModeLine(ICollection<string> configFileContent, bool isEnabled, string acDbV82Name)
        {
            var unsafeModeLine = GetUnsafeModeLine(configFileContent);

            var unsafeModeParams = unsafeModeLine.Split('=');
            var unsafeModeParamName = unsafeModeParams[0];

            var unsafeModeDatabases = unsafeModeParams[1].Split(';').
                Select(line => line.Trim()).
                Where(line => !string.IsNullOrEmpty(line)).ToList();

            UpdateAcDbInUnsafeModeParams(unsafeModeDatabases, acDbV82Name, isEnabled);

            configFileContent.Remove(unsafeModeLine);
            configFileContent.Add($"{unsafeModeParamName}={GetStringFromUnsafeModeDatabases(unsafeModeDatabases)};");
        }

        /// <summary>
        /// Обновить список баз, которые работают в небезопасном режиме
        /// </summary>
        /// <param name="unsafeModeDatabases">Список баз, которые работают в небезопасном режиме</param>
        /// <param name="isEnabled">Признак включения небезопасного режима</param>
        /// <param name="acDbV82Name">Номер инф. базы</param>
        private void UpdateAcDbInUnsafeModeParams(ICollection<string> unsafeModeDatabases, string acDbV82Name, bool isEnabled)
        {
            var acDbInUnsafeMode = unsafeModeDatabases.FirstOrDefault(param => param.Contains(acDbV82Name));

            var editAction =
                _mapConditionToUpdateAcDbAction.FirstOrDefault(tuple =>
                    tuple.Item1(acDbInUnsafeMode, isEnabled)).Item2;

            editAction(unsafeModeDatabases, acDbV82Name);
        }

        /// <summary>
        /// Удалить инф. базу из списка баз, которые работают в небезопасном режиме
        /// </summary>
        /// <param name="unsafeModeDatabases">Список баз, которые работают в небезопасном режиме</param>
        /// <param name="acDbV82Name">Номер инф. базы</param>
        private static void RemoveAcDbFromUnsafeMode(ICollection<string> unsafeModeDatabases, string acDbV82Name)
        {
            var acDbInUnsafeMode = unsafeModeDatabases.FirstOrDefault(param => param.Contains(acDbV82Name));
            unsafeModeDatabases.Remove(acDbInUnsafeMode);
        }

        /// <summary>
        /// Удалить инф. базу из списка баз, которые работают в небезопасном режиме
        /// </summary>
        /// <param name="unsafeModeDatabases">Список баз, которые работают в небезопасном режиме</param>
        /// <param name="acDbV82Name">Номер инф. базы</param>
        private static void AddAcDbInUnsafeMode(ICollection<string> unsafeModeDatabases, string acDbV82Name)
        {
            unsafeModeDatabases.Add($"^.*{acDbV82Name}.*");
        }

        /// <summary>
        /// Получить строку из списка баз, которые работают в небезопасном режиме
        /// </summary>
        /// <param name="unsafeModeDatabases">Список баз, которые работают в небезопасном режиме</param>
        /// <returns>Строка из списка баз, которые работают в небезопасном режиме</returns>
        private static string GetStringFromUnsafeModeDatabases(ICollection<string> unsafeModeDatabases)
            => string.Join(";", unsafeModeDatabases);

        /// <summary>
        /// Получить строку, отвечающую за небезопасный режим работы
        /// </summary>
        /// <param name="configFileContent">Содержимое файла конфигурации</param>
        /// <returns>Строка, отвечающая за небезопасный режим работы</returns>
        private string GetUnsafeModeLine(ICollection<string> configFileContent)
            => configFileContent.FirstOrDefault(line => line.StartsWith(_1CEnterpriseUnsafeModeParamName.Value));
    }
}
