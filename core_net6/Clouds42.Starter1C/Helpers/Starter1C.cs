﻿using System;
using System.Diagnostics;
using System.IO;
using Clouds42.Configurations.Configurations;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Tools1C.Helpers;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;

namespace Clouds42.Starter1C.Helpers
{
    /// <summary>
    /// Хэлпер для запуска 1С.
    /// </summary>
    internal class Starter1C(
        LogUpdate1CParser logUpdate1CParser,
        IHandlerException handlerException,
        Starter1CHelper starter1CHelper,
        Starter1CUnsafeModeHelper starter1CUnsafeModeHelper,
        ILogger42 logger)
        : IStarter1C
    {
        private IUpdateDatabaseDto _updateDatabase;

        /// <summary>
        /// Инициализация параметра updateDatabase
        /// </summary>
        /// <param name="updateDatabase">данные обновления базы</param>
        /// <returns></returns>
        public IStarter1C SetUpdateDatabase(IUpdateDatabaseDto updateDatabase)
        {
            _updateDatabase = updateDatabase;
            return this;
        }

        /// <summary>
        /// Запустить 1С с параметрами.
        /// </summary>
        /// <param name="param">параметр запуска.</param>
        /// <param name="isEnterprise">Признак Enterprise</param>
        /// <param name="logOut">Логировать процесс.</param>
        /// <returns>Признак успешности.</returns>
        public bool StartApp(string param, bool isEnterprise, bool logOut = false)
        {

            var commandLine = $"{starter1CHelper.GetStartLine(isEnterprise, _updateDatabase)} {param}";

            if (logOut)
                commandLine += $" /out \"{Log1CPathHelper.GetPathOutByTime(DateTime.Now, _updateDatabase.UpdateDatabaseName)}\"";

            logger.Debug($"[StartApp] {_updateDatabase.UpdateDatabaseName} {commandLine}");

            var process = new Process { StartInfo = { FileName = starter1CHelper.Get1CExePath(_updateDatabase), Arguments = commandLine } };

            try
            {
                var timeout = CloudConfigurationProvider.AccountDatabase.Support.GetTehSupportTimeout();

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                process.Start();
                process.WaitForExit(timeout);
                stopwatch.Stop();

                if (process.HasExited)
                {
                    logger.Debug($"[StartApp] {_updateDatabase.UpdateDatabaseName} end. duration - {stopwatch.Elapsed.Hours}:{stopwatch.Elapsed.Minutes}:{stopwatch.Elapsed.Seconds}.{stopwatch.Elapsed.Milliseconds / 10}");
                    return true;
                }

                logger.Debug($"[StartApp] {_updateDatabase.UpdateDatabaseName} Not end");

                process.Kill();

                return false;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка запуска 1С для базы] {_updateDatabase.UpdateDatabaseName}.");
                return false;
            }
        }

        /// <summary>
        /// Обновить информационную базу.
        /// </summary>
        /// <param name="cfuPath">Путь к пакету обновления</param>
        /// <param name="logResult">Результат выполнения из лога 1С</param>
        /// <returns>Результат выполнения</returns>    
        public bool UpdateVersion(string cfuPath, out LogUpdate1CParser.Result logResult)
        {

            var logFilePath = Log1CPathHelper.GetPathOutByName(_updateDatabase.UpdateDatabaseName, Path.GetFileNameWithoutExtension(cfuPath));

            var commandLine = starter1CHelper.GetStartLine(false, _updateDatabase) +
                              $" /UpdateCfg \"{cfuPath}\" /UpdateDBCfg /Out {logFilePath} " +
                              $"{Start1CCommandLineConstants.DisableStartupDialogs} " +
                              $"{Start1CCommandLineConstants.DisableStartupMessages} " +
                              $"{Start1CCommandLineConstants.UsePrivilegedMode}";

            logger.Debug(commandLine);
            var process = new Process { StartInfo = { FileName = starter1CHelper.Get1CExePath(_updateDatabase), Arguments = commandLine } };

            var timeout = CloudConfigurationProvider.AccountDatabase.Support.GetAutoUpdateTimeout();

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            process.Start();
            process.WaitForExit(timeout);

            stopwatch.Stop();

            if (!process.HasExited)
            {
                logger.Warn($"Обновление базы данных {_updateDatabase.UpdateDatabaseName} прервано принудительно.");
                process.Kill();

                logResult = logUpdate1CParser.Parse(logFilePath);
                return false;
            }

            var fileName = Path.GetFileName(cfuPath);
            logger.Debug($"База данных {_updateDatabase.UpdateDatabaseName} обновлена успешно к релизу {fileName}. Время обновления {stopwatch.ElapsedMilliseconds / 1000} секунд.");
            logResult = logUpdate1CParser.Parse(logFilePath);
            return true;
        }

        /// <summary>
        /// Принять обновления в инф. базе
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public bool ApplyUpdate()
        {
            var commandLine = $"{starter1CHelper.GetStartLine(true, _updateDatabase)} /C\"{CloudConfigurationProvider.AccountDatabase.Support.GetApplyUpdateCommand()}\"";
            logger.Debug(commandLine);
            var timeout = CloudConfigurationProvider.AccountDatabase.Support.GetApplyUpdateTimeout();

            var applyUpdateProcess = new Process
            {
                StartInfo =
                {
                    FileName = starter1CHelper.Get1CExePath(_updateDatabase),
                    Arguments = commandLine,
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    ErrorDialog = false
                }
            };

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            applyUpdateProcess.Start();
            applyUpdateProcess.WaitForExit(timeout);
            stopwatch.Stop();

            var processRunningTime = stopwatch.ElapsedMilliseconds / 1000;

            return starter1CHelper.FinalizeApplyUpdateProcess(applyUpdateProcess, processRunningTime, _updateDatabase);
        }

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>
        /// <returns>Метаданные информационной базы</returns>
        public ConnectorResultDto<MetadataResultDto> GetMetadata()
        {
            var metadataFilePath = Log1CPathHelper.GetPathForMetadataJsonFile(_updateDatabase.UpdateDatabaseName);
            var logFilePath = Log1CPathHelper.GetPathForLogFile(_updateDatabase.UpdateDatabaseName);
            var timeout = CloudConfigurationProvider.AccountDatabase.Support.GetReceiveMetadataCommandTimeout();

            try
            {
                starter1CUnsafeModeHelper.Manage1CEnterpriseUnsafeMode(true, _updateDatabase.UpdateDatabaseName);
                var getMetadataProcess = starter1CHelper.CreateProcessForGetMetadata(_updateDatabase, metadataFilePath, logFilePath);

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                getMetadataProcess.Start();

                var processIdInfo = $"ID процесса { getMetadataProcess.Id}";
                logger.Trace($"Запускается процесс получения метаданных инф. базы {_updateDatabase.UpdateDatabaseName}. {processIdInfo}");

                getMetadataProcess.WaitForExit(timeout);
                stopwatch.Stop();

                if (!getMetadataProcess.HasExited)
                {
                    logger.Warn($"Получение метаданных инф. базы {_updateDatabase.UpdateDatabaseName} прервано принудительно. {processIdInfo}");
                    getMetadataProcess.Kill();
                }

                var processRunningTime = stopwatch.ElapsedMilliseconds / 1000;

                var metadataInfo =
                    starter1CHelper.FinalizeGetMetadataProcess(_updateDatabase, metadataFilePath, logFilePath);

                logger.Trace($"Для инф. базы {_updateDatabase.UpdateDatabaseName} получены метаданные. " +
                              $"Конфигурация: {metadataInfo.Result.SynonymConfiguration}, версия релиза: {metadataInfo.Result.Version}. " +
                              $"Время выполнения: {processRunningTime}");

                return metadataInfo;
            }
            finally
            {
                starter1CUnsafeModeHelper.Manage1CEnterpriseUnsafeMode(false, _updateDatabase.UpdateDatabaseName);
                if (File.Exists(metadataFilePath))
                    File.Delete(metadataFilePath);
            }
        }
    }
}
