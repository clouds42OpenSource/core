﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.Starter1C.Helpers;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Starter1C
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddStarter1C(this IServiceCollection services)
        {
            services.AddTransient<IStarter1CProvider, Starter1CProvider>();
            services.AddTransient<IStarter1C, Helpers.Starter1C>();
            services.AddTransient<Starter1CHelper>();
            services.AddTransient<Starter1CUnsafeModeHelper>();
            services.AddTransient<IUpdateDatabaseDto, AccountDatabaseSupportModelDto>();

            return services;
        }
    }
}
