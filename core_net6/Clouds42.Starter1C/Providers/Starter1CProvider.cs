﻿using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.RemoveDirectory;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers.Interfaces;
using Clouds42.Tools1C.Helpers;
using System;
using System.IO;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Starter1C.Providers
{
    /// <summary>
    /// Провайдер для запуска 1С.
    /// </summary>
    public class Starter1CProvider(
        IStarter1C c,
        IUpdateDatabaseDto database,
        ICfuProvider cfuProvider,
        IRemoveDirectoryJobWrapper removeDirectoryJobWrapper)
        : IStarter1CProvider
    {
        public delegate bool CanContinue();

        private CanContinue _canContinueAction;

        /// <summary>
        /// Обновить информационную базу до последней доступной версии.
        /// </summary>
        /// <param name="currentVersionDb">Текущая версия конфигурации инф. базы</param>
        /// <param name="configuration">Конфигурация 1С</param>
        /// <param name="afterEachUpdateAction">Действие после каждого обновления</param>
        /// <param name="targetVersion">Последняя версия, на которую получилось обновить</param>
        /// <returns>Результат обновления</returns>
        public bool TryUpdateToLastVersion(string currentVersionDb, IConfigurations1C configuration, Action<string> afterEachUpdateAction,
            out string targetVersion)
        {
            targetVersion = null;
            if (configuration == null)
                return false;

            var cfu = cfuProvider.GetNextUpdateVersion(configuration, currentVersionDb, database.PlatformVersion);

            if (cfu == null)
                return false;

            try
            {
                do
                {
                    if (!cfuProvider.CfuExist(cfu, configuration))
                    {
                        cfuProvider.DownloadCfu(cfu);
                    }

                    var cfuPath = CopyCfuToTemp(cfuProvider.GetCfuFullPath(cfu));

                    ProcessAutoUpdate(cfu.Version, cfuPath, out targetVersion);

                    currentVersionDb = cfu.Version;

                    afterEachUpdateAction(targetVersion);

                    if (!_canContinueAction())
                    {
                        break;
                    }

                    cfu = cfuProvider.GetNextUpdateVersion(configuration, currentVersionDb, database.PlatformVersion);
                } while (cfu != null);
            }
            finally
            {
                DropTempCfuStorage();
            }

            return true;
        }

        /// <summary>
        /// Принять обновления в инф. базе
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public bool TryApplyUpdate()
            => c.ApplyUpdate();

        /// <summary>
        /// Попытаться получить метаданные информационной базы.
        /// </summary>
        /// <returns>Метаданные информационной базы</returns>
        public ConnectorResultDto<MetadataResultDto> TryGetMetadata()
            => c.GetMetadata();

        /// <summary>
        /// Путь до временного хранилища пакетов обновлений.
        /// </summary>
        private string TemporaryCfusStoragePath
            => Path.Combine(CloudConfigurationProvider.Enterprise1C.GetTemporaryStoragePath(), "Cfus", database.UpdateDatabaseName);

        /// <summary>
        /// Удалить врменное хранилище пакетов обновлений.
        /// </summary>
        private void DropTempCfuStorage()
        {
            if (!Directory.Exists(TemporaryCfusStoragePath))
                return;

            removeDirectoryJobWrapper.Start(new RemoveDirectoryJobParams
                {
                    DerectoryPath = TemporaryCfusStoragePath
                });
        }

        /// <summary>
        /// Скопировать пакет CFU во временную директорию для принятия обновлений.
        /// </summary>
        /// <param name="cfuPath">Полный путь до копируемого CFU</param>
        /// <returns>Полный путь копии CFU.</returns>
        private string CopyCfuToTemp(string cfuPath)
        {
            if (!Directory.Exists(TemporaryCfusStoragePath))
                Directory.CreateDirectory(TemporaryCfusStoragePath);

            var cfuName = $"{Path.GetFileNameWithoutExtension(cfuPath)?.Replace(".", "_")}.cfu";
            var copyCfuPath = $"{TemporaryCfusStoragePath}\\{cfuName}";

            if (File.Exists(copyCfuPath))
                return copyCfuPath;

            File.Copy(cfuPath, copyCfuPath);

            return copyCfuPath;
        }

        /// <summary>
        /// Обновить информационную базу до новой версии релиза.
        /// </summary>
        /// <param name="version">Текущая версия конфигурации</param>
        /// <param name="cfuPath">Путь до пакета cfu</param>
        /// <param name="targetVersion">Новая версия конфигурации</param>
        private void ProcessAutoUpdate(string version, string cfuPath, out string targetVersion)
        {
            var result = c.UpdateVersion(cfuPath, out var logResult);

            if (!result)
                throw new DbHasModificationException(
                    "Процесс был прерван, возможно база содержит программные доработки. Попробуйте обновить вручную");

            if (logResult.LogState == LogUpdate1CParser.LogState.DbHasModification)
                throw new DbHasModificationException("База содержит программные доработки.");

            if (logResult.LogState == LogUpdate1CParser.LogState.CfuDamaged)
                throw new CfuDamagedException(logResult.ToString());

            if (logResult.LogState == LogUpdate1CParser.LogState.AcDbHasActiveSessions)
                throw new AcDbHasActiveSessionsException("В инф. базе есть активные сеансы");

            if (logResult.LogState == LogUpdate1CParser.LogState.Error)
            {
                var errorMessage = logResult.ToString();
                throw new InvalidOperationException(
                    $"Ошибка обновления базы :: {(!string.IsNullOrEmpty(errorMessage) ? errorMessage : logResult.LogState.ToString())}");
            }

            targetVersion = version;
        }


        public IStarter1CProvider SetStarter1C(IStarter1C starter1C)
        {
            c = starter1C;
            return this;
        }
        public IStarter1CProvider SetUpdateDatabase(IUpdateDatabaseDto updateDatabase)
        {
            database = updateDatabase;
            return this;
        }
        public IStarter1CProvider SetCanContinueAction(CanContinue canContinueAction)
        {
            _canContinueAction = canContinueAction;
            return this;
        }
    }
}
