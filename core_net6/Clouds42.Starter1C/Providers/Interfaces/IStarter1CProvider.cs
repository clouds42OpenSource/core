﻿using System;
using Clouds42.Starter1C.Helpers.Interfaces;
using static Clouds42.Starter1C.Providers.Starter1CProvider;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Starter1C.Providers.Interfaces
{
    /// <summary>
    /// Провайдер для запуска 1С.
    /// </summary>
    public interface IStarter1CProvider
    {
        /// <summary>
        /// Обновить информационную базу до последней доступной версии.
        /// </summary>
        /// <param name="currentVersionDb">Текущая версия конфигурации инф. базы</param>
        /// <param name="configuration">Конфигурация 1С</param>
        /// <param name="afterEachUpdateAction">Действие после каждого обновления</param>
        /// <param name="targetVersion">Последняя версия, на которую получилось обновить</param>
        /// <returns>Результат обновления</returns>
        bool TryUpdateToLastVersion(string currentVersionDb, IConfigurations1C configuration, Action<string> afterEachUpdateAction, out string targetVersion);

        /// <summary>
        /// Принять обновления в инф. базе
        /// </summary>
        /// <returns>Результат выполнения</returns>
        bool TryApplyUpdate();

        /// <summary>
        /// Попытаться получить метаданные информационной базы.
        /// </summary>
        /// <returns>Метаданные информационной базы</returns>
        ConnectorResultDto<MetadataResultDto> TryGetMetadata();
        IStarter1CProvider SetStarter1C(IStarter1C starter1C);
        IStarter1CProvider SetCanContinueAction(CanContinue canContinueAction);
        IStarter1CProvider SetUpdateDatabase(IUpdateDatabaseDto updateDatabase);
    }
}
