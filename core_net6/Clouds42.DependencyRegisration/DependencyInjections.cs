﻿using Clouds42.AccountDatabase;
using Clouds42.Accounts;
using Clouds42.AccountUsers;
using Clouds42.ActiveDirectory;
using Clouds42.AdvertisingBanner;
using Clouds42.AgencyAgreement;
using Clouds42.BigQuery;
using Clouds42.Billing;
using Clouds42.BLL.Common;
using Clouds42.CloudServices;
using Clouds42.Cluster1CProviders;
using Clouds42.CoreWorker.AccountDatabasesOnDelimiters;
using Clouds42.CoreWorker.ActiveDirectory.Jobs;
using Clouds42.CoreWorker.ExportDataFromCore;
using Clouds42.CoreWorker.JobWrappers;
using Clouds42.CoreWorkerTask;
using Clouds42.DaData;
using Clouds42.GoogleCloud;
using Clouds42.HandlerException;
using Clouds42.LetterNotification;
using Clouds42.Link42;
using Clouds42.Locales;
using Clouds42.Logger.Serilog;
using Clouds42.MyDatabaseService;
using Clouds42.OnlineCashier;
using Clouds42.RasClient;
using Clouds42.Resources;
using Clouds42.Segment;
using Clouds42.SmsClient;
using Clouds42.SmtpClient;
using Clouds42.Starter1C;
using Clouds42.StateMachine;
using Clouds42.Suppliers;
using Clouds42.Tools1C;
using Clouds42.Tools1C.Helpers;
using Clouds42.Validators;
using Clouds42.YookassaAggregator;
using Core42.Application;
using Core42.Application.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.DependencyRegistration
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddGlobalServices(this IServiceCollection serviceCollection, IConfiguration configuration, bool isWorker = false, bool ignoreAppServices = false)
        {
            serviceCollection
                .AddTransient<LogUpdate1CParser>()
                .AddCommonBll()
                .AddValidators()
                .AddSerilog()
                .AddAccountDatabase()
                .AddAccounts()
                .AddAccountUser()
                .AddActiveDirectory()
                .AddDaData()
                .AddAgencyAgreement(isWorker)
                .AddBigQuery()
                .AddBilling()
                .AddCloudServices()
                .AddMyDatabase()
                .AddMyDatabasesResource()
                .AddArrCluster()
                .AddSupplierReference()
                .AddOnlineCashierServices()
                .AddCluster1CProviders()
                .AddActiveDirectoryJobs()
                .AddJobWrappersServices()
                .AddHandlerException()
                .AddTools1CServices()
                .AddProcessFlows()
                .AddLink42()
                .AddLogging()
                .AddRasClient()
                .AddStarter1C()
                .AddAccountDatabaseOnDelimitersServices()
                .AddLocale()
                .AddAdvertisingBannerAudience()
                .AddYookassaAgregator()
                .AddLetterEmailAddress()
                .AddCoreWorkerJobWrappers()
                .AddSmsClient()
                .AddSMTP()
                .AddCoreWorkerExportDataFromCoreWrappers()
                .AddAuditCoreWorkerTasksQueue()
                .AddGoogleCloud(configuration);

            if (ignoreAppServices)
            {
                return serviceCollection;
            }

            serviceCollection.AddCore42Application();
            serviceCollection.AddCore42ApplicationContracts();
            
            return serviceCollection;
        }
    }
}
