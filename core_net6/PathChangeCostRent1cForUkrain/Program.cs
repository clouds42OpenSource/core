﻿using Clouds42.Common.Constants;
using Clouds42.DependencyRegistration;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PathChangeCostRent1cForUkrain.Constants;
using PathChangeCostRent1cForUkrain.Model;
using Repositories;

namespace PathChangeCostRent1cForUkrain;

internal class Program
{
    private static readonly ServiceCollection _serviceCollection = [];
    private static IUnitOfWork _dbLayer;
    private static ILogger42 logger;
    private static ISender _sender;
    /// <summary>
    /// Флаг запуска патча в тестовом режиме
    /// В тестовом режиме транзакции не будут завершены
    /// </summary>
    public static bool NoCommitMode { get; set; }

    static void Main()
    {
        try
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables()
                .Build();

            _serviceCollection
                .AddGlobalServices(configuration)
                .AddSerilogWithElastic(configuration)
                .AddTelegramBots(configuration)
                .AddScoped<IUnitOfWork, UnitOfWork>()
                .AddSingleton(configuration)
                .AddTransient<ILogger42, SerilogLogger42>();

            var serviceProvider = _serviceCollection.BuildServiceProvider();
            logger = serviceProvider.GetRequiredService<ILogger42>();
            _sender = serviceProvider.GetRequiredService<ISender>();
            _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();

            NoCommitMode = bool.Parse(configuration[nameof(NoCommitMode)]);
            Logger42.Init(serviceProvider);

            Recalculate();
            Console.WriteLine("Обновление стоимости закончено");
            Console.ReadLine();
            Console.WriteLine("Сохранить результат?");
            Console.ReadLine();
            Console.ReadLine();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"ОШИБКА ПРИЛОЖЕНИЯ {ex}");
            Console.ReadLine();
        }
    }


    /// <summary>
    /// Пересчитать стоимость Аренды 1С для аккаунтов
    /// </summary>
    public static void Recalculate()
    {
        Console.WriteLine($"Обновление стоимости в таблице Rate для 'Мой диск' в UA локали  на {MyDiskCostConst.RateDisk}");
        UpdateRatesDisk(ResourceType.DiskSpace, MyDiskCostConst.RateDisk);
        UpdateRatesRent(SelectRent1CRatesData().ToList());
        UpdateCurrentAccounts();
    }

    private static void UpdateRatesDisk(ResourceType resourceType, decimal newRate)
    {
        var rates = SelectRent1CStandardRatesDataForLocale(LocaleConst.Ukraine, resourceType)
            .Where(x => x.Rate.Cost != 0)
            .ToList();

        int updatedCount = 0;
        using var transaction = _dbLayer.SmartTransaction.Get();
        try
        {
            foreach (var rate in rates)
            {
                if (rate.ResourceType != resourceType ||
                    rate.Rate.Cost == newRate)
                {
                    Console.WriteLine($"Пересчет тарифов завершен так как ресурс не совпадает или стоимость уже изменена для сервиса {resourceType}");
                    continue;
                }

                var prevValue = rate.Rate.Cost;
                rate.Rate.Cost = newRate;
                _dbLayer.RateRepository.Update(rate.Rate);
                _dbLayer.Save();
                Console.WriteLine($"Тариф {rate.Rate.Id} изменен с {prevValue} на {rate.Rate.Cost} для сервиса {resourceType}");
                updatedCount++;
            }
            if (!NoCommitMode)
            {
                transaction.Commit();
            }
            else
            {
                transaction.Rollback();
            }
            Console.WriteLine($"Изменение тарифов завершено. Изменено записей: {updatedCount} для сервиса {resourceType}");
        }
        catch (Exception)
        {
            transaction.Rollback();
            throw;
        }
    }
    public static IQueryable<RateData> SelectRent1CStandardRatesDataForLocale(string localeName, ResourceType resourceType) =>
        (from rate in _dbLayer.RateRepository.WhereLazy()
            join locale in _dbLayer.LocaleRepository.WhereLazy() on rate.LocaleId equals locale.ID
            join serviceType in _dbLayer.BillingServiceTypeRepository.WhereLazy() on rate.BillingServiceTypeId
                equals serviceType.Id
            where locale.Name == localeName && serviceType.SystemServiceType == resourceType
            select new RateData
            {
                Rate = rate,
                ResourceType = serviceType.SystemServiceType.Value
            });

    private static void UpdateRatesRent(List<RateData> reateData)
    {
        using var transaction = _dbLayer.SmartTransaction.Get();
        try
        {
            foreach (var rate in reateData)
            {
                var cosr = rate.ResourceType == ResourceType.MyEntUser
                    ? Rent1CLicenseCostConst.MyEntUser
                    : Rent1CLicenseCostConst.MyEntUserWeb;

                var prevValue = rate.Rate.Cost;
                rate.Rate.Cost = cosr;
                _dbLayer.RateRepository.Update(rate.Rate);
                _dbLayer.Save();
                Console.WriteLine($"Тариф {rate.Rate.Id} изменен с {prevValue} на {rate.Rate.Cost}");
                logger.Info($"Тариф {rate.Rate.Id} изменен с {prevValue} на {rate.Rate.Cost}");
            }

            if (!NoCommitMode)
            {
                transaction.Commit();
            }
            else
            {
                transaction.Rollback();
            }
            Console.WriteLine("Изменение тарифов для аренды завершено.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Ошибка при изменение тарифов для аренды {ex.Message}.");
            logger.Warn(ex, $"Ошибка при изменение тарифов для аренды {ex.Message}.");
            transaction.Rollback();
            throw;
        }
    }

    /// <summary>
    /// Выбрать тарифы Аренды 1С украинской локали
    /// </summary>
    /// <returns>Тарифы Аренды 1С украинской локали</returns>
    public static IEnumerable<RateData> SelectRent1CRatesData() =>
        (from rate in _dbLayer.RateRepository.WhereLazy()
            join locale in _dbLayer.LocaleRepository.WhereLazy() on rate.LocaleId equals locale.ID
            join serviceType in _dbLayer.BillingServiceTypeRepository.WhereLazy() on rate.BillingServiceTypeId
                equals serviceType.Id
            where locale.Name == LocaleConst.Ukraine &&
                  (serviceType.SystemServiceType == ResourceType.MyEntUserWeb ||
                   serviceType.SystemServiceType == ResourceType.MyEntUser)
            select new RateData
            {
                Rate = rate,
                ResourceType = serviceType.SystemServiceType ?? ResourceType.MyEntUser
            }).AsEnumerable();

    private static void UpdateCurrentAccounts()
    {
        var accountIds = SelectAccountRent1CStandardResourcesForAccount(_dbLayer, LocaleConst.Ukraine)
            .Where(x => x.ResourceType == ResourceType.MyEntUser || x.ResourceType == ResourceType.MyEntUserWeb)
            .GroupBy(x => x.BillingAccountId)
            .Select(x => x.Key)
            .ToList();
        int updatedCount = accountIds.Count;
        Console.WriteLine($"Выбрано аккаунтав для изменения {updatedCount}");
        foreach (var accountId in accountIds)
        {

            try
            {
                logger.Info($"Обработка аккаунта {accountId}");
                Console.WriteLine($"Обработка аккаунта {accountId}");
                UpdateAccountResources(accountId);

                logger.Info($"Обработка аккаунта {accountId} успешно завершена");
                Console.WriteLine($"Обработка аккаунта {accountId} успешно завершена");
            }
            catch (Exception ex)
            {
                logger.Info($"ERROR: При обработке аккаунта {accountId} произошла ошибка. {ex.Message}");
                Console.WriteLine($"ERROR: При обработке аккаунта {accountId} произошла ошибка. {ex.Message}");
            }
            Console.WriteLine($"Осталось аккаунтав для изменения {updatedCount}");

            updatedCount--;
        }
    }

    private static void UpdateAccountResources(Guid accountId)
    {

        var resourcesToBeUpdatedRdp = SelectResourceDataForBillingAccount(_dbLayer, accountId)
            .Where(x => x.Resource.BillingServiceType.SystemServiceType == ResourceType.MyEntUser)
            .Where(x => x.Resource.Cost != Rent1CLicenseCostConst.MyEntUser)
            .ToList();
        var resourcesToBeUpdatedWeb = SelectResourceDataForBillingAccount(_dbLayer, accountId)
            .Where(x => x.Resource.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb)
            .Where(x => x.Resource.Cost != Rent1CLicenseCostConst.MyEntUserWeb)
            .ToList();

        if(!resourcesToBeUpdatedRdp.Any() && resourcesToBeUpdatedWeb.Any())
        {
            Console.WriteLine($"Для аккаунта {accountId} нет изменяемых ресурсов");
            logger.Info($"Для аккаунта {accountId} нет изменяемых ресурсов");
            return;
        }
        var updatesTracker = new AccountUpdatesTracker();
        if (resourcesToBeUpdatedRdp.Any())
        {
            Console.WriteLine($"Для аккаунта {accountId} выявлено {resourcesToBeUpdatedRdp.Count} ресурсов для изменения");
            logger.Info($"Для аккаунта {accountId} выявлено {resourcesToBeUpdatedRdp.Count} ресурсов для изменения");
            foreach (var resourceInfo in resourcesToBeUpdatedRdp)
            {
                Console.WriteLine($"Изменение стоимости ресурсов  {ResourceType.MyEntUser} для аккаунта {accountId}");
                logger.Info($"Изменение стоимости ресурсов  {ResourceType.MyEntUser} для аккаунта {accountId}");
                UpdateAccountResource(resourceInfo.Resource, resourceInfo.ResourceConfiguration, updatesTracker, Rent1CLicenseCostConst.MyEntUser);
            }
        }

        if (resourcesToBeUpdatedWeb.Any())
        {
            logger.Info($"Для аккаунта {accountId} выявлено {resourcesToBeUpdatedWeb.Count} ресурсов для изменения");
            Console.WriteLine($"Для аккаунта {accountId} выявлено {resourcesToBeUpdatedWeb.Count} ресурсов для изменения");

            foreach (var resourceInfo in resourcesToBeUpdatedWeb)
            {
                Console.WriteLine($"Изменение стоимости ресурсов  {ResourceType.MyEntUserWeb} для аккаунта {accountId}");
                logger.Info($"Изменение стоимости ресурсов  {ResourceType.MyEntUserWeb} для аккаунта {accountId}");
                UpdateAccountResource(resourceInfo.Resource, resourceInfo.ResourceConfiguration, updatesTracker, Rent1CLicenseCostConst.MyEntUserWeb);
            }

        }

        Console.WriteLine($"Начинаем пересчета стоимости сервиса  для аккаунта {accountId}");
        logger.Info($"Начинаем пересчета стоимости сервиса  для аккаунта {accountId}");

        var servicesForUpdateCost = updatesTracker.UpdatedResources
            .Select(x => x.UpdatedResource.BillingServiceType.Service)
            .Distinct().ToList();
        
        _sender.Send(new RecalculateResourcesConfigurationCostCommand([accountId],
            servicesForUpdateCost.Select(x => x.Id).ToList(), Clouds42Service.MyEnterprise)).Wait();
       
        Console.WriteLine($"Изменение стоимости ресурсов для аккаунта {accountId} успешно завершено. Изменено ресурсов: {updatesTracker.UpdatedResources.Count}");

        logger.Info($"Изменение стоимости ресурсов для аккаунта {accountId} успешно завершено. Изменено ресурсов: {updatesTracker.UpdatedResources.Count}");

    }


    /// <summary>
    /// Выбрать данные ресурсов для аккаунта-плательшика
    /// </summary>
    /// <param name="billingAccountId">ID аккаунта плательщика</param>
    /// <returns>Данные ресурсов для аккаунта-плательщика</returns>
    public static IQueryable<AccountResourceData> SelectResourceDataForBillingAccount(IUnitOfWork dbLayer, Guid billingAccountId)
    {
        return
            from resource in dbLayer.ResourceRepository.WhereLazy()
            join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy()
                on resource.BillingServiceTypeId equals serviceType.Id
            where
                resource.AccountSponsorId == billingAccountId ||
                (!resource.AccountSponsorId.HasValue && resource.AccountId == billingAccountId)
            select new
            {
                sponsorAccountId = resource.AccountSponsorId ?? resource.AccountId,
                resource
            } into rent1CResourcesInfo
            join resourcesConfiguration in dbLayer.ResourceConfigurationRepository.WhereLazy()
                on new
                {
                    rent1CResourcesInfo.resource.BillingServiceType.ServiceId,
                    AccountId = rent1CResourcesInfo.sponsorAccountId
                } equals new
                {
                    ServiceId = resourcesConfiguration.BillingServiceId, resourcesConfiguration.AccountId
                } into resourceConfigurations
            from resourceConfiguration in resourceConfigurations.Take(1).DefaultIfEmpty()
            select new AccountResourceData
            {
                BillingAccountId = rent1CResourcesInfo.sponsorAccountId,
                Resource = rent1CResourcesInfo.resource,
                ResourceConfiguration = resourceConfiguration
            };
    }

    private static void UpdateAccountResource(Resource resource, ResourcesConfiguration resourceConfiguration,
        AccountUpdatesTracker updatesTracker, decimal newRate)
    {
        using var transaction = _dbLayer.SmartTransaction.Get();
        try 
        {

            if (resource.Cost == newRate)
            {
                logger.Info($"Пропускаю ресурс {resource.Id}. Стоимость соответствует ожидаемой {resource.Cost}");
                return;
            }
            var trackerItem = new AccountUpdatesTrackerResourceItem
            {
                ResourceCostBeforeUpdate = resource.Cost,
                IsDemoResourceBeforeUpdate = resource.DemoExpirePeriod.HasValue && (resource.DemoExpirePeriod.Value - DateTime.Now).Days > 0,
                ResourceSubjectBeforeUpdate = resource.Subject,
                IsServiceCostFixedBeforeUpdate = resourceConfiguration == null || resourceConfiguration.CostIsFixed == true,
                UpdatedResource = resource,
            };
            resource.Cost = newRate;
            _dbLayer.ResourceRepository.Update(resource);
            _dbLayer.Save();
            updatesTracker.UpdatedResources.Add(trackerItem);


            if (!NoCommitMode)
            {
                transaction.Commit();
            }
            else
            {
                transaction.Rollback();
            }
        } 
        catch (Exception ex)
        {
            Console.WriteLine($"Ошибка при обновлении ресурсов {resource.BillingServiceType} для аккаунта {resource.AccountId} {ex.Message}.");
            logger.Warn(ex, $"Ошибка при обновлении ресурсов {resource.BillingServiceType} для аккаунта {resource.AccountId} {ex.Message}.");
            transaction.Rollback();
        }
    }

    /// <summary>
    /// Выбрать данные для пересчета Аренды 1С Standard, для аккаунтов локали
    /// </summary>
    /// <param name="localeName">Имя локали по которой нужно выбрать аккаунты</param>
    /// <returns>Данные для пересчета стоимости Аренды 1С Standard для аккаунтов</returns>
    public static IQueryable<BillingAccountResourceData> SelectAccountRent1CStandardResourcesForAccount(IUnitOfWork dbLayer, string localeName) =>
        from resource in dbLayer.ResourceRepository.WhereLazy()
        join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource.BillingServiceTypeId
            equals serviceType.Id
        select new
        {
            accountId = resource.AccountSponsorId ?? resource.AccountId,
            resource,
            serviceType
        }
        into rent1CResourcesInfo
        join accountConfiguration in dbLayer.GetGenericRepository<AccountConfiguration>().WhereLazy()
            on rent1CResourcesInfo.accountId equals accountConfiguration.AccountId
        join locale in dbLayer.LocaleRepository.WhereLazy()
            on accountConfiguration.LocaleId equals locale.ID
        where
            locale.Name == localeName &&
            !accountConfiguration.IsVip
        select new BillingAccountResourceData
        {
            BillingAccountId = rent1CResourcesInfo.accountId,
            ResourceId = rent1CResourcesInfo.resource.Id,
            ResourceType = rent1CResourcesInfo.serviceType.SystemServiceType
        };


}

public static class DependencyConfiguration
{
    public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
    {
        var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

        var elasticConfig = new ElasticConfigDto(
            conf["Elastic:Uri"],
            conf["Elastic:UserName"],
            conf["Elastic:Password"],
            conf["Elastic:ApiIndex"],
            conf["Logs:AppName"]
        );

        SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);
        return collection;
    }
}
