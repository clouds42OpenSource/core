﻿namespace PathChangeCostRent1cForUkrain.Constants
{
    /// <summary>
    /// Константы стоимости лицензий Аренды 1С
    /// </summary>
    public static class Rent1CLicenseCostConst
    {
        /// <summary>
        /// Стоимость Rdp лицензии
        /// </summary>
        public const decimal MyEntUser = 150;

        /// <summary>
        /// Стоимость Web лицензии
        /// </summary>
        public const decimal MyEntUserWeb = 580;
    }
}
