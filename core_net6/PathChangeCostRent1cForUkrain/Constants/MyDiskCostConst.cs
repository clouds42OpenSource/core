﻿namespace PathChangeCostRent1cForUkrain.Constants
{
    /// <summary>
    /// Константы стоимости лицензий Диска
    /// </summary>
    public static class MyDiskCostConst
    {
        /// <summary>
        /// Стоимость Диска
        /// </summary>
        public const decimal RateDisk = 50;

    }
}
