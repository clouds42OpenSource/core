﻿using CommonLib.Enums;

namespace PathChangeCostRent1cForUkrain.Model
{
    /// <summary>
    /// Данные о ресурсе
    /// </summary>
    public class BillingAccountResourceData
    {
        /// <summary>
        /// Id аккаунта биллинга
        /// </summary>
        public Guid BillingAccountId { get; set; }

        /// <summary>
        /// ID ресурса
        /// </summary>
        public Guid ResourceId { get; set; }

        /// <summary>
        /// Тип ресурса
        /// </summary>
        public ResourceType? ResourceType { get; set; }
    }
}
