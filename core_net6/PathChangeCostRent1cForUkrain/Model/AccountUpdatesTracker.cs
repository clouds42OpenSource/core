﻿
namespace PathChangeCostRent1cForUkrain.Model
{
    /// <summary>
    /// Трекер изменений ресурсов аккаунта
    /// </summary>
    public class AccountUpdatesTracker
    {
        /// <summary>
        /// Данные измененний ресурсов
        /// </summary>
        public List<AccountUpdatesTrackerResourceItem> UpdatedResources { get; }
            = [];
    }
}
