﻿using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;

namespace PathChangeCostRent1cForUkrain.Model
{
    /// <summary>
    /// Данные тарифа
    /// </summary>
    public class RateData
    {
        /// <summary>
        /// Тариф
        /// </summary>
        public Rate Rate { get; set; }

        /// <summary>
        /// Тип ресурса
        /// </summary>
        public ResourceType ResourceType { get; set; }
    }
}
