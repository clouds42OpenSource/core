﻿using System;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Segment.Contracts.AccountUserJsonWebToken.Interfaces;

namespace Clouds42.CoreWorker.AccountUserJobs.RemoveObsoleteUserJsonWebTokens
{
    /// <summary>
    /// Джоба для удаления устаревших токенов
    /// пользователей
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RemoveObsoleteAccountUserJsonWebTokensJob)]
    public class RemoveObsoleteAccountUserJsonWebTokensJob(
        IRemoveObsoleteUserTokensProvider removeObsoleteUserTokensProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить джобу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId) =>
            removeObsoleteUserTokensProvider.Remove();
    }
}
