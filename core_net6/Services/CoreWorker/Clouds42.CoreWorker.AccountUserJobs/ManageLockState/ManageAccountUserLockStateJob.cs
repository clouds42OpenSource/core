﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.CoreWorker;
using System;
using Clouds42.AccountUsers.Contracts.ManageAccountUserLockState.Interfaces;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountUserJobs.ManageLockState
{
    /// <summary>
    /// Задача управления состоянием блокировки пользователя в МС
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ManageAccountUserLockStateJob)]
    public class ManageAccountUserLockStateJob(
        IUnitOfWork dbLayer,
        IAccountUserLockStateProvider accountUserLockStateProvider)
        : CoreWorkerParamsJobWithRetry<ManageAccountUserLockStateParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры перезапуска задачи</returns>
        protected override RetryJobParamsDto ExecuteAndDetermineRetryNeed(ManageAccountUserLockStateParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                accountUserLockStateProvider.ChangeLockState(jobParams);
                return CreateRetryParams(taskQueueId, false);
            }
            catch (Exception)
            {
                return CreateRetryParams(taskQueueId, true, 90);
            }
        }
    }
}
