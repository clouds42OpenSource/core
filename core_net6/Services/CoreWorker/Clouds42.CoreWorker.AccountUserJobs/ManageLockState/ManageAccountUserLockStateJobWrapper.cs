﻿using System;
using Clouds42.Common.Exceptions;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountUserJobs.ManageLockState
{
    /// <summary>
    /// Обработчик задачи по управлению состоянием блокировки пользователя в МС
    /// </summary>
    public class ManageAccountUserLockStateJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<ManageAccountUserLockStateJob, ManageAccountUserLockStateParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        public override TypingParamsJobWrapperBase<ManageAccountUserLockStateJob, ManageAccountUserLockStateParamsDto> Start(ManageAccountUserLockStateParamsDto paramsJob)
        {
            StartTask(paramsJob, GetTaskComment(paramsJob.AccountUserId));
            return this;
        }

        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <param name="startTaskDelay">Задержка выполения задачи</param>
        public TypingParamsJobWrapperBase<ManageAccountUserLockStateJob, ManageAccountUserLockStateParamsDto> Start(ManageAccountUserLockStateParamsDto paramsJob, DateTime startTaskDelay)
        {
            StartTask(paramsJob, GetTaskComment(paramsJob.AccountUserId), startTaskDelay);
            return this;
        }
        /// <summary>
        /// Получить комментарий для задачи
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Комментарий для задачи</returns>
        private string GetTaskComment(Guid accountUserId)
        {
            var accountUser = dbLayer.AccountUsersRepository.GetAccountUser(accountUserId)
                              ?? throw new NotFoundException($"Пользователь по ID {accountUserId} не найден");

            return $"Смена состояния блокировки пользователя '{accountUser.Login}' в МС.";
        }
    }
}
