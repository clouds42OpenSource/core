﻿using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.AccountDatabaseUpdateJob)]
    public class AccountDatabaseUpdateJob(
        IUnitOfWork dbLayer,
        IAutoUpdateAccountDatabasesProvider autoUpdateAccountDatabasesProvider)
        : CoreWorkerParamsJob<AccountDatabaseUpdateJobParamsDto>(dbLayer)
    {
        protected override void Execute(AccountDatabaseUpdateJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            autoUpdateAccountDatabasesProvider.UpdateDatabase(jobParams.AccountDatabaseId);
        }
    }
}
