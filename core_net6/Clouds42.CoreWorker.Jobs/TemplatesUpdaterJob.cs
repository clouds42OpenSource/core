﻿using Clouds42.AccountDatabase.Contracts.Delimiters.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.TemplatesUpdaterJob)]
    public class TemplatesUpdaterJob(IDbTemplateUpdateProvider dbTemplateUpdateProvider) : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            dbTemplateUpdateProvider.UpdateTemplates();
        }
    }
}
