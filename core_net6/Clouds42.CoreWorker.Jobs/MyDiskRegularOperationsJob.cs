﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Джоба регламентных операций с пространством сервиса "мой диск"
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.MyDiskRegularOperations)]
    public class MyDiskRegularOperationsJob(
        IMyDiskAnalyzeProvider myDiskAnalyzeProvider,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            ExecuteCalculateDiskMemory();
        }

        /// <summary>
        /// Выполнение регламентных операций с пространством сервиса "мой диск":
        /// дискового пространства.
        /// </summary>       
        private void ExecuteCalculateDiskMemory()
        {
            logger.Info("Начинаем расчет пространства.");

            try
            {
                var ignoreAccountIds = configuration.GetSection("MyDisk.AccountIdsForIgnore").Get<List<Guid>>() ?? [];

                var accountIds = dbLayer.AccountsRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.BillingAccount.ResourcesConfigurations.Any(z => z.BillingService.SystemService == Clouds42Service.MyEnterprise && z.Frozen.HasValue && (!z.Frozen.Value || z.ExpireDate > DateTime.Now)) && (!ignoreAccountIds.Any() || !ignoreAccountIds.Contains(x.Id)))
                    .Select(x => x.Id)
                    .Distinct()
                    .ToList();

                logger.Info($"Для рассчета пространства выбрано {accountIds.Count}");

                var count = accountIds.Count;

                foreach ( var accountId in accountIds)
                {
                    logger.Info($"Начинаем пересчет занятого пространства для аккаунта {accountId}");
                    myDiskAnalyzeProvider.RecalculateClientFilesInMyDisk(accountId);
                    count--;
                    logger.Info($"Для рассчета пространства осталось аккаунтов {count}");
                }

            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, "[Ошибка при выполнении (Пересчет дискового пространства)]");
            }
            logger.Info("Завершен расчет пространства.");
        }
    }
}
