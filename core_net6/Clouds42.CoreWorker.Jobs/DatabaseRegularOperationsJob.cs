﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Newtonsoft.Json;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    ///     Выполнение регламентных операций с базами данных:
    ///     - пересчет размера информационных баз
    ///     Вызывается 1 раз в сутки - ночью
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DatabaseRegularOperations)]
    public class DatabaseRegularOperationsJob(
        IAccountDatabaseManager accountDatabaseManager,
        IUnitOfWork dbLayer)
        : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            Logger.Info($"[{taskQueueId}] :: Таска DatabaseRegularOperationsJob стартовала");

            var tasksQueue = dbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.Id == taskQueueId);
            if (tasksQueue.TaskParameter != null && !string.IsNullOrEmpty(tasksQueue.TaskParameter.TaskParams))
            {
                Logger.Info($"[{taskQueueId}] :: TaskParams: {tasksQueue.TaskParameter.TaskParams}");
                var accountDatabaseId = JsonConvert.DeserializeObject<Guid>(tasksQueue.TaskParameter.TaskParams);
                if (!accountDatabaseId.IsNullOrEmpty())
                {
                    Logger.Info($"[{taskQueueId}] :: Запускаем перерасчет базы: {accountDatabaseId}");
                    accountDatabaseManager.RecalculateSizeOfAccountDatabase(accountDatabaseId);
                    Logger.Info($"[{taskQueueId}] :: Перерасчет базы '{accountDatabaseId}' выполнен!");

                    Logger.Info($"[{taskQueueId}] :: Таска DatabaseRegularOperationsJob закончена");
                    return;
                }
            }

            Logger.Info($"[{taskQueueId}] :: Запускаем перерасчет всех баз!");

            var accountDatabases = dbLayer.DatabasesRepository
                .WhereLazy(db => db.State == DatabaseState.Ready.ToString() && db.AccountDatabaseOnDelimiter == null)
                .Select(d => d.Id).ToList();

            accountDatabases.ForEach(accountDatabaseManager.RecalculateSizeOfAccountDatabase);

            Logger.Info($"[{taskQueueId}] :: Перерасчет всех баз закончен!");

            Logger.Info($"[{taskQueueId}] :: Таска DatabaseRegularOperationsJob закончена");
        }
    }
}