﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Получение последней активности в базе
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.InfoBaseLastActiveUpdateJob)]
    public class InfoBaseLastActiveUpdateJob(IUnitOfWork dbLayer, IFileStorageHelper fileStorageHelper) : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var publishedInfoBases =
               dbLayer.DatabasesRepository.Where(db => db.PublishState == PublishState.Published.ToString() 
               && db.State == DatabaseState.Ready.ToString()).ToList();

            Logger.Info($"Найдено {publishedInfoBases.Count} опубликованных баз.");

            foreach (var infoBase in publishedInfoBases)
            {
                Logger.Info($"Обновление даты активности для базы {infoBase.V82Name}");
                if (infoBase.IsFile.HasValue && infoBase.IsFile.Value)
                {
                    try
                    {
                        var lastActivity = fileStorageHelper.GetLastActiveDateFileBase(infoBase);

                        if (lastActivity > infoBase.LastActivityDate)
                        {
                            infoBase.LastActivityDate = lastActivity;
                            dbLayer.DatabasesRepository.Update(infoBase);
                            dbLayer.Save();

                            Logger.Info(
                                $"Дата активности базы {infoBase.V82Name} обновлена на {lastActivity:yyyy-MM-dd hh:mm:ss}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Warn(ex, $"[Ошибка обновления даты активности для базы] {infoBase.V82Name} ");
                    }
                }

            }
        }
    }
}

