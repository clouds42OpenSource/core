﻿using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;
using Newtonsoft.Json;

namespace Clouds42.CoreWorker.Jobs.Archivation
{
    /// <summary>
    ///     Архивация в склеп файлов аккаунта
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ArchivateAccountFilesToTombJob)]
    public class ArchivateAccountFilesToTombJob(
        IUnitOfWork dbLayer,
        IArchiveAccounFilesToTombProvider archiveAccounFilesToTombProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            Logger.Info($"[{taskQueueId}] :: Задача {nameof(ArchivateAccountFilesToTombJob)} запущена");

            var taskParams = GetTaskParams(taskQueueId);
            if (taskParams == null)
                return;

            Logger.Info($"[{taskQueueId}] :: Отправка в склеп файлов аккаунта {taskParams.AccountId}");

            archiveAccounFilesToTombProvider.DetachAccounFilesToTomb(taskParams);

            Logger.Info($"[{taskQueueId}] :: Задача {nameof(ArchivateAccountFilesToTombJob)} закончена");
        }

        /// <summary>
        /// Получить параметры задачи
        /// </summary>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры задачи</returns>
        private DeleteAccountFilesToTombJobParamsDto? GetTaskParams(Guid taskQueueId)
        {
            var task = dbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(w => w.Id == taskQueueId);
            if (task.TaskParameter == null || string.IsNullOrEmpty(task.TaskParameter.TaskParams))
            {
                Logger.Info($"[{taskQueueId}] :: TaskParams пустой");
                Logger.Info($"[{taskQueueId}] :: Задача {nameof(ArchivateAccountFilesToTombJob)} закончена");
                return null;
            }

            return JsonConvert.DeserializeObject<DeleteAccountFilesToTombJobParamsDto>(task.TaskParameter.TaskParams);
        }
    }
}
