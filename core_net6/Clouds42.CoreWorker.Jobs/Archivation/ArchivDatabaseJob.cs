﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;

namespace Clouds42.CoreWorker.Jobs.Archivation
{
    /// <summary>
    ///     Архивация и удаление в склеп ИБ неактивных аккаунтов 
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ArchivDatabaseJob)]
    public class ArchivDatabaseJob(
        IAccountFilesToTombProvider accountFilesToTombProvider,
        IAccountDatabaseManager accountDatabaseManager,
        IStartProcessOfArchiveDatabaseToTombProvider startProcessOfArchiveDatabaseToTombProvider,
        ILogger42 logger)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var archiveDatabaseList = accountDatabaseManager.GetArchiveDatabaseList().Result;
            var databasesIds = archiveDatabaseList.Select(s => new { s.DataBaseId, s.V82Name }).Distinct().ToList();

            logger.Info($"Количество баз для архивации: {databasesIds.Count}");

            foreach (var db in databasesIds)
            {
                startProcessOfArchiveDatabaseToTombProvider.StartProcessOfArchiveDatabaseToTomb(
                    new StartProcessOfArchiveDatabaseToTombParamsDto
                    {
                        DatabaseId = db.DataBaseId,
                        Trigger = CreateBackupAccountDatabaseTrigger.OverdueRent,
                        NeedForceFileUpload = true,
                        NeedNotifyHotlineAboutFailure = false
                    });

                logger.Info($"Попытка архивации базы в склепе '{db.V82Name}',");
            }

            var accountIds = archiveDatabaseList.Select(db => db.AccountId).Distinct().ToList();

            accountIds.ForEach(accountId =>
            {
                accountFilesToTombProvider.ArchiveAccountFilesToTomb(accountId);
                logger.Info($"Попытка архивации файлов аккаунта '{accountId}' в склеп");
            });
        }
    }
}