﻿using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Constants;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData;
using Clouds42.LetterNotification.Mails.MailNotificationFactoryData.Templates.AccountDatabaseDeleteToTomb;
using Clouds42.Repositories.Interfaces.Common;
using Newtonsoft.Json;

namespace Clouds42.CoreWorker.Jobs.Archivation
{
    /// <summary>
    ///     Архивация в склеп информационной базы
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ArchivateAccountDatabaseToTombJob)]
    public class ArchivateAccountDatabaseToTombJob(
        IUnitOfWork dbLayer,
        MailNotificationFactory mailNotificationFactory,
        IArchiveAccountDatabaseToTombManager archiveAccountDatabaseToTombManager)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            Logger.Info($"[{taskQueueId}] :: Задача {nameof(ArchivateAccountDatabaseToTombJob)} запущена");

            var taskParams = GetTaskParams(taskQueueId);
            if (taskParams == null)
                return;

            Logger.Info($"[{taskQueueId}] :: Отправка в склеп ИБ {taskParams.AccountDatabaseId}");

            var result = archiveAccountDatabaseToTombManager.DetachAccountDatabaseToTomb(taskParams);

            ProcessArchiveDatabaseResult(result, taskParams, taskQueueId);

            Logger.Info($"[{taskQueueId}] :: ИБ {taskParams.AccountDatabaseId} отправлена в склеп");
            Logger.Info($"[{taskQueueId}] :: Задача {nameof(ArchivateAccountDatabaseToTombJob)} закончена");
        }

        /// <summary>
        /// Получить параметры задачи
        /// </summary>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры задачи</returns>
        private DeleteAccountDatabaseToTombJobParamsDto GetTaskParams(Guid taskQueueId)
        {
            var task = dbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(w => w.Id == taskQueueId);
            if (task.TaskParameter == null || string.IsNullOrEmpty(task.TaskParameter.TaskParams))
            {
                Logger.Info($"[{taskQueueId}] :: TaskParams пустой");
                Logger.Info($"[{taskQueueId}] :: Задача {nameof(ArchivateAccountDatabaseToTombJob)} закончена");
                return null;
            }

            return JsonConvert.DeserializeObject<DeleteAccountDatabaseToTombJobParamsDto>(task.TaskParameter.TaskParams);
        }

        /// <summary>
        /// Обработать результат архивации базы
        /// </summary>
        /// <param name="result">Результат архивации базы</param>
        /// <param name="taskParams">Параметры задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        private void ProcessArchiveDatabaseResult(ManagerResult result, DeleteAccountDatabaseToTombJobParamsDto taskParams, Guid taskQueueId)
        {
            if (!result.Error)
                return;

            Logger.Info($"[{taskQueueId}] :: Ошибка отправки в склеп базы {taskParams.AccountDatabaseId}. {result.Message}");

            if (taskParams.SendMail)
                mailNotificationFactory.SendMail<MailNotifySendToTombAccountDatabaseJobHotlineError, string>(
                    $"Ошибка архивации информационной базы '{taskParams.AccountDatabaseId}' в склеп: " +
                    result.Message);
        }
    }
}