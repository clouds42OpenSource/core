﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.CoreWorker.Jobs
{

    /// <summary>
    /// Задача стартующая обработчик по начислению лицензий в рамках активного сервиса Аренда 1С.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DocLoaderByRent1CProlongationJob)]
    public class DocLoaderByRent1CProlongationJob(
        IDocLoaderByRent1CProlongationProcessor docLoaderByRent1CProlongationProcessor,
        IHandlerException handlerException)
        : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            try
            {
                docLoaderByRent1CProlongationProcessor.ProcessIncreaseForAccounts();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка регламентного начисления ЗД аккаунтам с арендой 1С.]");
                throw;
            }
        }
    }
}
