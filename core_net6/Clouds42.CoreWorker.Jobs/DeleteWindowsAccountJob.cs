﻿using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Задача для удаления профиля Windows для пользователей
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DeleteWindowsProfileJob)]
    public class DeleteWindowsProfileJob(IUnitOfWork dbLayer) : CoreWorkerJob
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Выполнить задачу 
        /// </summary>
        /// <param name="taskId">Id задачи</param>
        /// <param name="taskQueueId">Id запущенной задачи</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            _logger.Info("Начата задача по удалению неактивных виндовс профилей");

            var deleteArchiveDays = int.Parse(CloudConfigurationProvider.CoreWorker.GetDeleteArchiveDays());

            var clearingDate = DateTime.Now.AddDays(-deleteArchiveDays);
            var accountIdsForClear = dbLayer.ResourceConfigurationRepository.WhereLazy(r =>
                    r.BillingService.SystemService == Clouds42Service.MyEnterprise && r.ExpireDate != null &&
                    r.ExpireDate < clearingDate)
                .Select(r => r.AccountId);

            var accountUsersForClear = from id in accountIdsForClear
                                       join accountUser in dbLayer.AccountUsersRepository.WhereLazy() on id equals accountUser.AccountId
                                       join account in dbLayer.AccountsRepository.WhereLazy() on accountUser.AccountId equals account.Id
                                       join accountConfiguration in dbLayer.GetGenericRepository<AccountConfiguration>().WhereLazy() on
                                           account.Id equals accountConfiguration.AccountId
                                       join segment in dbLayer.CloudServicesSegmentRepository.WhereLazy() on accountConfiguration.SegmentId
                                           equals segment.ID
                                       select new { accountUser, segment };

            foreach (var item in accountUsersForClear.ToList())
            {
                if (string.IsNullOrEmpty(item.segment.WindowsAccountsPath))
                    continue;

                TryRemoveWindowsProfile(item.accountUser.Login, item.segment.WindowsAccountsPath);
            }

            _logger.Info("Задача завершена");
        }

        /// <summary>
        /// Попытаться удалить профиль windows
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="windowsAccountsPath">Путь к аккаунту windows</param>
        private static void TryRemoveWindowsProfile(string login, string windowsAccountsPath)
        {
            var nameV2 = $"{login}.AD.V2";
            var folderPath = Path.Combine(windowsAccountsPath, nameV2);
            try
            {
                if (!Directory.Exists(folderPath)) return;
                Directory.Delete(folderPath, true);
                _logger.Trace($"Профиль {nameV2} успешно удален");
            }
            catch (Exception ex)
            {
                _logger.Warn($"Ошибка удаления виндовс профиля {nameV2} : {ex}");
            }
        }
    }
}
