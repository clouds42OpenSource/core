﻿using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Название задачи не отражает её смысл, задача выполняет импорт баз, поставленных на автообновление или проведения ТиИ, в обновлятор
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.UpdateAccountDatabasesJob)]
    public class UpdateAccountDatabasesJob(
        IUnitOfWork dbLayer,
        IAutoUpdateAccountDatabasesProvider accountDatabasesSupportManager)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var accountDatabasesSupports = dbLayer.AcDbSupportRepository
                .Where(s => s.HasAutoUpdate && s.PrepearedForUpdate &&
                            s.AccountDatabase.State == DatabaseState.Ready.ToString() &&
                            (int)s.TimeOfUpdate == DateTime.Now.Hour)
                .ToList();

            accountDatabasesSupportManager.ProcessAutoUpdate(accountDatabasesSupports);
        }
    }
}
