﻿using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.TehSupportAccountDatabasesJob)]
    public class TehSupportAccountDatabasesJob(
        IUnitOfWork dbLayer,
        IAutoUpdateAccountDatabasesProvider autoUpdateAccountDatabasesProvider)
        : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {

            var accountDatabasesSupports = dbLayer.AcDbSupportRepository
                .Where(s => s.HasSupport && s.PreparedForTehSupport &&
                            s.AccountDatabase.State == DatabaseState.Ready.ToString())
                .ToList();

            autoUpdateAccountDatabasesProvider.ProcessAutoUpdate(accountDatabasesSupports);

        }

    }
}
