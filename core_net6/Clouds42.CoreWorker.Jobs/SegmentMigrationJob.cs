﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorkerTask.Helpers;
using Clouds42.DataContracts.Segment;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Задача по смене сегмента для аккаунтов
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.SegmentMigrationJob)]
    public class SegmentMigrationJob(
        IUnitOfWork dbLayer,
        IAccountSegmentMigrationProvider accountSegmentMigrationProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var migrationParams = GetSegmentMigrationParams(taskQueueId);
            accountSegmentMigrationProvider.ChangeAccountsSegment(migrationParams);
        }

        /// <summary>
        /// Получить параметры задачи
        /// </summary>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры задачи</returns>
        private SegmentMigrationParamsDto GetSegmentMigrationParams(Guid taskQueueId)
        {
            var task = dbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.Id == taskQueueId);

            if (task.TaskParameter == null || string.IsNullOrEmpty(task.TaskParameter.TaskParams))
                throw new ArgumentException($"{nameof(task.TaskParameter.TaskParams)} is null");

            var ser = new CoreWorkerTaskParamSerializer<SegmentMigrationParamsDto>();
            return ser.DeSerialize(task.TaskParameter.TaskParams);
        }
    }
}
