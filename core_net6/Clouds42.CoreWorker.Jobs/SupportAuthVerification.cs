﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Logger;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.SupportAuthVerification)]
    public class SupportAuthVerification(
        IAccountDatabaseManager accountDatabaseManager,
        IUnitOfWork dbLayer,
        ILogger42 logger)
        : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var task = dbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(x => x.Id == taskQueueId);

            if (task.TaskParameter == null || string.IsNullOrEmpty(task.TaskParameter.TaskParams))
            {
                logger.Warn($"Не обноружена задача воркера в очереди с id - {taskQueueId}");
                return;
            }

            var supportId = Guid.Parse(task.TaskParameter.TaskParams);
            accountDatabaseManager.TryAdminAuthorizeToAccountDatabase(supportId);
        }
    }
}
