﻿using System.Net;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Logger;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Проверка подключения к абонцентру
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.CorpPingTask)]
    public class CorpPingJob(IHandlerException handlerException, ILogger42 logger, IMessagesManager messagesManager)
        : CoreWorkerJob
    {
        private static readonly Uri _urlCorp = CloudConfigurationProvider.CorpApi.Url.GetUrlPing();

        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            logger.Trace("CorpPing task started");
            //адрес публикации абонцентра
            var req = (HttpWebRequest)WebRequest.Create(_urlCorp);
            req.ContentType = "application/xml";
            req.UserAgent = "CORE";
            try
            {
                var resp = (HttpWebResponse)req.GetResponse();
                logger.Trace(resp.StatusCode.ToString());
            }
            catch (Exception ex)
            {
                var messageText = $"[Ping] {_urlCorp} {ex.Message}";
                handlerException.Handle(ex, "[Отсутствует связь с абонцентром]");
                messagesManager.SendWorkerMail(messageText,
                    CloudConfigurationProvider.CoreWorker.GetSenderEmail(),
                    "Отсутствует связь с абонцентром.");
            }
            logger.Trace("CorpPing task finished");
        }
    }
}
