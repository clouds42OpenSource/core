﻿using Clouds42.AccountDatabase.Contracts.Tomb.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Отправка бэкапа информационной базы в склеп
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.SendBackupAccountDatabaseToTombJob)]
    public class SendBackupAccountDatabaseToTombJob(
        ITombManager tombManager,
        IUnitOfWork dbLayer) : CoreWorkerParamsJob<SendBackupAccountDatabaseToTombJobParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(SendBackupAccountDatabaseToTombJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            Logger.Info($"[{taskQueueId}] :: Отправка в склеп ИБ {jobParams.AccountDatabaseBackupId}");
            tombManager.MoveBackupToTomb(jobParams.AccountDatabaseId, jobParams.AccountDatabaseBackupId, jobParams.ForceUpload);
        }
    }
}