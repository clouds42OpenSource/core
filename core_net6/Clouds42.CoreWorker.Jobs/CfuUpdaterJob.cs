﻿using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Задача по обновлению CFU пакетов
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.CfuUpdater)]
    public class CfuUpdaterJob(ICfuManager cfuManager) : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            cfuManager.DownloadAvailableCfus();
        }
    }
}
