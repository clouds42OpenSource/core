﻿using Clouds42.AccountDatabase.Convert.Exceptions;
using Clouds42.AccountDatabase.Converter;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.FileBaseToServerTransformation;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.EmailContext.Commands;
using MediatR;

namespace Clouds42.CoreWorker.Jobs;



/// <summary>
/// джоб для запуска процесса перевода файловой базы в серверную
/// </summary>
[CoreWorkerJobName(CoreWorkerTasksCatalog.TransformFileBaseToServerJob)]
public class TransformFileBaseToServerJob(
    IUnitOfWork dbLayer,
    IConverter converter,
    IHandlerException handlerException,
    IAccessProvider accessProvider,
    ISender mediator)
    : CoreWorkerParamsJob<FileBaseToServerTransformationParams>(dbLayer)
{
    protected override void Execute(FileBaseToServerTransformationParams jobParams, Guid taskId, Guid taskQueueId)
    {
        try
        {
            Logger.Info($"[{taskQueueId}] :: Перевод файловой базы. Стартую конвертацию базы {jobParams.AccountDatabaseId}");
            
            converter
                .ConvertAsync(
                    jobParams.AccountDatabaseId,
                    jobParams.AccountId,
                    jobParams.AccountUserId,
                    jobParams.Login,
                    jobParams.Password,
                    jobParams.PhoneNumber,
                    jobParams.IsCopy)
                .Wait();
            
            SendLetter(jobParams, $"База { GetDatabaseName(jobParams.AccountDatabaseId)} была успешно переведена в серверную ");
            
        }
        catch (SessionStoppingException ex)
        {
            LogEventHelper.LogEvent(
                DbLayer,
                jobParams.AccountId,
                accessProvider,
                LogActions.SessionStoppingFailed, 
                ex.Message);
            SendLetter(jobParams, $"Перевод базы { GetDatabaseName(jobParams.AccountDatabaseId)} с файловой в серверную завершился с ошибкой во время остановки пулов");
            handlerException.Handle(ex, ex.Message);

        }
        catch (CdToDtTransformationException ex)
        {
            LogEventHelper.LogEvent(
                DbLayer,
                jobParams.AccountId,
                accessProvider,
                LogActions.DtTransformationFailed, 
                ex.Message);
            SendLetter(jobParams, $"Перевод базы { GetDatabaseName(jobParams.AccountDatabaseId)} с файловой в серверную завершился с ошибкой " +
                                  $"во время трансформации 1cd в dt");
            handlerException.Handle(ex, ex.Message); 

        }
        catch (Exception ex)
        {
            LogEventHelper.LogEvent(
                DbLayer,
                jobParams.AccountId,
                accessProvider,
                LogActions.FileToClusterTransformationFailed, 
                ex.Message);
            SendLetter(jobParams, $"Перевод базы { GetDatabaseName(jobParams.AccountDatabaseId)} с файловой в серверную завершился с ошибкой ");
            handlerException.Handle(ex, ex.Message);
        }
    }
    
    private  string? GetEmail(Guid accountUserId) =>   DbLayer
        .AccountUsersRepository
        .AsQueryableNoTracking()
        .Where(a => a.Id == accountUserId)
        .Select(a => a.Email)
        .FirstOrDefault();
    
    private void SendLetter(FileBaseToServerTransformationParams jobParams, string message) => mediator.Send(
        new SendEmailCommand
        {
            Body = $"База { GetDatabaseName(jobParams.AccountDatabaseId)} была успешно переведена в серверную ",
            Header = "Перевод базы в серверную",
            Subject = "Перевод базы в серверную",
            Locale = "ru-ru",
            To = GetEmail(jobParams.AccountUserId) ?? string.Empty,
            SendGridTemplateId = CloudConfigurationProvider.SendGrid.GetDefaultTemplateId()
        }).Wait();
    
    private string? GetDatabaseName(Guid databaseId) =>  DbLayer
        .DatabasesRepository
        .AsQueryableNoTracking()
        .Where(a => a.Id == databaseId).Select(d => d.V82Name)
        .FirstOrDefault();
}
