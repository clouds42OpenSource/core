﻿using System.Text;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Logger;
using Clouds42.LetterNotification.Contracts;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ReportJob)]
    public class ReportJob(
        IUnitOfWork unitOfWork,
        IMessagesManager messager,
        ILogger42 logger,
        IReportCoreWorkerTaskProvider reportCoreWorkerTaskProvider)
        : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var reportDate = DateTime.Now.AddDays(-1).Date.AddHours(8);
            var reportDateEndDay = reportDate.AddDays(1);

            //1.Выполенные задачи
            logger.Info("Начало выполнения");
            var allTasks = unitOfWork.CoreWorkerTaskRepository.Where();

            var tasks = unitOfWork.CoreWorkerTasksQueueRepository
                .Where(t => t.CreateDate > reportDate && t.CreateDate < reportDateEndDay)
                .Select(q => new
                {
                    taskName = allTasks.FirstOrDefault(t => t.ID == q.CoreWorkerTaskId)?.TaskName ?? "TaskNameNotFound",
                    q.CreateDate,
                    q.EditDate,
                    q.Status,
                    q.Comment
                }).ToList();

            logger.Info($"Найдено {tasks.Count} выполненных тасок.");
            StringBuilder sb = new StringBuilder();

            if (tasks.Any(t => t.Status == "Error"))
            {
                sb.Append("<p>Ошибки при выполнении регламентных задач:</p>");
                foreach (var it in tasks.Where(t => t.Status == "Error").OrderBy(k => k.CreateDate))
                {
                    sb.Append($"<p>{it.taskName} - {it.Status}. Время добавления: {it.CreateDate}. <br>Комментарий: {it.Comment}</p>");
                }
            }

            if (tasks.Any(t => t.Status != "Error"))
            {
                sb.Append("<p>Выполенные задачи.</p>");
                foreach (var it in tasks.Where(t => t.Status != "Error").OrderBy(k => k.CreateDate))
                {
                    sb.Append($"<p>{it.taskName} - {it.Status}. Время добавления: {it.CreateDate}</p>");
                }
            }

            string topik = CloudConfigurationProvider.CoreWorker.GetEmailReportJobTopik();
            messager.SendWorkerReport(
                sb.ToString(),
                topik);

            logger.Info("Выполнения завершено");

            reportCoreWorkerTaskProvider.SendDailyReportToTelegram();
        }
    }
}
