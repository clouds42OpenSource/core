﻿using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs.Audit
{
    /// <summary>
    /// Джоба аудита проверки целостности услуг ресурсов (по зависимостям) у всех аккаунтов
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.AuditAccountResourceIntegralityJob)]
    public class AccountServiceDependenciesAuditJob(
        IAccountResourceIntegralityAuditProvider accountResourceIntegralityAuditProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            accountResourceIntegralityAuditProvider.AuditAccountResourceIntegrality();
        }
    }
}
