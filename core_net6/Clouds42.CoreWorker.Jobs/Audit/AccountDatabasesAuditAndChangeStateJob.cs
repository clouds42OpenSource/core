﻿using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs.Audit
{
    /// <summary>
    ///     Формирование списка баз с зависшими статусами, изменение статусов баз и отправка письма на cloud-services
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.AccountDatabasesAuditAndChangeStateJob)]
    public class AccountDatabasesAuditAndChangeStateJob(
        IAuditAccountDatabasesStatesProvider auditAccountDatabasesStatesProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            auditAccountDatabasesStatesProvider.ProcessAudit();
        }
    }
}
