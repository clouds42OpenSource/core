﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs.Audit
{
    /// <summary>
    /// Задача для проверки очереди задач воркеров
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.AuditCoreWorkerTasksQueueJob)]
    public class AuditCoreWorkerTasksQueueJob(IAuditCoreWorkerTasksQueueProvider auditCoreWorkerTasksQueueProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            auditCoreWorkerTasksQueueProvider.AuditTasksQueue();
        }
    }
}
