﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.Invoices;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    ///     Создание фискального чека
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.CreateInvoiceFiscalReceiptJob)]
    public class CreateInvoiceFiscalReceiptJob(
        IUnitOfWork dbLayer,
        IInvoiceReceiptProvider paymentReceiptProvider)
        : CoreWorkerParamsJobWithRetry<CreateInvoiceFiscalReceiptJobParamsDc>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры перезапуска задачи</returns>
        protected override RetryJobParamsDto ExecuteAndDetermineRetryNeed(CreateInvoiceFiscalReceiptJobParamsDc jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                var invoice = DbLayer.InvoiceRepository.GetInvoiceById(jobParams.InvoiceId)
                              ?? throw new NotFoundException($"Не найден счет на оплату по id {jobParams.InvoiceId}");

                paymentReceiptProvider.CreateFiscalReceiptByInvoice(invoice);

                return CreateRetryParams(taskQueueId, false);
            }

            catch (Exception)
            {
                return CreateRetryParams(taskQueueId, true);
            }
        }
    }
}
