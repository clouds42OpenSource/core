﻿using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.DataContracts.CoreWorkerTasks.Results;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Восстановление информационной базы после не успешной попытки АО.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RestoreAccountDatabaseAfterFailedAutoUpdateJob)]
    public class RestoreAccountDatabaseAfterFailedAutoUpdateJob(
        IUnitOfWork dbLayer,
        IRestoreAccountDatabaseBackupManager restoreAccountDatabaseBackupManager)
        : CoreWorkerResultsJob<RestoreAccountDatabaseFromTombWorkerTaskParam, RestoreAccountDatabaseFromTombResultDto>(
            dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override RestoreAccountDatabaseFromTombResultDto ExecuteResult(RestoreAccountDatabaseFromTombWorkerTaskParam jobParams,
            Guid taskId, Guid taskQueueId)
        {
            var result = restoreAccountDatabaseBackupManager.RestoreAfterFailedAutoUpdate(jobParams);
            return new RestoreAccountDatabaseFromTombResultDto
            {
                Success = !result.Error,
                Message = result.Message
            };
        }
    }
}
