﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Configurations;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Logger;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.HandleRequestToChangeAccDbTypeJob)]
    public class HandleRequestToChangeAccDbTypeJob(
        IUnitOfWork unitOfWork,
        IAccountDatabaseManager accountDatabaseManager,
        ILogger42 logger,
        AccountDatabasePathHelper accountDatabasePathHelper,
        IAccessProvider accessProvider)
        : CoreWorkerParamsJob<RequestToChangeAccDbTypeDto>(unitOfWork)
    {
        readonly IUnitOfWork _unitOfWork = unitOfWork;
        readonly CloudServicesEnterpriseServerProvider _cloudServicesEnterpriseServerProvider = new(unitOfWork);

        protected override void Execute(RequestToChangeAccDbTypeDto jobParams, Guid taskId, Guid taskQueueId)
        {
            logger.Info($"Начинаю обрабатывать зявку на смену режима базы {jobParams.AccountDatabseId}");
            var acDbSupport = _unitOfWork.AcDbSupportRepository.FirstOrDefault(acdb => acdb.AccountDatabasesID == jobParams.AccountDatabseId)
                ?? throw new InvalidOperationException($"База по айди {jobParams.AccountDatabseId} не найдена");

            var result = accountDatabaseManager.TryAdminAuthorizeToAccountDatabase(acDbSupport.AccountDatabasesID);
            logger.Info($"Авторизация в базу {jobParams.AccountDatabseId} прошла {!result.Error}");

            var entServer = _cloudServicesEnterpriseServerProvider.GetEnterpriseServerData(acDbSupport.AccountDatabase);
            string databasePath, type;

            if (acDbSupport.AccountDatabase.IsFile != null && !acDbSupport.AccountDatabase.IsFile.Value)
            {
                type = "файловый";
                databasePath = $"{Path.Combine(entServer.ConnectionAddress, acDbSupport.AccountDatabase.V82Name)}";
            }
            else
            {
                type = "серверный";
                databasePath = accountDatabasePathHelper.GetPath(acDbSupport.AccountDatabase);
            }

            StringBuilder sb = new StringBuilder()
                .Append($"От аккаунта {acDbSupport.AccountDatabase.Account.IndexNumber} поступил запрос на перевод базы в {type} режим\n")
                .Append($"Телефон для связи {jobParams.AdminPhoneNumber}\n")
                .Append($"Логин: {jobParams.DatabaseAdminLogin},  Пароль: {jobParams.DatabaseAdminPassword}\n")
                .Append($"Авторизация в базе: {!result.Error}\n")
                .Append($"Номер базы {acDbSupport.AccountDatabase.V82Name}\n")
                .Append($"Дата проведения {jobParams.ChangeTypeDate}\n")
                .Append($"Путь к базе {databasePath} \n");

            var hotelinemail = ConfigurationHelper.GetConfigurationValue("HotlineEmail");

            logger.Trace($"Отправляем письмо Запрос на перевод базы в {type} на {hotelinemail}");

            new Manager42CloudsMail()
             .DisplayName("Команда 42Clouds")
             .To(hotelinemail)
             .Subject($"Запрос на перевод базы в {type}")
             .Body(sb.ToString())
             .SendViaNewThread();

            LogEventHelper.LogEvent(_unitOfWork, acDbSupport.AccountDatabase.AccountId, accessProvider, LogActions.EditInfoBase,
                    $"Задача на обработку заявки смены режима в {type} для базы {acDbSupport.AccountDatabase.V82Name} отправлена на хотлайн");
        }
    }
}
