﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Cluster1CProviders;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.CheckCluster1CActiveSessions;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs.Cluster1CJobs
{
    /// <summary>
    /// Джоба по удалению базы 1С с кластера.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DropDatabaseFromClusterJob)]
    public class DropDatabaseFromClusterJob(
        ICluster1CProvider cluster1CProvider,
        IAccountDatabaseModelCreator accountDatabaseModelCreator,
        IUnitOfWork dbLayer)
        :
            CoreWorkerParamsJob<CheckCluster1CActiveSessionsJobParams>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу по удалению базы 1С с кластера.
        /// </summary>
        protected override void Execute(CheckCluster1CActiveSessionsJobParams jobParams,
            Guid taskId,
            Guid taskQueueId)
        {
            var accountDatabaseModel = accountDatabaseModelCreator.CreateModel(jobParams.AccountDatabaseId);
            cluster1CProvider.DropDatabase(accountDatabaseModel);
        }
    }
}