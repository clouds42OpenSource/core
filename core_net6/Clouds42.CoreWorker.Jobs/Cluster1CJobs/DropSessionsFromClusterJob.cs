﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Cluster1CProviders;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.CheckCluster1CActiveSessions;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs.Cluster1CJobs
{
    /// <summary>
    /// Джоба по отключению активных сессий в базе 1С на кластере.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DropSessionsFromClusterJob)]
    public class DropSessionsFromClusterJob(
        ICluster1CProvider cluster1CProvider,
        IAccountDatabaseModelCreator accountDatabaseModelCreator,
        IUnitOfWork dbLayer)
        :
            CoreWorkerParamsJob<CheckCluster1CActiveSessionsJobParams>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу по отключению активных сессий в базе 1С на кластере.
        /// </summary>
        protected override void Execute(CheckCluster1CActiveSessionsJobParams jobParams,
            Guid taskId,
            Guid taskQueueId)
        {
            var accountDatabaseModel = accountDatabaseModelCreator.CreateModel(jobParams.AccountDatabaseId);
            cluster1CProvider.DropSessions(accountDatabaseModel);
        }
    }
}