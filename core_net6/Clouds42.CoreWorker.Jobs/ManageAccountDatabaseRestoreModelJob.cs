﻿using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Задача для управления моделью восстановления для инф. баз
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ManageAccountDatabaseRestoreModelJob)]
    public class ManageAccountDatabaseRestoreModelJob(
        IManageAccountDatabaseRestoreModelProvider manageAccountDatabaseRestoreModelProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            manageAccountDatabaseRestoreModelProvider.ManageAccountDatabasesRestoreModel();
        }
    }
}
