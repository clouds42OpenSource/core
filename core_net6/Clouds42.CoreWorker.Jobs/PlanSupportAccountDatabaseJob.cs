﻿
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Задачи планирования проведения ТиИ/АО информационных баз.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.PlanSupportAccountDatabaseJob)]
    public class PlanSupportAccountDatabaseJob(IPlanSupportAccountDatabaseProvider planSupportAccountDatabaseProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачи
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="taskQueueId"></param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            planSupportAccountDatabaseProvider.RunPlaning();
        }
    }
}
