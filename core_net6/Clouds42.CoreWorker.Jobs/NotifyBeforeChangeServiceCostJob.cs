﻿using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.NotifyBeforeChangeServiceCost;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Джоба по уведомлению аккаунтов
    /// о скором изменении стоимости сервиса
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.NotifyBeforeChangeServiceCostJob)]
    public class NotifyBeforeChangeServiceCostJob(
        IUnitOfWork dbLayer,
        INotifyBeforeChangeServiceCostProvider notifyBeforeChangeServiceCostProvider)
        : CoreWorkerParamsJob<NotifyBeforeChangeServiceCostJobParams>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(NotifyBeforeChangeServiceCostJobParams jobParams, Guid taskId, Guid taskQueueId)
        {
            Logger.Info(
                $"[{taskQueueId}] :: Уведомление аккаунтов о скором изменении стоимости сервиса, по изменениям {jobParams.BillingServiceChangesId}");

            notifyBeforeChangeServiceCostProvider.NotifyAccounts(jobParams.BillingServiceChangesId,
                jobParams.AccountIds, jobParams.ServiceCostChangeDate);
        }
    }
}
