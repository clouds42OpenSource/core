﻿using Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs.AccountDatabase
{
    /// <summary>
    /// Задача раньше была по автообновлению, сейчас занимается сбором и обработкой отчётов обновлятора
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.AccountDatabaseAutoUpdateJob)]
    public class AccountDatabaseAutoUpdateJob(IAccountDatabaseAutoUpdateProvider accountDatabaseAutoUpdateManager)
        : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            accountDatabaseAutoUpdateManager.HandleSupportResults();
        }

    }
}
