﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase;
using Clouds42.Domain.Constants;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs.AccountDatabase
{
    /// <summary>
    ///     Джоба предоставления веб прав к базе на IIS
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.AddWebAccessToDatabaseJob)]
    public class AddWebAccessToDatabaseJob(
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IPublishDbProvider publishDatabaseManager,
        IServiceProvider serviceProvider)
        : CoreWorkerParamsJob<WebAccessToDatabaseJobParams>(unitOfWork)
    {
        /// <summary>
        /// Выполнить задачу по предоставления веб прав к базе
        /// </summary>
        protected override void Execute(WebAccessToDatabaseJobParams jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                publishDatabaseManager.AddWebAccessToDatabase(serviceProvider, jobParams.AccountDatabaseId, jobParams.AccountUserId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при предоставления веб прав к базе] {jobParams.AccountDatabaseId}.");
                throw;
            }
        }
    }
}
