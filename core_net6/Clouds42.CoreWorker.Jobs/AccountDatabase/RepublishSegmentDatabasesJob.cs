﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs.AccountDatabase
{
    /// <inheritdoc />
    /// <summary>
    ///     Джоба переопубликации баз сегмента на IIS
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RepublishSegmentDatabasesJob)]
    public class RepublishSegmentDatabasesJob(
        IUnitOfWork unitOfWork,
        IRepublishSegmentDatabasesProvider republishSegmentDatabasesProvider)
        : CoreWorkerParamsJob<RepublishSegmentDatabasesJobParamsDto>(unitOfWork)
    {
        /// <inheritdoc />
        /// <summary>
        /// Выполнить задачу по переопубликации баз
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(RepublishSegmentDatabasesJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            republishSegmentDatabasesProvider.RepublishSegmentDatabases(jobParams.SegmentId, jobParams.PlatformType, jobParams.DistributionType);
        }
    }
}
