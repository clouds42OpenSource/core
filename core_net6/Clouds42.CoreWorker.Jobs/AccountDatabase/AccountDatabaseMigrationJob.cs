﻿using Clouds42.AccountDatabase.Contracts.Migration.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorkerTask.Contracts.Models;
using Clouds42.CoreWorkerTask.Helpers;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs.AccountDatabase
{
    /// <inheritdoc />
    /// <summary>
    /// Джоба миграции информационных баз
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.AccountDatabaseMigrationJob)]
    public class AccountDatabaseMigrationJob(
        IUnitOfWork dbLayer,
        IAccountDatabaseMigrationManager accountDatabaseMigrationManager)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить джобу
        /// </summary>
        /// <param name="taskId">Id таски</param>
        /// <param name="taskQueueId">Id очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var migrationParams = GetTaskParameters(taskQueueId);
            accountDatabaseMigrationManager.MigrateAccountDatabases(migrationParams.AccountDatabaseIds,
                migrationParams.ToFileStorageId, migrationParams.InitiatorId);
        }

        /// <summary>
        /// Получить параметры задачи
        /// </summary>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры задачи</returns>
        private DatabasesMigrationParams GetTaskParameters(Guid taskQueueId)
        {
            var task = dbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.Id == taskQueueId);

            if (task.TaskParameter == null || string.IsNullOrEmpty(task.TaskParameter.TaskParams))
                throw new InvalidOperationException($"У задачи : {taskQueueId} отсутвуют входящие параметры");

            var ser = new CoreWorkerTaskParamSerializer<DatabasesMigrationParams>();
            return ser.DeSerialize(task.TaskParameter.TaskParams);
        }
    }
}
