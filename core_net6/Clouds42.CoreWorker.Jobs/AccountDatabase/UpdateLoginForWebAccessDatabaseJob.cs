﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs.AccountDatabase
{
    /// <summary>
    /// Джоба обновления логина
    /// веб доступа базы
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.UpdateLoginForWebAccessDatabaseJob)]
    public class UpdateLoginForWebAccessDatabaseJob(
        IUnitOfWork dbLayer,
        IPublishDbProvider publishDatabaseManager,
        IServiceProvider serviceProvider)
        : CoreWorkerParamsJob<UpdateLoginForWebAccessDatabaseDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(UpdateLoginForWebAccessDatabaseDto jobParams, Guid taskId, Guid taskQueueId)
        {
            publishDatabaseManager.UpdateLoginForWebAccessDatabase(serviceProvider, jobParams);
        }
    }
}
