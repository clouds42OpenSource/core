﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase;
using Clouds42.Domain.Constants;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs.AccountDatabase
{
    /// <summary>
    ///     Джоба удалению веб прав к базе на IIS
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RemoveWebAccessToDatabaseJob)]
    public class RemoveWebAccessToDatabaseJob(
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IPublishDbProvider publishDatabaseManager,
        IServiceProvider serviceProvider)
        : CoreWorkerParamsJob<WebAccessToDatabaseJobParams>(unitOfWork)
    {
        /// <summary>
        /// Выполнить задачу по удалению веб прав к базе
        /// </summary>
        protected override void Execute(WebAccessToDatabaseJobParams jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                publishDatabaseManager.RemoveWebAccessToDatabase(serviceProvider, jobParams.AccountDatabaseId, jobParams.AccountUserId);
            }
            catch (Exception ex)
            {
               handlerException.Handle(ex,$"[Ошибка при удалении веб прав к базе] {jobParams.AccountDatabaseId}");
                throw;
            }
        }
    }
}
