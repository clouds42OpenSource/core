﻿using Clouds42.Configurations;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.HandlerExeption.Contract;
using System.Text;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Common.Helpers;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.LetterNotification.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs.AccountDatabase
{
    /// <summary>
    ///     Проверка всех баз со статусом Ready
    /// на наличие файла базы 1C в хранилище
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.Check1CDatabaseFileForExistenceJob)]
    public class Check1CDatabaseFileForExistenceJob(
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IMessagesManager messagesManager,
        IAccountDatabasePathHelper databasePathHelper)
        : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var databasesForChecking = dbLayer.DatabasesRepository
                .Where(item => item.State == DatabaseState.Ready.ToString()
                && item.IsFile.HasValue
                && item.IsFile.Value);

            Logger.Info("Начинаем проверку баз на наличие файла конфигурации (1CD, MD)");

            var errorsMessages = new StringBuilder();

            foreach (var database in databasesForChecking)
            {
                try
                {
                    var pathToDb = databasePathHelper.GetPath(database);

                    if (string.IsNullOrEmpty(pathToDb))
                    {
                        var errorMessage = $"Не смогли получить путь для базы {database.Id}";
                        Logger.Warn(errorMessage);
                        errorsMessages.AppendLine($"<p>{errorMessage}</p>");
                        continue;
                    }

                    if (!DirectoryHelper.DirectoryPingandHas1CDatabaseFile(pathToDb))
                    {
                        var errorMessage = $"Файл базы отсутствует по пути {pathToDb}.";
                        Logger.Warn(errorMessage);
                        errorsMessages.AppendLine($"<p>{errorMessage}</p>");
                    }
                }
                catch (Exception ex)
                {
                    var errorMessage = $"Ошибка при проверке базы {database.Id} на наличие файла конфигурации. Описание ошибки: {ex.Message}";
                    handlerException.Handle(ex, $"[Ошибка при проверке базы на наличие файла конфигурации ]{database.Id}");
                    errorsMessages.AppendLine($"<p>{errorMessage}</p>");
                }
            }

            var messageToSend = errorsMessages.ToString();

            if (!string.IsNullOrWhiteSpace(messageToSend))
            {
                var mailTopic = "Ядро сбой. Базы у которых отсутствует файл конфигурации (1CD, MD)";

                var cloudServicesMail = ConfigurationHelper.GetConfigurationValue<string>("cloud-services");

                messagesManager.SendWorkerReport(
                    messageToSend,
                    mailTopic,
                    cloudServicesMail);
            }

            Logger.Info("Проверка баз на наличие файла конфигурации (1CD, MD) завершена");

        }
    }
}
