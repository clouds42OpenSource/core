﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.RecalculateServiceCostAfterChanges;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.ResourcesContext.Commands;
using MediatR;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Джоба по пересчету стоимости сервиса после изменений
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RecalculateServiceCostAfterChangesJob)]
    public class RecalculateServiceCostAfterChangesJob(IUnitOfWork dbLayer, ISender sender)
        : CoreWorkerParamsJob<RecalculateServiceCostAfterChangesJobParams>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(RecalculateServiceCostAfterChangesJobParams jobParams, Guid taskId, Guid taskQueueId)
        {
            Logger.Info($"[{taskQueueId}] :: Пересчет стоимости сервиса после изменений {jobParams.BillingServiceChangesId}");

            sender.Send(new RecalculateResourcesCostForAccountsCommand { AccountsIds = jobParams.AccountIds, BillingServiceChangesId = jobParams.BillingServiceChangesId }).Wait();
        }
    }
}
