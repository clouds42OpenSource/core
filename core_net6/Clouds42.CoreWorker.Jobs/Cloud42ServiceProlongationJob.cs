﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Джоба пролонгации сервисов
    /// </summary>

    [CoreWorkerJobName(CoreWorkerTasksCatalog.Regular42CloudServiceProlongationJob)]
    public class Regular42CloudServiceProlongationJob(
        IBillingManager billingManager)
        : CoreWorkerJob
    {

        /// <summary>
        /// Выполнить джобу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            billingManager.ProcessExpiredPromisePayments();
            billingManager.ProlongLockedServices();
            billingManager.ProlongOrLockExpiredServices();
            billingManager.UnlockSponsoredServices();
            billingManager.NotifyBeforeLockServices();
            billingManager.NotifyServiceDemoPeriodIsComingToEnd();
            billingManager.NotifyBeforeLockServicesAutoPay();
            billingManager.LockExpiredDemoServices();
            billingManager.ProlongOrLockHybridExpiredServices();
        }
        
    }
}
