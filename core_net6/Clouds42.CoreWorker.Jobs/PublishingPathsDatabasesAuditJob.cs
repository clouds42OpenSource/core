﻿using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Джоба аудита путей публикации информационных баз
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.PublishingPathsDatabasesAuditJob)]
    public class PublishingPathsDatabasesAuditJob(IPublishingPathsDatabasesAudit auditor) : CoreWorkerJob
    {
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            auditor.ProcessAudit();
        }
    }
}
