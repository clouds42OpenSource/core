﻿using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs
{
    /// <summary>
    /// Удаление в склеп информационной базы
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DeleteAccountDatabaseToTombJob)]
    public class DeleteAccountDatabaseToTombJob(
        IUnitOfWork dbLayer,
        IDeleteAccountDatabaseToTombProvider deleteAccountDatabaseToTombManager)
        : CoreWorkerParamsJob<DeleteAccountDatabaseToTombJobParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(DeleteAccountDatabaseToTombJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                deleteAccountDatabaseToTombManager.DeleteAccountDatabaseToTomb(jobParams);
            }
            catch (Exception ex)
            {
                Logger.Info($"[{taskQueueId}] :: Ошибка отправки в склеп базы {jobParams.AccountDatabaseId}. {ex.GetFullInfo()}");
            }
        }
    }
}