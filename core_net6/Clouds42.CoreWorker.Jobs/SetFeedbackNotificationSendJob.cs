﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.NotificationContext.Commands;
using MediatR;

namespace Clouds42.CoreWorker.Jobs;


/// <inheritdoc />
/// <summary>
/// Задача, которая отправляет сообщение об отзыве
/// </summary>
///
[CoreWorkerJobName(CoreWorkerTasksCatalog.SetFeedbackNotificationSendJob)]
public class SetFeedbackNotificationSendJob(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    ISender sender,
    IHandlerException handlerException,
    IAccessProvider accessProvider)
    : CoreWorkerJob
{


    public override void Execute(Guid taskId, Guid taskQueueId)
    {
        try 
        {
            logger.Info($"Начало работы SetFeedbackNotificationSendJob с айди {taskId} в очереди {taskQueueId}");
            var dateTime = DateTime.Now;
            var oneDayAgo = dateTime.AddDays(-1);
            var sixMonthsAgo = dateTime.AddMonths(-6);

            var accountUserData = unitOfWork.AccountsRepository
                .Where(a => 
                    a.Payments.Any(p => p.Date >= oneDayAgo && p.Date <= dateTime && p.BillingService.SystemService == Clouds42Service.MyEnterprise)
                )
                .Select(a => new 
                {
                    AccountId = a.Id,
                    AccountUsers = a.AccountUsers.Where(au => au.CreationDate <= sixMonthsAgo).ToList()
                })
                .ToList();
            
    
            var userIds = accountUserData
                .Select(aud => 
                {

                    logger.Info($"Для аккаунта {aud.AccountId} все пользователи до фильтрации: {string.Join(", ", aud.AccountUsers.Select(u => u.Id))}");

                    var filteredUsers = aud.AccountUsers
                        .Where(au => !unitOfWork.FeedbackRepository.Any(f => 
                            f.AccountUserId == au.Id && (f.FeedbackLeftDate != null ||
                                                         (f.Delay.HasValue && f.Delay >= dateTime))))
                        .ToList();

                    logger.Info($"Для аккаунта {aud.AccountId} пользователи после фильтрации без обратной связи: {string.Join(", ", filteredUsers.Select(u => u.Id))}");
                    
                    var selectedUsers = filteredUsers
                        .Take((int)Math.Ceiling(filteredUsers.Count * 0.1))
                        .ToList();

                    logger.Info($"Для аккаунта {aud.AccountId} выбрано {selectedUsers.Count} пользователей из {filteredUsers.Count} подходящих. " +
                                $"Идентификаторы: {string.Join(", ", selectedUsers.Select(u => u.Id))}");

                    return selectedUsers.Select(u => u.Id).ToList();
                })
                .SelectMany(x => x)  
                .ToList();

            sender.Send(new CreateNotificationCommand
            {
                State = NotificationState.Feedback,
                Message = "Уважаемый клиент, спасибо, что пользуетесь продуктами 42Clouds. Мы действительно рады, что вы остаетесь с нами.\nНаша команда стремится развивать продукты, поэтому нам важно получить обратную связь от наших клиентов.\nПоделитесь своим мнением, это займет всего несколько минут.\n\nОцените, пожалуйста, продукты 42Clouds.",
                UserIds = userIds
            }).Wait();

            foreach (var data in accountUserData)
            {
                foreach (var user in data.AccountUsers.Where(user => userIds.Contains(user.Id)))
                {
                    LogEventHelper.LogEvent (unitOfWork, data.AccountId, accessProvider, 
                        LogActions.FeedbackNotificationSent, 
                        $"Отправлена нотификация с просьбой оставить обратную связь для пользователя {user.Id}");
                }
            }
            logger.Info($"SetFeedbackNotificationSendJob с айди {taskId} в очереди {taskQueueId} успешно отработала. " +
                        $"Айди пользователей: {string.Join(", ", userIds)}");
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex, $"Ошибка в SetFeedbackNotificationSendJob с айди {taskId}: {ex.Message}");
        }
    }
}
