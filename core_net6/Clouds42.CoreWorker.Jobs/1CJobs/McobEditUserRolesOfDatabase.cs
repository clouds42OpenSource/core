﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Constants;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Jobs._1CJobs
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.McobEditUserRolesOfDatabase)]
    public class McobEditUserRolesOfDatabase(
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAccountDatabaseManager accountDatabaseManager,
        AccountDatabasePathHelper accountDatabasePathHelper,
        ILogger42 logger)
        : CoreWorkerJob
    {
        private RunJobOfMsobModelDto _taskParams;


        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            try
            {
                logger.Info("Редактирование прав МЦОБ начато");

                var task = dbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.Id == taskQueueId);
                if (task == null)
                    throw new InvalidOperationException($"Не удалось получить задачу по номеру : {taskQueueId}");

                if (task.TaskParameter == null || string.IsNullOrEmpty(task.TaskParameter.TaskParams))
                    throw new InvalidOperationException($"У задачи : {taskQueueId} отсутвуют входящие параметры");

                _taskParams = task.TaskParameter.TaskParams.DeserializeFromJson<RunJobOfMsobModelDto>();

                var dataBase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.V82Name == _taskParams.DbName);
                if (dataBase == null)
                    throw new NotFoundException($"не удалось получить базу по имени : {_taskParams.DbName}");

                accountDatabaseManager.Run1CEpfMcob(_taskParams.DbName, dataBase.IsFile ?? false,
                    accountDatabasePathHelper.GetPath(dataBase), _taskParams.Parametrs, dataBase.PlatformType);

                logger.Info("Редактирование прав МЦОБ завершенно");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования прав МЦОБ] для базы {_taskParams.DbName} и параметрами {_taskParams.Parametrs}" );
            }
        }
    }
}
