﻿using System;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BaseJobs
{
    /// <summary>
    /// Задача с входящими параметрами, с возможностью перезапуска
    /// </summary>
    /// <typeparam name="TParams">Тип модели входящих параметров</typeparam>
    public abstract class CoreWorkerParamsJobWithRetry<TParams>(IUnitOfWork dbLayer) : CoreWorkerJobWithRetry(dbLayer)
        where TParams : class
    {
        /// <summary>
        /// Выполнить задачу.
        /// </summary>
        /// <param name="taskId">Номер задачи.</param>
        /// <param name="taskQueueId">Номер задачи пула.</param>        
        protected override RetryJobParamsDto ExecuteAndDetermineRetryNeed(Guid taskId, Guid taskQueueId)
        {
            var taskQueueItem = GetCoreWorkerTasksQueue(taskQueueId);

            if (taskQueueItem.TaskParameter == null || string.IsNullOrEmpty(taskQueueItem.TaskParameter.TaskParams))
                throw new InvalidOperationException($"У задачи {taskQueueId} не определены входящие параметры");

            var taskParams = taskQueueItem.TaskParameter.TaskParams.DeserializeFromJson<TParams>();
            return ExecuteAndDetermineRetryNeed(taskParams, taskId, taskQueueId);
        }

        /// <summary>
        /// Выполнить задачу с входящими параметрами и определить необходимость перезапуска.
        /// </summary>
        /// <param name="jobParams">Параметры задачи.</param>
        /// <param name="taskId">Номер задачи.</param>
        /// <param name="taskQueueId">Номер задачи пула.</param>
        /// <returns>Параметры перезапуска задачи.</returns>
        protected abstract RetryJobParamsDto ExecuteAndDetermineRetryNeed(TParams jobParams, Guid taskId,
            Guid taskQueueId);
    }
}
