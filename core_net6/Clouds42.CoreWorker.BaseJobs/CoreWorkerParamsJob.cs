﻿using System;
using Clouds42.Common.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BaseJobs
{

    /// <summary>
    /// Задача с входящими параметрами.
    /// </summary>
    /// <typeparam name="TParams">Тип модели входящих параметров.</typeparam>    
    public abstract class CoreWorkerParamsJob<TParams>(IUnitOfWork dbLayer) : CoreWorkerJob
        where TParams : class
    {

        protected readonly IUnitOfWork DbLayer = dbLayer;

        /// <summary>
        /// Выполнить задачу.
        /// </summary>
        /// <param name="taskId">Номер задачи.</param>
        /// <param name="taskQueueId">Номер задачи пула.</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var taskQueueItem = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.Id == taskQueueId);

            if (taskQueueItem.TaskParameter == null || string.IsNullOrEmpty(taskQueueItem.TaskParameter.TaskParams))
                throw new InvalidOperationException($"У задачи {taskQueueId} не определены входящие параметры");

            var taskParams = taskQueueItem.TaskParameter.TaskParams.DeserializeFromJson<TParams>();

            Execute(taskParams, taskId, taskQueueId);            
        }

        /// <summary>
        /// Выполнить задачу с входящими параметрами.
        /// </summary>
        /// <param name="jobParams">Параметры задачи.</param>
        /// <param name="taskId">Номер задачи.</param>
        /// <param name="taskQueueId">Номер задачи пула.</param>        
        protected abstract void Execute(TParams jobParams, Guid taskId, Guid taskQueueId);
    }
}
