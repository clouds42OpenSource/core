﻿using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using System;

namespace Clouds42.CoreWorker.BaseJobs
{    
    /// <summary>
    /// Базовый класс задачи воркера
    /// </summary>
    public abstract class CoreWorkerJob
    {        
        protected readonly ILogger42 Logger = Logger42.GetLogger();
        protected readonly IHandlerException HandlerException = HandlerException42.GetHandler();
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public abstract void Execute(Guid taskId, Guid taskQueueId);
        

        /// <summary>
        /// Признак необходимости перезапускать задачу
        /// </summary>
        public bool NeedRetry { get; set; }

        /// <summary>
        /// Задержка перед повторным перезапуском задачи
        /// </summary>
        public int RetryDelayInSeconds { get; set; } = 30;
    }
}
