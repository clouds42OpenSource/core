﻿using System;
using Clouds42.Common.Extensions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BaseJobs
{

    /// <summary>
    /// Задача с входящими параметрами и результатом выполнения
    /// </summary>
    /// <typeparam name="TParams">Тип модели входящих параметров.</typeparam>
    /// <typeparam name="TResult">Тип результирующей модели.</typeparam>
    public abstract class CoreWorkerResultsJob<TParams, TResult>(IUnitOfWork dbLayer)
        : CoreWorkerParamsJob<TParams>(dbLayer)
        where TParams : class
        where TResult : class
    {
        /// <summary>
        /// Выполнить задачу с входящими параметрами.
        /// </summary>
        /// <param name="jobParams">Параметры задачи.</param>
        /// <param name="taskId">Номер задачи.</param>
        /// <param name="taskQueueId">Номер задачи пула.</param>        
        protected override void Execute(TParams jobParams, Guid taskId, Guid taskQueueId)
        {
            var taskQueueItem = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.Id == taskQueueId);

            if (taskQueueItem.TaskParameter == null || string.IsNullOrEmpty(taskQueueItem.TaskParameter.TaskParams))
                throw new InvalidOperationException($"У задачи {taskQueueId} не определены входящие параметры");

            var taskParams = taskQueueItem.TaskParameter.TaskParams.DeserializeFromJson<TParams>();

            var result = ExecuteResult(taskParams, taskId, taskQueueId);

            var jsonResult = TryConvertResultToJson(result);

            taskQueueItem.ExecutionResult = jsonResult;
            DbLayer.CoreWorkerTasksQueueRepository.Update(taskQueueItem);
            DbLayer.Save();
        }

        /// <summary>
        /// Попытка сконвертировать результат работы задачи в JSON
        /// </summary>
        /// <param name="result">Конвертируемая модель.</param>
        /// <returns>Строковое представление в формате JSON.</returns>
        private string TryConvertResultToJson(TResult result)
        {
            try
            {
                return result.ToJson();
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, "[Ошибка преобразования объект в JSON]");
                throw;
            }
        }

        /// <summary>
        /// Выполнить задачу с входящими параметрами и получить результат выполнения.
        /// </summary>
        /// <param name="jobParams">Параметры задачи.</param>
        /// <param name="taskId">Номер задачи.</param>
        /// <param name="taskQueueId">Номер задачи пула.</param>
        /// <returns>Результат выполнения задачи.</returns>
        protected abstract TResult ExecuteResult(TParams jobParams, Guid taskId, Guid taskQueueId);

    }
}
