﻿using System;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BaseJobs
{
    /// <summary>
    /// Задача с возможностью перезапуска
    /// </summary>
    public abstract class CoreWorkerJobWithRetry(IUnitOfWork dbLayer) : CoreWorkerJob
    {
        protected readonly IUnitOfWork DbLayer = dbLayer;

        /// <summary>
        /// Выполнить задачу.
        /// </summary>
        /// <param name="taskId">Номер задачи.</param>
        /// <param name="taskQueueId">Номер задачи пула.</param>        
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var taskQueueItem = GetCoreWorkerTasksQueue(taskQueueId);
            var result = ExecuteAndDetermineRetryNeed(taskId, taskQueueId);
            UpdateRetryTaskAttemptsCount(taskQueueItem);

            NeedRetry = result.NeedRetry;
            SetRetryDelayInSeconds(taskQueueItem, result.RetryDelayInSeconds);
        }

        /// <summary>
        /// Выполнить задачу с входящими параметрами и определить необходимость перезапуска.
        /// </summary>
        /// <param name="taskId">Номер задачи.</param>
        /// <param name="taskQueueId">Номер задачи пула.</param>
        /// <returns>Параметры перезапуска задачи.</returns>
        protected abstract RetryJobParamsDto ExecuteAndDetermineRetryNeed(Guid taskId, Guid taskQueueId);

        /// <summary>
        /// Создать параметры перезапуска
        /// </summary>
        /// <param name="taskQueueId">Id запущенной задачи</param>
        /// <param name="needRetry">Признак необходимости перезапуска</param>
        /// <param name="retryDelayInSeconds">Задержка перезапуска</param>
        /// <param name="maxRetryAttemptsCount">Максимальное количество попыток перезапуска задачи
        /// (Если параметр не указан, то берется максимальное число попыток)</param>
        /// <returns>Параметры перезапуска</returns>
        protected RetryJobParamsDto CreateRetryParams(Guid taskQueueId, bool needRetry, int? retryDelayInSeconds = null,
            int maxRetryAttemptsCount = int.MaxValue)
        {
            var taskQueueItem = GetCoreWorkerTasksQueue(taskQueueId);

            return new RetryJobParamsDto
            {
                NeedRetry = needRetry && (!taskQueueItem.RetryTaskAttemptsCount.HasValue
                                          || taskQueueItem.RetryTaskAttemptsCount.Value < maxRetryAttemptsCount),

                RetryDelayInSeconds = retryDelayInSeconds ?? 30
            };
        }

        /// <summary>
        /// Получить запущенную задачу
        /// </summary>
        /// <param name="taskQueueId">Id запущенной задачи</param>
        /// <returns>Задача</returns>
        protected CoreWorkerTasksQueue GetCoreWorkerTasksQueue(Guid taskQueueId) =>
            DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.Id == taskQueueId) ??
            throw new NotFoundException($"Не удалось получить задачу по Id '{taskQueueId}'");

        /// <summary>
        /// Обновить количество попыток перезапуска задачи
        /// </summary>
        /// <param name="coreWorkerTasksQueue">Запущенная задача</param>
        private void UpdateRetryTaskAttemptsCount(CoreWorkerTasksQueue coreWorkerTasksQueue)
        {
            coreWorkerTasksQueue.RetryTaskAttemptsCount = (coreWorkerTasksQueue.RetryTaskAttemptsCount ?? 0) + 1;
            DbLayer.CoreWorkerTasksQueueRepository.Update(coreWorkerTasksQueue);
            DbLayer.Save();
        }

        /// <summary>
        /// Установить задержку повторного выполнения задачи
        /// </summary>
        /// <param name="coreWorkerTasksQueue">Запущенная задача</param>
        /// <param name="retryDelayInSeconds">Задержка выполнения</param>
        private void SetRetryDelayInSeconds(CoreWorkerTasksQueue coreWorkerTasksQueue, int retryDelayInSeconds)
        {
            if (coreWorkerTasksQueue.RetryTaskAttemptsCount > 5)
                retryDelayInSeconds = 300;

            RetryDelayInSeconds = retryDelayInSeconds;
        }
    }
}
