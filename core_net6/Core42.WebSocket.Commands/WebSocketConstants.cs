﻿namespace Core42.WebSocketCommands
{
    /// <summary>
    /// Константы для Web сокета
    /// </summary>
    public static class WebSocketConstants
    {
        /// <summary>
        /// Префикс токена с логином для внутрених систем
        /// </summary>
        public const string InternalSystemTokenWithLoginPrefix = "system_token_with_login_";
    }
}