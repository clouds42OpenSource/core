﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using System;

namespace Core42.WebSocketCommands.Implementation
{

    /// <summary>
    /// Базовая команда WebSocket.
    /// </summary>
    [Serializable]
    public abstract class WebSocketCommand
    {
    }

    /// <summary>
    /// Команда создания новой базы в облаке.
    /// </summary>
    [Serializable]
    public class CreateDatabaseCommand : WebSocketCommand
    {
        public AccountDatabasePropertiesDto AccountDatabase { get; set; }
    }

    /// <summary>
    /// Команда предоставления доступа к базе.
    /// </summary>
    [Serializable]
    public class GrandAccessDatabaseCommand : WebSocketCommand
    {
        public AccountDatabasePropertiesDto AccountDatabase { get; set; }
    }

    /// <summary>
    /// Команда обновления информационной базы в облаке.
    /// </summary>
    [Serializable]
    public class UpdateDatabaseCommand : WebSocketCommand
    {
        public AccountDatabasePropertiesDto AccountDatabase { get; set; }
    }

    /// <summary>
    /// Команда удаления базы с облака.
    /// </summary>
    [Serializable]
    public class DeleteDatabaseCommand : WebSocketCommand
    {
        public Guid AccountDatabaseId { get; set; }
    }

    /// <summary>
    /// Команда удаления доступа к базе у пользователя.
    /// </summary>
    [Serializable]
    public class DeleteAccessDatabaseCommand : WebSocketCommand
    {
        public Guid AccountDatabaseId { get; set; }
    }

}
