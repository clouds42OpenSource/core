SET SonarScanner="C:/sonar-scanner-net6/SonarScanner.MSBuild.dll"

cd core_net6

dotnet %SonarScanner% begin /k:"clouds42OpenSource_core_00bb3eb1-0ef7-4d3c-a366-5c56b80ddfba" /d:sonar.host.url="https://sonarqube-test.42clouds.com" /d:sonar.login="sqp_e7455fd739992e8fb43f750e12459cd55348e8ca"
nuget restore Core42.sln
dotnet build Core42.sln --no-incremental
dotnet %SonarScanner% end /d:sonar.login="sqp_e7455fd739992e8fb43f750e12459cd55348e8ca"