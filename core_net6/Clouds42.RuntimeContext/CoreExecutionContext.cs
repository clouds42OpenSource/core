﻿namespace Clouds42.RuntimeContext
{

    public interface ICoreExecutionContext
    {
    }

    public interface ICoreWorkerExecutionContext : ICoreExecutionContext
    {
        short? WorkerId { get; }
    }

}
