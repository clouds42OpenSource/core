﻿using System.Text;
using Clouds42.Common.Exceptions;
using Clouds42.DomainContext;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces.Common;
using Google.Api.Gax.Grpc;
using Google.Api.Gax.ResourceNames;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.SecretManager.V1;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Newtonsoft.Json;
using Repositories;

namespace GCP.Extensions.Configuration.SecretManager
{
    public class GcpKeyValueSecretOptions 
    {
        public GoogleCredential? GoogleCredential {get;set;}
        public SecretManagerServiceClient? SecretMangerClient { get; set; }
        public string? SecretNamePrefix { get; set; }
        public string? ProjectId { get; set; }
        public bool StripPrefixFromKey {get;set;} = true;
        public IUnitOfWork UnitOfWork { get; set; }
    }

    public class GcpKeyValueSecretsConfigurationProvider : JsonStreamConfigurationProvider
    {
        private readonly GcpKeyValueSecretOptions _options;
        private readonly CallSettings _callSettings = CallSettings.FromRetry(RetrySettings.FromConstantBackoff(10, TimeSpan.FromSeconds(5), _ => true));
        internal SecretManagerServiceClient BuildClient(ICredential credential)
        {
            switch (credential)
            {
                case null:
                    _options.ProjectId ??= Helpers.GetProjectId();
                    return SecretManagerServiceClient.Create();
                case ServiceAccountCredential sac:
                    _options.ProjectId ??= sac.ProjectId ?? Helpers.GetProjectId();
                    break;
                default:
                    _options.ProjectId ??= Helpers.GetProjectId();
                    break;
            }

            var builder = new SecretManagerServiceClientBuilder
            {
               GoogleCredential = _options.GoogleCredential
            };

            return builder.Build();
        }

        internal static string ReplacePathSeparator(string path, string oldSeparator = Helpers.DoubleUnderscore)
        {
            var result = path.Replace(oldSeparator, ConfigurationPath.KeyDelimiter);

            return result;
        }

        internal string RemovePrefix(string value)
        {
            if (string.IsNullOrEmpty(_options.SecretNamePrefix)) { return value; }

            if (!_options.StripPrefixFromKey) return value;
            var prefixLen = _options.SecretNamePrefix.Length;
            const string secretsName = "secrets/";
            var secretsIndex = value.IndexOf(secretsName, StringComparison.InvariantCultureIgnoreCase);
            var valueWithOutSecrets = value[(secretsIndex + secretsName.Length)..];
            const string slashSymbol = "/";
            var slashIndex = valueWithOutSecrets.IndexOf(slashSymbol, StringComparison.InvariantCultureIgnoreCase);

            var resultValue = valueWithOutSecrets[..slashIndex];

            return prefixLen < (resultValue?.Length ?? 0) ? resultValue[prefixLen..] : resultValue;
        }

        public GcpKeyValueSecretsConfigurationProvider(GcpKeyValueSecretsConfigurationSource source, GcpKeyValueSecretOptions options): base(source)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));

            if (_options.SecretMangerClient != null) return;

            _options.GoogleCredential ??= GoogleCredential.GetApplicationDefault();

            _options.SecretMangerClient = BuildClient(_options.GoogleCredential.UnderlyingCredential);
        }

        public override void Load()
        {
            try
            {
                var projectName = new ProjectName(_options.ProjectId);
                var prefix = string.IsNullOrEmpty(_options.SecretNamePrefix)
                    ? null
                    : _options.SecretNamePrefix.ToLower();
                var filter = (null == prefix) ? string.Empty : $"name:{prefix}";

                var request = new ListSecretsRequest { ParentAsProjectName = projectName, Filter = filter };
                var secret = _options.SecretMangerClient!.ListSecrets(request, _callSettings).FirstOrDefault() ?? 
                    throw new NotFoundException($"Не найдены секреты в облаке по префиксу(имени) стенда '{prefix}'");

                var version = _options.SecretMangerClient!
                    .ListSecretVersions(
                        new ListSecretVersionsRequest
                        {
                            Filter = Helpers.FilterVersionsEnabled, ParentAsSecretName = secret.SecretName
                        }, _callSettings)
                    .MaxBy(x => x.CreateTime) ?? throw new NotFoundException($"Не найдена последняя версия секрета по названию стенда '{prefix}'");
                
                var secretVersion = _options.SecretMangerClient.AccessSecretVersion(version.SecretVersionName);

                var byteArray = secretVersion!.Payload!.Data!.ToByteArray();
                if (!byteArray.Any()) { throw new InvalidOperationException($"Не найдены секреты по названию стенда '{prefix}'");}
                Stream stream = new MemoryStream(byteArray);

                base.Load(stream);
            }

            catch (Exception ex)
            {
                var directory = $"{Environment.CurrentDirectory}/Logs";
                if(!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                File.WriteAllText($"{directory}/{nameof(GcpKeyValueSecretsConfigurationProvider)}-{DateTime.Now:d}.txt",$"{ex.Message}\n{ex.StackTrace}");

                LoadFromDb();
            }
        }

        private void LoadFromDb()
        {
            var cloudConfiguration = _options.UnitOfWork.CloudConfigurationRepository
                .AsQueryableNoTracking()
                .Where(x => !string.IsNullOrEmpty(x.Value))
                .ToDictionary(x => x.ContextSupplierId.HasValue ? $"{x.Key}_{x.ContextSupplierId.Value}" : $"{x.Key}",
                    z => z.Value);

            var json = JsonConvert.SerializeObject(cloudConfiguration, Formatting.Indented);

            base.Load(new MemoryStream(Encoding.UTF8.GetBytes(json)));
        }
    }

    public class GcpKeyValueSecretsConfigurationSource(
        IUnitOfWork unitOfWork,
        string? secretNamePrefix = null,
        GoogleCredential? googleCredential = null,
        string? projectId = null)
        : JsonStreamConfigurationSource
    {
        private readonly GcpKeyValueSecretOptions _options = new()
        {
            GoogleCredential = googleCredential,
            SecretNamePrefix = secretNamePrefix,
            ProjectId = projectId,
            UnitOfWork = unitOfWork
        };

        public override IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return new GcpKeyValueSecretsConfigurationProvider(this, _options);
        }
    }

    public class DbKeyValueSecretsConfigurationSource(IUnitOfWork unitOfWork) : IConfigurationSource
    {
        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return new DbKeyValueSecretsConfigurationProvider(unitOfWork);
        }
    }

    public class DbKeyValueSecretsConfigurationProvider(IUnitOfWork unitOfWork) : ConfigurationProvider
    {
        public override void Load()
        {
            Data = unitOfWork.CloudConfigurationRepository.AsQueryableNoTracking()
                .Where(x => !string.IsNullOrEmpty(x.Value))
                .ToDictionary(x => x.ContextSupplierId.HasValue ? $"{x.Key}_{x.ContextSupplierId.Value}" : $"{x.Key}",
                    z => z.Value)!;
        }
    }

    public static class KeyValueConfigurationExtensions
    {
        public static IConfigurationBuilder AddGcpKeyValueSecrets (this IConfigurationBuilder builder, string? envName = null)
        {
            var appSettingsFileName =
                string.IsNullOrEmpty(envName) ? "appsettings.json" : $"appsettings.{envName}.json";

            var configurationBuilder = new ConfigurationBuilder()
                .AddJsonFile(appSettingsFileName);

            if (File.Exists(Path.Combine(AppContext.BaseDirectory, "stand.json")))
            {
                configurationBuilder.AddJsonFile("stand.json");
            }

            var configuration = configurationBuilder.Build();
            
            var standName = configuration.GetSection("Stand:Name")?.Value;
            var credentialsPath = configuration.GetSection("Stand:GoogleCloudCredentialsPath")?.Value;

            var optionsBuilder = new DbContextOptionsBuilder<Clouds42DbContext>();
            ;
            if (configuration.GetSection("ConnectionStrings:DatabaseType").Value == ProviderTypeConstants.MsSql)
            {
                optionsBuilder.UseSqlServer(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
            }

            else
            {
                optionsBuilder.UseNpgsql(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
            }

            IConfigurationSource source = new DbKeyValueSecretsConfigurationSource(new UnitOfWork(new Clouds42DbContext(optionsBuilder.Options)));

            if (!string.IsNullOrEmpty(standName) && !string.IsNullOrEmpty(credentialsPath))
            {
                source = new GcpKeyValueSecretsConfigurationSource(new UnitOfWork(new Clouds42DbContext(optionsBuilder.Options)), standName, GoogleCredential.FromFile(credentialsPath));
            }

            return builder.Add(source);
        }
    }
}
