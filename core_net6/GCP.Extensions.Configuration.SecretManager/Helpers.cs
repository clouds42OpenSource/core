﻿namespace GCP.Extensions.Configuration.SecretManager
{
    public static class Helpers
    {
        public const string FilterVersionsEnabled = "state:ENABLED";
        public const string DoubleUnderscore = "__";

        public static string? GetProjectId()
        {
            return (_ = Google.Api.Gax.Platform.Instance()?.ProjectId) ?? (_ = Environment.GetEnvironmentVariable("GOOGLE_CLOUD_PROJECT")) ?? (Environment.GetEnvironmentVariable("GCLOUD_PROJECT"));
        }
    }
}
