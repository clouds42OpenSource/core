﻿namespace NotificationTelegramBotApi.Models
{
    public class AnalyticCounters
    {
        public YandexMetrika yandexMetrika { get; set; }
        public GoogleAnalytics googleAnalytics { get; set; }
    }

    public class Attachment
    {
        public string id { get; set; }
        public string url { get; set; }
        public int fileSize { get; set; }
        public string fileName { get; set; }
        public Type type { get; set; }
    }

    public class Client
    {
        public int searchId { get; set; }
        public string login { get; set; }
        public string clientId { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public CustomData customData { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public FirstVisit firstVisit { get; set; }
        public LastVisit lastVisit { get; set; }
        public string ip { get; set; }
        public string useragent { get; set; }
        public List<string> target { get; set; }
        public bool isBlocked { get; set; }
        public int ban { get; set; }
        public string questionCategory { get; set; }
        public string referer { get; set; }
        public string searchKeyword { get; set; }
        public string flag { get; set; }
        public Utm utm { get; set; }
        public string roistatVisitId { get; set; }
        public int visitsCount { get; set; }
        public int status { get; set; }
        public Source source { get; set; }
        public string infoLink { get; set; }
        public List<string> attachedToOperators { get; set; }
        public string applicationLink { get; set; }
        public AnalyticCounters analyticCounters { get; set; }
        public ContextClient contextClient { get; set; }
    }

    public class Content
    {
        public string text { get; set; }
        public List<Attachment> attachments { get; set; }
        public Sticker sticker { get; set; }
        public List<KnowledgeBaseArticle> knowledgeBaseArticles { get; set; }
    }

    public class ContextClient
    {
    }

    public class CustomData
    {
        public string foo { get; set; }
        public string another { get; set; }
    }

    public class Data
    {
        public Site site { get; set; }
        public Client client { get; set; }
        public Message message { get; set; }
        public Page page { get; set; }
        public Operator @operator { get; set; }
        public string property1 { get; set; }
        public string property2 { get; set; }
    }

    public class FirstVisit
    {
        public string dateTime { get; set; }
        public string dateTimeUTC { get; set; }
        public Page page { get; set; }
    }

    public class GoogleAnalytics
    {
        public string clientId { get; set; }
        public string trackingId { get; set; }
    }

    public class KnowledgeBaseArticle
    {
        public string registerId { get; set; }
        public string articleId { get; set; }
        public string groupId { get; set; }
        public string descr { get; set; }
        public string url { get; set; }
    }

    public class LastVisit
    {
        public string dateTime { get; set; }
        public string dateTimeUTC { get; set; }
        public Page page { get; set; }
    }

    public class Message
    {
        public int id { get; set; }
        public string text { get; set; }
        public Content content { get; set; }
        public string @operator { get; set; }
        public string whoSend { get; set; }
        public string dateTime { get; set; }
        public string dateTimeUTC { get; set; }
        public object messageType { get; set; }
        public int dialogId { get; set; }
        public bool isVisibleForClient { get; set; }
        public string tag { get; set; }
        public Page page { get; set; }
        public string error { get; set; }
        public ReplyTo replyTo { get; set; }
        public OperatorGroup operatorGroup { get; set; }
    }

    public class Operator
    {
        public string login { get; set; }
        public string name { get; set; }
        public string avatar { get; set; }
        public int status { get; set; }
        public List<string> groupIds { get; set; }
    }

    public class OperatorGroup
    {
        public string id { get; set; }
        public string descr { get; set; }
    }

    public class Page
    {
        public string url { get; set; }
        public string title { get; set; }
    }

    public class ReplyTo
    {
    }

    public class TalkMeIncomingClientMessageDto
    {
        public string eventId { get; set; }
        public Data data { get; set; }
        public string secretKey { get; set; }
    }

    public class Site
    {
        public string id { get; set; }
        public string domain { get; set; }
    }

    public class Source
    {
        public Type type { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string link { get; set; }
        public Data data { get; set; }
        public string context { get; set; }
    }

    public class Sticker
    {
        public string fileName { get; set; }
        public bool isStaticImage { get; set; }
        public bool isTgs { get; set; }
        public string url { get; set; }
        public string emoji { get; set; }
    }

    public class Type
    {
        public string id { get; set; }
        public string name { get; set; }
        public string descr { get; set; }
        public string icon { get; set; }
    }

    public class Utm
    {
        public string utm_source { get; set; }
        public string utm_medium { get; set; }
        public string utm_campaign { get; set; }
        public string utm_term { get; set; }
        public string utm_content { get; set; }
        public string utm_referrer { get; set; }
    }

    public class YandexMetrika
    {
        public string clientId { get; set; }
        public string counterId { get; set; }
    }
}
