﻿using System.Linq.Expressions;

namespace Clouds42.HandlerExeption.Contract
{
    public interface IHandlerException
    {
        /// <summary>
        /// Базовый метод отправки сообщения об ошибке
        /// </summary>
        /// <param name="exception">ошибка</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        /// <param name="parameters">Входящие параметры.</param>
        void Handle(Exception exception, string operationTitle, params Expression<Func<object>>[] parameters);

        /// <summary>
        /// Базовый метод отправки сообщения об ошибке
        /// </summary>
        /// <param name="exception">ошибка</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку.</param>
        /// <param name="extraErrorMessage">Дополнителные сведения об ошибке.</param>
        /// <param name="parameters">Входящие параметры.</param>
        void Handle(Exception exception, string incidentMessage, string extraErrorMessage, params Expression<Func<object>>[] parameters);

        /// <summary>
        /// Отправка письма ядро предупреждение
        /// </summary>
        /// <param name="errorMessage">полный стек ошибки</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        void HandleWarning(string errorMessage, string operationTitle);

        /// <summary>
        /// Отправка письма ядро предупреждение
        /// </summary>
        /// <param name="errorMessage">Текст предупреждения.</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        /// <param name="parameters">Входящие параметры.</param>
        void HandleWarning(string errorMessage, string operationTitle, params Expression<Func<object>>[] parameters);
    }
}
