﻿using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.HandlerExeption.Contract
{
    public static class HandlerException42
    {
        private static IHandlerException _handlerException;

        public static void Init(IServiceProvider provider)
        {
            _handlerException = provider.GetRequiredService<IHandlerException>();
        }

        public static IHandlerException GetHandler() => _handlerException;
    }
}
