﻿namespace LinqExtensionsNetFramework
{
    public interface IHasSpecificSearch
    {
        /// <summary>
        /// Search property
        /// </summary>
        string SearchLine { get; set; }
    }
}
