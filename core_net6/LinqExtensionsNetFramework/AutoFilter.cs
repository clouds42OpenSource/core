﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace LinqExtensionsNetFramework
{
    /// <summary>
    /// Тип конкатенации выражений
    /// </summary>
    public enum ComposeKind
    {
        /// <summary>
        /// И
        /// </summary>
        And,

        /// <summary>
        /// Или
        /// </summary>
        Or
    }

    /// <summary>
    /// Sorting order directions
    /// </summary>
    public enum SortingOrder
    {
        /// <summary>
        /// Ascending
        /// </summary>
        Asc = 1,
        /// <summary>
        /// Descending
        /// </summary>
        Desc = 2,
    };

    /// <summary>
    /// Вспомогательный класс для автофильтрации
    /// </summary>
    public static class Filter
    {
        /// <summary>
        /// Автофильтрация
        /// </summary>
        /// <param name="query"></param>
        /// <param name="predicate"></param>
        /// <param name="composeKind"></param>
        /// <typeparam name="TSubject"></typeparam>
        /// <typeparam name="TPredicate"></typeparam>
        /// <returns></returns>
        public static IQueryable<TSubject> AutoFilter<TSubject, TPredicate>(
            this IQueryable<TSubject> query, TPredicate predicate, ComposeKind composeKind = ComposeKind.And)
            where TPredicate : class
        {
            return Conventions<TSubject>.Filter(query, predicate, composeKind);
        }

        /// <summary>
        /// Авто сортировка
        /// </summary>
        /// <param name="query"></param>
        /// <param name="predicate"></param>
        /// <param name="comparison"></param>
        /// <typeparam name="TSubject"></typeparam>
        /// <typeparam name="TPredicate"></typeparam>
        /// <returns></returns>
        public static IQueryable<TSubject> AutoSort<TSubject, TPredicate>(
            this IQueryable<TSubject> query, TPredicate predicate, StringComparison comparison = StringComparison.Ordinal) where TPredicate : ISortedQuery
        {
            var orderBy = FastTypeInfo<TPredicate>.PublicProperties.FirstOrDefault(x => x.Name.Equals(nameof(predicate.OrderBy), comparison));

            return orderBy?.GetValue(predicate, null) is not string propertyValue
                ? query
                : Conventions<TSubject>.MultipleSort(query, propertyValue);
        }

        /// <summary>
        /// Класс хелпер с extention методами для фильтрации
        /// </summary>
        /// <typeparam name="TSubject"></typeparam>
        public static class Conventions<TSubject>
        {
            /// <summary>
            /// Ссылка на метод string.Contains()
            /// </summary>
            public static readonly MethodInfo Contains = typeof(string)
                .GetMethod(nameof(Contains), [typeof(string)]);

            /// <summary>
            /// Автофильтрация
            /// </summary>
            /// <param name="query"></param>
            /// <param name="predicate"></param>
            /// <param name="composeKind"></param>
            /// <typeparam name="TPredicate"></typeparam>
            /// <returns></returns>
            public static IQueryable<TSubject> Filter<TPredicate>(IQueryable<TSubject> query, TPredicate predicate,
                ComposeKind composeKind = ComposeKind.And)
            {
                if (predicate == null)
                    return query;

                var filterProperties = FastTypeInfo<TPredicate>
                    .PublicProperties
                    .ToArray();

                var filterPropertiesNames = filterProperties
                    .Select(x => x.Name)
                    .ToArray();

                var modelType = typeof(TSubject);

                var parameter = Expression.Parameter(modelType);

                var props = FastTypeInfo<TSubject>
                    .PublicProperties
                    .Where(x => filterPropertiesNames.Contains(x.Name))
                    .Select(x => new
                    {
                        Property = x,
                        Value = filterProperties.Single(y => y.Name == x.Name).GetValue(predicate)
                    })
                    .Where(x => x.Value != null)
                    .Select(x =>
                    {
                        Expression property = Expression.Property(parameter, x.Property);
                        Expression value = Expression.Constant(x.Value);
                        value = Expression.Convert(value, property.Type);

                        var body = (Expression)Expression.Equal(property, value);

                        if (property.Type != typeof(string))
                        {
                            return Expression.Lambda<Func<TSubject, bool>>(body, parameter);
                        }

                        MethodInfo toLowerMethod = typeof(string).GetMethod("ToLower", Type.EmptyTypes);
                        Expression lowerProperty = Expression.Call(property, toLowerMethod);
                        Expression lowerValue = Expression.Call(value, toLowerMethod);
                        body = Expression.Call(lowerProperty, Contains, lowerValue);

                        return Expression.Lambda<Func<TSubject, bool>>(body, parameter);
                    })
                    .ToArray();

                if (!props.Any())
                {
                    return query;
                }

                var expr = composeKind == ComposeKind.And
                    ? props.Aggregate((c, n) => c.And(n))
                    : props.Aggregate((c, n) => c.Or(n));

                return query.Where(expr);
            }

            /// <summary>
            /// Multiple column sorting
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="toSort"></param>
            /// <param name="propertyNames"></param>
            /// <returns></returns>
            public static IOrderedQueryable<T> MultipleSort<T>(IQueryable<T> toSort, string propertyNames)
            {
                var sortOptions = GetSorting(propertyNames);

                return sortOptions
                    .Aggregate<KeyValuePair<string, SortingOrder>, IOrderedQueryable<T>>(null, 
                        (current, entry) => current != null ? current.ApplyOrder(entry.Key, entry.Value == SortingOrder.Asc ? "ThenBy" : "ThenByDescending") : 
                            toSort.ApplyOrder(entry.Key, entry.Value == SortingOrder.Asc ? "OrderBy" : "OrderByDescending"));
            }
        }

        /// <summary>
        /// Build Dictionary of sorting params by input property value (Property names)
        /// Ex: name.asc,info.status.desc
        /// </summary>
        /// <param name="propertyNames"></param>
        /// <returns></returns>
        private static Dictionary<string, SortingOrder> GetSorting(string propertyNames)
        {
            var data = new Dictionary<string, SortingOrder>();
            foreach (var propertyName in propertyNames.Split(','))
            {
                var arr = propertyName.SplitLast('.');

                if (arr.Length == 1)
                {
                    data.Add(arr[0], SortingOrder.Asc);
                    continue;
                }

                data.Add(arr[0],
                    string.Equals(arr[1], "desc", StringComparison.CurrentCultureIgnoreCase)
                        ? SortingOrder.Desc
                        : SortingOrder.Asc);
            }

            return data;
        }

        /// <summary>
        /// Apply order methods to query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="property"></param>
        /// <param name="methodName"></param>
        /// <returns></returns>
        private static IOrderedQueryable<T> ApplyOrder<T>(this IQueryable<T> source, string property, string methodName)
        {
            var param = Expression.Parameter(typeof(T), "x");
            Expression expr = property.Split('.').Aggregate<string, Expression>(param, Expression.PropertyOrField);
            var delegateType = typeof(Func<,>).MakeGenericType(typeof(T), expr.Type);
            var lambda = Expression.Lambda(delegateType, expr, param);

            var mi = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                            && method.IsGenericMethodDefinition
                            && method.GetGenericArguments().Length == 2
                            && method.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(T), expr.Type);
            return (IOrderedQueryable<T>)mi.Invoke(null, [source, lambda]);
        }

        /// <summary>
        /// Split last
        /// </summary>
        /// <param name="value"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string[] SplitLast(this string value, char separator)
        {
            var lastIndex = value.LastIndexOf(separator);

            return lastIndex != -1
                ? [value[..lastIndex], value[(lastIndex + 1)..]]
                : [value];
        }
    }
}
