﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace LinqExtensionsNetFramework
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"></param>
    /// <typeparam name="T"></typeparam>
    public delegate T ObjectActivator<out T>(params object[] args);

    /// <summary>
    /// Extention класс для работы с рефлексией
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class FastTypeInfo<T>
    {
        private static readonly Attribute[] _attributes;

        private static readonly PropertyInfo[] _properties;

        private static readonly MethodInfo[] _methods;

        private static ConstructorInfo[] _constructors;

        private static ConcurrentDictionary<string, ObjectActivator<T>> _activators;

        static FastTypeInfo()
        {
            var type = typeof(T);
            _attributes = type.GetCustomAttributes().ToArray();

            _properties = type
                .GetProperties()
                .Where(x => x.CanRead && x.CanWrite)
                .ToArray();

            _methods = type.GetMethods()
                .Where(x => x.IsPublic && !x.IsAbstract)
                .ToArray();

            _constructors = typeof(T).GetConstructors();
            _activators = new ConcurrentDictionary<string, ObjectActivator<T>>();
        }

        /// <summary>
        /// Ссылка на публичные свойства типа
        /// </summary>
        public static PropertyInfo[] PublicProperties => _properties;

        /// <summary>
        /// Ссылка на публичные методы типа
        /// </summary>
        public static MethodInfo[] PublicMethods => _methods;

        /// <summary>
        /// Ссылка на аттрибуты типа
        /// </summary>
        public static Attribute[] Attributes => _attributes;

        /// <summary>
        /// Содержит ли класс аттрибут типа TAttr
        /// </summary>
        /// <typeparam name="TAttr"></typeparam>
        /// <returns></returns>
        public static bool HasAttribute<TAttr>()
            where TAttr : Attribute
            => Attributes.Any(x => x.GetType() == typeof(TAttr));

        /// <summary>
        /// Получение аттрибута типа TAttr
        /// </summary>
        /// <typeparam name="TAttr"></typeparam>
        /// <returns></returns>
        public static TAttr GetCustomAttribute<TAttr>()
            where TAttr : Attribute
            => (TAttr)_attributes.FirstOrDefault(x => x.GetType() == typeof(TAttr));

        /// <summary>
        /// Acctivator построенный с помощью деревье выражений
        /// </summary>
        private static ObjectActivator<T> GetActivator(ConstructorInfo ctor)
        {
            var paramsInfo = ctor.GetParameters();

            var param = Expression.Parameter(typeof(object[]), "args");

            var argsExp = new Expression[paramsInfo.Length];

            for (var i = 0; i < paramsInfo.Length; i++)
            {
                var index = Expression.Constant(i);
                var paramType = paramsInfo[i].ParameterType;

                Expression paramAccessorExp = Expression.ArrayIndex(param, index);
                Expression paramCastExp = Expression.Convert(paramAccessorExp, paramType);

                argsExp[i] = paramCastExp;
            }

            var newExp = Expression.New(ctor, argsExp);

            var lambda = Expression.Lambda(typeof(ObjectActivator<T>), newExp, param);

            var compiled = (ObjectActivator<T>)lambda.Compile();
            return compiled;
        }

        /// <summary>
        /// Создание метода
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static Delegate CreateMethod(MethodInfo method)
        {
            if (method == null)
            {
                throw new ArgumentNullException(nameof(method));
            }

            if (!method.IsStatic)
            {
                throw new ArgumentException("The provided method must be static.", nameof(method));
            }

            if (method.IsGenericMethod)
            {
                throw new ArgumentException("The provided method must not be generic.", nameof(method));
            }

            var parameters = method.GetParameters()
                .Select(p => Expression.Parameter(p.ParameterType, p.Name))
                .ToArray();

            var call = Expression.Call(null, method, parameters);
            return Expression.Lambda(call, parameters).Compile();
        }

        /// <summary>
        /// Получение значения свойства
        /// </summary>
        /// <param name="propertyName"></param>
        /// <typeparam name="TObject"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <returns></returns>
        public static Func<TObject, TProperty> PropertyGetter<TObject, TProperty>(string propertyName)
        {
            var paramExpression = Expression.Parameter(typeof(TObject), "value");

            var propertyGetterExpression = Expression.Property(paramExpression, propertyName);

            var result = Expression.Lambda<Func<TObject, TProperty>>(propertyGetterExpression, paramExpression)
                .Compile();

            return result;
        }

        /// <summary>
        /// Установщик значений в свойство
        /// </summary>
        /// <param name="propertyName"></param>
        /// <typeparam name="TObject"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <returns></returns>
        public static Action<TObject, TProperty> PropertySetter<TObject, TProperty>(string propertyName)
        {
            var paramExpression = Expression.Parameter(typeof(TObject));
            var paramExpression2 = Expression.Parameter(typeof(TProperty), propertyName);
            var propertyGetterExpression = Expression.Property(paramExpression, propertyName);
            var result = Expression.Lambda<Action<TObject, TProperty>>
            (
                Expression.Assign(propertyGetterExpression, paramExpression2), paramExpression, paramExpression2
            ).Compile();

            return result;
        }
    }
}
