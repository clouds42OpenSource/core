﻿using System;
using System.Collections.Concurrent;
using System.Linq.Expressions;

namespace LinqExtensionsNetFramework
{
    public static class CompiledExpressions<TIn, TOut>
    {
        private static readonly ConcurrentDictionary<Expression<Func<TIn, TOut>>, Func<TIn, TOut>> Cache = new ConcurrentDictionary<Expression<Func<TIn, TOut>>, Func<TIn, TOut>>();

        /// <summary>
        /// Сохранение в кеш
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static Func<TIn, TOut> AsFunc(Expression<Func<TIn, TOut>> expression) =>
            Cache.GetOrAdd(expression, expression.Compile());
    }

    /// <summary>
    /// 
    /// </summary>
    public static class ExpressionExtensions
    {
        private static readonly ConcurrentDictionary<string, object> Cache = new ConcurrentDictionary<string, object>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expr"></param>
        /// <typeparam name="TIn"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <returns></returns>
        public static Func<TIn, TOut> AsFunc<TIn, TOut>(this Expression<Func<TIn, TOut>> expr)
            => CompiledExpressions<TIn, TOut>.AsFunc(expr);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="expr"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool Is<T>(this T entity, Expression<Func<T, bool>> expr)
            => expr.AsFunc().Invoke(entity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="mapFrom"></param>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <typeparam name="TReturn"></typeparam>
        /// <returns></returns>
        public static Expression<Func<TDestination, TReturn>> From<TSource, TDestination, TReturn>(
            this Expression<Func<TSource, TReturn>> source, Expression<Func<TDestination, TSource>> mapFrom)
            => Expression.Lambda<Func<TDestination, TReturn>>(
                Expression.Invoke(source, mapFrom.Body), mapFrom.Parameters);
    }
}
