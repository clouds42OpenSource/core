﻿using System;
using System.Linq.Expressions;

namespace LinqExtensionsNetFramework
{
    public class Spec<T>(Expression<Func<T, bool>> expression)
    {
        /// <summary>
        /// Перегрузка, оператов, чтобы была возможность использования двойных амперсандов
        /// </summary>
        /// <param name="_">Спеицифкация</param>
        /// <returns></returns>
        public static bool operator false(Spec<T> _) => false;

        /// <summary>
        /// Перегрузка, оператов, чтобы была возможность использования двойных амперсандов
        /// </summary>
        /// <param name="_">Спецификация</param>
        /// <returns></returns>
        public static bool operator true(Spec<T> _) => false;

        /// <summary>
        /// Оператор логического И
        /// </summary>
        /// <param name="spec1">Первая спецификация</param>
        /// <param name="spec2">Вторая спецификация</param>
        /// <returns></returns>
        public static Spec<T> operator &(Spec<T> spec1, Spec<T> spec2)
            => new(spec1._expression.And(spec2._expression));

        /// <summary>
        /// Оператор логического ИЛИ
        /// </summary>
        /// <param name="spec1">Первая спецификация</param>
        /// <param name="spec2">Вторая спецификация</param>
        /// <returns></returns>
        public static Spec<T> operator |(Spec<T> spec1, Spec<T> spec2)
            => new(spec1._expression.Or(spec2._expression));

        /// <summary>
        /// Опервтор инверсии логической операции
        /// </summary>
        /// <param name="spec">Спецификация</param>
        /// <returns></returns>
        public static Spec<T> operator !(Spec<T> spec)
            => new(spec._expression.Not());

        /// <summary>
        /// Перегрузка implicit оператора, чтобы компилятор сам мог подставить нужный тип спецификации
        /// </summary>
        /// <param name="spec">Спецификация</param>
        /// <returns></returns>
        public static implicit operator Expression<Func<T, bool>>(Spec<T> spec)
            => spec._expression;

        /// <summary>
        /// Перегрузка implicit оператора, чтобы компилятор сам мог подставить нужный тип спецификации
        /// </summary>
        /// <param name="spec">Спецификация</param>
        public static implicit operator Func<T, bool>(Spec<T> spec)
            => spec.IsSatisfiedBy;

        /// <summary>
        /// Перегрузка implicit оператора, чтобы компилятор сам мог подставить нужный тип спецификации
        /// </summary>
        /// <param name="expression">Выражение</param>
        public static implicit operator Spec<T>(Expression<Func<T, bool>> expression)
            => new(expression);

        private readonly Expression<Func<T, bool>> _expression = expression ?? throw new ArgumentNullException(nameof(expression));

        /// <summary>
        /// Компиляция выражения
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool IsSatisfiedBy(T obj) => _expression.AsFunc()(obj);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapFrom"></param>
        /// <typeparam name="TParent"></typeparam>
        /// <returns></returns>
        public Spec<TParent> From<TParent>(Expression<Func<TParent, T>> mapFrom)
            => _expression.From(mapFrom);
    }
}
