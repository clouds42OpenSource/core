﻿namespace LinqExtensionsNetFramework
{
    /// <summary>
    ///     Query with sort support
    /// </summary>
    public interface ISortedQuery
    {
        /// <summary>
        ///     Order by field
        /// </summary>
        string OrderBy { get; set; }
    }
}
