﻿using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations;
using Clouds42.DependencyRegistration;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using Core42.Application.Features.AccountUsersContext.Commands;
using GCP.Extensions.Configuration.SecretManager;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories;

namespace EmailMailingAndVerify;

public enum Mode
{
    OnlyChangeAdminUsers = 1,
    OnlyEmailNotify = 2,
    AllowAll = 3
}
internal class Program
{
    private static readonly ServiceCollection ServiceCollection = [];
    private static IUnitOfWork _dbLayer;
    private static ILogger42 _logger;
    private static List<Guid>? _userIds;
    private static Mode _mode;
    static void Main()
    {
        try
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            IConfiguration configuration = new ConfigurationBuilder()
                .AddGcpKeyValueSecrets()
                .AddJsonFile("appsettings.json", true, true)
                .Build();
            
            ServiceCollection
                .AddSerilogWithElastic(configuration)
                .AddScoped<IUnitOfWork, UnitOfWork>()
                .AddGlobalServices(configuration)
                .AddTransient<IAccessProvider, SystemAccessProvider>()
                .AddTelegramBots(configuration)
                .AddSingleton(configuration)
                .AddTransient<ILogger42, SerilogLogger42>();

            var serviceProvider = ServiceCollection.BuildServiceProvider();
            _logger = serviceProvider.GetRequiredService<ILogger42>();
            _userIds = configuration.GetSection("UserIds").Get<List<Guid>?>();
            _mode = configuration.GetSection("Mode").Get<Mode>();
            _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
            ConfigurationHelper.SetConfiguration(configuration);
            HandlerException42.Init(serviceProvider);
            Logger42.Init(serviceProvider);


            switch (_mode)
            {
                case Mode.AllowAll:
                    ChangeUserAdminsEmailStatusAndSetUpnInAd();
                    EmailVerifyNotifyAndPushToNotifyCenter(serviceProvider);
                    break;

                case Mode.OnlyChangeAdminUsers:
                    ChangeUserAdminsEmailStatusAndSetUpnInAd();
                    break;


                case Mode.OnlyEmailNotify:
                    EmailVerifyNotifyAndPushToNotifyCenter(serviceProvider);
                    break;

                default: throw new NotFoundException("Не выбран тип приложения, проверить Mode в appsettings.json");
            }

        }

        catch (Exception ex)
        {
            Console.WriteLine($"ОШИБКА ПРИЛОЖЕНИЯ {ex}");
            Console.ReadLine();
        }
    }

    static void ChangeUserAdminsEmailStatusAndSetUpnInAd()
    {
        try
        {
            Console.WriteLine("Начинаем верификацию почты всем администраторам");
            if (_userIds != null && _userIds.Any())
            {
                Console.WriteLine(
                    "Коллекция с идентификаторами пользователей из конфига содержит данные. Верификация будет произведена только указанным пользователям");

                var users = _dbLayer.AccountUsersRepository.AsQueryable().Where(x => _userIds.Contains(x.Id)).ToList();

                foreach (var user in users)
                {
                    user.EmailStatus = "Checked";
                    ActiveDirectoryUserFunctions.ChangeEmail(user.Email, user.Login);
                }
                _dbLayer.BulkUpdate(users);
                _dbLayer.Save();

                Console.WriteLine("Верификация завершена");
                Console.ReadLine();
            }

            else
            {
                Console.WriteLine("Коллекция с идентификаторами пользователей из конфига НЕ содержит данные. Верификация будет произведена всем пользователям");

                var users = _dbLayer.AccountUsersRepository
                    .AsQueryable()
                    .Where(x => x.EmailStatus != "Checked" && x.CorpUserSyncStatus != "Deleted" &&
                                x.CorpUserSyncStatus != "SyncDeleted" &&
                                x.AccountUserRoles.Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin))
                    .ToList();

                foreach (var user in users)
                {
                    user.EmailStatus = "Checked";
                    ActiveDirectoryUserFunctions.ChangeEmail(user.Email, user.Login);
                }

                _dbLayer.BulkUpdate(users);
                _dbLayer.Save();

                Console.WriteLine("Верификация завершена");
            }
        }
        catch (Exception e)
        {
            const string message = "Произошла ошибка при верификации почт админов пользователей";
            _logger.Error(e, message);

            Console.WriteLine($"{message}\n{e.Message}");
        }
            
    }

    static void EmailVerifyNotifyAndPushToNotifyCenter(IServiceProvider serviceProvider)
    {
        try
        {
            Console.WriteLine("Начинаем отправку email рассылки с подтверждением почты и добавлением уведомлений");
            var sender = serviceProvider.GetRequiredService<ISender>();
            if (_userIds != null && _userIds.Any())
            {
                Console.WriteLine("Коллекция с идентификаторами пользователей из конфига содержит данные. Рассылка будет произведена только указанным пользователям");

                var users = _dbLayer.AccountUsersRepository.AsQueryable().Where(x => _userIds.Contains(x.Id)).ToList();
                var notifications = (from user in users let result = sender.Send(new VerifyEmailCommand { AccountUserId = user.Id, Email = user.Email, SendOnlyLink = true }).Result select new Notification { AccountUserId = user.Id, CreatedOn = DateTime.Now, Message = $"Для подтверждения вашей почты пройдите по ссылке {result.Result.Link}" }).ToList();

                _dbLayer.BulkInsert(notifications);
                _dbLayer.Save();
                Console.WriteLine("Рассылка завершена");
                Console.ReadLine();
            }

            else
            {
                Console.WriteLine("Коллекция с идентификаторами пользователей из конфига НЕ содержит данные. Рассылка будет будет произведена всем пользователям");

                var users = _dbLayer.AccountUsersRepository
                    .AsQueryable()
                    .Where(x => x.EmailStatus != "Checked" && x.CorpUserSyncStatus != "Deleted" &&
                                x.CorpUserSyncStatus != "SyncDeleted")
                    .ToList();
                var notifications = (from user in users let result = sender.Send(new VerifyEmailCommand { AccountUserId = user.Id, Email = user.Email, SendOnlyLink = true }).Result select new Notification { AccountUserId = user.Id, CreatedOn = DateTime.Now, Message = $"Для подтверждения вашей почты пройдите по ссылке {result.Result.Link}" }).ToList();
                
                _dbLayer.BulkInsert(notifications);
                _dbLayer.Save();

                Console.WriteLine("Рассылка завершена");
            }
        }
        catch (Exception e)
        {
            const string message = "Произошла ошибка при рассылке";
            _logger.Error(e, message);

            Console.WriteLine($"{message}\n{e.Message}");
        }
    }
}

public static class DependencyConfiguration
{
    public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
    {
        var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

        var elasticConfig = new ElasticConfigDto(
            conf["Elastic:Uri"],
            conf["Elastic:UserName"],
            conf["Elastic:Password"],
            conf["Elastic:ApiIndex"],
            conf["Logs:AppName"]
        );

        SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);
        return collection;
    }
}
