﻿using Clouds42.DaData.Models.Request;
using Clouds42.Domain.Enums;

namespace Clouds42.DaData.Providers
{
    /// <summary>
    /// Провайдер отправки запроса
    /// на сервис получения реквизитов
    /// </summary>
    public interface IDaDataRequestProvider
    {
        /// <summary>
        /// Отправить запрос
        /// </summary>
        /// <param name="requestData">Данные запроса</param>
        /// <param name="requestType">Тип запроса к сервису</param>
        /// <param name="success">Успешность</param>
        /// <returns>Ответ на запрос</returns>
        string SendRequest(DaDataRequestModel requestData, DaDataRequestType requestType, out bool success);
    }
}
