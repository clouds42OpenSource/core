﻿using System;
using System.Collections.Generic;
using System.Net;
using Clouds42.Configurations.Configurations;
using Clouds42.DaData.Models;
using Clouds42.DaData.Models.Request;
using Clouds42.Domain.Enums;
using Clouds42.WebKit;
using Newtonsoft.Json;

namespace Clouds42.DaData.Providers
{
    /// <summary>
    /// Провайдер отправки запроса
    /// на сервис DaData для получения реквизитов
    /// </summary>
    internal class DaDataRequestProvider(IHttpWebRequestProvider webRequestProvider) : IDaDataRequestProvider
    {
        /// <summary>
        /// Отправить запрос
        /// </summary>
        /// <param name="requestData">Данные запроса</param>
        /// <param name="requestType">Тип запроса к сервису</param>
        /// <param name="success">Успешность</param>
        /// <returns>Ответ на запрос</returns>
        public string SendRequest(DaDataRequestModel requestData, DaDataRequestType requestType, out bool success)
        {
            var daDataConfiguration = GetConfiguration(requestType);
            var requestJson = JsonConvert.SerializeObject(requestData);
            var webHeaderCollection = new WebHeaderCollection
            {
                {HttpRequestHeader.Authorization, $"Token {daDataConfiguration.Token}"},
                {"X-Secret", daDataConfiguration.Key}
            };

            var result = webRequestProvider.SendPostJsonWithHeaders(daDataConfiguration.Url, requestJson, webHeaderCollection, out success);
            return result;
        }

        /// <summary>
        /// Получить конфигурацию
        /// для сервиса
        /// </summary>
        /// <returns>Конфигурация для сервиса получения реквизитов</returns>
        private DaDataConfiguration GetConfiguration(DaDataRequestType requestType) => new()
        {
            Url = _methodsForGettingUrl[requestType](),
            Key = CloudConfigurationProvider.DaData.GetKey(),
            Token = CloudConfigurationProvider.DaData.GetToken()
        };

        /// <summary>
        /// Методы для получения УРЛ
        /// </summary>
        private readonly IDictionary<DaDataRequestType, Func<string>> _methodsForGettingUrl =
            new Dictionary<DaDataRequestType, Func<string>>
            {
                [DaDataRequestType.GettingBankDetails] = CloudConfigurationProvider.DaData.GetUrlApiForBankDetails,
                [DaDataRequestType.GettingCompanyDetails] =
                    CloudConfigurationProvider.DaData.GetUrlApiForCompanyDetails,
            };
    }
}
