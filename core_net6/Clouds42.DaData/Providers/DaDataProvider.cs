﻿using System;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.DaData.Mappers;
using Clouds42.DaData.Models;
using Clouds42.DaData.Models.Request;
using Clouds42.DaData.Models.Results.Bank;
using Clouds42.DaData.Models.Results.LegalEntity;
using Clouds42.DaData.Models.Results.SoleProprietor;
using Clouds42.Domain.Enums;
using Newtonsoft.Json;

namespace Clouds42.DaData.Providers
{
    /// <summary>
    /// Провайдер сервиса DaData для получения реквизитов
    /// </summary>
    internal class DaDataProvider(IDaDataRequestProvider daDataRequestProvider) : IDaDataProvider
    {
        /// <summary>
        /// Получить банковские реквизиты
        /// </summary>
        /// <param name="queryString">Строка запроса (БИК/SWIFT)</param>
        /// <returns>Результат получения реквизитов банка</returns>
        public ResultOfGettingBankDetailsDto GetBankDetails(string queryString)
        {
            try
            {
                var queryResult = daDataRequestProvider.SendRequest(new DaDataRequestModel { QueryString = queryString },
                    DaDataRequestType.GettingBankDetails, out bool success);

                if (!success)
                     throw new InvalidOperationException(queryResult);

                var resultModel = JsonConvert.DeserializeObject<DaDataBankDetailsResultModel>(queryResult);
                var suggestion = resultModel.Suggestions.FirstOrDefault() ??
                                       throw new NotFoundException(
                                           $"По заданной строке поиска {queryString}, реквизиты банка не найдены");

                return suggestion.MapToResultOfGettingBankDetails();
            }
            catch (Exception ex)
            {
                return new ResultOfGettingBankDetailsDto { ErrorMessage = ex.Message, Success = false };
            }
        }

        /// <summary>
        /// Получить реквизиты ИП
        /// </summary>
        /// <param name="queryString">Строка запроса (ИНН/ОГРН)</param>
        /// <returns>Результат получения реквизитов ИП</returns>
        public ResultOfGettingSoleProprietorDetailsDto GetSoleProprietorDetails(string queryString)
        {
            try
            {
                var queryResult = daDataRequestProvider.SendRequest(new DaDataRequestModel { QueryString = queryString },
                    DaDataRequestType.GettingCompanyDetails, out bool success);

                if (!success)
                    throw new InvalidOperationException(queryResult);

                var resultModel = JsonConvert.DeserializeObject<DaDataSoleProprietorDetailsResultModel>(queryResult);
                var suggestion = resultModel.Suggestions.FirstOrDefault() ??
                                       throw new NotFoundException(
                                           $"По заданной строке поиска {queryString}, реквизиты ИП не найдены");

                return suggestion.MapToResultOfGettingSoleProprietorDetails();
            }
            catch (Exception ex)
            {
                return new ResultOfGettingSoleProprietorDetailsDto { ErrorMessage = ex.Message, Success = false };
            }
        }

        /// <summary>
        /// Получить реквизиты юр. лица
        /// </summary>
        /// <param name="queryString">Строка запроса (ИНН/ОГРН)</param>
        /// <returns>Результат получения реквизитов юр. лица</returns>
        public ResultOfGettingLegalEntityDetailsDto GetLegalEntityDetails(string queryString)
        {
            try
            {
                var queryResult = daDataRequestProvider.SendRequest(new DaDataRequestModel { QueryString = queryString },
                    DaDataRequestType.GettingCompanyDetails, out bool success);

                if (!success)
                    throw new InvalidOperationException(queryResult);

                var resultModel = JsonConvert.DeserializeObject<DaDataLegalEntityDetailsResultModel>(queryResult);
                var suggestion = resultModel.Suggestions.FirstOrDefault() ??
                                 throw new NotFoundException(
                                     $"По заданной строке поиска {queryString}, реквизиты юр. лица не найдены");

                return suggestion.MapToResultOfGettingLegalEntityDetails();
            }
            catch (Exception ex)
            {
                return new ResultOfGettingLegalEntityDetailsDto { ErrorMessage = ex.Message, Success = false };
            }
        }
    }
}
