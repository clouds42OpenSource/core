﻿using System;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DaData.Models;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.DaData.Providers
{
    /// <summary>
    /// Менеджер сервиса DaData для получения реквизитов
    /// </summary>
    public class DaDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IDaDataProvider daDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить банковские реквизиты
        /// </summary>
        /// <param name="queryString">Строка запроса (БИК/SWIFT)</param>
        /// <returns>Результат получения реквизитов банка</returns>
        public ManagerResult<ResultOfGettingBankDetailsDto> GetBankDetails(string queryString)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DaData_View, () => AccessProvider.ContextAccountId);
                var result = daDataProvider.GetBankDetails(queryString);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ResultOfGettingBankDetailsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить реквизиты ИП
        /// </summary>
        /// <param name="queryString">Строка запроса (ИНН/ОГРН)</param>
        /// <returns>Результат получения реквизитов ИП</returns>
        public ManagerResult<ResultOfGettingSoleProprietorDetailsDto> GetSoleProprietorDetails(string queryString)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DaData_View, () => AccessProvider.ContextAccountId);
                var result = daDataProvider.GetSoleProprietorDetails(queryString);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ResultOfGettingSoleProprietorDetailsDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить реквизиты юр. лица
        /// </summary>
        /// <param name="queryString">Строка запроса (ИНН/ОГРН)</param>
        /// <returns>Результат получения реквизитов юр. лица</returns>
        public ManagerResult<ResultOfGettingLegalEntityDetailsDto> GetLegalEntityDetails(string queryString)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.DaData_View, () => AccessProvider.ContextAccountId);
                var result = daDataProvider.GetLegalEntityDetails(queryString);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<ResultOfGettingLegalEntityDetailsDto>(ex.Message);
            }
        }
    }
}
