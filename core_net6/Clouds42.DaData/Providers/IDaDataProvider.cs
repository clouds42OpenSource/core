﻿using Clouds42.DaData.Models;

namespace Clouds42.DaData.Providers
{
    /// <summary>
    /// Провайдер сервиса получения реквизитов
    /// </summary>
    public interface IDaDataProvider
    {
        /// <summary>
        /// Получить банковские реквизиты
        /// </summary>
        /// <param name="queryString">Строка запроса (БИК/SWIFT)</param>
        /// <returns>Результат получения реквизитов банка</returns>
        ResultOfGettingBankDetailsDto GetBankDetails(string queryString);

        /// <summary>
        /// Получить реквизиты ИП
        /// </summary>
        /// <param name="queryString">Строка запроса (ИНН/ОГРН)</param>
        /// <returns>Результат получения реквизитов ИП</returns>
        ResultOfGettingSoleProprietorDetailsDto GetSoleProprietorDetails(string queryString);

        /// <summary>
        /// Получить реквизиты юр. лица
        /// </summary>
        /// <param name="queryString">Строка запроса (ИНН/ОГРН)</param>
        /// <returns>Результат получения реквизитов юр. лица</returns>
        ResultOfGettingLegalEntityDetailsDto GetLegalEntityDetails(string queryString);
    }
}
