﻿using Clouds42.DaData.Providers;
using Clouds42.WebKit;
using Clouds42.WebKit.RequestProviders;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.DaData
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddDaData(this IServiceCollection services)
        {
            services.AddTransient<IDaDataProvider, DaDataProvider>();
            services.AddTransient<IHttpWebRequestProvider, HttpWebRequestProvider>();
            services.AddTransient<IDaDataRequestProvider, DaDataRequestProvider>();
            services.AddTransient<DaDataManager>();

            return services;
        }
    }
}
