﻿using Clouds42.DaData.Models;
using Clouds42.DaData.Models.Results;
using Clouds42.DaData.Models.Results.Bank;
using Clouds42.DaData.Models.Results.LegalEntity;
using Clouds42.DaData.Models.Results.SoleProprietor;

namespace Clouds42.DaData.Mappers
{
    /// <summary>
    /// Маппер моделей сервиса DaData для получения реквизитов
    /// </summary>
    public static class DaDataMapper
    {

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="model">Предложенная информация по банку</param>
        /// <returns>Результат получения реквизитов банка</returns>
        public static ResultOfGettingBankDetailsDto MapToResultOfGettingBankDetails(this SuggestionByBank model)
        {
            return new ResultOfGettingBankDetailsDto
            {
                Success = true,
                BankName = model.BankName,
                BankAdress = model.BankData?.BankAdress?.Adress,
                Bik = model.BankData?.Bik,
                CorrespondentAccount = model.BankData?.CorrespondentAccount,
                RegistrationNumber = model.BankData?.RegistrationNumber,
                Swift = model.BankData?.Swift
            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="model">Предложенная информация по ИП</param>
        /// <returns>Результат получения реквизитов ИП</returns>
        public static ResultOfGettingSoleProprietorDetailsDto MapToResultOfGettingSoleProprietorDetails(
            this SuggestionBySoleProprietor model)
        {
            return new ResultOfGettingSoleProprietorDetailsDto
            {
                Success = true,
                CompanyName = model.CompanyName,
                Inn = model.SoleProprietorData?.Inn,
                Ogrn = model.SoleProprietorData?.Ogrn,
                FullName = model.SoleProprietorData?.NameOfSoleProprietorOwner?.FullName,
                Address = model.SoleProprietorData?.Address?.AddressData.GetFullAddress() ?? string.Empty
            };
        }

        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="model">Предложенная информация по юр. лицу</param>
        /// <returns>Результат получения реквизитов юр. лица</returns>
        public static ResultOfGettingLegalEntityDetailsDto MapToResultOfGettingLegalEntityDetails(
            this SuggestionByLegalEntity model)
        {
            return new ResultOfGettingLegalEntityDetailsDto
            {
                Success = true,
                CompanyName = model.CompanyName,
                Inn = model.LegalEntityData?.Inn,
                Kpp = model.LegalEntityData?.Kpp,
                Ogrn = model.LegalEntityData?.Ogrn,
                Address = model.LegalEntityData?.Address?.FullAdress,
                HeadFullName = model.LegalEntityData?.ManagementLegalEntity?.HeadFullName,
                HeadPosition = model.LegalEntityData?.ManagementLegalEntity?.HeadPosition
            };
        }

        /// <summary>
        /// Получить полный адрес
        /// </summary>
        /// <param name="addressData">Данные об адресе</param>
        /// <returns>Полный адрес</returns>
        private static string GetFullAddress(this AddressData addressData) =>
            $"{addressData?.PostalCode ?? string.Empty}{addressData?.Country.GetValueWithDelimiter()}{addressData?.City.GetValueWithDelimiter()}";

        /// <summary>
        /// Получить значение с разделителем
        /// </summary>
        /// <param name="value">Значение</param>
        /// <returns>Значение с разделителем</returns>
        private static string GetValueWithDelimiter(this string value)
        {
            return !string.IsNullOrEmpty(value) ? $" {value}, " : string.Empty;
        }
    }
}
