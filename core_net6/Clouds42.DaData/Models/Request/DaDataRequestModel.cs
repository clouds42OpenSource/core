﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Request
{
    /// <summary>
    /// Модель запроса сервиса для получения реквизитов
    /// </summary>
    public class DaDataRequestModel
    {
        /// <summary>
        /// Строка запроса
        /// </summary>
        [JsonProperty("query")]
        public string QueryString { get; set; }

        /// <summary>
        /// Тип филиала
        /// </summary>
        [JsonProperty("branch_type")]
        public string BranchType { get; set; } = "MAIN";
    }
}
