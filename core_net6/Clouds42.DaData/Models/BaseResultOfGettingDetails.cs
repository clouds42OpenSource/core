﻿namespace Clouds42.DaData.Models
{
    /// <summary>
    /// Базовая модель результата
    /// получения реквизитов
    /// </summary>
    public abstract class BaseResultOfGettingDetails
    {
        /// <summary>
        /// Успех
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
