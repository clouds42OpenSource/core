﻿namespace Clouds42.DaData.Models
{
    /// <summary>
    /// Результат получения реквизитов юр. лица
    /// </summary>
    public class ResultOfGettingLegalEntityDetailsDto : BaseResultOfGettingDetails
    {
        /// <summary>
        /// Наименование компании
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        public string Ogrn { get; set; }

        /// <summary>
        /// КПП
        /// </summary>
        public string Kpp { get; set; }
        
        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// ФИО руководителя
        /// </summary>
        public string HeadFullName { get; set; }

        /// <summary>
        /// Должность руководителя
        /// </summary>
        public string HeadPosition { get; set; }
    }
}
