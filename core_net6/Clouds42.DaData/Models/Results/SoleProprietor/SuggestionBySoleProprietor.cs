﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.SoleProprietor
{
    /// <summary>
    /// Предложенная информация по ИП
    /// </summary>
    public class SuggestionBySoleProprietor
    {
        /// <summary>
        /// Наименование ИП
        /// </summary>
        [JsonProperty("value")]
        public string CompanyName { get; set; }

        /// <summary>
        /// Данные ИП
        /// </summary>
        [JsonProperty("data")]
        public SoleProprietorData SoleProprietorData { get; set; }
    }
}
