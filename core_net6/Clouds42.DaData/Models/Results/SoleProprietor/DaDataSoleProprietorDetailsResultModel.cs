﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.SoleProprietor
{
    /// <summary>
    /// Модель результата запроса на сервис DaData
    /// для получения реквизитов ИП 
    /// </summary>
    public class DaDataSoleProprietorDetailsResultModel
    {
        /// <summary>
        /// Предложенная информация
        /// </summary>
        [JsonProperty("suggestions")]
        public List<SuggestionBySoleProprietor> Suggestions { get; set; }
    }
}
