﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.SoleProprietor
{
    /// <summary>
    /// Имя владельца ИП
    /// </summary>
    public class NameOfSoleProprietorOwner
    {
        /// <summary>
        /// ФИО
        /// </summary>
        [JsonProperty("full")]
        public string FullName { get; set; }
    }
}
