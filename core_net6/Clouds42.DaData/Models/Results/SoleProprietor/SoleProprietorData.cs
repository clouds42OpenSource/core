﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.SoleProprietor
{
    /// <summary>
    /// Данные ИП
    /// </summary>
    public class SoleProprietorData
    {
        /// <summary>
        /// ИНН
        /// </summary>
        [JsonProperty("inn")]
        public string Inn { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        [JsonProperty("ogrn")]
        public string Ogrn { get; set; }

        /// <summary>
        /// Имя владельца ИП
        /// </summary>
        [JsonProperty("name")]
        public NameOfSoleProprietorOwner NameOfSoleProprietorOwner { get; set; }

        [JsonProperty("address")]
        public Address Address { get; set; }

    }
}
