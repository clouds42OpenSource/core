﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.Bank
{
    /// <summary>
    /// Модель результата запроса на сервис DaData
    /// для получения реквизитов банка 
    /// </summary>
    public class DaDataBankDetailsResultModel
    {
        /// <summary>
        /// Предложенная информация
        /// </summary>
        [JsonProperty("suggestions")]
        public List<SuggestionByBank> Suggestions { get; set; }
    }
}
