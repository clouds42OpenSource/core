﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.Bank
{
    /// <summary>
    /// Адрес банка
    /// </summary>
    public class BankAdress
    {
        [JsonProperty("unrestricted_value")]
        public string Adress { get; set; }
    }
}
