﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.Bank
{
    /// <summary>
    /// Банковские данные
    /// </summary>
    public class BankData
    {
        /// <summary>
        /// Бик банка
        /// </summary>
        [JsonProperty("bic")]
        public string Bik { get; set; }

        /// <summary>
        /// Свифт банка
        /// </summary>
        [JsonProperty("swift")]
        public string Swift { get; set; }

        /// <summary>
        /// Корреспондентский счет
        /// </summary>
        [JsonProperty("correspondent_account")]
        public string CorrespondentAccount { get; set; }

        /// <summary>
        /// Регистрационный номер
        /// </summary>
        [JsonProperty("registration_number")]
        public string RegistrationNumber { get; set; }

        /// <summary>
        /// Адрес банка
        /// </summary>
        [JsonProperty("address")]
        public BankAdress BankAdress { get; set; }
    }
}
