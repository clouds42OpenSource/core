﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.Bank
{
    /// <summary>
    /// Предложенная информация по банку
    /// </summary>
    public class SuggestionByBank
    {
        /// <summary>
        /// Наименование банка
        /// </summary>
        [JsonProperty("value")]
        public string BankName { get; set; }

        /// <summary>
        /// Банковские данные
        /// </summary>
        [JsonProperty("data")]
        public BankData BankData { get; set; }
    }
}
