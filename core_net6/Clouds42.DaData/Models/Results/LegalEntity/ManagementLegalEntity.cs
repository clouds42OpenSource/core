﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.LegalEntity
{
    /// <summary>
    /// Управление юр. лицом
    /// </summary>
    public class ManagementLegalEntity
    {
        /// <summary>
        /// ФИО руководителя
        /// </summary>
        [JsonProperty("name")]
        public string HeadFullName { get; set; }

        /// <summary>
        /// Должность руководителя
        /// </summary>
        [JsonProperty("post")]
        public string HeadPosition { get; set; }
    }
}
