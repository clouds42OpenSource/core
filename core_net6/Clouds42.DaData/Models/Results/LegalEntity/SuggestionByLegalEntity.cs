﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.LegalEntity
{
    /// <summary>
    /// Предложенная информация по юр. лицу
    /// </summary>
    public class SuggestionByLegalEntity
    {
        /// <summary>
        /// Наименование компании
        /// </summary>
        [JsonProperty("value")]
        public string CompanyName { get; set; }

        /// <summary>
        /// Данные юр. лица
        /// </summary>
        [JsonProperty("data")]
        public LegalEntityData LegalEntityData { get; set; }
    }
}
