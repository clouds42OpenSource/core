﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.LegalEntity
{
    /// <summary>
    /// Данные юр. лица
    /// </summary>
    public class LegalEntityData
    {
        /// <summary>
        /// ИНН
        /// </summary>
        [JsonProperty("inn")]
        public string Inn { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        [JsonProperty("ogrn")]
        public string Ogrn { get; set; }

        /// <summary>
        /// КПП
        /// </summary>
        [JsonProperty("kpp")]
        public string Kpp { get; set; }

        /// <summary>
        /// Имя владельца ИП
        /// </summary>
        [JsonProperty("management")]
        public ManagementLegalEntity ManagementLegalEntity { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        [JsonProperty("address")]
        public Address Address { get; set; }
    }
}
