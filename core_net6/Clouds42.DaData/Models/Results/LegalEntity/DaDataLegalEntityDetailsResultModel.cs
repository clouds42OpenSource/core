﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results.LegalEntity
{
    /// <summary>
    /// Модель результата запроса на сервис DaData
    /// для получения реквизитов юр. лица
    /// </summary>
    public class DaDataLegalEntityDetailsResultModel
    {
        /// <summary>
        /// Предложенная информация
        /// </summary>
        [JsonProperty("suggestions")]
        public List<SuggestionByLegalEntity> Suggestions { get; set; }
    }
}
