﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results
{
    /// <summary>
    /// Данные об адресе
    /// </summary>
    public class AddressData
    {
        /// <summary>
        /// Почтовый индекс
        /// </summary>
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        [JsonProperty("region_with_type")]
        public string Region { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        [JsonProperty("city_with_type")]
        public string City { get; set; }
    }
}
