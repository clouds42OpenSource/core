﻿using Newtonsoft.Json;

namespace Clouds42.DaData.Models.Results
{
    /// <summary>
    /// Адресс
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Полный адрес
        /// </summary>
        [JsonProperty("value")]
        public string FullAdress { get; set; }

        /// <summary>
        /// Данные об адресе
        /// </summary>
        [JsonProperty("data")]
        public AddressData AddressData { get; set; }
    }
}
