﻿namespace Clouds42.DaData.Models
{
    /// <summary>
    /// Конфигурация для сервиса получения реквизитов
    /// </summary>
    public class DaDataConfiguration
    {
        /// <summary>
        /// Урл
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Токен
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Ключ
        /// </summary>
        public string Key { get; set; }
    }
}
