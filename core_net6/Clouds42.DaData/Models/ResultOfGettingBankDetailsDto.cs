﻿namespace Clouds42.DaData.Models
{
    /// <summary>
    /// Результат получения реквизитов банка
    /// </summary>
    public class ResultOfGettingBankDetailsDto : BaseResultOfGettingDetails
    {
        /// <summary>
        /// Наименование банка
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// Бик банка
        /// </summary>
        public string Bik { get; set; }

        /// <summary>
        /// Свифт банка
        /// </summary>
        public string Swift { get; set; }

        /// <summary>
        /// Корреспондентский счет
        /// </summary>
        public string CorrespondentAccount { get; set; }

        /// <summary>
        /// Регистрационный номер
        /// </summary>
        public string RegistrationNumber { get; set; }

        /// <summary>
        /// Адрес банка
        /// </summary>
        public string BankAdress { get; set; }
    }
}
