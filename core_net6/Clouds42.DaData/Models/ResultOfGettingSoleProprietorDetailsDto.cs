﻿namespace Clouds42.DaData.Models
{
    /// <summary>
    /// Результат получения реквизитов ИП
    /// </summary>
    public class ResultOfGettingSoleProprietorDetailsDto : BaseResultOfGettingDetails
    {
        /// <summary>
        /// Наименование ИП
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        public string Ogrn { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }
    }
}
