﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Clouds42.Jwt.Contracts;
using Clouds42.Jwt.Contracts.Settings;
using Clouds42.Jwt.Contracts.Simple;
using Microsoft.IdentityModel.Tokens;

namespace Clouds42.Jwt
{
    /// <summary>
    /// Объект для работы с JWT
    /// </summary>
    public class JwtWorker(IJwtHelper jwtHelper) : IJwtWorker
    {
        /// <summary>
        /// Метод для создания Json Web Token
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="claims">Claims которые нужно добавить в Json Web Token</param>
        /// <returns>Возвращает Json Web Token и Refresh Token</returns>
        public async Task<JwtInfo> CreateJsonWebTokenAsync(IEnumerable<Claim> claims, CreateJwtSettings settings)
        {
            var issuer = settings.Issuer;
            var audience = settings.Audience;
            var expires = settings.TimeToLive;

            var secretKey = await jwtHelper.GetRsaCryptoProviderWithSettings();

            var token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.Add(expires),
                signingCredentials: new SigningCredentials(new RsaSecurityKey(secretKey), SecurityAlgorithms.RsaSha256){ CryptoProviderFactory = new CryptoProviderFactory { CacheSignatureProviders = false } }
            );

            token.Payload.Add("iat", token.Payload.Nbf);

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
            var refreshToken = GenerateRefreshToken();

            return new JwtInfo
            {
                JsonWebToken = jwtToken,
                RefreshToken = refreshToken,
                ExpiresIn = (int)expires.TotalSeconds
            };
        }

        /// <summary>
        /// Генерирует refresh токен
        /// </summary>
        public byte[] GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomNumber);
            return randomNumber;
        }

        /// <summary>
        /// Получает claims из Json Web Token
        /// </summary>
        /// <param name="token">Json Web Token из которого получить claims</param>
        public async Task<ClaimsPrincipal> GetPrincipalFromExpiredJsonWebTokenAsync(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, await jwtHelper.GeTokenValidationParameters(validateLifeTime: false), out var securityToken);
            
            if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.RsaSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        /// <summary>
        /// Проверяет на валидность Json Web Token и возвращает claims
        /// </summary>
        /// <param name="jsonWebToken">Json Web Token который проверить на валидность и получить claims</param>

        public async Task<ClaimsPrincipal> ValidateJsonWebTokenAsync(string jsonWebToken)
        {
            var handler = new JwtSecurityTokenHandler();

            var claimsPrincipal = handler.ValidateToken(jsonWebToken, await jwtHelper.GeTokenValidationParameters(), out _);

            return claimsPrincipal;
        }

        
        [Obsolete("Данный метод устарел. Использовать временно для рефреша токена у старых пользователей")]
        public ClaimsPrincipal GetPrincipalFromExpiredJsonWebTokenObsolete(string token, GetClaimsFromJwtSettings settings)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(settings.SigningKeyBase64)),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var securityToken);

            if (securityToken is not JwtSecurityToken jwtSecurityToken ||
                !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }
    }
}
