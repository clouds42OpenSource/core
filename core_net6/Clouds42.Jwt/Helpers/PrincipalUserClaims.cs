﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Clouds42.Jwt.Helpers
{
    /// <summary>
    /// Доступные claims
    /// </summary>
    public static class PrincipalUserClaims
    {
        /// <summary>
        /// Claim для того что бы держать имя пользователя
        /// </summary>
        public static string UsernameClaim => ClaimTypes.Name;

        /// <summary>
        /// Claim для того что бы держать id пользователя
        /// </summary>
        public static string UserIdClaim => ClaimTypes.NameIdentifier;

        /// <summary>
        /// Claim для того что бы держать id токена
        /// </summary>
        public static string TokenIdClaim => "tkn";

        /// <summary>
        /// Claim для того что бы держать email пользователя
        /// </summary>
        public static string EmailClaim => ClaimTypes.Email;
        
        /// <summary>
        /// Claim для того что бы держать роль пользователя
        /// </summary>
        public static string RoleClaim => ClaimTypes.Role;

        /// <summary>
        /// Claim для того что бы держать дату создания
        /// </summary>
        public static string JwtIssueDateClaim => JwtRegisteredClaimNames.Nbf;
    }
}