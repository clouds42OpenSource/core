﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Clouds42.Configurations.Configurations;
using Clouds42.Jwt.Contracts;
using Clouds42.Logger;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Clouds42.Jwt.Helpers
{
    public class JwtHelper(IMemoryCache memoryCache, IHttpClientFactory httpClientFactory, ILogger42 logger)
        : IJwtHelper
    {
        public async Task<RSACryptoServiceProvider> GetRsaCryptoProviderWithSettings()
        {
            const string cacheKey = "jwt_keys";
            var urlForGetJwtSettings = CloudConfigurationProvider.JsonWebToken.GetUrlForGetJwtSettingsFromMsProvider();
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

            try
            {
                if (memoryCache.TryGetValue(cacheKey, out JwtSettingsDto dto))
                {
                    rsa.ImportParameters(
                        new RSAParameters
                        {
                            Modulus = FromBase64Url(dto.Mod), Exponent = FromBase64Url(dto.Exp)
                        });

                    return rsa;
                }

                using var client = httpClientFactory.CreateClient();
                
                var response = await client.GetAsync(new Uri(urlForGetJwtSettings));
                
                if (response is not { IsSuccessStatusCode: true })
                    throw new InvalidOperationException("Провайдер МС вернул не валидный статус при получение настроек jwt");

                var data = await response.Content.ReadAsStringAsync();

                var jwtSettings = JsonConvert.DeserializeObject<JwtResponseDto>(data);

                if (!jwtSettings.Keys.Any())
                    throw new InvalidOperationException("Провайдер МС вернул пустую коллекцию настроек jwt");

                var key = jwtSettings.Keys.FirstOrDefault(x => x.Alg == "RSA") ?? throw new InvalidOperationException("Не удалось распознать настройку jwt");
                memoryCache.Set(cacheKey, key);

                rsa.ImportParameters(
                    new RSAParameters
                    {
                        Modulus = FromBase64Url(key.Mod), Exponent = FromBase64Url(key.Exp)
                    });

                return rsa;
            }

            catch (Exception e)
            {
                logger.Error(e, $"Ошибка при инициализации аутентификации через провайдера {urlForGetJwtSettings}. Берем значение из базы");

                var defaultKeys = CloudConfigurationProvider.JsonWebToken.GetDefaultJwtKeysFromDb();
                var jwtSettings = JsonConvert.DeserializeObject<JwtSettingsDto>(defaultKeys);

                rsa.ImportParameters(
                    new RSAParameters
                    {
                        Modulus = FromBase64Url(jwtSettings.Mod),
                        Exponent = FromBase64Url(jwtSettings.Exp)
                    });

                return rsa;

            }
        }

        public async Task<TokenValidationParameters> GeTokenValidationParameters(bool validateLifeTime = true)
        {
            var issuer = CloudConfigurationProvider.JsonWebToken.GetIssuer();
            var audience = CloudConfigurationProvider.JsonWebToken.GetAudienceAny();

            var rsa = await GetRsaCryptoProviderWithSettings();
            
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = issuer,
                ValidateAudience = false,
                ValidAudience = audience,
                ValidateLifetime = validateLifeTime,
                ClockSkew = TimeSpan.FromMinutes(5),
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new RsaSecurityKey(rsa)
            };
        }

        private static byte[] FromBase64Url(string base64Url)
        {
            string padded = base64Url.Length % 4 == 0
                ? base64Url : base64Url + "===="[(base64Url.Length % 4)..];
            string base64 = padded.Replace("_", "/")
                .Replace("-", "+");

            return Convert.FromBase64String(base64);
        }
    }

    public class JwtResponseDto
    {
        public List<JwtSettingsDto> Keys { get; set; }
    }

    public class JwtSettingsDto
    {
        public string Alg { get; set; }
        public string Kid { get; set; }
        public string Exp { get; set; }
        public string Mod { get; set; }
    }
}
