﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.JsonWebToken;
using Clouds42.Identity.Contracts;
using Clouds42.Jwt.Contracts;
using Clouds42.Jwt.Contracts.Simple;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Clouds42.Jwt.JwtProvider
{
    /// <summary>
    /// Провайдер для работы с JWT
    /// </summary>
    public sealed class JwtProvider : IJwtProvider
    {
        private readonly IUnitOfWork _unitOfWork;
        
        /// <summary>
        /// Объект для работы с jwt
        /// </summary>
        private readonly IJwtWorker _jwtWorker;

        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger42 _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="jwtWorker"></param>
        /// <param name="logger"></param>
        /// <param name="factory"></param>
        public JwtProvider(IUnitOfWork unitOfWork, IJwtWorker jwtWorker, ILogger42 logger, IHttpClientFactory factory)
        {
            _unitOfWork = unitOfWork;
            _jwtWorker = jwtWorker;
            _logger = logger;
            _httpClientFactory = factory;
        }

        /// <summary>
        /// Метод для создания Json Web Token
        /// </summary>
        /// <param name="password"></param>
        /// <param name="audience">Строковое значение аудитории</param>
        /// <param name="user">Для кого создать Json Web Token</param>
        /// <param name="login"></param>
        /// <returns>Возвращает JsonWebToken и RefreshToken</returns>
        public async Task<JwtInfo> CreateJsonWebTokenFor(string login, string password, string audience, ApplicationUser user)
        {
            JwtInfo jwtInfo;

            var jwtEntity = new AccountUserJsonWebToken
            {
                AccessToken = [],
                RefreshToken = [],
                AccountUserId = user.Id
            };
            
            var jwtRepository = _unitOfWork.GetGenericRepository<AccountUserJsonWebToken>();
            
            try
            {
                jwtRepository.Insert(jwtEntity);
                await _unitOfWork.SaveAsync();
                    
                jwtInfo = await CreateJsonWebTokenAsync(login, password);
                jwtEntity.AccessToken = Encoding.UTF8.GetBytes(jwtInfo.JsonWebToken);
                jwtEntity.RefreshToken = jwtInfo.RefreshToken;
                jwtRepository.Update(jwtEntity);
                await _unitOfWork.SaveAsync();

                _logger.Info("Создание и запись в базу данных пары токенов(JsonWebToken и RefreshToken) завершилось успешно");
            }

            catch (Exception ex)
            {
                _logger.Error(ex,$"Создание и запись в базу данных пары токенов(JsonWebToken и RefreshToken) завершилось с ошибкой");

                throw;
            }

            return jwtInfo;
        }
        

        /// <summary>
        /// Получить <see cref="ClaimsPrincipal"/> identity из JWT токена
        /// </summary>
        /// <param name="jsonWebToken">JWT токен</param>
        /// <returns><see cref="ClaimsPrincipal"/> identity</returns>
        private async Task<ClaimsPrincipal> GetClaimsPrincipalFromJsonWebToken(string jsonWebToken)
        {
            try
            {
                return await _jwtWorker.GetPrincipalFromExpiredJsonWebTokenAsync(jsonWebToken);
            }

            catch (Exception exception)
            {
                _logger.Warn(exception, $"Не валиднный JsonWebToken: {jsonWebToken}");

                return null;
            }
        }

        /// <summary>
        /// Получить доменную сущность JWT токен из БД
        /// </summary>
        /// <param name="userIdClaim">ID токена</param>
        /// <param name="jsonWebToken">Токен</param>
        /// <returns>Доменная сущность JWT токена</returns>
        private async Task<AccountUserJsonWebToken> GetJsonWebTokenFromDatabase(Claim userIdClaim, string jsonWebToken)
        {
            if (userIdClaim == null || !Guid.TryParse(userIdClaim.Value, out var userId))
            {
                _logger.Warn("Не валиднный токен id.");

                return null;
            }

            var jwtToken = await GetJsonWebTokenById(userId);

            var cloudService = await _unitOfWork.CloudServiceRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.AccountUserId == userId);

            if (jwtToken == null && cloudService == null)
            {
                _logger.Warn($"В базе JsonWebToken:({jsonWebToken}) не найден.");

                return null;
            }

            var jwtFromDb = Encoding.UTF8.GetString(jwtToken?.AccessToken ?? cloudService?.JsonWebToken);

            if (jwtFromDb == jsonWebToken)
            {
                return jwtToken ?? new AccountUserJsonWebToken{ AccessToken = cloudService.JsonWebToken, AccountUserId = cloudService.AccountUserId!.Value, RefreshToken = Encoding.UTF8.GetBytes(cloudService.RefreshToken)};
            }

            _logger.Warn($"В базе JsonWebToken:({jsonWebToken}) не найден.");
            
            return null;
        }

        /// <summary>
        /// Получить JWT токен по ID пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>JWT токен</returns>
        private async Task<AccountUserJsonWebToken> GetJsonWebTokenById(Guid accountUserId)
        {
            return await _unitOfWork.GetGenericRepository<AccountUserJsonWebToken>()
                .AsQueryable()
                .OrderByDescending(x => x.Created)
                .FirstOrDefaultAsync(x => x.AccountUserId == accountUserId &&
                                          !x.HasBeenTakenRefreshToken &&
                                          (!x.AccountUser.DateTimeOfLastChangePasswordOrLogin.HasValue ||
                                           x.Created > x.AccountUser.DateTimeOfLastChangePasswordOrLogin));
        }

        /// <summary>
        /// Метод для создания нового токена JsonWebToken по токену RefreshToken
        /// </summary>
        /// <param name="jsonWebToken">Токен который соответствует RefreshToken</param>
        /// <param name="refreshToken">Токен с помощью которого обновить</param>
        /// <returns>Возвращает JsonWebToken и RefreshToken</returns>
        public async Task<JwtInfo> RefreshJsonWebToken(string jsonWebToken, string refreshToken)
        {
            try
            {
                var claimsPrincipal = await GetClaimsPrincipalFromJsonWebToken(jsonWebToken);

                if (claimsPrincipal == null)
                {
                    return null;
                }

                var tokenIdClaim = claimsPrincipal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier) ?? claimsPrincipal.Claims.FirstOrDefault(claim => claim.Type == "sub");

                var jwtTokenFromDb = await GetJsonWebTokenFromDatabase(tokenIdClaim, jsonWebToken);

                if (jwtTokenFromDb == null)
                {
                    return null;
                }
               
                var providerUrl = CloudConfigurationProvider.OpenId.Sauri.GetUrlOidProvider();
                var encodedJwt = Encoding.UTF8.GetString(Convert.FromBase64String(refreshToken));

                var formData = new Dictionary<string, string>
                {
                    ["openid.42clouds.jwt"] = "true",
                    ["openid.42clouds.refresh_token"] = encodedJwt
                };

                using var client = _httpClientFactory.CreateClient();

                var requestUri = new Uri(new Uri(providerUrl), "?cmd=check");
                var response = await client.PostAsync(requestUri,
                    new FormUrlEncodedContent(formData));

                var stringData = string.Join("\n", formData.Select(x => $"{x.Key}={x.Value}"));

                _logger.Info($"Request to {requestUri.AbsoluteUri} with data {stringData}");

                var responseString = await response?.Content?.ReadAsStringAsync();

                _logger.Info($"Response from {requestUri.AbsoluteUri}:\nStatusCode= {response?.StatusCode} \nData= {responseString}");

                if (response is not { IsSuccessStatusCode: true })
                    throw new InvalidOperationException("Не удалось обновить jwt-токен по refresh-токену у провайдера. Обратитесь в техподдержку");

                var jwtDto = JsonConvert.DeserializeObject<OpenIdAuthResponseDto>(responseString);

                var refreshBytes = Encoding.UTF8.GetBytes(jwtDto.RefreshToken);

                var newJwtToken = new AccountUserJsonWebToken
                {
                    AccessToken = Encoding.UTF8.GetBytes(jwtDto.AccessToken),
                    RefreshToken = refreshBytes,
                    AccountUserId = jwtTokenFromDb.AccountUserId
                };

                var jwtRepository = _unitOfWork.GetGenericRepository<AccountUserJsonWebToken>();

                jwtRepository.Insert(newJwtToken);
                await _unitOfWork.SaveAsync();
                
                _logger.Info("Обновление пары токенов(JsonWebToken и RefreshToken) завершилось успешно");

                return new JwtInfo { ExpiresIn = jwtDto.ExpiresIn, RefreshToken = refreshBytes, JsonWebToken = jwtDto.AccessToken };
            }

            catch (Exception ex)
            {
                _logger.Error(ex, $"Обновление пары токенов(JsonWebToken и RefreshToken) завершилось с ошибкой");

                return null;
            }
        }

        /// <summary>
        /// Проверить валидность текущего токена
        /// </summary>
        /// <param name="jsonWebToken">Токен который соответствует RefreshToken</param>
        /// <param name="refreshToken">Токен с помощью которого обновить</param>
        /// <returns>true - если токен есть и не просрочен</returns>
        public async Task<bool> CheckCurrentTokenValidity(string jsonWebToken, string refreshToken)
        {
            var claimsPrincipal = await GetClaimsPrincipalFromJsonWebToken(jsonWebToken);

            if (claimsPrincipal == null)
                return false;

            var tokenIdClaim = claimsPrincipal.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier);

            return await CheckCurrentTokenValidity(tokenIdClaim, jsonWebToken);
        }

        /// <summary>
        /// Проверить валидность текущего токена
        /// </summary>
        /// <param name="tokenIdClaim">ID токена</param>
        /// <param name="jsonWebToken">Токен который соответствует RefreshToken</param>
        /// <returns>true - если токен есть и не просрочен</returns>
        private async Task<bool> CheckCurrentTokenValidity(Claim tokenIdClaim, string jsonWebToken)
        {
            var jwtTokenFromDb = await GetJsonWebTokenFromDatabase(tokenIdClaim, jsonWebToken);

            return jwtTokenFromDb != null;
        }


        /// <summary>
        /// Метод для создания JsonWebToken
        /// </summary>
        /// <param name="password"></param>
        /// <param name="login"></param>
        /// <returns>Возвращает JsonWebToken и RefreshToken</returns>
        private async Task<JwtInfo> CreateJsonWebTokenAsync(string login, string password)
        {
            var msAuthUrl = CloudConfigurationProvider.OpenId.Sauri.GetUrlOidProvider();

            using var client = _httpClientFactory.CreateClient();

            var formData = new Dictionary<string, string>
            {
                ["openid.auth.user"] = login,
                ["openid.auth.pwd"] = password,
                ["openid.auth.short"] = "false",
                ["openid.auth.check"] = "true",
                ["openid.42clouds.jwt"] = "true",
                ["openid.42clouds.jwt_audience"] = "CP",
                ["openid.42clouds.jwt_scope"] = "core"
            };

            var requestUri = new Uri(new Uri(msAuthUrl), "?cmd=auth");
            var response = await client.PostAsync(requestUri, new FormUrlEncodedContent(formData));

            var stringData = string.Join("\n", formData.Select(x => $"{x.Key}={x.Value}"));

            _logger.Info($"Request to {requestUri.AbsoluteUri} with data {stringData}");

            var responseString = await response?.Content?.ReadAsStringAsync();

            _logger.Info($"Response from {requestUri.AbsoluteUri}:\nStatusCode= {response?.StatusCode} \nData= {responseString}");

            if (response is not { IsSuccessStatusCode: true }) 
                throw new InvalidOperationException("Не удалось получить ответ oidc провайдера. Обратитесь в техподдержку");


            var jwtDto = JsonConvert.DeserializeObject<OpenIdAuthResponseDto>(responseString);

            return new JwtInfo
            {
                ExpiresIn = jwtDto.ExpiresIn,
                JsonWebToken = jwtDto.AccessToken,
                RefreshToken = Encoding.UTF8.GetBytes(jwtDto.RefreshToken)
            };
        }
    }
}
