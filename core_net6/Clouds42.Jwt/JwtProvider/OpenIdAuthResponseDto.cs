﻿using Newtonsoft.Json;

namespace Clouds42.Jwt.JwtProvider;

/// <summary>
/// Модель ответа oidc провайдера
/// </summary>
public class OpenIdAuthResponseDto
{
    /// <summary>
    /// Тип токена, например bearer
    /// </summary>
    [JsonProperty(PropertyName = "token_type")]
    public string TokenType { get; set; }

    /// <summary>
    /// JWT токен
    /// </summary>
    [JsonProperty(PropertyName = "access_token")]
    public string AccessToken { get; set; }

    /// <summary>
    /// Refresh токен
    /// </summary>
    [JsonProperty(PropertyName = "refresh_token")]
    public string RefreshToken { get; set; }

    /// <summary>
    /// Время жизни в секундах
    /// </summary>
    /// 
    [JsonProperty(PropertyName = "expires_in")]
    public int ExpiresIn { get; set; }
}
