﻿using Clouds42.Jwt.Contracts;
using Clouds42.Jwt.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Jwt
{
    public static class DependencyInjectionsExtensions
    {
        public static IServiceCollection AddJwt(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IJwtHelper, JwtHelper>();
            serviceCollection.AddScoped<IJwtWorker, JwtWorker>();
            serviceCollection.AddScoped<IJwtProvider, JwtProvider.JwtProvider>();

            return serviceCollection;
        }
    }
}
