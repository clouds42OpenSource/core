# Get the directory where the script is located
$rootPath = Split-Path -Path $MyInvocation.MyCommand.Path -Parent

# Get all 'bin' and 'obj' directories in the specified path
$directoriesToDelete = Get-ChildItem -Path $rootPath -Recurse -Directory -Force |
    Where-Object { $_.Name -eq 'bin' -or $_.Name -eq 'obj' }

# Remove the directories
foreach ($dir in $directoriesToDelete) {
    Remove-Item -Path $dir.FullName -Recurse -Force -ErrorAction SilentlyContinue
    Write-Host "Deleted: $($dir.FullName)"
}

Write-Host "Cleanup completed."