﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUser
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.EditUserInAdJob)]
    public class EditUserInAdJob(IUnitOfWork dbLayer, IActiveDirectoryProvider activeDirectoryProvider)
        : CoreWorkerParamsJob<EditUserInAdParamsModel>(dbLayer)
    {
        protected override void Execute(EditUserInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            activeDirectoryProvider.EditUser(jobParams.AccountUserId, jobParams.OldLogin, jobParams);
        }
    }
}
