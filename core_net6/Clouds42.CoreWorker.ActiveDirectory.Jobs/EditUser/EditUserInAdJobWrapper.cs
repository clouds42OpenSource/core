﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUser
{
    public class EditUserInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<EditUserInAdJob, EditUserInAdParamsModel>(registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<EditUserInAdJob, EditUserInAdParamsModel> Start(EditUserInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Редактирование пользователя '{paramsJob.UserName}' в домене.");
            return this;
        }
    }
}
