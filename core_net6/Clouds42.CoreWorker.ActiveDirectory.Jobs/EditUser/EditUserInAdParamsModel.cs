﻿using System;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUser
{
    public class EditUserInAdParamsModel : 
        IPrincipalUserAdapterDto
    {
        
        public Guid AccountUserId { get; set; }

        public string OldLogin { get; set; }

        public string UserName { get; set; }

        public string GetUserName()
            => UserName;

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }

        public string Email { get; set; }
    }
}
