﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewAccount
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RegisterNewAccountInAdJob)]
    public class RegisterNewAccountInAdJob(
        IUnitOfWork dbLayer,
        IActiveDirectoryProvider activeDirectoryProvider)
        : CoreWorkerParamsJob<CreateNewAccountInAdParamsModel>(dbLayer)
    {
        protected override void Execute(CreateNewAccountInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            activeDirectoryProvider.RegisterNewAccount(jobParams);
        }
    }
}
