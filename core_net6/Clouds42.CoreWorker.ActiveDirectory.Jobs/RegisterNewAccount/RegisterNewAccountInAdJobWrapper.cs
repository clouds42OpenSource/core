﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewAccount
{
    public class RegisterNewAccountInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<RegisterNewAccountInAdJob, CreateNewAccountInAdParamsModel>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<RegisterNewAccountInAdJob, CreateNewAccountInAdParamsModel> Start(CreateNewAccountInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Регистрация аккаунта '{paramsJob.AccountId}' в домене.");
            return this;
        }

    }
}
