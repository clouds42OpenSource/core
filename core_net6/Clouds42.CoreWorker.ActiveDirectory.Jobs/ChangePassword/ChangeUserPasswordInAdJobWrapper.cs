﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangePassword
{
    public class ChangeUserPasswordInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<ChangeUserPasswordInAdJob, ChangeUserPasswordInAdParamsModel>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<ChangeUserPasswordInAdJob, ChangeUserPasswordInAdParamsModel> Start(ChangeUserPasswordInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Смена пароля пользователя '{paramsJob.UserName}' в домене.");
            return this;
        }
    }
}
