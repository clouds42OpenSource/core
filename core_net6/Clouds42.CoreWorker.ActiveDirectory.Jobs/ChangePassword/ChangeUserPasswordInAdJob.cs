﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangePassword
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ChangeUserPasswordInAdJob)]
    public class ChangeUserPasswordInAdJob(IUnitOfWork dbLayer, IActiveDirectoryProvider activeDirectoryProvider)
        : CoreWorkerParamsJob<ChangeUserPasswordInAdParamsModel>(dbLayer)
    {
        protected override void Execute(ChangeUserPasswordInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            activeDirectoryProvider.ChangePassword(jobParams.AccountUserId, jobParams.UserName, jobParams.NewPassword);
        }
    }
}
