﻿using System;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangePassword
{
    public class ChangeUserPasswordInAdParamsModel
    {
        public Guid AccountUserId { get; set; }
        public string UserName { get; set; }
        public string NewPassword { get; set; }
    }
}