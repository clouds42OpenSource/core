﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.RemoveDirectoryAclForUser
{
    public class RemoveDirectoryAclForUserInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<RemoveDirectoryAclForUserInJob, RemoveDirectoryAclForUserInAdParamsModel>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<RemoveDirectoryAclForUserInJob, RemoveDirectoryAclForUserInAdParamsModel> Start(RemoveDirectoryAclForUserInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Удаление прав на папку '{string.Join(", ", paramsJob.StrPath)}' у пользователей '{string.Join(", ", paramsJob.UserName)}");
            return this;
        }
    }
}
