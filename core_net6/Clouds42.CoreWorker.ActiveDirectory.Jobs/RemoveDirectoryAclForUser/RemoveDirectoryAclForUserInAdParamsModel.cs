﻿namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.RemoveDirectoryAclForUser
{
    public class RemoveDirectoryAclForUserInAdParamsModel
    {
        public string UserName { get; set; }
        public string StrPath { get; set; }
    }
}
