﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.RemoveDirectoryAclForUser
{

    [CoreWorkerJobName(CoreWorkerTasksCatalog.RemoveDirectoryAclForUserInJob)]
    public class RemoveDirectoryAclForUserInJob(IUnitOfWork dbLayer, IActiveDirectoryProvider activeDirectoryProvider)
        : CoreWorkerParamsJob<RemoveDirectoryAclForUserInAdParamsModel>(dbLayer)
    {
        protected override void Execute(RemoveDirectoryAclForUserInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            activeDirectoryProvider.RemoveDirectoryAclForUser(jobParams.UserName, jobParams.StrPath);
        }
    }
}
