﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAcl
{
    public class SetDirectoryAclInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<SetDirectoryAclInAdJob, SetDirectoryAclInAdParamsModel>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<SetDirectoryAclInAdJob, SetDirectoryAclInAdParamsModel> Start(SetDirectoryAclInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Установка прав на папку '{string.Join(", ", paramsJob.StrPath)}' для группы '{string.Join(", ", paramsJob.GroupName)}");
            return this;
        }
    }
}
