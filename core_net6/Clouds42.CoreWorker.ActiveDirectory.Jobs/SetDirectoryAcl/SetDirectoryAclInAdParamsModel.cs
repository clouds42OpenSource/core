﻿namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAcl
{
    public class SetDirectoryAclInAdParamsModel
    {
        public string GroupName { get; set; }

        public string StrPath { get; set; }

    }
}