﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAcl
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.SetDirectoryAclInAdJob)]
    public class SetDirectoryAclInAdJob : CoreWorkerParamsJob<SetDirectoryAclInAdParamsModel>
    {
        private readonly IActiveDirectoryProvider _activeDirectoryProvider;
        public SetDirectoryAclInAdJob(IUnitOfWork dbLayer, IActiveDirectoryProvider activeDirectoryProvider) : base(dbLayer)
        {
            _activeDirectoryProvider = activeDirectoryProvider;
        }

        protected override void Execute(SetDirectoryAclInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            _activeDirectoryProvider.SetDirectoryAcl(jobParams.GroupName, jobParams.StrPath);
        }
        public SetDirectoryAclInAdJob(IUnitOfWork dbLayer) : base(dbLayer)
        {
        }
    }
}