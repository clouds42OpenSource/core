﻿using Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangeLogin;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangePassword;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.DeleteUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUserGroup;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewAccount;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RemoveDirectoryAclForUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAcl;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAclForUser;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddActiveDirectoryJobs(this IServiceCollection service)
        {
            service
                .AddTransient<ChangeUserLoginInAdJob>()
                .AddTransient<ChangeUserLoginInAdJobWrapper>()
                .AddTransient<ChangeUserPasswordInAdJob>()
                .AddTransient<ChangeUserPasswordInAdJobWrapper>()
                .AddTransient<DeleteUserInAdJob>()
                .AddTransient<DeleteUserInAdJobWrapper>()
                .AddTransient<EditUserInAdJob>()
                .AddTransient<EditUserInAdJobWrapper>()
                .AddTransient<EditUserGroupsInAdListInAdJob>()
                .AddTransient<EditUserGroupsInAdWrapper>()
                .AddTransient<RegisterNewAccountInAdJob>()
                .AddTransient<RegisterNewAccountInAdJobWrapper>()
                .AddTransient<RegisterNewUserInAdJob>()
                .AddTransient<RegisterNewUserInAdJobWrapper>()
                .AddTransient<RemoveDirectoryAclForUserInJob>()
                .AddTransient<RemoveDirectoryAclForUserInAdJobWrapper>()
                .AddTransient<SetDirectoryAclInAdJob>()
                .AddTransient<SetDirectoryAclInAdJobWrapper>()
                .AddTransient<SetDirectoryAclForUserInAdJob>()
                .AddTransient<SetDirectoryAclForUserInAdJobWrapper>();

            return service;
        }
    }
}
