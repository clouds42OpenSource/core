﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAclForUser
{

    [CoreWorkerJobName(CoreWorkerTasksCatalog.SetDirectoryAclForUserInAdJob)]
    public class SetDirectoryAclForUserInAdJob(IUnitOfWork dbLayer, IActiveDirectoryProvider activeDirectoryProvider)
        : CoreWorkerParamsJob<SetDirectoryAclForUserInAdParamsModel>(dbLayer)
    {
        protected override void Execute(SetDirectoryAclForUserInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            activeDirectoryProvider.SetDirectoryAclForUser(jobParams.UserName, jobParams.StrPath);
        }
    }

}
