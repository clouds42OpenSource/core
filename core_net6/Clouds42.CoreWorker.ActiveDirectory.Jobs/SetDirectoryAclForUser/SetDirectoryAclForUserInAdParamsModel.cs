﻿namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAclForUser
{
    public class SetDirectoryAclForUserInAdParamsModel
    {
        public string UserName { get; set; }
        public string StrPath { get; set; }
    }
}
