﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAclForUser
{
    public class SetDirectoryAclForUserInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<SetDirectoryAclForUserInAdJob, SetDirectoryAclForUserInAdParamsModel>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<SetDirectoryAclForUserInAdJob, SetDirectoryAclForUserInAdParamsModel> Start(SetDirectoryAclForUserInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Установка прав на папку '{string.Join(", ", paramsJob.StrPath)}' для пользователя '{string.Join(", ", paramsJob.UserName)}");
            return this;
        }
    }
}
