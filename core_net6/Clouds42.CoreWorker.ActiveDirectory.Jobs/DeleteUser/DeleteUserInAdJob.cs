﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.DeleteUser
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DeleteUserInAdJob)]
    public class DeleteUserInAdJob(IUnitOfWork dbLayer, IActiveDirectoryProvider activeDirectoryProvider)
        : CoreWorkerParamsJob<DeleteUserInAdParamsModel>(dbLayer)
    {
        protected override void Execute(DeleteUserInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            activeDirectoryProvider.DeleteUser(jobParams.AccountUserId, jobParams.UserName);
        }
    }
}
