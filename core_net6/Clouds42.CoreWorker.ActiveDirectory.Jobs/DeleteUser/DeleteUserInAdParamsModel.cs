﻿using System;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.DeleteUser
{
    public class DeleteUserInAdParamsModel
    {
        public Guid AccountUserId { get; set; }
        public string UserName { get; set; }
    }
}