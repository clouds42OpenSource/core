﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.DeleteUser
{
    public class DeleteUserInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<DeleteUserInAdJob, DeleteUserInAdParamsModel>(registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<DeleteUserInAdJob, DeleteUserInAdParamsModel> Start(DeleteUserInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Удаление пользователя '{paramsJob.UserName}' из домена.");
            return this;
        }
    }
}
