﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clouds42.DataContracts.ActiveDirectory.JobsParams;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUserGroup
{
    class EditUserGroupsInAdParamsModel
    {
        public List<EditUserGroupsAdModel> Items { get; set; } = new List<EditUserGroupsAdModel>();
    }
}
