﻿using System;
using System.Linq;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUserGroup
{
    public class EditUserGroupsInAdWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<EditUserGroupsInAdListInAdJob, EditUserGroupsAdListModel>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<EditUserGroupsInAdListInAdJob, EditUserGroupsAdListModel> Start(EditUserGroupsAdListModel paramsJob)
        {
            StartTask(paramsJob, $"Редактирование групп доcтупа у пользователей '{string.Join(", ", paramsJob.EditUserGroupsModel.SelectMany(s=>s.Items).Select(p=>p.UserName).Distinct().ToList())}''", DateTime.Now.AddMinutes(2));
            return this;
        }
    }
}
