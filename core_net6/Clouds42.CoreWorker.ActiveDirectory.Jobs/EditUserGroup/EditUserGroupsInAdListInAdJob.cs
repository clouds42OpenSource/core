﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUserGroup
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.EditUserGroupsInAdListInAdJob)]
    public class EditUserGroupsInAdListInAdJob(
        IUnitOfWork dbLayer,
        IActiveDirectoryProvider activeDirectoryProvider)
        : CoreWorkerParamsJob<EditUserGroupsAdListModel>(dbLayer)
    {
        protected override void Execute(EditUserGroupsAdListModel jobParams, Guid taskId, Guid taskQueueId)
        {
            activeDirectoryProvider.EditUserGroupsAdList(jobParams);
        }

    }
}
