﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewUser
{
    public class RegisterNewUserInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<RegisterNewUserInAdJob, RegisterNewUserInAdParamsModel>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<RegisterNewUserInAdJob, RegisterNewUserInAdParamsModel> Start(RegisterNewUserInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Регистрация пользователя '{paramsJob.Login}' в домене.");
            return this;
        }

    }
}
