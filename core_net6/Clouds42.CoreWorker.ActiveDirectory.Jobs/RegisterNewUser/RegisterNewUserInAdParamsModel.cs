﻿using System;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewUser
{
    public class RegisterNewUserInAdParamsModel : ICreateNewUserModel
    {
        public string Login { get; set; }

        public Guid AccountUserId { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Password { get; set; }

        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public Guid AccountId { get; set; }

        public Guid GetAccountUserId()
            => AccountUserId;

        public string GetUserName()
            => Login;

        public string ClientFilesPath { get; set; }
    }
}
