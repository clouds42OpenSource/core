﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewUser
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RegisterNewUserInAdJob)]
    public class RegisterNewUserInAdJob(
        IUnitOfWork dbLayer,
        IRegisterUserInAccountAdHelper registerUserInAccountAdHelper)
        : CoreWorkerParamsJob<RegisterNewUserInAdParamsModel>(dbLayer)
    {
        protected override void Execute(RegisterNewUserInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            registerUserInAccountAdHelper.RegisterUser(jobParams);
        }
    }

}
