﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangeLogin
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ChangeUserLoginInAdJob)]
    public class ChangeUserLoginInAdJob(IUnitOfWork dbLayer, IActiveDirectoryProvider activeDirectoryProvider)
        : CoreWorkerParamsJob<ChangeUserLoginInAdParamsModel>(dbLayer)
    {
        protected override void Execute(ChangeUserLoginInAdParamsModel jobParams, Guid taskId, Guid taskQueueId)
        {
            activeDirectoryProvider.ChangeLogin(jobParams.AccountUserId, jobParams.UserName, jobParams.NewLogin, jobParams.Email);
        }
    }
}
