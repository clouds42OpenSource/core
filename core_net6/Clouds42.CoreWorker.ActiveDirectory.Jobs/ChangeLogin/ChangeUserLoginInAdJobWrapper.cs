﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangeLogin
{
    public class ChangeUserLoginInAdJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<ChangeUserLoginInAdJob, ChangeUserLoginInAdParamsModel>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<ChangeUserLoginInAdJob, ChangeUserLoginInAdParamsModel> Start(ChangeUserLoginInAdParamsModel paramsJob)
        {
            StartTask(paramsJob, $"Смена логина пользователя '{paramsJob.UserName}' в домене.");
            return this;
        }
    }
}
