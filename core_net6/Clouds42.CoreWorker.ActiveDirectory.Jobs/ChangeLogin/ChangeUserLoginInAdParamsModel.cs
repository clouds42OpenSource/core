﻿using System;

namespace Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangeLogin
{
    public class ChangeUserLoginInAdParamsModel
    {
        public Guid AccountUserId { get; set; }
        public string UserName { get; set; }
        public string NewLogin { get; set; }
        public string Email { get; set; }
    }
}
