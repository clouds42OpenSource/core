﻿using System;
using Clouds42.CloudServices.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BillingJobs.MainServiceResourcesChanges
{
    /// <summary>
    /// Джоба для уведомления об изменении ресурсов главного сервиса (Аредна 1С)
    /// у вип аккаунтов
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.NotifyAboutMainServiceResourcesChangesJob)]
    public class NotifyAboutMainServiceResourcesChangesJob(
        IUnitOfWork dbLayer,
        INotifyAboutMainServiceResourcesChangesProvider notifyAboutMainServiceResourcesChangesProvider)
        : CoreWorkerParamsJob<
            NotifyAboutMainServiceResourcesChangesJobParamsDc>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(NotifyAboutMainServiceResourcesChangesJobParamsDc jobParams, Guid taskId,
            Guid taskQueueId) =>
            notifyAboutMainServiceResourcesChangesProvider.Notify(jobParams.ChangesDate);
    }
}