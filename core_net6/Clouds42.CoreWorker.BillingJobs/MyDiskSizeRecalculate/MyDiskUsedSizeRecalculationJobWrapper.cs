﻿using System;
using Clouds42.Common.Exceptions;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BillingJobs.MyDiskSizeRecalculate
{
    /// <summary>
    /// Обертка джобы пересчета занимаемого места
    /// </summary>
    public class MyDiskUsedSizeRecalculationJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<MyDiskUsedSizeRecalculationJob, RecalculateAccountMyDiskSizeDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Создать задачу по пересчету занимаемого места на диске аккаунта
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Обработчик задачи по пересчету занимаемого места</returns>
        public override TypingParamsJobWrapperBase<MyDiskUsedSizeRecalculationJob, RecalculateAccountMyDiskSizeDto> Start(RecalculateAccountMyDiskSizeDto paramsJob)
        {
            var account = GetAccountById(paramsJob.AccountId);
            StartTask(paramsJob, $"Пересчет занимаемого места на диске аккаунта - [{account.IndexNumber}] {account.AccountCaption}");
            return this;
        }

        /// <summary>
        /// Получить модель аккаунта по Id 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель аккаунта</returns>
        private Account GetAccountById(Guid accountId) =>
            dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId) ??
            throw new NotFoundException($"Не удалось найти аккаунт по Id = {accountId}");
    }
}
