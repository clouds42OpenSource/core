﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.BillingService;
using System;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BillingJobs.MyDiskSizeRecalculate
{
    /// <summary>
    /// Задача пересчета занимаемого места на диске
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.MyDiskUsedSizeRecalculationJob)]
    public class MyDiskUsedSizeRecalculationJob(
        IUnitOfWork dbLayer,
        IRecalculateMyDiskUsedSizeManager recalculateMyDiskUsedSizeManager)
        : CoreWorkerParamsJob<RecalculateAccountMyDiskSizeDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">Id задачи</param>
        /// <param name="taskQueueId">Id очереди задач воркера</param>
        protected override void Execute(RecalculateAccountMyDiskSizeDto jobParams, Guid taskId, Guid taskQueueId)
        {
            recalculateMyDiskUsedSizeManager.RecalculateMyDiskUsedSize(jobParams.AccountId);
        }
    }
}
