﻿using System;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BillingJobs.AutoPayJob
{
    /// <summary>
    /// Джоба по автооплате за день до истечения лицензии
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.AutoPayJob)]
    public class AutoPayJob(
        IUnitOfWork dbLayer,
        IInvoiceProvider invoiceProvider,
        IPayWithSavedPaymentMethodManager payWithSavedPaymentMethodManager)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="taskQueueId"></param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var resourceConfigurations = dbLayer.ResourceConfigurationRepository.GetSoonExpiredMainServices(beforeLockDays: 1);
            foreach (var resourceConfiguration in resourceConfigurations)
            {
                var autoPayMethod = dbLayer.AccountAutoPayConfigurationRepository.GetByAccountId(resourceConfiguration.AccountId);

                if (autoPayMethod == null)
                    continue;
                var invoice = invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(resourceConfiguration, resourceConfiguration.BillingAccounts);

                payWithSavedPaymentMethodManager.PayWithSavedPaymentMethodAsync(new PayWithSavedPaymentMethodDto
                {
                    AccountId = resourceConfiguration.AccountId,
                    PaymentMethodId = autoPayMethod.SavedPaymentMethodId,
                    TotalSum = invoice.Sum
                }).GetAwaiter().GetResult();
            }
        }
    }
}
