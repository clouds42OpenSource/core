﻿using System;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BillingJobs.FinalizeYokassaPayments
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.FinalizeYookassaPaymentsJob)]
    public class FinalizeYookassaPaymentsJob(
        IUnitOfWork unitOfWork,
        FinalizeYookassaPaymentJobWrapper finalizeYookassaPaymentJobWrapper)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить джобу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            var paymentsToFinalize = unitOfWork.PaymentRepository.GetPendingPaymentsByPaymentSystemAndLessSomeDate(PaymentSystem.Yookassa.ToString(), DateTime.Now);

            foreach (var payment in paymentsToFinalize)
                finalizeYookassaPaymentJobWrapper.Start(new FinalizeYookassaPaymentJobParamsDto
                {
                    YookassaAgregatorPaymentId = new Guid(payment.PaymentSystemTransactionId)
                });

        }
    }
}
