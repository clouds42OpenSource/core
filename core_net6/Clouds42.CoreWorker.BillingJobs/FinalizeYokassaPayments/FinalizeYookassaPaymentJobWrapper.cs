﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BillingJobs.FinalizeYokassaPayments
{
    /// <summary>
    /// Обработчик задачи по проверке статуса платежа агрегатора ЮKassa
    /// </summary>
    public class FinalizeYookassaPaymentJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<FinalizeYookassaPaymentJob,
            FinalizeYookassaPaymentJobParamsDto>(registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Прослойка выполнения</returns>
        public override TypingParamsJobWrapperBase<FinalizeYookassaPaymentJob, FinalizeYookassaPaymentJobParamsDto> Start(FinalizeYookassaPaymentJobParamsDto paramsJob)
        {
            StartTask(paramsJob, GetTaskComment(paramsJob));
            return this;
        }

        /// <summary>
        /// Получить комментарий для задачи
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Комментарий для задачи</returns>
        private string GetTaskComment(FinalizeYookassaPaymentJobParamsDto paramsJob) =>
            $"Проверка статуса платежа '{paramsJob.YookassaAgregatorPaymentId}' агрегатора ЮKassa";
    }
}
