﻿using System;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BillingJobs.FinalizeYokassaPayments
{
    /// <summary>
    /// Задача для проверки статуса платежа агрегатора ЮKassa 
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.FinalizeYookassaPaymentJob)]
    public class FinalizeYookassaPaymentJob(
        IUnitOfWork dbLayer,
        IFinalizeYookassaAggregatorPaymentManager finalizeYookassaAggregatorPaymentManager)
        : CoreWorkerParamsJobWithRetry<FinalizeYookassaPaymentJobParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры перезапуска задачи</returns>
        protected override RetryJobParamsDto ExecuteAndDetermineRetryNeed(FinalizeYookassaPaymentJobParamsDto jobParams, Guid taskId,
            Guid taskQueueId)
        {
            var managerResult =
                finalizeYookassaAggregatorPaymentManager.FinalizePayment(jobParams
                    .YookassaAgregatorPaymentId);

            return CreateRetryParams(taskQueueId, managerResult.Error, retryDelayInSeconds: 60, maxRetryAttemptsCount: 120);
        }
    }
}
