﻿using Clouds42.Common.Providers;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Managers
{
    public static class SettingsManager
    {       

        public static T Get<T>(this IMetaDataWebKey<T> y, IConfiguration configuration)
        {
            return ContextFactory.MakeWebConfigSettingProvider().Get(y, configuration);
        }
       
    }

    internal static class ContextFactory
    {
        public static WebConfigSettingProvider MakeWebConfigSettingProvider()
        {
            return new WebConfigSettingProvider();
        }

    }
}
