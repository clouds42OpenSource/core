﻿using System;
using System.Collections.Generic;
using Clouds42.Common.Models;

namespace Clouds42.Common.Providers
{

    public interface IMetaDataWebKey
    {
        string Key { get; }
    }

    public interface IMetaDataWebKey<T> : ITypedKey<IMetaDataWebKey, T>, IMetaDataWebKey
    {
    }

    public static class ConfigurationProvider
    {
        private static readonly IList<IMetaDataWebKey> Keys = new List<IMetaDataWebKey>();
        private static readonly KeyMaker Make = new();
        public static readonly IMetaDataWebKey<string> Ad_Mcob_AllGroups = Make.Required.Text("Ad.Mcob.AllGroups");

        private sealed class KeyMaker
        {
            private static readonly HashSet<string> KeyValues = [];

            private static IMetaDataWebKey<T> Register<T>(IMetaDataWebKey<T> settingKey)
            {
                string key = settingKey.Key;
                if (!KeyValues.Add(key))
                    throw new InvalidOperationException($"Дублирующийся ключ '{key}' в файле web.config");
                Keys.Add(settingKey);
                return settingKey;
            }

            private IMetaDataWebKey<T> Create<T>(string key, T defaultValue, Converter<T> converter)
            {
                return Register(new TypedMetaDataWebKey<T>(key, defaultValue, converter, _required, _cashed));
            }

            private readonly bool _required;
            private readonly bool _cashed;

            public KeyMaker()
                : this(false, false)
            {
            }

            private KeyMaker(bool required, bool cashed)
            {
                _required = required;
                _cashed = cashed;
            }

            public KeyMaker Required => new(true, _cashed);
            public IMetaDataWebKey<string> Text(string key, string defaultValue = null)
            {
                return Create(key, defaultValue, (string x, out string result) =>
                {
                    result = x;
                    return true;
                });
            }

        }
    }

    internal class TypedMetaDataWebKey<T>(
        string key,
        T defaultvalue,
        Converter<T> converter,
        bool required,
        bool cashed)
        : TypedKey<IMetaDataWebKey, T>(defaultvalue, converter, required, cashed), IMetaDataWebKey<T>
    {
        public string Key { get; } = key;

        public override IMetaDataWebKey UntypedKey => this;

        public override string ToString()
        {
            return Key;
        }
    }
}
