﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.Common.Providers
{
    /// <summary>
    /// Интерфейс провайдера для работы с Uri в пределах нескольких окружений и сред развёртывания.
    /// </summary>
    public interface ICoreUriProvider
    {
        /// <summary>
        /// Получить uri для хоста и окружения
        /// </summary>
        /// <param name="hostType">Хост</param>
        /// <param name="deployEnvironment">Окружение развёртывания</param>
        /// <returns>Uri</returns>
        Uri GetUri(HostTypes hostType, DeployEnvironments deployEnvironment);

        /// <summary>
        /// Получить uri для нового хоста при том же окружении развёртывания
        /// </summary>
        /// <param name="uri">Исходный адрес</param>
        /// <param name="newHostType">Новый хост</param>
        /// <returns>Новый адрес</returns>
        Uri GetUri(Uri uri, HostTypes newHostType);

        /// <summary>
        /// Получить uri для нового окружения развёртывания
        /// </summary>
        /// <param name="uri">Исходный адрес</param>
        /// <param name="newDeployEnvironment">Новая среда развёртывания</param>
        /// <returns>Новый адрес</returns>
        Uri GetUri(Uri uri, DeployEnvironments newDeployEnvironment);
    }
}