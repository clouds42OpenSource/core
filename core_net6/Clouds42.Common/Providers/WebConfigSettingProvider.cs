using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Providers
{

	public interface IWebConfigSettingProvider {
		T Get<T>( IMetaDataWebKey<T> key, IConfiguration configuration);		
	}

	public class WebConfigSettingProvider : SettingsProviderBase<IMetaDataWebKey>, IWebConfigSettingProvider {
		protected override string GetBase( IMetaDataWebKey key, IConfiguration configuration ) {
			return null;			
		}

		protected override string GetCurrent( IMetaDataWebKey key, IConfiguration configuration ) {

		    return configuration[key.Key];
		}

		public T Get<T>( IMetaDataWebKey<T> key, IConfiguration configuration) {
			return InnerGet( key, configuration);
		}
	}
}