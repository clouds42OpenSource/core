﻿using System;
using Clouds42.Common.Models;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Providers
{
	public abstract class SettingsProviderBase<TKey> {

		protected abstract string GetBase( TKey key, IConfiguration configuration);

		protected abstract string GetCurrent( TKey key, IConfiguration configuration);

		protected string GetString( TKey key, IConfiguration configuration) {
			return GetCurrent( key, configuration) ?? GetBase( key, configuration );
		}

        /// <summary>
        /// Внутренний метод получения настройки с учетом всех оверрайдов и значений по умолчанию
        /// Должен быть обернут в публичный метод в дочерних классах
        /// </summary>
        /// <typeparam name="TValue">Тип значения настройки</typeparam>
        /// <param name="key">Ключ настройки</param>
        /// <param name="configuration"></param>
        /// <returns>Значений настройки соответствующее ключу</returns>
        protected TValue InnerGet<TValue>(ITypedKey<TKey, TValue> key, IConfiguration configuration) {
			string stringResult = GetString( key.UntypedKey, configuration);
			if ( stringResult == null ) {
				if ( key.Required ) throw new ArgumentException($"Не задано значение ключа '{key}'");
				return key.Defaultvalue;
			}

            if ( !key.Convert( stringResult, out var result ) )
				throw new ArgumentException($"Некорректное значение по ключу '{key}': '{stringResult}'");
			return result;
		}
	}
}
