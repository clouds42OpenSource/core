﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;

namespace Clouds42.Common.Providers
{
    /// <inheritdoc />
    public class CoreUriProvider : ICoreUriProvider
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="hostTypeEnvToUriMap">Словарь соответствий набора хостов и окружений с адресами.</param>
        public CoreUriProvider(Dictionary<(HostTypes, DeployEnvironments), Uri> hostTypeEnvToUriMap)
        {
            _hostTypeEnvToUriMap = hostTypeEnvToUriMap;
            _uriToHostEnvMap = _hostTypeEnvToUriMap.Reverse();
        }

        private readonly Dictionary<(HostTypes, DeployEnvironments), Uri> _hostTypeEnvToUriMap;

        private readonly Dictionary<Uri, (HostTypes, DeployEnvironments)> _uriToHostEnvMap;

        /// <inheritdoc />
        [SuppressMessage("ReSharper", "ConvertIfStatementToReturnStatement")]
        public Uri GetUri(HostTypes hostType, DeployEnvironments deployEnvironment)
        {
            if (_hostTypeEnvToUriMap.TryGetValue((hostType, deployEnvironment), out var uri))
            {
                return uri;
            }

            throw new ArgumentException($"Невозможно найти Uri для указанного сочетания {nameof(hostType)}={hostType} и {nameof(deployEnvironment)}={deployEnvironment}");
        }

        /// <inheritdoc />
        public Uri GetUri(Uri uri, HostTypes newHostType)
        {
            var leftPart = uri.GetLeftPart(UriPartial.Authority);

            if (!_uriToHostEnvMap.TryGetValue(new Uri(leftPart), out var t))
            {
                throw new InvalidOperationException($"Не удалось определить {nameof(HostTypes)} и {nameof(DeployEnvironments)} по {nameof(Uri)}={leftPart}. Убедитесь, что в карте {nameof(_hostTypeEnvToUriMap)} есть соответствующая запись");
            }

            var (_, deployEnvironment) = t;

            if (_hostTypeEnvToUriMap.TryGetValue((newHostType, deployEnvironment), out var newUri))
            {
                return newUri;
            }

            throw new ArgumentException($"Невозможно найти Uri для указанного сочетания {nameof(uri)}={uri} и {nameof(newHostType)}={newHostType}");
        }

        /// <inheritdoc />
        public Uri GetUri(Uri uri, DeployEnvironments newDeployEnvironment)
        {
            var leftPart = uri.GetLeftPart(UriPartial.Authority);

            if (!_uriToHostEnvMap.TryGetValue(new Uri(leftPart), out var t))
            {
                throw new InvalidOperationException($"Не удалось определить {nameof(HostTypes)} и {nameof(DeployEnvironments)} по {nameof(Uri)}={leftPart}. Убедитесь, что в карте {nameof(_hostTypeEnvToUriMap)} есть соответствующая запись");
            }

            var (hostType, _) = t;

            if (_hostTypeEnvToUriMap.TryGetValue((hostType, newDeployEnvironment), out var newUri))
            {
                return newUri;
            }

            throw new ArgumentException($"Невозможно найти Uri для указанного сочетания {nameof(uri)}={uri} и {nameof(newDeployEnvironment)}={newDeployEnvironment}");
        }
    }
}
