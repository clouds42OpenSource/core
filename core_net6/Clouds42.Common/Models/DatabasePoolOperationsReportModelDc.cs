﻿using System;

namespace Clouds42.Common.Models
{
    /// <summary>
    /// Модель отчета о выполненных операциях с пулом
    /// </summary>
    public class DatabasePoolOperationsReportModelDc
    {
        /// <summary>
        /// Адрес ноды публикации
        /// </summary>
        public string NodeAddress { get; set; }
        
        /// <summary>
        /// Номер базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Время затраченное на получение состояния пула
        /// </summary>
        public double GetApplicationPoolStateTimeStamp { get; set; }

        /// <summary>
        /// Время затраченное на получение пути приложения
        /// </summary>
        public double GetApplicationPathTimeStamp { get; set; }

        /// <summary>
        /// Время затраченное на получение пула
        /// </summary>
        public double GetApplicationPoolTimeStamp { get; set; }

        /// <summary>
        /// Время затраченное на запуск пула
        /// </summary>
        public double StartApplicationPoolTimeStamp { get; set; }

        /// <summary>
        /// Время затраченное на остановку пула
        /// </summary>
        public double StopApplicationPoolTimeStamp { get; set; }

        /// <summary>
        /// Дата выполнения
        /// </summary>
        public DateTime OperationDateTime { get; set; }
    }
}
