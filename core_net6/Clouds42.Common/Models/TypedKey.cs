﻿namespace Clouds42.Common.Models {

	public delegate bool Converter<T>( string input, out T result );

	/// <summary>
	/// Интерфейс описывает абстракный ключ, по которому можно получить значение абстрактного типа
	/// </summary>
	/// <typeparam name="TKey">Тип ключа - </typeparam>
	/// <typeparam name="TValue">Тип значения, которое можно получить по ключу</typeparam>
	public interface ITypedKey<TKey, TValue> {
		TValue Defaultvalue { get; }
		Converter<TValue> Convert { get; }
		TKey UntypedKey { get; }
		bool Required { get; }
		bool Cashed { get; }
	}

	internal abstract class TypedKey<TKey, TValue>(
        TValue defaultvalue,
        Converter<TValue> convert,
        bool required,
        bool cashed)
        : ITypedKey<TKey, TValue>
    {
		public TValue Defaultvalue { get; } = defaultvalue;
        public Converter<TValue> Convert { get; } = convert;

        /// <summary>
		/// Следует реализовать в наследнике в виде "return this"
		/// </summary>
		public abstract TKey UntypedKey { get; }

		public bool Required { get; } = required;
        public bool Cashed { get; } = cashed;
    }
}
