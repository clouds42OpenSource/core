﻿using Clouds42.Domain.Enums;

namespace Clouds42.Common.Models
{
    /// <summary>
    /// Модель фиксации времени выполнения операции
    /// </summary>
    public class TraceRuntimeDataDc
    {
        /// <summary>
        /// Время выполнения в секундах
        /// </summary>
        public double ElapsedSeconds { get; set; }

        /// <summary>
        /// Операция
        /// </summary>
        public ExecutionTimeOperationsEnum ExecutionTimeOperation { get; set; }
    }
}
