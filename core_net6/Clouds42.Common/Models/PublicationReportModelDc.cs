﻿using System;

namespace Clouds42.Common.Models
{
    /// <summary>
    /// Модель отчета о публикации
    /// </summary>
    public class PublicationReportModelDc
    {
        /// <summary>
        /// Адрес ноды публикации 
        /// </summary>
        public string NodeAddress { get; set; }

        /// <summary>
        /// Время на создание пула
        /// </summary>
        public double CreatePoolTime { get; set; }

        /// <summary>
        /// Время на создание VRD файла
        /// </summary>
        public double CreateVrdFileTimestamp { get; set; }

        /// <summary>
        /// Время на создание Web config файла
        /// </summary>
        public double CreateWebConfigFileTimestamp { get; set; }

        /// <summary>
        /// Время на редактирование VRD файла
        /// </summary>
        public double EditVrdFileTimestamp { get; set; }

        /// <summary>
        /// Время на редактирование Web config файла
        /// </summary>
        public double EditWebConfigFileTimestamp { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public string DatabaseNumber { get; set; }

        /// <summary>
        /// Время выпонения операции
        /// </summary>
        public DateTime PublicationDateTime { get; set; }
    }
}
