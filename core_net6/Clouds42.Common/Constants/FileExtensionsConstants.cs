﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Константы для расширений файлов
    /// </summary>
    public static class FileExtensionsConstants
    {
        /// <summary>
        /// ZIP файл
        /// </summary>
        public static string ZipFile => ".zip";

        /// <summary>
        /// Файл бэкапа SQL
        /// </summary>
        public static string SqlBackup => ".bak";

        /// <summary>
        /// DT файл базы
        /// </summary>
        public static string DtFile => ".dt";
    }
}
