﻿namespace Clouds42.Common.Constants
{
    // ReSharper disable once ClassNeverInstantiated.Global
    /// <summary>
    /// Содержит константы к 1С exe файлам
    /// </summary>
    public static class Client1CConstants
    {
        /// <summary>
        /// Имя 1С толстого клиента: "1cv8.exe". 
        /// </summary>
        public const string ThickNameWithExe = "1cv8.exe";

        /// <summary>
        /// Имя 1С тонкого клиента: "1cv8c.exe".
        /// </summary>
        public const string ThinNameWithExe = "1cv8c.exe";
    }
}