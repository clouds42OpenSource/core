﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Системные константы
    /// </summary>
    public static class Clouds42SystemConstants 
    {
        /// <summary>
        /// Доступный объем дискового пространства (иска R) по умолчанию при создание. 
        /// </summary>
        public const int DefaultAvailableMemorySize = 10240;

        /// <summary>
        /// Доступный покупаемый объем дискового пространства (для диска R за одну покупку 10 Гб).
        /// </summary>
        public const int PayedDiskSpace = 10240;

        /// <summary>
        /// Количество дней демо периода
        /// </summary>
        public const double MyEnterpriseDemoPeriodDays = 7;
    }
}
