﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    ///     Константы для полей объектов AccountUser
    /// </summary>
    public static class AccountUserConstants
    {
        /// <summary>
        /// Максимальная длина логина
        /// </summary>
        public const int MaxLoginLength = 20;

        /// <summary>
        /// Минимальная длина логина
        /// </summary>
        public const int MinLoginLength = 3;

        /// <summary>
        /// Минимальная длина пароля
        /// </summary>
        public const int MinPasswordLength = 8;
        
        /// <summary>
        /// Максимальная длина пароля
        /// </summary>
        public const int MaxPasswordLength = 25;
    }
}
