﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Класс константов общего провайдера щифрования
    /// </summary>
    public static class CommonCryptoProviderConst
    {
        /// <summary>
        /// Константа ключа для пользователей
        /// </summary>
        public const string Core42TeamPwd = "CORE42CLOUDPWD";

        /// <summary>
        /// Константа ключа для общего шифрования
        /// </summary>
        public const string Core42CommonPwd = "CORE42CLOUDCOMMONPWD";
    }
}