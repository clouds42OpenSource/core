﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Константы расширений хоста
    /// </summary>
    public static class HostExtensionName
    {
        /// <summary>
        /// Для промо хоста
        /// </summary>
        public static class Promo
        {
            /// <summary>
            /// Расширение для промо хоста для России
            /// </summary>
            public const string RuHostExt = "com";

            /// <summary>
            /// Расширение для промо хоста для Украины
            /// </summary>
            public const string UaHostExt = "pro";
        }
    }
}