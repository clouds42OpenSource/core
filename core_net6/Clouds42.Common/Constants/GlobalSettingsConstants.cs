﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Глобальные константы настроек системы
    /// </summary>
    public static class GlobalSettingsConstants
    {
        /// <summary>
        /// Страницы справочников
        /// </summary>
        public static class ReferenceDataPages
        {
            /// <summary>
            /// Дефолтное количество записей в таблице данных
            /// </summary>
            public const int DefaultDataGridRecordsCount = 50;
        }
    }
}
