﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Константы названий заголовков в запросе
    /// </summary>
    public static class HttpRequestHeaderName
    {
        /// <summary>
        /// Название ключа в котором содержатся адреса от кого был запрос
        /// </summary>
        public const string Referer = nameof(Referer);

        /// <summary>
        /// Название ключа в котором содержатся источник запроса
        /// </summary>
        public const string Origin = "origin";
    }
}