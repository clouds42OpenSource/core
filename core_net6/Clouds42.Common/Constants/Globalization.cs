﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// 
    /// </summary>
    public static class Globalization
    {

        /// <summary>
        /// 
        /// </summary>
        public static string DateFormat => string.Format("dd{0}MM{0}yyyy HH:mm:ss", DateSeparator);

        /// <summary>
        /// 
        /// </summary>
        public static string ShortDateFormat => string.Format("dd{0}MM{0}yyyy", DateSeparator);

        /// <summary>
        /// 
        /// </summary>
        public static string DateTimeFormatJavaScript => string.Format("dd{0}MM{0}yyyy hh:mm:ss", DateSeparator);

        /// <summary>
        /// 
        /// </summary>
        public static string DateFormatJavaScript => string.Format("dd{0}MM{0}yyyy", DateSeparator);

        /// <summary>
        /// 
        /// </summary>
        public const string DateSeparator = ".";

    }

}
