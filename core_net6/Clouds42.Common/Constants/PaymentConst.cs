﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Константы для работы с платежами
    /// </summary>
    public static class PaymentConst
    {
        /// <summary>
        /// Детали платежа
        /// </summary>
        public const string OriginDetails = "core-cp";
    }
}
