﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Константы вэб запроса
    /// </summary>
    public static class WebRequestConst
    {
        /// <summary>
        /// Тип контента
        /// </summary>
        public const string ContentType = "Content-Type";

        /// <summary>
        /// Тип контента Json
        /// </summary>
        public const string JsonContentType = "application/json";
    }
}
