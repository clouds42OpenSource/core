﻿namespace Clouds42.Common.Constants
{
    /// <summary>
    /// Константы информационной базы
    /// </summary>
    public static class DatabaseConst
    {
        /// <summary>
        /// Префикс файловой базы
        /// </summary>
        public const string FileDatabasePrefix = "1c_my_";

        /// <summary>
        /// Префикс файловой базы для всех локалей
        /// </summary>
        public const string AllLocalesFileDatabasePrefix = "base_";
    }
}
