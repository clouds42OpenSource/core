﻿namespace Clouds42.Common.ManagersResults
{
    public interface IManagerResult
    {
        object ResultObject { get; }

        ManagerResultState State { get; set; }

        string Message { get; set; }
        bool Error { get; }

        object ErrorObject { get; set; }
    }
}