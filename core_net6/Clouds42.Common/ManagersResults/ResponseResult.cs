﻿
namespace Clouds42.Common.ManagersResults
{
    /// <summary>
    /// Новый резалт для ответов в контроллерах
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseResult<T>
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }

        public ResponseResult()
        {
            
        }
        public ResponseResult(bool success, T data)
        {
            Success = success;
            Data = data;
        }

        public ResponseResult(bool success, string message)
        {
            Success = success;
            Message = message;
        }

        public ResponseResult(bool success, string message, T data) : this(success, message)
        {
            Data = data;
        }
    }



    public class ResponseResult(bool success, string message)
    {
        public bool Success { get; set; } = success;
        public string Message { get; set; } = message;
    }
}
