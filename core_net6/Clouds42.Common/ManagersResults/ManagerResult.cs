﻿using System;

namespace Clouds42.Common.ManagersResults
{
    public class ManagerResult<T> : ManagerResult, IManagerResult
    {
        public int? Code { get; set; }
        public T Result { get; set; }
        public object ResultObject => Result;
        public Exception Exception { get; set; }
    }

    public class ManagerResult
    {
        public ManagerResultState State { get; set; }
        public string Message { get; set; }
        public bool Error => State != ManagerResultState.Ok;
        public object ErrorObject { get; set; }
    }
}
