﻿namespace Clouds42.Common.ManagersResults
{
    public enum ManagerResultState
    {
        PreconditionFailed,
        NotFound,
        Ok,
        Forbidden,
        BadRequest,
        Conflict,
        Unauthorized,
        MethodNotAllowed
    }
}