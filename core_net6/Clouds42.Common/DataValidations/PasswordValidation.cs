﻿using System.Text.RegularExpressions;
using Clouds42.Common.Constants;
using Clouds42.Common.DataModels;

namespace Clouds42.Common.DataValidations
{
    /// <summary>
    /// Валидатор пароля
    /// </summary>        
    public class PasswordValidation
    {
        private readonly string _password;
        private const string Pattern = "^([A-Za-z0-9!@#$^&*_+\\-=[\\];':\"\\\\|,.<>\\/?]){8,25}$";

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="password">Пароль</param>
        public PasswordValidation(string password)
        {
            _password = password;
        }

        /// <summary>
        /// Пароль валиден
        /// </summary>
        /// <returns>Результат валидации пароля</returns>
        public AccountUserDataValidationResultDto PasswordIsValid()
        {
            string messedg = (
                $@"Пароль должен содержать от {AccountUserConstants.MinPasswordLength} до {AccountUserConstants.MaxPasswordLength} символов, 
цифры, латинские буквы или спец. символы"
                );

            var model = new AccountUserDataValidationResultDto
            {
                DataIsValid = Regex.IsMatch(_password, Pattern)
            };

            if (!model.DataIsValid)
                model.ValidationMessages.Add(messedg);

            return model;
        }
    }
}
