﻿using System.Text.RegularExpressions;

namespace Clouds42.Common.DataValidations
{
    /// <summary></summary>        
    public class NameValidation
    {        
        private readonly string name;

        /// <summary></summary>        
        public NameValidation(string name)
        {
            this.name = name;
        }
     

        /// <summary></summary>
        public bool NameMaskIsValid()
        {
            return !string.IsNullOrEmpty(name) && new Regex(@"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z ]+$").IsMatch(name); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool NameLengthIsValid()
        {
            return name.Length <= 30;
        }

        
    }
}
