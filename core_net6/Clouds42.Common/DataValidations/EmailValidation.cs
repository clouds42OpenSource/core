﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Clouds42.Common.DataValidations
{
    /// <summary>
    /// Валидатор почты
    /// </summary>
    public class EmailValidation
    {
        /// <summary>
        /// Регулярное выражение для проверки валидности почты
        /// </summary>
        public const string Pattern = @"^[A-Za-z0-9]+(?:[.\-/_]+[A-Za-z0-9]+)*@[A-Za-z0-9]+(?:[.-][A-Za-z0-9]+)*\.[a-z]{2,}$";

        /// <summary>
        /// Почта
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// User login
        /// </summary>
        private string UserLogin { get; set ; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="email">Почта</param>
        public EmailValidation(string email)
        {
            Email = email;
        }

        public EmailValidation(string email, string userLogin) : this(email)
        {
            UserLogin = userLogin;
        }



        /// <summary>
        /// Маска почты валидна
        /// </summary>
        /// <returns>Результат проверки</returns>
        public bool EmailMaskIsValid()
        {
            try 
            {
                var mailAddress = new MailAddress(Email);
                return new Regex(Pattern).IsMatch(mailAddress.ToString());
            } 
            catch (Exception) 
            {
                return false;
            }   
        }
    }
}
