﻿using Quartz;

namespace Clouds42.Common.DataValidations
{
    /// <summary>
    /// 
    /// </summary>
    public static class CronExpressionValidator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        public static bool Validate(string expr)
        {
            return CronExpression.IsValidExpression(expr);
        }
    }
}
