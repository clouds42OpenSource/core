﻿using System;
using System.Linq;
using Clouds42.Common.Extensions;
using Clouds42.Logger;
using PhoneNumbers;

namespace Clouds42.Common.DataValidations
{
    /// <summary>
    /// Класс для валидации телефонных номеров
    /// </summary>
    public class PhoneNumberValidation
    {
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; private set; }

        private static readonly ILogger42 Logger = Logger42.GetLogger();

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="phoneNumber">Номер телефона</param>
        public PhoneNumberValidation(string phoneNumber)
        {
            PhoneNumber = phoneNumber;
        }

        /// <summary>
        /// Осуществляет проверку объекта phoneNumberObject на валидность.
        /// </summary>
        /// <remarks>
        /// Валидация использует библиотеку "libphonenumber"
        /// </remarks>
        /// <returns>Если все проверки прошли успешно, то true иначе false</returns>
        public bool PhoneNumberMaskIsValid()
        {
            try
            {
                if (PhoneNumber.FirstOrDefault() != '+')
                {
                    PhoneNumber = "+" + PhoneNumber;
                }
                if (!PhoneNumberJustValid(PhoneNumber))
                {
                    if (PhoneNumber.StartsWith("+8"))
                    {
                        PhoneNumber = PhoneNumber.Replace("+8", "+7");
                        if (PhoneNumberJustValid(PhoneNumber))
                        {
                            Logger.Trace($"Номер телефона {PhoneNumber} валиден.");
                            return true;
                        }
                    }

                    Logger.Warn($"Проверка номера телефона {PhoneNumber} неудачна");
                    return false;
                }

                Logger.Trace($"Номер телефона {PhoneNumber} валиден.");

                
            }
            catch (Exception ex)
            {
                Logger.Warn($"Проверка номера телефона {PhoneNumber} неудачна, ошибка {ex.Message}");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Номер телефона валиден
        /// </summary>
        /// <param name="phoneNumber">Номер телефона</param>
        /// <returns>Результат проверки</returns>
        private bool PhoneNumberJustValid(string phoneNumber)
        {
            var phoneUtil = PhoneNumberUtil.GetInstance();
            try
            {
                return phoneUtil.IsValidNumber(phoneUtil.Parse(phoneNumber, null));
            }
            catch (Exception e)
            {
                Logger.Warn("PhoneNumberMaskIsValid error msg: {0}", e.GetFullInfo());
                return false;
            }
        }

        /// <summary>
        /// Вариант метода проверки, содержащий дополнительные параметры.
        /// </summary>
        /// <remarks>
        /// В настоящий момент фактически вызывает метод PhoneNumberMaskIsValid
        /// который без параметров. Возможно legacy.
        /// </remarks>
        /// <param name="locale">Имя локали, в настоящий момент не используется</param>
        /// <param name="errorMessage">Выходной параметр: сообщение об ошибке</param>
        /// <returns>true, если проверка удачна, и false в противоположном случае</returns>
        public bool PhoneNumberMaskIsValid(string locale, out string errorMessage)
        {
            errorMessage = null;
            var isvalid = PhoneNumberMaskIsValid();
            if (!isvalid)
            {
                Logger.Trace($"Недопустимый формат номера телефона :{PhoneNumber}");
                errorMessage = "Недопустимый формат номера телефона";
            }

            return isvalid;
        }

    }
}
