﻿using System.Linq;
using Clouds42.Common.DataModels;

namespace Clouds42.Common.DataValidations
{
    /// <summary>
    /// Валидатор подписи аккаунта
    /// </summary>
    public class AccountCaptionValidation
    {
        private const int MinLength = 1;
        private const int MaxLength = 100;
        private readonly string _caption;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="caption">Подпись аккаунта</param>
        public AccountCaptionValidation(string caption)
        {
            _caption = caption;
        }

        /// <summary>
        /// Подпись валидна
        /// </summary>
        /// <returns>Результат валидации подписи</returns>
        public AccountUserDataValidationResultDto CaptionIsValid()
        {
            var result = new AccountUserDataValidationResultDto();

            if (_caption != null && !CaptionMaskIsValid())
                result.ValidationMessages.Add(
                    $"Количество символов в подписи пользователя должно находиться в пределах {MinLengthAllowed}-{MaxLengthAllowed}");

            result.DataIsValid = !result.ValidationMessages.Any();
            return result;
        }

        /// <summary>
        /// Минимально доступная длина подписи
        /// </summary>
        public int MinLengthAllowed => MinLength;

        /// <summary>
        /// Максимально доступная длина подписи
        /// </summary>
        public int MaxLengthAllowed => MaxLength;

        /// <summary>
        /// Маска подписи валидна
        /// </summary>    
        public bool CaptionMaskIsValid()
            => !string.IsNullOrEmpty(_caption) && _caption.Length is >= MinLength and <= MaxLength;
    }
}
