﻿using System.Linq;
using System.Text.RegularExpressions;
using Clouds42.Common.Constants;
using Clouds42.Common.DataModels;

namespace Clouds42.Common.DataValidations
{
    /// <summary>
    /// Login validator
    /// </summary>
    public class LoginValidation(string login)
    {
        /// <summary>
        /// User login
        /// </summary>
        private string Login { get; } = login;

        /// <summary>
        /// Login max length
        /// </summary>
        private readonly int _maxLoginLength = AccountUserConstants.MaxLoginLength;

        /// <summary>
        /// Login min length
        /// </summary>
        private readonly int _minLoginLength = AccountUserConstants.MinLoginLength;

        /// <summary>
        /// Is login valid
        /// </summary>
        /// <returns><see langword="true"/> if login is valid otherwise <see langword="false"/></returns>
        public AccountUserDataValidationResultDto LoginIsValid()
        {
            var result = new AccountUserDataValidationResultDto();
            if (!LoginSymbolsIsValid(out var errorMessage))
                result.ValidationMessages.Add(errorMessage);

            if (!LoginLengthIsValid())
                result.ValidationMessages.Add($"Длина логина должна быть от {AccountUserConstants.MinLoginLength} до {AccountUserConstants.MaxLoginLength} символов!");

            result.DataIsValid = !result.ValidationMessages.Any();
            return result;
        }

        /// <summary>
        /// Is login mask valid
        /// </summary>
        /// <param name="errorMessage">Error message if login mask is not valid otherwise <see langword="null"/></param>
        /// <returns><see langword="true"/> if login mask is valid otherwise <see langword="false"/></returns>
        public bool LoginSymbolsIsValid(out string errorMessage)
        {
            errorMessage = null;

            if (string.IsNullOrEmpty(Login))
            {
                errorMessage = "Логин не может быть пустым";
                return false;
            }

            if (Login.EndsWith("."))
            {
                errorMessage = "В логине не допускается последний символ '.'";
                return false;
            }

            var isValid = Regex.IsMatch(Login, @"^[A-Z-a-z0-9_.\-()]+$");
            if (!isValid)
               errorMessage = "В логине допускаются только латинские буквы, цифры и знак _";
               
            return isValid;
        }

        /// <summary>
        /// Is login length valid
        /// </summary>
        /// <returns><see langword="true"/> if login length is valid otherwise <see langword="false"/></returns>
        public bool LoginLengthIsValid()
            => !string.IsNullOrEmpty(Login) && Login.Length >= _minLoginLength && Login.Length <= _maxLoginLength;

    }
}
