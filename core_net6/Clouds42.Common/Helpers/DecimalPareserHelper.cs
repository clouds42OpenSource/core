﻿using System.Globalization;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Хэлпер для преобразования строки в тип decimal
    /// </summary>
    public static class DecimalPareserHelper
    {
        /// <summary>
        /// Преобразовать в decimal
        /// </summary>
        /// <param name="val">Входная строка</param>
        /// <returns>Число типа decimal</returns>
        public static decimal Parse(string val)
        {
            string wantedSeperator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
            string alternateSeperator = (wantedSeperator == "," ? "." : ",");

            if (val.IndexOf(wantedSeperator) == -1 && val.IndexOf(alternateSeperator) != -1)
            {
                val = val.Replace(alternateSeperator, wantedSeperator);
            }
            return  decimal.Parse(val, NumberStyles.Any);
        }
    }
}
