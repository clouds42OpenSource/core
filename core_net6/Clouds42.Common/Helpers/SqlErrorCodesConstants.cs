﻿namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Список кодов ошибок SQL
    /// ПРИМЕЧАНИЕ: смотреть таблицу master.dbo.sysmessages
    /// </summary>
    public static class SqlErrorCodesConstants
    {
        /// <summary>
        /// Таймаут подключения
        /// </summary>
        public const int ConnectionTimeout = -2;

        /// <summary>
        /// Взаимоблокировка транзакций
        /// </summary>
        public const int TransactionsDeadlock = 1205;

        /// <summary>
        /// Ошибка входа пользователя
        /// </summary>
        public const int LoginFailed = 4060;
    }
}
