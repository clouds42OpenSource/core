﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    ///     Класс-конвертор в Base64 для изображений
    /// </summary>
    public static class BitmapToBase64Converter
    {
        /// <summary>
        ///     Конвертировать изображение в Base64
        /// </summary>
        /// <param name="bitmap">Изображение</param>
        /// <param name="imageFormat">Формат изображения</param>
        /// <returns>Строка в виде Base64</returns>
        public static string ConvertBitmapToBase64(Bitmap bitmap, ImageFormat imageFormat)
        {
            byte[] bitmapInBytesArray;

            using (var memoryStream = new MemoryStream())
            {
                bitmap.Save(memoryStream, imageFormat);

                bitmapInBytesArray = memoryStream.ToArray();
            }

            return Convert.ToBase64String(bitmapInBytesArray);
        }
    }
}
