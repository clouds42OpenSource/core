﻿namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Класс для определения заголовков ззпросов и оветов
    /// </summary>
    public static class WebHeaders
    {
        /// <summary>
        /// заголовок, информирующий о том, что модель запроса не прошла валидацию
        /// </summary>
        public const string ValidationModelFailedWebHeader = "ValidationModelFailed";
    }
}