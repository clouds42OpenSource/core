﻿using System;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Хелпер для работы со специальными папками Windows
    /// </summary>
    public static class SpecialFoldersHelper
    {
        /// <summary>
        /// Полный путь до директории Local в AppData
        /// </summary>
        public static readonly string LocalAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

        /// <summary>
        /// Полный путь до директории System32
        /// </summary>
        public static readonly string System32Path = Environment.GetFolderPath(Environment.SpecialFolder.System);
    }
}
