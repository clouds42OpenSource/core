﻿using System;
using System.Collections.Generic;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Реализует интерфейс компарера версий
    /// </summary>
    public class VersionComparer : IComparer<string>
    {
        /// <inheritdoc />
        /// <summary>Сравнивает версии, заданые строкой</summary>		
        /// <returns>
        /// <param name="x">Версия 1</param>
        /// <param name="y">Версия 2</param>
        /// 1 - если версия 1 больше чем версия 2,
        /// 0 - если версии равны,
        /// -1 - если версия 1 меньше чем версия 2/>
        /// </returns>
        public int Compare(string x, string y)
        {
            string[] v1 = x.Split(["."], StringSplitOptions.RemoveEmptyEntries);
            string[] v2 = y.Split(["."], StringSplitOptions.RemoveEmptyEntries);
            if (v1.Length != v2.Length) return 0;

            return Compare(v1, v2);
        }

        /// <summary>
        /// Сравнивает версии, заданые массивом
        /// </summary>
        /// <param name="ver1">Версия 1</param>
        /// <param name="ver2">Версия 2</param>
        /// <returns>
        /// 1 - если версия 1 больше чем версия 2,
        /// 0 - если версии равны,
        /// -1 - если версия 1 меньше чем версия 2</returns>
        public int Compare(string[] ver1, string[] ver2)
        {
            int tmp1, tmp2;
            try
            {
                for (int i = 0; i < ver1.Length; i++)
                {
                    tmp1 = int.Parse(ver1[i]);
                    tmp2 = int.Parse(ver2[i]);
                    if (tmp1 > tmp2) return 1;
                    if (tmp1 < tmp2) return -1;
                }
            }
            catch
            {
                // ignored
            }

            return 0;
        }
    }
}
