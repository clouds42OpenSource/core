﻿using System;
using System.Linq;
using Clouds42.Common.Extensions;
using Clouds42.Common.Http;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Хэлпер для работы с контекстом Http
    /// </summary>
    public static class HttpContextHelper
    {
        /// <summary>
        /// Получить значение заголовка запроса
        /// </summary>
        /// <param name="request">Контекст запроса</param>
        /// <param name="headerKey">Ключ заголовка</param>
        /// <returns>Значение заголовка запроса</returns>
        public static string[] GetHeaderValue(this ThinHttpRequest request, string headerKey)
            => request.Headers.GetValues(headerKey);

        /// <summary>
        /// Получить ID загружаемого файла из контекста запроса
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        /// <returns>ID загружаемого файла</returns>
        public static Guid GetUploadedFileId(this ThinHttpRequest httpRequest)
        {
            var headerValue = httpRequest.GetHeaderValue("UploadedFileId")?.FirstOrDefault();
            return headerValue == null
                ? throw new InvalidOperationException(
                                  "Не найдено значение ID загружаемого файла в контексте запроса")
                : Guid.Parse(headerValue);
        }

        /// <summary>
        /// Получить количество частей файла из контекста запроса
        /// </summary>
        /// <param name="httpRequest">Контекст запроса</param>
        /// <returns>Количество частей файла</returns>
        public static int GetCountOfLoadedParts(this ThinHttpRequest httpRequest)
        {
            var headerValue = httpRequest.GetHeaderValue("CountOfFileParts")?.FirstOrDefault();
            return headerValue?.ToInt() ?? throw new InvalidOperationException(
                "Не найдено значение ID загружаемого файла в контексте запроса");
        }
    }
}
