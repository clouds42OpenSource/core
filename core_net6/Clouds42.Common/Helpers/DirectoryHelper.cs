﻿using System;
using System.Globalization;
using System.IO;
using System.Net.NetworkInformation;
using Clouds42.Common.Extensions;
using Clouds42.Logger;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Хэлпер для работы с директориями
    /// </summary>
    public static class DirectoryHelper
    {
        static readonly ILogger42 Logger = Logger42.GetLogger();
        /// <summary>
        /// Удалить директорию
        /// </summary>
        /// <param name="path">Путь к директории</param>
        /// <param name="onlyDirectoryContent">
        /// Признак что необходимо удалить только содержимое директории,
        /// иначе саму директорию в том числе
        /// </param>
        public static void DeleteDirectory(string path, bool onlyDirectoryContent = false)
        {
            foreach (var directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            var di = new DirectoryInfo(path);
            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }

            if (onlyDirectoryContent)
                return;

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        /// <summary>
        /// Пытаться удалить директорию несколько раз.
        /// </summary>
        /// <param name="path">Путь до директории.</param>
        /// <param name="tryCount">Количество попыток.</param>
        public static void TryDeleteDirectoryOrThrowException(string path, int tryCount = 10)
        {
            var message = $"Удаление директории {path}:";
            for (var tryNumber = 0; tryNumber < tryCount; tryNumber++)
            {
                try
                {
                    if (!Exists(path))
                        return;

                    DeleteDirectoryRecursively(path);
                    return;
                }
                catch (IOException ex)
                {
                    message = GenerateExceptionMessages(tryNumber, ex.GetFullInfo(), message);
                }
                catch (AggregateException ex)
                {
                    message = GenerateExceptionMessages(tryNumber, ex.GetFullInfo(), message);
                }
                catch (UnauthorizedAccessException ex)
                {
                    message = GenerateExceptionMessages(tryNumber, ex.GetFullInfo(), message);
                }
            }

            throw new InvalidOperationException($"Не удалось удалить директорию : {path} :: {message}");
        }

        /// <summary>
        /// Сформировать сообщение об ошибке
        /// </summary>
        /// <param name="tryNumber">Номер попытки</param>
        /// <param name="exception">Сообщение исключения</param>
        /// <param name="message">Сообщение об ошибке</param>
        /// <returns>Сообщение об ошибке</returns>
        private static string GenerateExceptionMessages(int tryNumber, string exception, string message)
            => $"{message} Попытка удаления {tryNumber} - ошибка {exception}";

        /// <summary>
        /// Признак что в директории есть файл 1CD
        /// </summary>
        /// <param name="path">Путь к директории</param>
        /// <returns>true - если файл есть</returns>
        public static bool DirectoryHas1CDatabaseFile(string path)
        {
            if (!Directory.Exists(path))
                return false;

            if (Directory.GetFiles(path, "*.1CD").Length > 0)
                return true;

            return Directory.GetFiles(path, "*.MD").Length > 0;
        }

        /// <summary>
        /// Признак что в директории есть файл 1CD
        /// </summary>
        /// <param name="path">Путь к директории</param>
        /// <returns>true - если файл есть</returns>
        public static bool DirectoryPingandHas1CDatabaseFile(string path)
        {
            if (!PingServerFromUNC(path))
                return true;

            if (!Directory.Exists(path))
                return false;

            if (Directory.GetFiles(path, "*.1CD").Length > 0)
                return true;

            return Directory.GetFiles(path, "*.MD").Length > 0;
        }

        /// <summary>
        /// Скопировать файлы директорию в другую директорию.
        /// </summary>
        /// <param name="sourceDirName">Исходный путь к директории</param>
        /// <param name="destDirName">Целевой путь к директории</param>
        /// <param name="copySubDirs">Признак что необходимо копировать дочерние директории</param> 
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            var dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Диектория не найдена: "
                    + sourceDirName);
            }

            var dirs = dir.GetDirectories();
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            var files = dir.GetFiles();
            foreach (var file in files)
            {
                var temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
                
            }

            if (!copySubDirs)
                return;

            foreach (var subdir in dirs)
            {
                var temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath, copySubDirs);
            }
        }


        /// <summary>
        /// Для удаления директории с длинными путями и атрибутами ReadOnly
        /// </summary>
        /// <param name="path">Путь до директории</param>
        public static void DeleteDirectoryRecursively(string path)
        {
            if (!Exists(path))
                throw new DirectoryNotFoundException(
                   $"Директория не найдена: {path}");

            var directoryInfo = new DirectoryInfo(path);

            try
            {
                directoryInfo.Delete(true);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Не удалось удалить директорию после удаления атрибутов", ex);
            }
        }

        /// <summary>
        /// Проверить наличие директории с длинными путями.
        /// </summary>
        /// <param name="path">Путь до директории.</param>
        /// <returns>true - если есть.</returns>
        public static bool Exists(string path) => Directory.Exists(path);

        /// <summary>
        /// Устанавливаем флаг "Норамальный" для папок и файлов.
        /// </summary>
        /// <param name="path">Путь до директории.</param>
        public static void SetAttributesNormal(string path)
        {
            var directoryInfo = new DirectoryInfo(path) { Attributes = FileAttributes.Normal };

            foreach (var info in directoryInfo.GetFileSystemInfos("*", SearchOption.AllDirectories))
            {
                info.Attributes = FileAttributes.Normal;
            }
        }

        /// <summary>
        /// Remove files from directory if older than specified age
        /// </summary>
        /// <param name="path">Directory to clear</param>
        /// <param name="maxAge">Threshold for file creation date. Any file older than speicified value will be deleted</param>
        /// <param name="logger">Logger</param>
        /// <returns>Number of files deleted</returns>
        public static int ClearDirectoryFromOldFiles(string path, TimeSpan maxAge,
            ILogger42 logger = null)
        {
            if (!Directory.Exists(path))
                return 0;

            logger?.Info($"Найдена директория {path}");

            var filesInDirectory = Directory.GetFiles(path);
            if (filesInDirectory.Length == 0)
                return 0;

            logger?.Info($"В директории {path} найдено файлов {filesInDirectory.Length}");
            int filesRemoved = 0;
            foreach (var file in filesInDirectory)
            {
                try
                {
                    var filepath = Path.Combine(path, file);
                    var fileCreationTime = File.GetCreationTime(filepath);
                    logger?.Info($"Файл {filepath} загружен {fileCreationTime.ToString(CultureInfo.InvariantCulture)}");

                    //если файл создан недавно, то переходим к слудующему
                    if (fileCreationTime > DateTime.Now.Subtract(maxAge))
                        continue;

                    logger?.Info($"Начало удаления файла {filepath}");
                    File.Delete(filepath);
                    logger?.Info($"Файл {filepath} удален");
                    filesRemoved++;
                }
                catch (Exception e)
                {
                    logger?.Error(e, $"Ошибка при удалении файла {file} по пути {path}");
                }
            }
            return filesRemoved;
        }

        /// <summary>
        /// Clear directory from files which are older than specified age
        /// </summary>
        /// <param name="directoryPath">Directory to clear</param>
        /// <param name="maxAge">Threshold for file creation date. Any file older than speicified value will be deleted</param>
        /// <param name="logger">Logger</param>
        /// <returns>Number of files deleted</returns>
        public static int ClearOldFiles(string directoryPath, TimeSpan maxAge,
            ILogger42 logger = null)
        {
            if (!Directory.Exists(directoryPath))
                return 0;

            logger?.Info($"Найдена директория {directoryPath}");

            var filesInDirectory = Directory.GetFiles(directoryPath);
            if (filesInDirectory.Length == 0)
                return 0;

            logger?.Info($"В директории {directoryPath} найдено файлов {filesInDirectory.Length}");
            int filesRemoved = 0;
            foreach (var file in filesInDirectory)
            {
                try
                {
                    var filepath = Path.Combine(directoryPath, file);
                    var fileCreationTime = File.GetCreationTime(filepath);
                    logger?.Info($"Файл {filepath} загружен {fileCreationTime.ToString(CultureInfo.InvariantCulture)}");

                    //если файл создан недавно, то переходим к слудующему
                    if (fileCreationTime > DateTime.Now.Subtract(maxAge))
                        continue;

                    logger?.Info($"Начало удаления файла {filepath}");
                    File.Delete(filepath);
                    logger?.Info($"Файл {filepath} удален");
                    filesRemoved++;
                }
                catch (Exception e)
                {
                    logger?.Error(e, $"Ошибка при удалении файла {file} по пути {directoryPath}");
                }
            }
            return filesRemoved;
        }
        public static bool PingServerFromUNC(string uncPath)
        {
            Logger.Info($"Пингую {uncPath}");
            var serverName = new Uri("file://" + uncPath);
            var st = serverName.Host;
            try
            {
                var ping = new Ping();
                var reply = ping.Send(st, 1000);  // 1000 мс = 1 секунда
                Logger.Info($"пинг прошел {reply.Status}");
                return reply.Status == IPStatus.Success;
            }
            catch
            {
                return false;
            }
        }
    }
}
