﻿using System;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Класс для сравнения версий файлов разработки сервиса
    /// </summary>
    public static class BillingServiceVersionComparer
    {
        private const string EmptyElementValue = "0";

        /// <summary>
        /// Сравнить версии
        /// </summary>
        /// <param name="oldVersion">Старая версия</param>
        /// <param name="newVersion">Новая версия</param>
        /// <returns>Результат сравнения</returns>
        public static bool Compare(string oldVersion, string newVersion)
        {
            var oldVersionArray = oldVersion.Split('.');
            var newVersionArray = newVersion.Split('.');

            if (oldVersionArray.Length != newVersionArray.Length)
            {
                var newVersionIsLarger = CheckNewVersionIsLonger(oldVersionArray.Length, newVersionArray.Length);

                if (newVersionIsLarger)
                    oldVersionArray = ChangeArrayLength(oldVersionArray, newVersionArray.Length);

                if (!newVersionIsLarger)
                    newVersionArray = ChangeArrayLength(newVersionArray, oldVersionArray.Length);
            }

            return new VersionComparer().Compare(oldVersionArray, newVersionArray) < 0;
        }

        /// <summary>
        /// Проверить, что новая версия длиннее
        /// </summary>
        /// <param name="olVersionLength">Длина старой версии</param>
        /// <param name="newVersionLength">Длина новой версии</param>
        /// <returns>Результат проверки</returns>
        private static bool CheckNewVersionIsLonger(int olVersionLength, int newVersionLength) =>
            newVersionLength > olVersionLength;

        /// <summary>
        /// Изменить длину массива
        /// </summary>
        /// <param name="array">Массив </param>
        /// <param name="length">Длина массива</param>
        /// <returns>Новый массив с длиной length</returns>
        private static string[] ChangeArrayLength(string[] array, int length)
        {
            var newArray = new string[length];
            Array.Copy(array, 0, newArray, 0, array.Length);
            return FillEmptyArrayElements(newArray);
        }

        /// <summary>
        /// Заполнить пустые элементы массива нулями
        /// </summary>
        /// <param name="array">Массив строк</param>
        /// <returns></returns>
        private static string[] FillEmptyArrayElements(string[] array)
        {
            for (int i = 0; i < array.Length; i++)
                if (string.IsNullOrEmpty(array[i]))
                    array[i] = EmptyElementValue;

            return array;
        }
    }
}
