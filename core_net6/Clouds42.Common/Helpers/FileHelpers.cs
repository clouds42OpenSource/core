﻿using System.Collections.Generic;

namespace Clouds42.Common.Helpers
{
    public static class FileHelpers
    {
        /// <summary>
        /// Получить заголовки запроса для файла
        /// </summary>
        /// <param name="startPoint">Начальная точка</param>
        /// <param name="chunkContentLength">Длина контента части файла</param>
        /// <param name="totalContentLength">Общая длина контента</param>
        /// <returns>Заголовки запроса для файла</returns>
        public static List<KeyValuePair<string, object>> GetRequestHeadersForFile(long startPoint,
            int chunkContentLength, long totalContentLength)
        {
            var endPoint = startPoint + chunkContentLength - 1;
            var contentRange = $"bytes {startPoint}-{endPoint}/{totalContentLength}";

            return
            [
                new("Content-Length", chunkContentLength),
                new("Content-Range", contentRange)
            ];
        }
    }
}