﻿using System;
using System.Collections.Generic;
using Clouds42.Common.Models;
using Clouds42.Domain.Enums;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Хелпер для подготовки отчета о выполненных операциях с пулом 
    /// </summary>
    public static class PreparePoolOperationDataForExportHelper
    {
        private static readonly
            Dictionary<ExecutionTimeOperationsEnum, Action<TraceRuntimeDataDc, DatabasePoolOperationsReportModelDc>>
            MapOperationTypeToSetOperation =
                new()
                {
                    {
                        ExecutionTimeOperationsEnum.GetApplicationPath,
                        (trw, dto) => dto.GetApplicationPathTimeStamp = trw.ElapsedSeconds
                    },
                    {
                        ExecutionTimeOperationsEnum.GetApplicationPool,
                        (trw, dto) => dto.GetApplicationPoolTimeStamp = trw.ElapsedSeconds
                    },
                    {
                        ExecutionTimeOperationsEnum.GetApplicationPoolState,
                        (trw, dto) => dto.GetApplicationPoolStateTimeStamp = trw.ElapsedSeconds
                    },
                    {
                        ExecutionTimeOperationsEnum.StartApplicationPool,
                        (trw, dto) => dto.StartApplicationPoolTimeStamp = trw.ElapsedSeconds
                    },
                    {
                        ExecutionTimeOperationsEnum.StopApplicationPool,
                        (trw, dto) => dto.StopApplicationPoolTimeStamp = trw.ElapsedSeconds
                    }
                };

        /// <summary>
        /// Подготовить данные к выгрузке в гугл таблицу
        /// </summary>
        /// <param name="traceRuntimeData">Модель фиксации времени выполнения операции</param>
        /// <param name="nodeAddress">Нода публикации</param>
        /// <param name="databaseNumber">Номер базы</param>
        /// <returns>Модель отчета о выполненных операциях с пулом</returns>
        public static DatabasePoolOperationsReportModelDc PrepareData(TraceRuntimeDataDc traceRuntimeData,
            string nodeAddress, string databaseNumber)
        {
            var reportModelDc = new DatabasePoolOperationsReportModelDc
            {
                NodeAddress = nodeAddress,
                OperationDateTime = DateTime.Now,
                V82Name = databaseNumber
            };

            MapOperationTypeToSetOperation[traceRuntimeData.ExecutionTimeOperation](traceRuntimeData, reportModelDc);
            return reportModelDc;
        }
    }
}