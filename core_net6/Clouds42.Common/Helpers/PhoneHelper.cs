﻿using System.Text.RegularExpressions;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Хэлпер для работы с номерами телефона
    /// </summary>
    public static class PhoneHelper
    {
        /// <summary>
        /// Заменить все символы кроме цифр на пустое значение
        /// </summary>
        /// <param name="val">Номер телефона</param>
        /// <returns>Обработанный номер телефона</returns>
        public static string ClearNonDigits(string val)
            => string.IsNullOrEmpty(val)
                ? val
                : Regex.Replace(val, @"\D", string.Empty);
    }
}
