﻿using System.Text.RegularExpressions;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Хелпер для преобразование строк с html тэгами в строку без тэгов
    /// </summary>
    public static class HtmlConverterHelper
    {
        /// <summary>
        /// Шаблон для поиска разметки HTML в строке
        /// </summary>
        private static string PatternHtmlTags => "<.*?>";

        /// <summary>
        /// Шаблон для поиска символа широкого пробела HTML в строке
        /// </summary>
        private static string PatternHtmlWideGap => "&emsp;-";
        
        /// <summary>
        /// Убрать html тэги со строки
        /// </summary>
        /// <param name="stringWithHtmlTags">Строка с html тэгами</param>
        /// <returns>Строка без html тэгов</returns>
        public static string ClearHtmlTagsFromString(this string stringWithHtmlTags) =>
            Regex.Replace(stringWithHtmlTags, PatternHtmlTags, string.Empty).Replace(PatternHtmlWideGap, string.Empty);
    }
}
