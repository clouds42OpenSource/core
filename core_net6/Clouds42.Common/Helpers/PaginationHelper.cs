﻿using System;

namespace Clouds42.Common.Helpers
{
    /// <summary>
    /// Хэлпер для работы с пагинацией
    /// </summary>
    public static class PaginationHelper
    {
        private const int FirstPageNumber = 1;

        /// <summary>
        /// Получить текущий номер страницы для фильтра на основании кол-ва элементов в выборке
        /// </summary>
        /// <param name="itemsCount">Кол-во элементов в выборке</param>
        /// <param name="itemsPerPageCount">Кол-во элементов на одну страницу</param>
        /// <param name="currentPageNumber">Номер текущей страницы</param>
        /// <returns>Текущий номер страницы для фильтра</returns>
        public static int GetFilterPageNumberBasedOnItemsCount(int itemsCount, int itemsPerPageCount, int currentPageNumber)
        {
            var totalPagesCount = (int)Math.Ceiling((double)itemsCount / itemsPerPageCount);
            return DefineCurrentPageNumber(currentPageNumber, totalPagesCount);
        }

        /// <summary>
        /// Определить текущий номер страницы для фильтра
        /// </summary>
        /// <param name="currentPageNumber">Номер текущей страницы</param>
        /// <param name="totalPagesCount">Общее кол-во страниц</param>
        /// <returns>Текущий номер страницы для фильтра</returns>
        private static int DefineCurrentPageNumber(int currentPageNumber, int totalPagesCount)
        {
            if (totalPagesCount == 0)
                return FirstPageNumber;

            return currentPageNumber > totalPagesCount ? totalPagesCount : currentPageNumber;
        }
    }
}