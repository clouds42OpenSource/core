﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.Common.Helpers
{
    public static class ManagerResultHelper
    {

        public static ManagerResult<T> Ok<T>(T result, string message = null)
        {
            return new ManagerResult<T>
            {
                State = ManagerResultState.Ok,
                Result = result,
                Message = message
            };
        }

        public static ManagerResult Ok(string message = null)
        {
            return new ManagerResult
            {
                Message = message,
                State = ManagerResultState.Ok
            };
        }

        public static ManagerResult<T> PreconditionFailed<T>(T result, string message = null)
        {
            return new ManagerResult<T>
            {
                State = ManagerResultState.PreconditionFailed,
                Result = result,
                Message = message
            };
        }

        public static ManagerResult PreconditionFailed(string message)
        {
            return new ManagerResult
            {
                State = ManagerResultState.PreconditionFailed,
                Message = message
            };
        }

        public static ManagerResult<T> Forbidden<T>(T result, string message = null)
        {
            return new ManagerResult<T>
            {
                Message = message,
                Result = result,
                State = ManagerResultState.Forbidden
            };
        }

        public static ManagerResult Forbidden(string message = null)
        {
            return new ManagerResult
            {
                Message = message,
                State = ManagerResultState.Forbidden
            };
        }
    }
}
