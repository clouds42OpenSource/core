﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Clouds42.Common.Helpers
{

    /// <summary>
    /// Вспомогательный класс сериализации объектов. 
    /// </summary>
    public static class SerializeObjectHelper
    {

        /// <summary>
        /// Сериализовать объект в byte[]
        /// </summary>
        /// <param name="obj">Объект сериализацации.</param>
        /// <returns>Сериализуемый объект byte[]</returns>
        [Obsolete("BinaryFormatter не поддерживается в NET6. Используем пока все клиенты не перейдут на новый Link42")]
        public static byte[] ToByteArray(this object obj)
        {
            var mStream = new MemoryStream();
            new BinaryFormatter().Serialize(mStream, obj);
            return mStream.ToArray();
        }

    }
}
