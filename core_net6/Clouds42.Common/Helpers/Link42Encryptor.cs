﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Clouds42.Common.Helpers
{
public static class Link42Encryptor
    {
        private const int Keysize = 256;
        private const string InitVector = "tu89geji340t89u2";
        private const string PassPhrase = "talk-with-1c";

        public static string EncryptProtected(string input)
        {
            var result = Encrypt(input);
            return Escaper.ReplaceEscapes(result);
        }

        public static string DecryptProtected(string input)
        {

            var result = Escaper.RestoreEscapes(input);
            return Decrypt(result);
        }
        public static string Encrypt(string input)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;

            var initVectorBytes = Encoding.UTF8.GetBytes(InitVector);
            var plainTextBytes = Encoding.UTF8.GetBytes(input);
            var password = new PasswordDeriveBytes(PassPhrase, null);
            var keyBytes = password.GetBytes(Keysize / 8);
            var symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            var cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static string Decrypt(string input)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;

            var initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
            var cipherTextBytes = Convert.FromBase64String(input);
            var password = new PasswordDeriveBytes(PassPhrase, null);
            var keyBytes = password.GetBytes(Keysize / 8);
            var symmetricKey = new RijndaelManaged {Mode = CipherMode.CBC};
            var decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            var plainTextBytes = new byte[cipherTextBytes.Length];
            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }
    }

    public static class StringCompressor
    {
        public static string Compress(string text)
        {
            var buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Escaper.ReplaceEscapes(Convert.ToBase64String(gZipBuffer));
        }

        public static string Decompress(string compressedText)
        {
            var tempstr = Escaper.RestoreEscapes(compressedText);
            var gZipBuffer = Convert.FromBase64String(tempstr);
            using var memoryStream = new MemoryStream();
            var dataLength = BitConverter.ToInt32(gZipBuffer, 0);
            memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

            var buffer = new byte[dataLength];

            memoryStream.Position = 0;
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
            {
                gZipStream.Read(buffer, 0, buffer.Length);
            }

            return Encoding.UTF8.GetString(buffer);
        }
    }

    public static class Escaper
    {
        private const string Equal = "_eq_";
        private const string Plus = "_pl_";
        private const string Minus = "_mi_";
        private const string Quest = "_qu_";
        private const string Amp = "_am_";
        private const string Sharp = "_sh_";
        private static readonly Dictionary<string, string> Escapes = new()
        {
            {"=", Equal},
            {"+", Plus},
            {"-", Minus},
            {"?", Quest},
            {"&", Amp},
            {"#", Sharp}
        };

        public static string ReplaceEscapes(string str) =>
            Escapes.Aggregate(str, (current, item) => current.Replace(item.Key, item.Value));

        public static string RestoreEscapes(string str) =>
            Escapes.Aggregate(str, (current, item) => current.Replace(item.Value, item.Key));
    }
}