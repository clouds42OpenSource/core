﻿using System.Linq;

namespace Clouds42.Common.DataModels
{
    /// <summary>
    /// Модель, содержащая в себе информацию о том, были ли
    /// внесены изменения в процессе обновления объекта и сообщения об изменениях
    /// </summary>
    public class UpdateObjectDataInfo
    {
        /// <summary>
        /// Были ли внесены какие-то изменения
        /// </summary>
        public bool WereChangesMade { get; set; }

        /// <summary>
        /// Сообщения, полученные в ходе обновления
        /// </summary>
        public string[] Messages { get; set; }

        /// <summary>
        /// Конструктор для класса <see cref="UpdateObjectDataInfo"/>
        /// </summary>
        public UpdateObjectDataInfo(bool wereChangesMade, string[] messages)
        {
            WereChangesMade = wereChangesMade;
            Messages = messages;
        }

        /// <summary>
        /// Создать модель с информацией о наличии изменений
        /// </summary>
        /// <param name="message">Опциональное сообщение о наличии изменения</param>
        /// <returns>Модель с информацией о наличии изменений</returns>
        public static UpdateObjectDataInfo CreateWithoutChanges(string message = null) =>
            Create(false, message);

        /// <summary>
        /// Создать модель с информацией об отсутствии изменений
        /// </summary>
        /// <param name="message">Опциональное сообщение об отсутствии изменения</param>
        /// <returns>Модель с информацией об отсутствии изменений</returns>
        public static UpdateObjectDataInfo CreateWithChanges(string message = null) =>
            Create(true, message);

        /// <summary>
        /// Аккумулировать в себе результат другого обновления
        /// </summary>
        /// <param name="updateInfo">Результат другого обновления</param>
        /// <returns>Аккумулированный результат текущего и другого обновления</returns>
        public UpdateObjectDataInfo Absorb(UpdateObjectDataInfo updateInfo)
        {
            WereChangesMade |= updateInfo.WereChangesMade;
            Messages = Messages.Concat(updateInfo.Messages).ToArray();
            return this;
        }

        /// <summary>
        /// Создать модель с опциональным сообщением
        /// </summary>
        /// <returns>Модель с опциональным сообщением</returns>
        private static UpdateObjectDataInfo Create(bool wereChangesMade, string message = null) =>
            new(wereChangesMade, message == null ? [] : [message]);
    }
}
