﻿using System.Collections.Generic;

namespace Clouds42.Common.DataModels
{
    /// <summary>
    /// Результат валидации данных пользователя
    /// </summary>
    public class AccountUserDataValidationResultDto
    {
        /// <summary>
        /// Признак что данные валидны
        /// </summary>
        public bool DataIsValid { get; set; }

        /// <summary>
        /// Список валидационных сообщений
        /// </summary>
        public List<string> ValidationMessages { get; set; } = [];
    }
}
