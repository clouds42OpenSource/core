﻿using System.Runtime.Serialization;

namespace Clouds42.Common.DataModels
{
    /// <summary>
    /// Модель содержащая публичные URL адреса
    /// </summary>
    public sealed class PublicUrlsDto
    {
        /// <summary>
        /// URL на промо сайт
        /// </summary>
        [DataMember(Name = "promoSiteUrl")]
        public string PromoSiteUrl { get; set; }
    }
}