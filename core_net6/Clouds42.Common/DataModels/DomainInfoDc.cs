﻿namespace Clouds42.Common.DataModels
{
    /// <summary>
    /// Информация о домене
    /// </summary>
    public class DomainInfoDc
    {
        /// <summary>
        /// Название хоста
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// Регистриуемый домен
        /// </summary>
        public string RegistrableDomain { get; set; }
    }
}
