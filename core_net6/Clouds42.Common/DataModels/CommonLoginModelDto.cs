﻿
namespace Clouds42.Common.DataModels
{
    public class CommonLoginModelDto
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
