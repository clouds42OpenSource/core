﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Access;

namespace Clouds42.Common.DataModels
{
    /// <summary>
    /// Модель пользователя
    /// </summary>
    public interface IUserPrincipalDto
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        Guid Id { get; set; }
        
        /// <summary>
        /// Токен пользователя
        /// </summary>
        Guid Token { get; set; }
        
        /// <summary>
        /// ID аккаунта
        /// </summary>
        Guid RequestAccountId { get; set; }

        /// <summary>
        /// Группы пользователя
        /// </summary>
        List<AccountUserGroup> Groups { get; set; }
        
        /// <summary>
        /// Имя пользователя
        /// </summary>
        string Name { get; set; }
        
        /// <summary>
        /// ID аккаунта в контексте
        /// </summary>
        Guid ContextAccountId { get; set; }
        
        /// <summary>
        /// Номер телефона
        /// </summary>
        string PhoneNumber { get; set; }
        
        /// <summary>
        /// Почта пользователя
        /// </summary>
        string Email { get; set; }

        bool? IsEmailVerified { get; set; }

        bool? IsPhoneVerified { get; set; }

        bool IsNew { get; set; }

        string Locale { get; set; }

        string UserSource { get; set; }

        bool IsService { get; set; }

        int? Deployment { get; set; }

        /// <summary>
        /// Проверить что есть роль только "Пользователь"
        /// </summary>
        /// <returns>true - если установлена только роль "Пользователь"</returns>
        bool IsAccountUser();

        string FirstName { get; set; }
    }
}
