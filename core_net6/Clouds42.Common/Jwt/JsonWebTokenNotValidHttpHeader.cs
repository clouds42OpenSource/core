﻿namespace Clouds42.Common.Jwt
{
    /// <summary>
    /// Http заголовки для JsonWebToken
    /// </summary>
    public static class JsonWebTokenNotValidHttpHeader
    {
        /// <summary>
        /// Header Token-Expired выставляется если у переданного JsonWebToken время жизни истекло
        /// </summary>
        public const string TokenExpiredHeader = "Token-Expired";

        /// <summary>
        /// Header Invalid-Token выставляется если переданный JsonWebToken невалидный
        /// </summary>
        public const string InvalidTokenHeader = "Invalid-Token";
    }
}