﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Clouds42.Common.CustomStructure
{
    /// <summary>
    /// Потокобезопасный сэт.
    /// </summary>
    /// <typeparam name="T">Тип содержимого коллекции.</typeparam>
    public sealed class ConcurrentHashSet<T> : IDisposable
    {
        private readonly ReaderWriterLockSlim _lock = new(LockRecursionPolicy.SupportsRecursion);
        private readonly HashSet<T> _hashSet = [];

        /// <summary>
        /// Добавить элемент в коллекцию.
        /// </summary>
        public bool Add(T item)
        {
            try
            {
                _lock.EnterWriteLock();
                return _hashSet.Add(item);
            }
            finally
            {
                if (_lock.IsWriteLockHeld) _lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Очистить коллекцию.
        /// </summary>
        public void Clear()
        {
            try
            {
                _lock.EnterWriteLock();
                _hashSet.Clear();
            }
            finally
            {
                if (_lock.IsWriteLockHeld) _lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Проверить входит ли элемент в коллекцию.
        /// </summary>
        public bool Contains(T item)
        {
            try
            {
                _lock.EnterReadLock();
                return _hashSet.Contains(item);
            }
            finally
            {
                if (_lock.IsReadLockHeld) _lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Удалить элемент из коллекции.
        /// </summary>
        public bool Remove(T item)
        {
            try
            {
                _lock.EnterWriteLock();
                return _hashSet.Remove(item);
            }
            finally
            {
                if (_lock.IsWriteLockHeld) _lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Количество элементов коллекции.
        /// </summary>
        public int Count
        {
            get
            {
                try
                {
                    _lock.EnterReadLock();
                    return _hashSet.Count;
                }
                finally
                {
                    if (_lock.IsReadLockHeld) _lock.ExitReadLock();
                }
            }
        }

        /// <summary>
        /// Завершить использование коллекции и освободить ресурсы.
        /// </summary>
        public void Dispose()
        {
            _lock?.Dispose();
        }

        /// <summary>
        /// Получить список всем элментов коллекции.
        /// </summary>
        /// <returns>Список элементов коллекции.</returns>
        public List<T> GetAll()
        {
            return _hashSet.ToList();
        }

    }

}
