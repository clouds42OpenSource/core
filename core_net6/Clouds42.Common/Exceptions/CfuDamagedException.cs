using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение при поврежденном CFU пакете
    /// </summary>
    [Serializable]
    public class CfuDamagedException : Exception
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public CfuDamagedException()
        {
        }

        /// <inheritdoc />
        public CfuDamagedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected CfuDamagedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}