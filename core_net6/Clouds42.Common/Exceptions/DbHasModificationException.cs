﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение при наличии доработок в инф. базе
    /// </summary>
    [Serializable]
    public class DbHasModificationException : Exception
    {
        /// <inheritdoc />
        public DbHasModificationException(string message) : base(message) { }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected DbHasModificationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}