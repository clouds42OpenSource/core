﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение воркера
    /// </summary>
    [Serializable]
    public class CoreWorkerException : Exception
    {
        /// <inheritdoc />
        public CoreWorkerException()
        {
        }

        /// <inheritdoc />
        public CoreWorkerException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public CoreWorkerException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected CoreWorkerException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    [Serializable]
    public class RepositoryException : Exception
    {
        public RepositoryException()
        {
        }

        public int Number { get; }
        public RepositoryException(Exception innerException, int number) : base(innerException.Message, innerException)
        {
            Number = number;
        }
    }
}