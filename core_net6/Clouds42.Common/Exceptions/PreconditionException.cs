﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions 
{
    /// <summary>
    /// Исключение при ошибке выполнения условия
    /// </summary>
    [Serializable]
    public class PreconditionException : Core42Exception
    {
        /// <inheritdoc />
        public PreconditionException()
        {
        }

        /// <inheritdoc />
        public PreconditionException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public PreconditionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected PreconditionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}