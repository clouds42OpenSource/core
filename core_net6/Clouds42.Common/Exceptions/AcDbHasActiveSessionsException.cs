﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение при наличии активных сессий в инф. базе
    /// </summary>
    [Serializable]
    public class AcDbHasActiveSessionsException : Exception
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">Сообщение об ошибке</param>
        public AcDbHasActiveSessionsException(string message) : base(message)
        {

        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected AcDbHasActiveSessionsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
