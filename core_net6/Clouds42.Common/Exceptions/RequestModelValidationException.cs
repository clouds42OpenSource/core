﻿using System;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using Clouds42.Common.Helpers;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Ошибка, которая говорит о том, что модель запроса не валидна
    /// </summary>
    [Serializable]
    public class RequestModelValidationException : Exception
    {
        /// <summary>
        /// Содержит ошибки валидации модели запроса
        /// </summary>
        public string ValidationErrors { get; private set; }
        /// <summary>
        /// Содержит Content-Type для свойства <see cref="ValidationErrors"/>
        /// </summary>
        public MediaTypeHeaderValue ValidationErrorContentType { get; private set; }

        /// <summary>
        /// Создание инстенса <see cref="RequestModelValidationException"/>
        /// </summary>
        /// <param name="validationErrors">Ошибки валидации модели запроса</param>
        /// <param name="validationErrorContentType">Content-Type данных ошибки <paramref name="validationErrors"/> </param>
        public RequestModelValidationException(string validationErrors, MediaTypeHeaderValue validationErrorContentType)
            : base($"{WebHeaders.ValidationModelFailedWebHeader}, валидация модели запроса не прошла.")
        {
            ValidationErrors = validationErrors;
            ValidationErrorContentType = validationErrorContentType;
        }

        /// <summary>
        /// Конструктор для сериализации
        /// </summary>
        /// <param name="info">Хранит информацию о данных сериализации</param>
        /// <param name="context">Контекст сериализации</param>
        protected RequestModelValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            ValidationErrors = info.GetString(nameof(ValidationErrors));
            ValidationErrorContentType = (MediaTypeHeaderValue) info.GetValue(nameof(ValidationErrorContentType), typeof(MediaTypeHeaderValue));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(nameof(ValidationErrors), ValidationErrors);
            info.AddValue(nameof(ValidationErrorContentType), ValidationErrorContentType, typeof(MediaTypeHeaderValue));

            base.GetObjectData(info, context);
        }
    }
}