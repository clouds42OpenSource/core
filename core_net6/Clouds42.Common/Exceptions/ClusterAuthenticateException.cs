﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение авториации в кластер 1С.
    /// </summary>
    [Serializable]
    public class ClusterAuthenticateException : Exception
    {
        public ClusterAuthenticateException(string message) : base(message)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected ClusterAuthenticateException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
