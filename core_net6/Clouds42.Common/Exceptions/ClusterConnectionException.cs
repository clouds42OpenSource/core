﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение подключения к кластеру 1С.
    /// </summary>
    [Serializable]
    public class ClusterConnectionException : Exception
    {
        public ClusterConnectionException(string message) : base(message)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected ClusterConnectionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
