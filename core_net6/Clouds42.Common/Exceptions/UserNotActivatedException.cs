﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение в случае если пользователь не активирован
    /// </summary>
    [Serializable]
    public class UserNotActivatedException : Core42Exception
    {
        /// <inheritdoc />
        public UserNotActivatedException()
        {
        }

        /// <inheritdoc />
        public UserNotActivatedException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public UserNotActivatedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected UserNotActivatedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}