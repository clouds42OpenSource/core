﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение при ошибке валидации
    /// </summary>
    [Serializable]
    public class ValidateException : Exception
    {
        /// <inheritdoc />
        public ValidateException()
        {
        }

        /// <inheritdoc />
        public ValidateException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected ValidateException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
