﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение при отсутствии запрашиваемой сущности
    /// </summary>
    [Serializable]
    public class NotFoundException : Core42Exception
    {
        /// <inheritdoc />
        public NotFoundException()
        {
        }

        /// <inheritdoc />
        public NotFoundException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected NotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
