﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение при неуспешном подключении через веб сокет
    /// </summary>
    [Serializable]
    public class WebSocketAuthenticationException : Exception
    {
        public WebSocketAuthenticationException(string message) : base(message) { }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected WebSocketAuthenticationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
