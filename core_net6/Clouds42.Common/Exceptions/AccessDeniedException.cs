﻿using System;
using System.Runtime.Serialization;
using Clouds42.Common.DataModels;
using Clouds42.Domain.Access;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение при ошибке доступа
    /// </summary>
    [Serializable]
    public class AccessDeniedException : Exception
    {
        /// <summary>
        /// Тип запрашиваемого действия
        /// </summary>
        public ObjectAction ObjectAction { get; }

        /// <summary>
        /// Причина
        /// </summary>
        public string ReasonMsg { get; }

        /// <summary>
        /// Пользователь права которого праоверяются
        /// </summary>
        public IUserPrincipalDto User { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="objectAction">Тип запрашиваемого действия</param>
        /// <param name="reasonMsg">Причина</param>
        public AccessDeniedException(ObjectAction objectAction, string reasonMsg, IUserPrincipalDto user)
            : base(reasonMsg)
        {
            ObjectAction = objectAction;
            ReasonMsg = reasonMsg;
            User = user;
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected AccessDeniedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
