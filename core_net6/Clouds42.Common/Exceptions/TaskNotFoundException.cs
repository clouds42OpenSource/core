﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions
{
    /// <summary>
    /// Исключение в случае если задача не найдена
    /// </summary>
    [Serializable]
    public class TaskNotFoundException : CoreWorkerException
    {
        /// <inheritdoc />
        public TaskNotFoundException()
        {
        }

        /// <inheritdoc />
        public TaskNotFoundException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public TaskNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected TaskNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
