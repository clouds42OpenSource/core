using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions 
{
    /// <summary>
    /// Исключение при ошибке авторизации
    /// </summary>
    [Serializable]
    public class NotAuthorizedException : Core42Exception
    {
        /// <inheritdoc />
        public NotAuthorizedException()
        {
        }

        /// <inheritdoc />
        public NotAuthorizedException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public NotAuthorizedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected NotAuthorizedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}