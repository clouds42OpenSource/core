﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.Common.Exceptions 
{
    /// <summary>
    /// Базовый тип исключения ядра
    /// </summary>
    [Serializable]
    public class Core42Exception : Exception
    {
        /// <inheritdoc />
        public Core42Exception()
        {
        }

        /// <inheritdoc />
        public Core42Exception(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public Core42Exception(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected Core42Exception(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}