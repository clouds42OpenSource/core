﻿namespace Clouds42.Common.PdfDocuments
{
    /// <summary>
    /// Константы для PDF документа
    /// </summary>
    public static class PdfDocumentConstants
    {
        /// <summary>
        /// Отступ сверху для страницы по умолчанию
        /// </summary>
        public const int MarginTopDefault = 0;

        /// <summary>
        /// Отступ снизу для страницы по умолчанию
        /// </summary>
        public const int MarginBottomDefault = 0;

        /// <summary>
        /// Отступ слева для страницы по умолчанию
        /// </summary>
        public const int MarginLeftDefault = 0;

        /// <summary>
        /// Отступ справа для страницы по умолчанию
        /// </summary>
        public const int MarginRightDefault = 0;

        /// <summary>
        /// Отступ снизу для отчета агента
        /// </summary>
        public const int MarginBottomForAgentReport = 30;

        /// <summary>
        /// Отступ сверху для отчета агента
        /// </summary>
        public const int MarginTopForAgentReport = 30;
    }
}
