﻿namespace Clouds42.Common.PdfDocuments
{
    /// <summary>
    /// Параметры PDF документа
    /// </summary>
    public class PdfDocumentParams
    {
        /// <summary>
        /// Отступ сверху для страницы
        /// </summary>
        public int MarginTop { get; set; } = PdfDocumentConstants.MarginTopDefault;

        /// <summary>
        /// Отступ снизу для страницы
        /// </summary>
        public int MarginBottom { get; set; } = PdfDocumentConstants.MarginBottomDefault;

        /// <summary>
        /// Отступ слева для страницы
        /// </summary>
        public int MarginLeft { get; set; } = PdfDocumentConstants.MarginLeftDefault;

        /// <summary>
        /// Отступ справа для страницы
        /// </summary>
        public int MarginRight { get; set; } = PdfDocumentConstants.MarginRightDefault;
    }
}
