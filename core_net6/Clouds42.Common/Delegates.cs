﻿using System.Linq;
using Clouds42.Domain.Enums;

namespace Clouds42.Common
{
    /// <summary>
    /// Делегат для сортитровки записей
    /// </summary>
    /// <typeparam name="TDataItem">Тип данных на сортировку</typeparam>
    /// <param name="records">Записи для сортировки</param>
    /// <param name="sortType">направление сортировки</param>
    /// <returns>Отсортированные записи</returns>
    public delegate IOrderedQueryable<TDataItem> SortingDelegateQueryable<TDataItem>(IQueryable<TDataItem> records, SortType sortType);

    /// <summary>
    /// Делегат для сортитровки записей
    /// </summary>
    /// <typeparam name="TDataItem">Тип данных на сортировку</typeparam>
    /// <param name="records">Записи для сортировки</param>
    /// <param name="sortType">направление сортировки</param>
    /// <returns>Отсортированные записи</returns>
    public delegate IOrderedQueryable<TDataItem> SortingDelegateEnumerable<TDataItem>(IQueryable<TDataItem> records, SortType sortType);
}