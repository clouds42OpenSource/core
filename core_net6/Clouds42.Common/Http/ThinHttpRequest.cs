﻿using System.Collections.Specialized;

namespace Clouds42.Common.Http
{
    /// <summary>
    /// Объект представляющий объект Request как минимальная его копия
    /// </summary>
    public sealed class ThinHttpRequest
    {
        /// <summary>
        /// Заголовки запроса
        /// </summary>
        public NameValueCollection Headers { get; }

        /// <summary>
        /// Переданные файлы в запросе
        /// </summary>
        public UploadedFilesCollection Files { get; }

        /// <summary>
        /// Конструктор класса <see cref="ThinHttpRequest"/>
        /// </summary>
        /// <param name="headers">Заголовки запроса</param>
        /// <param name="files">Переданные файлы в запросе</param>
        public ThinHttpRequest(NameValueCollection headers, UploadedFilesCollection files)
        {
            Headers = headers;
            Files = files;
        }
    }
}