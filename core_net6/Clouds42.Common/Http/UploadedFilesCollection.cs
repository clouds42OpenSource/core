﻿using System.Collections;
using System.Collections.Generic;

namespace Clouds42.Common.Http
{
    /// <summary>
    /// Класс для хранения потоков загружённых файлов и заголовков запроса 
    /// </summary>
    public sealed class UploadedFilesCollection : IEnumerable<UploadedFileItem>
    {
        /// <summary>
        /// Потоки загружённых файлов
        /// </summary>
        private readonly IEnumerable<UploadedFileItem> _uploadedFiles;

        /// <summary>
        /// Конструктор класса <see cref="UploadedFilesCollection"/>
        /// </summary>
        /// <param name="uploadedFiles">Переданные файлы в запросе</param>
        public UploadedFilesCollection(IEnumerable<UploadedFileItem> uploadedFiles)
        {
            _uploadedFiles = uploadedFiles ?? new UploadedFileItem[0];
        }
        
        /// <summary>Возвращает Enumerator, который выполняет итерацию по коллекции.</summary>
        /// <returns>Enumerator, который выполняет итерацию по коллекции.</returns>
        public IEnumerator<UploadedFileItem> GetEnumerator() 
            => _uploadedFiles.GetEnumerator();

        /// <summary>Возвращает Enumerator, который выполняет итерацию по коллекции.</summary>
        /// <returns>Enumerator, который выполняет итерацию по коллекции.</returns>
        IEnumerator IEnumerable.GetEnumerator() 
            => GetEnumerator();
    }
}
