﻿using System.IO;

namespace Clouds42.Common.Http
{
    /// <summary>
    /// Модель загружённого файла
    /// </summary>
    public sealed class UploadedFileItem
    {
        /// <summary>
        /// Поток загружённого файла
        /// </summary>
        public Stream InputStream { get; set; }

        /// <summary>
        /// Размер файла в байтах
        /// </summary>
        public long ContentLength { get; set; }

        /// <summary>
        /// Название загружённого файла
        /// </summary>
        public string FileName { get; set; }
    }
}
