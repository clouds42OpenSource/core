﻿using System.Collections.Generic;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Общий класс провайдера шифровки
    /// </summary>
    public class CommonCryptoProvider
    {
        private readonly Dictionary<CryptoDestinationTypeEnum, ICryptoProvider> _dictionaryCryptoProviders;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public CommonCryptoProvider(IConfiguration configuration)
        {
            _dictionaryCryptoProviders = new Dictionary<CryptoDestinationTypeEnum, ICryptoProvider>
            {
                {CryptoDestinationTypeEnum.CommonAes, new CommonAesCryptoProvider(configuration)},
                {CryptoDestinationTypeEnum.UserAes, new AccountUsersCryptoProvider(configuration)}
            };
        }

        /// <summary>
        /// Шифровать строку
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <param name="providerTypeEnum">Тип шифровки</param>
        /// <returns>Зашифрованная строка</returns>
        public string Encrypt(string value, CryptoDestinationTypeEnum providerTypeEnum) =>
            _dictionaryCryptoProviders[providerTypeEnum].Encrypt(value);

        /// <summary>
        /// Дешифровать строку
        /// </summary>
        /// <param name="value">Строка для дешифрования</param>
        /// <param name="providerTypeEnum">Тип шифровки</param>
        /// <returns>Дешифрованная строка</returns>
        public string Decrypt(string value, CryptoDestinationTypeEnum providerTypeEnum) =>
            _dictionaryCryptoProviders[providerTypeEnum].Decrypt(value);
    }
}
