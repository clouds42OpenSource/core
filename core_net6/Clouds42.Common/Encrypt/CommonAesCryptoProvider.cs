﻿using System;
using System.IO;
using System.Security.Cryptography;
using Clouds42.Common.Constants;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Класс для шифрования и дешифрования по алгоритму AES
    /// </summary>
    public sealed class CommonAesCryptoProvider(IConfiguration configuration)
        : BaseEncryptionProvider(configuration), ICryptoProvider
    {
        /// <summary>
        /// Зашифровать строку
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <returns>Шифр строки</returns>
        public string Encrypt(string value) =>
            AesEncryptType2(value, GetPassword(CommonCryptoProviderConst.Core42CommonPwd).Value);

        /// <summary>
        /// Дешифровать строку
        /// </summary>
        /// <param name="value">Строка для дешифрования</param>
        /// <returns>Дешифрованная строка</returns>
        public string Decrypt(string value)
        {
            var bytesValueForDecrypt = Convert.FromBase64String(value);
            var cryptoKeyBytes = GetPassword(CommonCryptoProviderConst.Core42CommonPwd).Value;

            using var aesCryptoProvider = new AesCryptoServiceProvider
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7,
                Key = SHA256.Create().ComputeHash(cryptoKeyBytes),
                IV = MD5.Create().ComputeHash(cryptoKeyBytes)
            };
            using MemoryStream msDecrypt = new MemoryStream(bytesValueForDecrypt);
            using CryptoStream csDecrypt =
                new CryptoStream(msDecrypt, aesCryptoProvider.CreateDecryptor(), CryptoStreamMode.Read);
            using StreamReader srDecrypt = new StreamReader(csDecrypt);
            return srDecrypt.ReadToEnd();
        }
    }
}
