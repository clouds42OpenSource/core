﻿namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Интерфейс для генерации и верефикации хеш суммы
    /// </summary>
    public interface IHashProvider
    {
        /// <summary>
        /// Подсчитать хеш сумму для строки
        /// </summary>
        /// <param name="value">Для какой строки подсчитать хеш сумму</param>
        /// <returns>Хеш сумма</returns>
        string ComputeHash(string value);

        /// <summary>
        /// Проверить хеш сумму на валидность
        /// </summary>
        /// <param name="value">Строка для которой создавалась хеш сумма</param>
        /// <param name="hash">Хеш сумма для верефикации</param>
        /// <returns><c>true</c>, если хеш сумма валидна, иначе <c>false</c></returns>
        bool VerifyHash(string value, string hash);
    }
}