﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Класс для генерации и верефикации MD5 хеш суммы
    /// </summary>
    public sealed class Md5HashProvider : IHashProvider
    {
        /// <summary>
        /// Подсчитать хеш сумму для строки
        /// </summary>
        /// <param name="value">Для какой строки подсчитать хеш сумму</param>
        /// <returns>Хеш сумма</returns>
        public string ComputeHash(string value)
        {
            using var md5Hash = MD5.Create();
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(value));

            var sBuilder = new StringBuilder();

            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// Проверить хеш сумму на валидность
        /// </summary>
        /// <param name="value">Строка для которой создавалась хеш сумма</param>
        /// <param name="hash">Хеш сумма для верефикации</param>
        /// <returns><c>true</c>, если хеш сумма валидна, иначе <c>false</c></returns>
        public bool VerifyHash(string value, string hash)
        {
            var hashOfInput = ComputeHash(value);

            var comparer = StringComparer.OrdinalIgnoreCase;

            return comparer.Compare(hashOfInput, hash) == 0;
        }
    }
}