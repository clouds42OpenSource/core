﻿using System;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Класс, обеспечивающий поддержку шифрования по алгоритму AES
    /// </summary>
    public sealed class AesEncryptionProvider(IConfiguration configuration)
        : BaseEncryptionProvider(configuration), IEncryptionProvider
    {
        /// <summary>
        /// Зашифровать строку
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <returns>Шифр строки</returns>
        public string Encrypt(string value)
            => EncryptType2(value, GetPassword().Value);

        /// <summary>
        /// Шифрование строки.
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <param name="cryptoKeyBytes">Набор байтов ключа шифрования</param>
        /// <returns>Шифр строки</returns>
        private static string EncryptType2(string value, byte[] cryptoKeyBytes)
        {
            using var aesCryptoProvider = new AesCryptoServiceProvider
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7,
                Key = SHA256.Create().ComputeHash(cryptoKeyBytes),
                IV = MD5.Create().ComputeHash(cryptoKeyBytes)
            };
            using var encryptor = aesCryptoProvider.CreateEncryptor();
            var bytes = Encoding.UTF8.GetBytes(value);
            var resultBytes = encryptor.TransformFinalBlock(bytes, 0, bytes.Length);
            return Convert.ToBase64String(resultBytes);
        }
    }
}
