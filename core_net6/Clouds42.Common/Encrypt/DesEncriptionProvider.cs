﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Класс, обеспечивающий поддержку шифрования по алгоритму DES
    /// </summary>
    public sealed class DesEncryptionProvider(IConfiguration configuration)
        : BaseEncryptionProvider(configuration), IEncryptionProvider, ICryptoProvider
    {
        /// <summary>
        /// Зашифровать строку
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <returns>Шифр строки</returns>
        public string Encrypt(string value)
        {
            return EncryptType2(value, GetPassword().Value, GetPassword().Value);
        }

        /// <summary>
        /// Дешифровать строку
        /// </summary>
        /// <param name="value">Строка для дешифрования в base64</param>
        /// <returns>Дешифрованная строка</returns>
        public string Decrypt(string value)
        {
            var byteArray = GetPassword().Value;
            var passwordBytes = Convert.FromBase64String(value);
            using var desCryptoProvider = new DESCryptoServiceProvider
            {
                Mode = CipherMode.ECB,
                Key = byteArray,
                IV = byteArray
            };
            using var cryptoTransform = desCryptoProvider.CreateDecryptor(byteArray, byteArray);
            CryptoStreamMode mode = CryptoStreamMode.Write;
            MemoryStream memStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memStream, cryptoTransform, mode);
            cryptoStream.Write(passwordBytes, 0, passwordBytes.Length);
            cryptoStream.FlushFinalBlock();

            return Encoding.Default.GetString(memStream.ToArray());
        }

        /// <summary>
        /// Шифрование строки.
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <param name="key">Ключ шифрования</param>
        /// <param name="iv">Вектор инициализации</param>
        /// <returns>Шифр строки</returns>
        private static string EncryptType2(string value, byte[] key, byte[] iv)
        {
            using var desCryptoProvider = new DESCryptoServiceProvider
            {
                Mode = CipherMode.ECB,
                Key = key,
                IV = iv
            };
            using var encryptor = desCryptoProvider.CreateEncryptor();
            var bytes = Encoding.UTF8.GetBytes(value);
            var resultBytes = encryptor.TransformFinalBlock(bytes, 0, bytes.Length);

            return Convert.ToBase64String(resultBytes);
        }
    }
}
