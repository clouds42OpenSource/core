﻿using System;
using System.Security.Cryptography;

namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Криптостойкий генератор псевдослучайных чисел
    /// </summary>
    public class CryptoRandomGeneric : Random
    {
        private readonly RNGCryptoServiceProvider _rng = new();

        /// <summary>
        /// Выбирает произвольное число со знаком
        /// </summary>
        public override int Next()
        {
            var array = new byte[sizeof(int)];
            _rng.GetBytes(array);
            var result = BitConverter.ToInt32(array, 0);
            return result;
        }

        /// <summary>
        /// Выбирает случайное число в заданном диапазоне
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        public override int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(maxValue), @"Максимальное число меньше минимального числа");

            return (int)Math.Floor(minValue + (maxValue - minValue) * NextDouble());
        }

        /// <summary>
        /// Выбирает случайное число, которое не больше заданного числа
        /// </summary>
        /// <param name="maxValue"></param>
        public override int Next(int maxValue)
        {
            return Next(0, maxValue);
        }

        /// <summary>
        /// Выбирает случайное число без знака с плавающей точкой
        /// </summary>
        public override double NextDouble()
        {
            var array = new byte[sizeof(uint)];
            _rng.GetBytes(array);
            var randUInt = BitConverter.ToUInt32(array, 0);
            return randUInt / (uint.MaxValue + 1.0);
        }
    }
}
