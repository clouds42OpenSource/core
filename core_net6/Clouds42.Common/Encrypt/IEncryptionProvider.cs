﻿namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Интерфейс для шифравания
    /// </summary>
    public interface IEncryptionProvider
    {
        /// <summary>
        /// Шифравать строку
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <returns>Зашифрованная строка</returns>
        string Encrypt(string value);
    }
}