﻿using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using Clouds42.Common.Constants;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Базовый провайдер шифрования
    /// </summary>
    public abstract class BaseEncryptionProvider
    {
        private readonly IConfiguration _configuration;
        /// <summary>
        /// Конструктор для класса <see cref="BaseEncryptionProvider"/>
        /// </summary>
        protected BaseEncryptionProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Получить ключ 
        /// </summary>
        /// <param name="nameEnvironmentPwd">Название перемены окружения для мастер ключа</param>
        /// <returns></returns>
        protected Lazy<byte[]> GetPassword(string nameEnvironmentPwd = null)
        {
            var core42Pwd = string.IsNullOrEmpty(nameEnvironmentPwd)
                ? CommonCryptoProviderConst.Core42TeamPwd
                : nameEnvironmentPwd;

            return new Lazy<byte[]>(() =>
            {
                var core42TeamPwd = GetCore42TeamPwdFromEnvironmentVariablesOrThrowException(core42Pwd);
                return Encoding.UTF8.GetBytes(core42TeamPwd);
            });
        }

        /// <summary>
        /// Шифрование строки по алгоритму AES.
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <param name="cryptoKeyBytes">Набор байтов ключа шифрования</param>
        /// <returns>Шифр строки</returns>
        protected string AesEncryptType2(string value, byte[] cryptoKeyBytes)
        {
            using var aesCryptoProvider = new AesCryptoServiceProvider
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7,
                Key = SHA256.Create().ComputeHash(cryptoKeyBytes),
                IV = MD5.Create().ComputeHash(cryptoKeyBytes)
            };
            using var encryption = aesCryptoProvider.CreateEncryptor();
            var bytes = Encoding.UTF8.GetBytes(value);
            var resultBytes = encryption.TransformFinalBlock(bytes, 0, bytes.Length);
            return Convert.ToBase64String(resultBytes);
        }

        /// <summary>
        /// Получить значение мастер пароля из переменных окружения или выбросить исключение
        /// </summary>
        /// /// <param name="nameEnvironmentPwd">Название перемены окружения для мастер ключа</param>
        /// <returns>Значение мастер пароля из переменных окружения</returns>
        private string GetCore42TeamPwdFromEnvironmentVariablesOrThrowException(string nameEnvironmentPwd)
        {
            var value = Environment.GetEnvironmentVariable(nameEnvironmentPwd, EnvironmentVariableTarget.User);

            if (!string.IsNullOrEmpty(value))
            {
                return value;
            }

            value = _configuration.GetRequiredSection(nameEnvironmentPwd)?.Value;

            if(string.IsNullOrEmpty(value))
            {
                throw new ConfigurationErrorsException(
                    $"Система сконфигурирована не верно. Отсутсвует значение для ключа {nameEnvironmentPwd}");
            }

            return value;

        }
    }
}
