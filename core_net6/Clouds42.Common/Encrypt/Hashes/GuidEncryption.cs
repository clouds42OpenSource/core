﻿using System;
using System.Linq;
using System.Text;

namespace Clouds42.Common.Encrypt.Hashes
{
    /// <summary>
    /// 
    /// </summary>
    public static class GuidEncryption
    {
        private const int ShiftStep = 10;
        private static readonly int[] RandomCharPositions = [5, 10, 15];
        /// <summary>
        ///   Преобразует Guid в строку base64:
        /// </summary>
        public static string Encrypt(Guid id)
        {
            var tmp = id.ToString().Replace("-", "").ToCharArray();
            Array.Reverse(tmp);
            var s = new string(tmp);
            s = ShiftStringRight(s);
            s = AddRandomChars(s, true);
            var buffer = Encoding.ASCII.GetBytes(s);
            var base64 = Convert.ToBase64String(buffer); // SGVsbG8sIFdvcmxk
            base64 = AddRandomChars(base64, false);
            return base64;
        }

        /// <summary>
        /// Из base64 строки получает Guid        
        /// </summary>
        public static Guid? Decrypt(string hash)
        {
            hash = RemoveRandomChars(hash);
            var buffer = Convert.FromBase64String(hash);
            var text = Encoding.ASCII.GetString(buffer); // Hello, World
            text = RemoveRandomChars(text);
            text = ShiftStringLeft(text);
            var tmp = text.ToCharArray();
            Array.Reverse(tmp);
            var s = new string(tmp);

            Guid.TryParse(s, out var g);

            return g;
        }
        private static string AddRandomChars(string text, bool isHexChar = false)
        {
            var sb = new StringBuilder(text);
            foreach (var position in RandomCharPositions)
                sb.Insert(position, GetRandomChar(isHexChar));
            return sb.ToString();
        }

        private static string RemoveRandomChars(string text)
        {
            var reversedCharPositions = RandomCharPositions.Reverse().ToArray();
            return reversedCharPositions.Aggregate(text, (current, position) => current.Remove(position, 1));
        }

        private static char GetRandomChar(bool isHexChar = false)
        {
            var chars = "$%#@!*abcdefghijklmnopqrstuvwxyz1234567890?;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^&".ToCharArray();
            if (isHexChar)
                chars = "abcdef1234567890ABCDEF".ToCharArray();
            var r = new Random();
            var i = r.Next(chars.Length);
            return chars[i];
        }

        private static string ShiftStringRight(string t)
        {
            return t.Substring(ShiftStep, t.Length - ShiftStep) + t.Substring(0, ShiftStep);
        }

        private static string ShiftStringLeft(string t)
        {
            return t.Substring(t.Length - ShiftStep, ShiftStep) + t.Substring(0, t.Length - ShiftStep);
        }
    }
}
