﻿using System;
using System.Text;

namespace Clouds42.Common.Encrypt.Hashes
{
    /// <summary></summary>
    public static class SimpleIdHash
    {
        /// <summary>
        ///   Преобразует Guid в строку base64:
        ///     а) убирает все "-"
        ///     б) полученную строку reverse
        ///     в) результат в base64
        /// </summary>
        public static string GetHashById(Guid id)
        {
            var tmp = id.ToString().Replace("-", "").ToCharArray();
            Array.Reverse(tmp);
            var s = new string(tmp);

            byte[] buffer = Encoding.ASCII.GetBytes(s);
            string base64 = Convert.ToBase64String(buffer); // SGVsbG8sIFdvcmxk

            return base64;
        }


        /// <summary>
        /// Из base64 строки получает Guid        
        /// </summary>
        public static Guid? GetIdByString(string hash)
        {
            try
            {
                byte[] buffer;
                try
                {
                    buffer = Convert.FromBase64String(hash);
                }
                catch
                {
                    var encodedId = Uri.UnescapeDataString(hash);
                    buffer = Convert.FromBase64String(encodedId);
                }
                var text = Encoding.ASCII.GetString(buffer);

                var tmp = text.ToCharArray();
                Array.Reverse(tmp);
                var s = new string(tmp);

                Guid.TryParse(s, out var g);
                return g;
            }
            catch (Exception )
            {
                return Guid.Empty;
            }
        }
    }
}