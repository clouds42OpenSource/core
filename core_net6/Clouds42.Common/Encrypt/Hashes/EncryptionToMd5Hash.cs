﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Clouds42.Common.Encrypt.Hashes
{
    /// <summary>
    /// 
    /// </summary>
    public static class EncryptionToMd5Hash
    {
        /// <summary>
        ///     Генерация подписи в формате md5 в нижнем регистре
        /// </summary>
        /// <param name="stringToCompute">Входящая строка</param>
        /// <returns>Вычисленная подпись</returns>
        public static string ComputeMd5Hash(string stringToCompute)
        {
            if (string.IsNullOrEmpty(stringToCompute))
                throw new InvalidOperationException("Строка пуста или содержит пробелы");

            using var md5Hash = new MD5CryptoServiceProvider();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(stringToCompute));
            var signature = new StringBuilder();
            foreach (byte b in data)
            {
                signature.Append(b.ToString("x2"));
            }

            return signature.ToString();
        }

        /// <summary>
        ///     Генерация случайной строки
        /// </summary>
        /// <returns>Строка из 8-ми символов</returns>
        public static string GenerateRandomString()
        {
            return Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 8);
        }
    }
}
