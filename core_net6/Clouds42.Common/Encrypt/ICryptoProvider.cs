﻿namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Интерфейс шифрования
    /// </summary>
    public interface ICryptoProvider
    {
        /// <summary>
        /// Шифровать строку
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <returns>Зашифрованная строка</returns>
        string Encrypt(string value);

        /// <summary>
        /// Дешифровать строку
        /// </summary>
        /// <param name="value">Строка для дешифрования</param>
        /// <returns>Дешифрованная строка</returns>
        string Decrypt(string value);
    }
}