﻿using Microsoft.Extensions.Configuration;

namespace Clouds42.Common.Encrypt
{
    /// <summary>
    /// Класс для пользователей для шифрования по алгоритму AES
    /// </summary>
    public sealed class AccountUsersCryptoProvider(IConfiguration configuration)
        : BaseEncryptionProvider(configuration), ICryptoProvider
    {
        /// <summary>
        /// Зашифровать строку
        /// </summary>
        /// <param name="value">Строка для шифрования</param>
        /// <returns>Шифр строки</returns>
        public string Encrypt(string value) => AesEncryptType2(value, GetPassword().Value);

        /// <summary>
        /// Дешифровать строку
        /// </summary>
        /// <param name="value">Строка для дешифрования</param>
        /// <returns>Дешифрованная строка</returns>
        public string Decrypt(string value)
        {
            throw new System.NotImplementedException();
        }
    }
}
