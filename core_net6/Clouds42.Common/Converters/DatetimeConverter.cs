﻿using Newtonsoft.Json.Converters;

namespace Clouds42.Common.Converters
{
    public class DatetimeConverter: IsoDateTimeConverter
    {
        public DatetimeConverter()
        {
            DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.ff";
        }
    }
}
