﻿using System;

namespace Clouds42.Common.Cache
{
    public class CacheProviderSelector : ICacheProvider
    {

        private static readonly Lazy<ICacheProvider> Lazy = new(CreateFromSettings);
        public static ICacheProvider Current => Lazy.Value;

        private static ICacheProvider CreateFromSettings()
        {
            return new LocalCacheProvider();
        }

        public void PutObject<T>(string key, T obj, string region)
        {
            Current.PutObject(key, obj, region);
        }

        public bool Exist(string key, string region)
        {
            return Current.Exist(key, region);
        }
    }
}
