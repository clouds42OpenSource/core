﻿
namespace Clouds42.Common.Cache
{
    /// <summary>
    /// Provider для работы с кэшем
    /// </summary>
    public interface ICacheProvider
    {
        /// <summary>
        /// Положить объект в кэш
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">Ключ</param>
        /// <param name="obj">Объект</param>
        /// <param name="region">Область в кэше</param>
        void PutObject<T>(string key, T obj, string region);
        
        /// <summary>
        /// Проверить наличие ключа в кэш
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="region">Область в кэше</param>
        bool Exist(string key, string region);
    }
}
