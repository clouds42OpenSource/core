﻿namespace Clouds42.Common.Cache
{
    public class CacheOptions
    {
        public string CacheType { get; set; }
        public bool EnableCache { get; set; }
    }
}
