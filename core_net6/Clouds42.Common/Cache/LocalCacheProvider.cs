﻿using System.Collections.Concurrent;

namespace Clouds42.Common.Cache
{
    public class LocalCacheProvider : ICacheProvider
    {
        private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, object>> _dictionary = new();

        private const string GenericRegion = "6AA0E6F2-6F82-4D19-B9EF-479E76229EA4";

        public void PutObject<T>(string key, T obj, string region)
        {
            GetRegion(region).AddOrUpdate(key, obj, (k, o) => o);
        }

        public bool Exist(string key, string region)
        {
            return GetRegion(region).ContainsKey(key);
        }

        private ConcurrentDictionary<string, object> GetRegion(string region)
        {
            return _dictionary.GetOrAdd(string.IsNullOrEmpty(region) 
                ? GenericRegion 
                : region,k => new ConcurrentDictionary<string, object>());
        }
    }
}
