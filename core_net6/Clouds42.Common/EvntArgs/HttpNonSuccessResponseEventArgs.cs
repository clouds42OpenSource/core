﻿using System;
using System.Net.Http;

namespace Clouds42.Common.EvntArgs
{
    /// <summary>
    /// Событие для обработки неуспешных http ответов
    /// </summary>
    public sealed class HttpNonSuccessResponseEventArgs : EventArgs
    {
        /// <summary>
        /// Полученный http ответ, который нужно обработать
        /// </summary>
        public HttpResponseMessage ResponseMessage { get; }
        
        /// <summary>
        /// Если <c>true</c>, то запрос будет переотправлен ещё раз
        /// </summary>
        public bool ResendCurrentRequest { get; set; }

        /// <summary>
        /// Инициализация класса <see cref="HttpNonSuccessResponseEventArgs"/>
        /// </summary>
        /// <param name="responseMessage">Http ответ, который нужно обработать</param>
        public HttpNonSuccessResponseEventArgs(HttpResponseMessage responseMessage)
        {
            ResponseMessage = responseMessage;
        }
    }
}