﻿using System.IO;
using System.Linq;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширение для работы с директориями
    /// </summary>
    public static class DirectoryExtensions
    {

        /// <summary>
        ///     Получить размер директории (InBytes)
        /// </summary>
        public static long GetSize(this DirectoryInfo directoryInfo)
        {
            if (!directoryInfo.Exists)
                return 0;

            var fileInfos = directoryInfo.GetFiles();
            var size = fileInfos.Sum(fi => fi.Length);

            var directories = directoryInfo.GetDirectories();
            size += directories.Sum(di => GetSize(di));

            return size;
        }

        /// <summary>
        ///     Получить размер файла (InBytes)
        /// </summary>
        public static long GetSize(this FileInfo fileInfo)
        {
            return fileInfo.Length;
        }

        /// <summary>
        /// Сконвертировать путь к директории в данные о директории
        /// </summary>
        /// <param name="directoryPath">Путь к директории</param>
        /// <returns>Данные о директории</returns>
        public static DirectoryInfo ToDirectoryInfo(this string directoryPath)
            => new(directoryPath);

        /// <summary>
        /// Сконвертировать путь к файлу в данные о файле
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        /// <returns>Данные о файле</returns>
        public static FileInfo ToFileInfo(this string filePath)
            => new(filePath);
    }
}
