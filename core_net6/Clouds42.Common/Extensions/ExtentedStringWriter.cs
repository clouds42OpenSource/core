﻿using System.IO;
using System.Text;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Обертка для типа StringWriter
    /// </summary>
    public sealed class ExtentedStringWriter : StringWriter
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="encoding">Кодировка</param>
        public ExtentedStringWriter(Encoding encoding)
        {
            Encoding = encoding;
        }

        /// <summary>
        /// Кодировка
        /// </summary>
        public override Encoding Encoding { get; }
    }
}