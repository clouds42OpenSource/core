﻿using System;

namespace Clouds42.Common.Extensions
{

    /// <summary>
    /// Расширения для массива.
    /// </summary>
    public static class ArrayExtensions
    {

        /// <summary>
        /// Получить подмножество.
        /// </summary>
        /// <typeparam name="T">Тип элементов массива.</typeparam>
        /// <param name="data">Исходный массив.</param>
        /// <param name="index">Позиция подмножества.</param>
        /// <param name="length">Количество элементов подмножества.</param>
        /// <returns>Полученное подмножество.</returns>
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
    }
}
