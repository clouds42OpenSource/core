﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширения
    /// </summary>
    public static class SerializationExtensions
    {
        /// <summary>
        /// Сконвертировать объект в JSON
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>JSON строка</returns>
        public static string ToJson(this object obj)
        {
            var s = obj as string;
            return s ?? JsonCustomSerializer.SerializeToJson(obj);
        }

        /// <summary>
        /// Десериализовать строку JSON в объект
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="json">JSON строка</param>
        /// <returns>Объект</returns>
        public static T DeserializeFromJson<T>(this string json)
        {
            return JsonCustomSerializer.DeserializeFromJson<T>(json);
        }

        /// <summary>
        /// Десериализовать строку JSON в объект
        /// </summary>
        /// <param name="json">JSON строка</param>
        /// <param name="objectType">Тип объекта</param>
        /// <returns>Объект</returns>
        public static object DeserializeFromJson(this string json, Type objectType)
        {
            return JsonCustomSerializer.DeserializeFromJson(json, objectType);
        }

        /// <summary>
        /// Десериализовать строку XML в объект
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="xml">XML строка</param>
        /// <returns>Объект</returns>
        public static T DeserializeXml<T>(this string xml)
        {
            var serializer = new XmlSerializer(typeof(T));
            using var reader = new StringReader(xml);
            return (T)serializer.Deserialize(reader);
        }
        
        /// <summary>
        /// Сериализовать объект в XML
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>Строка XML</returns>
        public static string SerializeToXml(this object obj)
        {
            var xsSubmit = new XmlSerializer(obj.GetType());
            using var sww = new ExtentedStringWriter(null);
            var writer = XmlWriter.Create(sww);
            var emptyNamespaces = new XmlSerializerNamespaces();
            emptyNamespaces.Add("", "any-non-empty-string");
            xsSubmit.Serialize(writer, obj, emptyNamespaces);
            return sww.ToString();
        }
    }
}
