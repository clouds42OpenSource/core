﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.Logger;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширения для HTTP
    /// </summary>
    public static class HttpExtensions
    {
        private static readonly ILogger42 Logger = Logger42.GetLogger();

        /// <summary>
        /// Токен аутентификации
        /// </summary>
        public static string AuthenticationJsonWebToken { get; set; }

        /// <summary>
        /// Токен для обновления пары токенов (токена аутентификации и refresh токена)
        /// </summary>
        public static string RefreshToken { get; set; }

        /// <summary>
        /// Токен аутентификации
        /// </summary>
        public static Guid Link42ServiceToken { get; set; }
        

        /// <summary>
        /// Отправить post запрос асинхронно
        /// </summary>
        /// <param name="client">Клиент</param>
        /// <param name="methodPath">Путь к методу</param>
        /// <param name="inModel">Отправляемая модель</param>
        /// <param name="headersList">Список заголовков запроса</param>
        /// <returns>Результат выполнения</returns>
        public static async Task<string> PostAsStringAsync(this HttpClient client, string methodPath, string inModel, List<KeyValuePair<string, string>> headersList)
        {
            var address = client.BaseAddress;

            var uriBuilder = new UriBuilder(address)
            {
                Path = methodPath
            };

            using var requestMessage = new HttpRequestMessage(HttpMethod.Post, uriBuilder.Uri.AbsoluteUri);
            requestMessage.Headers.CacheControl = new CacheControlHeaderValue
            {
                NoTransform = true,
                NoStore = true,
                NoCache = true
            };

            headersList.ForEach(header =>
            {
                requestMessage.Headers.Add(header.Key, header.Value);
            });

            requestMessage.Content = new StringContent(inModel, Encoding.UTF8, MediaTypeNames.Text.Xml);

            using (client)
            {
                var response = client.SendAsync(requestMessage).Result;
                await EnsureSuccessStatusCode(response);
                var responseContent = await response.Content.ReadAsStringAsync();
                Logger.Trace($"Результат оповещения Tardis: - запрос {inModel} - ответ {responseContent}");
                return responseContent;
            }
        }
        

        /// <summary>
        /// вызывает Exception, если <see cref="HttpResponseMessage.IsSuccessStatusCode"/> содержит <code>false</code>
        /// </summary>
        /// <param name="response">Ответ http запроса</param>
        private static async Task EnsureSuccessStatusCode(HttpResponseMessage response)
        {
            await EnsureValidRequestModel(response);
            response.EnsureSuccessStatusCode();
        }

        /// <summary>
        /// вызывает <see cref="RequestModelValidationException"/> exception, если модель запроса не валидна и в Header есть значение <see cref="WebHeaders.ValidationModelFailedWebHeader"/>
        /// </summary>
        /// <param name="response">Ответ http запроса</param>
        private static async Task EnsureValidRequestModel(HttpResponseMessage response)
        {
            if (!response.Headers.Contains(WebHeaders.ValidationModelFailedWebHeader)) 
                return;
            
            var responseContentLength = response.Content.Headers.ContentLength;

            if (responseContentLength == 0)
                throw new RequestModelValidationException(null, null);

            var validationErrors = await response.Content.ReadAsStringAsync();
            var responseContentType = response.Content.Headers.ContentType;
            throw new RequestModelValidationException(validationErrors, responseContentType);
        }
    }
}
