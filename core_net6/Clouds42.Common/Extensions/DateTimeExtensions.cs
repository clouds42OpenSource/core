﻿using System;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширения для типа <see cref="DateTime"/>
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Посчитать затраченное время в секундах
        /// </summary>
        /// <param name="operationStartDateTime">Дата и время начала операции</param>
        /// <returns>КОл-во затраченных секунд</returns>
        public static double CalculateElapsedTimeInSeconds(this DateTime operationStartDateTime)
            => (DateTime.Now - operationStartDateTime).TotalSeconds;
    }
}
