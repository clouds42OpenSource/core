﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Хэлпер для сериализации в xml
    /// </summary>
    public static class XmlSerializerHelper
    {

        /// <summary>
        /// Десериализовать xml в объект
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="xml">Строка xml</param>
        /// <returns>Объект</returns>
        public static T Deserialize<T>(this string xml) where T : class
        {
            var serializer = new XmlSerializer(typeof(T));
            var r = new StringReader(xml);
            var result = (T)serializer.Deserialize(r);
            return result;
        }

        /// <summary>
        /// Получить XmlDocument из строки
        /// </summary>
        /// <param name="xml">Строка xml</param>
        /// <returns>XmlDocument из строки</returns>
        public static XmlDocument GetXmlDocument(this string xml)
        {
            var xmlDocument = CreateSafeXmlDocument();
            xmlDocument.LoadXml(xml);
            return xmlDocument;
        }

        /// <summary>
        /// Получить XmlDocument из пути
        /// </summary>
        /// <param name="xmlFilePath">Путь к xml документу</param>
        /// <returns>XmlDocument из пути</returns>
        public static XmlDocument GetXmlDocumentFromPath(this string xmlFilePath)
        {
            var xmlDocument = CreateSafeXmlDocument();
            xmlDocument.Load(xmlFilePath);
            return xmlDocument;
        }

        /// <summary>
        /// Сериализовать в чистый xml (без декларации xml version="1.0" encoding="utf-16")
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="toSerialize">Объект</param>
        /// <returns>Строка xml</returns>
        public static string SerializeToCleanXml<T>(this T toSerialize) where T : class
        {
            var serializerNamespaces = new XmlSerializerNamespaces([XmlQualifiedName.Empty]);
            var serializer = new XmlSerializer(toSerialize.GetType());
            var settings = new XmlWriterSettings { Indent = true, OmitXmlDeclaration = true };

            using var stream = new StringWriter();
            using var writer = XmlWriter.Create(stream, settings);
            serializer.Serialize(writer, toSerialize, serializerNamespaces);
            return stream.ToString();
        }

        /// <summary>
        /// Создать безопасный XML документ
        /// </summary>
        /// <returns>Безопасный XML документ</returns>
        private static XmlDocument CreateSafeXmlDocument()
            => new()
            {
                XmlResolver = null
            };
    }
}
