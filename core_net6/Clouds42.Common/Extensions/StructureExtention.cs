﻿using System;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Хелпер для работы со структурами данных
    /// </summary>
    public static class StructureExtention
    {
        /// <summary>
        /// Привести величену мегабайт в классический формат (ГБ)
        /// </summary>
        /// <param name="mb">Величина в МБ</param>
        /// <param name="numberAfterPoint">Округлить до</param>
        /// <returns>Классический формат</returns>
        public static string MegaByteToClassicFormat(this long mb, int numberAfterPoint = 1)
        {
            return $"{Math.Round(mb / 1024f, numberAfterPoint)} Гб";
        }
    }
}
