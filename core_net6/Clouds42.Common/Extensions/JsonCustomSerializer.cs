﻿using System;
using System.Collections.Generic;
using Clouds42.Common.Converters;
using Newtonsoft.Json;

namespace Clouds42.Common.Extensions {
    
    /// <summary>
    /// Сериализатор JSON
    /// </summary>
    public static class JsonCustomSerializer
    {
        /// <summary>
        /// Десериализовать JSON строку в объект
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="json">JSON строка</param>
        /// <returns>Объект нужного типа из JSON строки</returns>
        public static T DeserializeFromJson<T>(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception)
            {
                return default;
            }
        }

        /// <summary>
        /// Десериализовать JSON строку в объект
        /// </summary>
        /// <param name="json">JSON строка</param>
        /// <param name="objectType">Тип объекта</param>
        /// <returns>Объект нужного типа из JSON строки</returns>
        public static object DeserializeFromJson(string json, Type objectType)
        {
            return JsonConvert.DeserializeObject(json, objectType);
        }

        /// <summary>
        /// Сериализовать объект в JSON строку
        /// </summary>
        /// <param name="obj">Объект данных</param>
        /// <returns>JSON строка</returns>
        public static string SerializeToJson(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj, settings: new JsonSerializerSettings{ Converters = new List<JsonConverter>{ new DatetimeConverter() }});
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
