﻿using System.Linq;
using System.Xml.Linq;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширение для типа XElement
    /// </summary>
    public static class XElementExtension
    {
        /// <summary>
        /// Получить значение аттрибута
        /// </summary>
        /// <param name="xml">Элемент xml</param>
        /// <param name="attrName">Название аттрибута</param>
        /// <returns>Значение аттрибута</returns>
        public static string GetAttributeValue(this XElement xml, string attrName)
        {
            string result = "";
            if (xml == null) return result;
            if (xml.HasAttributes)
            {
                var attr = xml.Attributes().FirstOrDefault(x => x.Name.ToString().ToLower() == attrName.ToLower());
                if (attr != null)
                {
                    result = attr.Value;
                }
            }
            return result;
        }
    }
}