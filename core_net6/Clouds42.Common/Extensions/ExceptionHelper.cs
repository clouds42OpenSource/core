﻿using System;
using System.Text;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширение для типа Exception
    /// </summary>
    public static class ExceptionHelper
    {
        /// <summary>
        /// Получить полную информацию из исключения
        /// </summary>
        /// <param name="exception">Исключение</param>
        /// <param name="needStackTrace">Признак необходимости захватывать трассировку стэка</param>
        /// <returns>Полная информация из исключения</returns>
        public static string GetFullInfo(this Exception exception, bool needStackTrace = true )
        {
            var message = needStackTrace ? new StringBuilder( exception.StackTrace): new StringBuilder();
            message.AppendLine(exception.Message);

            while ( exception.InnerException != null ) {
                exception = exception.InnerException;
                message.AppendLine( exception.Message );
            }

            return message.ToString();
        }

        /// <summary>
        /// Проверить тип исключения на предмет переполнения диска. 
        /// </summary>
        /// <param name="ex">Исключение.</param>
        /// <returns>true-если исключение по причине переполненго диска.</returns>
        public static bool IsDiskFullException(this Exception ex)
        {
            const int HR_ERROR_HANDLE_DISK_FULL = unchecked((int)0x80070027);
            const int HR_ERROR_DISK_FULL = unchecked((int)0x80070070);

            return ex.HResult == HR_ERROR_HANDLE_DISK_FULL
                   || ex.HResult == HR_ERROR_DISK_FULL;
        }

    }
}
