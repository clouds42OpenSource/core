﻿using System;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Упрощение преобразования массива байтов в строку Base64 и обратно.
    /// </summary>
    public static class Base64ToByteExtensions
    {
        /// <summary>
        /// Преобразовать строку Base64 в массив байтов.
        /// </summary>
        /// <param name="base64Str">Строка в Base64.</param>
        /// <returns>Массив байтов</returns>
        public static byte[] FromBase64(this string base64Str)
        {
            return Convert.FromBase64String(base64Str);
        }

        /// <summary>
        /// Преобразовать массив байтов в строку Base64.
        /// </summary>
        /// <param name="byteArray">Массив байтов</param>
        /// <returns>Строка в Base64</returns>
        public static string ToBase64(this byte[] byteArray)
        {
            return Convert.ToBase64String(byteArray);
        }
    }
}