﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширения для строк
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Сконвертировать в Int
        /// </summary>
        /// <param name="input">Входная строка</param>
        /// <returns>Результат конвертации</returns>
        public static int ToInt(this string input)
        {
            int result = 0;
            if (string.IsNullOrEmpty(input)) return result;
            int.TryParse(input, out result);
            return result;
        }

        /// <summary>
        /// Сконвертировать в Guid
        /// </summary>
        /// <param name="input">Входная строка</param>
        /// <returns>Результат конвертации</returns>
        public static Guid ToGuid(this string input)
        {
            var result = Guid.Empty;
            if (string.IsNullOrEmpty(input)) return result;
            Guid.TryParse(input, out result);
            return result;
        }

        /// <summary>
        /// Сконвертировать в int
        /// </summary>
        /// <param name="input">Входное значение</param>
        /// <returns>Результат конвертации</returns>
        public static int ToInt(this bool input)
            => input ? 1 : 0;

        /// <summary>
        /// Сконвертировать название поля в Id
        /// </summary>
        /// <param name="string">Название поля</param>
        /// <returns>Результат конвертации</returns>
        public static string FieldNameToId(this string @string)
            => string.IsNullOrEmpty(@string) ? null : Regex.Replace(@string, @"\.|\[|\]+", "_");
        
        /// <summary>
        /// Проверить строку на null или пустоту
        /// </summary>
        /// <param name="value">Строка</param>
        /// <returns>Результат проверки</returns>
        public static bool IsNullOrEmpty(this string value)
            => string.IsNullOrEmpty(value);

        /// <summary>
        /// Привести строчные переносы к HTML стилю.
        /// </summary>
        /// <param name="value">Исходная строка.</param>
        /// <returns>Приведенная к стилю HTML строка.</returns>
        public static string LineEndingToHtmlStyle(this string value)
            => string.IsNullOrEmpty(value) ? value : value.Replace("\n", "<br>");
        
        /// <summary>
        /// Сконвертировать данные в формат номера реквизитов
        /// </summary>
        /// <param name="accountIndexNumber">Номер аккаунта</param>
        /// <param name="number">Номер реквизитов</param>
        /// <returns>Номер документа</returns>
        public static string ConvertToContractNumber(int accountIndexNumber, int number)
            => $"{accountIndexNumber}-{number:00}";

        /// <summary>
        /// Удалить запятые из имени файла
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns>Имя файла без запятых</returns>
        public static string RemoveCommas(this string fileName)
            => fileName?.Replace(",", "");

        public static string RemoveSlashes(this string text) => text?.Replace("\\", "");

        public static string Escape(this string text, string escapeChar, params string[] chars)
        {
            if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(escapeChar) || chars == null || chars.Length == 0)
            {
                return text;
            }

            return chars.Aggregate(text, (current, ch) => current.Replace(ch, escapeChar + ch));
        }
    }
}
