﻿using System.Text.RegularExpressions;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Хелпер для работы с ФИО
    /// </summary>
    public static class FullNameExtension
    {
        /// <summary>
        /// Строка регулярного выражения
        /// для получения сокращенного имени (Фамилии с инициалами)
        /// </summary>
        private static string ShortNameRegxString { get; } =
            @"(?'Surname'[А-ЯЁа-яё]+)(\s(?'Name'(?'N'[А-ЯЁа-яё])[А-ЯЁа-яё]*))?(\s(?'MiddleName'(?'M'[А-ЯЁа-яё])[А-ЯЁа-яё]*))?";

        /// <summary>
        /// Получить сокращенное имя
        /// (Фамилия с инициалами)
        /// </summary>
        /// <param name="fullName">ФИО</param>
        /// <returns>Сокращенное имя</returns>
        public static string GetShortName(this string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
                return "";

            var match = Regex.Match(fullName, ShortNameRegxString);

            return string.IsNullOrEmpty(match.Value) 
                ? fullName 
                : $"{match.Groups["Surname"]?.Value ?? string.Empty} {match.Groups["N"]?.Value.GetValueWithDelimiter()}{match.Groups["M"]?.Value.GetValueWithDelimiter()}";
        }

        /// <summary>
        /// Получить значение с разделителем
        /// </summary>
        /// <param name="value">Значение</param>
        /// <returns>Значение с разделителем</returns>
        private static string GetValueWithDelimiter(this string value) =>
            !string.IsNullOrEmpty(value) ? $"{value}." : string.Empty;
    }
}
