﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширения xml-сериализатора для работы с SASL (Simple Authentication and Security Layer) xml.
    /// </summary>
    public static class SaslXmlExtenstions
    {
        /// <summary>
        /// Пространство имён SASL.
        /// </summary>
        private const string SaslNamespace = "urn:ietf:params:xml:ns:xmpp-sasl";

        /// <summary>
        /// Сериализовать объект в sasl-xml.
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="obj">Экземпляр объекта</param>
        /// <returns>xml-строка</returns>
        public static string SerializeToSaslXml<T>(this T obj)
        {
            var xmlSerializer = new XmlSerializer(typeof(T), SaslNamespace);

            var ns = new XmlSerializerNamespaces();
            ns.Add("", SaslNamespace);
            var sw = new StringWriter();
            var xmlWriter = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true });

            xmlSerializer.Serialize(xmlWriter, obj, ns);
            var xml = sw.ToString();
            return xml;
        }

        /// <summary>
        /// Десериализовать объект из xml-строки.
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="xml">xml-строка</param>
        /// <returns>Экземпляр объекта</returns>
        public static T DeserializeFromSaslXml<T>(this string xml)
        {
            var serializer = new XmlSerializer(typeof(T), SaslNamespace);
            var sr = new StringReader(xml);

            var settings = new XmlReaderSettings { DtdProcessing = DtdProcessing.Ignore, MaxCharactersFromEntities = 1024 };

            using var xmlReader = XmlReader.Create(sr, settings);
            return (T)serializer.Deserialize(xmlReader);
        }
    }
}