﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Clouds42.Common.Extensions;

/// <summary>
///  Расширение для Enum
/// </summary>
/// <typeparam name="T">Тип Enum</typeparam>
public static class GenericEnumExtensions<T>
{
    private static readonly Dictionary<T, DisplayAttribute> Cache = new();

    static GenericEnumExtensions()
    {
        var type = typeof(T);
        foreach (var member in type.GetFields())
        {
            var custAttr = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            if (!custAttr.Any())
                continue;
            Cache[(T)Enum.Parse(type, member.Name)] = (DisplayAttribute)custAttr.First();
        }
    }

    /// <summary>
    /// Получить DisplayAttribute
    /// </summary>
    /// <param name="value">Значение Enum</param>
    /// <returns>DisplayAttribute</returns>
    private static DisplayAttribute GetDisplayAttribute(T value)
    {
        Cache.TryGetValue(value, out var result);
        return result;
    }

    /// <summary>
    /// Получить отображаемое имя
    /// </summary>
    /// <param name="value">Значение Enum</param>
    /// <returns>Отображаемое имя</returns>
    public static string GetDisplayName(T value)
    {
        var attr = GetDisplayAttribute(value);
        return attr == null ? value.ToString() : attr.GetName();
    }


}
