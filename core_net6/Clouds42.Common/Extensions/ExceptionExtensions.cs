﻿using System;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширение для типа Exception
    /// </summary>
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Получить сообщение исключения
        /// </summary>
        /// <param name="e">Исключение</param>
        /// <returns>Сообщение исключения</returns>
        public static string GetMessage(this Exception e)
        {
            var err = e.InnerException == null ? e.Message : e.InnerException.Message;
            return err;
        }
    }
}
