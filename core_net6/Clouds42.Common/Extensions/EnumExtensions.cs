﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Clouds42.Domain.Attributes;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    ///  Расширение для Enum
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Получить строковое значение Enum
        /// </summary>
        /// <param name="value">Значение Enum</param>
        /// <returns>Строковое значение Enum</returns>
        public static string GetStringValue(this Enum value)
        {
            var fieldInfo = value.GetFieldInfo();

            if (fieldInfo == null)
                return string.Empty;

            if (fieldInfo.GetCustomAttributes(
                    typeof(StringValueAttribute), false) is not StringValueAttribute[] attributes)
                return value.ToString();

            return attributes.Length > 0 ? attributes[0].StringValue : value.ToString();
        }

        /// <summary>
        /// Получить значение Enum по строковому значению
        /// </summary>
        /// <typeparam name="TEnum">Тип Enum</typeparam>
        /// <param name="stringValue">строковому значение Enum</param>
        /// <returns>Значение Enum</returns>
        public static TEnum GetEnumByStringValue<TEnum>(this string stringValue)
        {
            foreach (var field in typeof(TEnum).GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                    typeof(StringValueAttribute)) is StringValueAttribute attribute)
                {
                    if (attribute.StringValue == stringValue)
                        return (TEnum)field.GetValue(null);
                }
                else
                {
                    if (field.Name == stringValue)
                        return (TEnum)field.GetValue(null);
                }
            }

            return default;
        }

        /// <summary>
        /// Получить значение поля Enum
        /// </summary>
        /// <param name="value">Значение Enum</param>
        /// <returns>Значение поля Enum</returns>
        private static FieldInfo GetFieldInfo(this Enum value)
        {
            var type = value.GetType();
            return type.GetField(value.ToString());
        }

        /// <summary>
        /// Получить значение DataValue аттрибута из значения Enum
        /// </summary>
        /// <param name="value">Значение Enum</param>
        /// <returns>Значение DataValue аттрибута</returns>
        public static int GetDataValue(this Enum value)
        {
            if (value.TryGetDataValue(out int dataValue))
                return dataValue;
            throw new ArgumentException(
                $"{value}, У перечисления не обнаружено ни числовое значение ни DataValueAttribute атрибут ");
        }

        /// <summary>
        /// Попытаться получить значение DataValue аттрибута из значения Enum
        /// </summary>
        /// <param name="value">Значение Enum</param>
        /// <param name="dataValue">Значение DataValue аттрибута</param>
        /// <returns>Признак успешного выполнения</returns>
        public static bool TryGetDataValue(this Enum value, out int dataValue)
        {
            var attrs = GetEnumAttributes<DataValueAttribute>(value);
            if (attrs.Count > 0)
            {
                dataValue = attrs[0].Value;
                return true;
            }

            dataValue = 0;
            return false;
        }

        /// <summary>
        /// Проверить есть ли у значения Enum аттрибут
        /// </summary>
        /// <typeparam name="TAttribute">Тип аттрибута</typeparam>
        /// <param name="value">Значение Enum</param>
        /// <param name="filter">Дополнительный фильтр аттрибутов</param>
        /// <returns>Результат проверки</returns>
        public static bool HasAttribute<TAttribute>(this Enum value, Func<TAttribute, bool> filter = null)
             where TAttribute : Attribute
        {
            var attributes = GetEnumAttributes<TAttribute>(value);
            if (filter == null)
                return attributes.Any();
            return attributes.Any(filter);
        }

        /// <summary>
        /// Получить аттрибуты из значения Enum
        /// </summary>
        /// <typeparam name="TAttribute">Тип аттрибута</typeparam>
        /// <param name="value">Значение Enum</param>
        /// <returns>Список аттрибутов</returns>
        public static List<TAttribute> GetEnumAttributes<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            Type enumType = value.GetType();
            var enumValue = Enum.GetName(enumType, value);

            MemberInfo member = enumType.GetMember(enumValue)[0];
            var attrs = member.GetCustomAttributes(typeof(TAttribute), false).Cast<TAttribute>().ToList();
            return attrs;
        }


        /// <summary>
        /// Получить описание значения Enum
        /// </summary>
        /// <typeparam name="TEnum">Тип Enum</typeparam>
        /// <param name="value">Значение Enum</param>
        /// <returns>Описание значения Enum</returns>
        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }
        
        /// <summary>
        /// Описание Enum
        /// </summary>
        /// <param name="value">Значение Enum</param>
        /// <returns>Описание Enum</returns>
        public static string Description(this Enum value)
        {
            if (value == null)
                return string.Empty;

            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());

            if (field == null)
                return string.Empty;

            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length == 0
                ? value.ToString()
                : ((DescriptionAttribute)attributes[0]).Description;
        }
        

        /// <summary>
        /// Получить отображаемое имя
        /// </summary>
        /// <param name="value">Значение Enum</param>
        /// <returns>Отображаемое имя</returns>
        public static string GetDisplayName(this Enum value)
        {
            var property = typeof(EnumExtensions).GetMethod(nameof(GetDisplayNameSlim));
            var generic = property.MakeGenericMethod(value.GetType());
            return (string)generic.Invoke(null, [value]);
        }

        /// <summary>
        /// Получить отображаемое имя
        /// </summary>
        /// <param name="value">Значение Enum</param>
        /// <returns>Отображаемое имя</returns>
        public static string GetDisplayNameSlim<T>(T value) where T : struct, IConvertible, IComparable, IFormattable
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enum.");
            }
            return GenericEnumExtensions<T>.GetDisplayName(value);
        }

        /// <summary>
        /// Сконвертировать структуру в Enum
        /// </summary>
        /// <typeparam name="T">Тип структуры</typeparam>
        /// <param name="value">Значение</param>
        /// <returns>Значение Enum</returns>
        public static T ToEnum<T>(this object value) where T : struct
        {
            try
            {
                var res = (T)Enum.Parse(typeof(T), value.ToString());
                return !Enum.IsDefined(typeof(T), res) ? default : res;
            }
            catch
            {
                return default;
            }
        }
        
    }
}
