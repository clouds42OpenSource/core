﻿using System;
using System.Collections.Generic;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Валидатор объектов
    /// </summary>
    public static class ObjectValidatorExtention
    {
        /// <summary>
        /// Проверить объект на NULL
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="objectToCheck">Объект для проверки</param>
        public static void CheckForNull<T>(this T objectToCheck)
        {
            if (EqualityComparer<T>.Default.Equals(objectToCheck, default(T)))
                throw new ArgumentNullException(nameof(objectToCheck));
        }
    }
}
