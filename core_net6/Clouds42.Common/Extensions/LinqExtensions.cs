﻿using System;
using System.Linq;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Вспомогательные расширения LINQ
    /// </summary>
    public static partial class LinqExtensions
    {
        /// <summary>
        /// Сделать фильтр для записей
        /// </summary>
        /// <typeparam name="TSource">Тип данных записей <see cref="source"/></typeparam>
        /// <param name="source">Записи по которым сделать фильтр</param>
        /// <param name="filters">Массив фильтров которые нужно применить к <see cref="source"/></param>
        /// <returns>Отфильтрованные записи по переданным фильтрам <see cref="filters"/></returns>
        public static IQueryable<TSource> ComposeFilters<TSource>(this IQueryable<TSource> source,
            params Func<IQueryable<TSource>, IQueryable<TSource>>[] filters)
        {
            var result = source;
            return filters == null
                ? result
                : filters.Aggregate(result, (current, filter) => filter(current));
        }
    }
}
