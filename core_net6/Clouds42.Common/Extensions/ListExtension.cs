﻿using System;
using System.Collections.Generic;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Класс для работы со списком
    /// </summary>
    public static class ListExtension
    {
        /// <summary>
        /// Разбить список на части
        /// </summary>
        /// <typeparam name="T">Тип данных списка</typeparam>
        /// <param name="locations">Список</param>
        /// <param name="nSize">Количество частей списка</param>
        /// <returns></returns>
        public static IEnumerable<List<T>> SplitListIntoParts<T>(this List<T> locations, int nSize = 100)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }

        /// <summary>
        /// Добавить если выполнится условие  
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="list">Список</param>
        /// <param name="element">Елемент списка</param>
        /// <param name="condition">Условие</param>
        public static void AddIfConditionIsMet<T>(this List<T> list, T element, bool condition)
        {
            if (condition)
                list.Add(element);
        }

        /// <summary>
        /// Создать список по условиям
        /// </summary>
        /// <typeparam name="T">Тип элемента</typeparam>
        /// <param name="elementsWithConditions">Элементы с условиями их добавления в список</param>
        /// <returns>Список элементов</returns>
        public static List<T> CreateListUnderConditions<T>(List<(T Element, bool Condition)> elementsWithConditions)
        {
            var list = new List<T>();
            elementsWithConditions.ForEach(elementWithCondition =>
                list.AddIfConditionIsMet(elementWithCondition.Element, elementWithCondition.Condition));
            return list;
        }
    }
}
