﻿using System.Collections.Generic;
using System.Linq;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширения для типа Dictionary
    /// </summary>
    public static class DictionaryExtension
    {
        /// <summary>
        /// Получить значение
        /// </summary>
        /// <typeparam name="T">Тип ключа</typeparam>
        /// <typeparam name="U">Тип значение</typeparam>
        /// <param name="input">Исходный Dictionary</param>
        /// <param name="value">Значение</param>
        /// <returns>Значение</returns>
        public static U GetValue<T,U>(this Dictionary<T, U> input, T value)
        {
            if(!input.Any(x=>x.Key.Equals(value))) return default;
            var node = input.First(x=>x.Key.Equals(value));
                return node.Value;               
        }
    }
}
