﻿using System;
using System.Linq;
using Clouds42.Common.DataModels;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширение для работы с унифицированным
    /// идентификатором ресурса(URI)
    /// </summary>
    public static class UriExtension
    {
        /// <summary>
        /// Получить информацию о домене
        /// </summary>
        /// <param name="uri">Унифицированный
        /// идентификатор ресурса(URI)</param>
        /// <returns>Информацию о домене</returns>
        public static DomainInfoDc GetDomainInfo(this Uri uri)
        {
            var siteAuthorityUrl = uri?.GetLeftPart(UriPartial.Authority);
            if (string.IsNullOrEmpty(siteAuthorityUrl))
                return null;

            return new DomainInfoDc
            {
                HostName = uri.DnsSafeHost,
                RegistrableDomain = GetRegistrableDomain(uri.DnsSafeHost)
            };
        }

        /// <summary>
        /// Получить регистрируемый домен
        /// </summary>
        /// <param name="hostName">Название хоста</param>
        /// <returns>Регистрируемый домен</returns>
        private static string GetRegistrableDomain(string hostName)
        {
            var domainLevels = hostName.Split('.');
            return domainLevels.Length < 2 ? hostName : string.Join(".", domainLevels.Reverse().Take(2).Reverse());
        }

        /// <summary>
        /// Скомбинировать абсолютный и относительный адрес в общий
        /// </summary>
        /// <param name="absolutePath">Абсолютный путь</param>
        /// <param name="relativePath">Относительный путь</param>
        /// <returns>Скомбинированный путь</returns>
        public static Uri Combine(string absolutePath, string relativePath)
            => new(new Uri(absolutePath), relativePath);
    }
}
