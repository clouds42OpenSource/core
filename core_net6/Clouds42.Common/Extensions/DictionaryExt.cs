﻿using System.Collections.Generic;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Класс для расширения функциональности Dictionary.
    /// </summary>
    public static class DictionaryExt
    {
        /// <summary>
        /// Обратить словарь. Ключи становятся значениями, а значения, наоборот, ключами
        /// </summary>
        /// <typeparam name="TKey">Тип ключа</typeparam>
        /// <typeparam name="TValue">Тип значения</typeparam>
        /// <param name="source">Исходный словарь</param>
        /// <returns>Обращённый словарь</returns>
        public static Dictionary<TValue, TKey> Reverse<TKey, TValue>(this IDictionary<TKey, TValue> source)
        {
            var dictionary = new Dictionary<TValue, TKey>();
            foreach (var entry in source)
            {
                if (!dictionary.ContainsKey(entry.Value))
                    dictionary.Add(entry.Value, entry.Key);
            }

            return dictionary;
        }
    }
}