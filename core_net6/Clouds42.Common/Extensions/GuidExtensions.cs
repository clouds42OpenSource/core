﻿using System;
using System.Text;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Расширение для GUID
    /// </summary>
    public static class GuidExtensions
    {
        /// <summary>
        /// Признак что guid заполнен дефолтным значением
        /// </summary>
        /// <param name="value">Guid</param>
        /// <returns>true - если значение равно Guid.Empty</returns>
        public static bool IsNullOrEmpty(this Guid value)
        {
            return value == Guid.Empty;
        }

        /// <summary>
        /// Признак что guid пуст или заполнен дефолтным значением
        /// </summary>
        /// <param name="value">Guid</param>
        /// <returns>true - если значения нет или оно Guid.Empty</returns>
        public static bool IsNullOrEmpty(this Guid? value)
        {
            return !value.HasValue || value == Guid.Empty;
        }

        /// <summary>
        /// Получить декодированный Guid
        /// </summary>
        /// <param name="value">Guid</param>
        /// <returns>Декодированный Guid</returns>
        public static string GetEncodeGuid(this Guid value)
        {
            var guidParts = value.ToString().Split('-');
            var cryptResBuilder = new StringBuilder();
            for (var i = guidParts.Length - 1; i > 0; i--)
            {
                cryptResBuilder.Append(guidParts[i]);
            }
            return cryptResBuilder.ToString();
        }
    }
}