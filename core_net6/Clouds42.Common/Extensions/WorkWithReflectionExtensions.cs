﻿using System;

namespace Clouds42.Common.Extensions
{
    /// <summary>
    /// Экстеншен хелпер для работы с рефлексией
    /// </summary>
    public static class WorkWithReflectionExtensions
    {
        /// <summary>
        ///     Получить тип класса по названию с Namespase
        /// </summary>
        /// <param name="typeNameWithNamespace">Название типа с Namespase</param>
        /// <returns>Тип класса</returns>
        public static Type GetTypeByNameWithNamespace(this string typeNameWithNamespace)
        {
            var type = Type.GetType(typeNameWithNamespace);
            if (type != null)
                return type;

            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = a.GetType(typeNameWithNamespace);
                if (type != null)
                    return type;
            }

            return null;
        }
    }
}
