﻿using System.Threading.Tasks;
using Clouds42.Common.Jwt;
using Clouds42.Jwt.Contracts;
using Clouds42.Jwt.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Clouds42.AuthServer.ServiceExtensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class AuthenticationExtension
    {
        /// <summary>
        /// Регестрирует возможность аутентификации при помощи JWT
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        public static IServiceCollection RegisterJwtAuthentication(this IServiceCollection services)
        {
            services.AddHttpClient();
            services.AddScoped<IJwtHelper, JwtHelper>();

            var sp = services.BuildServiceProvider();

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(async options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = await sp.GetRequiredService<IJwtHelper>().GeTokenValidationParameters();

                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            context.Response.Headers.Add(context.Exception.GetType() == typeof(SecurityTokenExpiredException) ?
                                JsonWebTokenNotValidHttpHeader.TokenExpiredHeader :
                                JsonWebTokenNotValidHttpHeader.InvalidTokenHeader, "true");

                            return Task.CompletedTask;
                        }
                    };
                });

            return services;
        }

    }
}
