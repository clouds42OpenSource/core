﻿using Clouds42.Identity;
using Clouds42.Identity.Contracts;
using Clouds42.Identity.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AuthServer.ServiceExtensions
{
    public static class IdentityExtension
    {
        /// <summary>
        /// Регестрирует возможности Identity системы
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        public static IServiceCollection RegisterIdentityCapability(this IServiceCollection services)
        {
            services.AddScoped<IPasswordHasher<ApplicationUser>, CustomPasswordHasher>();

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
                {
                    options.User.RequireUniqueEmail = true;
                })
                .AddDefaultTokenProviders();

            services.AddScoped<IUserStore<ApplicationUser>, UserStoreService>();
            services.AddScoped<IRoleStore<ApplicationRole>, RoleStoreService>();

            return services;
        }
    }
}
