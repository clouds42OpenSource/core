﻿using System.Text.Json.Serialization;
using Clouds42.AuthServer.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AuthServer.ServiceExtensions
{
    public static class ConfigureServiceOptionsExtension
    {
        /// <summary>
        /// Редактирует настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        public static IServiceCollection ConfigureAllOptions(this IServiceCollection services)
        {
            return services
                .ConfigureMvcOptions()
                .ConfigureJsonOptions()
                .ConfigureApiBehaviorOptions();
        }

        /// <summary>
        /// Редактирует MVC настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        private static IServiceCollection ConfigureMvcOptions(this IServiceCollection services)
        {
            return services.Configure<MvcOptions>(options =>
            {
                //Регестрируем свой фильтр валидации моделей
                options.Filters.Add<ValidateModelActionFilterAttribute>();
                options.Filters.Add<UnhandledExceptionFilterAttribute>();
                options.Filters.Add<RemoveErrorHeadersForAnonymousAsyncActionFilter>();
            });
        }

        /// <summary>
        /// Редактирует JSON настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        private static IServiceCollection ConfigureJsonOptions(this IServiceCollection services)
        {
            return services.Configure<JsonOptions>(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
        }

        /// <summary>
        /// Редактирует ApiBehavior настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        private static IServiceCollection ConfigureApiBehaviorOptions(this IServiceCollection services)
        {
            return services.Configure<ApiBehaviorOptions>(options =>
            {
                //Отключаем системную проверку волидации модели
                options.SuppressModelStateInvalidFilter = true;
            });
        }
    }
}