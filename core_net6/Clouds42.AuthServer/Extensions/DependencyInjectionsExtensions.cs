﻿using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AuthServer.Extensions
{
    public static class DependencyInjectionsExtensions
    {
        public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
        {
            var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

            var elasticConfig = new ElasticConfigDto(
                conf["Elastic:Uri"],
                conf["Elastic:UserName"],
                conf["Elastic:Password"],
                conf["Elastic:AuthServerIndex"],
                conf["Logs:AppName"]
            );

            SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);

            return collection;
        }
    }
}
