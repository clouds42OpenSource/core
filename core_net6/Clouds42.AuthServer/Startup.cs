﻿using System;
using System.IO;
using System.Reflection;
using Clouds42.AuthServer.Extensions;
using Clouds42.AuthServer.Requirements;
using Clouds42.AuthServer.ServiceExtensions;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.Configurations;
using Clouds42.DomainContext;
using Clouds42.HandlerException;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Identity;
using Clouds42.Jwt;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Repositories;

namespace Clouds42.AuthServer
{
    public class Startup(IWebHostEnvironment env)
    {
        public IConfigurationRoot Configuration { get; } = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
            .AddGcpKeyValueSecrets(env.EnvironmentName)
            .AddEnvironmentVariables()
            .Build();


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddClouds42DbContext(Configuration);
            services.RegisterIdentityCapability();

            services.AddHttpContextAccessor();
            services.AddMemoryCache();
            services.AddHttpClient();
            services.AddIdentity();
            services.AddJwt();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddHandlerException();
            services.AddTelegramBots(Configuration);
            services.AddSerilogWithElastic(Configuration);
            services.AddScoped<IAccessMapping, AccessMapping>();
            services.AddScoped<IAccessProvider, AuthServerAccessProvider>();
            services.AddScoped<IEncryptionProvider, DesEncryptionProvider>();
            services.AddSingleton<ILogger42, SerilogLogger42>();
            services
                .AddOptions<CORS>()
                .Bind(Configuration.GetSection(nameof(CORS)));

            var cors = Configuration.GetSection(nameof(CORS))
                .Get<CORS>();

            ApiAuthorizationDefaults.API_AUTHORIZATION_POLICY_NAME = cors.PolicyName;

            services.AddCors(options =>
            {
                options.AddPolicy(cors.PolicyName, builder =>
                {
                    if (cors.UseCors)
                    {
                        builder
                            .WithOrigins(cors.AllowedOrigins)
                            .SetIsOriginAllowedToAllowWildcardSubdomains()
                            .WithHeaders(cors.AcceptHeaders)
                            .WithMethods(cors.WebMethods)
                            .WithExposedHeaders(cors.ExposedHeaders);

                    }

                    else
                    {
                        builder
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowAnyOrigin();
                    }
                });
            });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Auth Server"
                });

                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });

            services.AddControllers();

            services.ConfigureAllOptions();
            services.RegisterJwtAuthentication();

            services.AddAuthorization(options =>
            {
                options.AddPolicy(cors.PolicyName,
                    policy => policy.AddRequirements(new PermissionAuthorizationRequirement()));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<CORS> corsOptions)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            Logger42.Init(app.ApplicationServices);
            HandlerException42.Init(app.ApplicationServices);
            ConfigurationHelper.SetConfiguration(Configuration);
            app.UseRouting();
            app.UseCors(corsOptions.Value.PolicyName);

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseHttpsRedirection();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", context =>
                {
                    context.Response.ContentType = "text/html";
                    return context.Response.WriteAsync(
                        "<center style='margin-top:50px'>" +
                        "<b>NET6 AuthServer works!</b>" +
                        "</center>");
                });
                endpoints.MapControllers();
            });
        }
    }
}
