﻿using System;
using System.Collections.Generic;
using System.Net;
using Clouds42.AuthServer.Models;
using Clouds42.HandlerExeption.Contract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;


namespace Clouds42.AuthServer.Filters
{
    /// <summary>
    /// Фильтр для обработки не обработанных ошибок
    /// </summary>
    public class UnhandledExceptionFilterAttribute(IHandlerException handlerException) : ExceptionFilterAttribute
    {
        /// <summary>
        /// Mapper типа ошибки с HttpStatusCode
        /// </summary>
        private static readonly Dictionary<Type, ExceptionHandleInfoModel> ExceptionMapperToExceptionHandlerInfoModel =
            new()
            {
                {
                    typeof(UnauthorizedAccessException), new ExceptionHandleInfoModel
                    {
                        StatusCode = HttpStatusCode.Unauthorized,
                        CanShowErrorToUser = true,
                        IsFatalError = false
                    }
                }
            };

        /// <summary>
        /// Получает информацию по ошибке
        /// </summary>
        /// <param name="exception">Ошибка по которой получить информацию</param>
        private static ExceptionHandleInfoModel GetExceptionInfoFromException(Exception exception)
        {
            return ExceptionMapperToExceptionHandlerInfoModel.TryGetValue(exception.GetType(), out var exceptionInfo)
                ? exceptionInfo
                : new ExceptionHandleInfoModel
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    CanShowErrorToUser = false,
                    IsFatalError = true
                };
        }

        /// <summary>
        /// Обработчик ошибки
        /// </summary>
        /// <param name="context">Контекст ошибки</param>
        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            
            var exceptionInfo = GetExceptionInfoFromException(exception);
            

            if (exceptionInfo.IsFatalError)
            {
                handlerException.Handle(exception, "[Необработанная ошибка на Clouds42.AuthServer]" );
            }
            
            context.HttpContext.Response.StatusCode = (int)exceptionInfo.StatusCode;
            context.HttpContext.Response.ContentType = "Application/json";

            var message = exceptionInfo.CanShowErrorToUser
                ? exception.Message
                : "Произошла необработанная ошибка.";

            context.Result = new JsonResult(new
            {
                Error = message
            });

            base.OnException(context);
        }
    }
}
