﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.AuthServer.Attrs;
using Clouds42.AuthServer.Helpers;
using Clouds42.AuthServer.Models.Requests;
using Clouds42.AuthServer.Models.Responses;
using Clouds42.Identity.Contracts;
using Clouds42.Jwt.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Clouds42.AuthServer.Controllers
{
    /// <summary>
    /// Контроллер для аутентификации
    /// </summary>
    [ApiController]
    [JwtAuthorize]
    [Route("api/[controller]/[action]")]
    public class IdentityController : ControllerBase
    {
        /// <summary>
        /// Обработчик аутентификации
        /// </summary>
        private readonly IIdentityHandler _identityHandler;
        /// <summary>
        /// Объект для работы с JWT
        /// </summary>
        private readonly IJwtProvider _jwtProvider;


        /// <summary>
        /// Инициализация класса <see cref="IdentityController"/>
        /// </summary>
        /// <param name="jwtProvider">Объект для работы с JWT</param>
        /// <param name="identityHandler">Обработчик аутентификации</param>
        public IdentityController(
            [FromServices] IJwtProvider jwtProvider,
            [FromServices] IIdentityHandler identityHandler)
        {
            _identityHandler = identityHandler;
            _jwtProvider = jwtProvider;


        }

        /// <summary>
        /// Действие для проверки JWT
        /// </summary>
        [HttpGet]
        public string TestConnection()
        {
            var exp = User.Claims.First(claim => claim.Type == "exp");
            var dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(exp.Value));

            var endDate = dateTimeOffset.LocalDateTime;
            var liveDate = endDate.Subtract(DateTime.Now);
            var str = $"Привет {User.Identity.Name}, осталось: {liveDate.TotalHours:f0}{liveDate:\\:mm\\:ss}";
            return str;
        }

        /// <summary>
        /// Проверить валидность текущего токена
        /// </summary>
        /// <param name="model">Модель с данными для получения токенов</param>
        /// <returns>Результат проверки</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<bool> CheckCurrentTokenValidity([FromBody] RefreshAccessTokenRequestModel model)
            => await _jwtProvider.CheckCurrentTokenValidity(model.JsonWebToken, model.RefreshToken);

        /// <summary>
        /// Действие для аутентификации
        /// </summary>
        /// <param name="model">Модель с данными для аутентификации</param>
        [HttpPost]
        [AllowAnonymous]
        public async Task<AuthenticateUserResponseModel> Authenticate([FromBody] AuthenticateUserRequestModel model)
        {
            var audience = Request.Headers[RequestHeaders.AudienceHeader];
            var user = await _identityHandler.AuthenticateAsync(model.Username, model.Password);

            var jwt = await _jwtProvider.CreateJsonWebTokenFor(model.Username, model.Password, audience.Any() ?  audience.ToString() : null, user);

            var jsonWebToken = jwt.JsonWebToken;
            var refreshToken = Convert.ToBase64String(jwt.RefreshToken);

            return new AuthenticateUserResponseModel
            {
                JsonWebToken = jsonWebToken,
                RefreshToken = refreshToken,
                ExpiresIn = jwt.ExpiresIn
            };
        }

        /// <summary>
        /// Действие для создания новой пары токенов (JsonWebToken и RefreshToken)
        /// </summary>
        /// <param name="model">Модель с данными для получения токенов</param>
        [HttpPost]
        [AllowAnonymous]
        public async Task<RefreshAccessTokenResponseModel> RefreshAccessToken([FromBody] RefreshAccessTokenRequestModel model)
        {
            var jwt = await _jwtProvider.RefreshJsonWebToken(model.JsonWebToken, model.RefreshToken);

            var isSuccess = jwt != null;
            var jsonWebToken = jwt?.JsonWebToken;
            var refreshToken = jwt == null
                ? null
                : Convert.ToBase64String(jwt.RefreshToken);

            return new RefreshAccessTokenResponseModel
            {
                IsSuccess = isSuccess,
                JsonWebToken = jsonWebToken,
                RefreshToken = refreshToken,
                ExpiresIn = jwt?.ExpiresIn ?? 0
            };
        }
    }
}
