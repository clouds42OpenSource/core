﻿namespace Clouds42.AuthServer
{
    /// <summary>
    /// Константы
    /// </summary>
    public static class ApiAuthorizationDefaults
    {
        /// <summary>
        /// Наименование POLICE для аутентификации
        /// </summary>
        public static string API_AUTHORIZATION_POLICY_NAME;

    }

    /// <summary>
    /// Конфигурация CORS из appsettings.json
    /// </summary>
    public class CORS
    {
        /// <summary>
        /// Название политики
        /// </summary>
        public string PolicyName { get; set; }

        /// <summary>
        /// Разрешенные заголовки шапки
        /// </summary>
        public string[] AcceptHeaders { get; set; }

        /// <summary>
        /// Разрешенные адреса
        /// </summary>
        public string[] AllowedOrigins { get; set; }

        /// <summary>
        /// Разрешенные методы
        /// </summary>
        public string[] WebMethods { get; set; }

        /// <summary>
        /// Незащищенные заголовки шапки
        /// </summary>
        public string[] ExposedHeaders { get; set; }


        /// <summary>
        /// Флаг - использовать cors?
        /// </summary>
        public bool UseCors { get; set; }
    }
}
