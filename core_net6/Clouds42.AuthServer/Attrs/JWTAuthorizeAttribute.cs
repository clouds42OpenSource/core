﻿using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace Clouds42.AuthServer.Attrs
{
    /// <summary>
    /// Класс применяемый к Action или к Controller что бы предотвратить анонимный доступ
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class JwtAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Инициализация класса <see cref="JwtAuthorizeAttribute"/>
        /// </summary>
        public JwtAuthorizeAttribute()
            : base(ApiAuthorizationDefaults.API_AUTHORIZATION_POLICY_NAME)
        {
            AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme;
        }
    }
}