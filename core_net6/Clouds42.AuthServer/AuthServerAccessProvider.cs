﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AuthServer
{
    /// <summary>
    /// Access provider для сервера аутентификации
    /// </summary>
    public class AuthServerAccessProvider(
        IUnitOfWork dbLayer,
        IHttpContextAccessor httpContextAccessor,
        ILogger42 logger,
        IConfiguration configuration,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : BaseAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping)
    {
        /// <summary>
        /// Контекст для получения HttpContext объекта
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor = httpContextAccessor;

        /// <summary>
        /// Получить UserIdentity
        /// </summary>
        protected override string GetUserIdentity()
        {
            return TryGetUserIdentityFromHttpContextUser(out var userIdentity)
                ? userIdentity
                : null;
        }

        /// <summary>
        /// Получить UserIdentity из HttpContext.Current.User
        /// </summary>
        /// <param name="userIdentity">Сюда сохраняется UserIdentity</param>
        /// <returns>Возвращает <c>true</c> если получилось взять из HttpContext.Current.User, иначе <c>false</c></returns>
        private bool TryGetUserIdentityFromHttpContextUser(out string userIdentity)
        {
            userIdentity = string.Empty;
            var user = _httpContextAccessor?.HttpContext?.User;
            if (user == null || 
                !(user.Identity?.IsAuthenticated ?? false) || 
                user.Identity is not ClaimsIdentity claimsIdentity)
            {
                return false;
            }

            userIdentity = claimsIdentity.Claims.Where(c => c.Type == ClaimTypes.Name)
                .Select(c => c.Value).SingleOrDefault();
           
            return !string.IsNullOrEmpty(userIdentity);
        }

        private IUserPrincipalDto _user;

        /// <summary>
        /// Аутентифицированный пользователь
        /// </summary>
        /// <returns></returns>
        public override IUserPrincipalDto GetUser()
        {
            if (_user != null)
                return _user;

            if (TryGetUserFromHttpContextUser(out var accountUserFromHttpContextUser))
            {
                return _user = accountUserFromHttpContextUser;
            }

            return null;
        }

        public override Task<IUserPrincipalDto> GetUserAsync()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получения AccountUser из HttpContext.Current.User
        /// </summary>
        /// <param name="userFromHttpContextUser">Полученный AccountUser</param>
        private bool TryGetUserFromHttpContextUser(out AccountUserPrincipalDto userFromHttpContextUser)
        {
            userFromHttpContextUser = null;
            var user = _httpContextAccessor?.HttpContext?.User;
            if (user == null || 
                !(user.Identity?.IsAuthenticated ?? false) || 
                user.Identity is not ClaimsIdentity claimsIdentity)
            {
                return false;
            }
            
            var id = claimsIdentity.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier)
                .Select(c => c.Value).SingleOrDefault();
           
            if (string.IsNullOrEmpty(id) || !Guid.TryParse(id, out var accountUserId))
            {
                return false;
            }

            var accountUser = DbLayer.AccountUsersRepository.GetById(accountUserId);
            if (accountUser == null)
            {
                return false;
            }

            userFromHttpContextUser = new AccountUserPrincipalDto
            {
                Id = accountUser.Id,
                Name = accountUser.Login,
                ContextAccountId = accountUser.AccountId,
                RequestAccountId = accountUser.AccountId,
                Email = accountUser.Email,
                PhoneNumber = accountUser.PhoneNumber,
                Groups = GetGroups(accountUser.Id),
            };

            return true;
        }

        /// <summary>
        /// Откуда прищёл запрос, через что
        /// </summary>
        /// <returns></returns>
        public override string GetUserAgent()
        {
            var currentRequest = _httpContextAccessor?.HttpContext?.Request;
            return currentRequest?.Headers.ContainsKey("UserAgent") ?? false
                ? currentRequest.Headers["UserAgent"].ToString()
                : string.Empty;
        }

        /// <summary>
        /// Адресс запроса
        /// </summary>
        /// <returns></returns>
        public override string GetUserHostAddress()
        {
            return _httpContextAccessor?.HttpContext?.Connection?.RemoteIpAddress?.ToString();
        }

        /// <summary>
        /// Получение имени аутентифицированного пользователя
        /// </summary>
        public override string Name => GetUser()?.Name;
    }
}
