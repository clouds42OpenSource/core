﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.AuthServer.Models.Requests
{
    /// <summary>
    /// Модель запроса на обновление пары токенов (JsonWebToken и RefreshToken)
    /// </summary>
    public class RefreshAccessTokenRequestModel
    {
        /// <summary>
        /// AccessToken, токен для аутентификации
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string JsonWebToken { get; set; }
        
        /// <summary>
        /// RefreshToken, токен для обновления пары токенов (JsonWebToken и RefreshToken)
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string RefreshToken { get; set; }
    }
}