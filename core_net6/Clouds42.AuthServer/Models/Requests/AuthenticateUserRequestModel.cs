﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.AuthServer.Models.Requests
{
    /// <summary>
    /// Модель запроса на аутентификацию
    /// </summary>
    public sealed class AuthenticateUserRequestModel
    {
        /// <summary>
        /// Логин
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Логин не может быть пустым.")]
        public string Username { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Пароль не может быть пустым.")]
        public string Password { get; set; }
    }
}