﻿using System.Net;

namespace Clouds42.AuthServer.Models
{
    /// <summary>
    /// Класс для описания того, как обрабатывать не обработанную ошибку
    /// </summary>
    public sealed class ExceptionHandleInfoModel
    {
        /// <summary>
        /// Статус возврата при полученной ошибки
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }
        
        /// <summary>
        /// Является ли ошибка фатальной
        /// </summary>
        public bool IsFatalError { get; set; }
        
        /// <summary>
        /// Можно ли показывать ошибку пользователя
        /// </summary>
        public bool CanShowErrorToUser { get; set; }
    }

}