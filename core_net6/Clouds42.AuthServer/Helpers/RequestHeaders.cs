﻿namespace Clouds42.AuthServer.Helpers
{
    /// <summary>
    /// Содержит названия заголовков
    /// </summary>
    public static class RequestHeaders
    {
        /// <summary>
        /// Заголовок для определения аудитории, при создании JWT токена
        /// </summary>
        public const string AudienceHeader = "audience";
    }
}