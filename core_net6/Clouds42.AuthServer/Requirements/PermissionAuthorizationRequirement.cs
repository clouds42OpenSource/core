﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Clouds42.AuthServer.Requirements
{
    /// <summary>
    /// Требование для успешной аутентификации
    /// </summary>
    public sealed class PermissionAuthorizationRequirement : AuthorizationHandler<PermissionAuthorizationRequirement>,
        IAuthorizationRequirement
    {
        /// <summary>
        /// Обработчик требования
        /// </summary>
        /// <param name="context">Контекст аутентификации</param>
        /// <param name="requirement">Требование, которое нужно пройти</param>
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            PermissionAuthorizationRequirement requirement)
        {
            if (context.User.Identity is { IsAuthenticated: false })
                return Task.CompletedTask;

            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
