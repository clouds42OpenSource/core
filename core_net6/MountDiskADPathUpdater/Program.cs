﻿using Clouds42.Accounts.SegmentMigration.Helpers;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.DomainContext;
using Clouds42.DomainContext.Context;
using Clouds42.Logger.Serilog;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Repositories;

try
{
    Console.OutputEncoding = System.Text.Encoding.UTF8;

    IConfiguration configuration = new ConfigurationBuilder()
        .AddGcpKeyValueSecrets()
        .AddJsonFile("appsettings.json", true, true)
        .Build();

    var updateType = configuration.GetSection("UpdateType").Value;
    var optionsBuilder = new DbContextOptionsBuilder<Clouds42DbContext>();
    ;
    if (configuration.GetSection("ConnectionStrings:DatabaseType").Value == ProviderTypeConstants.MsSql)
    {
        optionsBuilder.UseSqlServer(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
    }

    else
    {
        optionsBuilder.UseNpgsql(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
    }
    var unitOfWork = new UnitOfWork(new Clouds42DbContext(optionsBuilder.Options));
    var accountFolderHelper = new AccountFolderHelper(new SerilogLogger42());

    Console.WriteLine($"Work started. UpdateType - {updateType}");

    if (updateType == "2")
    {
        var accountIndexNumber = configuration.GetSection("AccountIndexNumber").Get<int?>();

        if (accountIndexNumber.HasValue)
        {
            var account = await unitOfWork.AccountsRepository
                .AsQueryableNoTracking()
                .Include(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .ThenInclude(x => x.CloudServicesFileStorageServer)
                .FirstOrDefaultAsync(x => x.IndexNumber == accountIndexNumber);

            var activeAccountUsersLogins = await unitOfWork.AccountUsersRepository
                .AsQueryableNoTracking()
                .Where(x => x.AccountId == account.Id && (!x.Removed.HasValue || !x.Removed.Value) &&
                            x.CorpUserSyncStatus != "Deleted" && x.CorpUserSyncStatus != "SyncDeleted")
                .Select(x => x.Login)
                .ToArrayAsync();


            var path = accountFolderHelper.GetAccountClientFileStoragePath(account.AccountConfiguration.Segment,
                account);

            ActiveDirectoryUserFunctions.SetUserFilesHomeDirectory(path, "R:", activeAccountUsersLogins);
        }
    }

    else
    {
        var activeUsersCount = await unitOfWork.AccountUsersRepository
            .AsQueryableNoTracking()
            .Where(x => (!x.Removed.HasValue || !x.Removed.Value) &&
                        x.CorpUserSyncStatus != "Deleted" && x.CorpUserSyncStatus != "SyncDeleted")
            .CountAsync();

        Console.WriteLine($"Active users count - {activeUsersCount}");
        Console.WriteLine($"Start working by batch count - 100");

        int totalPages = (int)Math.Ceiling(activeUsersCount / 100.0);

        Console.WriteLine($"Total pages {totalPages}");

        var includeVip = configuration.GetSection("IncludeVIP").Get<bool>();

        Console.WriteLine($"IncludeVIP - {includeVip}");

        var errorLogins = new List<string>();
        for (int pageNumber = 1; pageNumber <= totalPages; pageNumber++)
        {
            Console.WriteLine($"Page number  - {pageNumber}");

            var records = await unitOfWork.AccountUsersRepository
                .AsQueryableNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .ThenInclude(x => x.CloudServicesFileStorageServer)
                .Where(x => (!x.Removed.HasValue || !x.Removed.Value)
                            && x.CorpUserSyncStatus != "Deleted"
                            && x.CorpUserSyncStatus != "SyncDeleted" &&
                            (includeVip || !x.Account.AccountConfiguration.IsVip))
                .Skip((pageNumber - 1) * 100)
                .Take(100)
                .ToListAsync();

            foreach (var record in records)
            {
                var path = accountFolderHelper.GetAccountClientFileStoragePath(
                    record.Account.AccountConfiguration.Segment, record.Account);
                try
                {
                    ActiveDirectoryUserFunctions.SetUserFilesHomeDirectory(path, "R:", record.Login);
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Error update in AD");
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    errorLogins.Add(record.Login);
                }
            }
        }

        if (errorLogins.Any())
        {
            File.WriteAllLines("errors.txt", errorLogins);
        }
    }

    Console.WriteLine("Work ended. Press any key to exit");
    Console.ReadKey();
}
catch (Exception e)
{
    Console.WriteLine("Unhandled error. Press any key to exit");
    Console.WriteLine(e.Message);
    Console.WriteLine(e.StackTrace);
    Console.ReadKey();
}

