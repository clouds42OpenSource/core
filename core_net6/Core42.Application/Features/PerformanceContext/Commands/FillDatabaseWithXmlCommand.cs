﻿using Clouds42.Common.ManagersResults;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Core42.Application.Features.PerformanceContext.Commands;

 public class FillDatabaseWithXmlCommand : IRequest<ManagerResult<string>>
    {
        public IFormFile File { get; set; }
    }
