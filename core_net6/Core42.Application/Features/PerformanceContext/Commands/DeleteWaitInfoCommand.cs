﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.PerformanceContext.Models;
using MediatR;

namespace Core42.Application.Features.PerformanceContext.Commands;

// Команда для удаления информации об ожидании
public class DeleteWaitInfoCommand : IRequest<ManagerResult<WaitDto>>
{
    // Тип ожидания, по которому будет выполняться удаление
    public string WaitType { get; set; }
}
