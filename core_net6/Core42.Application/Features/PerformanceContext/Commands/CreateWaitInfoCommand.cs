﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.PerformanceContext.Models;
using MediatR;

namespace Core42.Application.Features.PerformanceContext.Commands;

// Команда для создания информации об ожидании
public class CreateWaitInfoCommand : IRequest<ManagerResult<WaitDto>>
{
    // DTO, содержащий информацию об ожидании
    public WaitDto WaitInfo { get; set; }
}
