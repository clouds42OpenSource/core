﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.PerformanceContext.Models;
using MediatR;
using Newtonsoft.Json;

namespace Core42.Application.Features.PerformanceContext.Commands;

// Команда для обновления информации об ожидании
public class UpdateWaitInfoCommand : IRequest<ManagerResult<WaitDto>>
{
    // тип ожидания, выполняет роль айди
    
    [JsonIgnore]
    public string Type { get; set; }
    // DTO, содержащий обновляемую информацию об ожидании
    public WaitDto WaitInfo { get; set; }
}
