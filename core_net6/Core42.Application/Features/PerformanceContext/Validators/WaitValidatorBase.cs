﻿using Core42.Application.Features.PerformanceContext.Models;
using FluentValidation;
using Newtonsoft.Json;

namespace Core42.Application.Features.PerformanceContext.Validators;

public class WaitValidatorBase: AbstractValidator<WaitDto>
{
    public WaitValidatorBase()
    {
        RuleFor(waitDto => waitDto.Recommendations)
            .Must(CanSerializeToJson).WithMessage("Рекомендации не представляют собой json массив.");

        RuleFor(waitDto => waitDto.Causes)
            .Must(CanSerializeToJson).WithMessage("Причины не представляют собой json массив.");
    }


    private bool CanSerializeToJson(List<string>? list)
    {
        if (list == null) return true;

        try
        {
            JsonConvert.SerializeObject(list);
            return true;
        }
        catch (JsonException)
        {
            return false;
        }
    }
}
