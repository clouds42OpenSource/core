﻿using Core42.Application.Features.PerformanceContext.Queries;
using FluentValidation;

namespace Core42.Application.Features.PerformanceContext.Validators;

public class GetWaitsQueryValidator : AbstractValidator<GetWaitsQuery>
{
    public GetWaitsQueryValidator()
    {
        RuleFor(command => command.Filter)
            .NotNull().WithMessage("Список типов ожиданий пуст");
    }
}
