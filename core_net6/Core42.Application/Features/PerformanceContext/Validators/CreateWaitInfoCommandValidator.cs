﻿using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Commands;
using FluentValidation;

namespace Core42.Application.Features.PerformanceContext.Validators;

public class CreateWaitInfoCommandValidator : AbstractValidator<CreateWaitInfoCommand>
{
    public CreateWaitInfoCommandValidator(IUnitOfWork unitOfWork)
    {
        RuleFor(command => command)
            .NotNull()
            .WithMessage("Не получилось правильно создать команду из запроса");
        RuleFor(command => command.WaitInfo)
            .NotNull()
            .WithMessage("Тело запроса - null")
            .SetValidator(new WaitValidatorBase());
    }
}
