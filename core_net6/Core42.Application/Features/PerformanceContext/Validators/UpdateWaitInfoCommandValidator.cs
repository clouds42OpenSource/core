﻿using Core42.Application.Features.PerformanceContext.Commands;
using FluentValidation;

namespace Core42.Application.Features.PerformanceContext.Validators;

public class UpdateWaitInfoCommandValidator : AbstractValidator<UpdateWaitInfoCommand>
{
    public UpdateWaitInfoCommandValidator()
    {
        RuleFor(command => command)
            .NotNull()
            .WithMessage("Не получилось правильно создать команду из зпроса");
        RuleFor(command => command.WaitInfo)
            .NotNull()
            .WithMessage("Тело запроса - null")
            .SetValidator(new WaitValidatorBase());
        
    }
}
