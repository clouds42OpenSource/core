﻿using Clouds42.Domain.DataModels.WaitInfos;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.PerformanceContext.Specifications;

public static class WaitInfoSpecification
{
    public static Spec<WaitInfo> WithWaitTypes(string[] waitTypes)
    {
        return new Spec<WaitInfo>(w => !waitTypes.Any() || waitTypes.Contains(w.WaitType));
    }
    
    public static Spec<WaitInfo> WithExactWaitType(string waitType)
    {
        return new Spec<WaitInfo>(w => string.IsNullOrEmpty(waitType) || w.WaitType == waitType);
    }
}
