﻿using AutoMapper;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Commands;
using Core42.Application.Features.PerformanceContext.Models;
using Core42.Application.Features.PerformanceContext.Specifications;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PerformanceContext.Handlers;

public class DeleteWaitInfoCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
    : IRequestHandler<DeleteWaitInfoCommand, ManagerResult<WaitDto>>
{
    public async Task<ManagerResult<WaitDto>> Handle(DeleteWaitInfoCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var waitInfo = await unitOfWork.WaitsInfoRepository
                .FirstOrDefaultAsync(WaitInfoSpecification.WithExactWaitType(request.WaitType));
            if (waitInfo == null)
            {
                string message = $"Тип ожидания {request.WaitType} не существует";
                logger.Info(message);
                
                return PagedListExtensions.ToPreconditionFailedManagerResult<WaitDto>(message);
            }    
            unitOfWork.WaitsInfoRepository.Delete(waitInfo);
            await unitOfWork.SaveAsync();
            
            return mapper.Map<WaitDto>(waitInfo).ToOkManagerResult(200);
        }
        catch (Exception e)
        {
            string message = "Проблема с удалением данных об ожидании";
            logger.Error(e, message);
            
            return PagedListExtensions.ToPreconditionFailedManagerResult<WaitDto>(message);
        }
    }
}
