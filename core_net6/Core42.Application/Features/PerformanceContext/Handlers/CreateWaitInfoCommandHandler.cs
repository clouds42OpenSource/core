﻿using AutoMapper;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.WaitInfos;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Commands;
using Core42.Application.Features.PerformanceContext.Models;
using Core42.Application.Features.PerformanceContext.Specifications;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PerformanceContext.Handlers;

public class CreateWaitInfoCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
    : IRequestHandler<CreateWaitInfoCommand, ManagerResult<WaitDto>>
{
    public async Task<ManagerResult<WaitDto>> Handle(CreateWaitInfoCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var alreadyExist = await unitOfWork
                .WaitsInfoRepository
                .AsQueryable()
                .AnyAsync(WaitInfoSpecification.WithExactWaitType(request.WaitInfo.WaitType), cancellationToken);
            if (alreadyExist)
            {
                string message = $"Тип ожидания {request.WaitInfo.WaitType} уже существует";
                logger.Info(message);
                
                return PagedListExtensions.ToPreconditionFailedManagerResult<WaitDto>(message);
            }
            var waitInfo = mapper.Map<WaitInfo>(request.WaitInfo);
            unitOfWork.WaitsInfoRepository.Insert(waitInfo);
            await unitOfWork.SaveAsync();
            
            return request.WaitInfo.ToOkManagerResult(200);
        }
        catch (Exception e)
        {
            string message = "Проблема с созданием данных об ожидании";
            logger.Error(e, message);
            
            return PagedListExtensions.ToPreconditionFailedManagerResult<WaitDto>(message);
        }
    }
}
