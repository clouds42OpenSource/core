﻿using System.Xml;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.WaitInfos;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Commands;
using MediatR;
using Newtonsoft.Json;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PerformanceContext.Handlers;

public class FillDatabaseWithXmlCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger)
    : IRequestHandler<FillDatabaseWithXmlCommand, ManagerResult<string>>
{
    public async Task<ManagerResult<string>> Handle(FillDatabaseWithXmlCommand request, CancellationToken cancellationToken)
        {
            if (request.File == null || request.File.Length == 0)
            {
                string message = "File is empty";
                logger.Info(message);
                return PagedListExtensions.ToPreconditionFailedManagerResult<string>(message);
            }

            try
            {
                await using var stream = request.File.OpenReadStream();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(stream);

                var waitTypes = xmlDoc.SelectNodes("/sql_waits/wait_type");

                if (waitTypes != null)
                {
                    foreach (XmlNode waitType in waitTypes)
                    {
                        var name = waitType.Attributes?["name"]?.Value;
                        var description = waitType.SelectSingleNode("description")?.InnerText;

                        var causes = (waitType.SelectNodes("causes/cause") ?? throw new InvalidOperationException())
                            .Cast<XmlNode>()
                            .Select(node => node.InnerText)
                            .ToList();

                        var recommendations =
                            ((waitType.SelectNodes("recommendations/rec")) ?? throw new InvalidOperationException())
                            .Cast<XmlNode>()
                            .Select(node => node.InnerText)
                            .ToList();

                        var waitInfo = new WaitInfo
                        {
                            WaitType = name,
                            Description = description,
                            CausesJson = JsonConvert.SerializeObject(causes, Newtonsoft.Json.Formatting.Indented),
                            RecommendationsJson = JsonConvert.SerializeObject(recommendations, Newtonsoft.Json.Formatting.Indented)
                        };

                        var existingWaitInfo = await unitOfWork
                            .WaitsInfoRepository
                            .FirstOrDefaultAsync(w => w.WaitType == waitInfo.WaitType);

                        if (existingWaitInfo != null)
                        {
                            existingWaitInfo.Description = waitInfo.Description;
                            existingWaitInfo.CausesJson = waitInfo.CausesJson;
                            existingWaitInfo.RecommendationsJson = waitInfo.RecommendationsJson;
                            unitOfWork.WaitsInfoRepository.Update(existingWaitInfo);
                        }
                        else
                        {
                            unitOfWork.WaitsInfoRepository.Insert(waitInfo);
                        }
                    }
                }

                await unitOfWork.SaveAsync();

                string successMessage = "Database filled successfully";
    
                return successMessage.ToOkManagerResult(200);
            }
            catch (Exception ex)
            {
                string message = "Проблема с заполнением базы данных из XML";
                logger.Error(ex, message);
                return PagedListExtensions.ToPreconditionFailedManagerResult<string>(message);
            }
        }
    }
