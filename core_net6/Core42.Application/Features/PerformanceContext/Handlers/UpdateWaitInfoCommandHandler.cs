﻿using AutoMapper;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Commands;
using Core42.Application.Features.PerformanceContext.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PerformanceContext.Handlers;

public class UpdateWaitInfoCommandHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IMapper mapper)
    : IRequestHandler<UpdateWaitInfoCommand, ManagerResult<WaitDto>>
{
    public async Task<ManagerResult<WaitDto>> Handle(UpdateWaitInfoCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var existingInfo = await unitOfWork.WaitsInfoRepository
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.WaitType == request.WaitInfo.WaitType, cancellationToken);

            if (existingInfo == null)
            {
                string message = $"Тип ожидания {request.Type} не существует";
                logger.Info(message);
                
                return PagedListExtensions.ToPreconditionFailedManagerResult<WaitDto>(message);
            } 
            mapper.Map(request.WaitInfo, existingInfo);
            existingInfo.WaitType = request.WaitInfo.WaitType;
            unitOfWork.WaitsInfoRepository.Update(existingInfo);
            await unitOfWork.SaveAsync();
            
            return request.WaitInfo.ToOkManagerResult(200);
        }
        catch (Exception e)
        {
            string message = "Проблема с изменением данных об ожидании";
            logger.Error(e, message);
            
            return PagedListExtensions.ToPreconditionFailedManagerResult<WaitDto>(message);
        }
    }
}
