﻿using AutoMapper;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Models;
using Core42.Application.Features.PerformanceContext.Queries;
using Core42.Application.Features.PerformanceContext.Specifications;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PerformanceContext.Handlers;

public class GetWaitsQueryHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IMapper mapper)
    : IRequestHandler<GetWaitsQuery, ManagerResult<List<WaitDto>>>
{
    public async Task<ManagerResult<List<WaitDto>>> Handle(GetWaitsQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var waits = await unitOfWork.WaitsInfoRepository
                .AsQueryableNoTracking()
                .Where(WaitInfoSpecification.WithWaitTypes(request.Filter.WaitTypes))
                .ToListAsync(cancellationToken);

            return mapper.Map<List<WaitDto>>(waits)
                .ToOkManagerResult(200);

        }
        catch (Exception e)
        {
            string message = "Не удалось получить информацию об ожиданиях";
            logger.Error(e, message);
            
            return PagedListExtensions.ToPreconditionFailedManagerResult<List<WaitDto>>(message);
        }
    }
}
