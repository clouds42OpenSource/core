﻿using AutoMapper;
using Clouds42.Domain.DataModels.WaitInfos;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.PerformanceContext.Models;

// DTO для представления информации об ожидании
public class WaitDto : IMapFrom<WaitInfo>
{
    // Тип ожидания
    public string WaitType { get; set; }
        
    // Описание ожидания
    public string Description { get; set; }
        
    // Список рекомендаций по ожиданию
    public List<string> Recommendations { get; set; }
        
    // Список причин ожидания
    public List<string> Causes { get; set; }
    
    public void Mapping(Profile profile)
    {
        profile.CreateMap<WaitInfo, WaitDto>()
            .ForMember(dest => dest.WaitType, opt => opt.MapFrom(src => src.WaitType))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.Recommendations, opt => opt.MapFrom(src => src.Recommendations))
            .ForMember(dest => dest.Causes, opt => opt.MapFrom(src => src.Causes));

        profile.CreateMap<WaitDto, WaitInfo>()
            .ForMember(dest => dest.WaitType, opt => 
                opt.Condition((src, dest) => src.WaitType != dest.WaitType))
            .ForMember(dest => dest.Description, opt => 
                opt.Condition((src, dest) => src.Description != dest.Description))
            .ForMember(dest => dest.Recommendations, opt => 
                opt.Condition((src, dest) => !src.Recommendations.SequenceEqual(dest.Recommendations)))
            .ForMember(dest => dest.Causes, opt => 
                opt.Condition((src, dest) => !src.Causes.SequenceEqual(dest.Causes)))
            .ForMember(dest => dest.CausesJson, opt => opt.Ignore())
            .ForMember(dest => dest.RecommendationsJson, opt => opt.Ignore())
            .ForMember(dest => dest.Id, opt => opt.Ignore());
    }
    
}
