﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.PerformanceContext.Queries.Filters;

public class WaitInfoFilter : IQueryFilter
{
    public string[] WaitTypes { get; set; }
}
