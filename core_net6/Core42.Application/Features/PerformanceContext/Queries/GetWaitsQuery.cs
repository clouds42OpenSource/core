﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.PerformanceContext.Models;
using Core42.Application.Features.PerformanceContext.Queries.Filters;
using MediatR;

namespace Core42.Application.Features.PerformanceContext.Queries;

// Запрос для получения списка информации об ожиданиях
public class GetWaitsQuery : IRequest<ManagerResult<List<WaitDto>>>, IHasFilter<WaitInfoFilter>
{
    // Фильтр для получения информации об ожиданиях
    public WaitInfoFilter Filter { get; set; }
}
