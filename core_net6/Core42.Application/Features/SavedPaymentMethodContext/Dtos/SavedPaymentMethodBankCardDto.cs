﻿using AutoMapper;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SavedPaymentMethodContext.Dtos
{
    /// <summary>
    /// Saved payment method bank card dto
    /// </summary>
    public class SavedPaymentMethodBankCardDto : IMapFrom<SavedPaymentMethodBankCard>
    {
        /// <summary>
        /// First 6 digit
        /// </summary>
        public string FirstSixDigits { get; set; }

        /// <summary>
        /// Last 4 digit
        /// </summary>
        public string LastFourDigits { get; set; }

        /// <summary>
        /// Expiry month
        /// </summary>
        public string ExpiryMonth { get; set; }

        /// <summary>
        /// Expiry year
        /// </summary>
        public string ExpiryYear { get; set; }

        /// <summary>
        /// Card type
        /// </summary>
        public string CardType { get; set; }


        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SavedPaymentMethodBankCard, SavedPaymentMethodBankCardDto>()
                .ForMember(x => x.FirstSixDigits, z => z.MapFrom(y => y.FirstSixDigits))
                .ForMember(x => x.LastFourDigits, z => z.MapFrom(y => y.LastFourDigits))
                .ForMember(x => x.CardType, z => z.MapFrom(y => y.CardType))
                .ForMember(x => x.ExpiryMonth, z => z.MapFrom(y => y.ExpiryMonth))
                .ForMember(x => x.ExpiryYear, z => z.MapFrom(y => y.ExpiryYear));
        }
    }
}
