﻿using AutoMapper;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SavedPaymentMethodContext.Dtos
{
    /// <summary>
    /// Saved payment method dto
    /// </summary>
    public class SavedPaymentMethodDto : IMapFrom<SavedPaymentMethod>
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Account id
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        public DateTimeOffset CreatedOn { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Payment name
        /// </summary>
        public string PaymentName { get; set; }

        /// <summary>
        /// Type (BankCard, YooMoney, SberPay)
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Bank card info
        /// </summary>
        public SavedPaymentMethodBankCardDto? BankCardDetails { get; set; }

        /// <summary>
        /// Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Metadata
        /// </summary>
        public string Metadata { get; set; }

        /// <summary>
        /// Is default autopayment flag
        /// </summary>
        public bool DefaultPaymentMethod { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SavedPaymentMethod, SavedPaymentMethodDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.AccountId, z => z.MapFrom(y => y.AccountId))
                .ForMember(x => x.CreatedOn, z => z.MapFrom(y => y.CreatedOn))
                .ForMember(x => x.Title, z => z.MapFrom(y => y.Title))
                .ForMember(x => x.Type, z => z.MapFrom(y => y.Type))
                .ForMember(x => x.Metadata, z => z.MapFrom(y => y.Metadata))
                .ForMember(x => x.Token, z => z.MapFrom(y => y.Token))
                .ForMember(x => x.BankCardDetails, z => z.MapFrom(y => y.BankCard));
        }
    }
}
