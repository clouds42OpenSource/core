﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SavedPaymentMethodContext.Dtos;
using Core42.Application.Features.SavedPaymentMethodContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SavedPaymentMethodContext.Handlers
{
    /// <summary>
    /// Get all saved payments methods by account id query handler
    /// </summary>
    public class GetAllSavedPaymentsMethodsByAccountIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetAllSavedPaymentsMethodsByAccountIdQuery, ManagerResult<List<SavedPaymentMethodDto>>>
    {
        /// <summary>
        /// Handle get all saved payments methods by account id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<SavedPaymentMethodDto>>> Handle(GetAllSavedPaymentsMethodsByAccountIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Payments_System, () => request.AccountId);
                

                if (request.AccountId == null)
                {
                   throw new ArgumentNullException(nameof(request.AccountId));
                }

                var savedPayments = (await unitOfWork.SavedPaymentMethodRepository
                    .AsQueryable()
                    .Include(x => x.BankCard)
                    .Where(x => x.AccountId == request.AccountId)
                    .ToListAsync(cancellationToken)).Select(x => mapper.Map<SavedPaymentMethodDto>(x)).ToList();
                       

                var autoPay = await unitOfWork.AccountAutoPayConfigurationRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.AccountId == request.AccountId, cancellationToken);

                savedPayments.ForEach(x =>
                {
                    x.DefaultPaymentMethod = autoPay != null && autoPay.SavedPaymentMethodId == x.Id;
                });

                return savedPayments.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[При получении сохраненных спсособов оплаты возникла ошибка]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<SavedPaymentMethodDto>>(ex.Message);
            }
        }
    }
}
