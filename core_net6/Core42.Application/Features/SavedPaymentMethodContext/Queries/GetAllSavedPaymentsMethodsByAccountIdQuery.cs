﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.SavedPaymentMethodContext.Dtos;
using MediatR;

namespace Core42.Application.Features.SavedPaymentMethodContext.Queries
{
    /// <summary>
    /// Get all saved payments methods by account id query
    /// </summary>
    public class GetAllSavedPaymentsMethodsByAccountIdQuery(Guid? accountId)
        : IRequest<ManagerResult<List<SavedPaymentMethodDto>>>
    {
        /// <summary>
        /// Account id
        /// </summary>
        public Guid? AccountId { get; set; } = accountId;
    }
}
