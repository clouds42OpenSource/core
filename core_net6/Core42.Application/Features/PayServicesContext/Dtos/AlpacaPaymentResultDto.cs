﻿namespace Core42.Application.Features.PayServicesContext.Dtos
{
    /// <summary>
    /// Account card main dto
    /// </summary>
    public class AlpacaPaymentResultDto
    {
        /// <summary>
        /// Токен подтверждения
        /// </summary>
        public string ConfirmationToken { get; set; }

        /// <summary>
        /// Урл на который пользователь вернется после оплаты 
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Урл для перенаправления пользователя для подтверждения платежа
        /// </summary>
        public string RedirectUrl { get; set; }

        /// <summary>
        /// Токен подтверждения (для виджета юкассы)
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// bltynbabrfnjh gkfnt;f
        /// </summary>
        public Guid InvoiceId { get; set; }
    }
}
