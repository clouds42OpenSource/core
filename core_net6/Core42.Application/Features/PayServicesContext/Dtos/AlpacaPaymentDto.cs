﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using MediatR;

namespace Core42.Application.Features.PayServicesContext.Dtos;

public class AlpacaPaymentDto : IRequest<ManagerResult<ConfirmYookassaPaymentForClientDataDto>>
{
    /// <summary>
    /// ID аккаунта
    /// </summary>
    public Guid AccountId { get; set; }

    /// <summary>
    /// Описание
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Сумма инвойса
    /// </summary>
    public decimal Amount { get; set; }

    /// <summary>
    /// URL переадресации после успешной оплаты
    /// </summary>
    public string ReturnUrl { get; set; }

    /// <summary>
    /// Идентификатор сохраненного способа оплаты
    /// </summary>
    public Guid? PaymentMethodId { get; set; }

    /// <summary>
    /// ID типа услуги сервиса
    /// </summary>
    public Guid ServiceTypeId { get; set; }

    public bool SavePaymentMethod { get; set; }

}
