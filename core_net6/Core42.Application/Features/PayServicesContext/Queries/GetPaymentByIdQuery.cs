﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.PayServicesContext.Queries
{
    public class GetPaymentByIdQuery : IRequest<ManagerResult<PaymentDto>>
    {
        public Guid PaymentId { get; set; }

        public Guid AccountId { get; set; }
    }
}
