﻿using AutoMapper;
using Clouds42.Domain.DataModels.billing;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.PayServicesContext.Queries;

public class PaymentDto : IMapFrom<Payment>
{

    /// <summary>
    /// Номер платежа
    /// </summary>
    public int Number { get; set; }

    /// <summary>
    /// Сумма
    /// </summary>
    public decimal Sum { get; set; }

    /// <summary>
    /// Дата совершения платежа
    /// </summary>
    public DateTime Date { get; set; }

    /// <summary>
    /// Статус обработки платежа
    /// </summary>
    public string Status { get; set; }

    /// <summary>
    /// Платежная система
    /// </summary>
    public string PaymentSystem { get; set; }

    /// <summary>
    /// Назначение платежа
    /// </summary>
    public string Description { get; set; }

    public Guid AccountId { get; set; }
    public Guid Id { get; set; }

    /// <summary>
    /// Внешний номер платежа
    /// </summary>
    public string PaymentSystemTransactionId { get; set; }

    /// <summary>
    /// Токен подтверждения платежа (для Yookassa)
    /// </summary>
    public string ConfirmationToken { get; set; }
    public void Mapping(Profile profile)
    {
        profile.CreateMap<Payment,PaymentDto > ()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.Id))
            .ForMember(x => x.Date, z => z.MapFrom(y => y.Date))
            .ForMember(x => x.Status, z => z.MapFrom(y => y.Status))
            .ForMember(x => x.PaymentSystem, z => z.MapFrom(y => y.PaymentSystem))
            .ForMember(x => x.Description, z => z.MapFrom(y => y.Description))
            .ForMember(x => x.AccountId, z => z.MapFrom(y => y.AccountId))
            .ForMember(x => x.PaymentSystemTransactionId, z => z.MapFrom(y => y.PaymentSystemTransactionId))
            .ForMember(x => x.ConfirmationToken, z => z.MapFrom(y => y.ConfirmationToken))
            .ForMember(x => x.ConfirmationToken, z => z.MapFrom(y => y.ConfirmationToken))
            .ForMember(x => x.Number, z => z.MapFrom(y => y.Number))
            .ForMember(x => x.Sum, z => z.MapFrom(y => y.Sum));

    }
}
