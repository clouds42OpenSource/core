﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.InvoiceContext.Enums;
using Core42.Application.Features.PayServicesContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PayServicesContext.Handlers;

public class GetPaymentByIdHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
    : IRequestHandler<GetPaymentByIdQuery, ManagerResult<PaymentDto>>
{
    public async Task<ManagerResult<PaymentDto>> Handle(GetPaymentByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var payment = await unitOfWork.PaymentRepository
                .AsQueryable()
                .ProjectTo<PaymentDto>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == request.PaymentId && x.AccountId == request.AccountId,
                    cancellationToken);

            if (payment == null)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<PaymentDto>(
                    $"Не найден платеж по идентификатору = {request.PaymentId} и accountId = {request.AccountId}",
                    (int)InvoiceCodes.NotFound);
            }
            
            return payment.ToOkManagerResult((int)InvoiceCodes.Ok);
            
        }

        catch (Exception ex)
        {
            logger.Error(ex, ex.Message);
            return PagedListExtensions.ToPreconditionFailedManagerResult<PaymentDto>(ex.Message,
                (int)InvoiceCodes.ServerError);
        }
    }
}
