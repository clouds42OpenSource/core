﻿using Clouds42.Billing.BillingOperations.Managers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Providers;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PayServicesContext.Dtos;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PayServicesContext.Handlers
{
    public class CreateAlpacaPaymentCommandHandler(IServiceProvider serviceProvider)
        : IRequestHandler<AlpacaPaymentDto, ManagerResult<ConfirmYookassaPaymentForClientDataDto>>
    {
        private readonly IHandlerException _handlerException = serviceProvider.GetRequiredService<IHandlerException>();
        private readonly IUnitOfWork _unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly ILogger42 _logger = serviceProvider.GetRequiredService<ILogger42>();
        private readonly IInvoiceCreationProvider _innerProvider = serviceProvider.GetRequiredService<IInvoiceCreationProvider>();
        private readonly IPayYookassaAggregatorPaymentProvider _prepareYookassaAggregatorPaymentProvider = serviceProvider.GetRequiredService<IPayYookassaAggregatorPaymentProvider>();
        private readonly RunTaskForYookassaAggregatorManager _runTaskForYookassaAggregatorManager = serviceProvider.GetRequiredService<RunTaskForYookassaAggregatorManager>();

        public async Task<ManagerResult<ConfirmYookassaPaymentForClientDataDto>> Handle(AlpacaPaymentDto request,
            CancellationToken cancellationToken)
        {

            try
            {
                var createInvoiceDc = ValidateAndConvertToCreateInvoiceDc(request);
                _logger.Info($"[{request.AccountId}]:: Начинаем создавать инвойс с суммой {request.Amount}, описанием {request.Description}");

                var invoice = await _innerProvider.CreateInvoiceAsync(createInvoiceDc);
                _logger.Info($"[{request.AccountId}]:: Cоздаем инвойс с суммой {request.Amount}, описанием {request.Description}, номер инвойса {invoice.Id}");

                var supplier = await _unitOfWork.SupplierRepository.AsQueryableNoTracking().FirstOrDefaultAsync(s=>s.Name == "ООО \"УчетОнлайн\"", cancellationToken: cancellationToken);

                var service = await _unitOfWork.BillingServiceTypeRepository.AsQueryableNoTracking().FirstOrDefaultAsync(s=>s.Id == request.ServiceTypeId, cancellationToken: cancellationToken);

                var yookassaModel = new PrepareYookassaAggregatorPaymentDto
                {
                    AccountId = request.AccountId,
                    PaymentSum = request.Amount,
                    ReturnUrl = request.ReturnUrl,
                    PaymentMethodId = request.PaymentMethodId,
                    IsAutoPay = request.SavePaymentMethod,
                    SupplierId = supplier?.Id,
                    ServiceId = service?.ServiceId,
                };

                _logger.Info($"[{request.AccountId}]:: Подгатавливаем платеж в юкассе на сумму {request.Amount}, сохраненный метод оплаты {request.PaymentMethodId}");

                var yookassaResult = _prepareYookassaAggregatorPaymentProvider.PreparePaymentOrPayWithSavedPaymentMethod(yookassaModel);

                if (yookassaModel.PaymentMethodId != null)
                {
                    var paymentMethod = await _unitOfWork.SavedPaymentMethodRepository.AsQueryableNoTracking().FirstOrDefaultAsync(apm => apm.Id == yookassaModel.PaymentMethodId, cancellationToken: cancellationToken);
                    yookassaResult.ConfirmationToken = paymentMethod?.Token;
                }

                _runTaskForYookassaAggregatorManager.RunTaskToCheckYookassaPaymentStatus(yookassaResult.PaymentId);

                return ManagerResultHelper.Ok(yookassaResult);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Во время проведения платежа для  Alpaca произошла ошибка {ex.Message}");
                return PagedListExtensions.ToPreconditionFailedManagerResult<ConfirmYookassaPaymentForClientDataDto>($"Во время проведения платежа для  Alpaca произошла ошибка {ex.Message}");
            }
        }

        private CreateInvoiceDto ValidateAndConvertToCreateInvoiceDc(AlpacaPaymentDto arbitraryAmountModel)
        {
            if (arbitraryAmountModel.Amount <= 0)
                throw new InvalidOperationException("Сумма должна быть больше 0");
            var result = new CreateInvoiceDto
            {
                AccountId = arbitraryAmountModel.AccountId,
                PayPeriod = default,
                Total = arbitraryAmountModel.Amount,
                Products =
                [
                    new()
                    {
                        Description =
                            string.IsNullOrWhiteSpace(arbitraryAmountModel.Description)
                                ? "-"
                                : arbitraryAmountModel.Description,
                        Name = "AlpacaMeet",
                        ServiceTypeId = arbitraryAmountModel.ServiceTypeId,
                        Quantity = 1,
                        TotalPrice = arbitraryAmountModel.Amount
                    }
                ]
            };
            return result;
        }

    }
}
