﻿using Clouds42.Domain.Enums.CoreWorker;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTasksQueueContext.Queries.Filters
{
    /// <summary>
    /// Core worker tasks queue filter
    /// </summary>
    public class CoreWorkerTasksQueueFilter : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Worker id
        /// </summary>
        public short? WorkerId { get; set; }

        /// <summary>
        /// Task id
        /// </summary>
        public Guid? TaskId { get; set; }

        /// <summary>
        /// Task status
        /// </summary>
        public CloudTaskQueueStatus? Status { get; set; }

        /// <summary>
        /// Period from
        /// </summary>
        public DateTime? PeriodFrom { get; set; }

        /// <summary>
        /// Period to
        /// </summary>
        public DateTime? PeriodTo { get; set; }

        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }
    }
}
