﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.CoreWorkerTasksQueueContext.Dtos;
using Core42.Application.Features.CoreWorkerTasksQueueContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTasksQueueContext.Queries
{
    /// <summary>
    /// Get core worker tasks queue filtered query
    /// </summary>
    public class GetCoreWorkerTasksQueueFilteredQuery :
        IPagedQuery,
        ISortedQuery,
        IHasFilter<CoreWorkerTasksQueueFilter>,
        IRequest<ManagerResult<PagedDto<CoreWorkerTaskInQueueItemDto>>>
    {
        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(CoreWorkerTasksQueue.CreateDate)}.desc";

        /// <summary>
        /// Core worker tasks queue filter
        /// </summary>
        public CoreWorkerTasksQueueFilter? Filter { get; set; }
    }
}
