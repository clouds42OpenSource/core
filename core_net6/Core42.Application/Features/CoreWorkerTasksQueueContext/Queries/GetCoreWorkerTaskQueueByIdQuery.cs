﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerTasksQueueContext.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTasksQueueContext.Queries
{
    /// <summary>
    /// Get core worker tasks queue by id query
    /// </summary>
    public class GetCoreWorkerTaskQueueByIdQuery(Guid id) : IRequest<ManagerResult<TaskInQueueItemInfoDto>>
    {
        /// <summary>
        /// Tasks queue item id
        /// </summary>
        public Guid Id { get; set; } = id;
    }

    /// <summary>
    /// Get core worker tasks queue by id query handler
    /// </summary>
    public class GetCoreWorkerTaskQueueByIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        ILogger42 logger)
        : IRequestHandler<GetCoreWorkerTaskQueueByIdQuery, ManagerResult<TaskInQueueItemInfoDto>>
    {
        /// <summary>
        /// Handle get core worker tasks queue by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<TaskInQueueItemInfoDto>> Handle(GetCoreWorkerTaskQueueByIdQuery request, CancellationToken cancellationToken)
        {

            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);

                return (await unitOfWork.CoreWorkerTasksQueueRepository
                    .AsQueryable()
                    .ProjectTo<TaskInQueueItemInfoDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.TaskInQueueItemId == request.Id, cancellationToken))
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения информации по задаче из очереди задач воркера. Причина: {ex.Message}";
                logger.Warn(errorMessage);

                return PagedListExtensions.ToPreconditionFailedManagerResult<TaskInQueueItemInfoDto>(errorMessage);
            }
        }
    }
}
