﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CoreWorkerTasksQueueContext.Dtos
{
    /// <summary>
    /// Task in queue item info dto
    /// </summary>
    public class TaskInQueueItemInfoDto : IMapFrom<CoreWorkerTasksQueue>
    {
        /// <summary>
        /// Task in queue item id
        /// </summary>
        public Guid TaskInQueueItemId { get; set; }

        /// <summary>
        /// Task name
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Task params
        /// </summary>

        public string TaskParams { get; set; }

        /// <summary>
        /// Start date time
        /// </summary>
        public DateTime? StartDateTime { get; set; }

        /// <summary>
        /// Priority
        /// </summary>
        public int? Priority { get; set; }

        /// <summary>
        /// Task type
        /// </summary>
        public WorkerTaskTypeEnum TaskType { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CoreWorkerTasksQueue, TaskInQueueItemInfoDto>()
                .ForMember(x => x.TaskInQueueItemId, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.TaskParams, z => z.MapFrom(y => y.TaskParameter.TaskParams))
                .ForMember(x => x.Comment, z => z.MapFrom(y => y.Comment))
                .ForMember(x => x.StartDateTime, z => z.MapFrom(y => y.StartDate))
                .ForMember(x => x.TaskType, z => z.MapFrom(y => y.CoreWorkerTask.WorkerTaskType))
                .ForMember(x => x.Priority, z => z.MapFrom(y => y.CoreWorkerTask.CoreWorkerAvailableTasksBags
                                                            .FirstOrDefault(item => item.CoreWorkerTaskId == y.CoreWorkerTaskId && item.CoreWorkerId == (y.CapturedWorkerId ?? -1)) != null ?
                                                            y.CoreWorkerTask.CoreWorkerAvailableTasksBags.
                                                            FirstOrDefault(item => item.CoreWorkerTaskId == y.CoreWorkerTaskId && item.CoreWorkerId == (y.CapturedWorkerId ?? -1)).Priority : (int?)null))
                .ForMember(x => x.Status, z => z.MapFrom(y => y.Status))
                .ForMember(x => x.TaskName, z => z.MapFrom(y => y.CoreWorkerTask.TaskName));
        }
    }
}
