﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CoreWorkerTasksQueueContext.Dtos
{
    /// <summary>
    /// Core worker task in queue item dto
    /// </summary>
    public class CoreWorkerTaskInQueueItemDto : IMapFrom<CoreWorkerTasksQueue>
    {
        /// <summary>
        /// Task id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Task name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Create date time
        /// </summary>
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// Finish date time
        /// </summary>
        public DateTime? FinishDateTime { get; set; }

        /// <summary>
        /// Worker id
        /// </summary>
        public short? WorkerId { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CoreWorkerTasksQueue, CoreWorkerTaskInQueueItemDto>()
                .ForMember(x => x.Id, m => m.MapFrom(z => z.Id))
                .ForMember(x => x.Name, m => m.MapFrom(z => z.CoreWorkerTask.TaskName))
                .ForMember(x => x.Status, m => m.MapFrom(z => z.Status))
                .ForMember(x => x.WorkerId, m => m.MapFrom(z => z.CapturedWorkerId))
                .ForMember(x => x.FinishDateTime, m => m.MapFrom(z => z.EditDate))
                .ForMember(x => x.CreateDateTime, m => m.MapFrom(z => z.CreateDate))
                .ForMember(x => x.Comment, m => m.MapFrom(z => z.Comment));
        }
    }
}
