﻿using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTasksQueueContext.Specifications
{
    /// <summary>
    /// Core worker tasks queue specification
    /// </summary>
    public static class CoreWorkerTaskQueueSpecification
    {
        /// <summary>
        /// Filter queue tasks by status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static Spec<CoreWorkerTasksQueue> ByStatus(string? status)
        {
            return new(x => string.IsNullOrEmpty(status) || x.Status == status);
        }

        /// <summary>
        /// Filter queue tasks by search line
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<CoreWorkerTasksQueue> BySearchLine(string? searchLine)
        {
            return new(x => string.IsNullOrEmpty(searchLine) || x.TaskParameter.TaskParams.Contains(searchLine));
        }

        /// <summary>
        /// Filter queue tasks by worker id
        /// </summary>
        /// <param name="workerId"></param>
        /// <returns></returns>
        public static Spec<CoreWorkerTasksQueue> ByWorkerId(short? workerId)
        {
            return new(x => !workerId.HasValue || x.CapturedWorkerId == workerId.Value);
        }

        /// <summary>
        /// Filter queue tasks by task id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public static Spec<CoreWorkerTasksQueue> ByTaskId(Guid? taskId)
        {
            return new(x => !taskId.HasValue || x.CoreWorkerTaskId == taskId.Value);
        }


        /// <summary>
        /// Filter queue tasks by date
        /// </summary>
        /// <param name="periodFrom"></param>
        /// <param name="periodTo"></param>
        /// <returns></returns>
        public static Spec<CoreWorkerTasksQueue> ByDate(DateTime? periodFrom, DateTime? periodTo)
        {
            return new(x => (!periodFrom.HasValue || x.CreateDate >= periodFrom) && (!periodTo.HasValue || x.CreateDate <= periodTo));
        }
    }
}
