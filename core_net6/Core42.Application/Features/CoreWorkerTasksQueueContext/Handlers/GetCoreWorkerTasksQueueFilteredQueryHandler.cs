﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerTasksQueueContext.Dtos;
using Core42.Application.Features.CoreWorkerTasksQueueContext.Queries;
using Core42.Application.Features.CoreWorkerTasksQueueContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTasksQueueContext.Handlers
{
    /// <summary>
    /// Get core worker tasks queue filtered query handler
    /// </summary>
    public class GetCoreWorkerTasksQueueFilteredQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        ILogger42 logger)
        : IRequestHandler<GetCoreWorkerTasksQueueFilteredQuery, ManagerResult<PagedDto<CoreWorkerTaskInQueueItemDto>>>
    {
        /// <summary>
        /// Handle get core worker tasks queue filtered query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<CoreWorkerTaskInQueueItemDto>>> Handle(GetCoreWorkerTasksQueueFilteredQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);

                return (await unitOfWork
                    .CoreWorkerTasksQueueRepository
                    .AsQueryable()
                    .Where(CoreWorkerTaskQueueSpecification.ByStatus(request?.Filter?.Status?.ToString()) &&
                           CoreWorkerTaskQueueSpecification.BySearchLine(request?.Filter?.SearchLine) &&
                           CoreWorkerTaskQueueSpecification.ByWorkerId(request?.Filter?.WorkerId) &&
                           CoreWorkerTaskQueueSpecification.ByTaskId(request?.Filter?.TaskId) &&
                           CoreWorkerTaskQueueSpecification.ByDate(request?.Filter?.PeriodFrom, request?.Filter?.PeriodTo))
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<CoreWorkerTaskInQueueItemDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения списка задач из очереди воркеров. Причина: {ex.Message}";
                logger.Warn(errorMessage);

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<CoreWorkerTaskInQueueItemDto>>(errorMessage);
            }
        }
    }
}
