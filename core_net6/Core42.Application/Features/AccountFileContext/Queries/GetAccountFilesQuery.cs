﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using LinqExtensionsNetFramework;
using MappingNetStandart.Mappings;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountFileContext.Queries
{
    public class GetAccountFilesQuery : IRequest<ManagerResult<PagedDto<AccountFileDto>>>, IHasFilter<AccountFilesFilter>, IPagedQuery, ISortedQuery, IHasSpecificSearch
    {
        public AccountFilesFilter? Filter { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public string? OrderBy { get; set; } = $"{nameof(AccountFileDto.Status)}.asc";
        public string? SearchLine { get; set; }
    }

    public class GetAccountFilesQueryHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
        : IRequestHandler<GetAccountFilesQuery, ManagerResult<PagedDto<AccountFileDto>>>
    {
        public async Task<ManagerResult<PagedDto<AccountFileDto>>> Handle(GetAccountFilesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                return (await unitOfWork.AccountFileRepository
                        .AsQueryableNoTracking()
                        .Where(AccountFileSpecification.BySearchLine(request.SearchLine))
                        .ProjectTo<AccountFileDto>(mapper.ConfigurationProvider)
                        .AutoSort(request)
                        .AutoFilter(request.Filter)
                        .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception e)
            {
                logger.Error(e, "Ошибка получения списка документов аккаунтов");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AccountFileDto>>(
                    "Ошибка получения списка документов аккаунтов");
            }
        }
    }

    public class AccountFilesFilter : IQueryFilter
    {
        public VerificationStatus? Status { get; set; }
        public DocumentType? Type { get; set; } = DocumentType.TaxAuthorityRegistrationCertificate;
        public int? AccountIndexNumber { get; set; }
        public Guid? AccountId { get; set; }
    }

    public class AccountFileDto : IMapFrom<AccountFile>
    {
        public Guid Id { get; set; }
        public VerificationStatus Status { get; set; }
        public DocumentType Type { get; set; }
        public int AccountIndexNumber { get; set; }
        public string AccountCaption { get; set; }
        public Guid AccountId { get; set; }
        public Guid CloudFileId { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountFile, AccountFileDto>()
            .ForMember(x => x.AccountIndexNumber, z => z.MapFrom(x => x.Account.IndexNumber))
            .ForMember(x => x.AccountCaption, z => z.MapFrom(x => x.Account.AccountCaption));
        }
    }

    public static class AccountFileSpecification
    {
        public static Spec<AccountFile> BySearchLine(string? searchLine)
        {
            return new Spec<AccountFile>(x => string.IsNullOrEmpty(searchLine) || x.Account.AccountCaption.Contains(searchLine));
        }
    }
}
