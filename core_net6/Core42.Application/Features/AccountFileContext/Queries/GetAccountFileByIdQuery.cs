﻿using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountFileContext.Queries
{
    public class GetAccountFileByIdQuery(Guid accountId, Guid fileId) : IRequest<ManagerResult<CloudFile>>
    {

        public Guid AccountId { get; set; } = accountId;
        public Guid FileId { get; set; } = fileId;
    }

    public class GetAccountFileByIdQueryHandler(IUnitOfWork unitOfWork, ILogger42 logger)
        : IRequestHandler<GetAccountFileByIdQuery, ManagerResult<CloudFile>>
    {
        public async Task<ManagerResult<CloudFile>> Handle(GetAccountFileByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var file = await unitOfWork.AccountFileRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.CloudFile)
                    .FirstOrDefaultAsync(x => x.AccountId == request.AccountId &&
                    x.CloudFileId == request.FileId, cancellationToken);

                return file == null ?
                    ManagerResultHelper.PreconditionFailed<CloudFile>(null, "Не найден файл аккаунта") :
                    file.CloudFile.ToOkManagerResult();
            }

            catch (Exception e)
            {
                logger.Error(e, "Ошибка получения файла аккаунта");

                return ManagerResultHelper.PreconditionFailed<CloudFile>(null, "Ошибка получения файла аккаунта");
            }
        }
    }
}
