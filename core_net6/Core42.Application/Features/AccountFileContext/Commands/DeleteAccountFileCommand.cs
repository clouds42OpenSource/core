﻿using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.AccountFileContext.Commands
{
    public class DeleteAccountFileCommand(Guid accountId, Guid fileId) : IRequest<ManagerResult>
    {
        public Guid AccountId { get; set; } = accountId;
        public Guid FileId { get; set; } = fileId;
    }

    public class DeleteAccountFileCommandHandler(ILogger42 logger, IUnitOfWork unitOfWork)
        : IRequestHandler<DeleteAccountFileCommand, ManagerResult>
    {
        public async Task<ManagerResult> Handle(DeleteAccountFileCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var accountFile = await unitOfWork.AccountFileRepository
                    .AsQueryable()
                    .Include(x => x.CloudFile)
                    .FirstOrDefaultAsync(x => x.Id == request.FileId && x.AccountId == request.AccountId,
                        cancellationToken);

                if (accountFile == null)
                {
                    return ManagerResultHelper.PreconditionFailed("Не удалось найти файл для удаления");
                }

                unitOfWork.CloudFileRepository.Delete(accountFile.CloudFile);
                unitOfWork.AccountFileRepository.Delete(accountFile);
                await unitOfWork.SaveAsync();

                return ManagerResultHelper.Ok("Файл успешно удален");

            }
            catch (Exception e)
            {
               logger.Error(e, "Не удалось удалить файл аккаунта");

               return ManagerResultHelper.PreconditionFailed("Произошла ошибка при попытке удаления файла");
            }
        }
    }
}
