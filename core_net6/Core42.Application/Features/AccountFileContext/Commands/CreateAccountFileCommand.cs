﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.EmailContext.Commands;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.AccountFileContext.Commands
{
    public class CreateAccountFileCommand : IRequest<ManagerResult>
    {
        public IFormFile File { get; set; }
        public Guid AccountId { get; set; }
        public DocumentType Type { get; set; }
    }

    public class CreateAccountFileCommandHandler(
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        ICloudFileProvider cloudFileProvider,
        ISender sender,
        IAccessProvider accessProvider)
        : IRequestHandler<CreateAccountFileCommand, ManagerResult>
    {
        public async Task<ManagerResult> Handle(CreateAccountFileCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var account = await unitOfWork.AccountsRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == request.AccountId, cancellationToken);

                if (account == null)
                {
                    return ManagerResultHelper.PreconditionFailed("Не найден аккаунт для верификации");
                }

                var fileId = cloudFileProvider.CreateCloudFile(new FileDataDto<IFormFile>
                {
                    Content = request.File,
                    FileName = request.File.FileName
                });

                unitOfWork.AccountFileRepository.Insert(new AccountFile
                {
                    Id = Guid.NewGuid(),
                    AccountId = request.AccountId,
                    CloudFileId = fileId,
                    Status = VerificationStatus.UnderReview,
                    Type = request.Type,
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now
                });

                var managerEmail = CloudConfigurationProvider.Emails.Get42CloudsManagerEmail();
                var fromEmail = CloudConfigurationProvider.Emails.GetCloudServicesEmail();

                await sender.Send(
                    new SendEmailCommand
                    {
                        Body =
                            $"Аккаунт {account.IndexNumber} добавил новый документ \"{request.Type.GetDisplayName()}\", необходимо провести проверку.",
                        To = managerEmail,
                        Header = $"Проверка документа \"{request.Type.GetDisplayName()}\"",
                        Subject = $"Проверка документа \"{request.Type.GetDisplayName()}\"",
                        From = fromEmail,
                        Categories = ["file-verification"],
                        SendGridTemplateId = CloudConfigurationProvider.SendGrid.GetDefaultTemplateId()
                    }, cancellationToken);

                LogEventHelper.LogEvent(unitOfWork,
                    account.Id,
                    accessProvider,
                    LogActions.LoadAccountFile,
                    $"Документ \"{request.Type.GetDisplayName()}\" отправлен на проверку менеджеру");

                await unitOfWork.SaveAsync();

                return ManagerResultHelper.Ok("Документ успешно создан и отправлен на верификацию");
            }

            catch (Exception e)
            {
                logger.Error(e, "Ошибка создания файла аккаунта");

                return ManagerResultHelper.PreconditionFailed("Произошла ошибка при попытке прикрепить файл к компании");
            }
        }

    }
}
