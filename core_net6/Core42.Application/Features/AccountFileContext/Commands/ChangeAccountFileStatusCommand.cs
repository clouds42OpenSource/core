﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.AccountFileContext.Commands
{
    public class ChangeAccountFileStatusCommand : IRequest<ManagerResult>
    {
        public Guid Id { get; set; }
        public VerificationStatus Status { get; set; }
    }

    public class ChangeAccountFileStatusCommandHandler(
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        IAccessProvider accessProvider)
        : IRequestHandler<ChangeAccountFileStatusCommand, ManagerResult>
    {
        public async Task<ManagerResult> Handle(ChangeAccountFileStatusCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var accountFile = await unitOfWork.AccountFileRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                if (accountFile == null)
                {
                    return ManagerResultHelper.PreconditionFailed("Не удалось найти файл компании");
                }

                accountFile.Status = request.Status;
                accountFile.UpdatedOn = DateTime.Now;

                unitOfWork.AccountFileRepository.Update(accountFile);

                await unitOfWork.SaveAsync();

                LogEventHelper.LogEvent(unitOfWork,
                    accountFile.AccountId,
                    accessProvider,
                    LogActions.ChangeAccountFileStatus,
                    $"Менеджер выставляет статус \"{request.Status.GetDisplayName()}\" документу \"{accountFile.Type.GetDisplayName()}\"");

                return ManagerResultHelper.Ok("Успешно изменен статус");
            }

            catch (Exception e)
            {
                logger.Error(e, "Ошибка смены статуса файла компании");

                return ManagerResultHelper.Ok("Не удалось сменить статус файла компании");
            }

        }
    }
}
