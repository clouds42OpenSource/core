﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.Configuration1CContext.Dtos
{
    /// <summary>
    /// Configuration 1C dto
    /// </summary>
    public class Configurations1CDto : IMapFrom<Configurations1C>, IConfigurations1C
    {
        /// <summary>
        /// Configuration name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ITS configuration catalog
        /// </summary>
        public string ConfigurationCatalog { get; set; }

        /// <summary>
        /// Redaction
        /// </summary>
        public string RedactionCatalogs { get; set; }

        /// <summary>
        /// Platform
        /// </summary>
        public string PlatformCatalog { get; set; }

        /// <summary>
        /// Catalog adress update
        /// </summary>
        public string UpdateCatalogUrl { get; set; }

        /// <summary>
        /// Configuration short code
        /// </summary>
        public string ShortCode { get; set; }

        /// <summary>
        /// Use com connection gor apply updates
        /// </summary>
        public bool UseComConnectionForApplyUpdates { get; set; }

        /// <summary>
        /// Need check updates flag
        /// </summary>
        public bool NeedCheckUpdates { get; set; }

        /// <summary>
        /// Its authorization data id
        /// </summary>
        public Guid ItsAuthorizationDataId { get; set; }

        /// <summary>
        /// Configuration cost
        /// </summary>
        public decimal ConfigurationCost { get; set; }

        /// <summary>
        /// Configuration variations
        /// </summary>
        public List<string> ConfigurationVariations { get; set; } = [];

        /// <summary>
        /// Reductions
        /// </summary>
        public List<ConfigurationRedactionDto> Reductions { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Configurations1C, Configurations1CDto>()
                .ForMember(x => x.ConfigurationCost, z => z.MapFrom(y => y.ConfigurationServiceTypeRelation.ServiceType.Rates.OrderByDescending(x => x.Cost).FirstOrDefault() != null 
                ? y.ConfigurationServiceTypeRelation.ServiceType.Rates.OrderByDescending(x => x.Cost).FirstOrDefault().Cost : 0))
                .ForMember(x => x.ConfigurationVariations, z => z.MapFrom(y => y.ConfigurationNameVariations.Select(u => u.VariationName)));
        }
    }
}
