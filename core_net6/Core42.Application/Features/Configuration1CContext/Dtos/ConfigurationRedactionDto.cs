﻿namespace Core42.Application.Features.Configuration1CContext.Dtos
{
    /// <summary>
    /// Configuration redaction dto
    /// </summary>
    public class ConfigurationRedactionDto
    {
        /// <summary>
        /// Redaction catalog
        /// </summary>
        public string RedactionCatalog { get; set; }

        /// <summary>
        /// Url for download redaction update map
        /// </summary>
        public string UrlOfMapping { get; set; }
    }
}
