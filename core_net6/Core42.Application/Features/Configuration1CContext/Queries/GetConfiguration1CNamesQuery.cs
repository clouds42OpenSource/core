﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.Configuration1CContext.Queries
{
    /// <summary>
    /// Get conf 1C names query
    /// </summary>
    public class GetConfiguration1CNamesQuery : IRequest<ManagerResult<List<string>>>
    {
    }
}
