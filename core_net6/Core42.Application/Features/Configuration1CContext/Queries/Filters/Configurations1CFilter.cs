﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.Configuration1CContext.Queries.Filters
{
    /// <summary>
    /// Configuration 1C filter
    /// </summary>
    public class Configurations1CFilter : IQueryFilter
    {
        /// <summary>
        /// Configuration name
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Configration catalog on ITS
        /// </summary>
        public string? ConfigurationCatalog { get; set; }
    }
}


