﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.Configuration1CContext.Dtos;
using Core42.Application.Features.Configuration1CContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Configuration1CContext.Queries
{
    /// <summary>
    /// Configuration 1C filtered query
    /// </summary>
    public class GetConfiguration1CFilteredQuery : ISortedQuery, IPagedQuery, IHasFilter<Configurations1CFilter>, IRequest<ManagerResult<PagedDto<Configurations1CDto>>>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(Configurations1C.Name)}.asc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Confuguration 1C filter
        /// </summary>
        public Configurations1CFilter? Filter { get; set; }
    }
}
