﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Configuration1CContext.Dtos;
using Core42.Application.Features.Configuration1CContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Configuration1CContext.Handlers
{
    /// <summary>
    /// Get filtered sorted and paginated configuration 1C query handler
    /// </summary>
    public class GetConfiguration1CFilteredQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException)
        : IRequestHandler<GetConfiguration1CFilteredQuery, ManagerResult<PagedDto<Configurations1CDto>>>
    {
        /// <summary>
        /// Handle get filtered sorted and paginated configuration 1C query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<Configurations1CDto>>> Handle(GetConfiguration1CFilteredQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ViewConfiguration1C, () => accessProvider.ContextAccountId);

                var records = (await unitOfWork.Configurations1CRepository
                    .AsQueryable()
                    .AutoFilter(request.Filter)
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<Configurations1CDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto();

                foreach (var record in records.Records)
                {
                    var rootCatalog = Clouds42.Configurations.Configurations.CloudConfigurationProvider.Enterprise1C.GetCfuDownloadRootCatalogUrl();
                    var configurationRedactionDtos = record.RedactionCatalogs.Split(';').Select(redaction => new ConfigurationRedactionDto { RedactionCatalog = redaction, UrlOfMapping = $"{rootCatalog}/{record.ConfigurationCatalog}/{redaction.Replace(".", "")}/{record.PlatformCatalog.Replace(".", "")}/v8upd11.zip" }).ToList();

                    record.Reductions = configurationRedactionDtos;
                }

                return records.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                var message = $"[Ошибка получения списка] {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Configurations1C, accessProvider.ContextAccountId)}";
                handlerException.Handle(ex, message);

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<Configurations1CDto>>(ex.Message);
            }
        }
    }
}
