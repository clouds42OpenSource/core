﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Configuration1CContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Configuration1CContext.Handlers
{
    /// <summary>
    /// Get configuration 1C names query handler
    /// </summary>
    public class GetConfiguration1CNamesQueryHandler(
        IUnitOfWork unitOfWork,
        ICloudLocalizer cloudLocalizer,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetConfiguration1CNamesQuery, ManagerResult<List<string>>>
    {
        /// <summary>
        /// Handle get configuration 1C names query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<string>>> Handle(GetConfiguration1CNamesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AccountDatabase_GetConfiguration1CNames);

                return (await unitOfWork.Configurations1CRepository
                    .AsQueryable()
                    .Select(x => x.Name)
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                var message = $"[Ошибка получения списка названий] {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Configurations1C, accessProvider.ContextAccountId)}";
                handlerException.Handle(ex, message);

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<string>>(message);
            }
        }
    }
}
