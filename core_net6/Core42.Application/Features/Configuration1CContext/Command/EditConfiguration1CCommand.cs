﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AccountDatabase._1C.Validators;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorker.BillingServiceType.RecalculateServiceTypeCostAfterChange;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.Its;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using FluentValidation;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Configuration1CContext.Command
{
    public class EditConfiguration1CCommandResultDto
    {
        public List<ConfigurationRedactionModelDto> Reductions { get; set; }
    }
    public class EditConfiguration1CCommand : IRequest<ManagerResult<EditConfiguration1CCommandResultDto>>, IConfigurations1C
    {
        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Каталог конфигурации на ИТС
        /// </summary>
        public string ConfigurationCatalog { get; set; }

        /// <summary>
        /// Редакция
        /// </summary>
        public string RedactionCatalogs { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        public string PlatformCatalog { get; set; }

        /// <summary>
        /// Адрес каталога обновлений.
        /// </summary>
        public string UpdateCatalogUrl { get; set; }

        /// <summary>
        /// Краткий код конфигурации, например bp, zup и тд.
        /// </summary>
        public string ShortCode { get; set; }

        /// <summary>
        /// Признак что необходимо использовать COM соединения для принятия обновлений в базе
        /// </summary>
        public bool UseComConnectionForApplyUpdates { get; set; }

        /// <summary>
        /// Признак необходимости проверять наличие обновлений
        /// </summary>
        public bool NeedCheckUpdates { get; set; }

        /// <summary>
        /// Стоимость конфигурации
        /// </summary>
        public decimal ConfigurationCost { get; set; }

        /// <summary>
        /// ID данных авторизации в ИТС
        /// </summary>
        public Guid ItsAuthorizationDataId { get; set; }

        /// <summary>
        /// Вариации имени конфигурации 1С
        /// </summary>
        public List<string> ConfigurationVariations { get; set; } = [];
    }
    public class EditConfiguration1CCommandValidator : AbstractValidator<EditConfiguration1CCommand>
    {
        public EditConfiguration1CCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage("Название обязательное поле")
                .Length(3, 255)
                .WithMessage("Длина поля 'Название' должна быть не менее 3 символов и не должна превышать 255 символов");

            RuleFor(x => x.ConfigurationCatalog)
                .NotEmpty()
                .NotNull()
                .WithMessage("Каталог конфигурации обязательное поле")
                .Length(1, 255)
                .WithMessage("Длина поля 'Каталог конфигурации' должна быть не менее 1 символа и не должна превышать 255 символов");

            RuleFor(x => x.RedactionCatalogs)
                .NotEmpty()
                .NotNull()
                .WithMessage("Укажите хотя бы одну редакцию.")
                .Length(1, 255)
                .WithMessage("Длина поля 'Редакция' должна быть не менее 1 символа и не должна превышать 250 символов");

            RuleFor(x => x.PlatformCatalog)
                .NotEmpty()
                .NotNull()
                .WithMessage("Платформа обязательное поле")
                .Length(1, 3)
                .WithMessage("Длина поля 'Платформа' не должна превышать 3 символа");

            RuleFor(x => x.UpdateCatalogUrl)
                .NotEmpty()
                .NotNull()
                .WithMessage("Адрес каталога обновлений обязательное поле")
                .Length(1, 255)
                .WithMessage("Длина поля 'Адрес каталога обновлений' не должна превышать 255 символов");

            When(x => !string.IsNullOrEmpty(x.ShortCode), () =>
            {
                RuleFor(x => x.ShortCode)
                    .Length(1, 5)
                    .WithMessage("Длина поля 'Код конфигурации' не должна превышать 5 символов");
            });

            RuleFor(x => x.ConfigurationCost)
                .NotNull()
                .WithMessage("Стоимость конфигурации обязательное поле");
        }
    }

    public class EditConfiguration1CCommandHandler : IRequestHandler<EditConfiguration1CCommand, ManagerResult<EditConfiguration1CCommandResultDto>>
    {
        private readonly IAccessProvider _accessProvider;
        private readonly IUnitOfWork _unitOfWork;
        private readonly CatalogOfConfigurationValidator _configurationValidator;
        private readonly ICfuProvider _cfuProvider;
        private readonly ILogger42 _logger;
        private readonly RecalculateServiceTypeCostAfterChangeJobWrapper _afterChangesJobWrapper;
        public EditConfiguration1CCommandHandler(IAccessProvider accessProvider, CatalogOfConfigurationValidator configurationValidator, IUnitOfWork unitOfWork, ILogger42 logger, ICfuProvider cfuProvider, ISender sender, RecalculateServiceTypeCostAfterChangeJobWrapper afterChangesJobWrapper)
        {
            _accessProvider = accessProvider;
            _configurationValidator = configurationValidator;
            _unitOfWork = unitOfWork;
            _logger = logger;
            _cfuProvider = cfuProvider;
            _afterChangesJobWrapper = afterChangesJobWrapper;
        }

        public async Task<ManagerResult<EditConfiguration1CCommandResultDto>> Handle(EditConfiguration1CCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _accessProvider.HasAccess(ObjectAction.EditConfiguration1C, () => _accessProvider.ContextAccountId);

                _configurationValidator.ValidateConfiguration(request, out var errorMessage);

                if (!string.IsNullOrEmpty(errorMessage))
                    return PagedListExtensions.ToPreconditionFailedManagerResult<EditConfiguration1CCommandResultDto>(
                        errorMessage);

                var configuration = await
                    _unitOfWork.Configurations1CRepository
                        .AsQueryable()
                        .FirstOrDefaultAsync(x => x.Name == request.Name, cancellationToken) ??
                    throw new NotFoundException(
                        $"Для редактирования конфигурации {request.Name} не найден обьект в базе");

                await EditConfigurations1CDomainModelFromAsync(request, configuration);

                var serviceType = (await _unitOfWork
                                        .GetGenericRepository<ConfigurationServiceTypeRelation>()
                                        .AsQueryable()
                                        .Include(x => x.ServiceType)
                                        .ThenInclude(x => x.Service)
                                        .FirstOrDefaultAsync(relation => relation.Configuration1CName == request.Name, cancellationToken))?.ServiceType ??
                                    throw new NotFoundException($"Не удалось определить услугу по названию конфигурации '{request.Name}'");
                var redaction = _cfuProvider.GetConfigurationRedactionInfo(configuration);

                var rate = await _unitOfWork.RateRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(w => w.BillingServiceTypeId == serviceType.Id, cancellationToken) ?? throw new NotFoundException($"Не удалось получить тариф для услуги '{serviceType.Id}'");
                
                if (rate.Cost == request.ConfigurationCost)
                {
                    return new EditConfiguration1CCommandResultDto
                    {
                        Reductions = redaction

                    }.ToOkManagerResult();
                }

                rate.Cost = request.ConfigurationCost;

                _unitOfWork.RateRepository.Update(rate);
                await _unitOfWork.SaveAsync();

                _afterChangesJobWrapper.Start(new RecalculateServiceTypeCostAfterChangeJobParamsDto { ServiceTypeId = serviceType.Id, Cost = request.ConfigurationCost, ServiceType = serviceType.Service.SystemService, ServiceId = serviceType.ServiceId});

                LogEventHelper.LogEvent(_unitOfWork, _accessProvider.ContextAccountId, _accessProvider,
                    LogActions.ChangeConfigurationCost,
                    $"Изменение стоимости лицензии на доступ в конфигурацию “{request.Name}”. Новая стоимость: {request.ConfigurationCost}");
              
                return new EditConfiguration1CCommandResultDto{ Reductions = redaction }.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                _logger.Error(ex,"Произошла ошибка при изменение и пересчете стоимости конфигурации 1С");

                return PagedListExtensions.ToPreconditionFailedManagerResult<EditConfiguration1CCommandResultDto>(
                    ex.Message);
            }
        }

        private async Task EditConfigurations1CDomainModelFromAsync(
            IConfigurations1C configurationToUpdateWithProps,
            Configurations1C configurationToUpdate)
        {
            configurationToUpdate.Name = configurationToUpdateWithProps.Name;
            configurationToUpdate.PlatformCatalog = configurationToUpdateWithProps.PlatformCatalog;
            configurationToUpdate.ConfigurationCatalog = configurationToUpdateWithProps.ConfigurationCatalog;
            configurationToUpdate.RedactionCatalogs = configurationToUpdateWithProps.RedactionCatalogs;
            configurationToUpdate.UpdateCatalogUrl = configurationToUpdateWithProps.UpdateCatalogUrl;
            configurationToUpdate.ShortCode = configurationToUpdateWithProps.ShortCode;
            configurationToUpdate.UseComConnectionForApplyUpdates =
                configurationToUpdateWithProps.UseComConnectionForApplyUpdates;
            configurationToUpdate.NeedCheckUpdates = configurationToUpdateWithProps.NeedCheckUpdates;
            configurationToUpdate.ItsAuthorizationDataId = configurationToUpdateWithProps.ItsAuthorizationDataId;

            _unitOfWork.Configurations1CRepository.Update(configurationToUpdate);

            var configurationNameVariationsOld = configurationToUpdate.ConfigurationNameVariations.ToList();

            if (configurationNameVariationsOld.Any())
                await _unitOfWork.BulkDeleteAsync(configurationNameVariationsOld);

            if (!configurationToUpdateWithProps.ConfigurationVariations.Any())
                return;

            var configurationNameVariations = configurationToUpdateWithProps.ConfigurationVariations.Select(w =>
                new ConfigurationNameVariation
                {
                    Id = Guid.NewGuid(),
                    Configuration1CName = configurationToUpdateWithProps.Name,
                    VariationName = w
                });

           await _unitOfWork.BulkInsertAsync(configurationNameVariations);
           await _unitOfWork.SaveAsync();
        }
    }

    public class AccountWithResourceCostDto
    {
        public Guid AccountId { get; set; }

        public decimal TotalCost { get; set; }
    }
}
