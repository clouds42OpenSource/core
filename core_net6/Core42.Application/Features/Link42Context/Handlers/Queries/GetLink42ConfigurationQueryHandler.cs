﻿using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Dtos;
using Core42.Application.Features.Link42Context.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using Microsoft.AspNetCore.Http;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Handlers.Queries;

public class GetLink42ConfigurationQueryHandler(
    IMapper mapper,
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IHttpContextAccessor httpContextAccessor)
    : IRequestHandler<GetLink42ConfigurationQuery,
        ManagerResult<Link42ConfigurationDto>?>
{
    private const string OsVersionHeader = "OSVersion";

    public async Task<ManagerResult<Link42ConfigurationDto>?> Handle(GetLink42ConfigurationQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var httpContext = httpContextAccessor.HttpContext;

            if (!httpContext!.Request.Headers.TryGetValue(OsVersionHeader, out var values))
            {
                var currentConfiguration = await unitOfWork.Link42ConfigurationsRepository
                    .AsQueryable()
                    .ProjectTo<Link42ConfigurationDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .FirstOrDefaultAsync(x => x.IsCurrentVersion, cancellationToken);


                return CheckClientAndGetConfiguration(currentConfiguration).ToOkManagerResult();
            }

            var osVersionString = values.FirstOrDefault();

            if (string.IsNullOrEmpty(osVersionString))
                return PagedListExtensions.ToPreconditionFailedManagerResult<Link42ConfigurationDto>("Значение заголовка версии не имело значение");

            if (!Version.TryParse(osVersionString, out var version))
                return PagedListExtensions.ToPreconditionFailedManagerResult<Link42ConfigurationDto>("Не удалось получить значение версии из заголовка");

            var majorVersion = version.Major;
            var minorVersion = version.Minor;

            var configuration = await unitOfWork.Link42ConfigurationsRepository
                .AsQueryable()
                .ProjectTo<Link42ConfigurationDto>(mapper.ConfigurationProvider)
                .AutoSort(request)
                .FirstOrDefaultAsync(
                    x => x.IsCurrentVersion && x.BitDepths.Any(z =>
                        z.MinSupportedMajorVersion <= majorVersion && z.MinSupportedMinorVersion <= minorVersion),
                    cancellationToken);

            return CheckClientAndGetConfiguration(configuration).ToOkManagerResult();
        }

        catch (Exception ex)
        {
            logger.Error(ex, "Произошла ошибка при получении информации о лимитах на скачивание Link42");

            return PagedListExtensions.ToPreconditionFailedManagerResult<Link42ConfigurationDto>(ex.Message);
        }
    }

    private Link42ConfigurationDto CheckClientAndGetConfiguration(Link42ConfigurationDto configuration)
    {
        var isElectronApp =
            httpContextAccessor.HttpContext!.Request.Headers.UserAgent.Any(x => x?.Contains("Electron") ?? false);
        if (!isElectronApp)
        {
            return configuration;
        }
        var os = httpContextAccessor.HttpContext.Request.Headers["sec-ch-ua-platform"];
        var osFromUserAgent = httpContextAccessor.HttpContext!.Request.Headers
            .UserAgent.Any(x => x?.Contains("Windows") ?? false) 
            ? "Windows" 
            : "Mac OS";
        var osResult = string.IsNullOrEmpty(osFromUserAgent) ? osFromUserAgent : os.ToString();
        
        logger.Info($"Клиент - Electron, операционная система - {osResult}");
        
        foreach (var bitDepth in configuration.BitDepths)
        {
            bitDepth.DownloadLink = RemoveLastSegment(bitDepth.DownloadLink);
        }

        return configuration;
    }

    static string RemoveLastSegment(string path)
    {
        const string pattern = @"/[^/]+$";

        return Regex.Replace(path, pattern, "");
    }
}
