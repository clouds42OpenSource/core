﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Dtos;
using Core42.Application.Features.Link42Context.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Handlers.Queries;

public class GetFilteredLink42ConfigurationQueryHandler(IMapper mapper, IUnitOfWork unitOfWork, ILogger42 logger)
    : IRequestHandler<GetFilteredLink42ConfigurationQuery, ManagerResult<PagedDto<Link42ConfigurationDto>>>
{
    public async Task<ManagerResult<PagedDto<Link42ConfigurationDto>>> Handle(GetFilteredLink42ConfigurationQuery request, CancellationToken cancellationToken)
    {
        try
        {
            return (await unitOfWork.Link42ConfigurationsRepository
                    .AsQueryable()
                    .ProjectTo<Link42ConfigurationDto>(mapper.ConfigurationProvider)
                    .AutoFilter(request.Filter)
                    .AutoSort(request)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }
        catch (Exception ex)
        {
            logger.Error(ex, "Ошибка получения списка конфигурации Link42");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<Link42ConfigurationDto>>(
                ex.Message);
        }
    }
}
