﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Dtos;
using Core42.Application.Features.Link42Context.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Handlers.Queries;

public class GetLink42ConfigurationByIdQueryHandler(IMapper mapper, IUnitOfWork unitOfWork, ILogger42 logger)
    : IRequestHandler<GetLink42ConfigurationByIdQuery, ManagerResult<Link42ConfigurationDto>>
{
    public async Task<ManagerResult<Link42ConfigurationDto>> Handle(GetLink42ConfigurationByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            return (await unitOfWork.Link42ConfigurationsRepository
                    .AsQueryable()
                    .ProjectTo<Link42ConfigurationDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            logger.Error(ex, $"Не удалось получить конфигурацию Link42 по идентификатору {request.Id}");

            return PagedListExtensions.ToPreconditionFailedManagerResult<Link42ConfigurationDto>(ex.Message);
        }
    }
}
