﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.Link;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Dtos;
using Core42.Application.Features.Link42Context.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Handlers.Queries;

/// <summary>
/// Get current and used link42 version by user login query handler
/// </summary>
public class GetCurrentLinkVersionAndUsedByUserLoginQueryHandler(ILogger42 logger, IUnitOfWork unitOfWork)
    : IRequestHandler<GetCurrentLinkVersionAndUsedByUserLoginQuery, ManagerResult<Dictionary<string, Link42InfoDto?>>>
{
    /// <summary>
    /// Handle get current and used link42 version by user login query
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<ManagerResult<Dictionary<string, Link42InfoDto?>>> Handle(GetCurrentLinkVersionAndUsedByUserLoginQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var nowDate = DateTime.Now;
            var thirtyDaysAgoDate = nowDate.AddDays(-30);

            var linkStarts = (await unitOfWork
                .GetGenericRepository<DatabaseLaunchRdpStartHistory>()
                .AsQueryable()
                .OrderByDescending(x => x.ActionCreated)
                .Where(
                    x => x.ActionCreated >= thirtyDaysAgoDate && x.ActionCreated <= nowDate && request.Logins.Contains(x.Login))
                .ToListAsync(cancellationToken))
                .DistinctBy(x => x.Login)
                .ToList();

            if (!linkStarts.Any() || string.IsNullOrEmpty(request.Link42Version))
                return request.Logins.ToDictionary(x => x, _ => (Link42InfoDto?)null).ToOkManagerResult();

            var resultParse = Version.TryParse(request.Link42Version, out var versionCurrent);
            var usedVersionsLogins = linkStarts.ToDictionary(x => x.Login, z => Version.Parse(z.LinkAppVersion));
            
            return request.Logins.ToDictionary(x => x, z => usedVersionsLogins.ContainsKey(z) ?
                new Link42InfoDto
                {
                    isCurrentVersion = resultParse && versionCurrent?.CompareTo(usedVersionsLogins[z]) == 0,
                    Version = usedVersionsLogins[z].ToString(),
                } : null).ToOkManagerResult();
        }

        catch (Exception e)
        {
            logger.Error("Ошибка получения актуальной версии Link42 по логину пользователя", e);

            return PagedListExtensions.ToPreconditionFailedManagerResult<Dictionary<string, Link42InfoDto?>>(e.Message);
        }
    }
}
