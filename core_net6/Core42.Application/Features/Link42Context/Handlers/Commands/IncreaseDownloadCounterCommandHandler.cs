﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Handlers.Commands;

public class IncreaseDownloadCounterCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger)
    : IRequestHandler<IncreaseDownloadCounterCommand, ManagerResult<Guid?>>
{
    public async Task<ManagerResult<Guid?>> Handle(IncreaseDownloadCounterCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var link42Configuration = await unitOfWork.Link42ConfigurationsRepository
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.Version == request.Version, cancellationToken);

            if (link42Configuration == null)
            {
                return ((Guid?)null).ToOkManagerResult();
            }

            link42Configuration.DownloadsCount += 1;
            unitOfWork.Link42ConfigurationsRepository.Update(link42Configuration);
            await unitOfWork.SaveAsync();
            
            return ((Guid?)link42Configuration.Id).ToOkManagerResult();

        }

        catch (Exception ex)
        {
            logger.Error(ex, "Ошибка при увелечении счетчика скачиваний Link42");

            return PagedListExtensions.ToPreconditionFailedManagerResult<Guid?>(ex.Message);
        }
    }
}
