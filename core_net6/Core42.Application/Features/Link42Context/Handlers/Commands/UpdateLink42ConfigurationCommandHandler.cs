﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Commands;
using Core42.Application.Features.Link42Context.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Handlers.Commands;

public class UpdateLink42ConfigurationCommandHandler(
    IMapper mapper,
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IAccessProvider accessProvider)
    : IRequestHandler<UpdateLink42ConfigurationCommand, ManagerResult<Link42ConfigurationDto>>
{
    public async Task<ManagerResult<Link42ConfigurationDto>> Handle(UpdateLink42ConfigurationCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var existsConfiguration = await unitOfWork.Link42ConfigurationsRepository
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            
            unitOfWork.Link42ConfigurationBitDepthRepository.DeleteRange(existsConfiguration!.BitDepths);

            var newBitDepths = request.BitDepths!
                .Select(x => new Link42ConfigurationBitDepth
                {
                    DownloadLink = x.DownloadLink,
                    BitDepth = x.BitDepth,
                    SystemType = x.SystemType,
                    Id = Guid.NewGuid(),
                    ConfigurationId = existsConfiguration.Id,
                    LinkAppType = x.LinkAppType,
                    MinSupportedMajorVersion = x.MinSupportedMajorVersion,
                    MinSupportedMinorVersion = x.MinSupportedMinorVersion
                }).ToList();

            unitOfWork.Link42ConfigurationBitDepthRepository.InsertRange(newBitDepths);

            existsConfiguration.DownloadsLimit = request.DownloadsLimit;
            existsConfiguration.Version = request.Version;
            existsConfiguration.UpdatedOn = DateTime.Now;
            existsConfiguration.UpdatedBy = (await accessProvider.GetUserAsync())?.Id;
            existsConfiguration.IsCurrentVersion = request.IsCurrentVersion;
            existsConfiguration.BitDepths = newBitDepths;

            unitOfWork.Link42ConfigurationsRepository.Update(existsConfiguration);

            await unitOfWork.SaveAsync();

            return mapper.Map<Link42ConfigurationDto>(existsConfiguration).ToOkManagerResult();
        }

        catch (Exception e)
        {
            logger.Error(e, $"При редактировании конфигурации с идентификатором {request.Id} произошла ошибка");

            return PagedListExtensions.ToPreconditionFailedManagerResult<Link42ConfigurationDto>(e.Message);
        }
    }
}
