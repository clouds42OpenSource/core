﻿using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Commands;
using Core42.Application.Features.Link42Context.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Handlers.Commands;

public class AddLink42ConfigurationCommandHandler(
    IMapper mapper,
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IAccessProvider accessProvider)
    : IRequestHandler<AddLink42ConfigurationCommand, ManagerResult<Link42ConfigurationDto>>
{
    public async Task<ManagerResult<Link42ConfigurationDto>> Handle(AddLink42ConfigurationCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var configurationId = Guid.NewGuid();
            
            var newConfiguration = new Link42Configuration
            {
                CreatedOn = DateTime.Now,
                DownloadsCount = 0,
                DownloadsLimit = request.DownloadsLimit,
                Id = configurationId,
                Version = request.Version,
                IsCurrentVersion = request.IsCurrentVersion,
                CreatedBy = (await accessProvider.GetUserAsync())?.Id,
                BitDepths = request.BitDepths.Select(x => new Link42ConfigurationBitDepth
                {
                    Id = Guid.NewGuid(),
                    ConfigurationId = configurationId,
                    DownloadLink = x.DownloadLink,
                    BitDepth = x.BitDepth,
                    LinkAppType = x.LinkAppType,
                    SystemType = x.SystemType,
                    MinSupportedMajorVersion = x.MinSupportedMajorVersion,
                    MinSupportedMinorVersion = x.MinSupportedMinorVersion
                }).ToList()
            };

            unitOfWork.Link42ConfigurationsRepository.Insert(newConfiguration);

            await unitOfWork.SaveAsync();

            return mapper.Map<Link42ConfigurationDto>(newConfiguration).ToOkManagerResult();
        }

        catch (Exception ex)
        {
            logger.Error(ex, "Не удалось добавить новую конфигурацию Link42");

            return PagedListExtensions.ToPreconditionFailedManagerResult<Link42ConfigurationDto>(ex.Message);
        }
    }
}
