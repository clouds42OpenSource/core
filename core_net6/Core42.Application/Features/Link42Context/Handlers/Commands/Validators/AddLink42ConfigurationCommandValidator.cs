﻿using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Commands;
using Core42.Application.Features.Link42Context.Helpers;
using FluentValidation;

namespace Core42.Application.Features.Link42Context.Handlers.Commands.Validators;

public class AddLink42ConfigurationCommandValidator : AbstractValidator<AddLink42ConfigurationCommand>
{
    private readonly IUnitOfWork _unitOfWork;
    public AddLink42ConfigurationCommandValidator(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;

        RuleFor(x => x.IsCurrentVersion)
            .Must(VersionIsGreaterThanExists)
            .WithMessage(x => $"Нельзя создать конфигурацию с версией {x.Version} по умолчанию. В системе обнаружена версия по умолчанию выше");

        const string notEmptyErrorMessage = "Конфигурация должна содержать хотя бы одну разрядность";

        RuleFor(x => x.BitDepths)
            .NotNull()
            .WithMessage(notEmptyErrorMessage)
            .NotEmpty()
            .WithMessage(notEmptyErrorMessage);
    }

    public bool VersionIsGreaterThanExists(AddLink42ConfigurationCommand command, bool isCurrentVersion)
    {
        if (!isCurrentVersion)
            return true;

        var existsConfiguration = _unitOfWork.Link42ConfigurationsRepository.AsQueryable()
            .FirstOrDefault(x => x.IsCurrentVersion);

        return existsConfiguration == null || VersionComparerHelper.Compare(existsConfiguration.Version, command.Version);
    }
}
