﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Handlers.Commands;

public class DeleteLink42ConfigurationCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger)
    : IRequestHandler<DeleteLink42ConfigurationCommand, ManagerResult<Guid>>
{
    public async Task<ManagerResult<Guid>> Handle(DeleteLink42ConfigurationCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var configuration = await unitOfWork.Link42ConfigurationsRepository
                .AsQueryable()
                .Include(x => x.BitDepths)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (configuration == null)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<Guid>(
                    $"Не найдена конфигурация Link42 для удаления по идентфикатору {request.Id}");
            }
            unitOfWork.Link42ConfigurationBitDepthRepository.Delete(configuration.BitDepths);
            unitOfWork.Link42ConfigurationsRepository.Delete(configuration);
            await unitOfWork.SaveAsync();

            return configuration.Id.ToOkManagerResult();
        }

        catch (Exception e)
        {
            logger.Error(e, $"Не удалось удалить конфигурацию Link42 по идентификатору {request.Id}");

            return PagedListExtensions.ToPreconditionFailedManagerResult<Guid>(e.Message);
        }

    }
}
