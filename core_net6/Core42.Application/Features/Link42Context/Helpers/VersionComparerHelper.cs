﻿namespace Core42.Application.Features.Link42Context.Helpers
{
    public static class VersionComparerHelper
    {
        public static bool Compare(string versionExists, string? versionNew)
        {
            if (string.IsNullOrEmpty(versionNew))
                return true;

            var existsVersion = Version.Parse(versionExists);
            var newVersion = Version.Parse(versionNew);

            var compareResult = existsVersion.CompareTo(newVersion);

            return compareResult < 1;
        }
    }
}
