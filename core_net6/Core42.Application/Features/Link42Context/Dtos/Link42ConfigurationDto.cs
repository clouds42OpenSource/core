﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.Link42Context.Dtos;

public class Link42ConfigurationDto : IMapFrom<Link42Configuration>
{
    public Guid Id { get; set; }
    public int DownloadsCount { get; set; }
    public int DownloadsLimit { get; set; }
    public string Version { get; set; }
    public bool IsCurrentVersion { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime? UpdatedOn { get; set; }
    public string? UpdatedBy { get; set; }
    public string CreatedBy { get; set; }
    public List<Link42ConfigurationBitDepthDto> BitDepths { get; set; }
    public bool AllowDownload
    {
        get
        {
            return DownloadsCount < DownloadsLimit;
        }
    }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<Link42Configuration, Link42ConfigurationDto>()
            .ForMember(x => x.UpdatedBy, z => z.MapFrom(y => y.UpdatedByAccountUser.Login))
            .ForMember(x => x.BitDepths, z => z.MapFrom(y => y.BitDepths.OrderBy(x => x.SystemType)))
            .ForMember(x => x.CreatedBy, z => z.MapFrom(y => y.CreatedByAccountUser.Login));
    }
}

public class Link42ConfigurationBitDepthDto : IMapFrom<Link42ConfigurationBitDepth>
{
    public int MinSupportedMajorVersion { get; set; }
    public int MinSupportedMinorVersion { get; set; }
    public SystemType SystemType { get; set; }
    public LinkAppType LinkAppType { get; set; }
    public BitDepth BitDepth { get; set; }
    public string DownloadLink { get; set; }
}
