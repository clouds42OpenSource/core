﻿namespace Core42.Application.Features.Link42Context.Dtos;

/// <summary>
/// Link42 info dto
/// </summary>
public class Link42InfoDto
{
    /// <summary>
    /// Used is current version
    /// </summary>
    public bool? isCurrentVersion { get; set; }

    /// <summary>
    /// Current user link42 version
    /// </summary>
    public string Version { get; set; }
}
