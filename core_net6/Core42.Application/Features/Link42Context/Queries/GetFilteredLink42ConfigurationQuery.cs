﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.Link42Context.Dtos;
using Core42.Application.Features.Link42Context.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Queries;

public class GetFilteredLink42ConfigurationQuery : IRequest<ManagerResult<PagedDto<Link42ConfigurationDto>>>, ISortedQuery, IPagedQuery, IHasFilter<Link42ConfigurationFilter>
{
    public string OrderBy { get; set; } = $"{nameof(Link42ConfigurationDto.CreatedOn)}.desc";
    public int? PageNumber { get; set; }
    public int? PageSize { get; set; }
    public Link42ConfigurationFilter Filter { get; set; }
}
