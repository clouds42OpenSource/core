﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.Link42Context.Queries.Filters;

public class Link42ConfigurationFilter : IQueryFilter
{
    public Guid? Id { get; set; }
    public string? Version { get; set; }
    public int? DownloadsCount { get; set; }
    public int? DownloadsLimit { get; set; }
    public bool? IsCurrentVersion { get; set; }
    public DateTime? CreatedOn { get; set; }
    public DateTime? UpdatedOn { get; set; }
    public string? UpdatedBy { get; set; }
    public string? CreatedBy { get; set; }
}
