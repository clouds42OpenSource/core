﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.Link42Context.Dtos;
using MediatR;

namespace Core42.Application.Features.Link42Context.Queries
{
    public class GetCurrentLinkVersionAndUsedByUserLoginQuery : IRequest<ManagerResult<Dictionary<string, Link42InfoDto?>>>
    {
        public IEnumerable<string> Logins { get; set; }
        public string? Link42Version { get; set; }
    }
}
