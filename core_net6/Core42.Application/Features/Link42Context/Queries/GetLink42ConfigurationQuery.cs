﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.Link42Context.Dtos;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.Link42Context.Queries
{
    public class GetLink42ConfigurationQuery : IRequest<ManagerResult<Link42ConfigurationDto>>, ISortedQuery
    {
        public string OrderBy { get; set; } = $"{nameof(Link42ConfigurationDto.Version)}.desc";
    }
}
