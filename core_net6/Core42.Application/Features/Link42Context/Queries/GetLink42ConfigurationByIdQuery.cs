﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.Link42Context.Dtos;
using MediatR;

namespace Core42.Application.Features.Link42Context.Queries;

public class GetLink42ConfigurationByIdQuery : IRequest<ManagerResult<Link42ConfigurationDto>>
{
    public Guid Id { get; set; }
}
