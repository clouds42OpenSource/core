﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.Link42Context.Commands;

public class DeleteLink42ConfigurationCommand : IRequest<ManagerResult<Guid>>
{
    public Guid Id { get; set; }
}
