﻿using Clouds42.Common.ManagersResults;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.Link42Context.Dtos;
using Core42.Application.Features.Link42Context.Helpers;
using FluentValidation;
using MediatR;

namespace Core42.Application.Features.Link42Context.Commands;

public class UpdateLink42ConfigurationCommand : IRequest<ManagerResult<Link42ConfigurationDto>>
{
    public string Version { get; set; }
    public Guid Id { get; set; }
    public int DownloadsLimit { get; set; }
    public bool IsCurrentVersion { get; set; }
    public List<Link42ConfigurationBitDepthDto>? BitDepths { get; set; }
}

public class UpdateLink42ConfigurationCommandValidator : AbstractValidator<UpdateLink42ConfigurationCommand>
{
    private readonly IUnitOfWork _unitOfWork;
    public UpdateLink42ConfigurationCommandValidator(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;

        RuleFor(x => x.Id)
            .NotEmpty()
            .WithMessage("Идентификатор конфигурации должен иметь значение")
            .Must(Link42ConfigurationExists)
            .WithMessage(x => $"Не найдена конфигурация с идентификатором {x.Id}");

        RuleFor(x => x.IsCurrentVersion)
            .Must(CurrentVersionMustBeGreaterThanExists)
            .WithMessage(x =>
                $"Нельзя обновить конфигурацию с версией {x.Version} по умолчанию. В системе обнаружена версия по умолчанию выше");

        const string notEmptyErrorMessage = "Конфигурация должна содержать хотя бы одну разрядность";

        RuleFor(x => x.BitDepths)
            .NotNull()
            .WithMessage(notEmptyErrorMessage)
            .NotEmpty()
            .WithMessage(notEmptyErrorMessage);
    }

    private bool Link42ConfigurationExists(UpdateLink42ConfigurationCommand command, Guid id)
    {
        return _unitOfWork.Link42ConfigurationsRepository.FirstOrDefault(x => x.Id == id) != null;
    }

    private bool CurrentVersionMustBeGreaterThanExists(UpdateLink42ConfigurationCommand command, bool isCurrentVersion)
    {
        if (!isCurrentVersion)
            return true;

        var exists = _unitOfWork.Link42ConfigurationsRepository.AsQueryable()
            .FirstOrDefault(x => x.IsCurrentVersion && x.Id != command.Id);

        return exists == null || VersionComparerHelper.Compare(exists.Version, command.Version);
    }
}
