﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.Link42Context.Commands;

public class IncreaseDownloadCounterCommand : IRequest<ManagerResult<Guid?>>
{
    public string Version { get; set; }
}
