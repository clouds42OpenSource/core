﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.Link42Context.Dtos;
using MediatR;

namespace Core42.Application.Features.Link42Context.Commands;

public class AddLink42ConfigurationCommand : IRequest<ManagerResult<Link42ConfigurationDto>>
{
    public bool IsCurrentVersion { get; set; }
    public string Version { get; set; }
    public int DownloadsLimit { get; set; }
    public List<Link42ConfigurationBitDepthDto> BitDepths { get; set; }
}
