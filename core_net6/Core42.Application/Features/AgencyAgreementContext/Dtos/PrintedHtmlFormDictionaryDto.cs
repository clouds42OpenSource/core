﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AgencyAgreementContext.Dtos
{
    /// <summary>
    /// Printed html form as key-value dto
    /// </summary>
    public class PrintedHtmlFormDictionaryDto : DictionaryDto<Guid, string>, IMapFrom<PrintedHtmlForm>
    {
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<PrintedHtmlForm, PrintedHtmlFormDictionaryDto>()
                .ForMember(x => x.Key, m => m.MapFrom(z => z.Id))
                .ForMember(x => x.Value, m => m.MapFrom(z => z.Name));
        }
    }
}
