﻿using AutoMapper;
using Clouds42.Domain.DataModels.billing;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AgencyAgreementContext.Dtos
{
    /// <summary>
    /// Agency agreement dto
    /// </summary>
    public class AgencyAgreementDto : IMapFrom<AgencyAgreement>
    {
        /// <summary>
        /// Agency agreement identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Agency agreement name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Printed html form identifier
        /// </summary>
        public Guid PrintedHtmlFormId { get; set; }

        /// <summary>
        /// Effective date
        /// </summary>
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Rent 1C reward percent
        /// </summary>
        public decimal Rent1CRewardPercent { get; set; }

        /// <summary>
        /// My disk service reward percent
        /// </summary>
        public decimal MyDiskRewardPercent { get; set; }

        /// <summary>
        /// Service owner reward percent
        /// </summary>
        public decimal ServiceOwnerRewardPercent { get; set; }

        /// <summary>
        /// Rent 1C reward percent for VIP accounts
        /// </summary>
        public decimal Rent1CRewardPercentForVipAccounts { get; set; }

        /// <summary>
        ///  My disk reward percent for VIP accounts
        /// </summary>
        public decimal MyDiskRewardPercentForVipAccounts { get; set; }

        /// <summary>
        /// Service owner reward percent  for VIP accounts
        /// </summary>
        public decimal ServiceOwnerRewardPercentForVipAccounts { get; set; }


        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AgencyAgreement, AgencyAgreementDto>();
        }
    }
}
