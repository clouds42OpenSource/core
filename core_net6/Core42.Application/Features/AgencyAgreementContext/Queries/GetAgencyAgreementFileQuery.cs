﻿using Clouds42.Common.ManagersResults;
using MediatR;
using Core42.Application.Features.PrintedHtmlFormContext.Dtos;

namespace Core42.Application.Features.AgencyAgreementContext.Queries
{
    /// <summary>
    /// Get agency agreement file query
    /// </summary>
    public class GetAgencyAgreementFileQuery(Guid id) : IRequest<ManagerResult<CloudFileDto>>
    {
        /// <summary>
        /// Agency agreement id
        /// </summary>
        public Guid Id { get; set; } = id;
    }
}
