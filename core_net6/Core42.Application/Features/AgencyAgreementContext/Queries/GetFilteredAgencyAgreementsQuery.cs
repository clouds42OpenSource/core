﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels.billing;
using Core42.Application.Features.AgencyAgreementContext.Dtos;
using Core42.Application.Features.AgencyAgreementContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AgencyAgreementContext.Queries
{
    /// <summary>
    /// Get filtered agency agreement query
    /// </summary>
    public class GetFilteredAgencyAgreementsQuery : IRequest<ManagerResult<PagedDto<AgencyAgreementDto>>>, ISortedQuery, IPagedQuery, IHasFilter<AgencyAgreementFilter>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(AgencyAgreement.Name)}.asc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Agency agreement filter
        /// </summary>
        public AgencyAgreementFilter? Filter { get; set; }
    }
}
