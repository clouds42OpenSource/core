﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AgencyAgreementContext.Dtos;
using MediatR;

namespace Core42.Application.Features.AgencyAgreementContext.Queries
{
    /// <summary>
    /// Get agency agreement printed html forms as key-value query
    /// </summary>
    public class GetAgencyAgreementPrintedHtmlFormsAsKeyValueQuery : IRequest<ManagerResult<List<PrintedHtmlFormDictionaryDto>>>
    {
    }
}
