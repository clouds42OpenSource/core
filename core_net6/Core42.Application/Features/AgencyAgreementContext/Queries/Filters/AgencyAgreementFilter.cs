﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AgencyAgreementContext.Queries.Filters
{
    /// <summary>
    /// Agency agreement query filter
    /// </summary>
    public class AgencyAgreementFilter : IQueryFilter
    {
        /// <summary>
        /// Agency agreement name
        /// </summary>
        public string? Name { get; set; }
    }
}
