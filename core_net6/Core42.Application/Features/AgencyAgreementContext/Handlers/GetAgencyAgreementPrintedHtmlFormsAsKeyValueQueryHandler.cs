﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AgencyAgreementContext.Dtos;
using Core42.Application.Features.AgencyAgreementContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AgencyAgreementContext.Handlers
{
    /// <summary>
    /// Get agency agreement printed html forms as key-value query handler
    /// </summary>
    public class GetAgencyAgreementPrintedHtmlFormsAsKeyValueQueryHandler(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetAgencyAgreementPrintedHtmlFormsAsKeyValueQuery,
            ManagerResult<List<PrintedHtmlFormDictionaryDto>>>
    {
        /// <summary>
        /// Handle get agency agreement printed html forms as key-value query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<PrintedHtmlFormDictionaryDto>>> Handle(GetAgencyAgreementPrintedHtmlFormsAsKeyValueQuery request, CancellationToken cancellationToken)
        {

            try
            {
                accessProvider.HasAccess(ObjectAction.GetPrintedHtmlForm);

                var agencyPrintedTypeName = typeof(PrintedAgencyAgreementDto).FullName;

                return (await unitOfWork.PrintedHtmlFormRepository
                    .AsQueryable()
                    .Where(x => string.IsNullOrEmpty(agencyPrintedTypeName) || x.ModelType == agencyPrintedTypeName)
                    .ProjectTo<PrintedHtmlFormDictionaryDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();

            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка печатных форм]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<PrintedHtmlFormDictionaryDto>>(ex.Message);
            }
        }
    }
}
