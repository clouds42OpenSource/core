﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AgencyAgreementContext.Dtos;
using Core42.Application.Features.AgencyAgreementContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AgencyAgreementContext.Handlers
{
    /// <summary>
    /// Get filtered, sorted and paginated agency agreements query handler
    /// </summary>
    public class GetFilteredAgencyAgreementsQueryHandler(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredAgencyAgreementsQuery, ManagerResult<PagedDto<AgencyAgreementDto>>>
    {
        /// <summary>
        /// Handle get filtered, sorted and paginated agency agreements query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<AgencyAgreementDto>>> Handle(GetFilteredAgencyAgreementsQuery request, CancellationToken cancellationToken)
        {

            try
            {
                accessProvider.HasAccess(ObjectAction.AgencyAgreement_GetAgencyAgreementsData);

                return (await unitOfWork.AgencyAgreementRepository
                    .AsQueryable()
                    .AutoFilter(request.Filter)
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<AgencyAgreementDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Получение данных агентских соглашений завершилось ошибкой.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AgencyAgreementDto>>(ex.Message);
            }
        }
    }
}
