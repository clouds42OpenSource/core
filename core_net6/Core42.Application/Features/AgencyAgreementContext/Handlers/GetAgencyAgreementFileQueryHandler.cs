﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.AgencyAgreement.Contracts.DocumentBuilder.Enums;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AgencyAgreementContext.Queries;
using Core42.Application.Features.PrintedHtmlFormContext.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace Core42.Application.Features.AgencyAgreementContext.Handlers
{
    /// <summary>
    /// Get agency agreement file query handler
    /// </summary>
    public class GetAgencyAgreementFileQueryHandler(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IHtmlToPdfProvider htmlToPdfProvider,
        ILogger42 logger)
        : IRequestHandler<GetAgencyAgreementFileQuery, ManagerResult<CloudFileDto>>
    {
        /// <summary>
        /// Handle get agency agreement file query handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<CloudFileDto>> Handle(GetAgencyAgreementFileQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AgencyAgreement_PrintAgencyAgreement);

                var account = await unitOfWork.AccountsRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.AccountConfiguration)
                    .ThenInclude(x => x.Supplier)
                    .FirstOrDefaultAsync(x => x.Id == accessProvider.ContextAccountId, cancellationToken);

                var printedAgencyAgreementDto = new PrintedAgencyAgreementDto
                {
                    ActualAgencyAgreementUrl = new Uri($"{Clouds42.Configurations.Configurations.CloudConfigurationProvider.Cp.GetSiteCoreUrl()}/partners/open-actual-agency-agreement").AbsoluteUri,
                    SupplierReferenceName = account?.AccountConfiguration?.Supplier?.Name
                };

                var printedHtmlForm = await unitOfWork.AgencyAgreementRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.PrintedHtmlForm)
                    .ThenInclude(x => x.Files)
                    .ProjectTo<PrintedHtmlFormDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(w => w.AgencyAgreementId == request.Id, cancellationToken);

                if (printedHtmlForm != null)
                {
                    foreach (var file in printedHtmlForm.Files)
                    {
                        file.Base64 = Convert.ToBase64String(file.Bytes);

                    }

                    const string title = "Агентское соглашение";
                    const PdfPageSizeEnum pdfPageSize = PdfPageSizeEnum.A4;

                    try
                    {
                        var razorData = printedHtmlForm.Files.Aggregate(printedHtmlForm.HtmlData, (current, attachment) => current.Replace($"@@{System.Web.HttpUtility.UrlEncode(attachment.FileName)}@@", $"data:{attachment.ContentType};base64,{attachment.Base64}"));
                        
                        var config = new TemplateServiceConfiguration
                        {
                            Debug = false,
                            DisableTempFileLocking = true,
                            CachingProvider = new DefaultCachingProvider(DeleteFileWithRetries)
                        };
                        Engine.Razor = RazorEngineService.Create(config);

                        var result = Engine.Razor.RunCompile(razorData, Guid.NewGuid().ToString(), printedAgencyAgreementDto.GetType(), printedAgencyAgreementDto);

                        return new CloudFileDto
                        {
                            Bytes = htmlToPdfProvider.GeneratePdf(result, title, pdfPageSize, null),
                            FileName = $"{title}.pdf"
                        }.ToOkManagerResult();
                    }

                    catch (Exception)
                    {
                        if (printedHtmlForm.HtmlData.Contains("@functions"))
                        {
                            printedHtmlForm.HtmlData = printedHtmlForm.HtmlData.Replace("functions ", "");
                            printedHtmlForm.HtmlData = printedHtmlForm.HtmlData.Replace("private", "");
                        }

                        var razorData = printedHtmlForm.Files.Aggregate(printedHtmlForm.HtmlData, (current, attachment) => current.Replace($"@@{System.Web.HttpUtility.UrlEncode(attachment.FileName)}@@", $"data:{attachment.ContentType};base64,{attachment.Base64}"));

                        var result = Engine.Razor.RunCompile(razorData, Guid.NewGuid().ToString(), printedAgencyAgreementDto.GetType(), printedAgencyAgreementDto);

                        return new CloudFileDto
                        {
                            Bytes = htmlToPdfProvider.GeneratePdf(result, title, pdfPageSize, null),
                            FileName = $"{title}.pdf"
                        }.ToOkManagerResult();
                    }
                }

                throw new NotFoundException($"Не найдено соглашение по идентификатору {request.Id}");

            }

            catch (Exception ex)
            {
                var message = $"Печать агентского соглашения '{request.Id}'";
                handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<CloudFileDto>(ex.Message);
            }
        }
        private void DeleteFileWithRetries(string filePath)
        {
            const int maxRetries = 3;
            const int delayBetweenRetries = 1000;

            for (int retry = 0; retry < maxRetries; retry++)
            {
                try
                {
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }
                    break; // Exit if file is deleted successfully
                }
                catch (UnauthorizedAccessException ex)
                {
                    logger.Error(ex, $"Access denied to file {filePath}: {ex.Message}");
                }
                catch (IOException ex)
                {
                    logger.Error(ex, $"IO Exception when deleting file {filePath}: {ex.Message}");
                }

                Task.Delay(delayBetweenRetries).Wait();
            }
        }
    }
}
