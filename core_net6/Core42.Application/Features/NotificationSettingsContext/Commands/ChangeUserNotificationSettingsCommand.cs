﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NotificationSettingsContext.Commands
{
    public class ChangeUserNotificationSettingsCommand : IRequest<ManagerResult<List<AccountUserNotificationSettingsDto>>>
    {
        public Guid AccountUserId { get; set; }
        public List<AccountUserNotificationSettingsDto> NotificationSettings { get; set; }
    }

    public class ChangeUserNotificationSettingsCommandHandler(
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        IAccessProvider accessProvider)
        : IRequestHandler<ChangeUserNotificationSettingsCommand,
            ManagerResult<List<AccountUserNotificationSettingsDto>>>
    {
        public async Task<ManagerResult<List<AccountUserNotificationSettingsDto>>> Handle(ChangeUserNotificationSettingsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var settingsForEdit = request.NotificationSettings.Where(x => x.Id.HasValue).ToList();
                var whatsAppSetting =
                    settingsForEdit.FirstOrDefault(s =>
                        s.Notification.Type == NotificationType.WhatsApp);
                CreateWhatsAppCodeIfNotExist(whatsAppSetting, request.AccountUserId);  
                var settingsForEditIds = settingsForEdit.Select(x => x.Id).ToList();
                var settingFromDb = await unitOfWork.AccountUserNotificationSettingsRepository
                    .AsQueryable()
                    .Include(x => x.AccountUser)
                    .Include(x => x.NotificationSettings)
                    .Where(x => x.AccountUserId == request.AccountUserId && settingsForEditIds.Contains(x.Id))
                    .ToListAsync(cancellationToken);

                settingFromDb
                    .GroupJoin(settingsForEdit, x => x.Id, z => z.Id, (exists, forEdit) => (exists, forEdit))
                    .ToList()
                    .ForEach(x =>
                    {
                        var forEdit = x.forEdit.FirstOrDefault();

                        if (forEdit == null)
                        {
                            return;
                        }

                        x.exists.IsActive = forEdit.IsActive;
                        
                        LogEventHelper.LogEvent(unitOfWork, accessProvider.ContextAccountId, accessProvider, LogActions.EditUserCard, $"Пользователь {x.exists.AccountUser.Email} {x.exists.AccountUser.Login} {(forEdit.IsActive ? "включил" : "отключил")} уведомления по {x.exists.NotificationSettings.TypeText}");

                        unitOfWork.AccountUserNotificationSettingsRepository.Update(x.exists);
                    });

                var settingsForCreate = request.NotificationSettings.Except(settingsForEdit).ToList();
                var notificationSettings = settingsForCreate.Select(x => x.Notification).ToList();
                var notificationSettingsTypes = notificationSettings.Select(x => x.Type).ToList();

                var databaseNotificationSettings = await unitOfWork.NotificationSettingsRepository
                    .AsQueryableNoTracking()
                    .Where(x => notificationSettingsTypes.Contains(x.Type) && x.IsActive)
                    .ToListAsync(cancellationToken);

                notificationSettings
                    .GroupJoin(databaseNotificationSettings, z => z.Type, y => y.Type,
                        (forCreate, exists) => (forCreate, exists))
                    .ToList()
                    .ForEach(x =>
                    {
                        var exists = x.exists.FirstOrDefault();
                        if (exists == null)
                            return;

                        x.forCreate.Id = exists.Id;
                    });

                var notificationSettingsEntities = settingsForCreate
                    .Where(x => x.Notification.Id.HasValue)
                    .Select(x => new AccountUserNotificationSettings
                    {
                        AccountUserId = request.AccountUserId,
                        IsActive = x.IsActive,
                        NotificationSettingsId = x.Notification.Id!.Value
                    }).ToList();
                unitOfWork.AccountUserNotificationSettingsRepository
                    .InsertRange(notificationSettingsEntities);

                await unitOfWork.SaveAsync();

                request.NotificationSettings
                    .GroupJoin(notificationSettingsEntities, z => z.Notification.Id, y => y.NotificationSettingsId,
                        (requestNotification, dbNotification) => (requestNotification, dbNotification))
                    .ToList()
                    .ForEach(x =>
                    {
                        var dbNotification = x.dbNotification.FirstOrDefault();
                        if (dbNotification == null)
                            return;

                        var dbNotificationSettings = databaseNotificationSettings
                            .FirstOrDefault(z => z.Id == dbNotification.NotificationSettingsId);

                        x.requestNotification.Id = dbNotification.Id;
                        x.requestNotification.IsActive = dbNotificationSettings?.IsActive ?? false;
                        x.requestNotification.Notification.TypeText = dbNotificationSettings?.TypeText!;
                    });

                return request.NotificationSettings.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка редактирования настроек уведомлений пользователя");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<AccountUserNotificationSettingsDto>>("Произошла ошибка при редактировании настроек уведомлений");
            }
        }

        /// <summary>
        /// Генерирует код уведомления для ватсаппа.
        /// </summary>
        /// <param name="whatsAppSetting">Настройки уведомлений ватсаппа, связанные с аккаунтом.</param>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        private static void CreateWhatsAppCodeIfNotExist(AccountUserNotificationSettingsDto? whatsAppSetting, Guid accountId)
        {
            if (whatsAppSetting == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(whatsAppSetting.Code))
            {
                whatsAppSetting.Code = "/start" + $" {accountId.ToString()}";
            }
        }
        
    }
}
