﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Primitives;

namespace Core42.Application.Features.NotificationContext.SignalR.Hubs
{
    [Authorize]
    public class NotificationHub(IAccessProvider accessProvider, IUnitOfWork unitOfWork, ILogger42 logger)
        : Hub<INotificationClient>
    {
        public override async Task OnConnectedAsync()
        {
            try
            {
                var user = await accessProvider.GetUserAsync();

                if(user != null)
                {
                    await base.OnConnectedAsync();

                    unitOfWork.ConnectionRepository.Insert(new Connection
                    {
                        UserAgent = Context.GetHttpContext()?.Request?.Headers?.TryGetValue("User-Agent", out StringValues value) ?? false ? value.First() : null,
                        AccountUserId = user.Id,
                        Connected = true,
                        ConnectionId = Context.ConnectionId
                    });

                    await unitOfWork.SaveAsync();
                }
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка при попытке подключиться к центру уведомлений");
            }
            
        }

        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            try
            {
                var connection = await unitOfWork.ConnectionRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.ConnectionId == Context.ConnectionId);

                if (connection != null)
                {
                    connection.Connected = false;
                    unitOfWork.ConnectionRepository.Update(connection);
                    await unitOfWork.SaveAsync();
                }

                await base.OnDisconnectedAsync(exception);
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка при попытке отключиться от центра уведомлений");
            }
            
        }
    }

    public interface INotificationClient
    {
        Task Broadcast(NotificationDto notification);
    }
}
