﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using Clouds42.TelegramBot.Interfaces;
using Core42.Application.Features.NotificationContext.Commands;
using MediatR;

namespace Core42.Application.Features.NotificationContext.Handlers;

public class SendNotificationViaTelegramBotCommandHandler : IRequestHandler<SendNotificationViaTelegramBotCommand>
{
    private readonly ITelegramBot42 _telegramBot42;
    private readonly IUnitOfWork _unitOfWork;
    private readonly ILogger42 _logger;

    public SendNotificationViaTelegramBotCommandHandler(IEnumerable<ITelegramBot42> telegramBots, IUnitOfWork unitOfWork, ILogger42 logger)
    {
        _telegramBot42 = telegramBots.FirstOrDefault(x => x.Type == BotType.Notification)!;
        _unitOfWork = unitOfWork;
        _logger = logger;
    }

    public async Task Handle(SendNotificationViaTelegramBotCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var notificationSettings = await _unitOfWork.NotificationSettingsRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Type == NotificationType.Telegram, cancellationToken);

            if (notificationSettings is { IsActive: false })
            {
                _logger.Warn("Отправка рассылок через TELEGRAM отключена на глобальном уровне. Проверьте настройки в таблице dbo.NotificationSettings");

                return;
            }

            _logger.Info($"Поступила команда на рассылку в TELEGRAM - {request.TelegramUsers.Count} пользователям. Начинаем выполнять.");

            foreach (var userNotificationSetting in request.TelegramUsers)
            {
                await _telegramBot42.SendNotification(userNotificationSetting.ChatId, request.Message);
            }

        }

        catch (Exception ex)
        {
            _logger.Error(ex, "Ошибка при отправке рассылки в TELEGRAM пользователям");
        }
    }
}
