﻿using AutoMapper;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using Core42.Application.Features.NotificationContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NotificationContext.Handlers;

public class CreateNotificationCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
    : IRequestHandler<CreateNotificationCommand, ManagerResult<List<NotificationDto>>>
{
    public async Task<ManagerResult<List<NotificationDto>>> Handle(CreateNotificationCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var message = request.Message?
                .Replace(@"\\", "")
                .Replace("\\", "")
                .Replace("\\\\", "")
                .Replace("*", "")
                .Replace("[", "")
                .Replace("]"," ")
                .Replace("(", " ")
                .Replace(")", "")
                .Replace("~", "")
                .Replace("`", "")
                .Replace("<", "")
                .Replace(">", "");

            if (string.IsNullOrEmpty(message))
            {
                return null;
            }

            var notifications = request.UserIds.Select(x => new Notification
            {
                Id = Guid.NewGuid(),
                AccountUserId = x,
                CreatedOn = DateTime.Now,
                Message = message,
                State = request.State
            }).ToList();
           
            await unitOfWork.BulkInsertAsync(notifications);

            var notificationsDto = notifications
                .Select(mapper.Map<NotificationDto>)
                .ToList();

            return notificationsDto.ToOkManagerResult();
        }

        catch (Exception e)
        {
            logger.Error(e, "Не удалось отправить уведомление в центр уведомлений");

            return PagedListExtensions.ToPreconditionFailedManagerResult<List<NotificationDto>>("Не удалось создать и отправить уведомление");
        }
    }
}
