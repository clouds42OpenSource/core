﻿using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.SmsClient;
using Core42.Application.Contracts.Features.NotificationContext.Commands;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using Core42.Application.Features.AccountUsersContext.Specifications;
using Core42.Application.Features.EmailContext.Commands;
using Core42.Application.Features.NotificationContext.Commands;
using Core42.Application.Features.ResourcesContext.Specifications;
using Core42.Application.Features.TalkMeContext;
using Core42.Application.Features.TalkMeContext.Dtos;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NotificationContext.Handlers;

public class GlobalSendNotificationsCommandHandler(
    ISender sender,
    ILogger42 logger,
    IUnitOfWork unitOfWork,
    ISmsClient smsClient)
    : IRequestHandler<GlobalSendNotificationsCommand, ManagerResult<List<NotificationDto>>>
{
    public async Task<ManagerResult<List<NotificationDto>>> Handle(GlobalSendNotificationsCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var usersIds = request.UserIds;

            switch (request.NotificationSendType)
            {
                case NotificationSendType.ByUsersIdsList:
                    break;
                case NotificationSendType.OnlyAccountAdminsOrManagers:
                    usersIds = await unitOfWork.AccountUsersRepository
                        .AsQueryableNoTracking()
                        .Where(AccountUserSpecification.ByUserGroups(new List<AccountUserGroup>{ AccountUserGroup.AccountAdmin }))
                        .Select(x => x.Id)
                        .ToListAsync(cancellationToken);
                    break;
                case NotificationSendType.OnlyActiveUsers:
                    usersIds = await unitOfWork.AccountUsersRepository
                        .AsQueryableNoTracking()
                        .Where(AccountUserSpecification.ByIsActiveAndNotDeleted())
                        .Select(x => x.Id)
                        .ToListAsync(cancellationToken);
                    break;
                case NotificationSendType.UsersWithConnectedService:
                    usersIds = await unitOfWork.ResourceRepository
                        .AsQueryableNoTracking()
                        .Where(ResourceSpecification.ByActiveRent())
                        .Select(x => x.Subject!.Value)
                        .Distinct()
                        .ToListAsync(cancellationToken);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (!usersIds.Any())
                return PagedListExtensions.ToPreconditionFailedManagerResult<List<NotificationDto>>("Не удалось найти пользователей для отправки уведомлений");

            var notifications = await sender.Send(new CreateNotificationCommand
            {
                Message = request.Message,
                State = request.State,
                UserIds = usersIds
            }, cancellationToken);

            if (request.State == NotificationState.Information)
                return notifications;

            var usersNotificationsSettings = await unitOfWork.AccountUserNotificationSettingsRepository
                .AsQueryableNoTracking()
                .Include(x => x.NotificationSettings)
                .Where(x => usersIds.Contains(x.AccountUserId) && x.IsActive && x.NotificationSettings.IsActive)
                .ToListAsync(cancellationToken);

            logger.Info($"Всего пользователей - {usersIds.Count}. Найдено конфигураций рассылок - {usersNotificationsSettings.Count}");

            var userIds = usersNotificationsSettings.Select(x => x.AccountUserId);

            var accountUsers = await unitOfWork.AccountUsersRepository
                .AsQueryableNoTracking()
                .Where(x => userIds.Contains(x.Id))
                .ToListAsync(cancellationToken);

            await SendEmailNotificationsAsync(usersNotificationsSettings, accountUsers,  request,  cancellationToken);
            await SendTelegramBotNotificationsAsync(usersNotificationsSettings, request, cancellationToken);
            await SendSmsNotificationsAsync(usersNotificationsSettings, accountUsers, request);
            await SendWhatsAppNotificationsAsync(usersNotificationsSettings, accountUsers, request, cancellationToken);

            return notifications;
        }

        catch (Exception e)
        {
            logger.Error(e, "Ошибка глобальной отправки уведомлений");

            return PagedListExtensions.ToPreconditionFailedManagerResult<List<NotificationDto>>("Не удалось выполнить отправку уведомлений");
        }
    }

    private async Task SendEmailNotificationsAsync(
        IEnumerable<AccountUserNotificationSettings> usersNotificationsSettings,
        IEnumerable<AccountUser> accountUsers,
        GlobalSendNotificationsCommand request,
        CancellationToken cancellationToken)
    {
        if (request.IgnoreMailSending)
        {
            logger.Warn($"Для расссылки по EMAIL передан флаг {nameof(request.IgnoreMailSending)} = {request.IgnoreMailSending}. Рассылка произведена не будет");

            return;
        }

        var userEmailNotifications = usersNotificationsSettings
            .Where(x => x.NotificationSettings.Type == NotificationType.Email)
            .ToList();

        if (!userEmailNotifications.Any())
        {
            logger.Warn("Для расссылки по EMAIL не найдено пользователей. Рассылка произведена не будет");

            return;
        }

        var userEmailNotificationsIds = userEmailNotifications
            .Select(x => x.AccountUserId)
            .ToList();

        var emailAddressesDto = accountUsers
                .Where(x => userEmailNotificationsIds.Contains(x.Id) && (!x.Unsubscribed.HasValue || !x.Unsubscribed.Value))
                .Distinct()
                .Select(x => new EmailAddressInfoDto(x.Email, $"{x.FirstName} {x.LastName}", x.Id))
                .ToList();

        var accountUsersCount = emailAddressesDto.Count;

        var sendEmailCommand = new SendEmailCommand
        {
            Tos = emailAddressesDto,
            Body = request.Message.RemoveSlashes(),
            Subject = request.Subject!,
            Header = request.Header!,
            Categories = ["notification"],
            SendGridTemplateId = CloudConfigurationProvider.SendGrid.GetDefaultTemplateId()
        };

        if (accountUsersCount > 1000)
        {
            const int batchSize = 1000;
            var numberOfBatches = (int)Math.Ceiling((double)accountUsersCount / batchSize);

            for (var i = 0; i < numberOfBatches; i++)
            {
                var batchAccountUsers = emailAddressesDto.Skip(i * batchSize).Take(batchSize).ToList();
                sendEmailCommand.Tos = batchAccountUsers;

                await sender.Send(sendEmailCommand, cancellationToken);
            }
        }

        else
        {
            await sender.Send(sendEmailCommand, cancellationToken);
        }
    }

    private async Task SendTelegramBotNotificationsAsync(
        IEnumerable<AccountUserNotificationSettings> usersNotificationsSettings,
        GlobalSendNotificationsCommand request,
        CancellationToken cancellationToken)
    {
        var usersTelegramBotNotifications = usersNotificationsSettings
            .Where(x => x.NotificationSettings.Type == NotificationType.Telegram && x is { ChatId: not null })
            .Select(x => new TelegramUserDto
            {
                ChatId = x.ChatId!.Value,
                UserId = x.AccountUserId
            })
            .ToList();

        if (!usersTelegramBotNotifications.Any())
        {
            logger.Warn("Для расссылки по TELEGRAM не найдено пользователей. Рассылка произведена не будет");

            return;
        }
        string[] markdownCharsToEscape = ["_", "*", "[", "]", "(", ")", "~", ">", "#", "+", "-", "=", "|", "{", "}", ".", "!"
        ];

        await sender.Send(new SendNotificationViaTelegramBotCommand
        {
            TelegramUsers = usersTelegramBotNotifications,
            Message = request.Message.Escape("\\", markdownCharsToEscape)
        }, cancellationToken);
    }

    private async Task SendSmsNotificationsAsync(
        IEnumerable<AccountUserNotificationSettings> usersNotificationsSettings,
        IEnumerable<AccountUser> accountUsers,
        GlobalSendNotificationsCommand request)
    {
        var accountUsersSmsNotificationsIds = usersNotificationsSettings
            .Where(x => x.NotificationSettings.Type == NotificationType.Sms && x.IsActive)
            .Select(x => x.AccountUserId)
            .ToList();

        var usersPhones = string.Join(";", accountUsers
            .Where(x => accountUsersSmsNotificationsIds.Contains(x.Id) &&  x.IsPhoneVerified.HasValue && x.IsPhoneVerified.Value)
            .Select(x => x.PhoneNumber)
            .ToList());

        if (!usersPhones.Any())
        {
            logger.Warn("Для расссылки по СМС не найдено пользователей. Рассылка произведена не будет");

            return;
        }

        await smsClient.SendSms(usersPhones, request.Message.RemoveSlashes());
    }

    private async Task SendWhatsAppNotificationsAsync(
        IEnumerable<AccountUserNotificationSettings> usersNotificationsSettings,
        IEnumerable<AccountUser> accountUsers,
        GlobalSendNotificationsCommand request,
        CancellationToken cancellationToken)
    {
        var accountUsersIds = usersNotificationsSettings
            .Where(x => x.NotificationSettings.Type == NotificationType.WhatsApp)
            .Select(x => x.AccountUserId)
            .ToList();

        var users = accountUsers
            .Where(x => x.IsPhoneVerified.HasValue && x.IsPhoneVerified.Value && accountUsersIds.Contains(x.Id))
            .Select(x => x.PhoneNumber)
            .ToList();

        if (!users.Any())
        {
            logger.Warn("Для рассылки по WHATSAPP не найдено пользователей. Рассылка произведена не будет.");

            return;
        }

        await sender.Send(new SendWhatsAppNotificationsCommand
        {
            Notifications = users
                .Select(x => new WhatsAppNotificationDto
                {
                    Message = request.Message.RemoveSlashes(),
                    Phone = x
                })
                .ToList()
        }, cancellationToken);
}
}
