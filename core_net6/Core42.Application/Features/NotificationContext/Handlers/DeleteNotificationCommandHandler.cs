﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.NotificationContext.Commands;
using Core42.Application.Features.NotificationContext.Specifications;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NotificationContext.Handlers;

public class DeleteNotificationCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger, IAccessProvider accessProvider)
    : IRequestHandler<DeleteNotificationCommand, ManagerResult<string>>
{
    public async Task<ManagerResult<string>> Handle(DeleteNotificationCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var currentUser = await accessProvider.GetUserAsync();

            await unitOfWork.NotificationRepository
                .AsQueryableNoTracking()
                .Where(NotificationSpecification.ByAccountUserIdOrMessagesIds(request.AccountUserId, request.MessageIds, currentUser.Id))
                .ExecuteUpdateAsync(x => x.SetProperty(z => z.IsDeleted, true), cancellationToken);
        
            return "Успешно удалено".ToOkManagerResult();
        }
        catch (Exception e)
        {
            logger.Error(e.Message, e);

            return PagedListExtensions.ToPreconditionFailedManagerResult<string>($"Не удалось удалить уведомление");
        }
    }
}
