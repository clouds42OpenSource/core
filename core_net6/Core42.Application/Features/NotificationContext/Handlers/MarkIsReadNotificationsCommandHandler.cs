﻿using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using Core42.Application.Features.NotificationContext.Commands;
using Core42.Application.Features.NotificationContext.Specifications;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NotificationContext.Handlers;

public class MarkIsReadNotificationsCommandHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<MarkIsReadNotificationsCommand, ManagerResult<List<NotificationDto>>>
{
    public async Task<ManagerResult<List<NotificationDto>>> Handle(MarkIsReadNotificationsCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await accessProvider.GetUserAsync();

            var spec = request.AccountUserId.HasValue
                ? NotificationSpecification.ByAccountUserIdOrNull(request.AccountUserId.Value, user.Id)
                : NotificationSpecification.ByMessageIds(request.MessageIds) &&
                  NotificationSpecification.ByAccountUserId(user.IsService, user.Id);

            var notifications = await unitOfWork.NotificationRepository
                .AsQueryableNoTracking()
                .Where(spec)
                .ToListAsync(cancellationToken);

            foreach (var notification in notifications)
            {
                notification.IsRead = true;
            }

            await unitOfWork.BulkUpdateAsync(notifications);

            return notifications
                .Select(mapper.Map<NotificationDto>)
                .ToList()
                .ToOkManagerResult();
        }

        catch (Exception e)
        {
            logger.Error(e.Message, e);

            return PagedListExtensions.ToPreconditionFailedManagerResult<List<NotificationDto>>(
                $"Не удалось отметить как 'Прочитано'");
        }
    }
}
