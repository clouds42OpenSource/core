﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using Core42.Application.Features.NotificationContext.Queries;
using Core42.Application.Features.NotificationContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NotificationContext.Handlers;

public class GetFilteredNotificationsQueryHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
    : IRequestHandler<GetFilteredNotificationsQuery, ManagerResult<PagedDto<NotificationDto>>>
{
    public async Task<ManagerResult<PagedDto<NotificationDto>>> Handle(GetFilteredNotificationsQuery request, CancellationToken cancellationToken)
    {
        try
        {
            return (await unitOfWork.NotificationRepository
                    .AsQueryableNoTracking()
                    .Where(x => !x.IsDeleted)
                    .ProjectTo<NotificationDto>(mapper.ConfigurationProvider)
                    .Where(NotificationSpecification.ByUserLogin(request?.Filter?.Login))
                    .AutoFilter(request?.Filter)
                    .AutoSort(request)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        catch (Exception e)
        {
            var message = $"Не удалось получить список уведомлений пользователя {request?.Filter?.AccountUserId}";
            logger.Error(message, e);

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<NotificationDto>>(message);
        }
    }
}
