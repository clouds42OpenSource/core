﻿using MediatR;

namespace Core42.Application.Features.NotificationContext.Commands
{
    public class SendNotificationViaTelegramBotCommand: IRequest
    {
        public List<TelegramUserDto> TelegramUsers { get; set; }
        public string Message { get; set; }
    }

    public class TelegramUserDto
    {
        public Guid UserId { get; set; }
        public long ChatId { get; set; }
    }
}
