﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.NotificationContext.Commands;

public class DeleteNotificationCommand(List<Guid> messageIds, Guid? accountUserId) : IRequest<ManagerResult<string>>
{
    public List<Guid> MessageIds { get; set; } = messageIds;

    public Guid? AccountUserId { get; set; } = accountUserId;
}
