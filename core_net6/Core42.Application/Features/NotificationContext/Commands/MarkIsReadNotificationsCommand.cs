﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using MediatR;

namespace Core42.Application.Features.NotificationContext.Commands
{
    public class MarkIsReadNotificationsCommand : IRequest<ManagerResult<List<NotificationDto>>>
    {
        public List<Guid> MessageIds { get; set; }

        public Guid? AccountUserId { get; set; }
    }
}
