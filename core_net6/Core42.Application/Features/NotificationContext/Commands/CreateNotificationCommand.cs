﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.Notification;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using MediatR;

namespace Core42.Application.Features.NotificationContext.Commands
{
    public class CreateNotificationCommand : IRequest<ManagerResult<List<NotificationDto>>>
    {
        public string Message { get; set; }
        public List<Guid> UserIds { get; set; }
        public NotificationState State { get; set; } = NotificationState.Normal;
    }
}
