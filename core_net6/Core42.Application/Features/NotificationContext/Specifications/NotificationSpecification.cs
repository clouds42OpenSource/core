﻿using Clouds42.Domain.DataModels.Notification;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.NotificationContext.Specifications
{
    public static class NotificationSpecification
    {
        public static Spec<Notification> ByMessageIds(List<Guid> messageIds) =>
            new(rec => messageIds.Contains(rec.Id));

        public static Spec<Notification> ByAccountUserIdOrNull(Guid accountUserId, Guid currentAccountUserId) =>
            new(rec => rec.AccountUserId == accountUserId && rec.AccountUserId == accountUserId);

        public static Spec<Notification> ByAccountUserId(bool isService, Guid accountUserId)=>
            new(rec => isService || rec.AccountUserId == accountUserId);
        
        public static Spec<NotificationDto> ByUserLogin(string? login) =>
            new(rec => string.IsNullOrEmpty(login) || rec.Login == login);

        public static Spec<Notification> ByAccountUserIdOrMessagesIds(Guid? accountUserId, List<Guid> messagesIds, Guid currentUserId) =>
            new(x => accountUserId.HasValue ? x.AccountUserId == accountUserId && x.AccountUserId == currentUserId : messagesIds.Contains(x.Id) && x.AccountUserId == currentUserId);

    }
}
