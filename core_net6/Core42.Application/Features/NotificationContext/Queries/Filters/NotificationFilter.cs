﻿using Clouds42.Domain.DataModels.Notification;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.NotificationContext.Queries.Filters
{
    public class NotificationFilter: IQueryFilter
    {
        public Guid? Id { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? AccountUserId { get; set; }
        public string? Login { get; set; }
        public NotificationState? State { get; set; }
    }
}
