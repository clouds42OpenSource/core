﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using Core42.Application.Features.NotificationContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NotificationContext.Queries
{
    public class GetFilteredNotificationsQuery: IRequest<ManagerResult<PagedDto<NotificationDto>>>, ISortedQuery, IHasFilter<NotificationFilter>, IPagedQuery
    {
        public string OrderBy { get; set; } = $"{nameof(NotificationDto.CreatedOn)}.desc";
        public NotificationFilter Filter { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
}
