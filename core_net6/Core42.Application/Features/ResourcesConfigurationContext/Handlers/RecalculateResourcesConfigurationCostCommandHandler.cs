﻿using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using Core42.Application.Features.Configuration1CContext.Command;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.ResourcesConfigurationContext.Handlers;

public class RecalculateResourcesConfigurationCostCommandHandler(IUnitOfWork unitOfWork)
    : IRequestHandler<RecalculateResourcesConfigurationCostCommand>
{
    public async Task Handle(RecalculateResourcesConfigurationCostCommand request, CancellationToken cancellationToken)
    {
        var resourceConfiguration = await unitOfWork.ResourceConfigurationRepository
            .AsQueryable()
            .Where(x => request.AccountIds.Contains(x.AccountId) && request.BillingServiceIds.Contains(x.BillingServiceId) && (!x.CostIsFixed.HasValue || !x.CostIsFixed.Value))
            .ToListAsync(cancellationToken);

        var accountResources = await unitOfWork.ResourceRepository
            .AsQueryableNoTracking()
            .Where(x => request.BillingServiceIds.Contains(x.BillingServiceType.ServiceId) &&
                        (request.AccountIds.Contains(x.AccountId) && x.AccountSponsorId == null ||
                         request.AccountIds.Contains(x.AccountSponsorId!.Value)) && x.Subject.HasValue &&
                        (!x.DemoExpirePeriod.HasValue || (x.DemoExpirePeriod!.Value - DateTime.Now).Days == 0))
            .ToListAsync(cancellationToken);

        var accountWithSumResources = request.AccountIds
            .GroupJoin(accountResources, x => x, z => z.AccountSponsorId ?? z.AccountId,
                (accountId, res) => (accountId, res))
            .Select(x => new AccountWithResourceCostDto { AccountId = x.accountId, TotalCost = x.res?.Sum(z => z.Cost) ?? 0 })
            .ToList();

        if (request.BillingServiceType == Clouds42Service.MyEnterprise)
        {
            var additionalAccountResources = await unitOfWork.BillingAccountRepository
                .AsQueryableNoTracking()
                .Where(x => request.AccountIds.Contains(x.Id) && x.Account.AccountConfiguration.IsVip)
                .ToListAsync(cancellationToken);

            accountWithSumResources
                .GroupJoin(additionalAccountResources, z => z.AccountId, y => y.Id, (account, billingAccount) => (account, billingAccount))
                .ToList()
                .ForEach(x =>
                {
                    var billingAccount = x.billingAccount.FirstOrDefault();

                    if (billingAccount == null)
                    {
                        return;
                    }

                    x.account.TotalCost += billingAccount.AdditionalResourceCost ?? 0;
                });
        }

        var resourcesConfigurations = accountWithSumResources
            .GroupJoin(resourceConfiguration, x => x.AccountId, z => z.AccountId,
                (accountWithCost, resConf) => (accountWithCost, resConf))
            .ToList()
            .Select(x =>
            {
                var rc = x.resConf.FirstOrDefault();

                if (rc == null)
                {
                    return null;
                }

                rc.Cost = x.accountWithCost.TotalCost;

                return rc;

            }).ToList();

        await unitOfWork.BulkUpdateAsync(resourcesConfigurations.Where(x => x != null));
        await unitOfWork.SaveAsync();
    }
}
