﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Handlers
{
    /// <summary>
    /// Get paged, filtered and sorted segments query handler
    /// </summary>
    public class GetSegmentsListQueryHandler(
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper)
        : IRequestHandler<GetSegmentsQuery, ManagerResult<List<SegmentsListDto>>>
    {
        /// <summary>
        /// Handle get paged, filtered and sorted segments query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<SegmentsListDto>>> Handle(GetSegmentsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                return (await unitOfWork.CloudServicesSegmentRepository
                .AsQueryable()
                .ProjectTo<SegmentsListDto>(mapper.ConfigurationProvider)
                .AutoSort(request)
                .AutoFilter(request?.Filter)
                .ToListAsync(cancellationToken))
                .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка сегментов]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<List<SegmentsListDto>>(ex.Message);
            }
        }

    }

}


