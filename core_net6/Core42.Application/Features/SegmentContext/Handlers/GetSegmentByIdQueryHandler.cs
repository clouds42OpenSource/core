﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Handlers;

/// <summary>
/// Get segment by id query handler
/// </summary>
public class GetSegmentByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IAccessProvider accessProvider,
    IHandlerException handlerException,
    IMapper mapper,
    ISender sender)
    : IRequestHandler<GetSegmentByIdQuery, ManagerResult<SegmentExtendedDto>>
{
    public async Task<ManagerResult<SegmentExtendedDto>> Handle(GetSegmentByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.ViewCloudServiceSegment);

            return new SegmentExtendedDto
            {
                Id = request.Id,
                BaseData = await GetBaseDataAsync(request.Id, cancellationToken),
                AdditionalData = (await sender.Send(new GetAllSegmentDropDownsListsQuery(), cancellationToken)).Result,
                MigrationData = await GetMigrationDataAsync(request.Id, cancellationToken),
                FileStoragesData = await GetFileStoragesDataAsync(request.Id, cancellationToken),
                TerminalServersData = await GetTerminalServersAsync(request.Id, cancellationToken)
            }.ToOkManagerResult();
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex,
                $"[Ошибка получения сегмента {request.Id} пользователем {accessProvider.Name}.]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<SegmentExtendedDto>(ex.Message);
        }
    }

    private async Task<SegmentBaseDataDto> GetBaseDataAsync(Guid id, CancellationToken cancellationToken)
    {
        return await unitOfWork.CloudServicesSegmentRepository
            .AsQueryable()
            .ProjectTo<SegmentBaseDataDto>(mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
    }
    private async Task<BaseAvailableDto<SegmentMigrationDto, SegmentMigrationDto>> GetMigrationDataAsync(Guid id, CancellationToken cancellationToken)
    {
        var availableMigration = (await unitOfWork.AvailableMigrationRepository
                .AsQueryable()
                .Where(x => x.SegmentIdFrom == id)
                .ProjectTo<SegmentMigrationDto>(mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken))
            .DistinctBy(x => x.Key)
            .ToList();

        var availableMigrationIds = availableMigration.Select(x => x.Key).ToList();

        var notAvailable = await unitOfWork.CloudServicesSegmentRepository
            .AsQueryable()
            .ProjectTo<SegmentMigrationDto>(mapper.ConfigurationProvider)
            .Where(x => !availableMigrationIds.Contains(x.Key))
            .ToListAsync(cancellationToken);

        var currentSegment = await unitOfWork.CloudServicesSegmentRepository
            .AsQueryable()
            .ProjectTo<SegmentMigrationDto>(mapper.ConfigurationProvider)
            .FirstOrDefaultAsync(x => x.Key == id, cancellationToken);

        var joined = availableMigration.Concat(notAvailable).ToList();

        foreach (var item in joined.Where(item => item.BackupStorageId == currentSegment.BackupStorageId &&
                                                  item.SqlServerId == currentSegment.SqlServerId &&
                                                  item.EnterpriseServer82Id == currentSegment.EnterpriseServer82Id &&
                                                  item.EnterpriseServer83Id == currentSegment.EnterpriseServer83Id &&
                                                  item.GatewayTerminalsId == currentSegment.GatewayTerminalsId &&
                                                  item.ServicesTerminalFarmId == currentSegment.ServicesTerminalFarmId))
        {
            item.IsSimilar = true;
        }

        var available = availableMigration
            .OrderByDescending(ms => ms.IsSimilar)
            .ThenBy(ms => ms.Value)
            .ToList();

        var selected = notAvailable
            .OrderByDescending(ms => ms.IsSimilar)
            .ThenBy(ms => ms.Value)
            .ToList();

        return new BaseAvailableDto<SegmentMigrationDto, SegmentMigrationDto>(available, selected);
    }
    private async Task<BaseAvailableDto<SegmentFileStorageDto, SegmentFileStorageDto>> GetFileStoragesDataAsync(Guid id,
        CancellationToken cancellationToken)
    {
        var notAvailable = (await unitOfWork.CloudServicesSegmentStorageRepository
                .AsQueryable()
                .Where(x => x.SegmentID == id)
                .ProjectTo<SegmentFileStorageDto>(mapper.ConfigurationProvider)
                .OrderBy(x => x.Value)
                .ToListAsync(cancellationToken))
            .DistinctBy(x => x.Key)
            .ToList();

        var notAvailableIds = notAvailable.Select(x => x.Key).ToList();

        var available = (await unitOfWork.CloudServicesFileStorageServerRepository
                .AsQueryable()
                .Where(x => !notAvailableIds.Contains(x.ID))
                .ProjectTo<SegmentFileStorageDto>(mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken))
            .DistinctBy(x => x.Key)
            .ToList();


        return new BaseAvailableDto<SegmentFileStorageDto, SegmentFileStorageDto>(available, notAvailable);
    }
    private async Task<BaseAvailableDto<SegmentTerminalServerDto, SegmentTerminalServerDto>> GetTerminalServersAsync(Guid id, CancellationToken cancellationToken)
    {

        var notAvailable = (await unitOfWork.CloudServicesSegmentTerminalServerRepository
                .AsQueryable()
                .Where(x => x.SegmentId == id)
                .Select(x => x.CloudTerminalServer)
                .ProjectTo<SegmentTerminalServerDto>(mapper.ConfigurationProvider)
                .OrderBy(x => x.Value)
                .ToListAsync(cancellationToken))
            .DistinctBy(x => x.Key)
            .ToList();

        var notAvailableIds = notAvailable.Select(x => x.Key).ToList();

        var available = await unitOfWork.CloudTerminalServerRepository
            .AsQueryable()
            .Where(x => !notAvailableIds.Contains(x.ID))
            .ProjectTo<SegmentTerminalServerDto>(mapper.ConfigurationProvider)
            .ToListAsync(cancellationToken);

        return new BaseAvailableDto<SegmentTerminalServerDto, SegmentTerminalServerDto>(available, notAvailable);
    }
}
