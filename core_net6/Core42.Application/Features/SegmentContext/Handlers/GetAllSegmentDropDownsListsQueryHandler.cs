﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Handlers;

public class GetAllSegmentDropDownsListsQueryHandler(
    ILogger42 logger,
    IUnitOfWork unitOfWork,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetAllSegmentDropDownsListsQuery, ManagerResult<SegmentAdditionalDataDto>>
{
    public async Task<ManagerResult<SegmentAdditionalDataDto>> Handle(GetAllSegmentDropDownsListsQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_View, () => accessProvider.ContextAccountId);

            return new SegmentAdditionalDataDto
            {
                ContentServers = await unitOfWork.CloudServicesContentServerRepository
                    .AsQueryable()
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken),

                TerminalFarms = await unitOfWork.CloudServicesTerminalFarmRepository
                    .AsQueryable()
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken),

                GatewayTerminals = await unitOfWork.CloudServicesGatewayTerminalRepository
                    .AsQueryable()
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken),

                CoreHostingList = await unitOfWork.CoreHostingRepository
                    .AsQueryable()
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken),

                SqlServers = await unitOfWork.CloudServicesSqlServerRepository
                    .AsQueryable()
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken),

                BackupStorages = await unitOfWork.CloudServicesBackupStorageRepository
                    .AsQueryable()
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken),

                EnterpriseServers = await unitOfWork.CloudServicesEnterpriseServerRepository
                    .AsQueryable()
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken),

                FileStorageServers = await unitOfWork.CloudServicesFileStorageServerRepository
                    .AsQueryable()
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken),

                Alpha83Versions = await unitOfWork.PlatformVersionReferencesRepository
                    .GetAlpha83Versions()
                    .ToListAsync(cancellationToken),

                Stable82Versions = await unitOfWork.PlatformVersionReferencesRepository
                    .GetStable82Versions()
                    .ToListAsync(cancellationToken),

                Stable83Versions = await unitOfWork.PlatformVersionReferencesRepository
                    .GetStable83Versions()
                    .ToListAsync(cancellationToken),

                AutoUpdateNodeList = await unitOfWork.AutoUpdateNodeRepository
                    .AsQueryable()
                    .Where(x => x.DontRunInGlobalAU)
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .ToListAsync(cancellationToken)

            }.ToOkManagerResult();
        }

        catch (Exception ex)
        {
            logger.Error(ex, "Не удалось получить выдающий список для страницы сегменты");

            return PagedListExtensions.ToPreconditionFailedManagerResult<SegmentAdditionalDataDto>(ex.Message);
        }
    }
}
