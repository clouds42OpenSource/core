﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SegmentContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Handlers
{
    public class GetSegmentsShortDataQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetSegmentsShortDataQuery, ManagerResult<Dictionary<Guid, string>>>
    {
        public async Task<ManagerResult<Dictionary<Guid, string>>> Handle(GetSegmentsShortDataQuery request, CancellationToken cancellationToken)
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);
            return (await unitOfWork.CloudServicesSegmentRepository
                .AsQueryable()
                .OrderBy(s => s.Name)
                .ToDictionaryAsync(x => x.ID, x => x.Name, cancellationToken))
            .ToOkManagerResult();
        }
    }
}
