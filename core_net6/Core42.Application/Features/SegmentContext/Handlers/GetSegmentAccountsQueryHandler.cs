﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Handlers;

public class GetSegmentAccountsQueryHandler(
    IAccessProvider accessProvider,
    IHandlerException handlerException,
    IUnitOfWork unitOfWork,
    IMapper mapper)
    : IRequestHandler<GetSegmentAccountsQuery, ManagerResult<PagedDto<SegmentAccountDto>>>
{
    public async Task<ManagerResult<PagedDto<SegmentAccountDto>>> Handle(GetSegmentAccountsQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.AccountsRepository
                    .AsQueryable()
                    .ProjectTo<SegmentAccountDto>(mapper.ConfigurationProvider)
                    .AutoFilter(request.Filter)
                    .AutoSort(request)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка аккаунтов для сегмента]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<SegmentAccountDto>>(ex.Message);
        }
    }
}
