﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Handlers
{
    /// <summary>
    /// Get paged, filtered and sorted segments query handler
    /// </summary>
    public class GetFilteredSegmentsQueryHandler(
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper)
        : IRequestHandler<GetFilteredSegmentsQuery, ManagerResult<PagedDto<SegmentDto>>>
    {
        /// <summary>
        /// Handle get paged, filtered and sorted segments query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<SegmentDto>>> Handle(GetFilteredSegmentsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                return (await unitOfWork.CloudServicesSegmentRepository
                        .AsQueryable()
                        .Where(SegmentSpecification.ByEnterpriseServerId(request?.Filter?.EnterpriseServerId))
                        .ProjectTo<SegmentDto>(mapper.ConfigurationProvider)
                        .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                        .AutoFilter(request?.Filter)
                        .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка сегментов]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<SegmentDto>>(ex.Message);
            }
        }

    }

    public static class SegmentSpecification
    {
        public static Spec<CloudServicesSegment> ByEnterpriseServerId(Guid? enterpriseServiceId)
        {
            return new Spec<CloudServicesSegment>(x =>
                !enterpriseServiceId.HasValue || x.EnterpriseServer83ID == enterpriseServiceId ||
                x.EnterpriseServer82ID == enterpriseServiceId);
        }
    }
}


