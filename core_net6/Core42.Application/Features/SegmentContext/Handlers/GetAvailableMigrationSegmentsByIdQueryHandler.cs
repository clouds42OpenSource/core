﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Handlers
{
    /// <summary>
    /// Get available migration segments by id query handler
    /// </summary>
    public class GetAvailableMigrationSegmentsByIdQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper)
        : IRequestHandler<GetAvailableMigrationSegmentsByIdQuery, ManagerResult<List<BaseDictionaryDto>>>
    {
        /// <summary>
        /// Handle get available migration segments by id query 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<BaseDictionaryDto>>> Handle(GetAvailableMigrationSegmentsByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var segmentDictionaryDtos = new List<BaseDictionaryDto>();

                if (!request.SegmentId.HasValue)
                    return segmentDictionaryDtos.ToOkManagerResult();

                var user = await accessProvider.GetUserAsync();
                if (user == null || (!user.Groups.Contains(AccountUserGroup.CloudAdmin)
                                     && !user.Groups.Contains(AccountUserGroup.Hotline)
                                     && !user.Groups.Contains(AccountUserGroup.CloudSE)))
                {
                    return segmentDictionaryDtos.ToOkManagerResult();
                }

                var availableMigrations = await unitOfWork.AvailableMigrationRepository
                    .AsQueryable()
                    .Where(x => x.SegmentIdFrom == request.SegmentId)
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                var segments = await unitOfWork.CloudServicesSegmentRepository
                    .AsQueryable()
                    .Where(x => x.ID == request.SegmentId)
                    .ProjectTo<BaseDictionaryDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                return availableMigrations.DistinctBy(x => x.Key).Concat(segments).OrderBy(x => x.Value).ToList().ToOkManagerResult();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения доступных сегментов для миграции пользователем {accessProvider.Name}.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<BaseDictionaryDto>>(ex.Message);
            }
        }
    }
}
