﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.SegmentContext.Dtos;

public class SegmentAccountDto : IMapFrom<Account>
{
    [JsonIgnore]
    public Guid SegmentId { get; set; }
    public int IndexNumber { get; set; }
    public string AccountCaption { get; set; }
    public int? SizeOfFileAccountDatabases { get; set; }
    public int? SizeOfServerAccountDatabases { get; set; }
    public long? SizeOfClientFiles { get; set; }
    public void Mapping(Profile profile)
    {
        profile.CreateMap<Account, SegmentAccountDto>()
            .ForMember(x => x.IndexNumber, z => z.MapFrom(y => y.IndexNumber))
            .ForMember(x => x.SegmentId, z => z.MapFrom(y => y.AccountConfiguration.SegmentId))
            .ForMember(x => x.AccountCaption, z => z.MapFrom(y => y.AccountCaption))
            .ForMember(x => x.SizeOfServerAccountDatabases, z => z.MapFrom(y => y.AccountDatabases
                .Where(a => (!a.IsFile.HasValue || !a.IsFile.Value) && (a.State == DatabaseState.Ready.ToString() ||
                    a.State == DatabaseState.NewItem
                        .ToString())).Sum(x => x.SizeInMB)))
            .ForMember(x => x.SizeOfFileAccountDatabases, z => z.MapFrom(y => y.AccountDatabases
                .Where(a => a.IsFile.HasValue && a.IsFile.Value && (a.State == DatabaseState.Ready.ToString() ||
                                                                            a.State == DatabaseState.NewItem
                                                                                .ToString())).Sum(x => x.SizeInMB)))
            .ForMember(x => x.SizeOfClientFiles, z => z.MapFrom(y => y.MyDiskPropertiesByAccount.MyDiskFilesSizeInMb));
    }
}
