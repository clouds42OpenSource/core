﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.SegmentContext.Dtos
{
    /// <summary>
    /// Segment dto
    /// </summary>
    public class SegmentDto : IMapFrom<CloudServicesSegment>
    {
        /// <summary>
        /// Segment id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Segment name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Segment description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gateway terminal name
        /// </summary>
        public string GatewayTerminalName { get; set; }

        /// <summary>
        /// Terminal farm name
        /// </summary>
        public string TerminalFarmName { get; set; }

        /// <summary>
        /// Enterprise server 8.2 name
        /// </summary>
        public string EnterpriseServer82Name { get; set; }

        /// <summary>
        /// Enterprise server 8.3 name
        /// </summary>
        public string EnterpriseServer83Name { get; set; }

        /// <summary>
        /// Content server name
        /// </summary>
        public string ContentServerName { get; set; }

        /// <summary>
        /// File storage name
        /// </summary>
        public string FileStorageServerName { get; set; }

        /// <summary>
        /// Sql server name
        /// </summary>
        public string SqlServerName { get; set; }

        /// <summary>
        /// Backup storage name
        /// </summary>
        public string BackupStorageName { get; set; }

        /// <summary>
        /// Users files storage path
        /// </summary>
        public string CustomFileStoragePath { get; set; }

        /// <summary>
        /// Default file storage name
        /// </summary>
        public string DefaultFileStorageName { get; set; }

        /// <summary>
        /// Stable 83 version
        /// </summary>
        public string Stable83Version { get; set; }

        /// <summary>
        /// Hosting name
        /// </summary>
        public string HostingName { get; set; }

        /// <summary>
        /// Terminal farm id
        /// </summary>
        [JsonIgnore]
        public Guid? TerminalFarmId { get; set; }
        
        /// <summary>
        /// Content server id
        /// </summary>
        [JsonIgnore]
        public Guid? ContentServerId { get; set; }

        /// <summary>
        /// File storage id
        /// </summary>
        [JsonIgnore]
        public Guid? FileStorageServerId { get; set; }

        /// <summary>
        /// Sql server id
        /// </summary>
        [JsonIgnore]
        public Guid? SqlServerId { get; set; }

        /// <summary>
        /// Terminal gateway id
        /// </summary>
        [JsonIgnore]
        public Guid? GatewayTerminalId { get; set; }

        /// <summary>
        /// Нода автообновления
        /// </summary>
        public Guid? AutoUpdateNodeId { get; set; }

        /// <summary>
        /// Признак необходимости монтирования диска
        /// </summary>
        public bool NotMountDiskR { get; set; }

        /// <summary>
        /// Auto mapper mapping method
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CloudServicesSegment, SegmentDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.ID))
                .ForMember(x => x.Name, z => z.MapFrom(y => y.Name))
                .ForMember(x => x.Description, z => z.MapFrom(y => y.Description))
                .ForMember(x => x.CustomFileStoragePath, z => z.MapFrom(y => y.CustomFileStoragePath))
                .ForMember(x => x.BackupStorageName, z => z.MapFrom(y => y.CloudServicesBackupStorage.Name))
                .ForMember(x => x.ContentServerName, z => z.MapFrom(y => y.CloudServicesContentServer.Name))
                .ForMember(x => x.EnterpriseServer82Name, z => z.MapFrom(y => y.CloudServicesEnterpriseServer82.Name))
                .ForMember(x => x.EnterpriseServer83Name, z => z.MapFrom(y => y.CloudServicesEnterpriseServer83.Name))
                .ForMember(x => x.FileStorageServerName, z => z.MapFrom(y => y.CloudServicesFileStorageServer.Name))
                .ForMember(x => x.GatewayTerminalName, z => z.MapFrom(y => y.CloudServicesGatewayTerminal.Name))
                .ForMember(x => x.TerminalFarmName, z => z.MapFrom(y => y.CloudServicesTerminalFarm.Name))
                .ForMember(x => x.SqlServerName, z => z.MapFrom(y => y.CloudServicesSQLServer.Name))
                .ForMember(x => x.Stable83Version, z => z.MapFrom(y => y.Stable83Version))
                .ForMember(x => x.HostingName, z => z.MapFrom(y => y.CoreHosting.Name))
                .ForMember(x => x.TerminalFarmId, z => z.MapFrom(y => y.ServicesTerminalFarmID))
                .ForMember(x => x.SqlServerId, z => z.MapFrom(y => y.SQLServerID))
                .ForMember(x => x.AutoUpdateNodeId, z => z.MapFrom(y => y.AutoUpdateNodeId))
                .ForMember(x => x.NotMountDiskR, z => z.MapFrom(y => y.NotMountDiskR))
                .ForMember(x => x.FileStorageServerId, z => z.MapFrom(y =>
                    y.CloudServicesSegmentStorages.FirstOrDefault(x => x.IsDefault)!.CloudServicesFileStorageServer
                        .ID))
                .ForMember(x => x.ContentServerId, z => z.MapFrom(y => y.ContentServerID))
                .ForMember(x => x.GatewayTerminalId, z => z.MapFrom(y => y.GatewayTerminalsID))
                .ForMember(x => x.DefaultFileStorageName,
                    z => z.MapFrom(y =>
                        y.CloudServicesSegmentStorages.FirstOrDefault(x => x.IsDefault)!.CloudServicesFileStorageServer
                            .Name));
        }
    }
}


