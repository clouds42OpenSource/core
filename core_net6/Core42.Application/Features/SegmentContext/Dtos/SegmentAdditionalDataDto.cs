﻿using Core42.Application.Features.BaseContext.Dtos;
using Newtonsoft.Json;

namespace Core42.Application.Features.SegmentContext.Dtos;

public class SegmentAdditionalDataDto
{
    /// <summary>
    /// Список бэкап серверов
    /// </summary>
    public List<BaseDictionaryDto> BackupStorages { get; set; }

    /// <summary>
    /// Список файловых хранилищ
    /// </summary>
    public List<BaseDictionaryDto> FileStorageServers { get; set; }

    /// <summary>
    /// Список серверов Предприятия 8.3
    /// </summary>
    public List<BaseDictionaryDto> Enterprise82Servers { get { return EnterpriseServers.Where(x => x.Version == "V82").ToList(); } }

    /// <summary>
    /// Список серверов Предприятия 8.3
    /// </summary>
    public List<BaseDictionaryDto> Enterprise83Servers { get { return EnterpriseServers.Where(x => x.Version != "V82").ToList(); } }

    [JsonIgnore]
    public List<BaseDictionaryDto> EnterpriseServers { get; set; }
    /// <summary>
    /// Список серверов публикаций
    /// </summary>
    public List<BaseDictionaryDto> ContentServers { get; set; }

    /// <summary>
    /// Список SQL серверов
    /// </summary>
    public List<BaseDictionaryDto> SqlServers { get; set; }

    /// <summary>
    /// Список ферм ТС
    /// </summary>
    public List<BaseDictionaryDto> TerminalFarms { get; set; }

    /// <summary>
    /// Список шлюзов
    /// </summary>
    public List<BaseDictionaryDto> GatewayTerminals { get; set; }

    /// <summary>
    /// Список стабильных версий 8.2
    /// </summary>
    public List<string> Stable82Versions { get; set; }

    /// <summary>
    /// Список стабильных версий 8.3
    /// </summary>
    public List<string> Stable83Versions { get; set; }

    /// <summary>
    /// Список альфа версий 8.3
    /// </summary>
    public List<string> Alpha83Versions { get; set; }

    /// <summary>
    /// Список хостингов облака
    /// </summary>
    public List<BaseDictionaryDto> CoreHostingList { get; set; }

    /// <summary>
    /// Список хостингов облака
    /// </summary>
    public List<BaseDictionaryDto> AutoUpdateNodeList { get; set; }
}
