﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SegmentContext.Dtos
{
    /// <summary>
    /// Segments list dto
    /// </summary>
    public class SegmentsListDto : IMapFrom<CloudServicesSegment>
    {
        /// <summary>
        /// Segment id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Segment name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Auto mapper mapping method
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CloudServicesSegment, SegmentsListDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.ID))
                .ForMember(x => x.Name, z => z.MapFrom(y => y.Name));
        }
    }
}


