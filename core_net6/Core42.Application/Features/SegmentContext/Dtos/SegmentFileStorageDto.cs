﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Core42.Application.Features.BaseContext.Dtos;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SegmentContext.Dtos;

public class SegmentFileStorageDto : BaseDictionaryDto, IMapFrom<CloudServicesSegmentStorage>, IMapFrom<CloudServicesFileStorageServer>
{
    public bool CanDeleteFromAvailable { get { return !IsDefault && DatabasesCount is not > 0; } }

    public int? DatabasesCount { get; set; }

    public bool IsDefault { get; set; }
    public new void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesSegmentStorage, SegmentFileStorageDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.CloudServicesFileStorageServer.ID))
            .ForMember(x => x.IsDefault, z => z.MapFrom(y => y.IsDefault))
            .ForMember(x => x.DatabasesCount, z => z.MapFrom(y => y.CloudServicesSegment.AccountConfigurations
                .Sum(x => x.Account.AccountDatabases.Count(database => database.FileStorageID == y.FileStorageID &&
                                                                       database.IsFile.HasValue &&
                                                                       database.IsFile == true &&
                                                                       (
                                                                           database.State == DatabaseState.Ready.ToString() ||
                                                                           database.State == DatabaseState.TransferDb.ToString() ||
                                                                           database.State == DatabaseState.NewItem.ToString() ||
                                                                           database.State == DatabaseState.ProcessingSupport.ToString()
                                                                       )))))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.CloudServicesFileStorageServer.Name));


        profile.CreateMap<CloudServicesFileStorageServer, SegmentFileStorageDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name))
            .ForMember(x => x.IsDefault, z => z.Ignore())
            .ForMember(x => x.DatabasesCount, z => z.Ignore());
    }
}
