﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.BaseContext.Dtos;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SegmentContext.Dtos;

public class SegmentTerminalServerDto : BaseDictionaryDto, IMapFrom<CloudTerminalServer>
{
    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    public new void Mapping(Profile profile)
    {
        profile.CreateMap<CloudTerminalServer, SegmentTerminalServerDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name))
            .ForMember(x => x.ConnectionAddress, z => z.MapFrom(y => y.ConnectionAddress));
    }
}
