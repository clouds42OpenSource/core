﻿using Core42.Application.Features.BaseContext.Dtos;

namespace Core42.Application.Features.SegmentContext.Dtos;

public class SegmentExtendedDto
{
    /// <summary>
    /// Segment id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Base segment data
    /// </summary>
    public SegmentBaseDataDto BaseData { get; set; }

    /// <summary>
    /// Migration data
    /// </summary>
    public BaseAvailableDto<SegmentMigrationDto, SegmentMigrationDto> MigrationData { get; set; }

    /// <summary>
    /// File storages info
    /// </summary>
    public BaseAvailableDto<SegmentFileStorageDto, SegmentFileStorageDto> FileStoragesData { get; set; }

    /// <summary>
    /// Terminal servers data
    /// </summary>
    public BaseAvailableDto<SegmentTerminalServerDto, SegmentTerminalServerDto> TerminalServersData { get; set; }

    /// <summary>
    /// Additional data
    /// </summary>
    public SegmentAdditionalDataDto AdditionalData { get; set; }
}
