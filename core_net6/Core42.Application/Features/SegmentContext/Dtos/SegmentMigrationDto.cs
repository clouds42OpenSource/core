﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.BaseContext.Dtos;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.SegmentContext.Dtos;

public class SegmentMigrationDto : BaseDictionaryDto, IMapFrom<AvailableMigration>, IMapFrom<CloudServicesSegment>
{
    /// <summary>
    /// Is available segment flag
    /// </summary>
    public bool IsAvailable { get; set; }

    /// <summary>
    /// Is similar segment flag
    /// </summary>
    public bool IsSimilar { get; set; }


    /// <summary>
    /// Backup storage id
    /// </summary>
    [JsonIgnore]
    public Guid BackupStorageId { get; set; }

    /// <summary>
    /// Sql storage id
    /// </summary>
    [JsonIgnore]
    public Guid SqlServerId { get; set; }

    /// <summary>
    /// Terminal farm id
    /// </summary>
    [JsonIgnore]
    public Guid ServicesTerminalFarmId { get; set; }

    /// <summary>
    /// V82 server id
    /// </summary>
    [JsonIgnore]
    public Guid EnterpriseServer82Id { get; set; }

    /// <summary>
    /// V83 server id
    /// </summary>
    [JsonIgnore]
    public Guid EnterpriseServer83Id { get; set; }

    /// <summary>
    /// Gateway terminal id
    /// </summary>
    [JsonIgnore]
    public Guid GatewayTerminalsId { get; set; }

    public new void Mapping(Profile profile)
    {
        profile.CreateMap<AvailableMigration, SegmentMigrationDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.SegmentIdTo))
            .ForMember(x => x.BackupStorageId, z => z.MapFrom(y => y.SegmentTo.BackupStorageID))
            .ForMember(x => x.SqlServerId, z => z.MapFrom(y => y.SegmentTo.SQLServerID))
            .ForMember(x => x.ServicesTerminalFarmId, z => z.MapFrom(y => y.SegmentTo.ServicesTerminalFarmID))
            .ForMember(x => x.EnterpriseServer82Id, z => z.MapFrom(y => y.SegmentTo.EnterpriseServer82ID))
            .ForMember(x => x.EnterpriseServer83Id, z => z.MapFrom(y => y.SegmentTo.EnterpriseServer83ID))
            .ForMember(x => x.GatewayTerminalsId, z => z.MapFrom(y => y.SegmentTo.GatewayTerminalsID))
            .ForMember(x => x.IsAvailable, z => z.MapFrom(y => true))
            .ForMember(x => x.IsSimilar, z => z.MapFrom(y => false))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.SegmentTo.Name));

        profile.CreateMap<CloudServicesSegment, SegmentMigrationDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.IsAvailable, z => z.MapFrom(y => false))
            .ForMember(x => x.IsSimilar, z => z.MapFrom(y => false))
            .ForMember(x => x.BackupStorageId, z => z.MapFrom(y => y.BackupStorageID))
            .ForMember(x => x.SqlServerId, z => z.MapFrom(y => y.SQLServerID))
            .ForMember(x => x.ServicesTerminalFarmId, z => z.MapFrom(y => y.ServicesTerminalFarmID))
            .ForMember(x => x.EnterpriseServer82Id, z => z.MapFrom(y => y.EnterpriseServer82ID))
            .ForMember(x => x.EnterpriseServer83Id, z => z.MapFrom(y => y.EnterpriseServer83ID))
            .ForMember(x => x.GatewayTerminalsId, z => z.MapFrom(y => y.GatewayTerminalsID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));
    }
}
