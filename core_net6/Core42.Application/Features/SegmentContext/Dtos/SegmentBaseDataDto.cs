﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.SegmentContext.Dtos;

public class SegmentBaseDataDto : IMapFrom<CloudServicesSegment>
{
    [JsonIgnore]
    public Guid? Id { get; set; }

    /// <summary>
    /// Segment element name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Backup storage id
    /// </summary>
    public Guid BackupStorageId { get; set; }

    /// <summary>
    /// File storage id
    /// </summary>
    public Guid FileStorageServersId { get; set; }

    /// <summary>
    /// v82 ent server id
    /// </summary>
    public Guid EnterpriseServer82Id { get; set; }

    /// <summary>
    /// v82 ent server id
    /// </summary>
    public Guid EnterpriseServer83Id { get; set; }

    /// <summary>
    /// Content server id
    /// </summary>
    public Guid ContentServerId { get; set; }

    /// <summary>
    /// Sql server id
    /// </summary>
    public Guid SqlServerId { get; set; }

    /// <summary>
    /// terminal farm id
    /// </summary>
    public Guid ServicesTerminalFarmId { get; set; }

    /// <summary>
    /// ID шлюза
    /// </summary>
    public Guid? GatewayTerminalsId { get; set; }

    /// <summary>
    /// stable v82 version id
    /// </summary>
    public string Stable82VersionId { get; set; }

    /// <summary>
    /// stable v83 version id
    /// </summary>
    public string Stable83VersionId { get; set; }

    /// <summary>
    /// alpha v83 version id
    /// </summary>
    public string Alpha83VersionId { get; set; }

    /// <summary>
    /// Custom file storage path
    /// </summary>
    public string CustomFileStoragePath { get; set; }

    /// <summary>
    /// hosting name
    /// </summary>
    public string HostingName { get; set; }

    /// <summary>
    /// description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// delimiter must use web service
    /// </summary>
    public bool DelimiterDatabaseMustUseWebService { get; set; }

    /// <summary>
    /// default segment storage id
    /// </summary>
    public Guid DefaultSegmentStorageId { get; set; }

    /// <summary>
    /// Нода автообновления
    /// </summary>
    public Guid? AutoUpdateNodeId { get; set; }
    /// <summary>
    /// Признак необходимости монтирования диска
    /// </summary>
    public bool NotMountDiskR { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesSegment, SegmentBaseDataDto>()
            .ForMember(x => x.Name, z => z.MapFrom(y => y.Name))
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.BackupStorageId, z => z.MapFrom(y => y.BackupStorageID))
            .ForMember(x => x.FileStorageServersId, z => z.MapFrom(y => y.FileStorageServersID))
            .ForMember(x => x.Stable82VersionId, z => z.MapFrom(y => y.Stable82Version))
            .ForMember(x => x.Alpha83VersionId, z => z.MapFrom(y => y.Alpha83Version))
            .ForMember(x => x.Stable83VersionId, z => z.MapFrom(y => y.Stable83Version))
            .ForMember(x => x.ContentServerId, z => z.MapFrom(y => y.ContentServerID))
            .ForMember(x => x.SqlServerId, z => z.MapFrom(y => y.SQLServerID))
            .ForMember(x => x.ServicesTerminalFarmId, z => z.MapFrom(y => y.ServicesTerminalFarmID))
            .ForMember(x => x.GatewayTerminalsId, z => z.MapFrom(y => y.GatewayTerminalsID))
            .ForMember(x => x.EnterpriseServer82Id, z => z.MapFrom(y => y.EnterpriseServer82ID))
            .ForMember(x => x.EnterpriseServer83Id, z => z.MapFrom(y => y.EnterpriseServer83ID))
            .ForMember(x => x.CustomFileStoragePath, z => z.MapFrom(y => y.CustomFileStoragePath))
            .ForMember(x => x.HostingName, z => z.MapFrom(y => y.CoreHosting.Name))
            .ForMember(x => x.Description, z => z.MapFrom(y => y.Description))
            .ForMember(x => x.AutoUpdateNodeId, z => z.MapFrom(y => y.AutoUpdateNodeId))
            .ForMember(x => x.NotMountDiskR, z => z.MapFrom(y => y.NotMountDiskR))
            .ForMember(x => x.DelimiterDatabaseMustUseWebService,
                z => z.MapFrom(y => y.DelimiterDatabaseMustUseWebService))
            .ForMember(x => x.DefaultSegmentStorageId,
                z => z.MapFrom(y => y.CloudServicesSegmentStorages.FirstOrDefault(x => x.IsDefault).FileStorageID));
    }
}
