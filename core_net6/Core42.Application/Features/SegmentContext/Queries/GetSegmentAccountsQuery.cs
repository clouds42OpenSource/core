﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Queries
{
    /// <summary>
    /// Get segment accounts query
    /// </summary>
    public class GetSegmentAccountsQuery : IRequest<ManagerResult<PagedDto<SegmentAccountDto>>>, IPagedQuery, IHasFilter<SegmentAccountFilter>, ISortedQuery
    {
        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Filter
        /// </summary>
        public SegmentAccountFilter Filter { get; set; }

        /// <summary>
        /// Order by 
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(SegmentAccountDto.IndexNumber)}.desc";
    }
}
