﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BaseContext.Dtos;
using MediatR;

namespace Core42.Application.Features.SegmentContext.Queries
{
    /// <summary>
    /// Get available migration segments by id query
    /// </summary>
    public class GetAvailableMigrationSegmentsByIdQuery(Guid? segmentId)
        : IRequest<ManagerResult<List<BaseDictionaryDto>>>
    {
        /// <summary>
        /// Segment id
        /// </summary>
        public Guid? SegmentId { get; set; } = segmentId;
    }
}
