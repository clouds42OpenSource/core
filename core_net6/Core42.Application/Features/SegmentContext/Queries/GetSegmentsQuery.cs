﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.SegmentContext.Queries
{
    /// <summary>
    /// Get paged, filtered and sorted segments query
    /// </summary>
    public class GetSegmentsQuery : IRequest<ManagerResult<List<SegmentsListDto>>>, ISortedQuery,  IHasFilter<SegmentFilter>
    {
        /// <summary>
        /// Order by field
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(SegmentsListDto.Name)}.asc";

        /// <summary>
        /// Segment filter
        /// </summary>
        public SegmentFilter Filter { get; set; }
    }
}
