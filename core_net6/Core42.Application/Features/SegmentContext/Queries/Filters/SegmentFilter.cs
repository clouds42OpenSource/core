﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Queries.Filters
{
    public class SegmentFilter : IQueryFilter
    {
        /// <summary>
        /// Segment id
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// Segment name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Segment description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Users files path
        /// </summary>
        public string CustomFileStoragePath { get; set; }

        /// <summary>
        /// Default file storage name
        /// </summary>
        public string DefaultFileStorageName { get; set; }

        /// <summary>
        /// Terminal farm id
        /// </summary>
        public Guid? TerminalFarmId { get; set; }

        /// <summary>
        /// Enterprise server id
        /// </summary>
        public Guid? EnterpriseServerId { get; set; }

        /// <summary>
        /// Content server id
        /// </summary>
        public Guid? ContentServerId { get; set; }

        /// <summary>
        /// File storage id
        /// </summary>
        public Guid? FileStorageServerId { get; set; }

        /// <summary>
        /// Sql server id
        /// </summary>
        public Guid? SqlServerId { get; set; }

        /// <summary>
        /// Terminal gateway id
        /// </summary>
        public Guid? GatewayTerminalId { get; set; }

        /// <summary>
        /// Hosting name
        /// </summary>
        public string HostingName { get; set; }
    }
}


