﻿using LinqExtensionsNetFramework;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.SegmentContext.Queries.Filters;

public class SegmentAccountFilter : IQueryFilter
{
    [FromRoute]
    public Guid SegmentId { get; set; }
    public int? IndexNumber { get; set; }
    public string? AccountCaption { get; set; }
}
