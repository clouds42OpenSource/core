﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.SegmentContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.SegmentContext.Queries
{
    /// <summary>
    /// Get segment by id query
    /// </summary>
    public class GetSegmentByIdQuery : IRequest<ManagerResult<SegmentExtendedDto>>
    {
        [FromRoute]
        public Guid Id { get; set; }
    }
}
