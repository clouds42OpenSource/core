﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SegmentContext.Queries
{
    /// <summary>
    /// Get paged, filtered and sorted segments query
    /// </summary>
    public class GetFilteredSegmentsQuery : IRequest<ManagerResult<PagedDto<SegmentDto>>>, ISortedQuery, IPagedQuery, IHasFilter<SegmentFilter>
    {
        /// <summary>
        /// Order by field
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(SegmentDto.Name)}.asc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Segment filter
        /// </summary>
        public SegmentFilter Filter { get; set; }
    }
}
