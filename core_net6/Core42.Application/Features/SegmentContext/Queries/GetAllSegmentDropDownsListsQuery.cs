﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.SegmentContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.SegmentContext.Queries
{
    public class GetAllSegmentDropDownsListsQuery : IRequest<ManagerResult<SegmentAdditionalDataDto>>, ISortedQuery
    {
        public string OrderBy { get; set; } = $"{nameof(BaseDictionaryDto.Value)}.asc";
    }
}
