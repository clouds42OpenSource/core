﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BaseContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.SegmentContext.Queries
{
    public class GetSegmentsShortDataQuery : IRequest<ManagerResult<Dictionary<Guid, string>>>, ISortedQuery
    {
        public string OrderBy { get; set; } = $"{nameof(BaseDictionaryDto.Value)}.asc";
    }
}
