﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.FastaContext.Dtos;
using MediatR;

namespace Core42.Application.Features.FastaContext.Queries
{
    /// <summary>
    /// Query to precalculate "Fasta" service cost
    /// </summary>
    public class GetFastaPrecalculatedCostQuery : IRequest<ManagerResult<FastaPrecalculatedCostDto>>
    {
        /// <summary>
        /// Total pages amount
        /// </summary>
        public int? PagesAmount { get; set; }

        /// <summary>
        /// Total years amount
        /// </summary>
        public int? YearsAmount { get; set; }
    }
}
