﻿using System.ComponentModel.DataAnnotations;

namespace Core42.Application.Features.FastaContext.Queries.Base
{
    /// <summary>
    /// Base query for "Fasta" service
    /// </summary>
    public class FastaBaseQuery(Guid accountId)
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; } = accountId;
    }
}
