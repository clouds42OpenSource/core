﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.FastaContext.Queries
{
    /// <summary>
    /// Query for the Fasta program
    /// </summary>
    public class GetFastaProgramQuery : IRequest<FileContentResult>
    {
    }
}
