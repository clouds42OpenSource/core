﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Core42.Application.Features.FastaContext.Queries.Base;
using MediatR;

namespace Core42.Application.Features.FastaContext.Queries
{
    /// <summary>
    /// Query to get info about "Fasta" service
    /// </summary>
    public class GetFastaInfoQuery(Guid accountId)
        : FastaBaseQuery(accountId), IRequest<ManagerResult<ServiceFastaViewModelDto>>;
}
