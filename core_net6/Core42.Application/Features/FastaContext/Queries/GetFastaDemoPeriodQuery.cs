﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service;
using Core42.Application.Features.FastaContext.Queries.Base;
using MediatR;

namespace Core42.Application.Features.FastaContext.Queries
{
    /// <summary>
    /// Query to get the Fasta service expire date
    /// </summary>
    public class GetFastaDemoPeriodQuery(Guid accountId)
        : FastaBaseQuery(accountId), IRequest<ManagerResult<ManageFastaDemoPeriodDto>>;
}
