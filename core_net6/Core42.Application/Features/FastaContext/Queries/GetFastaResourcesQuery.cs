﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Core42.Application.Features.FastaContext.Queries.Base;
using MediatR;

namespace Core42.Application.Features.FastaContext.Queries
{
    /// <summary>
    /// Query to get "Fasta" service resources info
    /// </summary>
    public class GetFastaResourcesQuery(Guid accountId)
        : FastaBaseQuery(accountId), IRequest<ManagerResult<Recognition42LicensesModelDto>>;
}
