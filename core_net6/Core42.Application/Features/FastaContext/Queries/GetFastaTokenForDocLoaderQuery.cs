﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.FastaContext.Queries
{
    /// <summary>
    /// Query to get the DocLoader session token for the account user
    /// </summary>
    public class GetFastaTokenForDocLoaderQuery(Guid accountUserId) : IRequest<ManagerResult<Guid>>
    {
        /// <summary>
        /// Account user identifier
        /// </summary>
        [Required]
        public Guid AccountUserId { get; set; } = accountUserId;
    }
}
