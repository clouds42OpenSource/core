﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Core42.Application.Features.FastaContext.Helpers
{
    public class FastaPagesHelper(
        IUnitOfWork unitOfWork,
        AccountCsResourceValuesDataManager accountCsResourceValuesDataManager,
        ICheckAccountDataProvider checkAccountDataProvider,
        IHandlerException handlerException)
    {
        private const string RecognitionName = "Recognition42LicenseResourceId";

        /// <summary>
        /// Get info about pages for "Fasta" service
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        public async Task<Recognition42LicensesModelDto> GetPagesInfo(Guid accountId)
        {
            try
            {
                var pagesResourceId = ConfigurationHelper.GetConfigurationValue<Guid>(RecognitionName);

                var esdlResConf = await unitOfWork.ResourceConfigurationRepository.AsQueryable().FirstOrDefaultAsync(conf =>
                    conf.AccountId == accountId && conf.BillingService.SystemService == Clouds42Service.Esdl)
                    ?? throw new NotFoundException($"Ресурс {Clouds42Service.Esdl} для аккаунта {accountId} не найден");

                if (!esdlResConf.ExpireDate.HasValue)
                    throw new NotFoundException($"Ресурс {Clouds42Service.Esdl} для аккаунта {accountId} не имеет даты окончания действия");

                return new Recognition42LicensesModelDto
                {
                    AccountId = accountId,
                    PagesResourceId = pagesResourceId,
                    ExpireDate = esdlResConf.ExpireDate.Value,
                    PagesLeft = (await accountCsResourceValuesDataManager.GetValueAsync(accountId, pagesResourceId)).Result,
                    CurrentExpireDate = esdlResConf.ExpireDate.Value,
                    IsDemoPeriod = checkAccountDataProvider.CheckAccountIsDemo(accountId),
                    IsFrozenEsdl = esdlResConf.FrozenValue,
                    IsFrozenRec42 = (await unitOfWork.ResourceConfigurationRepository.AsQueryable()
                        .FirstOrDefaultAsync(conf => conf.AccountId == accountId && conf.BillingService.SystemService == Clouds42Service.Recognition))
                        ?.FrozenValue ?? false
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения информации о страницах для сервиса \"Fasta\" для аккаунта] {accountId}.");
                throw;
            }
        }
    }
}
