﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.FastaContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.FastaContext.Handlers
{
    /// <summary>
    /// Handler to get the DocLoader session token for the account user
    /// </summary>
    public class GetFastaTokenForDocLoaderQueryHandler(
        IUnitOfWork unitOfWork,
        IAccountUserSessionProvider accountUserSessionProvider)
        : IRequestHandler<GetFastaTokenForDocLoaderQuery, ManagerResult<Guid>>
    {
        /// <summary>
        /// Handle request to get the DocLoader session token for the account user
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<Guid>> Handle(GetFastaTokenForDocLoaderQuery request, CancellationToken cancellationToken)
        {
            try
            {
                Guid token = (await unitOfWork.AccountUserSessionsRepository.AsQueryable().OrderByDescending(s => s.TokenCreationTime)
                .FirstOrDefaultAsync(s=> s.AccountUserId == request.AccountUserId, cancellationToken))?.Token
                ?? accountUserSessionProvider.CreateSession(new LoginModelDto{ ClientDescription = "Сессия для ЗД" }, request.AccountUserId);

                return token.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<Guid>(ex.Message);
            }
        }
    }
}
