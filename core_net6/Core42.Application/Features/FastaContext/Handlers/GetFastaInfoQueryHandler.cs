﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Features.FastaContext.Helpers;
using Core42.Application.Features.FastaContext.Queries;
using MediatR;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.FastaContext.Handlers
{
    /// <summary>
    /// Handler to get info about "Fasta" service
    /// </summary>
    public class GetFastaInfoQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        FastaPagesHelper fastaPagesHelper,
        IConfiguration configuration,
        IResourcesService resourcesService,
        IServiceStatusHelper serviceStatusHelper)
        : IRequestHandler<GetFastaInfoQuery, ManagerResult<ServiceFastaViewModelDto>>
    {
        /// <summary>
        /// Handle request for getting info about "Fasta" service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<ServiceFastaViewModelDto>> Handle(GetFastaInfoQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ServiceMenu_Esdl, () => request.AccountId);

                if (await unitOfWork.ServiceAccountRepository.AsQueryable()
                    .FirstOrDefaultAsync(w => w.Id == request.AccountId, cancellationToken) != null)
                    throw new InvalidOperationException("Служебным аккаунтам недоступно управление сервисами");

                var pages = await fastaPagesHelper.GetPagesInfo(request.AccountId);

                var recRates = unitOfWork.RateRepository.AsQueryable()
                    .Where(r =>
                        r.BillingServiceType.Service.SystemService == Clouds42Service.Recognition &&
                        r.BillingServiceType.SystemServiceType.HasValue)
                    .Select(r => r.BillingServiceType.SystemServiceType).OrderBy(r => r);

                var tariffList = recRates.AsEnumerable().Select(rec => rec.GetDataValue()).ToList();

                var currentType = await unitOfWork.ResourceRepository.AsQueryable().FirstOrDefaultAsync(
                        r => r.AccountId == request.AccountId && r.BillingServiceType.Service.SystemService == Clouds42Service.Recognition,
                        cancellationToken);

                var currentPagesTariff = currentType?.BillingServiceType?.SystemServiceType?.GetDataValue() ??
                    ConfigurationHelper.GetConfigurationValue<int>("Recognition42DemoLicenseCount");

                var locale = (await unitOfWork.AccountConfigurationRepository.AsQueryable().Include(conf => conf.Locale)
                    .FirstOrDefaultAsync(conf => conf.AccountId == request.AccountId, cancellationToken))?.Locale
                    ?? await unitOfWork.LocaleRepository.AsQueryable().FirstOrDefaultAsync(loc => loc.ID == Guid.Parse(configuration["DefaultLocale"]),
                    cancellationToken);

                var esdlResConf = await unitOfWork.ResourceConfigurationRepository.AsQueryable().FirstOrDefaultAsync(
                    conf => conf.AccountId == request.AccountId && conf.BillingService.SystemService == Clouds42Service.Esdl,
                    cancellationToken);

                var model = new ServiceFastaViewModelDto
                {
                    ServiceNameRepresentation = ServiceNameRepresentationDto.GetService(Clouds42Service.Esdl),
                    DefaultPagesTariff = tariffList.FirstOrDefault(),
                    PagesRemain = pages.PagesLeft,
                    ExpireDate = pages.CurrentExpireDate,
                    IsFrozen = pages.IsFrozenEsdl || pages.IsFrozenRec42,
                    PagesTariffList = tariffList,
                    AllowIncreaseDocLoaderByStandardRent1C = await CheckAllow(request.AccountId),
                    Rent1CExpiredDate =  (await unitOfWork.ResourceConfigurationRepository.AsQueryable().FirstOrDefaultAsync(rc =>
                        rc.AccountId == request.AccountId && rc.BillingService.SystemService == Clouds42Service.MyEnterprise, cancellationToken))?.ExpireDate,
                    CurrentPagesTariff = currentPagesTariff,
                    AccountLevel = resourcesService.GetAccountIfExistsOrCreateNew(request.AccountId).AccountTypeEnum,
                    Locale = new LocaleDto
                    {
                        Name = locale.Name,
                        Currency = locale.Currency
                    },
                    ServiceStatus = serviceStatusHelper.CreateServiceStatusModel(request.AccountId, esdlResConf) 
                };

                if (!model.ServiceStatus.ServiceIsLocked)
                    model.ServiceStatus.ServiceIsLocked = model.IsEsdlLocked;

                return model.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения модели сервиса Fasta для аккаунта] {request.AccountId}");
                return PagedListExtensions.ToPreconditionFailedManagerResult<ServiceFastaViewModelDto>(ex.Message);
            }
        }

        /// <summary>
        /// Checking the possibility of free accrual of ZD traffic
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        private async Task<bool> CheckAllow(Guid accountId)
        {
            var resConfig = await  unitOfWork.ResourceConfigurationRepository.AsQueryable().FirstOrDefaultAsync(rc =>
                rc.AccountId == accountId && rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var hasActiveStandardResources = await unitOfWork.ResourceRepository.AsQueryable()
                .AnyAsync(r => r.AccountId == accountId && r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);

            if (resConfig == null || resConfig.Frozen == true || !hasActiveStandardResources)
                return false;

            return await unitOfWork.PaymentRepository.AsQueryable().AnyAsync(
                p => p.AccountId == accountId && p.Status == PaymentStatus.Done.ToString() && p.OperationType == PaymentType.Inflow.ToString() && p.Sum > 0);
        }
    }
}
