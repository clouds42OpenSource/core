﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using CommonLib.Enums;
using Core42.Application.Features.FastaContext.Helpers;
using Core42.Application.Features.FastaContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.FastaContext.Handlers
{
    /// <summary>
    /// Handler to get info on the Fasta service resources
    /// </summary>
    public class GetFastaResourcesQueryHandler(
        IAccessProvider accessProvider,
        FastaPagesHelper fastaPagesHelper,
        IHandlerException handlerException)
        : IRequestHandler<GetFastaResourcesQuery, ManagerResult<Recognition42LicensesModelDto>>
    {
        /// <summary>
        /// Handle a query to get info on the Fasta service resources
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<Recognition42LicensesModelDto>> Handle(GetFastaResourcesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_Fasta_ChangeDemoPeriod, () => request.AccountId);
                return (await fastaPagesHelper.GetPagesInfo(request.AccountId)).ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Во время получения информации о ресурсах сервиса {ResourceType.Esdl} произошла ошибка] " +
                    $"Аккаунт {request.AccountId}");
                return PagedListExtensions.ToPreconditionFailedManagerResult<Recognition42LicensesModelDto>(ex.Message);
            }
        }
    }
}
