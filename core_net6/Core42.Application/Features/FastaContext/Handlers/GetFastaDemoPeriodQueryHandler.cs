﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.FastaContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.FastaContext.Handlers
{
    /// <summary>
    /// Handler for request to get the expire date of the Fasta service
    /// </summary>
    public class GetFastaDemoPeriodQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : IRequestHandler<GetFastaDemoPeriodQuery, ManagerResult<ManageFastaDemoPeriodDto>>
    {
        private const Clouds42Service FastaSystemServiceType = Clouds42Service.Esdl;

        /// <summary>
        /// Handle a request to get the expire date of the Fasta service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<ManageFastaDemoPeriodDto>> Handle(GetFastaDemoPeriodQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var account = await unitOfWork.AccountsRepository.AsQueryable()
                    .FirstOrDefaultAsync(acc => acc.Id == request.AccountId, cancellationToken)
                    ?? throw new NotFoundException($"Аккаунта с идентификатором {request.AccountId} не существует");

                accessProvider.HasAccess(ObjectAction.ControlPanel_Fasta_ChangeDemoPeriod, () => request.AccountId);

                var demoExpireDate = (await unitOfWork.ResourceConfigurationRepository.AsQueryable()
                    .Include(conf => conf.BillingService)
                    .FirstOrDefaultAsync(conf => conf.AccountId == request.AccountId
                        && conf.BillingService.SystemService == FastaSystemServiceType, cancellationToken))?.ExpireDateValue
                    ?? throw new NotFoundException($"У аккаунта отсутствует конфигурация ресурса для сервиса {FastaSystemServiceType}");

                var maxDemoExpireDate = (await accessProvider.GetUserAsync())?.Groups?.Contains(AccountUserGroup.CloudAdmin) == true ?
                        DateTime.MaxValue : account.RegistrationDate?.AddMonths(1);

                return new ManageFastaDemoPeriodDto
                {
                    AccountId = request.AccountId,
                    DemoExpiredDate = demoExpireDate,
                    MaxDemoExpiredDate = maxDemoExpireDate
                }.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения модели управления демо периодом сервиса {FastaSystemServiceType}]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<ManageFastaDemoPeriodDto>(ex.Message);
            }
        }
    }
}
