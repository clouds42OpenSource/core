﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Features.FastaContext.Dtos;
using Core42.Application.Features.FastaContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.FastaContext.Handlers
{
    /// <summary>
    /// Hanlder to get precalculated cost of the Fasta service
    /// </summary>
    public class FastaPrecalculatedCostQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IResourcesService resourcesService,
        IRateProvider rateProvider,
        ILogger42 logger)
        : IRequestHandler<GetFastaPrecalculatedCostQuery, ManagerResult<FastaPrecalculatedCostDto>>
    {
        /// <summary>
        /// Handle a query to get precalculated cost of the Fasta service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<FastaPrecalculatedCostDto>> Handle(GetFastaPrecalculatedCostQuery request, CancellationToken cancellationToken)
        {
            try
            {
                decimal recognitionCost = 0m, fastaCost = 0m;

                var userPrincipal = await accessProvider.GetUserAsync();
                accessProvider.HasAccess(ObjectAction.ControlPanel_ServiceMenu_Esdl, () => userPrincipal.ContextAccountId);

                var billingAccount = resourcesService.GetAccountIfExistsOrCreateNew(userPrincipal.ContextAccountId);

                if (request.YearsAmount.HasValue)
                {
                    var serviceType = await unitOfWork.BillingServiceTypeRepository.AsQueryable()
                        .FirstOrDefaultAsync(x => x.SystemServiceType == ResourceType.Esdl, cancellationToken);
                    if (serviceType != null)
                    {
                        fastaCost = rateProvider.GetOptimalRate(billingAccount.Id, serviceType.Id).Cost *
                                    request.YearsAmount.Value;
                    }
                }

                if (!request.PagesAmount.HasValue)
                {
                    return new FastaPrecalculatedCostDto(fastaCost, recognitionCost).ToOkManagerResult();
                }
                else
                {
                    if (Enum.TryParse($"Rec{request.PagesAmount.Value}", out ResourceType resType))
                    {
                        var serviceType = await unitOfWork.BillingServiceTypeRepository.AsQueryable()
                            .FirstOrDefaultAsync(x => x.SystemServiceType == resType, cancellationToken);
                        recognitionCost = rateProvider.GetOptimalRate(billingAccount.Id, serviceType.Id).Cost;
                    }
                    else
                    {
                        logger.Warn($"Для сервиса Recognition42 отсутствует тариф для указанного количества страниц ({request.PagesAmount.Value})");
                    }
                }
                return new FastaPrecalculatedCostDto(fastaCost, recognitionCost).ToOkManagerResult();
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"Во время предварительного расчета стоимости тарифа сервиса {ResourceType.Esdl} для аккаунта " +
                    $"{accessProvider.ContextAccountId} произошла ошибка");
                return PagedListExtensions.ToPreconditionFailedManagerResult<FastaPrecalculatedCostDto>(ex.Message);
            }
        }
    }
}
