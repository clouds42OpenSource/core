﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.FastaContext.Queries;
using MediatR;


namespace Core42.Application.Features.FastaContext.Handlers
{
    /// <summary>
    /// Handler to get Fasta program
    /// </summary>
    public class GetFastaProgramQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ILogger42 logger)
        : IRequestHandler<GetFastaProgramQuery, FileContentResult>
    {
        private readonly string _docLoaderFullPath = Clouds42.Configurations.Configurations.CloudConfigurationProvider.Files.PathToDocLoader();

        /// <summary>
        /// Handle a query to get Fasta program
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<FileContentResult> Handle(GetFastaProgramQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var userPrincipal = await accessProvider.GetUserAsync();

                logger.Info($"download event DocLoader.zip Account: {userPrincipal.RequestAccountId}, UserLogin: {userPrincipal.Name}," +
                    $"service: {Clouds42Service.Esdl}");

                var fileName = Path.GetFileName(_docLoaderFullPath);
                var content = await File.ReadAllBytesAsync(_docLoaderFullPath, cancellationToken);
                new FileExtensionContentTypeProvider().TryGetContentType(fileName, out string? contentType);

                return new FileContentResult(content, contentType) { FileDownloadName = fileName };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Не удалось извлечь файл программы Fasta]");
                throw;
            }
        }
    }
}
