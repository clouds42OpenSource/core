﻿namespace Core42.Application.Features.FastaContext.Dtos
{
    /// <summary>
    /// Precalculated cost of "Fasta" service
    /// </summary>
    public class FastaPrecalculatedCostDto(decimal fastaCost, decimal recognitionCost)
    {
        /// <summary>
        /// "Fasta" service cost
        /// </summary>
        public decimal FastaCost { get; set; } = fastaCost;

        /// <summary>
        /// "Recognition" service cost
        /// </summary>
        public decimal RecognitionCost { get; set; } = recognitionCost;

        /// <summary>
        /// Total cost
        /// </summary>
        public decimal TotalCost { get => FastaCost + RecognitionCost; }
    }
}
