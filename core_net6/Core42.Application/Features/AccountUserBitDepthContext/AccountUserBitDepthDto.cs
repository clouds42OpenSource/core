﻿using Clouds42.Domain.DataModels;

namespace Core42.Application.Features.AccountUserBitDepthContext;

public class AccountUserBitDepthDto
{
    public Guid AccountUserId { get; set; }
    public BitDepth BitDepth { get; set; }
}
