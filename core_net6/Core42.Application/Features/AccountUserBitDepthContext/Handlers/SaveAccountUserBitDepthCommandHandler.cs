﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUserBitDepthContext.Handlers;

public class SaveAccountUserBitDepthCommandHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    ILogger42 logger42)
    : IRequestHandler<SaveAccountUserBitDepthCommand, ManagerResult<bool>>
{
    public async Task<ManagerResult<bool>> Handle(SaveAccountUserBitDepthCommand request, CancellationToken cancellationToken)
    {
        try 
        {
            logger42.Info($"Начинаю сохранение разрядности пользователя {request.AccountUserId}");
            var bitDepthValue =  await  unitOfWork.AccountUserBitDepthRepository
                .FirstOrDefaultAsync(d => d.AccountUserId == request.AccountUserId && d.AccountDatabaseId == request.AccountDatabaseId);
            if (bitDepthValue == null)
            {
                logger42.Info($"Создаю запись для сохранения разрядности для пользователя {request.AccountUserId}" );
                bitDepthValue = new AccountUserBitDepth {AccountUserId = request.AccountUserId, AccountDatabaseId = request.AccountDatabaseId, BitDepth = request.BitDepth, Id = Guid.NewGuid()};
                unitOfWork.AccountUserBitDepthRepository.Insert(bitDepthValue);
                await unitOfWork.SaveAsync();
                return true.ToOkManagerResult();
            }
            bitDepthValue.BitDepth = request.BitDepth;
            unitOfWork.AccountUserBitDepthRepository.Update(bitDepthValue);
            await unitOfWork.SaveAsync();
            return true.ToOkManagerResult();
        }
        catch (Exception ex)
        {
          var message = "Ошибка сохранения или создания записи о разрядности запуска пользователя";
          handlerException.Handle(ex, message + ex.Message);
          return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(message);
        }
    }
}
