﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using MediatR;

namespace Core42.Application.Features.AccountUserBitDepthContext;

public class SaveAccountUserBitDepthCommand : IRequest<ManagerResult<bool>>
{
    public Guid AccountUserId { get; set; }

    public Guid AccountDatabaseId { get; set; }

    public ApplicationBitDepthEnum BitDepth { get; set; }
}
