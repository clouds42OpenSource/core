﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Handlers
{
    /// <summary>
    /// Get workers for auto update query handler
    /// </summary>
    public class GetWorkersForAutoUpdateQueryHandler(
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetWorkersForAutoUpdateQuery, ManagerResult<List<short>>>
    {
        /// <summary>
        /// Handle get workers for auto update query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<short>>> Handle(GetWorkersForAutoUpdateQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AccountDatabase_GetAutoUpdateData);
                var coreWorkerTaskId = Clouds42.Configurations.Configurations.CloudConfigurationProvider.AutoUpdateDatabase.GetCoreWorkerTaskId();

                return (await unitOfWork
                    .GetGenericRepository<CoreWorkerAvailableTasksBag>().AsQueryable()
                    .AsQueryable()
                    .Where(x => x.CoreWorkerTaskId == coreWorkerTaskId)
                    .Select(x => x.CoreWorkerId)
                    .Distinct()
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения воркеров которые выполняют авто обновление]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<List<short>>(ex.Message);
            }

        }
    }
}
