﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries;
using Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Handlers
{
    public class DatabaseInAutoUpdateQueueQueryHandler(IAccessProvider accessProvider, IHandlerException handlerException, IUnitOfWork unitOfWork): IRequestHandler<DatabaseInAutoUpdateQueueQuery, ManagerResult<PagedDto<AutoUpdateAccountDatabaseDto>>>
    {
        public async Task<ManagerResult<PagedDto<AutoUpdateAccountDatabaseDto>>> Handle(DatabaseInAutoUpdateQueueQuery query, CancellationToken cancellationToken)
        {
            try
            {
                return query.AutoUpdateState switch
                {
                    AutoUpdateState.Queued => await HandleQueuedState(query, cancellationToken),
                    AutoUpdateState.Connected => await HandleConnectedState(query, cancellationToken),
                    AutoUpdateState.Performed => await HandlePerformedState(query, cancellationToken),
                    AutoUpdateState.SoloUpdate => await HandleSoloUpdateState(query, cancellationToken),
                    _ => throw new ArgumentOutOfRangeException()
                };
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения инф. баз, которые в очереди на АО]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AutoUpdateAccountDatabaseDto>>(ex.Message);
            }
        }

        private IQueryable<AutoUpdateAccountDatabaseDto> GetAutoUpdateDatabasesQueryable(bool needDbWithProcessingSupport, bool needPreparedForUpdate)
        {
            var v83PlatformType = PlatformType.V83.GetDisplayName();

            return unitOfWork.AcDbSupportRepository
                .AsQueryableNoTracking()
                .Where(x => x.HasAutoUpdate &&
                            x.State == (int)DatabaseSupportOperation.PlanAutoUpdate &&
                            x.AccountDatabase.Account.BillingAccount.ResourcesConfigurations.Any(y =>
                                y.BillingService.SystemService == Clouds42Service.MyEnterprise))
                .Where(AutoUpdateDatabasesSpecification.ByNeedDbWithProcessingSupport(needDbWithProcessingSupport) &
                    AutoUpdateDatabasesSpecification.ByNeedPreparedForUpdate(needPreparedForUpdate))
                .Select(x => new AutoUpdateAccountDatabaseDto
                {
                    
                    Id = x.AccountDatabasesID,
                    V82Name = x.AccountDatabase.V82Name,
                    Caption = x.AccountDatabase.Caption,
                    CurrentVersion = x.CurrentVersion == "" ? null : x.CurrentVersion,
                    CurrentVersionBuild = x.Build,
                    CurrentVersionMajor = x.Major,
                    CurrentVersionMinor = x.Minor,
                    CurrentVersionPatch = x.Patch,
                    PlatformVersion =
                        x.AccountDatabase.DistributionType == DistributionType.Alpha.ToString()
                            ? x.AccountDatabase.Account.AccountConfiguration.Segment.Alpha83Version
                            : (x.AccountDatabase.ApplicationName == v83PlatformType
                                ? x.AccountDatabase.Account.AccountConfiguration.Segment.Stable83Version
                                : x.AccountDatabase.Account.AccountConfiguration.Segment.Stable82Version),
                    ConfigurationName = x.ConfigurationName ?? x.SynonymConfiguration,
                    ActualVersion = x.AccountDatabase.AcDbSupport.Configurations1C.ConfigurationsCfus
                                .OrderByDescending(y => y.DownloadDate)
                                .FirstOrDefault(y =>
                                    y.ConfigurationName == (x.ConfigurationName ?? x.SynonymConfiguration))
                                .Version,
                    ActualVersionBuild = x.AccountDatabase.AcDbSupport.Configurations1C.ConfigurationsCfus
                            .OrderByDescending(y => y.DownloadDate)
                            .FirstOrDefault(y =>
                                y.ConfigurationName == (x.ConfigurationName ?? x.SynonymConfiguration))
                            .Build,
                    ActualVersionMajor = x.AccountDatabase.AcDbSupport.Configurations1C.ConfigurationsCfus
                            .OrderByDescending(y => y.DownloadDate)
                            .FirstOrDefault(y =>
                                y.ConfigurationName == (x.ConfigurationName ?? x.SynonymConfiguration))
                            .Major,
                    ActualVersionMinor = x.AccountDatabase.AcDbSupport.Configurations1C.ConfigurationsCfus
                            .OrderByDescending(y => y.DownloadDate)
                            .FirstOrDefault(y =>
                                y.ConfigurationName == (x.ConfigurationName ?? x.SynonymConfiguration))
                            .Minor,
                    ActualVersionPatch = x.AccountDatabase.AcDbSupport.Configurations1C.ConfigurationsCfus
                        .OrderByDescending(y => y.DownloadDate)
                        .FirstOrDefault(y =>
                            y.ConfigurationName == (x.ConfigurationName ?? x.SynonymConfiguration))
                        .Patch,
                    IsActiveRent1COnly =
                        x.AccountDatabase.Account.BillingAccount.ResourcesConfigurations.Any(y =>
                            (!y.Frozen.HasValue || !y.Frozen.Value) &&
                            y.BillingService.SystemService == Clouds42Service.MyEnterprise),
                    IsVipAccountsOnly = x.AccountDatabase.Account.AccountConfiguration.IsVip,
                    ConnectDate = x.ConnectDate,
                    AccountId = x.AccountDatabase.AccountId,
                    LastSuccessAuDate = x.AcDbSupportHistories.Any(y =>
                        y.State == SupportHistoryState.Success && y.Operation == DatabaseSupportOperation.AutoUpdate)
                        ? x.AcDbSupportHistories
                            .OrderByDescending(y => y.EditDate)
                            .FirstOrDefault(y =>
                                y.State == SupportHistoryState.Success &&
                                y.Operation == DatabaseSupportOperation.AutoUpdate)!.EditDate
                        : null
                });
        }


        private async Task<ManagerResult<PagedDto<AutoUpdateAccountDatabaseDto>>> HandleQueuedState(
            DatabaseInAutoUpdateQueueQuery query, CancellationToken cancellationToken)
        {
            query.OrderBy ??= $"{nameof(AutoUpdateAccountDatabaseDto.AddedDate)}.desc";

            var dataQuery = ApplyCommonFilters(GetAutoUpdateDatabasesQueryable(true, true), query.Filter);

            var maxDateQuery = unitOfWork.AcDbSupportHistoryRepository
                .AsQueryable()
                .GroupBy(h => h.AcDbSupport.AccountDatabase.V82Name)
                .Select(g => new
                {
                    V82Name = g.Key,
                    MaxEditDate = g.Max(h => h.EditDate)
                });

            return (await dataQuery
                    .Join(maxDateQuery,
                        dto => dto.V82Name,
                        max => max.V82Name,
                        (dto, max) => new { dto, max })
                    .Select(x => new AutoUpdateAccountDatabaseDto
                    {
                        Id = x.dto.Id,
                        AddedDate = x.max.MaxEditDate,
                        AccountId = x.dto.Id,
                        ActualVersion = x.dto.ActualVersion,
                        Caption = x.dto.Caption,
                        ConfigurationName = x.dto.ConfigurationName,
                        ConnectDate = x.dto.ConnectDate,
                        CurrentVersion = x.dto.CurrentVersion,
                        IsActiveRent1COnly = x.dto.IsActiveRent1COnly,
                        IsProcessingAutoUpdate = false,
                        LastSuccessAuDate = x.dto.LastSuccessAuDate,
                        PlatformVersion = x.dto.PlatformVersion,
                        IsVipAccountsOnly = x.dto.IsVipAccountsOnly,
                        V82Name = x.dto.V82Name
                    })
                    .AutoFilter(query.Filter)
                    .AutoSort(query)
                    .ToPagedListAsync(query.PageNumber ?? 1, query.PageSize ?? 100, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult()!;
        }

        private async Task<ManagerResult<PagedDto<AutoUpdateAccountDatabaseDto>>> HandlePerformedState(
            DatabaseInAutoUpdateQueueQuery query, CancellationToken cancellationToken)
        {
            query.OrderBy ??= $"{nameof(AutoUpdateAccountDatabaseDto.AutoUpdateStartDate)}.desc";

            return (await unitOfWork.AcDbSupportHistoryRepository
                    .AsQueryableNoTracking()
                    .Where(AutoUpdateDatabasesSpecification.InAutoUpdateStatus() &
                           AutoUpdateDatabasesSpecification.ByDate(query.Filter.AutoUpdatePeriodFrom, query.Filter.AutoUpdatePeriodTo) &
                           AutoUpdateDatabasesSpecification.BySupportSearchLine(query.Filter.SearchLine) &
                           AutoUpdateDatabasesSpecification.BySupportConfigurationNames(query.Filter
                               .Configurations))
                    .Select(x => new AutoUpdateAccountDatabaseDto
                    {
                        Id = x.AcDbSupport.AccountDatabase.Id,
                        V82Name = x.AcDbSupport.AccountDatabase.V82Name,
                        Caption = x.AcDbSupport.AccountDatabase.Caption,
                        ConfigurationName =
                            x.AcDbSupport.ConfigurationName ?? x.AcDbSupport.SynonymConfiguration,
                        Status = x.State,
                        Description = x.Description,
                        AutoUpdateStartDate = x.EditDate,
                        IsVipAccountsOnly = x.AcDbSupport.AccountDatabase.Account.AccountConfiguration.IsVip
                    })
                    .AutoFilter(query!.Filter)
                    .AutoSort(query)
                    .ToPagedListAsync(query.PageNumber ?? 1, query.PageSize ?? 100, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        private async Task<ManagerResult<PagedDto<AutoUpdateAccountDatabaseDto>>> HandleConnectedState(
            DatabaseInAutoUpdateQueueQuery query, CancellationToken cancellationToken)
        {

            query.OrderBy ??= $"{nameof(AutoUpdateAccountDatabaseDto.ConnectDate)}.desc";

            var dataQuery = ApplyCommonFilters(GetAutoUpdateDatabasesQueryable(false, false), query.Filter);
            var dataQuery2 = ApplyCommonFilters(GetAutoUpdateDatabasesQueryable(true, true), query.Filter);

            var records = await dataQuery.Union(dataQuery2)
                .Where(AutoUpdateDatabasesSpecification.ByVersionCompare(query?.Filter?.NeedAutoUpdateOnly))
                .AutoFilter(query!.Filter)
                .AutoSort(query)
                .ToPagedListAsync(query.PageNumber ?? 1, query.PageSize ?? 100, cancellationToken);

            return records
                .ToPagedDto()
                .ToOkManagerResult();
        }

        private async Task<ManagerResult<PagedDto<AutoUpdateAccountDatabaseDto>>> HandleSoloUpdateState(
            DatabaseInAutoUpdateQueueQuery query, CancellationToken cancellationToken)
        {
            return (await unitOfWork.UpdateNodeQueueRepository
                    .AsQueryable()
                    .Where(x => !query.Filter.IsVipAccountsOnly.HasValue || x.AccountDatabase.Account.AccountConfiguration.IsVip)
                    .OrderBy(x => x.AddDate)
                    .Select(x => new AutoUpdateAccountDatabaseDto { Name = x.AccountDatabase.V82Name, AddDate = x.AddDate })
                    .ToPagedListAsync(query.PageNumber ?? 1, query.PageSize ?? 100, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }


        /// <summary>
        /// Применяет общие фильтры к запросу на основе переданного фильтра.
        /// </summary>
        /// <param name="query">Исходный запрос.</param>
        /// <param name="filter">Фильтры для применения.</param>
        /// <returns>Запрос с примененными фильтрами.</returns>
        private static IQueryable<AutoUpdateAccountDatabaseDto> ApplyCommonFilters(IQueryable<AutoUpdateAccountDatabaseDto> query, DatabaseInAutoUpdateQueryFilter? filter)
        {
            if (filter == null) return query;

            return query
                .Where(AutoUpdateDatabasesSpecification.BySearchLine(filter.SearchLine))
                .Where(AutoUpdateDatabasesSpecification.ByConfigurationNames(filter.Configurations));
        }
    }
}
