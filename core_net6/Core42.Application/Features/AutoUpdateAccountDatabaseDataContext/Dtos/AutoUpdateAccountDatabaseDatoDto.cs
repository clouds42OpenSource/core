﻿namespace Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Dtos
{
    /// <summary>
    /// Auto update account database data dto
    /// </summary>
    public class AutoUpdateAccountDatabaseDatoDto
    {
        /// <summary>
        /// Database added to queue autoupdate date time
        /// </summary>
        public DateTime AddedDate { get; set; }

        /// <summary>
        /// Is processing auto update
        /// </summary>
        public bool IsProcessingAutoUpdate { get; set; }

        /// <summary>
        /// Database identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Database V82 name
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Database caption
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Configuration name
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Current release version
        /// </summary>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Actual release version
        /// </summary>
        public string ActualVersion { get; set; }

        /// <summary>
        /// Platform version
        /// </summary>
        public string PlatformVersion { get; set; }

        /// <summary>
        /// Connection date
        /// </summary>
        public DateTime? ConnectDate { get; set; }

        /// <summary>
        /// Last success auto update date
        /// </summary>
        public DateTime? LastSuccessAuDate { get; set; }
    }
}
