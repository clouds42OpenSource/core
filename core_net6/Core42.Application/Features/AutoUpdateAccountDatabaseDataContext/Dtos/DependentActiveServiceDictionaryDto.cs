﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Dtos
{
    /// <summary>
    /// Dependent active service as key-value dto
    /// </summary>
    public class DependentActiveServiceDictionaryDto : DictionaryDto<ResourceType, List<string>>, IMapFrom<Resource>
    {
        public List<Guid> DependServiceIds;
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Resource, DependentActiveServiceDictionaryDto>()
                .ForMember(x => x.Key, z => z.MapFrom(y => y.BillingServiceType.DependServiceType.SystemServiceType))
                .ForMember(x => x.Value, z => z.MapFrom(y => y.BillingServiceType.Service.Name));
        }
    }
}
