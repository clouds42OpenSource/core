﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.BaseModel;

namespace Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Queries
{
    /// <summary>
    /// Get configuration 1C auto update query
    /// </summary>
    public class GetConfiguration1CAutoUpdateQuery : IHasFilter<AutoUpdateConfiguration1CDetailsFilterDto>
    {
        /// <summary>
        /// Auto update configuration 1C filter
        /// </summary>
        public AutoUpdateConfiguration1CDetailsFilterDto? Filter { get; set; }
    }
}
