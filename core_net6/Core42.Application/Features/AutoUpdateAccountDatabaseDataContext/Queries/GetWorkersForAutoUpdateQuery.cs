﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Queries
{
    /// <summary>
    /// Get worker for auto update query
    /// </summary>
    public class GetWorkersForAutoUpdateQuery : IRequest<ManagerResult<List<short>>>
    {
    }
}
