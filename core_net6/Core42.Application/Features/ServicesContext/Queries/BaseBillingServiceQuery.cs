﻿using System.ComponentModel.DataAnnotations;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Base query for billing services queries
    /// </summary>
    public class BaseBillingServiceQuery
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        [Required]
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingServiceId">Service identifier</param>
        public BaseBillingServiceQuery(Guid billingServiceId)
        {
            BillingServiceId = billingServiceId;
        }
    }
}
