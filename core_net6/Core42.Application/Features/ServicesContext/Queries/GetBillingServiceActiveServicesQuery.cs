﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get all active services for the billing services
    /// </summary>
    public class GetBillingServiceActiveServicesQuery : BaseBillingServiceQuery, IRequest<ManagerResult<List<Guid>>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingServiceId">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        public GetBillingServiceActiveServicesQuery(Guid billingServiceId, Guid accountId)
            : base(billingServiceId)
        {
            AccountId = accountId;
        }
    }
}
