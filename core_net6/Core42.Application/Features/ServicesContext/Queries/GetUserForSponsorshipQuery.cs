﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using MediatR;
using Clouds42.DataContracts.BillingService.BillingServiceGet;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get a user for sponsorship for a billing service
    /// </summary>
    public class GetUserForSponsorshipQuery : IRequest<ManagerResult<AccountUserBillingServiceTypeDto>>
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        [Required]
        [FromRoute]
        public Guid Id { get; set; }

        /// <summary>
        /// Email of the user
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        /// <summary>
        /// Service identifier of the billing service
        /// </summary>
        public Guid? ServiceId { get; set; }
    }
}
