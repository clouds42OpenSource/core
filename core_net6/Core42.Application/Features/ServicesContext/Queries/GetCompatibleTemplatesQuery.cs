﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using MediatR;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get account database templates compatible with the billing service
    /// </summary>
    public class GetCompatibleTemplatesQuery : BaseBillingServiceQuery, IRequest<ManagerResult<List<AvailableDatabaseForServiceExtensionDto>>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingServiceId">Billing service identifier</param>
        public GetCompatibleTemplatesQuery(Guid billingServiceId) : base(billingServiceId) { }
    }
}
