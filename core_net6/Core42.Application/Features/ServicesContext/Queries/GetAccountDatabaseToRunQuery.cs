﻿
using System.ComponentModel.DataAnnotations;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get account databases to run the billing service
    /// </summary>
    public class GetAccountDatabaseToRunQuery : BaseBillingServiceQuery, IRequest<ManagerResult<AccountDatabaseToRunDto>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingServiceId">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        public GetAccountDatabaseToRunQuery(Guid billingServiceId, Guid accountId)
            : base(billingServiceId)
        {
            AccountId = accountId;
        }
    }
}
