﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ServicesContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get info on the service for the account
    /// </summary>
    public class GetBillingServiceForAccountQuery : BaseBillingServiceQuery, IRequest<ManagerResult<BillingServiceInfoDto>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingServiceId">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        public GetBillingServiceForAccountQuery(Guid billingServiceId, Guid accountId)
            : base(billingServiceId)
        {
            AccountId = accountId;
        }
    }
}
