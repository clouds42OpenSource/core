﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using LinqExtensionsNetFramework;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get filtered, paged and ordered users with services of the billing service
    /// </summary>
    public class GetUsersWithBillingServicesQuery : IRequest<ManagerResult<PagedDto<AccountUserBillingServiceTypeDto>>>, IHasFilter<UsersWithBillingServicesFilter>,
        ISortedQuery, IPagedQuery
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        [Required]
        [FromRoute]
        public Guid Id { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Sorting field and type
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(AccountUserBillingServiceTypeDto.AccountUserName)}";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        public UsersWithBillingServicesFilter Filter { get; set; }
    }

    public class UsersWithBillingServicesFilter : IQueryFilter
    {
        public string AccountUserName { get; set; }
    }
}
