﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using MediatR;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get info on services for the billing service
    /// </summary>
    public class GetBillingServiceInfoQuery : BaseBillingServiceQuery, IRequest<ManagerResult<List<BillingServiceTypeInfoDto>>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingServiceId">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        public GetBillingServiceInfoQuery(Guid billingServiceId, Guid accountId)
            : base(billingServiceId)
        {
            AccountId = accountId;
        }
    }
}
