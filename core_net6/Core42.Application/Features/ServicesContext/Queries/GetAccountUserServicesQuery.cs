﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner;
using MediatR;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get all account user's services of the billing service
    /// </summary>
    public class GetAccountUserServicesQuery : BaseBillingServiceQuery, IRequest<ManagerResult<ServiceTypeStateForUserDto>>
    {
        /// <summary>
        /// Account user login
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string UserLogin { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userLogin">Account user login</param>
        /// <param name="billingServiceId">Billing service identifier</param>
        public GetAccountUserServicesQuery(Guid billingServiceId, string userLogin)
            : base(billingServiceId)
        {
            UserLogin = userLogin;
        }
    }
}
