﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get an account database on delimiters to run the billing service
    /// </summary>
    public class GetAccountDatabaseOnDelimitersToRunQuery : IRequest<ManagerResult<AccountDatabaseOnDelimitersToRunDto>>
    {
        /// <summary>
        /// Account database on delimiters identifier
        /// </summary>
        [Required]
        public Guid DatabaseId { get; set; }
    }
}
