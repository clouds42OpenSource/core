﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.ServicesContext.Queries
{
    /// <summary>
    /// Query to get service activation info for the account
    /// </summary>
    public class GetBillingServiceActivationQuery : BaseBillingServiceQuery, IRequest<ManagerResult<bool>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="billingServiceId">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        public GetBillingServiceActivationQuery(Guid billingServiceId, Guid accountId)
            : base(billingServiceId)
        {
            AccountId = accountId;
        }
    }
}
