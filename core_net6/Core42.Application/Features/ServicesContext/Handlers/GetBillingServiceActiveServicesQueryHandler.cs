﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get active all services for the billing services
    /// </summary>
    public class GetBillingServiceActiveServicesQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ILogger42 logger,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetBillingServiceActiveServicesQuery, ManagerResult<List<Guid>>>
    {
        /// <summary>
        /// Handle a query to get all active services for the billing services
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<Guid>>> Handle(GetBillingServiceActiveServicesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.BillingServiceTypes_GetInfo, () => request.AccountId);
                logger.Info($"Получение активных услуг сервиса {request.BillingServiceId} для аккаунта {request.AccountId}");

                return (await unitOfWork.ResourceRepository
                        .AsQueryableNoTracking()
                        .Where(res => res.Subject == request.AccountId && res.AccountId == request.AccountId &&
                            res.BillingServiceType.ServiceId == request.BillingServiceId)
                        .Select(res => res.BillingServiceTypeId)
                        .Distinct()
                        .ToListAsync(cancellationToken))
                .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения активных услуг сервиса] {request.BillingServiceId} для аккаунта {request.AccountId}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<Guid>>(ex.Message);
            }
        }
    }
}
