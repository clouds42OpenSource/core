﻿using System.Data;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Dtos;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for query to get the service info 
    /// </summary>
    public class GetServicesForAccountQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ICloudLocalizer cloudLocalizer,
        IGetConfigurationDatabaseCommand getConfigurationDatabaseCommand,
        IAccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        IPartialAmountForApplyDemoServiceSubsribesCalculator partialAmountForApplyDemoServiceSubscribesCalculator,
        IServiceStatusHelper serviceStatusHelper,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetBillingServiceForAccountQuery, ManagerResult<BillingServiceInfoDto>>
    {
        /// <summary>
        /// Handle a query to get the service info 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<BillingServiceInfoDto>> Handle(GetBillingServiceForAccountQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var isServiceAccount = await unitOfWork.ServiceAccountRepository
                    .AsQueryableNoTracking()
                    .AnyAsync(x => x.Id == request.AccountId, cancellationToken);

                if (isServiceAccount) throw new InvalidOperationException("Служебным аккаунтам недоступно управление сервисами");

                accessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => request.AccountId);

                var billingService = await unitOfWork.BillingServiceRepository
                        .AsQueryableNoTracking()
                        .Include(x => x.BillingServiceTypes)
                        .ThenInclude(x => x.DependServiceType)
                        .FirstOrDefaultAsync(x => x.Id == request.BillingServiceId, cancellationToken) 
                                     ?? throw new NotFoundException($"Сервис '{request.BillingServiceId}' не найден!");

                var localizedServiceName = ServiceNameLocalizer.LocalizeForAccount(cloudLocalizer, request.AccountId,
                    billingService.Name, billingService.SystemService);

                if (!billingService.IsActive)
                    throw new InvalidExpressionException(
                        GenerateMessageStatingThatServiceIsBlockedHelper.GenerateMessage(localizedServiceName));

                var resourceConfiguration = await unitOfWork.ResourceConfigurationRepository
                                                .AsQueryableNoTracking()
                                                .FirstOrDefaultAsync(x => x.AccountId == request.AccountId && x.BillingServiceId == request.BillingServiceId, cancellationToken) 
                                            ?? throw new NotFoundException($"У аккаунта '{request.AccountId}' сервис '{localizedServiceName}' не активирован.");

                var rent1CResConfig = await unitOfWork.ResourceConfigurationRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(rc => rc.AccountId == request.AccountId &&
                                               rc.BillingService.SystemService == Clouds42Service.MyEnterprise, cancellationToken);

                var groupedData = await NeedCreateDatabaseToActivateService(billingService, request.AccountId, cancellationToken);

                var canInstallExtension = billingService.CanInstallServiceExtension() && groupedData.dbWithService.Any();


                var dependServiceType = (!billingService.IsHybridService || rent1CResConfig != null)
                    ? billingService.BillingServiceTypes.FirstOrDefault(w => w.DependServiceTypeId != null)?.DependServiceType
                    : null;

                ResourcesConfiguration mainResourceConfiguration = null;

                if (dependServiceType != null)
                {
                    var mainServiceResConfig = await unitOfWork.ResourceConfigurationRepository
                            .AsQueryableNoTracking()
                            .FirstOrDefaultAsync(x => x.AccountId == request.AccountId && x.BillingServiceId == dependServiceType.ServiceId, cancellationToken)
                                               ?? throw new NotFoundException($"У аккаунта '{request.AccountId}' для сервиса '{localizedServiceName}' не найден ресурс конфигурации сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, request.AccountId)}.");

                    mainResourceConfiguration = mainServiceResConfig ?? resourceConfiguration;
                }

                return new BillingServiceInfoDto
                {
                    Id = billingService.Id,
                    Name = localizedServiceName,
                    Opportunities = billingService.Opportunities,
                    ShortDescription = billingService.ShortDescription,
                    SystemService = billingService.SystemService,
                    AccountOwnerId = billingService.AccountOwnerId,
                    MainServiceId = dependServiceType?.ServiceId,
                    IconCloudFileId = billingService.IconCloudFileId,
                    DependServiceTypeId = dependServiceType?.DependServiceTypeId,
                    BillingServiceStatus = billingService.BillingServiceStatus,
                    ServiceActivationDate = billingService.ServiceActivationDate,
                    IsActive = billingService.IsActive,
                    IsHybrid = billingService.IsHybridService,
                    ServiceExpireDate  = resourceConfiguration.IsDemoPeriod ? resourceConfiguration.ExpireDateValue : mainResourceConfiguration!.ExpireDateValue,
                    IsDemoPeriod = resourceConfiguration.IsDemoPeriod,
                    ServiceDependsOnRent = dependServiceType != null,
                    AccountDatabaseToRun = await GetAccountDatabaseToRun(billingService,  groupedData.configurationDependencies, groupedData.dbWithService, cancellationToken),
                    CanInstallExtension = canInstallExtension,
                    NeedCreateDatabaseToActivateService = !groupedData.dbWithService.Any(),
                    Rent1CExpireDate = rent1CResConfig?.ExpireDate,
                    ServiceStatus = serviceStatusHelper.CreateServiceStatusModel(request.AccountId, resourceConfiguration.IsDemoPeriod ? resourceConfiguration : mainResourceConfiguration),
                    AmountData = new BillingServiceAmountDataModelDto
                    { 
                        AmountForOneDay = partialAmountForApplyDemoServiceSubscribesCalculator.CalculateForDays(resourceConfiguration,
                            mainResourceConfiguration, 1) ?? 0,
                        ServiceAmount = resourceConfiguration.Cost,
                        ServicePartialAmount = partialAmountForApplyDemoServiceSubscribesCalculator.Calculate(resourceConfiguration,
                            mainResourceConfiguration)
                    },
                    MainServiceData = new MainServiceData
                    {
                       Id = mainResourceConfiguration.Id,
                       Cost = mainResourceConfiguration.Cost,
                       Frozen = mainResourceConfiguration.Frozen,
                       DiscountGroup = mainResourceConfiguration.DiscountGroup,
                       CostIsFixed = mainResourceConfiguration.CostIsFixed,
                       AccountId = mainResourceConfiguration.AccountId,
                       BillingServiceId = mainResourceConfiguration.BillingServiceId,
                       CreateDate = mainResourceConfiguration.CreateDate,
                       ExpireDate = mainResourceConfiguration.ExpireDate,
                       IsDemoPeriod = mainResourceConfiguration.IsDemoPeriod
                    }
                }.ToOkManagerResult();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Во время получения базовой информации о сервисе {request.BillingServiceId} для аккаунта {request.AccountId} произошла ошибка]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<BillingServiceInfoDto>(ex.Message);
            }
        }
        private async Task<AccountDatabaseToRunDto> GetAccountDatabaseToRun(
           BillingService billingService,
           List<string> compabilityConfigurations,
           List<AccountDatabase> dbsWithService,
           CancellationToken cancellationToken)
        {
            if (billingService.InternalCloudService == InternalCloudServiceEnum.Tardis)
                return await GetTardisAccountDatabaseToRun();

            if (!compabilityConfigurations.Any())
                return new AccountDatabaseToRunDto();


            var databasesWithStatusReadyExist = dbsWithService
                .Where(db => db.State == DatabaseState.Ready.ToString())
                .ToList();

            var defaultConfiguration1C = compabilityConfigurations.FirstOrDefault();

            var template = await unitOfWork.DbTemplateDelimitersReferencesRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(w => w.ConfigurationId == defaultConfiguration1C, cancellationToken);

            var compatibleConfigurationsNames = await unitOfWork.DbTemplateDelimitersReferencesRepository
                .AsQueryableNoTracking()
                .Where(temp => compabilityConfigurations.Contains(temp.ConfigurationId))
                .Select(temp => temp.Name)
                .ToListAsync(cancellationToken);

            var accountDatabasesOnDelimitersToRun =
                billingService.CanInstallServiceExtension()
                    ? GetAccountDatabasesOnDelimitersToRun(dbsWithService)
                    : GetAccountDatabasesOnDelimitersToRun(databasesWithStatusReadyExist);

            return new AccountDatabaseToRunDto
            {
                IsCompatibleDatabasesWithStatusReadyExist = databasesWithStatusReadyExist.Any(),
                IsCompatibleDatabasesWithStatusNewExist = dbsWithService.Any(db => db.State == DatabaseState.NewItem.ToString()),
                AvailabilityOfSuitableConfigurations1C = true,
                TemplateId = template!.TemplateId,
                TemplateNames = string.Join(" , ", compatibleConfigurationsNames),
                AccountDatabasesOnDelimitersToRun = accountDatabasesOnDelimitersToRun
            };
        }

        private List<AccountDatabaseOnDelimitersToRunDto> GetAccountDatabasesOnDelimitersToRun(
            IEnumerable<AccountDatabase> accountDatabases)
            => accountDatabases.Select(w =>
                new AccountDatabaseOnDelimitersToRunDto
                {
                    Id = w.Id,
                    DatabaseState = w.StateEnum,
                    Caption = w.Caption,
                    WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(w),
                }).ToList();

    
        private async Task<AccountDatabaseToRunDto> GetTardisAccountDatabaseToRun()
        {
            var tardisAccountDatabaseId = CloudConfigurationProvider.Tardis.GetAccountDatabaseId();

            var tardisAccountDatabase = await unitOfWork.DatabasesRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Id == tardisAccountDatabaseId) ?? throw new NotFoundException("Инф. база Тардис по ID не найдена.");

            var accountDatabasesOnDelimitersToRun = new List<AccountDatabaseOnDelimitersToRunDto>();
            if (tardisAccountDatabase.PublishStateEnum == PublishState.Published)
            {
                accountDatabasesOnDelimitersToRun.Add(new AccountDatabaseOnDelimitersToRunDto
                {
                    Id = tardisAccountDatabase.Id,
                    DatabaseState = tardisAccountDatabase.StateEnum,
                    Caption = tardisAccountDatabase.Caption,
                    WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(tardisAccountDatabase),
                });
            }

            return new AccountDatabaseToRunDto
            {
                IsCompatibleDatabasesWithStatusReadyExist = true,
                IsCompatibleDatabasesWithStatusNewExist = false,
                AvailabilityOfSuitableConfigurations1C = true,
                TemplateId = tardisAccountDatabase.DbTemplate.Id,
                TemplateNames = tardisAccountDatabase.DbTemplate.DefaultCaption,
                AccountDatabasesOnDelimitersToRun = accountDatabasesOnDelimitersToRun
            };
        }

        private async Task<(List<AccountDatabase> dbWithService, List<string> configurationDependencies)> NeedCreateDatabaseToActivateService(BillingService billingService, Guid accountId, CancellationToken cancellationToken)
        {
            if (billingService.InternalCloudService == InternalCloudServiceEnum.Tardis)
                return ([], []);

            var configurationDependencies = await getConfigurationDatabaseCommand.ExecuteAsync(billingService.Id, cancellationToken);

            return (await unitOfWork.DatabasesRepository
                .AsQueryableNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .Where(db =>
                    (db.State == DatabaseState.Ready.ToString() || db.State == DatabaseState.NewItem.ToString()) &&
                    db.AccountId == accountId &&
                    db.AccountDatabaseOnDelimiter != null &&
                    configurationDependencies.Contains(db.AccountDatabaseOnDelimiter.DbTemplateDelimiter.ConfigurationId))
                .ToListAsync(cancellationToken), configurationDependencies);
        }
          
    }
}
