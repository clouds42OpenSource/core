﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get info on services for the billing service
    /// </summary>
    public class GetBillingServiceInfoQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ILogger42 logger,
        IBillingServiceInfoProvider billingServiceInfoProvider,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetBillingServiceInfoQuery, ManagerResult<List<BillingServiceTypeInfoDto>>>
    {
        /// <summary>
        /// Handle a query to get info on services for the billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ManagerResult<List<BillingServiceTypeInfoDto>>> Handle(GetBillingServiceInfoQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.BillingServiceTypes_GetInfo, () => request.AccountId);
                logger.Info($"Получение информации об услугах сервиса {request.BillingServiceId} для аккаунта {request.AccountId}");

                var servicesOfBillingService = billingServiceInfoProvider.GetBillingServiceTypesInfo(request.BillingServiceId, request.AccountId);

                var activeServiceOfBillingService = await unitOfWork.ResourceRepository.AsQueryable().AsNoTracking()
                    .Where(res => res.Subject == request.AccountId && res.AccountId == request.AccountId &&
                        res.BillingServiceType.ServiceId == request.BillingServiceId)
                    .Select(res => res.BillingServiceTypeId)
                    .Distinct()
                    .ToListAsync(cancellationToken);

                foreach (var service in servicesOfBillingService)
                {
                    if (activeServiceOfBillingService.Contains(service.Id))
                        service.IsActive = true;
                }

                return servicesOfBillingService.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Получение информации об услугах сервиса {request.BillingServiceId} для аккаунта {request.AccountId} завершилось ошибкой]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<BillingServiceTypeInfoDto>>(ex.Message);
            }
        }
    }
}
