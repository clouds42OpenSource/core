﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get account databases to run the billing service
    /// </summary>
    public class GetAccountDatabaseToRunQueryHandler(
        IAccessProvider accessProvider,
        ILogger42 logger,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IAccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper,
        IGetDatabasesForServicesExtensionCommand getDatabasesForServicesExtensionCommand)
        : IRequestHandler<GetAccountDatabaseToRunQuery, ManagerResult<AccountDatabaseToRunDto>>
    {
        /// <summary>
        /// Handle a query to get account databases to run the billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ManagerResult<AccountDatabaseToRunDto>> Handle(GetAccountDatabaseToRunQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => request.AccountId);

                logger.Info($"Получение модели для запуска базы для сервиса {request.BillingServiceId} для аккаунта {request.AccountId}");

                var allowedDatabasesForExtensions = await getDatabasesForServicesExtensionCommand.ExecuteAsync(request.AccountId, request.BillingServiceId);
                
                if (!allowedDatabasesForExtensions.Any())
                {
                    return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDatabaseToRunDto>(
                        "Не удалось получить базы в которые установлен сервис");
                }

                var accountDatabasesIds = allowedDatabasesForExtensions
                    .Where(x => x.AccountDatabaseServiceState is ServiceExtensionDatabaseStatusEnum.DoneInstall or ServiceExtensionDatabaseStatusEnum.ProcessingInstall or ServiceExtensionDatabaseStatusEnum.ProcessingDelete).Select(x => x.AccountDatabaseID).ToList();

                var accountDatabases = (await unitOfWork.DatabasesRepository
                        .AsQueryableNoTracking()
                        .Where(x => accountDatabasesIds.Contains(x.Id))
                        .ToListAsync(cancellationToken))
                    .Select(x => new AccountDatabaseOnDelimitersToRunDto
                    {
                        Caption = x.Caption,
                        DatabaseState = x.StateEnum,
                        Id = x.Id,
                        WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(x)
                    })
                    .ToList();

                return new AccountDatabaseToRunDto
                {
                    IsCompatibleDatabasesWithStatusReadyExist = accountDatabases.Any(x => x.DatabaseState == DatabaseState.Ready),
                    IsCompatibleDatabasesWithStatusNewExist = accountDatabases.Any(x => x.DatabaseState == DatabaseState.NewItem),
                    AvailabilityOfSuitableConfigurations1C = true,
                    AccountDatabasesOnDelimitersToRun = accountDatabases
                }.ToOkManagerResult();

            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения модели для запуска базы для сервиса] {request.BillingServiceId}, для аккаунта {request.AccountId}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDatabaseToRunDto>(ex.Message);
            }
        }
    }
}
