﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Repositories.Interfaces.Common;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get billing service activation info
    /// </summary>
    public class GetServiceActivationQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ReactivateServiceManager reactivateServiceManager,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetBillingServiceActivationQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle a query to billing service activation 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ManagerResult<bool>> Handle(GetBillingServiceActivationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var accountAdmins = accessProvider.GetAccountAdmins(request.AccountId);

                if (!accountAdmins.Any())
                    PagedListExtensions.ToPreconditionFailedManagerResult<bool>(
                        "Невозможно активировать сервис. У аккаунта нет ни одного администратора.");

                var billingService = await unitOfWork.BillingServiceRepository.AsQueryable()
                    .FirstOrDefaultAsync(serv => serv.Id == request.BillingServiceId, cancellationToken)
                    ?? throw new NotFoundException($"Сервис с идентификатором '{request.BillingServiceId}' не существует");

                var result = reactivateServiceManager.ActivateServiceForExistingAccount(billingService.Id, accountAdmins.First());

                if (result.Error)
                {
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(result.Message);
                }

                return (!result.Error).ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка активации сервиса] {request.BillingServiceId} для аккаунта {request.AccountId}");
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
            }
        }
    }
}
