﻿using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get filtered, paged and ordered users with services of the billing service
    /// </summary>
    public class GetUsersWithServicesQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        ILogger42 logger)
        : IRequestHandler<GetUsersWithBillingServicesQuery, ManagerResult<PagedDto<AccountUserBillingServiceTypeDto>>>
    {
        /// <summary>
        /// Handle a query to get filtered, paged and ordered users with services of the billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<AccountUserBillingServiceTypeDto>>> Handle(GetUsersWithBillingServicesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.BillingServiceTypes_GetAccountUsers, () => request.AccountId);

                logger.Info($"Получение списка пользователей с информацией о услугах сервиса " +
                    $"{request.Id} для аккаунта {request.AccountId}");

                var resources = await unitOfWork.ResourceRepository
                    .AsQueryable()
                    .AsSplitQuery()
                    .Include(x => x.BillingServiceType)
                    .ThenInclude(x => x.ParentRelations)
                    .Include(x => x.AccountSponsor)
                    .Where(x => (x.AccountSponsorId == request.AccountId || x.AccountId == request.AccountId) &&
                                x.Subject.HasValue && x.BillingServiceType.ServiceId == request.Id)
                    .GroupBy(x => x.Subject!.Value)
                    .Select(x => new ServiceTypeInfoGroupedByAccountUserModel
                    {
                        AccountUserId = x.Key,
                        ServiceTypesInfo = x.Select(z => new ServiceTypeInfoModel
                        {
                            AccountSponsorCaption = z.AccountSponsor.AccountCaption,
                            AccountSponsorId = z.AccountSponsorId,
                            ServiceTypeId = z.BillingServiceTypeId,
                            ParentServiceTypeRelations = z.BillingServiceType.ParentRelations
                        }).ToList()
                    })
                    .ToListAsync(cancellationToken);

                return (await(await unitOfWork.AccountUsersRepository
                    .AsQueryableNoTracking()
                    .AsSplitQuery()
                    .Include(x => x.Account)
                    .Where(x => x.AccountId == request.AccountId)
                    .Union(unitOfWork.ResourceRepository
                        .AsQueryable()
                        .Where(x => x.AccountSponsorId == request.AccountId && x.BillingServiceTypeId == request.Id)
                        .Join(unitOfWork.AccountUsersRepository
                                .GetNotDeletedAccountUsersQuery()
                                .Include(x => x.Account), z => z.Subject, x => x.Id,
                            (resource, accountUser) => new { resource, accountUser })
                        .Select(x => x.accountUser))
                    .ToListAsync(cancellationToken))
                    .GroupJoin(resources, z => z.Id, x => x.AccountUserId, (user, resource) => new { User = user, Resource = resource.FirstOrDefault() })
                    .Select(x => new AccountUserBillingServiceTypeDto
                    {
                        AccountUserId = x.User.Id,
                        Sponsorship = CreateServicesSponsoringModel(x.User, x.Resource?.ServiceTypesInfo ?? [], request.AccountId),
                        AccountUserName = GetAccountUserName(x.User),
                        Services = GetServicesByParentRelations(x.Resource?.ServiceTypesInfo ?? []),
                        SponsoredServiceTypesByMyAccount = GetSponsoredServices(x.Resource?.ServiceTypesInfo ?? [], request.AccountId),
                    })
                    .AsQueryable()
                    .AutoFilter(request.Filter)
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения списка пользователей с информацией о услугах сервиса] {request.AccountId}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AccountUserBillingServiceTypeDto>>(ex.Message);
            }
        }

        /// <summary>
        /// Create service sponsoring model for user
        /// </summary>
        /// <returns></returns>
        private static AccountUserBillingServiceTypeSponsorshipDto CreateServicesSponsoringModel(AccountUser user, List<ServiceTypeInfoModel> services, Guid accountId)
            => new()
            {
                I = user.AccountId != accountId,
                Me = user.AccountId == accountId && services.Any(
                        serviceTypesInfo => serviceTypesInfo.AccountSponsorId != null &&
                        serviceTypesInfo.AccountSponsorId != user.AccountId),
                Label = user.AccountId == accountId && services.Any(
                        x => x.AccountSponsorId != null && x.AccountSponsorId != user.AccountId)
                        ? services.FirstOrDefault(
                            x => x.AccountSponsorId != null && x.AccountSponsorId != user.AccountId)?.AccountSponsorCaption
                        : user.Account.AccountCaption
            };

        /// <summary>
        /// Get account user name
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static string GetAccountUserName(AccountUser model)
            => !string.IsNullOrEmpty(model.FirstName + model.MiddleName + model.LastName)
                    ? $"{model.Login} ({$"{model.FirstName} {model.MiddleName} {model.LastName}".Trim()})".Trim()
                    : model.Login;

        private static List<Guid> GetServicesByParentRelations(List<ServiceTypeInfoModel> serviceTypeInfoList)
            => serviceTypeInfoList
                .Where(serviceTypeInfo => serviceTypeInfoList
                    .All(dependServiceTypeInfo => !serviceTypeInfo.ParentServiceTypeRelations.
                        Select(relation => relation.MainServiceTypeId)
                        .Contains(dependServiceTypeInfo.ServiceTypeId)))
                .Select(x => x.ServiceTypeId)
                .Distinct()
                .ToList();

        /// <summary>
        /// Get sponsored services for the specified account
        /// </summary>
        /// <param name="servicesInfo">Service info</param>
        /// <param name="accountId">Account identifier for which services are sponsored</param>
        /// <returns></returns>
        private static List<Guid> GetSponsoredServices(List<ServiceTypeInfoModel> servicesInfo, Guid accountId)
            => servicesInfo
                .Where(info => info.AccountSponsorId != null && info.AccountSponsorId != accountId)
                .Select(x => x.ServiceTypeId)
                .ToList();
    }
}
