﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get an account database on delimiters to run the billing service
    /// </summary>
    public class GetAccountDatabaseOnDelimitersToRunQueryHandler(
        IAccessProvider accessProvider,
        ILogger42 logger,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IAccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper)
        : IRequestHandler<GetAccountDatabaseOnDelimitersToRunQuery,
            ManagerResult<AccountDatabaseOnDelimitersToRunDto>>
    {
        /// <summary>
        /// Handle a query to get an account database on delimiters to run the billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ManagerResult<AccountDatabaseOnDelimitersToRunDto>> Handle(GetAccountDatabaseOnDelimitersToRunQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var accountDatabase = await unitOfWork.DatabasesRepository.AsQueryable()
                    .FirstOrDefaultAsync(db => db.Id == request.DatabaseId && db.AccountDatabaseOnDelimiter != null, cancellationToken);

                accessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => accountDatabase.AccountId);
                logger.Info($"Получение базы на разделителях {request.DatabaseId} для запуска");
                if (accountDatabase == null)
                    throw new NotFoundException($"База на разделителях {request.DatabaseId} не найдена");

                return new AccountDatabaseOnDelimitersToRunDto
                {
                    Id = accountDatabase.Id,
                    Caption = accountDatabase.Caption,
                    DatabaseState = accountDatabase.StateEnum,
                    WebPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(accountDatabase),
                }.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения базы на разделителях] {request.DatabaseId} для запуска");
                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDatabaseOnDelimitersToRunDto>(ex.Message);
            }
        }
    }
}
