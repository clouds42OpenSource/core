﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;
using PagedListExtensions = PagedListExtensionsNetFramework.PagedListExtensions;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get the billing service's activation status
    /// </summary>
    public class GetBillingServiceActivationStatusQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetBillingServiceActivationStatusQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle a query to get the billing service's activation status
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<bool>> Handle(GetBillingServiceActivationStatusQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var billingService = await unitOfWork.BillingServiceRepository.AsQueryable()
                    .FirstOrDefaultAsync(serv => serv.Id == request.BillingServiceId, cancellationToken)
                    ?? throw new NotFoundException("Указанного сервиса не существует");
                var currentUser = await accessProvider.GetUserAsync();
                var accountAdmin = GetAccountAdminFirstOrDefault(request.AccountId);
                var errorMessage =
                $@"Для подключения сервиса “{billingService.Name}” необходимо обратиться к администратору аккаунта: </br>
                        ФИО: {accountAdmin?.LastName} {accountAdmin?.FirstName} {accountAdmin?.MiddleName};</br>
                        Тел.: {accountAdmin?.PhoneNumber};</br>
                        E-mail: {accountAdmin?.Email}.";

                if (currentUser != null && currentUser.IsAccountUser())
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(errorMessage);

                if (!billingService.IsActive)
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(
                        GenerateMessageStatingThatServiceIsBlockedHelper.GenerateMessage(billingService.Name));

                var resourceConfiguration = await unitOfWork.ResourceConfigurationRepository.AsQueryable()
                    .FirstOrDefaultAsync(res => res.AccountId == request.AccountId && res.BillingService.Id == billingService.Id, cancellationToken);

                return (resourceConfiguration != null).ToOkManagerResult();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Во время получения статуса сервиса {request.BillingServiceId} для аккаунта {request.AccountId} произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
            }
        }

        // <summary>
        /// Получить объект пользователя аккаунт админа.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>Объект пользователя.</returns>
        private IAccountUser GetAccountAdminFirstOrDefault(Guid accountId)
        {
            var accountAdmins = accessProvider.GetAccountAdmins(accountId);
            if (!accountAdmins.Any())
                return null;

            var accountAdminId = accountAdmins.First();
            return unitOfWork.AccountUsersRepository.FirstOrDefault(au => au.Id == accountAdminId);
        }
    }
}
