﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.BLL.Common.CloudsLocalization;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Locales.Extensions;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get a user for sponsorship for a billing service
    /// </summary>
    public class GetUserForSponsorshipQueryHandler(
        IAccessProvider accessProvider,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        ICloudLocalizer cloudLocalizer,
        ICheckAccountDataProvider checkAccountDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IRequestHandler<GetUserForSponsorshipQuery, ManagerResult<AccountUserBillingServiceTypeDto>>
    {
        /// <summary>
        /// Handle a query to get a user for sponsorship for a billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="NotFoundException"></exception>
        /// <exception cref="ValidateException"></exception>
        public async Task<ManagerResult<AccountUserBillingServiceTypeDto>> Handle(GetUserForSponsorshipQuery request, CancellationToken cancellationToken)
        {
            Guid sponsoringAccountId = Guid.Empty;
            string logMessage = $"Поиск внешнего пользователя с e-mail '{request.Email}' для спонсирования сервиса '{request.Id}' " +
                $"аккаунтом '{sponsoringAccountId}'";
            try
            {
                sponsoringAccountId = accessProvider.ContextAccountId;

                accessProvider.HasAccess(ObjectAction.ManageResources, () => sponsoringAccountId);
                logger.Info(logMessage);

                if (string.IsNullOrEmpty(request.Email))
                    throw new ArgumentException("Не указан e-mail пользователя");

                var accountUserForSponsorship = await unitOfWork.AccountUsersRepository.AsQueryable().AsNoTracking()
                    .FirstOrDefaultAsync(user => user.Email.ToLower() == request.Email.ToLower(), cancellationToken)
                    ?? throw new NotFoundException("Пользователь с указанным e-mail не существует");

                if (accountUserForSponsorship.AccountId == sponsoringAccountId)
                    throw new ValidateException("Спонсирование доступно только для пользователей других аккаунтов. " +
                        "Спонсирование пользователей своего аккаунта недоступно");

                if (accountConfigurationDataProvider.GetAccountLocale(sponsoringAccountId) !=
                    accountConfigurationDataProvider.GetAccountLocale(accountUserForSponsorship.AccountId))
                    throw new ValidateException("Локали аккаунтов не совпадают. Спонсирование пользователей других стран недоступно");

                var dependBillingServiceOfSponsoringUser = await unitOfWork.BillingServiceTypeRepository.AsQueryable().AsNoTracking()
                    .FirstOrDefaultAsync(type => type.ServiceId == request.Id && type.DependServiceTypeId.HasValue, cancellationToken);

                var currentAccountResourceConfiguration = await unitOfWork.ResourceConfigurationRepository.AsQueryable().AsNoTracking()
                        .FirstOrDefaultAsync(conf => conf.AccountId == sponsoringAccountId && conf.BillingServiceId == request.Id, cancellationToken);

                if (dependBillingServiceOfSponsoringUser != null)
                {
                    var dependServiceResourceConfiguration = await unitOfWork.ResourceConfigurationRepository.AsQueryable().AsNoTracking()
                        .FirstOrDefaultAsync(conf => conf.AccountId == sponsoringAccountId && conf.BillingServiceId == dependBillingServiceOfSponsoringUser.ServiceId, cancellationToken);

                    if (dependServiceResourceConfiguration == null || dependServiceResourceConfiguration.FrozenValue)
                        throw new ValidateException(
                            $"У аккаунта cпонсируемого пользователя не активирован/заблокирован сервис " +
                            $"\"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, sponsoringAccountId)}\"");
                }
                else
                {
                    if (currentAccountResourceConfiguration == null || currentAccountResourceConfiguration.FrozenValue)
                        throw new ValidateException("Сервис спонсирующего аккаунта не активирован/заблокирован");
                }

                var sponsoringAccount = await unitOfWork.AccountsRepository.AsQueryable().AsNoTracking()
                    .FirstOrDefaultAsync(acc => acc.Id == sponsoringAccountId, cancellationToken);

                if (checkAccountDataProvider.CheckAccountIsDemo(sponsoringAccount))
                    throw new ValidateException("У спонсирующего аккаунта сервис находится в демо-периоде. Спонсирование недоступно");

                var resourceConfigurationOfSponsoredAccount = await unitOfWork.ResourceConfigurationRepository.AsQueryable().AsNoTracking()
                    .FirstOrDefaultAsync(conf => conf.AccountId == accountUserForSponsorship.AccountId &&
                        conf.BillingServiceId == request.Id, cancellationToken);

                if (resourceConfigurationOfSponsoredAccount == null || resourceConfigurationOfSponsoredAccount.FrozenValue)
                    throw new ValidateException($"У аккаунта спонсируемого пользователя не активен сервис " +
                        $"{currentAccountResourceConfiguration.GetLocalizedServiceName(cloudLocalizer)}");

                if (await unitOfWork.ResourceRepository.AsQueryable().AsNoTracking()
                    .Where(res => res.Subject == accountUserForSponsorship.Id && res.AccountSponsorId != null &&
                    (request.ServiceId == null && res.BillingServiceType.ServiceId == request.Id ||
                    request.ServiceId == res.BillingServiceTypeId)).AnyAsync(cancellationToken))
                    throw new ValidateException("Указанная услуга уже проспонсирована для пользователя");

                if (await unitOfWork.ResourceRepository.AsQueryable().AsNoTracking()
                    .Where(x => x.Subject == accountUserForSponsorship.Id &&
                    (request.ServiceId == null && x.BillingServiceType.ServiceId == request.Id ||
                    request.ServiceId == x.BillingServiceTypeId)).AnyAsync(cancellationToken))
                    throw new ValidateException("Указанная услуга уже подключена для пользователя");

                return new AccountUserBillingServiceTypeDto
                {
                    AccountUserId = accountUserForSponsorship.Id,
                    Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                    {
                        I = true,
                        Label = accountUserForSponsorship.Account.AccountCaption ?? accountUserForSponsorship.Account.IndexNumber.ToString()
                    },
                    AccountUserName =
                    !string.IsNullOrEmpty(accountUserForSponsorship.FirstName + accountUserForSponsorship.MiddleName + accountUserForSponsorship.LastName)
                    ? $"{accountUserForSponsorship.Login} (" +
                    $"{$"{accountUserForSponsorship.FirstName} {accountUserForSponsorship.MiddleName} {accountUserForSponsorship.LastName}".Trim()})".Trim()
                    : accountUserForSponsorship.Login
                }.ToOkManagerResult();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{logMessage} завершился с ошибкой]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountUserBillingServiceTypeDto>(ex.Message);
            }
        }
    }
}
