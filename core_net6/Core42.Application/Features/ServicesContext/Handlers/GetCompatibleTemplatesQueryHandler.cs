﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get account database templates compatible with the billing service
    /// </summary>
    public class GetCompatibleTemplatesQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IGetDatabasesForServicesExtensionCommand getDatabasesForServicesExtensionCommand)
        : IRequestHandler<GetCompatibleTemplatesQuery, ManagerResult<List<AvailableDatabaseForServiceExtensionDto>>>
    {
        /// <summary>
        /// Handle a query to get account database templates compatible with the billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<AvailableDatabaseForServiceExtensionDto>>> Handle(GetCompatibleTemplatesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var contextAccountId = accessProvider.ContextAccountId;

                accessProvider.HasAccess(ObjectAction.GetServiceExtensionDatabase, () => contextAccountId);

                var databasesForService = await getDatabasesForServicesExtensionCommand.ExecuteAsync(contextAccountId, request.BillingServiceId);

                if (!databasesForService.Any())
                    return new List<AvailableDatabaseForServiceExtensionDto>().ToOkManagerResult();

                var databaseIDs = databasesForService.Select(c => c.AccountDatabaseID).ToList();

                var databasesAccount = await unitOfWork.DatabasesRepository
                    .AsQueryableNoTracking()
                    .Where(db => (db.State == DatabaseState.Ready.ToString() || 
                                  db.State == DatabaseState.NewItem.ToString()) &&
                        db.AccountId == contextAccountId && 
                        db.AccountDatabaseOnDelimiter != null && 
                        databaseIDs.Contains(db.Id))
                    .Include(x => x.DbTemplate)
                    .Select(db => new AvailableDatabaseForServiceExtensionDto
                    {
                        Caption = db.Caption,
                        Id = db.Id,
                        DatabaseStateString = db.State,
                        TemplateName = db.DbTemplate.Name,
                        DatabaseImageCssClass = db.DbTemplate.ImageUrl,
                        ExtensionLastActivityDate = DateTime.Now
                    })
                    .ToListAsync(cancellationToken);

                databasesAccount.GroupJoin(databasesForService, z => z.Id, x => x.AccountDatabaseID, (database, service) => new { database, service })
                    .ToList()
                    .ForEach(x =>
                    {
                        x.database.Error = x.service.FirstOrDefault()?.Error;
                        x.database.ExtensionState = x.service.FirstOrDefault()?.AccountDatabaseServiceState;
                        x.database.IsInstalled =
                            x.service.FirstOrDefault()?.AccountDatabaseServiceState is
                                ServiceExtensionDatabaseStatusEnum.DoneInstall
                                or ServiceExtensionDatabaseStatusEnum.ProcessingInstall
                                or ServiceExtensionDatabaseStatusEnum.ProcessingDelete;
                    });

                return databasesAccount.ToOkManagerResult();

            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения данных о шаблонах инф. баз, совместимы с сервисом {request.BillingServiceId}]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<AvailableDatabaseForServiceExtensionDto>>(ex.Message);
            }
        }
    }
}
