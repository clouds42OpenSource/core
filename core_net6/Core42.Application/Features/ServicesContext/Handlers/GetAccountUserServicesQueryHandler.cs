﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Partner;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get all user's services of the billing service
    /// </summary>
    public class GetAccountUserServicesQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetAccountUserServicesQuery, ManagerResult<ServiceTypeStateForUserDto>>
    {
        /// <summary>
        /// Handle a query to get all user's services of the billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<ServiceTypeStateForUserDto>> Handle(GetAccountUserServicesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var accountUser = await unitOfWork.AccountUsersRepository
                                      .AsQueryableNoTracking()
                                      .FirstOrDefaultAsync(user => user.Login == request.UserLogin, cancellationToken)
                                         ?? throw new NotFoundException("Пользователя с указанным логином не существует");

                accessProvider.HasAccess(ObjectAction.BillingServiceType_GetStateForUser, () => accountUser.AccountId);

                var billingService = await unitOfWork.BillingServiceRepository
                                         .AsQueryableNoTracking()
                                         .FirstOrDefaultAsync(x => x.Id == request.BillingServiceId, cancellationToken)
                                            ?? throw new NotFoundException("Сервиса с указанным идентификатором не существует");

                var servicesForAccount = await unitOfWork.ResourceRepository
                    .AsQueryableNoTracking()
                    .Where(res => res.BillingServiceType.ServiceId == billingService.Id &&
                        res.AccountId == accountUser.Id)
                    .ToListAsync(cancellationToken);

                var sponsoringAccountId = servicesForAccount.FirstOrDefault(x => x.AccountSponsorId.HasValue)?.AccountId;

                var resourceConfiguration = await unitOfWork.ResourceConfigurationRepository
                    .AsQueryableNoTracking()
                    .Include(conf => conf.BillingService)
                    .ThenInclude(x => x.BillingServiceTypes)
                    .FirstOrDefaultAsync(conf => conf.BillingServiceId == billingService.Id &&
                        conf.AccountId == (sponsoringAccountId ?? accountUser.AccountId), cancellationToken);

                if (resourceConfiguration == null)
                    return new ServiceTypeStateForUserDto { EnabledServiceTypesList = new GuidListItemDto() }.ToOkManagerResult();

                if (sponsoringAccountId.HasValue && resourceConfiguration.IsDemoPeriod)
                    throw new PreconditionException($"Операция спонсирования сервиса {billingService.Name} недоступна в режим демо-периода");

                if (!sponsoringAccountId.HasValue && resourceConfiguration is { IsDemoPeriod: true, ExpireDate: not null } && resourceConfiguration.ExpireDateValue <= DateTime.Now)
                    throw new PreconditionException($"У сервиса {billingService.Name} закончился демо-период");

                var mainBillingServiceId = resourceConfiguration.BillingService.BillingServiceTypes.FirstOrDefault(w =>
                    w.DependServiceTypeId != null)?.DependServiceType.ServiceId;

                var mainResourcesConfiguration = await unitOfWork.ResourceConfigurationRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(r => r.AccountId == resourceConfiguration.AccountId
                                              && r.BillingService.Id == mainBillingServiceId, cancellationToken);

                if (mainResourcesConfiguration == null && mainBillingServiceId.HasValue)
                    throw new NotFoundException(
                        $"Информация об основной услуге для услуги {resourceConfiguration.BillingService.Name} для аккаунта {accountUser.AccountId} не найдена");

                mainResourcesConfiguration ??= resourceConfiguration;

                bool serviceIsActive = (servicesForAccount.Any() 
                                        || resourceConfiguration.BillingServiceId == mainResourcesConfiguration.BillingServiceId) && mainResourcesConfiguration.FrozenValue;

                return new ServiceTypeStateForUserDto
                {
                    ServiceIsActive = serviceIsActive,
                    ServiceExpiredDate = mainResourcesConfiguration.ExpireDateValue,
                    ServiceDemoExpiredDate = resourceConfiguration.ExpireDate ?? DateTime.MinValue,
                    IsDemoPeriod = resourceConfiguration.IsDemoPeriod,
                    IsServiceDisabled = !billingService.IsActive,
                    EnabledServiceTypesList = new GuidListItemDto
                    {
                        List = servicesForAccount?
                            .Where(x => x.Subject == accountUser.Id || x.Subject == accountUser.AccountId)
                            .Select(x => x.BillingServiceTypeId)
                            .Distinct()
                            .ToList()
                    }
                }.ToOkManagerResult();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[При извлечении информации об услугах сервиса {request.BillingServiceId} для пользователя] " +
                    $"{request.UserLogin} произошла ошибка");

                return PagedListExtensions.ToPreconditionFailedManagerResult<ServiceTypeStateForUserDto>(ex.Message);
            }
        }
    }
}
