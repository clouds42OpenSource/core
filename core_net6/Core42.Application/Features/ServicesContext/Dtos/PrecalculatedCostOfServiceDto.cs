﻿using System.ComponentModel.DataAnnotations;
using Clouds42.DataContracts.BillingService;

namespace Core42.Application.Features.ServicesContext.Dtos
{
    /// <summary>
    /// Precalculated cost of services of the billing service for account users
    /// </summary>
    public class PrecalculatedCostOfServiceDto
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        [Required]
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Current users' services of the billing service
        /// </summary>
        public CalculateBillingServiceTypeDto ChangedLicense { get; set; }

        /// <summary>
        /// New users' services of the billing service
        /// </summary>
        public IEnumerable<CalculateBillingServiceTypeDto> ChangedUsers { get; set; }
    }
}
