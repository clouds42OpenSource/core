﻿namespace Core42.Application.Features.ServicesContext.Dtos
{
    /// <summary>
    /// модель продления сервиса
    /// </summary>
    public class ProlongDemoServiceDto
    {
        public Guid ServiceId { get; set; }
        public Guid AccountId { get; set; }
        public int ProlongDayCount { get; set; }
    }
}
