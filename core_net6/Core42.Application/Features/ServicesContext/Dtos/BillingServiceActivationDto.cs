﻿namespace Core42.Application.Features.ServicesContext.Dtos
{
    /// <summary>
    /// Billing service activation info dto
    /// </summary>
    public class BillingServiceActivationDto
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Billing service name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Short description of the billing service
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Billing service icon cloud file identifier
        /// </summary>
        public Guid? IconCloudFileId { get; set; }

        /// <summary>
        /// Billing service activation link
        /// </summary>
        public string ServiceActivationLink { get; set; }
    }
}
