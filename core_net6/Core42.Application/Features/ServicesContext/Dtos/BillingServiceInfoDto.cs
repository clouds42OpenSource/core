﻿using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Core42.Application.Features.ServicesContext.Dtos
{
    /// <summary>
    /// Billing service info dto
    /// </summary>
    public class BillingServiceInfoDto
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Billing service name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Short description of the billing service
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Description of the billing service opportunities
        /// </summary>
        public string Opportunities { get; set; }

        /// <summary>
        /// Billing service icon cloud file identifier
        /// </summary>
        public Guid? IconCloudFileId { get; set; }

        /// <summary>
        /// Main billing service identifier
        /// </summary>
        public Guid? MainServiceId { get; set; }

        /// <summary>
        /// Identifier of the billing service on which the current billing service depends
        /// </summary>
        public Guid? DependServiceTypeId { get; set; }

        /// <summary>
        /// Account identifier that owns the billing service
        /// </summary>
        public Guid? AccountOwnerId { get; set; }

        /// <summary>
        /// System type of the billing service
        /// </summary>
        public Clouds42Service? SystemService { get; set; }

        /// <summary>
        /// Billing service status
        /// </summary>
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Billing service activation date
        /// </summary>
        public DateTime? ServiceActivationDate { get; set; }

        /// <summary>
        /// Is the billing service active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Is the billing serivse hybrid   
        /// </summary>
        public bool IsHybrid { get; set; }

        /// <summary>
        /// Billing service expire date
        /// </summary>
        public DateTime ServiceExpireDate { get; set; }

        /// <summary>
        /// Main billing service data
        /// </summary>
        public IResourcesConfiguration MainServiceData { get; set; }

        /// <summary>
        /// Is the billing service on demo-period
        /// </summary>
        public bool IsDemoPeriod { get; set; }

        /// <summary>
        /// Billing service depends on the Rent1C service
        /// </summary>
        public bool ServiceDependsOnRent { get; set; }

        /// <summary>
        /// Amount of the billing service
        /// </summary>
        public BillingServiceAmountDataModelDto AmountData { get; set; }

        /// <summary>
        /// Billing service status info
        /// </summary>
        public ServiceStatusModelDto ServiceStatus { get; set; }

        /// <summary>
        /// Is the billing service system
        /// </summary>
        public bool IsSystemService { get => SystemService != null; }

        /// <summary>
        /// Infobase launch model
        /// </summary>
        public AccountDatabaseToRunDto AccountDatabaseToRun { get; set; }

        /// <summary>
        /// Can install the service extension
        /// </summary>
        public bool CanInstallExtension { get; set; }

        /// <summary>
        /// Need create a database to activate the billing service
        /// </summary>
        public bool NeedCreateDatabaseToActivateService { get; set; }

        /// <summary>
        /// Rent1C expire date
        /// </summary>
        public DateTime? Rent1CExpireDate { get; set; }
    }

    public class MainServiceData : IResourcesConfiguration
    {
        public Guid Id { get; set; }

        public decimal Cost { get; set; }

        public bool? CostIsFixed { get; set; }

        public DateTime? ExpireDate { get; set; }

        public bool? Frozen { get; set; }

        public int? DiscountGroup { get; set; }

        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Признак демо периода.
        /// </summary>
        public bool IsDemoPeriod { get; set; }

        public Guid AccountId { get; set; }

        public Guid BillingServiceId { get; set; }

        public DateTime GetExpireDateValue()
            => ExpireDate ?? DateTime.Today.AddMonths(1);

        public IBillingService GetBillingService()
            => throw new NotImplementedException();
    }
}
