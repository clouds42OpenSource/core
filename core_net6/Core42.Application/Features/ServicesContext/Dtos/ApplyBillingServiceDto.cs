﻿using System.ComponentModel.DataAnnotations;
using Clouds42.DataContracts.BillingService;

namespace Core42.Application.Features.ServicesContext.Dtos
{
    /// <summary>
    /// Apply the billing service for the account
    /// </summary>
    public class ApplyBillingServiceDto
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        [Required]
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Need using promise payment
        /// </summary>
        public bool UsePromisePayment { get; set; }

        /// <summary>
        /// Changed licenses of the billing services
        /// </summary>
        public ICollection<CalculateBillingServiceTypeDto> ChangedLicenses { get; set; }

        /// <summary>
        /// Changed users of billing services
        /// </summary>
        public ICollection<CalculateBillingServiceTypeDto> ChangedUsers { get; set; }
    }
}
