﻿namespace Core42.Application.Features.ServicesContext.Dtos
{
    /// <summary>
    /// Account database template model
    /// </summary>
    public class DatabaseTemplateDto
    {
        /// <summary>
        /// Database template identifier
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Database template name
        /// </summary>
        public string TemplateName { get; set; }
    }
}
