﻿using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Features.ServicesContext.MyDatabases.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServicesContext.MyDatabases.Handlers
{
    /// <summary>
    /// Handler for a query to get info on a service of the "My Databases" billing service
    /// </summary>
    public class GetMyDatabasesServiceInfoQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ICloudLocalizer cloudLocalizer,
        IUnitOfWork unitOfWork,
        IConfiguration configuration,
        IServiceStatusHelper serviceStatusHelper,
        ServiceTypeResourcesDataCollector serviceTypeResourcesDataCollector)
        : IRequestHandler<GetMyDatabasesServiceInfoQuery,
            ManagerResult<MyDatabasesServiceTypeCardDto>>
    {
        private const Clouds42Service MyDatabasesBillingServiceType = Clouds42Service.MyDatabases;

        /// <summary>
        /// Handle a query to get info on a service of the "My Databases" billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<MyDatabasesServiceTypeCardDto>> Handle(GetMyDatabasesServiceInfoQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var serviceAccountAny = await unitOfWork.ServiceAccountRepository
                    .AsQueryableNoTracking()
                    .AnyAsync(acc => acc.Id == request.AccountId, cancellationToken);

                if (serviceAccountAny)
                    throw new InvalidOperationException("Служебным аккаунтам недоступно управление сервисами");

                accessProvider.HasAccess(ObjectAction.BillingService_GetInfo, () => request.AccountId);
                var localizedServiceName = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, request.AccountId);

                var serviceType = await unitOfWork.BillingServiceTypeRepository
                                      .AsQueryableNoTracking()
                                      .Include(x => x.ConfigurationServiceTypeRelations)
                                      .FirstOrDefaultAsync(x => x.Id == request.ServiceId, cancellationToken)
                    ?? throw new NotFoundException("Услуга с указанным идентификатором не найдена");

                var rent1CResourceConfiguration = await unitOfWork.ResourceConfigurationRepository
                                                      .AsQueryableNoTracking()
                                                      .FirstOrDefaultAsync(conf => conf.AccountId == request.AccountId && conf.BillingService.SystemService == Clouds42Service.MyEnterprise, cancellationToken)
                    ?? throw new NotFoundException($"Для указанного аккаунта не найден ресурс конфигурация сервиса {localizedServiceName}");

                var serviceStatus = serviceStatusHelper.CreateServiceStatusModel(request.AccountId, rent1CResourceConfiguration);

                DateTime displayedExpireDate = (serviceStatus.PromisedPaymentIsActive &&
                    serviceStatus.PromisedPaymentExpireDate <= rent1CResourceConfiguration.ExpireDate
                    ? serviceStatus.PromisedPaymentExpireDate : rent1CResourceConfiguration.ExpireDate)
                    ?? throw new InvalidOperationException("Не удалось получить дату окончания сервиса для указанного аккаунта");

                string currency = (await unitOfWork.AccountConfigurationRepository
                                      .AsQueryableNoTracking()
                                      .Include(conf => conf.Locale)
                                      .FirstOrDefaultAsync(conf => conf.Account.Id == request.AccountId, cancellationToken))?.Locale?.Currency
                                ?? (await unitOfWork.LocaleRepository
                                    .AsQueryableNoTracking()
                                    .FirstOrDefaultAsync(loc => loc.ID == Guid.Parse(configuration["DefaultLocale"]!), cancellationToken))?.Currency
                                ?? throw new InvalidOperationException("Не удалось получить локаль для указанного аккаунта");

                var accountLimits = (await unitOfWork
                        .GetGenericRepository<LimitOnFreeCreationDbForAccount>()
                        .AsQueryableNoTracking()
                        .FirstOrDefaultAsync(limit => limit.AccountId == request.AccountId, cancellationToken))?.LimitedQuantity ?? CloudConfigurationProvider.MyDatabasesService.GetDefaultLimitOnFreeCreationDb();

                var myDatabasesBillingServiceInfo = await unitOfWork.BillingServiceTypeRepository
                            .AsQueryableNoTracking()
                            .AsSplitQuery()
                            .Where(x => x.Service.SystemService == Clouds42Service.MyDatabases && (x.SystemServiceType == ResourceType.CountOfDatabasesOverLimit || x.SystemServiceType == ResourceType.ServerDatabasePlacement))
                            .Select(x => new MyDatabasesServiceTypeBillingDataDto
                            {
                                ServiceId = x.ServiceId,
                                ServiceName = localizedServiceName,
                                ServiceTypeId = x.Id,
                                ServiceTypeName = x.Name,
                                ServiceTypeDescription = x.Description,
                                SystemServiceType = x.SystemServiceType!.Value,
                                Clouds42ServiceType = x.Service.SystemService,
                                BillingType = x.BillingType,
                                IsActiveService = x.Service.IsActive,
                                LimitOnFreeLicenses = x.SystemServiceType != ResourceType.CountOfDatabasesOverLimit
                                    ? 0
                                    : accountLimits,
                                CostPerOneLicense = x.AccountRates.Any(z => z.AccountId == request.AccountId)
                                ? x.AccountRates.First(z => z.AccountId == request.AccountId).Cost
                                    : x.Rates.First().Cost,
                                VolumeInQuantity = (x.SystemServiceType != ResourceType.CountOfDatabasesOverLimit ? 0 : accountLimits) >
                                x.Resources.Where(z => z.Subject.HasValue && (z.AccountId == request.AccountId && z.AccountSponsorId == null || z.AccountSponsorId == request.AccountId)).Sum(z => z.MyDatabasesResourceId.HasValue ? z.MyDatabasesResource.ActualDatabasesCount : 1) ? 0 :
                                                    x.Resources.Where(z => z.Subject.HasValue && (z.AccountId == request.AccountId && z.AccountSponsorId == null || z.AccountSponsorId == request.AccountId)).Sum(z => z.MyDatabasesResourceId.HasValue ? z.MyDatabasesResource.ActualDatabasesCount : 1) - (x.SystemServiceType != ResourceType.CountOfDatabasesOverLimit ? 0 : accountLimits),
                                TotalAmount = x.Resources.Where(z => z.Subject.HasValue && (z.AccountId == request.AccountId && z.AccountSponsorId == null || z.AccountSponsorId == request.AccountId)).Sum(z => z.Cost)
                            })
                            .ToListAsync(cancellationToken);

                var databasesOfService = myDatabasesBillingServiceInfo
                                             .FirstOrDefault(st => st.SystemServiceType == ResourceType.CountOfDatabasesOverLimit) ?? 
                                         throw new NotFoundException($"Не удалось получить данные биллинга для услуги {ResourceType.CountOfDatabasesOverLimit.Description()}");

                var serverDatabasesOfService = myDatabasesBillingServiceInfo
                                                   .FirstOrDefault(st => st.SystemServiceType == ResourceType.ServerDatabasePlacement) ?? 
                                               throw new NotFoundException($"Не удалось получить данные биллинга для услуги {ResourceType.ServerDatabasePlacement.Description()}");

                bool serviceHasAccessToServerDb = serviceType.SystemServiceType == ResourceType.AccessToServerDatabase;


                var accountRate = await unitOfWork.AccountRateRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.AccountId == request.AccountId && x.BillingServiceTypeId == serviceType.Id, cancellationToken);

                var rateService = await unitOfWork.RateRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(r => r.BillingServiceTypeId == serviceType.Id, cancellationToken);


                var resources =  await unitOfWork.ResourceRepository
                    .AsQueryableNoTracking()
                    .Where(x => (x.AccountId == request.AccountId || x.AccountSponsorId == request.AccountId) &&
                                x.BillingServiceType.ServiceId == request.ServiceId && x.Subject.HasValue)
                    .ToListAsync(cancellationToken);

                var serviceTypeResourcesData = serviceTypeResourcesDataCollector.Collect(serviceType, request.AccountId, resources, rateService!);

                return new MyDatabasesServiceTypeCardDto
                {
                    Id = serviceType.Id,
                    BillingServiceId = serviceType.ServiceId,
                    Description = serviceType.Description,
                    DisplayedName = serviceType.Name,
                    SystemServiceType = serviceType.SystemServiceType,
                    ServiceTypeExpireDate = rent1CResourceConfiguration.ExpireDateValue,
                    DisplayedExpireDate = displayedExpireDate,
                    ServiceTypeStatus = serviceStatus,
                    CurrencyCode = currency,
                    DatabasesCount = serviceHasAccessToServerDb ? serverDatabasesOfService.VolumeInQuantity
                        : databasesOfService.VolumeInQuantity,
                    CostPerOneDatabase = serviceHasAccessToServerDb ? serverDatabasesOfService.CostPerOneLicense
                        : databasesOfService.CostPerOneLicense,
                    TotalDatabasesCost = serviceHasAccessToServerDb ? serverDatabasesOfService.TotalAmount
                        : databasesOfService.TotalAmount,
                    UsedLicenses = serviceTypeResourcesData.UsedLicensesCount,
                    IamSponsorLicenses = serviceTypeResourcesData.IamSponsorLicensesCount,
                    MeSponsorLicenses = serviceTypeResourcesData.MeSponsorLicensesCount,
                    Amount = serviceTypeResourcesData.ServiceTypeAmount,
                    Cost = (accountRate?.Cost ?? rateService?.Cost) ?? 0
                }.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Во время получения информации услуги для сервиса \"{MyDatabasesBillingServiceType.Description()}\"] для аккаунта {request.AccountId} произошла ошибка");

                return PagedListExtensions.ToPreconditionFailedManagerResult<MyDatabasesServiceTypeCardDto>(ex.Message);
            }
        }
    }
}
