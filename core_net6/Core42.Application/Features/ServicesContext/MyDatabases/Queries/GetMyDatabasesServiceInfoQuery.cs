﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDatabasesService;
using MediatR;

namespace Core42.Application.Features.ServicesContext.MyDatabases.Queries
{
    /// <summary>
    /// Query to get info on a service of the "My Databases" billing service
    /// </summary>
    public class GetMyDatabasesServiceInfoQuery : IRequest<ManagerResult<MyDatabasesServiceTypeCardDto>>
    {
        /// <summary>
        /// Service identifier of the "My Databases" billing service
        /// </summary>
        [Required]
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
