﻿using Clouds42.Domain.Enums;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.PartnersContext.Queries.Filters
{
    public class PartnerServiceFilter : IQueryFilter, IHasSpecificSearch
    {
        public string? SearchLine { get; set; }
        public Guid? PartnerAccountId { get; set; }
        public BillingServiceStatusEnum? BillingServiceStatus { get; set; }
        public string? Name { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}
