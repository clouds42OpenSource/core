﻿
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.PartnersContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PartnersContext.Queries
{
    public class GetPartnerServicesQuery : ISortedQuery, IPagedQuery, IRequest<ManagerResult<PaginationDataResultDto<PartnerBillingServiceDto>>>
    {
        public string OrderBy { get; set; }
        public PartnerServiceFilter? Filter { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
}
