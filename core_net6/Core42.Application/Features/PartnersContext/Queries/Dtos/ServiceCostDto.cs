﻿namespace Core42.Application.Features.PartnersContext.Queries.Dtos
{
    public class ServiceCostDto
    {
        public Guid ServiceId { get; set; }
        public decimal Cost { get; set; }
        public int CountUsers { get; set; }
    }
}
