﻿using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PartnersContext.Handlers.Specifications;
using Core42.Application.Features.PartnersContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PartnersContext.Handlers
{
    public class GetPartnerServicesQueryHandler(
        IUnitOfWork dbLayer, 
        IAccessProvider accessProvider, 
        IHandlerException handlerException, 
        IAgencyAgreementDataProvider agencyAgreementDataProvider) : IRequestHandler<GetPartnerServicesQuery, ManagerResult<PaginationDataResultDto<PartnerBillingServiceDto>>>
    {
        public async Task<ManagerResult<PaginationDataResultDto<PartnerBillingServiceDto>>> Handle(GetPartnerServicesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Partner_GetBillingServices, () => request.Filter?.PartnerAccountId);

                var actualAgencyAgreement = await agencyAgreementDataProvider.GetActualAgencyAgreementAsync();

                var data = (await dbLayer.BillingServiceRepository
                    .AsQueryableNoTracking()
                    .AsSplitQuery()
                    .Include(x => x.AccountOwner)
                    .Include(x => x.BillingServiceTypes)
                    .ThenInclude(x => x.Resources.Where(y => y.Subject.HasValue))
                    .Include(x => x.ResourcesConfigurations.Where(z => !z.IsDemoPeriod && (!z.Frozen.HasValue || !z.Frozen.Value)))
                    .Where(x => x.AccountOwnerId.HasValue)
                    .Select(x => new
                        {
                            Service = x,
                            Configuration = x.ResourcesConfigurations.FirstOrDefault(),
                            CountUsers = x.BillingServiceTypes.SelectMany(z => z.Resources).Select(z => z.Subject).Distinct().Count(),
                            ServiceCost = (x.ResourcesConfigurations.FirstOrDefault() != null && x.ResourcesConfigurations.FirstOrDefault()!.BillingAccounts.Account.AccountConfiguration.IsVip) ? x.ResourcesConfigurations.FirstOrDefault()!.Cost * (actualAgencyAgreement.ServiceOwnerRewardPercentForVipAccounts / 100)
                                : x.ResourcesConfigurations.FirstOrDefault() != null ? x.ResourcesConfigurations.FirstOrDefault()!.Cost : 0 * (actualAgencyAgreement.ServiceOwnerRewardPercent / 100)
                    })
                    .GroupBy(x => new
                    {
                        x.Service.Id,
                        x.Service.Name,
                        x.Service.BillingServiceStatus,
                        PartnerAccountCaption = x.Service.AccountOwner.AccountCaption,
                        PartnerAccountId = x.Service.AccountOwner.Id,
                        PartnerAccountIndexNumber = x.Service.AccountOwner.IndexNumber,
                        ServiceActivationDate = x.Service.ServiceActivationDate ?? DateTime.MinValue,
                        x.Service.StatusDateTime,
                        x.Service.IsActive
                    })
                    .Select(g => new PartnerBillingServiceDto
                    {
                        Id = g.Key.Id,
                        Name = g.Key.Name,
                        BillingServiceStatus = g.Key.BillingServiceStatus,
                        PartnerAccountCaption = g.Key.PartnerAccountCaption,
                        PartnerAccountId = g.Key.PartnerAccountId,
                        PartnerAccountIndexNumber = g.Key.PartnerAccountIndexNumber,
                        ServiceActivationDate = g.Key.ServiceActivationDate,
                        StatusDateTime = g.Key.StatusDateTime,
                        ServiceCost = g.Sum(x => x.ServiceCost),
                        CountUsers = g.Sum(z => z.CountUsers),
                        IsActive = g.Key.IsActive
                    })
                    .AutoFilter(request.Filter)
                    .Where(PartnerServiceSpecification.ByDate(request.Filter?.From, request.Filter?.To) &
                           PartnerServiceSpecification.BySearchLine(request.Filter?.SearchLine))
                    .AutoSort(request)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                    .ToPagedDto();

                return new PaginationDataResultDto<PartnerBillingServiceDto>(data.Records, data.Metadata.TotalItemCount,
                    request.PageSize ?? 50, request.PageNumber ?? 1).ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения сервисов партнера] {request.Filter!.PartnerAccountId}");

                return PagedListExtensions
                    .ToPreconditionFailedManagerResult<PaginationDataResultDto<PartnerBillingServiceDto>>(ex.Message);
            }
        }
    }
}
