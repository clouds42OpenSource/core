﻿using Clouds42.Billing.Contracts.BillingServices.Models;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.PartnersContext.Handlers.Specifications
{
    public class PartnerServiceSpecification
    {
        public static Spec<PartnerBillingServiceDto> BySearchLine(string? value)
        {
            var lowerValue = value?.ToLower();

            return new Spec<PartnerBillingServiceDto>(x =>
                string.IsNullOrEmpty(lowerValue) || x.PartnerAccountCaption.ToLower().Contains(lowerValue) ||
                x.PartnerAccountIndexNumber.ToString().ToLower().Contains(lowerValue));
        }
        public static Spec<PartnerBillingServiceDto> ByDate(DateTime? from, DateTime? to)
        {
            return new Spec<PartnerBillingServiceDto>(x => (!from.HasValue || x.ServiceActivationDate >= from) && (!to.HasValue || x.ServiceActivationDate <= to));
        }
    }
}
