﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.Feedback;

/// <summary>
/// Команда для обновления отзыва пользователя
/// /// </summary>

public class UpdateFeedbackCommand : IRequest<ManagerResult<bool>>
{
    /// <summary>
    /// Данные отзыва для обновления
    /// </summary>
    public FeedbackDto Feedback { get; set; }
}
