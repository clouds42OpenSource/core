﻿using AutoMapper;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.Feedback;

public class FeedbackDto : IMapFrom<Clouds42.Domain.DataModels.Feedback>
{
    public Guid Id { get; set; }
    public Guid AccountUserId { get; set; }
    public DateTime? Delay { get; set; }
    public DateTime? FeedBackLeftDate { get; set; }
    
    
    public void Mapping(Profile profile)
    {
        profile.CreateMap<Clouds42.Domain.DataModels.Feedback, FeedbackDto>();

    }
    
}

