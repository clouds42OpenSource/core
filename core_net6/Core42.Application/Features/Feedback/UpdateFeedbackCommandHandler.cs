﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.Feedback;

public class UpdateFeedbackCommandHandler(ILogger42 logger,
    IUnitOfWork unitOfWork,
    IAccessProvider accessProvider,
    IHandlerException handlerException) :  IRequestHandler<UpdateFeedbackCommand, ManagerResult<bool>>
{
    private readonly ILogger42 _logger = logger;
    private readonly IUnitOfWork _unitOfWork = unitOfWork;
    private readonly IAccessProvider _accessProvider = accessProvider;
    private readonly IHandlerException _handlerException = handlerException;

    public async Task<ManagerResult<bool>> Handle(UpdateFeedbackCommand request, CancellationToken cancellationToken)
    {
        try 
        {
            _logger.Info($"Попытка обновить фидбек пользователя {request.Feedback.AccountUserId}");
            var existingFeedback = await _unitOfWork.FeedbackRepository.FirstOrDefaultAsync(
                f => f.AccountUser.Id == request.Feedback.AccountUserId);

            if (existingFeedback == null)
            {
                var newFeedback = new Clouds42.Domain.DataModels.Feedback
                {
                    Id = Guid.NewGuid(),
                    AccountUserId = request.Feedback.AccountUserId,
                    Delay = request.Feedback.Delay, 
                    FeedbackLeftDate = null,
                };
                _logger.Info($"Новая запись по фидбеку для пользователя {newFeedback.AccountUserId}");
                 _unitOfWork.FeedbackRepository.Insert(newFeedback);
            }
            else
            {
                existingFeedback.Delay = request.Feedback.Delay ?? existingFeedback.Delay;
                existingFeedback.FeedbackLeftDate = request.Feedback.FeedBackLeftDate ?? existingFeedback.FeedbackLeftDate;
                _logger.Info($"Обновление существующей записи {existingFeedback.Id}. " +
                             $"Изменения: Delay = {(request.Feedback.Delay.HasValue ? "Обновлено" : "Без изменений")}, " +
                             $"FeedbackLeftDate = {(request.Feedback.FeedBackLeftDate.HasValue ? "Обновлено" : "Без изменений")}");                
                _unitOfWork.FeedbackRepository.Update(existingFeedback);
            }
            await _unitOfWork.SaveAsync();
            var logAction = request.Feedback.Delay == null ? LogActions.FeedbackLeft : LogActions.FeedbackDelayed;
            var account = await _unitOfWork.AccountsRepository.FirstOrDefaultAsync(a =>
                    a.AccountUsers.Any(u => u.Id == request.Feedback.AccountUserId));
            string description = logAction switch
            {
                LogActions.FeedbackLeft => $"Пользователь {request.Feedback.AccountUserId} оставил обратную связь",
                LogActions.FeedbackDelayed => $"Пользователь {request.Feedback.AccountUserId} отложил обратную связь на {request.Feedback.Delay} времени",
            };
            
            LogEventHelper.LogEvent(_unitOfWork,account.Id , _accessProvider, logAction, description);
            return true.ToOkManagerResult();
        }
        catch (Exception ex)
        {
            var message = $"Ошибка при обновлении фидбека для пользователя {request.Feedback.AccountUserId}";
            _handlerException.Handle(ex, message);
            return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(message);
        }
    }
}
