﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Queries.Filters
{
    /// <summary>
    /// Account database query filter
    /// </summary>
    public class AccountDatabaseItemsFilter: IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        ///  Search line 
        /// </summary>
        public string? SearchLine { get; set; }
    }
}
