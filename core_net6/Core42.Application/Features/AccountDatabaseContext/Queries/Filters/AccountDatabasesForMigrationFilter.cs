﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Queries.Filters
{
    /// <summary>
    /// Account database for migration query filter
    /// </summary>
    public class AccountDatabasesForMigrationFilter : IQueryFilter
    {
        /// <summary>
        /// Account index number
        /// </summary>
        public int? AccountIndexNumber { get; set; }

        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }

        /// <summary>
        /// Storage type, <c>true</c> - file, <c>false</c> - server, <c>null</c> - file and server
        /// </summary>
        public bool? IsTypeStorageFile { get; set; }

        /// <summary>
        /// Account id
        /// </summary>
        public Guid? AccountId { get; set; }    
    }
}
