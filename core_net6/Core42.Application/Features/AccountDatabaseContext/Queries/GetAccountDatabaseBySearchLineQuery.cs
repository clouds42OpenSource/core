﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AccountDatabaseContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.AccountDatabaseContext.Queries
{
    /// <summary>
    /// Get account database by search line query
    /// </summary>
    public class GetAccountDatabaseBySearchLineQuery : IHasSpecificSearch, IRequest<ManagerResult<List<AccountDatabaseInfoDto>>>
    {
        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }
    }
}
