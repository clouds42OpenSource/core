﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Queries
{
    public class CheckIfDatabaseNameExistsQuery(string name) : IRequest<ManagerResult<bool>>
    {
        public string Name { get; set; } = name;
    }

    public class CheckIfDatabaseNameExistsQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        ILogger42 logger)
        : IRequestHandler<CheckIfDatabaseNameExistsQuery, ManagerResult<bool>>
    {
        public async Task<ManagerResult<bool>> Handle(CheckIfDatabaseNameExistsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var databaseExists = await unitOfWork.DatabasesRepository
                    .AsQueryableNoTracking()
                    .AnyAsync(x => x.Caption == request.Name && x.AccountId == accessProvider.ContextAccountId && x.State == DatabaseState.Ready.ToString(),
                        cancellationToken);

                return databaseExists.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                const string message = "Ошибка проверки уникальности имени базы";
                logger.Error(ex, message);

                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(message);
            }
        }
    }
}
