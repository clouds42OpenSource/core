﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.BLL.Common.Extensions;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorkerTask;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Locales.Extensions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Queries
{
    public class GetLaunchDatabaseInstructionsQuery(Guid databaseId, Guid accountUserId)
        : IRequest<ManagerResult<AccountDatabaseLaunchInstructionsDto>>
    {
        public Guid DatabaseId { get; set; } = databaseId;
        public Guid AccountUserId { get; set; } = accountUserId;
    }

    public class GetLaunchDatabaseInstructionsQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        IConfiguration configuration,
        ICloudLocalizer cloudLocalizer,
        IDbObjectLockProvider dbObjectLockProvider)
        : IRequestHandler<GetLaunchDatabaseInstructionsQuery, ManagerResult<AccountDatabaseLaunchInstructionsDto>>
    {
        private readonly Lazy<int> _promisePaymentDateLazy = new(() => ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays"));

        public async Task<ManagerResult<AccountDatabaseLaunchInstructionsDto>> Handle(GetLaunchDatabaseInstructionsQuery request, CancellationToken cancellationToken)
        {
            using var uow = unitOfWork;

            try
            {
                using (await dbObjectLockProvider.AcquireLockAsync(nameof(GetLaunchDatabaseInstructionsQueryHandler),TimeSpan.FromSeconds(5)))
                {
                    var user = await uow.AccountUsersRepository
                        .AsQueryableNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == request.AccountUserId, cancellationToken);

                    accessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => user?.AccountId);

                    var accountDatabase = await uow.DatabasesRepository
                        .AsQueryableNoTracking()
                        .Include(i => i.Account)
                        .Include(x => x.AccountDatabaseOnDelimiter)
                        .ThenInclude(x => x.SourceAccountDatabase)
                        .FirstOrDefaultAsync(db => db.Id == request.DatabaseId, cancellationToken);

                    if (accountDatabase == null)
                        return PagedListExtensions
                            .ToPreconditionFailedManagerResult<AccountDatabaseLaunchInstructionsDto>(
                                $"Информационная база '{request.DatabaseId}' не найдена!");

                    if (user == null)
                        return PagedListExtensions
                            .ToPreconditionFailedManagerResult<AccountDatabaseLaunchInstructionsDto>(
                                $"Пользователь '{request.AccountUserId}' не найден!");
                    var launchInstructions = new AccountDatabaseLaunchInstructionsDto();

                        logger.Trace(
                            $"Начало проверки статуса сервиса пользователем '{user.Id}' и базы {accountDatabase.Id}");

                        var account = await uow.AccountsRepository
                            .AsQueryableNoTracking()
                            .Include(i => i.AccountConfiguration)
                            .ThenInclude(i => i.Segment)
                            .ThenInclude(x => x.Stable82PlatformVersionReference)
                            .Include(x => x.AccountConfiguration)
                            .ThenInclude(x => x.Segment)
                            .ThenInclude(x => x.Stable83PlatformVersionReference)
                            .Include(x => x.AccountConfiguration)
                            .ThenInclude(x => x.Segment)
                            .ThenInclude(x => x.Alpha83PlatformVersionReference)
                            .FirstOrDefaultAsync(x => x.Id == accountDatabase.AccountId, cancellationToken);

                        launchInstructions.ValidationErrorMessage =
                            await GetAcDbStatusDescriptionAsync(account!, accountDatabase, uow);

                        if (!string.IsNullOrEmpty(launchInstructions.ValidationErrorMessage))
                        {
                            logger.Warn(
                                $"База '{accountDatabase.Id}' не готова к запуску по причине {launchInstructions.ValidationErrorMessage}");
                            launchInstructions.LaunchAllowed = false;

                            return launchInstructions.ToOkManagerResult();
                        }

                        launchInstructions.LaunchAllowed = true;

                        var platformVersion =
                            GetPlatformVersion(accountDatabase, account!.AccountConfiguration.Segment);

                        launchInstructions.Path1CClient = platformVersion?.PathToPlatform;
                        launchInstructions.Path1CClientX64 = platformVersion?.PathToPlatfromX64;
                        launchInstructions.PlatformVersion = platformVersion?.Version;

                        return launchInstructions.ToOkManagerResult();
                }
            }

            catch (Exception ex)
            {
                logger.Warn(ex,"Ошибка при получении инструкции по запуску информационной базы.");

                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDatabaseLaunchInstructionsDto>(
                    ex.Message);
            }

            finally
            {
                uow.Dispose();
            }
            
        }

        private async Task<string?> GetAcDbStatusDescriptionAsync(Account account, AccountDatabase accountDatabase, IUnitOfWork uow)
        {

            if (accountDatabase.StateEnum != DatabaseState.Ready)
            {
                logger.Warn($"База '{accountDatabase.Id}' в статусе '{accountDatabase.StateEnum}' и не готова к запуску");
                return accountDatabase.StateEnum.Description();
            }

            var resConfig = await uow.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x =>
                    x.AccountId == account.Id && x.BillingService.SystemService == Clouds42Service.MyEnterprise);

            if (resConfig == null)
            {
                logger.Info($"У аккаунта {account.Id} сервис Аренды не найден.");

                return "У аккаунта сервис Аренды не активирован.";
            }

            logger.Trace("Получаем состояние сервисов");

            var serviceStatus = await CreateModelForRentAndMyDiskAsync(account, resConfig, uow);

            return serviceStatus.ServiceIsLocked switch
            {
                false => null,
                true when serviceStatus.PromisedPaymentIsActive =>
                    $"Использование сервиса остановлено. Пожалуйста, погасите обещанный платеж на сумму {serviceStatus.PromisedPaymentSum}",
                _ => serviceStatus.ServiceLockReason
            };
        }

        public async Task<ServiceStatusModelDto> CreateModelForRentAndMyDiskAsync(Account account, ResourcesConfiguration? resourcesConfiguration, IUnitOfWork uow)
        {
            ServiceStatusModelDto result = new ServiceStatusModelDto();
            
            if (resourcesConfiguration == null)
            {
                logger.Info($"У аккаунта {account.Id} сервис Аренда 1С не найдена.");

                return result;
            }
            
            result = await CreateServiceStatusModelAsync(account.Id, resourcesConfiguration, uow);

            if (result.ServiceIsLocked)
                return result;
            
            resourcesConfiguration = await uow.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.AccountId == account.Id && x.BillingService.SystemService == Clouds42Service.MyDisk);
            
            result = await CreateServiceStatusModelAsync(account.Id, resourcesConfiguration!, uow);

            var myDiskInfo = await GetMyDiskInfo(account, uow);

            if (myDiskInfo.MyDiskVolumeInfo.UsedSizeOnDisk <= myDiskInfo.MyDiskVolumeInfo.AvailableSize)
            {
                return result;
            }

            var diskInfo =
                $"Доступно: {myDiskInfo.MyDiskVolumeInfo.AvailableSize.MegaByteToClassicFormat(2)}. Используется: {myDiskInfo.MyDiskVolumeInfo.UsedSizeOnDisk.MegaByteToClassicFormat(2)}.";

            logger.Info($"У аккаунта {account.Id} нет свободного дискового пространства. {diskInfo}");

            return new ServiceStatusModelDto
            {
                ServiceLockReason = $"Нет свободного дискового пространства. {diskInfo}",
                ServiceLockReasonType = ServiceLockReasonType.NoDiskSpace,
                ServiceIsLocked = true
            };
        }

        public async Task<ServiceStatusModelDto> CreateServiceStatusModelAsync(Guid accountId, ResourcesConfiguration resConfig, IUnitOfWork uow)
        {
            var result = new ServiceStatusModelDto();
            
            var billingAccount = await uow.BillingAccountRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Id == accountId);

            var locale = (await uow.AccountsRepository
                .AsQueryableNoTracking()
                .Include(x => x.AccountConfiguration.Locale)
                .FirstOrDefaultAsync(x => x.Id == accountId))?.AccountConfiguration?.Locale ??
                         await uow.LocaleRepository
                                .AsQueryable()
                                .AsNoTracking()
                                .FirstOrDefaultAsync(x => x.ID == Guid.Parse(configuration["DefaultLocale"]!));
            
            var mainServiceResConfig = await GetMainConfigurationOrThrowExceptionAsync(resConfig, uow);

            result.ServiceIsLocked = mainServiceResConfig.FrozenValue || resConfig.FrozenValue;
            result.PromisedPaymentIsActive = billingAccount.HasPromisedPayment();

            if (result.PromisedPaymentIsActive)
            {
                result.PromisedPaymentSum = $"{billingAccount!.PromisePaymentSum!.Value:0.00} {locale!.Currency}.";
                result.PromisedPaymentExpireDate =
                    billingAccount.PromisePaymentDate!.Value.AddDays(_promisePaymentDateLazy.Value);
            }

            if (billingAccount.IsPromisedPaymentExpired())
            {
                result.ServiceIsLocked = true;
                result.ServiceLockReasonType = ServiceLockReasonType.OverduePromisedPayment;
                result.ServiceLockReason =
                    $" Cервис '{resConfig.GetLocalizedServiceName(cloudLocalizer)}' заблокирован по причине наличия обещанного платежа на сумму {result.PromisedPaymentSum}";
                logger.Info(
                    $"У аккаунта {accountId} сервис '{resConfig.BillingService.Name}' заблокирован по причине ОП на сумму {result.PromisedPaymentSum}");
                return result;
            }

            if (!result.ServiceIsLocked)
            {
                return result;
            }

            result.ServiceLockReasonType = ServiceLockReasonType.ServiceNotPaid;
            result.ServiceLockReason = GetServiceLockReason(resConfig, mainServiceResConfig);

            logger.Info($"У аккаунта {accountId} сервис {resConfig.BillingService.Name} заблокирован.");

            return result;
        }

        private static async Task<ResourcesConfiguration> GetMainConfigurationOrThrowExceptionAsync(ResourcesConfiguration resourceConfiguration, IUnitOfWork uow)
        {
            var mainServiceId =
                resourceConfiguration.BillingService.BillingServiceTypes.FirstOrDefault(w =>
                    w.DependServiceTypeId != null)?.DependServiceType.ServiceId;

            var mainResConfig = await uow.ResourceConfigurationRepository.FirstOrDefaultAsync(r =>
                r.AccountId == resourceConfiguration.AccountId && r.BillingService.Id == mainServiceId);

            if (mainResConfig == null && mainServiceId.HasValue && !resourceConfiguration.BillingService.IsHybridService)
                throw new NotFoundException(
                    $"Не удалось получить запись ResourceConfiguration основной услуги для аккаунта '{resourceConfiguration.AccountId}' по услуге '{resourceConfiguration.BillingService.Name}'");

            return mainResConfig ?? resourceConfiguration;
        }
        
        private string GetServiceLockReason(ResourcesConfiguration resConfig,
            ResourcesConfiguration mainServiceResConfig)
        {
            var rent1CName = resConfig.GetLocalizedServiceName(cloudLocalizer);

            if (mainServiceResConfig.BillingServiceId != resConfig.BillingServiceId && mainServiceResConfig.FrozenValue)
                return $"Работа с сервисом “{rent1CName}” возможна " +
                       $"при активном сервисе “{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, resConfig.AccountId)}”.";

            return resConfig.IsDemoPeriod ? $"У сервиса '{rent1CName}' завершился демо период" : $"Сервис '{rent1CName}' заблокирован.";
        }

        public static async Task<ServiceMyDiskDto> GetMyDiskInfo(Account account, IUnitOfWork uow, long availableSizeOnDisk = 0)
        {
            var dataBases = await uow.DatabasesRepository
                .AsQueryableNoTracking()
                .Where(d => d.AccountId == account.Id && d.State != DatabaseState.DeletedToTomb.ToString()
                                         && d.State != DatabaseState.DelitingToTomb.ToString()
                                         && d.State != DatabaseState.DetachingToTomb.ToString()
                                         && d.State != DatabaseState.DeletedFromCloud.ToString()).ToListAsync();

            long serverDataBaseSize = 0, fileDataBaseSize = 0;

            if (availableSizeOnDisk == 0)
                availableSizeOnDisk += await GetAvailableSpaceOnDiskAsync(account.Id, uow);
        
            var serverDataBases = dataBases.Where(d => d.IsFile is null or false).ToList();
            serverDataBaseSize = serverDataBases.Aggregate(serverDataBaseSize, (current, serverDb) => current + serverDb.SizeInMB);

            var fileDataBases = dataBases.Where(d => d.IsFile == true).ToList();
            fileDataBaseSize = fileDataBases.Aggregate(fileDataBaseSize, (current, fileDb) => current + fileDb.SizeInMB);

            var myDiskPropertiesByAccount = await GetIfExistsOrCreateNewAsync(account.Id, uow);

            var clientFileSize = myDiskPropertiesByAccount.MyDiskFilesSizeInMb;
            

            var myDiskInfoModelDto = new ServiceMyDiskDto
            {
                MyDiskVolumeInfo = new MyDiskVolumeInfoDto
                {
                    AvailableSize = availableSizeOnDisk,
                    ClientFilesSize = clientFileSize,
                    ServerDataBaseSize = serverDataBaseSize,
                    FileDataBaseSize = fileDataBaseSize,
                }
            };

            return myDiskInfoModelDto;
        }
        
        public static async Task<long> GetAvailableSpaceOnDiskAsync(Guid accountId, IUnitOfWork uow)
        {
            var payedSpaceOnDisk = await GetPayedSpaceOnDiskAsync(accountId, uow);
            return payedSpaceOnDisk > 0 ? payedSpaceOnDisk : Clouds42SystemConstants.DefaultAvailableMemorySize;
        }
        
        public static async Task<long> GetPayedSpaceOnDiskAsync(Guid accountId, IUnitOfWork uow)
        {
            var companyInfo = await uow.AccountsRepository.AsQueryableNoTracking().FirstOrDefaultAsync(c => c.Id == accountId);

            if (companyInfo == null)
                return 0;

            var resource = await uow.DiskResourceRepository.AsQueryableNoTracking().FirstOrDefaultAsync(r => r.Resource.AccountId == accountId
                                                                               && r.Resource.Subject == accountId);

            if (resource == null)
                return 0;

            return resource.VolumeGb * 1024;
        }

        public static async Task<MyDiskPropertiesByAccount> GetIfExistsOrCreateNewAsync(Guid accountId, IUnitOfWork uow)
        {
            var myDiskPropertiesByAccount = await uow
                .GetGenericRepository<MyDiskPropertiesByAccount>()
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(diskProperties => diskProperties.AccountId == accountId);

            if (myDiskPropertiesByAccount != null)
            {
                return myDiskPropertiesByAccount;
            }

            var newDiskProp = new MyDiskPropertiesByAccount { AccountId = accountId };
            uow.GetGenericRepository<MyDiskPropertiesByAccount>().Insert(newDiskProp);
            await uow.SaveAsync();

            return newDiskProp;
        }
        
        public PlatformVersionReference? GetPlatformVersion(AccountDatabase accountDatabase, CloudServicesSegment segment)
        {
            if (accountDatabase.IsDelimiter())
                return GetPlatformVersionForDelimiterDatabase(accountDatabase, segment);

            if (accountDatabase.IsFile == false)
                return accountDatabase.PlatformType == PlatformType.V82
                    ? segment.Stable82PlatformVersionReference
                    : segment.Stable83PlatformVersionReference;

            if (accountDatabase.DistributionTypeEnum != DistributionType.Alpha)
            {
                return accountDatabase.PlatformType == PlatformType.V82
                    ? segment.Stable82PlatformVersionReference
                    : segment.Stable83PlatformVersionReference;
            }

            if (segment.Alpha83PlatformVersionReference == null)
                throw new InvalidOperationException("Альфа версия 8.3 не выбрана.");

            return segment.Alpha83PlatformVersionReference;

        }

        private static PlatformVersionReference? GetPlatformVersionForDelimiterDatabase(AccountDatabase accountDatabase, CloudServicesSegment segment)
        {
            if (accountDatabase.StateEnum is DatabaseState.ErrorCreate or DatabaseState.NewItem)
                return null;
            var serviceAccountIdForDemoDelimiterDatabases = CloudConfigurationProvider.DbTemplateDelimiters
                .GetServiceAccountIdForDemoDelimiterDatabases();
            var accountId = accountDatabase.IsDemoDelimiter() && accountDatabase.AccountDatabaseOnDelimiter.Zone == null
                ? serviceAccountIdForDemoDelimiterDatabases
                : accountDatabase.AccountDatabaseOnDelimiter.SourceAccountDatabase?.AccountId;

            if (!accountId.HasValue)
                throw new NotFoundException(GenerateErrorMessageAboutReceivingStable83PlatformVersion(accountDatabase));

            return segment.Stable83PlatformVersionReference ??
                   throw new NotFoundException(
                       GenerateErrorMessageAboutReceivingStable83PlatformVersion(accountDatabase));
        }

        private static string? GenerateErrorMessageAboutReceivingStable83PlatformVersion(AccountDatabase accountDatabase)
        {
            var serviceAccountIdForDemoDelimiterDatabases = CloudConfigurationProvider.DbTemplateDelimiters.GetServiceAccountIdForDemoDelimiterDatabases();

            return accountDatabase.IsDemoDelimiter() && accountDatabase.AccountDatabaseOnDelimiter.Zone == null
                 ? $"Не удалось получить версию платформы для демо базы на разделителях {accountDatabase.Id} по служебному аккаунту {serviceAccountIdForDemoDelimiterDatabases}"
                 : $"Не удалось получить версию платформы для базы на разделителях {accountDatabase.Id}";
        }


    }
}
