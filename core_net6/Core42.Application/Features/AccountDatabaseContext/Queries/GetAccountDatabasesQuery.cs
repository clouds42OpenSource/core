﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Features.AccountDatabaseContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Queries
{
    /// <summary>
    /// Get filtered, sorted and paginated databases query
    /// </summary>
    public class GetAccountDatabasesQuery : IHasFilter<AccountDatabaseItemsFilter>, ISortedQuery, IPagedQuery, IRequest<ManagerResult<PagedDto<AccountDatabaseItemDto>>>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(AccountDatabase.CreationDate)}.desc";

        /// <summary>
        /// Account database items filter
        /// </summary>
        public AccountDatabaseItemsFilter? Filter { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }
    }
}
