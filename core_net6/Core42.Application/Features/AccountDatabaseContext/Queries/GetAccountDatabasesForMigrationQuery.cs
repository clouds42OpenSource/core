﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Features.AccountDatabaseContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Queries
{
    /// <summary>
    /// Get filtered, sorted and paginated account databases for migration query
    /// </summary>
    public class GetAccountDatabasesForMigrationQuery : ISortedQuery, IHasFilter<AccountDatabasesForMigrationFilter>, IPagedQuery, IRequest<ManagerResult<AccountDatabasesForMigrationResultDto>>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(AccountDatabase.V82Name)}.asc";

        /// <summary>
        /// Database for migration filter
        /// </summary>
        public AccountDatabasesForMigrationFilter? Filter { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }
    }
}
