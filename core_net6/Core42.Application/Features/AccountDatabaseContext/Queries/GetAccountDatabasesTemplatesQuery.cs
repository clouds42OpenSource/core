﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using MediatR;

namespace Core42.Application.Features.AccountDatabaseContext.Queries
{
    /// <summary>
    /// Query to get all databases tempaltes for the account
    /// </summary>
    public class GetAccountDatabasesTemplatesQuery : IRequest<ManagerResult<CreateAcDbFromTemplateDataDto>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Show only templatesfor databases on delimiters
        /// </summary>
        public bool OnlyTemplatesOnDelimiters { get; set; }
    }
}
