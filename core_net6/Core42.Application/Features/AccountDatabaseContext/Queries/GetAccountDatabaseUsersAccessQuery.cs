﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Enums;
using MediatR;

namespace Core42.Application.Features.AccountDatabaseContext.Queries
{
    public class GetAccountDatabaseUsersAccessQuery: IRequest<ManagerResult<DatabaseCardAcDbAccessesDataDto>>
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Строка поиска (логин, эл. почта, фио)
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// Тип фильтра пользователей по доступам
        /// </summary>
        public UsersByAccessFilterTypeEnum UsersByAccessFilterType { get; set; }
    }
}
