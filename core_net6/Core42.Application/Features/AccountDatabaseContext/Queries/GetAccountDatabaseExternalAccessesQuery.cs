﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using MediatR;

namespace Core42.Application.Features.AccountDatabaseContext.Queries;

public class GetAccountDatabaseExternalAccessesQuery: IRequest<IQueryable<AcDbAccessDataDto>>
{
    public Guid AccountDatabaseId { get; set; }
}
