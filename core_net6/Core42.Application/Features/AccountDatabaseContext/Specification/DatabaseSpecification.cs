﻿using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Specification
{
    /// <summary>
    /// Account database specification
    /// </summary>
    public static class DatabaseSpecification
    {
        /// <summary>
        /// Filter account databases by search line
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<AccountDatabase> BySearchLine(string? searchLine)
        {
            var zone = searchLine.ToInt();
            return new(rec => string.IsNullOrEmpty(searchLine) || 
                           rec.Caption != null && rec.Caption.Contains(searchLine) ||
                           rec.V82Name != null && rec.V82Name.Contains(searchLine) ||
                           rec.Account.AccountCaption != null && rec.Account.AccountCaption.Contains(searchLine) ||
                           rec.Account.IndexNumber.ToString().Contains(searchLine) ||
                           rec.DbNumber.ToString().Contains(searchLine) ||
                           rec.AccountDatabaseOnDelimiter != null &&  rec.AccountDatabaseOnDelimiter.Zone == zone);
        }
        
        /// <summary>
        /// Filter account databases by account is not null
        /// </summary>
        /// <returns></returns>
        public static Spec<AccountDatabase> ByAccountNotNull()
        {
            return new(item => item.Account != null);
        }

        /// <summary>
        /// Filter account databases by state not equal
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public static Spec<AccountDatabase> ByStateNotEquals(DatabaseState state)
        {
            return new(item => item.State != state.ToString());
        }

        /// <summary>
        /// Filter account databases by v82 name or size in MB
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<AccountDatabase> ByV82NameOrSizeInMb(string? searchLine)
        {
            return new(rec => string.IsNullOrEmpty(searchLine) || rec.V82Name != null && rec.V82Name.Contains(searchLine) || rec.SizeInMB.ToString().Contains(searchLine));
        }

        /// <summary>
        /// Filter account databases by is file flag
        /// </summary>
        /// <param name="isTypeStorageFile"></param>
        /// <returns></returns>
        public static Spec<AccountDatabase> ByIsFile(bool? isTypeStorageFile)
        {
            return new(rec => !isTypeStorageFile.HasValue || rec.IsFile == isTypeStorageFile);
        }

        /// <summary>
        /// Filter account databases by account index number
        /// </summary>
        /// <param name="indexNumber"></param>
        /// <returns></returns>
        public static Spec<AccountDatabase> ByAccountIndexNumber(int indexNumber)
        {
            return new(rec => rec.Account.IndexNumber == indexNumber);
        }

        /// <summary>
        /// Filter account databases by is ready state
        /// </summary>
        /// <returns></returns>
        public static Spec<AccountDatabase> ByReadyState()
        {
            return new(rec => rec.State == DatabaseState.Ready.ToString());
        }

        /// <summary>
        /// Filter account databases by is not on delimiters
        /// </summary>
        /// <returns></returns>
        public static Spec<AccountDatabase> ByIsNotOnDelimiter()
        {
            return new(rec => rec.AccountDatabaseOnDelimiter == null);
        }
    }
}
