﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Specification;

public static class AccountDatabaseAccessSpecification
{
    public static Spec<AcDbAccessDataDto> ByUsersAccess(UsersByAccessFilterTypeEnum usersAccess)
    {
        return new Spec<AcDbAccessDataDto>(x => usersAccess == UsersByAccessFilterTypeEnum.All ||
                                                usersAccess == UsersByAccessFilterTypeEnum.ExternalUsers && x.IsExternalAccess ||
                                                usersAccess == UsersByAccessFilterTypeEnum.WithAccess && x.HasAccess && x.State == AccountDatabaseAccessState.Done ||
                                                usersAccess == UsersByAccessFilterTypeEnum.WithoutAccess && !x.HasAccess && x.State != AccountDatabaseAccessState.Done);
    }

    public static Spec<AcDbAccessDataDto> BySearchString(string searchString)
    {
        return new Spec<AcDbAccessDataDto>(x => string.IsNullOrEmpty(searchString)
                                                || x.UserLastName.ToLower().Contains(searchString.ToLower())
                                                || x.UserFirstName.ToLower().Contains(searchString.ToLower())
                                                || x.UserMiddleName.ToLower().Contains(searchString.ToLower())
                                                || x.UserLogin.ToLower().Contains(searchString.ToLower())
                                                || !string.IsNullOrEmpty(x.UserEmail) && x.UserEmail.ToLower().Contains(searchString.ToLower()));
    }
}
