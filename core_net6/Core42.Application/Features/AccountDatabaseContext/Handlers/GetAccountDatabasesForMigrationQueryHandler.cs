﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using Core42.Application.Features.AccountDatabaseContext.Specification;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Handlers
{
    /// <summary>
    /// Get filtered, sorted and paginated account databases for migration query handler
    /// </summary>
    public class GetAccountDatabasesForMigrationQueryHandler(
        ILogger42 logger,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ISegmentHelper segmentHelper)
        : IRequestHandler<GetAccountDatabasesForMigrationQuery, ManagerResult<AccountDatabasesForMigrationResultDto>>
    {
        /// <summary>
        /// Handle get filtered, sorted and paginated account databases for migration query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<AccountDatabasesForMigrationResultDto>> Handle(GetAccountDatabasesForMigrationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                if (request.Filter != null)
                {
                    Account? account = null;
                    if (request.Filter.AccountId.HasValue)
                    {
                        account = await unitOfWork.AccountsRepository
                            .AsQueryable()
                            .Include(x => x.AccountConfiguration)
                            .FirstOrDefaultAsync(ac => ac.Id == request.Filter.AccountId, cancellationToken);
                    }

                    else if (request.Filter.AccountIndexNumber.HasValue)
                    {
                        account = await unitOfWork.AccountsRepository
                            .AsQueryable()
                            .Include(x => x.AccountConfiguration)
                            .FirstOrDefaultAsync(ac => ac.IndexNumber == request.Filter.AccountIndexNumber, cancellationToken);
                    }

                    if (account == null)
                        return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDatabasesForMigrationResultDto>($"Аккаунт с индексом {request.Filter?.AccountIndexNumber.ToString() ?? "не определен"} " +
                            $"или идентификатором {request.Filter?.AccountId.ToString() ?? "не определен"} не найден");

                    request.Filter.AccountIndexNumber = account.IndexNumber;

                    accessProvider.HasAccess(ObjectAction.AccountDatabase_MigrationListView, () => account.Id);

                    var records = unitOfWork.DatabasesRepository
                                    .AsQueryable()
                                    .Include(d => d.AccountDatabaseOnDelimiter)
                                    .Include(d => d.CloudServicesFileStorageServer)
                                    .Where(DatabaseSpecification.ByAccountIndexNumber(request.Filter.AccountIndexNumber.Value))
                                    .Where(DatabaseSpecification.ByIsNotOnDelimiter())
                                    .Where(DatabaseSpecification.ByReadyState());

                    var pagedAndFilteredRecords = await records
                        .Where(DatabaseSpecification.ByV82NameOrSizeInMb(request?.Filter?.SearchLine))
                        .Where(DatabaseSpecification.ByIsFile(request?.Filter?.IsTypeStorageFile))
                        .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                        .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 30, cancellationToken);

                    var mappedRecords = new List<AccountDatabaseMigrationDto>();

                    foreach (var db in pagedAndFilteredRecords)
                    {
                        var mappedItem = mapper.Map<AccountDatabaseMigrationDto>(db);
                        mappedItem.Path = new AccountDatabasePathHelper(unitOfWork, logger, segmentHelper, handlerException).GetPath(db);
                        mappedRecords.Add(mappedItem);
                    }

                    var totalInfoModel = await GetFileStorageSummaryModel(records, cancellationToken);

                    var available = await GetAvailableFileStorages(account.AccountConfiguration.SegmentId, cancellationToken);
                    var availableStoragePath = totalInfoModel.Select(x => x.PathStorage).OrderByDescending(x => x).ToList();
                    var numberOfDatabasesToMigrate = ConfigurationHelper.GetConfigurationValue<int>("LimitMigrationDatabases");

                    return new AccountDatabasesForMigrationResultDto
                    {
                        Databases = mappedRecords
                            .ToPagedList(request?.PageNumber ?? 1, request?.PageSize ?? 30)
                            .ToPagedDto(),
                        FileStorageAccountDatabasesSummary = totalInfoModel,
                        AvalableFileStorages = available,
                        AvailableStoragePath = availableStoragePath,
                        NumberOfDatabasesToMigrate = numberOfDatabasesToMigrate
                    }.ToOkManagerResult();

                }
                else
                    return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDatabasesForMigrationResultDto>("Не найден фильтр запроса, не удалось обнаружить аккаунт");
            }


            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Произошла ошибка при получении информационных баз аккаунта]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDatabasesForMigrationResultDto>(ex.Message);
            }
        }

        /// <summary>
        /// Get file storage summary model
        /// </summary>
        /// <param name="accountDatabases"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task<List<FileStorageDto>> GetFileStorageSummaryModel(IQueryable<AccountDatabase> accountDatabases, CancellationToken cancellationToken)
        {
            var storages = await unitOfWork.CloudServicesFileStorageServerRepository.AsQueryableNoTracking().ToListAsync(cancellationToken);
            var storageGroupsWithFileDatabases = accountDatabases.Where(x => x.IsFile.HasValue && x.IsFile.Value).GroupBy(x => x.FileStorageID).ToArray();

            var totalInfoModel = storageGroupsWithFileDatabases
                .Select(group => new FileStorageDto
                {
                    FileStorage = storages.FirstOrDefault(x => x.ID == group.Key)?.Name ?? "Не указано",
                    DatabaseCount = group.Count(),
                    PathStorage = storages.FirstOrDefault(x => x.ID == group.Key)?.ConnectionAddress ?? "",
                    TotatlSizeOfDatabases = group.Sum(database => database.SizeInMB)
                })
                .OrderBy(x => x.FileStorage)
                .ToList();

            var servDatabases = accountDatabases
                .Where(x => !x.IsFile.HasValue || !x.IsFile.Value)
                .ToList();

            totalInfoModel.Add(new FileStorageDto
            {
                FileStorage = "Серверные базы",
                DatabaseCount = servDatabases.Count,
                TotatlSizeOfDatabases = servDatabases.Sum(x => x.SizeInMB)
            });

            return totalInfoModel.ToList();
        }

        /// <summary>
        /// Get available file storages by segmentId
        /// </summary>
        private async Task<List<AvailableCloudStoragesDictionaryDto>> GetAvailableFileStorages(Guid segmentId, CancellationToken cancellationToken)
        {
            var storageIds = await unitOfWork.CloudServicesSegmentStorageRepository
                .AsQueryableNoTracking()
                .Where(x => x.SegmentID == segmentId)
                .Select(x => x.FileStorageID)
                .ToListAsync(cancellationToken);

            return await unitOfWork.CloudServicesFileStorageServerRepository
                .AsQueryable()
                .Where(x => storageIds.Contains(x.ID))
                .ProjectTo<AvailableCloudStoragesDictionaryDto>(mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
    }
}
