﻿using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Handlers
{
    /// <summary>
    /// Hander to get databases templates for the account
    /// </summary>
    public class GetAccountDatabasesTemplatesQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ICreateAccountDatabaseDataProvider createAccountDatabaseDataProvider)
        : IRequestHandler<GetAccountDatabasesTemplatesQuery, ManagerResult<CreateAcDbFromTemplateDataDto>>
    {
        /// <summary>
        /// Handle a query to get databases templates for the account
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<CreateAcDbFromTemplateDataDto>> Handle(GetAccountDatabasesTemplatesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => request.AccountId);
                return (await createAccountDatabaseDataProvider.GetDataForCreateAcDbFromTemplate(request.AccountId, request.OnlyTemplatesOnDelimiters))
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Во время получения доступных шаблонов для создания баз произошла ошибка]. " +
                    $"Аккаунт {request.AccountId}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<CreateAcDbFromTemplateDataDto>(ex.Message);
            }
        }
    }
}
