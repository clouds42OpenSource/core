﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using Core42.Application.Features.AccountDatabaseContext.Specification;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Handlers;

public class GetAccountDatabaseUsersAccessQueryHandler(
    IAccessProvider accessProvider,
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    ISender sender,
    IMyDatabasesServiceTypeBillingDataProvider myDatabasesServiceTypeBillingDataProvider,
    IConfiguration configuration) : IRequestHandler<GetAccountDatabaseUsersAccessQuery, ManagerResult<DatabaseCardAcDbAccessesDataDto>>
{
    public async Task<ManagerResult<DatabaseCardAcDbAccessesDataDto>> Handle(GetAccountDatabaseUsersAccessQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var accountDatabase = await unitOfWork.DatabasesRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Id == request.DatabaseId, cancellationToken) ?? throw new InvalidOperationException($"Не удалось получить Id аккаунта по Id базы {request.DatabaseId}");

            await accessProvider.HasAccessAsync(ObjectAction.AccountDatabase_GetAcDbAccesses, accountDatabase.AccountId, optionalCheck: async () => await AccountHasAccessToDataBase(accessProvider.ContextAccountId, accountDatabase, cancellationToken));


            var accountId = accountDatabase.AccountId;

            var myDatabasesBillingServiceInfo = myDatabasesServiceTypeBillingDataProvider
                                                    .GetBillingDataForMyDatabasesServiceTypes(accountId)
                                                    .FirstOrDefault(st => st.SystemServiceType == ResourceType.ServerDatabasePlacement)
                                                ?? throw new NotFoundException($"Не удалось получить данные биллинга для услуги {ResourceType.ServerDatabasePlacement}");

            var clientServerServiceType = await unitOfWork.BillingServiceTypeRepository
                                              .AsQueryable()
                                              .FirstOrDefaultAsync(st => st.SystemServiceType == ResourceType.AccessToServerDatabase, cancellationToken)
                                          ?? throw new InvalidOperationException($"Не удалось найти услугу {ResourceType.AccessToServerDatabase}");

            var accountRate = await unitOfWork.AccountRateRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.AccountId == accountId && x.BillingServiceTypeId == clientServerServiceType.Id, cancellationToken);

            var rateService = await unitOfWork.RateRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(r => r.BillingServiceTypeId == clientServerServiceType.Id, cancellationToken);

            var internalAccesses =
                unitOfWork.DatabasesRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.Account)
                    .ThenInclude(x => x.BillingAccount)
                    .ThenInclude(x => x.Resources.Where(r => r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb || r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser))
                    .Include(x => x.AcDbAccesses)
                    .ThenInclude(x => x.ManageDbOnDelimitersAccess)
                    .Join(unitOfWork.AccountUsersRepository.GetNotDeletedAccountUsersQuery(), z => z.AccountId,
                        y => y.AccountId, (database, user) => new { database, user })
                    .Where(x => x.database.Id == request.DatabaseId &&
                                (x.database.Account.BillingAccount.Resources.Any(z => z.Subject == x.user.Id) || x.database.AcDbAccesses.Any()))
                    .Select(x => new AcDbAccessDataDto
                    {
                        UserId = x.user.Id,
                        UserLogin = x.user.Login,
                        UserEmail = x.user.Email,
                        UserFirstName = x.user.FirstName,
                        UserLastName = x.user.LastName,
                        UserMiddleName = x.user.MiddleName,
                        AccountCaption = x.database.Account.AccountCaption,
                        AccountIndexNumber = x.database.Account.IndexNumber,
                        HasAccess = x.database.AcDbAccesses.Any(z => z.AccountUserID == x.user.Id),
                        IsExternalAccess = false,
                        State =
                            x.database.AcDbAccesses.FirstOrDefault(z =>
                                z.AccountUserID == x.user.Id) != null
                                ? x.database.AcDbAccesses.FirstOrDefault(z =>
                                    z.AccountUserID == x.user.Id)!.ManageDbOnDelimitersAccess != null
                                    ?
                                    x.database.AcDbAccesses.FirstOrDefault(z =>
                                            z.AccountUserID == x.user.Id)!.ManageDbOnDelimitersAccess
                                        .AccessState
                                    : x.database.AcDbAccesses.FirstOrDefault(z =>
                                        z.AccountUserID == x.user.Id) != null
                                        ? AccountDatabaseAccessState.Done
                                        :
                                        AccountDatabaseAccessState.Undefined
                                : AccountDatabaseAccessState.Undefined
                    });

            var databaseAccesses = await internalAccesses
                .Concat(await sender.Send(new GetAccountDatabaseExternalAccessesQuery { AccountDatabaseId = request.DatabaseId }, cancellationToken))
                .Where(AccountDatabaseAccessSpecification.ByUsersAccess(request.UsersByAccessFilterType) & AccountDatabaseAccessSpecification.BySearchString(request.SearchString))
                .OrderByDescending(data => data.HasAccess)
                .ThenByDescending(data => data.IsExternalAccess)
                .ToListAsync(cancellationToken);

            return new DatabaseCardAcDbAccessesDataDto
            {
                DatabaseAccesses = databaseAccesses,
                RateData = myDatabasesBillingServiceInfo,
                Currency = (await unitOfWork.AccountsRepository.GetLocaleAsync(accountId, Guid.Parse(configuration["DefaultLocale"]!))).Currency,
                ClientServerAccessCost = (accountRate?.Cost ?? rateService?.Cost) ?? 0
            }.ToOkManagerResult();

        }

        catch (Exception ex)
        {
            logger.Warn(ex, $"[Ошибка получения данных доступов информационной базы]  '{request.DatabaseId}'.");

            return PagedListExtensions.ToPreconditionFailedManagerResult<DatabaseCardAcDbAccessesDataDto>(ex.Message);
        }
    }

    /// <summary>
    /// Определить, имеет ли аккаунт доступ к базе данных
    /// </summary>
    /// <param name="accountId">Идентификатор аккаунта</param>
    /// <param name="accountDatabase"></param>
    /// <param name="cancellationToken"></param>
    /// <returns>true - есть доступ, false - нет доступа</returns>
    public async Task<bool> AccountHasAccessToDataBase(Guid accountId, AccountDatabase accountDatabase, CancellationToken cancellationToken)
    {
        var account = await unitOfWork.AccountsRepository
            .AsQueryableNoTracking()
            .Include(x => x.AccountUsers)
            .FirstOrDefaultAsync(a => a.Id == accountId, cancellationToken);

        if (account == null)
            return false;

        if (accountDatabase.AccountId == accountId)
            return true;

        var accountUsersOfAccount = account.AccountUsers.Select(a => a.Id);

        return await unitOfWork.AcDbAccessesRepository
            .AsQueryableNoTracking()
            .AnyAsync(a => a.AccountDatabaseID == accountDatabase.Id &&
                           a.AccountUserID != null &&
                           accountUsersOfAccount.Contains(a.AccountUserID.Value), cancellationToken);
    }
}
