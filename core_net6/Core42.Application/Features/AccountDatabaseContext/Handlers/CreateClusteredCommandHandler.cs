﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorker.JobWrappers.FileBaseToServerTransformation;
using Clouds42.DataContracts.AccountDatabase.DatabasesPlacement;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.MyDatabaseService.Contracts.BuyDatabasePlacement.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Features.AccountDatabaseContext.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Handlers;

public class CreateClusteredCommandHandler(
    IHandlerException handlerException,
    IUnitOfWork unitOfWork,
    IAccessProvider accessProvider,
    IFileBaseToServerTransformationWrapper fileBaseToServerTransformationWrapper,
    ILogger42 logger,
    IBuyDatabasesPlacementProvider buyDatabasesPlacementProvider)
    : IRequestHandler<CreateClusteredCommand, ManagerResult<bool>>
{
    public async Task<ManagerResult<bool>> Handle(CreateClusteredCommand request, CancellationToken cancellationToken)
    {
        try
        {
            if (!await CredentialsExist(request.Login, request.Password, cancellationToken))
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Был передан неверный логин или пароль");
            }

            var database =
                await unitOfWork.DatabasesRepository.FirstOrDefaultAsync(d => d.Id == request.AccountDatabaseId);
            PayServerPlacement(database);
            database.StateEnum = DatabaseState.TransferDb ;
            logger.Info($"Перевод файловой базы в серверную: начинаю выполнение задачи с параметрами: " +
                         $"AccountDatabaseId={request.AccountDatabaseId}, AccountId={request.AccountId}, AccountUserId={request.AccountUserId}, " +
                         $"IsCopy={request.IsCopy}, Login={request.Login}, Phone={request.PhoneNumber}");
            fileBaseToServerTransformationWrapper.Start(new FileBaseToServerTransformationParams
            {
                AccountDatabaseId = request.AccountDatabaseId,
                AccountId = request.AccountId,
                AccountUserId = request.AccountUserId,
                IsCopy = request.IsCopy,
                Login = request.Login,
                Password = request.Password,
                PhoneNumber = request.PhoneNumber,
            });
            await unitOfWork.SaveAsync();
            return true.ToOkManagerResult();
        }
        catch (Exception ex)
        {
            LogEventHelper.LogEvent(
                unitOfWork,
                request.AccountId,
                accessProvider,
                LogActions.FileToClusterTransformationFailed, 
                ex.Message);
            handlerException.Handle(ex, ex.Message);
            return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
        }
    }
    
    /// <summary>
    /// Оплатить серверное место
    /// </summary>
    /// <param name="database"></param>
    /// <exception cref="PreconditionException"></exception>
    private void PayServerPlacement(AccountDatabase database)
    {
        if (database.IsFile != null && database.IsFile.Value)
        {
            var databasesPlacementResult = buyDatabasesPlacementProvider.BuyDatabasesPlacement(new BuyDatabasesPlacementDto
            {
                AccountId = database.Account.Id,
                SystemServiceType = ResourceType.ServerDatabasePlacement,
                IsPromisePayment = false,
                DatabasesForPlacementCount = 1,
                PaidDate = DateTime.UtcNow,
            });

            if (!databasesPlacementResult.IsComplete)
                throw new PreconditionException("Ошибка оплаты размещения серверной базы");
        }
    }
    private async Task<bool> CredentialsExist(string login, string password, CancellationToken cancellationToken) =>
        await unitOfWork.AcDbSupportRepository.AsQueryableNoTracking().AnyAsync(s => s.Login == login && s.Password == password, cancellationToken);
}
