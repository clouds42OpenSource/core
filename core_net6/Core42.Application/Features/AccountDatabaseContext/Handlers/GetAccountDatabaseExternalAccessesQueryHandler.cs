﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.AccountDatabaseContext.Handlers;

public class GetAccountDatabaseExternalAccessesQueryHandler(IUnitOfWork unitOfWork) : IRequestHandler<GetAccountDatabaseExternalAccessesQuery, IQueryable<AcDbAccessDataDto>>
{
    public Task<IQueryable<AcDbAccessDataDto>> Handle(GetAccountDatabaseExternalAccessesQuery request, CancellationToken cancellationToken)
    {
        return Task.FromResult(unitOfWork.AcDbAccessesRepository
            .AsQueryableNoTracking()
            .Include(x => x.Database)
            .Include(x => x.AccountUser)
            .ThenInclude(x => x.Account)
            .Include(x => x.ManageDbOnDelimitersAccess)
            .Where(x => x.AccountDatabaseID == request.AccountDatabaseId && x.AccountID != x.Database.AccountId)
            .Select(x => new AcDbAccessDataDto
            {
                UserId = x.AccountUser.Id,
                UserLogin = x.AccountUser.Login,
                UserEmail = x.AccountUser.Email,
                UserFirstName = x.AccountUser.FirstName,
                UserLastName = x.AccountUser.LastName,
                UserMiddleName = x.AccountUser.MiddleName,
                AccountCaption = x.AccountUser.Account.AccountCaption,
                AccountIndexNumber = x.AccountUser.Account.IndexNumber,
                HasAccess = true,
                IsExternalAccess = true,
                State = x.ManageDbOnDelimitersAccess != null
                    ? x.ManageDbOnDelimitersAccess.AccessState
                    : AccountDatabaseAccessState.Done
            }));
    }
}
