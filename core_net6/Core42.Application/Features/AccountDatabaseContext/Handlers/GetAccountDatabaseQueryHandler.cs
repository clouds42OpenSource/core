﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.AccountDatabase.Contracts.Card.Interfaces;
using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.DataModels;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Microsoft.Extensions.Configuration;

namespace Core42.Application.Features.AccountDatabaseContext.Handlers
{

    /// <summary>
    /// Get account database by number query handler
    /// </summary>
    public class GetAccountDatabaseQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ICloudLocalizer cloudLocalizer,
        IDatabasePlatformVersionHelper platformVersionHelper,
        IMyDatabasesServiceTypeDataProvider myDatabasesServiceTypeDataProvider,
        IAccountDatabasesAdditionalDataProvider accountDatabasesAdditionalDataProvider,
        IDatabaseCardDbBackupsDataProvider databaseCardDbBackups,
        IConfiguration configuration,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        AccountDatabasePathHelper accountDatabasePathHelper,
        AccountDatabaseWebPublishPathHelper accountDatabaseWebPublish)
        : IRequestHandler<GetAccountDatabaseQuery, ManagerResult<AccountDatabaseCardResultDto>>
    {
        private readonly Dictionary<SupportState, string> _mapSupportStateToStateDescription = new()
        {
            {
                SupportState.NotAutorized,
                "Все регламентные операции выполняются в установленное вами время по МСК и только при отсутствии активных сессий пользователя."
            },
            {
                SupportState.AutorizationFail,
                "Ошибка подключения к базе, попробуйте еще раз"
            },
            {
                SupportState.NotValidAuthData,
                SupportMessagesConstsDto.InvalidLoginPasswordFull.Replace("\n", "<br>")
            },
            {
                SupportState.AutorizationProcessing,
                "Уважаемый пользователь!\r\nПроводится авторизация в информационной базе.\r\nПроцесс может занять до 30 минут.\r\nПо завершении процесса авторизации вам будет доступна возможность подключить Автоматическое обновление (АО) и Тестирование и Исправление ошибок (ТиИ)\r\nПожалуйста, подождите."
            },
            {
                SupportState.AutorizationSuccess,
                "Авторизация успешно завершена!"
            }
        };

        /// <summary>
        /// Handle get account database by number query
        /// </summary>
        public async Task<ManagerResult<AccountDatabaseCardResultDto>> Handle(GetAccountDatabaseQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var accountDatabase = await unitOfWork.DatabasesRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.AccountDatabaseOnDelimiter)
                    .Include(x => x.AcDbSupport)
                    .FirstOrDefaultAsync(d => request.AccountDatabaseId.HasValue ? d.Id == request.AccountDatabaseId.Value : d.V82Name.ToLower() == request.AccountDatabaseNumber.ToLower(), cancellationToken) ??
                            throw new NotFoundException(
                                                        $"По {(request.AccountDatabaseId.HasValue ? "идентификатору" : "номеру")} '{(request.AccountDatabaseId.HasValue ? request.AccountDatabaseId.Value : request.AccountDatabaseNumber)}' не удалось найти информационную базу.");

                accessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => accountDatabase.AccountId, null,  () => AccountHasAccessToDataBase(accessProvider.ContextAccountId, accountDatabase.Id));

                var databaseAdditionalDto = await unitOfWork.DatabasesRepository
                    .AsQueryable()
                    .ProjectTo<AccountDatabaseAdditionalDataDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == accountDatabase.Id, cancellationToken);

                if (databaseAdditionalDto == null)
                {
                    throw new NotFoundException(
                        $"По номеру '{request.AccountDatabaseNumber}' не удалось найти информационную базу.");
                }

                var backup = databaseCardDbBackups.GetDataByFilter(new DatabaseCardDbBackupsFilterDto { DatabaseId = databaseAdditionalDto.Id });
                databaseAdditionalDto.Stable83Version = platformVersionHelper.GetStable83VersionFromSegment(accountDatabase);
                databaseAdditionalDto.DatabaseConnectionPath = accountDatabaseDataHelper.GetDatabaseConnectionPath(accountDatabase);
                databaseAdditionalDto.FilePath = accountDatabasePathHelper.GetPath(accountDatabase);
                databaseAdditionalDto.CanEditDatabase = CheckAccountDatabaseHelper.CanEditDatabase(await accessProvider.GetUserAsync(), accountDatabase);
                databaseAdditionalDto.MyDatabasesServiceTypeId = myDatabasesServiceTypeDataProvider.GetServiceTypeIdByDatabaseId(databaseAdditionalDto.Id);
                databaseAdditionalDto.WebPublishPath = accountDatabaseWebPublish.GetWebPublishPath(accountDatabase);
                databaseAdditionalDto.CommonDataForWorkingWithDB = accountDatabasesAdditionalDataProvider.GetAdditionalDataForWorkingWithDatabases();
                var currentUser = await accessProvider.GetUserAsync();

                var localeId = (await unitOfWork.AccountsRepository.AsQueryableNoTracking()
                    .Select(x => new { x.Id, x.AccountConfiguration.LocaleId })
                    .FirstOrDefaultAsync(x => x.Id == currentUser.ContextAccountId, cancellationToken))?.LocaleId ?? Guid.Parse(configuration["DefaultLocale"]!);

                databaseAdditionalDto.DatabaseOperations = await GetOperations(localeId);
                var accesses = GetDatabaseAccesses(databaseAdditionalDto);
                var tabVisibility = GetTabVisibilityDto(databaseAdditionalDto, currentUser);

                return new AccountDatabaseCardResultDto
                    {
                        Database = databaseAdditionalDto,
                        LaunchPublishedDbUrl = accesses.CanShowWebPublishPath ? databaseAdditionalDto.WebPublishPath : string.Empty,
                        Backups = tabVisibility.IsBackupTabVisible ? backup.DatabaseBackups : [],
                        FieldsAccessReadModeInfo = accesses,
                        TehSupportInfo = tabVisibility.IsTehSupportTabVisible ? GetTehSupportInfoForDatabaseCard(accountDatabase.AcDbSupport, currentUser.ContextAccountId, databaseAdditionalDto.Id) : null,
                        TabVisibility = tabVisibility,
                        CommandVisibility = GetDatabaseCommandsForDatabaseCard(databaseAdditionalDto),
                        AccountCardEditModeDictionaries = GetAccountCardEditModeDictionaries(databaseAdditionalDto),
                        CanEditDatabase = databaseAdditionalDto.CanEditDatabase, 
                    }
                    .ToOkManagerResult();

            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                   "[Ошибка получения информации об информационной базе]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDatabaseCardResultDto>(ex.Message);
            }
        }


        /// <summary>
        /// Get account has access to database
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        private bool AccountHasAccessToDataBase(Guid accountId, Guid databaseId)
        {
            var account = unitOfWork.AccountsRepository.FirstOrDefault(a => a.Id == accountId);
            if (account == null)
                return false;

            var database = unitOfWork.DatabasesRepository.FirstOrDefault(d => d.Id == databaseId);
            if (database == null)
                return false;

            if (database.AccountId == accountId)
                return true;

            var accountUsersOfAccount = account.AccountUsers.Select(a => a.Id);
            var accessList = unitOfWork.AcDbAccessesRepository.Where(
                a =>
                    a.AccountDatabaseID == databaseId && a.AccountUserID != null &&
                    accountUsersOfAccount.Contains(a.AccountUserID.Value));

            return accessList.Any();
        }

        
        private async Task<List<DatabaseOperation>> GetOperations(Guid? localeId)
        {
            return await unitOfWork.RateRepository
                .AsQueryableNoTracking()
                .Where(x => (x.BillingServiceType.Service.Name == "Создание бекапа базы" ||
                            x.BillingServiceType.Service.Name == "Создание архива базы") && x.LocaleId == localeId)
                .Select(x => new DatabaseOperation
                {
                    Cost = x.Cost,
                    Currency = x.Locale.Currency,
                    Id = x.BillingServiceType.ServiceId,
                    Name = x.BillingServiceType.Service.Name
                }).ToListAsync();
        }

        /// <summary>
        /// Get account database commands visibility dto
        /// </summary>
        /// <param name="database"></param>
        /// <returns></returns>
        private DatabaseCardCommandVisibilityDto GetDatabaseCommandsForDatabaseCard(AccountDatabaseAdditionalDataDto database)
        {
            var commandVisibility = new DatabaseCardCommandVisibilityDto();

            if (database.State == DatabaseState.Ready)
            {
                commandVisibility.IsPublishDatabaseCommandVisible =
                    database.PublishState == PublishState.Unpublished &&
                    accessProvider.HasAccessBool(ObjectAction.AccountDatabases_Publish,
                        () => database.AccountId) &&
                    database is { CanWebPublish: true, IsDbOnDelimiters: false };

                commandVisibility.IsCancelPublishDatabaseCommandVisible =
                    database.PublishState == PublishState.Published &&
                    accessProvider.HasAccessBool(ObjectAction.AccountDatabases_Unpublish,
                        () => accessProvider.AccountIdByAccountDatabase(database.Id)) &&
                    !database.IsDbOnDelimiters;

                commandVisibility.IsDatabasePublishingInfoVisible =
                    database.PublishState == PublishState.PendingPublication &&
                    accessProvider.HasAccessBool(ObjectAction.AccountDatabases_Publish,
                        () => accessProvider.AccountIdByAccountDatabase(database.Id)) &&
                    !database.IsDbOnDelimiters;

                commandVisibility.IsCanacelingDatabasePublishingInfoVisible =
                    database.PublishState == PublishState.PendingUnpublication &&
                    accessProvider.HasAccessBool(ObjectAction.AccountDatabases_Publish,
                        () => accessProvider.AccountIdByAccountDatabase(database.Id)) &&
                    !database.IsDbOnDelimiters;
            }

            commandVisibility.IsDeleteDatabaseCommandVisible =
                accessProvider.HasAccessBool(ObjectAction.AccountDatabase_DeleteToTomb,
                    () => accessProvider.AccountIdByAccountDatabase(database.Id)) &&
                database.State != DatabaseState.Detached && database.State != DatabaseState.DeletedToTomb &&
                database.State != DatabaseState.DelitingToTomb && database.State != DatabaseState.DetachingToTomb;

            return commandVisibility;
        }

        /// <summary>
        /// Get account database teh support dto
        /// </summary>
        /// <param name="support"></param>
        /// <param name="accountId"></param>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        private AccountDatabaseTehSupportInfoDto GetTehSupportInfoForDatabaseCard(AcDbSupport? support, Guid accountId, Guid databaseId)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AccountDatabase_GetAcDbSupportData,
                    () => accountId);

                if (support == null)
                {
                    return new AccountDatabaseTehSupportInfoDto
                    {
                        SupportState = SupportState.NotAutorized,
                        SupportStateDescription = GetSupportStateDescription(SupportState.NotAutorized)
                    };
                }

                var supportState = (SupportState)support.State;

                return new AccountDatabaseTehSupportInfoDto
                {
                    IsConnects = supportState is SupportState.AutorizationSuccess or SupportState.AutorizationProcessing,
                    SupportState = supportState,
                    SupportStateDescription = support.State == (int)SupportState.NotValidAuthData
                        ? GetInvalidAuthorizationMessage(accountId)
                        : GetSupportStateDescription(supportState),
                    LastHistoryDate = support.TehSupportDate
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения данных технической поддержки информационной базы] {databaseId}.");

                throw;
            }
        }

        /// <summary>
        /// Get support state description
        /// </summary>
        /// <param name="supportState"></param>
        /// <returns></returns>
        private string GetSupportStateDescription(SupportState supportState)
        {
            return !_mapSupportStateToStateDescription.TryGetValue(supportState, out var supportStateDescription) ? supportState.ToString() : supportStateDescription;
        }


        /// <summary>
        /// Get invalid auth message
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        private string GetInvalidAuthorizationMessage(Guid accountId)
        {
            return "Не удалось авторизоваться в информационной базе<br>" +
                    "Убедитесь, что:<br>" +
                    "1. Введены корректные регистрационные данные<br>" +
                    $"2. {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.UserIsAnAdministratorInThe1CDatabase, accountId)}<br>";
        }

        /// <summary>
        /// Get account database dictionaries for edit mode
        /// </summary>
        /// <param name="database"></param>
        /// <returns></returns>
        private AccountDatabaseDictionariesDto GetAccountCardEditModeDictionaries(
            AccountDatabaseAdditionalDataDto database)
        {

            var availablePlatformTypes = GetAvailablePlatformTypes(database, database.AccountId);

            var distributionTypes = GetDistributionTypes(database, database.AccountId);

            var availableDatabaseStates = GetAvailableDatabaseStates();

            return new AccountDatabaseDictionariesDto
            {
                AvailablePlatformTypes = availablePlatformTypes,
                AvailableDestributionTypes = distributionTypes,
                AvailableDatabaseStates = availableDatabaseStates,
                AvailableFileStorages = database.AvailableFileStorages,
                AvailableDatabaseTemplates = database is { IsDbOnDelimiters: false } ? database.CommonDataForWorkingWithDB.AvailableDbTemplates : []
            };
        }
        
        /// <summary>
        /// Get available platform types
        /// </summary>
        /// <param name="canEditDatabase"></param>
        /// <param name="database"></param>
        /// <param name="ownerDatabaseId"></param>
        /// <returns></returns>
        private List<PlatformType> GetAvailablePlatformTypes( AccountDatabaseAdditionalDataDto database,
            Guid? ownerDatabaseId)
        {
            if ( database.PlatformType == PlatformType.V82 &&
                database.IsFile.HasValue && database.IsFile.Value && !database.IsDbOnDelimiters)
            {
                return Enum.GetValues(typeof(PlatformType)).Cast<PlatformType>().ToList();
            }

            return [];
        }

        /// <summary>
        /// Get distribution types
        /// </summary>
        /// <param name="canEditDatabase"></param>
        /// <param name="database"></param>
        /// <param name="ownerDatabaseId"></param>
        /// <returns></returns>
        private List<KeyValuePair<DistributionType, string>> GetDistributionTypes(AccountDatabaseAdditionalDataDto database,
           Guid? ownerDatabaseId)
        {
            if (
                database is { Alpha83Version: not null, IsFile: not null } && database.IsFile.Value &&
                database.PlatformType == PlatformType.V83 && database.Alpha83Version != database.Stable83Version &&
                !database.IsDbOnDelimiters)
            {
                return
                [
                    new KeyValuePair<DistributionType, string>(DistributionType.Alpha, database.Alpha83Version),
                    new KeyValuePair<DistributionType, string>(DistributionType.Stable, database.Stable83Version)
                ];
            }

            return [];
        }

        /// <summary>
        /// Get available database states
        /// </summary>
        /// <param name="canEditDatabase"></param>
        /// <returns></returns>
        private static List<DatabaseState> GetAvailableDatabaseStates() => Enum
            .GetValues(typeof(DatabaseState))
            .Cast<DatabaseState>()
            .Where(state => state != DatabaseState.TransferArchive
                            && state != DatabaseState.Attaching
                            && state != DatabaseState.Detached
                            && state != DatabaseState.Undefined)
            .ToList();
       

        /// <summary>
        /// Get database read mode field access dto
        /// </summary>
        /// <param name="database"></param>
        /// <returns></returns>
        private AccountDatabaseReadModeFieldAccessDto GetDatabaseAccesses(AccountDatabaseAdditionalDataDto database)
        {
            return new AccountDatabaseReadModeFieldAccessDto
            {
                CanShowWebPublishPath = database.PublishState == PublishState.Published &&
                                                (!database.IsDbOnDelimiters ||
                                                 database is { IsDbOnDelimiters: true, State: DatabaseState.Ready }),

                CanShowUsedWebServices = accessProvider.HasAccessBool(ObjectAction.AccountDatabases_EditUsedWebServices, () => database.AccountId) &&
                !database.IsDbOnDelimiters,

                CanShowDatabaseDetails = accessProvider.HasAccessBool(ObjectAction.AccountDatabases_EditFull, () => database.AccountId),

                CanShowSqlServer = accessProvider.HasAccessBool(ObjectAction.AccountDatabases_EditFull, () => database.AccountId) &&
                                           (!database.IsFile.HasValue || !database.IsFile.Value) &&
                                           accessProvider.HasAccessBool(ObjectAction.AccountDatabases_ViewSqlServerInfo, () => accessProvider.ContextAccountId),

                CanShowDatabaseState = accessProvider.HasAccessBool(ObjectAction.AccountDatabases_EditStatus, () => database.AccountId),

                CanShowDatabaseFileStorage = accessProvider.HasAccessBool(ObjectAction.AccountDatabase_MigrationListView, () => database.AccountId)
                                                      && database.IsFile == true
            };
        }

        /// <summary>
        /// Get database card tabs visibility dto
        /// </summary>
        /// <param name="databaseAdditionalDto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private DatabaseCardTabVisibilityDto GetTabVisibilityDto(AccountDatabaseAdditionalDataDto databaseAdditionalDto, IUserPrincipalDto user)
        {
            return new DatabaseCardTabVisibilityDto
            {
                IsBackupTabVisible = accessProvider.HasAccessBool(ObjectAction.AcDbAccesses_Edit, () => databaseAdditionalDto.AccountId) &&
                                     user.Groups.Any(g => g is AccountUserGroup.CloudAdmin or AccountUserGroup.Hotline),

                IsTehSupportTabVisible = accessProvider.HasAccessBool(ObjectAction.AccountDatabases_EditSupport, () => databaseAdditionalDto.AccountId) &&
                                                 !databaseAdditionalDto.IsDbOnDelimiters
            };
        }

    }
}
