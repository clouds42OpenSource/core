﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Handlers
{
    /// <summary>
    /// Get account database by search line query handler
    /// </summary>
    public class GetAccountDatabaseBySearchLineQueryHandler(
        ILogger42 logger,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper)
        : IRequestHandler<GetAccountDatabaseBySearchLineQuery, ManagerResult<List<AccountDatabaseInfoDto>>>
    {
        /// <summary>
        /// Handle get account database by search line query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<AccountDatabaseInfoDto>>> Handle(GetAccountDatabaseBySearchLineQuery request, CancellationToken cancellationToken)
        {

            var message = $"Поиск информационных баз для выподающего списка по строке поиска {request.SearchLine}";
            try
            {
                accessProvider.HasAccess(ObjectAction.DelimiterSourceAccountDatabase_GetData);
                logger.Info(message);

                if (string.IsNullOrEmpty(request.SearchLine))
                    return new List<AccountDatabaseInfoDto>().ToOkManagerResult();

                return (await unitOfWork.DatabasesRepository
                    .AsQueryable()
                    .Where(db => db.AccountDatabaseOnDelimiter == null && (db.IsFile == null || db.IsFile == false) &&
                                 db.State == DatabaseState.Ready.ToString() &&
                                 (db.Caption != null && db.Caption.ToLower().Contains(request.SearchLine.ToLower()) ||
                                 db.V82Name != null && db.V82Name.ToLower().Contains(request.SearchLine.ToLower())))
                    .ProjectTo<AccountDatabaseInfoDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось ошибкой]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<List<AccountDatabaseInfoDto>>(ex.Message);
            }
        }
    }
}
