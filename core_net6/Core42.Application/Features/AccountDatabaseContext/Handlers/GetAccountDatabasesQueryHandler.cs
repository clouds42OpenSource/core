﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using Core42.Application.Features.AccountDatabaseContext.Specification;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Handlers
{
    /// <summary>
    /// Get filtered, sorted and paginated account databases query handler
    /// </summary>
    public class GetAccountDatabasesQueryHandler(
        ILogger42 logger,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper)
        : IRequestHandler<GetAccountDatabasesQuery, ManagerResult<PagedDto<AccountDatabaseItemDto>>>
    {
        /// <summary>
        /// Handle get filtered, sorted and paginated account databases query
        /// </summary>
        public async Task<ManagerResult<PagedDto<AccountDatabaseItemDto>>> Handle(GetAccountDatabasesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccessForMultiAccounts(ObjectAction.AccountDatabase_Detail);

                var records = (await unitOfWork
                 .DatabasesRepository
                 .AsQueryable()
                 .Where(DatabaseSpecification.ByAccountNotNull()
                         && DatabaseSpecification.ByStateNotEquals(DatabaseState.DeletedFromCloud)
                         && DatabaseSpecification.BySearchLine(request.Filter?.SearchLine))
                 .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                 .ProjectTo<AccountDatabaseItemDto>(mapper.ConfigurationProvider)
                 .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                 .ToPagedDto()
                 .ToOkManagerResult();

                logger.Trace("Получение списка информационных баз завершено успешно.");

                return records;
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "Получение списка информационных баз завершено с ошибкой");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AccountDatabaseItemDto>>(ex.Message);
            }
        }
    }
}
