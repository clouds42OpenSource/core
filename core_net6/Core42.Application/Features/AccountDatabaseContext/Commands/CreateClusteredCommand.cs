﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountDatabaseContext.Commands;

/// <summary>
/// Команда для создания кластерного ресурса.
/// Реализует интерфейс IRequest для работы с MediatR.
/// </summary>
public class CreateClusteredCommand : IRequest<ManagerResult<bool>>
{
    /// <summary>
    /// Идентификатор учетной записи.
    /// </summary>
    public Guid AccountId { get; set; }

    /// <summary>
    /// Идентификатор пользователя учетной записи.
    /// </summary>
    public Guid AccountUserId { get; set; }

    /// <summary>
    /// Идентификатор базы данных учетной записи.
    /// </summary>
    public Guid AccountDatabaseId { get; set; }

    /// <summary>
    /// Логин для подключения.
    /// </summary>
    public string Login { get; set; }

    /// <summary>
    /// Пароль для подключения.
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    /// Номер телефона, связанный с учетной записью.
    /// </summary>
    public string PhoneNumber { get; set; }

    /// <summary>
    /// Указывает, является ли операция копированием.
    /// </summary>
    public bool IsCopy { get; set; }
}
