﻿namespace Core42.Application.Features.AccountDatabaseContext.Dtos
{
    public class FileStorageDto
    {
        /// <summary>
        /// File storage name
        /// </summary>
        public string FileStorage { get; set; }

        /// <summary>
        /// File storage path
        /// </summary>
        public string PathStorage { get; set; }

        /// <summary>
        /// Database in current storage count
        /// </summary>
        public int DatabaseCount { get; set; }

        /// <summary>
        /// Total databases size
        /// </summary>
        public int TotatlSizeOfDatabases { get; set; }
    }
}
