﻿using AutoMapper;
using Clouds42.DataContracts.BaseModel;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Database template dto
    /// </summary>
    public class AccountDatabaseTemplateDto : IMapFrom<KeyValueDto<Guid>>
    {
        /// <summary>
        /// Template id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Template default caption
        /// </summary>
        public string DefaultCaption { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<KeyValueDto<Guid>, AccountDatabaseTemplateDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.Key))
                .ForMember(x => x.DefaultCaption, z => z.MapFrom(y => y.Value));
        }
    }
}
