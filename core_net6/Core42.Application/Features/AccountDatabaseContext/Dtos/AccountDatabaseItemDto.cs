﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.DataBases;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Account database item dto
    /// </summary>
    public class AccountDatabaseItemDto : IMapFrom<AccountDatabase>
    {
        /// <summary>
        /// Database identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Database V82 name
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Database caption
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Account caption
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        public int AccountNumber { get; set; }

        /// <summary>
        /// Creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Last activity date
        /// </summary>
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Is file database
        /// </summary>
        public bool? IsFile { get; set; }

        /// <summary>
        /// Platform version
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Db size in Mb
        /// </summary>
        public int SizeInMB { get; set; }

        /// <summary>
        /// Locked state
        /// </summary>
        public string LockedState { get; set; }

        /// <summary>
        /// Publish state
        /// </summary>
        public bool IsPublish { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountDatabase, AccountDatabaseItemDto>()
                .ForMember(x => x.AccountCaption, z => z.MapFrom(y => y.Account.AccountCaption))
                .ForMember(x => x.IsPublish, z => z.MapFrom(y => y.PublishState == PublishState.Published.ToString()))
                .ForMember(x => x.ApplicationName, z => z.MapFrom(y =>
                    y.ApplicationName == "8.3"
                        ? y.DistributionType == "Stable"
                            ? y.Account.AccountConfiguration.Segment.Stable83Version
                            : y.Account.AccountConfiguration.Segment.Alpha83Version
                        : y.Account.AccountConfiguration.Segment.Stable82Version))
                .ForMember(x => x.AccountNumber, z => z.MapFrom(y => y.Account.IndexNumber));
        }

    }
}
