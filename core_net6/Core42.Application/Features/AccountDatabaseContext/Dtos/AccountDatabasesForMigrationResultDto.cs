﻿using Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Databases for migration dto
    /// </summary>
    public class AccountDatabasesForMigrationResultDto
    {
        /// <summary>
        /// Account database file storages summary info
        /// </summary>
        public List<FileStorageDto> FileStorageAccountDatabasesSummary { get; set; }

        /// <summary>
        /// Account databases
        /// </summary>
        public PagedDto<AccountDatabaseMigrationDto> Databases { get; set; }

        /// <summary>
        /// Available file storages key-value array
        /// </summary>
        public List<AvailableCloudStoragesDictionaryDto> AvalableFileStorages { get; set; }

        /// <summary>
        /// Available storage paths
        /// </summary>
        public List<string> AvailableStoragePath { get; set; }

        /// <summary>
        /// Databases count to migrate
        /// </summary>
        public int NumberOfDatabasesToMigrate { get; set; }
    }
}
