﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Account database migration dto
    /// </summary>
    public class AccountDatabaseMigrationDto : IMapFrom<AccountDatabase>
    {
        /// <summary>
        /// Database id
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Database v82 name
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Database path
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Database size
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Database state enum
        /// </summary>
        public DatabaseState Status { get { return (DatabaseState)Enum.Parse(typeof(DatabaseState), State); } }

        /// <summary>
        /// Database state string
        /// </summary>
        [JsonIgnore]
        public string State { get; set; }  

        /// <summary>
        /// Database is publish flag
        /// </summary>
        public bool IsPublishDatabase { get; set; }

        /// <summary>
        /// Database services is published flag
        /// </summary>
        public bool IsPublishServices { get; set; }

        /// <summary>
        /// Database is file flag
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountDatabase, AccountDatabaseMigrationDto>()
                .ForMember(z => z.IsFile, m => m.MapFrom(item => item.IsFile.HasValue && item.IsFile.Value))
                .ForMember(z => z.AccountDatabaseId, m => m.MapFrom(item => item.Id))
                .ForMember(z => z.State, m => m.MapFrom(item => item.State))
                .ForMember(z => z.IsPublishDatabase, m => m.MapFrom(item => item.PublishStateEnum == PublishState.Published))
                .ForMember(z => z.IsPublishServices, m => m.MapFrom(item => item.UsedWebServices.HasValue && item.UsedWebServices.Value))
                .ForMember(z => z.Size, m => m.MapFrom(item => item.SizeInMB));
        }
    }
}
