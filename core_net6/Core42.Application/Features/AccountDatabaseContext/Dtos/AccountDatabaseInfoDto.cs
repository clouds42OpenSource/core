﻿using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Account database info dto
    /// </summary>
    public class AccountDatabaseInfoDto : IMapFrom<AccountDatabase>
    {
        /// <summary>
        /// Database caption
        /// </summary>
        [NotMapped]
        public string AccountDatabaseCaption { get { return $"{Caption} ({V82Name})"; } }

        /// <summary>
        /// Database identifier
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Database V82 name
        /// </summary>
        public string AccountDatabaseV82Name { get; set; }

        /// <summary>
        /// Database caption
        /// </summary>
        [JsonIgnore]
        public string Caption { get; set; }
        /// <summary>
        /// Database v82 name
        /// </summary>
        [JsonIgnore]
        public string V82Name { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountDatabase, AccountDatabaseInfoDto>()
                .ForMember(x => x.AccountDatabaseId, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.AccountDatabaseV82Name, z => z.MapFrom(y => y.V82Name))
                .ForMember(x => x.Caption, z => z.MapFrom(y => y.Caption))
                .ForMember(x => x.V82Name, z => z.MapFrom(y => y.V82Name));
        }
    }
}
