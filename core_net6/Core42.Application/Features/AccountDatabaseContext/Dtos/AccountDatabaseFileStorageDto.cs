﻿using AutoMapper;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Account database file storage dto
    /// </summary>
    public class AccountDatabaseFileStorageDto : IMapFrom<AvailableCloudStoragesDictionaryDto>
    {
        /// <summary>
        /// File storage identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// File storage name
        /// </summary>
        public string Name { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AvailableCloudStoragesDictionaryDto, AccountDatabaseFileStorageDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.Key))
                .ForMember(x => x.Name, z => z.MapFrom(y => y.Value));
        }
    }
}
