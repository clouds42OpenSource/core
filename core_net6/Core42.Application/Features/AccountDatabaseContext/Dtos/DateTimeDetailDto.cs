﻿namespace Core42.Application.Features.AccountDatabaseContext.Dtos
{
    public class DateTimeDetailDto
    {
        /// <summary>
        /// Hours
        /// </summary>
        public int Hours { get; set; }

        /// <summary>
        /// Minutes
        /// </summary>
        public int Minutes { get; set; }

        /// <summary>
        /// Seconds
        /// </summary>
        public int Seconds { get; set; }

        /// <summary>
        /// Total seconds
        /// </summary>
        public long TotalSeconds { get; set; }
    }
}
