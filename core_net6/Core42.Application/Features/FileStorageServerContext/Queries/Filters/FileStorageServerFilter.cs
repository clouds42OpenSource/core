﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.FileStorageServerContext.Queries.Filters;

public class FileStorageServerFilter : IQueryFilter
{
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    /// <summary>
    /// Dns name
    /// </summary>
    public string DnsName { get; set; }
}
