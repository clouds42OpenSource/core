﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.FileStorageServerContext.Dtos;
using Core42.Application.Features.FileStorageServerContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.FileStorageServerContext.Queries
{
    public class GetFilteredFileStorageServersQuery: IRequest<ManagerResult<PagedDto<FileStorageServerDto>>>, ISortedQuery, IPagedQuery, IHasFilter<FileStorageServerFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(FileStorageServerDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public FileStorageServerFilter Filter { get; set; }
    }
}
