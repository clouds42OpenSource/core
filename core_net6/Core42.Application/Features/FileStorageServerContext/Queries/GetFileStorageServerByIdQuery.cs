﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.FileStorageServerContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.FileStorageServerContext.Queries;

public class GetFileStorageServerByIdQuery : IRequest<ManagerResult<FileStorageServerDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
