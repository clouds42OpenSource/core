﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.FileStorageServerContext.Dtos;
using Core42.Application.Features.FileStorageServerContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.FileStorageServerContext.Handlers;

public class GetFileStorageServerByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFileStorageServerByIdQuery,
        ManagerResult<FileStorageServerDto>>
{
    public async Task<ManagerResult<FileStorageServerDto>> Handle(GetFileStorageServerByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesFileStorageServerRepository
                    .AsQueryable()
                    .ProjectTo<FileStorageServerDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                .ToOkManagerResult();
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex,
                $"[Ошибка получения файлового хранилища {request.Id}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<FileStorageServerDto>(ex.Message);
        }
    }
}
