﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.FileStorageServerContext.Dtos;
using Core42.Application.Features.FileStorageServerContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.FileStorageServerContext.Handlers;

public class GetFilteredFileStorageServersQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredFileStorageServersQuery, ManagerResult<PagedDto<FileStorageServerDto>>>
{
    public async Task<ManagerResult<PagedDto<FileStorageServerDto>>> Handle(GetFilteredFileStorageServersQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesFileStorageServerRepository
                    .AsQueryable()
                    .ProjectTo<FileStorageServerDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка файловых хранилищ]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<FileStorageServerDto>>(ex.Message);
        }
    }
}
