﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CloudServicesContext.Dtos;
using MediatR;

namespace Core42.Application.Features.CloudServicesContext.Queries
{
    /// <summary>
    /// Get all cloud services query
    /// </summary>
    public class GetAllCloudServicesQuery : IRequest<ManagerResult<List<CloudServiceDto>>>
    {
    }
}
