﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CloudServicesContext.Dtos;
using MediatR;

namespace Core42.Application.Features.CloudServicesContext.Queries
{
    /// <summary>
    /// Get default configuration query
    /// </summary>
    public class GetDefaultConfigurationQuery : IRequest<ManagerResult<CloudCoreDefConfigurationDto>>
    {
    }
}
