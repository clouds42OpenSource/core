﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CloudServicesContext.Dtos;
using Core42.Application.Features.CloudServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CloudServicesContext.Handlers
{
    /// <summary>
    /// Get default configuration query handler
    /// </summary>
    public class GetDefaultConfigurationQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetDefaultConfigurationQuery, ManagerResult<CloudCoreDefConfigurationDto>>
    {
        /// <summary>
        /// Handle get default configuration query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<CloudCoreDefConfigurationDto>> Handle(GetDefaultConfigurationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);

                var cloudCore = await unitOfWork.CloudCoreRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(cancellationToken);

                if (cloudCore == null)
                    return null;

                var segments = await unitOfWork.CloudServicesSegmentRepository
                    .AsQueryable()
                    .OrderBy(x => x.Name)
                    .ProjectTo<CloudCoreSegmentConfigDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                var defSegment = segments.FirstOrDefault(x => x.IsDefault);

                var result = new CloudCoreDefConfigurationDto
                {
                    DefaultSegment = defSegment ?? new CloudCoreSegmentConfigDto { DefaultSegmentName = "...", SegmentId = Guid.Empty, IsDefault = true },
                    HoursDifferenceOfUkraineAndMoscow = cloudCore.HourseDifferenceOfUkraineAndMoscow ?? 0,
                    Segments = segments,
                    MainPageNotification = cloudCore.MainPageNotification,
                    RussianLocalePaymentAggregator = cloudCore.RussianLocalePaymentAggregator,
                    BannerUrl = cloudCore.BannerUrl,
                };

                return result.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения дефолтных конфигураций]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<CloudCoreDefConfigurationDto>(ex.Message);
            }
        }
    }
}
