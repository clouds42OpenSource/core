﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Identity.Contracts;
using Clouds42.Jwt.Contracts;
using Clouds42.Jwt.Contracts.Simple;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.CloudServiceContext.Dtos;
using Core42.Application.Contracts.Features.CloudServiceContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CloudServicesContext.Handlers
{
    /// <summary>
    /// Get jwt by service id query handler
    /// </summary>
    public class GetJwtByServiceIdQueryHandler(
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        DesEncryptionProvider desEncryptionProvider,
        IJwtProvider jwtProvider)
        : IRequestHandler<GetJwtByServiceIdQuery, ManagerResult<CloudServiceJwtDto>>
    {
        /// <summary>
        /// Handle get jwt by service id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ManagerResult<CloudServiceJwtDto>> Handle(GetJwtByServiceIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var foundCloudService = await unitOfWork.CloudServiceRepository
                    .AsQueryable()
                    .Include(x => x.AccountUser)
                    .FirstOrDefaultAsync(x => x.CloudServiceId.ToLower() == "Core42".ToLower(), cancellationToken)
                    ?? throw new NotFoundException($"Запись с id:{request.Id} не найдена");

                var userPassword = desEncryptionProvider.Decrypt(foundCloudService.AccountUser.Password);
                var userLogin = foundCloudService.AccountUser.Login;

                JwtInfo? jwtDto;

                if (!string.IsNullOrEmpty(foundCloudService.RefreshToken))
                {
                    try
                    {
                        jwtDto = await jwtProvider.RefreshJsonWebToken(
                            Encoding.UTF8.GetString(foundCloudService.JsonWebToken),
                            Convert.ToBase64String(Encoding.UTF8.GetBytes(foundCloudService.RefreshToken)));

                        jwtDto ??= await jwtProvider.CreateJsonWebTokenFor(userLogin, userPassword, null,
                                new ApplicationUser { Id = foundCloudService.AccountUserId!.Value });
                    }

                    catch (Exception _)
                    {
                        jwtDto = await jwtProvider.CreateJsonWebTokenFor(userLogin, userPassword, null,
                            new ApplicationUser { Id = foundCloudService.AccountUserId!.Value });
                    }
                }
                  
                else
                {
                    jwtDto = await jwtProvider.CreateJsonWebTokenFor(userLogin, userPassword, null,
                        new ApplicationUser { Id = foundCloudService.AccountUserId!.Value });
                }

                foundCloudService.JsonWebToken = Encoding.UTF8.GetBytes(jwtDto.JsonWebToken);
                foundCloudService.RefreshToken = Encoding.UTF8.GetString(jwtDto.RefreshToken);

                unitOfWork.CloudServiceRepository.UpdateCloudService(foundCloudService);

                await unitOfWork.SaveAsync();

                return new CloudServiceJwtDto { JsonWebToken = jwtDto.JsonWebToken }.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка генерации JsonWebToken]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<CloudServiceJwtDto>(ex.Message);
            }
        }
    }
}
