﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CloudServicesContext.Dtos;
using Core42.Application.Features.CloudServicesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CloudServicesContext.Handlers
{
    /// <summary>
    /// Get all cloud services query handler
    /// </summary>
    public class GetAllCloudServicesQueryHandler(IMapper mapper, IUnitOfWork unitOfWork, IAccessProvider accessProvider)
        : IRequestHandler<GetAllCloudServicesQuery, ManagerResult<List<CloudServiceDto>>>
    {
        /// <summary>
        /// Handle get all cloud services query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<CloudServiceDto>>> Handle(GetAllCloudServicesQuery request, CancellationToken cancellationToken)
        {
            accessProvider.HasAccess(ObjectAction.CloudServices_View);

            return (await unitOfWork.CloudServiceRepository
                .AsQueryable()
                .ToListAsync(cancellationToken))
                .Select(x => mapper.Map<CloudServiceDto>(x))
                .ToList()
                .ToOkManagerResult();
        }
    }
}
