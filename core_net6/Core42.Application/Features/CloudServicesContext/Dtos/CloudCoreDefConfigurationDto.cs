﻿using Clouds42.Domain.Enums;

namespace Core42.Application.Features.CloudServicesContext.Dtos
{
    /// <summary>
    /// default configuration object dto
    /// </summary>
    public class CloudCoreDefConfigurationDto
    {
        /// <summary>
        /// House difference of Ukraine and Moscow
        /// </summary>
        public int HoursDifferenceOfUkraineAndMoscow { get; set; }

        /// <summary>
        /// Default segment object
        /// </summary>
        public CloudCoreSegmentConfigDto DefaultSegment { get; set; }

        /// <summary>
        /// Notification page content
        /// </summary>
        public string MainPageNotification { get; set; }

        /// <summary>
        /// Aggregator type
        /// </summary>
        public RussianLocalePaymentAggregatorEnum RussianLocalePaymentAggregator { get; set; }

        /// <summary>
        /// Banner url
        /// </summary>
        public string BannerUrl { get; set; }

        /// <summary>
        /// Segments collection
        /// </summary>
        public List<CloudCoreSegmentConfigDto> Segments { get; set; }
    }
}
