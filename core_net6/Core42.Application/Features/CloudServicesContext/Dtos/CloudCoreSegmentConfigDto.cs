﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CloudServicesContext.Dtos
{
    /// <summary>
    /// default segment object
    /// </summary>
    public class CloudCoreSegmentConfigDto : IMapFrom<CloudServicesSegment>
    {
        /// <summary>
        /// default segment name
        /// </summary>
        public string DefaultSegmentName { get; set; }

        /// <summary>
        /// default segment identifier
        /// </summary>
        public Guid SegmentId { get; set; }

        public bool IsDefault { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CloudServicesSegment, CloudCoreSegmentConfigDto>()
                .ForMember(z => z.DefaultSegmentName, m => m.MapFrom(z => z.Name))
                .ForMember(z => z.IsDefault, m => m.MapFrom(z => z.IsDefault))
                .ForMember(z => z.SegmentId, m => m.MapFrom(z => z.ID));
        }
    }
}
