﻿using System.Text;
using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CloudServicesContext.Dtos
{
    /// <summary>
    /// Cloud service dto
    /// </summary>
    public class CloudServiceDto: IMapFrom<CloudService>
    {
        /// <summary>
        /// Cloud service identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Service identifier
        /// </summary>
        public string CloudServiceId { get; set; }

        /// <summary>
        /// Service caption
        /// </summary>
        public string ServiceCaption { get; set; }

        /// <summary>
        /// JsonWebToken
        /// </summary>
        public string JsonWebToken { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CloudService, CloudServiceDto>()
                .ForMember(x => x.JsonWebToken, z => z.MapFrom(y => y.JsonWebToken == null ? string.Empty : Encoding.ASCII.GetString(y.JsonWebToken)));
        }
    }
}
