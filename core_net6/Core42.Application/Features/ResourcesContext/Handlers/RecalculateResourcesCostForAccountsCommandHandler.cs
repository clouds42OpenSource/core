﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using Core42.Application.Contracts.Features.ResourcesContext.Commands;
using MediatR;

namespace Core42.Application.Features.ResourcesContext.Handlers;

public class RecalculateResourcesCostForAccountsCommandHandler(
    IHandlerException handlerException,
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    ISender sender)
    : IRequestHandler<RecalculateResourcesCostForAccountsCommand>
{
    public async Task Handle(RecalculateResourcesCostForAccountsCommand request, CancellationToken cancellationToken)
    {
        var message = $"Пересчет стоимости сервиса по изменениям {request.BillingServiceChangesId}";
        logger.Info(message);
        try
        {
            Guid? billingServiceId = null;
            Clouds42Service? clouds42Service = null;
            List<Guid>? accountIds = null;
            if (request.BillingServiceChangesId.HasValue)
            {
                var editServiceRequest = await unitOfWork.GetGenericRepository<EditServiceRequest>()
                                             .AsQueryableNoTracking()
                                             .Include(x => x.BillingServiceChanges)
                                             .ThenInclude(x => x.BillingServiceTypeChanges)
                                             .Include(x => x.BillingService)
                                             .FirstOrDefaultAsync(bc => bc.BillingServiceChangesId == request.BillingServiceChangesId, cancellationToken) ??
                                         throw new NotFoundException($"Не удалось найти заявку на изменение сервиса по Id {request.BillingServiceChangesId}");

                billingServiceId = editServiceRequest.BillingServiceId;
                clouds42Service = editServiceRequest.BillingService.SystemService;
                accountIds = request.AccountsIds;

                var serviceTypeChanges = editServiceRequest.BillingServiceChanges
                    .BillingServiceTypeChanges
                    .Where(bst => bst.Cost.HasValue && bst.BillingServiceTypeId.HasValue && bst.BillingServiceTypeId != Guid.Empty)
                    .ToList();

                var serviceTypeIds = serviceTypeChanges.Select(x => x.BillingServiceTypeId!.Value).ToList();

                var accountResources = await unitOfWork.ResourceRepository
                    .AsQueryable()
                    .Where(rs => (accountIds.Contains(rs.AccountId) && rs.AccountSponsorId == null || accountIds.Contains(rs.AccountSponsorId!.Value)) && serviceTypeIds.Contains(rs.BillingServiceTypeId))
                    .ToListAsync(cancellationToken);

                var accountRates = await unitOfWork.AccountRateRepository
                    .AsQueryable()
                    .Where(ar => accountIds.Contains(ar.AccountId) && serviceTypeIds.Contains(ar.BillingServiceTypeId))
                    .ToListAsync(cancellationToken);

                accountResources.GroupJoin(serviceTypeChanges, x => x.BillingServiceTypeId, z => z.BillingServiceTypeId, (resource, serviceTypes) => (resource, serviceTypes))
                    .ToList()
                    .ForEach(x =>
                    {
                        var serviceType = x.serviceTypes.FirstOrDefault();
                        if (serviceType == null)
                            return;

                        x.resource.Cost = serviceType.Cost!.Value;
                    });
               
                await unitOfWork.BulkUpdateAsync(accountResources);
                await unitOfWork.BulkDeleteAsync(accountRates);
            }

            if (request.ServiceTypeId.HasValue)
            {
                var cost = request.Cost ?? (await unitOfWork.RateRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.BillingServiceTypeId == request.ServiceTypeId.Value, cancellationToken))?.Cost ?? throw new NotFoundException($"Не удалось получить тариф для услуги '{request.ServiceTypeId.Value}'");

                billingServiceId = request.ServiceId;
                clouds42Service = request.ServiceType;

                accountIds =  await ChangeByServiceTypeId(request.ServiceTypeId.Value, cost, cancellationToken);
            }

            await unitOfWork.SaveAsync();

            await sender.Send(new RecalculateResourcesConfigurationCostCommand(accountIds, [billingServiceId!.Value],
                clouds42Service), cancellationToken);

            logger.Info($"{message} завершился успешно");
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, $"[Ошибка пересчета стоимости сервиса по изменениям] {request.BillingServiceChangesId}");

            throw;
        }
    }

    private async Task<List<Guid>?> ChangeByServiceTypeId(Guid serviceTypeId, decimal cost, CancellationToken cancellationToken)
    {
        var groupedAccountsWithResources = (await unitOfWork.ResourceRepository
              .AsQueryable()
              .Where(x => x.BillingServiceTypeId == serviceTypeId)
              .Select(x => new { resource = x, resourceAccountId = x.AccountSponsorId ?? x.AccountId })
              .ToListAsync(cancellationToken))
          .GroupBy(x => x.resourceAccountId)
          .Select(x => new
          {
              AccountId = x.Key,
              Resources = x.Select(z => z.resource).ToList()
          })
        .ToList();

        if (!groupedAccountsWithResources.Any())
        {
            return null;
        }

        logger.Info($"Пересчет стоимости услуги '{serviceTypeId}'");

        var accountIds = groupedAccountsWithResources.Select(x => x.AccountId).ToList();

        var accountIdsForNeedUpdateServiceTypeCost = await unitOfWork.AccountRateRepository
            .AsQueryableNoTracking()
            .Where(x => accountIds.Contains(x.AccountId) && x.BillingServiceTypeId == serviceTypeId)
            .Select(x => x.AccountId)
            .ToListAsync(cancellationToken);


        var accountsWithResourcesForUpdateServiceTypeCost = groupedAccountsWithResources
            .Where(x => !accountIdsForNeedUpdateServiceTypeCost.Contains(x.AccountId)).ToList();

        var resources = accountsWithResourcesForUpdateServiceTypeCost.SelectMany(x => x.Resources).ToList();

        foreach (var resource in resources)
        {
            logger.Info($"Пересчет стоимости услуги сервиса '{resource.Id}' для аккаунта {resource.AccountId}");

            resource.Cost = cost;
        }

        await unitOfWork.BulkUpdateAsync(resources);

        return groupedAccountsWithResources.Select(x => x.AccountId).ToList();
    }
}
