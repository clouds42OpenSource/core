﻿using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ResourcesContext.Specifications
{
    public static class ResourceSpecification
    {
        public static Spec<Resource> ByActiveRent()
        {
            return new(x =>
                x.BillingServiceType.SystemServiceType == ResourceType.MyEntUser ||
                x.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb && x.Subject.HasValue);
        }
    }
}
