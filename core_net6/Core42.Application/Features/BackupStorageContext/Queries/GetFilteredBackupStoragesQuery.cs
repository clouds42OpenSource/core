﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.BackupStorageContext.Dtos;
using Core42.Application.Features.BackupStorageContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BackupStorageContext.Queries
{
    public class GetFilteredBackupStoragesQuery : IRequest<ManagerResult<PagedDto<BackupStorageDto>>>, ISortedQuery,
        IPagedQuery, IHasFilter<BackupStorageFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(BackupStorageDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public BackupStorageFilter Filter { get; set; }
    }
}
