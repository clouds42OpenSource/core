﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BackupStorageContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.BackupStorageContext.Queries;

public class GetBackupStorageByIdQuery : IRequest<ManagerResult<BackupStorageDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
