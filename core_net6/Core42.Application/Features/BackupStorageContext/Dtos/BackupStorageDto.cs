﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.BackupStorageContext.Dtos;

public class BackupStorageDto : IMapFrom<CloudServicesBackupStorage>
{
    public Guid Id { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    /// <summary>
    /// Physical path
    /// </summary>
    public string PhysicalPath { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesBackupStorage, BackupStorageDto>()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID));
    }
}
