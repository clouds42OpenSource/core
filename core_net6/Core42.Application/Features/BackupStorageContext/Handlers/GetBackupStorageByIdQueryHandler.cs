﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BackupStorageContext.Dtos;
using Core42.Application.Features.BackupStorageContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BackupStorageContext.Handlers;

public class GetBackupStorageByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetBackupStorageByIdQuery, ManagerResult<BackupStorageDto>>
{
    public async Task<ManagerResult<BackupStorageDto>> Handle(GetBackupStorageByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesBackupStorageRepository
                    .AsQueryable()
                    .ProjectTo<BackupStorageDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                .ToOkManagerResult();
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex,
                $"[Ошибка получения хранилища бэкапов {request.Id}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<BackupStorageDto>(ex.Message);
        }
    }
}
