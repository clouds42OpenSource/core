﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BackupStorageContext.Dtos;
using Core42.Application.Features.BackupStorageContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BackupStorageContext.Handlers;

public class GetFilteredBackupStoragesQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredBackupStoragesQuery, ManagerResult<PagedDto<BackupStorageDto>>>
{
    public async Task<ManagerResult<PagedDto<BackupStorageDto>>> Handle(GetFilteredBackupStoragesQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesBackupStorageRepository
                    .AsQueryable()
                    .ProjectTo<BackupStorageDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка хранилищ бэкапов]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<BackupStorageDto>>(ex.Message);
        }
    }
}
