﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Handlers
{
    /// <summary>
    /// Handler for getting account settings
    /// </summary>
    public class GetAccountConnectionSettingsQueryHandler(
        IHandlerException handlerException,
        IAccessProvider accountProvider,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetAccountConnectionSettingsQuery, ManagerResult<AccountSettingsDataDto>>
    {
        /// <summary>
        /// Handle query to get account settings
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<AccountSettingsDataDto>> Handle(GetAccountConnectionSettingsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var account = await unitOfWork.AccountUsersRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(a => a.Id == request.AccountUserId, cancellationToken);

                accountProvider.HasAccess(ObjectAction.Accounts_GetSettings, () => account?.AccountId);

                return (await GetAccountSettings(request.AccountUserId, cancellationToken)).ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[An error occurred while getting the account settings for the user ID {request.AccountUserId}]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountSettingsDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Get account setting by account user id
        /// </summary>
        /// <param name="accountUserId">Account user identifier</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        private async Task<AccountSettingsDataDto> GetAccountSettings(Guid accountUserId, CancellationToken cancellationToken)
        {
            var userWithAccountConf = await unitOfWork.AccountUsersRepository.
                AsQueryable()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Locale)
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .FirstOrDefaultAsync(user => user.Id == accountUserId, cancellationToken) ?? throw new NotFoundException($"Не удалось получить настройки аккаунта по пользователю {accountUserId}");

            var farmConnectionAddress = (await unitOfWork.CloudServicesTerminalFarmRepository
                                            .AsQueryable()
                                            .FirstOrDefaultAsync(farm => farm.ID == userWithAccountConf.Account.AccountConfiguration.Segment.ServicesTerminalFarmID, cancellationToken))?.ConnectionAddress
                                                ?? throw new NotFoundException($"Не удалось найти настройки для терминала облачных ферм по пользователю {accountUserId}");

            var gatewayConnectionAddress = (await unitOfWork.CloudServicesGatewayTerminalRepository
                                               .AsQueryable()
                                               .FirstOrDefaultAsync(gate => gate.ID == userWithAccountConf.Account.AccountConfiguration.Segment.GatewayTerminalsID, cancellationToken))?.ConnectionAddress
                                                    ?? throw new NotFoundException($"Не удалось найти настройки для шлюза терминала по пользователю {accountUserId}");

            var platformThinClientVersions = (await GetPlatformThinClientVersions(cancellationToken))
                .OrderByDescending(thinClient => thinClient.Version, new VersionComparer()).ToList();

            return new AccountSettingsDataDto
            {
                Login = userWithAccountConf.Login,
                GatewayTerminalsName = gatewayConnectionAddress,
                ServicesTerminalFarmName = farmConnectionAddress,
                LocaleName = userWithAccountConf.Account.AccountConfiguration.Locale.Name,
                PlatformThinClientVersions = platformThinClientVersions,
            };
        }

        /// <summary>
        /// Get a list of thin client versions
        /// </summary>
        /// <returns></returns>
        private async Task<IEnumerable<PlatformThinClientVersionDto>> GetPlatformThinClientVersions(CancellationToken cancellationToken)
            => await unitOfWork.PlatformVersionReferencesRepository
                .AsQueryable()
                .Where(x =>!string.IsNullOrEmpty(x.MacOsThinClientDownloadLink) && !string.IsNullOrEmpty(x.WindowsThinClientDownloadLink))
                .Select(x => new PlatformThinClientVersionDto
                {
                    Version = x.Version,
                    MacOsDownloadLink = x.MacOsThinClientDownloadLink,
                    WindowsDownloadLink = x.WindowsThinClientDownloadLink
                })
                .ToListAsync(cancellationToken);
    }
}
