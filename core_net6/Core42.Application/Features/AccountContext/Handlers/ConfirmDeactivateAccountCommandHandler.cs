﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AlphaNumericsSupport;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountContext.Commands;
using Core42.Application.Features.AccountUsersContext.Commands;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.DataModels;

namespace Core42.Application.Features.AccountContext.Handlers
{
    public class ConfirmDeactivateAccountCommandHandler : IRequestHandler<ConfirmDeactivateAccountCommand, ManagerResult>
    {
        private readonly IHandlerException _handlerException;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger42 _logger;
        private readonly IRent1CConfigurationAccessProvider _rent1CConfigurationAccessProvider;
        private readonly DesEncryptionProvider _desEncryptionProvider;
        private readonly IStartProcessOfArchiveDatabaseToTombProvider _startProcessOfArchiveDatabaseToTombProvider;
        private readonly IArchiveAccountDatabaseToTombProcess _archiveAccountDatabaseToTombProcess;
        private readonly ISender _sender;
        public ConfirmDeactivateAccountCommandHandler(IServiceProvider serviceProvider)
        {
            _sender = serviceProvider.GetRequiredService<ISender>();
            _unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();
            _handlerException = serviceProvider.GetRequiredService<IHandlerException>();
            _logger = serviceProvider.GetRequiredService<ILogger42>();
            _rent1CConfigurationAccessProvider = serviceProvider.GetRequiredService<IRent1CConfigurationAccessProvider>();
            _desEncryptionProvider = serviceProvider.GetRequiredService<DesEncryptionProvider>();
            serviceProvider.GetRequiredService<AesEncryptionProvider>();
            _startProcessOfArchiveDatabaseToTombProvider = serviceProvider.GetRequiredService<IStartProcessOfArchiveDatabaseToTombProvider>();
            _archiveAccountDatabaseToTombProcess = serviceProvider.GetRequiredService<IArchiveAccountDatabaseToTombProcess>();
        }

        public async Task<ManagerResult> Handle(ConfirmDeactivateAccountCommand request,
            CancellationToken cancellationToken)
        {
           
            
            var deleteConfirmation = await _unitOfWork.EmailSmsVerificationRepository
                .AsQueryable()
                .FirstOrDefaultAsync(x =>
                    x.Code == request.ConfirmationCode, cancellationToken);

            if (deleteConfirmation is null)
            {
                return ManagerResultHelper.PreconditionFailed("Такой заявки на удаление не существует");
            }
            else if (deleteConfirmation.ExpiresOn < DateTime.Now)
            {
                return ManagerResultHelper.PreconditionFailed("Заявка просрочена, отправьте новую");
            }

            var account =
                (await _unitOfWork.AccountUsersRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x =>
                        x.Id == deleteConfirmation.AccountUserId, cancellationToken));
            
            
            if (account == null)
            {
                string message = "Аккаунта не существует";
                _logger.Error(message);
                return ManagerResultHelper.PreconditionFailed(message);
            }

            if (account.Removed == true)
            {
                string message = "Аккаунт уже был удален";
                _logger.Warn(message);
                return ManagerResultHelper.PreconditionFailed(message);
            }
            try
            {
                await DeactivateAccount(account.AccountId);
                account.Removed = true;
                account.CorpUserSyncStatus = "Deleted";
                await _unitOfWork.SaveAsync();
                return ManagerResultHelper.Ok();
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Ошибка удаления аккаунта] '{account.AccountId}'.";
                _handlerException.Handle(ex, errorMessage);
                return ManagerResultHelper.PreconditionFailed($"{errorMessage}. Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Метод деактивации аккаунта
        /// </summary>
        /// <returns></returns>
        public async Task DeactivateAccount(Guid accountId)
        {
            _logger.Info($"Начинаю деактивацию аккаунта {accountId}");
            var allAccountUsers = await _unitOfWork.AccountUsersRepository.GetAccountUsersAsync(accountId);

            _logger.Info("Деактивирую пользователей");
            var alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());

            foreach (var accountUser in allAccountUsers)
            {
                _logger.Info($"Начинаю редактирование пользователя {accountUser.Id}");
                var newPassword = alphaNumeric42CloudsGenerator.GeneratePassword();
                var userEditCommand = new AccountUserEditCommand
                {
                    Id = accountUser.Id,
                    Email = null,
                    PhoneNumber = null,
                    Password = _desEncryptionProvider.Encrypt(newPassword),
                    Login = accountUser.Login, 
                };
                var result =  await _sender.Send(userEditCommand);
                _logger.Info($"Результат редактирования пользователя {accountUser.Id}:    {result.Result} с сообщением {result.Message}");
            }
            _logger.Info("Закончил деактивацию пользователей");
            DisableRent1CAllAccountUser(accountId, allAccountUsers);
            await ArchiveAllAccountDatabase(accountId);
        }


        private void DisableRent1CAllAccountUser(Guid accountId, List<AccountUser> allAccountUsers)
        {
            _logger.Info($"Всего в аккаунте {allAccountUsers.Count} пользователей, начинаю выключение аренды");
            var userForDisableRent = allAccountUsers
                .Select(x => new UpdaterAccessRent1CRequestDto
                {
                    AccountUserId = x.Id, StandartResource = false, WebResource = false
                })
                .ToList();

            _rent1CConfigurationAccessProvider.ConfigureAccesses(accountId, userForDisableRent);
        }

        private async Task ArchiveAllAccountDatabase(Guid accountId)
        {
            try
            {
                var allAccountDatabases = _unitOfWork.DatabasesRepository
                    .AsQueryable()
                    .Where(x => x.AccountId == accountId && x.State == DatabaseState.Ready.ToString());

                var allCoreAccountDatabases = await allAccountDatabases
                    .Where(x => x.AccountDatabaseOnDelimiter == null)
                    .Select(x => x.Id)
                    .ToListAsync();
                _logger.Info($"Всего баз в ядре {allCoreAccountDatabases.Count} начинаю архивацию");

                _startProcessOfArchiveDatabaseToTombProvider.StartProcessOfArchiveDatabasesToTomb(allCoreAccountDatabases);

                var allMsAccountDatabases = await allAccountDatabases
                    .Where(x => x.AccountDatabaseOnDelimiter != null)
                    .ToListAsync();

                _logger.Info($"Всего баз в МС {allMsAccountDatabases.Count} начинаю архивацию");
                allMsAccountDatabases.ForEach(x => _archiveAccountDatabaseToTombProcess.DeleteDatabaseOnDelimiterInMs(x));
            } 
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка архивации баз при удалении аккаунта] {accountId}");
            }
        }
    }
}
