﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using Newtonsoft.Json;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Handlers;

public class GetAccountIdByFilterQueryHandler(IUnitOfWork unitOfWork, ILogger42 logger)
    : IRequestHandler<GetAccountIdByFilterQuery, ManagerResult<Guid?>>
{
    public async Task<ManagerResult<Guid?>> Handle(GetAccountIdByFilterQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var prefix = CloudConfigurationProvider.Invoices.UniqPrefix();


            if (request.Invoice != null)
            {
                if (request.Invoice.Uniq.HasValue)
                {
                    var uniqString = request.Invoice.Uniq.Value.ToString();
                    if (uniqString.Length > prefix.Length)
                    {
                        request.Invoice.Uniq = long.Parse(uniqString[prefix.Length..]);
                    }
                }

                var invoice = await unitOfWork.InvoiceRepository
                    .AsQueryable()
                    .AutoFilter(request.Invoice, ComposeKind.Or)
                    .FirstOrDefaultAsync(cancellationToken);

                if (invoice != null)
                {
                    return new ManagerResult<Guid?> { State = ManagerResultState.Ok, Result = invoice.AccountId };
                }
            }

            // ReSharper disable once InvertIf
            if (request.AccountUser != null)
            {
                var user = await unitOfWork.AccountUsersRepository
                    .AsQueryable()
                    .AutoFilter(request.AccountUser, ComposeKind.Or)
                    .FirstOrDefaultAsync(x => x.AccountUserRoles.Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin), cancellationToken);

                if (user != null)
                {
                    return new ManagerResult<Guid?> { State = ManagerResultState.Ok, Result = user.AccountId };
                }
            }

            return PagedListExtensions.ToPreconditionFailedManagerResult<Guid?>(
                $"Не удалось найти идентификатор аккаунта по присланным данным:\n{JsonConvert.SerializeObject(request)}");
        }

        catch (Exception ex)
        {
            logger.Error(ex, ex.Message);

            return PagedListExtensions.ToPreconditionFailedManagerResult<Guid?>(ex.Message);
        }

    }
}
