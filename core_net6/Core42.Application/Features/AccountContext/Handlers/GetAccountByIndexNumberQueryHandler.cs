﻿using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountContext.Dtos;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.SegmentContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Handlers
{

    /// <summary>
    /// Get account by index number query handler
    /// </summary>
    public class GetAccountByIndexNumberQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ILogger42 logger,
        ISender sender)
        : IRequestHandler<GetAccountByIndexNumberQuery, ManagerResult<AccountCardDto>>
    {
        /// <summary>
        /// Handle get account by index number query
        /// </summary>
        public async Task<ManagerResult<AccountCardDto>> Handle(GetAccountByIndexNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Accounts_View);

                var accountByIndexNumber = await unitOfWork.AccountsRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(a => a.IndexNumber == request.IndexNumber, cancellationToken) 
                    ?? throw new NotFoundException($"По номер {request.IndexNumber} аккаунт не найден");

                var accountDto = await sender.Send(new GetAccountByIdQuery(accountByIndexNumber.Id), cancellationToken);

                if (accountDto.Error)
                    throw new InvalidOperationException(accountDto.Message);

                return new AccountCardDto
                {
                    CommonData = mapper.Map<AccountCardCommonDataDto>(accountDto.Result),
                    AccountInfo = accountDto.Result,
                    Locales = accountDto.Result.Locales,
                    Segments = (await sender.Send(new GetAvailableMigrationSegmentsByIdQuery(accountDto.Result.SegmentId), cancellationToken)).Result,
                    EditableFields = new AccountCardEditableFieldsDto
                    {
                        IsAccountCaptionEditable = accessProvider.HasAccessBool(ObjectAction.Accounts_Edit, () => accountDto.Result.AccountId),
                        IsInnEditable = accessProvider.HasAccessBool(ObjectAction.Accounts_Edit, () => accountDto.Result.AccountId),
                        IsLocaleEditable = accountDto.Result.CanChangeLocale
                    },
                    TabVisibility = new AccountCardTabsVisibilityDto
                    {
                        IsAccountDetailsTabVisible = accessProvider.HasAccessBool(ObjectAction.Accounts_MoveInfoBase),
                        IsMigrateDatabaseTabVisible = accessProvider.HasAccessBool(ObjectAction.Accounts_MoveInfoBase),
                        IsChangeSegmentTabVisible = accessProvider.HasAccessBool(ObjectAction.Accounts_MoveInfoBase) &&
                                                    accessProvider.HasAccessBool(ObjectAction.Accounts_EditFullInfoWithSegments, () => accountDto.Result.AccountId)
                    },
                    VisibleFields = new AccountCardFieldsVisibilityDto
                    {
                        CanShowFullInfo = accessProvider.HasAccessBool(ObjectAction.Accounts_ViewFullInfo, () => accountDto.Result.AccountId),

                        CanShowSaleManagerInfo = accessProvider.HasAccessBool(ObjectAction.ControlPanel_SetSalemanager, () => accountDto.Result.AccountId)
                    },
                    ButtonsVisibility = new AccountCardButtonsVisibilityDto
                    {
                        IsGotoToAccountButtonVisible = accessProvider.HasAccessBool(ObjectAction.ControlPanel_ModeOtherAccount, () => accountDto.Result.AccountId) && accessProvider.ContextAccountId != accountDto.Result.AccountId
                    },
                }.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                logger.Error(ex, $"При получение аккаунта по номеру {request.IndexNumber} произошла ошибка");

                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountCardDto>(ex.Message);
            }

        }
    }
}
