﻿using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Core42.Application.Features.AccountContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;
using Clouds42.Configurations.Configurations;
using AutoMapper.QueryableExtensions;
using Core42.Application.Features.AccountContext.Specifications;
using Core42.Application.Features.AccountContext.Queries;
using CommonLib.Enums;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.AccountContext.Handlers
{
    /// <summary>
    /// Get filtered, sorted and paginated accounts query handler
    /// </summary>
    public class GetFilteredAccountsQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ILogger42 logger)
        : IRequestHandler<GetFilteredAccountsQuery, ManagerResult<PagedDto<AccountDto>>>
    {
        /// <summary>
        /// Handle get filtered, sorted and paginated accounts query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<AccountDto>>> Handle(GetFilteredAccountsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccessForMultiAccounts(ObjectAction.Accounts_View);

                const int minSearchYear = 2000;

                var registeredFrom = request?.Filter?.RegisteredFrom;

                var searchInvoice = "";
                var invoicePref = CloudConfigurationProvider.Invoices.UniqPrefix();

                if (registeredFrom is { Year: < minSearchYear })
                    registeredFrom = new DateTime(minSearchYear, registeredFrom.Value.Month, registeredFrom.Value.Day);

                if (!string.IsNullOrEmpty(request?.Filter?.SearchLine) && request?.Filter?.SearchLine.Length >= invoicePref.Length)
                    searchInvoice = request?.Filter?.SearchLine?[invoicePref.Length..];

                var result = (await unitOfWork.AccountsRepository
                    .AsQueryable()
                    .Where((AccountSpecification.BySearchLine(request?.Filter?.SearchLine) ||
                            AccountSpecification.ByInvoiceUniq(searchInvoice) ||
                            AccountSpecification.ByIsVip(request?.Filter?.SearchLine)) &&
                            AccountSpecification.ByRegisterDate(registeredFrom, request?.Filter?.RegisteredTo) &&
                            AccountSpecification.ByOnlyMine(request?.Filter?.OnlyMine, (await accessProvider.GetUserAsync()).Id) &&
                            AccountSpecification.ByNotDeleted())
                    .ProjectTo<AccountDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                .ToPagedDto();


                var accountIds = result.Records.Select(x => x.AccountId).Distinct().ToList();

                var rent1CResource = new List<ResourceType> { ResourceType.MyEntUserWeb, ResourceType.MyEntUser };
                var accountsActiveRent1CUsers = (await unitOfWork.ResourceRepository
                    .AsQueryable()
                    .Where(x => x.Subject != null && rent1CResource.Contains(x.BillingServiceType.SystemServiceType.Value) &&
                                (accountIds.Contains(x.AccountId) || accountIds.Contains(x.AccountSponsorId.Value)))
                    .ToListAsync(cancellationToken))
                    .GroupBy(x => new { x.AccountId, x.Subject });

                result.Records
                    .GroupJoin(accountsActiveRent1CUsers, x => x.AccountId, z => z.Key.AccountId, (record, rents) => (record, rents))
                    .ToList()
                    .ForEach(x => x.record.CountOfActiveRent1CUsers = x.rents.Count());

                return result.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                logger.Warn(ex,
                    "Произошла ошибка при получении модели отображения аккаунтов");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AccountDto>>(ex.Message);
            }
        }
    }
}
