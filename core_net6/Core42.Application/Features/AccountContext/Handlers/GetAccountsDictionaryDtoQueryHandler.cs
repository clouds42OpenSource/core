﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountContext.Dtos;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountContext.Specifications;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Handlers
{

    /// <summary>
    /// Get accounts as key-value query handler
    /// </summary>
    public class GetAccountsDictionaryDtoQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper)
        : IRequestHandler<GetAccountsDictionaryDtoQuery, ManagerResult<List<AccountDictionaryDto>>>
    {
        /// <summary>
        /// Handle get accounts as key-value query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<AccountDictionaryDto>>> Handle(GetAccountsDictionaryDtoQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.SupplierReference_View, () => accessProvider.ContextAccountId);

                return (await unitOfWork.AccountsRepository
                    .AsQueryable()
                    .Where(AccountSpecification.BySearchLine(request.SearchLine))
                    .ProjectTo<AccountDictionaryDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при получении рефералов аккаунта для поставщика.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<AccountDictionaryDto>>(ex.Message);
            }
        }
    }
}
