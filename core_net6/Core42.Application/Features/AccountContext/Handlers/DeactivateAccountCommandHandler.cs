﻿using System.Text;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountContext.Commands;
using Core42.Application.Features.EmailContext.Commands;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Core42.Application.Features.AccountContext.Handlers
{
    public class DeactivateAccountCommandHandler(IServiceProvider serviceProvider)
        : IRequestHandler<DeactivateAccountCommand, ManagerResult>
    {
        private readonly IAccessProvider _accessProvider = serviceProvider.GetRequiredService<IAccessProvider>();
        private readonly ILogger42 _logger = serviceProvider.GetRequiredService<ILogger42>();
        private readonly IHandlerException _handlerException = serviceProvider.GetRequiredService<IHandlerException>();
        private readonly IUnitOfWork _unitOfWork = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly ISender _sender = serviceProvider.GetRequiredService<ISender>();

        public async Task<ManagerResult> Handle(DeactivateAccountCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _logger.Info($"Начинаю отправлять письмо с подтверждением удаления аккаунта {request.AccountId}");
                _accessProvider.HasAccess(ObjectAction.Accounts_Delete);

                var currentUser = await _accessProvider.GetUserAsync();
                var userAdminId = currentUser.Id;

                if (currentUser.RequestAccountId != request.AccountId)
                    userAdminId = _unitOfWork.AccountsRepository.GetAccountAdminIds(request.AccountId).FirstOrDefault();

                var accountUserAdmin =
                    await _unitOfWork.AccountUsersRepository.FirstOrDefaultAsync(x => x.Id == userAdminId);

                var code = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Guid.NewGuid()}"));
                var minutesToExpire = CloudConfigurationProvider.Emails.GetMinutesToExpireCode();

                var databaseModel = new EmailSmsVerification
                {
                    AccountUserId = accountUserAdmin.Id,
                    Code = code,
                    ExpiresOn = DateTime.Now.AddMinutes(int.Parse(minutesToExpire)),
                    Type = VerificationType.Email,
                    CreatedOn = DateTime.Now
                };
                _unitOfWork.EmailSmsVerificationRepository.Insert(databaseModel);
                await _unitOfWork.SaveAsync();

                _logger.Info($"Заявка на подтверждение удаления аккаунта {request.AccountId} добавлена в базу");

                var linkToConfirmDeleteAccount =
                    string.Format(CloudConfigurationProvider.Emails.GetEmailConfirmDeleteAccountLink(), code);
                var sendGridDefaultTemplateId = CloudConfigurationProvider.SendGrid.GetDefaultTemplateId();
                var emailTo = accountUserAdmin.Email;

                if (string.IsNullOrEmpty(emailTo))
                {
                    return ManagerResultHelper.PreconditionFailed(
                        $"У админа {accountUserAdmin.Login} не указана почта");
                }

                var body = "<div style=\"padding: 0 7%\">" +
                           $"<p>Для подтверждения удаления аккаунта введите этот код <mark>{code}</mark></p>" +
                           "<p>или нажмите на кнопку ниже</p>" +
                           $"<a href=\"{linkToConfirmDeleteAccount}\" style=\"display: inline-block; padding: 10px 20px; color: white; background-color: red; " +
                           $"border: none; border-radius: 5px; text-decoration: none; text-align: center; cursor: pointer;\">Удалить аккаунт</a>" +
                           "</div>";

                await _sender.Send(
                    new SendEmailCommand
                    {
                        Body = body,
                        Categories = ["account-delete"],
                        Header = "Удаление аккаунта",
                        Subject = "Удаление аккаунта",
                        Locale = "ru-ru",
                        To = emailTo,
                        SendGridTemplateId = sendGridDefaultTemplateId
                    }, cancellationToken);

                _logger.Info(
                    $"На почту {emailTo} пользователя {accountUserAdmin.Id} отправлено письмо-подтверждение для удаления аккаунта");
                LogEventHelper.LogEvent(_unitOfWork, currentUser?.RequestAccountId ?? accountUserAdmin.AccountId,
                    _accessProvider, LogActions.SendEmailVerificationCodeToUser,
                    $"На почту {emailTo} отправлено письмо-подтверждение для удаления аккаунта");

                return ManagerResultHelper.Ok();
            }
            catch (AccessDeniedException ex)
            {
                return ManagerResultHelper.Forbidden(ex.Message);
            }
            catch (Exception ex)
            {
                var errorMessage = $"[Отправки письма-подтверждения удаления] аккаунта '{request.AccountId}'.";
                _handlerException.Handle(ex, errorMessage);
                return ManagerResultHelper.PreconditionFailed($"{errorMessage}. Причина: {ex.Message}");
            }
        }
    }
}
