﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountContext.Dtos;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.DepartmentsContext;
using Core42.Application.Features.LocaleContext.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Handlers
{
    /// <summary>
    /// Get account by id query handler
    /// </summary>
    public class GetAccountByIdQueryHandler(
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ICheckAccountDataProvider checkAccountDataProvider,
        ICloud42ServiceHelper cloud42ServiceHelper,
        AccountDataProvider accountDataProvider)
        : IRequestHandler<GetAccountByIdQuery, ManagerResult<AccountDetailsDto>>
    {
        /// <summary>
        /// Handler get account by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<AccountDetailsDto>> Handle(GetAccountByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var account = await unitOfWork.AccountsRepository
                    .AsQueryable()
                    .ProjectTo<AccountDetailsDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.AccountId == request.AccountId, cancellationToken);

                account.CanChangeLocale = checkAccountDataProvider.CanChangeLocaleForAccount(account.AccountId).Item1;
                account.UsedSizeOnDisk = cloud42ServiceHelper.GetMyDiskInfo(account.AccountId).UsedSizeOnDisk;
                account.AccountType = accountDataProvider.GetAccountType(account.ReferralAccountId, account.ConfigurationType);
                account.Locales = await unitOfWork.LocaleRepository.AsQueryable().ProjectTo<LocaleDictionaryDto>(mapper.ConfigurationProvider).ToListAsync(cancellationToken);
                account.Departments = await GetDepartments(account.AccountId);
                return account.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Получение детальной информации аккаунта] '{request.AccountId}' завершилось ошибкой");

                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountDetailsDto>(ex.Message);
            }
        }

        private async Task<List<DepartmentsDto>> GetDepartments(Guid accountId)
        {
            return await unitOfWork
                .DepartmentRepository
                .AsQueryableNoTracking()
                .Where(d => d.AccountId == accountId)
                .Select(d => new DepartmentsDto { DepartmentId = d.Id, Name = d.Name })
                .ToListAsync();
        }
    }
}
