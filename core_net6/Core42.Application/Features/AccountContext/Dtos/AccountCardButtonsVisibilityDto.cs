﻿namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Account card buttons visibilty dto
    /// </summary>
    public class AccountCardButtonsVisibilityDto
    {
        /// <summary>
        /// Flag to show go to account button
        /// </summary>
        public bool IsGotoToAccountButtonVisible { get; set; }
    }
}
