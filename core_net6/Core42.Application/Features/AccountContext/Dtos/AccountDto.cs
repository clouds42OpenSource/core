﻿using AutoMapper;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Account dto
    /// </summary>
    public class AccountDto : IMapFrom<Account>
    {
        /// <summary>
        ///  Account identifier
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Account index number
        /// </summary>
        public int IndexNumber { set; get; }

        /// <summary>
        /// Account caption
        /// </summary>
        public string AccountCaption { set; get; }

        /// <summary>
        /// Admin account login
        /// </summary>
        public string AccountAdminLogin { set; get; }

        /// <summary>
        /// Admin account email
        /// </summary>
        public string AccountAdminEmail { set; get; }

        /// <summary>
        /// Account users count
        /// </summary>
        public int AccountUsersCount { set; get; }

        /// <summary>
        /// Account reg date
        /// </summary>
        public DateTime? AccountRegistrationDate { set; get; }

        /// <summary>
        /// Rent 1C expire date
        /// </summary>
        public DateTime? Rent1CExpiredDate { set; get; }

        /// <summary>
        /// Count of user with active rent 1C
        /// </summary>
        public int CountOfActiveRent1CUsers { get; set; }

        /// <summary>
        /// Flag if account is VIP
        /// </summary>
        public bool IsVip { set; get; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Account, AccountDto>()
                .ForMember(x => x.IndexNumber, z => z.MapFrom(y => y.IndexNumber))
                .ForMember(x => x.AccountId, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.AccountCaption, z => z.MapFrom(y => y.AccountCaption))
                .ForMember(x => x.AccountAdminEmail, z => z.MapFrom(y => y.AccountUsers.FirstOrDefault(x =>
                                                                     x.AccountUserRoles.Any(zy => zy.AccountUserGroup == AccountUserGroup.AccountAdmin)).Email))
                .ForMember(x => x.AccountAdminLogin, z => z.MapFrom(y => y.AccountUsers.FirstOrDefault(x =>
                                                                     x.AccountUserRoles.Any(zy => zy.AccountUserGroup == AccountUserGroup.AccountAdmin)).Login))
                .ForMember(x => x.AccountUsersCount, z => z.MapFrom(y => y.AccountUsers.Count(z => z.CorpUserSyncStatus != "Deleted" && z.CorpUserSyncStatus != "SyncDeleted")))
                .ForMember(x => x.AccountRegistrationDate, z => z.MapFrom(y => y.RegistrationDate))
                .ForMember(x => x.Rent1CExpiredDate, z => z.MapFrom(y => y.BillingAccount.ResourcesConfigurations.FirstOrDefault(x => x.BillingService.SystemService == Clouds42Service.MyEnterprise).ExpireDate))
                .ForMember(x => x.IsVip, z => z.MapFrom(y => y.AccountConfiguration.IsVip));
        }
    }
}
