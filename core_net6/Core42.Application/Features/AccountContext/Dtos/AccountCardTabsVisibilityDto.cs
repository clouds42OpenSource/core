﻿namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Account card tabs visibilty dto
    /// </summary>
    public class AccountCardTabsVisibilityDto
    {
        /// <summary>
        /// Can show account details tab
        /// </summary>
        public bool IsAccountDetailsTabVisible { get; set; }

        /// <summary>
        /// Can show change segment tab
        /// </summary>
        public bool IsChangeSegmentTabVisible { get; set; }

        /// <summary>
        /// Can show database migration tab
        /// </summary>
        public bool IsMigrateDatabaseTabVisible { get; set; }
    }
}
