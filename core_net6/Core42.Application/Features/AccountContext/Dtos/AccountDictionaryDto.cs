﻿using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Account key-value dto
    /// </summary>
    public class AccountDictionaryDto : DictionaryDto<Guid, string>, IMapFrom<Account>
    {
        /// <summary>
        /// Account index number
        /// </summary>
        [JsonIgnore]
        public string IndexNumber { get; set; }

        /// <summary>
        /// Account caption
        /// </summary>
        [JsonIgnore]
        public string AccountCaption { get; set; }


        /// <summary>
        /// Account user login
        /// </summary>
        [JsonIgnore]
        public string AccountUserLogin { get; set; }

        [NotMapped]
        public new string Value
        {
            get { return $"{IndexNumber} ({AccountCaption} {AccountUserLogin})"; }
        }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Account, AccountDictionaryDto>()
                .ForMember(x => x.Key, m => m.MapFrom(z => z.Id))
                .ForMember(x => x.IndexNumber, m => m.MapFrom(z => z.IndexNumber))
                .ForMember(x => x.AccountCaption, m => m.MapFrom(z => z.AccountCaption))
                .ForMember(x => x.AccountUserLogin, m => m.MapFrom(z => z.AccountUsers
                    .FirstOrDefault(x => x.AccountUserRoles.Any(y => y.AccountUserGroup == AccountUserGroup.AccountAdmin))!.Login))
                .ForMember(x => x.Value, m => m.Ignore());
        }
    }
}
