﻿namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Account card editable fields dto
    /// </summary>
    public class AccountCardEditableFieldsDto
    {
        /// <summary>
        /// Can edit account caption field
        /// </summary>
        public bool IsAccountCaptionEditable { get; set; }
        /// <summary>
        /// Can edit inn field
        /// </summary>
        public bool IsInnEditable { get; set; }

        /// <summary>
        /// Can edit locale field
        /// </summary>
        public bool IsLocaleEditable { get; set; }
    }
}
