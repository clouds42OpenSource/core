﻿using AutoMapper;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Account card common data dto
    /// </summary>
    public class AccountCardCommonDataDto : IMapFrom<AccountDetailsDto>
    {
        /// <summary>
        /// Used size on disk
        /// </summary>
        public long? UsedSizeOnDisk { get; set; }

        /// <summary>
        /// Account services total sum
        /// </summary>
        public decimal ServiceTotalSum { get; set; }

        /// <summary>
        /// Account type
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// Account sale manager caption
        /// </summary>
        public string AccountSaleManagerCaption { get; set; }

        /// <summary>
        /// Account balance
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Last activity
        /// </summary>
        public DateTime? LastActivity { get; set; }

        /// <summary>
        /// Segment name
        /// </summary>
        public string SegmentName { get; set; }

        /// <summary>
        /// Emails for mailing
        /// </summary>
        public List<string> Emails { get; set; }


        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountDetailsDto, AccountCardCommonDataDto>();
        }
    }
}
