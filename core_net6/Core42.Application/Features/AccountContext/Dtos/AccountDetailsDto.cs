﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.AccountAdditionalCompanyContext.Dtos;
using Core42.Application.Features.DepartmentsContext;
using Core42.Application.Features.LocaleContext.Dtos;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Account details dto
    /// </summary>
    public class AccountDetailsDto : IMapFrom<Account>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Account caption
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Account index number
        /// </summary>
        public int IndexNumber { get; set; }

        /// <summary>
        /// Account INN
        /// </summary>
        public string AccountInn { get; set; }

        /// <summary>
        /// Flag if account is VIP
        /// </summary>
        public bool IsVip { get; set; }
        
        /// <summary>
        /// Flag if requeserd clustered db creation
        /// </summary>
        public bool CreateClusterDatabase { get; set; }

        /// <summary>
        /// Account description
        /// </summary>
        public string AccountDescription { get; set; }

        /// <summary>
        /// Segment identifier
        /// </summary>
        public Guid? SegmentId { get; set; }

        /// <summary>
        /// Supplair identifier
        /// </summary>
        public Guid? SupplierId { get; set; }

        /// <summary>
        /// Locale identifier
        /// </summary>
        public Guid? LocaleId { get; set; }

        public List<DepartmentsDto> Departments { get; set; }

        /// <summary>
        /// Registration date
        /// </summary>
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Account currency
        /// </summary>
        public string LocaleCurrency { get; set; }

        /// <summary>
        /// Cloud storage web link
        /// </summary>
        public string CloudStorageWebLink { get; set; }

        /// <summary>
        /// User source
        /// </summary>
        public string UserSource { get; set; }

        /// <summary>
        /// Account type
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// Used size on disk
        /// </summary>
        public long? UsedSizeOnDisk { get; set; }

        /// <summary>
        /// Sale manager caption
        /// </summary>
        public string AccountSaleManagerCaption { get { return string.IsNullOrEmpty(AccountUserLogin) ? "-" : $"{AccountUserLogin} ({SalesManagerDivision})"; } }

        /// <summary>
        /// Emails for mailing
        /// </summary>
        public List<string> Emails { get; set; }

        /// <summary>
        /// Segment name
        /// </summary>
        public string SegmentName { get; set; }

        /// <summary>
        /// Account balance
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Last activity
        /// </summary>
        public DateTime? LastActivity { get; set; }

        /// <summary>
        /// Account services total sum
        /// </summary>
        public decimal ServiceTotalSum { get; set; }

        /// <summary>
        /// Flag to change locale
        /// </summary>
        public bool CanChangeLocale { get; set; }

        /// <summary>
        /// All locales
        /// </summary>
        public List<LocaleDictionaryDto> Locales { get; set; }

        /// <summary>
        /// Account user login
        /// </summary>
        [JsonIgnore]
        public string AccountUserLogin { get; set; }

        /// <summary>
        /// Sale manager division
        /// </summary>
        [JsonIgnore]
        public string SalesManagerDivision { get; set; }

        /// <summary>
        /// Account configuration type
        /// </summary>
        [JsonIgnore]
        public string ConfigurationType { get; set; }

        /// <summary>
        /// Referral account identifier
        /// </summary>
        [JsonIgnore]
        public Guid? ReferralAccountId { get; set; }

        public int? Deployment { get; set; }

        public VerificationStatus? VerificationStatus { get; set; }

        public List<AccountAdditionalCompanyDto> AdditionalCompanies { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Account, AccountDetailsDto>()
                .ForMember(z => z.IndexNumber, x => x.MapFrom(y => y.IndexNumber))
                .ForMember(z => z.ReferralAccountId, x => x.MapFrom(y => y.ReferralAccountID))
                .ForMember(z => z.Balance, x => x.MapFrom(y => y.BillingAccount.Balance))
                .ForMember(z => z.Emails, x => x.MapFrom(y => y.AccountEmails.Select(x => x.Email)))
                .ForMember(z => z.LocaleId, x => x.MapFrom(y => y.AccountConfiguration.LocaleId))
                .ForMember(z => z.AccountCaption, x => x.MapFrom(y => y.AccountCaption))
                .ForMember(z => z.AccountInn, x => x.MapFrom(y => y.AccountRequisites.Inn))
                .ForMember(z => z.IsVip, x => x.MapFrom(y => y.AccountConfiguration.IsVip))
                .ForMember(z => z.CreateClusterDatabase, x => x.MapFrom(y => y.AccountConfiguration.CreateClusterDatabase))
                .ForMember(z => z.CloudStorageWebLink, x => x.MapFrom(y => y.AccountConfiguration.CloudStorageWebLink))
                .ForMember(z => z.RegistrationDate, x => x.MapFrom(y => y.RegistrationDate))
                .ForMember(z => z.SegmentId, x => x.MapFrom(y => y.AccountConfiguration.SegmentId))
                .ForMember(z => z.SupplierId, x => x.MapFrom(y => y.AccountConfiguration.SupplierId))
                .ForMember(z => z.AccountId, x => x.MapFrom(y => y.Id))
                .ForMember(z => z.UserSource, x => x.MapFrom(y => y.UserSource))
                .ForMember(z => z.LocaleCurrency, x => x.MapFrom(y => y.AccountConfiguration.Locale.Currency))
                .ForMember(z => z.AccountDescription, x => x.MapFrom(y => y.Description))
                .ForMember(z => z.ServiceTotalSum, x => x.MapFrom(y => y.BillingAccount.ResourcesConfigurations.Where(x => !x.IsDemoPeriod).Sum(x => x.Cost)))
                .ForMember(z => z.SegmentName, x => x.MapFrom(y => y.AccountConfiguration.Segment.Name))
                .ForMember(z => z.LastActivity, x => x.MapFrom(y => y.Payments.Any() ? y.Payments.OrderByDescending(x => x.Date).FirstOrDefault().Date : y.RegistrationDate))
                .ForMember(z => z.AccountUserLogin, x => x.MapFrom(y => y.AccountSaleManager.AccountUser.Login))
                .ForMember(z => z.ConfigurationType, x => x.MapFrom(y => y.AccountConfiguration.Type))
                .ForMember(z => z.VerificationStatus, x => x.MapFrom(y => y.AccountFiles.OrderByDescending(m => m.UpdatedOn).FirstOrDefault(z => z.Type == DocumentType.TaxAuthorityRegistrationCertificate).Status))
                .ForMember(z => z.AdditionalCompanies, x => x.MapFrom(y => y.AdditionalCompanies))
                .ForMember(z => z.SalesManagerDivision, x => x.MapFrom(y => y.AccountSaleManager.Division));
        }
    }
}
