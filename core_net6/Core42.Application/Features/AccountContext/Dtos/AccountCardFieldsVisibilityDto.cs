﻿namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Acount card fields visibilty dto
    /// </summary>
    public class AccountCardFieldsVisibilityDto
    {
        /// <summary>
        /// Can show full info
        /// </summary>
        public bool CanShowFullInfo { get; set; }

        /// <summary>
        ///  Can show sale manager info
        /// </summary>
        public bool CanShowSaleManagerInfo { get; set; }
    }
}
