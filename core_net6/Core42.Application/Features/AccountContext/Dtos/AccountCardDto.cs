﻿using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.LocaleContext.Dtos;

namespace Core42.Application.Features.AccountContext.Dtos
{
    /// <summary>
    /// Account card main dto
    /// </summary>
    public class AccountCardDto
    {
        /// <summary>
        /// Button visibility info
        /// </summary>
        public AccountCardButtonsVisibilityDto ButtonsVisibility { get; set; }

        /// <summary>
        /// Account card common data dto
        /// </summary>
        public AccountCardCommonDataDto CommonData { get; set; }

        /// <summary>
        /// Account info
        /// </summary>
        public AccountDetailsDto AccountInfo { get; set; }

        /// <summary>
        /// Available locales list
        /// </summary>
        public List<LocaleDictionaryDto> Locales { get; set; }


        /// <summary>
        /// Available segments list
        /// </summary>
        public List<BaseDictionaryDto>? Segments { get; set; }

        /// <summary>
        /// Account card fields visibilty dto
        /// </summary>
        public AccountCardFieldsVisibilityDto VisibleFields { get; set; }

        /// <summary>
        /// Account card editable fields dto
        /// </summary>
        public AccountCardEditableFieldsDto EditableFields { get; set; }

        /// <summary>
        /// Account card tabs visibility dto
        /// </summary>
        public AccountCardTabsVisibilityDto TabVisibility { get; set; }
    }
}
