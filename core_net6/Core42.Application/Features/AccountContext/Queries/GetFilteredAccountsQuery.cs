﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.AccountContext.Dtos;
using Core42.Application.Features.AccountContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Queries
{
    /// <summary>
    /// Get filtered, sorted, paginated accounts query
    /// </summary>
    public class GetFilteredAccountsQuery : IRequest<ManagerResult<PagedDto<AccountDto>>>, ISortedQuery, IPagedQuery, IHasFilter<AccountFilter>
    {
        /// <summary>
        /// Account filter
        /// </summary>
        public AccountFilter? Filter { get; set; }

        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(AccountDto.AccountRegistrationDate)}.desc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// PageSize
        /// </summary>
        public int? PageSize { get; set; }
    }
}
