﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AccountContext.Queries.Filters;
using Core42.Application.Features.InvoiceContext.Queries.Filters;
using MediatR;

namespace Core42.Application.Features.AccountContext.Queries
{
    public class GetAccountIdByFilterQuery : IRequest<ManagerResult<Guid?>>
    {
        public InvoiceFilter? Invoice { get; set; }
        public AccountUserFilter? AccountUser { get; set; }
    }
}
