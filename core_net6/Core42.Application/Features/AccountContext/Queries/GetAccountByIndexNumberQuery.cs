﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AccountContext.Dtos;
using MediatR;

namespace Core42.Application.Features.AccountContext.Queries
{
    /// <summary>
    /// Get account by index number query
    /// </summary>
    public class GetAccountByIndexNumberQuery(int indexNumber) : IRequest<ManagerResult<AccountCardDto>>
    {
        /// <summary>
        /// Account index number
        /// </summary>
        public int IndexNumber { get; set; } = indexNumber;
    }
}
