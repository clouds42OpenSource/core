﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AccountContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.AccountContext.Queries
{
    /// <summary>
    /// Get acounts as key-value query
    /// </summary>
    public class GetAccountsDictionaryDtoQuery : IRequest<ManagerResult<List<AccountDictionaryDto>>>, IHasSpecificSearch
    {
        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }
    }
}
