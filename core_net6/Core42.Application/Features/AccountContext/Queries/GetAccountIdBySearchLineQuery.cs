﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using Newtonsoft.Json;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Queries
{
    public class GetAccountIdBySearchLineQuery : IRequest<ManagerResult<Guid?>>
    {
        public long? Uniq { get; set; }
        public string? Tin { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
    }

    public class GetAccountIdBySearchLineQueryHandler(IUnitOfWork unitOfWork, ILogger42 logger)
        : IRequestHandler<GetAccountIdBySearchLineQuery, ManagerResult<Guid?>>
    {
        public async Task<ManagerResult<Guid?>> Handle(GetAccountIdBySearchLineQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var prefix = CloudConfigurationProvider.Invoices.UniqPrefix();

                if (request.Uniq.HasValue)
                {
                    var uniqString = request.Uniq.Value.ToString();
                    if (uniqString.Length > prefix.Length)
                    {
                        request.Uniq = long.Parse(uniqString[prefix.Length..]);
                    }
                }

                var invoice = await unitOfWork.InvoiceRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x =>
                        string.IsNullOrEmpty(request.Tin) ||
                        x.Requisite == request.Tin ||
                        !request.Uniq.HasValue ||
                        x.Uniq == request.Uniq.Value, cancellationToken);

                if (invoice != null)
                {
                    return new ManagerResult<Guid?> { State = ManagerResultState.Ok, Result = invoice.AccountId };
                }

                var user = await unitOfWork.AccountUsersRepository.AsQueryable()
                    .FirstOrDefaultAsync(x => (string.IsNullOrEmpty(request.Email) ||
                                               x.Email == request.Email || 
                                               string.IsNullOrEmpty(request.Phone) || 
                                               x.PhoneNumber == request.Phone)  && 
                                               x.AccountUserRoles.Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin), cancellationToken);
                if (user != null)
                {
                    return new ManagerResult<Guid?> { State = ManagerResultState.Ok, Result = user.AccountId};
                }

                return PagedListExtensions.ToPreconditionFailedManagerResult<Guid?>(
                    $"Не удалось найти идентификатор аккаунта по присланным данным:\n{JsonConvert.SerializeObject(request)}");
            }

            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);

                return PagedListExtensions.ToPreconditionFailedManagerResult<Guid?>(ex.Message);
            }

        }
    }
}
