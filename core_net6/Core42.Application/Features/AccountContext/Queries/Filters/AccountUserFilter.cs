﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Queries.Filters;

public class AccountUserFilter : IQueryFilter
{
    public string? Email { get; set; }
    public string? PhoneNumber { get; set; }
}
