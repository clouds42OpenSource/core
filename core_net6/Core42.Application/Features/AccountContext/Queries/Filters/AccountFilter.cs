﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Queries.Filters
{
    /// <summary>
    /// Account query filter
    /// </summary>
    public class AccountFilter : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }

        /// <summary>
        /// Register date from
        /// </summary>
        public DateTime? RegisteredFrom { get; set; }

        /// <summary>
        /// Register date to
        /// </summary>
        public DateTime? RegisteredTo { get; set; }

        /// <summary>
        /// Only mine accounts
        /// </summary>
        public bool? OnlyMine { get; set; }
    }
}
