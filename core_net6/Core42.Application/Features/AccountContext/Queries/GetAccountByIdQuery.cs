﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AccountContext.Dtos;
using MediatR;

namespace Core42.Application.Features.AccountContext.Queries
{
    /// <summary>
    /// Get account by id query
    /// </summary>
    public class GetAccountByIdQuery(Guid accountId) : IRequest<ManagerResult<AccountDetailsDto>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; } = accountId;
    }
}
