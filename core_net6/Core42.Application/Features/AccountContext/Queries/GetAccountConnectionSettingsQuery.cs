﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using MediatR;

namespace Core42.Application.Features.AccountContext.Queries
{
    /// <summary>
    /// Model for getting account settings
    /// </summary>
    public class GetAccountConnectionSettingsQuery : IRequest<ManagerResult<AccountSettingsDataDto>>
    {
        /// <summary>
        /// Account user identifier
        /// </summary>
        public Guid AccountUserId { get; set; }
    }
}
