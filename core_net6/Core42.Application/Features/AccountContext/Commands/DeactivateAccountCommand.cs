﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountContext.Commands
{
    public class DeactivateAccountCommand : IRequest<ManagerResult>
    {
        public Guid AccountId { get; set; }
    }
}
