﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountContext.Commands
{
    public class ConfirmDeactivateAccountCommand : IRequest<ManagerResult>
    {
        public string ConfirmationCode { get; set; }
    }
}
