﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountContext.Commands;
using FluentValidation;

namespace Core42.Application.Features.AccountContext
{
    public class DeactivateAccountCommandValidator : AbstractValidator<DeactivateAccountCommand>
    {

        public DeactivateAccountCommandValidator(IUnitOfWork unitOfWork, IAccessProvider accessProvider)
        {
            RuleFor(x => x.AccountId)
                .NotNull()
                .Must(y =>
                {
                    var currentUser = accessProvider.GetUser();
                    var userAdminId = currentUser.Id;
                    var fiveMinutesAgoDateTime = DateTime.Now.AddMinutes(-5);

                    if (currentUser.RequestAccountId != y)
                        userAdminId = unitOfWork.AccountsRepository.GetAccountAdminIds(y).FirstOrDefault();

                    var accountUserAdmin =
                        unitOfWork.AccountUsersRepository.FirstOrDefault(x => x.Id == userAdminId);

                    var activeConfirm = unitOfWork.EmailSmsVerificationRepository
                        .AsQueryableNoTracking()
                        .OrderByDescending(esv => esv.CreatedOn)
                        .FirstOrDefault(x =>
                            x.AccountUserId == accountUserAdmin.Id &&
                            x.CreatedOn > fiveMinutesAgoDateTime);

                    return activeConfirm is null;
                })
                .WithMessage("За последние 5 минут у Вас уже есть активные заявки, проверьте свою почту");
        }
    }
}
