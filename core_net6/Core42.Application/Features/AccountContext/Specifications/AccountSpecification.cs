﻿using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountContext.Specifications
{
    /// <summary>
    /// Account specification class
    /// </summary>
    public static class AccountSpecification
    {
        /// <summary>
        /// Filter accounts by search line
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<Account> BySearchLine(string? searchLine)
        {
            return !string.IsNullOrEmpty(searchLine) ? new Spec<Account>(x =>
                       x.IndexNumber.ToString().Contains(searchLine) ||
                       x.AccountCaption.ToLower().Contains(searchLine.ToLower()) ||
                       x.AccountUsers.AsQueryable().Any(au => au.CorpUserSyncStatus != "Deleted" && au.CorpUserSyncStatus != "SyncDeleted" && 
                                                              au.Login.ToLower().Contains(searchLine.ToLower()) ||
                                                              au.FirstName.ToLower().Contains(searchLine.ToLower()) ||
                                                              au.LastName.ToLower().Contains(searchLine.ToLower()) ||
                                                              au.FullName.ToLower().Contains(searchLine.ToLower()) ||
                                                              au.MiddleName.ToLower().Contains(searchLine.ToLower()) ||
                                                              au.Email.ToLower().Contains(searchLine.ToLower()) || 
                                                              au.PhoneNumber.ToLower().Contains(searchLine.ToLower())) || 
                                                              x.AccountRequisites.Inn.ToLower().Contains(searchLine.ToLower())) : new Spec<Account>(x => true);
        }

        /// <summary>
        /// Filter accounts by invoices uniq
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<Account> ByInvoiceUniq(string? searchLine)
        {
            return !string.IsNullOrEmpty(searchLine) ? new Spec<Account>(x => x.Invoices.AsQueryable().Any(y => y.Uniq.ToString() == searchLine && y.AccountId == x.Id)) : new Spec<Account>(x => true);
        }

        /// <summary>
        /// Filter accounts by is vip flag
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<Account> ByIsVip(string? searchLine)
        {
            return !string.IsNullOrEmpty(searchLine) ? new Spec<Account>(x => (searchLine.Contains("vip") || searchLine.Contains("VIP")) && x.AccountConfiguration.IsVip) : new Spec<Account>(x => true);
        }

        /// <summary>
        /// Filter accounts by register date
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public static Spec<Account> ByRegisterDate(DateTime? dateFrom, DateTime? dateTo)
        {
            return dateFrom.HasValue && dateTo.HasValue ? new Spec<Account>(x => x.RegistrationDate.HasValue && dateFrom.Value <= x.RegistrationDate && 
                                                                                 x.RegistrationDate.HasValue && dateTo.Value >= x.RegistrationDate) : new Spec<Account>(x => true);
        }

        /// <summary>
        /// Filter accounts by is not deleted
        /// </summary>
        /// <returns></returns>
        public static Spec<Account> ByNotDeleted()
        {
            return new Spec<Account>(x => x.Removed != true && x.Status != "Deleted" && x.Status != "SyncDeleted");
        }

        /// <summary>
        /// Filter accounts by is only mine
        /// </summary>
        /// <param name="onlyMine"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        internal static Spec<Account> ByOnlyMine(bool? onlyMine, Guid userId)
        {
            return onlyMine.HasValue && onlyMine.Value ? new Spec<Account>(x => x.AccountSaleManager.SaleManagerId == userId) : new Spec<Account>(x => true);
        }
    }
}
