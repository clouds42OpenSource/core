﻿using Clouds42.Domain.DataModels.StateMachine;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Core42.Application.Features.ProcessFlowContext.Constants;
using Core42.Application.Features.ProcessFlowContext.Dtos;

namespace Core42.Application.Features.ProcessFlowContext.Extensions
{
    /// <summary>
    /// Process flow extensions
    /// </summary>
    public static class ProcessFlowStateExtension
    {

        /// <summary>
        /// Get state description
        /// </summary>
        /// <param name="processFlow">process flow</param>
        /// <returns>Process flow action description in FlowActionDescriptionAttribute</returns>
        public static string GetStateDescription(this ProcessFlow processFlow)
        {
            if (string.IsNullOrEmpty(processFlow.State))
                return null;

            var lastActionType = GetActionsCallChain(processFlow.State).LastOrDefault();
            if (lastActionType == null)
                return null;

            return GetFlowActionDescription(lastActionType) ?? lastActionType;
        }

        /// <summary>
        /// Get process flow type
        /// </summary>
        /// <param name="processFlow">Process flow.</param>
        /// <returns>Process flow type.</returns>
        public static Type GetProcessFlowType(this ProcessFlow processFlow)
        {
            var processFlowStringType = GetActionsCallChain(processFlow.Key).FirstOrDefault();

            if (string.IsNullOrEmpty(processFlowStringType))
                throw new InvalidOperationException($"Не удалось получить тип рабочего процесса из ключа '{processFlow.Key}'");

            return Type.GetType(processFlowStringType) ??
                   throw new InvalidOperationException(
                       $"Не удалось получить тип рабочего процесса по значению ключа '{processFlowStringType}'");
        }

        /// <summary>
        /// Get actions call chain
        /// </summary>
        /// <param name="stateMachineComponentState"></param>
        /// <returns></returns>
        public static List<string> GetActionsCallChain(string stateMachineComponentState)
            => stateMachineComponentState.Split([ProcessFlowsConstants.StateActionDelimiter],
                StringSplitOptions.None).ToList();

        /// <summary>
        /// Get action flow description from FlowActionDescriptionAttribute.
        /// </summary>
        /// <param name="flowActionType"></param>
        /// <returns></returns>
        public static string GetFlowActionDescription(string flowActionType)
        {
            var typeOfAction = GetActionFlowType(flowActionType);

            var flowActionDescriptionAttr = typeOfAction?.GetCustomAttributes(
                typeof(FlowActionDescriptionAttribute), true
            ).FirstOrDefault() as FlowActionDescriptionAttribute;

            return flowActionDescriptionAttr?.ActionDescription;
        }

        /// <summary>
        /// Get action flow name
        /// </summary>
        /// <param name="flowActionType">Action flow type</param>
        /// <returns>Action flow name</returns>
        public static string GetActionFlowName(string flowActionType)
            => GetActionFlowType(flowActionType)?.Name;

        /// <summary>
        /// Get action flow type
        /// </summary>
        /// <param name="flowActionType">Flow action type</param>
        /// <returns>Type action flow/returns>
        private static Type GetActionFlowType(string flowActionType)
            => Type.GetType(flowActionType);

        /// <summary>
        /// Get actions flows data
        /// </summary>
        /// <param name="actionFlows">action flows</param>
        /// <returns>actions flow info data</returns>
        public static List<ActionFlowDto> GetActionFlowsData(this ICollection<ActionFlow> actionFlows)
            => actionFlows.Select(acFl => new
            {
                acFl.Status,
                acFl.CountOfAttempts,
                acFl.ErrorMessage,
                ActionsChain = GetActionsCallChain(acFl.StateRefKey)
            })
            .OrderBy(acFl => acFl.ActionsChain.Count)
            .ToList()
            .Select(acFl =>
            {
                var currentActionType = acFl.ActionsChain.LastOrDefault();
                return new ActionFlowDto
                {
                    ErrorMessage = acFl.ErrorMessage,
                    CountOfAttemps = acFl.CountOfAttempts,
                    Status = acFl.Status,
                    ActionName = GetActionFlowName(currentActionType),
                    ActionDescription = GetFlowActionDescription(currentActionType)
                };
            }).ToList();

    }
}
