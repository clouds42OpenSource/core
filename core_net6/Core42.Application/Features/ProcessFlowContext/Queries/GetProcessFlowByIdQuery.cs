﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ProcessFlowContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ProcessFlowContext.Queries
{
    /// <summary>
    /// Get process flow by id query
    /// </summary>
    public class GetProcessFlowByIdQuery(Guid id) : IRequest<ManagerResult<ProcessFlowDto>>
    {
        /// <summary>
        /// Process flow id
        /// </summary>
        public Guid Id { get; set; } = id;
    }
}
