﻿using Clouds42.Domain.Enums.StateMachine;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ProcessFlowContext.Queries.Filters
{
    /// <summary>
    /// Process flow filter
    /// </summary>
    public class ProcessFlowFilter : IHasSpecificSearch, IQueryFilter
    {
        /// <summary>
        /// Status
        /// </summary>
        public StateMachineComponentStatus? Status { get; set; }

        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }
    }
}
