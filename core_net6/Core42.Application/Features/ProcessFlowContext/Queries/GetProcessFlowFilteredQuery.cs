﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels.StateMachine;
using Core42.Application.Features.ProcessFlowContext.Dtos;
using Core42.Application.Features.ProcessFlowContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ProcessFlowContext.Queries
{
    /// <summary>
    /// Get processf flow filtered query
    /// </summary>
    public class GetProcessFlowFilteredQuery : ISortedQuery, IPagedQuery, IHasFilter<ProcessFlowFilter>, IRequest<ManagerResult<PagedDto<ProcessFlowDto>>>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(ProcessFlow.CreationDateTime)}.desc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Process flow filter
        /// </summary>
        public ProcessFlowFilter? Filter { get; set; }
    }
}
