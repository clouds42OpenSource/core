﻿using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.Enums.StateMachine;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ProcessFlowContext.Specifications
{
    /// <summary>
    /// Process flow specification
    /// </summary>
    public static class ProcessFlowSpecification
    {
        /// <summary>
        /// Filter process flow by id
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<ProcessFlow> ById(string? searchLine)
        {
            return Guid.TryParse(searchLine, out var id) ? new(x => x.Id == id) : new(x => false);
        }

        /// <summary>
        /// Filter process flow by search line
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<ProcessFlow> BySearchLine(string? searchLine)
        {
           return string.IsNullOrEmpty(searchLine) ? new(x => true) : new(x => x.ProcessedObjectName.ToLower().Contains(searchLine.ToLower()));
        }

        /// <summary>
        /// Filter process flow by status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static Spec<ProcessFlow> ByStatus(StateMachineComponentStatus? status)
        {
            return !status.HasValue ? new (x => true) : new(x => x.Status == status.Value);
        }
    }
}
