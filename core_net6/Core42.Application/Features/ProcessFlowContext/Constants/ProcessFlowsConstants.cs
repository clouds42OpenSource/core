﻿namespace Core42.Application.Features.ProcessFlowContext.Constants
{
    /// <summary>
    /// Process  flow const
    /// </summary>
    public static class ProcessFlowsConstants
    {
        /// <summary>
        /// Action delimiter in process state
        /// </summary>
        public const string StateActionDelimiter = "->";
    }
}
