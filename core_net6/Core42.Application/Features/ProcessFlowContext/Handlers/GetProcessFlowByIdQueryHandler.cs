﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Dtos;
using Core42.Application.Features.ProcessFlowContext.Extensions;
using Core42.Application.Features.ProcessFlowContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ProcessFlowContext.Handlers
{
    /// <summary>
    /// Get process flow by id query handler
    /// </summary>
    public class GetProcessFlowByIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetProcessFlowByIdQuery, ManagerResult<ProcessFlowDto>>
    {
        /// <summary>
        /// Handle get process flow by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<ProcessFlowDto>> Handle(GetProcessFlowByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ProcessFlow_ViewList);

                var record = await unitOfWork
                    .ProcessFlowRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken) ?? throw new ArgumentNullException($"Не найден рабочий процесс по идентификатору {request.Id}");
                
                var actionFlows = await unitOfWork.ActionFlowRepository
                    .AsQueryable()
                    .Where(x => x.ProcessFlowId == record.Id)
                    .ToListAsync(cancellationToken);

                var mappedProcessFlow = mapper.Map<ProcessFlowDto>(record);

                mappedProcessFlow.ActionFlows = actionFlows.GetActionFlowsData();

                return mappedProcessFlow.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения деталей рабочего процесса]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<ProcessFlowDto>(ex.Message);
            }
        }
    }
}
