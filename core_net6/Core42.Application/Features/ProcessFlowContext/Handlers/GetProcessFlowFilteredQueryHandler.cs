﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Dtos;
using Core42.Application.Features.ProcessFlowContext.Extensions;
using Core42.Application.Features.ProcessFlowContext.Queries;
using Core42.Application.Features.ProcessFlowContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ProcessFlowContext.Handlers
{

    /// <summary>
    /// Get process flow filtered query handler
    /// </summary>
    public class GetProcessFlowFilteredQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        ILogger42 logger)
        : IRequestHandler<GetProcessFlowFilteredQuery, ManagerResult<PagedDto<ProcessFlowDto>>>
    {
        /// <summary>
        /// Handle get process flow filtered query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<ProcessFlowDto>>> Handle(GetProcessFlowFilteredQuery request, CancellationToken cancellationToken)
        {

            try
            {
                accessProvider.HasAccess(ObjectAction.ProcessFlow_ViewList);

                var pagedList = await unitOfWork.ProcessFlowRepository
                    .AsQueryable()
                    .Where((ProcessFlowSpecification.ById(request?.Filter?.SearchLine) ||
                           ProcessFlowSpecification.BySearchLine(request?.Filter?.SearchLine)) &&
                           ProcessFlowSpecification.ByStatus(request?.Filter?.Status))
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken);

                var processFlowIds = pagedList.Select(p => p.Id).ToList();

                var actionFlows = await unitOfWork.ActionFlowRepository
                    .AsQueryable()
                    .Where(x => processFlowIds.Contains(x.ProcessFlowId))
                    .ToListAsync(cancellationToken);

                var result = pagedList.Select(x => mapper.Map<ProcessFlowDto>(x)).ToList()
                    .GroupJoin(actionFlows, z => z.Id, x => x.ProcessFlowId,
                        (processFlow, af) => new { processFlow, af })
                    .ToList();

                result.ForEach(x =>
                {
                    x.processFlow.ActionFlows = x.af.ToList().GetActionFlowsData();
                });
                         

                return result.Select(x => x.processFlow).ToList().ToPagedDto(pagedList.MetaData).ToOkManagerResult();
            }

            catch (Exception e)
            {
                logger.Warn(e, "Получение списка рабочих процессов");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<ProcessFlowDto>>(e.Message);
            }
        }
    }
}
