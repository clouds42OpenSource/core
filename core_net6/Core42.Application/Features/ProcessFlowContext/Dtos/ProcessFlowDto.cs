﻿using AutoMapper;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.Enums.StateMachine;
using Core42.Application.Features.ProcessFlowContext.Extensions;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ProcessFlowContext.Dtos
{
    /// <summary>
    /// Process flow dto
    /// </summary>
    public class ProcessFlowDto : IMapFrom<ProcessFlow>
    {
        /// <summary>
        /// Action flow list
        /// </summary>
        public List<ActionFlowDto> ActionFlows { get; set; } = [];

        /// <summary>
        /// Process id
        /// </summary>        
        public Guid Id { get; set; }

        /// <summary>
        /// Create date time
        /// </summary>
        public DateTime CreationDateTime { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Processed object name
        /// </summary>
        public string ProcessedObjectName { get; set; }

        /// <summary>
        /// Status
        /// </summary>        
        public StateMachineComponentStatus Status { get; set; }

        /// <summary>
        /// State description
        /// </summary>
        public string StateDescription { get; set; }

        /// <summary>
        /// Comment
        /// </summary>        
        public string Comment { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<ProcessFlow, ProcessFlowDto>()
                .ForMember(x => x.StateDescription, z => z.MapFrom(y => y.GetStateDescription()))
                .ForMember(x => x.ActionFlows, z => z.Ignore());
        }
    }
}
