﻿using Clouds42.Domain.Enums.StateMachine;

namespace Core42.Application.Features.ProcessFlowContext.Dtos
{
    /// <summary>
    /// Action flow dto
    /// </summary>
    public class ActionFlowDto
    {
        /// <summary>
        /// Error mesage
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public StateMachineComponentStatus Status { get; set; }

        /// <summary>
        /// Attemps count
        /// </summary>
        public int CountOfAttemps { get; set; }

        /// <summary>
        /// Action name
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Action description
        /// </summary>
        public string ActionDescription { get; set; }
    }
}
