﻿namespace Core42.Application.Features.ProcessFlowContext.Attributes
{
    /// <summary>
    /// Process flow description attribute
    /// </summary>
    public class FlowActionDescriptionAttribute(string actionDescription) : Attribute
    {
        /// <summary>
        /// Action description
        /// </summary>
        public string ActionDescription { get; } = actionDescription;
    }
}
