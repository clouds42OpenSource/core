﻿using MappingNetStandart.Mappings;
using AutoMapper;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Clouds42.Domain.DataModels.AccountProperties;

namespace Core42.Application.Features.BillingAccountContext.Dtos
{
    /// <summary>
    /// Billing account balance DTO
    /// </summary>
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class BillingAccountBalanceDto : IMapFrom<AccountConfiguration>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [XmlElement(nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Current balance of the billing account
        /// </summary>
        [XmlElement(nameof(Balance))]
        [DataMember(Name = nameof(Balance))]
        public decimal Balance { get; set; }

        /// <summary>
        /// Billing account locale name
        /// </summary>
        [XmlElement(nameof(LocaleName))]
        [DataMember(Name = nameof(LocaleName))]
        public string LocaleName { get; set; }

        /// <summary>
        /// Billing account locale currency
        /// </summary>
        [XmlElement(nameof(LocaleCurrency))]
        [DataMember(Name = nameof(LocaleCurrency))]
        public string LocaleCurrency { get; set; }

        /// <summary>
        /// Billing account locale currency
        /// </summary>
        [XmlElement(nameof(BonusBalance))]
        [DataMember(Name = nameof(BonusBalance))]
        public decimal BonusBalance { get; set; }

        /// <summary>
        /// Billing account locale currency
        /// </summary>
        [XmlElement(nameof(MonthlyPayment))]
        [DataMember(Name = nameof(MonthlyPayment))]
        public decimal MonthlyPayment { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountConfiguration, BillingAccountBalanceDto>()
                .ForMember(x => x.AccountId, y => y.MapFrom(z => z.Account.Id))
                .ForMember(x => x.Balance, y => y.MapFrom(z => z.Account.BillingAccount.Balance))
                .ForMember(x => x.BonusBalance, y => y.MapFrom(z => z.Account.BillingAccount.BonusBalance))
                .ForMember(x => x.LocaleName, y => y.MapFrom(z => z.Locale.Name))
                .ForMember(x => x.LocaleCurrency, y => y.MapFrom(z => z.Locale.Currency));
        }
    }
}
