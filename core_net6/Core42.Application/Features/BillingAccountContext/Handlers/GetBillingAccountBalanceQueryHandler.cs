﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BillingAccountContext.Dtos;
using Core42.Application.Features.BillingAccountContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BillingAccountContext.Handlers
{
    /// <summary>
    /// Handler for a query to get balance of the billing account
    /// </summary>
    public class GetBillingAccountBalanceQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IMapper mapper)
        : IRequestHandler<GetBillingAccountBalanceQuery, ManagerResult<BillingAccountBalanceDto>>
    {
        /// <summary>
        /// Handle a query to get balance of the billing account
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<BillingAccountBalanceDto>> Handle(GetBillingAccountBalanceQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Accounts_View, () => accessProvider.ContextAccountId);

                var balanceInfo = await unitOfWork.AccountConfigurationRepository
                                      .AsQueryable()
                                      .AsNoTracking()
                                      .ProjectTo<BillingAccountBalanceDto>(mapper.ConfigurationProvider)
                                      .FirstOrDefaultAsync(conf => conf.AccountId == request.AccountId, cancellationToken)
                    ?? throw new NotFoundException("Аккаунта с указанным идентификатором не существует");

                balanceInfo.MonthlyPayment = await unitOfWork.ResourceConfigurationRepository
                    .AsQueryable()
                    .AsNoTracking()
                    .Include(x => x.BillingService)
                    .Where(x => x.BillingService.SystemService != Clouds42Service.Esdl &&
                                x.BillingService.SystemService != Clouds42Service.Recognition && !x.IsDemoPeriod &&
                                x.AccountId == request.AccountId)
                    .SumAsync(x => x.Cost, cancellationToken);


                return balanceInfo.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Во время получения информации о балансе для аккаунта '{request.AccountId}' произошла ошибка]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<BillingAccountBalanceDto>(ex.Message);
            }
        }
    }
}
