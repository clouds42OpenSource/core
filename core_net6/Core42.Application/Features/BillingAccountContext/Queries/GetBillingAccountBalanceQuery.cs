﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BillingAccountContext.Dtos;
using MediatR;

namespace Core42.Application.Features.BillingAccountContext.Queries
{
    /// <summary>
    /// Query to get balance of the billing account
    /// </summary>
    public class GetBillingAccountBalanceQuery : IRequest<ManagerResult<BillingAccountBalanceDto>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }
    }
}
