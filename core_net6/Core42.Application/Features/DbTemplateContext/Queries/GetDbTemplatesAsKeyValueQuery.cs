﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.DbTemplateContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.DbTemplateContext.Queries
{
    /// <summary>
    /// Get db templates as key-value query
    /// </summary>
    public class GetDbTemplatesAsKeyValueQuery : IRequest<ManagerResult<List<DbTemplateDictionaryDto>>>, ISortedQuery
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(DbTemplate.DefaultCaption)}.asc";
    }
}
