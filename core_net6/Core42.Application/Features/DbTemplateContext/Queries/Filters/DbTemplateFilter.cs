﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.DbTemplateContext.Queries.Filters
{
    /// <summary>
    /// Db template filter
    /// </summary>
    public class DbTemplateFilter : IQueryFilter
    {
        /// <summary>
        /// Template name
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Template description
        /// </summary>
        public string? DefaultCaption { get; set; }
    }
}
