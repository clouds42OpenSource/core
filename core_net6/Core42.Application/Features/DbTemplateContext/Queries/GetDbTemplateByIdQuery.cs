﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.DbTemplateContext.Dtos;
using MediatR;

namespace Core42.Application.Features.DbTemplateContext.Queries
{
    /// <summary>
    /// Get db template by id query
    /// </summary>
    public class GetDbTemplateByIdQuery(Guid id) : IRequest<ManagerResult<DbTemplateDto>>
    {
        /// <summary>
        /// Db template id
        /// </summary>
        public Guid? Id { get; set; } = id;
    }
}
