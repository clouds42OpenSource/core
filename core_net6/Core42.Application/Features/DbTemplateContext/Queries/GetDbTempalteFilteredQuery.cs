﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.DbTemplateContext.Dtos;
using Core42.Application.Features.DbTemplateContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DbTemplateContext.Queries
{
    /// <summary>
    /// Get db template filtered query
    /// </summary>
    public class GetDbTempalteFilteredQuery : ISortedQuery, IPagedQuery, IHasFilter<DbTemplateFilter>, IRequest<ManagerResult<PagedDto<DbTemplateDto>>>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(DbTemplate.Name)}.asc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Db template filter
        /// </summary>
        public DbTemplateFilter? Filter { get; set; }
    }
}
