﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.DbTemplateContext.Dtos;
using Core42.Application.Features.DbTemplateContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DbTemplateContext.Handlers
{
    /// <summary>
    /// Get db template filtered query handler
    /// </summary>
    public class GetDbTemplateFilteredQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetDbTempalteFilteredQuery, ManagerResult<PagedDto<DbTemplateDto>>>
    {
        /// <summary>
        /// Handle get db template filtered query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<DbTemplateDto>>> Handle(GetDbTempalteFilteredQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.DbTemplates_View, () => accessProvider.ContextAccountId);

                return (await unitOfWork.DbTemplateRepository
                    .AsQueryable()
                    .AutoFilter(request.Filter)
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<DbTemplateDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception exception)
            {
                handlerException.Handle(exception, "[Ошибка получения пагинированного списка шаблонов.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<DbTemplateDto>>(exception.Message);
            }
        }
    }
}
