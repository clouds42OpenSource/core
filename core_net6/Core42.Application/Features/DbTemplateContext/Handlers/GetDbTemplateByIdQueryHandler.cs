﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.DbTemplateContext.Dtos;
using Core42.Application.Features.DbTemplateContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DbTemplateContext.Handlers
{
    /// <summary>
    /// Get db template by id query handler
    /// </summary>
    public class GetDbTemplateByIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetDbTemplateByIdQuery, ManagerResult<DbTemplateDto>>
    {
        /// <summary>
        /// Handle get db template by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ManagerResult<DbTemplateDto>> Handle(GetDbTemplateByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.DbTemplates_View, () => accessProvider.ContextAccountId);

                if (!request.Id.HasValue)
                {
                    var defaultItem = new DbTemplateDto
                    {
                        Id = Guid.Empty,
                        Name = "",
                        LocaleId = null,
                        Platform = PlatformType.Undefined.ToString(),
                        Order = 0,
                        DefaultCaption = "",
                        DemoTemplateId = null,
                        CanWebPublish = false,
                        NeedUpdate = false,
                        AdminLogin = "",
                        AdminPassword = ""
                    };

                    return defaultItem.ToOkManagerResult();
                }

                var record = await unitOfWork.DbTemplateRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken) ?? throw new NotFoundException($"Шаблон с ID:{request.Id} не найден.");

                var mappedResult = mapper.Map<DbTemplateDto>(record);

                var configuration1CName = await unitOfWork
                    .GetGenericRepository<ConfigurationDbTemplateRelation>()
                    .FirstOrDefaultAsync(confRel => confRel.DbTemplateId == mappedResult.Id);
                mappedResult.Configuration1CName = configuration1CName?.Configuration1CName ?? string.Empty;

                return mappedResult.ToOkManagerResult();
            }

            catch (Exception exception)
            {
                handlerException.Handle(exception, "[Ошибка получения данных о шаблоне для редактирования.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<DbTemplateDto>(exception.Message);
            }

        }
    }
}
