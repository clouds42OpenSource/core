﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.DbTemplateContext.Dtos;
using Core42.Application.Features.DbTemplateContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DbTemplateContext.Handlers
{
    /// <summary>
    /// Get db template as key-value query handler
    /// </summary>
    public class GetDbTemplatesAsKeyValueQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetDbTemplatesAsKeyValueQuery, ManagerResult<List<DbTemplateDictionaryDto>>>
    {
        /// <summary>
        /// Handle get db template as key-value query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<DbTemplateDictionaryDto>>> Handle(GetDbTemplatesAsKeyValueQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.DbTemplates_View, () => accessProvider.ContextAccountId);

                return (await unitOfWork.DbTemplateRepository
                    .AsQueryable()
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<DbTemplateDictionaryDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();

            }

            catch (Exception exception)
            {
                handlerException.Handle(exception, "[Ошибка получения списка шаблонов.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<DbTemplateDictionaryDto>>(exception.Message);
            }
        }
    }
}
