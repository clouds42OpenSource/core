﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.DbTemplateContext.Dtos
{
    /// <summary>
    /// Db template as key-value dto
    /// </summary>
    public class DbTemplateDictionaryDto : DictionaryDto<Guid, string>, IMapFrom<DbTemplate>
    {
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DbTemplate, DbTemplateDictionaryDto>()
                .ForMember(x => x.Key, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.Value, z => z.MapFrom(y => y.DefaultCaption));
        }
    }
}
