﻿using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.DbTemplateContext.Dtos
{
    /// <summary>
    /// Db template dto
    /// </summary>
    public class DbTemplateDto : IMapFrom<DbTemplate>
    {
        /// <summary>
        /// Template identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Template name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Template description
        /// </summary>
        public string DefaultCaption { get; set; }

        /// <summary>
        /// Platform version enum
        /// </summary>
        [NotMapped]
        public PlatformType PlatformType { get{ return Platform.ToPlatformTypeEnum(); } }

        /// <summary>
        /// Platform version 
        /// </summary>
        public string Platform { get; set; }

        /// <summary>
        /// Order
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// 1C configuration name
        /// </summary>
        public string Configuration1CName { get; set; }

        /// <summary>
        /// Locale identifier
        /// </summary>
        public Guid? LocaleId { get; set; }

        /// <summary>
        /// Demo template identifier
        /// </summary>
        public Guid? DemoTemplateId { get; set; }

        /// <summary>
        /// Flag to can web publish
        /// </summary>
        public bool CanWebPublish { get; set; }

        /// <summary>
        /// Administrator login
        /// </summary>
        public string AdminLogin { get; set; }

        /// <summary>
        /// Administrator password
        /// </summary>
        public string AdminPassword { get; set; }

        /// <summary>
        /// Flag to need update
        /// </summary>
        public bool NeedUpdate { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DbTemplate, DbTemplateDto>();
        }
    }
}
