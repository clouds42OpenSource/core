﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.PrintedHtmlFormContext.Dtos
{
    /// <summary>
    /// Printed html form dto
    /// </summary>
    public class PrintedHtmlFormDto : IMapFrom<PrintedHtmlForm>, IMapTo<AgencyAgreement>
    {
        /// <summary>
        /// Printed html form identifier
        /// </summary>
        public Guid Id { get; set; }

        [JsonIgnore]
        public Guid AgencyAgreementId { get; set; }

        /// <summary>
        /// Printed html form name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Html text data
        /// </summary>
        public string HtmlData { get; set; }

        /// <summary>
        /// Model type
        /// </summary>
        public string ModelType { get; set; }

        /// <summary>
        /// Model types
        /// </summary>
        public List<KeyValuePair<string, string>> ModelTypes { get; set; } = [];

        /// <summary>
        /// Attached files
        /// </summary>
        public List<CloudFileDto> Files { get; set; } = [];

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<PrintedHtmlForm, PrintedHtmlFormDto>()
                .ForMember(x => x.Files, z => z.MapFrom(y => y.Files));

            profile.CreateMap<AgencyAgreement, PrintedHtmlFormDto>()
                .ForMember(x => x.Files, z => z.MapFrom(y => y.PrintedHtmlForm.Files))
                .ForMember(x => x.AgencyAgreementId, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.Id, z => z.MapFrom(y => y.PrintedHtmlForm.Id))
                .ForMember(x => x.Name, z => z.MapFrom(y => y.PrintedHtmlForm.Name))
                .ForMember(x => x.ModelType, z => z.MapFrom(y => y.PrintedHtmlForm.ModelType))
                .ForMember(x => x.HtmlData, z => z.MapFrom(y => y.PrintedHtmlForm.HtmlData));
        }
    }
}
