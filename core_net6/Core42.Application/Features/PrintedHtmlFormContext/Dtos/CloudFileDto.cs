﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.PrintedHtmlFormContext.Dtos
{
    /// <summary>
    /// Cloud file dto
    /// </summary>
    public class CloudFileDto : IMapFrom<PrintedHtmlFormFile>, IMapFrom<CloudFile>
    {
        public Guid Id { get; set; }

        /// <summary>
        /// File type
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// File content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Hash data
        /// </summary>
        public string Base64 { get; set; }

        /// <summary>
        /// File name
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// File bytes array
        /// </summary>
        public byte[] Bytes { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<PrintedHtmlFormFile, CloudFileDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.CloudFileId))
                .ForMember(x => x.ContentType, z => z.MapFrom(y => y.CloudFile.ContentType))
                .ForMember(x => x.FileName, z => z.MapFrom(y => y.CloudFile.FileName))
                .ForMember(x => x.Bytes, z => z.MapFrom(y => y.CloudFile.Content));

            profile.CreateMap<CloudFile, CloudFileDto>()
                .ForMember(x => x.Bytes, z => z.MapFrom(y => y.Content))
                .ForMember(x => x.Content, z => z.Ignore());
        }
    }
}
