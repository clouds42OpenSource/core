﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.PrintedHtmlFormContext.Dtos;
using MediatR;

namespace Core42.Application.Features.PrintedHtmlFormContext.Queries
{
    /// <summary>
    /// Get printed html form by id query
    /// </summary>
    public class GetPrintedHtmlFormByIdQuery(Guid id) : IRequest<ManagerResult<PrintedHtmlFormDto>>
    {
        /// <summary>
        /// Printed html form id
        /// </summary>
        public Guid Id { get; set; } = id;
    }
}
