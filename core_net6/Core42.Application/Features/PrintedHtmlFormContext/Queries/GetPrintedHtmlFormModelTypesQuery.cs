﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.PrintedHtmlFormContext.Queries
{
    /// <summary>
    /// Get printed html form model types query
    /// </summary>
    public class GetPrintedHtmlFormModelTypesQuery : IRequest<ManagerResult<List<KeyValuePair<string, string>>>>
    {
    }
}
