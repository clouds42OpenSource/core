﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.PrintedHtmlFormContext.Dtos;
using Core42.Application.Features.PrintedHtmlFormContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PrintedHtmlFormContext.Queries
{
    /// <summary>
    /// Get filtered printed html form query 
    /// </summary>
    public class GetFilteredPrintedHtmlFormsQuery : IRequest<ManagerResult<PagedDto<PrintedHtmlFormDto>>>, IPagedQuery, ISortedQuery, IHasFilter<PrintedHtmlFormFilter>
    {
        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(PrintedHtmlForm.Name)}.asc";

        /// <summary>
        /// Printed html form filter
        /// </summary>
        public PrintedHtmlFormFilter? Filter { get; set; }
    }
}
