﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.PrintedHtmlFormContext.Queries.Filters
{
    /// <summary>
    /// Printed html form filter
    /// </summary>
    public class PrintedHtmlFormFilter : IQueryFilter
    {
        /// <summary>
        /// Name
        /// </summary>
        public string? Name { get; set; }
    }
}
