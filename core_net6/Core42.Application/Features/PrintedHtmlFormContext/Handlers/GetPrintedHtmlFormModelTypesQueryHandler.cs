﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.PrintedHtmlFormContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PrintedHtmlFormContext.Handlers
{
    /// <summary>
    /// Get printed html form model types query handler
    /// </summary>
    public class GetPrintedHtmlFormModelTypesQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetPrintedHtmlFormModelTypesQuery, ManagerResult<List<KeyValuePair<string, string>>>>
    {
        /// <summary>
        /// Handle get printed html form model types query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<KeyValuePair<string, string>>>> Handle(GetPrintedHtmlFormModelTypesQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var accountId = accessProvider.ContextAccountId;

                try
                {
                    accessProvider.HasAccess(ObjectAction.ChangePrintedHtmlForm, () => accountId);

                    var interfaceType = typeof(IPrintedHtmlFormModelDto);
                    var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                    var types = new List<Type>();

                    assemblies.ToList().ForEach(assembly =>
                    {
                        try
                        {
                            var assemblyTypes = assembly.GetTypes().Where(p =>
                                interfaceType.IsAssignableFrom(p) && !interfaceType.IsAssignableFrom(p.BaseType)).ToList();

                            types.AddRange(assemblyTypes);
                        }

                        catch
                        {
                            // ignored
                        }
                    });

                    if (!types.Any())
                        return new Dictionary<string, string>().ToList().ToOkManagerResult();

                    var modelTypes = new Dictionary<string, string>();
                    types.ForEach(w =>
                    {
                        if (w != null && w != interfaceType)
                            modelTypes.Add(w.FullName ?? "",
                                ((IPrintedHtmlFormModelDto)Activator.CreateInstance(w)).GetModelName());
                    });

                    return modelTypes.ToList().ToOkManagerResult();
                }


                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Ошибка получения модели печатной формы для создания] аккаунта {accountId}");

                    return PagedListExtensions.ToPreconditionFailedManagerResult<List<KeyValuePair<string, string>>>(ex.Message);
                }
            }, cancellationToken);

        }
    }
}
