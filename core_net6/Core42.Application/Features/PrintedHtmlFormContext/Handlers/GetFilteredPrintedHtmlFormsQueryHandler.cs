﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PrintedHtmlFormContext.Dtos;
using Core42.Application.Features.PrintedHtmlFormContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PrintedHtmlFormContext.Handlers
{
    /// <summary>
    /// Get filtered printed html forms query handler
    /// </summary>
    public class GetFilteredPrintedHtmlFormsQueryHandler(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredPrintedHtmlFormsQuery, ManagerResult<PagedDto<PrintedHtmlFormDto>>>
    {
        /// <summary>
        /// Handle get filtered printed html forms query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<PrintedHtmlFormDto>>> Handle(GetFilteredPrintedHtmlFormsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.GetPrintedHtmlForm, () => accessProvider.ContextAccountId);

                return (await unitOfWork.PrintedHtmlFormRepository
                     .AsQueryable()
                     .AutoFilter(request.Filter)
                     .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                     .ProjectTo<PrintedHtmlFormDto>(mapper.ConfigurationProvider)
                     .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                     .ToPagedDto()
                     .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения печатных форм HTML]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<PrintedHtmlFormDto>>(ex.Message);
            }
        }
    }
}
