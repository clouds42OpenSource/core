﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PrintedHtmlFormContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PrintedHtmlFormContext.Handlers
{
    /// <summary>
    /// Get printed html form by id query handler
    /// </summary>
    public class GetPrintedHtmlFormByIdQueryHandler(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ISender sender)
        : IRequestHandler<GetPrintedHtmlFormByIdQuery, ManagerResult<Dtos.PrintedHtmlFormDto>>
    {
        /// <summary>
        /// Handle get printed html form by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<Dtos.PrintedHtmlFormDto>> Handle(GetPrintedHtmlFormByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.GetPrintedHtmlForm, () => accessProvider.ContextAccountId);

                var record = await unitOfWork
                     .PrintedHtmlFormRepository
                     .AsQueryable()
                     .ProjectTo<Dtos.PrintedHtmlFormDto>(mapper.ConfigurationProvider)
                     .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                record.ModelTypes = (await sender.Send(new GetPrintedHtmlFormModelTypesQuery(), cancellationToken))?.Result ??
                                    [];
                record.Files.ForEach(x => { x.Content = Convert.ToBase64String(x.Bytes); });

                return record.ToOkManagerResult();

            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения печатной формы HTML] {request.Id}");
                return PagedListExtensions.ToPreconditionFailedManagerResult<Dtos.PrintedHtmlFormDto>(ex.Message);
            }
        }
    }
}
