﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CloudChangesContext.Dtos
{
    /// <summary>
    /// Cloud changes log record dto
    /// </summary>
    public sealed class CloudChangesLogRecordDto : IMapFrom<CloudChanges>
    {
        /// <summary>
        /// Account number
        /// </summary>
        public int AccountNumber { get; set; }

        /// <summary>
        /// Cloud change id
        /// </summary>
        public Guid CloudChangeId { get; set; }

        /// <summary>
        /// Cloud change create date
        /// </summary>
        public DateTime CreateCloudChangeDate { get; set; }

        /// <summary>
        /// Cloud change name
        /// </summary>
        public string CloudChangesActionName { get; set; }

        /// <summary>
        /// Clouds change description
        /// </summary>
        public string CloudChangesActionDescription { get; set; }

        /// <summary>
        /// Initiator login
        /// </summary>
        public string InitiatorLogin { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CloudChanges, CloudChangesLogRecordDto>()
                .ForMember(x => x.CloudChangesActionName, z => z.MapFrom(item => item.CloudChangesAction.Name))
                .ForMember(x => x.CloudChangesActionDescription, z => z.MapFrom(item => item.Description))
                .ForMember(x => x.CreateCloudChangeDate, z => z.MapFrom(item => item.Date))
                .ForMember(x => x.AccountNumber, z => z.MapFrom(item => item.Account.IndexNumber))
                .ForMember(x => x.CloudChangeId, z => z.MapFrom(item => item.Id))
                .ForMember(x => x.InitiatorLogin, z => z.MapFrom(item => item.AccountUser.Login));
        }
    }
}
