﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CloudChangesContext.Dtos;
using Core42.Application.Features.CloudChangesContext.Queries;
using Core42.Application.Features.CloudChangesContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CloudChangesContext.Handlers
{
    /// <summary>
    /// Get clouds changes query handler
    /// </summary>
    public class CloudChangesQueryHandler(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        :
            IRequestHandler<CloudChangesQuery, ManagerResult<PagedDto<CloudChangesLogRecordDto>>>
    {
        /// <summary>
        /// Handle get clouds changes query
        /// </summary>
        /// <param name="query">Данные фильтрации</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<CloudChangesLogRecordDto>>> Handle(CloudChangesQuery query, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccessForMultiAccounts(ObjectAction.ControlPanel_ManagmentMenu_Logging);

                return (await unitOfWork.CloudChangesRepository
                  .AsQueryable()
                  .Where(
                          CloudChangesSpecification.ByCreationDate(query?.Filter?.CreateCloudChangeDateFrom, query?.Filter?.CreateCloudChangeDateTo) &&
                          CloudChangesSpecification.ByCloudChangesAction(query?.Filter?.CloudChangesActionId != Guid.Empty ? query?.Filter?.CloudChangesActionId : null) &&
                          CloudChangesSpecification.ByOnlyMyAccounts(query?.Filter?.ShowMyAccountsOnly, query?.Filter?.CurrentAccountUserId) &&
                          CloudChangesSpecification.ByVipAccountOnly(query?.Filter?.ShowVipAccountsOnly) &&
                          CloudChangesSpecification.ByIsForeignAccount(query?.Filter?.IsForeignAccount, query?.Filter?.ContextAccountId) &&
                          CloudChangesSpecification.BySearchLine(query?.Filter?.SearchLine))
                  .AutoSort(query, StringComparison.OrdinalIgnoreCase)
                  .ProjectTo<CloudChangesLogRecordDto>(mapper.ConfigurationProvider)
                  .ToPagedListAsync(query?.PageNumber ?? 1, query?.PageSize ?? 100, cancellationToken))
                  .ToPagedDto()
                  .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка логов]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<CloudChangesLogRecordDto>>(ex.Message);
            }

        }
    }
}
