﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.CloudChangesContext.Queries.Filters
{
    /// <summary>
    /// Clouds changes filter 
    /// </summary>
    public sealed class CloudChangesFilter : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Is foreign account flag
        /// </summary>
        public bool? IsForeignAccount { get; set; }

        /// <summary>
        /// Context account id
        /// </summary>
        public Guid? ContextAccountId { get; set; }

        /// <summary>
        /// Context account user id
        /// </summary>
        public Guid? CurrentAccountUserId { get; set; }

        /// <summary>
        /// Cloud changes action id
        /// </summary>
        public Guid? CloudChangesActionId { get; set; }

        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }

        /// <summary>
        /// Clouds changes create date from
        /// </summary>
        public DateTime? CreateCloudChangeDateFrom { get; set; }

        /// <summary>
        /// Clouds changes create date to
        /// </summary>
        public DateTime? CreateCloudChangeDateTo { get; set; }

        /// <summary>
        /// Show my accounts only flag
        /// </summary>
        public bool? ShowMyAccountsOnly { get; set; }

        /// <summary>
        /// Show VIP accounts only
        /// </summary>
        public bool? ShowVipAccountsOnly { get; set; }
    }
}
