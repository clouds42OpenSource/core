﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.CloudChangesContext.Dtos;
using Core42.Application.Features.CloudChangesContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CloudChangesContext.Queries
{
    /// <summary>
    /// Get filtered, sorted, paginated clouds changes query
    /// </summary>
    public class CloudChangesQuery : ISortedQuery, IPagedQuery, IHasFilter<CloudChangesFilter>, IRequest<ManagerResult<PagedDto<CloudChangesLogRecordDto>>>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(CloudChanges.Date)}.desc";

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Clouds changes filter
        /// </summary>
        public CloudChangesFilter? Filter { get; set; }
    }
}
