﻿using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.CloudChangesContext.Specifications
{

    /// <summary>
    /// Cloud changes specification
    /// </summary>
    public static class CloudChangesSpecification
    {
        /// <summary>
        /// Filter cloud changes by creation date
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static Spec<CloudChanges> ByCreationDate(DateTime? from, DateTime? to)
        {
            return new(rec => (!from.HasValue || rec.Date >= from) && (!to.HasValue || rec.Date <= to));
        }

        /// <summary>
        /// Filter cloud changes by cloud changes action id
        /// </summary>
        /// <param name="cloudChangesActionId"></param>
        /// <returns></returns>
        public static Spec<CloudChanges> ByCloudChangesAction(Guid? cloudChangesActionId)
        {
            return cloudChangesActionId.HasValue ? new(rec => rec.Action == cloudChangesActionId) : new(rec => true);
        }

        /// <summary>
        /// Filter cloud changes by only my accounts
        /// </summary>
        /// <param name="onlyMyAccounts"></param>
        /// <param name="currentAccountId"></param>
        /// <returns></returns>
        public static Spec<CloudChanges> ByOnlyMyAccounts(bool? onlyMyAccounts, Guid? currentAccountId)
        {
            return onlyMyAccounts.HasValue && onlyMyAccounts.Value && currentAccountId.HasValue ? new(rec => rec.Account.AccountSaleManager.SaleManagerId == currentAccountId) : new(rec => true);
        }

        /// <summary>
        /// Filter cloud changes by only VIP accounts
        /// </summary>
        /// <param name="isVip"></param>
        /// <returns></returns>
        public static Spec<CloudChanges> ByVipAccountOnly(bool? isVip)
        {
            return isVip.HasValue && isVip.Value ? new(rec => rec.Account.AccountConfiguration.IsVip) : new(rec => true);
        }

        /// <summary>
        /// Filter cloud changes by is foreign account
        /// </summary>
        /// <param name="isForeignAccount"></param>
        /// <param name="contextAccountId"></param>
        /// <returns></returns>
        public static Spec<CloudChanges> ByIsForeignAccount(bool? isForeignAccount, Guid? contextAccountId)
        {
            return isForeignAccount.HasValue && isForeignAccount.Value && contextAccountId.HasValue ? new(rec => rec.AccountId == contextAccountId) : new(rec => true);
        }

        /// <summary>
        /// Filter cloud changes by search line
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static Spec<CloudChanges> BySearchLine(string? line)
        {
            return new(rec => string.IsNullOrEmpty(line)
                                                || rec.Account.IndexNumber.ToString().Contains(line)
                                                || rec.Account.AccountCaption.Contains(line)
                                                || rec.AccountUser.Login.Contains(line)
                                                || rec.CloudChangesAction.Name.Contains(line)
                                                || rec.Description.Contains(line));
        }
    }
}
