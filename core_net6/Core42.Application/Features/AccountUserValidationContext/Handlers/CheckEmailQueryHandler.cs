﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataValidations;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Account.Helpers;
using Core42.Application.Features.AccountUserValidationContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUserValidationContext.Handlers
{
    /// <summary>
    /// Check account user email query handler
    /// </summary>
    public class CheckEmailQueryHandler(IUnitOfWork unitOfWork, IAccessProvider accessProvider)
        : IRequestHandler<CheckEmailQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle check account user email query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<bool>> Handle(CheckEmailQuery request, CancellationToken cancellationToken)
        {
            try
            {
                AccountUser? userToEdit = null;

                if (request.UserId.HasValue)
                {
                    accessProvider.HasAccess(ObjectAction.AccountUsers_View, null, () => request.UserId);

                    userToEdit = await unitOfWork.AccountUsersRepository.AsQueryable().FirstOrDefaultAsync(x => x.Id == request.UserId, cancellationToken);
                }
                if (string.IsNullOrEmpty(request.Email))
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Значение почты пустое");

                var validator = new EmailValidation(request.Email);

                if (!validator.EmailMaskIsValid())
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Недопустимое значение");

                return validator.EmailIsAvailable(unitOfWork, userToEdit?.Login)
                    ? new ManagerResult<bool> { Result = true, State = ManagerResultState.Ok }
                    : PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Пользователь с указанной почтой уже существует");
            }

            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
            }

        }
    }
}
