﻿using Clouds42.Common.DataValidations;
using Clouds42.Common.ManagersResults;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Account.Helpers;
using Core42.Application.Features.AccountUserValidationContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUserValidationContext.Handlers
{
    /// <summary>
    /// Check account user phone query handler
    /// </summary>
    public class CheckPhoneQueryHandler(IUnitOfWork unitOfWork) : IRequestHandler<CheckPhoneQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle check account user phone query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<bool>> Handle(CheckPhoneQuery request, CancellationToken cancellationToken)
        {
            var validator = new PhoneNumberValidation(request.Phone);

            if (!validator.PhoneNumberMaskIsValid(locale: null, out var errorMessage))
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(errorMessage);

            return await validator.PhoneNumberIsAvailable(unitOfWork) ? true.ToOkManagerResult() : PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Указанный номер телефона уже зарегистрирован");
        }
    }
}
