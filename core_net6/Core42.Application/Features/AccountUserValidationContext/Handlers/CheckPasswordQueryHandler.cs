﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUserValidationContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUserValidationContext.Handlers
{
    /// <summary>
    /// Check account user password query handler
    /// </summary>
    public class CheckPasswordQueryHandler(
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        DesEncryptionProvider desEncryptionProvider,
        AesEncryptionProvider aesEncryptionProvider)
        : IRequestHandler<CheckPasswordQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle check account user password query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<bool>> Handle(CheckPasswordQuery request, CancellationToken cancellationToken)
        {
            try
            {
                AccountUser? accountUser = null;

                if (request.UserId.HasValue)
                {
                    accessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => request.UserId);

                    accountUser = await unitOfWork.AccountUsersRepository.AsQueryable().FirstOrDefaultAsync(au => au.Id == request.UserId, cancellationToken);
                }
              

                if (accountUser == null || string.IsNullOrEmpty(accountUser.Password) || string.IsNullOrEmpty(request.PasswordHash))
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>($"Не найден пользователь по идентификатору {request.UserId} или не " +
                        $"передан хэш пароля или у пользователя отсуствовал пароль");

                return (aesEncryptionProvider.Encrypt(request.PasswordHash) == accountUser.PasswordHash ||
                    desEncryptionProvider.Encrypt(request.PasswordHash) == accountUser.Password)
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Проверка текущего пароля для пользователя {request.UserId} завершилась ошибкой]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
            }
        }
    }
}
