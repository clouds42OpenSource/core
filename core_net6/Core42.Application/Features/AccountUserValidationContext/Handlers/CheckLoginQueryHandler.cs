﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.Constants;
using Clouds42.Common.DataValidations;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Account.Helpers;
using Core42.Application.Features.AccountUserValidationContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUserValidationContext.Handlers
{
    /// <summary>
    /// Check account user login query handler
    /// </summary>
    public class CheckLoginQueryHandler(
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        CreateNewUserAvailableLoginValidator createNewUserAvailableLoginValidator)
        : IRequestHandler<CheckLoginQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle check account user login query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<bool>> Handle(CheckLoginQuery request, CancellationToken cancellationToken)
        {
            try
            {

                AccountUser? accountUser = null;
                if (request.Id.HasValue)
                {
                    accountUser = await unitOfWork.AccountUsersRepository
                        .AsQueryableNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
                }

                if (accountUser != null && accountUser.Login == request.Login)
                    return true.ToOkManagerResult();

                var loginValidation = new LoginValidation(request.Login);

                if (!loginValidation.LoginSymbolsIsValid(out var validationMessage))
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(validationMessage);

                if (!loginValidation.LoginLengthIsValid())
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(
                        $"Допустимое количество символов: {AccountUserConstants.MinLoginLength}–{AccountUserConstants.MaxLoginLength}");

                return createNewUserAvailableLoginValidator.LoginIsAvailable(request.Login) ? true.ToOkManagerResult() : PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Пользователь с таким логином уже существует");
            }
            catch (Exception e)
            {
                logger.Warn(e.Message);
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(e.Message);
            }
        }
    }
}
