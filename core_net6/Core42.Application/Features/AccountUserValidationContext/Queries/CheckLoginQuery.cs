﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountUserValidationContext.Queries
{
    /// <summary>
    /// Check account user login query
    /// </summary>
    public class CheckLoginQuery(string login, Guid? id) : IRequest<ManagerResult<bool>>
    {
        /// <summary>
        /// Login
        /// </summary>
        public string Login { get; set; } = login;

        /// <summary>
        /// Id
        /// </summary>
        public Guid? Id { get; set; } = id;
    }
}
