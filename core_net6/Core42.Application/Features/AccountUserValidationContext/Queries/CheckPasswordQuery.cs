﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountUserValidationContext.Queries
{
    /// <summary>
    /// Check account user password query
    /// </summary>
    public class CheckPasswordQuery(Guid? userId, string passwordHash) : IRequest<ManagerResult<bool>>
    {
        /// <summary>
        /// User id
        /// </summary>
        public Guid? UserId { get; set; } = userId;

        /// <summary>
        /// Password hash
        /// </summary>
        public string PasswordHash { get; set; } = passwordHash;
    }
}
