﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountUserValidationContext.Queries
{
    /// <summary>
    /// Check account user email query
    /// </summary>
    public class CheckEmailQuery(Guid? userId, string email) : IRequest<ManagerResult<bool>>
    {
        /// <summary>
        /// User id
        /// </summary>
        public Guid? UserId { get; set; } = userId;

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; } = email;
    }
}
