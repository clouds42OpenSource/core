﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountUserValidationContext.Queries
{
    /// <summary>
    /// Check account user phone query
    /// </summary>
    public class CheckPhoneQuery(Guid? userId, string phone) : IRequest<ManagerResult<bool>>
    {
        /// <summary>
        /// User id
        /// </summary>
        public Guid? UserId { get; set; } = userId;

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; } = phone;
    }
}
