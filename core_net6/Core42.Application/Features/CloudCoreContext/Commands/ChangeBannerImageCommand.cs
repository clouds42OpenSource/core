﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CloudCoreContext.Commands
{
    public class ChangeBannerImageCommand(IFormFile file) : IRequest<ManagerResult<string>>
    {
        public IFormFile File { get; set; } = file;
    }

    public class ChangeBannerImageCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger, IConfiguration configuration)
        : IRequestHandler<ChangeBannerImageCommand, ManagerResult<string>>
    {
        public async Task<ManagerResult<string>> Handle(ChangeBannerImageCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var settingsPath = configuration.GetSection("FileSettings:PhysicalPath").Value;

                var physicalPath = Path.Combine(Directory.GetCurrentDirectory(), settingsPath ?? "StaticFiles");
                var machineName = Environment.MachineName;
                var root = Path.GetPathRoot(physicalPath);
                var physicalPathNotRoot = physicalPath.Remove(0, root.Length + @"\inetpub".Length);
                physicalPath = Path.Combine(@$"\\{machineName}", physicalPathNotRoot);

                logger.Info($"Путь куда сохраняется баннер {physicalPath}");

                var relativePath = CopyFile(physicalPath, request, cancellationToken, settingsPath).Result;

                StringBuilder mbuilder = new StringBuilder(machineName);

                logger.Info($"Имя машины до изменеий {machineName}");

                if (machineName[mbuilder.Length - 1] == 1)
                    machineName = machineName.Remove(machineName.Length - 1, 1) + "2";
                else
                    machineName = machineName.Remove(machineName.Length - 1, 1) + "1";

                logger.Info($"Имя машины после изменеий {machineName}");

                physicalPath = Path.Combine(@$"\\{machineName}", physicalPathNotRoot);
                logger.Info($"Пробуем на вторую ноду залить файл {physicalPath}");

                relativePath = await CopyFile(physicalPath, request, cancellationToken, settingsPath);

                return relativePath.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка при обновлении картинки баннера");

                return PagedListExtensions.ToPreconditionFailedManagerResult<string>(
                    "Ошибка обновления картинки баннера");
            }
        }

        public async Task<string> CopyFile (string physicalPath, ChangeBannerImageCommand request, 
            CancellationToken cancellationToken, string? settingsPath)
        {
            var cloudCore = await unitOfWork.CloudCoreRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(cancellationToken) ?? throw new NotFoundException("Не найдены настройки облака");

            var bannerUrl = cloudCore.BannerUrl;

            var fileExtension = Path.GetExtension(request.File.FileName);

            var bannerName = $"Market42_Banner22-{Guid.NewGuid()}{fileExtension}";

            var relativeDirectory = Path.Combine("img", "Market");

            var fullDirectory = Path.Combine(physicalPath, relativeDirectory);

            if (!Directory.Exists(fullDirectory))
            {
                Directory.CreateDirectory(fullDirectory);
            }

            var fullFilePath = Path.Combine(fullDirectory, bannerName);

            var relativePath = Path.Combine(relativeDirectory, bannerName);

            await using var fs = new FileStream(fullFilePath, FileMode.Create);

            await request.File.CopyToAsync(fs, cancellationToken);

            cloudCore.BannerUrl = relativePath;

            unitOfWork.CloudCoreRepository.Update(cloudCore);

            await unitOfWork.SaveAsync();

            var fullOldBannerPath = Path.Combine(Directory.GetCurrentDirectory(), settingsPath ?? "StaticFiles",
                bannerUrl);

            if (!string.IsNullOrEmpty(bannerUrl) && File.Exists(fullOldBannerPath))
                File.Delete(fullOldBannerPath);

            return relativePath;
        }
    }

    public class ChangeBannerImageCommandValidator : AbstractValidator<ChangeBannerImageCommand>
    {
        public ChangeBannerImageCommandValidator()
        {
            RuleFor(x => x.File)
                .NotNull()
                .WithMessage("Картинка обязательна к передаче");

            RuleFor(x => x.File.FileName)
                .Must(x => x.EndsWith(".jpg") || x.EndsWith(".png") || x.EndsWith(".jpeg"))
                .WithMessage("Проверьте формат отправляемой картинки. Допустимые форматы .jpg, .jpeg, .png");
        }
    }
}
