﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.PublishNodeContext.Dtos;

public class PublishNodeDto : IMapFrom<PublishNodeReference>
{
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Address
    /// </summary>
    public string Address { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<PublishNodeReference, PublishNodeDto>()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID));
    }
}
