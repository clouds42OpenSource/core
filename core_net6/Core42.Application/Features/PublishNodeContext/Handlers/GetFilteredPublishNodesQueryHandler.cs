﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PublishNodeContext.Dtos;
using Core42.Application.Features.PublishNodeContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PublishNodeContext.Handlers;

public class GetFilteredPublishNodesQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredPublishNodesQuery,
        ManagerResult<PagedDto<PublishNodeDto>>>
{
    public async Task<ManagerResult<PagedDto<PublishNodeDto>>> Handle(GetFilteredPublishNodesQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.ViewPublishNodeReference, () => accessProvider.ContextAccountId);

            return (await unitOfWork.PublishNodeReferenceRepository
                    .AsQueryable()
                    .ProjectTo<PublishNodeDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка нод публикаций.]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<PublishNodeDto>>(ex.Message);
        }
    }
}
