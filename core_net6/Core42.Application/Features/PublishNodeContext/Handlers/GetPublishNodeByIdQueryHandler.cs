﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PublishNodeContext.Dtos;
using Core42.Application.Features.PublishNodeContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PublishNodeContext.Handlers;

public class GetPublishNodeByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetPublishNodeByIdQuery, ManagerResult<PublishNodeDto>>
{
    public async Task<ManagerResult<PublishNodeDto>> Handle(GetPublishNodeByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.ViewPublishNodeReference,
                () => accessProvider.ContextAccountId);

            return (await unitOfWork.PublishNodeReferenceRepository
                    .AsQueryable()
                    .ProjectTo<PublishNodeDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                .ToOkManagerResult();
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex,
                $"Ошибка получения ноды публикаций {request.Id} пользователем {accessProvider.Name}.");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PublishNodeDto>(ex.Message);
        }
    }
}
