﻿using System.Collections.Concurrent;
using System.Net.NetworkInformation;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.PublishNodeContext.Dtos;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PublishNodeContext.Queries
{
    public class GetAvailablePublishNodesQuery : IRequest<ManagerResult<BaseAvailableDto<PublishNodeDto, PublishNodeDto>>>
    {
    }

    public class GetAvailablePublishNodesQueryHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
        : IRequestHandler<GetAvailablePublishNodesQuery,
            ManagerResult<BaseAvailableDto<PublishNodeDto, PublishNodeDto>>>
    {
        public async Task<ManagerResult<BaseAvailableDto<PublishNodeDto, PublishNodeDto>>> Handle(GetAvailablePublishNodesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var availableNodes = await unitOfWork.PublishNodeReferenceRepository
                    .AsQueryable()
                    .Where(x => !x.CloudServicesContentServerNodes.Any())
                    .ProjectTo<PublishNodeDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                var notWorkedNodes = new ConcurrentBag<Guid>();

                await Parallel.ForEachAsync(availableNodes, cancellationToken, (x, _) =>
                {
                    try
                    {
                        using var ping = new Ping();
                        var reply = ping.Send(x.Address);
                        logger.Info($"Проверка статуса ноды ping статус - {reply.Status}");

                        if (reply.Status != IPStatus.Success)
                        {
                            notWorkedNodes.Add(x.Id);
                        }
                    }

                    catch (NullReferenceException ex)
                    {
                        logger.Warn(ex, $"Нода публикации {x.Address} не найдена. Пропускаем");
                        notWorkedNodes.Add(x.Id);
                    }

                    catch (Exception ex)
                    {
                        logger.Warn(ex, $"Нода публикации {x.Address} была недоступна. Пропускаем");
                        notWorkedNodes.Add(x.Id);
                    }

                    return ValueTask.CompletedTask;
                });

                return new BaseAvailableDto<PublishNodeDto, PublishNodeDto>(availableNodes.Where(x => !notWorkedNodes.Contains(x.Id)).ToList(), null).ToOkManagerResult();
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка при получение списка доступных нод публикации");

                return PagedListExtensions.ToPreconditionFailedManagerResult<BaseAvailableDto<PublishNodeDto, PublishNodeDto>>(ex.Message);
            }
        }
    }
}
