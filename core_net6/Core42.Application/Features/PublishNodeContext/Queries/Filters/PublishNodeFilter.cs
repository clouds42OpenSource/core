﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.PublishNodeContext.Queries.Filters;

public class PublishNodeFilter : IQueryFilter
{
    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Address
    /// </summary>
    public string Address { get; set; }
}
