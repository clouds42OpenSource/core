﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.PublishNodeContext.Dtos;
using Core42.Application.Features.PublishNodeContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PublishNodeContext.Queries
{
    public class GetFilteredPublishNodesQuery: IRequest<ManagerResult<PagedDto<PublishNodeDto>>>, ISortedQuery, IPagedQuery, IHasFilter<PublishNodeFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(PublishNodeDto.Description)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public PublishNodeFilter Filter { get; set; }
    }
}
