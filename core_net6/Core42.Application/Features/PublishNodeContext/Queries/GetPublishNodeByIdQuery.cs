﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.PublishNodeContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.PublishNodeContext.Queries;

public class GetPublishNodeByIdQuery : IRequest<ManagerResult<PublishNodeDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
