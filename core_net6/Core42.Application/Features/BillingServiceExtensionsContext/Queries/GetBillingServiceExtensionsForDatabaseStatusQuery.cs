﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Microsoft.AspNetCore.Mvc;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using MediatR;

namespace Core42.Application.Features.BillingServiceExtensionsContext.Queries
{
    /// <summary>
    /// Query to get status of extensions of a billing service for an account database
    /// </summary>
    public class GetBillingServiceExtensionsForDatabaseStatusQuery : IRequest<ManagerResult<ServiceExtensionDatabaseStatusDto>>
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        [Required]
        [FromRoute]
        public Guid Id { get; set; }

        /// <summary>
        /// Account database identifier
        /// </summary>
        [Required]
        public Guid AccountDatabaseId { get; set; }
    }
}
