﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BillingServiceExtensionsContext.Queries
{
    /// <summary>
    /// Query to get all infobases' templates compatible with the billing service
    /// </summary>
    public class GetCompatibleTemplatesForExtensionsQuery : IRequest<ManagerResult<PagedDto<AvailableDatabaseForServiceExtensionDto>>>,
        IPagedQuery
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        [Required]
        [FromRoute]
        public Guid Id { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <inheritdoc/>
        public int? PageSize { get; set; }

        /// <inheritdoc/>
        public int? PageNumber { get; set; }
    }
}
