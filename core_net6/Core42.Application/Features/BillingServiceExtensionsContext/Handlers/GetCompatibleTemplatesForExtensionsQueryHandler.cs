﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BillingServiceExtensionsContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BillingServiceExtensionsContext.Handlers
{
    /// <summary>
    /// Handler for a query to get all infobases' templates compatible with the billing service
    /// </summary>
    public class GetCompatibleTemplatesForExtensionsQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IGetDatabasesForServicesExtensionCommand getDatabasesForServicesExtensionCommand)
        : IRequestHandler<GetCompatibleTemplatesForExtensionsQuery,
            ManagerResult<PagedDto<AvailableDatabaseForServiceExtensionDto>>>
    {
        /// <summary>
        /// Handle a query to get all infobases' templates compatible with the billing service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<AvailableDatabaseForServiceExtensionDto>>> Handle(GetCompatibleTemplatesForExtensionsQuery request, CancellationToken cancellationToken)
        {
            var logMessage = $"Получение списка информационных баз, совместимых с сервисом {request.Id}, " +
                $"для аккаунта {request.AccountId}";
            try
            {
                accessProvider.HasAccess(ObjectAction.GetServiceExtensionDatabase, () => request.AccountId);
                logger.Info(logMessage);

                var allowedDatabasesForExtensions = await getDatabasesForServicesExtensionCommand.ExecuteAsync(request.AccountId, request.Id);

                if (!allowedDatabasesForExtensions.Any())
                    return new List<AvailableDatabaseForServiceExtensionDto>()
                        .ToPagedList(request?.PageNumber ?? 1, request?.PageSize ?? 50)
                        .ToPagedDto()
                        .ToOkManagerResult();

                var databasesIds = allowedDatabasesForExtensions.Select(db => db.AccountDatabaseID);

                var resultData = (await unitOfWork.DatabasesRepository
                        .AsQueryableNoTracking()
                        .Where(db =>
                            (db.State == DatabaseState.Ready.ToString() ||
                             db.State == DatabaseState.NewItem.ToString()) &&
                            db.AccountId == request.AccountId && db.AccountDatabaseOnDelimiter != null &&
                            databasesIds.Contains(db.Id))
                        .Select(x => new AvailableDatabaseForServiceExtensionDto
                        {
                            Caption = x.Caption,
                            Id = x.Id,
                            DatabaseStateString = x.State,
                            TemplateName = x.DbTemplate.Name,
                            DatabaseImageCssClass = x.DbTemplate.ImageUrl,
                            ExtensionLastActivityDate = DateTime.Now
                        })
                        .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto();


                resultData.Records
                    .GroupJoin(allowedDatabasesForExtensions, z => z.Id, x => x.AccountDatabaseID, (database, allowForExtensions) => (database, allowForExtensions))
                    .ToList()
                    .ForEach(x =>
                    {
                        x.database.Error = x.allowForExtensions.FirstOrDefault()?.Error;
                        x.database.ExtensionState = x.allowForExtensions.FirstOrDefault()?.AccountDatabaseServiceState;
                        x.database.IsInstalled = x.allowForExtensions.FirstOrDefault()?.AccountDatabaseServiceState is
                                ServiceExtensionDatabaseStatusEnum.DoneInstall
                                or ServiceExtensionDatabaseStatusEnum.ProcessingInstall
                                or ServiceExtensionDatabaseStatusEnum.ProcessingDelete;
                    });

                return resultData.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{logMessage} завершилось с ошибкой]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AvailableDatabaseForServiceExtensionDto>>(ex.Message);
            }
        }
    }
}
