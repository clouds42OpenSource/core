﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BillingServiceExtensionsContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BillingServiceExtensionsContext.Handlers
{
    /// <summary>
    /// Handler for a query to get status of extensions of a billing service for an account database
    /// </summary>
    public class GetBillingServiceExtensionsForDatabaseStatusQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IGetServiceExtensionDatabaseStatusCommand getServiceExtensionDatabaseStatusCommand)
        : IRequestHandler<GetBillingServiceExtensionsForDatabaseStatusQuery,
            ManagerResult<ServiceExtensionDatabaseStatusDto>>
    {
        /// <summary>
        /// Handle a query to get status of extensions of a billing service for an account database
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<ServiceExtensionDatabaseStatusDto>> Handle(GetBillingServiceExtensionsForDatabaseStatusQuery request, CancellationToken cancellationToken)
        {
            string logMessage = $"Получение статуса расширения сервиса '{request.Id}' для инф. базы {request.AccountDatabaseId}";

            try
            {
                var accountDatabase = await unitOfWork.DatabasesRepository.AsQueryable().AsNoTracking()
                    .FirstOrDefaultAsync(db => db.Id == request.AccountDatabaseId, cancellationToken)
                    ?? throw new NotFoundException($"Информационная база с идентификатором '{request.AccountDatabaseId}' не существует");

                accessProvider.HasAccess(ObjectAction.GetServiceExtensionDatabase, () => accountDatabase.AccountId);

                logger.Info(logMessage);

                return getServiceExtensionDatabaseStatusCommand.Execute(request.Id, request.AccountDatabaseId)
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{logMessage} завершилось с ошибкой]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<ServiceExtensionDatabaseStatusDto>(ex.Message);
            }
        }
    }
}
