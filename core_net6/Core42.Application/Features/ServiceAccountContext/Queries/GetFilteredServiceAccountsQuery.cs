﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.ServiceAccountContext.Dtos;
using Core42.Application.Features.ServiceAccountContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServiceAccountContext.Queries
{
    /// <summary>
    /// Get filtered service account query
    /// </summary>
    public class GetFilteredServiceAccountsQuery : IRequest<ManagerResult<PagedDto<ServiceAccountDto>>>, IHasFilter<ServiceAccountFilter>, ISortedQuery, IPagedQuery
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(ServiceAccount.Account.IndexNumber)}.asc";

        /// <summary>
        /// Service account filter
        /// </summary>
        public ServiceAccountFilter? Filter { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }
    }
}
