﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ServiceAccountContext.Queries.Filters
{
    /// <summary>
    /// Service account query filter
    /// </summary>
    public class ServiceAccountFilter : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }
    }
}
