﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ServiceAccountContext.Dtos
{
    /// <summary>
    /// Service account dto
    /// </summary>
    public class ServiceAccountDto : IMapFrom<ServiceAccount>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Account index number
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Account caption
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Account user initiator login
        /// </summary>
        public string AccountUserInitiatorName { get; set; }

        /// <summary>
        /// Creation date time
        /// </summary>
        public DateTime CreationDateTime { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<ServiceAccount, ServiceAccountDto>()
                .ForMember(x => x.AccountIndexNumber, m => m.MapFrom(z => z.Account.IndexNumber))
                .ForMember(x => x.CreationDateTime, m => m.MapFrom(z => z.CreationDateTime))
                .ForMember(x => x.AccountCaption, m => m.MapFrom(z => z.Account.AccountCaption))
                .ForMember(x => x.AccountUserInitiatorName, m => m.MapFrom(z => z.AccountUser.Login))
                .ForMember(x => x.AccountId, m => m.MapFrom(z => z.Id));
        }
    }
}
