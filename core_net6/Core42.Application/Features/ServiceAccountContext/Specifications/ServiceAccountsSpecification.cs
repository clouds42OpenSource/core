﻿using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ServiceAccountContext.Specifications
{
    /// <summary>
    /// Service account specification
    /// </summary>
    public static class ServiceAccountsSpecification
    {
        /// <summary>
        /// Filter service accounts by search line
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<ServiceAccount> BySearchLine(string? searchLine)
        {
            return new(x => string.IsNullOrEmpty(searchLine) || x.Account.IndexNumber.ToString().Contains(searchLine) ||
                                     x.Account.AccountCaption.Contains(searchLine));
        }
    }
}
