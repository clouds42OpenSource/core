﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ServiceAccountContext.Dtos;
using Core42.Application.Features.ServiceAccountContext.Queries;
using Core42.Application.Features.ServiceAccountContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ServiceAccountContext.Handlers
{
    /// <summary>
    /// Get filtered service account query handler
    /// </summary>
    public class GetFilteredServiceAccountsQueryHandler(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredServiceAccountsQuery, ManagerResult<PagedDto<ServiceAccountDto>>>
    {
        /// <summary>
        /// Handle get filtered service account query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<ServiceAccountDto>>> Handle(GetFilteredServiceAccountsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.GetServiceAccounts, () => accessProvider.ContextAccountId);

                return (await unitOfWork
                    .ServiceAccountRepository
                    .AsQueryableNoTracking()
                    .Where(ServiceAccountsSpecification.BySearchLine(request?.Filter?.SearchLine))
                    .ProjectTo<ServiceAccountDto>(mapper.ConfigurationProvider)
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка служебных аккаунтов]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<ServiceAccountDto>>(ex.Message);
            }

        }
    }
}
