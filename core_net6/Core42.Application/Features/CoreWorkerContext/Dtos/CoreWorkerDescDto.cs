﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CoreWorkerContext.Dtos
{
    /// <summary>
    /// Core worker description dto
    /// </summary>
    public class CoreWorkerDescDto : IMapFrom<CoreWorker>
    {
        /// <summary>
        /// Id
        /// </summary>
        public short Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CoreWorker, CoreWorkerDescDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.CoreWorkerId))
                .ForMember(x => x.Name, z => z.MapFrom(y => y.CoreWorkerAddress));
        }
    }
}
