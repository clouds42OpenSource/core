﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.CoreWorkerContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.CoreWorkerContext.Queries
{
    /// <summary>
    /// Get all core workers query
    /// </summary>
    public class GetAllWorkersQuery : IRequest<ManagerResult<List<CoreWorkerDescDto>>>, ISortedQuery
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(CoreWorker.CoreWorkerId)}.asc";
    }
}
