﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerContext.Dtos;
using Core42.Application.Features.CoreWorkerContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerContext.Handlers
{
    /// <summary>
    /// Get all core workers query handler
    /// </summary>
    public class GetAllWorkersQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetAllWorkersQuery, ManagerResult<List<CoreWorkerDescDto>>>
    {
        /// <summary>
        /// Handle get all core workers query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<CoreWorkerDescDto>>> Handle(GetAllWorkersQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);

                return (await unitOfWork.CoreWorkerRepository
                    .AsQueryable()
                    .AutoSort(request)
                    .ProjectTo<CoreWorkerDescDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();

            }
            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения списка воркеров в системе. Причина: {ex.Message}";
                handlerException.Handle(ex, "[Ошибка получения списка воркеров в системе].");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<CoreWorkerDescDto>>(errorMessage);
            }
        }
    }
}
