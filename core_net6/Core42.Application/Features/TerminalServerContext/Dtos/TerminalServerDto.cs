﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.TerminalServerContext.Dtos;

public class TerminalServerDto : IMapFrom<CloudTerminalServer>
{
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudTerminalServer, TerminalServerDto>()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID));
    }
}
