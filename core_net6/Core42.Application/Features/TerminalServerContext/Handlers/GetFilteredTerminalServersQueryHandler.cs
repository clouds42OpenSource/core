﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.TerminalServerContext.Dtos;
using Core42.Application.Features.TerminalServerContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.TerminalServerContext.Handlers;

public class GetFilteredTerminalServersQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredTerminalServersQuery, ManagerResult<PagedDto<TerminalServerDto>>>
{
    public async Task<ManagerResult<PagedDto<TerminalServerDto>>> Handle(GetFilteredTerminalServersQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudTerminalServerRepository
                    .AsQueryable()
                    .ProjectTo<TerminalServerDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка терминальных серверов]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<TerminalServerDto>>(ex.Message);
        }
    }
}
