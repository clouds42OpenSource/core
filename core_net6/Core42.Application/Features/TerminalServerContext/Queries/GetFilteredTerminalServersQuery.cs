﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.TerminalServerContext.Dtos;
using Core42.Application.Features.TerminalServerContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.TerminalServerContext.Queries
{
    public class GetFilteredTerminalServersQuery : IRequest<ManagerResult<PagedDto<TerminalServerDto>>>, ISortedQuery, IPagedQuery, IHasFilter<TerminalServerFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(TerminalServerDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public TerminalServerFilter Filter { get; set; }
    }
}
