﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.TerminalServerContext.Queries.Filters;

public class TerminalServerFilter : IQueryFilter
{
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }
}
