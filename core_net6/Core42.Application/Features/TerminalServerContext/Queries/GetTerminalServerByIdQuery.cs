﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.TerminalServerContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.TerminalServerContext.Queries;

public class GetTerminalServerByIdQuery : IRequest<ManagerResult<TerminalServerDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
