﻿namespace Core42.Application.Features.TalkMeContext.Dtos
{
    public class WhatsAppNotificationDto
    {
        public string Message { get; set; }
        public string Phone { get; set; }
    }
}
