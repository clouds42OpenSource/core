﻿using Newtonsoft.Json;

namespace Core42.Application.Features.TalkMeContext.Dtos
{
    public class TalkMeWhatsAppNotificationRequest
    {
        [JsonProperty("messages")]
        public List<TalkMeWhatsAppMessage> Messages { get; set; }
    }

    public class TalkMeWhatsAppMessage
    {
        [JsonProperty("recipient")]
        public string Recipient { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
