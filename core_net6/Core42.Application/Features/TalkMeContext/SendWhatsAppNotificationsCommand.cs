﻿using System.Net.Mime;
using System.Text;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.TalkMeContext.Dtos;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Core42.Application.Features.TalkMeContext
{
    public class SendWhatsAppNotificationsCommand: IRequest
    {
        public List<WhatsAppNotificationDto> Notifications { get; set; }
    }

    public class SendWhatsAppNotificationsCommandHandler(
        IHttpClientFactory httpClientFactory,
        ILogger42 logger,
        IUnitOfWork unitOfWork)
        : IRequestHandler<SendWhatsAppNotificationsCommand>
    {
        public async Task Handle(SendWhatsAppNotificationsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                if (await unitOfWork.NotificationSettingsRepository
                    .AsQueryableNoTracking()
                    .AnyAsync(x => x.Type == NotificationType.WhatsApp && !x.IsActive, cancellationToken))
                {
                    logger.Warn("Отправка рассылок через WhatsApp отлючена на глобальном уровне. Проверьте настройки в таблице dbo.NotificationSettings");

                    return;
                }

                logger.Info("Производим рассылку по WhatsApp");

                using var client = httpClientFactory.CreateClient();

                var baseAddress = CloudConfigurationProvider.TalkMe.GetBaseApiUrl();

                client.BaseAddress = new Uri(baseAddress);

                var token = CloudConfigurationProvider.TalkMe.GetToken();

                client.DefaultRequestHeaders.Add("X-Token", token);

                var sourceName = CloudConfigurationProvider.TalkMe.GetSourceName();

                var whatsAppMessages = request.Notifications
                    .Select(x => new TalkMeWhatsAppMessage { Recipient = x.Phone, Text = x.Message, Source = sourceName })
                    .ToList();

                var requestModel = new TalkMeWhatsAppNotificationRequest { Messages = whatsAppMessages };

                logger.Info($"Отправляем сообщения в WhatsApp в количестве {request.Notifications.Count} штук");

                var response = await client.PostAsync("whatsapp/send/text",
                    new StringContent(JsonConvert.SerializeObject(requestModel), Encoding.UTF8, MediaTypeNames.Application.Json), cancellationToken);

                var data = await response.Content.ReadAsStringAsync(cancellationToken);

                logger.Info($"Получили ответ сервиса. Статус код {response.StatusCode}. Тело ответа {data}");

            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка рассылки WhatsApp");
            }
        }
    }
}
