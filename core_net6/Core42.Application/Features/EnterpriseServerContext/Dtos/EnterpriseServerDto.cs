﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.EnterpriseServerContext.Dtos;

public class EnterpriseServerDto : IMapFrom<CloudServicesEnterpriseServer>
{
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Platform type
    /// </summary>
    public PlatformType VersionEnum => (PlatformType)Enum.Parse(typeof(PlatformType), Version);

    /// <summary>
    /// Platform type string
    /// </summary>
    public string Version { get; set; }

    /// <summary>
    /// Admin login
    /// </summary>
    public string AdminName { get; set; }

    /// <summary>
    /// Admin password
    /// </summary>
    public string AdminPassword { get; set; }

    /// <summary>
    /// Путь к настройкам кластера
    /// </summary>
    public string ClusterSettingsPath { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesEnterpriseServer, EnterpriseServerDto>()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID));
    }
}
