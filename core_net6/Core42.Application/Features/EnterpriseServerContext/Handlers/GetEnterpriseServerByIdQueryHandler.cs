﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.EnterpriseServerContext.Dtos;
using Core42.Application.Features.EnterpriseServerContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.EnterpriseServerContext.Handlers;

public class GetEnterpriseServerByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetEnterpriseServerByIdQuery,
        ManagerResult<EnterpriseServerDto>>
{
    public async Task<ManagerResult<EnterpriseServerDto>> Handle(GetEnterpriseServerByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.ViewCloudServiceEnterpriseServer, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesEnterpriseServerRepository
                    .AsQueryable()
                    .ProjectTo<EnterpriseServerDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex,
                $"[Ошибка получения сервера 1С:Предприятие {request.Id} пользователем {accessProvider.Name}.]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<EnterpriseServerDto>(ex.Message);
        }
    }
}
