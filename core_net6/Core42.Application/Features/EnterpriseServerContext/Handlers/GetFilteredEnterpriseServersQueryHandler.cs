﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.EnterpriseServerContext.Dtos;
using Core42.Application.Features.EnterpriseServerContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.EnterpriseServerContext.Handlers;

public class GetFilteredEnterpriseServersQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredEnterpriseServersQuery, ManagerResult<PagedDto<EnterpriseServerDto>>>
{
    public async Task<ManagerResult<PagedDto<EnterpriseServerDto>>> Handle(GetFilteredEnterpriseServersQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.ViewCloudServiceEnterpriseServer, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesEnterpriseServerRepository
                    .AsQueryable()
                    .ProjectTo<EnterpriseServerDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();

        }
        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка серверов 1С:Предприятие.]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<EnterpriseServerDto>>(ex.Message);
        }
    }
}
