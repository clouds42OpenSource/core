﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.EnterpriseServerContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.EnterpriseServerContext.Queries;

public class GetEnterpriseServerByIdQuery : IRequest<ManagerResult<EnterpriseServerDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
