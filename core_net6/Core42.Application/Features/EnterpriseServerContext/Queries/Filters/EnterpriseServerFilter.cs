﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.EnterpriseServerContext.Queries.Filters;

public class EnterpriseServerFilter : IQueryFilter
{
    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Platform type string
    /// </summary>
    public string Version { get; set; }
}
