﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.EnterpriseServerContext.Dtos;
using Core42.Application.Features.EnterpriseServerContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.EnterpriseServerContext.Queries
{
    public class GetFilteredEnterpriseServersQuery : IRequest<ManagerResult<PagedDto<EnterpriseServerDto>>>, ISortedQuery, IPagedQuery, IHasFilter<EnterpriseServerFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(EnterpriseServerDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public EnterpriseServerFilter Filter { get; set; }
    }
}
