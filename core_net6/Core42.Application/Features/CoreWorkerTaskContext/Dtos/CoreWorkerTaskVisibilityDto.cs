﻿namespace Core42.Application.Features.CoreWorkerTaskContext.Dtos
{
    /// <summary>
    /// Core worker task visibilty dto
    /// </summary>
    public class CoreWorkerTaskVisibilityDto
    {
        /// <summary>
        /// Is add task button visible flag
        /// </summary>
        public bool IsAddTaskButtonVisible { get; set; }

        /// <summary>
        /// Is control task tab visible flag
        /// </summary>
        public bool IsControlTaskTabVisible { get; set; }

        /// <summary>
        /// Is worker available tasks bags tab visible flag
        /// </summary>
        public bool IsWorkerAvailableTasksBagsTabVisible { get; set; }

        /// <summary>
        /// Can cancel core worker tasks queue
        /// </summary>
        public bool CanCancelCoreWorkerTasksQueue { get; set; }
    }
}
