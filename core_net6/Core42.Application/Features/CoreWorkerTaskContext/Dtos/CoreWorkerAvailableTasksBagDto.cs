﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CoreWorkerTaskContext.Dtos
{
    /// <summary>
    /// Core worker available tasks bag dto
    /// </summary>
    public class CoreWorkerAvailableTasksBagDto : IMapFrom<CoreWorker>
    {
        /// <summary>
        /// Worker id
        /// </summary>
        public short WorkerId { get; set; }

        /// <summary>
        /// Worker name
        /// </summary>
        public string WorkerName { get; set; }

        /// <summary>
        /// Available tasks for add
        /// </summary>
        public List<CoreWorkerTaskDictionaryDto> AvailableTasksForAdd { get; set; }

        /// <summary>
        /// Worker tasks bags
        /// </summary>
        public List<CoreWorkerTaskDictionaryDto> WorkerTasksBags { get; set; }


        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CoreWorker, CoreWorkerAvailableTasksBagDto>()
                .ForMember(x => x.WorkerId, z => z.MapFrom(y => y.CoreWorkerId))
                .ForMember(x => x.WorkerName, z => z.MapFrom(y => y.CoreWorkerAddress));
        }
    }
}
