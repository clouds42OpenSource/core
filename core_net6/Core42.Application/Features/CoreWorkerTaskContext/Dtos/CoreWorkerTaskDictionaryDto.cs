﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CoreWorkerTaskContext.Dtos
{
    /// <summary>
    /// Core worker task as key value dto
    /// </summary>
    public class CoreWorkerTaskDictionaryDto : DictionaryDto<Guid, string>, IMapFrom<CoreWorkerTask>
    {

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CoreWorkerTask, CoreWorkerTaskDictionaryDto>()
                .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
                .ForMember(x => x.Value, z => z.MapFrom(y => y.TaskName));
        }
    }
}
