﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CoreWorkerTaskContext.Dtos
{
    /// <summary>
    /// Core worker task dto
    /// </summary>
    public class CoreWorkerTaskDto: IMapFrom<CoreWorkerTask>
    {
        /// <summary>
        /// Task name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Task id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Task execution lifetime in minutes
        /// </summary>
        public int TaskExecutionLifetimeInMinutes { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {

            profile.CreateMap<CoreWorkerTask, CoreWorkerTaskDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.ID))
                .ForMember(x => x.Name, z => z.MapFrom(y => y.TaskName))
                .ForMember(x => x.TaskExecutionLifetimeInMinutes, z => z.MapFrom(y => y.TaskExecutionLifetimeInMinutes));
        }
    }
}
