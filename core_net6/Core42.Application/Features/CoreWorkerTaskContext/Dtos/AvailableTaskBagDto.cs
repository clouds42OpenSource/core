﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CoreWorkerTaskContext.Dtos
{
    /// <summary>
    /// Available task bag dto
    /// </summary>
    public class AvailableTaskBagDto : IMapFrom<CoreWorkerAvailableTasksBag>
    {
        /// <summary>
        /// Worker id
        /// </summary>
        public short WorkerId { get; set; }

        /// <summary>
        /// Worker name
        /// </summary>
        public string WorkerName { get; set; }

        /// <summary>
        /// Task id
        /// </summary>
        public Guid TaskId { get; set; }

        /// <summary>
        /// Task name
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Task priority
        /// </summary>
        public int TaskPriority { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CoreWorkerAvailableTasksBag, AvailableTaskBagDto>()
                .ForMember(x => x.WorkerId, z => z.MapFrom(y => y.CoreWorkerId))
                .ForMember(x => x.WorkerName, z => z.MapFrom(y => y.CoreWorker.CoreWorkerAddress))
                .ForMember(x => x.TaskId, z => z.MapFrom(y => y.CoreWorkerTaskId))
                .ForMember(x => x.TaskName, z => z.MapFrom(y => y.CoreWorkerTask.TaskName))
                .ForMember(x => x.TaskPriority, z => z.MapFrom(y => y.Priority));
        }
    }
}
