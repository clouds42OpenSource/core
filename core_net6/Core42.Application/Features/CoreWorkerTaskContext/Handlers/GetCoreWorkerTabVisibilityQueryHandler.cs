﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Handlers
{

    /// <summary>
    /// Get core worker tab visibilty query handler
    /// </summary>
    public class GetCoreWorkerTabVisibilityQueryHandler(IAccessProvider accessProvider, ILogger42 logger)
        : IRequestHandler<GetCoreWorkerTabVisibilityQuery, ManagerResult<CoreWorkerTaskVisibilityDto>>
    {
        /// <summary>
        /// Handle get core worker tab visibilty query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<CoreWorkerTaskVisibilityDto>> Handle(GetCoreWorkerTabVisibilityQuery request, CancellationToken cancellationToken)
        {

            try
            {
                return await Task.Run(() =>
                {
                    accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);

                    var hasAccessToControlTab = accessProvider.HasAccessBool(ObjectAction.CoreWorkerTask_GetAll);

                    return new CoreWorkerTaskVisibilityDto
                    {
                        IsAddTaskButtonVisible = accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagementMenu_CreateTask),
                        IsControlTaskTabVisible = hasAccessToControlTab,
                        IsWorkerAvailableTasksBagsTabVisible = hasAccessToControlTab,
                        CanCancelCoreWorkerTasksQueue = accessProvider.HasAccessBool(ObjectAction.CoreWorkerTasksQueue_Cancel)
                    }.ToOkManagerResult();
                }, cancellationToken);
            }

            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения модели видимости вкладок страницы 'Задачи'. Причина: {ex.Message}";
                logger.Warn(ex, errorMessage);

                return PagedListExtensions.ToPreconditionFailedManagerResult<CoreWorkerTaskVisibilityDto>(ex.Message);
            }
        }
    }
}
