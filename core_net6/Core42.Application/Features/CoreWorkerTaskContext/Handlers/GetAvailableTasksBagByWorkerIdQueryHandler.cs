﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Handlers
{
    /// <summary>
    /// Get available tasks bag by worker id query handler
    /// </summary>
    public class GetAvailableTasksBagByWorkerIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider)
        : IRequestHandler<GetAvailableTasksBagByWorkerIdQuery, ManagerResult<CoreWorkerAvailableTasksBagDto>>
    {
        /// <summary>
        /// Handle get available tasks bag by worker id query
        /// </summary>
        public async Task<ManagerResult<CoreWorkerAvailableTasksBagDto>> Handle(GetAvailableTasksBagByWorkerIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.CoreWorkerTask_Edit);

                var coreWorker = await unitOfWork.CoreWorkerRepository
                    .AsQueryable()
                    .ProjectTo<CoreWorkerAvailableTasksBagDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.WorkerId == request.WorkerId, cancellationToken) ?? throw new NotFoundException($"Worker по номеру {request.WorkerId} не найден");


                var bags = 
                    await unitOfWork.CoreWorkerTaskRepository
                     .AsQueryable()
                     .Include(x => x.CoreWorkerAvailableTasksBags.Where(z => z.CoreWorkerId == coreWorker.WorkerId))
                     .OrderBy(x => x.TaskName)
                     .ToListAsync(cancellationToken);

                coreWorker.AvailableTasksForAdd = bags.Where(x => !x.CoreWorkerAvailableTasksBags.Any()).Select(mapper.Map<CoreWorkerTaskDictionaryDto>).ToList();
                coreWorker.WorkerTasksBags = bags.Where(x => x.CoreWorkerAvailableTasksBags.Any()).Select(mapper.Map<CoreWorkerTaskDictionaryDto>).ToList();

                return coreWorker.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<CoreWorkerAvailableTasksBagDto>(ex.Message);
            }
        }
    }
}
