﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries;
using Core42.Application.Features.CoreWorkerTaskContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Handlers
{

    /// <summary>
    /// Get filtered available tasks bags query handler
    /// </summary>
    public class GetFilteredAvailableTasksBagQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredAvailableTasksBagQuery, ManagerResult<PagedDto<AvailableTaskBagDto>>>
    {
        /// <summary>
        /// Handle get filtered available tasks bags query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<AvailableTaskBagDto>>> Handle(GetFilteredAvailableTasksBagQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud);

                return (await unitOfWork
                    .GetGenericRepository<CoreWorkerAvailableTasksBag>()
                    .AsQueryable()
                    .Where(AvailableTaskSpecification.ByWorkerId(request?.Filter?.WorkerId) &&
                           AvailableTaskSpecification.ByTaskId(request?.Filter?.TaskId) &&
                           AvailableTaskSpecification.ByTaskPriority(request?.Filter?.TaskPriority))
                    .AutoSort(request)
                    .ProjectTo<AvailableTaskBagDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                var errorMessage = $"Ошибка получения данных по доступным задачам воркеров. Причина: {ex.Message}";
                handlerException.Handle(ex, "[Ошибка получения данных по доступным задачам воркеров]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AvailableTaskBagDto>>(errorMessage);
            }
        }
    }
}
