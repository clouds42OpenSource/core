﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Handlers
{
    /// <summary>
    /// Get available tasks bag priority query handler
    /// </summary>
    public class GetAvailableTasksBagPriorityQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider)
        : IRequestHandler<GetAvailableTasksBagPriorityQuery, ManagerResult<AvailableTaskBagDto>>
    {
        /// <summary>
        /// Handle get available tasks bag priority query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ManagerResult<AvailableTaskBagDto>> Handle(GetAvailableTasksBagPriorityQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.CoreWorkerTask_Edit);
                var record = await unitOfWork.GetGenericRepository<CoreWorkerAvailableTasksBag>()
                    .AsQueryable()
                    .ProjectTo<AvailableTaskBagDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.WorkerId == request.WorkerId && x.TaskId == request.TaskId, cancellationToken)
                    ?? throw new NotFoundException($"Доступная задача по ID {request.TaskId} для воркера {request.WorkerId} не найдена");

                return record.ToOkManagerResult();

            }

            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<AvailableTaskBagDto>(ex.Message);
            }
        }
    }
}
