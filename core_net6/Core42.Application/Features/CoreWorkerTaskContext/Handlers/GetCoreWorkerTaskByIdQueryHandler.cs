﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Handlers
{
    /// <summary>
    /// Get core worker tasks by id query handler
    /// </summary>
    public class GetCoreWorkerTaskByIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider)
        : IRequestHandler<GetCoreWorkerTaskByIdQuery, ManagerResult<CoreWorkerTaskDto>>
    {
        /// <summary>
        /// Handle get core worker tasks by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ManagerResult<CoreWorkerTaskDto>> Handle(GetCoreWorkerTaskByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.CoreWorkerTask_GetAll);

                var record = await unitOfWork.CoreWorkerTaskRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => request.Id == x.ID, cancellationToken) ?? 
                    throw new NotFoundException($"Task with ID:{request.Id} not found.");

                return  mapper.Map<CoreWorkerTaskDto>(record).ToOkManagerResult();
            }

            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<CoreWorkerTaskDto>(ex.Message);
            }
        }
    }
}
