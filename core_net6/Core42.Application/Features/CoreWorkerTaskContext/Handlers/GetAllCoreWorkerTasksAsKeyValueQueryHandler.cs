﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Handlers
{
    /// <summary>
    /// Get all core worker tasks as key value query handler
    /// </summary>
    public class GetAllCoreWorkerTasksAsKeyValueQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider)
        : IRequestHandler<GetAllCoreWorkerTasksAsKeyValueQuery, ManagerResult<List<CoreWorkerTaskDictionaryDto>>>
    {
        /// <summary>
        /// Handle get all core worker tasks as key value query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<CoreWorkerTaskDictionaryDto>>> Handle(GetAllCoreWorkerTasksAsKeyValueQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.CoreWorkerTask_Search);

                return (await unitOfWork.CoreWorkerTaskRepository
                    .AsQueryable()
                    .ProjectTo<CoreWorkerTaskDictionaryDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<List<CoreWorkerTaskDictionaryDto>>(ex.Message);
            }
        }
    }
}
