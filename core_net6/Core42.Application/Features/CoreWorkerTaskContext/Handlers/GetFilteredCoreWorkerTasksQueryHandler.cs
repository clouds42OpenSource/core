﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Handlers
{
    /// <summary>
    /// Get filtered core worker tasks query handler
    /// </summary>
    public class GetFilteredCoreWorkerTasksQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider)
        : IRequestHandler<GetFilteredCoreWorkerTasksQuery, ManagerResult<PagedDto<CoreWorkerTaskDto>>>
    {
        /// <summary>
        /// Handle get filtered core worker tasks query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<CoreWorkerTaskDto>>> Handle(GetFilteredCoreWorkerTasksQuery request, CancellationToken cancellationToken)
        {

            try
            {
                accessProvider.HasAccess(ObjectAction.CoreWorkerTask_GetAll);

                var taskId = request.Filter?.TaskId;

                return (await unitOfWork.CoreWorkerTaskRepository
                    .AsQueryable()
                    .Where(x => !taskId.HasValue || x.ID == taskId.Value)
                    .AutoSort(request)
                    .ProjectTo<CoreWorkerTaskDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 10, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<CoreWorkerTaskDto>>(ex.Message);
            }
        }
    }
}
