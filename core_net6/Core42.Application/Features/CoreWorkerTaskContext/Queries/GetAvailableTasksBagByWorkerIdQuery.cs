﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using MediatR;

namespace Core42.Application.Features.CoreWorkerTaskContext.Queries
{
    /// <summary>
    /// Get available tasks bag by worker id query
    /// </summary>
    public class GetAvailableTasksBagByWorkerIdQuery(short workerId)
        : IRequest<ManagerResult<CoreWorkerAvailableTasksBagDto>>
    {
        /// <summary>
        /// Worker id
        /// </summary>
        public short WorkerId { get; set; } = workerId;
    }
}
