﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using MediatR;

namespace Core42.Application.Features.CoreWorkerTaskContext.Queries
{
    /// <summary>
    /// Get all core worker tasks as key-value query
    /// </summary>
    public class GetAllCoreWorkerTasksAsKeyValueQuery : IRequest<ManagerResult<List<CoreWorkerTaskDictionaryDto>>>
    {
    }
}
