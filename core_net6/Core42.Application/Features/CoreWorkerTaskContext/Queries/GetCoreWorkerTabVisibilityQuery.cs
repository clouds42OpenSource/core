﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using MediatR;

namespace Core42.Application.Features.CoreWorkerTaskContext.Queries
{
    /// <summary>
    /// Get core worker tab visibility query
    /// </summary>
    public class GetCoreWorkerTabVisibilityQuery : IRequest<ManagerResult<CoreWorkerTaskVisibilityDto>>
    {
    }
}
