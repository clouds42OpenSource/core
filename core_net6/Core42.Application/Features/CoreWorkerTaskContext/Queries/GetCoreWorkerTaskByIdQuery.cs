﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using MediatR;


namespace Core42.Application.Features.CoreWorkerTaskContext.Queries
{
    /// <summary>
    /// Get core worker task by id query
    /// </summary>
    public class GetCoreWorkerTaskByIdQuery(Guid id) : IRequest<ManagerResult<CoreWorkerTaskDto>>
    {
        /// <summary>
        /// Core worker task id
        /// </summary>
        public Guid Id { get; set; } = id;
    }
}
