﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Queries.Filters
{
    /// <summary>
    /// Core worker task filter dto
    /// </summary>
    public class CoreWorkerTaskFilter : IQueryFilter
    {
        /// <summary>
        /// Task id
        /// </summary>
        public Guid? TaskId { get; set; }
    }
}
