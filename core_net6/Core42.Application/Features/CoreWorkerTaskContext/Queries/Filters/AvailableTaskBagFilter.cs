﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Queries.Filters
{
    /// <summary>
    /// Available task bag filter
    /// </summary>
    public class AvailableTaskBagFilter : IQueryFilter
    {
        /// <summary>
        /// Worker id
        /// </summary>
        public short? WorkerId { get; set; }

        /// <summary>
        /// Task id
        /// </summary>
        public Guid? TaskId { get; set; }

        /// <summary>
        /// Task priority
        /// </summary>
        public int? TaskPriority { get; set; }
    }
}
