﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using MediatR;

namespace Core42.Application.Features.CoreWorkerTaskContext.Queries
{
    /// <summary>
    /// Get available tasks bag priority query
    /// </summary>
    public class GetAvailableTasksBagPriorityQuery(short workerId, Guid taskId)
        : IRequest<ManagerResult<AvailableTaskBagDto>>
    {
        /// <summary>
        /// Worker id
        /// </summary>
        public short WorkerId { get; set; } = workerId;

        /// <summary>
        /// Task id
        /// </summary>
        public Guid TaskId { get; set; } = taskId;
    }
}
