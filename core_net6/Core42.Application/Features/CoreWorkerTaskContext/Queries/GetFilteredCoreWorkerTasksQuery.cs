﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Queries
{
    /// <summary>
    /// Get filtered core worker tasks query
    /// </summary>
    public class GetFilteredCoreWorkerTasksQuery : IRequest<ManagerResult<PagedDto<CoreWorkerTaskDto>>>, IPagedQuery, ISortedQuery, IHasFilter<CoreWorkerTaskFilter>
    {
        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; } = 1;

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; } = 10;

        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(CoreWorkerTask.TaskName)}.asc";

        /// <summary>
        /// Core worker task filter
        /// </summary>
        public CoreWorkerTaskFilter? Filter { get; set; }
    }
}
