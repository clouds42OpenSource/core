﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Core42.Application.Features.CoreWorkerTaskContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Queries
{
    /// <summary>
    /// Get filtered available tasks bag query
    /// </summary>
    public class GetFilteredAvailableTasksBagQuery : IRequest<ManagerResult<PagedDto<AvailableTaskBagDto>>>, IPagedQuery, ISortedQuery, IHasFilter<AvailableTaskBagFilter>
    {
        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(CoreWorkerAvailableTasksBag.CoreWorkerId)}.desc";

        /// <summary>
        /// Available task bag filter
        /// </summary>
        public AvailableTaskBagFilter? Filter { get; set; }
    }
}
