﻿using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.CoreWorkerTaskContext.Specifications
{
    /// <summary>
    /// Available task specification
    /// </summary>
    public static class AvailableTaskSpecification
    {

        /// <summary>
        /// Filter tasks bag by worker id
        /// </summary>
        /// <param name="workerId"></param>
        /// <returns></returns>
        public static Spec<CoreWorkerAvailableTasksBag> ByWorkerId(short? workerId) 
        {
            return new(x => !workerId.HasValue || x.CoreWorkerId == workerId);
        }

        /// <summary>
        /// Filter tasks bag by task id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public static Spec<CoreWorkerAvailableTasksBag> ByTaskId(Guid? taskId)
        {
            return new(x => !taskId.HasValue || x.CoreWorkerTaskId == taskId);
        }

        /// <summary>
        /// Filter tasks bag by task priority
        /// </summary>
        /// <param name="priority"></param>
        /// <returns></returns>
        public static Spec<CoreWorkerAvailableTasksBag> ByTaskPriority(int? priority)
        {
            return new(x => !priority.HasValue || x.Priority == priority);
        }
    }
}
