﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PlatformVersionContext.Dtos;
using Core42.Application.Features.PlatformVersionContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PlatformVersionContext.Handlers;

public class GetPlatformVersionByVersionQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetPlatformVersionByVersionQuery,
        ManagerResult<PlatformVersionDto>>
{
    public async Task<ManagerResult<PlatformVersionDto>> Handle(GetPlatformVersionByVersionQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.ViewPlatformVersionReference,
                () => accessProvider.ContextAccountId);

            return (await unitOfWork.PlatformVersionReferencesRepository
                    .AsQueryable()
                    .ProjectTo<PlatformVersionDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Version == request.Version, cancellationToken))
                .ToOkManagerResult();
        }
        catch (Exception exception)
        {
            handlerException.Handle(exception,
                $"[Ошибка получения платформы {request.Version} пользователем {accessProvider.Name}.]");
            return PagedListExtensions.ToPreconditionFailedManagerResult<PlatformVersionDto>(exception.Message);
        }
    }
}
