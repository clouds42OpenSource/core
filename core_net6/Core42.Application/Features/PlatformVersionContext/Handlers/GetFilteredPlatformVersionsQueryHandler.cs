﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PlatformVersionContext.Dtos;
using Core42.Application.Features.PlatformVersionContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PlatformVersionContext.Handlers;

public class GetFilteredPlatformVersionsQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredPlatformVersionsQuery,
        ManagerResult<PagedDto<PlatformVersionDto>>>
{
    public async Task<ManagerResult<PagedDto<PlatformVersionDto>>> Handle(GetFilteredPlatformVersionsQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.ViewPlatformVersionReference, () => accessProvider.ContextAccountId);

            return (await unitOfWork.PlatformVersionReferencesRepository
                    .AsQueryable()
                    .ProjectTo<PlatformVersionDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();

        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка платформ.]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<PlatformVersionDto>>(ex.Message);
        }
    }
}
