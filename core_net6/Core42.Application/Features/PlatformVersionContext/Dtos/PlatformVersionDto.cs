﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.PlatformVersionContext.Dtos;

public class PlatformVersionDto : IMapFrom<PlatformVersionReference>
{
    /// <summary>
    /// Version
    /// </summary>
    public string Version { get; set; }

    /// <summary>
    /// Path
    /// </summary>
    public string PathToPlatform { get; set; }

    /// <summary>
    /// X64 path
    /// </summary>
    public string PathToPlatformX64 { get; set; }

    /// <summary>
    /// Thin client mac os download link
    /// </summary>
    public string MacOsThinClientDownloadLink { get; set; }

    /// <summary>
    /// Thin client windows download link
    /// </summary>
    public string WindowsThinClientDownloadLink { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<PlatformVersionReference, PlatformVersionDto>()
            .ForMember(x => x.PathToPlatformX64, z => z.MapFrom(y => y.PathToPlatfromX64));
    }
}
