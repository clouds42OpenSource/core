﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.PlatformVersionContext.Dtos;
using MediatR;

namespace Core42.Application.Features.PlatformVersionContext.Queries;

public class GetPlatformVersionByVersionQuery : IRequest<ManagerResult<PlatformVersionDto>>
{
    public string Version { get; set; }
}
