﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.PlatformVersionContext.Dtos;
using Core42.Application.Features.PlatformVersionContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.PlatformVersionContext.Queries
{
    public class GetFilteredPlatformVersionsQuery : IRequest<ManagerResult<PagedDto<PlatformVersionDto>>>, ISortedQuery, IPagedQuery, IHasFilter<PlatformVersionFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(PlatformVersionDto.Version)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public PlatformVersionFilter Filter { get; set; }
    }
}
