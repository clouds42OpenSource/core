﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.PlatformVersionContext.Queries.Filters;

public class PlatformVersionFilter : IQueryFilter
{
    /// <summary>
    /// Version
    /// </summary>
    public string Version { get; set; }

    /// <summary>
    /// Path
    /// </summary>
    public string PathToPlatform { get; set; }

    /// <summary>
    /// X64 path
    /// </summary>
    public string PathToPlatformX64 { get; set; }
}
