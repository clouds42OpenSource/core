﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ContentServerContext.Queries.Filters;

public class ContentServerFilter : IQueryFilter
{
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Publish site
    /// </summary>
    public string PublishSiteName { get; set; }
}
