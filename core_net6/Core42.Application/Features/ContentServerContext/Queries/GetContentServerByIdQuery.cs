﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ContentServerContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.ContentServerContext.Queries;

public class GetContentServerByIdQuery : IRequest<ManagerResult<ContentServerDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
