﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.ContentServerContext.Dtos;
using Core42.Application.Features.ContentServerContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ContentServerContext.Queries
{
    public class GetFilteredContentServersQuery: IRequest<ManagerResult<PagedDto<ContentServerDto>>>, ISortedQuery, IPagedQuery, IHasFilter<ContentServerFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(ContentServerDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public ContentServerFilter Filter { get; set; }
    }
}
