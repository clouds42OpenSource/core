﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.PublishNodeContext.Dtos;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ContentServerContext.Dtos;

public class ContentServerDto : IMapFrom<CloudServicesContentServer>
{
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Available and user nodes
    /// </summary>
    public BaseAvailableDto<PublishNodeDto, PublishNodeDto> NodesData { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Publish site
    /// </summary>
    public string PublishSiteName { get; set; }

    /// <summary>
    /// Group by account
    /// </summary>
    public bool GroupByAccount { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesContentServer, ContentServerDto>()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID));
    }
}
