﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.ContentServerContext.Dtos;
using Core42.Application.Features.ContentServerContext.Queries;
using Core42.Application.Features.PublishNodeContext.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ContentServerContext.Handlers;

public class GetContentServerByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetContentServerByIdQuery, ManagerResult<ContentServerDto>>
{
    public async Task<ManagerResult<ContentServerDto>> Handle(GetContentServerByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            var server = await unitOfWork.CloudServicesContentServerRepository
                .AsQueryable()
                .ProjectTo<ContentServerDto>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            var selectedNodes = await unitOfWork.CloudServicesContentServerNodeRepository
                .AsQueryable()
                .Where(x => x.ContentServerId == server.Id)
                .Select(x => x.PublishNodeReference)
                .ProjectTo<PublishNodeDto>(mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            var availableNodes = await unitOfWork.PublishNodeReferenceRepository
                .AsQueryable()
                .Where(x => !x.CloudServicesContentServerNodes.Any())
                .ProjectTo<PublishNodeDto>(mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            server.NodesData = new BaseAvailableDto<PublishNodeDto, PublishNodeDto>(availableNodes, selectedNodes);

            return server.ToOkManagerResult();

        }

        catch (Exception ex)
        {
            handlerException.Handle(ex,
                $"Ошибка получения сервера публикации {request.Id}");

            return PagedListExtensions.ToPreconditionFailedManagerResult<ContentServerDto>(ex.Message);
        }
    }
}
