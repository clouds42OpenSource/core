﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ContentServerContext.Dtos;
using Core42.Application.Features.ContentServerContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ContentServerContext.Handlers;

public class GetFilteredContentServersQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredContentServersQuery, ManagerResult<PagedDto<ContentServerDto>>>
{
    public async Task<ManagerResult<PagedDto<ContentServerDto>>> Handle(GetFilteredContentServersQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesContentServerRepository
                    .AsQueryable()
                    .ProjectTo<ContentServerDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();

        }
        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка серверов публикации]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<ContentServerDto>>(ex.Message);
        }
    }
}
