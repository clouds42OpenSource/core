﻿using AutoMapper;
using Clouds42.Domain.DataModels.Link;
using Clouds42.Domain.Enums.Link42;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Dtos
{
    /// <summary>
    /// Launch db and start rpd log record dto
    /// </summary>
    public class LaunchDbAndStartRdpLogRecordDto : IMapFrom<DatabaseLaunchRdpStartHistory>
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Account index number
        /// </summary>
        public int AccountNumber { get; set; }

        /// <summary>
        ///  Action create date
        /// </summary>
        public DateTime ActionCreated { get; set; }

        /// <summary>
        /// Link action type
        /// </summary>
        public LinkActionType Action { get; set; }

        /// <summary>
        /// Login
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Link app version
        /// </summary>
        public string LinkAppVersion { get; set; }

        /// <summary>
        /// Link app type
        /// </summary>
        public AppModes LinkAppType { get; set; }

        /// <summary>
        ///  V82 db name
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Db launch type
        /// </summary>
        public LaunchType LaunchType { get; set; }

        /// <summary>
        /// External ip address
        /// </summary>
        public string ExternalIpAddress { get; set; }

        /// <summary>
        /// Internal ip address
        /// </summary>
        public string InternalIpAddress { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DatabaseLaunchRdpStartHistory, LaunchDbAndStartRdpLogRecordDto>();
        }
    }
}
