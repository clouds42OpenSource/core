﻿using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.HandlerExeption.Contract;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Link;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Dtos;
using Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Queries;

namespace Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Handlers
{
    /// <summary>
    ///     Database start and link42 laucnh query handler
    /// </summary>
    public class LaunchDbAndStartRdpLogQueryHandler :
        IRequestHandler<LaunchDbAndStartRdpLogQuery, ManagerResult<PagedDto<LaunchDbAndStartRdpLogRecordDto>>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccessProvider _accessProvider;
        private readonly IHandlerException _handlerException;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="mapper"></param>
        public LaunchDbAndStartRdpLogQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IAccessProvider accessProvider, IHandlerException handlerException)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _accessProvider = accessProvider;
            _handlerException = handlerException;
        }

        /// <summary>
        ///    Database start and link42 launch handle method
        /// </summary>
        /// <param name="query">Query data</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<LaunchDbAndStartRdpLogRecordDto>>> Handle(LaunchDbAndStartRdpLogQuery query, CancellationToken cancellationToken)
        {
            try
            {
                _accessProvider.HasAccessForMultiAccounts(ObjectAction.ControlPanel_ManagmentMenu_Logging);

                return (await _unitOfWork
                    .GetGenericRepository<DatabaseLaunchRdpStartHistory>()
                    .AsQueryable()
                    .ProjectTo<LaunchDbAndStartRdpLogRecordDto>(_mapper.ConfigurationProvider)
                    .AutoFilter(query.Filter)
                    .AutoSort(query, StringComparison.OrdinalIgnoreCase)
                    .ToPagedListAsync(query.PageNumber ?? 1, query.PageSize ?? 100, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Database start and link42 launch query handler]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<LaunchDbAndStartRdpLogRecordDto>>(ex.Message);
            }
        }
    }
}
