﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels.Link;
using Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Dtos;
using Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Queries
{
    /// <summary>
    /// Logging entry filter model for launching databases and opening RDP
    /// </summary>
    public class LaunchDbAndStartRdpLogQuery :
        IPagedQuery,
        ISortedQuery,
        IHasFilter<LaunchDbAndStartRdpLogFilter>,
        IRequest<ManagerResult<PagedDto<LaunchDbAndStartRdpLogRecordDto>>>
    {
        /// <summary>
        /// Filter for receiving logging of launching databases and RDP
        /// </summary>
        public LaunchDbAndStartRdpLogFilter? Filter { get; set; }

        /// <summary>
        /// Records on page
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Number of page
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Sorting, etc. Name.ASC, Name.DESC
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(DatabaseLaunchRdpStartHistory.ActionCreated)}.desc";
    }
}
