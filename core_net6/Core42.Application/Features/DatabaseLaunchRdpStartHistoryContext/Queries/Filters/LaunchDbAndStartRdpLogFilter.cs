﻿using Clouds42.Domain.Enums.Link42;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Queries.Filters
{
    /// <summary>
    /// Launch db and start rdp log filter
    /// </summary>
    public sealed class LaunchDbAndStartRdpLogFilter : IQueryFilter
    {
        /// <summary>
        /// Account number
        /// </summary>
        public int? AccountNumber { get; set; }

        /// <summary>
        /// Login
        /// </summary>
        public string? Login { get; set; }

        /// <summary>
        /// V82 name
        /// </summary>
        public string? V82Name { get; set; }

        /// <summary>
        /// Link app version
        /// </summary>
        public string? LinkAppVersion { get; set; }

        /// <summary>
        /// External ip address
        /// </summary>
        public string? ExternalIpAddress { get; set; }

        /// <summary>
        /// Internal ip address
        /// </summary>
        public string? InternalIpAddress { get; set; }

        /// <summary>
        /// Link app type
        /// </summary>
        public AppModes? LinkAppType { get; set; }
    }
}
