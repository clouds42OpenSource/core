﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.LocaleContext.Dtos;
using Core42.Application.Features.LocaleContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.LocaleContext.Handlers
{
    /// <summary>
    /// Get locale by id query handler
    /// </summary>
    public class GetLocaleByIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetLocaleByIdQuery, ManagerResult<LocaleDto>>
    {
        /// <summary>
        /// Handle get locale by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<LocaleDto>> Handle(GetLocaleByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Locales_Edit, () => accessProvider.ContextAccountId);

                return (await unitOfWork.LocaleRepository
                     .AsQueryable()
                     .ProjectTo<LocaleDto>(mapper.ConfigurationProvider)
                     .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                     .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения данных локали по ID {request.Id}]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<LocaleDto>(ex.Message);
            }
        }
    }
}
