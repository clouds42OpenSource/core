﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.LocaleContext;
using Core42.Application.Contracts.Features.LocaleContext.Dtos;
using Core42.Application.Features.LocaleContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.LocaleContext.Handlers
{
    /// <summary>
    /// Get filtered locales query handler
    /// </summary>
    public class GetFilteredLocalesQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredLocalesQuery, ManagerResult<PagedDto<LocaleDto>>>
    {
        /// <summary>
        /// Handle get filtered locales query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<LocaleDto>>> Handle(GetFilteredLocalesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Locales_Edit, () => accessProvider.ContextAccountId);

                return (await unitOfWork.LocaleRepository
                    .AsQueryable()
                    .Where(LocaleSpecification.BySearchLine(request.Filter?.SearchLine))
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<LocaleDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения локалей по фильтру]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<LocaleDto>>(ex.Message);
            }
        }
    }
}
