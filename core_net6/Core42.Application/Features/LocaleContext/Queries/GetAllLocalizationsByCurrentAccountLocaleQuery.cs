﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Logger;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.LocaleContext.Queries
{
    public class GetAllLocalizationsByCurrentAccountLocaleQuery(Guid? localeId, Guid? accountId)
        : IRequest<ManagerResult<List<CloudLocalizationDto>>>
    {
        public Guid? LocaleId { get; set; } = localeId;
        public Guid? AccountId { get; set; } = accountId;
    }

    public class GetAllLocalizationsByCurrentAccountLocaleQueryHandler(ILogger42 logger, IAccessProvider accessProvider, ICloudLocalizer cloudLocalizer)
        : IRequestHandler<GetAllLocalizationsByCurrentAccountLocaleQuery, ManagerResult<List<CloudLocalizationDto>>>
    {
        public async Task<ManagerResult<List<CloudLocalizationDto>>> Handle(GetAllLocalizationsByCurrentAccountLocaleQuery request, CancellationToken cancellationToken)
        {
            try
            {
                return await Task.Run(() => cloudLocalizer.GetAllLocalizationsByAccount(
                    request.AccountId ?? accessProvider.ContextAccountId,
                    request.LocaleId).ToOkManagerResult(), cancellationToken);
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Произошла ошибка при получении списка локализаций аккаунта");
                return PagedListExtensions.ToPreconditionFailedManagerResult<List<CloudLocalizationDto>>(ex.Message);
            }
        }
    }
}
