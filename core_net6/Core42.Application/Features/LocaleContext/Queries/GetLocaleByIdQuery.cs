﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Contracts.Features.LocaleContext.Dtos;
using MediatR;

namespace Core42.Application.Features.LocaleContext.Queries
{
    /// <summary>
    /// Get locale by id query
    /// </summary>
    public class GetLocaleByIdQuery(Guid id) : IRequest<ManagerResult<LocaleDto>>
    {
        /// <summary>
        /// Locale id
        /// </summary>
        public Guid Id { get; set; } = id;
    }
}
