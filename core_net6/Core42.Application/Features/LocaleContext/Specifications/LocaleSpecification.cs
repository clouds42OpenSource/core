﻿using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.LocaleContext.Specifications
{
    /// <summary>
    /// Locale specification
    /// </summary>
    public static class LocaleSpecification
    {
        /// <summary>
        /// Filter locales by search line
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<Locale> BySearchLine(string? searchLine)
        {
            return new Spec<Locale>(x => string.IsNullOrEmpty(searchLine) || 
                                         x.Name.ToLower() == searchLine.ToLower()||
                                         x.Country.ToLower() == searchLine.ToLower()||
                                         x.Currency.ToLower() == searchLine.ToLower());
        }
    }
}
