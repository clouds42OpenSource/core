﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.LocaleContext.Dtos
{
    /// <summary>
    /// Locale as key-value dto
    /// </summary>
    public class LocaleDictionaryDto: DictionaryDto<Guid, string>, IMapFrom<Locale>
    {
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Locale, LocaleDictionaryDto>()
                .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
                .ForMember(x => x.Value, z => z.MapFrom(y => y.Country));
        }
    }
}
