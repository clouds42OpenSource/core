﻿using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.LetterNotification.Constants;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.Getters;
using Clouds42.Logger;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.EmailContext.Commands
{
    public class SendEmailCommand : IRequest<ManagerResult<string>>
    {
        public string Subject { get; set; }
        public string Header { get; set; }
        public List<string> Categories { get; set; }
        public string Body { get; set; }
        public string SendGridTemplateId { get; set; }
        public string? Locale { get; set; }
        public string To { get; set; }
        public string? From { get; set; }
        public List<EmailAddressInfoDto> Tos { get; set; }
        public List<string>? CopyTo { get; set; }
        public List<DocumentDataDto>? Attachments { get; set; }
    }

    public class SendEmailCommandHandler(ILogger42 logger, ISendGridProvider sendGridProvider)
        : IRequestHandler<SendEmailCommand, ManagerResult<string>>
    {
        public async Task<ManagerResult<string>> Handle(SendEmailCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var apiKey = CloudConfigurationProvider.SendGrid.GetApiKey();
                var agentName = CloudConfigurationProvider.SendGrid.GetAgentName();
                var agentUniqueId = CloudConfigurationProvider.SendGrid.GetAgentUniqueId();
                var companyContactInfo = CompanyContactInfoForLetterGetter.Get(request.Locale ?? "ru-ru");
                var fromEmail = string.IsNullOrEmpty(request.From) ?CloudConfigurationProvider.Emails.Get42CloudsManagerEmail(): request.From;

                var sendGridEmailPayload = new CommandExecuteRequestPayloadDto
                {
                    EmailRequestSettings = new EmailRequestSettingsDto
                    {
                        Attachments = request.Attachments,
                        Categories = request.Categories,
                        CopyToEmailAddresses = request.CopyTo?.Select(x => new EmailAddressInfoDto(x))?.ToList(),
                        EmailSender = new EmailAddressInfoDto(fromEmail),
                        EmailReceiver = new EmailAddressInfoDto(request.To),
                        SendGridApiKey = apiKey,
                        EmailReceivers = request.Tos,
                        SendGridAgentName = agentName,
                        SendGridAgentUniqueId = agentUniqueId,
                        SendGridTemplateId = request.SendGridTemplateId,
                        IsBccRecipients = true
                    },
                    LetterTemplateCommonData = new LetterTemplateCommonDataDto
                    {
                        LetterTemplateData = new LetterTemplateDataModelDto
                        {
                            Body = request.Body,
                            Header = request.Header,
                            Subject = request.Subject
                        },
                        CompanyDetails = new CompanyDetailsDto
                        {
                            Email = companyContactInfo.Email,
                            TelegramLink = companyContactInfo.TelegramLink,
                            FacebookLink = companyContactInfo.FacebookLink,
                            InstagramLink = companyContactInfo.InstagramLink,
                            NeedShowSocialMediaBlock = companyContactInfo.NeedShowSocialMediaBlock,
                            Phone = companyContactInfo.Phone,
                            SiteLink = companyContactInfo.SiteLink,
                            UnsubscribeText = LetterHtmlTemplateTextConst.UnsubscribeText
                        }
                    }
                };
                
                await sendGridProvider.SendEmailAsync(sendGridEmailPayload);

                const string successMessage = "Письмо успешно отправлено";

                return successMessage.ToOkManagerResult();

            }

            catch (Exception e)
            {
               logger.Error(e,"Ошибка отправки письма в SendGrid");

               return PagedListExtensions.ToPreconditionFailedManagerResult<string>(e.Message);
            }
        }
    }
}
