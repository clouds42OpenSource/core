﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.IndustyContext.Dtos;
using Core42.Application.Features.IndustyContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.IndustyContext.Queries
{
    /// <summary>
    /// Get industry filtered query
    /// </summary>
    public class GetIndustryFilteredQuery : IRequest<ManagerResult<PagedDto<IndustryDataDto>>>, ISortedQuery, IPagedQuery, IHasFilter<IndustryDataFilter?>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(Industry.Name)}.asc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Industry data filter
        /// </summary>
        public IndustryDataFilter? Filter { get; set; }
    }
}
