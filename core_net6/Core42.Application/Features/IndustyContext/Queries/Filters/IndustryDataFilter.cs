﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.IndustyContext.Queries.Filters
{
    /// <summary>
    /// Industry data filter
    /// </summary>
    public class IndustryDataFilter : IQueryFilter
    {
        /// <summary>
        /// Industry name
        /// </summary>
        public string? Name { get; set; }
    }
}
