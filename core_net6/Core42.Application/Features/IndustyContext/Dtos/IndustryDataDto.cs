﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.IndustyContext.Dtos
{
    /// <summary>
    /// Industry data dto
    /// </summary>
    public class IndustryDataDto : IMapFrom<Industry>
    {
        /// <summary>
        /// Industry id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Industry name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Industry description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Industry, IndustryDataDto>();
        }
    }
}
