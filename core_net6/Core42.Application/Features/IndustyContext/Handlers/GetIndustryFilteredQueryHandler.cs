﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.IndustyContext.Dtos;
using Core42.Application.Features.IndustyContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.IndustyContext.Handlers
{
    /// <summary>
    /// Get industry filtered query handler
    /// </summary>
    public class GetIndustryFilteredQueryHandler(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetIndustryFilteredQuery, ManagerResult<PagedDto<IndustryDataDto>>>
    {
        /// <summary>
        /// Handle get industry filtered query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<IndustryDataDto>>> Handle(GetIndustryFilteredQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ViewIndustries, () => accessProvider.ContextAccountId);

                return (await unitOfWork.IndustryRepository
                    .AsQueryable()
                    .AutoFilter(request.Filter)
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<IndustryDataDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception exception)
            {
                handlerException.Handle(exception, "[Ошибка получения списка отраслей.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<IndustryDataDto>>(exception.Message);
            }
        }
    }
}
