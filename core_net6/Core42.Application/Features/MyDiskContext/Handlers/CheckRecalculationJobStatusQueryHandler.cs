﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.MyDiskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.MyDiskContext.Handlers
{
    /// <summary>
    /// Handler for check recalculation job status
    /// </summary>
    public class CheckRecalculationJobStatusQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork)
        : IRequestHandler<CheckRecalculationJobStatusQuery, ManagerResult<bool>>
    {
        public async Task<ManagerResult<bool>> Handle(CheckRecalculationJobStatusQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Resources_RecalculateMyDisk, () => accessProvider.ContextAccountId);
                return (await unitOfWork.CoreWorkerTasksQueueRepository.AsQueryable()
                    .AnyAsync(t => t.Id == request.TaskId
                    && (t.Status == CloudTaskQueueStatus.Ready.ToString() || t.Status == CloudTaskQueueStatus.Error.ToString()), cancellationToken))
                .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
            }
        }
    }
}
