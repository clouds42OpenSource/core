﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDisk;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.MyDiskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.MyDiskContext.Handlers
{
    /// <summary>
    /// Handler to get the management model for "My Disk" service
    /// </summary>
    public class GetMyDiskManagementDataQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork,
        ICloud42ServiceHelper cloud42ServiceHelper,
        IResourceConfigurationDataProvider resourceConfigurationDataProvider)
        : IRequestHandler<GetMyDiskManagmentDataQuery, ManagerResult<ServiceMyDiskManagerDto>>
    {
        /// <summary>
        /// Handle query to get the managment model for "My Disk" service
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ManagerResult<ServiceMyDiskManagerDto>> Handle(GetMyDiskManagmentDataQuery request, CancellationToken cancellationToken)
        {
            string message = $"Получение модели управления сервисом Мой диск для аккаунта {request.AccountId}";

            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_EditResources_MyDisk, () => request.AccountId);

                var accountConfiguration = await unitOfWork.AccountConfigurationRepository.AsQueryable()
                    .Include(conf => conf.Account).Include(conf => conf.Locale)
                    .FirstOrDefaultAsync(conf => conf.AccountId == request.AccountId && conf.Account != null, cancellationToken)
                    ?? throw new NotFoundException($"Не удалось получить аккаунт и его конфигурацию по Id '{request.AccountId}'");

                var resourceConfiguration = await unitOfWork.ResourceConfigurationRepository.AsQueryable()
                    .FirstOrDefaultAsync(conf => conf.AccountId == request.AccountId && conf.BillingService.SystemService == Clouds42Service.MyDisk, cancellationToken)
                    ?? throw new NotFoundException($"Не удалось найти конфигурацию ресурса для аккаунта {request.AccountId}");

                var diskInfo = cloud42ServiceHelper.GetMyDiskInfo(request.AccountId)
                ?? throw new NotFoundException($"Не удалось получить информацию по состоянию дискового пространства для аккаунта {request.AccountId}");

                // if main service expire date is null - service is inactive
                var expireDate = resourceConfigurationDataProvider.GetExpireDate(resourceConfiguration) == null ? default
                    : resourceConfigurationDataProvider.GetExpireDateOrThrowException(resourceConfiguration);

                return new ServiceMyDiskManagerDto
                {
                    AccountIsVip = accountConfiguration.IsVip,
                    Cost = resourceConfiguration.Cost,
                    DiscountGroup = resourceConfiguration.DiscountGroup,
                    ExpireDate = expireDate,
                    Tariff = diskInfo.BillingInfo.CurrentTariffInGb,
                    Locale = new LocaleDto
                    {
                        Name = accountConfiguration.Locale.Name,
                        Currency = accountConfiguration.Locale.Currency
                    }
                }.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{message} завершилось ошибкой.]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<ServiceMyDiskManagerDto>(ex.Message);
            }
        }
    }
}
