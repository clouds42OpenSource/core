﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.MyDiskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.MyDiskContext.Handlers
{
    public class GetCalculationSizeChangeQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ICloud42ServiceHelper cloud42ServiceHelper)
        : IRequestHandler<GetCalculationSizeChangeQuery, ManagerResult<MyDiskChangeTariffPreviewDto>>
    {
        public Task<ManagerResult<MyDiskChangeTariffPreviewDto>> Handle(GetCalculationSizeChangeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ManageResources, () => request.AccountId);

                return Task.FromResult(cloud42ServiceHelper.ChangeSizeOfServicePreview(request.AccountId, request.SizeGb).ToOkManagerResult());
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[An error occurred while pre-calculation disk size change for account {request.AccountId}]");

                return Task.FromResult(PagedListExtensions.ToPreconditionFailedManagerResult<MyDiskChangeTariffPreviewDto>(ex.Message));
            }
        }
    }
}
