﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Billing.Contracts.Billing.ModelProcessors;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.MyDiskContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.MyDiskContext.Handlers
{
    /// <summary>
    /// Handler for gettting data on "My Disk" service
    /// </summary>
    public class GetMyDiskDataQueryHandler(
        IAccessProvider accessProvider,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IServiceMyDiskDomainModelProcessor serviceMyDiskDomainModelProcessor)
        : IRequestHandler<GetMyDiskDataQuery, ManagerResult<ServiceMyDiskDto>>
    {
        public async Task<ManagerResult<ServiceMyDiskDto>> Handle(GetMyDiskDataQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ServiceMenu_MyDisk, () => request.AccountId);

                if(await unitOfWork.ServiceAccountRepository
                       .AsQueryableNoTracking()
                       .AnyAsync(w => w.Id == request.AccountId, cancellationToken))
                  throw new InvalidOperationException("Служебные аккаунты не могут управлять сервисами");
                

                return serviceMyDiskDomainModelProcessor.GetModel(request.AccountId).ToOkManagerResult();
            }
            catch (Exception ex)
            {
                logger.Warn(
                    $"Произошла ошибка при получении модели для Моего Диска для учетной записи. {request.AccountId}. Причина: {ex.GetFullInfo()}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<ServiceMyDiskDto>(ex.Message);
            }
        }
    }
}
