﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDisk;
using MediatR;

namespace Core42.Application.Features.MyDiskContext.Queries
{
    /// <summary>
    /// Query for preliminary calculation of disk size change
    /// </summary>
    public class GetCalculationSizeChangeQuery : IRequest<ManagerResult<MyDiskChangeTariffPreviewDto>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Disk size in GB
        /// </summary>
        [Required]
        public int SizeGb { get; set; }
    }
}
