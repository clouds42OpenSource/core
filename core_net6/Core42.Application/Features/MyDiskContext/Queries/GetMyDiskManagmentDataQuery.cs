﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDisk;
using MediatR;

namespace Core42.Application.Features.MyDiskContext.Queries
{
    /// <summary>
    /// Query for getting managment data for "My Disk" service
    /// </summary>
    public class GetMyDiskManagmentDataQuery(Guid accountId) : IRequest<ManagerResult<ServiceMyDiskManagerDto>>
    {
        /// <summary>
        /// Requesting account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; } = accountId;
    }
}
