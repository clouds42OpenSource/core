﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.MyDiskContext.Queries
{
    /// <summary>
    /// Query to check recalculation job status
    /// </summary>
    public class CheckRecalculationJobStatusQuery : IRequest<ManagerResult<bool>>
    {
        /// <summary>
        /// Task identifier
        /// </summary>
        public Guid TaskId { get; set; }
    }
}
