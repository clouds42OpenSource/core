﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.MyDisk;
using MediatR;

namespace Core42.Application.Features.MyDiskContext.Queries
{
    /// <summary>
    /// Query to get "My Disk" service info
    /// </summary>
    public class GetMyDiskDataQuery : IRequest<ManagerResult<ServiceMyDiskDto>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
