﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.Supplier;
using Core42.Application.Features.SupplierContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SupplierContext.Queries
{
    /// <summary>
    /// Get filtered supplier query
    /// </summary>
    public class GetFilteredSuppliersQuery : ISortedQuery, IPagedQuery, IRequest<ManagerResult<PagedDto<SupplierDto>>>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(Supplier.Name)}.asc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }
    }

}
