﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.SupplierContext.Dtos;
using MediatR;

namespace Core42.Application.Features.SupplierContext.Queries
{
    /// <summary>
    /// Get supplier references combo-boxes
    /// </summary>
    public class GetSupplierReferencesComboBoxesQuery : IRequest<ManagerResult<SupplierComboBoxesDto>>
    {
    }
}
