﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.SupplierContext.Dtos;
using MediatR;

namespace Core42.Application.Features.SupplierContext.Queries
{
    /// <summary>
    /// Get supplier reference by id query
    /// </summary>
    public class GetSupplierReferenceByIdQuery(Guid id) : IRequest<ManagerResult<SupplierReferenceDto>>
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; } = id;
    }
}
