﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.SupplierContext.Dtos;
using MediatR;

namespace Core42.Application.Features.SupplierContext.Queries
{
    /// <summary>
    /// Get supplier reference referral accounts query
    /// </summary>
    public class GetSupplierReferenceReferralAccountsQuery(Guid supplierId)
        : IRequest<ManagerResult<List<SupplierReferralAccountsDictionaryDto>>>
    {
        /// <summary>
        /// Supplier id
        /// </summary>
        public Guid SupplierId { get; set; } = supplierId;
    }
}
