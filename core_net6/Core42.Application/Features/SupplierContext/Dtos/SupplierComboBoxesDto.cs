﻿namespace Core42.Application.Features.SupplierContext.Dtos
{
    /// <summary>
    /// Supplier combo-boxes dto
    /// </summary>
    public class SupplierComboBoxesDto(
        List<PrintedHtmlFormComboBoxDto> locales,
        List<PrintedHtmlFormComboBoxDto> printedHtmlForms)
    {
        /// <summary>
        /// All locales
        /// </summary>
        public IEnumerable<PrintedHtmlFormComboBoxDto> Locales { get; set; } = locales;

        /// <summary>
        /// Все printed html forms
        /// </summary>
        public IEnumerable<PrintedHtmlFormComboBoxDto> PrintedHtmlForms { get; set; } = printedHtmlForms;
    }
}
