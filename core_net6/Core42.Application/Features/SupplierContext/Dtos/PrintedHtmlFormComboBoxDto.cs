﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SupplierContext.Dtos
{
    /// <summary>
    /// Printed html form combo box dto
    /// </summary>
    public class PrintedHtmlFormComboBoxDto: ComboBoxDto<Guid>, IMapFrom<PrintedHtmlForm>, IMapFrom<Locale>
    {
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<PrintedHtmlForm, PrintedHtmlFormComboBoxDto>()
                .ForMember(x => x.Value, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.Text, z => z.MapFrom(y => y.Name));

            profile.CreateMap<Locale, PrintedHtmlFormComboBoxDto>()
                .ForMember(x => x.Value, z => z.MapFrom(y => y.ID))
                .ForMember(x => x.Text, z => z.MapFrom(y => y.Country));
        }
    }
}
