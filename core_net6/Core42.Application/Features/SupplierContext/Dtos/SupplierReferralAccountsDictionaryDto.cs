﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels.Supplier;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SupplierContext.Dtos
{
    /// <summary>
    /// Supplier referral accounts as key-value dto
    /// </summary>
    public class SupplierReferralAccountsDictionaryDto : DictionaryDto<Guid, string>, IMapFrom<SupplierReferralAccount>
    {
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SupplierReferralAccount, SupplierReferralAccountsDictionaryDto>()
                .ForMember(x => x.Key, m => m.MapFrom(z => z.ReferralAccountId))
                .ForMember(x => x.Value, m => m.MapFrom(z => z.ReferralAccount.AccountCaption.Replace("\"", "'")));
        }
    }
}
