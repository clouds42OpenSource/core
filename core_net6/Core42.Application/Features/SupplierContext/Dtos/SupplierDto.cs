﻿using AutoMapper;
using Clouds42.Domain.DataModels.Supplier;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SupplierContext.Dtos
{
    /// <summary>
    /// Supplier dto
    /// </summary>
    public class SupplierDto : IMapFrom<Supplier>
    {
        /// <summary>
        /// Locale name
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// Supplier identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Supplier name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Supplier code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Is default supplier on locale
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Supplier, SupplierDto>()
                .ForMember(x => x.LocaleName, z => z.MapFrom(y => y.Locale.Name));
        }
    }
}
