﻿namespace Core42.Application.Features.SupplierContext.Dtos
{
    /// <summary>
    /// Combo box dto
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class ComboBoxDto<TValue>
    {
        /// <summary>
        /// Element value
        /// </summary>
        public TValue Value { get; set; }

        /// <summary>
        /// Element text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Element group
        /// </summary>
        public string Group { get; set; }
    }
}
