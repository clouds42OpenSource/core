﻿using AutoMapper;
using Clouds42.Domain.DataModels.Supplier;
using Core42.Application.Contracts.Features.LocaleContext.Dtos;
using Core42.Application.Features.PrintedHtmlFormContext.Dtos;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SupplierContext.Dtos
{
    /// <summary>
    /// Supplier reference dto
    /// </summary>
    public class SupplierReferenceDto : IMapFrom<Supplier>
    {
        /// <summary>
        /// ID
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Locale id
        /// </summary>
        public Guid LocaleId { get; set; }

        /// <summary>
        /// Locale dto
        /// </summary>
        public LocaleDto Locale { get; set; }

        /// <summary>
        /// Is default flag
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Agreement id
        /// </summary>
        public Guid? AgreementId { get; set; }

        /// <summary>
        /// Agreement file dto
        /// </summary>
        public CloudFileDto Agreement { get; set; }


        /// <summary>
        /// Printed html from invoice id
        /// </summary>
        public Guid? PrintedHtmlFormInvoiceId { get; set; }

        /// <summary>
        /// Printed html form invoice receipt id
        /// </summary>
        public Guid? PrintedHtmlFormInvoiceReceiptId { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Supplier, SupplierReferenceDto>();
        }
    }
}
