﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SupplierContext.Dtos;
using Core42.Application.Features.SupplierContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SupplierContext.Handlers
{
    /// <summary>
    /// Get filtered suppliers query handler
    /// </summary>
    public class GetFilteredSuppliersQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredSuppliersQuery, ManagerResult<PagedDto<SupplierDto>>>
    {
        /// <summary>
        /// Handle get filtered suppliers query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<SupplierDto>>> Handle(GetFilteredSuppliersQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.SupplierReference_View, () => accessProvider.ContextAccountId);

                return (await unitOfWork.SupplierRepository
                    .AsQueryable()
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<SupplierDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при получении списка поставщиков по фильтру.]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<SupplierDto>>(ex.Message);
            }
        }
    }

}
