﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SupplierContext.Dtos;
using Core42.Application.Features.SupplierContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SupplierContext.Handlers
{
    /// <summary>
    /// Get supplier reference by id query handler
    /// </summary>
    public class GetSupplierReferenceByIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider)
        : IRequestHandler<GetSupplierReferenceByIdQuery, ManagerResult<SupplierReferenceDto>>
    {
        /// <summary>
        /// Handle get supplier reference by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<SupplierReferenceDto>> Handle(GetSupplierReferenceByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.SupplierReference_View, () => accessProvider.ContextAccountId);

                var record = await unitOfWork.SupplierRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                return mapper.Map<SupplierReferenceDto>(record).ToOkManagerResult();
            }

            catch (Exception e)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<SupplierReferenceDto>(e.Message);
            }
        }
    }
}
