﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SupplierContext.Dtos;
using Core42.Application.Features.SupplierContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SupplierContext.Handlers
{
    /// <summary>
    /// Get supplier references combo-boxes query handler
    /// </summary>
    public class GetSupplierReferencesComboBoxesQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : IRequestHandler<GetSupplierReferencesComboBoxesQuery, ManagerResult<SupplierComboBoxesDto>>
    {
        /// <summary>
        /// Handle get supplier references combo-boxes query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<SupplierComboBoxesDto>> Handle(GetSupplierReferencesComboBoxesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var printedHtmlForms = await unitOfWork.PrintedHtmlFormRepository
                    .AsQueryable()
                    .ProjectTo<PrintedHtmlFormComboBoxDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                printedHtmlForms.Insert(0,
                    new PrintedHtmlFormComboBoxDto { Text = "Выберите печатную форму", Value = Guid.Empty });

                var locales = await unitOfWork.LocaleRepository
                    .AsQueryable()
                    .ProjectTo<PrintedHtmlFormComboBoxDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                return new SupplierComboBoxesDto(locales, printedHtmlForms).ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка при получении элементов для инициализации страницы создания/редактирования поставщика.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<SupplierComboBoxesDto>(ex.Message);
            }
        }
    }
}
