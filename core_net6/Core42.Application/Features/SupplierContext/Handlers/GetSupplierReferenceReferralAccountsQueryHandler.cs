﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SupplierContext.Dtos;
using Core42.Application.Features.SupplierContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SupplierContext.Handlers
{
    /// <summary>
    /// Get supplier reference accounts query handler
    /// </summary>
    public class GetSupplierReferenceReferralAccountsQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetSupplierReferenceReferralAccountsQuery,
            ManagerResult<List<SupplierReferralAccountsDictionaryDto>>>
    {
        /// <summary>
        /// Handle get supplier reference accounts query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<SupplierReferralAccountsDictionaryDto>>> Handle(GetSupplierReferenceReferralAccountsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.SupplierReference_View, () => accessProvider.ContextAccountId);

                return (await unitOfWork.SupplierReferralAccountRepository
                    .AsQueryable()
                    .Where(x => x.SupplierId == request.SupplierId)
                    .ProjectTo<SupplierReferralAccountsDictionaryDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при получении рефералов аккаунта для поставщика.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<SupplierReferralAccountsDictionaryDto>>(ex.Message);
            }
        }
    }
}
