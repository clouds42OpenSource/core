﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.SqlServerContext.Dtos;
using Core42.Application.Features.SqlServerContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SqlServerContext.Queries
{
    public class GetFilteredSqlServersQuery: IRequest<ManagerResult<PagedDto<SqlServerDto>>>, ISortedQuery, IPagedQuery, IHasFilter<SqlServerFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(SqlServerDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public SqlServerFilter Filter { get; set; }
    }
}
