﻿using Clouds42.Domain.Enums.AccountDatabase;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.SqlServerContext.Queries.Filters;

public class SqlServerFilter : IQueryFilter
{
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    /// <summary>
    /// Restore model type
    /// </summary>
    public AccountDatabaseRestoreModelTypeEnum? RestoreModelType { get; set; }
}
