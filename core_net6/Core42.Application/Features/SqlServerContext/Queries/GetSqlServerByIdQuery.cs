﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.SqlServerContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.SqlServerContext.Queries;

public class GetSqlServerByIdQuery : IRequest<ManagerResult<SqlServerDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
