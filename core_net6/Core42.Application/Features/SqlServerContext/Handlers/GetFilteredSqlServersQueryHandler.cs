﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SqlServerContext.Dtos;
using Core42.Application.Features.SqlServerContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SqlServerContext.Handlers;

public class GetFilteredSqlServersQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredSqlServersQuery,
        ManagerResult<PagedDto<SqlServerDto>>>
{
    public async Task<ManagerResult<PagedDto<SqlServerDto>>> Handle(GetFilteredSqlServersQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesSqlServerRepository
                    .AsQueryable()
                    .ProjectTo<SqlServerDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка sql серверов]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<SqlServerDto>>(ex.Message);
        }
    }
}
