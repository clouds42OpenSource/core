﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.SqlServerContext.Dtos;
using Core42.Application.Features.SqlServerContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.SqlServerContext.Handlers;

public class GetSqlServerByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetSqlServerByIdQuery, ManagerResult<SqlServerDto>>
{
    public async Task<ManagerResult<SqlServerDto>> Handle(GetSqlServerByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesSqlServerRepository
                    .AsQueryable()
                    .ProjectTo<SqlServerDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                .ToOkManagerResult();
        }
        catch (Exception ex)
        {
            handlerException.Handle(ex,
                $"[Ошибка получения sql сервера {request.Id}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<SqlServerDto>(ex.Message);
        }
    }
}
