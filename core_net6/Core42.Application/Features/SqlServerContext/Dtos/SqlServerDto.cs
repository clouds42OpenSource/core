﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.SqlServerContext.Dtos;

public class SqlServerDto : IMapFrom<CloudServicesSqlServer>
{
    public Guid Id { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    /// <summary>
    /// Restore model type
    /// </summary>
    public AccountDatabaseRestoreModelTypeEnum RestoreModelType { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesSqlServer, SqlServerDto>()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID));
    }
}
