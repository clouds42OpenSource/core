﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.TerminalFarmContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.TerminalFarmContext.Queries;

public class GetTerminalFarmByIdQuery : IRequest<ManagerResult<TerminalFarmDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
