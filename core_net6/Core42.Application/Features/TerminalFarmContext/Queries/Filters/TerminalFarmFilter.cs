﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.TerminalFarmContext.Queries.Filters;

public class TerminalFarmFilter : IQueryFilter
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string ConnectionAddress { get; set; }
}
