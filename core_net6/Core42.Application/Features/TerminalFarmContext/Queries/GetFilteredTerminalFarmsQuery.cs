﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.TerminalFarmContext.Dtos;
using Core42.Application.Features.TerminalFarmContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.TerminalFarmContext.Queries
{
    public class GetFilteredTerminalFarmsQuery : IRequest<ManagerResult<PagedDto<TerminalFarmDto>>>, ISortedQuery, IPagedQuery, IHasFilter<TerminalFarmFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(TerminalFarmDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public TerminalFarmFilter Filter { get; set; }
    }
}
