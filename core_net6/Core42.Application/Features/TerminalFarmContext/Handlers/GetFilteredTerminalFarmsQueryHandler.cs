﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.TerminalFarmContext.Dtos;
using Core42.Application.Features.TerminalFarmContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.TerminalFarmContext.Handlers;

public class GetFilteredTerminalFarmsQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredTerminalFarmsQuery,
        ManagerResult<PagedDto<TerminalFarmDto>>>
{
    public async Task<ManagerResult<PagedDto<TerminalFarmDto>>> Handle(GetFilteredTerminalFarmsQuery request,
        CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.ViewCloudServiceTerminalFarm,
                () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesTerminalFarmRepository
                    .AsQueryable()
                    .ProjectTo<TerminalFarmDto>(mapper.ConfigurationProvider)
                    .AutoFilter(request.Filter)
                    .AutoSort(request)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка ферм ТС.]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<TerminalFarmDto>>(ex.Message);
        }
    }
}
