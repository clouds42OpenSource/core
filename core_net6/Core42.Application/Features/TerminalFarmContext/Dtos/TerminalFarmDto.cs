﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.TerminalFarmContext.Dtos;

public class TerminalFarmDto : IMapFrom<CloudServicesTerminalFarm>
{
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    public bool UsingOutdatedWindows { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesTerminalFarm, TerminalFarmDto>()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Name, z => z.MapFrom(y => y.Name))
            .ForMember(x => x.Description, z => z.MapFrom(y => y.Description))
            .ForMember(x => x.ConnectionAddress, z => z.MapFrom(y => y.ConnectionAddress))
            .ForMember(x => x.UsingOutdatedWindows, z => z.MapFrom(y => y.UsingOutdatedWindows));
    }
}
