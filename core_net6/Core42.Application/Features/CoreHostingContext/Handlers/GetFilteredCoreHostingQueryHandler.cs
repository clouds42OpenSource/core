﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreHostingContext.Dtos;
using Core42.Application.Features.CoreHostingContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreHostingContext.Handlers;

public class GetFilteredCoreHostingQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredCoreHostingQuery,
        ManagerResult<PagedDto<CoreHostingDto>>>
{
    public async Task<ManagerResult<PagedDto<CoreHostingDto>>> Handle(GetFilteredCoreHostingQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_View, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CoreHostingRepository
                    .AsQueryable()
                    .ProjectTo<CoreHostingDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка хостингов]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<CoreHostingDto>>(ex.Message);
        }
    }
}
