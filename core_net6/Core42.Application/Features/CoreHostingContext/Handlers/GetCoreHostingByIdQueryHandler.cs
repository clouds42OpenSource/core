﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CoreHostingContext.Dtos;
using Core42.Application.Features.CoreHostingContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreHostingContext.Handlers;

public class GetCoreHostingByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetCoreHostingByIdQuery, ManagerResult<CoreHostingDto>>
{
    public async Task<ManagerResult<CoreHostingDto>> Handle(GetCoreHostingByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CoreHostingRepository
                    .AsQueryable()
                    .ProjectTo<CoreHostingDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, $"[Ошибка получения хостинга] {request.Id}");

            return PagedListExtensions.ToPreconditionFailedManagerResult<CoreHostingDto>(ex.Message);
        }
    }
}
