﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CoreHostingContext.Dtos;

public class CoreHostingDto : IMapFrom<CoreHosting>
{
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// File path
    /// </summary>
    public string UploadFilesPath { get; set; }

    /// <summary>
    /// Api path
    /// </summary>
    public string UploadApiUrl { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CoreHosting, CoreHostingDto>();
    }
}
