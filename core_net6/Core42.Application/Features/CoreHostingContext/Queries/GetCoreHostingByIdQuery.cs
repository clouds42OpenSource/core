﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CoreHostingContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.CoreHostingContext.Queries;

public class GetCoreHostingByIdQuery : IRequest<ManagerResult<CoreHostingDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
