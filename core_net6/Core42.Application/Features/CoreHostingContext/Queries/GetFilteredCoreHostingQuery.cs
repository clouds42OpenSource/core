﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.CoreHostingContext.Dtos;
using Core42.Application.Features.CoreHostingContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CoreHostingContext.Queries
{
    public class GetFilteredCoreHostingQuery : IRequest<ManagerResult<PagedDto<CoreHostingDto>>>, ISortedQuery, IPagedQuery, IHasFilter<CoreHostingFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(CoreHostingDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public CoreHostingFilter Filter { get; set; }
    }
}
