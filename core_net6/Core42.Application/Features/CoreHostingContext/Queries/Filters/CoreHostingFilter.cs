﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.CoreHostingContext.Queries.Filters;

public class CoreHostingFilter : IQueryFilter
{
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// File path
    /// </summary>
    public string UploadFilesPath { get; set; }

    /// <summary>
    /// Api path
    /// </summary>
    public string UploadApiUrl { get; set; }
}
