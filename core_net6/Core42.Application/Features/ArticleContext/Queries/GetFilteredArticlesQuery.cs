﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ArticleContext.Dtos;
using Core42.Application.Features.ArticleContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Queries
{
    public class GetFilteredArticlesQuery : IRequest<ManagerResult<PagedDto<ArticleDto>>>, ISortedQuery, IPagedQuery
    {
        public ArticleFilter? Filter { get; set; }
        public string OrderBy { get; set; } = $"{nameof(ArticleDto.CreatedOn)}.desc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
}
