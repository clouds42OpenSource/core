﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArticleContext.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Queries
{
    public class GetArticleByIdQuery(int id) : IRequest<ManagerResult<ArticleExtendedDto>>
    {
        public int Id { get; set; } = id;
    }

    public class GetArticleByIdQueryHandler(ILogger42 logger, IUnitOfWork unitOfWork, IMapper mapper)
        : IRequestHandler<GetArticleByIdQuery, ManagerResult<ArticleExtendedDto>>
    {
        public async Task<ManagerResult<ArticleExtendedDto>> Handle(GetArticleByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                return (await unitOfWork.ArticleRepository
                    .AsQueryableNoTracking()
                    .ProjectTo<ArticleExtendedDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                    .ToOkManagerResult();
            }

            catch (Exception e)
            {
                logger.Error(e, $"Ошибка получения статьи по идентификатору {request.Id}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<ArticleExtendedDto>(
                 $"Произошла ошибка при получении информации о статье по идентификатору {request.Id}");
            }
        }
    }
}
