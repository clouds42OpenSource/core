﻿using Core42.Application.Features.ArticleContext.Dtos;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Queries.Specifications
{
    public static class ArticleSpecification
    {
        public static Spec<ArticleDto> ByDate(DateTime? from, DateTime? to)
        {
            return new Spec<ArticleDto>(x =>
                (!from.HasValue || x.CreatedOn >= from.Value) && (!to.HasValue || x.CreatedOn <= to.Value));
        }
    }
}
