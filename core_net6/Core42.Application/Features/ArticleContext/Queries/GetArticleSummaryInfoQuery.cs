﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Queries
{
    public class GetArticleSummaryInfoQuery(Guid id) : IRequest<ManagerResult<ArticleSummaryInfoDto>>
    {
        /// <summary>
        /// идентификатор пользователя
        /// </summary>
        public Guid AccountUserId { get; set; } = id;
    }

    public class GetArticleSummaryInfoQueryHandler(
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        IAccessProvider accessProvider)
        : IRequestHandler<GetArticleSummaryInfoQuery, ManagerResult<ArticleSummaryInfoDto>>
    {
        private readonly IAccessProvider _accessProvider = accessProvider;

        public async Task<ManagerResult<ArticleSummaryInfoDto>> Handle(GetArticleSummaryInfoQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await unitOfWork.AccountUsersRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == request.AccountUserId, cancellationToken)
                        ?? throw new NotFoundException($"Не найден пользователь по идентификатору {request.AccountUserId}");

                var articlesCount = await unitOfWork.ArticleRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.AccountUserId == user.Id)
                    .CountAsync(cancellationToken);
                logger.Debug($"Кол-во статей {articlesCount}");

                var registrationCount = await unitOfWork.ArticleRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.AccountUserId == user.Id)
                    .SumAsync(x => x.RegistrationCount, cancellationToken);
                logger.Debug($"Кол-во регистраций {registrationCount}");

                var articleWallets = await unitOfWork.ArticleWalletsRepository
                     .AsQueryable()
                     .FirstOrDefaultAsync(x => x.AccountUserId == user.Id, cancellationToken)??
                     new ArticleWallets 
                     {
                            AvailableSum = 0,
                            TotalEarned = 0,
                     };

                logger.Debug($"Кол-во всего заработано {articleWallets.TotalEarned}, доступное к выплате {articleWallets.AvailableSum}");

                return new ArticleSummaryInfoDto { 
                    Count = articlesCount, 
                    UsersCount = registrationCount.HasValue? registrationCount.Value : 0,
                    AvailableSum = articleWallets.AvailableSum, 
                    TotalEarned = articleWallets.TotalEarned
                }.ToOkManagerResult();

            }

            catch (Exception e)
            {
                logger.Error(e, "Ошибка при получении общей информации о статьях");

                return PagedListExtensions.ToPreconditionFailedManagerResult<ArticleSummaryInfoDto>(
                    "Произошла ошибка при получении общей информации по статьям");
            }
        }
    }

    public class ArticleSummaryInfoDto
    {
        //кол-во статей
        public int Count { get; set; }
        //кол-во регистраций
        public int UsersCount { get; set; }
        //всего заработано
        public decimal TotalEarned { get; set; }
        //доступно к выводу
        public decimal AvailableSum { get; set; }

    }
}
