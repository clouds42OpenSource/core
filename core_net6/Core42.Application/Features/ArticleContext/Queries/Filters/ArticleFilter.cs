﻿using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Queries.Filters;

public class ArticleFilter : IQueryFilter
{
    public ArticleStatus? Status { get; set; }
    public ArticleType? Type { get; set; }
    public DateTime? CreatedFrom { get; set; }
    public DateTime? CreatedTo { get; set; }
    public string AccountUserLogin { get; set; }
    public Guid? AccountUserId { get; set; }
    public string Topic { get; set; }
}
