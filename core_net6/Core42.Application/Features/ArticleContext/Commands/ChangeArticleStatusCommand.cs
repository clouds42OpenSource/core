﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.ArticleContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ArticleContext.Commands
{
    public class ChangeArticleStatusCommand: IRequest<ManagerResult<ArticleDto>>
    {
        public int Id { get; set; }
        public ArticleStatus Status { get; set; }
        public string Reason { get; set; }
    }
}
