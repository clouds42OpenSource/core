﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.ArticleContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ArticleContext.Commands
{
    public class CreateArticleCommand : IRequest<ManagerResult<ArticleDto>>
    {
        public string Topic { get; set; }
        public string TopicId { get; set; }
        public ArticleType Type { get; set; }
        public string WpId { get; set; }
    }
}
