﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ArticleContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ArticleContext.Commands
{
    public class UpdateArticleCommand: IRequest<ManagerResult<ArticleExtendedDto>>
    {
        public int Id { get; set; }
        public string Topic { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string WpLink { get; set; }
        public string WpId { get; set; }
        public int? RegistrationCount { get; set; }
    }
}
