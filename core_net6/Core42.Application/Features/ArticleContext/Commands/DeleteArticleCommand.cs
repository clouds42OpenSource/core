﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.GoogleCloud;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Commands
{
    public class DeleteArticleCommand(int id) : IRequest<ManagerResult<string>>
    {
        public int Id { get; set; } = id;
    }

    public class DeleteArticleCommandHandler(
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IGoogleCloudProvider googleCloudProvider,
        IAccessProvider accessProvider)
        : IRequestHandler<DeleteArticleCommand, ManagerResult<string>>
    {
        public async Task<ManagerResult<string>> Handle(DeleteArticleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await accessProvider.GetUserAsync();
                var article = await unitOfWork.ArticleRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                if (article == null)
                {
                    return PagedListExtensions.ToPreconditionFailedManagerResult<string>(
                        "Не найдена статья для удаления");
                }

                if(!string.IsNullOrEmpty(article.GoogleCloudDocumentId))
                    await googleCloudProvider.DeleteDocumentAsync(article.GoogleCloudDocumentId);

                unitOfWork.ArticleRepository.Delete(article);

                await unitOfWork.SaveAsync();

                LogEventHelper.LogEvent(unitOfWork, user.ContextAccountId, accessProvider, LogActions.DeleteArticle, $"Удалена статья {article.Topic}, пользователем {user.Email}");

                const string successMessage = "Статья успешно удалена";

                return successMessage.ToOkManagerResult();
            }

            catch (Exception e)
            {
               logger.Error(e, "Произошла ошибка при удалении статьи");

               return PagedListExtensions.ToPreconditionFailedManagerResult<string>(
                   "Произошла ошибка при удалении статьи");
            }
        }
    }
}
