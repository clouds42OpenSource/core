﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArticleContext.Dtos;
using Core42.Application.Features.ArticleContext.Queries;
using Core42.Application.Features.ArticleContext.Queries.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Handlers;

public class GetFilteredArticlesQueryHandler(ILogger42 logger, IUnitOfWork unitOfWork, IMapper mapper)
    : IRequestHandler<GetFilteredArticlesQuery, ManagerResult<PagedDto<ArticleDto>>>
{
    public async Task<ManagerResult<PagedDto<ArticleDto>>> Handle(GetFilteredArticlesQuery request, CancellationToken cancellationToken)
    {
        try
        {
            return (await unitOfWork.ArticleRepository
                    .AsQueryableNoTracking()
                    .ProjectTo<ArticleDto>(mapper.ConfigurationProvider)
                    .AutoFilter(request.Filter)
                    .Where(ArticleSpecification.ByDate(request.Filter?.CreatedFrom, request.Filter?.CreatedTo))
                    .AutoSort(request)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            logger.Error(ex, "Ошибка при получении данных по статьям");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<ArticleDto>>(
                "Произошла ошибка при получении информации по статьям.");
        }
    }
}
