﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArticleContext.Commands;
using Core42.Application.Features.ArticleContext.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Handlers;

public class CreateArticleCommandHandler(
    ILogger42 logger,
    IUnitOfWork unitOfWork,
    IAccessProvider accessProvider,
    IMapper mapper)
    : IRequestHandler<CreateArticleCommand, ManagerResult<ArticleDto>>
{
    public async Task<ManagerResult<ArticleDto>> Handle(CreateArticleCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await accessProvider.GetUserAsync();

            var article = new Article
            {
                Topic = request.Topic,
                TopicId = request.TopicId,
                AccountUserId = user.Id,
                Status = ArticleStatus.Draft,
                CreatedOn = DateTime.Now,
                Type = request.Type,
                WpId = request.WpId,
                RegistrationCount = 0,
                RewardAmount = 0
            };
            
            unitOfWork.ArticleRepository.Insert(article);
            
            await unitOfWork.SaveAsync();

            LogEventHelper.LogEvent(unitOfWork, user.ContextAccountId, accessProvider, LogActions.CreateArticle, $"Создана статья {article.Topic}, пользователем {user.Email}");

            return mapper.Map<Article, ArticleDto>(article).ToOkManagerResult();
        }

        catch (Exception ex)
        {
            logger.Error(ex, "Ошибка при создании статьи");

            return PagedListExtensions.ToPreconditionFailedManagerResult<ArticleDto>(
                "Произошла ошибка при попытке создать статью");
        }
    }
}
