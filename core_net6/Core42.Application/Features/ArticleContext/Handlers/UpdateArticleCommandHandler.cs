﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArticleContext.Commands;
using Core42.Application.Features.ArticleContext.Dtos;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleContext.Handlers;

public class UpdateArticleCommandHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<UpdateArticleCommand, ManagerResult<ArticleExtendedDto>>
{
    public async Task<ManagerResult<ArticleExtendedDto>> Handle(UpdateArticleCommand request, CancellationToken cancellationToken)
    {
        try
        {
            var user = await accessProvider.GetUserAsync();

            var article = await unitOfWork.ArticleRepository
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (article == null)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<ArticleExtendedDto>($"Не удалось найти статью по идентификатору {request.Id}");
            }

            article.Topic = request.Topic;
            article.Title = request.Title;
            article.Text = request.Text;
            article.WpLink = request.WpLink;
            article.WpId = request.WpId;
            article.RegistrationCount = request.RegistrationCount == null ? 0: article.RegistrationCount;
            unitOfWork.ArticleRepository.Update(article);

            await unitOfWork.SaveAsync();

            LogEventHelper.LogEvent(unitOfWork, user.ContextAccountId, accessProvider, LogActions.EditArticle, $"Изменена статья {article.Topic}, пользователем {user.Email}");

            return mapper.Map<ArticleExtendedDto>(article).ToOkManagerResult();
        }
        catch (Exception e)
        {
            logger.Error(e, "Ошибка при редактировании статьи");

            return PagedListExtensions.ToPreconditionFailedManagerResult<ArticleExtendedDto>("Произошла ошибка при редактировании статьи");
        }
    }
}
