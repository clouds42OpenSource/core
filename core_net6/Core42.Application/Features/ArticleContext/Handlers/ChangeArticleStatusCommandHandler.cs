﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Core42.Application.Features.ArticleContext.Commands;
using Core42.Application.Features.ArticleContext.Dtos;
using Core42.Application.Features.EmailContext.Commands;
using Core42.Application.Features.NotificationContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Core42.Application.Features.ArticleContext.Handlers;

public class ChangeArticleStatusCommandHandler(
    ILogger42 logger,
    IUnitOfWork unitOfWork,
    IMapper mapper,
    ISender sender,
    IAccessProvider accessProvider)
    : IRequestHandler<ChangeArticleStatusCommand, ManagerResult<ArticleDto>>
{
    private readonly decimal CountRewardPayment = 3000; //вынести в конфиг сумму

    public async Task<ManagerResult<ArticleDto>> Handle(ChangeArticleStatusCommand request, CancellationToken cancellationToken)
    {
        string usernamePassword  = "user=admin&password=Admin123";

        try
        {
            var user = await accessProvider.GetUserAsync();

            var article = await unitOfWork.ArticleRepository
                .AsQueryable()
                .Include(x => x.AccountUser)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            
            if (article == null)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<ArticleDto>(
                    $"Не удалось найти статью по идентификатору {request.Id}");
            }

            var articleStatus = article.Status;

            article.Status = request.Status;
            article.Reason = request.Reason;
            article.PublicationDate = request.Status == ArticleStatus.Published ? DateTime.Now : null;

            if (request.Status == ArticleStatus.Published)
            {
                var articleTransaction = await unitOfWork.ArticleTransactionRepository
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.ArticleId == article.Id, cancellationToken);

                if (articleTransaction == null)
                {
                    logger.Debug($"Начисление вознаграждения первый раз за публикацию статьи {article.Id} автору {article.AccountUser.Name}");
                    articleTransaction = new ArticleTransaction
                    {
                        Id = Guid.NewGuid(),
                        ArticleId = article.Id,
                        Cause = ArticleTransactionCause.ArticlePublication,
                        TransactionType = ArticleTransactionType.Inflow,
                        Amount = CountRewardPayment,
                        CreatedOn = DateTime.Now
                    };
                    unitOfWork.ArticleTransactionRepository.Insert(articleTransaction);

                    var articleWallets = await unitOfWork.ArticleWalletsRepository
                        .AsQueryable()
                        .Include(x => x.AccountUser)
                        .FirstOrDefaultAsync(x => x.AccountUserId == article.AccountUserId, cancellationToken);

                    if (articleWallets == null)
                    {
                        articleWallets = new ArticleWallets
                        {
                            AccountUserId = article.AccountUserId,
                            AvailableSum = articleTransaction.Amount,
                            TotalEarned = articleTransaction.Amount
                        };
                        unitOfWork.ArticleWalletsRepository.Insert(articleWallets);
                    }
                    else
                    {
                        articleWallets.AvailableSum = articleWallets.AvailableSum + articleTransaction.Amount;
                        articleWallets.TotalEarned = articleWallets.TotalEarned + articleTransaction.Amount;
                        unitOfWork.ArticleWalletsRepository.Update(articleWallets);
                    }

                    LogEventHelper.LogEvent(unitOfWork, user.ContextAccountId, accessProvider, LogActions.RewardArticle, $"Начислено вознаграждение за публикацию статьи номер {article.Id} заголовок статьи {article.Title} на сумму {articleTransaction.Amount}");
                    article.RewardAmount = CountRewardPayment;
                }
            }
            logger.Debug($"Вознаграждение добавлено ");
            var articleTypeUrl = article.Type == ArticleType.ForSite ? "/for-site" : "/for-blog";

            unitOfWork.ArticleRepository.Update(article);

            await unitOfWork.SaveAsync();

            if (article.Status != ArticleStatus.ForRevision && request.Status == ArticleStatus.ForRevision)
            {
                await sender.Send(new SendEmailCommand
                {
                    To = CloudConfigurationProvider.Emails.Get42CloudsManagerEmail(),
                    Body = $"Пользователем '{article.AccountUser.Login}' создана статья '{article.Topic}', ссылка - {CloudConfigurationProvider.Article.GetLinkForArticle()}{articleTypeUrl}/{article.Id}. Необходимо провести рецензию",
                    Categories = ["articles"],
                    Header = "Уведомление о рецензии",
                    Subject = "Создана новая статья",
                    SendGridTemplateId = CloudConfigurationProvider.SendGrid.GetDefaultTemplateId()
                }, cancellationToken);
            }

            else
            {
                await sender.Send(new CreateNotificationCommand
                {
                    Message = $"У вашей статьи '{article.Topic}' с идентификатором '{article.Id}' изменился статус, проверить можно на вкладке 'Статьи'",
                    UserIds = [article.AccountUserId]
                }, cancellationToken);

                await sender.Send(new SendEmailCommand
                {
                    To = article.AccountUser.Email,
                    Body = $"Облачное приветствие! У вашей статьи '{article.Topic}' с идентификатором '{article.Id}' изменился статус, перейдите по ссылке для просмотра {CloudConfigurationProvider.Article.GetLinkForArticle()}",
                    Categories = ["articles"],
                    Header = "Изменение статуса статьи",
                    Subject = $"Изменен статус статьи {article.Id}",
                    SendGridTemplateId = CloudConfigurationProvider.SendGrid.GetDefaultTemplateId()
                }, cancellationToken);
            }

            LogEventHelper.LogEvent(unitOfWork, user.ContextAccountId, accessProvider, LogActions.ChangeArticleStatus, $"Изменен статус статьи с {articleStatus.Description()} на {request.Status.Description()}, пользователем {user.Email}");

            return mapper.Map<ArticleDto>(article).ToOkManagerResult();
        }

        catch (Exception e)
        {
            logger.Error(e, "При изменении статуса статьи произошла ошибка");

            return PagedListExtensions.ToPreconditionFailedManagerResult<ArticleDto>("При изменении статуса статьи произошла ошибка");
        }
    }



}
