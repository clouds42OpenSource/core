﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ArticleContext.Dtos;

public class ArticleExtendedDto : IMapFrom<Article>
{
    public int Id { get; set; }
    public string Topic { get; set; }
    public string TopicId { get; set; }
    public string Title { get; set; }
    public string Text { get; set; }
    public DateTime CreatedOn { get; set; }
    public ArticleStatus Status { get; set; }
    public DateTime? PublicationDate { get; set; }
    public ArticleType Type { get; set; }
    public string Reason { get; set; }
    public string GoogleCloudLink { get; set; }
    public string WpLink { get; set; }
    public Guid AccountUserId { get; set; }
    public int RegistrationCount { get; set; }
    public string WpId { get; set; }
    public decimal RewardAmount { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<Article, ArticleExtendedDto>();
    }
}
