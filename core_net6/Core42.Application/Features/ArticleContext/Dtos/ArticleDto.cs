﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ArticleContext.Dtos;

public class ArticleDto : IMapFrom<Article>
{
    public int Id { get; set; }
    public string Topic { get; set; }
    public string TopicId { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime? PublicationDate { get; set; }
    public ArticleStatus Status { get; set; }
    public ArticleType Type { get; set; }
    public string GoogleCloudLink { get; set; }
    public string WpLink { get; set; }
    public string WpId { get; set; }
    public Guid AccountUserId { get; set; }
    public string AccountUserLogin { get; set; }
    public int RegistrationCount { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<Article, ArticleDto>()
            .ForMember(x => x.AccountUserLogin, z => z.MapFrom(y => y.AccountUser.Login));
    }
}
