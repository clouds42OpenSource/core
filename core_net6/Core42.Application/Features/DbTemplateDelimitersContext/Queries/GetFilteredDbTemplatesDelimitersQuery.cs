﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.DbTemplateDelimitersContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DbTemplateDelimitersContext.Queries
{
    /// <summary>
    /// Get filtered db templates delimiters query
    /// </summary>
    public class GetFilteredDbTemplatesDelimitersQuery : IRequest<ManagerResult<PagedDto<DbTemplateDelimiterDto>>>, ISortedQuery, IPagedQuery
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(DbTemplateDelimiters.Name)}.asc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }
    }
}
