﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.DbTemplateDelimitersContext.Dtos;
using MediatR;

namespace Core42.Application.Features.DbTemplateDelimitersContext.Queries
{
    /// <summary>
    /// Get db template delimiters as key-value query
    /// </summary>
    public class GetDbTemplateDelimitersAsKeyValueQuery : IRequest<ManagerResult<List<DbTemplateDelimiterDictionaryDto>>>
    {
    }
}
