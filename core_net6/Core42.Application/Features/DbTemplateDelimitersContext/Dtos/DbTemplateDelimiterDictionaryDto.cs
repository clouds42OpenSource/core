﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.DbTemplateDelimitersContext.Dtos
{
    /// <summary>
    /// Db template delimiters as key-value dto
    /// </summary>
    public class DbTemplateDelimiterDictionaryDto : DictionaryDto<string, string>, IMapFrom<DbTemplateDelimiters>
    {
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DbTemplateDelimiters, DbTemplateDelimiterDictionaryDto>()
                .ForMember(x => x.Key, z => z.MapFrom(y => y.ConfigurationId))
                .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));
        }
    }
}
