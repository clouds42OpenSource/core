﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.DbTemplateDelimitersContext.Dtos
{
    /// <summary>
    /// Db template delimiter dto
    /// </summary>
    public class DbTemplateDelimiterDto : IMapFrom<DbTemplateDelimiters>
    {
        /// <summary>
        /// Configuration identifier
        /// </summary>
        public string ConfigurationId { get; set; }

        /// <summary>
        /// Configuration name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Template identifier
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Configuration short name (for zip upload)
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Demo publication
        /// </summary>
        public string DemoDatabaseOnDelimitersPublicationAddress { get; set; }

        /// <summary>
        /// Current configuration release version
        /// </summary>
        public string ConfigurationReleaseVersion { get; set; }

        /// <summary>
        /// Minimal release version for update
        /// </summary>
        public string MinReleaseVersion { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DbTemplateDelimiters, DbTemplateDelimiterDto>();
        }
    }
}
