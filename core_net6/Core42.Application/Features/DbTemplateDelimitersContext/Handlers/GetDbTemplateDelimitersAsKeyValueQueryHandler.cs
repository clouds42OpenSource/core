﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.DbTemplateDelimitersContext.Dtos;
using Core42.Application.Features.DbTemplateDelimitersContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DbTemplateDelimitersContext.Handlers
{
    /// <summary>
    /// Get db template delimiters as key-value query handler
    /// </summary>
    public class GetDbTemplateDelimitersAsKeyValueQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetDbTemplateDelimitersAsKeyValueQuery, ManagerResult<List<DbTemplateDelimiterDictionaryDto>>>
    {
        /// <summary>
        /// Handle get db template delimiters as key-value query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<DbTemplateDelimiterDictionaryDto>>> Handle(GetDbTemplateDelimitersAsKeyValueQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ViewDbTemplateDelimitersReferences, () => accessProvider.ContextAccountId);

                return (await unitOfWork.DbTemplateDelimitersReferencesRepository
                     .AsQueryable()
                     .ProjectTo<DbTemplateDelimiterDictionaryDto>(mapper.ConfigurationProvider)
                     .ToListAsync(cancellationToken))
                     .ToOkManagerResult();


            }

            catch (Exception exception)
            {
                handlerException.Handle(exception, "[Ошибка получения шаблонов баз на разделителях]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<DbTemplateDelimiterDictionaryDto>>(exception.Message);
            }
        }
    }
}
