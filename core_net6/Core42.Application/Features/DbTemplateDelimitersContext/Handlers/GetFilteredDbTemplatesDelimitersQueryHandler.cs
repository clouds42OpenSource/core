﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using PagedListExtensionsNetFramework;
using LinqExtensionsNetFramework;
using Core42.Application.Features.DbTemplateDelimitersContext.Dtos;
using Core42.Application.Features.DbTemplateDelimitersContext.Queries;

namespace Core42.Application.Features.DbTemplateDelimitersContext.Handlers
{
    /// <summary>
    /// Get filtered db templates delimiters query handler
    /// </summary>
    public class GetFilteredDbTemplatesDelimitersQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredDbTemplatesDelimitersQuery, ManagerResult<PagedDto<DbTemplateDelimiterDto>>>
    {
        /// <summary>
        /// Handle get filtered db templates delimiters query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<DbTemplateDelimiterDto>>> Handle(GetFilteredDbTemplatesDelimitersQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ViewDbTemplateDelimitersReferences, () => accessProvider.ContextAccountId);

                return (await unitOfWork.DbTemplateDelimitersReferencesRepository
                    .AsQueryable()
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<DbTemplateDelimiterDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception exception)
            {
                handlerException.Handle(exception, "[Ошибка получения шаблонов баз на разделителях]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<DbTemplateDelimiterDto>>(exception.Message);
            }
        }
    }
}
