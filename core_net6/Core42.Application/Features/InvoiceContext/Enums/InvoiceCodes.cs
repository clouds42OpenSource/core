﻿namespace Core42.Application.Features.InvoiceContext.Enums;

public enum InvoiceCodes
{
    Ok = 200,
    NotFound = 404,
    AmountDoesNotMatch = 412,
    ServerError = 500,
    UnknownBehavior = 999
}
