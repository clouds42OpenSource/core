﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing;
using Core42.Application.Features.InvoiceContext.Dtos;
using Core42.Application.Features.InvoiceContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.InvoiceContext.Queries
{
    public class GetAllInvoicesQuery : IRequest<ManagerResult<List<InvoiceDto>>>, ISortedQuery
    {
        public InvoicesFilter? Filter { get; set; }
        public string OrderBy { get; set; } = $"{nameof(Invoice.Date)}.desc";
    }
}
