﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.InvoiceContext.Dtos;
using MediatR;

namespace Core42.Application.Features.InvoiceContext.Queries
{
    public class GetInvoiceByFilterQuery : IRequest<ManagerResult<InvoiceDto>>
    {
        public Guid AccountId { get; set; }
        public long Uniq { get; set; }
        public decimal Sum { get; set; }
    }
}
