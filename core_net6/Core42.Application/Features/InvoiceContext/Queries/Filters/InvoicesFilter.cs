﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.Queries.Filters;

public class InvoicesFilter : IQueryFilter
{
    public DateTime? NowDate { get; set; }
    public DateTime? PreviousDate { get; set; }
    public List<string>? PaymentSystems { get; set; }
}
