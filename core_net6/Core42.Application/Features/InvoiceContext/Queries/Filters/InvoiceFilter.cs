﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.Queries.Filters;

public class InvoiceFilter : IQueryFilter
{
    public long? Uniq { get; set; }
    public string? Requisite { get; set; }
}
