﻿using AutoMapper;
using Clouds42.Domain.DataModels.billing;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.InvoiceContext.Dtos;

public class InvoiceDto : IMapFrom<Invoice>
{
    public long Uniq { get; set; }
    public string Nomenclature { get; set; }
    public string ServiceContent { get; set; }
    public string Tin { get; set; }
    public Guid AccountId { get; set; }
    public decimal Sum { get; set; }
    public string? AccountAdminEmail { get; set; }
    public string? AccountAdminPhone { get; set; }
    public string? SaleManagerPhone { get; set; }
    public string? PaymentSystem { get; set; }
    
    [JsonIgnore]
    public List<InvoiceProduct> InvoiceProducts { get; set; }
    [JsonIgnore]
    public int? Period { get; set; }
    [JsonIgnore]
    public string LocaleName { get; set; }
    [JsonIgnore]
    public string State { get; set; }

    public DateTime? Date { get; set; }

    public DateTime? PaymentDate { get; set; }
    public string SupplierCode { get; set; }
    public void Mapping(Profile profile)
    {
        profile.CreateMap<Invoice, InvoiceDto>()
            .ForMember(x => x.Uniq, z => z.MapFrom(y => y.Uniq))
            .ForMember(x => x.Tin, z => z.MapFrom(y => y.Requisite))
            .ForMember(x => x.AccountId, z => z.MapFrom(y => y.AccountId))
            .ForMember(x => x.SaleManagerPhone, z => z.MapFrom(y => y.Account.AccountSaleManager.AccountUser.PhoneNumber))
            .ForMember(x => x.Sum, z => z.MapFrom(y => y.Sum))
            .ForMember(x => x.State, z => z.MapFrom(y => y.State))
            .ForMember(x => x.Period, z => z.MapFrom(y => y.Period))
            .ForMember(x => x.Date, z => z.MapFrom(y => y.Date))
            .ForMember(x => x.LocaleName, z => z.MapFrom(y => y.Account.AccountConfiguration.Locale.Name))
            .ForMember(x => x.SupplierCode, z => z.MapFrom(y => y.Account.AccountConfiguration.Supplier.Code))
            .ForMember(x => x.PaymentSystem, z => z.MapFrom(y => y.Payment.PaymentSystem))
            .ForMember(x => x.PaymentDate, z => z.MapFrom(y => y.Payment.Date))
            .ForMember(x => x.InvoiceProducts, z => z.MapFrom(y => y.InvoiceProducts));
    }
}
