﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.InvoiceContext.ActContext.Dtos;
using MediatR;

namespace Core42.Application.Features.InvoiceContext.ActContext.Queries;

public class GetSignedActsQuery : IRequest<ManagerResult<List<InvoiceCorpDto>>>
{
}
