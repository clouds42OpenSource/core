﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.ActContext.Queries.Filters;

public class ClosingDocumentFilter : IQueryFilter
{
    public Guid[] InvoiceIds { get; set; }
}
