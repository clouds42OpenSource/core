﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing.ClosingDocuments;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.InvoiceContext.ActContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.ActContext.Handlers;

public class ChangeClosingDocumentStatusesCommandHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    InvoiceActManager invoiceActManager,
    IHandlerException handlerException)
    : IRequestHandler<ChangeClosingDocumentStatusesCommand, ManagerResult<bool>>
{
    public async Task<ManagerResult<bool>> Handle(ChangeClosingDocumentStatusesCommand request,
        CancellationToken cancellationToken)
    {
        try
        {
            foreach (var statusChange in request.InvoiceStatuses)
            {
                var invoice = await unitOfWork.InvoiceRepository.FirstOrDefaultAsync(i => i.Id == statusChange.InvoiceId) ??
                             throw new NotFoundException($"По номеру {statusChange.InvoiceId} не найден счет на оплату.");
                logger.Info(
                    $"Изменение статуса счета. Идентификатор счета: {invoice.Id}, Старый статус: {invoice.Status}, Новый статус: {statusChange.Status}");
                if (statusChange.Status != Status.Accepted && statusChange.Status != Status.Processed)
                {
                    await invoiceActManager.DeleteActFileAsync(invoice.ActID);
                    logger.Info($"Удален акт с ID {invoice.ActID} для счета с ID {invoice.Id}.");
                }

                invoice.Status = statusChange.Status;
                invoice.RequiredSignature = statusChange.Status != Status.Accepted && statusChange.Status != Status.Processed;
                unitOfWork.InvoiceRepository.Update(invoice);

            }
            await unitOfWork.SaveAsync();
            return true.ToOkManagerResult();
        }
        catch (Exception ex)
        {
            var message = $"Ошибка при массовом изменении статусов закрывающих документов + {ex.Message}";
            handlerException.Handle(ex, message);
            return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(message);
        }
    }
}
