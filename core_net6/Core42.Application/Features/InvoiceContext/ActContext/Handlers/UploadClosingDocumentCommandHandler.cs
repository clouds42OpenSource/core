﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing.ClosingDocuments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.EmailContext.Commands;
using Core42.Application.Features.InvoiceContext.ActContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.ActContext.Handlers;

public class UploadClosingDocumentCommandHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IHandlerException handlerException,
    IAccessProvider accessProvider,
    IAccountSaleManagerProvider accountSaleManagerProvider,
    ISender sender)
    : IRequestHandler<UploadClosingDocumentCommand, ManagerResult<bool>>
{
    private readonly IUnitOfWork _unitOfWork = unitOfWork;


    public async Task<ManagerResult<bool>> Handle(UploadClosingDocumentCommand request,
        CancellationToken cancellationToken)
    {
        try
        {
            var invoiceDto = request.Invoice;

            var actId = invoiceDto.ActId;
            using var memoryStream = new MemoryStream();
            await invoiceDto.File.CopyToAsync(memoryStream, cancellationToken);
            var fileBytes = memoryStream.ToArray();

            logger.Info($"Файл для акта {actId} загружен и конвертирован в массив байт.");
            var file = await _unitOfWork.CloudFileRepository.FirstOrDefaultAsync(f => f.Id == actId);
            
            if (file == null)
            {
                file = new CloudFile
                {
                    Id = actId,
                    FileName = $"Act_{actId}.pdf",
                    ContentType = "application/pdf",
                    Content = fileBytes
                }; 
                _unitOfWork.CloudFileRepository.Insert(file);
            }
            else
            {
                file.Content = fileBytes;
            }
            
            var invoice = await _unitOfWork.InvoiceRepository.FirstOrDefaultAsync(i => i.Id == request.Invoice.InvoiceId) ??
                          throw new NotFoundException($"По номеру {request.Invoice.InvoiceId} не найден счет на оплату.");
            invoice.Status = Status.Processing;
            invoice.RequiredSignature = false;
            _unitOfWork.InvoiceRepository.Update(invoice);
            
            await _unitOfWork.SaveAsync();
            logger.Info($"Счет на оплату с ID {request.Invoice.InvoiceId} обновлен до статуса {Status.Processing}.");
            AccountSaleManagerInfoDto?
                saleManager = accountSaleManagerProvider.GetAccountSaleManagerInfo(request.AccountId);
           await SendEmailWithFileAndButtons(fileBytes,saleManager.Email, file.FileName, request, cancellationToken);
            LogEventHelper.LogEvent(_unitOfWork, invoice.AccountId, accessProvider,LogActions.SignedActUploaded, $"Загружен подписанный акт для инвойса {invoice.Id}" );
            return true.ToOkManagerResult();
        }
        catch (Exception e)
        {
            var message = "Ошибка при загрузке закрывающего документа";
            handlerException.Handle(e, message);
            return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(message);
        }
    }
    
    /// <summary>
    /// Отправить письмо менеджеру с прикрепленным файлом и кнопками
    /// </summary>
    private async Task SendEmailWithFileAndButtons(byte[] file,
        string emailTo,
        string fileName,
        UploadClosingDocumentCommand request ,
        CancellationToken cancellationToken)
    {
        logger.Info($"Отправка сообщения о загруженном акте на почту {emailTo}");

        string baseUri = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
    
        var approveUrl = $"{baseUri}/closing-documents/change-status?invoiceId={request.Invoice.InvoiceId}&status={(int)Status.Accepted}"; 
        var rejectUrl = $"{baseUri}/closing-documents/change-status?invoiceId={request.Invoice.InvoiceId}&status={(int)Status.Rejected}";

        var emailContent = $@"<p style=""color: #333;"">Уважаемый менеджер,</p>
    <p style=""color: #444;"">Пожалуйста, ознакомьтесь с закрывающим документом инвойса {request.Invoice.InvoiceId} </p>
    <p style=""margin: 20px 0;"">
        <a href=""{approveUrl}"" style=""background-color: green; color: white; padding: 10px 20px; text-decoration: none; border-radius: 5px; display: inline-block; margin-right: 10px;"">Утвердить</a>
        <a href=""{rejectUrl}"" style=""background-color: red; color: white; padding: 10px 20px; text-decoration: none; border-radius: 5px; display: inline-block;"">Отклонить</a>
    </p>
    <p style=""color: #666; font-size: 0.9em;"">С уважением, команда 42Clouds.</p>";
        
        await sender.Send(
            new SendEmailCommand
            {
                Body = emailContent,
                Header = "Рассмотрение акта",
                Subject = "Рассмотрение акта",
                Locale = "ru-ru",
                To = emailTo,
                Attachments = new List<DocumentDataDto> {new() {FileName = fileName, Bytes = file}},
                SendGridTemplateId = CloudConfigurationProvider.SendGrid.GetDefaultTemplateId()
            }, cancellationToken);
    }
}


