﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing.ClosingDocuments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.InvoiceContext.ActContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.ActContext.Handlers;

public class ChangeClosingDocumentStatusCommandHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    InvoiceActManager invoiceActManager,
    IHandlerException handlerException,
    IAccessProvider accessProvider)
    : IRequestHandler<ChangeClosingDocumentStatusCommand, ManagerResult<bool>>
{
    public async Task<ManagerResult<bool>> Handle(ChangeClosingDocumentStatusCommand request,
            CancellationToken cancellationToken)
        {
            try
            {
                logger.Info($"Обновляем статус акта для инвойса {request.InvoiceStatus.InvoiceId}");
                var status = request.InvoiceStatus.Status;
                var invoice = await unitOfWork.InvoiceRepository.FirstOrDefaultAsync(i => i.Id == request.InvoiceStatus.InvoiceId) ??
                              throw new NotFoundException($"По номеру {request.InvoiceStatus.InvoiceId} не найден счет на оплату.");
                var condition = (status != Status.Accepted);
                if (condition)
                {
                    if (invoice.ActID != null)
                    {
                        await invoiceActManager.DeleteActFileAsync(invoice.ActID);
                        logger.Info($"Удален акт с ID {invoice.ActID} для счета с ID {invoice.Id}.");
                    }

                }
                invoice.Status = status;
                invoice.RequiredSignature = condition && status != Status.Processed;
                unitOfWork.InvoiceRepository.Update(invoice);
            
                await unitOfWork.SaveAsync();
                LogEventHelper.LogEvent(unitOfWork, invoice.AccountId ,accessProvider,LogActions.ActStatusUpdated, $"Статус акта {invoice.ActID} изменен на {invoice.Status}" );
                return true.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                var message = "Ошибка при изменении статуса закрывающего документа";
                handlerException.Handle(ex, message);
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(message);
            }
        }
}
