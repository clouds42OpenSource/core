﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing.ClosingDocuments;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.InvoiceContext.ActContext.Dtos;
using Core42.Application.Features.InvoiceContext.ActContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.ActContext.Handlers;

public class GetSignedActsQueryHandler(
    IUnitOfWork unitOfWork,
    InvoiceActManager invoiceActManager,
    IHandlerException handlerException)
    : IRequestHandler<GetSignedActsQuery, ManagerResult<List<InvoiceCorpDto>>>
{
    public async Task<ManagerResult<List<InvoiceCorpDto>>> Handle(GetSignedActsQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var acceptedInvoicesActs = await unitOfWork.InvoiceRepository
                .AsQueryableNoTracking()
                .Where(i => i.Status == Status.Accepted)
                .ToListAsync(cancellationToken);

            var result = new List<InvoiceCorpDto>();

            foreach (var invoice in acceptedInvoicesActs)
            {
                var actFile = await invoiceActManager.GetActFileAsync(invoice.ActID);
                result.Add(new InvoiceCorpDto
                {
                    ActId = invoice.ActID,
                    BillingNumber = invoice.Uniq,
                    InvoiceId = invoice.Id,
                    File = actFile?.Content
                });
            }
            return result.ToList().ToOkManagerResult();
        }
        catch (Exception ex)
        {
            var message = $"Ошибка при получении подписанных актов + {ex.Message}";
            handlerException.Handle(ex, message);
            return PagedListExtensions.ToPreconditionFailedManagerResult<List<InvoiceCorpDto>>(message);
        }
    }
}
