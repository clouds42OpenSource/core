﻿namespace Core42.Application.Features.InvoiceContext.ActContext.Dtos;

public class InvoiceDtoFromCorp
{
    public Guid InvoiceId { get; set; }
    public Guid ActId { get; set; }
    public bool Status { get; set; }
    public string Error { get; set; }
}
    
