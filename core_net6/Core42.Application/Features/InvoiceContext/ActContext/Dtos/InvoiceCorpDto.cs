﻿namespace Core42.Application.Features.InvoiceContext.ActContext.Dtos;

public class InvoiceCorpDto 
{
    public Guid InvoiceId { get; set; }
    public Guid? ActId { get; set; }
    public long BillingNumber { get; set; }
    public byte[]? File { get; set; }
    
}
