﻿using Microsoft.AspNetCore.Http;

namespace Core42.Application.Features.InvoiceContext.ActContext.Dtos;

public class InvoiceDto
{
    public Guid InvoiceId { get; set; }
    public Guid ActId { get; set; }
    public IFormFile File { get; set; }
    
}
