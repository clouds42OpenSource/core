﻿using Clouds42.Domain.DataModels.billing.ClosingDocuments;

namespace Core42.Application.Features.InvoiceContext.ActContext.Dtos;

public class InvoiceStatusDto
{
    public Guid InvoiceId { get; set; }
    public Status Status { get; set; }
}
