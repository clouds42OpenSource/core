﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.InvoiceContext.ActContext.Dtos;
using MediatR;

namespace Core42.Application.Features.InvoiceContext.ActContext.Commands;
/// <summary>
/// Команда для загрузки документа закрытия счета.
/// </summary>
public class UploadClosingDocumentCommand  : IRequest<ManagerResult<bool>>
{

    public Guid AccountId { get; set; }
    
    /// <summary>
    /// Инвойс, связанный с документом закрытия.
    /// </summary>
    public InvoiceDto  Invoice { get; set; }
}
