﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.InvoiceContext.ActContext.Dtos;
using MediatR;

namespace Core42.Application.Features.InvoiceContext.ActContext.Commands;
/// <summary>
/// команда для обновления одного акта
/// </summary>
public class ChangeClosingDocumentStatusCommand : IRequest<ManagerResult<bool>>
{
    public InvoiceStatusDto InvoiceStatus { get; set; }
}
