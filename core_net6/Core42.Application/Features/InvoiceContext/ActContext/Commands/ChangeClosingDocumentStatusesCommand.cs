﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.InvoiceContext.ActContext.Dtos;
using MediatR;

namespace Core42.Application.Features.InvoiceContext.ActContext.Commands;


/// <summary>
/// команда только для обновления нескольких актов 
/// </summary>
public class ChangeClosingDocumentStatusesCommand : IRequest<ManagerResult<bool>>
{
    public List<InvoiceStatusDto> InvoiceStatuses { get; set; }

}
