﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Billing.Invoice.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.InvoiceContext.Constants;
using Core42.Application.Features.InvoiceContext.Dtos;
using Core42.Application.Features.InvoiceContext.Enums;
using Core42.Application.Features.InvoiceContext.Queries;
using Core42.Application.Features.InvoiceContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.Handlers;

public class GetAllInvoicesQueryHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
    : IRequestHandler<GetAllInvoicesQuery, ManagerResult<List<InvoiceDto>>>
{
    public async Task<ManagerResult<List<InvoiceDto>>> Handle(GetAllInvoicesQuery request,
        CancellationToken cancellationToken)
    {
        try
        {
            var nowDate = DateTime.Now;
            var previousDate = nowDate.AddDays(-1);
            var paymentSystems = new List<string>
            {
                PaymentSystem.Yookassa.ToString(),
                PaymentSystem.Robokassa.ToString(),
                PaymentSystem.UkrPays.ToString(),
                PaymentSystem.Paybox.ToString()
            };

            if (request.Filter == null)
            {
                request.Filter = new InvoicesFilter
                {
                    PaymentSystems = paymentSystems,
                    NowDate = nowDate,
                    PreviousDate = previousDate
                };
            }

            else
            {
                request.Filter.NowDate ??= nowDate;
                request.Filter.PreviousDate ??= previousDate;
                request.Filter.PaymentSystems ??= paymentSystems;
            }

            var invoices = await unitOfWork.InvoiceRepository
                .AsQueryable()
                .Where(x => x.State == InvoiceStatus.Processed.ToString() &&
                            x.Payment.Date >= request.Filter.PreviousDate && x.Payment.Date <= request.Filter.NowDate &&
                            x.PaymentID.HasValue && request.Filter.PaymentSystems.Contains(x.Payment.PaymentSystem) &&
                            x.Payment.Status == PaymentStatus.Done.ToString())
                .AutoSort(request)
                .ProjectTo<InvoiceDto>(mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            var prefix = CloudConfigurationProvider.Invoices.UniqPrefix();
            var supplierYo = await unitOfWork.SupplierRepository.AsQueryable()
                    .FirstOrDefaultAsync(x => x.Name == "ООО \"УчетОнлайн\"", cancellationToken);
            foreach (var invoice in invoices)
            {
                var description = InvoiceAdditionalDataHelper.GetProductsDescription(invoice.InvoiceProducts.ToList(),
                    invoice.Period, invoice.LocaleName);

                invoice.Nomenclature = InvoiceConstants.Nomenclature;
                invoice.ServiceContent = !string.IsNullOrEmpty(description) ? description : "-";
                invoice.Uniq = long.Parse(prefix + invoice.Uniq);
                               
                invoice.SupplierCode = invoice.InvoiceProducts.Any(x => x.ServiceName == "AlpacaMeet") ? supplierYo.Code : invoice.SupplierCode;

                var accountUser = await unitOfWork.AccountUsersRepository.AsQueryable()
                    .FirstOrDefaultAsync(x => x.AccountId == invoice.AccountId, cancellationToken);

                if (accountUser == null)
                {
                    continue;
                }
                
                var accountAdmin = (await unitOfWork.AccountsRepository
                        .AsQueryable()
                        .Include(x => x.AccountUsers)
                        .ThenInclude(x => x.AccountUserRoles)
                        .FirstOrDefaultAsync(x => x.AccountUsers
                            .Any(z => z.Id == accountUser.Id), cancellationToken))?.AccountUsers?
                    .FirstOrDefault(x => x.AccountUserRoles
                        .Any(y => y.AccountUserGroup is AccountUserGroup.AccountAdmin or AccountUserGroup.CloudAdmin));

                invoice.AccountAdminEmail = accountAdmin?.Email;
                invoice.AccountAdminPhone = accountAdmin?.PhoneNumber;

            }

            return invoices.ToOkManagerResult((int)InvoiceCodes.Ok);
        }

        catch (Exception ex)
        {
            logger.Error(ex, "При получении списка платежей возникла ошибка");

            return PagedListExtensions.ToPreconditionFailedManagerResult<List<InvoiceDto>>(ex.Message,
                (int)InvoiceCodes.ServerError);
        }
    }
}
