﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Billing.Invoice.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.InvoiceContext.Constants;
using Core42.Application.Features.InvoiceContext.Dtos;
using Core42.Application.Features.InvoiceContext.Enums;
using Core42.Application.Features.InvoiceContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.InvoiceContext.Handlers;

public class GetInvoiceByFilterQueryHandler(IUnitOfWork unitOfWork, ILogger42 logger, IMapper mapper)
    : IRequestHandler<GetInvoiceByFilterQuery, ManagerResult<InvoiceDto>>
{
    public async Task<ManagerResult<InvoiceDto>> Handle(GetInvoiceByFilterQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var statuses = new List<string>
            {
                InvoiceStatus.Processing.ToString(), InvoiceStatus.Processed.ToString()
            };

            var prefix = CloudConfigurationProvider.Invoices.UniqPrefix();
            request.Uniq = long.Parse(request.Uniq.ToString()[prefix.Length..]);

            var invoice = await unitOfWork.InvoiceRepository
                .AsQueryable()
                .ProjectTo<InvoiceDto>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Uniq == request.Uniq && x.AccountId == request.AccountId,
                    cancellationToken);

            var supplierYo = await unitOfWork.SupplierRepository.AsQueryable()
                .FirstOrDefaultAsync(x => x.Name == "ООО \"УчетОнлайн\"", cancellationToken);

            if (invoice == null)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<InvoiceDto>(
                    $"Не найден инвойс по uniq = {request.Uniq} и accountId = {request.AccountId}",
                    (int)InvoiceCodes.NotFound);
            }

            var description = InvoiceAdditionalDataHelper.GetProductsDescription(invoice.InvoiceProducts.ToList(),
                invoice.Period, invoice.LocaleName);

            invoice.Nomenclature = InvoiceConstants.Nomenclature;
            invoice.ServiceContent = !string.IsNullOrEmpty(description) ? description : "-";
            invoice.Uniq = long.Parse(prefix + invoice.Uniq);
            invoice.SupplierCode = invoice.InvoiceProducts.Any(x => x.ServiceName == "AlpacaMeet") ? supplierYo.Code : invoice.SupplierCode;

            if (invoice.Sum == request.Sum && statuses.Contains(invoice.State))
            {
                return invoice.ToOkManagerResult((int)InvoiceCodes.Ok);
            }

            if (invoice.Sum != request.Sum && statuses.Contains(invoice.State))
            {
                return invoice.ToOkManagerResult((int)InvoiceCodes.AmountDoesNotMatch,
                    $"Сумма инвойса {invoice.Sum} не соответствует присланной сумме {request.Sum}");
            }

            return PagedListExtensions.ToPreconditionFailedManagerResult<InvoiceDto>(
                "Найденный инвойс не прошел ни один из критериев отбора. Повторите попытку позже",
                (int)InvoiceCodes.UnknownBehavior);
        }

        catch (Exception ex)
        {
            logger.Error(ex, ex.Message);
            return PagedListExtensions.ToPreconditionFailedManagerResult<InvoiceDto>(ex.Message,
                (int)InvoiceCodes.ServerError);
        }
    }
}
