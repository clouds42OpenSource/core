﻿using AutoMapper;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.CloudChangesActionContext.Dtos
{
    /// <summary>
    /// Cloud changes action dto
    /// </summary>
    public class CloudChangesActionDto : KeyValueGroupDto<Guid, string>, IMapFrom<CloudChangesAction>
    {
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CloudChangesAction, CloudChangesActionDto>()
                .ForMember(z => z.Value, m => m.MapFrom(item => item.Name))
                .ForMember(z => z.Key, m => m.MapFrom(item => item.Id));
        }
    }
}
