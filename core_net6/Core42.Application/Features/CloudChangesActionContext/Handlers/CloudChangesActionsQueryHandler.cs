﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.CloudChangesActionContext.Dtos;
using Core42.Application.Features.CloudChangesActionContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.CloudChangesActionContext.Handlers
{
    /// <summary>
    /// Get Cloud changes actions query handler
    /// </summary>
    public class CloudChangesActionsQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<CloudChangesActionsQuery, ManagerResult<List<CloudChangesActionDto>>>
    {
        private const string AllActionsString = "Все действия";

        /// <summary>
        /// Handle get Cloud changes actions query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<CloudChangesActionDto>>> Handle(CloudChangesActionsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccessForMultiAccounts(ObjectAction.ControlPanel_ManagmentMenu_Logging);

                var hiddenActionIndexes = new List<int>
                {
                    (int)LogActions.ConnectToSponsoring,
                    (int)LogActions.DisconnectToSponsoring
                };

                var cloudChangesActions = new List<CloudChangesActionDto>
                    {
                        new() {Key = Guid.Empty, Value = AllActionsString, Group = string.Empty}
                    };

                var groups = await unitOfWork.CloudChangesGroupRepository
                    .AsQueryable()
                    .OrderBy(item => item.GroupName)
                    .ToListAsync(cancellationToken);

                var actions = await unitOfWork.CloudChangesActionRepository
                    .AsQueryable()
                    .Where(i => !hiddenActionIndexes.Contains(i.Index))
                    .ToListAsync(cancellationToken);

                foreach (var group in groups)
                {
                    var actionInGroup = actions.Where(x => x.GroupId == group.Id).ToArray();

                    cloudChangesActions.AddRange(actionInGroup.OrderBy(item => item.Name).Select(item =>
                    {
                        var mappedDto = mapper.Map<CloudChangesActionDto>(item);
                        mappedDto.Group = group.GroupName;

                        return mappedDto;
                    }));
                }

                return cloudChangesActions.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения списка логов]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<CloudChangesActionDto>>(ex.Message);
            }
        }
    }
}
