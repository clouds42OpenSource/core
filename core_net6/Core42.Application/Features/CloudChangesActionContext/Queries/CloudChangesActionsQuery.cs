﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.CloudChangesActionContext.Dtos;
using MediatR;

namespace Core42.Application.Features.CloudChangesActionContext.Queries
{
    /// <summary>
    /// Get clouds changes actions query
    /// </summary>
    public class CloudChangesActionsQuery : IRequest<ManagerResult<List<CloudChangesActionDto>>>
    {
    }
}
