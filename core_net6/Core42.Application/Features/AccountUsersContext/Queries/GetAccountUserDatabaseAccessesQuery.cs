﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using MediatR;

namespace Core42.Application.Features.AccountUsersContext.Queries
{
    public class GetAccountUserDatabaseAccessesQuery: IRequest<ManagerResult<UserCardAcDbAccessesDataDto>>
    {
        public Guid AccountUserId { get; set; }
    }
}
