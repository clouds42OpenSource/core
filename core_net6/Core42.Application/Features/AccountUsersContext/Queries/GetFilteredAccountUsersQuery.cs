﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.AccountUsersContext.Queries.Filters;
using MediatR;
using PagedListExtensionsNetFramework;
using Core42.Application.Features.AccountUsersContext.Dtos;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Queries
{
    /// <summary>
    /// Get filtered, sorted and paginated account users query
    /// </summary>
    public class GetFilteredAccountUsersQuery : IRequest<ManagerResult<PagedDto<AccountUserDto>>>, ISortedQuery, IPagedQuery, IHasFilter<AccountUsersFilter>
    {   
        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set;}

        /// <summary>
        /// Account users filter
        /// </summary>
        public AccountUsersFilter? Filter { get; set; }

        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(AccountUser.CreationDate)}.asc";
    }
}
