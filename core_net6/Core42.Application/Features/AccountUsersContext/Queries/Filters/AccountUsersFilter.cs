﻿using Clouds42.Domain.Access;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Queries.Filters
{
    /// <summary>
    /// Account users filter
    /// </summary>
    public class AccountUsersFilter : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Account id
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// User identifier
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// Specific search line
        /// </summary>
        public string? SearchLine { get; set; }

        /// <summary>
        /// User groups
        /// </summary>
        public List<AccountUserGroup> Groups { get; set; }
    }
}
