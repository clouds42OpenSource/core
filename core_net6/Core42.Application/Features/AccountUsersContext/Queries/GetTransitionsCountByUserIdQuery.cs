﻿using Clouds42.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Queries
{
    public class TransitionDto
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
    public class GetTransitionsCountByUserIdQuery(Guid id) : IRequest<ManagerResult<List<TransitionDto>>>
    {
        public Guid Id { get; set; } = id;
    }

    public class GetTransitionsCountByUserIdQueryHandler(ILogger42 logger, IUnitOfWork unitOfWork)
        : IRequestHandler<GetTransitionsCountByUserIdQuery, ManagerResult<List<TransitionDto>>>
    {
        public async Task<ManagerResult<List<TransitionDto>>> Handle(GetTransitionsCountByUserIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await unitOfWork.AccountUsersRepository
                               .AsQueryableNoTracking()
                               .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken) ??
                           throw new NotFoundException($"Не найден пользователь по идентификатору {request.Id}");

                var transitions = await unitOfWork.AccountsRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.UserSource.Contains(user.Login))
                    .GroupBy(x => x.UserSource)
                    .ToListAsync(cancellationToken);

                return transitions
                    .Select(x => new TransitionDto { Name = x.Key.Split(":")[1], Count = x.Count() })
                    .ToList()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                logger.Error(ex, $"Не удалось получить количество переходов по идентификатору пользователя {request.Id}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<TransitionDto>>(ex.Message);
            }
        }
    }
}
