﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Features.DepartmentsContext;
using Core42.Application.Features.MyDiskContext.Queries;
using LinqExtensionsNetFramework;
using MappingNetStandart.Mappings;
using MediatR;
using Newtonsoft.Json;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Queries
{
    public class GetAccountUserByIdQuery(Guid id)
        : IRequest<ManagerResult<AccountUserProfileDto>>, IHasFilter<ByIdFilter>
    {
        public ByIdFilter Filter { get; set; } = new(id);
    }

    public class ByIdFilter(Guid id) : IQueryFilter
    {
        public Guid Id { get; set; } = id;
    }

    public class GetAccountUserByIdQueryHandler(
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IMapper mapper,
        IUnitOfWork unitOfWork,
        ISender sender)
        : IRequestHandler<GetAccountUserByIdQuery, ManagerResult<AccountUserProfileDto>>
    {
        public async Task<ManagerResult<AccountUserProfileDto>> Handle(GetAccountUserByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = (await unitOfWork.AccountUsersRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.NotificationsSettings)
                    .ThenInclude(x => x.NotificationSettings)
                    .Include(x => x.Account)
                    .AutoFilter(request?.Filter)
                    .FirstOrDefaultAsync(cancellationToken)) ?? 
                    throw new NotFoundException($"Не удалось найти пользователя по идентификатору {request!.Filter.Id}");

                var currentUser = await accessProvider.GetUserAsync();
                var currentUserRoles = currentUser.Groups;
                var currentAccountId = currentUser.ContextAccountId;

                if(currentUserRoles.Count == 1 && currentUserRoles.Any(x =>
                        x is AccountUserGroup.AccountAdmin or AccountUserGroup.AccountUser) && currentAccountId != entity.AccountId)
                    throw new NotFoundException($"У вас не достаточно прав для просмотра этой карточки");

                var resources = await unitOfWork.ResourceRepository
                    .AsQueryableNoTracking()
                    .Where(x =>
                        x.Subject == entity.Id && (x.BillingServiceType.SystemServiceType == ResourceType.MyEntUser ||
                        x.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb))
                    .Select(x => x.BillingServiceType.SystemServiceType)
                    .ToListAsync(cancellationToken);

                var myDiskInfo = (await sender.Send(new GetMyDiskDataQuery { AccountId = entity.AccountId }, cancellationToken)).Result;

                var mappedResult = mapper.Map<AccountUserProfileDto>(entity);

                mappedResult.MyDiskInfo = myDiskInfo;
                
                mappedResult.Department = await unitOfWork
                    .AccountUserDepartmentRepository
                    .AsQueryableNoTracking()
                    .Where(aud => aud.AccountUserId == request!.Filter.Id)
                    .Select(aud => new DepartmentsDto(){DepartmentId = aud.DepartmentId, Name = aud.Department.Name})
                    .FirstOrDefaultAsync(cancellationToken);

                mappedResult.RentType =
                    resources.Contains(ResourceType.MyEntUser) && resources.Contains(ResourceType.MyEntUserWeb)
                        ?
                        RentType.Standart
                        : resources.Contains(ResourceType.MyEntUserWeb)
                            ? RentType.Web
                            : RentType.None;
                
                return mappedResult.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Retrieving account user data failed.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountUserProfileDto>(ex.Message);
            }
        }
    }
    
    public class AccountUserProfileDto : IMapFrom<AccountUser>
    {
        /// <summary>
        /// User identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>        
        public string LastName { get; set; }

        /// <summary>
        /// User middle name
        /// </summary>        
        public string MiddleName { get; set; }

        /// <summary>
        /// User full name
        /// </summary>        
        public string FullName { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>        
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Is user activated (user phone number is confirmed or approved by manager)
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        /// Is user created at Active Directory domain
        /// </summary>
        public bool CreatedInAd { get; set; }

        /// <summary>
        /// Is user unsubscribed from the mailing list
        /// </summary>
        public bool? Unsubscribed { get; set; }

        public DepartmentsDto? Department { get; set; }

        /// <summary>
        /// User creation date
        /// </summary>
        public DateTime? CreationDate { get; set; }

        [JsonIgnore]
        public string EmailStatus { get; set; }

        public bool? IsPhoneVerified { get; set; }

        [NotMapped]
        public bool? IsEmailVerified { get => EmailStatus == "Checked"; }
        
        /// <summary>
        /// User roles in the account
        /// </summary>
        [NotMapped]
        public List<string> AccountRoles { get { return AccountUserGroups.Select(x => x.ToString()).ToList(); } }

        [JsonIgnore]
        public List<AccountUserGroup> AccountUserGroups { get; set; }

        public List<AccountUserNotificationSettingsDto> NotificationSettings { get; set; }

        public RentType RentType { get; set; }

        public int AccountIndexNumber { get; set; }
        public ServiceMyDiskDto MyDiskInfo { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountUser, AccountUserProfileDto>()
                .ForMember(dest => dest.AccountUserGroups, opt => opt.MapFrom(src =>
                    src.AccountUserRoles.Select(r => r.AccountUserGroup).ToList()))
                .ForMember(x => x.NotificationSettings, z => z.MapFrom(y => y.NotificationsSettings))
                .ForMember(x => x.AccountIndexNumber, z => z.MapFrom(y => y.Account.IndexNumber));
        }
    }

    public enum RentType
    {
        None = 0,
        Standart = 1,
        Web = 2
    }

    public class AccountUserNotificationSettingsDto : IMapFrom<AccountUserNotificationSettings>
    {
        public Guid? Id { get; set; }
        public NotificationSettingsDto Notification { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountUserNotificationSettings, AccountUserNotificationSettingsDto>()
                .ForMember(x => x.Notification, z => z.MapFrom(y => y.NotificationSettings))
                .ForMember(x => x.IsActive, z => z.MapFrom(y => y.IsActive))
                .ForMember(x => x.Code, z => z.MapFrom(y => y.Code))
                .ForMember(x => x.Id, z => z.MapFrom(y => y.Id));
        }
    }

    public class NotificationSettingsDto : IMapFrom<NotificationSettings>
    {
        [JsonIgnore]
        public Guid? Id { get; set; }
        public NotificationType Type { get; set; }
        public string TypeText { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<NotificationSettings, NotificationSettingsDto>()
                .ForMember(x => x.Type, z => z.MapFrom(y => y.Type))
                .ForMember(x => x.TypeText, z => z.MapFrom(y => y.TypeText))
                .ForMember(x => x.Id, z => z.MapFrom(y => y.Id));
        }
    }
}
