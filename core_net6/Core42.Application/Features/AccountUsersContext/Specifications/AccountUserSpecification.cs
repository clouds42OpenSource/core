﻿using Clouds42.Common.DataModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Specifications
{
    public static class AccountUserSpecification
    {
        public static Spec<AccountUser> ByUserInGroups(IUserPrincipalDto user)
        {
            var groupsToFilter = new List<AccountUserGroup> { AccountUserGroup.Undefined, AccountUserGroup.AccountUser };

            return user.Groups.All(x => groupsToFilter.Contains(x))
                ? new(x => x.Id == user.Id)
                : new(x => true);
        }

        public static Spec<AccountUser> ByIsActiveAndNotDeleted()
        {
            return new(usr =>
                (!usr.Removed.HasValue || !usr.Removed.Value) && !usr.CorpUserSyncStatus.Contains("Deleted"));
        }

        public static Spec<AccountUser> BySearchLine(string? searchLine)
        {
            return string.IsNullOrEmpty(searchLine)
                ? new(x => true)
                : new(x => x.Login.ToLower().Contains(searchLine.ToLower()) ||
                                             x.FirstName.ToLower().Contains(searchLine.ToLower()) ||
                                             x.LastName.ToLower().Contains(searchLine.ToLower()) ||
                                             x.MiddleName.ToLower().Contains(searchLine.ToLower()) ||
                                             x.FullName.ToLower().Contains(searchLine.ToLower()) ||
                                             x.Email.ToLower().Contains(searchLine.ToLower()) ||
                                             x.PhoneNumber.ToLower().Contains(searchLine.ToLower()));
        }

        public static Spec<AccountUser> ByUserGroups(List<AccountUserGroup>? groups)
        {
            return groups == null || !groups.Any() ? new(x => true) : new(x => x.AccountUserRoles.Any(z => groups.Contains(z.AccountUserGroup)));
        }
    }
}
