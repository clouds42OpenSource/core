﻿namespace Core42.Application.Features.AccountUsersContext.Dtos
{
    public class СonfirmUserDataDto
    {
        public Guid AccountUserId { get; set; }

        public bool isVerified { get; set; }
    }
}
