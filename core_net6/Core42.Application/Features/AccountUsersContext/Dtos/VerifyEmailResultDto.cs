﻿using Newtonsoft.Json;

namespace Core42.Application.Features.AccountUsersContext.Dtos
{
    public class VerifyEmailResultDto
    {
        public string Text { get; set; }

        [JsonIgnore]
        public string Link { get; set; }
    }
}
