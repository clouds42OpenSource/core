﻿using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountUsersContext.Dtos
{
    /// <summary>
    /// Account user dto model
    /// </summary>
    public class AccountUserDto : IMapFrom<AccountUser>
    {
        /// <summary>
        /// User identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>        
        public string LastName { get; set; }

        /// <summary>
        /// User middle name
        /// </summary>        
        public string MiddleName { get; set; }

        /// <summary>
        /// User full name
        /// </summary>        
        public string FullName { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>        
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Is user activated (user phone number is confirmed or approved by manager)
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        /// Is user created at Active Directory domain
        /// </summary>
        public bool CreatedInAd { get; set; }

        /// <summary>
        /// Is user unsubscribed from the mailing list
        /// </summary>
        public bool? Unsubscribed { get; set; }

        /// <summary>
        /// User creation date
        /// </summary>
        public DateTime? CreationDate { get; set; }

        [JsonIgnore]
        public string EmailStatus { get; set; }

        public bool? IsPhoneVerified { get; set; }

        [NotMapped]
        public bool? IsEmailVerified { get => EmailStatus == "Checked";}

        /// <summary>
        /// User roles in the account
        /// </summary>
        [NotMapped]
        public List<string> AccountRoles { get { return AccountUserGroups.Select(x => x.ToString()).ToList(); } }

        [JsonIgnore]
        public List<AccountUserGroup> AccountUserGroups { get; set; }
        
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountUser, AccountUserDto>()
                .ForMember(dest => dest.AccountUserGroups,opt => opt.MapFrom(src =>
                        src.AccountUserRoles.Select(r => r.AccountUserGroup).ToList()));
        }
    }
}
