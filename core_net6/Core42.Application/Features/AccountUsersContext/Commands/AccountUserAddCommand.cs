﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;
using IAccountUserProfileHelper = Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers.IAccountUserProfileHelper;

namespace Core42.Application.Features.AccountUsersContext.Commands
{
    public class AccountUserAddCommand : IRequest<ManagerResult<AccountUserDto>>
    {
        /// <summary>
        /// User identifier
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Is user activated
        /// </summary>
        public bool IsActivated { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        public string? Login { get; set; }

        /// <summary>
        /// Authorization token
        /// </summary>
        public Guid? AuthToken { get; set; }

        /// <summary>
        /// User roles
        /// </summary>
        public List<AccountUserGroup> Roles { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        public string? LastName { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// User middle name
        /// </summary>
        public string? MiddleName { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Current password
        /// </summary>
        public string? CurrentPassword { get; set; }

        /// <summary>
        /// New password
        /// </summary>
        public string? Password { get; set; }

        /// <summary>
        /// Password confirmation
        /// </summary>
        public string? ConfirmPassword { get; set; }

        /// <summary>
        /// User phone number
        /// </summary>
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// Is user account admin
        /// </summary>
        public bool IsAccountAdmin { set; get; }

        /// <summary>
        /// Is profile mine
        /// </summary>
        public bool IsMyProfile { get; set; }

        /// <summary>
        /// Ability to delete users
        /// </summary>
        public bool CanAccountUsersDelete { get; set; }

        /// <summary>
        /// Need to confirm current password
        /// </summary>
        public bool NeedInputCurrentPassword { get; set; }

        /// <summary>
        /// Has user active connections
        /// </summary>
        public bool HasActiveConnections { get; set; }
    }

    public class AccountUserAddCommandHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        ILogger42 logger,
        IConfiguration configuration,
        IOpenIdProvider openIdProvider,
        IAccountUserProfileHelper accountUserProfileHelper,
        IActiveDirectoryProvider activeDirectoryProvider,
        ISender sender,
        IAccountUserRegistrationNotificationProvider accountUserRegistrationNotificationProvider,
        IAccountFolderHelper accountFolderHelper)
        : IRequestHandler<AccountUserAddCommand, ManagerResult<AccountUserDto>>
    {
        public async Task<ManagerResult<AccountUserDto>> Handle(AccountUserAddCommand request, CancellationToken cancellationToken)
        {
            using var transaction = unitOfWork.SmartTransaction.Get();
            try
            {
                accessProvider.HasAccess(ObjectAction.AccountUsers_AddToAccount, () => accessProvider.ContextAccountId);

                var contextAccountId = accessProvider.ContextAccountId;

                if (!accessProvider.HasAccessBool(ObjectAction.AccountUsers_RoleEdit, () => contextAccountId) || (!request.Roles?.Any() ?? true))
                    request.Roles = [AccountUserGroup.AccountUser];

                var account = await unitOfWork.AccountsRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.AccountConfiguration)
                    .ThenInclude(x => x.Locale)
                    .Include(x => x.AccountConfiguration)
                    .ThenInclude(x => x.Segment)
                    .ThenInclude(x => x.CloudServicesFileStorageServer)
                    .Include(x => x.AccountSaleManager)
                    .ThenInclude(x => x.AccountUser)
                    .FirstOrDefaultAsync(a => a.Id == contextAccountId, cancellationToken);

                request.Id = Guid.NewGuid();

                var accountUser = new AccountUser
                {
                    Id = request.Id.Value,
                    AccountId = accessProvider.ContextAccountId,
                    Email = request.Email?.Trim(),
                    EmailStatus = "Unchecked",
                    Login = request.Login?.Trim(),
                    Password = new DesEncryptionProvider(configuration).Encrypt(request.Password),
                    PasswordHash = new AesEncryptionProvider(configuration).Encrypt(request.Password),
                    LastName = string.IsNullOrWhiteSpace(request.LastName) ? null : request.LastName.TrimStart().TrimEnd(),
                    FirstName = string.IsNullOrWhiteSpace(request.FirstName) ? null : request.FirstName.TrimStart().TrimEnd(),
                    MiddleName = string.IsNullOrWhiteSpace(request.MiddleName) ? null : request.MiddleName.TrimStart().TrimEnd(),
                    AuthToken = request.AuthToken,
                    Activated = true,
                    PhoneNumber = string.IsNullOrWhiteSpace(request.PhoneNumber) ? null : PhoneHelper.ClearNonDigits(request.PhoneNumber),
                    CreatedInAd = true,
                    CorpUserSyncStatus = "Added",
                    CreationDate = DateTime.Now,
                    ResetCode = null
                };

                var roles = request.Roles.Select(w =>
                    new AccountUserRole
                    {
                        AccountUserGroup = w,
                        Id = Guid.NewGuid(),
                        AccountUserId = accountUser.Id
                    });

                unitOfWork.AccountUsersRepository.Insert(accountUser);
                unitOfWork.AccountUserRoleRepository.InsertRange(roles);
                
                var notificationSettings = await unitOfWork.NotificationSettingsRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.IsActive)
                    .ToListAsync(cancellationToken);

                var accountUserNotificationSettings = notificationSettings
                    .Select(nt => new AccountUserNotificationSettings
                    {
                        AccountUserId = accountUser.Id,
                        IsActive = nt.Type == NotificationType.Email,
                        NotificationSettingsId = nt.Id,
                        Code = CreateCode(nt.Type, accountUser.Id)
                    })
                    .ToList();

                unitOfWork.AccountUserNotificationSettingsRepository.InsertRange(accountUserNotificationSettings);
                
                await unitOfWork.SaveAsync();

                logger.Trace("Пользователь {0} успешно сохранен в контексте компании {1}({2})", accountUser.Login,
                    account.AccountCaption, account.IndexNumber);

                var phoneNumber = string.IsNullOrWhiteSpace(request.PhoneNumber) ? null : request.PhoneNumber;

                var isAdmin = request.Roles.Contains(AccountUserGroup.AccountAdmin);
                CreateSauriOpenIdUser(accountUser, request.Password, phoneNumber, account.AccountConfiguration.Locale.Name, isAdmin);
                
                request.Email = null;

                var clientFilesPath = accountFolderHelper.GetAccountClientFileStoragePath(account.AccountConfiguration.Segment, account);

                var accountUserDto = new AccountUserDto
                {
                    AccountCaption = account.AccountCaption,
                    AccountId = account.Id,
                    Activated = request.IsActivated,
                    Email = null,
                    FirstName = accountUser.FirstName,
                    LastName = accountUser.LastName,
                    MiddleName = accountUser.MiddleName,
                    PhoneNumber = phoneNumber,
                    Password = request.Password,
                    Login = accountUser.Login,
                    ClientFilesPath = clientFilesPath
                };

                activeDirectoryProvider.RegisterNewUser(accountUserDto);
                accountUserProfileHelper.RunTaskCreate1CStartFolder(accountUser.Login);

                LogEventHelper.LogEvent(unitOfWork, account.Id, accessProvider, LogActions.AddingUser, $"Добавлен пользователь \"{request.Login}\"");

                transaction.Commit();


                accountUser.Account = account;

                accountUserRegistrationNotificationProvider.NotifyAccountUser(accountUser, new AccountRegistrationModelDto(), request.Password);

                if (!string.IsNullOrEmpty(accountUser.Email))
                    await sender.Send(new VerifyEmailCommand { AccountUserId = accountUser.Id, Email = accountUser.Email, SendOnlyLink = true }, cancellationToken);

                accountUserDto.Roles = request.Roles;
                accountUserDto.CorpUserSyncStatus = accountUser.CorpUserSyncStatus;
                accountUserDto.PhoneNumber = accountUser.PhoneNumber;
                accountUserDto.AuthToken = accountUser.AuthToken;
                accountUserDto.Email = accountUser.Email;
                accountUserDto.Id = accountUser.Id;
                accountUserDto.Password = accountUser.Password;
                
                return accountUserDto.ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка во время создания пользователя] {request.Login}");

                try
                {
                    openIdProvider.DeleteUser(
                        new SauriOpenIdControlUserModel { AccountUserID = request.Id.Value, });

                    activeDirectoryProvider.DeleteUser(request.Id.Value, request.Login);
                }

                catch (Exception ex2)
                {
                    handlerException.Handle(ex2, $"[Ошибка отката пользователя {request.Login} в AD или OpedId при добавлении из ЛК]");

                    transaction.Rollback();

                    return PagedListExtensions.ToPreconditionFailedManagerResult<AccountUserDto> (ex2.Message);
                }

                transaction.Rollback();

                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountUserDto>(ex.Message);
            }
        }
        
        private static string? CreateCode(NotificationType type, Guid id) => type switch
        {
            NotificationType.Telegram => Convert.ToBase64String(Encoding.UTF8.GetBytes(id.ToString())),
            NotificationType.WhatsApp => "/start" + $" {id.ToString()}",
            _ => null
        };

        /// <summary>
        /// Создание пользователя в провайдере
        /// </summary>
        /// <param name="accountUser">Аккаунт пользователя</param>
        /// <param name="password">Пароль не шифрованый</param>
        /// <param name="phoneNumber">Номер телефона при регистрации</param>
        private void CreateSauriOpenIdUser(AccountUser accountUser, string password, string phoneNumber, string localeName, bool IsAdmin)
        {
            try
            {
                openIdProvider.AddUser(new SauriOpenIdControlUserModel
                {
                    AccountID = accountUser.AccountId,
                    AccountUserID = accountUser.Id,
                    Login = accountUser.Login,
                    Name = accountUser.Name,
                    Password = password,
                    Email = "",
                    Phone = phoneNumber,
                    FullName = accountUser.FullName,
                    IsAdmin = IsAdmin
                }, localeName);

                logger.Trace("Пользователь {0} Id={1} успешно зарегистрирован в провайдере OpenID", accountUser.Login,
                    accountUser.Id);
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка добавления пользователя в провайдер] {accountUser.Login}");

                throw;
            }
        }



    }
}
