﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using PagedListExtensionsNetFramework;
using Clouds42.Logger;

namespace Core42.Application.Features.AccountUsersContext.Commands
{
    public class RegisterNewUserCommand : IRequest<ManagerResult<AccountUserRegisterDto>>
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Password { get; set; }
        public string Login { get; set; }
        public string LocaleName { get; set; }
        public string UserSource { get; set; }
        public bool PhoneNumberVerified { get; set; }
        public bool EmailVerified { get; set; } = true;
        public long? TelegramChatId { get; set; }

        public RegisterConfigsDto Configs { get; set; }
    }

    public class RegisterNewUserCommandHandler(
        IHttpContextAccessor contextAccessor,
        CreateAccountManager createAccountManager,
        ILogger42 logger)
        : IRequestHandler<RegisterNewUserCommand, ManagerResult<AccountUserRegisterDto>>
    {
        public async Task<ManagerResult<AccountUserRegisterDto>> Handle(RegisterNewUserCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var domainInfo = new Uri(contextAccessor.HttpContext?.Request.GetDisplayUrl() ?? "42clouds.com").GetDomainInfo();
                var cloudService = request.Configs.GetSystemCloudServiceEnumValue();

                var accountRegistrationModel = new AccountRegistrationModelDto
                {
                    Email = request.Email,
                    FullPhoneNumber = request.PhoneNumber,
                    Login = request.Login,
                    Password = request.Password,
                    PhoneNumberVerified = request.PhoneNumberVerified,
                    TelegramChatId = request.TelegramChatId,
                    UserSource = string.IsNullOrEmpty(request.UserSource) ? domainInfo?.RegistrableDomain : request.UserSource,
                    LastName = request.LastName,
                    FirstName = request.FirstName,
                    MiddleName = request.MiddleName,
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = cloudService,
                        CloudServiceId = request.Configs.CloudServiceId,
                        Configuration1C = request.Configs.Configuration1C,
                        RegistrationSource = request.Configs.RegistrationSource,
                        ReferralAccountId = request.Configs.ReferralAccountId
                    },
                    LocaleName = request.LocaleName,
                    SkipOpenIdCreating = true,
                    IsEmailVerified = request.EmailVerified
                };

               var createResult = await createAccountManager.AddAccount(accountRegistrationModel);

                return createResult.Error ? PagedListExtensions.ToPreconditionFailedManagerResult<AccountUserRegisterDto>(createResult.Message) : new AccountUserRegisterDto { AccountId = createResult.Result.Account.Id, AccountUserId = createResult.Result.AccountUserId }.ToOkManagerResult();
            }

            catch(Exception ex)
            {
                logger.Error($"Ошибка регистрации пользователя {request.Email} {request.Login}", ex);

                return PagedListExtensions.ToPreconditionFailedManagerResult<AccountUserRegisterDto>("Произошла ошибка регистрации пользователя");
            }
        }
    }

    public class RegisterConfigsDto
    {
        /// <summary>
        /// Имя сервиса.
        /// Примечание только системные сервисы. 
        /// </summary>
        public string CloudService { get; set; }

        /// <summary>
        /// Номепр сервиса.
        /// Примечание только не системные сервисы.
        /// </summary>
        public Guid? CloudServiceId { get; set; }

        /// <summary>
        /// Имя конфигурации которую нужно развернуть.
        /// </summary>
        public string Configuration1C { get; set; }

        /// <summary>
        /// Номер рефферала.
        /// </summary>
        public Guid? ReferralAccountId { get; set; }

        /// <summary>
        /// Источник регистрации.
        /// </summary>
        public ExternalClient RegistrationSource { get; set; }

        /// <summary>
        /// Получить имя системного сервиса из строки.
        /// </summary>        
        public Clouds42Service GetSystemCloudServiceEnumValue()
        {
            if (string.IsNullOrEmpty(CloudService))
                return Clouds42Service.Unknown;

            if (!Enum.TryParse(CloudService, out Clouds42Service cloudService))
                cloudService = Clouds42Service.MyEnterprise;

            return cloudService;
        }
    }


    public class AccountUserRegisterDto
    {
        public Guid? AccountUserId { get; set; }
        public Guid AccountId { get; set; }
    }
}
