﻿using Clouds42.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Commands
{
    public class DeleteUserCommand(Guid userId, Guid accountId) : IRequest<ManagerResult<string>>
    {
        public Guid UserId { get; set; } = userId;
        public Guid AccountId { get; set; } = accountId;
    }

    public class DeleteUserCommandHandler(IUnitOfWork unitOfWork, ILogger42 logger)
        : IRequestHandler<DeleteUserCommand, ManagerResult<string>>
    {
        public async Task<ManagerResult<string>> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await unitOfWork.AccountUsersRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.Id == request.UserId, cancellationToken) 
                    ?? throw new NotFoundException($"Не удалось найти пользователя по идентификатору {request.UserId}");

                if (user.AccountId != request.AccountId)
                    throw new InvalidOperationException($"Идентификатор аккаунта переданный в запросе, отличается от аккаунта привязанного к пользователю {user.Id}");

                var usersCount = await unitOfWork.AccountUsersRepository.AsQueryableNoTracking()
                    .CountAsync(x => x.AccountId == request.AccountId, cancellationToken);

                unitOfWork.AccountUsersRepository.Delete(user);

                if (usersCount == 1)
                {
                    var account = await unitOfWork.AccountsRepository
                        .AsQueryable()
                        .FirstOrDefaultAsync(x => x.Id == request.AccountId, cancellationToken) 
                        ?? throw new NotFoundException($"Не удалось найти пользователя по идентификатору {request.UserId}");
                    
                    await DeleteMyDiskProperties(account.Id, cancellationToken);
                    await DeleteRequisites(account.Id, cancellationToken);
                    await DeleteConfigurations(account.Id, cancellationToken);
                    await DeleteAgentWallets(account.Id, cancellationToken);
                    await DeleteFlowResources(account.Id, cancellationToken);
                    await DeleteBillingAccounts(account.Id, cancellationToken);
                    await DeleteResources(account.Id, cancellationToken);
                    await DeleteResourcesConfigurations(account.Id, cancellationToken);
                    await DeleteAccountCsResourceValues(account.Id, cancellationToken);
                    await DeleteNotificationBuffers(account.Id, cancellationToken);
                    await DeleteProvidedServices(account.Id, cancellationToken);

                    unitOfWork.AccountsRepository.Delete(account);
                }

                await unitOfWork.SaveAsync();

                return "Пользователь успешно удален".ToOkManagerResult();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);

                return PagedListExtensions.ToPreconditionFailedManagerResult<string>(ex.Message);
            }
        }

        private async Task DeleteMyDiskProperties(Guid accountId, CancellationToken cancellationToken)
        {
            var myDiskPropertiesByAccount = await unitOfWork
                .GetGenericRepository<MyDiskPropertiesByAccount>()
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork
                .GetGenericRepository<MyDiskPropertiesByAccount>()
                .DeleteRange(myDiskPropertiesByAccount);
        }

        private async Task DeleteRequisites(Guid accountId, CancellationToken cancellationToken)
        {
            var requisites = await unitOfWork
                .GetGenericRepository<AccountRequisites>()
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork
                .GetGenericRepository<AccountRequisites>()
                .DeleteRange(requisites);
        }

        private async Task DeleteConfigurations(Guid accountId, CancellationToken cancellationToken)
        {
            var configurations = await unitOfWork.AccountConfigurationRepository
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork.AccountConfigurationRepository.DeleteRange(configurations);
        }

        private async Task DeleteAgentWallets(Guid accountId, CancellationToken cancellationToken)
        {
            var agentWallets = await unitOfWork.AgentWalletRepository
                .AsQueryable()
                .Where(x => x.AccountOwnerId == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork.AgentWalletRepository.DeleteRange(agentWallets);
        }

        private async Task DeleteFlowResources(Guid accountId, CancellationToken cancellationToken)
        {
            var flowResources = await unitOfWork.FlowResourcesScopeRepository
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork.FlowResourcesScopeRepository.DeleteRange(flowResources);
        }

        private async Task DeleteBillingAccounts(Guid accountId, CancellationToken cancellationToken)
        {
            var billingAccounts = await unitOfWork.BillingAccountRepository
                .AsQueryable()
                .Where(x => x.Id == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork.BillingAccountRepository.DeleteRange(billingAccounts);
        }

        private async Task DeleteResources(Guid accountId, CancellationToken cancellationToken)
        {
            var resources = await unitOfWork.ResourceRepository
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ToListAsync(cancellationToken);

            await DeleteMyDatabasesResources(resources.Select(x => x.Id).ToList(), cancellationToken);
            
            unitOfWork.ResourceRepository.DeleteRange(resources);
        }

        private async Task DeleteMyDatabasesResources(ICollection<Guid> resourcesIds, CancellationToken cancellationToken)
        {
            var myDatabasesResources = await unitOfWork
                .GetGenericRepository<MyDatabasesResource>()
                .AsQueryable()
                .Where(x => resourcesIds.Contains(x.ResourceId))
                .ToListAsync(cancellationToken);
            
            unitOfWork.GetGenericRepository<MyDatabasesResource>().DeleteRange(myDatabasesResources);
        }

        private async Task DeleteResourcesConfigurations(Guid accountId, CancellationToken cancellationToken)
        {
            var resourceConfigurations = await unitOfWork.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork.ResourceConfigurationRepository.DeleteRange(resourceConfigurations);
        }

        private async Task DeleteAccountCsResourceValues(Guid accountId, CancellationToken cancellationToken)
        {
            var accountCsValues = await unitOfWork.AccountCsResourceValueRepository
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork.AccountCsResourceValueRepository.DeleteRange(accountCsValues);
        }

        private async Task DeleteNotificationBuffers(Guid accountId, CancellationToken cancellationToken)
        {
            var notificationBuffers = await unitOfWork.NotificationBufferRepository
                .AsQueryable()
                .Where(x => x.Account == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork.NotificationBufferRepository.DeleteRange(notificationBuffers);
        }

        private async Task DeleteProvidedServices(Guid accountId, CancellationToken cancellationToken)
        {
            var providedServices = await unitOfWork.ProvidedServiceRepository
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ToListAsync(cancellationToken);

            unitOfWork.ProvidedServiceRepository.DeleteRange(providedServices);
        }
    }
}
