﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using AutoMapper;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.AccountUsers.ServiceManagerConnector.Commands;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.Department;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MappingNetStandart.Mappings;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Commands
{
    public class AccountUserEditCommand : IRequest<ManagerResult<bool>>, IMapTo<AccountUserDto>, IMapFrom<AccountUser>
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// ID Аккаунта пользователя
        /// </summary>        
        public Guid AccountId { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>        
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>        
        public string MiddleName { get; set; }

        
        /// <summary>
        /// Департамент
        /// </summary>
        public DepartmentRequest? DepartmentRequest { get; set; }

        /// <summary>
        /// Эл. почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>        
        public string PhoneNumber { get; set; }
        public Guid? AuthToken { get; set; }

        /// <summary>
        /// Пользователь активирован (подтвержден мобильный или одобрен менеджером)
        /// </summary>
        public bool Activated { get; set; }
        public List<AccountUserGroup> Roles { get; set; }
        public string Password { get; set; }

        public string GetDisplayName()
        {
            var displayName = string.Join(" ", string.IsNullOrEmpty(FirstName) ? "" : FirstName,
                string.IsNullOrEmpty(LastName) ? "" : LastName,
                string.IsNullOrEmpty(MiddleName) ? "" : MiddleName).Trim();

            return !string.IsNullOrEmpty(displayName) ? displayName : Login;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountUserEditCommand, AccountUserDto>();
            profile.CreateMap<AccountUser, AccountUserEditCommand>();
        }
    }

    public class AccountUserEditCommandHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IOpenIdProvider openIdProvider,
        ILogger42 logger,
        IUpdateLoginForWebAccessDatabaseJobWrapper updateLoginForWebAccessDatabaseJobWrapper,
        IAccountUserUpdateProvider accountUserUpdateProvider,
        IMapper mapper,
        SynchronizeAccountUserWithTardisHelper synchronizeAccountUserWithTardisHelper,
        ISender sender,
        ExecuteRequestCommand executeRequestCommand)
        : IRequestHandler<AccountUserEditCommand, ManagerResult<bool>>
    {
        public async Task<ManagerResult<bool>> Handle(AccountUserEditCommand request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => request.Id);

                var user = await unitOfWork.AccountUsersRepository
                    .AsQueryable()
                    .Include(x => x.Account)
                    .ThenInclude(x => x.AccountConfiguration)
                    .ThenInclude(x => x.Locale)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                if (user == null)
                {
                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(
                        "Не найден пользователь для редактирования");
                }

                var mappedOld = mapper.Map<AccountUserEditCommand>(user);

                var compensationActions = new Stack<Func<Task>>();

                try
                {
                    await ChangeInActiveDirectory(user, request, mappedOld.Email);
                    LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Изменение в Active Directory для пользователя \"{request.Login}\"");
                    compensationActions.Push(() => ChangeInActiveDirectory(user, mappedOld, mappedOld.Email));

                    await ChangeInDatabase(user, request);
                    LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Изменение в базе данных для пользователя \"{request.Login}\"");
                    compensationActions.Push(() => ChangeInDatabase(user, mappedOld));

                    await ChangeInOpenIdProvider(user, request, mappedOld.Email);
                    LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Изменение в OpenId Provider для пользователя \"{request.Login}\"");
                    compensationActions.Push(() => ChangeInOpenIdProvider(user, mappedOld, mappedOld.Email));

                    await ChangeLoginInDatabasesWebAccess(user, request, cancellationToken);
                    LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Изменение логина в DatabasesWebAccess для пользователя \"{request.Login}\"");
                    compensationActions.Push(() => ChangeLoginInDatabasesWebAccess(user, mappedOld, cancellationToken));

                    synchronizeAccountUserWithTardisHelper.SynchronizeAccountUserUpdateViaNewThread(user.Id);
                    LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Синхронизация с Tardis для пользователя \"{request.Login}\"");
                    compensationActions.Push(() => Task.Run(() => synchronizeAccountUserWithTardisHelper.SynchronizeAccountUserUpdateViaNewThread(user.Id), cancellationToken));

                    if (string.IsNullOrEmpty(request.Email) || request.Email == mappedOld.Email)
                    {
                        return true.ToOkManagerResult();
                    }

                    await sender.Send(new VerifyEmailCommand { AccountUserId = user.Id, Email = user.Email, SendOnlyLink = true }, cancellationToken);
                    LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Отправка команды верификации email для пользователя \"{request.Login}\"");

                    return true.ToOkManagerResult();
                }
                catch (Exception ex)
                {
                    while (compensationActions.Count > 0)
                    {
                        var compensationAction = compensationActions.Pop();
                        try
                        {
                            await compensationAction();
                        }
                        catch (Exception compEx)
                        {
                            LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Ошибка при выполнении компенсирующей операции: {compEx.Message}");
                        }
                    }

                    handlerException.Handle(ex, $"[Ошибка редактирования пользователя] {request.Id} {request.Login}");
                    LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Ошибка редактирования пользователя \"{request.Login}\". Откат изменений. Причина: {ex.Message}");

                    return PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Не удалось отредактировать пользователя");
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования пользователя] {request.Id} {request.Login}");
                LogEventHelper.LogEvent(unitOfWork, request.AccountId, accessProvider, LogActions.EditUserCard, $"Ошибка редактирования пользователя \"{request.Login}\". Причина: {ex.Message}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Не удалось отредактировать пользователя");
            }
        }
       

        private static async Task ChangeInActiveDirectory(AccountUser exists, AccountUserEditCommand request, string oldEmail)
        {
            await Task.Run(() =>
            {
                if(oldEmail != request.Email)
                    ActiveDirectoryUserFunctions.ChangeEmail(null, exists.Login);

                ActiveDirectoryUserFunctions.ChangePassword(request.Login, request.Password);
                ActiveDirectoryUserFunctions.ChangeName(request.Login, request.FirstName);
                ActiveDirectoryUserFunctions.ChangeMiddleName(request.Login, request.MiddleName);
                ActiveDirectoryUserFunctions.ChangeLastName(request.Login, request.LastName);
                ActiveDirectoryUserFunctions.ChangePhoneNumber(request.Login, PhoneHelper.ClearNonDigits(request.PhoneNumber));
                ActiveDirectoryUserFunctions.ChangeDisplayName(request.Login, request.GetDisplayName());
            });
        }

        private async Task ChangeInOpenIdProvider(AccountUser exists, AccountUserEditCommand request, string oldEmail)
        {
            await Task.Run(() =>
            {
                var openIdModel = new SauriOpenIdControlUserModel
                {
                    AccountUserID = exists.Id,
                    AccountID = exists.Account.Id,
                    Login = request.Login,
                    Email = oldEmail != request.Email ? "" : null,
                    FullName = $"{request.LastName} {request.FirstName} {request.MiddleName}",
                    Password = !string.IsNullOrEmpty(request.Password)
                        ? request.Password
                        : string.Empty,
                };

                if (string.IsNullOrEmpty(openIdModel.FullName)
                    && string.IsNullOrEmpty(openIdModel.Login)
                    && string.IsNullOrEmpty(openIdModel.Password)
                    && string.IsNullOrEmpty(openIdModel.Phone)
                    && string.IsNullOrEmpty(openIdModel.Email))
                    return;

                openIdProvider.ChangeUserProperties(openIdModel, exists.Account.AccountConfiguration.Locale.Name);

                logger.Info($"Для пользователя Id={openIdModel.AccountUserID} успешно измены данные" +
                             $"Логин:{openIdModel.Login}, Пароль:{openIdModel.Password}, Почта:{openIdModel.Email}," +
                             $" Телефон:{openIdModel.Phone} Полное Имя:{openIdModel.FullName}, в провайдере OpenID");
            });
        }

        private async Task ChangeLoginInDatabasesWebAccess(AccountUser exists, AccountUserEditCommand request, CancellationToken cancellationToken)
        {
            if (exists.Login == request.Login)
                return;

            var accountDatabasesIds = await unitOfWork.AcDbAccessesRepository
                .AsQueryable()
                .Where(access => access.AccountUserID == exists.Id &&
                                 access.Database.PublishState == PublishState.Published.ToString() &&
                                 access.Database.State == DatabaseState.Ready.ToString() &&
                                 access.Database.AccountDatabaseOnDelimiter == null)
                .Select(x => x.AccountDatabaseID)
                .ToListAsync(cancellationToken);

            updateLoginForWebAccessDatabaseJobWrapper.Start(new UpdateLoginForWebAccessDatabaseDto
            {
                AccountDatabasesId = accountDatabasesIds, NewLogin = request.Login, OldLogin = exists.Login
            });
        }

        private async Task ChangeInDatabase(AccountUser exists, AccountUserEditCommand request)
        {
            var message = $"Редактирование пользователя {request.Login}";
            var traceInfoBuilder = new StringBuilder();
            traceInfoBuilder.AppendLine($"{message}. ");

            logger.Info(message);

            var dto = mapper.Map<AccountUserEditCommand, AccountUserDto>(request);

            var updateResult = accountUserUpdateProvider.UpdateExceptRoles(exists, dto);

            var hasAccessToEditRoles =
                accessProvider.HasAccessBool(ObjectAction.AccountUsers_RoleEdit, () => exists.AccountId);

            var isUserCloudAdmin =
                exists.AccountUserRoles.Any(r => r.AccountUserGroup == AccountUserGroup.CloudAdmin);

            if (hasAccessToEditRoles || isUserCloudAdmin)
                updateResult.Absorb(accountUserUpdateProvider.UpdateOnlyRoles(exists, dto));

            var department = await unitOfWork
                .AccountUserDepartmentRepository
                .AsQueryable()
                .FirstOrDefaultAsync(aud => aud.AccountUserId == exists.Id);
            
            if (department != null && request.DepartmentRequest != null)
            {
                await ChangeUserDepartment(department, request.DepartmentRequest, exists);
            }

            if (!updateResult.WereChangesMade)
                return;

            foreach (var msg in updateResult.Messages)
            {
                traceInfoBuilder.Append(msg);
                traceInfoBuilder.AppendLine(". ");
            }

            exists.EditDate = DateTime.Now;

            unitOfWork.AccountUsersRepository.Update(exists);

            LogEventHelper.LogEvent(unitOfWork, exists.AccountId, accessProvider, LogActions.EditUserCard,
                traceInfoBuilder.ToString());
        }
        private async Task NotifyDepartmentChanged(DepartmentRequest? request, Guid userId, bool isDetach)
        {
            try
            {
                var neuroUrl = CloudConfigurationProvider.Neuro.GetBaseApiUrl();
                var formatedUrl = string.Format(neuroUrl, "departments", "editusers");
                var body = new { request.DepartmentId, DepartmentName = request.Name, UserIds = new List<Guid> { userId }, isDetach }
                    .ToJson();
                logger.Info($"Составлен запрос на нейро: {formatedUrl} \n Тело запроса : {body}");
                var response =  executeRequestCommand.ExecuteJsonRequest(formatedUrl, HttpMethod.Post, body);
                var message = await response.Content.ReadAsStringAsync();
                logger.Info(message);
            }
            catch (Exception e)
            {
                logger.Error($"Не удалось отправить запрос на нейро {e.Message}");
            }
        }
        
        private async Task ChangeUserDepartment(
            AccountUserDepartment? existingDepartment,
            DepartmentRequest? request,
            AccountUser accountUser)
        {
            switch (request?.ActionType)
            {
                case DepartmentAction.Create:
                    await InsertDepartment(accountUser, request, existingDepartment);
                    await NotifyDepartmentChanged(request, accountUser.Id, false);
                    break;
                case DepartmentAction.Update:
                    await UpdateDepartment(existingDepartment, request);
                    await NotifyDepartmentChanged(request, accountUser.Id, false);
                    break;
                case DepartmentAction.Remove:
                    await RemoveDepartment(existingDepartment);
                    await NotifyDepartmentChanged(request, accountUser.Id, true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(request.ActionType),
                        "Неизвестное действие по управлению департаментами.");
            }
            
            LogEventHelper.LogEvent(unitOfWork,
                accountUser.AccountId,
                accessProvider,
                LogActions.DepartmentAccountUserAction,
                $"Действие {nameof(request.ActionType)} с пользователем {accountUser.Login} завершено успешно");
        }


        private async Task InsertDepartment(AccountUser accountUser, DepartmentRequest request, AccountUserDepartment? existsDepartment)
        {
            if (existsDepartment != null)
                throw new InvalidOperationException($"Пользователь {existsDepartment.AccountUser.Login} уже в департаменте {existsDepartment.Department.Name}. Добавление невозможно.");

            var newDepartment = new AccountUserDepartment
            {
                Id = Guid.NewGuid(),
                AccountUserId = accountUser.Id,
                DepartmentId = request.DepartmentId,
            };

            unitOfWork.AccountUserDepartmentRepository.Insert(newDepartment);
            await unitOfWork.SaveAsync();
        }

        private async Task UpdateDepartment(AccountUserDepartment? existsDepartment, DepartmentRequest request)
        {
            if (existsDepartment == null)
                throw new InvalidOperationException($"Департамента {request.Name} нет. Обновление невозможно");

            existsDepartment.DepartmentId = request.DepartmentId;
            unitOfWork.AccountUserDepartmentRepository.Update(existsDepartment);
            await unitOfWork.SaveAsync();
        }

        private async Task RemoveDepartment(AccountUserDepartment? existsDepartment)
        {
            if (existsDepartment == null)
                 return;

            unitOfWork.AccountUserDepartmentRepository.Delete(existsDepartment);
            await unitOfWork.SaveAsync();
        }

    }
}
