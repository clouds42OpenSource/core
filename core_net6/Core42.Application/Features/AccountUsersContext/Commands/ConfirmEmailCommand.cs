﻿using Clouds42.Common.ManagersResults;
using MediatR;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountUsersContext.Commands
{
    public class ConfirmEmailCommand(string code, Guid? accountUserId, bool? isLinkClicked)
        : IRequest<ManagerResult<string>>
    {
        public string Code { get; set; } = code;

        public Guid? AccountUserId { get; set; } = accountUserId;

        public bool? IsLinkClicked { get; set; } = isLinkClicked;

        [JsonIgnore]
        public bool IsExpired { get; set; }
        [JsonIgnore]
        public bool EmailConfirmed { get; set; }
    }
}
