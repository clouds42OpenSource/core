﻿using Clouds42.Common.ManagersResults;
using MediatR;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountUsersContext.Commands;

public class VerifyPhoneCommand : IRequest<ManagerResult<string>>
{
    [JsonIgnore]
    public int LeftSeconds { get; set; }

    [JsonIgnore]
    public int LimitCount { get; set; }

    [JsonIgnore]
    public bool PhoneConfirmed { get; set; }
}
