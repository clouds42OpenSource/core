﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountUsersContext.Commands
{
    public class ConfirmEmailNoCodeCommand(bool isEmailVerified, Guid accountUserId) : IRequest<ManagerResult<string>>
    {
        public Guid AccountUserId { get; set; } = accountUserId;

        public bool IsEmailVerified { get; set; } = isEmailVerified;
    }

}
