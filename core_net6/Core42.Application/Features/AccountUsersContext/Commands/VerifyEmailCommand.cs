﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AccountUsersContext.Dtos;
using MediatR;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountUsersContext.Commands
{
    public class VerifyEmailCommand : IRequest<ManagerResult<VerifyEmailResultDto>>
    {
        [JsonIgnore]
        public int LeftSeconds { get; set; }

        [JsonIgnore]
        public int LimitCount { get; set; }
        
        [JsonIgnore]
        public bool EmailConfirmed { get; set; }

        [JsonIgnore]
        public Guid? AccountUserId { get; set; }

        [JsonIgnore]
        public string Email { get; set; }

        [JsonIgnore]
        public bool SendOnlyLink { get; set; }
    }
}
