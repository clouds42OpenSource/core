﻿using Clouds42.Common.ManagersResults;
using MediatR;
using Newtonsoft.Json;

namespace Core42.Application.Features.AccountUsersContext.Commands;

public class ConfirmPhoneCommand(string code, Guid? accountUserId) : IRequest<ManagerResult<string>>
{
    public string Code { get; set; } = code;
    public Guid? AccountUserId { get; set; } = accountUserId;

    [JsonIgnore]
    public bool IsExpired { get; set; }

    [JsonIgnore]
    public bool PhoneConfirmed { get; set; }
}
