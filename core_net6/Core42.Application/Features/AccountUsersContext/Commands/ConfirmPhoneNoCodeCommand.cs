﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AccountUsersContext.Commands;

public class ConfirmPhoneNoCodeCommand(bool isPhoneVerified, Guid accountUserId) : IRequest<ManagerResult<string>>
{
    public Guid AccountUserId { get; set; } = accountUserId;

    public bool IsPhoneVerified { get; set; } = isPhoneVerified;
}
