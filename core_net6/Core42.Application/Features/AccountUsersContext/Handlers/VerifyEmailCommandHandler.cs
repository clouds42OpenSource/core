﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using Core42.Application.Features.AccountUsersContext.Dtos;
using Core42.Application.Features.EmailContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Handlers;

public class VerifyEmailCommandHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IAccessProvider accessProvider,
    ISender sender)
    : IRequestHandler<VerifyEmailCommand, ManagerResult<VerifyEmailResultDto>>
{
    public async Task<ManagerResult<VerifyEmailResultDto>> Handle(VerifyEmailCommand request, CancellationToken cancellationToken)
    {
        var currentUser = await accessProvider.GetUserAsync();
        var userId = request.AccountUserId ?? currentUser.Id;
        try
        {
            var code = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Guid.NewGuid()}"));
            var minutesToExpire = CloudConfigurationProvider.Emails.GetMinutesToExpireCode();
            var databaseUser =
                await unitOfWork.AccountUsersRepository.AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == userId, cancellationToken) ??
                throw new InvalidOperationException($"Не найден пользователь по идентификатору {userId}");

            var emailTo = request.AccountUserId.HasValue ? request.Email : currentUser.Email;

            var emailused =
                await unitOfWork.AccountUsersRepository.AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Email == emailTo && x.EmailStatus == "Checked", cancellationToken);

            if (emailused != null)
                return PagedListExtensions.ToPreconditionFailedManagerResult<VerifyEmailResultDto>(
                $"Ошибка отправки кода подтверждения,данная почта {emailTo} уже использается в другом аккаунте, попробуйте указать другую ");


            var nowDate = DateTime.Now;

            var databaseModel = new EmailSmsVerification
            {
                AccountUserId = userId,
                Code = code,
                ExpiresOn = nowDate.AddMinutes(int.Parse(minutesToExpire)),
                Type = VerificationType.Email,
                CreatedOn = nowDate
            };

            unitOfWork.EmailSmsVerificationRepository.Insert(databaseModel);

            var oldestVerificationCodes = await unitOfWork.EmailSmsVerificationRepository
                .AsQueryable()
                .Where(x => x.Type == VerificationType.Email && x.AccountUserId == currentUser.Id)
                .ToListAsync(cancellationToken);

            foreach (var oldVerificationCode in oldestVerificationCodes)
            {
                oldVerificationCode.ExpiresOn = nowDate;

                unitOfWork.EmailSmsVerificationRepository.Update(oldVerificationCode);
            }

            await unitOfWork.SaveAsync();

            var linkToConfirmEmail = string.Format(CloudConfigurationProvider.Emails.GetEmailVerificationLink(), code);
            var sendGridDefaultTemplateId = CloudConfigurationProvider.SendGrid.GetDefaultTemplateId();
            


            var body = request.SendOnlyLink
                ? "<div style=\"padding: 0 7%\">" +
                  "<p>Для подтверждения почты перейдите по ссылке:</p>" +
                  $"<a href=\"{linkToConfirmEmail}\">{linkToConfirmEmail}</a>" +
                  "</div>"

                : "<div style=\"padding: 0 7%\">" +
                  $"<p>Для подтверждения почты введите этот код <mark>{code}</mark></p>" +
                  "<p>или перейдите по ссылке для подтверждения почты</p>" +
                  $"<a href=\"{linkToConfirmEmail}\">{linkToConfirmEmail}</a>" +
                  "</div>";

            await sender.Send(new SendEmailCommand
            {
                Body = body,
                Categories = ["email-verify"],
                Header = "Подтверждение почты",
                Subject = "Подтверждение почты",
                Locale = "ru-ru",
                To = emailTo,
                SendGridTemplateId = sendGridDefaultTemplateId
            }, cancellationToken);

            LogEventHelper.LogEvent(unitOfWork, currentUser?.RequestAccountId ?? databaseUser.AccountId, accessProvider, LogActions.SendEmailVerificationCodeToUser, $"Пользователю {emailTo} отправлено письмо с кодом подтверждения");

            return new VerifyEmailResultDto
            {
                Text = $"На ваш почтовый ящик {emailTo} отправлено письмо с кодом подтверждения Email",
                Link = linkToConfirmEmail
            }.ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, $"[Не удалось провести верификацию почты пользователя с почтой {currentUser?.Email}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<VerifyEmailResultDto>(
                "Ошибка отправки кода подтверждения Email");
        }
    }
}
