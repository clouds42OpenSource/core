﻿using Clouds42.AccountDatabase.Data.Constants;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Features.AccountUsersContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Handlers;

public class GetAccountUserDatabaseAccessesQueryHandler(IAccessProvider accessProvider, IUnitOfWork unitOfWork, ILogger42 logger, IConfiguration configuration) : IRequestHandler<GetAccountUserDatabaseAccessesQuery, ManagerResult<UserCardAcDbAccessesDataDto>>
{
    public async Task<ManagerResult<UserCardAcDbAccessesDataDto>> Handle(GetAccountUserDatabaseAccessesQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.AccountDatabase_GetAcDbAccesses,
                () => accessProvider.ContextAccountId);

            var accessList = await SelectDatabaseAccessesForUser(request.AccountUserId, cancellationToken);

            return new UserCardAcDbAccessesDataDto
            {
                DatabaseAccesses = accessList,
                AccessToServerDatabaseServiceTypeId = (await unitOfWork.BillingServiceTypeRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(servType => servType.SystemServiceType == ResourceType.AccessToServerDatabase, cancellationToken))?.Id ?? throw new NotFoundException($"По типу системной услуги {ResourceType.AccessToServerDatabase.Description()} не найдена услуга"),
                Currency = (await unitOfWork.AccountsRepository.GetLocaleAsync(unitOfWork.AccountsRepository.GetAccountByUserId(request.AccountUserId).Id, Guid.Parse(configuration["DefaultLocale"]!))).Currency,
                MyDatabaseBillingServiceId = (await unitOfWork.BillingServiceRepository.AsQueryableNoTracking().FirstOrDefaultAsync(bs => bs.SystemService == Clouds42Service.MyDatabases, cancellationToken))!.Id,
            }.ToOkManagerResult();

        }

        catch (Exception ex)
        {
            logger.Warn(ex,
                $"[Ошибка получения данных доступов карточки пользователей]  '{request.AccountUserId}'.");

            return PagedListExtensions.ToPreconditionFailedManagerResult<UserCardAcDbAccessesDataDto>(ex.Message);
        }
    }

    private async Task<List<AcDbAccessUserDto>> SelectDatabaseAccessesForUser(Guid userId, CancellationToken cancellationToken)
    {
        return await unitOfWork.AcDbAccessesRepository
            .AsQueryableNoTracking()
            .Where(x => x.Database.State == DatabaseState.Ready.ToString() &&
                        x.Account.AccountUsers.Any(z => z.Id == userId))
            .GroupBy(x => x.AccountDatabaseID)
            .Select(x => new AcDbAccessUserDto
            {
                Caption = x.First().Database.Caption,
                ConfigurationName =
                    x.First().Database.DbTemplate.ConfigurationDbTemplateRelations.FirstOrDefault()!.Configuration1CName ==
                    AccountDatabaseDataProviderConstants.ClearTemplateName
                        ? AccountDatabaseDataProviderConstants.ClearTemplateConfigurationName
                        : x.First().Database.DbTemplate.ConfigurationDbTemplateRelations.FirstOrDefault()!
                            .Configuration1CName,
                IsDbOnDelimiters = x.First().Database.AccountDatabaseOnDelimiter != null,
                IsFile = x.First().Database.IsFile.HasValue && x.First().Database.IsFile!.Value,
                DatabaseId = x.First().Database.Id,
                MyDatabasesServiceTypeId =
                    x.First().Database.DbTemplate.ConfigurationDbTemplateRelations.FirstOrDefault()!.Configuration1C
                        .ConfigurationServiceTypeRelation.ServiceType.Id,
                TemplateImageName =
                    string.IsNullOrEmpty(x.First().Database.DbTemplate.ImageUrl) ? "" : x.First().Database.DbTemplate.ImageUrl,
                V82Name = x.First().Database.V82Name,
                HasAccess = x.First().AccountUserID == userId,
                State = x.First().AccountUserID == userId && x.First().ManageDbOnDelimitersAccess != null ? x.First().ManageDbOnDelimitersAccess.AccessState :
                    x.First().AccountUserID == userId ? AccountDatabaseAccessState.Done :
                    AccountDatabaseAccessState.Undefined,
                ConfigurationCode = x.First().Database.AccountDatabaseOnDelimiter != null
                    ? x.First().Database.AccountDatabaseOnDelimiter.DbTemplateDelimiterCode
                    : null,
            })
            .ToListAsync(cancellationToken);
    }
}
