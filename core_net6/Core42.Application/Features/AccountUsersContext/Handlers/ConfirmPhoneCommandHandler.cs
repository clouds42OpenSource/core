﻿using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Handlers;

public class ConfirmPhoneCommandHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IAccessProvider accessProvider,
    IOpenIdProvider openIdProvider)
    : IRequestHandler<ConfirmPhoneCommand, ManagerResult<string>>
{
    public async Task<ManagerResult<string>> Handle(ConfirmPhoneCommand request, CancellationToken cancellationToken)
    {
        var currentUser = await accessProvider.GetUserAsync();
        var currentUserId = currentUser.Id;
        AccountUser? userModel = null;

        try
        {
            if (request.AccountUserId.HasValue)
            {
                currentUserId = request.AccountUserId.Value;
            }

            userModel = await unitOfWork.AccountUsersRepository.FirstOrDefaultAsync(x => x.Id == currentUserId);

            userModel.IsPhoneVerified = true;

            unitOfWork.AccountUsersRepository.Update(userModel);
            await unitOfWork.SaveAsync();

             var openIdModel = new SauriOpenIdControlUserModel
             {
                 AccountUserID = userModel.Id,
                 AccountID = userModel.Account.Id,
                 Login = userModel.Login,
                 Phone = userModel.PhoneNumber,
             };

            openIdProvider.ChangeUserProperties(openIdModel, userModel.Account.AccountConfiguration.Locale.Name);

            LogEventHelper.LogEvent(unitOfWork, currentUser?.RequestAccountId ?? userModel.AccountId, accessProvider, LogActions.SuccessEmailVerification, $"Пользователь {userModel.Login} {userModel.PhoneNumber} успешно верифицировал телефон");

            
            return "Ваш телефон успешно подтвержден.".ToOkManagerResult();
        }

        catch (Exception e)
        {
            handlerException.Handle(e, $"[Не удалось верифицировать телефон {userModel?.Email} {userModel?.PhoneNumber}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<string>(
                "Произошла ошибки при подтверждении телефона. Попробуйте позже");
        }
    }
}
