﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Handlers;

public class ConfirmPhoneNoCodeCommandHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IAccessProvider accessProvider)
    : IRequestHandler<ConfirmPhoneNoCodeCommand, ManagerResult<string>>
{
    public async Task<ManagerResult<string>> Handle(ConfirmPhoneNoCodeCommand request, CancellationToken cancellationToken)
    {
        AccountUser? userModel = null;

        try
        {

            userModel = await unitOfWork.AccountUsersRepository.FirstOrDefaultAsync(x => x.Id == request.AccountUserId);

            userModel.IsPhoneVerified = request.IsPhoneVerified;

            unitOfWork.AccountUsersRepository.Update(userModel);

            await unitOfWork.SaveAsync();
            var logmessage = request.IsPhoneVerified ? "верифицировал телефон" : "отменил верефикацию телефона";
            LogEventHelper.LogEvent(unitOfWork, userModel.AccountId, accessProvider, LogActions.SuccessEmailVerification, 
                $"Пользователь {userModel.Login} {userModel.PhoneNumber} успешно {logmessage}");

            if (!request.IsPhoneVerified) 
            {
                var emailNotificationSettings = await unitOfWork.NotificationSettingsRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Type == NotificationType.Email, cancellationToken);

                var userNotificationSettings = await unitOfWork.AccountUserNotificationSettingsRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.AccountUserId == userModel.Id
                                              && x.NotificationSettings.Type == NotificationType.Email, cancellationToken);

                if (userNotificationSettings == null)
                    unitOfWork.AccountUserNotificationSettingsRepository.Insert(new AccountUserNotificationSettings
                    { AccountUserId = userModel.Id, IsActive = true, NotificationSettingsId = emailNotificationSettings!.Id });
            }

            return request.IsPhoneVerified ? "Ваш телефон успешно подтвержден.".ToOkManagerResult() : "Подтверждение вашего номера телефона успешно отменено.".ToOkManagerResult();
        }

        catch (Exception e)
        {
            handlerException.Handle(e, $"[Не удалось верифицировать телефон {userModel?.Email} {userModel?.PhoneNumber}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<string>(
                "Произошла ошибки при подтверждении телефона. Попробуйте позже");
        }
    }
}
