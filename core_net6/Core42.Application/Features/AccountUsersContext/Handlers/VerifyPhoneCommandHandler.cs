﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AlphaNumericsSupport;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.SmsClient;
using Core42.Application.Features.AccountUsersContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Handlers;

public class VerifyPhoneCommandHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IAccessProvider accessProvider,
    ISmsClient smsClient)
    : IRequestHandler<VerifyPhoneCommand, ManagerResult<string>>
{
    public async Task<ManagerResult<string>> Handle(VerifyPhoneCommand request, CancellationToken cancellationToken)
    {
        var currentUser = await accessProvider.GetUserAsync();

        try
        {
            var alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
            var code = alphaNumeric42CloudsGenerator.GenerateConfirmationCode();
            var minutesToExpire = CloudConfigurationProvider.Phones.GetMinutesToExpireCode();

            var nowDate = DateTime.Now;

            var databaseModel = new EmailSmsVerification
            {
                AccountUserId = currentUser.Id,
                Code = code,
                ExpiresOn = nowDate.AddMinutes(int.Parse(minutesToExpire)),
                Type = VerificationType.Sms,
                CreatedOn = nowDate
            };

            unitOfWork.EmailSmsVerificationRepository.Insert(databaseModel);

            var oldestVerificationCodes = await unitOfWork.EmailSmsVerificationRepository
                .AsQueryable()
                .Where(x => x.Type == VerificationType.Sms && x.AccountUserId == currentUser.Id)
                .ToListAsync(cancellationToken);

            foreach (var oldVerificationCode in oldestVerificationCodes)
            {
                oldVerificationCode.ExpiresOn = nowDate;

                unitOfWork.EmailSmsVerificationRepository.Update(oldVerificationCode);
            }

            await unitOfWork.SaveAsync();

            await smsClient.SendSms(currentUser.PhoneNumber,
                $"Ваш код для верификации телефона: {code}");

            LogEventHelper.LogEvent(unitOfWork, currentUser.RequestAccountId, accessProvider, LogActions.SendPhoneVerificationCodeToUser, $"Пользователю {currentUser.Email} {currentUser.PhoneNumber} отправлено смс с кодом верификации");

            return $"Вам отправлена смс с кодом верификации на номер {currentUser.PhoneNumber}"
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, $"[Не удалось провести верификацию телефона пользователя с почтой {currentUser.Email} {currentUser.PhoneNumber}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<string>(
                "Ошибка отправки кода подтверждения телефона");
        }
    }
}
