﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Dtos;
using Core42.Application.Features.AccountUsersContext.Queries;
using Core42.Application.Features.AccountUsersContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Handlers
{
    /// <summary>
    /// Get filtered, sorted and paginated account users query handler
    /// </summary>
    public class GetFilteredAccountUsersQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredAccountUsersQuery, ManagerResult<PagedDto<AccountUserDto>>>
    {
        /// <summary>
        /// Handler get filtered, sorted and paginated account users query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<AccountUserDto>>> Handle(GetFilteredAccountUsersQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AccountUsers_View);
                
                var user = await accessProvider.GetUserAsync();

                return (await unitOfWork.AccountUsersRepository
                    .AsQueryableNoTracking()
                    .AutoFilter(request?.Filter)
                    .Where(AccountUserSpecification.ByUserInGroups(user) && 
                           AccountUserSpecification.ByIsActiveAndNotDeleted() && 
                           AccountUserSpecification.BySearchLine(request?.Filter?.SearchLine) && 
                           AccountUserSpecification.ByUserGroups(request?.Filter?.Groups))
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<AccountUserDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Retrieving account users data failed.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AccountUserDto>>(ex.Message);
            }
        }
    }
}
