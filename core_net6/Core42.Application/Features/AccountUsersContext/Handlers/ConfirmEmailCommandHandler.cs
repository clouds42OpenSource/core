﻿using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Handlers;

public class ConfirmEmailCommandHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IAccessProvider accessProvider,
    IOpenIdProvider openIdProvider)
    : IRequestHandler<ConfirmEmailCommand, ManagerResult<string>>
{
    public async Task<ManagerResult<string>> Handle(ConfirmEmailCommand request, CancellationToken cancellationToken)
    {
        var currentUser = await accessProvider.GetUserAsync();
        var currentUserId = currentUser?.Id;
        AccountUser? userModel = null;

        try
        {
            if (request.IsLinkClicked.HasValue && request.IsLinkClicked.Value)
            {
                currentUserId = (await unitOfWork.EmailSmsVerificationRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Code == request.Code && x.Type == VerificationType.Email,
                        cancellationToken))!.AccountUserId;
            }

            if (request.AccountUserId.HasValue)
            {
               currentUserId = request.AccountUserId.Value;
            }

            userModel = await unitOfWork.AccountUsersRepository
                .AsQueryable()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Locale)
                .FirstOrDefaultAsync(x => x.Id == currentUserId, cancellationToken) 
                        ?? throw new NotFoundException($"Не найден пользователь по идентификатору {currentUserId}");

            userModel.EmailStatus = "Checked";

            unitOfWork.AccountUsersRepository.Update(userModel);

            var emailNotificationSettings = await unitOfWork.NotificationSettingsRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Type == NotificationType.Email, cancellationToken);

            var userNotificationSettings = await unitOfWork.AccountUserNotificationSettingsRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.AccountUserId == userModel.Id 
                                          && x.NotificationSettings.Type == NotificationType.Email, cancellationToken);

            if(userNotificationSettings == null)
                unitOfWork.AccountUserNotificationSettingsRepository.Insert(new AccountUserNotificationSettings 
                    { AccountUserId = userModel.Id, IsActive = true, NotificationSettingsId = emailNotificationSettings!.Id });

            await unitOfWork.SaveAsync();

            LogEventHelper.LogEvent(unitOfWork, currentUser?.RequestAccountId 
                ?? userModel.AccountId, accessProvider, LogActions.SuccessEmailVerification, $"Пользователь {userModel.Login} {userModel.Email} успешно верифицировал почту");

            ActiveDirectoryUserFunctions.ChangeEmail(userModel.Email, userModel.Login);

            openIdProvider.ChangeUserProperties(new SauriOpenIdControlUserModel
            {
                AccountID = userModel.AccountId, 
                Email = userModel.Email,
                AccountUserID = userModel.Id
            }, userModel.Account.AccountConfiguration.Locale.Name);

           
            return "Ваш Email успешно подтвержден.".ToOkManagerResult();
        }

        catch (Exception e)
        {
            handlerException.Handle(e, $"[Не удалось верифицировать Email {userModel?.Email}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<string>("Произошла ошибки при подтверждении почты. Попробуйте позже");
        }
    }
}
