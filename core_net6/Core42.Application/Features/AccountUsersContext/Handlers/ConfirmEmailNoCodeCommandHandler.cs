﻿using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountUsersContext.Handlers;

public class ConfirmEmailNoCodeCommandHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IAccessProvider accessProvider,
    IOpenIdProvider openIdProvider)
    : IRequestHandler<ConfirmEmailNoCodeCommand, ManagerResult<string>>
{
    private readonly IOpenIdProvider _openIdProvider = openIdProvider;

    public async Task<ManagerResult<string>> Handle(ConfirmEmailNoCodeCommand request, CancellationToken cancellationToken)
    {
        AccountUser? userModel = null;

        try
        {

            userModel = await unitOfWork.AccountUsersRepository
                .AsQueryable()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Locale)
                .FirstOrDefaultAsync(x => x.Id == request.AccountUserId, cancellationToken) 
                        ?? throw new NotFoundException($"Не найден пользователь по идентификатору {request.AccountUserId}");
            if (request.IsEmailVerified)
            {
                userModel.EmailStatus = "Checked";

                unitOfWork.AccountUsersRepository.Update(userModel);

                var emailNotificationSettings = await unitOfWork.NotificationSettingsRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Type == NotificationType.Email, cancellationToken);

                var userNotificationSettings = await unitOfWork.AccountUserNotificationSettingsRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.AccountUserId == userModel.Id
                                              && x.NotificationSettings.Type == NotificationType.Email, cancellationToken);

                if (userNotificationSettings == null)
                    unitOfWork.AccountUserNotificationSettingsRepository.Insert(new AccountUserNotificationSettings
                    { AccountUserId = userModel.Id, IsActive = true, NotificationSettingsId = emailNotificationSettings!.Id });

                await unitOfWork.SaveAsync();

                LogEventHelper.LogEvent(unitOfWork, request.AccountUserId, accessProvider, LogActions.SuccessEmailVerification, $"Пользователь {userModel.Login} {userModel.Email} успешно верифицировал почту");

                ActiveDirectoryUserFunctions.ChangeEmail(userModel.Email, userModel.Login);

                return "Ваш Email успешно подтвержден.".ToOkManagerResult();
            }

            userModel.EmailStatus = "Unchecked";

            unitOfWork.AccountUsersRepository.Update(userModel);
            await unitOfWork.SaveAsync();

            LogEventHelper.LogEvent(unitOfWork, request.AccountUserId, accessProvider, LogActions.SuccessEmailVerification, $"Пользователь {userModel.Login} {userModel.Email} успешно отменил верифицикацию почты");

            ActiveDirectoryUserFunctions.ChangeEmail(null, userModel.Login);

            return "Для Вашего Email успешно отменено подтверждение.".ToOkManagerResult();

        }

        catch (Exception e)
        {
            handlerException.Handle(e, $"[Не удалось верифицировать Email {userModel?.Email}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<string>("Произошла ошибки при подтверждении почты. Попробуйте позже");
        }
    }
}
