﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using FluentValidation;

namespace Core42.Application.Features.AccountUsersContext.Validators;

public class ConfirmPhoneCommandValidator : AbstractValidator<ConfirmPhoneCommand>
{
    public ConfirmPhoneCommandValidator(IAccessProvider accessProvider, IUnitOfWork unitOfWork)
    {
        var currentUser = accessProvider.GetUser();
       
        RuleLevelCascadeMode = CascadeMode.Stop;

        When(x => x.AccountUserId.HasValue, () =>
        {
            AccountUser? accountUser = null;

            RuleFor(x => x.AccountUserId)
               .Must(_ => currentUser.Groups.Any(x => x is AccountUserGroup.AccountSaleManager or AccountUserGroup.CloudAdmin))
               .WithMessage("Недостаточно прав для выполнения операции")
               .DependentRules(() =>
               {
                   RuleFor(x => x.AccountUserId)
                       .Must(z =>
                       {
                               accountUser = unitOfWork.AccountUsersRepository
                                   .AsQueryableNoTracking()
                                   .FirstOrDefault(x => x.Id == z);

                               return accountUser != null;
                        })
                       .WithMessage(z => $"Не найден пользователь по идентификатору {z.AccountUserId}")
                       .DependentRules(() =>
                       {
                           RuleFor(z => z.AccountUserId)
                               .Must(x => !accountUser.IsPhoneVerified.HasValue || !accountUser.IsPhoneVerified.Value)
                               .WithMessage("Ваш телефон уже подтвержден");
                       });
               });
        })
        .Otherwise(() =>
        {
            var nowDate = DateTime.Now;
            EmailSmsVerification? confirmModel = null;
            RuleFor(x => x.PhoneConfirmed)
                .NotNull()
                .WithMessage("Не найден пользователь контекста")
                .Must(_ => !currentUser.IsPhoneVerified.HasValue || !currentUser.IsPhoneVerified.Value)
                .WithMessage("Ваш телефон уже подтвержден")
                .DependentRules(() =>
                {
                   RuleFor(x => x.Code)
                            .Must(z =>
                            {
                                confirmModel = unitOfWork.EmailSmsVerificationRepository
                                    .AsQueryable()
                                    .FirstOrDefault(x => x.AccountUserId == currentUser.Id && x.Code == z && x.Type == VerificationType.Sms);

                                return confirmModel != null;
                            })
                            .WithMessage("Вы ввели некорректный код подтверждения. Попробуйте ввести снова или запросите новый код.")
                            .DependentRules(() =>
                            {
                                RuleFor(x => x.IsExpired)
                                    .NotNull()
                                    .Must(_ => confirmModel?.ExpiresOn > nowDate)
                                    .WithMessage("Код подтверждения истек или уже использован. Попробуйте запросить новый код еще раз");
                            });
                 });

         });
}
}
