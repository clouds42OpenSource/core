﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using FluentValidation;
using PhoneNumbers;

namespace Core42.Application.Features.AccountUsersContext.Validators;

public class VerifyPhoneCommandValidator : AbstractValidator<VerifyPhoneCommand>
{
    private static readonly PhoneNumberUtil PhoneUtil = PhoneNumberUtil.GetInstance();
    public VerifyPhoneCommandValidator(IUnitOfWork unitOfWork, IAccessProvider accessProvider)
    {
        var currentUser = accessProvider.GetUser();
        var nowDate = DateTime.Now;
        var maxSendCount = int.Parse(CloudConfigurationProvider.Phones.GetVerificationCodeMaxSendCountInDay());
        var minutesToResendConfirmationCode = int.Parse(CloudConfigurationProvider.Phones.GetMinutesToResendCode());

        var lastSendConfirmationCode = unitOfWork.EmailSmsVerificationRepository
            .AsQueryableNoTracking()
            .OrderByDescending(x => x.CreatedOn)
            .FirstOrDefault(x =>
                x.AccountUserId == currentUser.Id && x.Type == VerificationType.Sms &&
                x.ExpiresOn > nowDate);

        var sendingCount = unitOfWork.EmailSmsVerificationRepository
            .AsQueryableNoTracking()
            .Count(x => x.AccountUserId == currentUser.Id && x.Type == VerificationType.Sms &&
                        (x.CreatedOn - nowDate.Date).Days == 0);

        var elapsedTime = (nowDate - (lastSendConfirmationCode?.CreatedOn ?? nowDate)).TotalMinutes;
    
        RuleFor(x => currentUser.PhoneNumber)
            .Must(x => IsValidLocale(x, currentUser))
            .WithMessage("Подтверждение телефона в вашем регионе недоступно")
            .Must(x => IsValidPhoneSignature(x, currentUser.Locale))
            .WithMessage("Неверный формат номера телефона. Проверьте правильность введенного номера. Поддерживаются только 'мобильные' номера.");
        
        RuleFor(x => x.PhoneConfirmed)
            .NotNull()
            .WithMessage("Не найден пользователь контекста")
            .Must(_ => !currentUser.IsPhoneVerified.HasValue || !currentUser.IsPhoneVerified.Value)
            .WithMessage("Ваш телефон уже подтвержден")
            .DependentRules(() =>
            {
                RuleFor(x => x.LeftSeconds)
                    .Must(_ =>
                    {
                        if (sendingCount < 2 || lastSendConfirmationCode == null)
                        {
                            return true;
                        }

                        return elapsedTime >= minutesToResendConfirmationCode;
                    })
                    .WithMessage(((int)((minutesToResendConfirmationCode - elapsedTime) * 60)).ToString());

                RuleFor(x => x.LimitCount)
                    .Must(_ => sendingCount < maxSendCount)
                    .WithMessage("Вы достигли лимита на сегодня");
            });
    }


    private static bool IsValidPhoneSignature(string phoneNumber, string locale)
    {
        PhoneNumber? number = locale switch
        {
            "ru-ru" => PhoneUtil.Parse(phoneNumber, "RU"),
            "ru-kz" => PhoneUtil.Parse(phoneNumber, "KZ"),
            _ => null
        };
        var type = PhoneUtil.GetNumberType(number);

        return PhoneUtil.IsValidNumber(number) && type == PhoneNumberType.MOBILE;
    }

    private static bool IsValidLocale(string _, IUserPrincipalDto user)
    {
        return user.Locale.Equals("ru-ru", StringComparison.InvariantCultureIgnoreCase) ||
               user.Locale.Equals("ru-kz", StringComparison.InvariantCultureIgnoreCase);
    }
}
