﻿using Core42.Application.Features.AccountUsersContext.Commands;
using Core42.Application.Features.AccountUserValidationContext.Queries;
using FluentValidation;
using MediatR;

namespace Core42.Application.Features.AccountUsersContext.Validators;

public class AccountUserAddCommandValidator: AbstractValidator<AccountUserAddCommand>
{
    private const string FullNameRegex = @"^[а-яА-ЯІіЇїЄєҐґёЁa-zA-Z ]+$";
    private const string AuthTokenRegex = @"[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}";
    private const string PasswordRegex = @"^[0-9A-Za-z_\\/\,.\-:;|!@#$]*$";

    public AccountUserAddCommandValidator(ISender sender)
    {
        RuleFor(x => x.Login)
            .NotNull()
            .WithMessage("Поле 'Логин' обязательно к заполнению")
            .MinimumLength(3)
            .WithMessage("Минимальная длина поля 'Логин' - 3 символа")
            .MaximumLength(20)
            .WithMessage("Максимальная длина поля 'Логин' - 20 символов")
            .Must(x => sender.Send(new CheckLoginQuery(x, null)).Result.Result)
            .WithMessage("Данный логин занят, попробуйте другой");

        When(x => x.AuthToken.HasValue, () =>
        {
            RuleFor(x => x.AuthToken.ToString())
                .Matches(AuthTokenRegex)
                .WithMessage("Недопустимое значение 'AuthToken'");
        });

        When(x => !string.IsNullOrWhiteSpace(x.LastName), () =>
        {
            RuleFor(x => x.LastName)
                .MaximumLength(30)
                .WithMessage("Допустимое количество символов для поля 'Фамилия' не более 30")
                .Matches(FullNameRegex)
                .WithMessage("В поле 'Фамилия' присутствуют недопустимые символы");
        });

        When(x => !string.IsNullOrWhiteSpace(x.FirstName), () =>
        {
            RuleFor(x => x.FirstName)
                .MaximumLength(30)
                .WithMessage("Допустимое количество символов для поля 'Имя' не более 30")
                .Matches(FullNameRegex)
                .WithMessage("В поле 'Имя' присутствуют недопустимые символы");
        });

        When(x => !string.IsNullOrWhiteSpace(x.MiddleName), () =>
        {
            RuleFor(x => x.MiddleName)
                .MaximumLength(30)
                .WithMessage("Допустимое количество символов для поля 'Отчество' не более 30")
                .Matches(FullNameRegex)
                .WithMessage("В поле 'Отчество' присутствуют недопустимые символы");
        });

        When(x => !string.IsNullOrWhiteSpace(x.CurrentPassword), () =>
        {
            RuleFor(x => x.CurrentPassword)
                .MaximumLength(25)
                .WithMessage("Допустимое количество символов для поля 'Текущий пароль' не более 25");
        });

        When(x => !string.IsNullOrWhiteSpace(x.Password), () =>
        {
            RuleFor(x => x.Password)
                .MinimumLength(7)
                .WithMessage("Минимальное количество символов для поля 'Новый пароль' не менее 7")
                .MaximumLength(25)
                .WithMessage("Допустимое количество символов для поля 'Новый пароль' не более 25")
                .Matches(PasswordRegex)
                .WithMessage("Поле 'Новый пароль' может содержать только латинские прописные\\заглавные буквы, цифры и спецсимволы");

            RuleFor(x => x.ConfirmPassword)
                .Equal(x => x.Password)
                .WithMessage("Поле 'Подверждение пароля' должно совпадать с полем 'Новый пароль'");
        });

        When(x => !string.IsNullOrWhiteSpace(x.PhoneNumber), () =>
        {
            RuleFor(x => x.PhoneNumber)
                .Must(x => sender.Send(new CheckPhoneQuery(null, x)).Result.Result)
                .WithMessage("Данный номер телефона уже используется, попробуйте другой");
        });
    }
}
