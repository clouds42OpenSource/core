﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using FluentValidation;

namespace Core42.Application.Features.AccountUsersContext.Validators;

public class ConfirmEmailCommandValidator : AbstractValidator<ConfirmEmailCommand>
{
    public ConfirmEmailCommandValidator(IAccessProvider accessProvider, IUnitOfWork unitOfWork)
    {
        var currentUser = accessProvider.GetUser();
       
        RuleLevelCascadeMode = CascadeMode.Stop;
        When(x => x.IsLinkClicked.HasValue && x.IsLinkClicked.Value, () =>
        {
            EmailSmsVerification? confirmModel = null;
            var nowDate = DateTime.Now;

            RuleFor(x => x.Code)
                .Must(z =>
                {
                    confirmModel = unitOfWork.EmailSmsVerificationRepository
                        .AsQueryableNoTracking()
                        .OrderByDescending(x => x.CreatedOn)
                        .FirstOrDefault(y => y.Code == z && y.Type == VerificationType.Email);

                    return confirmModel != null;
                })
                .WithMessage("Вы ввели некорректный код подтверждения. Попробуйте ввести снова или запросите новый код.")
                .DependentRules(() =>
                {
                    RuleFor(x => x.IsExpired)
                        .NotNull()
                        .Must(_ => confirmModel?.ExpiresOn > nowDate)
                        .WithMessage("Код подтверждения истек или уже использован. Попробуйте запросить новый код еще раз")
                        .DependentRules(() =>
                        {
                            RuleFor(x => x.AccountUserId)
                                .Must(_ =>
                                {
                                    var user = unitOfWork.AccountUsersRepository.AsQueryableNoTracking()
                                        .FirstOrDefault(z => z.Id == confirmModel.AccountUserId);

                                    return user.EmailStatus == "Unchecked";
                                })
                                .WithMessage("Ваш email уже подтвержден");
                        });
                });

        }).Otherwise(() =>
        {
            When(x => x.AccountUserId.HasValue, () =>
            {
                AccountUser? accountUser = null;

                RuleFor(x => x.AccountUserId)
                    .Must(_ => currentUser.Groups.Any(x =>
                        x is AccountUserGroup.AccountSaleManager or AccountUserGroup.CloudAdmin))
                    .WithMessage("Недостаточно прав для выполнения этой операции")
                    .DependentRules(() =>
                    {
                        RuleFor(x => x.AccountUserId)
                            .Must(z =>
                            {
                                accountUser = unitOfWork.AccountUsersRepository
                                    .AsQueryableNoTracking()
                                    .FirstOrDefault(x => x.Id == z);

                                return accountUser != null;
                            })
                            .WithMessage(z => $"Не найден пользователь по идентификатору {z.AccountUserId}")
                            .DependentRules(() =>
                            {
                                RuleFor(z => z.AccountUserId)
                                    .Must(_ => accountUser?.EmailStatus == "Unchecked")
                                    .WithMessage("Ваш email уже подтвержден");
                            });
                    });

            })
                .Otherwise(() =>
                {
                    EmailSmsVerification? confirmModel = null;
                    var nowDate = DateTime.Now;

                    RuleFor(x => x.EmailConfirmed)
                        .Must(_ => currentUser != null)
                        .WithMessage("Не найден пользователь контекста")
                        .Must(_ => !currentUser.IsEmailVerified.HasValue || !currentUser.IsEmailVerified.Value)
                        .WithMessage("Ваш email уже подтвержден")
                        .DependentRules(() =>
                        {
                            RuleFor(x => x.Code)
                                .Must(z =>
                                {
                                    confirmModel = unitOfWork.EmailSmsVerificationRepository
                                        .AsQueryable()
                                        .FirstOrDefault(x =>
                                            x.AccountUserId == currentUser.Id && x.Code == z &&
                                            x.Type == VerificationType.Email);

                                    return confirmModel != null;
                                })
                                .WithMessage(
                                    "Вы ввели некорректный код подтверждения. Попробуйте ввести снова или запросите новый код.")
                                .DependentRules(() =>
                                {
                                    RuleFor(x => x.IsExpired)
                                        .NotNull()
                                        .Must(_ => confirmModel?.ExpiresOn > nowDate)
                                        .WithMessage(
                                            "Код подтверждения истек или уже использован. Попробуйте запросить новый код еще раз");
                                });
                        });
                });
        });
    }
}
