﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountUsersContext.Commands;
using FluentValidation;

namespace Core42.Application.Features.AccountUsersContext.Validators;

public class VerifyEmailCommandValidator : AbstractValidator<VerifyEmailCommand>
{
    public VerifyEmailCommandValidator(IUnitOfWork unitOfWork, IAccessProvider accessProvider)
    {
        When(x => !x.AccountUserId.HasValue, () =>
        {
            var currentUser = accessProvider.GetUser();
            var nowDate = DateTime.Now;
            var maxSendCount = int.Parse(CloudConfigurationProvider.Emails.GetVerificationCodeMaxSendCountInDay());
            var minutesToResendConfirmationCode = int.Parse(CloudConfigurationProvider.Emails.GetMinutesToResendCode());

            var lastSendConfirmationCode = unitOfWork.EmailSmsVerificationRepository
                .AsQueryableNoTracking()
                .OrderByDescending(x => x.CreatedOn)
                .FirstOrDefault(x =>
                    x.AccountUserId == currentUser.Id && x.Type == VerificationType.Email &&
                    x.ExpiresOn > nowDate);

            var sendingCount = unitOfWork.EmailSmsVerificationRepository
                .AsQueryableNoTracking()
                .Count(x => x.AccountUserId == currentUser.Id && x.Type == VerificationType.Email &&
                            (x.CreatedOn - nowDate.Date).Days == 0);

            var elapsedTime = (nowDate - (lastSendConfirmationCode?.CreatedOn ?? nowDate)).TotalMinutes;

            RuleFor(x => x.EmailConfirmed)
                .NotNull()
                .WithMessage("Не найден пользователь контекста")
                .Must(_ => !currentUser.IsEmailVerified.HasValue || !currentUser.IsEmailVerified.Value)
                .WithMessage("Ваш Email уже подтвержден")
                .DependentRules(() =>
                {
                    RuleFor(x => x.LeftSeconds)
                        .Must(_ =>
                        {
                            if (sendingCount < 2 || lastSendConfirmationCode == null)
                            {
                                return true;
                            }

                            return elapsedTime >= minutesToResendConfirmationCode;
                        })
                        .WithMessage(((int)((minutesToResendConfirmationCode - elapsedTime) * 60)).ToString());

                    RuleFor(x => x.LimitCount)
                        .Must(_ => sendingCount < maxSendCount)
                        .WithMessage("Вы достигли лимита на сегодня");
                });
        });

    }
}
