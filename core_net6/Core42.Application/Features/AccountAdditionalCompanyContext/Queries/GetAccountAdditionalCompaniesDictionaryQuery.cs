﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AccountAdditionalCompanyContext.Dtos;
using MediatR;

namespace Core42.Application.Features.AccountAdditionalCompanyContext.Queries;

public class GetAccountAdditionalCompaniesDictionaryQuery : IRequest<ManagerResult<List<AccountAdditionalCompanyDictionaryDto>>>
{
    public Guid AccountId { get; set; }
}
