﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountAdditionalCompanyContext.Dtos;
using Core42.Application.Features.AccountAdditionalCompanyContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AccountAdditionalCompanyContext.Handlers;

public class GetAccountAdditionalCompaniesDictionaryQueryHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger42,
    IMapper mapper) : IRequestHandler<
    GetAccountAdditionalCompaniesDictionaryQuery, ManagerResult<List<AccountAdditionalCompanyDictionaryDto>>>
{
    public async Task<ManagerResult<List<AccountAdditionalCompanyDictionaryDto>>> Handle(
        GetAccountAdditionalCompaniesDictionaryQuery request,
        CancellationToken cancellationToken)
    {

        try
        {
            return (await unitOfWork.AccountAdditionalCompanyRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.AccountId == request.AccountId)
                    .ProjectTo<AccountAdditionalCompanyDictionaryDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            logger42.Error(ex, "Произошла ошибка при получении списка ключ-значение доп. организации");

            return ManagerResultHelper.PreconditionFailed<List<AccountAdditionalCompanyDictionaryDto>>(null,
                "Произошла ошибка при получении ключ-значения доп.организаций");
        }
    }
}
