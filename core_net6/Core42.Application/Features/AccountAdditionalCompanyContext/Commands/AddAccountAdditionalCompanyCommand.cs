﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountAdditionalCompanyContext.Dtos;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.AccountAdditionalCompanyContext.Commands
{
    public class AddAccountAdditionalCompanyCommand: IRequest<ManagerResult<AccountAdditionalCompanyDto>>
    {
        public string Name { get; set; }
        public string Inn { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public Guid AccountId { get; set; }
    }

    public class AddAccountAdditionalCompanyCommandHandler(
        IUnitOfWork unitOfWork,
        ILogger42 logger42,
        IMapper mapper,
        IAccessProvider accessProvider)
        : IRequestHandler<AddAccountAdditionalCompanyCommand, ManagerResult<AccountAdditionalCompanyDto>>
    {
        public async Task<ManagerResult<AccountAdditionalCompanyDto>> Handle(AddAccountAdditionalCompanyCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var account = await unitOfWork.AccountsRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == request.AccountId, cancellationToken);

                if (account == null)
                {
                    return ManagerResultHelper.PreconditionFailed<AccountAdditionalCompanyDto>(null,
                        "Не найден аккаут для создания доп организации");
                }

                var additionalCompany = new AccountAdditionalCompany
                {
                    Id = Guid.NewGuid(),
                    AccountId = request.AccountId,
                    Name = request.Name,
                    Number = request.Number,
                    Description = request.Description,
                    Email = request.Email,
                    Inn = request.Inn,
                };

                unitOfWork.AccountAdditionalCompanyRepository.Insert(additionalCompany);
                
                await unitOfWork.SaveAsync();

                LogEventHelper.LogEvent(unitOfWork,
                    request.AccountId,
                    accessProvider,
                    LogActions.CreateAdditionalCompany,
                    $"Пользователь добавил доп организацию {request.Name}");

                return ManagerResultHelper.Ok(mapper.Map<AccountAdditionalCompanyDto>(additionalCompany), "Доп. организация успешно создана");
            }

            catch (Exception ex)
            {
                logger42.Error(ex, "Ошибка при создании доп. организации");

                return ManagerResultHelper.PreconditionFailed<AccountAdditionalCompanyDto>(null, "Произошла ошибка при попытке создать доп.организацию");
            }
        }
    }
}
