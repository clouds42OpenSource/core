﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AccountAdditionalCompanyContext.Dtos;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.AccountAdditionalCompanyContext.Commands
{
    public class UpdateAccountAdditionalCompanyCommand: AccountAdditionalCompanyDto, IRequest<ManagerResult<AccountAdditionalCompanyDto>>
    {
    }

    public class UpdateAccountAdditionalCompanyCommandHandler(
        IUnitOfWork unitOfWork,
        ILogger42 logger42,
        IMapper mapper,
        IAccessProvider accessProvider)
        : IRequestHandler<UpdateAccountAdditionalCompanyCommand,
            ManagerResult<AccountAdditionalCompanyDto>>
    {
        public async Task<ManagerResult<AccountAdditionalCompanyDto>> Handle(UpdateAccountAdditionalCompanyCommand request, CancellationToken cancellationToken)
        {

            try
            {
                var existsAdditionalCompany = await unitOfWork.AccountAdditionalCompanyRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                if (existsAdditionalCompany == null)
                {
                    return ManagerResultHelper.PreconditionFailed<AccountAdditionalCompanyDto>(null,
                        "Не найдена доп организация для редактирования");
                }

                existsAdditionalCompany.Description = request.Description;
                existsAdditionalCompany.Email = request.Email;
                existsAdditionalCompany.Inn = request.Inn;
                existsAdditionalCompany.Name = request.Name;
                existsAdditionalCompany.Number = request.Number;


             

                unitOfWork.AccountAdditionalCompanyRepository.Update(existsAdditionalCompany);

                await unitOfWork.SaveAsync();

                LogEventHelper.LogEvent(unitOfWork,
                    request.AccountId,
                    accessProvider,
                    LogActions.EditAdditionalCompany,
                    "Пользователь отредактировал доп организацию");

                return ManagerResultHelper.Ok(mapper.Map<AccountAdditionalCompanyDto>(existsAdditionalCompany), "Доп организация успешно отредактирована");
            }

            catch (Exception e)
            {
               logger42.Error(e, "Ошибка при редактировании доп организации");

               return ManagerResultHelper.PreconditionFailed<AccountAdditionalCompanyDto>(null, "Ошибка при редактировании доп. организации");
            }
        }
    }
}
