﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core42.Application.Features.AccountAdditionalCompanyContext.Commands
{
    public class DeleteAccountAdditionalCompanyCommand: IRequest<ManagerResult>
    {
        public Guid AccountId { get; set; }
        public Guid Id { get; set; }
    }

    public class DeleteAccountAdditionalCompanyCommandHandler(
        IUnitOfWork unitOfWork,
        ILogger42 logger42,
        IAccessProvider accessProvider)
        : IRequestHandler<DeleteAccountAdditionalCompanyCommand, ManagerResult>
    {
        public async Task<ManagerResult> Handle(DeleteAccountAdditionalCompanyCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var additionalCompany = await unitOfWork.AccountAdditionalCompanyRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

                if (additionalCompany == null)
                {
                    return ManagerResultHelper.PreconditionFailed("Не удалось найти доп.организацию для удаления");
                }

                if (additionalCompany.AccountId != request.AccountId)
                {
                    return ManagerResultHelper.PreconditionFailed("Не удается удалить доп.организацию. Доп. организация принадлежит другому аккаунту");
                }
                unitOfWork.AccountAdditionalCompanyRepository.Delete(additionalCompany);
                await unitOfWork.SaveAsync();

                LogEventHelper.LogEvent(unitOfWork,
                    request.AccountId,
                    accessProvider,
                    LogActions.DeleteAdditionalCompany,
                    $"Пользователь удалил доп организацию {additionalCompany.Name}");

                return ManagerResultHelper.Ok("Успешно удалена доп. организация");
            }

            catch (Exception e)
            {
                logger42.Error(e, "Не удалось удалить доп. организацию");

                return ManagerResultHelper.PreconditionFailed("Произошла ошибка при попытке удалить доп.организацию");
            }
        }
    }
}
