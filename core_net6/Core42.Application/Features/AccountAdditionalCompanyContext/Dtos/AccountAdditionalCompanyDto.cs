﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AccountAdditionalCompanyContext.Dtos;

public class AccountAdditionalCompanyDto : IMapFrom<AccountAdditionalCompany>
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Inn { get; set; }
    public string Number { get; set; }
    public string Description { get; set; }
    public string Email { get; set; }
    public Guid AccountId { get; set; }

    public void Mapping(Profile profile)
    {
        profile
            .CreateMap<AccountAdditionalCompany, AccountAdditionalCompanyDto>()
            .ReverseMap();
    }
}
