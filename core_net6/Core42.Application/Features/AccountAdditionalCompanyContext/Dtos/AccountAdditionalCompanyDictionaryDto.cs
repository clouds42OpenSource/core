﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AccountAdditionalCompanyContext.Dtos;

public class AccountAdditionalCompanyDictionaryDto : DictionaryDto<Guid, string>, IMapFrom<AccountAdditionalCompany>
{
    public void Mapping(Profile profile)
    {
        profile.CreateMap<AccountAdditionalCompany, AccountAdditionalCompanyDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.Id))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));
    }
}
