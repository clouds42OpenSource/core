﻿namespace Core42.Application.Features.DepartmentsContext;

public class DepartmentsDto
{
    public Guid DepartmentId { get; set; }

    public string Name { get; set; }
}
