﻿using Clouds42.Domain.DataModels;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Specifications
{
    /// <summary>
    /// Delimiter source account database specification
    /// </summary>
    public static class DelimiterSourceAccountDatabaseSpecification
    {
        /// <summary>
        /// Filter delimiter source account database by search line
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<DelimiterSourceAccountDatabase> BySearchLine(string? searchLine)
        {
            return new(x => string.IsNullOrEmpty(searchLine) || x.DbTemplateDelimiterCode.Contains(searchLine) ||
                                     x.AccountDatabase.V82Name.Contains(searchLine) ||
                                     x.DatabaseOnDelimitersPublicationAddress.Contains(searchLine));
        }
    }
}
