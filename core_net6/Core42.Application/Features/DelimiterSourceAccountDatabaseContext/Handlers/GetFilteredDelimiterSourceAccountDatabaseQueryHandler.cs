﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Dtos;
using Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Queries;
using Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Handlers
{

    /// <summary>
    /// Get filtered delimiter source account database query handler
    /// </summary>
    public class GetFilteredDelimiterSourceAccountDatabaseQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : IRequestHandler<GetFilteredDelimiterSourceAccountDatabaseQuery,
            ManagerResult<PagedDto<DelimiterSourceAccountDatabaseDto>>>
    {
        /// <summary>
        /// Handle get filtered delimiter source account database query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<DelimiterSourceAccountDatabaseDto>>> Handle(GetFilteredDelimiterSourceAccountDatabaseQuery request, CancellationToken cancellationToken)
        {

            try
            {
                accessProvider.HasAccess(ObjectAction.DelimiterSourceAccountDatabase_GetData);

                return (await unitOfWork.DelimiterSourceAccountDatabaseRepository
                    .AsQueryable()
                    .Where(DelimiterSourceAccountDatabaseSpecification.BySearchLine(request?.Filter?.SearchLine))
                    .AutoFilter(request.Filter)
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<DelimiterSourceAccountDatabaseDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Получение данных материнских баз разделителей завершилось ошибкой]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<DelimiterSourceAccountDatabaseDto>>(ex.Message);
            }
        }
    }
}
