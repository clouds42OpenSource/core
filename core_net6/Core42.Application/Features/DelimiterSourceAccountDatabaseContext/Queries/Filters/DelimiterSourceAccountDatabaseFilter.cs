﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Queries.Filters
{
    /// <summary>
    /// Delimiter source account database filter
    /// </summary>
    public class DelimiterSourceAccountDatabaseFilter : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }
        public Guid? AccountDatabaseId { get; set; }
    }
}
