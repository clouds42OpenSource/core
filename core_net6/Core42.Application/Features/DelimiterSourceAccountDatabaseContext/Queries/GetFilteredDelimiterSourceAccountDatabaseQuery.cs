﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Dtos;
using Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Queries
{
    /// <summary>
    /// Get filtered delimiter source account database query
    /// </summary>
    public class GetFilteredDelimiterSourceAccountDatabaseQuery : IRequest<ManagerResult<PagedDto<DelimiterSourceAccountDatabaseDto>>>, ISortedQuery, IPagedQuery, IHasFilter<DelimiterSourceAccountDatabaseFilter>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(DelimiterSourceAccountDatabase.DbTemplateDelimiterCode)}.asc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Delimiter source account database filter
        /// </summary>
        public DelimiterSourceAccountDatabaseFilter? Filter { get; set; }
    }
}
