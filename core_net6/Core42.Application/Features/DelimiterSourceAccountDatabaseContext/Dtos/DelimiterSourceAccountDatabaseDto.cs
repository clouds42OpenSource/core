﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Dtos
{
    /// <summary>
    /// Delimiter source account database dto
    /// </summary>
    public class DelimiterSourceAccountDatabaseDto : IMapFrom<DelimiterSourceAccountDatabase>
    {
        /// <summary>
        ///  Information database identifier
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Database on delimiters configuration code
        /// </summary>
        public string DbTemplateDelimiterCode { get; set; }

        /// <summary>
        /// Database on delimiters publication address
        /// </summary>
        public string DatabaseOnDelimitersPublicationAddress { get; set; }

        /// <summary>
        /// Information database name
        /// </summary>
        public string AccountDatabaseV82Name { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DelimiterSourceAccountDatabase, DelimiterSourceAccountDatabaseDto>()
                .ForMember(x => x.AccountDatabaseV82Name, z => z.MapFrom(y => y.AccountDatabase.V82Name));
        }
    }
}
