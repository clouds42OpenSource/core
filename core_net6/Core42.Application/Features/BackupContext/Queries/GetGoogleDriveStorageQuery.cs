﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BackupContext.Dtos;
using MediatR;

namespace Core42.Application.Features.BackupContext.Queries
{
    /// <summary>
    /// Query to get Google Disk storage path for backups
    /// </summary>
    public class GetGoogleDriveStorageQuery : IRequest<ManagerResult<GoogleBackupDto>>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }
    }
}
