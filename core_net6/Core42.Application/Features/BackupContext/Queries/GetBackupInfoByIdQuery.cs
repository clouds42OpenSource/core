﻿using MediatR;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;

namespace Core42.Application.Features.BackupContext.Queries
{
    /// <summary>
    /// Query to get the backup information
    /// </summary>
    public class GetBackupInfoByIdQuery(Guid backupId) : IRequest<ManagerResult<BackupRestoringDto>>
    {
        /// <summary>
        /// Backup identifier
        /// </summary>
        [Required]
        public Guid BackupId { get; set; } = backupId;
    }
}
