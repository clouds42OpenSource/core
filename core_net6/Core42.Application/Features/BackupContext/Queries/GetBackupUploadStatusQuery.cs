﻿using System.ComponentModel.DataAnnotations;
using Clouds42.AccountDatabase.Contracts.Backup.Models;
using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.BackupContext.Queries
{
    /// <summary>
    /// Query to get uploaded backup file status
    /// </summary>
    public class GetBackupUploadStatusQuery : IRequest<ManagerResult<UploadedFileInfoDto>>
    {
        /// <summary>
        /// Uploaded backup file identifier
        /// </summary>
        [Required]
        public Guid UploadedFileId { get; set; }
    }
}
