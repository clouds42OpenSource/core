﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BackupContext.Dtos;
using Core42.Application.Features.BackupContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BackupContext.Handlers
{
    public class GetGoogleDriveStorageQueryHandler(
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IUpdateAccountConfigurationProvider updateAccountConfigurationProvider)
        : IRequestHandler<GetGoogleDriveStorageQuery, ManagerResult<GoogleBackupDto>>
    {
        /// <summary>
        /// Handle query to get account Google Drive path for storage backups
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<GoogleBackupDto>> Handle(GetGoogleDriveStorageQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var account = await unitOfWork.AccountsRepository
                                  .AsQueryable()
                                  .AsNoTracking()
                                  .Include(x => x.AccountConfiguration)
                                  .FirstOrDefaultAsync(acc => acc.Id == request.AccountId, cancellationToken)
                    ?? throw new NotFoundException("Указанного аккаунта не существует");
                
                var companyName = $"company_{account.IndexNumber}";

                updateAccountConfigurationProvider.UpdateCloudStorageWebLink(account.Id, companyName);

                var bucketName = CloudConfigurationProvider.Tomb.GetBucketName();
                var cloudLink = CloudConfigurationProvider.Tomb.GetCloudLink();

                return new GoogleBackupDto
                {
                    BucketName = bucketName, 
                    CloudStorageWebLink = string.Format(cloudLink, bucketName, companyName),
                    CompanyName = companyName
                }.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Во время получения пути к хранилищу бэкапов на Google-Диске произошла ошибка]. Аккаунт {request.AccountId}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<GoogleBackupDto>(ex.Message);
            }
        }
    }
}
