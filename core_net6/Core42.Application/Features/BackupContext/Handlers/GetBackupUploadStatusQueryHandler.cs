﻿using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Clouds42.AccountDatabase.Contracts.Backup.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BackupContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BackupContext.Handlers
{
    /// <summary>
    /// Handler to get uploaded backup file status
    /// </summary>
    public class GetBackupUploadStatusQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : IRequestHandler<GetBackupUploadStatusQuery, ManagerResult<UploadedFileInfoDto>>
    {
        /// <summary>
        /// Handle query to get uploaded backup file status
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<UploadedFileInfoDto>> Handle(GetBackupUploadStatusQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var uploadedFile = await unitOfWork.UploadedFileRepository.AsQueryable()
                    .FirstOrDefaultAsync(file => file.Id == request.UploadedFileId, cancellationToken)
                    ?? throw new NotFoundException("Не удалось найти загружаемый файл бэкапа");

                accessProvider.HasAccess(ObjectAction.AccountDatabase_Restore, () => uploadedFile?.AccountId);

                return new UploadedFileInfoDto
                {
                    UploadedFileStatus = uploadedFile.Status,
                    StatusComment = (uploadedFile.Status.GetType().GetMember(uploadedFile.Status.ToString()).First()?.
                        GetCustomAttribute(typeof(DescriptionAttribute), false) as DescriptionAttribute)?.Description
                }.ToOkManagerResult();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                   $"[Ошибка проверки статуса загрузки бэкапа ИБ]. При проверке статуса загрузки файла (Id={request.UploadedFileId}) бэкапа инф. базы произошла ошибка.");
                return PagedListExtensions.ToPreconditionFailedManagerResult<UploadedFileInfoDto>(ex.Message);
            }
        }
    }
}
