﻿using Microsoft.EntityFrameworkCore;
using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BackupContext.Queries;
using MediatR;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BackupContext.Handlers
{
    /// <summary>
    /// Handler for getting backup info by id
    /// </summary>
    public class GetBackupInfoByIdQueryHandler(
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        IConfiguration configuration,
        IBackupProvider backupProvider)
        : IRequestHandler<GetBackupInfoByIdQuery, ManagerResult<BackupRestoringDto>>
    {
        /// <summary>
        /// Handle request to get the backup information
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<BackupRestoringDto>> Handle(GetBackupInfoByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var accountId = (await unitOfWork.AccountDatabaseBackupRepository
                                    .AsQueryableNoTracking()
                                    .Include(backup => backup.AccountDatabase)
                                    .FirstOrDefaultAsync(backup => backup.Id == request.BackupId, cancellationToken))?.AccountDatabase?.AccountId
                                        ?? throw new NotFoundException("Указанный бэкап не привязан ни к какому аккаунту");

                CheckPermissions(accountId);

                var data = backupProvider.GetDataForRestoreAccountDatabase(request.BackupId);

                data.UploadFilesUri = (await unitOfWork.AccountsRepository
                                        .AsQueryable()
                                        .Where(acc => acc.Id == accountId)
                                        .Include(acc => acc.AccountConfiguration)
                                        .ThenInclude(x => x.Segment)
                                        .Select(acc => acc.AccountConfiguration.Segment)
                                        .FirstOrDefaultAsync(cancellationToken))?.CoreHosting?.UploadApiUrl
                                         ?? throw new NotFoundException($"Не удалось получить конфигурацию учетной записи по сегменту (account ID '{accountId}')");
                
                var contextAccountId = accessProvider.ContextAccountId;

                data.CurrencyName = (await unitOfWork.AccountConfigurationRepository
                                        .AsQueryableNoTracking()
                                        .FirstOrDefaultAsync(a => a.AccountId == contextAccountId, cancellationToken))?.Locale?.Currency
                                           ?? (await unitOfWork.LocaleRepository
                                               .AsQueryableNoTracking()
                                               .FirstOrDefaultAsync(loc => loc.ID == Guid.Parse(configuration["DefaultLocale"]!), cancellationToken))?.Currency;

                data.MyDatabasesServiceId =
                    (await unitOfWork.BillingServiceRepository
                        .AsQueryableNoTracking()
                        .FirstOrDefaultAsync(s => s.SystemService == Clouds42Service.MyDatabases, cancellationToken))?.Id ?? throw new NotFoundException("Не удалось получить идентификатор для сервиса \"Мои информационные базы\"");

                LogEventHelper.LogEvent(unitOfWork,contextAccountId, accessProvider, LogActions.RestoreAccountDatabase,
                   $"База {data.AccountDatabaseName} поставлена в очередь на скачивание архива.");

                return data.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                var message = $"[Произошла ошибка при получении данных для восстановления информационной базы из резервной копии] ({request.BackupId})";
                handlerException.Handle(ex, message);
                return PagedListExtensions.ToPreconditionFailedManagerResult<BackupRestoringDto>(ex.Message);
            }
        }

        /// <summary>
        /// Check permissions for handler action
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        private void CheckPermissions(Guid? accountId)
        {
            accessProvider.HasAccess(ObjectAction.AccountDatabase_Restore, () => accountId);
            accessProvider.HasAccess(ObjectAction.AccountDatabases_Add, () => accountId);
            accessProvider.HasAccess(ObjectAction.AccountUsers_View, () => accountId);
        }
    }
}
