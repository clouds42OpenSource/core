﻿namespace Core42.Application.Features.BackupContext.Dtos
{
    public class GoogleBackupDto
    {
        public string BucketName { get; set; }
        public string CloudStorageWebLink { get; set; }
        public string CompanyName { get; set; }
    }
}
