﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.ArticleBillingContext.Queries
{
    public class GetTransferBalanseQuery : IRequest<ManagerResult<decimal>>
    {
        public int? Sum { get; set; }
        public Guid AccountUserId { get; set; }
    }
}
