﻿using Core42.Application.Features.ArticleBillingContext.Dtos;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ArticleBillingContext.Queries.Specifications
{
    public static class ArticleTransactionSpecification
    {
        public static Spec<ArticleTransactionDto> ByDate(DateTime? from, DateTime? to)
        {
            return new Spec<ArticleTransactionDto>(x =>
                (!from.HasValue || x.CreatedOn >= from.Value) && (!to.HasValue || x.CreatedOn <= to.Value));
        }

        public static Spec<ArticleTransactionDto> ByAccountUser(Guid? userId)
        {
            return new Spec<ArticleTransactionDto>(x =>
                x.AccountUserId == userId);
        }
    }
}
