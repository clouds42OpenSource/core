﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing;
using Core42.Application.Features.ArticleBillingContext.Dtos;
using Core42.Application.Features.ArticleBillingContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleBillingContext.Queries
{
    public class GetTransactionArticlesQuery : IRequest<ManagerResult<PagedDto<ArticleTransactionDto>>>, ISortedQuery, IPagedQuery
    {
        public ArticleTransactionFilter? Filter { get; set; }
        public string OrderBy { get; set; } = $"{nameof(ArticleTransaction.CreatedOn)}.desc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
}
