﻿using Clouds42.Domain.Enums;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ArticleBillingContext.Queries.Filters;

public class ArticleTransactionFilter : IQueryFilter
{
    public ArticleTransactionType? Type { get; set; }
    public DateTime? CreatedFrom { get; set; }
    public DateTime? CreatedTo { get; set; }
    public Guid? AccountUserId { get; set; }
    public string Topic { get; set; }
}
