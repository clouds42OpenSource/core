﻿using AutoMapper;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ArticleBillingContext.Dtos;

public class ArticleTransactionDto : IMapFrom<ArticleTransaction>
{
    public int Id { get; set; }
    public Guid AccountUserId { get; set; }
    public string Topic { get; set; }
    public DateTime CreatedOn { get; set; }
    public ArticleTransactionCause TransactionCause { get; set; }
    public ArticleTransactionType TransactionType { get; set; }
    public decimal Amount { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<ArticleTransaction, ArticleTransactionDto>()
            .ForMember(x => x.Topic, z => z.MapFrom(y => y.Article.Topic))
            .ForMember(x => x.AccountUserId, z => z.MapFrom(y => y.Article.AccountUserId))
            .ForMember(x => x.TransactionCause, z => z.MapFrom(y => y.Cause))
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ArticleId));
        
    }
}
