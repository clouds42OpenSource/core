﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using MediatR;

namespace Core42.Application.Features.ArticleBillingContext.Commands
{
    public class CreateTransferBalanceCommand : IRequest<ManagerResult<string>>
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// ID реквизитов агента
        /// </summary>
        public Guid AgentRequisitesId { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public AgentCashOutRequestStatusEnum RequestStatus { get; set; }

        /// <summary>
        /// Общая сумма к выводу
        /// </summary>
        public decimal TotalSum { get; set; }

        /// <summary>
        /// Сумма к выплате
        /// </summary>
        public decimal PaySum { get; set; }

        /// <summary>
        /// Режим редактирования
        /// </summary>
        public bool IsEditMode { get; set; }
    }
}
