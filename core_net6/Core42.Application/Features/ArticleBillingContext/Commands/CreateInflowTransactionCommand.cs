﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using MediatR;

namespace Core42.Application.Features.ArticleBillingContext.Commands
{
    public class CreateInflowTransactionCommand : IRequest<ManagerResult<bool>>
    {
        public Article Article { get; set; }
        public ArticleTransactionCause Cause { get; set; }
        public decimal Amount { get; set; } 
    }
}
