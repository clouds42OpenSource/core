﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArticleBillingContext.Commands;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleBillingContext.Handlers
{
    public class CreateInflowTransactionCommandHandler(IUnitOfWork unitOfWork, IHandlerException handlerException)
        : IRequestHandler<CreateInflowTransactionCommand, ManagerResult<bool>>
    {
        public async Task<ManagerResult<bool>> Handle(CreateInflowTransactionCommand request, CancellationToken cancellationToken)
        {
            if (request.Amount <= 0)
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>("Сумма начисления должна быть больше 0.");

            try
            {
                request.Article.RewardAmount += request.Amount;
                var articleTransaction = new ArticleTransaction
                {
                    Id = new Guid(),
                    ArticleId = request.Article.Id,
                    Cause = request.Cause,
                    TransactionType = ArticleTransactionType.Inflow,
                    Amount = request.Amount,
                    CreatedOn = DateTime.Now
                };
                unitOfWork.ArticleTransactionRepository.Insert(articleTransaction);
                await unitOfWork.SaveAsync();

                return true.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Во время добавления транзакции за статью произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
            }
            
        }
    }
}
