﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Partner.Models;
using Core42.Application.Features.ArticleBillingContext.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleBillingContext.Handlers
{
    public class CreateTransferBalanceCommandHandler(
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : IRequestHandler<CreateTransferBalanceCommand, ManagerResult<string>>
    {
        readonly IHandlerException _handlerException = handlerException;

        public async Task<ManagerResult<string>> Handle(CreateTransferBalanceCommand request, CancellationToken cancellationToken)
        {
            logger.Trace($"Создание заявки на вывод средств для агента {request.AgentRequisitesId} на сумму {request.PaySum} и пользователя {request.AccountUserId}.");

            var validateResult = ValidateAgentCashOutRequest(request);

            if (validateResult.Any())
            {
                var message = "Ошибки в форме создания заявки :";
                validateResult.ForEach(w => { message += $"</br> {w.Message}"; });
                return PagedListExtensions.ToPreconditionFailedManagerResult<string>(message);

            }

            try
            {
    
                var user = await unitOfWork.AccountUsersRepository
                .AsQueryable()
                .Include(x=>x.Account)
                .FirstOrDefaultAsync(x => x.Id == request.AccountUserId, cancellationToken) 
                ?? throw new NotFoundException($"Не удалось найти пользователя по идентификатору {request.AccountUserId}"); ;

                logger.Trace($"Нашли пользователя {user.Login} и номер компании{user.Account.IndexNumber}.");

                var agentCashOutRequest = await unitOfWork.AgentCashOutRequestRepository
                .AsQueryable()
                .OrderByDescending(w => w.Number)
                .FirstOrDefaultAsync(x => x.AgentRequisitesId == request.AgentRequisitesId, cancellationToken);

                int number = agentCashOutRequest == null ? 1 : agentCashOutRequest.Number + 1;

                logger.Trace($"Номер счета на вабор  {number} .");

                var agentRequisites = await unitOfWork.AgentRequisitesRepository
                .AsQueryable()
                .Include(x => x.AccountOwner)
                .FirstOrDefaultAsync(x => x.AccountOwnerId == user.AccountId && x.Id == request.AgentRequisitesId, cancellationToken);

                logger.Trace($"Нашли реквезит номер {agentRequisites.Number} .");

                var agentCashOutRequestDto = new AgentCashOutRequest
                {
                    Id = Guid.NewGuid(),
                    AgentRequisitesId = request.AgentRequisitesId,
                    RequestStatus = request.RequestStatus,
                    CreationDateTime = DateTime.Now,
                    StatusDateTime = DateTime.Now,
                    RequestedSum = request.TotalSum,
                    AccountUserId = request.AccountUserId,
                    RequestNumber = string.Concat(user.Account.IndexNumber.ToString(), '-', agentRequisites.Number.ToString(), '-', number.ToString()),
                    Number = number,
                };
;
                logger.Trace($"Собрали модель для добавления {agentCashOutRequestDto.RequestNumber} .");

                unitOfWork.AgentCashOutRequestRepository.Insert(agentCashOutRequestDto);
                await unitOfWork.SaveAsync();
 
                logger.Trace($"Создание заявки на вывод средств для агента {request.AgentRequisitesId} на сумму {request.PaySum} завершено.");
                return agentCashOutRequestDto.RequestNumber.ToOkManagerResult();

            }
            catch (Exception ex)
            {
                logger.Trace($"Создание заявки на вывод средств для агента {request.AgentRequisitesId} на сумму {request.PaySum} завершено с ошибкой. Причина: {ex.Message}.");
                return PagedListExtensions.ToPreconditionFailedManagerResult<string>(ex.Message);
            }

            

        }
        /// <summary>
        /// Провалидировать модель создания заявки
        /// </summary>
        /// <param name="createAgentCashOutRequestDto">Модель создания заявки</param>
        /// <returns>Результат валидации</returns>
        private List<ValidateResult> ValidateAgentCashOutRequest(CreateTransferBalanceCommand createAgentCashOutRequestDto)
        {
            var validationResults = new List<ValidateResult>();

            if (createAgentCashOutRequestDto == null)
            {
                validationResults.Add(new ValidateResult { Success = false, Message = "Форма создания заявки пуста!" });
                return validationResults;
            }

            if (createAgentCashOutRequestDto.PaySum <= 0)
                validationResults.Add(new ValidateResult { Success = false, Message = "Сумма для вывода не может равняться 0!" });

            if (createAgentCashOutRequestDto.AgentRequisitesId == Guid.Empty)
                validationResults.Add(new ValidateResult { Success = false, Message = "Не выбран агентские реквизиты!" });

            if (createAgentCashOutRequestDto.RequestStatus == AgentCashOutRequestStatusEnum.Paid)
                validationResults.Add(new ValidateResult { Success = false, Message = $"Заявку со статусом 'Оплачена' создать невозможно!" });

            return validationResults;
        }
    }
}
