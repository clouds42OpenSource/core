﻿using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArticleBillingContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleBillingContext.Handlers
{
    public class GetArticleTransferBalanseRequestHandler(IUnitOfWork unitOfWork, IHandlerException handlerException)
        : IRequestHandler<GetTransferBalanseQuery, ManagerResult<decimal>>
    {
        public async Task<ManagerResult<decimal>> Handle(GetTransferBalanseQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var message = string.Empty;
                var articleWallets = await unitOfWork.ArticleWalletsRepository
                    .AsQueryable()
                    .FirstOrDefaultAsync(x => x.AccountUserId == request.AccountUserId, cancellationToken)
                    ?? throw new NotFoundException($"Кошелек агента {request.AccountUserId} не найден"); ;

                var activeAgentRequisites = unitOfWork.AgentRequisitesRepository.Where(w =>
                    w.AccountOwnerId == articleWallets.AccountUser.AccountId && w.AgentRequisitesStatus == AgentRequisitesStatusEnum.Verified).ToList();

                if (!activeAgentRequisites.Any())
                {
                    var siteUrl = Clouds42.Configurations.Configurations.CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
                    var routeValueForCreateRequisites = Clouds42.Configurations.Configurations.CloudConfigurationProvider.Cp.GetRouteValueForCreateAgentRequisites();
                    var linkForCreateRequisites = $"<a href='{siteUrl}/{routeValueForCreateRequisites}'>добавить</a>";
                    return PagedListExtensions.ToPreconditionFailedManagerResult<decimal>($"Для создания заявки на вывод необходимо {linkForCreateRequisites} реквизиты для выплаты");
                }

                if (articleWallets.AvailableSum < 1000)
                {
                    return PagedListExtensions.ToPreconditionFailedManagerResult<decimal>("- Для создания заявки необходима минимальная сумма - 1000 руб.");
                }

                if (articleWallets.AvailableSum <= 0)
                    return PagedListExtensions.ToPreconditionFailedManagerResult<decimal>($"Перевод баланса агента не возможен. " +
                        $"Доступно для перевода {articleWallets.AvailableSum:0.00} руб."); ;


                if (request.Sum != null && articleWallets.AvailableSum < request.Sum)
                    return PagedListExtensions.ToPreconditionFailedManagerResult<decimal>($"Перевод баланса агента не возможен. " +
                        $"Сумма перевода={request.Sum}. Доступно для перевода {articleWallets.AvailableSum:0.00} руб.");
 
                if (TryGetCashOutRequestNumberWhichIsProcessed(articleWallets.AccountUser.AccountId, articleWallets.AccountUserId, out var cashOutRequestNumber))
                    return PagedListExtensions.ToPreconditionFailedManagerResult<decimal>($@"- У Вас уже создана заявка № {cashOutRequestNumber}.
                                                            Создавать новые заявки Вы сможете после выполнения текущей.");


                return articleWallets.AvailableSum.ToOkManagerResult();

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Во время получения информации о доступной сумме вывода за статью произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<decimal>(ex.Message);
            }
            
        }

        /// <summary>
        /// Попробовать получить номер заявки на расходование средств
        /// которая находится в обработке
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="cashOutRequestNumber">Номер заявки</param>
        /// <returns>Результат поиска</returns>
        public bool TryGetCashOutRequestNumberWhichIsProcessed(Guid accountId, Guid accountUserId, out string cashOutRequestNumber)
        {
            cashOutRequestNumber = string.Empty;

            var inProcessAgentCashOutRequests = unitOfWork.AgentCashOutRequestRepository.Where(w =>
                w.AgentRequisites.AccountOwnerId == accountId && w.AccountUserId == accountUserId &&
                (w.RequestStatus == AgentCashOutRequestStatusEnum.InProcess ||
                 w.RequestStatus == AgentCashOutRequestStatusEnum.New)).ToList();

            if (!inProcessAgentCashOutRequests.Any())
                return false;

            cashOutRequestNumber = inProcessAgentCashOutRequests.Last().RequestNumber;

            return true;
        }
    }
}
