﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArticleBillingContext.Dtos;
using Core42.Application.Features.ArticleBillingContext.Queries;
using Core42.Application.Features.ArticleBillingContext.Queries.Specifications;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArticleBillingContext.Handlers
{
    public class GetTransactionInfoHandler(ILogger42 logger, IUnitOfWork unitOfWork, IMapper mapper)
        : IRequestHandler<GetTransactionArticlesQuery, ManagerResult<PagedDto<ArticleTransactionDto>>>
    {
        public async Task<ManagerResult<PagedDto<ArticleTransactionDto>>> Handle(GetTransactionArticlesQuery request, CancellationToken cancellationToken)
        {

            logger.Debug($"Значение которое пришло по пользователю {request.Filter?.AccountUserId}");

            try
            {
                return (await unitOfWork.ArticleTransactionRepository
                        .AsQueryableNoTracking()
                        .ProjectTo<ArticleTransactionDto>(mapper.ConfigurationProvider)
                        .AutoFilter(request.Filter)
                        .Where(ArticleTransactionSpecification.ByDate(request.Filter?.CreatedFrom, request.Filter?.CreatedTo) ||
                       ArticleTransactionSpecification.ByAccountUser(request.Filter?.AccountUserId))
                        .AutoSort(request)
                        .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка при получении данных по транзакциям за статьи");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<ArticleTransactionDto>>(
                    "Произошла ошибка при получении информации по транзакциям за статьи.");
            }
        }
    }
}
