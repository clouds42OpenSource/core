﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorker.BillingJobs.FinalizeYokassaPayments;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.YookassaContext.Commands
{
    public class PayWithQrCodeCommand : IRequest<ManagerResult<PaymentResultDto>>
    {
        /// <summary>
        /// Идентификатор аккаунта пользоватея
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal TotalSum { get; set; }
    }

    public class  PayWithQrCodeCommandHandler : IRequestHandler<PayWithQrCodeCommand, ManagerResult<PaymentResultDto>>
    {
        private readonly IAccessProvider _accessProvider;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly ICreateYookassaPaymentObjectProvider _createYookassaPaymentObjectProvider;
        private readonly FinalizeYookassaPaymentJobWrapper _finalizeYookassaPaymentJobWrapper;
        private readonly ILogger42 _logger;

        public PayWithQrCodeCommandHandler(IAccessProvider accessProvider, IUnitOfWork unitOfWork, IConfiguration configuration, ILogger42 logger, ICreateYookassaPaymentObjectProvider createYookassaPaymentObjectProvider, IRunTaskToCheckYookassaPaymentStatusProvider runTaskToCheckYookassaPaymentStatusProvider, FinalizeYookassaPaymentJobWrapper finalizeYookassaPaymentJobWrapper)
        {
            _accessProvider = accessProvider;
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            _logger = logger;
            _createYookassaPaymentObjectProvider = createYookassaPaymentObjectProvider;
            _finalizeYookassaPaymentJobWrapper = finalizeYookassaPaymentJobWrapper;
        }
        public async Task<ManagerResult<PaymentResultDto>> Handle(PayWithQrCodeCommand request, CancellationToken cancellationToken)
        {
            using var dbScope = _unitOfWork.SmartTransaction.Get();
            try
            {
                _accessProvider.HasAccess(ObjectAction.Payments_System, () => request.AccountId);

                var locale = await _unitOfWork.AccountsRepository.GetLocaleAsync(request.AccountId,
                    Guid.Parse(_configuration["DefaultLocale"]!));

                switch (locale.Name)
                {
                    case LocaleConst.Russia:
                    {
                       
                        var account = await _unitOfWork.BillingAccountRepository
                            .AsQueryableNoTracking()
                            .Include(x => x.Account)
                            .ThenInclude(x => x.AccountConfiguration)
                            .Include(x => x.Account)
                            .ThenInclude(x => x.AccountEmails)
                            .FirstOrDefaultAsync(x => x.Id == request.AccountId, cancellationToken) ?? 
                            throw new NotFoundException($"Не найден аккаунт биллинга {request.AccountId}");

                        var accountsEmail = _unitOfWork.AccountsRepository.GetEmails(account.Id);

                        var payment = await PrepareAndGetPaymentNumberAsync(account, request.TotalSum);
                            
                        var yookassaPaymentObjectDto = await _createYookassaPaymentObjectProvider.CreateQrAsync(
                            new YookassaPaymentObjectCreationDataDto
                            {
                                PaymentSum = request.TotalSum,
                                Description = $"Платеж №{payment.Number} Id компании {account.Account.IndexNumber}, Email {accountsEmail.FirstOrDefault()}",
                                IdempotencyKey = $"{payment.Id:N}",
                                SupplierId = account.Account.AccountConfiguration.SupplierId,
                                AggregatorPaymentMethodId = null,
                                IsAutoPay = false,
                                PaymentType = "spb"
                            }) ?? throw new InvalidOperationException($"Не удалось создать объект платежа ЮKassa, для системного платежа №{payment.Number}");

                        await UpdatePaymentAsync(payment, yookassaPaymentObjectDto);

                        _finalizeYookassaPaymentJobWrapper.Start(new FinalizeYookassaPaymentJobParamsDto
                        {
                            YookassaAgregatorPaymentId = yookassaPaymentObjectDto.Id
                        });

                        dbScope.Commit();
                        
                        return new PaymentResultDto
                        {
                            PaymentId = payment.Id,
                            TotalSum = request.TotalSum,
                            PaymentSystem = PaymentSystem.Yookassa.ToString(),
                            RedirectUrl = yookassaPaymentObjectDto?.PaymentConfirmationData?.ConfirmationUrl,
                            Type = yookassaPaymentObjectDto?.PaymentConfirmationData?.Type
                        }.ToOkManagerResult();
                    }
                    default:
                        throw new InvalidOperationException("Оплата по СБП(QR) доступна только пользователям РФ");
                }
            }

            catch (Exception ex)
            {
                dbScope.Rollback();

                _logger.Warn(ex, $"[Ошибка оплаты через по СБП(QR) с идентификатором аккаунта {request.AccountId}]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<PaymentResultDto>(ex.Message);
            }
           
        }

        private async Task<Payment> PrepareAndGetPaymentNumberAsync(BillingAccount account, decimal sum)
        {
            var payment = new Payment
            {
                AccountId = account.Id,
                Sum = sum,
                Status = PaymentStatus.Waiting.ToString(),
                PaymentSystem = PaymentSystem.Yookassa.ToString(),
                OriginDetails = $"{PaymentSystem.Yookassa} import",
                Date = DateTime.Now,
                Description = $"Ввод средств через платежную систему {PaymentSystem.Yookassa} СБП(QR)",
                OperationType = PaymentType.Inflow.ToString(),
                Id = Guid.NewGuid()
            };

            _unitOfWork.PaymentRepository.Insert(payment);
            await _unitOfWork.SaveAsync();

            _logger.Info($"Номер платежа ({payment.Number}), Успешно создан платеж для компании(ID) {account.Id} на сумму {sum} по системе {PaymentSystem.Yookassa} СБП(QR)");

            return payment;
        }

        private async Task UpdatePaymentAsync(Payment payment, YookassaPaymentObjectDto yookassaPaymentObjectDto)
        {
            payment.PaymentSystemTransactionId = yookassaPaymentObjectDto.Id.ToString();

            payment.ConfirmationRedirectUrl = yookassaPaymentObjectDto.PaymentConfirmationData?.ConfirmationUrl;
            payment.ConfirmationToken = yookassaPaymentObjectDto.PaymentConfirmationData?.ConfirmationToken;
            payment.ConfirmationType = yookassaPaymentObjectDto.PaymentConfirmationData?.Type;

            _unitOfWork.PaymentRepository.Update(payment);

            await _unitOfWork.SaveAsync();
        }
    }
}
