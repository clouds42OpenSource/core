﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BillingServiceContext.Dtos;
using MediatR;

namespace Core42.Application.Features.BillingServiceContext.Commands
{
    public class TryPayRent1CAccessCommand : IRequest<ManagerResult<UpdaterRent1CAccessDto>>
    {
         public List<AccessSettings> Accesses { get; set; }
    }
}
