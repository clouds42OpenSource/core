﻿using Core42.Application.Features.BillingServiceContext.Dtos;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.BillingServiceContext.Queries.Filters;

public class Market42ServiceFilter : IQueryFilter
{
    public Guid AccountId { get; set; }
    public string Cloud42ServiceKey { get; set; }
    public string Representation { get; set; }
    public Market42ServiceState? State { get; set; }
}
