﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.BillingServiceContext.Dtos;
using Core42.Application.Features.BillingServiceContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;

namespace Core42.Application.Features.BillingServiceContext.Queries
{
    /// <summary>
    /// Get market42 billing services query with filter and sorting
    /// </summary>
    public class GetMarker42BillingServicesQuery : IRequest<ManagerResult<Market42ServiceResultDto>>, ISortedQuery, IHasFilter<Market42ServiceFilter>
    {
        public string OrderBy { get; set; } = $"State.asc";
        public Market42ServiceFilter? Filter { get; set; }
    }
}
