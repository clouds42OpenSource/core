﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing;
using Core42.Application.Features.BillingServiceContext.Dtos;
using MediatR;

namespace Core42.Application.Features.BillingServiceContext.Queries
{
    public class GetOptimalRatesByAccountAndServiceTypesQuery : IRequest<ManagerResult<List<LicenseOfRent1CDto>>>
    {
        public DateTime? ExpireDate { get; set; }
        public List<BillingServiceType> ServiceTypes { get; set; }
        public List<LicenseOfRent1CDto> Licenses { get; set; }
    }
}
