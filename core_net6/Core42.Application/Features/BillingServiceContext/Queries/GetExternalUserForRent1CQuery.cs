﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BillingServiceContext.Dtos;
using MediatR;

namespace Core42.Application.Features.BillingServiceContext.Queries
{
    /// <summary>
    /// Get external user for rent 1c query
    /// </summary>
    public class GetExternalUserForRent1CQuery : IRequest<ManagerResult<LicenseOfRent1CDto>?>
    {
        public string Email { get; set; }
    }
}
