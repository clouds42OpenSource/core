﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.BillingServiceContext.Dtos;
using MediatR;

namespace Core42.Application.Features.BillingServiceContext.Queries
{
    /// <summary>
    /// Get rent 1C query
    /// </summary>
    public class GetRent1CInfoQuery : IRequest<ManagerResult<Rent1CInfoDto>>
    {
    }
}
