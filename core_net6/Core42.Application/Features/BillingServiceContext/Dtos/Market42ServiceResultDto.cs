﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core42.Application.Features.BillingServiceContext.Dtos;

[XmlRoot(ElementName = "Result")]
[DataContract(Name = "Result")]
public class Market42ServiceResultDto
{
    [XmlElement(ElementName = "Table")]
    [DataMember(Name = "Table")]
    public Market42ServiceListDto Services { get; set; } = new();
}
