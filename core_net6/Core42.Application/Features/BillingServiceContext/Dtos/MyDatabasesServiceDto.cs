﻿namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    /// <summary>
    /// My databases service dto
    /// </summary>
    public class MyDatabasesServiceDto 
    {
        /// <summary>
        /// Limit on free creation db
        /// </summary>
        public int? LimitOnFreeCreationDb { get; set; }

        /// <summary>
        /// Count databases over limit
        /// </summary>
        public int? CountDatabasesOverLimit { get; set; }

        /// <summary>
        /// Server databases count
        /// </summary>
        public int? ServerDatabasesCount { get; set; }

        /// <summary>
        /// Database placement cost
        /// </summary>
        public decimal? DatabasePlacementCost { get; set; }

        /// <summary>
        /// Server database placement cost
        /// </summary>
        public decimal? ServerDatabasePlacementCost { get; set; }

        /// <summary>
        /// Total amount for server databases
        /// </summary>
        public decimal? TotalAmountForServerDatabases { get; set; }

        /// <summary>
        /// Total amount for databases
        /// </summary>
        public decimal? TotalAmountForDatabases { get; set; }
    }
}
