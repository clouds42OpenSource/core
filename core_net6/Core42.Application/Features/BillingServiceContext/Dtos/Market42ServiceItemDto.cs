﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using AutoMapper;
using Clouds42.Domain.DataModels.billing;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.BillingServiceContext.Dtos;

[XmlRoot(ElementName = "Item")]
[DataContract(Name = "Item")]
public class Market42ServiceItemDto : IMapFrom<ResourcesConfiguration>
{
    [XmlElement(ElementName = "Key")]
    [DataMember(Name = "Key")]
    public string Cloud42ServiceKey { get; set; }

    [XmlElement(ElementName = nameof(Representation))]
    [DataMember(Name = nameof(Representation))]
    public string Representation { get; set; }

    [XmlElement(ElementName = nameof(State))]
    [DataMember(Name = nameof(State))]
    public Market42ServiceState State { get; set; }

    [XmlIgnore]
    [JsonIgnore]
    public Guid AccountId { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<ResourcesConfiguration, Market42ServiceItemDto>()
            .ForMember(x => x.Cloud42ServiceKey, z => z.MapFrom(y => y.BillingService.Key))
            .ForMember(x => x.AccountId, z => z.MapFrom(y => y.AccountId))
            .ForMember(x => x.Representation, z => z.MapFrom(y => y.BillingService.Name))
            .ForMember(x => x.State,
                z => z.MapFrom(y => 
                    !y.IsDemoPeriod && (y.Frozen == null || !y.Frozen.Value) ? Market42ServiceState.Enabled : 
                    y.IsDemoPeriod && y.Frozen != null && y.Frozen.Value ? Market42ServiceState.DemoDisabled : 
                    y.IsDemoPeriod && (y.Frozen == null || !y.Frozen.Value) ? Market42ServiceState.Demo: Market42ServiceState.Disabled));
    }
}
