﻿namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    /// <summary>
    /// Additional resources dto
    /// </summary>
    public class AdditionalAccountResourceDto
    {
        /// <summary>
        /// Additional resources name
        /// </summary>
        public string? AdditionalResourcesName { get; set; }

        /// <summary>
        /// Additional resources cost
        /// </summary>
        public decimal? AdditionalResourcesCost { get; set; }

        /// <summary>
        /// Need show additional resources flag
        /// </summary>
        public bool? NeedShowAdditionalResources { get; set; }
    }
}
