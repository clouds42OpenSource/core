﻿using AutoMapper;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    public class AccessSettings : IMapTo<UpdaterAccessRent1CRequestDto>
    {
        public Guid AccountUserId { get; set; }
        public Guid? WebResourceId { get; set; }
        public Guid? RdpResourceId { get; set; }
        public bool WebResource { get; set; }
        public bool StandartResource { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccessSettings, UpdaterAccessRent1CRequestDto>();
        }
    }
}
