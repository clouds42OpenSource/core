﻿namespace Core42.Application.Features.BillingServiceContext.Dtos;

public class ValidationDto(bool isSuccess, string? message = null)
{
    public bool IsSuccess { get; set; } = isSuccess;
    public string? Message { get; set; } = message;
}
