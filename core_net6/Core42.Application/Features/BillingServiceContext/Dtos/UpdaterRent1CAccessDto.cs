﻿namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    /// <summary>
    /// Rent 1C promise payment dto
    /// </summary>
    public class UpdaterRent1CAccessDto
    {
        /// <summary>
        /// Enough money to rent
        /// </summary>
        public decimal EnoughMoney { get; set; }

        /// <summary>
        /// Can get promise payment flag
        /// </summary>
        public bool CanGetPromisePayment { get; set; }

        /// <summary>
        /// Cost for pay
        /// </summary>
        public decimal CostForPay { get; set; }
    }
}
