﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core42.Application.Features.BillingServiceContext.Dtos;

[XmlRoot(ElementName = "Table")]
[DataContract(Name = "Table")]
public class Market42ServiceListDto
{
    [XmlAttribute(AttributeName = nameof(Type))]
    [DataMember(Name = nameof(Type))]
    public string Type { get; set; } = "Table";

    [XmlElement(ElementName = "Row")]
    [DataMember(Name = "Row")]
    public List<Market42ServiceItemDto> Items { get; set; } = [];
}
