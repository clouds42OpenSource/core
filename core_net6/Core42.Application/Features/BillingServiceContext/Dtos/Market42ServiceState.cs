﻿namespace Core42.Application.Features.BillingServiceContext.Dtos;

public enum Market42ServiceState
{
    Disabled = 0,
    Enabled = 1,
    Demo = 2,
    DemoDisabled = 3,
}
