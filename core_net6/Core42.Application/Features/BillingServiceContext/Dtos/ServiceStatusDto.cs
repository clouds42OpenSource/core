﻿using Clouds42.Domain.Enums;

namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    /// <summary>
    /// Service status info dto
    /// </summary>
    public class ServiceStatusDto
    {
        /// <summary>
        /// Service is locked flag
        /// </summary>
        public bool? ServiceIsLocked { get; set; }

        /// <summary>
        /// Service lock reason
        /// </summary>
        public string ServiceLockReason { get; set; }

        /// <summary>
        /// Service lock reason type
        /// </summary>
        public ServiceLockReasonType? ServiceLockReasonType { get; set; }

        /// <summary>
        /// Promised payment is active flag
        /// </summary>
        public bool? PromisedPaymentIsActive { get; set; }

        /// <summary>
        /// Promised payment sum
        /// </summary>
        public string PromisedPaymentSum { get; set; }

        /// <summary>
        /// Promised payment expire date
        /// </summary>
        public DateTime? PromisedPaymentExpireDate { get; set; }
    }
}
