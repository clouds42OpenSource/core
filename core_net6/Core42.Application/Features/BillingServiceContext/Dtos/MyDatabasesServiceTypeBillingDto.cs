﻿using AutoMapper;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using CommonLib.Enums;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    public class MyDatabasesServiceTypeBillingDto : IMapFrom<BillingServiceType>
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Описание услуги сервиса
        /// </summary>
        public string ServiceTypeName { get; set; }

        /// <summary>
        /// Описание услуги сервиса
        /// </summary>
        public string ServiceTypeDescription { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Системный тип услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// Тип системного сервиса 42clouds
        /// </summary>
        public Clouds42Service? Clouds42ServiceType { get; set; }

        /// <summary>
        /// Признак что сервис услуги активен
        /// </summary>
        public bool IsActiveService { get; set; }

        /// <summary>
        /// Стоимость одной лицензии услуги
        /// </summary>
        public decimal CostPerOneLicense { get; set; }

        /// <summary>
        /// Лимит на бесплатное количество ресурсов
        /// </summary>
        public int LimitOnFreeLicenses { get; set; }

        [JsonIgnore]
        public int VolumeInQuantityClear { get; set; }

        /// <summary>
        /// Объем(количество ресурсов) 
        /// </summary>
        public int VolumeInQuantity 
        { 
            get 
            {
                return LimitOnFreeLicenses > VolumeInQuantityClear ? 0 : VolumeInQuantityClear - LimitOnFreeLicenses;
            }
        }

        /// <summary>
        /// Общая стоимость ресурсов
        /// </summary>
        public decimal TotalAmount { get; set; }

        [JsonIgnore]
        public List<AccountRate> AccountRates { get; set; }
        [JsonIgnore]
        public List<Rate> Rates { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<BillingServiceType, MyDatabasesServiceTypeBillingDto>()
                .ForMember(x => x.ServiceTypeId, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.ServiceTypeName, z => z.MapFrom(y => y.Name))
                .ForMember(x => x.ServiceTypeDescription, z => z.MapFrom(y => y.Description))
                .ForMember(x => x.SystemServiceType, z => z.MapFrom(y => y.SystemServiceType))
                .ForMember(x => x.Clouds42ServiceType, z => z.MapFrom(y => y.Service != null ? y.Service.SystemService : null))
                .ForMember(x => x.BillingType, z => z.MapFrom(y => y.BillingType))
                .ForMember(x => x.IsActiveService, z => z.MapFrom(y => y.Service != null && y.Service.IsActive))
                .ForMember(x => x.AccountRates, z => z.MapFrom(y => y.AccountRates))
                .ForMember(x => x.Rates, z => z.MapFrom(y => y.Rates))
                .ForMember(x => x.VolumeInQuantityClear, z => z.Ignore())
                .ForMember(x => x.TotalAmount, z => z.Ignore());
        }
    }
}
