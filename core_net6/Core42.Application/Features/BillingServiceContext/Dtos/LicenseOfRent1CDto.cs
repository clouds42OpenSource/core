﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Dtos;
using Core42.Application.Features.Link42Context.Dtos;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    /// <summary>
    /// License of rent 1c dto
    /// </summary>
    public class LicenseOfRent1CDto : IMapFrom<AccountUser>
    {
        /// <summary>
        /// Web cost
        /// </summary>
        [JsonIgnore]
        public decimal WebCost { get; set; }

        /// <summary>
        /// Rdp cost
        /// </summary>
        [JsonIgnore]
        public decimal RdpCost { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Account id
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Login
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Full user name
        /// </summary>
        public string FullUserName { get { return $"{LastName} {FirstName} {MiddleName}".Trim(); } }

        /// <summary>
        /// Is active flag
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Web resource id
        /// </summary>
        public Guid? WebResourceId { get; set; }

        /// <summary>
        /// Rdp resource id
        /// </summary>
        public Guid? RdpResourceId { get; set; }

        /// <summary>
        /// Sponsored license account name
        /// </summary>
        public string? SponsoredLicenseAccountName { get; set; }

        /// <summary>
        /// Sponsorship license account name
        /// </summary>
        public string? SponsorshipLicenseAccountName { get; set; }

        /// <summary>
        /// Partial user cost web
        /// </summary>
        public decimal? PartialUserCostWeb { get; set; }

        /// <summary>
        /// Partial user cost rdp
        /// </summary>
        public decimal? PartialUserCostRdp { get; set; }

        /// <summary>
        /// Partial user cost standart
        /// </summary>
        public decimal? PartialUserCostStandart { get; set; }

        /// <summary>
        /// Names of dependent active services
        /// </summary>
        public List<DependentActiveServiceDictionaryDto> NamesOfDependentActiveServices { get; set; } = [];

        public Link42InfoDto? Link42Info { get; set; }

        [JsonIgnore]
        public Guid? BillingAccountId { get; set; }
        [JsonIgnore]
        public Guid? LocaleId { get; set; }
        [JsonIgnore]
        public string? AccountType { get; set; }
        [JsonIgnore]
        public string LocaleName { get; set; }
        [JsonIgnore]
        public string FirstName { get; set; }
        [JsonIgnore]
        public string LastName { get; set; }
        [JsonIgnore]
        public string MiddleName { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AccountUser, LicenseOfRent1CDto>()
                .ForMember(x => x.Id, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.Email, z => z.MapFrom(y => y.Email))
                .ForMember(x => x.Login, z => z.MapFrom(y => y.Login))
                .ForMember(x => x.IsActive, z => z.MapFrom(y => y.Activated))
                .ForMember(x => x.Phone, z => z.MapFrom(y => y.PhoneNumber))
                .ForMember(x => x.FirstName, z => z.MapFrom(y => y.FirstName))
                .ForMember(x => x.LastName, z => z.MapFrom(y => y.LastName))
                .ForMember(x => x.MiddleName, z => z.MapFrom(y => y.MiddleName))
                .ForMember(x => x.BillingAccountId, z => z.MapFrom(y => y.Account.BillingAccount.Id))
                .ForMember(x => x.LocaleId, z => z.MapFrom(y => y.Account.AccountConfiguration.LocaleId))
                .ForMember(x => x.AccountType, z => z.MapFrom(y => y.Account.BillingAccount.AccountType))
                .ForMember(x => x.LocaleName, z => z.MapFrom(y => y.Account.AccountConfiguration.Locale.Name))
                .ForMember(x => x.SponsoredLicenseAccountName, z => z.MapFrom(y => y.Account.AccountCaption ?? y.Account.IndexNumber.ToString()))
                .ForMember(x => x.FullUserName, z => z.Ignore());
        }
    }
}
