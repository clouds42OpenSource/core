﻿namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    public class ResourcesByServiceTypeDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Общая стоимость ресурсов
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Объем(количество ресурсов) 
        /// </summary>
        public int VolumeInQuantity { get; set; }
    }
}
