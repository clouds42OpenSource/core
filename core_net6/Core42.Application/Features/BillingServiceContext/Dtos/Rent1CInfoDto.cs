﻿using Core42.Application.Contracts.Features.LocaleContext.Dtos;

namespace Core42.Application.Features.BillingServiceContext.Dtos
{
    /// <summary>
    /// Rent 1C info dto
    /// </summary>
    public class Rent1CInfoDto
    {
        /// <summary>
        /// Expire date
        /// </summary>
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// Account is VIP flag
        /// </summary>
        public bool? AccountIsVip { get; set; }

        /// <summary>
        /// Locale info
        /// </summary>
        public LocaleDto? Locale { get; set; }

        /// <summary>
        /// Monthly cost total
        /// </summary>
        public decimal? MonthlyCostTotal { get; set; }

        /// <summary>
        /// Monthly cost web
        /// </summary>
        public decimal? MonthlyCostWeb { get; set; }

        /// <summary>
        /// Monthly cost standart
        /// </summary>
        public decimal? MonthlyCostStandart { get; set; }

        /// <summary>
        /// Rate web
        /// </summary>
        public decimal? RateWeb { get; set; }

        /// <summary>
        /// Rate rdp
        /// </summary>
        public decimal? RateRdp { get; set; }

        /// <summary>
        /// Web resources count
        /// </summary>
        public decimal? WebResourcesCount { get; set; }

        /// <summary>
        /// Standart resources count
        /// </summary>
        public decimal? StandartResourcesCount { get; set; }

        /// <summary>
        /// Sponsored web count
        /// </summary>
        public decimal? SponsoredWebCount { get; set; }

        /// <summary>
        /// Sponsored standart count
        /// </summary>
        public decimal? SponsoredStandartCount { get; set; }

        /// <summary>
        /// Sponsorship web count
        /// </summary>
        public decimal? SponsorshipWebCount { get; set; }

        /// <summary>
        /// Sponsorship standart count
        /// </summary>
        public decimal? SponsorshipStandartCount { get; set; }

        /// <summary>
        /// Free web resources
        /// </summary>
        public List<Guid>? FreeWebResources { get; set; } = [];

        /// <summary>
        /// Free rdp resources
        /// </summary>
        public List<Guid>? FreeRdpResources { get; set; } = [];

        /// <summary>
        /// Users list
        /// </summary>
        public List<LicenseOfRent1CDto>? UserList { get; set; } = [];

        /// <summary>
        /// Service status info
        /// </summary>
        public ServiceStatusDto? ServiceStatus { get; set; }

        /// <summary>
        /// My databases service data short info
        /// </summary>
        public MyDatabasesServiceDto? MyDatabasesServiceData { get; set; }

        /// <summary>
        /// Additional account resources
        /// </summary>
        public AdditionalAccountResourceDto? AdditionalAccountResources { get; set; }
    }
}
