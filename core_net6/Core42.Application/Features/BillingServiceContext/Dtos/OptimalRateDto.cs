﻿namespace Core42.Application.Features.BillingServiceContext.Dtos
{

    /// <summary>
    /// Optimal rate dto
    /// </summary>
    public class OptimalRateDto
    {
        /// <summary>
        /// Web cost
        /// </summary>
        public decimal WebCost { get; set; }

        /// <summary>
        /// Rdp cost
        /// </summary>
        public decimal RdpCost { get; set; }

        /// <summary>
        /// Partial web cost
        /// </summary>
        public decimal PartialCostOfWebLicense { get; set; }

        /// <summary>
        /// Partial rdp cost
        /// </summary>
        public decimal PartialCostOfRdpLicense { get; set; }

        /// <summary>
        /// Partial standart cost
        /// </summary>
        public decimal PartialCostOfStandartLicense { get; set; }
    }
}
