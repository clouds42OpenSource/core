﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BillingServiceContext.Dtos;
using Core42.Application.Features.BillingServiceContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BillingServiceContext.Handlers.Queries;

/// <summary>
/// Get market42 billing services query handler
/// </summary>
public class GetMarker42BillingServicesQueryHandler(IMapper mapper, IUnitOfWork unitOfWork, ILogger42 logger)
    : IRequestHandler<GetMarker42BillingServicesQuery, ManagerResult<Market42ServiceResultDto>>
{
    /// <summary>
    /// Handle get market42 billing services query
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<ManagerResult<Market42ServiceResultDto>> Handle(GetMarker42BillingServicesQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var items = await unitOfWork.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .Where(rc => rc.BillingService.SystemService == null && rc.BillingService.InternalCloudService == null && rc.BillingService.IsActive && rc.BillingService.Name != "Дополнительные сеансы" )
                .ProjectTo<Market42ServiceItemDto>(mapper.ConfigurationProvider)
                .AutoFilter(request.Filter)
                .AutoSort(request)
                .ToListAsync(cancellationToken);

            var result = new Market42ServiceResultDto { Services = { Items = items } };
            
            return result.ToOkManagerResult();
        }

        catch (Exception e)
        {
            logger.Error(e, "Ошибка при получении списка сервисов");

            return PagedListExtensions.ToPreconditionFailedManagerResult<Market42ServiceResultDto>(
                "Произошла ошибка при получении списка сервисов");
        }
    }
}
