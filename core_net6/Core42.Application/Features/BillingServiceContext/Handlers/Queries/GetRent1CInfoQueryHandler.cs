﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.LocaleContext.Dtos;
using Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Dtos;
using Core42.Application.Features.BillingServiceContext.Dtos;
using Core42.Application.Features.BillingServiceContext.Queries;
using Core42.Application.Features.Link42Context.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BillingServiceContext.Handlers.Queries
{
    /// <summary>
    /// Get rent 1C query handler
    /// </summary>
    public class GetRent1CInfoQueryHandler(
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        ICloudLocalizer cloudLocalizer,
        IHandlerException handlerException,
        ILogger42 logger,
        IConfiguration configuration,
        IMapper mapper,
        ISender sender)
        : IRequestHandler<GetRent1CInfoQuery, ManagerResult<Rent1CInfoDto>>
    {
        private readonly Guid _defaultLocale = Guid.Parse(configuration["DefaultLocale"]!);

        /// <summary>
        /// Handle get rent 1C query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public async Task<ManagerResult<Rent1CInfoDto>> Handle(GetRent1CInfoQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var accountId = accessProvider.ContextAccountId;
                accessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => accountId);
                
                var isServiceAccount = await unitOfWork.ServiceAccountRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == accountId, cancellationToken);
             
                if (isServiceAccount != null)
                {
                    throw new InvalidOperationException("Служебным аккаунтам недоступно управление сервисами");
                }

                var model = new Rent1CInfoDto();

                var account = await unitOfWork.AccountsRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.AccountConfiguration)
                    .ThenInclude(x => x.Locale)
                    .Include(x => x.BillingAccount)
                    .ThenInclude(x => x.ResourcesConfigurations)
                    .ThenInclude(x => x.BillingService)
                    .ThenInclude(x => x.BillingServiceTypes)
                    .ThenInclude(x => x.DependServiceType)
                    .FirstOrDefaultAsync(x => x.Id == accountId, cancellationToken);
               
                var resConfig = account?.BillingAccount.ResourcesConfigurations.FirstOrDefault(x => x.BillingService.SystemService == Clouds42Service.MyEnterprise);

                if (resConfig == null)
                {
                    model.ServiceStatus = null;
                }

                else
                {
                    model.ExpireDate = resConfig.ExpireDateValue.Date;
                    model.MonthlyCostTotal = resConfig.Cost;
                    model.ServiceStatus = await GetServiceStatusAsync(account?.AccountConfiguration?.Locale, resConfig, cancellationToken);
                }

                model.Locale = mapper.Map<LocaleDto>(account?.AccountConfiguration?.Locale);

                var resTypes = new List<ResourceType> { ResourceType.MyEntUser, ResourceType.MyEntUserWeb };

                var allResources = await unitOfWork.ResourceRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.BillingServiceType.SystemServiceType.HasValue && resTypes.Contains(x.BillingServiceType.SystemServiceType.Value) &&
                            (x.AccountId == accountId || x.AccountSponsorId == accountId))
                    .Include(x => x.AccountSponsor)
                    .ToListAsync(cancellationToken);
               
                var resourcesWithSubject = allResources.Where(x => x.Subject != null && x.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise).ToList();
                var resourcesWithOutSubject = allResources.Where(x => x.Subject == null && x.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise).ToList();

                model.UserList = await GetUsersAsync(accountId, resourcesWithSubject, cancellationToken);
               
                var webResource = allResources.FirstOrDefault(x => x.BillingServiceType.SystemServiceType is ResourceType.MyEntUserWeb)?.BillingServiceType ?? 
                                  await unitOfWork.BillingServiceTypeRepository.AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.SystemServiceType == ResourceType.MyEntUserWeb, cancellationToken);

                var rdpResource = allResources
                    .FirstOrDefault(x => x.BillingServiceType.SystemServiceType is ResourceType.MyEntUser)
                    ?.BillingServiceType ?? await unitOfWork.BillingServiceTypeRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.SystemServiceType == ResourceType.MyEntUser, cancellationToken);

                var key = Guid.NewGuid();
                
                var currentAccountLicense = new List<LicenseOfRent1CDto>
                {
                    new()
                    {
                        AccountId = account!.Id,
                        Id = key,
                        BillingAccountId = account.BillingAccount?.Id,
                        LocaleId = account.AccountConfiguration?.LocaleId,
                        AccountType = account.BillingAccount?.AccountType,
                    }
                };

                var query = new GetOptimalRatesByAccountAndServiceTypesQuery
                {
                    Licenses = model.UserList.Concat(currentAccountLicense).ToList(),
                    ServiceTypes = [webResource!, rdpResource!],
                    ExpireDate = model.ExpireDate
                };

                var optimalRatesResult = (await sender.Send(query, cancellationToken)).Result;

                model.RateWeb = optimalRatesResult?.FirstOrDefault(x => x.Id == key)?.WebCost ?? 0;
                model.RateRdp = optimalRatesResult?.FirstOrDefault(x => x.Id == key)?.RdpCost ?? 0;
                model.UserList = optimalRatesResult?.Where(x => x.Id != key).ToList();
                
                model.FreeWebResources = resourcesWithOutSubject
                    .Where(x => x.BillingServiceType is { SystemServiceType: ResourceType.MyEntUserWeb })
                    .Select(x => x.Id)
                    .ToList();

                model.FreeRdpResources = resourcesWithOutSubject
                    .Where(x => x.BillingServiceType is { SystemServiceType: ResourceType.MyEntUser })
                    .Select(x => x.Id)
                    .ToList();

                var payedResRdp = resourcesWithSubject
                    .Where(r => (r.AccountSponsorId == null || r.AccountSponsorId == accountId) &&
                                r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser)
                    .ToList();

                var allResWeb = resourcesWithSubject
                    .Where(r => (r.AccountSponsorId == null || r.AccountSponsorId == accountId) &&
                                r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb)
                    .ToList();

                var payedResWeb = allResWeb
                    .Where(r => !payedResRdp.Select(rdp => rdp.Subject)
                        .Contains(r.Subject))
                    .ToList();

                model.MonthlyCostWeb = payedResWeb.Sum(r => r.Cost);
                model.MonthlyCostStandart = payedResRdp.Sum(r => r.Cost) + allResWeb
                    .Where(r => payedResRdp.Select(rdp => rdp.Subject).Contains(r.Subject)).Sum(r => r.Cost);
                model.SponsoredStandartCount = payedResRdp.Count(r => r.AccountSponsorId == accountId);
                model.SponsoredWebCount = payedResWeb.Count(r => r.AccountSponsorId == accountId);
                model.SponsorshipStandartCount = resourcesWithSubject.Count(r =>
                    r.AccountSponsorId != null && r.AccountSponsorId != accountId &&
                    r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);
                model.SponsorshipWebCount = Math.Max(0,
                    resourcesWithSubject.Count(r =>
                        r.AccountSponsorId != null && r.AccountSponsorId != accountId &&
                        r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb) -
                   (model.SponsorshipStandartCount ?? 0));
                model.StandartResourcesCount = resourcesWithSubject.Count(r => r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);
                model.WebResourcesCount = (resourcesWithSubject.Select(r=>r.Subject).Distinct().Count() - model.StandartResourcesCount) ?? 0;
                model.AccountIsVip = account?.AccountConfiguration?.IsVip ?? false;
                model.MyDatabasesServiceData = await GetMyDatabasesServiceDtoAsync(accountId);
              
                if (!(account?.AccountConfiguration?.IsVip ?? false))
                    model.MonthlyCostTotal += model.MyDatabasesServiceData.TotalAmountForDatabases;

                model.AdditionalAccountResources = new AdditionalAccountResourceDto
                {
                    AdditionalResourcesCost = account?.BillingAccount?.AdditionalResourceCost ?? 0,
                    AdditionalResourcesName = account?.BillingAccount?.AdditionalResourceName,
                    NeedShowAdditionalResources = (account?.AccountConfiguration?.IsVip ?? false) &&
                                                  !string.IsNullOrEmpty(account.BillingAccount
                                                      ?.AdditionalResourceName)
                };

                return model.ToOkManagerResult();
            }

            catch (Exception exception)
            {
                string errorGettingDataAboutRent1CPhrase;

                try
                {
                    errorGettingDataAboutRent1CPhrase = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.ErrorGettingDataAboutRent1C, accessProvider.ContextAccountId);
                }
                catch (Exception e)
                {
                    errorGettingDataAboutRent1CPhrase = e.Message;
                }


                handlerException.Handle(exception, $"[{errorGettingDataAboutRent1CPhrase} для компании {accessProvider.ContextAccountId}.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<Rent1CInfoDto>(
                    $"{errorGettingDataAboutRent1CPhrase} : {exception.Message}");
            }
        }

        /// <summary>
        /// Get service status info dto
        /// </summary>
        /// <param name="accountLocale"></param>
        /// <param name="resConfig"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        private async Task<ServiceStatusDto> GetServiceStatusAsync(Locale? accountLocale, ResourcesConfiguration resConfig, CancellationToken cancellationToken)
        {
            var accountId = accessProvider.ContextAccountId;
            var result = new ServiceStatusDto();

            var billingAccount = await unitOfWork.BillingAccountRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Id == accountId, cancellationToken);

            var locale = accountLocale ?? await unitOfWork.LocaleRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.ID == _defaultLocale, cancellationToken);

            var mainServiceId = resConfig.BillingService.BillingServiceTypes.FirstOrDefault(w => w.DependServiceTypeId != null)?.DependServiceType.ServiceId;

            var mainResConfig = await unitOfWork.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(r => r.AccountId == resConfig.AccountId && r.BillingService.Id == mainServiceId, cancellationToken);

            if (mainResConfig == null && mainServiceId.HasValue)
                throw new NotFoundException($"Не удалось получить запись ResourceConfiguration основной услуги для аккаунта '{resConfig.AccountId}' по услуге '{resConfig.BillingService.Name}'");

            result.ServiceIsLocked = (mainResConfig?.FrozenValue ?? false) || resConfig.FrozenValue;
            result.PromisedPaymentIsActive = billingAccount is { PromisePaymentSum: > 0, PromisePaymentDate: not null };

            var promisePaymentsDays = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");

            if (result.PromisedPaymentIsActive.HasValue && result.PromisedPaymentIsActive.Value)
            {
                result.PromisedPaymentSum = $"{billingAccount?.PromisePaymentSum ?? 0m:0.00} {locale?.Currency ?? "руб"}.";
                result.PromisedPaymentExpireDate = billingAccount?.PromisePaymentDate?.AddDays(promisePaymentsDays) ?? DateTime.Now;
            }

            var isPromisePaymentExpired = billingAccount is { PromisePaymentSum: > 0, PromisePaymentDate: not null } &&
                                          (DateTime.Now.Date - billingAccount.PromisePaymentDate.Value.Date).Days > promisePaymentsDays;

            var serviceNameLocalized = ServiceNameLocalizer.LocalizeForAccount(cloudLocalizer, accountId, resConfig.BillingService.Name, resConfig.BillingService.SystemService);

            if (isPromisePaymentExpired)
            {
                result.ServiceIsLocked = true;
                result.ServiceLockReasonType = ServiceLockReasonType.OverduePromisedPayment;
                result.ServiceLockReason = $" Cервис '{serviceNameLocalized}' " +
                    $"заблокирован по причине наличия обещанного платежа на сумму {result.PromisedPaymentSum}";

                logger.Info($"У аккаунта {accountId} сервис '{resConfig.BillingService.Name}' заблокирован по причине ОП на сумму {result.PromisedPaymentSum}");

                return result;
            }

            if (result.ServiceIsLocked.HasValue && !result.ServiceIsLocked.Value)
            {
                return result;
            }

            result.ServiceLockReasonType = ServiceLockReasonType.ServiceNotPaid;
            result.ServiceLockReason = GetServiceLockReason(resConfig, serviceNameLocalized, mainResConfig);
            logger.Info($"У аккаунта {accountId} сервис {resConfig.BillingService.Name} заблокирован.");

            return result;
        }

        /// <summary>
        /// Get service lock reason
        /// </summary>
        /// <param name="resConfig"></param>
        /// <param name="localizedName"></param>
        /// <param name="mainServiceResConfig"></param>
        /// <returns></returns>
        private string GetServiceLockReason(ResourcesConfiguration? resConfig, string localizedName,
            ResourcesConfiguration? mainServiceResConfig)
        {
            if (mainServiceResConfig?.BillingServiceId != resConfig?.BillingServiceId &&
                (mainServiceResConfig?.FrozenValue ?? false))
                return $"Работа с сервисом “{localizedName}” возможна " +
                       $"при активном сервисе “{(resConfig?.AccountId != null ? cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, resConfig.AccountId) : "Аренда 1С")}”.";

            return resConfig?.IsDemoPeriod ?? false ? $"У сервиса '{localizedName}' завершился демо период" : $"Сервис '{localizedName}' заблокирован.";
        }

        /// <summary>
        /// Get license of rent 1C dto
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="users"></param>
        /// <param name="usersResources"></param>
        /// <param name="cancellation"></param>
        /// <returns></returns>
        private async Task<List<LicenseOfRent1CDto>> GetLicenseOfRent1DtoAsyncBulk(Guid accountId,
            IReadOnlyCollection<AccountUser> users, IReadOnlyCollection<Resource> usersResources,
            CancellationToken cancellation)
        {
            var userIds = users.Select(x => x.Id).ToList();
            var sponsoredLicenseAccountName = await GetSponsoredLicenseAccountNameAsync(usersResources, accountId, userIds);
            var namesOfDependentActiveServices = await GetNamesOfDependentActiveServicesAsync(userIds);
            var sponsorshipLicenseAccountName = GetSponsorshipLicenseAccountName(usersResources, accountId, userIds);

            var currentLinkVersionQuery = new GetCurrentLinkVersionAndUsedByUserLoginQuery
            {
                Logins = users.Select(x => x.Login),
                Link42Version = (await unitOfWork.Link42ConfigurationsRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(x => x.IsCurrentVersion, cancellation))?.Version
            };

            var linkInfos = (await sender.Send(currentLinkVersionQuery, cancellation))?.Result;

            return users.Select(x =>
            {
                var mapped = mapper.Map<LicenseOfRent1CDto>(x);
                mapped.AccountId = accountId;
                mapped.RdpResourceId = usersResources.FirstOrDefault(f => f.BillingServiceType.SystemServiceType == ResourceType.MyEntUser &&  f.Subject == x.Id)?.Id;
                mapped.WebResourceId = usersResources.FirstOrDefault(f => f.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb && f.Subject == x.Id)?.Id;
                mapped.SponsoredLicenseAccountName = sponsoredLicenseAccountName[x.Id];
                mapped.NamesOfDependentActiveServices = namesOfDependentActiveServices[x.Id];
                mapped.SponsorshipLicenseAccountName = sponsorshipLicenseAccountName[x.Id];
                mapped.Link42Info = linkInfos?[x.Login];

                return mapped;

            }).ToList();
        }

        /// <summary>
        /// Get sponsorship license account name
        /// </summary>
        /// <param name="userResources"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        private static Dictionary<Guid, string?> GetSponsorshipLicenseAccountName(IEnumerable<Resource> userResources, Guid accountId, IEnumerable<Guid> usersIds)
        {
            return usersIds.ToDictionary(x => x, z =>
            {
                var sponsorResource = userResources.FirstOrDefault(r =>
                    r.AccountId == accountId && r.AccountSponsorId != null && r.AccountSponsorId != accountId &&
                    r.Subject == z);

                return sponsorResource?.AccountSponsor.GetAccountCaptionOrIndexNumber();
            });
        }

        /// <summary>
        /// Get sponsored license account name
        /// </summary>
        /// <param name="userResources"></param>
        /// <param name="accountId"></param>
        /// <param name="usersIds"></param>
        /// <returns></returns>
        private async Task<Dictionary<Guid, string?>> GetSponsoredLicenseAccountNameAsync(IEnumerable<Resource> userResources, Guid accountId, IEnumerable<Guid> usersIds)
        {
            var sponsoredResource = userResources.Where(r => r.AccountId != accountId && r.AccountSponsorId == accountId && r.Subject.HasValue && usersIds.ToList().Contains(r.Subject.Value)).ToList();
            var sponsoredResourceAccountIds = sponsoredResource.Select(x => x.AccountId).ToList();
            if (!sponsoredResource.Any())
                return usersIds.ToDictionary(x => x, _ => (string?)null);

            var accountOfSponsoredLicense = await unitOfWork.AccountsRepository
                .AsQueryableNoTracking()
                .Where(a => sponsoredResourceAccountIds.Contains(a.Id))
                .ToListAsync();

            return usersIds.ToDictionary(x => x, z =>
            {
                var accountName = sponsoredResource
                    .GroupJoin(accountOfSponsoredLicense, y => y.AccountId, x => x.Id,
                        (resource, accounts) => new { resource, accounts })
                    .ToList()
                    .FirstOrDefault(ra => ra.resource.Subject.HasValue && ra.resource.Subject.Value == z);

                return accountName?.accounts.FirstOrDefault()?.GetAccountCaptionOrIndexNumber();
            });

        }
        
        /// <summary>
        /// Get names of dependent active services
        /// </summary>
        /// <param name="accountUserIds"></param>
        /// <returns></returns>
        private async Task<Dictionary<Guid, List<DependentActiveServiceDictionaryDto>>> GetNamesOfDependentActiveServicesAsync(IEnumerable<Guid> accountUserIds)
        {
            var resourcesTypes = new List<ResourceType> { ResourceType.MyEntUser, ResourceType.MyEntUserWeb };

            var namesOfDependentActiveService = await unitOfWork.ResourceRepository
                .AsQueryableNoTracking()
                .Where(x => x.Subject.HasValue && accountUserIds.Contains(x.Subject.Value))
                .Where(x => x.BillingServiceType.Service.SystemService != Clouds42Service.MyDatabases &&
                            x.BillingServiceType.Service.SystemService != Clouds42Service.MyEnterprise &&
                            x.BillingServiceType.DependServiceType.SystemServiceType.HasValue &&
                            resourcesTypes.Contains(x.BillingServiceType.DependServiceType.SystemServiceType.Value))
                .Select(x => new
                {
                    x.BillingServiceType.Service.Name,
                    x.BillingServiceType.DependServiceType.SystemServiceType,
                    x.Subject,
                    x.BillingServiceType.ServiceId,
                })
                .Distinct()
                .ToListAsync();

            return accountUserIds.ToDictionary(x => x,
                z => new List<DependentActiveServiceDictionaryDto>
                {
                    new()
                    {
                        Key = resourcesTypes[0],
                        Value = namesOfDependentActiveService
                            .Where(x => x.SystemServiceType == resourcesTypes[0] && x.Subject == z)
                            .Select(x => x.Name).ToList(),
                        DependServiceIds = namesOfDependentActiveService
                            .Where(x => x.SystemServiceType == resourcesTypes[0] && x.Subject == z)
                            .Select(x => x.ServiceId).ToList(),

                    },
                    new()
                    {
                        Key = resourcesTypes[1],
                        Value = namesOfDependentActiveService
                            .Where(x => x.SystemServiceType == resourcesTypes[1] && x.Subject == z)
                            .Select(x => x.Name).ToList(),
                        DependServiceIds = namesOfDependentActiveService
                            .Where(x => x.SystemServiceType == resourcesTypes[1] && x.Subject == z)
                            .Select(x => x.ServiceId).ToList(),
                    }
                });
        }

        /// <summary>
        /// Get 'My Databases' service dto
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        private async Task<MyDatabasesServiceDto> GetMyDatabasesServiceDtoAsync(Guid accountId)
        {
            var limitOnFreeCreationDbForAccount = (await unitOfWork
                .GetGenericRepository<LimitOnFreeCreationDbForAccount>()
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.AccountId == accountId))?.LimitedQuantity ?? CloudConfigurationProvider.MyDatabasesService.GetDefaultLimitOnFreeCreationDb();

            var resourceTypes = new List<ResourceType> { ResourceType.CountOfDatabasesOverLimit, ResourceType.ServerDatabasePlacement };

            var serviceTypesBillingData = await unitOfWork.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .Include(x => x.Service)
                .Include(x => x.Rates)
                .Include(x => x.AccountRates)
                .ProjectTo<MyDatabasesServiceTypeBillingDto>(mapper.ConfigurationProvider)
                .Where(x => x.Clouds42ServiceType.HasValue &&
                            x.Clouds42ServiceType.Value == Clouds42Service.MyDatabases &&
                            x.SystemServiceType != null && resourceTypes.Contains(x.SystemServiceType.Value))
                .ToListAsync();

            var serviceTypesIds = serviceTypesBillingData.Select(x => x.ServiceTypeId).ToList();

            var services = await unitOfWork.ResourceRepository
                .AsQueryableNoTracking()
                .Include(x => x.MyDatabasesResource)
                .Where(x => serviceTypesIds.Contains(x.BillingServiceTypeId) && x.AccountId == accountId)
                .ToListAsync();

            foreach (var item in serviceTypesBillingData)
            {
                var resource = services.FirstOrDefault(x => x.BillingServiceTypeId == item.ServiceTypeId);
                var accountRate = item.AccountRates.FirstOrDefault(x => x.AccountId == accountId);
                var rate = item.Rates.MaxBy(x => x.Cost);
                item.LimitOnFreeLicenses = item.SystemServiceType != ResourceType.CountOfDatabasesOverLimit ? 0 : limitOnFreeCreationDbForAccount;
                item.CostPerOneLicense = accountRate?.Cost ?? rate?.Cost ?? 0;
                item.VolumeInQuantityClear =
                    resource?.MyDatabasesResource?.ActualDatabasesCount ?? 1;
                item.TotalAmount = resource?.Cost ?? 0;
            }

            var databasePlacementServiceTypeData = serviceTypesBillingData.FirstOrDefault(x => x.SystemServiceType == ResourceType.CountOfDatabasesOverLimit) ??
                    throw new NotFoundException("Не удалось получить данные биллинга для услуги 'Информационные базы'");

            var serverDatabasePlacementServiceTypeData = serviceTypesBillingData.FirstOrDefault(x => x.SystemServiceType == ResourceType.ServerDatabasePlacement) ??
                throw new NotFoundException("Не удалось получить данные биллинга для услуги 'Размещение серверных баз'");

            return new MyDatabasesServiceDto
            {
                ServerDatabasePlacementCost = serverDatabasePlacementServiceTypeData.CostPerOneLicense,
                DatabasePlacementCost = databasePlacementServiceTypeData.CostPerOneLicense,
                LimitOnFreeCreationDb = databasePlacementServiceTypeData.LimitOnFreeLicenses,
                CountDatabasesOverLimit = databasePlacementServiceTypeData.VolumeInQuantity,
                ServerDatabasesCount = serverDatabasePlacementServiceTypeData.VolumeInQuantity,
                TotalAmountForDatabases = databasePlacementServiceTypeData.TotalAmount,
                TotalAmountForServerDatabases = serverDatabasePlacementServiceTypeData.TotalAmount
            };
        }

        /// <summary>
        /// Get users by account id
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="resourcesWithSubject"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task<List<LicenseOfRent1CDto>> GetUsersAsync(Guid accountId, IReadOnlyCollection<Resource> resourcesWithSubject, CancellationToken cancellationToken)
        {
            var ids = resourcesWithSubject.Select(x => x.Subject).ToList();

            var notDeletedUsers = await unitOfWork.AccountUsersRepository
                   .AsQueryableNoTracking()
                   .Include(x => x.Account)
                   .ThenInclude(x => x.AccountConfiguration)
                   .ThenInclude(x => x.Locale)
                   .Where(x => x.CorpUserSyncStatus != "Deleted" && x.CorpUserSyncStatus != "SyncDeleted" && (x.AccountId == accountId || ids.Contains(x.Id)) && x.Activated)
                   .ToListAsync(cancellationToken);

            var result =
                await GetLicenseOfRent1DtoAsyncBulk(accountId, notDeletedUsers, resourcesWithSubject,
                    cancellationToken);
            
            return result.OrderBy(o => o.SponsoredLicenseAccountName + o.SponsorshipLicenseAccountName).ToList();
        }
    }
}
