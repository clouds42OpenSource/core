﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Constants;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.BillingServiceContext.Dtos;
using Core42.Application.Features.BillingServiceContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BillingServiceContext.Handlers.Queries
{
    /// <summary>
    /// Get external user for rent 1c query handler
    /// </summary>
    public class GetExternalUserForRent1CQueryHandler(
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        ICloudLocalizer cloudLocalizer,
        ILogger42 logger,
        IMapper mapper,
        ISender sender)
        : IRequestHandler<GetExternalUserForRent1CQuery, ManagerResult<LicenseOfRent1CDto>?>
    {
        private readonly IDictionary<string, string> _mapLocaleNameToCountryNameDictionary =
            new Dictionary<string, string>
            {
                {LocaleConst.Russia, "России"},
                {LocaleConst.Kazakhstan, "Казахстана"},
                {LocaleConst.Ukraine, "Украины"},
                {LocaleConst.Uzbekistan, "Узбекистана"}
            };

        /// <summary>
        /// Handle get external user for rent 1c
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<LicenseOfRent1CDto>?> Handle(GetExternalUserForRent1CQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var accountId = accessProvider.ContextAccountId;
                var email = request.Email.Trim();

                accessProvider.HasAccess(ObjectAction.ManageResources, () => accountId);

                logger.Trace($"Начало поиска аккаунтом {accountId} для спонсирования пользователя с email:{email}");

                var sponsoredAccountUser = await unitOfWork.AccountUsersRepository
                        .AsQueryable()
                        .ProjectTo<LicenseOfRent1CDto>(mapper.ConfigurationProvider)
                        .FirstOrDefaultAsync(acUs => acUs.Email == email, cancellationToken);

                if(sponsoredAccountUser == null)
                    return PagedListExtensions.ToPreconditionFailedManagerResult<LicenseOfRent1CDto>($"Пользователь с такой почтой {email} не найден");

                var resourceConfiguration = await unitOfWork.ResourceConfigurationRepository.AsQueryable()
                    .FirstOrDefaultAsync(x =>
                        x.AccountId == sponsoredAccountUser.AccountId &&
                        x.BillingService.SystemService == Clouds42Service.MyEnterprise, cancellationToken);

                if (sponsoredAccountUser == null)
                    return PagedListExtensions.ToPreconditionFailedManagerResult<LicenseOfRent1CDto>($"У пользователя с почтой {email} не подключен сервис Аренда 1С");

                var error = await CanApplySponsorshipAsync(accountId, email, sponsoredAccountUser, resourceConfiguration, cancellationToken);

                if (!string.IsNullOrEmpty(error))
                    return PagedListExtensions.ToPreconditionFailedManagerResult<LicenseOfRent1CDto>(error);

                logger.Trace($"Название или номер компании которую спонсируют {sponsoredAccountUser?.SponsoredLicenseAccountName}");


                logger.Trace($"Начало подсчета стоимости спонсируемой лицензии для аккаунта {accountId}");

                var sponsoredAccountUserResult = await CalculatePartialCostForSponsorAccountAsync(sponsoredAccountUser, resourceConfiguration, cancellationToken);

                return (sponsoredAccountUserResult ?? sponsoredAccountUser)?.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка поиска внешнего пользователя для подключения аренды 1С] {accessProvider.ContextAccountId}");

                return PagedListExtensions.ToPreconditionFailedManagerResult<LicenseOfRent1CDto>(
                    $"Ошибка поиска внешнего пользователя для подключения сервиса {cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accessProvider.ContextAccountId)}");
            }
        }

        /// <summary>
        /// Check can apply sponsorship
        /// </summary>
        /// <param name="accountSponsorId">ID аккаунта-спонсора</param>
        /// <param name="sponsoredAccountUserEmail">Почта спонсируемого аккаунта</param>
        /// <param name="sponsoredAccountUser">Спонсируемый пользователь</param>
        /// <param name="resourcesConfiguration"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>Результат проверки</returns>
        private async Task<string?> CanApplySponsorshipAsync(Guid accountSponsorId, string sponsoredAccountUserEmail,
            LicenseOfRent1CDto? sponsoredAccountUser, ResourcesConfiguration resourcesConfiguration, CancellationToken cancellationToken)
        {
            var account = await unitOfWork.AccountsRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Id == accountSponsorId, cancellationToken);

            var sponsorAccountUser = await unitOfWork.AccountUsersRepository
                .AsQueryableNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Locale)
                .FirstOrDefaultAsync(acUs => acUs.AccountId == accountSponsorId, cancellationToken);

            var applySponsorshipBySponsorResult = await CanApplySponsorshipBySponsorRent1CResConfigAsync(accountSponsorId);

            if (!applySponsorshipBySponsorResult.IsSuccess)
            {
                return applySponsorshipBySponsorResult.Message;
            }

            var accountHasPayments = await unitOfWork.PaymentRepository
                .AsQueryableNoTracking()
                .AnyAsync(p => p.AccountId == account.Id &&
                               p.Status == PaymentStatus.Done.ToString() &&
                               p.OperationType == PaymentType.Inflow.ToString(), cancellationToken);

            var accountHasLicenses = await unitOfWork.ResourceRepository
                .AsQueryableNoTracking()
                .AnyAsync(resource => resource.AccountId == account.Id &&
                                       resource.Cost > 0 && resource.AccountSponsorId.HasValue &&
                                       resource.AccountSponsor.BillingAccount.ResourcesConfigurations.Any(rc => rc.Frozen == false && rc.BillingService.SystemService == Clouds42Service.MyEnterprise), cancellationToken);

            var accountIsDemo = !accountHasPayments && !accountHasLicenses;
            if (accountIsDemo)
            {
                const string error = "Спонсирование пользователей внешних аккаунтов будет доступно после оплаты сервиса.";

                logger.Warn(error);

                return error;
            }

            if (sponsoredAccountUser == null)
            {
                logger.Warn($"Пользователь с email {sponsoredAccountUserEmail} не найден");
                return $"Пользователь с email {sponsoredAccountUserEmail} не найден";
            }

            var applySponsorshipErrorBySponsoredResult = await CanApplySponsorshipBySponsoredAccountRent1CResConfigAsync(accountSponsorId, sponsoredAccountUser, resourcesConfiguration, cancellationToken);
            if (!applySponsorshipErrorBySponsoredResult.IsSuccess)
            {
                return applySponsorshipErrorBySponsoredResult.Message;
            }

            if (sponsoredAccountUser?.AccountId == accountSponsorId)
            {
                logger.Warn($"Попытка добавить аккаунтом {accountSponsorId} своего же пользователя {sponsoredAccountUser.Login}");

                return "Можно добавлять пользователей только внешних аккаунтов.";
            }

            var applySponsorshipByLocaleResult = CanApplySponsorshipByLocale(sponsorAccountUser, sponsoredAccountUser);

            return !applySponsorshipByLocaleResult.IsSuccess ? applySponsorshipByLocaleResult.Message : null;
        }

        /// <summary>
        /// Check can apply sponsorship by sponsor rent 1c resource config
        /// </summary>
        /// <param name="accountSponsorId">ID аккаунта-спонсора</param>
        /// <returns>Результат проверки</returns>
        private async Task<ValidationDto> CanApplySponsorshipBySponsorRent1CResConfigAsync(Guid accountSponsorId)
        {
            var rent1CResourceConfiguration = await unitOfWork.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x =>
                    x.AccountId == accountSponsorId && x.BillingService.SystemService == Clouds42Service.MyEnterprise);

            if (rent1CResourceConfiguration is not { FrozenValue: true })
            {
                return new ValidationDto(true);
            }

            logger.Warn($"Сервис {Clouds42Service.MyEnterprise} у аккаунта {accountSponsorId} заблокирован");

            return new ValidationDto(false, "Ваш сервис заблокирован. Пожалуйста, оплатите сервис.");

        }

        /// <summary>
        /// Проверить возможность применить спонсирование по конфигурации Аренды спонсируемого аккаунта
        /// </summary>
        /// <param name="accountSponsorId">ID аккаунта-спонсора</param>
        /// <param name="sponsoredAccountUser">Спонсируемый пользователь</param>
        /// <param name="resourcesConfiguration"></param>
        /// <param name="cancellationToken">Спонсируемый пользователь</param>
        /// <returns>Результат проверки</returns>
        private async Task<ValidationDto> CanApplySponsorshipBySponsoredAccountRent1CResConfigAsync(Guid accountSponsorId,
            LicenseOfRent1CDto sponsoredAccountUser, ResourcesConfiguration? resourcesConfiguration, CancellationToken cancellationToken)
        {
            var errorMessage = string.Empty;

            if (resourcesConfiguration == null)
            {
                logger.Warn(
                    $"У аккаунта пользователя:{sponsoredAccountUser.AccountId} не найден сервис {Clouds42Service.MyEnterprise} в ResourceConfiguration");

                return new ValidationDto(false, $"Пользователь {sponsoredAccountUser.Email} принадлежит аккаунту, для которого сервис " +
                                                      $"\"{cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accountSponsorId)}\" неактивен.");
            }

            var resources = await unitOfWork.ResourceRepository
                .AsQueryableNoTracking()
                .Where(res => res.Subject == sponsoredAccountUser.Id && res.AccountSponsorId != null && res.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise)
                .ToListAsync(cancellationToken);

            if (!resources.Any())
            {
                return new ValidationDto(true);
            }

            errorMessage = $"Пользователь '{sponsoredAccountUser.Email}' уже проспонсирован.";
            logger.Warn(errorMessage);
            return new ValidationDto(false, errorMessage);

        }

        /// <summary>
        /// Проверить возможность применить спонсирование по локали
        /// </summary>
        /// <param name="sponsorAccountUser">Пользователь-спонсор</param>
        /// <param name="sponsoredAccountUser">Спонсируемый пользователь</param>
        /// <returns>Результат проверки</returns>
        private ValidationDto CanApplySponsorshipByLocale(AccountUser sponsorAccountUser, LicenseOfRent1CDto sponsoredAccountUser)
        {
            var sponsorAccountLocaleName = sponsorAccountUser.Account.AccountConfiguration.Locale.Name;
            var sponsoredAccountLocaleName = sponsoredAccountUser.LocaleName;

            if (sponsorAccountLocaleName == sponsoredAccountLocaleName)
            {
                return new ValidationDto(true);
            }

            var sponsorAccountUserCountry = DefineCountryNameByLocaleName(sponsorAccountLocaleName);
            var sponsoredAccountUserCountry = DefineCountryNameByLocaleName(sponsoredAccountLocaleName);

            return new ValidationDto(false, $"Допускается спонсирование только пользователей из {sponsorAccountUserCountry}, пользователь '{sponsoredAccountUser.Email}' - из {sponsoredAccountUserCountry}");

        }

        /// <summary>
        /// Определить название страны по названию локали
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Название страны</returns>
        private string DefineCountryNameByLocaleName(string localeName)
            => !_mapLocaleNameToCountryNameDictionary.ContainsKey(localeName)
                ? string.Empty
                : _mapLocaleNameToCountryNameDictionary[localeName];


        /// <summary>
        /// Подсчет стоимости ресурса для спонсируемого аккаунта
        /// </summary>
        /// <param name="accountUser"></param>
        /// <param name="resourcesConfiguration"></param>
        /// <param name="cancellationToken"></param>
        public async Task<LicenseOfRent1CDto?> CalculatePartialCostForSponsorAccountAsync(LicenseOfRent1CDto? accountUser, ResourcesConfiguration? resourcesConfiguration, CancellationToken cancellationToken)
        {
            if (accountUser == null || resourcesConfiguration == null)
            {
                return null;
            }

            var expireDateOfService = resourcesConfiguration.ExpireDateValue > DateTime.Now ? resourcesConfiguration.ExpireDateValue : DateTime.Now.AddMonths(1);

            var optimalRates = (await sender.Send(
                new GetOptimalRatesByAccountAndServiceTypesQuery
                {
                    Licenses = [accountUser],
                    ServiceTypes = resourcesConfiguration.BillingService.BillingServiceTypes.ToList(),
                    ExpireDate = expireDateOfService,
                }, cancellationToken)).Result;
                
            logger.Info($"Стоимость веб {accountUser.PartialUserCostWeb}, стоимость рдп {accountUser.PartialUserCostRdp}, " +
                         $"стоимость стандарт {accountUser.PartialUserCostStandart} для аккаунта {accountUser.AccountId}");

            return optimalRates?.FirstOrDefault();

        }
    }
}
