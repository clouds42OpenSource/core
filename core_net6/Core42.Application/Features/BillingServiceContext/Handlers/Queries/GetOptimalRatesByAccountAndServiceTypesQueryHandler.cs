﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.Billing;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Core42.Application.Features.BillingServiceContext.Dtos;
using Core42.Application.Features.BillingServiceContext.Queries;
using MediatR;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.BillingServiceContext.Handlers.Queries
{

    /// <summary>
    /// Get optimal rates by account and service types query handler
    /// </summary>
    public class GetOptimalRatesByAccountAndServiceTypesQueryHandler(
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IConfiguration configuration)
        : IRequestHandler<
            GetOptimalRatesByAccountAndServiceTypesQuery, ManagerResult<List<LicenseOfRent1CDto>>>
    {
        private readonly Guid _defaultLocale = Guid.Parse(configuration["DefaultLocale"]!);


        /// <summary>
        /// Handle get optimal rates by account and service types query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public async Task<ManagerResult<List<LicenseOfRent1CDto>>> Handle(GetOptimalRatesByAccountAndServiceTypesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                foreach (var license in request.Licenses)
                {
                    if (license.BillingAccountId.HasValue)
                    {
                        continue;
                    }

                    var billingAccount = new BillingAccount
                    {
                        Id = license.AccountId,
                        AccountType = AccountLevel.Standart.ToString(),
                        MonthlyPaymentDate = DateTime.Today.AddMonths(1),
                    };

                    unitOfWork.BillingAccountRepository.Insert(billingAccount);

                    await unitOfWork.SaveAsync();

                    license.BillingAccountId = billingAccount.Id;
                    license.AccountType = billingAccount.AccountType;
                }

                var billingAccounts = request.Licenses.Select(x => x.BillingAccountId).ToList();
                var accountTypes = request.Licenses.Select(x => x.AccountType).ToList();
                var licensesLocaleIds = request.Licenses.Select(x => x.LocaleId).ToList();

                var ids = request.ServiceTypes.Select(x => x.Id).ToList();

                var webCostServiceId = request.ServiceTypes
                    .FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUserWeb)?.Id;
                var rdpCostServiceId = request.ServiceTypes
                    .FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser)?.Id;

                var accountRates = await unitOfWork.AccountRateRepository
                    .AsQueryable()
                    .Where(x => billingAccounts.Contains(x.AccountId) && ids.Contains(x.BillingServiceTypeId))
                    .ToListAsync(cancellationToken);

                List<Guid> localeIds = licensesLocaleIds.Any(x => x.HasValue) ? licensesLocaleIds
                    .Where(x => x.HasValue)
                    .Select(x => x!.Value).ToList() : [_defaultLocale];

                var licensesLocaleNames = request.Licenses.Where(x => x.LocaleId.HasValue).Select(x => new { Name = x.LocaleName, ID = x.LocaleId!.Value}).ToList();

                if (!licensesLocaleNames.Any())
                {
                    licensesLocaleNames = await unitOfWork.LocaleRepository
                        .AsQueryable()
                        .Where(x => localeIds.Contains(x.ID))
                        .Select(x => new { x.Name, x.ID })
                        .ToListAsync(cancellationToken);
                }

                var rates = await unitOfWork.RateRepository
                    .AsQueryable()
                    .Where(x => ids.Contains(x.BillingServiceTypeId) && accountTypes.Contains(x.AccountType) && x.LocaleId.HasValue &&
                                localeIds.Contains(x.LocaleId.Value))
                    .ToListAsync(cancellationToken);
                
                if (!request.ExpireDate.HasValue)
                {
                    request.Licenses.ForEach(x => 
                    {
                        var webCost = (accountRates.FirstOrDefault(z => z.BillingServiceTypeId == webCostServiceId && z.AccountId == x.AccountId)?.Cost
                                       ?? rates.FirstOrDefault(y => y.BillingServiceTypeId == webCostServiceId && y.AccountType == x.AccountType && y.LocaleId == (x.LocaleId ?? _defaultLocale))?.Cost) ??
                                      throw new ArgumentNullException(
                                          $"У данного сервиса {webCostServiceId} не найдено подходящего тарифа");

                        var rdpCost = (accountRates.FirstOrDefault(z => z.BillingServiceTypeId == rdpCostServiceId && z.AccountId == x.AccountId)?.Cost ??
                                       rates.FirstOrDefault(y => y.BillingServiceTypeId == rdpCostServiceId && y.AccountType == x.AccountType && y.LocaleId == (x.LocaleId ?? _defaultLocale))?.Cost) ??
                                      throw new ArgumentNullException(
                                          $"У данного сервиса {rdpCostServiceId} не найдено подходящего тарифа");

                        x.WebCost = webCost;
                        x.RdpCost = rdpCost;
                    });

                    return request.Licenses.ToOkManagerResult();
                }

                request.Licenses.ForEach(x =>
                {
                    var locale = licensesLocaleNames.FirstOrDefault(z => z.ID == x.LocaleId);

                    var webCost = (accountRates.FirstOrDefault(z => z.BillingServiceTypeId == webCostServiceId && z.AccountId == x.AccountId)?.Cost
                                   ?? rates.FirstOrDefault(y => y.BillingServiceTypeId == webCostServiceId && y.AccountType == x.AccountType && y.LocaleId == (locale?.ID ?? _defaultLocale))?.Cost) ??
                                  throw new ArgumentNullException(
                                      $"У данного сервиса {webCostServiceId} не найдено подходящего тарифа");

                    var rdpCost = (accountRates.FirstOrDefault(z => z.BillingServiceTypeId == rdpCostServiceId && z.AccountId == x.AccountId)?.Cost ??
                                   rates.FirstOrDefault(y => y.BillingServiceTypeId == rdpCostServiceId && y.AccountType == x.AccountType && y.LocaleId == (locale?.ID ?? _defaultLocale))?.Cost) ??
                                  throw new ArgumentNullException(
                                      $"У данного сервиса {rdpCostServiceId} не найдено подходящего тарифа");

                    var partialUserCostWeb =
                        BillingServicePartialCostCalculator.CalculateUntilExpireDate(webCost, request.ExpireDate ?? DateTime.Now,
                            locale?.Name);

                    var partialUserCostRdp =
                        BillingServicePartialCostCalculator.CalculateUntilExpireDate(rdpCost, request.ExpireDate ?? DateTime.Now,
                            locale?.Name);

                    var partialUserCostStandart = BillingServicePartialCostCalculator.CalculateUntilExpireDate(
                        webCost + rdpCost,
                        request.ExpireDate ?? DateTime.Now, locale?.Name);

                    x.PartialUserCostRdp = partialUserCostRdp;
                    x.PartialUserCostWeb = partialUserCostWeb;
                    x.PartialUserCostStandart = partialUserCostStandart;
                    x.WebCost = webCost;
                    x.RdpCost = rdpCost;

                });

                return request.Licenses
                    .ToOkManagerResult();
            }

            catch (Exception e)
            {
                logger.Error("Ошибка получения оптимальных значений стоимости веб и рдп", e);

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<LicenseOfRent1CDto>>(e.Message);
            }
        }
    }
}
