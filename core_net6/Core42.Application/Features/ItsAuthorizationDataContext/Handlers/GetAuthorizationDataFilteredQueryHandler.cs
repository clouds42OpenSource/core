﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ItsAuthorizationDataContext.Dtos;
using Core42.Application.Features.ItsAuthorizationDataContext.Queries;
using Core42.Application.Features.ItsAuthorizationDataContext.Specifications;
using MediatR;
using PagedListExtensionsNetFramework;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ItsAuthorizationDataContext.Handlers
{
    /// <summary>
    /// Get auth data filtered query handler
    /// </summary>
    public class GetAuthorizationDataFilteredQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider)
        : IRequestHandler<GetAuthorizationDataFilteredQuery, ManagerResult<PagedDto<ItsAuthorizationDataDto>>>
    {
        /// <summary>
        /// Handle get auth data filtered query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<ItsAuthorizationDataDto>>> Handle(GetAuthorizationDataFilteredQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.GetItsAuthorizationDataForSelectList, () => accessProvider.ContextAccountId);

                return (await unitOfWork.GetGenericRepository<ItsAuthorizationData>()
                    .AsQueryable()
                    .Where(ItsAuthorizationDataSpecification.BySearchLine(request?.Filter?.SearchLine))
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<ItsAuthorizationDataDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }

            catch (Exception ex)
            {
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<ItsAuthorizationDataDto>>(ex.Message);
            }
        }
    }
}
