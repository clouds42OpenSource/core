﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ItsAuthorizationDataContext.Dtos;
using Core42.Application.Features.ItsAuthorizationDataContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ItsAuthorizationDataContext.Handlers
{
    /// <summary>
    /// Get ITS auth by id query handler
    /// </summary>
    public class GetItsAuthorizationByIdQueryHandler(
        IMapper mapper,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException,
        CommonCryptoProvider commonCryptoProvider)
        : IRequestHandler<GetItsAuthorizationByIdQuery, ManagerResult<ItsAuthorizationDataDto>>
    {
        /// <summary>
        /// Handle get ITS auth by id query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ManagerResult<ItsAuthorizationDataDto>> Handle(GetItsAuthorizationByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.EditItsAuthorizationData, () => accessProvider.ContextAccountId);

                var record = await unitOfWork
                     .GetGenericRepository<ItsAuthorizationData>()
                     .AsQueryable()
                     .ProjectTo<ItsAuthorizationDataDto>(mapper.ConfigurationProvider)
                     .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken) ?? throw new NotFoundException($"Данные авторизации ИТС не найдены по ID: {request.Id}");

                if (!string.IsNullOrEmpty(record.PasswordHash))
                {
                    record.PasswordHash = commonCryptoProvider.Decrypt(record.PasswordHash, CryptoDestinationTypeEnum.CommonAes);
                }

                return record.ToOkManagerResult();


            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при получении данных авторизации в ИТС по ID.]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<ItsAuthorizationDataDto>(ex.Message);
            }
        }
    }
}
