﻿using AutoMapper;
using Clouds42.Domain.DataModels.Security;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ItsAuthorizationDataContext.Dtos
{
    /// <summary>
    /// ITS auth data dto
    /// </summary>
    public class ItsAuthorizationDataDto : IMapFrom<ItsAuthorizationData>
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Login
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Password hash
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<ItsAuthorizationData, ItsAuthorizationDataDto>();
        }
    }
}
