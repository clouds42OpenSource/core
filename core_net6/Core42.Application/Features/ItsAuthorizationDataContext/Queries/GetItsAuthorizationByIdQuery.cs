﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ItsAuthorizationDataContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ItsAuthorizationDataContext.Queries
{
    /// <summary>
    /// Get ITS auth by id query
    /// </summary>
    public class GetItsAuthorizationByIdQuery(Guid id) : IRequest<ManagerResult<ItsAuthorizationDataDto>>
    {
        /// <summary>
        /// ITS Auth id
        /// </summary>
        public Guid Id { get; set; } = id;
    }
}
