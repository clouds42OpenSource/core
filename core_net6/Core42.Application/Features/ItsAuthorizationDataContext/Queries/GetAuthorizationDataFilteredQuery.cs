﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels.Security;
using Core42.Application.Features.ItsAuthorizationDataContext.Dtos;
using Core42.Application.Features.ItsAuthorizationDataContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ItsAuthorizationDataContext.Queries
{
    /// <summary>
    /// Get auth data filtered query
    /// </summary>
    public class GetAuthorizationDataFilteredQuery : 
        IHasFilter<AuthorizationDataFilter>, 
        IRequest<ManagerResult<PagedDto<ItsAuthorizationDataDto>>>, 
        ISortedQuery, 
        IPagedQuery
    {
        /// <summary>
        /// Auth data filter
        /// </summary>
        public AuthorizationDataFilter? Filter { get; set; }

        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(ItsAuthorizationData.Login)}.desc";

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get;set; }
    }
}
