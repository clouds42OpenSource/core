﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ItsAuthorizationDataContext.Queries.Filters
{
    /// <summary>
    /// Auth data filter
    /// </summary>
    public class AuthorizationDataFilter : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }
    }
}
