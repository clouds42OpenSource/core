﻿using Clouds42.Domain.DataModels.Security;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ItsAuthorizationDataContext.Specifications
{
    /// <summary>
    /// ITS auth specification
    /// </summary>
    public static class ItsAuthorizationDataSpecification
    {
        /// <summary>
        /// Filter ITS auth data by search line
        /// </summary>
        /// <param name="searchLine"></param>
        /// <returns></returns>
        public static Spec<ItsAuthorizationData> BySearchLine(string? searchLine)
        {
            return new Spec<ItsAuthorizationData>(x => string.IsNullOrEmpty(searchLine) || x.Login.ToLower().Contains(searchLine.ToLower()));
        }
    }
}
