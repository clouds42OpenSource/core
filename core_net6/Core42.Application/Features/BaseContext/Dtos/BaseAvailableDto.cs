﻿namespace Core42.Application.Features.BaseContext.Dtos
{
    public class BaseAvailableDto<T, T1>(List<T> available, List<T1>? selected)
    {
        public List<T> Available { get; set; } = available;
        public List<T1>? Selected { get; set; } = selected;
    }
}
