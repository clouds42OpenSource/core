﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Features.BaseContext.Dtos;

public class BaseDictionaryDto : DictionaryDto<Guid, string>,
    IMapFrom<CloudServicesContentServer>,
    IMapFrom<CloudServicesTerminalFarm>,
    IMapFrom<CloudServicesEnterpriseServer>,
    IMapFrom<CloudServicesFileStorageServer>,
    IMapFrom<CloudServicesSqlServer>,
    IMapFrom<CloudServicesGatewayTerminal>,
    IMapFrom<CoreHosting>,
    IMapFrom<CloudServicesBackupStorage>,
    IMapFrom<AvailableMigration>,
    IMapTo<CloudServicesSegment>,
    IMapTo<AutoUpdateNode>
{
    [JsonIgnore]
    public string Version { get; set; }
    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesContentServer, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<CloudServicesTerminalFarm, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<CloudServicesEnterpriseServer, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Version, z => z.MapFrom(y => y.Version))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<CloudServicesFileStorageServer, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<CloudServicesSqlServer, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<CloudServicesGatewayTerminal, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<CoreHosting, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.Id))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<CloudServicesBackupStorage, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<AvailableMigration, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.SegmentIdTo))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.SegmentTo.Name));

        profile.CreateMap<CloudServicesSegment, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.ID))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.Name));

        profile.CreateMap<AutoUpdateNode, BaseDictionaryDto>()
            .ForMember(x => x.Key, z => z.MapFrom(y => y.Id))
            .ForMember(x => x.Value, z => z.MapFrom(y => y.NodeAddress));
    }
}
