﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Mailing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.MarketingContext.Dtos;
using Core42.Application.Features.MarketingContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.MarketingContext.Handlers
{
    /// <summary>
    /// Handler for a query to get letters' templates
    /// </summary>
    public class GetLetterTemplateQueryHandler(
        IAccessProvider accessProvider,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : IRequestHandler<GetLetterTemplateQuery, ManagerResult<List<LetterTemplateDto>>>
    {
        /// <summary>
        /// Handle a query to get letters' templates
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<LetterTemplateDto>>> Handle(GetLetterTemplateQuery request, CancellationToken cancellationToken)
        {
            string logMessage = "Получение шаблонов писем";
            try
            {
                accessProvider.HasAccess(ObjectAction.LetterTemplate_Get, () => accessProvider.ContextAccountId);
                logger.Info(logMessage);

                return (await
                    (from letter in unitOfWork.GetGenericRepository<LetterTemplate>().AsQueryable().AsNoTracking()
                     join relatedBanner in unitOfWork.GetGenericRepository<LetterAdvertisingBannerRelation>().AsQueryable().AsNoTracking()
                         on letter.Id equals relatedBanner.LetterTemplateId into relations
                     from relation in relations.DefaultIfEmpty()
                     select new LetterTemplateDto
                     {
                         Id = letter.Id,
                         Subject = letter.Subject,
                         LetterCategory = letter.LetterTypeName,
                         IsSystem = letter.IsSystemLetter,
                         AdvertisingBannerTemplateName = relation.AdvertisingBannerTemplate.Header
                     })
                    .ToListAsync(cancellationToken))
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{logMessage} завершилось с ошибкой]");

                return PagedListExtensions.ToPreconditionFailedManagerResult<List<LetterTemplateDto>>(ex.Message);
            }
        }
    }
}
