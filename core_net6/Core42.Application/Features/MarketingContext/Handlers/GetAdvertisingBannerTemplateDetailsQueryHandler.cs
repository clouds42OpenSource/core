﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Clouds42.AdvertisingBanner.Contracts;
using Clouds42.AdvertisingBanner.Mappers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.MarketingContext.Dtos;
using Core42.Application.Features.MarketingContext.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using PagedListExtensionsNetFramework;
using AdvertisingBannerAudienceDto = Core42.Application.Features.MarketingContext.Dtos.AdvertisingBannerAudienceDto;
using AdvertisingBannerTemplateDetailsDto = Core42.Application.Features.MarketingContext.Dtos.AdvertisingBannerTemplateDetailsDto;

namespace Core42.Application.Features.MarketingContext.Handlers
{
    /// <summary>
    /// Handler for a query to get detials on advertising banner's template
    /// </summary>
    public class GetAdvertisingBannerTemplateDetailsQueryHandler(
        IAccessProvider accessProvider,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IAdvertisingBannerAudienceProvider advertisingBannerAudienceProvider,
        IMapper mapper)
        : IRequestHandler<GetAdvertisingBannerTemplateDetailsQuery,
            ManagerResult<AdvertisingBannerTemplateDetailsDto>>
    {
        /// <summary>
        /// Handle a query to get detials on advertising banner's template
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<AdvertisingBannerTemplateDetailsDto>> Handle(GetAdvertisingBannerTemplateDetailsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AdvertisingBannerTemplate_Get, () => accessProvider.ContextAccountId);
                logger.Info($"Получение шаблона рекламного баннера с идентификатором {request.TemplateId}");

                var advertisingBannerTemplate = await unitOfWork.GetGenericRepository<AdvertisingBannerTemplate>()
                    .AsQueryable()
                    .AsNoTracking()
                    .FirstOrDefaultAsync(template => template.Id == request.TemplateId, cancellationToken)
                    ?? throw new NotFoundException($"Шаблон рекламного баннера '{request.TemplateId}' не существует");

                var advertisingBannerAudience = mapper.Map<AdvertisingBannerAudienceDto>(advertisingBannerTemplate.AdvertisingBannerAudience);

                var calculationAdvertisingBannerAudience = await advertisingBannerAudienceProvider
                    .SelectAdvertisingBannerAudience(advertisingBannerAudience.Locales, 
                        advertisingBannerAudience.AccountType, 
                        advertisingBannerAudience.AvailabilityPayment, 
                        advertisingBannerAudience.RentalServiceState)
                    .GroupBy(aud => aud.Id)
                    .Select(group => new CalculationAdvertisingBannerAudienceDto
                    {
                        AccountsCount = group.Count(),
                        AccountUsersCount = group.Sum(g => g.AccountUsers.Count),
                    })
                    .FirstOrDefaultAsync(cancellationToken);

                return new AdvertisingBannerTemplateDetailsDto
                {
                    TemplateId = advertisingBannerTemplate.Id,
                    Body = advertisingBannerTemplate.Body,
                    Header = advertisingBannerTemplate.Header,
                    CaptionLink = advertisingBannerTemplate.CaptionLink,
                    Link = advertisingBannerTemplate.Link,
                    ShowFrom = advertisingBannerTemplate.DisplayBannerDateFrom,
                    ShowTo = advertisingBannerTemplate.DisplayBannerDateTo,
                    Image = advertisingBannerTemplate.Image.MapToFileDataDto<IFormFile>(),
                    AdvertisingBannerAudience = advertisingBannerAudience,
                    CalculationAdvertisingBannerAudience = calculationAdvertisingBannerAudience
                }.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения модели шаблона рекламного баннера]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<AdvertisingBannerTemplateDetailsDto>(ex.Message);
            }
        }
    }
}
