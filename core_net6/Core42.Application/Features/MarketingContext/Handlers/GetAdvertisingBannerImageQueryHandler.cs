﻿using Microsoft.EntityFrameworkCore;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.MarketingContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.MarketingContext.Handlers
{
    /// <summary>
    /// Handler for a query to get advertising banner's image
    /// </summary>
    public class GetAdvertisingBannerImageQueryHandler(
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : IRequestHandler<GetAdvertisingBannerImageQuery, ManagerResult<FileDataDto<byte[]>>>
    {
        /// <summary>
        /// Handle a query to get advertising banner's image
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<FileDataDto<byte[]>>> Handle(GetAdvertisingBannerImageQuery request, CancellationToken cancellationToken)
        {
            try
            {
                if (request.BannerId == default)
                    throw new ArgumentException("Не указан идентификатор рекламного баннера");

                var advertisingBannerTemplate = await unitOfWork.GetGenericRepository<AdvertisingBannerTemplate>()
                    .AsQueryable()
                    .AsNoTracking()
                    .FirstOrDefaultAsync(template => template.Id == request.BannerId, cancellationToken)
                    ?? throw new NotFoundException($"Шаблона рекламного баннера с указанным идентификатором ''{request.BannerId} не существует");

                if (advertisingBannerTemplate?.Image == null)
                    throw new NotFoundException(
                        $"У шаблона рекламного баннера с указанным идентфикатором '{request.BannerId}' отсутствует изображение");

                return new FileDataDto<byte[]>
                {
                    Content = advertisingBannerTemplate.Image.Content,
                    FileName = advertisingBannerTemplate.Image.FileName,
                    ContentType = advertisingBannerTemplate.Image.ContentType,
                    Base64 = Convert.ToBase64String(advertisingBannerTemplate.Image.Content)
                }.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Во время получения изображения рекламного баннера с идентфикатором '{request.BannerId}' произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<FileDataDto<byte[]>>(ex.Message);
            }
        }
    }
}
