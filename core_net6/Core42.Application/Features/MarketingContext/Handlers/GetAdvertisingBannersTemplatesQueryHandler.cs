﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.MarketingContext.Dtos;
using Core42.Application.Features.MarketingContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.MarketingContext.Handlers
{
    /// <summary>
    /// Handler for a query to get advertising banners templates
    /// </summary>
    public class GetAdvertisingBannersTemplatesQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IMapper mapper)
        : IRequestHandler<GetAdvertisingBannersTemplatesQuery,
            ManagerResult<IEnumerable<AdvertisingBannerTemplateDto>>>
    {
        /// <summary>
        /// Handle a query to get advertising banners templates
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<IEnumerable<AdvertisingBannerTemplateDto>>> Handle(GetAdvertisingBannersTemplatesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.AdvertisingBannerTemplate_Get, () => accessProvider.ContextAccountId);
                return (await unitOfWork.GetGenericRepository<AdvertisingBannerTemplate>()
                    .AsQueryable()
                    .AsNoTracking()
                    .OrderByDescending(template => template.CreationDate)
                    .ProjectTo<AdvertisingBannerTemplateDto>(mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken))
                    .AsEnumerable()
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Во время получения рекламных баннеров произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<IEnumerable<AdvertisingBannerTemplateDto>>(ex.Message);
            }
        }
    }
}
