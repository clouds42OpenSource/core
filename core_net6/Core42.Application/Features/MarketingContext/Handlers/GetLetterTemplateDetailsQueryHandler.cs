﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Market;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.Mailing;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.MarketingContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.MarketingContext.Handlers
{
    /// <summary>
    /// Handler for a query to get letter template's details
    /// </summary>
    public class GetLetterTemplateDetailsQueryHandler(
        IAccessProvider accessProvider,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : IRequestHandler<GetLetterTemplateDetailsQuery, ManagerResult<LetterTemplateDetailsDto>>
    {
        /// <summary>
        /// Handle a query to get letter template's details
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<LetterTemplateDetailsDto>> Handle(GetLetterTemplateDetailsQuery request, CancellationToken cancellationToken)
        {
            string logMessage = "Получение детальной информации о шаблоне письма";
            try
            {
                accessProvider.HasAccess(ObjectAction.LetterTemplate_Get, () => accessProvider.ContextAccountId);
                logger.Info(logMessage);

                var letterTemplate = await unitOfWork.GetGenericRepository<LetterTemplate>().AsQueryable().AsNoTracking()
                    .FirstOrDefaultAsync(letter => letter.Id == request.TemplateId, cancellationToken)
                    ?? throw new NotFoundException($"Шаблона письма с указанным идентификаторм {request.TemplateId} не существует");

                var shortBannersInfo = (await unitOfWork.GetGenericRepository<AdvertisingBannerTemplate>().AsQueryable().AsNoTracking()
                    .OrderByDescending(banner => banner.CreationDate)
                    .Select(banner => new AdvertisingBannerTemplateShortInfoDto
                    {
                        Id = banner.Id,
                        Header = banner.Header
                    })
                    .ToListAsync(cancellationToken))
                    .AsEnumerable();
                    

                var advertisingBannerTemplate = (await unitOfWork.GetGenericRepository<LetterAdvertisingBannerRelation>().AsQueryable().AsNoTracking()
                    .FirstOrDefaultAsync(relation => relation.LetterTemplateId == request.TemplateId, cancellationToken))?.AdvertisingBannerTemplate;

                return new LetterTemplateDetailsDto
                {
                    Id = letterTemplate.Id,
                    Header = letterTemplate.Header,
                    Body = letterTemplate.Body,
                    Subject = letterTemplate.Subject,
                    Category = letterTemplate.LetterTypeName,
                    SendGridTemplateId = letterTemplate.SendGridTemplateId,
                    AdvertisingBannerTemplatesShortInfo = shortBannersInfo,
                    AdvertisingBannerTemplateId = advertisingBannerTemplate?.Id,
                    NotificationText = letterTemplate.NotificationText,
                }.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[{logMessage} завершилось с ошибкой]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<LetterTemplateDetailsDto>(ex.Message);
            }
        }
    }
}
