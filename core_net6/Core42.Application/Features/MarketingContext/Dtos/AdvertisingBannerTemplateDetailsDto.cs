﻿using Clouds42.DataContracts.BaseModel;
using Microsoft.AspNetCore.Http;

namespace Core42.Application.Features.MarketingContext.Dtos
{
    public class AdvertisingBannerTemplateDetailsDto
    {
        /// <summary>
        /// Id шаблона рекламного баннера
        /// </summary>
        public int TemplateId { get; set; }

        /// <summary>
        /// Изображение баннера
        /// </summary>
        public FileDataDto<IFormFile> Image { get; set; }

        /// <summary>
        /// Заголовок баннера
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Тело баннера
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Описание ссылки
        /// </summary>
        public string CaptionLink { get; set; }

        /// <summary>
        /// Ссылка баннера
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Аудитория рекламного баннера
        /// </summary>
        public AdvertisingBannerAudienceDto AdvertisingBannerAudience { get; set; }

        /// <summary>
        /// Показывать баннер с
        /// </summary>
        public DateTime? ShowFrom { get; set; }

        /// <summary>
        /// Показывать баннер по
        /// </summary>
        public DateTime? ShowTo { get; set; }

        /// <summary>
        /// Результат подсчета аудитории рекламного баннера
        /// </summary>
        public CalculationAdvertisingBannerAudienceDto CalculationAdvertisingBannerAudience { get; set; }
    }

    public class CalculationAdvertisingBannerAudienceDto
    {
        /// <summary>
        /// Количество аккаунтов
        /// </summary>
        public int AccountsCount { get; set; }

        /// <summary>
        /// Количество пользователей
        /// </summary>
        public int AccountUsersCount { get; set; }
    }
}
