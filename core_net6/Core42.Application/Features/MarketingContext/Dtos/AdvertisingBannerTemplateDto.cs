﻿using AutoMapper;
using Clouds42.Domain.DataModels.Marketing;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.MarketingContext.Dtos
{
    /// <summary>
    /// Advertising banner template DTO
    /// </summary>
    public class AdvertisingBannerTemplateDto : IMapFrom<AdvertisingBannerTemplate>
    {
        /// <summary>
        /// Banner template identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Banner's header
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Banner's body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Caption link
        /// </summary>
        public string CaptionLink { get; set; }

        /// <summary>
        /// Show from date
        /// </summary>
        public DateTime? ShowFrom { get; set; }

        /// <summary>
        /// Show to date
        /// </summary>
        public DateTime? ShowTo { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AdvertisingBannerTemplate, AdvertisingBannerTemplateDto>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                .ForMember(x => x.Header, y => y.MapFrom(z => z.Header))
                .ForMember(x => x.Body, y => y.MapFrom(z => z.Body))
                .ForMember(x => x.CaptionLink, y => y.MapFrom(z => z.CaptionLink))
                .ForMember(x => x.ShowFrom, y => y.MapFrom(z => z.DisplayBannerDateFrom))
                .ForMember(x => x.ShowTo, y => y.MapFrom(z => z.DisplayBannerDateTo));
        }
    }
}
