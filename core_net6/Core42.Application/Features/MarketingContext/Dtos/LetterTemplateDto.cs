﻿namespace Core42.Application.Features.MarketingContext.Dtos
{
    /// <summary>
    /// Letter template DTO
    /// </summary>
    public class LetterTemplateDto
    {
        /// <summary>
        /// Letter template identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Letter subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Letter category
        /// </summary>
        public string LetterCategory { get; set; }

        /// <summary>
        /// Is letter system
        /// </summary>
        public bool IsSystem { get; set; }

        /// <summary>
        /// Advertising banner template's name
        /// </summary>
        public string AdvertisingBannerTemplateName { get; set; }
    }
}
