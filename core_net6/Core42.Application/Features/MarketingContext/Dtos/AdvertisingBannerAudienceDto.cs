﻿using AutoMapper;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Domain.Enums.Marketing;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.MarketingContext.Dtos
{
    /// <summary>
    /// Модель аудитории рекламного баннера
    /// </summary>
    public class AdvertisingBannerAudienceDto : IMapFrom<AdvertisingBannerAudience>
    {
        /// <summary>
        /// Тип аккаунта
        /// </summary>
        public AccountTypeEnum AccountType { get; set; }

        /// <summary>
        /// Наличие оплаты
        /// </summary>
        public AvailabilityPaymentEnum AvailabilityPayment { get; set; }

        /// <summary>
        /// Состояние сервиса Аренда 1С
        /// </summary>
        public RentalServiceStateEnum RentalServiceState { get; set; }

        /// <summary>
        /// Локали аккаунтов
        /// </summary>
        public List<Guid> Locales { get; set; } = [];

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AdvertisingBannerAudience, AdvertisingBannerAudienceDto>()
                .ForMember(x => x.AccountType, y => y.MapFrom(z => z.AccountType))
                .ForMember(x => x.AvailabilityPayment, y => y.MapFrom(z => z.AvailabilityPayment))
                .ForMember(x => x.RentalServiceState, y => y.MapFrom(z => z.AvailabilityPayment))
                .ForMember(x => x.Locales, y => y.MapFrom(z => z.AdvertisingBannerAudienceLocaleRelations
                    .Select(w => w.LocaleId).ToList()));
        }
    }
}
