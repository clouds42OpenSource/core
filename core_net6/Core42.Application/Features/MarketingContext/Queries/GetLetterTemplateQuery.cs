﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.MarketingContext.Dtos;
using MediatR;

namespace Core42.Application.Features.MarketingContext.Queries
{
    /// <summary>
    /// Query to get letters' templates
    /// </summary>
    public class GetLetterTemplateQuery : IRequest<ManagerResult<List<LetterTemplateDto>>>
    {
    }
}
