﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Core42.Application.Features.MarketingContext.Dtos;
using MediatR;

namespace Core42.Application.Features.MarketingContext.Queries
{
    /// <summary>
    /// Query to get detials on advertising banner's template
    /// </summary>
    public class GetAdvertisingBannerTemplateDetailsQuery : IRequest<ManagerResult<AdvertisingBannerTemplateDetailsDto>>
    {
        /// <summary>
        /// Advertising banner's template identifier
        /// </summary>
        [Required]
        public int TemplateId { get; set; }
    }
}
