﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using MediatR;

namespace Core42.Application.Features.MarketingContext.Queries
{
    /// <summary>
    /// Query to get letter template's details
    /// </summary>
    public class GetLetterTemplateDetailsQuery : IRequest<ManagerResult<LetterTemplateDetailsDto>>
    {
        /// <summary>
        /// Letter template identifier
        /// </summary>
        [Required]
        public int TemplateId { get; set; }
    }
}
