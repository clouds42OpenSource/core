﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using MediatR;

namespace Core42.Application.Features.MarketingContext.Queries
{
    /// <summary>
    /// Query to get advertising banner's image
    /// </summary>
    public class GetAdvertisingBannerImageQuery : IRequest<ManagerResult<FileDataDto<byte[]>>>
    {
        /// <summary>
        /// Advertising banner identifier
        /// </summary>
        [Required]
        public int BannerId { get; set; }
    }
}
