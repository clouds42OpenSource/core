﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.MarketingContext.Dtos;
using MediatR;

namespace Core42.Application.Features.MarketingContext.Queries
{
    /// <summary>
    /// Query to get advertising banners templates
    /// </summary>
    public class GetAdvertisingBannersTemplatesQuery : IRequest<ManagerResult<IEnumerable<AdvertisingBannerTemplateDto>>> { }
}
