﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.GatewayTerminalContext.Dtos;

public class GatewayTerminalDto : IMapFrom<CloudServicesGatewayTerminal>
{
    public Guid Id { get; set; }

    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CloudServicesGatewayTerminal, GatewayTerminalDto>()
            .ForMember(x => x.Id, z => z.MapFrom(y => y.ID));
    }
}
