﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.GatewayTerminalContext.Dtos;
using Core42.Application.Features.GatewayTerminalContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.GatewayTerminalContext.Handlers;

public class GetGatewayTerminalByIdQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetGatewayTerminalByIdQuery,
        ManagerResult<GatewayTerminalDto>>
{
    public async Task<ManagerResult<GatewayTerminalDto>> Handle(GetGatewayTerminalByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesGatewayTerminalRepository
                    .AsQueryable()
                    .ProjectTo<GatewayTerminalDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken))
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex,
                $"[Ошибка получения терминального шлюза {request.Id}]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<GatewayTerminalDto>(ex.Message);
        }
    }
}
