﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.GatewayTerminalContext.Dtos;
using Core42.Application.Features.GatewayTerminalContext.Queries;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.GatewayTerminalContext.Handlers;

public class GetFilteredGatewayTerminalsQueryHandler(
    IUnitOfWork unitOfWork,
    IHandlerException handlerException,
    IMapper mapper,
    IAccessProvider accessProvider)
    : IRequestHandler<GetFilteredGatewayTerminalsQuery,
        ManagerResult<PagedDto<GatewayTerminalDto>>>
{
    public async Task<ManagerResult<PagedDto<GatewayTerminalDto>>> Handle(GetFilteredGatewayTerminalsQuery request, CancellationToken cancellationToken)
    {
        try
        {
            accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

            return (await unitOfWork.CloudServicesGatewayTerminalRepository
                    .AsQueryable()
                    .ProjectTo<GatewayTerminalDto>(mapper.ConfigurationProvider)
                    .AutoSort(request)
                    .AutoFilter(request.Filter)
                    .ToPagedListAsync(request.PageNumber ?? 1, request.PageSize ?? 50, cancellationToken))
                .ToPagedDto()
                .ToOkManagerResult();
        }

        catch (Exception ex)
        {
            handlerException.Handle(ex, "[Ошибка получения списка терминальных шлюзов]");

            return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<GatewayTerminalDto>>(ex.Message);
        }
    }
}
