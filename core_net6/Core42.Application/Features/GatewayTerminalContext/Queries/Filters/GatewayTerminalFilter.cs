﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Features.GatewayTerminalContext.Queries.Filters;

public class GatewayTerminalFilter : IQueryFilter
{
    /// <summary>
    /// Name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Connection address
    /// </summary>
    public string ConnectionAddress { get; set; }
}
