﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.GatewayTerminalContext.Dtos;
using Core42.Application.Features.GatewayTerminalContext.Queries.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.GatewayTerminalContext.Queries
{
    public class GetFilteredGatewayTerminalsQuery: IRequest<ManagerResult<PagedDto<GatewayTerminalDto>>>, ISortedQuery, IPagedQuery, IHasFilter<GatewayTerminalFilter>
    {
        public string OrderBy { get; set; } = $"{nameof(GatewayTerminalDto.Name)}.asc";
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public GatewayTerminalFilter Filter { get; set; }
    }
}
