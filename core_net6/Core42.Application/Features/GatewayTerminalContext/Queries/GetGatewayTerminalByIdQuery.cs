﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.GatewayTerminalContext.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Application.Features.GatewayTerminalContext.Queries;

public class GetGatewayTerminalByIdQuery : IRequest<ManagerResult<GatewayTerminalDto>>
{
    [FromRoute]
    public Guid Id { get; set; }
}
