﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.AutoUpdateNodesContext.Dtos
{
    /// <summary>
    /// Auto update node DTO
    /// </summary>
    public class AutoUpdateNodeDto : IMapFrom<AutoUpdateNode>
    {
        /// <summary>
        /// Node identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Node address
        /// </summary>
        public string NodeAddress { get; set; }

        /// <summary>
        /// Путь до авто обновлятора
        /// </summary>
        public string AutoUpdateObnovlyatorPath { get; set; }

        /// <summary>
        /// Путь до ручного обновлятора 
        /// </summary>
        public string ManualUpdateObnovlyatorPath { get; set; }

        /// <summary>
        /// флаг отражающий участие в общем АО
        /// </summary>
        public bool DontRunInGlobalAU { get; set; }

        /// <summary>
        /// Занят воркер или нет?
        /// </summary>
        public bool IsBusy { get; set; }

        /// <summary>
        /// Тип ноды
        /// </summary>
        public AutoUpdateNodeType AutoUpdateNodeType { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AutoUpdateNode, AutoUpdateNodeDto>();
        }
    }
}
