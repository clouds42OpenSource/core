﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AutoUpdateNodesContext.Dtos;
using Core42.Application.Features.AutoUpdateNodesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.AutoUpdateNodesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get all auto update nodes
    /// </summary>
    public class GetAllAutoUpdatesNodesQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IMapper mapper)
        : IRequestHandler<GetAllAutoUpdatesNodesQuery, ManagerResult<PagedDto<AutoUpdateNodeDto>>>
    {
        /// <summary>
        /// Handle a query to get all auto update nodes
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<PagedDto<AutoUpdateNodeDto>>> Handle(GetAllAutoUpdatesNodesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

                return (await unitOfWork.AutoUpdateNodeRepository.AsQueryable().AsNoTracking()
                    .AutoSort(request, StringComparison.OrdinalIgnoreCase)
                    .ProjectTo<AutoUpdateNodeDto>(mapper.ConfigurationProvider)
                    .ToPagedListAsync(request?.PageNumber ?? 1, request?.PageSize ?? 50, cancellationToken))
                    .ToPagedDto()
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Во время получения нод АО произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<PagedDto<AutoUpdateNodeDto>>(ex.Message);
            }
        }
    }
}
