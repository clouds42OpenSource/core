﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AutoUpdateNodesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AutoUpdateNodesContext.Handlers
{
    /// <summary>
    /// Handler for a query to delete an auto update node
    /// </summary>
    public class DeleteAutoUpdateNodeQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException)
        : IRequestHandler<DeleteAutoUpdateNodeQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle a query to delete an auto update node
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<bool>> Handle(DeleteAutoUpdateNodeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

                unitOfWork.AutoUpdateNodeRepository.Delete(request.Id);
                await unitOfWork.SaveAsync();

                return true.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Во время удаления ноды АО произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
            }
        }
    }
}
