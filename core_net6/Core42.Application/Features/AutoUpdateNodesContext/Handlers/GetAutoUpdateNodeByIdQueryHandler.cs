﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AutoUpdateNodesContext.Dtos;
using Core42.Application.Features.AutoUpdateNodesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AutoUpdateNodesContext.Handlers
{
    /// <summary>
    /// Handler for a query to get an auto update node
    /// </summary>
    public class GetAutoUpdateNodeByIdQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IMapper mapper)
        : IRequestHandler<GetAutoUpdateNodeByIdQuery, ManagerResult<AutoUpdateNodeDto>>
    {
        /// <summary>
        /// Handle a query to get an auto update node
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<AutoUpdateNodeDto>> Handle(GetAutoUpdateNodeByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

                return (await unitOfWork.AutoUpdateNodeRepository.AsQueryable().AsNoTracking()
                    .ProjectTo<AutoUpdateNodeDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(node => node.Id == request.Id, cancellationToken))
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Во время получения информации о ноде АО произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<AutoUpdateNodeDto>(ex.Message);
            }
        }
    }
}
