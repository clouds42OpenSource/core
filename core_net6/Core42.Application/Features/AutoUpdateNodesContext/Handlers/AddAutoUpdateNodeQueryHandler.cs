﻿using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AutoUpdateNodesContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AutoUpdateNodesContext.Handlers
{
    /// <summary>
    /// Handler for a query to add an auto update node
    /// </summary>
    public class AddAutoUpdateNodeQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IMapper mapper)
        : IRequestHandler<AddAutoUpdateNodeQuery, ManagerResult<Guid>>
    {
        /// <summary>
        /// Handle a query to add an auto update node
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<Guid>> Handle(AddAutoUpdateNodeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);

                var entity = mapper.Map<AddAutoUpdateNodeQuery, AutoUpdateNode>(request);

                unitOfWork.AutoUpdateNodeRepository.Insert(entity);
                await unitOfWork.SaveAsync();

                return entity.Id.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Во время добавления ноды АО произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<Guid>(ex.Message);
            }
        }
    }
}
