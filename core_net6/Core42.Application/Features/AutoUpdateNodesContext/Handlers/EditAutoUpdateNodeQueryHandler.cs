﻿using AutoMapper;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.AutoUpdateNodesContext.Queries;
using MediatR;
using Newtonsoft.Json;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AutoUpdateNodesContext.Handlers
{
    /// <summary>
    /// Handler for a query to edit an auto update node
    /// </summary>
    public class EditAutoUpdateNodeQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IMapper mapper,
        ILogger42 logger)
        : IRequestHandler<EditAutoUpdateNodeQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle a query to edit an auto update node
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<bool>> Handle(EditAutoUpdateNodeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.Segment_Edit, () => accessProvider.ContextAccountId);
                
                var entity = mapper.Map<EditAutoUpdateNodeQuery, AutoUpdateNode>(request);

                unitOfWork.AutoUpdateNodeRepository.Update(entity);
                logger.Info($"Состояние ноды {entity} изменено на {JsonConvert.SerializeObject(entity)}");
                await unitOfWork.SaveAsync();

                return true.ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Во время редактирования ноды АО произошла ошибка]");
                return PagedListExtensions.ToPreconditionFailedManagerResult<bool>(ex.Message);
            }
        }
    }
}
