﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Core42.Application.Features.AutoUpdateNodesContext.Dtos;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.AutoUpdateNodesContext.Queries
{
    /// <summary>
    /// Query to get all auto update nodes
    /// </summary>
    public class GetAllAutoUpdatesNodesQuery : IRequest<ManagerResult<PagedDto<AutoUpdateNodeDto>>>, IPagedQuery, ISortedQuery
    {
        /// <inheritdoc/>
        public int? PageNumber { get; set; }

        /// <inheritdoc/>
        public int? PageSize { get; set; }

        /// <inheritdoc/>
        public string OrderBy { get; set; } = $"{nameof(AutoUpdateNode.NodeAddress)}.asc";
    }
}
