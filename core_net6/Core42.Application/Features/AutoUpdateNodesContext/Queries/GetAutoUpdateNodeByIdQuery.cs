﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using Core42.Application.Features.AutoUpdateNodesContext.Dtos;
using MediatR;

namespace Core42.Application.Features.AutoUpdateNodesContext.Queries
{
    /// <summary>
    /// Query to get an auto update node
    /// </summary>
    public class GetAutoUpdateNodeByIdQuery : IRequest<ManagerResult<AutoUpdateNodeDto>>
    {
        /// <summary>
        /// Auto update node identifier
        /// </summary>
        [Required]
        public Guid Id { get; set; }
    }
}
