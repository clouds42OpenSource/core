﻿using AutoMapper;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using MappingNetStandart.Mappings;
using MediatR;

namespace Core42.Application.Features.AutoUpdateNodesContext.Queries
{
    /// <summary>
    /// Query to edit an auto update node
    /// </summary>
    public class EditAutoUpdateNodeQuery : IRequest<ManagerResult<bool>>, IMapTo<AutoUpdateNode>
    {
        /// <summary>
        /// Node identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Node address
        /// </summary>
        public string NodeAddress { get; set; }

        /// <summary>
        /// Путь до авто обновлятора
        /// </summary>
        public string AutoUpdateObnovlyatorPath { get; set; }

        /// <summary>
        /// Путь до ручного обновлятора 
        /// </summary>
        public string ManualUpdateObnovlyatorPath { get; set; }

        /// <summary>
        /// флаг отражающий участие в общем АО
        /// </summary>
        public bool DontRunInGlobalAU { get; set; }

        /// <summary>
        /// node type
        /// </summary>
        public AutoUpdateNodeType AutoUpdateNodeType { get; set; }

        /// <summary>
        /// Занят воркер или нет?
        /// </summary>
        public bool IsBusy { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<EditAutoUpdateNodeQuery, AutoUpdateNode>();
        }
    }
}
