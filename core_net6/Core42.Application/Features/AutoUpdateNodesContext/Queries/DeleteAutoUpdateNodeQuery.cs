﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.AutoUpdateNodesContext.Queries
{
    /// <summary>
    /// Query to delete an auto update node
    /// </summary>
    public class DeleteAutoUpdateNodeQuery : IRequest<ManagerResult<bool>>
    {
        /// <summary>
        /// Auto update node identifier
        /// </summary>
        [Required]
        public Guid Id { get; set; }
    }
}
