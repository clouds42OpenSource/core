﻿using AutoMapper;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using MappingNetStandart.Mappings;
using MediatR;

namespace Core42.Application.Features.AutoUpdateNodesContext.Queries
{
    /// <summary>
    /// Query to add an auto update node
    /// </summary>
    public class AddAutoUpdateNodeQuery : IRequest<ManagerResult<Guid>>, IMapTo<AutoUpdateNode>
    {
        /// <summary>
        /// Node address
        /// </summary>
        public string NodeAddress { get; set; }

        /// <summary>
        /// Path to a folder contains import of the AutoUpdater
        /// </summary>
        public string ImportFolderPath { get; set; }

        /// <summary>
        /// Путь до авто обновлятора
        /// </summary>
        public string AutoUpdateObnovlyatorPath { get; set; }

        /// <summary>
        /// Путь до ручного обновлятора 
        /// </summary>
        public string ManualUpdateObnovlyatorPath { get; set; }

        /// <summary>
        /// node type
        /// </summary>
        public AutoUpdateNodeType AutoUpdateNodeType { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AddAutoUpdateNodeQuery, AutoUpdateNode>()
                .AfterMap((query, node) => node.Id = Guid.NewGuid());
        }
    }
}
