﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArmContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArmContext.Handlers
{
    /// <summary>
    /// Handler for a query to change database configuration status
    /// </summary>
    public class ChangeDbConfigurationStatusQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork)
        : IRequestHandler<ChangeDbConfigurationStatusQuery, ManagerResult<bool>>
    {
        /// <summary>
        /// Handle a query to change database configuration status
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<bool>> Handle(ChangeDbConfigurationStatusQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ARM);

                var configuration = await unitOfWork.ConfigurationsCfuRepository.AsQueryable()
                    .FirstOrDefaultAsync(conf => conf.Id == request.ConfigurationId, cancellationToken)
                    ?? throw new NotFoundException("Указанной конфигурации не существует");

                configuration.ValidateState = request.ApprovalStatus;
                configuration.InitiatorId = (await accessProvider.GetUserAsync())?.Id;

                unitOfWork.ConfigurationsCfuRepository.Update(configuration);
                await unitOfWork.SaveAsync();

                return true.ToOkManagerResult();

            }
            catch (Exception ex)
            {
                return false.ToOkManagerResult(message: ex.Message);
            }
        }
    }
}
