﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums._1C;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArmContext.Dtos;
using Core42.Application.Features.ArmContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArmContext.Handlers
{
    /// <summary>
    /// Handler for a query to get info of TiI and AO for specified period
    /// </summary>
    public class GetInfoForPeriodQueryHandler(
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IAccessProvider accessProvider)
        : IRequestHandler<GetInfoForPeriodQuery, ManagerResult<List<DbSupportDto>>>
    {
        /// <summary>
        /// Handle a query to get info of TiI and AO for specified period
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<List<DbSupportDto>>> Handle(GetInfoForPeriodQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ARM);
                return (await unitOfWork.AcDbSupportHistoryRepository.AsQueryable().AsNoTracking()
                    .Include(d => d.AcDbSupport)
                    .Where(ArmSpecification.ByPeriod(request.Filter.StartRange, request.Filter.EndRange.AddDays(1).Date))
                    .Where(ArmSpecification.ByDatabaseOperation(request.Filter?.DbOperation))
                    .Where(ArmSpecification.ByConfigurations(request.Filter?.Configurations))
                    .GroupBy(hist => hist.EditDate.Date)
                    .Select(group => new DbSupportDto
                    {
                        Date = group.Key,
                        TehSupportPlanCount = group.Count(w => w.Operation == DatabaseSupportOperation.PlanTehSupport && w.State == SupportHistoryState.Success),
                        TehSupportFactCount = group.Count(w => w.Operation == DatabaseSupportOperation.TechSupport && w.State == SupportHistoryState.Success),
                        AutoUpdatePlanCount = group.Count(w => w.Operation == DatabaseSupportOperation.PlanAutoUpdate && w.State == SupportHistoryState.Success),
                        AutoUpdateFactCount = group.Count(w => w.Operation == DatabaseSupportOperation.AutoUpdate && w.State == SupportHistoryState.Success),
                        ErrorsCount = group.Count(w => w.State == SupportHistoryState.Error && w.Operation != DatabaseSupportOperation.AuthToDb),
                        ErrorsConnectCount = group.Count(w => w.State == SupportHistoryState.Error && w.Operation == DatabaseSupportOperation.AuthToDb)
                    })
                    .OrderBy(d => d.Date)
                    .ToListAsync(cancellationToken)).ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"Во время получения информации ТиИ и АО за период " +
                    $"'{request?.Filter?.StartRange.Date} - {request?.Filter?.EndRange.Date}'  по типу операции " +
                    $"'{request?.Filter?.DbOperation?.Description() ?? "Все"}' произошла ошибка. ");
                return PagedListExtensions.ToPreconditionFailedManagerResult<List<DbSupportDto>>(ex.Message);
            }
        }
    }
}
