﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArmContext.Dtos;
using Core42.Application.Features.ArmContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArmContext.Handlers
{
    /// <summary>
    /// Handler for a query to get new releases of db configuration
    /// </summary>
    public class GetDbConfigurationQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IHandlerException handlerException,
        IMapper mapper)
        : IRequestHandler<GetDbConfigurationQuery, ManagerResult<IEnumerable<DbConfigurationDto>>>
    {
        /// <summary>
        /// Handle a query to get new releases of db configuration
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<IEnumerable<DbConfigurationDto>>> Handle(GetDbConfigurationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ARM);

                return (await unitOfWork.ConfigurationsCfuRepository.AsQueryable().AsNoTracking()
                    .Where(conf => conf.DownloadDate == null ||
                        conf.DownloadDate.Value >= request.Filter.StartRange.Date && conf.DownloadDate.Value < request.Filter.EndRange.AddDays(1).Date)
                    .ProjectTo<DbConfigurationDto>(mapper.ConfigurationProvider)
                    .OrderByDescending(conf => conf.Version)
                    .ToListAsync(cancellationToken))
                    .AsEnumerable()
                    .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"Во время получения конфигураций баз за период " +
                    $"'{request?.Filter?.StartRange.Date} - {request?.Filter?.EndRange.Date}' произошла ошибка. ");
                return PagedListExtensions.ToPreconditionFailedManagerResult<IEnumerable<DbConfigurationDto>>(ex.Message);
            }
        }
    }
}
