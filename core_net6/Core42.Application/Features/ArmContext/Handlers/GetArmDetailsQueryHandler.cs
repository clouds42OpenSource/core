﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ArmContext.Dtos;
using Core42.Application.Features.ArmContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.ArmContext.Handlers
{
    /// <summary>
    /// Handler for a query to get details of TiI and AO
    /// </summary>
    public class GetArmDetailsQueryHandler(
        IAccessProvider accessProvider,
        IMapper mapper,
        IHandlerException handlerException,
        IUnitOfWork unitOfWork)
        : IRequestHandler<GetArmDetailsQuery, ManagerResult<IEnumerable<TiIUpdatesDetailDto>>>
    {
        /// <summary>
        /// Handle a query to get details of TiI and AO
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<IEnumerable<TiIUpdatesDetailDto>>> Handle(GetArmDetailsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ARM);

                return (await unitOfWork.AcDbSupportHistoryRepository
                        .AsQueryableNoTracking()
                        .Include(x => x.AcDbSupport)
                        .ThenInclude(x => x.AccountDatabase)
                        .ThenInclude(x => x.Account)
                        .Where(ArmSpecification.ByPeriod(request.Filter.StartRange, request.Filter.EndRange))
                        .Where(ArmSpecification.ByDatabaseOperation(request.Filter?.DbOperation))
                        .Where(ArmSpecification.ByConfigurations(request.Filter?.Configurations))
                        .ProjectTo<TiIUpdatesDetailDto>(mapper.ConfigurationProvider)
                        .OrderByDescending(d => d.Date)
                        .ToListAsync(cancellationToken))
                        .AsEnumerable()
                        .ToOkManagerResult();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"Во время получения детализации о ТиИ и АО за период " +
                    $"'{request?.Filter?.StartRange.Date} - {request?.Filter?.EndRange.Date}'  по типу операции " +
                    $"'{request?.Filter?.DbOperation?.Description() ?? "Все"}' произошла ошибка. ");
                return PagedListExtensions.ToPreconditionFailedManagerResult<IEnumerable<TiIUpdatesDetailDto>>(ex.Message);
            }
        }
    }
}
