﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ArmContext.Dtos
{
    /// <summary>
    /// Database release configuration DTO
    /// </summary>
    public class DbConfigurationDto : IMapFrom<ConfigurationsCfu>
    {
        /// <summary>
        /// Release configuration identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Release configuration name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Release configuration version
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Responsible for applying / rejecting a release
        /// </summary>
        public string Initiator { get; set; }

        /// <summary>
        /// Validate state
        /// </summary>
        public bool? ValidateState { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ConfigurationsCfu, DbConfigurationDto>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                .ForMember(x => x.Name, y => y.MapFrom(z => z.ConfigurationName))
                .ForMember(x => x.Version, y => y.MapFrom(z => z.Version))
                .ForMember(x => x.Initiator, y => y.MapFrom(z => z.InitiatorId.HasValue ? $"{z.Initiator.Name} ({z.Initiator.Login})" : "-"))
                .ForMember(x => x.ValidateState, y => y.MapFrom(z => z.ValidateState));
        }
    }
}
