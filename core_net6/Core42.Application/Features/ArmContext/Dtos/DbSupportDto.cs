﻿using Clouds42.Common.Constants;

namespace Core42.Application.Features.ArmContext.Dtos
{
    /// <summary>
    /// Info on account database support action dto
    /// </summary>
    public class DbSupportDto
    {
        /// <summary>
        /// <see cref="Date"/> as string
        /// </summary>
        public string DateString => Date.ToString(Globalization.ShortDateFormat);

        /// <summary>
        /// Date of account database support action
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// TiI (plan)
        /// </summary>
        public int TehSupportPlanCount { get; set; }

        /// <summary>
        /// TiI (fact)
        /// </summary>
        public int TehSupportFactCount { get; set; }

        /// <summary>
        /// AO (plan)
        /// </summary>
        public int AutoUpdatePlanCount { get; set; }

        /// <summary>
        /// AO (fact)
        /// </summary>
        public int AutoUpdateFactCount { get; set; }

        /// <summary>
        /// TiI and AO errors count
        /// </summary>
        public int ErrorsCount { get; set; }

        /// <summary>
        /// TiI and AO connection errors count
        /// </summary>
        public int ErrorsConnectCount { get; set; }
    }
}
