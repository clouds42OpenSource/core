﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.ArmContext.Dtos
{
    /// <summary>
    /// Details of TiI and AO dto
    /// </summary>
    public class TiIUpdatesDetailDto : IMapFrom<AcDbSupportHistory>
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Account database identifier
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Account caption
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Account database name
        /// </summary>
        public string AccountDatabaseName { get; set; }

        /// <summary>
        /// Account database number
        /// </summary>
        public string AccountDatabaseNumber { get; set; }

        /// <summary>
        /// Date of account database support action
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Current configuration release
        /// </summary>
        public string CurrentRelease { get; set; }

        /// <summary>
        /// Previous configuration release
        /// </summary>
        public string PreviousRelease { get; set; }

        /// <summary>
        /// Account database support action description
        /// </summary>
        public string Description { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AcDbSupportHistory, TiIUpdatesDetailDto>()
                .ForMember(d => d.AccountId, s => s.MapFrom(h => h.AcDbSupport.AccountDatabase.Account.Id))
                .ForMember(d => d.AccountNumber, s => s.MapFrom(h => h.AcDbSupport.AccountDatabase.Account.IndexNumber))
                .ForMember(d => d.AccountDatabaseId, s => s.MapFrom(h => h.AcDbSupport.AccountDatabase.Id))
                .ForMember(d => d.AccountCaption, s => s.MapFrom(h => h.AcDbSupport.AccountDatabase.Account.AccountCaption))
                .ForMember(d => d.AccountDatabaseName, s => s.MapFrom(h => h.AcDbSupport.AccountDatabase.Caption))
                .ForMember(d => d.AccountDatabaseNumber, s => s.MapFrom(h => h.AcDbSupport.AccountDatabase.V82Name))
                .ForMember(d => d.Date, s => s.MapFrom(h => h.EditDate))
                .ForMember(d => d.CurrentRelease, s => s.MapFrom(h => h.CurrentVersionCfu))
                .ForMember(d => d.PreviousRelease, s => s.MapFrom(h => h.OldVersionCfu))
                .ForMember(d => d.Description, s => s.MapFrom(h => h.Description));
        }
    }
}
