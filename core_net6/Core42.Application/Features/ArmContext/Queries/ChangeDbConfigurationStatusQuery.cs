﻿using Clouds42.Common.ManagersResults;
using MediatR;

namespace Core42.Application.Features.ArmContext.Queries
{
    /// <summary>
    /// Query to change database configuration status
    /// </summary>
    public class ChangeDbConfigurationStatusQuery : IRequest<ManagerResult<bool>>
    {
        /// <summary>
        /// Configuration identifier
        /// </summary>
        public Guid ConfigurationId { get; set; }

        /// <summary>
        /// Approval status
        /// </summary>
        public bool ApprovalStatus { get; set; }
    }
}
