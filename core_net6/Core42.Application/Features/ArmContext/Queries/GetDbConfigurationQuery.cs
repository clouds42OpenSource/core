﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ArmContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ArmContext.Queries
{
    /// <summary>
    /// Query to get new releases of db configuration
    /// </summary>
    public class GetDbConfigurationQuery : BaseArmQuery, IRequest<ManagerResult<IEnumerable<DbConfigurationDto>>>
    {
    }
}
