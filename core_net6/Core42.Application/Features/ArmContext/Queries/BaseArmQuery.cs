﻿using Clouds42.DataContracts.BaseModel;
using Core42.Application.Features.ArmContext.Queries.Filters;

namespace Core42.Application.Features.ArmContext.Queries
{
    /// <summary>
    /// Base query to ARM's queries
    /// </summary>
    public class BaseArmQuery : IHasFilter<BaseArmFilter>
    {
        /// <summary>
        /// Filter
        /// </summary>
        public BaseArmFilter Filter { get; set; } = new();

    }
}
