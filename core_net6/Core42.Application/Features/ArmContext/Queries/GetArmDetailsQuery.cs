﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ArmContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ArmContext.Queries
{
    /// <summary>
    /// Query to get details of TiI and AO
    /// </summary>
    public class GetArmDetailsQuery : BaseArmQuery, IRequest<ManagerResult<IEnumerable<TiIUpdatesDetailDto>>>
    {
    }
}
