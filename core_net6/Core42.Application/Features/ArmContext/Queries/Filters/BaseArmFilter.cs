﻿using Clouds42.Domain.Enums._1C;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ArmContext.Queries.Filters
{
    /// <summary>
    /// Base filter for ARM query
    /// </summary>
    public class BaseArmFilter : IQueryFilter
    {
        /// <summary>
        /// Start date of period
        /// </summary>
        public DateTime StartRange { get; set; } = DateTime.Now.AddDays(-7);

        /// <summary>
        /// End date of period
        /// </summary>
        public DateTime EndRange { get; set; } = DateTime.Now;

        /// <summary>
        /// Operation with databases
        /// </summary>
        public DatabaseSupportOperation? DbOperation { get; set; }

        /// <summary>
        /// List of configurations
        /// </summary>
        public List<string> Configurations { get; set; }
    }
}
