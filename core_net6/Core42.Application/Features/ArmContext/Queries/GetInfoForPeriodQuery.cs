﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.ArmContext.Dtos;
using MediatR;

namespace Core42.Application.Features.ArmContext.Queries
{
    /// <summary>
    /// Query to get info of TiI and AO for specified period
    /// </summary>
    public class GetInfoForPeriodQuery : BaseArmQuery, IRequest<ManagerResult<List<DbSupportDto>>>
    {
    }
}
