﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using LinqExtensionsNetFramework;

namespace Core42.Application.Features.ArmContext
{
    /// <summary>
    /// ARM specification
    /// </summary>
    public static class ArmSpecification
    {
        /// <summary>
        /// Filter by period
        /// </summary>
        /// <param name="startDate">Start date of period</param>
        /// <param name="endDate">End date of period</param>
        /// <returns></returns>
        public static Spec<AcDbSupportHistory> ByPeriod(DateTime startDate, DateTime endDate) =>
            new(rec => rec.EditDate >= startDate.Date && rec.EditDate < endDate);

        /// <summary>
        /// Filter by database support operation
        /// </summary>
        /// <param name="dbOperation">Database support operation</param>
        /// <returns></returns>
        public static Spec<AcDbSupportHistory> ByDatabaseOperation(DatabaseSupportOperation? dbOperation) => dbOperation switch
        {
            DatabaseSupportOperation.AutoUpdate =>
                new(rec => rec.State == SupportHistoryState.Success && rec.Operation == DatabaseSupportOperation.AutoUpdate),
            DatabaseSupportOperation.TechSupport =>
                new(rec => rec.State == SupportHistoryState.Success && rec.Operation == DatabaseSupportOperation.TechSupport),
            DatabaseSupportOperation.PlanAutoUpdate =>
                new(rec => rec.State == SupportHistoryState.Success && rec.Operation == DatabaseSupportOperation.PlanAutoUpdate),
            DatabaseSupportOperation.PlanTehSupport =>
                new(rec => rec.State == SupportHistoryState.Success && rec.Operation == DatabaseSupportOperation.PlanTehSupport),
            DatabaseSupportOperation.AuthToDb =>
                new(rec => rec.State == SupportHistoryState.Error && rec.Operation == DatabaseSupportOperation.AuthToDb),
            DatabaseSupportOperation.AutoUpdateRequest =>
                new(rec => rec.State == SupportHistoryState.Error && rec.Operation != DatabaseSupportOperation.AuthToDb),
            _ =>
                new(rec => true)
        };

        /// <summary>
        /// Filter by database support operation
        /// </summary>
        /// <param name="dbOperation">Database support operation</param>
        /// <returns></returns>
        public static Spec<AcDbSupportHistory> ByConfigurations (List<string>? configuration)
        {
            if (configuration == null)
                return new(rec => true);

            return new(rec => configuration.Contains(rec.AcDbSupport.ConfigurationName));

        } 
    }
}
