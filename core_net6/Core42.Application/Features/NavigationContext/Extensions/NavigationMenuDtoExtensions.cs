﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Constants;
using Clouds42.Common.DataModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.NavigationContext.Dtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using static Clouds42.Configurations.Configurations.CloudConfigurationProvider;

namespace Core42.Application.Features.NavigationContext.Extensions
{
    /// <summary>
    /// Navigation menu extensions
    /// </summary>
    public static class NavigationMenuDtoExtensions
    {
        public const string DefaultIcon = "fa-th-large";

        /// <summary>
        /// Add home panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddHomePanel(this NavigationMenuDto dto)
        {
            dto.LeftSideMenu.Add(
                new NavigationSideMenuDto
                {
                    CategoryKey = string.Empty,
                    StartMenu = new NavigationMenuItemDto
                    {
                        Caption = "Главная",
                        FaIcon = "fa-home",
                        Href = "/"
                    },
                    Children = []
                });

            return dto;
        }

        /// <summary>
        /// Add admin panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <param name="groups"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddAdminPanel(this NavigationMenuDto dto, IAccessProvider accessProvider, List<AccountUserGroup> groups)
        {
           
            var list = new[]
              {
                AccountUserGroup.AccountSaleManager,
                AccountUserGroup.CloudAdmin,
                AccountUserGroup.Hotline,
                AccountUserGroup.ProcOperator,
                AccountUserGroup.CloudSE,
                AccountUserGroup.ArticleEditor
            };

            if (!groups.Any(x => list.Contains(x)))
            {
                return dto;
            }

            var childs = new List<NavigationMenuItemDto>();
            if (accessProvider.HasAccessForMultiAccountsBool(ObjectAction.ControlPanel_Main_Accounts))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/accounts",
                    FaIcon = "fa-users",
                    Caption = "Аккаунты"
                });
            }

            if (accessProvider.HasAccessForMultiAccountsBool(ObjectAction.ControlPanel_Main_AccountDatabase))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/account-databases",
                    FaIcon = "fa-database",
                    Caption = "Информационные базы"
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_ConfigurationCloud))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/tasks",
                    FaIcon = "fa-tasks",
                    Caption = "Задачи"
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_SegmentCloud))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/cloud-segments-reference",
                    Caption = "Сегменты",
                    FaIcon = DefaultIcon
                });

            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_Logging))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/logging",
                    FaIcon = "fa-pencil",
                    Caption = "Логирование"
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_SegmentCloud))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/cloud-core",
                    FaIcon = "fa-cog",
                    Caption = "Настройка облака"
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_Main_FlowProcess))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/process-flow",
                    FaIcon = "fa-cog",
                    Caption = "Рабочие процессы"
                });
            }

            if (accessProvider.HasAccessForMultiAccountsBool(ObjectAction.ControlPanel_Main_Accounts))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/partners",
                    FaIcon = "fa-handshake-o",
                    Caption = "Партнеры"
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_Main_Marketing))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/marketing",
                    FaIcon = "fa-line-chart",
                    Caption = "Маркетинг"
                });

                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/sending-notifications",
                    FaIcon = DefaultIcon,
                    Caption = "Рассылка уведомлений"
                });


            }

            if (childs.Any())
            {
                dto.LeftSideMenu.Add(new NavigationSideMenuDto
                {
                    CategoryKey = "admin_panel",
                    StartMenu = new NavigationMenuItemDto
                    {
                        Caption = "Админ панель",
                        FaIcon = DefaultIcon
                    },
                    Children = childs
                });
            }

            return dto;
        }


        /// <summary>
        /// Add catalogs panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <param name="userGroups"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddCatalogsPanel(this NavigationMenuDto dto, IAccessProvider accessProvider, List<AccountUserGroup> userGroups)
        {
            if (!userGroups.Any(x => x is AccountUserGroup.CloudAdmin or AccountUserGroup.CloudSE))
            {
                return dto;
            }

            var childs = new List<NavigationMenuItemDto>();

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_CatalogOfConfigurations))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/configurations-1c",
                    Caption = "Конфигурации",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_IndustryReference))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/industry",
                    Caption = "Отрасли",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_DbTemplateReference))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/db-templates",
                    Caption = "Шаблоны",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_DbTemplateDelimiters))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/db-template-delimiters",
                    Caption = "Базы на разделителях",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_SupplierReference))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/supplier-reference",
                    Caption = "Поставщики",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_PrintedHtmlFormReference))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/printed-html-form-reference",
                    Caption = "Печатные формы HTML",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_ServiceAccounts))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/service-accounts",
                    Caption = "Служебные аккаунты",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.AgencyAgreement_GetAgencyAgreementsData))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/agency-agreement",
                    Caption = "Агентские соглашения",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.DelimiterSourceAccountDatabase_GetData))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/dictionary/delimiter-source-databases",
                    Caption = "Материнские базы",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_CloudServices))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/dictionary/cloud-services",
                    Caption = "Службы Clouds 42",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.GetItsDataAuthorizations))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/its-authorization-data",
                    Caption = "Доступ к ИТС",
                    FaIcon = DefaultIcon
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.Locales_Edit))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/locales",
                    FaIcon = "fa-globe",
                    Caption = "Локали"
                });
            }

            if (childs.Any())
            {
                dto.LeftSideMenu.Add(new NavigationSideMenuDto
                {
                    CategoryKey = "dictionaries",
                    Children = childs,
                    StartMenu = new NavigationMenuItemDto
                    {
                        Caption = "Справочники",
                        FaIcon = "fa-book"
                    }
                });
            }

            return dto;
        }


        /// <summary>
        /// Add support panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddSupportPanel(this NavigationMenuDto dto, IAccessProvider accessProvider)
        {
            var childs = new List<NavigationMenuItemDto>();

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_ARM))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/arm-manager",
                    Caption = "АРМ",
                    FaIcon = DefaultIcon
                });

                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/administration/auto-update-account-db",
                    Caption = "АРМ Автообновления",
                    FaIcon = DefaultIcon
                });
            }

            if (childs.Any())
            {
                dto.LeftSideMenu.Add(new NavigationSideMenuDto
                {
                    CategoryKey = "support",
                    Children = childs,
                    StartMenu = new NavigationMenuItemDto
                    {
                        Caption = "ТиИ/АО",
                        FaIcon = "fa-book"
                    }
                });

            }

            return dto;
        }

        /// <summary>
        /// Add rent 1c panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="locale"></param>
        /// <param name="configuration"></param>
        /// <param name="cloudLocalizer"></param>
        /// <returns></returns>
        public static async Task<NavigationMenuDto> AddRentPanel(
            this NavigationMenuDto dto, 
            IAccessProvider accessProvider,
            IUnitOfWork unitOfWork,
            Locale locale,
            IConfiguration configuration,
            ICloudLocalizer cloudLocalizer
            )
        {
            var rent1CName = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C, accessProvider.ContextAccountId);
            var childs = new List<NavigationMenuItemDto>
            {
                new()
                {
                    Href = "/rent",
                    Caption = rent1CName,
                    FaIcon = "fa-custom-rent"
                }
            };

            if (locale.Name == LocaleConst.Russia)
            {
                var resConfigExists = await unitOfWork.ResourceConfigurationRepository
                    .AsQueryableNoTracking()
                    .AnyAsync(r => r.AccountId == accessProvider.ContextAccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);

                if (resConfigExists)
                {
                    childs.Add(new NavigationMenuItemDto
                    {
                        Href = "/additional-sessions",
                        Caption = "Дополнительные сеансы",
                        FaIcon = "fa-custom-additional-sessions"
                    });
                }
            }
            
            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ServiceMenu_MyDisk,
                    () => accessProvider.ContextAccountId))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/my-disk",
                    FaIcon = "fa-hdd-o",
                    Caption = "Мой диск"
                });
            }
            
            if (childs.Any())
            {
                dto.LeftSideMenu.Add(new NavigationSideMenuDto
                {
                    CategoryKey = "rent_1c",
                    Children = childs,
                    StartMenu = new NavigationMenuItemDto
                    {
                        Caption = "Аренда 1С",
                        FaIcon = "fa-sliders"
                    },
                });
            }
            return dto;
        }
        public static NavigationMenuDto AddInfoDatabasesPanel(this NavigationMenuDto dto,
            IAccessProvider accessProvider)
        {
            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_AccountDatabase,
                    () => accessProvider.ContextAccountId))
            {
                dto.LeftSideMenu.Add(new NavigationSideMenuDto
                {
                    CategoryKey = string.Empty,
                    Children = [],
                    StartMenu = new NavigationMenuItemDto
                    {
                        Href = "/my-databases",
                        FaIcon = "fa-database",
                        Caption = "Информационные базы"
                    },
                });
            }
            return dto;
        }
            
        

        /// <summary>
        /// Add management panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddManagementPanel(this NavigationMenuDto dto, IAccessProvider accessProvider)
        {
            var childs = new List<NavigationMenuItemDto>();
            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_AccountDatas,
                    () => accessProvider.ContextAccountId))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/my-company",
                    FaIcon = "fa-info-circle",
                    Caption = "Данные о компании"
                });
            }
            
            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_BillingAccounts,
                () => accessProvider.ContextAccountId))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/billing",
                    FaIcon = "fa-credit-card",
                    Caption = "Баланс"
                });
            }
            
            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_AccountUsers,
                    () => accessProvider.ContextAccountId))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/users",
                    FaIcon = "fa-users",
                    Caption = "Пользователи"
                });
            }
                 
            childs.Add(new NavigationMenuItemDto
            {
                Caption = "Профиль",
                FaIcon = "fa-user",
                Href = "/profile"
            });

            if (childs.Any())
            {
                dto.LeftSideMenu.Add(new NavigationSideMenuDto
                {
                    CategoryKey = "account_management",
                    Children = childs,
                    StartMenu = new NavigationMenuItemDto
                    {
                        Caption = "Управление аккаунтом",
                        FaIcon = "fa-sliders"
                    },
                });
            }

            return dto;
        }


        /// <summary>
        /// Add corp cloud panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddCorpCloudPanel(this NavigationMenuDto dto,
            IAccessProvider accessProvider)
        {

            var childs = new List<NavigationMenuItemDto>();

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_AccountDatas,
                () => accessProvider.ContextAccountId))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/ultra/servers",
                    FaIcon = "fa-database",
                    Caption = "Мониторинг серверов"
                });
            }


            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_AccountDatas,
                () => accessProvider.ContextAccountId))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/ultra/performance",
                    FaIcon = "fa-tasks",
                    Caption = "Производительность 1С"
                });
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ManagmentMenu_AccountDatas,
                    () => accessProvider.ContextAccountId))
            {
                childs.Add(new NavigationMenuItemDto
                {
                    Href = "/ultra/devops",
                    FaIcon = DefaultIcon,
                    Caption = "DevOps для 1С"
                });
            }

            if (childs.Any())
            {
                dto.LeftSideMenu.Add(new NavigationSideMenuDto
                {
                    CategoryKey = "ultra",
                    Children = childs,
                    StartMenu = new NavigationMenuItemDto
                    {
                        Caption = "Корпоративное облако",
                        FaIcon = "fa-line-chart",
                    }
                });
            }

            return dto;
        }


        /// <summary>
        /// Add services panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="configuration"></param>
        /// <param name="userLocale"></param>
        /// <param name="isServiceAccount"></param>
        /// <param name="myDatabasesServiceDataProvider"></param>
        /// <param name="userGroups"></param>
        /// <returns></returns>
        public static async Task<NavigationMenuDto> AddServicesPanel(this NavigationMenuDto dto,
            IAccessProvider accessProvider,
            IUnitOfWork unitOfWork,
            IConfiguration configuration,
            List<AccountUserGroup> userGroups,
            Locale userLocale,
            bool isServiceAccount,
            IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider)
        {
            var list = new[]
            {
                AccountUserGroup.AccountAdmin,
                AccountUserGroup.AccountSaleManager,
                AccountUserGroup.CloudAdmin,
                AccountUserGroup.Hotline,
                AccountUserGroup.ProcOperator
            };

            var isVisibleServices = !isServiceAccount;

            if (!userGroups.Any(x => list.Contains(x)) || !isVisibleServices)
            {
                return dto;
            }

            var childs = new List<NavigationMenuItemDto>();
            
            if (userLocale.Name is LocaleConst.Russia or LocaleConst.Kazakhstan)
            {
                if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ServiceMenu_CustomService,
                        () => accessProvider.ContextAccountId))
                {
                    childs.Add(new NavigationMenuItemDto
                    {
                        Href = "/m42",
                        FaIcon = "fa-custom-market",
                        Caption = "Маркет42"
                    });
                }
            }

            if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ServiceMenu_AccountDatabase, () => accessProvider.ContextAccountId))
            {
                var activeServiceTypes = await
                    myDatabasesServiceDataProvider.GetAccountActivePaidServiceTypesIdNamePairs(accessProvider.ContextAccountId, userLocale.Name);

                activeServiceTypes.ForEach(serviceType =>
                {
                    childs.Add(new NavigationMenuItemDto
                    {
                        Href = $"/my-database-services/{serviceType.Key}",
                        Caption = serviceType.Value,
                        FaIcon = DefaultIcon
                    });

                });
                
            }
            
            
            if (userLocale.Name == LocaleConst.Russia)
            {
                if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ServiceMenu_CustomService, () => accessProvider.ContextAccountId))
                {
                    var servicesEnum = new List<InternalCloudServiceEnum> { 
                        InternalCloudServiceEnum.Sauri, 
                        InternalCloudServiceEnum.Delans, 
                        InternalCloudServiceEnum.Tardis, 
                        InternalCloudServiceEnum.Mcob, 
                        InternalCloudServiceEnum.AlpacaMeet,
                        InternalCloudServiceEnum.Neuro42
                    };

                    var restrictedServices = new List<InternalCloudServiceEnum> {
                        InternalCloudServiceEnum.Tardis,
                        InternalCloudServiceEnum.Delans
                    };

                    var services = await unitOfWork.BillingServiceRepository
                        .AsQueryableNoTracking()
                        .GroupJoin(
                            unitOfWork.ResourceConfigurationRepository
                                .AsQueryable()
                                .Where(x => x.AccountId == accessProvider.ContextAccountId), 
                            z => z.Id, 
                            x => x.BillingServiceId, 
                            (service, configurations) => new { service, configurations }
                        )
                        .Where(x => 
                            x.service.InternalCloudService.HasValue && 
                            servicesEnum.Contains(x.service.InternalCloudService.Value) && 
                            x.service.IsActive &&
                            (
                                !restrictedServices.Contains(x.service.InternalCloudService.Value) || 
                                (
                                    (x.service.InternalCloudService == InternalCloudServiceEnum.Tardis ||
                                     x.service.InternalCloudService == InternalCloudServiceEnum.Delans) && 
                                    x.configurations.Any(c => c.Cost > 0 && (c.Frozen == null || !c.Frozen.Value))
                                )
                            )
                        )
                        .Select(x => new { 
                            x.service.Id, 
                            x.service.Name, 
                            x.service.InternalCloudService, 
                            x.configurations 
                        })
                        .ToListAsync();

                    var alpacaService = services.FirstOrDefault(x => x.InternalCloudService == InternalCloudServiceEnum.AlpacaMeet);
                    var aiService = services.FirstOrDefault(x => x.InternalCloudService == InternalCloudServiceEnum.Neuro42);
                    services.Where(x => x.InternalCloudService is not (InternalCloudServiceEnum.AlpacaMeet and InternalCloudServiceEnum.Neuro42)).ToList().ForEach(x =>
                    {
                        childs.Add(new NavigationMenuItemDto
                        {
                            Caption = x.Name,
                            FaIcon = x.InternalCloudService switch
                            {
                                InternalCloudServiceEnum.Delans => "fa-custom-delans",
                                InternalCloudServiceEnum.Sauri => "fa-custom-sauri",
                                _ => DefaultIcon
                            },
                            Href = $"/billing-service/{x.Id}"
                        });
                    });
                    
                    if (accessProvider.HasAccessBool(ObjectAction.ControlPanel_ServiceMenu_Esdl,
                            () => accessProvider.ContextAccountId) && userLocale.Name == LocaleConst.Russia)
                    {
                        childs.Add(new NavigationMenuItemDto
                        {
                            Href = "/esdl",
                            FaIcon = "fa-upload",
                            Caption = $"{NomenclaturesNameConstants.Esdl}"
                        });
                    }
                    
                    if (alpacaService != null)
                    {
                        childs.Add(new NavigationMenuItemDto
                        {
                            Caption = alpacaService.Name,
                            FaIcon = "fa-custom-alpaca",
                            Href = "/alpaca-meet"
                        });
                    }
                    if (aiService != null)
                    {
                        childs.Add(new NavigationMenuItemDto
                        {
                            Caption = aiService.Name,
                            FaIcon = DefaultIcon,
                            Href = "/neuro42"
                        });
                    }

                    childs.Add(new NavigationMenuItemDto
                    {
                        Href = "/my-services",
                        FaIcon = "fa-custom-extensions-and-processing",
                        Caption = "Расширения и обработки"
                    });
                }

            }

            if (childs.Any())
            {
                dto.LeftSideMenu.Add(new NavigationSideMenuDto
                {
                    CategoryKey = "services",
                    StartMenu = new NavigationMenuItemDto
                    {
                        Caption = "Используемые сервисы",
                        FaIcon = "fa-tasks"
                    },
                    Children = childs
                });
            }

            return dto;
        }


        /// <summary>
        /// Add partners panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <param name="userGroups"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddPartnersPanel(this NavigationMenuDto dto, 
            IAccessProvider accessProvider, 
            List<AccountUserGroup> userGroups,
            Locale locale)
        {
            if (!userGroups.Any())
            {
                return dto;
            }
            if (!accessProvider.HasAccessBool(ObjectAction.Partners_GetInfo) ||
                locale.Name != LocaleConst.Russia)
            {
                return dto;
            }

            dto.LeftSideMenu.Add(new NavigationSideMenuDto
            {
                CategoryKey = string.Empty,
                Children = [],
                StartMenu = new NavigationMenuItemDto
                {
                    Caption = "Партнерам и разработчикам",
                    FaIcon = "fa-handshake-o",
                    Href = "/partners"
                }
            });

            return dto;
        }


        /// <summary>
        /// Add articles panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <param name="userGroups"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddArticlesPanel(this NavigationMenuDto dto, 
            IAccessProvider accessProvider, 
            List<AccountUserGroup> userGroups,
            Locale locale)
        {
            if (!userGroups.Any())
            {
                return dto;
            }
            if (!accessProvider.HasAccessBool(ObjectAction.Partners_GetInfo) ||
                locale.Name != LocaleConst.Russia)
            {
                return dto;
            }
            
            dto.LeftSideMenu.Add(new NavigationSideMenuDto
            {
                CategoryKey = string.Empty,
                Children = [],
                StartMenu = new NavigationMenuItemDto
                {
                    Caption = "Статьи",
                    FaIcon = "fa-custom-articles",
                    Href = "/articles"
                }
            });

            return dto;
        }

        /// <summary>
        /// Add settings panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddSettingsPanel(this NavigationMenuDto dto,
            Locale locale)
        {

            dto.LeftSideMenu.Add(new NavigationSideMenuDto
            {
                CategoryKey = "settings",
                Children =
                [
                    new NavigationMenuItemDto { Href = "/connection-settings", FaIcon = "fa-wrench", Caption = "Настройки подключения" },

                    new NavigationMenuItemDto
                    {
                        Caption = "Инструкции по использованию сервисов",
                        FaIcon = "fa-question",
                        Href =
                            $"{GetPromoSiteUrl(locale)}{(locale.Name != LocaleConst.Ukraine ? $"/{locale.Name}" : "")}/manuals"
                    },

                    new NavigationMenuItemDto
                    {
                        Caption = "Нужна консультация по 1С",
                        FaIcon = "fa-custom-consultant",
                        Href = $"/consultations-1C"
                    },

                    new NavigationMenuItemDto { Caption = "1С ЭДО", FaIcon = "fa-custom-edo", Href = "/edo" },

                    new NavigationMenuItemDto { Caption = "1С Отчетность", FaIcon = DefaultIcon, Href = "/reporting" },

                    new NavigationMenuItemDto { Caption = "Регистрационный номер", FaIcon = DefaultIcon, Href = "/registration-number" },

                    new NavigationMenuItemDto
                    {
                        Caption = "Акт сверки взаиморасчетов",
                        FaIcon = DefaultIcon,
                        Href = "/reconciliation-statement"
                    }

                ],
                StartMenu = new NavigationMenuItemDto
                {
                    Caption = "Настройки",
                    FaIcon = "fa-cogs"
                }
            });

            return dto;
        }


        /// <summary>
        /// Add sign out panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddSignOutPanel(this NavigationMenuDto dto)
        {
            dto.LeftSideMenu.Add(new NavigationSideMenuDto
            {
                CategoryKey = string.Empty,
                Children = [],
                StartMenu = new NavigationMenuItemDto
                {
                    Caption = "ВЫХОД",
                    FaIcon = "fa-sign-out",
                    Href = "/signin"
                }
            });

            return dto;
        }

        /// <summary>
        /// Add balance panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="accessProvider"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="locale"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddBalancePanel(this NavigationMenuDto dto, 
            IAccessProvider accessProvider, 
            IUnitOfWork unitOfWork, 
            Locale locale,
            IConfiguration configuration)
        {
            if (!accessProvider.HasAccessBool(ObjectAction.Balance_View,
              () => accessProvider.ContextAccountId))
            {
                return dto;
            }
            var balance = decimal.Round(unitOfWork.BillingAccountRepository.GetBalance(accessProvider.Name), 2);

            dto.TopSideMenu.BalanceMenu = new NavigationSideMenuDto 
            { 
                CategoryKey = string.Empty,
                Children = [],
                StartMenu = new NavigationMenuItemDto 
                { 
                    Caption = $"Баланс {balance} {locale.Currency}.",
                    Href = "/billing",
                    FaIcon = "fa-"
                }
            };

            return dto;
        }


        /// <summary>
        /// Add greetings panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddGreetingsPanel(this NavigationMenuDto dto,
            IUserPrincipalDto user)
        {
            var caption = string.IsNullOrEmpty(user?.FirstName)
                ? "Здравствуйте!"
                : $"Здравствуйте, {user.FirstName}!";

            dto.TopSideMenu.GreetingsMenu = new NavigationSideMenuDto
            {
                CategoryKey = string.Empty,
                Children = [],
                StartMenu = new NavigationMenuItemDto
                {
                    Caption = caption,
                    Href = "/profile",
                    FaIcon = "fa-"
                }
            };

            return dto;
        }

        /// <summary>
        /// Add sign out top side panel menu
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static NavigationMenuDto AddSignOutTopSidePanel(this NavigationMenuDto dto)
        {
            dto.TopSideMenu.ExitMenu = new NavigationSideMenuDto 
            {
                CategoryKey = string.Empty,
                Children = [], 
                StartMenu = new NavigationMenuItemDto 
                {
                    Caption = "Выйти",
                    Href = $"/signin",
                    FaIcon = "fa-sign-out"
                } 
            };

            return dto;
        }

        /// <summary>
        /// Получить адрес промо сайта
        /// </summary>
        /// <returns>Адрес промо сайта</returns>
        private static string GetPromoSiteUrl(Locale locale)
            => locale.Name == LocaleConst.Ukraine
                ? PromoSite.GetUaPromoSiteUrl()
                : PromoSite.GetRuPromoSiteUrl();
    }
}
