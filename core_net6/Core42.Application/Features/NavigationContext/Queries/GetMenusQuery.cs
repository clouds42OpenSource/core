﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.NavigationContext.Dtos;
using MediatR;

namespace Core42.Application.Features.NavigationContext.Queries
{
    /// <summary>
    /// Get menus query
    /// </summary>
    public class GetMenusQuery : IRequest<ManagerResult<NavigationMenuDto>>
    {
    }
}
