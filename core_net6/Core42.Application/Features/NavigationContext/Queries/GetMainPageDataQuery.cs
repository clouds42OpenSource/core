﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Features.NavigationContext.Dtos;
using MediatR;

namespace Core42.Application.Features.NavigationContext.Queries
{
    public class GetMainPageDataQuery : IRequest<ManagerResult<MainPageDto>>
    {
    }
}
