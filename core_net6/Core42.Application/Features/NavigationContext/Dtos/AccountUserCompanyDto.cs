﻿namespace Core42.Application.Features.NavigationContext.Dtos;

public class AccountUserCompanyDto
{

    /// <summary>
    /// Название аккаунта
    /// </summary>
    public string AccountCaption { get; set; }

    /// <summary>
    /// ИНН аккаунта
    /// </summary>
    public string AccountInn { get; set; }

    /// <summary>
    /// Название локали аккаунта
    /// </summary>
    public string AccountLocaleName { get; set; }

    /// <summary>
    /// Данные по администраторам аккаунта
    /// </summary>
    public List<AccountAdministratorDto>? AccountAdministrators { get; set; }
}
