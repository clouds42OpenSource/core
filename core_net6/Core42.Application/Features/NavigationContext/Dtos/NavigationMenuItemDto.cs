﻿namespace Core42.Application.Features.NavigationContext.Dtos
{
    public class NavigationMenuItemDto
    {
        /// <summary>
        /// Menu icon
        /// </summary>
        public string? FaIcon { get; set; }

        /// <summary>
        /// Menu caption
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Menu href
        /// </summary>
        public string Href { get; set; }
    }
        
}
