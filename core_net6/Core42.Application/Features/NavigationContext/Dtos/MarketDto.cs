﻿namespace Core42.Application.Features.NavigationContext.Dtos;

public class MarketDto
{
    /// <summary>
    /// Урл магазина приложений
    /// с указанием источника
    /// </summary>
    public string MarketUrlWithSource { get; set; }

    /// <summary>
    /// Уведомление для главной страницы
    /// </summary>
    public string? MainPageNotification { get; set; }

    /// <summary>
    /// Путь до баннера
    /// </summary>
    public string? BannerUrl { get; set; }
}
