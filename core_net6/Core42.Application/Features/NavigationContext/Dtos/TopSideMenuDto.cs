﻿namespace Core42.Application.Features.NavigationContext.Dtos
{
    /// <summary>
    /// Top side menu dto
    /// </summary>
    public class TopSideMenuDto
    {
        /// <summary>
        /// Balance menu
        /// </summary>
        public NavigationSideMenuDto BalanceMenu { get; set; }

        /// <summary>
        /// Greetings menu
        /// </summary>
        public NavigationSideMenuDto GreetingsMenu { get; set; }

        /// <summary>
        /// Exit menu
        /// </summary>
        public NavigationSideMenuDto ExitMenu { get; set; }
    }
        
}
