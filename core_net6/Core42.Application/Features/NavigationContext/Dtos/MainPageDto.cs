﻿namespace Core42.Application.Features.NavigationContext.Dtos;

public class MainPageDto
{
    /// <summary>
    /// Id аккаунта пользователя
    /// </summary>
    public Guid AccountId { get; set; }

    /// <summary>
    /// Id пользователя
    /// </summary>
    public Guid AccountUserId { get; set; }

    /// <summary>
    /// Имя пользователя
    /// </summary>
    public string FirstName { get; set; }

    /// <summary>
    /// Логин пользователя
    /// </summary>
    public string Login { get; set; }

    /// <summary>
    /// Электронная почта пользователя
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    /// Дополнительная информация для пользователя аккаунта
    /// </summary>
    public MarketDto Market { get; set; }

    /// <summary>
    /// Данные о компании пользователя аккаунта
    /// </summary>
    public AccountUserCompanyDto AccountUser { get; set; }
}
