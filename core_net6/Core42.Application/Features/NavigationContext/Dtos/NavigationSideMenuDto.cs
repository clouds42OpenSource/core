﻿namespace Core42.Application.Features.NavigationContext.Dtos
{
    /// <summary>
    /// Navigation side menu dto
    /// </summary>
    public class NavigationSideMenuDto
    {
        /// <summary>
        /// Start menu
        /// </summary>
        public NavigationMenuItemDto StartMenu { get; set; }

        /// <summary>
        /// Category key
        /// </summary>
        public string CategoryKey { get; set; }

        /// <summary>
        /// Childs
        /// </summary>
        public List<NavigationMenuItemDto> Children { get; set; } = [];
    }
        
}
