﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Features.NavigationContext.Dtos;

public class AccountAdministratorDto : IMapFrom<AccountUser>
{
    /// <summary>
    /// Имя пользователя(администратора)
    /// </summary>
    public string FirstName { get; set; }

    /// <summary>
    /// Фамилия пользователя(администратора)
    /// </summary>
    public string LastName { get; set; }

    /// <summary>
    /// Отчество пользователя(администратора)
    /// </summary>
    public string MiddleName { get; set; }

    /// <summary>
    /// Электронная почта пользователя(администратора)
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    /// Номер телефона пользователя(администратора)
    /// </summary>
    public string PhoneNumber { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<AccountUser, AccountAdministratorDto>();
    }
}
