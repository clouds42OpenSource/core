﻿namespace Core42.Application.Features.NavigationContext.Dtos
{
    /// <summary>
    /// Navigation menu dto
    /// </summary>
    public class NavigationMenuDto
    {
        /// <summary>
        /// Left side menus dtos
        /// </summary>
        public List<NavigationSideMenuDto> LeftSideMenu { get; set; } = [];

        /// <summary>
        /// Top side menu dto
        /// </summary>
        public TopSideMenuDto TopSideMenu { get; set;} = new();
    }
        
}
