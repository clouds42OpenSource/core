﻿using Clouds42.Accounts.Contracts.ServiceAccounts.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.ManagersResults;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.NavigationContext.Dtos;
using Core42.Application.Features.NavigationContext.Extensions;
using Core42.Application.Features.NavigationContext.Queries;
using MediatR;
using Microsoft.Extensions.Configuration;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NavigationContext.Handlers
{
    /// <summary>
    /// Get menus query handler
    /// </summary>
    public class GetMenusQueryHandler(
        IAccessProvider accessProvider,
        IUnitOfWork unitOfWork,
        IConfiguration configuration,
        ICloudLocalizer cloudLocalizer,
        IMyDatabasesServiceDataProvider dataProvider,
        IServiceAccountDataHelper serviceAccountDataHelper)
        : IRequestHandler<GetMenusQuery, ManagerResult<NavigationMenuDto>>
    {
        /// <summary>
        /// Handle get menus query
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ManagerResult<NavigationMenuDto>> Handle(GetMenusQuery request, CancellationToken cancellationToken)
        {
            var contextAccountId = accessProvider.ContextAccountId;

            var user = await accessProvider.GetUserAsync();
            var roles = user?.Groups ?? [];
            var locale = await unitOfWork.AccountsRepository.GetLocaleAsync(contextAccountId,  Guid.Parse(configuration["DefaultLocale"]!));
            var isServiceAccount = await serviceAccountDataHelper.IsServiceAccountAsync(contextAccountId, cancellationToken);

            var navigationMenu = new NavigationMenuDto();

            navigationMenu = await navigationMenu
                .AddHomePanel()
                .AddAdminPanel(accessProvider, roles)
                .AddCatalogsPanel(accessProvider, roles)
                .AddSupportPanel(accessProvider)
                .AddInfoDatabasesPanel(accessProvider)
                .AddRentPanel(accessProvider, unitOfWork, locale, configuration, cloudLocalizer);

            navigationMenu = await navigationMenu
                .AddManagementPanel(accessProvider)
                .AddCorpCloudPanel(accessProvider)
                .AddServicesPanel(accessProvider, unitOfWork, configuration, roles, locale, isServiceAccount, dataProvider);

            navigationMenu = navigationMenu
                .AddPartnersPanel(accessProvider, roles, locale)
                .AddSettingsPanel(locale)
                .AddArticlesPanel(accessProvider, roles, locale)
                .AddSignOutPanel()
                .AddBalancePanel(accessProvider, unitOfWork, locale, configuration)
                .AddGreetingsPanel(user)
                .AddSignOutTopSidePanel();

            return navigationMenu.ToOkManagerResult();
        }
    }
}
