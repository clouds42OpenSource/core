﻿using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.NavigationContext.Dtos;
using Core42.Application.Features.NavigationContext.Queries;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Features.NavigationContext.Handlers;

public class GetMainPageDataQueryHandler(
    IUnitOfWork unitOfWork,
    ILogger42 logger,
    IAccessProvider accessProvider,
    IMapper mapper)
    : IRequestHandler<GetMainPageDataQuery, ManagerResult<MainPageDto>>
{
    public async Task<ManagerResult<MainPageDto>> Handle(GetMainPageDataQuery request,
        CancellationToken cancellationToken)
    {
        try
        {
            var user = await accessProvider.GetUserAsync();

            var marketSourceUrl = CloudConfigurationProvider.Market.GetUrlWithSource();

            var cloudCore =
                await unitOfWork.CloudCoreRepository
                    .AsQueryable()
                    .AsNoTracking()
                    .FirstOrDefaultAsync(cancellationToken);

            var currentUser = (await unitOfWork.AccountUsersRepository
                .AsQueryable()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == user.Id, cancellationToken)) ?? throw new NotFoundException(
                $"Пользователь по ID {user.Id} не найден");

            var account = (await unitOfWork.AccountConfigurationRepository
                              .AsQueryable()
                              .AsNoTracking()
                              .Include(x => x.Locale)
                              .FirstOrDefaultAsync(x => x.AccountId == user.ContextAccountId,
                                  cancellationToken)) ??
                          throw new NotFoundException(
                              $"Не удалось получить детальную информацию о пользователе аккаунта '{user.Id}'");

            var requisites = await unitOfWork
                .GetGenericRepository<AccountRequisites>()
                .AsQueryable()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.AccountId == account.AccountId, cancellationToken);

            var accountAdmins = await unitOfWork.AccountUsersRepository
                .AsQueryable()
                .AsNoTracking()
                .Where(x => x.AccountUserRoles.Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin) &&
                            x.AccountId == account.AccountId)
                .ProjectTo<AccountAdministratorDto>(mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);


            return new MainPageDto
            {
                AccountId = account.Account.Id,
                Email = currentUser.Email,
                FirstName = currentUser.FirstName,
                Login = currentUser.Login,
                AccountUserId = currentUser.Id,
                AccountUser = new AccountUserCompanyDto
                {
                    AccountCaption = account.Account.AccountCaption,
                    AccountInn = requisites.Inn,
                    AccountLocaleName = account.Locale.Name,
                    AccountAdministrators = accountAdmins
                },
                Market = new MarketDto
                {
                    MarketUrlWithSource = marketSourceUrl,
                    MainPageNotification = cloudCore?.MainPageNotification,
                    BannerUrl = cloudCore?.BannerUrl
                }
            }.ToOkManagerResult();
        }

        catch (Exception ex)
        {
            logger.Error(ex, "Ошибка получения главной страницы");

            return PagedListExtensions.ToPreconditionFailedManagerResult<MainPageDto>(ex.Message);
        }
    }
}
