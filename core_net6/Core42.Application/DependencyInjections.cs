﻿using System.Reflection;
using Core42.Application.Features.FastaContext.Helpers;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Core42.Application
{
    /// <summary>
    /// DI extension class
    /// </summary>
    public static class DependencyInjections
    {
        /// <summary>
        /// Registry core42 application assembly
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddCore42Application(this IServiceCollection services)
        {
            var assembly = Assembly.GetExecutingAssembly();

            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(assembly));
            services.AddAutoMapper(assembly);
            services.AddFluentValidation(assembly);

            services.AddTransient<FastaPagesHelper>();
            
  	        return services;
        }


        /// <summary>
        /// Add fluent validation
        /// </summary>
        /// <param name="services"></param>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static IServiceCollection AddFluentValidation(this IServiceCollection services, Assembly assembly)
        {
            services
                .AddFluentValidationAutoValidation()
                .AddFluentValidationClientsideAdapters()
                .AddValidatorsFromAssembly(assembly);

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));

            ValidatorOptions.Global.DefaultRuleLevelCascadeMode = CascadeMode.Stop;

            return services;
        }
    }
}
