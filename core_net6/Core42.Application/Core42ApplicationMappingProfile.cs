﻿using AutoMapper;
using MappingNetStandart.Mappings;

namespace Core42.Application
{
    /// <summary>
    /// Core42 Application mappings profiles
    /// </summary>
    public class Core42ApplicationMappingProfile : Profile, IMappingProfile
    {
        /// <summary>
        /// Constuctor, recursive search mappings in current assembly and registry
        /// </summary>
        public Core42ApplicationMappingProfile()
        {
            var self = this as IMappingProfile;
            self.ApplyMappingsFromAssembly(typeof(Core42ApplicationMappingProfile).Assembly);
        }
    }
}
