﻿using System;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.ResourcesContext.Commands;
using MediatR;

namespace Clouds42.CoreWorker.BillingServiceType.RecalculateServiceTypeCostAfterChange
{
    /// <summary>
    /// Задача по пересчету стоимости услуги сервиса для всех аккаунтов
    /// после изменения(Пересчет стоимости ресурсов услуги и стоимости сервиса)
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RecalculateServiceTypeCostAfterChangeJob)]
    public class RecalculateServiceTypeCostAfterChangeJob(IUnitOfWork dbLayer, ISender sender)
        : CoreWorkerParamsJob<RecalculateServiceTypeCostAfterChangeJobParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(RecalculateServiceTypeCostAfterChangeJobParamsDto jobParams, Guid taskId,
            Guid taskQueueId) => sender.Send(new RecalculateResourcesCostForAccountsCommand { ServiceTypeId = jobParams.ServiceTypeId, Cost = jobParams.Cost, ServiceType = jobParams.ServiceType, ServiceId = jobParams.ServiceId}).Wait();
    }
}
