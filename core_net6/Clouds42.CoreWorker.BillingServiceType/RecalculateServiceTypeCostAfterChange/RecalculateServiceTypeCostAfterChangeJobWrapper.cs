﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.BillingServiceType.RecalculateServiceTypeCostAfterChange
{
    /// <summary>
    /// Обработчик задачи по пересчету стоимости услуги сервиса для всех аккаунтов
    /// после изменения(Пересчет стоимости ресурсов услуги и стоимости сервиса)
    /// </summary>
    public class RecalculateServiceTypeCostAfterChangeJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<
            RecalculateServiceTypeCostAfterChangeJob, RecalculateServiceTypeCostAfterChangeJobParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Прослойка выполнения</returns>
        public override
            TypingParamsJobWrapperBase<RecalculateServiceTypeCostAfterChangeJob,
                RecalculateServiceTypeCostAfterChangeJobParamsDto> Start(
                RecalculateServiceTypeCostAfterChangeJobParamsDto paramsJob)
        {
            StartTask(paramsJob, GetTaskComment(paramsJob));

            return this;
        }

        /// <summary>
        /// Получить комментарий для задачи
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Комментарий для задачи</returns>
        private string GetTaskComment(RecalculateServiceTypeCostAfterChangeJobParamsDto paramsJob) =>
            $"Пересчет стоимости услуги '{paramsJob.ServiceTypeId}' сервиса для всех аккаунтов после изменения";
    }
}
