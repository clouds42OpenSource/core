import 'vite/client';

declare module '*.module.css' {
    const value: {[key: string]: string};

    export default value;
}

declare global {
    type ImportMetaEnv = {
        NODE_ENV: 'development' | 'production' | 'test';
        PUBLIC_URL: string;
        REACT_APP_CP_HOST: string;
        REACT_APP_API_HOST: string;
        REACT_APP_MS_HOST: string;
        REACT_APP_CAPTCHA_TOKEN: string;
        REACT_APP_GA_TRACKING_ID: string;
        REACT_APP_SENTRY_DSN: string;
        REACT_APP_MAIN_MS_HOST: string;
        REACT_APP_UPLOAD_HOST: string;
        REACT_APP_MS_PROVIDER: string;
        DEV: boolean;
        PROD: boolean;
        SSR: boolean;
        MODE: 'local' | 'development' | 'beta' | 'production';
        REACT_ALPACA_MEET_ID: string;
        REACT_NEURO_SERVICE_ID: string;
        REACT_WP_PROMO_URL: string;
        REACT_MANAGER_EMAIL: string;
        REACT_SEND_GRID_TEMPLATE_ID: string;
    };

    type ImportMeta = {
        readonly env: ImportMetaEnv;
    };
}