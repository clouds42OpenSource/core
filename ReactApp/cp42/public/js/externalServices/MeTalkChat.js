(function (d, w, m) {
  window.supportAPIMethod = m;
  var s = d.createElement('script');
  s.type = 'text/javascript';
  s.id = 'supportScript';
  s.charset = 'utf-8';
  s.async = true;
  var id = '79545b749a2d2d92cdc0864ab239e67c';
  s.src = '//me-talk.ru/support/support.js?h=' + id;
  var sc = d.getElementsByTagName('script')[0];
  w[m] =
    w[m] ||
    function () {
      (w[m].q = w[m].q || []).push(arguments);
    };
  if (sc) sc.parentNode.insertBefore(s, sc);
  else d.documentElement.firstChild.appendChild(s);
})(document, window, 'MeTalk');
