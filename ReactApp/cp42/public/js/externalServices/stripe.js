const INITIAL_DATA = { data: { IsLogin: false } };

const extractLanguageCode = url => {
    const regex = /\/([a-z]{2}-[a-z]{2})\//i;
    const match = url.match(regex);

    return match ? match[1] : null;
};

const deleteCookie = (name, domain) => {
    const domainPart = domain ? `; Domain=${ domain }` : '';
    document.cookie = `${ name }=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/${ domainPart }`;
};

const setCookie = (name, value, seconds) => {
    let expires = '';
    if (seconds) {
        const date = new Date();
        date.setTime(date.getTime() + (seconds * 1000));
        expires = `; expires = ${ date.toUTCString() };`;
    }
    const domain = '42clouds.com';
    document.cookie = `${ name }=${ value || '' }${ expires }; path = /; Domain=${ domain };`;
};

const getBaseURL = () => {
    const { hostname } = window.location;

    switch (hostname) {
        case '42clouds.com':
        case 'wp.42clouds.com':
            return 'https://cp.42clouds.com';
        case 'beta.42clouds.com':
        case 'wpdev.42clouds.com':
        case 'wpbeta.42clouds.com':
            return 'https://betacp.42clouds.com';
        case '42clouds.pro':
            return 'https://cp.42clouds.pro';
        default:
            return 'https://cp.42clouds.com';
    }
};

const getLogoutURL = () => {
    const { hostname, href } = window.location;

    if (hostname === '42clouds.com') {
        return `https://provider.42clouds.com/openid/e1cib/oid2op?cmd=logout&openid.return_to=${ href }`;
    }

    return `https://beta-provider.42clouds.com/openid/e1cib/oid2op?cmd=logout&openid.return_to=${ href }`;
};

const injectStyles = () => {
    const styles = `
            #stripe {
                background-color: #333;
                width: 100%;
                height: 30px;
                background: #323a45;
                font-family: 'Open Sans', sans-serif;
            }
            
            #stripe .stripeDiv {
                position: fixed;
                background-color: #323a45;
                z-index: 100;
                width: 100%;
                color: #fff;
                height: 30px;
                display: flex;
                align-items: center;
                color: #DEDEDE;
                padding: 0 5px;
            }
            
            #stripe #LoggedInMenu, #stripe #LocaleMenu {
                display: flex;
                justify-content: space-between;
                align-items: center;
                list-style: none;
                padding: 0 20px 0 5px;
                width: 100%;
            }
            
            #stripe #LoggedInMenu > div, #stripe #LocaleMenu > div {
                display: flex;
                gap: 5px;
            }

            #stripe button {
                color: inherit;
                background: none;
                border: none;
                display: flex;
                align-items: center;
            }

            #stripe #LeftStripeMenu, #stripe .top-right {
                list-style: none;
                padding: 0;
                display: flex;
                margin: 0;
            }

            #stripe #LeftStripeMenu li:last-child, #stripe .top-right li:last-child {
                margin-right: 0;
            }

            #stripe a {
                transition: color 0.3s;
                text-decoration: underline;
                color: #DEDEDE;
            }

            #stripe a:hover {
                color: #00E1FF;
                text-decoration-color: #00E1FF;
            }

            #stripe .flag-icon {
                display: inline-block;
                width: 25px;
                height: 25px;
                background-repeat: no-repeat;
            }

            #stripe .login-icon, #stripe .register-icon {
                background: 50% 50% no-repeat;
                display: inline-block;
                width: 20px;
                height: 20px;
                margin-right: 5px;
            }
            
            #stripe .noHover a {
                text-decoration: none;
                font-weight: bold;
                color: #f77121;
            }
            
            #stripe .noHover a:hover {
                color: #f77121;
            }

             @media (max-width: 700px) {
                #stripe #LoggedInMenu > div:first-of-type, #stripe #LocaleMenu > div:first-of-type {
                    display: none;
                }
            }
        `;
    const styleSheet = document.createElement('style');
    styleSheet.type = 'text/css';
    styleSheet.innerText = styles;
    document.head.appendChild(styleSheet);
};

const createButton = (text, link) => {
    const btn = document.createElement('button');
    const a = document.createElement('a');
    a.href = link;
    a.textContent = text;
    btn.appendChild(a);

    return btn;
};

const createLoggedInMenu = data => {
    const baseURL = getBaseURL();
    const menu = document.createElement('div');
    menu.id = 'LoggedInMenu';
    const leftMenu = document.createElement('div');
    const accountButton = createButton('Личный кабинет', `${ baseURL }/`);
    leftMenu.appendChild(accountButton);
    const rightMenu = document.createElement('div');
    const balanceButton = createButton(`Баланс: ${ data.CurrentUserBalance.toFixed(2) } ${ data.LocaleCurrency }`, `${ baseURL }/billing`);
    rightMenu.appendChild(balanceButton);
    const logoutButton = createButton('Выйти', `${ getLogoutURL() }`);
    logoutButton.addEventListener('click', () => {
        deleteCookie('AccessToken', '42clouds.com');
    });
    rightMenu.appendChild(logoutButton);
    const nameButton = createButton(data.CurrentUser, `${ baseURL }/profile`);
    rightMenu.appendChild(nameButton);
    menu.appendChild(leftMenu);
    menu.appendChild(rightMenu);

    return menu;
};

const createNotLoggedInMenu = () => {
    const baseURL = getBaseURL();
    const language = extractLanguageCode(window.location.href);
    const localeMenu = document.createElement('div');
    localeMenu.id = 'LocaleMenu';
    const flagsDiv = document.createElement('div');
    const buttonsDiv = document.createElement('div');
    const loginButton = createButton(language !== 'uk-ua' ? 'Войти' : 'Увійти', `${ baseURL }/signin`);

    if (language !== 'uk-ua') {
        const locales = ['ru-ru', 'ru-kz'];

        locales.forEach(locale => {
            const country = locale.split('-')[1];
            const hrefParts = window.location.origin.split('.');

            if (!window.location.origin.includes('beta')) {
                hrefParts.pop();
                hrefParts.push(country);
            } else if (country === 'kz') {
                const shifted = hrefParts.shift();
                hrefParts.unshift(shifted.replace('wp', 'wpkz'));
            }

            const li = document.createElement('div');
            const a = document.createElement('a');
            a.href = hrefParts.join('.');
            a.setAttribute('locale', locale);
            const flag = document.createElement('span');
            flag.className = 'flag-icon';
            flag.style.backgroundImage = country === 'kz'
                ? `url(https://cp.42clouds.com/Content/img/stripe/flag-${ country }.png)`
                : `url(https://cp.42clouds.com/Content/img/stripe/flag_${ country }_big.png)`;
            a.appendChild(flag);
            li.appendChild(a);
            flagsDiv.appendChild(li);
        });

        const registerIcon = document.createElement('span');
        registerIcon.className = 'register-icon';
        registerIcon.style.backgroundImage = 'url(https://cp.42clouds.com/Content/img/stripe/cloud.png)';
        const registerButton = createButton('Зарегистрироваться', `${ baseURL }/signup?LocaleName=${ language }`);
        registerButton.insertBefore(registerIcon, registerButton.firstChild);
        buttonsDiv.appendChild(registerButton);
    } else {
        loginButton.className = 'noHover';
    }

    const loginIcon = document.createElement('span');
    loginIcon.className = 'login-icon';
    loginIcon.style.backgroundImage = 'url(https://cp.42clouds.com/Content/img/stripe/login.png)';
    loginButton.insertBefore(loginIcon, loginButton.firstChild);
    localeMenu.appendChild(flagsDiv);
    buttonsDiv.appendChild(loginButton);
    localeMenu.appendChild(buttonsDiv);

    return localeMenu;
};

const apiHost = () => {
    if (window.location.hostname === '42clouds.com' || window.location.hostname === 'wp.42clouds.com' || window.location.hostname === '42clouds.kz') {
        return 'https://core.42clouds.com';
    }

    return 'https://betacore.42clouds.com';
};

const getCookie = name => {
    const cookieValue = `; ${ document.cookie }`;
    const parts = cookieValue.split(`; ${ name }=`);

    if (parts.length === 2) {
        return parts.pop().split(';').shift();
    }

    return null;
};

const fetchData = async () => {
    const AccessToken = getCookie('AccessToken');

    if (AccessToken) {
        const headers = {
            headers: {
                Authorization: `Bearer ${ AccessToken }`
            },
        };

        try {
            const response = await fetch(`${ apiHost() }/stripe/account-data`, headers);
            return response.json();
        } catch (error) {
            return INITIAL_DATA;
        }
    }
    return INITIAL_DATA;
};

const updateUserData = data => {
    localStorage.removeItem('MARKET/PROMO/ACCOUNT-ADMIN-EMAIL');
    localStorage.removeItem('MARKET/PROMO/ACCOUNT-ADMIN-PHONE');
    localStorage.removeItem('MARKET/PROMO/ACCOUNT-ID');
    localStorage.removeItem('MARKET/PROMO/ACCOUNT-USER-ID');
    localStorage.setItem('MARKET/PROMO/ACCOUNT-ADMIN-EMAIL', data.AccountAdminEmail || '');
    localStorage.setItem('MARKET/PROMO/ACCOUNT-ADMIN-PHONE', data.AccountAdminPhone || '');
    localStorage.setItem('MARKET/PROMO/ACCOUNT-ID', data.ContextAccountId || '');
    localStorage.setItem('MARKET/PROMO/ACCOUNT-USER-ID', data.CurrentAccountUserId || '');
    setCookie('AccountId', data.ContextAccountId, 3600);
};
const renderStripeMenu = async () => {
    const { data } = await fetchData();
    updateUserData(data);
    const stripeWrapper = document.getElementById('stripe');

    const stripeDiv = document.createElement('div');
    stripeDiv.appendChild(data.IsLogin ? createLoggedInMenu(data) : createNotLoggedInMenu(data));
    stripeDiv.className = 'stripeDiv';
    stripeWrapper.appendChild(stripeDiv);
};

document.addEventListener('DOMContentLoaded', () => {
    injectStyles();
    void renderStripeMenu();
}, false);