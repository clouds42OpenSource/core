(async function (_w, d, u) {
    if (!/apple/i.test(navigator.vendor)) {
        const getCookie = (name) => {
            const matches = document.cookie.match(new RegExp(
                `(?:^|; )${ name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') }=([^;]*)`
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        };

        const userId = localStorage.getItem('UsrID');
        const token = getCookie('AccessToken');
        const currentUrl = window.location.href;

        const userInfo = await fetch(
            `https://core.42clouds.com/account-users/${userId}/bitrix-info?currentUrl=${currentUrl}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                    Accept: 'application/json'
                }
            }
        ).then(response => response.json()).catch(err => console.log(err));

        if (userInfo) {
            _w.addEventListener('onBitrixLiveChat', function (event) {
                const widget = event.detail.widget;

                widget.setCustomData([
                    { 'GRID': userInfo.Data }
                ]);
            });

            const s = d.createElement('script'); s.async = true; s.src = u + '?' + (Date.now() / 60000 | 0);
            const h = d.getElementsByTagName('script')[0]; h.parentNode.insertBefore(s, h);
        }
    }
})(window, document, 'https://cdn-ru.bitrix24.ru/b7644257/crm/site_button/loader_5_t9ii5z.js');