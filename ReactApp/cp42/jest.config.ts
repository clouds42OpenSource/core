const jestConfig = {
    coverageReporters: ['html', 'text'],
    roots: ['<rootDir>/src'],
    collectCoverageFrom: ['<rootDir>/src/**/*.{js,jsx,ts,tsx}', '!<rootDir>/src/**/*.d.ts', '!<rootDir>/**/node_modules/**'],
    reporters: ['default', 'jest-junit'],
    setupFilesAfterEnv: ['<rootDir>/setup.jest.ts'],
    testMatch: [
        '<rootDir>/src/**/*.{spec,test}.{js,jsx,ts,tsx}',
    ],
    testEnvironment: 'jest-environment-jsdom',
    testRunner:
        '<rootDir>/node_modules/jest-circus/runner.js',
    transform: {
        '^.+\\.[t|j]sx?$': 'babel-jest'
    },
    transformIgnorePatterns: [
        '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs|cjs|ts|tsx)$',
    ],
    modulePaths: ['<rootDir>/src'],
    moduleNameMapper: {
        '\\.(css|less)$': '<rootDir>/mock.jest.js',
        '^@/(.*)$': '<rootDir>/src/$1',
    },
    moduleFileExtensions: [
        'web.js',
        'js',
        'web.ts',
        'ts',
        'web.tsx',
        'tsx',
        'json',
        'web.jsx',
        'jsx',
        'node',
    ],
    resetMocks: true,
};

export default jestConfig;