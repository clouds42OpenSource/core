import * as fs from 'node:fs';
import path from 'path';
import progress from 'vite-plugin-progress';
import checker from 'vite-plugin-checker';
import { defineConfig, UserConfig } from 'vite';
import react from '@vitejs/plugin-react';

export default defineConfig(({ mode }) => {
    const DEV_PROJECT_NAME = 'devreactapp.42clouds.com';
    const ENV_PREFIX = 'REACT';

    const app = { find: 'app', replacement: path.join(__dirname, 'src', 'app') };
    const core = { find: 'core', replacement: path.join(__dirname, 'src', 'core') };
    const pages = { find: 'pages', replacement: path.join(__dirname, 'src', 'pages') };

    const config: UserConfig = {
        plugins: [
            react(),
            progress(),
            checker({ typescript: true })
        ],
        envPrefix: ENV_PREFIX,
        resolve: {
            alias: [app, core, pages],
        },
        build: {
            commonjsOptions: {
                transformMixedEsModules: true,
            },
            outDir: 'build'
        },
        define: {
            global: {},
        },
        clearScreen: false,
    };

    if (mode === 'localdev') {
        config.server = {
            port: 443,
            host: DEV_PROJECT_NAME,
            https: {
                key: fs.readFileSync(path.join(__dirname, 'cert', `${ DEV_PROJECT_NAME }.key`)),
                cert: fs.readFileSync(path.join(__dirname, 'cert', `${ DEV_PROJECT_NAME }.crt`))
            },
        };
    }

    return config;
});