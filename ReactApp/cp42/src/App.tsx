import { ThemeProvider } from '@mui/material';
import { AppRoutes } from 'app/AppRoutes';
import { AccountUserGroup } from 'app/common/enums';
import { RouteHelper } from 'app/common/helpers/RouteHelper';
import { useAppSelector } from 'app/hooks';
import { setupStore } from 'app/redux';
import { THEME } from 'app/utils';
import { Layout } from 'app/views/Layout';
import { ProjectTour } from 'app/views/Layout/ProjectTour';
import { LoadingView } from 'app/views/Loading';
import { CommonTableWithFilterContextProvider } from 'app/views/modules/_common/components/CommonTableWithFilter/context/CommonTableWithFilterContextProvider';
import { ScrollToTop } from 'app/views/modules/ScrollToTop';
import { SnackbarProvider } from 'notistack';
import * as pages from 'pages';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

const store = setupStore();

const adminRole = [AccountUserGroup.Hotline, AccountUserGroup.CloudAdmin, AccountUserGroup.Cloud42Service, AccountUserGroup.AccountSaleManager, AccountUserGroup.CloudSE];

const RoutesWithLayout = () => {
    const { locale } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer.settings.currentContextInfo);

    const isRuLocale = locale === 'ru-ru';

    return (
        <Layout>
            <CommonTableWithFilterContextProvider>
                <Switch>
                    <Route
                        exact={ true }
                        path={ AppRoutes.homeRoute }
                        component={ pages.MainPage }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.accessError }
                        component={ pages.ErrorAccessPage }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.dictionary.editCloudServicesRoute }
                        component={ pages.CloudServicesPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.loggingRoute }
                        component={ pages.LoggingPageAsync }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.tasksRoute }
                        component={ pages.TasksPageAsync }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.processFlowRoute }
                        component={ pages.ProcessFlowPageAsync }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.getProcessFlowDetailsRoute }
                        component={ pages.ProcessFlowDetailsPage }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.dictionary.delimiterSourceAccountDatabasesRoute }
                        component={ pages.DelimiterSourceAccountDatabasesPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.serviceAccountsRoute }
                        component={ pages.ServiceAccountsPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.configurations1CRoute }
                        component={ pages.Configurations1CPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.industryRoute }
                        component={ pages.IndustryPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.dbTemplatesRoute }
                        component={ pages.DbTemplatesPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.dbTemplateDelimitersRoute }
                        component={ pages.DbTemplateDelimetersPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.agencyAgreementRoute }
                        component={ pages.AgencyAgreementPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.accountDatabasesListRoute }
                        component={ pages.DatabaseListPageAsync }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.settingCloudRoute }
                        component={ pages.CloudCorePageAsync }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.accountsListRoute }
                        component={ pages.AccountListPageAsync }
                        rules={ [...adminRole, AccountUserGroup.ArticleEditor] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.dbTemplateUpdatesRoute }
                        component={ pages.DbTemplateUpdatesPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.suppliersRoute }
                        component={ pages.SuppliersPageAsync }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.suppliers.editCreateSupplier }
                        component={ pages.CreateEditSupplierPage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.suppliers.supplierReferalAccounts }
                        component={ pages.SupplierReferalAccountsPage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.localesRoute }
                        component={ pages.LocalesPage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.printedHtmlFormsReferenceRoute }
                        component={ pages.PrintedHtmlFormsReferencePage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.printedHtmlForms.createPrintedHtmlForm }
                        component={ pages.CreateEditPrintedHtmlFormPage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.marketing }
                        component={ pages.MarketingPage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.marketing.createAdvertisingBanner }
                        component={ pages.AdvertisingBannerCreatePage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.marketing.editAdvertisingBanner }
                        component={ pages.AdvertisingBannerEditPage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.marketing.editLetterTemplate }
                        component={ pages.LetterTemplateEditPage }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.printedHtmlForms.editPrintedHtmlForm }
                        component={ pages.CreateEditPrintedHtmlFormPage }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.segmentsRoute }
                        component={ pages.SegmentsPage }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.CloudSE] }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.itsAuthorizationDataRoute }
                        component={ pages.ItsDataAuthorizationPage }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.armAutoUpdateAccountDatabase }
                        component={ pages.ArmAutoUpdateAccountDatabasePage }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.accountManagement.balanceManagement }
                        component={ pages.BalanceManagementPage }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.accountManagement.replenishBalance }
                        component={ pages.ReplenishBalancePage }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.accountManagement.confirmPayment }
                        component={ pages.ConfirmPaymentView }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.accountManagement.userManagement }
                        component={ pages.UsersManagementPage }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.accountManagement.myCompany }
                        component={ pages.MyCompanyPage }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.services.additionalSessions }
                        component={ pages.AdditionalSessionsPage }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.accountManagement.informationBases }
                        component={ pages.InformationDatabases }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.accountManagement.legacyInformationBases }
                        component={ pages.InformationDatabasesLegacy }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.services.m42 }
                        component={ pages.M42Page }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.services.myServices }
                        component={ pages.ExtensionsAndProcessingPage }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.services.createService }
                        component={ pages.CreateServicePage }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.services.serviceCard }
                        component={ pages.ServiceCardPage }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.services.editService }
                        component={ pages.EditServicePage }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.services.myDisk }
                        component={ pages.MyDiskPage }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.accountManagement.createAccountDatabase.loadingDatabase }
                        component={ pages.LoadingDatabase }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.settings.profile }
                        component={ pages.ProfilePage }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.services.rentReference }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                        component={ pages.RentViewPage }
                    />
                    <RouteHelper
                        exact={ true }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                        path={ AppRoutes.services.fasta }
                        component={ pages.FastaViewPage }
                    />
                    <RouteHelper
                        path={ AppRoutes.services.rent.editRentResource }
                        component={ pages.RentEditResourcePage }
                        rules={ adminRole }
                        exact={ true }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.settings.connectionSettings }
                        component={ pages.ConnectionSettingsPage }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.accountManagement.createAccountDatabase.createDb }
                        rules={ [...adminRole, AccountUserGroup.AccountAdmin] }
                        component={ pages.CreateDbPage }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.partners }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.Hotline, AccountUserGroup.AccountSaleManager, AccountUserGroup.ArticleEditor] }
                        component={ pages.Partners }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.partners.toPartners }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.Hotline, AccountUserGroup.AccountAdmin] }
                        component={ pages.ToPartners }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.partners.createAgentRequisites }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.Hotline, AccountUserGroup.AccountAdmin] }
                        component={ pages.CreateAgentRequisites }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.partners.editAgentRequisites }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.Hotline, AccountUserGroup.AccountAdmin, AccountUserGroup.ArticleEditor] }
                        component={ pages.EditAgentRequisites }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.partners.createAgentCashOut }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.Hotline, AccountUserGroup.AccountAdmin] }
                        component={ pages.CreateAgentCashOut }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.partners.editAgentCashOut }
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.Hotline, AccountUserGroup.AccountAdmin, AccountUserGroup.ArticleEditor] }
                        component={ pages.EditAgentCashOut }
                        exact={ true }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.services.managingServiceSubscriptions }
                        component={ pages.ManagingServiceSubscriptions }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.neuro }
                        component={ pages.Neuro }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.services.managingServicesMyDatabaseSubscription }
                        component={ pages.ManagingServicesMyDatabaseSubscription }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.accountManagement.armManager }
                        component={ pages.ArmManagerPage }
                    />
                    <RouteHelper
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.Hotline, AccountUserGroup.CloudSE, AccountUserGroup.AccountAdmin, AccountUserGroup.AccountUser] }
                        exact={ true }
                        path={ AppRoutes.ultra.performance }
                        component={ pages.UltraPage }
                    />
                    <RouteHelper
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.Hotline, AccountUserGroup.CloudSE, AccountUserGroup.AccountAdmin, AccountUserGroup.AccountUser] }
                        exact={ true }
                        path={ AppRoutes.ultra.servers }
                        component={ pages.UltraPage }
                    />
                    <RouteHelper
                        rules={ [AccountUserGroup.CloudAdmin, AccountUserGroup.AccountSaleManager, AccountUserGroup.Hotline, AccountUserGroup.CloudSE, AccountUserGroup.AccountAdmin, AccountUserGroup.AccountUser] }
                        exact={ true }
                        path={ AppRoutes.ultra.devops }
                        component={ pages.UltraDevops }
                    />
                    <Route
                        exact={ true }
                        path={ AppRoutes.articlesReference }
                        component={ pages.ArticlesPage }
                    />
                    <Route
                        path={ AppRoutes.articles.editArticleForBlog }
                        component={ pages.EditArticlePage }
                        exact={ true }
                    />
                    <Route
                        path={ AppRoutes.articles.editArticleForSite }
                        component={ pages.EditArticlePage }
                        exact={ true }
                    />
                    <Route
                        path={ AppRoutes.articles.editArticleForSiteReference }
                        component={ pages.EditArticlePage }
                        exact={ true }
                    />
                    <RouteHelper
                        path={ AppRoutes.administration.sendingNotifications }
                        rules={ [AccountUserGroup.CloudAdmin] }
                        exact={ true }
                        component={ pages.SendingNotificationsPage }
                    />
                    <Route
                        path={ AppRoutes.confirmationOfAccountDeletion }
                        exact={ true }
                        component={ pages.ConfirmationOfAccountDeletionPage }
                    />
                    <Route
                        path={ AppRoutes.confirmationAct }
                        exact={ true }
                        component={ pages.ConfirmationActPage }
                    />
                    <Route
                        path={ AppRoutes.services.alpacaMeet }
                        exact={ true }
                        component={ pages.AlpacaMeetPage }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.settings.consultations1C }
                        component={ pages.Consultations1CPage }
                        conditions={ isRuLocale }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.settings.registrationNumber }
                        component={ pages.RegistrationNumberPage }
                        conditions={ isRuLocale }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.settings.reporting }
                        component={ pages.ReportingPage }
                        conditions={ isRuLocale }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.settings.edo }
                        component={ pages.EdoPage }
                        conditions={ isRuLocale }
                    />
                    <RouteHelper
                        exact={ true }
                        path={ AppRoutes.settings.reconciliationStatement }
                        component={ pages.ReconciliationStatementPage }
                        conditions={ isRuLocale }
                    />
                    <Route
                        exact={ true }
                        path="*"
                        component={ pages.Error404Page }
                    />
                </Switch>
            </CommonTableWithFilterContextProvider>
        </Layout>
    );
};

const App = () => (
    <Provider store={ store }>
        <ThemeProvider theme={ THEME }>
            <BrowserRouter basename="">
                { /*<ErrorBoundary fallback={ <Boundary /> }>*/ }
                <ScrollToTop />
                <SnackbarProvider
                    maxSnack={ 3 }
                    anchorOrigin={ {
                        horizontal: 'right',
                        vertical: 'top',
                    } }
                >
                    <GoogleReCaptchaProvider
                        reCaptchaKey={ import.meta.env.REACT_APP_CAPTCHA_TOKEN ?? '' }
                        scriptProps={ {
                            async: false,
                            defer: false,
                            appendTo: 'head',
                            nonce: undefined,
                        } }
                    >
                        <ProjectTour>
                            <Switch>
                                <Route exact={ true } path={ AppRoutes.signInRoute } component={ pages.SignInPage } />
                                <Route exact={ true } path={ AppRoutes.signUpRoute } component={ pages.SignUpPage } />
                                <Route exact={ true } path={ AppRoutes.resetPassword } component={ pages.ResetPasswordPage } />
                                <Route exact={ true } path={ AppRoutes.unsubscribe } component={ pages.UnsubscribePage } />
                                <Route exact={ true } path={ AppRoutes.proxyPage } component={ pages.ProxyPage } />
                                <Route exact={ true } path={ AppRoutes.emailVerify } component={ pages.EmailVerify } />
                                <Route exact={ true } path={ AppRoutes.link42Downloader } component={ pages.Link42DownloaderPage } />
                                <Route path="/">
                                    <RoutesWithLayout />
                                </Route>
                            </Switch>
                        </ProjectTour>
                        <LoadingView />
                    </GoogleReCaptchaProvider>
                </SnackbarProvider>
                { /*</ErrorBoundary>*/ }
            </BrowserRouter>
        </ThemeProvider>
    </Provider>
);

export default App;