/**
 * Represents boolean value with three values
 */
export enum TripleBoolean {
    /**
     * Unset value
     */
    NONE = -1,

    /**
     * Represents boolean false
     */
    NO = 0,

    /**
     * Represents boolean true
     */
    YES = 1
}