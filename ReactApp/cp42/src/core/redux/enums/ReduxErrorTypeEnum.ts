/**
 * Represents error types of redux
 */
export enum ReduxErrorTypeEnum {
    /**
     * Reducer is in busy state for a long time
     */
    REDUCER_BUSY = 'REDUCER_BUSY'
}