import { TripleBoolean } from 'core/enums';
import { ProcessIdType } from 'core/redux/types';
import { IReducerStateProcessInfo } from '../interfaces';

export const isProcessInSuccessState = <TProcessId extends ProcessIdType>(process: IReducerStateProcessInfo<TProcessId>, processIdToCheck: TProcessId, prevProcessWasInProgress: boolean): TripleBoolean => {
    if (!process ||
        !processIdToCheck ||
        process.id !== processIdToCheck ||
        process.isInProgress ||
        (!process.isInProgress && !prevProcessWasInProgress)) {
        return TripleBoolean.NONE;
    }

    return !process.error
        ? TripleBoolean.YES
        : TripleBoolean.NO;
};