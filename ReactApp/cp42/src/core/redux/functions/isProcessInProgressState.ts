import { TripleBoolean } from 'core/enums';
import { ProcessIdType } from 'core/redux/types';
import { IReducerStateProcessInfo } from '../interfaces';

export const isProcessInProgressState = <TProcessId extends ProcessIdType>(process: IReducerStateProcessInfo<TProcessId>, processIdToCheck: TProcessId): TripleBoolean => {
    if (!process ||
        !process.isInProgress ||
        !processIdToCheck) {
        return TripleBoolean.NONE;
    }

    return process.id === processIdToCheck
        ? TripleBoolean.YES
        : TripleBoolean.NO;
};