import { getReducerProcessActionInfoIfChanged } from 'core/redux/functions';
import { IReducerState, IReducerStateProcessInfo } from '../interfaces';
import { ProcessIdType, TManualReducerState } from '../types';

/**
 * Creates new reducer state
 * @param state - State to make succeed
 * @param processId - For what process id make the state succeed
 * @param newProcess
 * @param manualState - Custom manual data
 * @TState Type of reducer's state
 */
export function reducerRecreateState<TState extends IReducerState<ProcessIdType> & TManualReducerState<TState>>(
    state: TState,
    processId: ProcessIdType,
    newProcess: IReducerStateProcessInfo<ProcessIdType>,
    manualState?: Partial<TManualReducerState<TState>>
): TState {
    const reducerAction = state.reducerActions[processId] ?? {
        process: state.process,
        processActionState: {
            isInErrorState: false,
            isInProgressState: false,
            isInSuccessState: false
        },
        hasProcessActionStateChanged: false,
    };

    const prevProcess = reducerAction.process;

    const newReducerProcessActionInfo = getReducerProcessActionInfoIfChanged(
        newProcess,
        prevProcess,
        reducerAction.processActionState,
        processId
    );

    const hasReducerProcessActionChanged = !!newReducerProcessActionInfo;

    return {
        ...state,
        process: newProcess,
        reducerActions: {
            ...state.reducerActions,
            [processId]: {
                process: newProcess,
                processActionState: hasReducerProcessActionChanged ? newReducerProcessActionInfo : state.reducerActions[processId].processActionState,
                hasProcessActionStateChanged: hasReducerProcessActionChanged
            }
        },
        ...(manualState ?? {})
    };
}