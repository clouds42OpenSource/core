import { IReducerState } from '../interfaces';
import { ProcessIdType, TManualReducerState } from '../types';

/**
 * Initializes reducer's state
 * @param stateName Unique name of state
 * @param state Manual properties of state
 */
export function initReducerState<TState extends IReducerState<ProcessIdType> & TManualReducerState<TState>>(
    stateName: string, state: Required<TManualReducerState<TState>>): TState {
    return {
        stateName: `${ stateName }_State`,
        process: {
            isInProgress: false
        },
        reducerActions: {},
        ...state
    } as TState;
}