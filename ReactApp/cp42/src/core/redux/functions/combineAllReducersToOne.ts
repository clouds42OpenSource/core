import { combineReducers } from 'redux';
import { TReducerObject } from '../types';

type TReducer = { [reducerState: string]: TReducerObject<any, any> };

/**
 * Combines reducers to one state
 * @param reducers Reducers to combine to one state
 */
export function combineAllReducersToOne(reducers: TReducer[]) {
    if (!Array.isArray(reducers) || reducers.length === 0) {
        return combineReducers({});
    }

    const allReducers = reducers.reduce((total, current) =>
        current ? Object.assign(total, { ...current }) : total
    );

    return combineReducers(allReducers);
}