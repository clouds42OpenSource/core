type ReduxActionType = 'START' | 'SUCCESS' | 'FAILED' | 'RESET';

const reducerActionSeparator = '--';

function createReducerActionValue(reducerStateName: string, action: string, actionType: ReduxActionType) {
    const separator = reducerActionSeparator;
    reducerStateName = reducerStateName?.toString()?.toUpperCase()?.replace(' ', '_')?.replace('-', '_') ?? (new Date().getTime()).toString();
    const actionStr = action?.toString()?.toUpperCase() ?? (new Date().getTime()).toString();
    return `${ reducerStateName }${ separator }${ actionStr }${ separator }${ actionType }_ACTION`;
}

export function changeReduxActionType(reduxAction: string, newActionType: ReduxActionType) {
    const actionParts = reduxAction.split(reducerActionSeparator);
    return createReducerActionValue(actionParts[0], actionParts[1], newActionType);
}

/**
 * Creates three types of action:
 *
 *     START_ACTION: {reducerStateName}--{action}--START_ACTION
 *     SUCCESS_ACTION: {reducerStateName}--{action}--SUCCESS_ACTION
 *     FAILED_ACTION: {reducerStateName}--{action}--FAILED_ACTION
 *     RESET_ACTION: {reducerStateName}--{action}--RESET_ACTION
 * @param reducerStateName - reducer's name
 * @param action - action of a reducer
 */
export function createReducerActions(reducerStateName: string, action: string): {
    /**
     * Действие при старте выполнения процесса
     */
    'START_ACTION': string;
    /**
     * Действие при успешном выполнении процесса
     */
    'SUCCESS_ACTION': string;
    /**
     * Действие при неуспешном выполнении процесса
     */
    'FAILED_ACTION': string;
    /**
     * Действие при сбросе о том что было какое-то действие
     */
    'RESET_ACTION': string;
} {
    return {
        START_ACTION: createReducerActionValue(reducerStateName, action, 'START'),
        SUCCESS_ACTION: createReducerActionValue(reducerStateName, action, 'SUCCESS'),
        FAILED_ACTION: createReducerActionValue(reducerStateName, action, 'FAILED'),
        RESET_ACTION: createReducerActionValue(reducerStateName, action, 'RESET')
    };
}