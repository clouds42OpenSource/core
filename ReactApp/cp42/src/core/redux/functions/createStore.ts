import { legacy_createStore as reduxCreateStore, StoreEnhancer } from 'redux';
import { combineAllReducersToOne } from '.';
import { TReducersContainer } from '../types';

/**
 * Creates REDUX store
 * @param initialReducers Initial reducers
 * @param enhancer Enhancer
 */
export const createStore = <T extends string>(initialReducers: TReducersContainer<T>, enhancer: StoreEnhancer) =>
    reduxCreateStore(combineAllReducersToOne([initialReducers]), enhancer);