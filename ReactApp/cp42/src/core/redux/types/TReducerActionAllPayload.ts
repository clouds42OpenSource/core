import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from '../interfaces';

/**
 * Represents all reducer's action types
 */
export type TReducerActionAllPayload =
  | ReducerActionStartPayload
  | ReducerActionSuccessPayload
  | ReducerActionFailedPayload;