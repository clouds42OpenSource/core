import { TRootState } from 'app/redux';
import { IBaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject/interfaces/IBaseReduxThunkObject';
import { changeReduxActionType } from 'core/redux/functions';
import { CombinedState } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { CommonError } from '..';
import { ReduxErrorTypeEnum } from '../enums';
import { IReducerAction, IReducerState, IReduxThunkAction, ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from '../interfaces';
import { ProcessIdType, TReducerStateContainer } from '../types';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from './interfaces';

/**
 * Базовый класс создания Thunk объекта для обработки некоторого запроса по правилам действия:
 *
 *    XXX--YYY--START_ACTION,
 *    XXX--YYY--SUCCESS_ACTION,
 *    XXX--YYY--FAILED_ACTION.
 */
export abstract class BaseReduxThunkObject<
    TStore extends TReducerStateContainer<string> | CombinedState<TRootState>,
    TReducerActionStartPayload extends ReducerActionStartPayload,
    TReducerActionSuccessPayload extends ReducerActionSuccessPayload,
    TReducerActionFailedPayload extends ReducerActionFailedPayload,
    TInputParams> implements IBaseReduxThunkObject<TInputParams> {
    protected constructor() {
        this.execute = this.execute.bind(this);
        this.getStartActionArguments = this.getStartActionArguments.bind(this);
        this.startExecution = this.startExecution.bind(this);
        this.tryStartAction = this.tryStartAction.bind(this);
        this.createActionInternal = this.createActionInternal.bind(this);
        this.createAction = this.createAction.bind(this);
    }

    /**
     * Метод, в котором происходит действие запроса по правилам действия:
     *
     *    XXX--YYY--START_ACTION,
     *    XXX--YYY--SUCCESS_ACTION,
     *    XXX--YYY--FAILED_ACTION.
     *
     * Возвращает Thunk функцию
     * @param args объект, который передаётся в метод для получение неких данных, нужных для обработки
     * @param showLoadingProgress Если true, будет показан анимация загрузки при диспатче иначе нет
     */
    public execute(args?: TInputParams, showLoadingProgress?: boolean): IReduxThunkAction<TReducerActionStartPayload | TReducerActionSuccessPayload | TReducerActionFailedPayload, TStore> {
        return async (dispatch, getStore) => {
            const startActionArguments = this.getStartActionArguments({
                getStore,
                thunkDispatch: dispatch,
                inputParams: args
            });

            if (
                !(await this.tryStartAction({
                    condition: startActionArguments.condition,
                    thunkDispatch: dispatch,
                    getReducerStateFunc: startActionArguments.getReducerStateFunc,
                    startActionPayload: startActionArguments.startActionPayload,
                    failedActionPayload: startActionArguments.failedActionPayload,
                    showLoadingProgress
                }))
            ) {
                return;
            }

            this.startExecution({
                thunkDispatch: <TThunkParams>(thunkObject: IBaseReduxThunkObject<TThunkParams>, inputArgs: TThunkParams) => {
                    return thunkObject.execute(inputArgs)(dispatch, getStore, undefined);
                },
                getStore,
                start: (startPayload?: TReducerActionStartPayload) => {
                    dispatch(this.createActionInternal({ name: this.startActionValue, payload: startPayload }));
                },
                success: (successPayload?: TReducerActionSuccessPayload) => {
                    dispatch(this.createActionInternal({
                        name: this.successActionValue,
                        payload: successPayload
                    }));
                    dispatch(this.createActionInternal({
                        name: changeReduxActionType(this.successActionValue, 'RESET')
                    }));
                },
                failed: (failedPayload: TReducerActionFailedPayload) => {
                    dispatch(this.createActionInternal({
                        name: this.failedActionValue,
                        payload: failedPayload
                    }));
                    dispatch(this.createActionInternal({
                        name: changeReduxActionType(this.failedActionValue, 'RESET')
                    }));
                },
                inputParams: args
            });
        };
    }

    protected abstract get startActionValue(): string;

    protected abstract get successActionValue(): string;

    protected abstract get failedActionValue(): string;

    protected abstract getStartActionArguments(
        args: IThunkStartActionParams<TStore, TReducerActionStartPayload, TReducerActionSuccessPayload, TReducerActionFailedPayload, TInputParams>):
        IThunkStartActionResult<TReducerActionStartPayload, TReducerActionFailedPayload>;

    protected abstract startExecution(
        args: IThunkStartExecutionParams<TStore, TReducerActionStartPayload, TReducerActionSuccessPayload, TReducerActionFailedPayload, TInputParams>
    ): void;

    private async tryStartAction(params: {
        condition: boolean,
        thunkDispatch: ThunkDispatch<any, any, any>,
        getReducerStateFunc: () => IReducerState<ProcessIdType>,
        startActionPayload?: TReducerActionStartPayload,
        failedActionPayload?: TReducerActionFailedPayload,
        showLoadingProgress?: boolean
    }): Promise<boolean> {
        if (!params.condition) {
            return false;
        }

        let reducerState = params.getReducerStateFunc();
        reducerState.process.showLoadingProgress = params.showLoadingProgress ?? true;

        if (reducerState.process.isInProgress) {
            for (let index = 1; index <= 100; ++index) {
                await new Promise(resolve => setTimeout(resolve, 150));
                if (!(reducerState = params.getReducerStateFunc()).process.isInProgress) {
                    params.thunkDispatch(this.createActionInternal({
                        name: this.startActionValue,
                        payload: params.startActionPayload
                    }));
                    return true;
                }
            }

            const failedPayload = params.failedActionPayload ?? {} as TReducerActionFailedPayload;
            params.thunkDispatch(this.createActionInternal({
                name: this.failedActionValue,
                payload: {
                    ...failedPayload,
                    error: new CommonError(
                        ReduxErrorTypeEnum.REDUCER_BUSY,
                        `The Reducer's state(${ reducerState.stateName }) is in busy state for a long time.`,
                        {
                            reducerName: reducerState.stateName,
                            Id: reducerState.process.id,
                            current: this.startActionValue
                        }
                    )
                }
            }));
            return false;
        }
        params.thunkDispatch(this.createActionInternal({
            name: this.startActionValue,
            payload: params.startActionPayload
        }));

        return true;
    }

    private createActionInternal(action: {
        name: string;
        payload?: TReducerActionStartPayload | TReducerActionSuccessPayload | TReducerActionFailedPayload
    }) {
        return action.payload === undefined
            ? this.createAction(action.name)
            : this.createAction(action.name, action.payload);
    }

    /**
     * Создаёт действие в котором передаются данные для обновления состояния в редюсере
     * @param type название действия
     * @param payload новые данные для обновления состояния в редюсере
     */
    protected createAction(type: string): IReducerAction<any>;

    protected createAction(type: string, payload: TReducerActionStartPayload | TReducerActionSuccessPayload | TReducerActionFailedPayload)
        : IReducerAction<TReducerActionStartPayload | TReducerActionSuccessPayload | TReducerActionFailedPayload>;

    protected createAction(type: string, payload?: TReducerActionStartPayload | TReducerActionSuccessPayload | TReducerActionFailedPayload | null) {
        return !payload
            ? {
                type
            }
            : {
                type,
                payload
            };
    }
}