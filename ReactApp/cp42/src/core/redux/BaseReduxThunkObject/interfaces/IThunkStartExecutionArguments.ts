import { IBaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject/interfaces/IBaseReduxThunkObject';

export interface IThunkStartExecutionParams<TStore, TReducerActionStartPayload, TReducerActionSuccessPayload, TReducerActionFailedPayload, TInputParams> {
    thunkDispatch: <TThunkParams>(thunkObject: IBaseReduxThunkObject<TThunkParams>, args: TThunkParams) => void;
    getStore: () => TStore;
    success: (successPayload?: TReducerActionSuccessPayload) => void;
    failed: (failedPayload: TReducerActionFailedPayload) => void;
    start: (startPayload?: TReducerActionStartPayload) => void;
    inputParams?: TInputParams;
}