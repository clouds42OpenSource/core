import { IReducerAction, ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { ThunkDispatch } from 'redux-thunk';

export interface IThunkStartActionParams<
    TStore,
    TReducerActionStartPayload extends ReducerActionStartPayload,
    TReducerActionSuccessPayload extends ReducerActionSuccessPayload,
    TReducerActionFailedPayload extends ReducerActionFailedPayload,
    TInputParams
> {
    thunkDispatch: ThunkDispatch<TStore, any, IReducerAction<TReducerActionStartPayload | TReducerActionSuccessPayload | TReducerActionFailedPayload>>;
    getStore: () => TStore;
    inputParams?: TInputParams;
}