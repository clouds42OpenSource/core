import { IReduxThunkAction } from 'core/redux/interfaces';

export interface IBaseReduxThunkObject<TParams> {
    execute: (args?: TParams) => IReduxThunkAction<any, any>;
}