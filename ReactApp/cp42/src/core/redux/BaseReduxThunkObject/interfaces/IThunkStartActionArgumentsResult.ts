import { IReducerState } from '../../interfaces';
import { ProcessIdType } from '../../types';

export interface IThunkStartActionResult<TReducerActionStartPayload, TReducerActionFailedPayload> {
    condition: boolean;
    getReducerStateFunc: () => IReducerState<ProcessIdType>;
    startActionPayload?: TReducerActionStartPayload;
    failedActionPayload?: TReducerActionFailedPayload;
}