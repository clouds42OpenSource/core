import { CombinedState } from 'redux';
import { TRootState } from 'app/redux';
import { IReduxThunkActionWithExtra } from '.';
import { TReducerActionAllPayload, TReducerStateContainer } from '../types';

/**
 * Base type to define Thunk action
 */
export type IReduxThunkAction<
    TPayload extends TReducerActionAllPayload,
    TStore extends TReducerStateContainer<string> | CombinedState<TRootState>
> = IReduxThunkActionWithExtra<TPayload, any, TStore>