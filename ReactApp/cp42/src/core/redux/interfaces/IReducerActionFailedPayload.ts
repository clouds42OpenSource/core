import { ErrorObject } from '..';

/**
 * Type to determine FAILED action for reducer
 */
export type ReducerActionFailedPayload = {
  /**
   * Contains error of the action
   */
  error: ErrorObject;
};