import { IReducerProcessActionInfo, IReducerStateProcessInfo } from '.';
import { ProcessIdType } from '../types';

/**
 * Represents reducer's state
 * @template TProcessId enum that contains process IDs
 */
export interface IReducerState<TProcessId extends ProcessIdType> {
  /**
   * Reducer's name
   */
  stateName: string;

  /**
   * Current process information
   */
  process: {
    /**
     * When equal NONE, it means process is in standby state, otherwise is in progress
     */
    id: TProcessId;

    /**
     * When equals true, it means process is in progress for the current reducer and only one process can be executing at time
     */
    isInProgress: boolean;

    /**
     * Если true, будет показан анимация загрузки при диспатче иначе нет
     */
    showLoadingProgress: boolean;

    error?: Error;
  };

  /**
   *
   */
  reducerActions: {
    [key in TProcessId]: IReducerActionInfo<TProcessId>
  };
}

export interface IReducerActionInfo<TProcessId extends ProcessIdType> {
  process: IReducerStateProcessInfo<TProcessId>;
  processActionState: IReducerProcessActionInfo;
  hasProcessActionStateChanged: boolean;
}