/**
 * Type to determine SUCCESS action for reducer
 */
export type ReducerActionSuccessPayload = NonNullable<unknown>;