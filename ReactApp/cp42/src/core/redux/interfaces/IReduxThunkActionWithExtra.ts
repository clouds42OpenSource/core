import { ThunkAction } from 'redux-thunk';
import { CombinedState } from 'redux';
import { TRootState } from 'app/redux';
import { IReducerAction } from '.';
import { TReducerActionAllPayload, TReducerStateContainer } from '../types';

/**
 *  Base type to define Thunk action with extra parameters
 */
export type IReduxThunkActionWithExtra<
    TPayload extends TReducerActionAllPayload,
    TExtra extends any,
    TStore extends TReducerStateContainer<string> | CombinedState<TRootState>
> = ThunkAction<void, TStore, TExtra, IReducerAction<TPayload>>