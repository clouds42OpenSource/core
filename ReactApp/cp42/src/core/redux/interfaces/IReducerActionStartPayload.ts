/**
 * Type to determine START action for reducer
 */
export type ReducerActionStartPayload = NonNullable<unknown>;