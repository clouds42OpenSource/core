import { AppRoutes } from 'app/AppRoutes';
import { ContentTypeEnum, WebVerb } from './enums';
import { IAjaxRequestParams, IAjaxSender, IRequestParams, paramsObject } from './interfaces';

import { IWebContentFormatterFactory } from '../contentFormatter/interfaces';

export abstract class AjaxSenderBase implements IAjaxSender {
    public constructor(contentFormatterFactory: IWebContentFormatterFactory) {
        this.contentFormatterFactory = contentFormatterFactory;

        this.submitWithPayload = this.submitWithPayload.bind(this);
        this.submitWithoutPayload = this.submitWithoutPayload.bind(this);
        this.submit = this.submit.bind(this);
        this.getAjax = this.getAjax.bind(this);
        this.postAjax = this.postAjax.bind(this);
        this.deleteAjax = this.deleteAjax.bind(this);
        this.putAjax = this.putAjax.bind(this);
        this.patchAjax = this.patchAjax.bind(this);
    }

    public static AuthenticationTypeKey = 'authentication-token-type';

    public static AuthenticationTokenKey = 'authentication-token';

    private contentFormatterFactory: IWebContentFormatterFactory;

    public static objectToQueryString(params: paramsObject, prefix = '') {
        let queryString = '';

        if (Array.isArray(params)) {
            for (const value of params) {
                if (value || value === 0) {
                    queryString += `${ encodeURIComponent(prefix) }=${ encodeURIComponent(value.toString()) }&`;
                }
            }
        } else {
            for (const key in params) {
                if (Object.hasOwn(params, key)) {
                    const paramKey = prefix ? `${ prefix }.${ key }` : key;
                    if (typeof params[key] === 'object') {
                        queryString += `${ this.objectToQueryString(params[key] as paramsObject, paramKey) }`;
                    } else {
                        queryString += params[key] || params[key] === 0 ? `${ encodeURIComponent(paramKey) }=${ encodeURIComponent(params[key].toString()) }&` : '';
                    }
                }
            }
        }
        return queryString;
    }

    public abstract getAuthenticationTokenType(): string | undefined | null;

    public abstract getAuthenticationToken(): string | undefined | null;

    private submitWithPayload = <TResponse extends NonNullable<unknown> | Blob>(method: WebVerb, url: string, params?: IRequestParams<any>) => {
        const p: IAjaxRequestParams = {
            method,
            requestMode: params?.requestMode,
            requestCache: params?.requestCache,
            abortSignal: params?.abortSignal,
            contentType: params?.contentType,
            acceptContentType: params?.acceptContentType,
            headers: params?.headers,
            dataToSend: params?.dataToSend
        };

        return this.submit<TResponse>(url, p);
    };

    private submitWithoutPayload = <TResponse extends NonNullable<unknown> | Blob>(method: WebVerb, url: string, params?: IRequestParams<any>) => {
        const p: IAjaxRequestParams = {
            method,
            requestMode: params?.requestMode,
            requestCache: params?.requestCache,
            abortSignal: params?.abortSignal,
            contentType: undefined,
            acceptContentType: params?.acceptContentType,
            headers: params?.headers,
            dataToSend: params?.dataToSend
        };
        let urlFull;
        if (params) {
            const data = params.dataToSend;
            if (data) {
                const queryString = `${ AjaxSenderBase.objectToQueryString(data as paramsObject).slice(0, -1) }`;
                urlFull = `${ url }${ queryString ? `?${ queryString }` : '' }`;
            } else {
                urlFull = url;
            }
        } else {
            urlFull = url;
        }

        return this.submit<TResponse>(urlFull, p);
    };

    protected abstract GetRejectedPromiseFromResponse<TResponse>(response: Response): Promise<TResponse> | null;

    public async submit<TResponse extends {} | Blob>(url: string, params: IAjaxRequestParams): Promise<TResponse> {
        const tokenType = this.getAuthenticationTokenType();
        const token = this.getAuthenticationToken();

        const headers: { [key: string]: string; } = params?.headers ?? {};

        if (params.acceptContentType) {
            headers.Accept = params.acceptContentType;
        }

        if (params.contentType) {
            headers['Content-Type'] = params.contentType;
        }

        // Informs about ajax request
        headers['X-Requested-With'] = 'XMLHttpRequest';

        if (tokenType && token && !headers.Authorization) {
            headers.Authorization = `${ tokenType } ${ token }`;
        }
        headers.Accept = 'application/json';

        let requestBody: any = null;

        if (params.dataToSend instanceof FormData) {
            requestBody = params.dataToSend;
        } else if (params.dataToSend && params.contentType) {
            const dataFormatter = this.contentFormatterFactory.getFormatter(
                params.contentType
            );

            if (!dataFormatter) {
                return Promise.reject(
                    new Error(
                        `Can not submit request with body content of type "${ params.contentType }" because of parser was not found to convert content-object to string.`
                    )
                );
            }

            requestBody = dataFormatter.convertToString(params.dataToSend);
        }
        return fetch(url, {
            headers,
            method: params.method,
            mode: params.requestMode,
            body: requestBody,
            cache: params.requestCache,
            signal: params.abortSignal,
            credentials: !url.includes(import.meta.env.REACT_APP_MAIN_MS_HOST!) && !url.includes(import.meta.env.REACT_APP_MS_PROVIDER!) ? 'include' : undefined
        }).then<TResponse>(async response => {
            const rejectedPromise = this.GetRejectedPromiseFromResponse<TResponse>(response);
            if (rejectedPromise) {
                return rejectedPromise;
            }

            const responseContentType = (response.headers.get('content-type') ?? '').split(';')[0].trim();

            const isInvalidReceivedContentType = params.acceptContentType &&
                (!responseContentType || params.acceptContentType.indexOf(responseContentType) === -1);

            if (isInvalidReceivedContentType) {
                return Promise.reject(
                    new Error(
                        `Response from "${ url }" has unexpected "content-type", expected:"${ params.acceptContentType }", actual: "${ responseContentType }"`
                    )
                );
            }

            if (responseContentType !== ContentTypeEnum.Json) return await response.blob() as TResponse;
            const responseText = await response.text();
            if (!responseText || !responseContentType) {
                return (undefined as unknown) as TResponse;
            }
            const responseDataFormatter = this.contentFormatterFactory.getFormatter(
                responseContentType
            );

            if (!responseDataFormatter) {
                return Promise.reject(
                    new Error(
                        `Can not find parser of type "${ responseContentType }" to convert response to JSON.`
                    )
                );
            }

            return responseDataFormatter.stringToJson<TResponse>(responseText);
        }).catch((ex: unknown) => {
            if (ex instanceof TypeError) {
                if (url.includes('GetPermissionsForContext')) {
                    window.location.href = AppRoutes.serverError;
                }
            }

            return Promise.reject(ex);
        });
    }

    public postAjax<TResponse extends NonNullable<unknown> | Blob>(url: string, params?: IRequestParams<any>) {
        return this.submitWithPayload<TResponse>(WebVerb.POST, url, params);
    }

    public getAjax<TResponse extends NonNullable<unknown> | Blob>(url: string, params?: IRequestParams<any>) {
        return this.submitWithoutPayload<TResponse>(WebVerb.GET, url, params);
    }

    public putAjax<TResponse extends NonNullable<unknown> | Blob>(url: string, params?: IRequestParams<any>) {
        return this.submitWithPayload<TResponse>(WebVerb.PUT, url, params);
    }

    public deleteAjax<TResponse extends NonNullable<unknown> | Blob>(url: string, params?: IRequestParams<any>, payload?: boolean) {
        if (payload) return this.submitWithPayload<TResponse>(WebVerb.DELETE, url, params);
        return this.submitWithoutPayload<TResponse>(WebVerb.DELETE, url, params);
    }

    public patchAjax<TResponse extends NonNullable<unknown> | Blob>(url: string, params?: IRequestParams<any>) {
        return this.submitWithPayload<TResponse>(WebVerb.PATCH, url, params);
    }
}