/**
 * Enum типов контента
 */
export enum ContentTypeEnum {
    /**
     * Тип контента JSON
     */
    Json = 'application/json'
}