import { RequestKind, WebVerb } from './enums';
import { IAjaxSender, IFetchParams, IRequestParams, IRequestSender } from './interfaces';

/**
 * Represents web request sender
 */
export abstract class RequestSenderBase<TResponseObject> implements IRequestSender {
    private static hasRequestSentByUser = false;

    private static sentByUserUrl: string | null = null;

    private static _maxAsynchronousRequestsSentByUser = 10;

    private static _reservedAsynchronousRequestsSentByUser = 0;

    private ajaxSender: IAjaxSender;

    constructor(ajaxSender: IAjaxSender) {
        this.ajaxSender = ajaxSender;
        this.ajaxSubmitMethods = {
            [WebVerb.POST]: this.ajaxSender.postAjax,
            [WebVerb.GET]: this.ajaxSender.getAjax,
            [WebVerb.PUT]: this.ajaxSender.putAjax,
            [WebVerb.DELETE]: this.ajaxSender.deleteAjax,
            [WebVerb.PATCH]: this.ajaxSender.patchAjax

        };
    }

    private ajaxSubmitMethods: {
        [key in keyof typeof WebVerb]: (
            url: string,
            params?: IRequestParams<any>,
            payload?: boolean
        ) => Promise<any>;
    };

    private freeRequestsSentByUser(requestKind: RequestKind) {
        switch (requestKind) {
            case RequestKind.SEND_BY_USER_SYNCHRONOUSLY: {
                RequestSenderBase.hasRequestSentByUser = false;
                RequestSenderBase.sentByUserUrl = null;
                break;
            }

            case RequestKind.SEND_BY_USER_ASYNCHRONOUSLY: {
                if (RequestSenderBase._reservedAsynchronousRequestsSentByUser > 0) {
                    RequestSenderBase._reservedAsynchronousRequestsSentByUser -= 1;
                }
                break;
            }
            default:
                break;
        }
    }

    protected abstract handleResponse<TResult>(result: TResponseObject, method: WebVerb, requestUrl: string): Promise<TResult>;

    private tryPrepareRequestSentByUser<TResult>(requestUrl: string): Promise<TResult> | undefined {
        if (RequestSenderBase.hasRequestSentByUser) {
            const error = new Error(`User can send only one non asynchronous request.\n${ RequestSenderBase.sentByUserUrl }`);

            (console.error ?? console.log)(error.message);
            return Promise.reject(error);
        }

        RequestSenderBase.hasRequestSentByUser = true;
        RequestSenderBase.sentByUserUrl = requestUrl;
        return undefined;
    }

    private tryPrepareRequestSentByUserAsynchronously<TResult>(): Promise<TResult> | undefined {
        if (!RequestSenderBase._maxAsynchronousRequestsSentByUser) {
            const error = new Error('User can not send asynchronous requests.');
            (console.error ?? console.log)(error.message);
            return Promise.reject(error);
        }
        if (RequestSenderBase._maxAsynchronousRequestsSentByUser === RequestSenderBase._reservedAsynchronousRequestsSentByUser) {
            const error = new Error(`User can send only ${ RequestSenderBase._maxAsynchronousRequestsSentByUser } asynchronous request(s).`);
            (console.error ?? console.log)(error.message);
            return Promise.reject(error);
        }

        RequestSenderBase._reservedAsynchronousRequestsSentByUser += 1;
        return undefined;
    }

    private tryPrepareRequestWhenSentByUser<TResult>(requestKind: RequestKind, requestUrl: string): Promise<TResult> | undefined {
        switch (requestKind) {
            case RequestKind.SEND_BY_USER_SYNCHRONOUSLY: {
                const rejectedPromise = this.tryPrepareRequestSentByUser<TResult>(requestUrl);
                if (rejectedPromise) {
                    return rejectedPromise;
                }
                break;
            }

            case RequestKind.SEND_BY_USER_ASYNCHRONOUSLY: {
                const rejectedPromise = this.tryPrepareRequestSentByUserAsynchronously<TResult>();
                if (rejectedPromise) {
                    return rejectedPromise;
                }
                break;
            }
            default:
                break;
        }

        return undefined;
    }

    /**
     * Отправить Веб запрос
     * @param method Веб метод запроса
     * @param url Url адрес
     * @param args Параметры запроса
     * @param payload
     * @template TResult Тип ответа
     * @returns Promise с обьектом ответа запроса
     */
    public submitRequest<TResult>(method: WebVerb, url: string, args: IFetchParams<any>, payload?: boolean): Promise<TResult> {
        const rejectedPromise = this.tryPrepareRequestWhenSentByUser<TResult>(args.requestKind, url);
        if (rejectedPromise) {
            return rejectedPromise;
        }
        if (payload) {
            return this.ajaxSubmitMethods[method](url, args.requestArgs, payload)
                .then(result => {
                    return this.handleResponse<TResult>(result, method, url);
                })
                .finally(() => {
                    this.freeRequestsSentByUser(args.requestKind);
                });
        }
        return this.ajaxSubmitMethods[method](url, args.requestArgs)
            .then(result => {
                return this.handleResponse<TResult>(result, method, url);
            })
            .finally(() => {
                this.freeRequestsSentByUser(args.requestKind);
            });
    }

    /**
     * Отправить Веб запрос с файлами
     * @param url Url адрес
     * @param params Параметры запроса
     * @template TResult Тип ответа
     * @returns Promise с обьектом ответа запроса
     */
    public submitFiles<TResult>(url: string, params: IFetchParams<File[]>): Promise<TResult> {
        const formData = new FormData();

        if (!params.requestArgs || !params.requestArgs.dataToSend) {
            return Promise.reject(new Error('Can not upload file{s}, no one file has been passed.'));
        }

        for (const file of params.requestArgs.dataToSend) {
            formData.append('file', file, file.name);
        }

        return this.ajaxSubmitMethods[WebVerb.POST](url, {
            ...params,
            dataToSend: formData
        });
    }

    /**
     * Отправить веб запрос для получения обьекта Blob файла
     * @param method Веб метод запроса
     * @param url Url адрес
     * @param args Параметры запроса
     * @template TResult Тип ответа
     * @returns Promise с Blob обьектом ответа запроса
     */
    public submitRequestFile(method: WebVerb, url: string, args: IFetchParams<any>): Promise<Blob> {
        const rejectedPromise = this.tryPrepareRequestWhenSentByUser<Blob>(args.requestKind, url);
        if (rejectedPromise) {
            return rejectedPromise;
        }

        return this.ajaxSubmitMethods[method](url, args.requestArgs)
            .then(async result => {
                await this.handleResponse<Blob>(result, method, url);
                return result;
            }).finally(() => {
                this.freeRequestsSentByUser(args.requestKind);
            });
    }
}