export * from './IAjaxSender';
export * from './IRequestParams';
export * from './IAjaxRequestParams';
export * from './IRequestSender';
export * from './IFetchParams';