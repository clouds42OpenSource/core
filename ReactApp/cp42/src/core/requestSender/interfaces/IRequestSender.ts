import { IFetchParams } from '.';
import { WebVerb } from '../enums';

/**
 * Интерфейс представления отправки Веб запроса
 */
export interface IRequestSender {
    /**
     * Отправить Веб запрос
     * @param method Веб метод запроса
     * @param url Url адрес
     * @param args Параметры запроса
     * @template TResult Тип ответа
     * @returns Promise с обьектом ответа запроса
     */
    submitRequest: <TResult, TBodyParams = any>(method: WebVerb, url: string, args: IFetchParams<TBodyParams>, payload?: boolean)
        => Promise<TResult>;

    /**
     * Отправить Веб запрос с файлами
     * @param method Веб метод запроса
     * @param url Url адрес
     * @param args Параметры запроса
     * @template TResult Тип ответа
     * @returns Promise с обьектом ответа запроса
     */
    submitFiles: <TResult>(url: string, params: IFetchParams<File[]>)
        => Promise<TResult>;

    /**
     * Отправить веб запрос для получения обьекта Blob файла
     * @param method Веб метод запроса
     * @param url Url адрес
     * @param args Параметры запроса
     * @template TResult Тип ответа
     * @returns Promise с Blob обьектом ответа запроса
     */
    submitRequestFile: <TBodyParams = any>(method: WebVerb, url: string, args: IFetchParams<TBodyParams>)
        => Promise<Blob>;
}