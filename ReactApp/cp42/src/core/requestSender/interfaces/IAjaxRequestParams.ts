import { WebVerb } from 'core/requestSender/enums';
import { IRequestParams } from '.';

/**
 * Represents ajax request parameters
 */
export interface IAjaxRequestParams extends IRequestParams<any> {
  /**
   * Gets or sets web verb, such as POST, GET, PUT, DELETE
   */
  method: WebVerb;
}