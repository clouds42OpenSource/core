import { IRequestParams } from '.';
import { RequestKind } from '../enums';

/**
 * Represents Ajax fetch parameters
 * @template TDataToSend type of data to send
 */
export interface IFetchParams<TDataToSend> {
    /**
     * Gets or sets type of the request
     */
    requestKind: RequestKind;
    /**
     * Gets or sets request parameters
     */
    requestArgs?: IRequestParams<TDataToSend>;
}

export interface paramsObject {
    [key: string]: string | number | boolean | paramsObject | string[];
}