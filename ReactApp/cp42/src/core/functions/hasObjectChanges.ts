export const hasObjectChanges = (() => {
    function isClass<T>(value: T) {
        return typeof value === 'function' &&
            /^class\s/.test(Function.prototype.toString.call(value));
    }

    /**
     * Проверяет на соответствие двух объектов,
     * если в массиве двух объектов элементы идентичны,
     * а сами массивы нет, то считаются идентичными
     *
     * typeOf(1)                 => 'number'
     *
     * typeOf(new Number(1))     => 'number'
     *
     * typeOf(NaN)               => 'number'
     *
     * typeOf('1')               => 'string'
     *
     * typeOf(new String('1'))   => 'string'
     *
     * typeOf([])                => 'array'
     *
     * typeOf(new Array(0))      => 'array'
     *
     * typeOf(true)              => 'boolean'
     *
     * typeOf(new Boolean(true)) => 'boolean'
     *
     * typeOf(new Error())       => 'error'
     *
     * typeOf(new (class Er extends Error {})()) => 'error'
     *
     * typeOf(new Date())        => 'date'
     *
     * typeOf(class Test {})     => 'class'
     *
     * typeOf(/test/)            => 'regexp'
     *
     * typeOf({})                => 'object'
     *
     * typeOf(function(){})      => 'function'
     *
     * typeOf(undefined)         => 'undefined'
     *
     * typeOf(void 0)            => 'undefined'
     *
     * typeOf(null)              => 'null'
     */
    function typeOf<T>(value: T) {
        if (isClass(value)) {
            return 'class';
        }
        return Object.prototype.toString.call(value).slice(8, -1).trim().toLowerCase();
    }

    return function compare<TProps, TObjKeys extends keyof TProps>(
        oldProps: TProps, newProps: TProps, options?: {
            doNotCompare?: TObjKeys[];
            compareAsRef?: TObjKeys[];
            compareObjectLength?: boolean;
            doNotCompareFunc?: (newItem: object, oldItem: object) => boolean;
            compareAsRefFunc?: (item: object, oldItem: object) => boolean;
        }): boolean {
        if (typeOf(newProps) !== 'object') {
            if (oldProps !== newProps) {
                return true;
            }
        }

        if (oldProps === newProps) {
            return false;
        }

        if (options?.compareAsRefFunc && options.compareAsRefFunc(newProps as {}, oldProps as {})) {
            if (newProps !== oldProps) {
                return true;
            }
        }

        if ((newProps && !oldProps) ||
            (!newProps && oldProps)) {
            return true;
        }

        const newKeys = Object.keys(newProps as {}) as TObjKeys[];
        const oldKeys = Object.keys(oldProps as {}) as TObjKeys[];

        const compareObjectLength = options?.compareObjectLength ?? true;

        if (compareObjectLength && newKeys.length !== oldKeys.length) {
            return true;
        }

        const doNotCompare = ['__proto__', 'prototype', ...(options?.doNotCompare ?? [])];

        for (const key of newKeys) {
            if (doNotCompare.indexOf(key) >= 0) {
                continue;
            }

            if (((!!newProps[key]) && !oldProps[key]) || (!newProps[key] && (!!oldProps[key]))) {
                return true;
            }

            switch (typeOf(newProps[key])) {
                case 'date': {
                    if ((newProps[key] as Date).getTime() !== (oldProps[key] as Date).getTime()) {
                        return true;
                    }
                    break;
                }

                case 'array': {
                    if ((newProps[key] as Array<any>).length !== (oldProps[key] as Array<any>).length) {
                        return true;
                    }
                    for (let i = 0; i < (newProps[key] as Array<any>).length; i++) {
                        if (compare((oldProps[key] as Array<any>)[i], (newProps[key] as Array<any>)[i], options)) {
                            return true;
                        }
                    }
                    break;
                }

                case 'object': {
                    if ((options?.compareAsRef && options?.compareAsRef.indexOf(key) >= 0) ||
                        (options?.compareAsRefFunc && options.compareAsRefFunc(newProps[key] as {}, oldProps[key] as {}))) {
                        if (newProps[key] !== oldProps[key]) {
                            return true;
                        }
                    } else if (options?.doNotCompareFunc && options.doNotCompareFunc(newProps[key] as {}, oldProps[key] as {})) {
                        break;
                    } else if (compare(oldProps[key], newProps[key], options as {})) {
                        return true;
                    }
                    break;
                }

                default: {
                    if (newProps[key] !== oldProps[key]) {
                        return true;
                    }
                    break;
                }
            }
        }

        return false;
    };
})();