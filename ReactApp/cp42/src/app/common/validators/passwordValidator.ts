import { AppConsts } from 'app/common/constants';

export const passwordValidator = (password: string) => AppConsts.passwordRegex.test(password);