import { Validator } from 'app/common/types';

export type Int32ValidatorProps = {
    negativeNumbersCanBeEntered: boolean;
};
export function createInt32Validator(_props?: Int32ValidatorProps): Validator<string> {
    const validator: Validator<string> = {
        onChange: (value, _prevValue, _state?) => {
            value = value ? value.replace(/^0+/, '') : '';
            if (_props?.negativeNumbersCanBeEntered && value === '-') return;

            if (!value) return;

            const numberValue = parseInt(value, 10);
            if (numberValue.toString() === '0') {
                return;
            }

            if (Number.isNaN(numberValue) || numberValue.toString() !== value.replace(/^0+/, '')) return 'Можно вводить только цифры';

            let minAvailableValue = 1;
            let valueComparer = (numberValue < minAvailableValue || numberValue > 214748364);

            if (_props?.negativeNumbersCanBeEntered) {
                minAvailableValue = -214748364;
                valueComparer = (numberValue < minAvailableValue || numberValue > 214748364);
            }

            if (valueComparer) {
                return `Значение может находится в диапазоне от ${ minAvailableValue } до 214748364`;
            }
        },
        onBlur: (value, prevValue, state?) => {
            const error = validator.onChange(value, prevValue, state);
            if (error) { return error; }
        }
    };
    return validator;
}