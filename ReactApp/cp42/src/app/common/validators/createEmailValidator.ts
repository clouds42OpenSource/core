import { Validator } from 'app/common/types';

export function createEmailValidator(): Validator<string, string[]> {
    const finalPattern = /^[\w-.]+@[\w-.]+\.[a-zA-Z]+$/;
    const wrongSymbolsPattern = /(.*[^\w-.@].*)|(.*@.*@.*)/;

    const validator: Validator<string, string[]> = {
        onChange: (emailTag, _prevValue, _emails?) => {
            if (!emailTag) {
                return;
            }
            if (!emailTag || wrongSymbolsPattern.test(emailTag)) {
                return 'Не разрешается вводить этот символ для email адреса.';
            }
        },

        onBlur: (emailTag, prevValue, emails?) => {
            const error = validator.onChange(emailTag, prevValue, emails);
            if (error) { return error; }

            if (!emailTag || emailTag === prevValue) {
                return;
            }

            if (!emailTag || !finalPattern.test(emailTag)) {
                return `Email ${ emailTag } неверный.`;
            }

            if (emails && emails.findIndex(item => item.toLowerCase() === emailTag.toLowerCase()) >= 0) {
                return `Email ${ emailTag } уже добавлен.`;
            }
        }
    };

    return validator;
}