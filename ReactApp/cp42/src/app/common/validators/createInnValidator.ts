import { Validator } from 'app/common/types';

export function createInnValidator(): Validator<string> {
    const validator: Validator<string> = {
        onChange: (value, _prevValue, _state?) => {
            if (!value) {
                return;
            }

            const reg = /^\d*$/;
            if (!reg.test(value)) {
                return 'Можно вводить только цифры.';
            }
        },

        onBlur: (value, prevValue, state?) => {
            const error = validator.onChange(value, prevValue, state);
            if (error) { return error; }

            if (value === '') {
                return;
            }

            const reg = /^\d{10,12}$/;
            if (!reg.test(value)) {
                return `Неверный ИНН, должен быть от 10-и до 12-ти цифр, введено ${ value.length }.`;
            }
        }
    };

    return validator;
}