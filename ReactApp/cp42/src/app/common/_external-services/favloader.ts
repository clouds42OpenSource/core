declare global {
    interface Window {
        /**
         * Объект отрисовки изображений на табе браузера
         */
        favloader: IFavloader;
    }
}

/**
 * Тип favloader
 */
interface IFavloader {
    /**
     * Начать анимацию на табе браузера
     */
    start: () => void;

    /**
     * Закончить анимацию на табе браузера
     */
    stop: () => void;

    /**
     * Инициализировать анимацию
     */
    init: (options: Partial<IFavloaderOptions>) => void;
    /**
     * Версия
     */
    version: string;
}

/**
 * Настройки для favloader
 */
interface IFavloaderOptions {
    /**
     * Размер спинера на табе браузера, по умолчанию: 16x16
     */
    size: number;
    /**
     * Радиус спинера на табе браузера, по умолчанию: 6
     */
    radius: number;

    /**
     * Размер бордюра у спинера на табе браузера, по умолчанию: 2
     */
    thickness: number;
    /**
     * Цвет спинера на табе браузера, по умолчанию: #0F60A8
     */
    color: string;

    /**
     * Время прокрутки спинера на табе браузера, по умолчанию: 5000
     */
    duration: number;

    /**
     * URL к gif анимация как иконка на табе браузера
     */
    gif: string;

    /**
     * Функция для отрисовки собственной анимации на табе браузера
     */
    frame: (ctx: CanvasRenderingContext2D) => void;
}

/**
 * favloader объект
 */
export const FavLoader = {
    /**
     *
     * @returns Instance favloader
     */
    getInstance: () => window.favloader
};