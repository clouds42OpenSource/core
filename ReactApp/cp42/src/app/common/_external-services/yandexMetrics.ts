import { replaceWhenNotExist } from 'app/common/functions/replaceWhenNotExist';

declare global {
    interface Window {
        /**
         * Объект Яндекс метрик
         */
        'yaCounter9386863': IYandexCounter;
    }
}

/**
 * Метрики Яндекс
 */
export enum YandexMetrics {
    /**
     * Метрика удаления информационной базы
     */
    DeleteDatabaseMetric = 'DeleteIB',

    /**
     * Метрика отмены опубликованной информационной базы
     */
    UnpublishDatabaseMetric = 'CancelPublication',

    /**
     * Метрика поиска информационной базы
     */
    SearchUserAdminDb = 'SearchUserAdminDb',

    /**
     * Выбор периода
     */
    ChoosePeriod = 'choosePeriod',

    /**
     * Поиск аккаунтов
     */
    SearchUser = 'SearchUser',

    /**
     * Вход пользователя
     */
    SignInEnter = 'SignIn_Enter',

    /**
     * Вход пользователя
     */
    SignUpEnter = 'SignUp_Enter'
}

/**
 * Интерфейс объекта Яндекс метрики
 */
interface IYandexCounter {
    /**
     * Достичь цели по метрики
     * @param metric название метрики по которой достичь цель
     */
    reachGoal: (metric: YandexMetrics) => void;
}

/**
 * Фейковая Яндекс метрика, используется если по какой то причине скрипт Яндекс метрики не загружён
 */
const fakeYaCounter: IYandexCounter = {
    reachGoal: (metric: YandexMetrics) => {
        const consoleLog = console.warn ?? console.log;
        if (consoleLog) {
            consoleLog(`Не получилось зарегистрировать метрику "${metric}".\nЯндекс метрика не подгружена, возможно её блокирует adblock или другое расширение.`);
        }
    }
};

/**
 * Объект Яндекс метрик
 */
export const yandexCounter = {
    getInstance: () => replaceWhenNotExist(window.yaCounter9386863, fakeYaCounter)
};