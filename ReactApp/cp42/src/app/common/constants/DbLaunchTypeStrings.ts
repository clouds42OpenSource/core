/**
 * Содержит строки типов запуска базы данных
 */
export const DbLaunchTypeStrings = {
    /**
     * Тип запуска базы как: Тонкий клиент
     */
    dbLaunchTypeThin: 'Тонкий клиент',

    /**
     * Тип запуска базы как: Толстый клиент
     */
    dbLaunchTypeThick: 'Толстый клиент',

    /**
     * Тип запуска базы как: Тонкий клиент (WEB)
     */
    dbLaunchTypeThinWeb: 'Тонкий клиент (WEB)',

    /**
     * Тип запуска базы как: WEB (личный кабинет)
     */
    dbLaunchTypeWebClientCP: 'WEB (личный кабинет)',

    /**
     * Тип запуска базы как: WEB
     */
    dbLaunchTypeWebClient: 'WEB'
};