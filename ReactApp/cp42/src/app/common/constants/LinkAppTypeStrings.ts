import { LinkAppType } from 'app/common/enums/LinkAppType';

/**
 * Содержит строки типов Линка
 */
const LinkAppTypeStrings = {
    /**
     * Локальный Линк
     */
    [LinkAppType.ClientApp]: 'Локальный',

    /**
     * терминальный Линк
     */
    [LinkAppType.CloudApp]: 'Терминальный',

    /**
     * терминальный Линк как RemoteApp
     */
    [LinkAppType.RemoteApp]: 'Терминальный(RemoteApp)',
};

/**
 * Получить строку ассоциирующуюся с типом Линка
 * @param linkAppType Тип Линка
 * @param whenNotFound значение, если строчка с ререданным типом Линка не найдена
 * @returns Строку ассоциирующуюся с типом Линка или [whenNotFound], если не найдена.
 */
export function getLinkAppTypeString(linkAppType: LinkAppType, whenNotFound: string): string {
    return LinkAppTypeStrings[linkAppType as keyof typeof LinkAppTypeStrings] ?? whenNotFound;
}