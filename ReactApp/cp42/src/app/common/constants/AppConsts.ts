/**
 * Константы к приложению
 */
export const AppConsts = {
    /**
     * Ширина левого меню для Desktop разрешения
     */
    desktopLeftMenuWidth: 270,
    /**
     * Ширина левого меню для мобильного разрешения
     */
    mobileLeftMenuWidth: 0,
    /**
     * Ширина мобильного экрана
     */
    mobileScreenWidth: 800,
    /**
     * отступы контейнера страницы от LeftSideBar
     */
    contentContainerLeftMargin: 16,
    /**
     * Максимальная сумма для выставления счета на оплату при произвольной сумме
     */
    maxSummToPay: 99999999,
    /**
     * Максимальный размер чанка при отправке файла БД в мс
     */
    maxSizeChunkForBase: 30,
    /**
     * Регулярка для проверки валидности пароля
     */
    passwordRegex: /^([A-Za-z0-9!@#$^&*_+\-=[\];':"\\|,.<>/?]){8,25}$/
};

export const domain = window.location.host.match(/\.42clouds\.(.+)/)?.[0] ?? '';