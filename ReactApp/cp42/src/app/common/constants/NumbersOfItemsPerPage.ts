import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

/**
 * Константа с массивом кол-во записей на одну страницу
 */
export const NumbersOfItemsPerPage: ComboboxItemModel<string>[] = [
    { text: '30 записей', value: '30' },
    { text: '50 записей', value: '50' },
    { text: '100 записей', value: '100' },
    { text: '200 записей', value: '200' },
];