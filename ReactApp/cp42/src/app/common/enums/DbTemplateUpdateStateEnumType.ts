/**
 * Статус для обновленного шаблона инф. базы
 */
export enum DbTemplateUpdateStateEnumType {
    /**
     * Новое
     */
    New = 0,

    /**
     * Принятое
     */
    Apllied = 1,

    /**
     * Отклоненное
     */
    Rejected = 2
}