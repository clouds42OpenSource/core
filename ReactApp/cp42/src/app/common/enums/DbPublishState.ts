/**
 * Состояния публикации базы
 */
export enum DbPublishState {
    /**
     * База опубликована
     */
    Published = 0,

    /**
     * База в процессе публикации
     */
    PendingPublication = 1,

    /**
     * База в процессе снятия с публикации
     */
    PendingUnpublication = 2,

    /**
     * База не опубликована
     */
    Unpublished = 3,

    /**
     * База в процессе перезапуска пула
     */
    RestartingPool = 4
}