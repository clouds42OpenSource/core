export enum AccessStateEnum {
    done = 'Done',
    processing = 'Processing',
    error = 'Error'
}

export enum AccessLevelEnum {
    admin = 'ЗапускИАдминистрирование',
    launch = 'Запуск',
    noAccess = 'БезДоступа'
}