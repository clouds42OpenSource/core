/**
 * Состояния сервиса в информационной базе
 */
export enum ExtensionStateServicesEnum {
    /**
    * Расширение установленно
    */
    DoneInstall = 0,

    /**
    * Расширение удалено
    */
    DoneDelete = 1,

    /**
    * Расширение в процессе установки
    */
    ProcessingInstall = 2,

    /**
    * Расширение в процессе удаления
    */
    ProcessingDelete = 3,

    /**
    * Ошибка в процессе установки
    */
    ErrorProcessingInstall = 4,

    /**
    * Ошибка в процессе удаления
    */
    ErrorProcessingDelete = 5,

    /**
    * Сервис никогда не установлен
    */
    NotInstalled = 6

}