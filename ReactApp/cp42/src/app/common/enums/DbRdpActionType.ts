/**
 * Действие при логировании
 */
export enum DbRdpActionType {
    /**
     * Действие запустить базу
     */
    LaunchDatabase = 1,

    /**
     * Действие запустить RDP
     */
    OpenRdp = 2
}