/**
 * Модель восстановления инф. базы
 */
export enum AccountDatabaseRestoreModelType {
    /**
     * Простая
     */
    Simple,

    /**
     * Полная
     */
    Full,

    /**
     * Смешанная
     */
    Mixed
}