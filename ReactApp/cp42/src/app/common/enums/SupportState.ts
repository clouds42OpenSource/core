/**
 * Режимы подключения информационной базы к тех-подержке
 */
export enum SupportState {
    /**
     * Отсутствует авторизация.
     */
    NotAutorized = 0,

    /**
     * Идет процесс авторизации.
     */
    AutorizationProcessing = 1,

    /**
     * Авторизация успешно завершена.
     */
    AutorizationSuccess = 2,

    /**
     * Не верные авторизационные данные.
     */
    NotValidAuthData = 3,

    /**
     * Ошибка подключения.
     */
    AutorizationFail = 4
}