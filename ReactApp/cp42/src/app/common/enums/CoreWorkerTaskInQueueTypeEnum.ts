/**
 * Тип задачи в очереди воркеров
 */
export enum CoreWorkerTaskInQueueTypeEnum {
    /**
     * Внутренняя задача
     */
    InternalTask=0,

    /**
     * Регламентная задача
     */
    RegulationsTask=1
}

/**
 * Описание перечисления статусов задачи в очереди воркеров
 */
export const CoreWorkerTaskInQueueTypeLabels = {
    [CoreWorkerTaskInQueueTypeEnum.InternalTask]: 'Внутренняя задача',
    [CoreWorkerTaskInQueueTypeEnum.RegulationsTask]: 'Регламентная задача'
};