/**
 * Enum платформы
 */
export enum PlatformEnumType {
    /**
     * Не определено
     */
    Undefined = 0,

    /**
     * Платформа 8.2
     */
    V82 = 1,

    /**
     * Платформа 8.3
     */
    V83 = 2
}