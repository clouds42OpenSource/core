/**
 * Причина блокировки сервиса
 */
export enum ServiceLockReasonType {
    /**
    * Не оплачено
    */
    ServiceNotPaid = 0,
    /**
    * Не погашен обещанный платеж
    */
    OverduePromisedPayment = 1,

    /**
    * Недостаточно места на диске
    */
    noDiskSpace = 2
}