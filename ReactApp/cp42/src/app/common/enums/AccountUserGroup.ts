/**
 * Тип роли пользователя
 */
export enum AccountUserGroup {
    /**
     * Не определен
     */
    Undefined = 0,

    /**
     * Пользователь аккаунта
     */
    AccountUser = 1,

    /**
     * Менеджер аккаунта
     */
    AccountAdmin = 2,

    /**
     * Сейлc менеджер
     */
    AccountSaleManager = 3,

    /**
     * Оператор процес центра
     */
    ProcOperator = 4,

    /**
     * Хотлайн
     */
    Hotline = 5,

    /**
     * Администратор облака
     */
    CloudAdmin = 6,

    /**
     * Служба
     */
    Cloud42Service = 7,

    /**
     * Anonymous
     */
    Anonymous = 8,

    /**
     * Инженер облака
     */
    CloudSE = 9,

    /**
     * Внешняя служба
     */
    ExternalService = 10,

    ArticleEditor = 11,

}