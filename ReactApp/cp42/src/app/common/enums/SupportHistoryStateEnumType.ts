/**
 * Результат выполнения технической поддержки инф. базы
 */
export enum SupportHistoryStateEnumType {
    /**
     * Ошибка
     */
    Error = 0,

    /**
     * Успешное выполнение
     */
    Success = 1
}

/**
 * Описание Enum'а выполнения технической поддержки инф. базы
 */
export const SupportHistoryStateEnumTypeLabels = {
    [SupportHistoryStateEnumType.Error]: 'Ошибка',
    [SupportHistoryStateEnumType.Success]: 'Успех',
};