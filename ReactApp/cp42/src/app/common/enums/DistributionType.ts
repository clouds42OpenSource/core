/**
 * Тип распространения
 */
export enum DistributionType {
    /**
     * Стабильная версия
     */
    Stable = 0,

    /**
     * Альфа версия
     */
    Alpha = 1
}