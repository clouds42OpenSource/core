import { SortingKind } from 'app/common/enums/SortingKind';
import { generateReadOnlyKeyValueDataModelArrayFrom } from 'app/common/functions';
import { makeReadOnlyKeyValueDataModelArrayByValueComparator } from 'app/common/functions/makeReadOnlyKeyValueDataModelArrayByValueComparator';
import { StatusLabelTypes } from 'app/views/components/controls/Labels/views/StatusLabelView/types';

/**
 * Статус компоменты конечного автомата
 */
export enum StateMachineComponentStatus {
    /**
     * Признак, что любой статус
     */
    Any = -1,

    /**
     * В процессе работы
     */
    Processing = 0,

    /**
     * Завершен с ошибкой
     */
    Error = 1,

    /**
     * Завершен
     */
    Finish = 2,

    /**
     * В процоцессе откатывания
     */
    RollingBack = 3,

    /**
     * Завершено откатывание
     */
    RolledBack = 4
}

/**
 * Описание перечисления статусов компоменты конечного автомата
 */
const stateMachineComponentStatusDescription = generateReadOnlyKeyValueDataModelArrayFrom(StateMachineComponentStatus, enumValue => {
    switch (enumValue) {
        case StateMachineComponentStatus.Any:
            return 'Все';
        case StateMachineComponentStatus.Processing:
            return 'В процессе работы';
        case StateMachineComponentStatus.Error:
            return 'Завершен с ошибкой';
        case StateMachineComponentStatus.Finish:
            return 'Завершен';
        case StateMachineComponentStatus.RollingBack:
            return 'В процессе откатывания';
        case StateMachineComponentStatus.RolledBack:
            return 'Завершено откатывание';
        default:
            return '';
    }
}).sort(makeReadOnlyKeyValueDataModelArrayByValueComparator(SortingKind.Asc, StateMachineComponentStatus.Any));

/**
 * Карта маппинга статуса компоменты конечного автомата к типу лейбла
 */
const stateMachineComponentStatusLabelMapping = {
    [StateMachineComponentStatus.Any]: 'primary',
    [StateMachineComponentStatus.Processing]: 'warning',
    [StateMachineComponentStatus.Error]: 'error',
    [StateMachineComponentStatus.Finish]: 'success',
    [StateMachineComponentStatus.RollingBack]: 'secondary',
    [StateMachineComponentStatus.RolledBack]: 'secondary'
};

export const StateMachineComponentStatusExtensions = {
    getAllDescriptions: () =>
        stateMachineComponentStatusDescription,

    getDescription: (status: StateMachineComponentStatus) =>
        stateMachineComponentStatusDescription.find(item => item.key === status)?.value ?? '',

    getLabel: (status: StateMachineComponentStatus) =>
        stateMachineComponentStatusLabelMapping[status] as StatusLabelTypes
};