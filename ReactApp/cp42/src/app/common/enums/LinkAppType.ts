/**
 * Режимы линка
 */
export enum LinkAppType {
    /**
     * Неопределён
     */
    None = -1,

    /**
     * Клиентский Линк
     */
    ClientApp = 0,

    /**
     * Терминальный Линк
     */
    CloudApp = 1,

    /**
     * Терминальный Линк в режиме RemoteApp
     */
    RemoteApp = 2
}