/**
 * Тип восстановления информационной базы из бэкапа
 */
export enum AccountDatabaseBackupRestoreType {
    /**
    * Восстановить с заменой в текущую
    */
    ToCurrent = 0,

    /**
    * Восстановить в новую
    */
    ToNew = 1
}