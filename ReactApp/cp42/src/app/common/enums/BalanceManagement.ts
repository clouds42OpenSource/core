import { generateReadOnlyKeyValueDataModelArrayFrom, makeReadOnlyKeyValueDataModelArrayByValueComparator } from 'app/common/functions';

import { SortingKind } from 'app/common/enums';

/**
 * Фильтр Enum для поиска транзакций
 */
export enum TransactionsFiltertStatus {
     /**
     * Признак, все типы
     */
    AllServices = -1,

    /**
     * Аренда-1С
     */
    Rent1C = 2,

    /**
     * Мой-диск
     */
    MyDisk = 1,

    /**
     * Fasta
     */
    Fasta = 4,

    /**
     * Распознование документов
     */
    Documents = 3,

    /**
     * Мои информационные базы
     */
    MyDatabases = 5,

}

/**
 * Описание перечисления Фильтр Enum для поиска транзакций
 */
const TransactionsFiltertStatusStatusDescription = generateReadOnlyKeyValueDataModelArrayFrom(TransactionsFiltertStatus, enumValue => {
    switch (enumValue) {
        case TransactionsFiltertStatus.AllServices:
            return 'Все Сервисы';
        case TransactionsFiltertStatus.Documents:
            return 'Распознование документов';
        case TransactionsFiltertStatus.Fasta:
            return 'Fasta';
        case TransactionsFiltertStatus.MyDisk:
            return 'Мой диск';
        case TransactionsFiltertStatus.Rent1C:
            return 'Аренда 1С';
        case TransactionsFiltertStatus.MyDatabases:
            return 'Мои информационные базы';
        default:
            return '';
    }
}).sort(makeReadOnlyKeyValueDataModelArrayByValueComparator(SortingKind.Asc, TransactionsFiltertStatus.AllServices));

/**
 * Тип платежа при создании транзакции
 */
export enum PaymentType {
    /**
     * Входящий
     */
    Incomming = 2,
     /**
     * Исходящий
     */
    Outgoing = 1

}
/**
 * Описание перечисления типов платежа при создании транзакции
 */
const PaymentTypeDescription = generateReadOnlyKeyValueDataModelArrayFrom(PaymentType, enumValue => {
    switch (enumValue) {
        case PaymentType.Incomming:
            return 'Входящий';
        case PaymentType.Outgoing:
            return 'Исходящий';
        default:
            return '';
    }
});

/**
 * Тип транзакции
 */
export enum TransactionType {
    /**
     * Денежная
     */
    Money = 2,
    /**
     * Бонусная
     */
    Bonus = 1
}

/**
 * Описание перечисления типов транзакции
 */
const TransactionTypeDescription = generateReadOnlyKeyValueDataModelArrayFrom(TransactionType, enumValue => {
    switch (enumValue) {
        case TransactionType.Bonus:
            return 'Бонусная';
        case TransactionType.Money:
            return 'Денежная';
        default:
            return '';
    }
});

export const TransactionsFilterStatusExtensions = {
    getAllDescriptions: () =>
        TransactionsFiltertStatusStatusDescription,
    getTransactionType: () =>
        TransactionTypeDescription,
    getPaymentType: () =>
        PaymentTypeDescription,
};