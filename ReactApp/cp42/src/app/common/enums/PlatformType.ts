import { SortingKind } from 'app/common/enums/SortingKind';
import { generateReadOnlyKeyValueDataModelArrayFrom, makeReadOnlyKeyValueDataModelArrayByValueComparator } from 'app/common/functions';

/**
 * Тип платформы
 */
export enum PlatformType {
    /**
     * Не определено
     */
    Undefined = 0,

    /**
     * Платформа 8.2
     */
    V82 = 1,

    /**
     * Платформа 8.3
     */
    V83 = 2
}

/**
 * Описание перечисления статусов платформ
 */
const platformTypeDescriptions = generateReadOnlyKeyValueDataModelArrayFrom(PlatformType, enumValue => {
    switch (enumValue) {
        case PlatformType.Undefined:
            return 'Не определено';

        case PlatformType.V82:
            return 'Платформа 8.2';

        case PlatformType.V83:
            return 'Платформа 8.3';
        default:
            return '';
    }
}).sort(makeReadOnlyKeyValueDataModelArrayByValueComparator(SortingKind.Asc, PlatformType.Undefined));

/**
 * Текста перечисления статусов платформ
 */
const platformTypeTexts = generateReadOnlyKeyValueDataModelArrayFrom(PlatformType, enumValue => {
    switch (enumValue) {
        case PlatformType.Undefined:
            return '';

        case PlatformType.V82:
            return 'V82';

        case PlatformType.V83:
            return 'V83';
        default:
            return '';
    }
}).sort(makeReadOnlyKeyValueDataModelArrayByValueComparator(SortingKind.Asc, PlatformType.Undefined));

export const PlatformTypeDescriptionExtensions = {
    getAllDescriptions: () => {
        return platformTypeDescriptions;
    },
    getPlatformTypeText: (platform: PlatformType) => {
        return platformTypeTexts.find(item => item.key === platform)?.value ?? '';
    }
};

/**
 * Тип платформы
 */
export const PlatformTypeDescription = {
    [PlatformType.Undefined]: 'Не определено',
    [PlatformType.V82]: 'Платформа 8.2',
    [PlatformType.V83]: 'Платформа 8.3',
    [PlatformType[PlatformType.Undefined]]: 'Не определено',
    [PlatformType[PlatformType.V82]]: 'Платформа 8.2',
    [PlatformType[PlatformType.V83]]: 'Платформа 8.3'
};