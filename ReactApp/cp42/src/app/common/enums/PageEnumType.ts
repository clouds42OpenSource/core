/**
 * Тип страницы для динамического компонента редактирования/создания
 */
export enum PageEnumType {
    /**
     * Страница для редактирования
     */
    Edit,

    /**
     * Страница для создания
     */
    Create
}