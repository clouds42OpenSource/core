/**
 * Содержит варианты расположения NavMenu
 */
export enum NavMenuSide {
    /**
     * NavBarMenu рассположено слева
     */
    LeftSide,

    /**
     * NavBarMenu расположено сверху
     */
    TopSide
}