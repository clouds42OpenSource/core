/**
 * Статусы задачи в очереди воркеров
 */
export enum CoreWorkerTaskInQueueStatusType {
    /**
     * Новая
     */
    New = 0,

    /**
     * Захвачена воркером
     */
    Captured = 1,

    /**
     * В процессе выполнения
     */
    Processing = 2,

    /**
     * Готова
     */
    Ready = 3,

    /**
     * Ошибка
     */
    Error = 4,

    /**
     * Будет перезапущена
     */
    NeedRetry = 5
}

/**
 * Описание перечисления статусов задачи в очереди воркеров
 */
export const CoreWorkerTaskInQueueStatusLabels = {
    [CoreWorkerTaskInQueueStatusType.New]: 'Новая',
    [CoreWorkerTaskInQueueStatusType.Captured]: 'Захвачена воркером',
    [CoreWorkerTaskInQueueStatusType.Processing]: 'В процессе выполнения',
    [CoreWorkerTaskInQueueStatusType.Ready]: 'Готова',
    [CoreWorkerTaskInQueueStatusType.Error]: 'Ошибка',
    [CoreWorkerTaskInQueueStatusType.NeedRetry]: 'Будет перезапущена',
};

/**
 * Карта маппинга статуса задачи к текстовому описанию
 */
export const MapTaskStatusToDescription = {
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.New]]: 'Новая',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Captured]]: 'Захвачена воркером',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Error]]: 'Ошибка',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.NeedRetry]]: 'Будет перезапущена',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Processing]]: 'В процессе выполнения',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Ready]]: 'Готова',
};

/**
 * Карта маппинга статуса задачи к типу лейбла
 */
export const MapTaskStatusToLabelType = {
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.New]]: 'info',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Captured]]: 'warning',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Error]]: 'error',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.NeedRetry]]: 'info',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Processing]]: 'secondary',
    [CoreWorkerTaskInQueueStatusType[CoreWorkerTaskInQueueStatusType.Ready]]: 'success',
};