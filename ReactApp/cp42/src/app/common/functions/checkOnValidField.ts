import { IErrorsType } from 'app/common/interfaces';

/**
 * Функция на проверку обьязательного поля
 * @param errors Интерфейс для валидации ошибок
 * @param fieldName Название поля
 * @param fieldValue Значение поля
 * @param errorMessage Сообщение об ошибке
 */
function checkRequiredFieldError(errors: IErrorsType, fieldName: string, fieldValue: string, errorMessage?: string) {
    if (fieldValue || !errorMessage) {
        return true;
    }

    errors[fieldName] = errorMessage ?? '';

    return false;
}

/**
 * Функция на проверку максимальной длинны значения
 * @param isValid Поле обозначающий валидный или нет предыдущая проверка
 * @param errors Интерфейс для валидации ошибок
 * @param fieldName Название поля
 * @param fieldValue Значение поля
 * @param maxLength Максимальная длинна поля
 */
function checkMaxLengthError(isValid: boolean, errors: IErrorsType, fieldName: string, fieldValue: string, maxLength?: number) {
    if (!isValid || !maxLength) {
        return isValid;
    }

    if (!fieldValue) {
        return isValid;
    }

    if (fieldValue.length < maxLength) {
        return isValid;
    }

    errors[fieldName] = `Пожалуйста, введите не больше чем ${ maxLength } символов`;

    return false;
}

/**
 * Функция для проверки полей на обьязательность к заполнению и на максимальную длинну
 * @param errors Интерфейс для валидации ошибок
 * @param fieldName Название поля
 * @param fieldValue Значение поля
 * @param errorMessage Сообщение об ошибке
 * @param maxLength Максимальная длинна поля
 * @returns True валидно, false не валидный
 */
export function checkOnValidField(errors: IErrorsType, fieldName: string, fieldValue: string, errorMessage?: string, maxLength?: number) {
    let isValid: boolean;
    fieldValue = fieldValue?.trim();
    isValid = checkRequiredFieldError(errors, fieldName, fieldValue, errorMessage);
    isValid = checkMaxLengthError(isValid, errors, fieldName, fieldValue, maxLength);

    return isValid;
}