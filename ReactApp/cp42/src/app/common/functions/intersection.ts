/**
 * Вернуть пересекающиеся элементы 2 списков
 * @param firstList первый список
 * @param secondList второй список
 */
export function intersection<TObj>(firstList: TObj[], secondList: TObj[]) {
    return firstList.filter(item => secondList.indexOf(item) !== -1);
}