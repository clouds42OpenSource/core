import { DateUtility } from 'app/utils';
import { Nullable } from 'app/common/types';

/**
 * Скоректировать перод времени фильтра
 * @param filterDatePeriod текущее значение периода в фильтре
 * @param isEndPeriod признак что это конечный период
 */
export function adjustFilterDatePeriod(filterDatePeriod:Nullable<Date>, isEndPeriod: boolean): Nullable<Date> {
    if (!filterDatePeriod) return filterDatePeriod;

    const newDate = new Date(filterDatePeriod as unknown as Date);

    let newFilterDatePeriod = DateUtility.setDayTimeToMidnight(newDate);

    if (isEndPeriod) {
        newFilterDatePeriod = DateUtility.setDayEndTime(newDate);
    }

    return newFilterDatePeriod;
}