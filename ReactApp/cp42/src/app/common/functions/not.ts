/**
 * Вернуть элементы первого списка, которых нет во втором
 * @param firstList первый список
 * @param secondList второй список
 */
export function not<TObj>(firstList: TObj[], secondList: TObj[]) {
    return firstList.filter(value => secondList.indexOf(value) === -1);
}