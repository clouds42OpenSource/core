/**
 * Функция на проверку обьязательного поля
 * @param newValue Новое значение при изменении поля
 * @param emptyMessage Сообщение если новое значение пустое
 */
function checkOnRequiredFieldError(newValue: string, emptyMessage?: string) {
    if (newValue || !emptyMessage) return '';
    return emptyMessage!;
}

/**
 * Функция на проверку максимальной длинны значения
 * @param errorText Текст ошибки предыдущей проверки
 * @param newValue Новое значение при изменении поля
 * @param maxLength Параметр на максимальную длинну значения поля
 */
function checkOnMaxLengthFieldError(errorText: string, newValue: string, maxLength?: number) {
    if (errorText || !maxLength) return errorText;
    if (!newValue) return errorText;
    if (newValue.length < maxLength) return errorText;
    return `Пожалуйста, введите не больше чем ${ maxLength } символов`;
}

/**
 * Функция на проверку есть ли значение в полях и на длинну значения поля
 * @param newValue Новое значение при изменении поля
 * @param emptyMessage Сообщение если новое значение пустое
 * @param maxLength Не обязательный параметр на максимальную длинну значения поля
 * @returns Текст ошибки
 */
export function checkValidOfStringField(newValue: string, emptyMessage?: string, maxLength?: number) {
    let errorText = '';
    newValue = newValue?.trim();
    errorText = checkOnRequiredFieldError(newValue, emptyMessage);
    errorText = checkOnMaxLengthFieldError(errorText, newValue, maxLength);
    return errorText;
}