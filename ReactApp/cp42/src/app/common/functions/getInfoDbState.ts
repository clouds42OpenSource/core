import { DatabaseState, DbPublishState } from 'app/common/enums';

export function getInfoDbState(value: DatabaseState, publish?: DbPublishState) {
    if (publish === DbPublishState.PendingPublication && value === DatabaseState.Ready) {
        return 'Обрабатывается публикация';
    }

    if (publish === DbPublishState.PendingUnpublication && value === DatabaseState.Ready) {
        return 'Обрабатывается снятие публикации';
    }

    switch (value) {
        case DatabaseState.Undefined:
            return 'База в неизвестном состоянии';
        case DatabaseState.NewItem:
            return 'База в процессе создания';
        case DatabaseState.Ready:
            return '';
        case DatabaseState.Unknown:
            return 'Статус базы не определен';
        case DatabaseState.Detached:
            return 'База отправлена в хранилище';
        case DatabaseState.Attaching:
            return 'База в процессе восстановления из архива';
        case DatabaseState.ErrorNotEnoughSpace:
            return 'База не создана, недостаточно места на диске';
        case DatabaseState.ErrorCreate:
            return 'База не создана, ошибка создания';
        case DatabaseState.ErrorDtFormat:
            return 'База не создана, ошибка загрузки из dt';
        case DatabaseState.ProcessingSupport:
            return 'Идет выполнение регламентных операций.  Это может занять некоторое время.';
        case DatabaseState.TransferDb:
            return 'База в состоянии переноса';
        case DatabaseState.TransferArchive:
            return 'Архив базы в состоянии переноса';
        case DatabaseState.RestoringFromTomb:
            return 'База в очереди на восстановление';
        case DatabaseState.DelitingToTomb:
            return 'База в очереди на удаление в склеп';
        case DatabaseState.DetachingToTomb:
            return 'База в очереди на архивацию';
        case DatabaseState.DeletedToTomb:
            return 'База удалена в склеп';
        case DatabaseState.DetachedToTomb:
            return 'База в архиве';
        case DatabaseState.DeletedFromCloud:
            return 'База удалена с облака';
        default:
            return '';
    }
}