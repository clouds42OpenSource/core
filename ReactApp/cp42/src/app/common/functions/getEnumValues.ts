import { Enum } from 'app/common/types';

/**
 * Возвращает массив значений `enum` объекта
 * @param enumObject `enum` объект
 * @returns если `enumObject` объект не определён, то возвращается пустой массив `[]`, иначе массив значений переданного `enumObject` объекта
 */
export function getEnumValues<TEnum>(enumObject: Enum<TEnum>): Array<TEnum> {
    return enumObject
        ? Object
            .keys(enumObject)
            .filter(enumKey => Number.isNaN(Number(enumKey)))
            .map(enumKey => enumObject[enumKey]) as Array<TEnum>
        : [];
}