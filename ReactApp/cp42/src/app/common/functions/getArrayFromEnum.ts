export const getArrayFromEnum = (value: any, number?: boolean): any[] => {
    return Object.keys(value).map(type => {
        return number ? +value[type] : value[type];
    });
};