import { Nullable } from 'app/common/types';
import { DateUtility } from 'app/utils';

/**
 * Получить формат для поля даты в таблице
 * @param date Дата
 * @returns Дату по формату
 */
export function getCommonDateFormat(date: Nullable<Date>) {
    return DateUtility.getDateByFormat(date, 'dd.MM.yyyy HH:mm');
}