import { SortingKind } from 'app/common/enums/SortingKind';
import { ReadOnlyKeyValueDataModel } from 'app/web/common/data-models';

export function makeReadOnlyKeyValueDataModelArrayByValueComparator(sortKind: SortingKind, makeFirstWithKey?: string | number) {
    return <TEnumKey>(a: ReadOnlyKeyValueDataModel<TEnumKey, string>, b: ReadOnlyKeyValueDataModel<TEnumKey, string>) => {
        if (makeFirstWithKey !== void 0 && b.key === makeFirstWithKey as unknown as TEnumKey) {
            return 2;
        }

        if (sortKind !== void 0) {
            if (a.value > b.value) {
                return sortKind === SortingKind.Asc ? 1 : -1;
            }
            if (b.value > a.value) {
                return sortKind === SortingKind.Asc ? -1 : 1;
            }
        }
        return 0;
    };
}