import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { KeyValueDataModel } from 'app/web/common/data-models';

/**
 * Получить список опции для выпадающего списка из описания енума
 * @param obj описание енума
 * @param includeFirstItem первый элемент списка
 * @returns Список опции для выпадающего списка из описания енума
 */
export function getSelectOptionsFromEnumLabels<TEnum extends { [key: string]: any }>(obj: TEnum, includeFirstItem?: ComboboxItemModel<string>): ComboboxItemModel<string>[] {
    const options = (Object.keys(obj)).map(key => {
        return {
            value: key,
            text: obj[key]
        };
    });
    if (includeFirstItem) options.unshift(includeFirstItem);

    return options;
}

/**
 * Получить список опции для выпадающего списка из массива KeyValueDataModel<TKey, string>
 * @param keyValueObj массив KeyValueDataModel<TKey, string>
 * первый элемент результирующего списка
 * @returns Список опций для выпадающего списка из массива KeyValueDataModel<TKey, string>
 */
export function getComboBoxFormOptionsFromKeyValueDataModel<TKey extends(string | number), TObject extends KeyValueDataModel<TKey, string>[]>(keyValueObj: TObject): ComboboxItemModel<TKey>[] {
    return keyValueObj?.map(item => {
        return {
            value: item.key,
            text: item.value
        };
    }) ?? [];
}