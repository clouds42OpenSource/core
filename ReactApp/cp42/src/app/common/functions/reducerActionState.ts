import { reducerStateProcessFail, reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { reducerStateProcessReset } from 'core/redux/functions/reducerStateProcessReset';
import { IReducerAction, IReducerState, ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { ProcessIdType, TReducerActionAllPayload } from 'core/redux/types';

type ReducerActionStateParams<TReducerState extends IReducerState<ProcessIdType>, TActionPayload extends TReducerActionAllPayload> = {
    /**
     * Состояние редюсера
     */
    state: TReducerState;

    /**
     * Действие над редюсером
     */
    action: IReducerAction<TActionPayload>;

    /**
     * Набор action
     */
    actions: {
        START_ACTION: string,
        SUCCESS_ACTION: string,
        FAILED_ACTION: string,
        RESET_ACTION: string
    };

    /**
     * Процесс action
     */
    processId: string;

    /**
     * Функция на успешный action тип
     */
    onSuccessProcessState?: () => TReducerState;

    /**
     * Функция для старта action типа
     */
    onStartProcessState?: () => TReducerState;
}

/**
 * Функция для Reducer на обработку действия и возвращения нового состояния
 * @param params Параметры редюсера
 * @returns Возвращает State
 */
export function reducerActionState<TReducerState extends IReducerState<ProcessIdType>, TActionPayload extends TReducerActionAllPayload>(params: ReducerActionStateParams<TReducerState, TActionPayload>) {
    const { action, actions, processId, state, onSuccessProcessState, onStartProcessState } = params;
    const { START_ACTION, SUCCESS_ACTION, FAILED_ACTION, RESET_ACTION } = actions;

    switch (action.type) {
        case START_ACTION: {
            const payload = action.payload as ReducerActionStartPayload;
            return onStartProcessState ? onStartProcessState() : reducerStateProcessStart(state, processId, payload);
        }

        case SUCCESS_ACTION: {
            const payload = action.payload as ReducerActionSuccessPayload;
            return onSuccessProcessState ? onSuccessProcessState() : reducerStateProcessSuccess(state, processId, payload);
        }

        case FAILED_ACTION: {
            const payload = action.payload as ReducerActionFailedPayload;
            return reducerStateProcessFail(state, processId, payload.error);
        }

        case RESET_ACTION: {
            return reducerStateProcessReset(state, processId);
        }

        default:
            return null;
    }
}