/**
 * Свойства объекта
 */
type TObjProps = {
    [property: string]: string | number | boolean | Date | undefined | object | null;
};

/**
 * Эмуляция заполнения полей объекта
 * @param obj Объект для которого выполнить эмуляцию заполнения
 * @returns Эмулированный с заполненными полями объект
 */
export function partialFillOf<TObj extends TObjProps>(obj?: Partial<TObj>): TObj {
    return { ...obj } as TObj;
}