import { PaginationDataModel } from 'app/web/common/data-models';

/**
 * Возвращает начальные значения по информации о страницах
 */
export function getInitialPagination(): PaginationDataModel {
    return {
        firstNumber: 1,
        lastNumber: 1,
        pageCount: 1,
        pageSize: 0,
        totalCount: 0,
        currentPage: 1
    };
}