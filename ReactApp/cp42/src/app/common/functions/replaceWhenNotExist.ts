/**
 * Возвращает первый элемент, если он не `null` и не `undefined`, иначе второй
 * @param checkItem Проверяемый объект на `null` и `undefined`
 * @param replaceWith если `checkItem` = `null` или `undefined`, то  возвращается `replaceWith`, иначе `checkItem`
 */
export function replaceWhenNotExist<T, T2 extends T>(checkItem: T, replaceWith: T2) {
    return checkItem === void 0 || checkItem === null
        ? replaceWith
        : checkItem;
}