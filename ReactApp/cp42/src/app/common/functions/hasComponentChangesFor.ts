import { hasObjectChanges } from 'core/functions';
import React from 'react';

export function hasComponentChangesFor<TProps, TObjKeys extends keyof TProps>(
    oldProps: TProps, newProps: TProps, options?: {
        doNotCompare?: TObjKeys[];
        compareAsRef?: TObjKeys[];
        compareObjectLength?: boolean;
    }): boolean {
    return hasObjectChanges(oldProps, newProps, {
        compareAsRef: options?.compareAsRef,
        doNotCompare: options?.doNotCompare,
        compareObjectLength: options?.compareObjectLength,
        compareAsRefFunc: (item: object) => {
            return React.isValidElement(item);
        }
    });
}