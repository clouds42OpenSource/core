import { MetadataModel } from 'app/web/common/data-models';

/**
 * Возвращает начальные значения по информации о страницах
 */
export function getInitialMetadata(): MetadataModel {
    return {
        firstItemOnPage: 1,
        hasNextPage: false,
        hasPreviousPage: false,
        isFirstPage: false,
        isLastPage: false,
        lastItemOnPage: 1,
        pageCount: 1,
        pageNumber: 1,
        pageSize: 10,
        totalItemCount: 1
    };
}