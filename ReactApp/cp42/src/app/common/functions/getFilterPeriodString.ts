import { DateUtility } from 'app/utils';
import { Nullable } from 'app/common/types';

/**
 * Получить строку даты для отправки в фильтре
 * @param currentFilterValue текущее значение фильтра по дате
 */
export function getFilterPeriodString(currentFilterValue: Nullable<Date>): Nullable<string> {
    if (!currentFilterValue) return null;
    const newFilterDate = new Date(currentFilterValue);

    return DateUtility.dateToIsoDateString(newFilterDate);
}