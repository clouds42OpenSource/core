export function compareArray(array1: string[], array2: string[]) {
    return JSON.stringify([...array1].sort()) !== JSON.stringify([...array2].sort());
}