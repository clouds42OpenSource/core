import { TAccountProfile } from 'app/api/endpoints/accountUsers/response';
import { getEmptyUuid } from 'app/common/helpers';
import { DropDownListWithCheckboxItem } from 'app/views/components/controls/forms/DropDownListWithCheckboxes/types';
import { ProfilesModel } from 'app/web/api/MsBackups/request-dto';

const EMPTY_UUID = getEmptyUuid();

type TRole = Omit<TAccountProfile, 'databases'>;

export const configurationProfilesToRole = (configurationProfiles: ProfilesModel[], items: DropDownListWithCheckboxItem[]) => {
    return configurationProfiles.reduce<{userRole: TRole[], adminRole: TRole[]}>((profiles, profile) => {
        if (items.find(item => item.value === EMPTY_UUID ? item.text === profile.name : item.value === profile.id)) {
            if (profile.admin) {
                profiles.adminRole.push(profile);
            } else {
                profiles.userRole.push(profile);
            }
        }

        return profiles;
    }, { adminRole: [], userRole: [] });
};