import { getEnumValues } from 'app/common/functions/getEnumValues';
import { Enum } from 'app/common/types';
import { ReadOnlyKeyValueDataModel } from 'app/web/common/data-models';

/**
 * Генерирует Массив объектов ReadOnlyKeyValueDataModel<TEnum, string> из передпнного объекта enum-а
 * @param enumObject Объект enum-а
 * @param getTextFor функция, которая возвращает строку на переданное значение enum-а, если возвращается undefined, то данное значение игнорируется
 * @returns Массив объектов ReadOnlyKeyValueDataModel<T, string>
 */
export function generateReadOnlyKeyValueDataModelArrayFrom<TEnum>(enumObject: Enum<TEnum>, getTextFor: (enumValue: TEnum) => string): ReadOnlyKeyValueDataModel<TEnum, string>[] {
    const enumValues = getEnumValues(enumObject);

    return enumValues.map(enumValue => {
        return {
            key: enumValue,
            value: getTextFor(enumValue) ?? ''
        };
    });
}