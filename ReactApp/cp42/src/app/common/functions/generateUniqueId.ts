export function generateUniqueId(prefix?: string): string {
    return `${ prefix ?? 'auto_generated_' }${ (Date.now() + Math.random()).toString().replace(',', '').replace('.', '') }`;
}