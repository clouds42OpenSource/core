import React from 'react';

/**
 * Проставляет объекты для всех переданных ссылок
 * @param `refs` ссылки на объекты к которым нужно присвоить эти объекты
 * @returns Полученные ссылки
 */
export function mergeRefs<TElement>(...refs: (React.Ref<TElement> | undefined)[]) {
    const filteredRefs = refs.filter(Boolean);

    if (!filteredRefs.length) {
        return null;
    }

    if (filteredRefs.length === 1) {
        return filteredRefs[0];
    }

    return (instance: TElement) => {
        for (const ref of filteredRefs) {
            if (typeof ref === 'function') {
                ref(instance);
            } else if (ref) {
                (ref as any).current = instance;
            }
        }
    };
}