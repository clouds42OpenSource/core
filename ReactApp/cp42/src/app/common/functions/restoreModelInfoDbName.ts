import { keyValueLowerType } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/keyValueLowerType';

export function restoreModelInfoDbName(id: number, arrModelType: keyValueLowerType[]): string {
    return arrModelType[id]?.value || '';
}