export type ValidationEvent = 'onChange' | 'onBlur';
export type Validator<TValue, TState = any> = { [key in ValidationEvent]: (value: TValue, prevValue: TValue, state?: TState) => string | string[] | null | undefined };
export type Nullable<T> = null | T;

/**
 * Представляет тип enum объекта
 * @template TEnum Тип enum
 */
export type Enum<TEnum> = {
    /**
     * @param enumKey ключ enum-а
     * @returns возвращает либо enum, либо строку
     */
    [enumKey: string]: TEnum | string;
    /**
     * @param enumValue значение enum-а
     * @returns возвращает ключ enum-а
     */
    [enumValue: number]: string;
};

/**
 * Тип для форматирования даты
 */
export type DateUtilityFormatType = 'dd.MM.yyyy' | 'dd.MM.yyyy HH:mm' | 'dd.MM.yyyy HH:mm:ss' | 'HH:mm' | 'HH:mm:ss';