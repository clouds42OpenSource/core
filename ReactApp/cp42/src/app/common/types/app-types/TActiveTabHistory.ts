export type TActiveTabHistory = undefined | {
    activeTabIndex?: number;
};