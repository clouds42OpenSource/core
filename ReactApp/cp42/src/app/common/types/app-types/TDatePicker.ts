export type TDatePicker = {
    from: Date | null;
    to: Date | null;
};