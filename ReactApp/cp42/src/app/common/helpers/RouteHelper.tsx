import { useAppSelector } from 'app/hooks';
import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { AccountUserGroup } from 'app/common/enums';
import { AppRoutes } from 'app/AppRoutes';

type TProps = {
    path?: string;
    component?: RouteProps['component'];
    rules?: AccountUserGroup[];
    exact: boolean;
    conditions?: boolean;
};

export const RouteHelper = ({ path, rules, exact, component, conditions = true }: TProps) => {
    const { hasSuccessFor: { hasSettingsReceived }, settings: { currentContextInfo: { currentUserGroups } } } = useAppSelector(state => state.Global.getCurrentSessionSettingsReducer);

    if (hasSettingsReceived) {
        if ((rules ? currentUserGroups.some(item => rules.includes(item)) : true) && conditions) {
            return (
                <Route
                    exact={ exact }
                    path={ path }
                    component={ component }
                />
            );
        }
        return (
            <Redirect to={ AppRoutes.accessError } />
        );
    }

    return <Route exact={ exact } path={ path } />;
};