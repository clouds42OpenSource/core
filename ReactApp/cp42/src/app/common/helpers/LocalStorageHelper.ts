import { Credentials } from 'app/api/endpoints/global/enum';
import { domain } from 'app/common/constants';
import Cookies from 'js-cookie';

export const localStorageHelper = {
    /**
     * Получение Id текущего пользователя
     * @returns Id текущего пользователя
     */
    getUserId: () => window.localStorage.getItem(Credentials.UserID),
    /**
     * Получение Id аккаунта
     * @returns Id аккаунта
     */
    getAccountId: () => window.localStorage.getItem(Credentials.AccountID),
    /**
     * Получение JWT токена
     * @returns JWT токен
     */
    getJWTToken: () => Cookies.get(Credentials.JsonWebToken) ?? '',
    /**
     * Получение Context AccountId
     * @returns Context AccountId
     */
    getContextAccountId: () => window.localStorage.getItem(Credentials.ContextAccountId),
    /**
     * Получение время истечения срока токена
     * @returns время истечения срока токена
     */
    getTokenExpireDate: () => parseInt(Cookies.get(Credentials.ExpiresIn) ?? '0', 10),
    /**
     * Получение токена обновления
     * @returns токена обновления
     */
    getRefreshToken: () => Cookies.get(Credentials.RefreshToken) ?? '',
    /**
     * Добавление элементов в local storage
     * @param key ключ
     * @param value значение
     */
    setItem: (key: string, value: string) => {
        window.localStorage.setItem(key, value);
    },
    /**
     * Удаление элемента с local storage пары ключ: значение по ключу
     * @param key ключ
     */
    removeItem: (key: string) => {
        window.localStorage.removeItem(key);
    },
    removeAllUserItems: () => {
        Object.values(Credentials).forEach(credential => {
            localStorageHelper.removeItem(credential);
        });
        Cookies.remove('.AspNetCore.Cookies', { domain, path: '/' });
        Cookies.remove(Credentials.JsonWebToken, { domain, expires: 1, path: '/', secure: true });
        Cookies.remove(Credentials.RefreshToken, { domain, expires: 1, path: '/', secure: true });
        Cookies.remove(Credentials.ExpiresIn, { domain, expires: 1, path: '/', secure: true });
        Cookies.remove('SelectedContextAccount', { domain, expires: 1, secure: true });
    }
};