import { IReducerActionInfo } from 'core/redux/interfaces';
import { ProcessIdType } from 'core/redux/types';

/**
 * Тип дейсвий состояния редюсера
 */
type ReducerActionsType<TProcessId extends ProcessIdType> = {
    /**
     * Ключ - ID процесса
     * Значение информация о процессе для ID процесса
     */
    [key in TProcessId]: IReducerActionInfo<TProcessId>
};

/**
 * Помошник по определению состочния для переданного ID процесса
 */
export const ProcessIdStateHelper = {
    /**
     * @param reducerActions Проверяет, находится ли сейчас проверяемый ID процесса в состоянии загрузки
     * @param processIdToCheck  ID процесса для проверки
     */
    isInProgress: <TProcessId extends ProcessIdType>(reducerActions: ReducerActionsType<TProcessId>, processIdToCheck: TProcessId) => {
        return !!reducerActions && !!reducerActions[processIdToCheck] &&
            reducerActions[processIdToCheck].hasProcessActionStateChanged &&
            reducerActions[processIdToCheck].processActionState.isInProgressState;
    }
};