import dayjs from 'dayjs';

export const setDateFormat = (value: string | null, defaultValue = '---') => (value ? dayjs(value).format('DD.MM.YYYY') : defaultValue);