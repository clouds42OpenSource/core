export const EnvironmentHelper = {
    isBetaOrProd: () => import.meta.env.MODE === 'production' || import.meta.env.MODE === 'beta',
};