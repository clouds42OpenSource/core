/**
 * Хелпер для работы с setTimeOut, setInterval и тд.
 */
export const TimerHelper = {
    /**
     * Время ожидания в миллисекундах перед наступлением события ValueApplied
     */
    beforeValueAppliedTimeoutInMilliseconds: 700,
    /**
     * Время ожидания в миллисекундах перед наступлением события validateAsyncUserFields
     */
    _waitTimeBeforeGetAsyncAccessStatus: 20000,
    /**
     * Время ожидания в миллисекундах перед наступлением события validateAsyncUserFields
     */
    waitTimeRefreshData: 15000,
    /**
     * Время ожидания в миллисекундах для обновления информации о базах в личных сервисах
     */
    waitTimeForRefreshDatabaseInformation: 30000,
    waitTimeForCheckTokenRefresh: 60000,
    waitTimeRefreshDatabaseState: 30000
};