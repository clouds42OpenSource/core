import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

export const isTimeToRefreshToken = () => {
    const expiredTime = localStorageHelper.getTokenExpireDate();

    return expiredTime < Math.round(new Date().getTime() / 1000) + 300;
};