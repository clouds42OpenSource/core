import '@testing-library/jest-dom/extend-expect';
import uuid from './uuid';

describe('UUID', () => {
    const regexString = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;

    it('returns UUID string', () => {
        expect(uuid()).toMatch(regexString);
    });
});