/**
 * Интерфейс для валидации ошибок
 */
export interface IErrorsType {
    /**
     * Сообщения об ошибках проверки для каждого поля (key - это имя поля)
     */
    [key: string]: string;
}