import { IocRegisteredObject } from 'app/IoCСontainer/enums';
import { ApiProxy } from 'app/web/api';
import { AjaxSender } from 'app/web/request-submitter/common/AjaxSender';
import { RequestSender } from 'app/web/request-submitter/common/RequestSender';
import initDIContainer, { InjectScope } from 'core/IoCContainer';
import { JSONFormatter, WebContentFormatterFactory } from 'core/contentFormatter';
import { IWebContentFormatterFactory } from 'core/contentFormatter/interfaces';
import { IAjaxSender, IRequestSender } from 'core/requestSender/interfaces';

const appIocContainer = initDIContainer<IocRegisteredObject>();

/**
 * Создать API хост изходя из текущего хоста
 * @returns API хост
 */
const createApiHost = () => {
    const currentHost = window.location.hostname;
    const hostParts = currentHost.split('.');
    const cpPart = hostParts[0].toLowerCase();
    const cpPrefix = cpPart.replace('cp', '');
    return `https://${ cpPrefix }core.42clouds.com`;
};

/**
 * Создать UploadFile хост исходя из текущего хоста
 * @returns uploadFile хост
 */
const createUploadFileHost = () => {
    const currentHost = window.location.hostname;
    const hostParts = currentHost.split('.');
    const uploadPart = hostParts[0].toLowerCase();
    const uploadPrefix = uploadPart.replace('cp', '');
    return `https://${ uploadPrefix }uploadfile.42clouds.com`;
};

/**
 * Получить MS хост изходя из текущего хоста
 * @returns MS хост
 */
const getMainMsHost = () => {
    const currentHost = window.location.hostname;
    const hostParts = currentHost.split('.');
    const cpPart = hostParts[0].toLowerCase();
    let prefix: string;

    switch (cpPart) {
        case 'cp':
            prefix = 'ms';
            break;
        case 'betacp':
            prefix = 'beta-ms';
            break;
        default:
            prefix = 'area-dev-02';
    }

    return `https://${ prefix }.42clouds.com`;
};

/**
 * @deprecated Since 16.01.2023
 * @see {@link getMainMsHost}
 * @returns MS host
 */
const getMSHost = () => {
    return import.meta.env.REACT_APP_MS_HOST ?? 'https://beta-ms.42clouds.com/pl42/hs/';
};

appIocContainer.register(InjectScope.SINGLETON, IocRegisteredObject.CONTENT_FORMATTER_FACTORY, _ => {
    return new WebContentFormatterFactory([new JSONFormatter('Application/json'), new JSONFormatter('text/json')]);
});

appIocContainer.register(InjectScope.SINGLETON, IocRegisteredObject.AJAX_SENDER, c => {
    return new AjaxSender(c.resolve<IWebContentFormatterFactory>(IocRegisteredObject.CONTENT_FORMATTER_FACTORY));
});

appIocContainer.register(InjectScope.SINGLETON, IocRegisteredObject.REQUEST_SENDER, c => {
    return new RequestSender(c.resolve<IAjaxSender>(IocRegisteredObject.AJAX_SENDER));
});

appIocContainer.register(InjectScope.SINGLETON, IocRegisteredObject.API_PROXY, c => {
    const msHost = getMSHost();
    const msMainHost = import.meta.env.REACT_APP_MAIN_MS_HOST ?? getMainMsHost();
    const cpHost = import.meta.env.REACT_APP_CP_HOST ?? '';
    const apiHost = import.meta.env.REACT_APP_API_HOST ?? createApiHost();
    const uploadHost = import.meta.env.REACT_APP_UPLOAD_HOST ?? createUploadFileHost();

    return new ApiProxy(c.resolve<IRequestSender>(IocRegisteredObject.REQUEST_SENDER), cpHost, apiHost, msHost, msMainHost, uploadHost);
});

export const IoCContainer = {
    instance: appIocContainer,
    getApiProxy: () => appIocContainer.resolve<ApiProxy>(IocRegisteredObject.API_PROXY),
};