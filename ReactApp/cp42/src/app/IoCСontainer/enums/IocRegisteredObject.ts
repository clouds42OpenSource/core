/**
 * Содержит названия типов зарегестрировнных объектов в IoC контейнере
 */
export enum IocRegisteredObject {
    /**
     * Объект для отправки запросов
     */
    REQUEST_SENDER = 'REQUEST_SENDER',

    /**
     * Объект для отправки ajax-http запросов
     */
    AJAX_SENDER = 'AJAX_SENDER',

    /**
     * Объект в котором содержаться форматеры перевода из http ответов в результат json
     */
    CONTENT_FORMATTER_FACTORY = 'CONTENT_FORMATTER_FACTORY',

    /**
     * Клиентский объект для отправки API запросов
     */
    API_PROXY = 'API_PROXY'
}