import { IFetchParams, IRequestSender } from 'core/requestSender/interfaces';

import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { Nullable } from 'app/common/types';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Базовый прокси
 */
export class BaseProxy {
    protected host: string;

    protected requestSender: IRequestSender;

    protected readonly defaultContentType = 'application/json';

    public constructor(requestSender: IRequestSender, host: string) {
        this.requestSender = requestSender;
        this.host = host;
    }

    private readonly jwtToken = localStorageHelper.getJWTToken();

    /**
     * Получить обьект аргументов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @param contentType Тип передаваемого контента
     * @param headers Заголовки запроса
     * @returns Обьект аргументов
     */
    protected getObjectArguments<TBodyParams>(requestKind: RequestKind, args?: TBodyParams, contentType?: Nullable<string>, headers?: { [key: string]: string; }): IFetchParams<TBodyParams> {
        return {
            requestKind,
            requestArgs: {
                contentType: contentType === null || contentType ? contentType ?? undefined : this.defaultContentType,
                dataToSend: args,
                headers: {
                    Authorization: `Bearer ${ this.jwtToken }`,
                    ...headers
                }
            }
        };
    }
}