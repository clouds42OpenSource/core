import { changeToCamelCase } from './changeToCamelCase';

describe('changeToCamelCase', () => {
    it('should change keys from PascalCase to camelCase', () => {
        const testObject = {
            TestField: 123
        };
        const result = changeToCamelCase(testObject);

        expect(typeof result).toEqual('object');
        expect(result.hasOwnProperty('testField')).toBeTruthy();
        expect(result.testField).toEqual(123);
    });

    it('should pass primitive types', () => {
        const testNumber = 123;
        const result = changeToCamelCase(testNumber);

        expect(result).toEqual(testNumber);
    });

    it('should pass objects into objects', () => {
        const testObject = {
            TestField1: {
                TestField2: {
                    TestField3: {
                        TestField4: 1
                    }
                }
            }
        };

        const result = changeToCamelCase(testObject);

        expect(typeof result).toEqual('object');
        expect(result.testField1.testField2.testField3.hasOwnProperty('testField4')).toBeTruthy();
        expect(result.testField1.testField2.testField3.testField4).toEqual(1);
    });
});