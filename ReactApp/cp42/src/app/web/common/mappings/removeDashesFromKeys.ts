/**
 *
 * @param obj Any element you want to remove keys dashes from it
 * @return Element by itself, if it is not an object, or object with renamed keys
 */
export const removeDashesFromKeys = (obj: any) => {
    const regex = /(-[a-zA-Z0-9])/g;

    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    if (Array.isArray(obj)) {
        for (let i = 0; i < obj.length; i++) {
            obj[i] = removeDashesFromKeys(obj[i]);
        }

        return obj;
    }

    const newObj: any = {};

    for (const [key, value] of Object.entries(obj)) {
        const newKey = key.replace(regex, (match) => match[1].toUpperCase());
        newObj[newKey] = removeDashesFromKeys(value);
    }

    return newObj;
};