/**
 * Mapping модели общий тип для получения записей
 * @param value response объекта общий тип для получения записей
 * @param outputResultMapFrom
 */
export function selectStatusDataResultCommonDataModelMapFrom<TInputResult, TOutputResult>(
    value: Array<TInputResult>,
    outputResultMapFrom: (input: TInputResult) => TOutputResult
): Array<TOutputResult> {
    return !value ? [] : value.map(item => outputResultMapFrom(item));
}