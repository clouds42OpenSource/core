import { removeDashesFromKeys } from './removeDashesFromKeys';

describe('removeDashesFromKeys', () => {
    it('should remove dashes', () => {
        const testObject = {
            'test-field': ''
        };
        const result = removeDashesFromKeys(testObject);

        expect(typeof result).toEqual('object');
        expect(result.hasOwnProperty('testField')).toBeTruthy();
    });

    it('should pass primitive types', () => {
        const testNumber = 123;
        const result = removeDashesFromKeys(testNumber);

        expect(result).toEqual(testNumber);
    });

    it('should pass objects into objects', () => {
        const testObject = {
            'test-field-1': {
                'test-field-2': {
                    'test-field-3': {
                        'test-field-4': 1
                    }
                }
            }
        };

        const result = removeDashesFromKeys(testObject);

        expect(typeof result).toEqual('object');
        expect(result.testField1.testField2.testField3.hasOwnProperty('testField4')).toBeTruthy();
    });
});