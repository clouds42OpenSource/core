import { SelectDataResultCommonDataModel, SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { metadataModelMapFrom, paginationDataModelMapFrom } from 'app/web/common/mappings';
import { SelectDataMetadataResponseDto, SelectDataResultCommonResponseDto } from 'app/web/common/response-dto';

/**
 * Mapping модели общий тип для получения записей
 * @param value response объекта общий тип для получения записей
 * @param outputResultMapFrom
 */
export function selectDataResultCommonDataModelMapFrom<TInputResult, TOutputResult>(
    value: SelectDataResultCommonResponseDto<TInputResult>,
    outputResultMapFrom: (input: TInputResult) => TOutputResult
): SelectDataResultCommonDataModel<TOutputResult> {
    return {
        pagination: paginationDataModelMapFrom(value.Pagination),
        records: !value.Records ? [] : value.Records.map(item => outputResultMapFrom(item)),
        AllDatabasesCount: value.AllDatabasesCount,
        ArchievedDatabasesCount: value.ArchievedDatabasesCount,
        ServerDatabasesCount: value.ServerDatabasesCount,
        DelimeterDatabasesCount: value.DelimeterDatabasesCount,
        FileDatabasesCount: value.FileDatabasesCount,
        DeletedDatabases: value.DeletedDatabases
    };
}

export function selectDataResultMetadataModelMapFrom<TInputResult, TOutputResult>(
    value: SelectDataMetadataResponseDto<TInputResult>,
    outputResultMapFrom: (input: TInputResult) => TOutputResult
): SelectDataResultMetadataModel<TOutputResult> {
    return {
        metadata: metadataModelMapFrom(value.metadata),
        records: !value.records ? [] : value.records.map(item => outputResultMapFrom(item)),
        allDatabasesCount: value.allDatabasesCount,
        archievedDatabasesCount: value.archievedDatabasesCount,
        serverDatabasesCount: value.serverDatabasesCount,
        delimeterDatabasesCount: value.delimeterDatabasesCount,
        fileDatabasesCount: value.fileDatabasesCount
    };
}