import { MetadataModel, PaginationDataModel } from 'app/web/common/data-models';
import { MetadataResponseDto, PaginationResponseDto } from 'app/web/common/response-dto';

/**
 * Mapping данных о страницах полученных по запросу в модель приложения
 * @param value данные о страницах в моделе ответа
 */
export function paginationDataModelMapFrom(value: PaginationResponseDto): PaginationDataModel {
    return {
        firstNumber: value?.FirstNumber ?? 0,
        lastNumber: value?.LastNumber ?? 0,
        pageCount: value?.PageCount ?? 0,
        pageSize: value?.PageSize ?? 0,
        totalCount: value?.TotalCount ?? 0,
        currentPage: value?.CurrentPage ?? 0
    };
}

export function metadataModelMapFrom(value: MetadataResponseDto): MetadataModel {
    return {
        ...value
    };
}