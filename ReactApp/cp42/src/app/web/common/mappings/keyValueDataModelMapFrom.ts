import { KeyValueDataModel } from 'app/web/common/data-models';
import { KeyValueResponseDto } from 'app/web/common/response-dto/KeyValueResponseDto';

/**
 * Mapping модели ключ-значение
 * @param value response объекта ключ-значение
 */
export function keyValueDataModelMapFrom<TKey, TValue>(value: KeyValueResponseDto<TKey, TValue>): KeyValueDataModel<TKey, TValue> {
    return {
        key: value.key,
        value: value.value
    };
}