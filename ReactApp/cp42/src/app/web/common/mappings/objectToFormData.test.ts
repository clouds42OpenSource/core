import { objectToFormData } from './objectToFormData';

describe('objectToFormData', () => {
    it('should return FormData type', () => {
        const formData = objectToFormData({});
        expect(formData instanceof FormData).toBeTruthy();
    });

    it('should pass primitive types', () => {
        const testObject = {
            field: 1
        };

        const formData = objectToFormData(testObject);
        expect(formData.get('field')).toEqual('1');
    });

    it('should objects and arrays', () => {
        const testObject = {
            fieldArray: [1, 2, 3],
            fieldObject: {
                field1: 1,
                field2: 2,
                field3: [1, 2, 3]
            }
        };

        const formData = objectToFormData(testObject);
        expect(formData.get('fieldArray[1]')).toEqual('2');
        expect(formData.get('fieldObject.field1')).toEqual('1');
        expect(formData.get('fieldObject.field3[2]')).toEqual('3');
    });
});