export const objectToFormData = (value: any, formData = new FormData(), namespace = '') => {
    if (typeof value !== 'undefined' && value !== null) {
        if (value instanceof Date) {
            formData.append(namespace, value.toISOString());
        } else if (value instanceof Array) {
            for (let i = 0; i < value.length; i++) {
                objectToFormData(value[i], formData, `${ namespace }[${ i }]`);
            }
        } else if (typeof value === 'object' && !(value instanceof File)) {
            for (const propertyName in value) {
                if (value.hasOwnProperty(propertyName)) {
                    objectToFormData(value[propertyName], formData, namespace ? `${ namespace }.${ propertyName }` : propertyName);
                }
            }
        } else if (value instanceof File) {
            formData.append(namespace, value);
        } else {
            formData.append(namespace, value.toString());
        }
    }

    return formData;
};