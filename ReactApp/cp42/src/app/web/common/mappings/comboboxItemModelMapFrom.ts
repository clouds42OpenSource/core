import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';
import { ComboboxItemResponseDto } from 'app/web/common/response-dto';

/**
 * Mapping модели комбобокса
 * @param value response объекта комбобокса
 */
export function comboboxItemModelMapFrom<TValue>(value: ComboboxItemResponseDto<TValue>): ComboboxItemModel<TValue> {
    return {
        text: value.Text,
        value: value.Value,
        group: value.Group
    };
}