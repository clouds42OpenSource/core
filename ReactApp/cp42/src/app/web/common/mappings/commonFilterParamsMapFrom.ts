import { FilterBaseRequestDto } from 'app/web/api/SegmentsProxy/common';
import { FilterBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common/params';

/**
 * Маппинг базового data model фильтра к request dto
 * @param value Базовый data model фильтра
 * @returns Базовый request dto фильтра
 */
export function commonFilterParamsMapFrom(value: FilterBaseParams): FilterBaseRequestDto {
    return {
        PageNumber: value.pageNumber,
        PageSize: value.pageSize,
        SortFieldName: value.sortFieldName,
        SortType: value.sortType
    };
}