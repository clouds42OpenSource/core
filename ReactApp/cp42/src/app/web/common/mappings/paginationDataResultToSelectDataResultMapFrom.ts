import { SelectDataResultCommonDataModel } from 'app/web/common/data-models';
import { paginationDataModelMapFrom } from 'app/web/common/mappings';
import { PaginationDataResultResponseDto } from 'app/web/common/response-dto';

/**
 * Mapping модели с пагинацией к общему типу data model с пагинацией
 * @param value response объекта общий тип для получения записей
 * @param outputResultMapFrom
 */
export function paginationDataResultToSelectDataResultMapFrom<TInputResult, TOutputResult>(
    value: PaginationDataResultResponseDto<TInputResult>,
    outputResultMapFrom: (input: TInputResult) => TOutputResult
): SelectDataResultCommonDataModel<TOutputResult> {
    return {
        pagination: paginationDataModelMapFrom({
            TotalCount: value.TotalCount,
            PageSize: value.PageSize,
            CurrentPage: value.PageNumber,
            FirstNumber: 0,
            LastNumber: 0,
            PageCount: Math.ceil(value.TotalCount / value.PageSize)
        }),
        records: !value.ChunkDataOfPagination ? [] : value.ChunkDataOfPagination.map(item => outputResultMapFrom(item))
    };
}