/**
 * @param obj Any element you want to change keys to CamelCase format
 * @param needToRevert Set to true, if you need make first letter uppercase
 * @return Element by itself, if it is not an object, or object with renamed keys
 */
export const changeToCamelCase = (obj: any, needToRevert = false) => {
    if (typeof obj !== 'object' || obj === null) {
        return obj;
    }

    if (Array.isArray(obj)) {
        for (let i = 0; i < obj.length; i++) {
            obj[i] = changeToCamelCase(obj[i], needToRevert);
        }

        return obj;
    }

    const newObj: any = {};

    for (const [key, value] of Object.entries(obj)) {
        const newKey = (needToRevert ? key.charAt(0).toUpperCase() : key.charAt(0).toLowerCase()) + key.slice(1);
        newObj[newKey] = changeToCamelCase(value, needToRevert);
    }

    return newObj;
};