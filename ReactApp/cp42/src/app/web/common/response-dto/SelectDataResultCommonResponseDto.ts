import { PaginationResponseDto, TPaginationResponseDto } from 'app/web/common/response-dto/PaginationResponseDto';

/**
 * Общий тип для получения записей
 * @template TResult тип записи в массиве `Records`
 */
export type SelectDataResultCommonResponseDto<TResult> = {
    /**
     * Список записей
     */
    Records: Array<TResult>;
    /**
     * Пагинация списка
     */
    Pagination: PaginationResponseDto;
    /**
     * Количества баз по типам
     */
    ServerDatabasesCount?: number;
    ArchievedDatabasesCount?: number;
    AllDatabasesCount?: number;
    DelimeterDatabasesCount?: number;
    FileDatabasesCount?: number;
    DeletedDatabases?: number;
};

export type TSelectDataResultCommonResponseDto<TResult> = {
    records: Array<TResult>;
    pagination: TPaginationResponseDto;
    serverDatabasesCount?: number;
    archievedDatabasesCount?: number;
    allDatabasesCount?: number;
    delimeterDatabasesCount?: number;
    fileDatabasesCount?: number;
    deletedDatabases?: number;
};