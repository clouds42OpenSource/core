import { MetadataResponseDto } from 'app/web/common/response-dto/MetadataResponseDto';

/**
 * Общий тип для получения записей
 * @template TResult тип записи в массиве `Records`
 */
export type SelectDataMetadataResponseDto<TResult> = {
    /**
     * Список записей
     */
    records: Array<TResult>;
    /**
     * Пагинация списка
     */
    metadata: MetadataResponseDto;
    /**
     * Количества баз по типам
     */
    serverDatabasesCount?: number;
    archievedDatabasesCount?: number;
    allDatabasesCount?: number;
    delimeterDatabasesCount?: number;
    fileDatabasesCount?: number;
};