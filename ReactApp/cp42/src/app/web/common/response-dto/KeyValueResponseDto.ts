/**
 * Тип ключ-значение
 * @template TKey тип ключа
 * @template TValue тип значения
 */
export type KeyValueResponseDto<TKey, TValue> = {
    /**
     * Ключ объекта
     */
    key: TKey;

    /**
     * Значение объекта
     */
    value: TValue
};