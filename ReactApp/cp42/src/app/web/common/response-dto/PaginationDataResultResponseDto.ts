/**
 * Общий модель для получения данных с пагинацией
 * @template TModel тип записи в массиве `ChunkDataOfPagination`
 */
export type PaginationDataResultResponseDto<TModel> = {
    /**
     * Порция данных
     */
    ChunkDataOfPagination: Array<TModel>,

    /**
     * Общее количество всех данных паггинации
     */
    TotalCount: number,

    /**
     * Количество строк страницы
     */
    PageSize: number,

    /**
     * Номер страницы данных
     */
    PageNumber: number
}