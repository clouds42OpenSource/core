/**
 * Данные о страницах
 */
export type PaginationResponseDto = {
    /**
     * Количество всего записей
     */
    TotalCount: number;
    /**
     * Количество записей на страницу
     */
    PageSize: number;
    /**
     * Номер первой страницы
     */
    FirstNumber: number;
    /**
     * Номер последней страницы
     */
    LastNumber: number;
    /**
     * Количество всего страниц
     */
    PageCount: number;
    /**
     * Текущая страница
     */
    CurrentPage: number;
};

export type TPaginationResponseDto = {
    totalCount: number;
    pageSize: number;
    firstNumber: number;
    lastNumber: number;
    pageCount: number;
    currentPage: number;
};