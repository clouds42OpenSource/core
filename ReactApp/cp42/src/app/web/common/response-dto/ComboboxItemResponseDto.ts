/**
 * Тип объекта списка элементов для ComboBox
 */
export type ComboboxItemResponseDto<TValue> = {
    /**
     * Значение элемента
     */
    Value: TValue;

    /**
     * Текст элемента
     */
    Text: string;

    /**
     * Группировка элемента
     */
    Group?: string;
};