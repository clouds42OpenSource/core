import { EInformationBasesType } from 'app/api/endpoints/informationBases/enum';
import { SortingDataModel } from 'app/web/common/data-models/SortingDataModel';

/**
 * Общий тип для выбора записей
 */
export type SelectDataCommonDataModel<TFilter> = {
    /**
     * Тип баз(все, серверные, в архиве)
     */
    type?: EInformationBasesType;
    /**
     * строка поиска
     */
    search?: string;
    /**
     * Сортировка по роли
     */
    group?: string;
    /**
     * ID аккаунта
     */
    accountID?: string;
    /**
     * Фильтр
     */
    filter?: TFilter;
    /**
     * какую страницу загрузить
     */
    pageNumber: number;
    size?: number;
    pageSize?: number;
    /**
     * Информация о сортировке
     */
    sortingData?: SortingDataModel
    orderBy?: string;
};