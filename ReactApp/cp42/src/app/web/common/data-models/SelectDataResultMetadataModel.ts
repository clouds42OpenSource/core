import { CountBasesDataModel } from 'app/views/modules/AccountManagement/InformationBases/types/CountBasesDataModel';
import { MetadataModel } from 'app/web/common/data-models/MetadataModel';

/**
 * Общий тип для получения записей
 * @template TResult тип записи в массиве `Records`
 */
export type SelectDataResultMetadataModel<TResult> = {
    /**
     * Список записей
     */
    records: Array<TResult>;
    /**
     * Пагинация списка
     */
    metadata: MetadataModel;
    countBase?: CountBasesDataModel;
    searchQuery?: string;
    orderBy?: string;
    allDatabasesCount?: number;
    archievedDatabasesCount?: number;
    serverDatabasesCount?: number;
    delimeterDatabasesCount?: number;
    fileDatabasesCount?: number;
};