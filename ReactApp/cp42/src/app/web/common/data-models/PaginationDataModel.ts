/**
 * Модель данных связанная с нумерацией
 */
export type PaginationDataModel = {
    /**
     * Количество всего записей
     */
    totalCount: number;
    /**
     * Количество записей на страницу
     */
    pageSize: number;
    /**
     * Номер первый
     */
    firstNumber: number;
    /**
     * Номер последний
     */
    lastNumber: number;
    /**
     * Количество страниц
     */
    pageCount: number;
    /**
     * Текущая страница
     */
    currentPage: number;
};