/**
 * Тип ключ-значение
 * @template TKey тип ключа
 * @template TValue тип значения
 */
export type KeyValueDataModel<TKey, TValue> = {
    /**
     * Ключ объекта
     */
    key: TKey;
    /**
     * Значение объекта
     */
    value: TValue
};

export type ReadOnlyKeyValueDataModel<TKey, TValue> = Readonly<KeyValueDataModel<TKey, TValue>>;