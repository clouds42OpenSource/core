import { SortingKind } from 'app/common/enums/SortingKind';

/**
 * Модель для сортировки по полю
 */
export type SortingDataModel = {
    /**
     * Вид сортировки
     */
    sortKind: SortingKind;
    /**
     * По какому полю сортировать
     */
    fieldName: string;
};