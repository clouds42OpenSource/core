import { SortingDataModel } from 'app/web/common';

export enum InfoDbTypeEnum {
    /**
     * Все базы
     */
    all = 0,
    /**
     * Только серверные
     */
    server = 1,
    /**
     * Только в архиве
     */
    archive = 2,
    /**
     * Только файловые
     */
    file = 3,
    /**
     * Только разделители
     */
    delimeter = 4
}

export type SelectInfoDbGetQueryParams<T> = {
    /**
     * Фильтр
     */
    filter?: T;
    /**
     * Строка поиска
     */
    search: string | undefined;
    /**
     * Номер страницы
     */
    pageNumber: number;
    pageSize?: number;
    /**
     * Размер страницы
     */
    size?: number;
    /**
     * своя/доступная база
     */
    affiliation?: number;
    /**
     * Поле сортировки
     */
    field: string;
    /**
     * Тип сортировки
     */
    sort: number;
    type: InfoDbTypeEnum;
    accountID: string;
    sortingData?: SortingDataModel;
    accountUserId: string;
};