import { CountBasesDataModel } from 'app/views/modules/AccountManagement/InformationBases/types/CountBasesDataModel';
import { PaginationDataModel } from 'app/web/common/data-models/PaginationDataModel';

/**
 * Общий тип для получения записей
 * @template TResult тип записи в массиве `Records`
 */
export type SelectDataResultCommonDataModel<TResult> = {
    countBase?: CountBasesDataModel;
    SearchQuery?: string;
    AllDatabasesCount?: number;
    ArchievedDatabasesCount?: number;
    ServerDatabasesCount?: number;
    DelimeterDatabasesCount?: number;
    FileDatabasesCount?: number;
    DeletedDatabases?: number;
    /**
     * Список записей
     */
    records: Array<TResult>;
    /**
     * Пагинация списка
     */
    pagination: PaginationDataModel;
};