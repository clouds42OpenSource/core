import { SortingKind } from 'app/common/enums';

/**
 * Базовый тип фильтра
 */
export type FilterBaseParams = {
    /**
     * Номер страницы
     */
    pageNumber: number;
    /**
     * Количество страниц
     */
    pageSize?: number;
    /**
     * Название поля для сортировки
     */
    sortFieldName?: string;
    /**
     * Тип сортировки
     */
    sortType?: SortingKind;
    orderBy?: string;
}