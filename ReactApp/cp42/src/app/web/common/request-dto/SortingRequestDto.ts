import { SortingKind } from 'app/common/enums/SortingKind';

/**
 * Модель для сортировки по полю
 */
export type SortingRequestDto = {
    /**
     * Вид сортировки
     */
    SortKind: SortingKind;

    /**
     * По какому полю сортировать
     */
    FieldName: string;
};