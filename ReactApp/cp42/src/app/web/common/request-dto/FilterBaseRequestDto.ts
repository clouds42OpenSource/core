import { SortingKind } from 'app/common/enums';

/**
 * Базовый тип фильтра
 */
export type FilterBaseRequestDto = {
    /**
     * Номер страницы
     */
    PageNumber: number,

    /**
     * Количество страниц
     */
    PageSize?: number,

    /**
     * Название поля для сортировки
     */
    SortFieldName?: string,

    /**
     * Тип сортировки
     */
    SortType?: SortingKind
}