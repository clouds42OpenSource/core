import { InfoDbTypeEnum } from 'app/web/common';

export type SelectDataCommonInfoDb = {
    /**
     * Строка поиска
     */
    search: string | undefined;
    /**
     * Номер страницы
     */
    page?: number;
    /**
     * Размер страницы
     */
    size?: number;
    /**
     * своя/доступная база
     */
    affiliation?: number;
    /**
     * Поле сортировки
     */
    field?: string;
    /**
     * Тип сортировки
     */
    sort?: number;
    type?: InfoDbTypeEnum;
    accountID?: string;
    accountUserId?: string;
};