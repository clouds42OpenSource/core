import { SortingRequestDto } from 'app/web/common';

/**
 * Общий тип для выбора записей
 */
export type SelectDataCommonRequestDto<TFilter> = {
    /**
     * Фильтр
     */
    Filter: TFilter | null;
    /**
     * какую страницу загрузить
     */
    PageNumber: number;
    /**
     * Информация о сортировке
     */
    SortingData?: SortingRequestDto | null;
    /**
     * ID аккаунта для запроса
     */
    accountID?: string;
    /**
     * Строка поиска
     */
    search?: string;
    /**
     * Тип базы(все, серверные, в архиве)
     */
    type?: string;
    /**
     * Количество элементов на странице
     */
    PageSize?: number;
    size?: number;
    /**
     * своя/доступная база
     */
    affiliation?: number;
    /**
     * Поле сортировки
     */
    field?: string;
    /**
     * Тип сортировки
     */
    sort?: number;
    orderBy?: string;
};