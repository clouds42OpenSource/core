export enum AccountUserGroupEnum {
    /** Пользователь аккаунта */
    AccountUser = 'AccountUser',
    /** Менеджер аккаунта */
    AccountAdmin = 'AccountAdmin',
    /** Сейлc менеджер */
    AccountSaleManager = 'AccountSaleManager',
    /** Оператор процес центра */
    ProcOperator = 'ProcOperator',
    /** Хотлайн */
    Hotline = 'Hotline',
    /** Администратор облака */
    CloudAdmin = 'CloudAdmin',
    /** Служба */
    Cloud42Service = 'Cloud42Service',
    /** Инженер облака */
    CloudSE = 'CloudSE',
    /** Внешняя служба */
    ExternalService = 'ExternalService',
}