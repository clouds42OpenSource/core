/** Доступы аккаунта в зависимости от роли */
export enum AccountPermissionsEnum {
    /** Доступ к добавлению транзакций */
    AddTransaction = 'Payments_Add',
    /** Доступ к просмотру оказанных услуг */
    ProvidedServicesView = 'ProvidedServices_View',
    /** Доступ к странице баланса */
    PaymentsView = 'Payments_View',
    /** Доступ к добавлению комментария при пополнении счета или активации обещаннаго платежа */
    ChangeDescription = 'Payments_ChangeDescription',
    /** Доступ для изменения поставщика */
    ChangeAccountSupplier = 'Account_EditSupplier',
     /** Доступ для редактирования пользователя */
    EditUserFields = 'AccountUsers_Edit',
     /** Доступ для редактирования роли пользователя */
    EditUserRoles = 'AccountUsers_RoleEdit',
     /** Доступ для создания пользователя */
    AddNewUser = 'AccountUsers_AddToAccount',
    AccountsView = 'Accounts_View',
    /** Доступ для переноса БД */
    AccountsMoveInfoBase = 'Accounts_MoveInfoBase',
    /** Отображение кнопки сменить сегмент. Страница Данные о компании */
    SegmentView = 'Segment_View',
    /** Смена сегмента. Страница Данные о компании */
    AccountsEditFullInfoWithSegments = 'Accounts_EditFullInfoWithSegments',
    /** Редактирование данных о компании */
    AccountsEdit = 'Accounts_Edit',
    /** Отображения менеджера и VIP статуса в просмотре и редактировании */
    AccountsViewFullInfo = 'Accounts_ViewFullInfo',
    /** Отображение сегмента */
    AccountsViewFullInfoWithSegments = 'Accounts_ViewFullInfoWithSegments',
    /** Отображение менеджера компании в режиме радактирования */
    ControlPanelSetSalemanager = 'ControlPanel_SetSalemanager',
    /** Отображение местоположения в режиме редактирования */
    ControlPanelSetLocale = 'ControlPanel_SetLocale'
}