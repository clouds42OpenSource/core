export enum InfoDbCardPermissionsEnum {
    AccountDatabases_EditFull = 'AccountDatabases_EditFull',
    AccountDatabase_Change = 'AccountDatabase_Change',
    AcDbAccesses_Edit = 'AcDbAccesses_Edit',
    AccountDatabases_EditCaption = 'AccountDatabases_EditCaption',
    AccountDatabases_EditPlatform = 'AccountDatabases_EditPlatform',
    AccountDatabases_EditUsedWebServices = 'AccountDatabases_EditUsedWebServices',
    AccountDatabases_EditStatus = 'AccountDatabases_EditStatus',
    AccountDatabases_EditSupport = 'AccountDatabases_EditSupport',
    AccountDatabases_EditDbType = 'AccountDatabases_EditDbType',
    AccountDatabases_Publish = 'AccountDatabases_Publish',
    AccountDatabases_Unpublish = 'AccountDatabases_Unpublish',
    AccountDatabase_DeleteToTomb = 'AccountDatabase_DeleteToTomb',
    AccountDatabase_Restore = 'AccountDatabase_Restore',
    AccountDatabases_LockUnlock = 'AccountDatabases_LockUnlock',
    AccountDatabases_RestartIisAppPool = 'AccountDatabases_RestartIisAppPool',
    AccountDatabases_ChangePlatformFrom83To82 = 'AccountDatabases_ChangePlatformFrom83To82',
    AccountDatabase_OfArchiveToTomb = 'AccountDatabase_OfArchiveToTomb',
    AccountDatabase_GetAcDbAccesses = 'AccountDatabase_GetAcDbAccesses',
    AccountDatabases_GrantAccessToUser = 'AccountDatabases_GrantAccessToUser',
    AccountDatabases_RemoveUserAccess = 'AccountDatabases_RemoveUserAccess',
    AccountDatabase_TerminateSessions = 'AccountDatabase_TerminateSessions',
    Database_CalculateCostOfCreating = 'Database_CalculateCostOfCreating',
    AccountDatabasesAdd = 'AccountDatabases_Add'
}