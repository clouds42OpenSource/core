/**
 * Модель данных авторизации в ИТС
 */
export type ItsAuthorizationDataResponseDto = {
    /**
     * ID записи
     */
    id: string;

    /**
     * Логин
     */
    login: string;

    /**
     * Хэш пароля
     */
    passwordHash: string;
};