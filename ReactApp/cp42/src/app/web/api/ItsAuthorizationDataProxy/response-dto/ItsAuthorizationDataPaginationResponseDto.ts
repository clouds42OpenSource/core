import { ItsAuthorizationDataResponseDto } from 'app/web/api/ItsAuthorizationDataProxy';
import { SelectDataResultMetadataModel } from 'app/web/common';

/**
 * Модель списка данных авторизации в ИТС с пагинацией
 */
export type ItsAuthorizationDataPaginationResponseDto = SelectDataResultMetadataModel<ItsAuthorizationDataResponseDto>;