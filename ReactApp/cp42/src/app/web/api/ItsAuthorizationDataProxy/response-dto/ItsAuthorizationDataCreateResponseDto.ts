/**
 * Модель данных авторизации в ИТС
 */
export type ItsAuthorizationDataCreateResponseDto = {
    /**
     * Логин
     */
    login: string;

    /**
     * Хэш пароля
     */
    passwordHash: string;
};