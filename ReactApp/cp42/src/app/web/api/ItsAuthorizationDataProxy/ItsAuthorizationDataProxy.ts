import { TResponseLowerCase } from 'app/api/types';
import { ItsAuthorizationDataCreateResponseDto, ItsAuthorizationDataPaginationResponseDto, ItsAuthorizationDataResponseDto, ItsAuthorizationFilterRequestDto } from 'app/web/api/ItsAuthorizationDataProxy';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с доступом к ИТС
 */
export class ItsAuthorizationDataProxy extends BaseProxy {
    /**
     * Получить список данных авторизации в ИТС с пагинацией
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public getItsDataAuthorizations(requestKind: RequestKind, args: ItsAuthorizationFilterRequestDto) {
        return this.requestSender.submitRequest<ItsAuthorizationDataPaginationResponseDto, ItsAuthorizationFilterRequestDto>(
            WebVerb.GET,
            `${ this.host }/its-authorization-data`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить модель данных авторизации в ИТС по ID
     * @param requestKind Вид запроса
     * @param itsAuthorizationId ID модель данных авторизации в ИТС
     */
    public getItsDataAuthorization(requestKind: RequestKind, itsAuthorizationId: string) {
        return this.requestSender.submitRequest<ItsAuthorizationDataResponseDto>(
            WebVerb.GET,
            `${ this.host }/its-authorization-data/${ itsAuthorizationId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Удалить данные авторизации в ИТС
     * @param requestKind Вид запроса
     * @param itsAuthorizationId ID модель данных авторизации в ИТС
     */
    public deleteItsDataAuthorization(requestKind: RequestKind, itsAuthorizationId: string) {
        return this.requestSender.submitRequest<void>(
            WebVerb.DELETE,
            `${ this.host }/its-authorization-data/${ itsAuthorizationId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новые данные авторизации в ИТС
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createItsDataAuthorization(requestKind: RequestKind, args: ItsAuthorizationDataCreateResponseDto) {
        return this.requestSender.submitRequest<TResponseLowerCase<null> | null, ItsAuthorizationDataCreateResponseDto>(
            WebVerb.POST,
            `${ this.host }/its-authorization-data`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать данные авторизации в ИТС
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editItsDataAuthorization(requestKind: RequestKind, args: ItsAuthorizationDataResponseDto) {
        return this.requestSender.submitRequest<void, ItsAuthorizationDataResponseDto>(
            WebVerb.PUT,
            `${ this.host }/its-authorization-data`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить коллекцию авторизации данных в ИТС
     * @param requestKind Вид запроса
     * @returns Коллекцию авторизации данных в ИТС
     */
    public getItsAuthorizationDataBySearchValue(requestKind: RequestKind) {
        return this.requestSender.submitRequest<ItsAuthorizationDataPaginationResponseDto>(
            WebVerb.GET,
            `${ this.host }/its-authorization-data`,
            this.getObjectArguments(requestKind)
        );
    }
}