import { Nullable } from 'app/common/types';

/**
 * Данные фильтра для получения данных авторизации в ИТС
 */
export type ItsAuthorizationFilterDataRequestDto = {
    /**
     * Строка поиска
     */
    searchLine: Nullable<string>;
};