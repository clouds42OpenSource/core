import { ItsAuthorizationFilterDataRequestDto } from 'app/web/api/ItsAuthorizationDataProxy/request-dto';
import { SelectDataCommonRequestDto } from 'app/web/common';

/**
 * Модель фильтра для получения списка данных авторизации в ИТС
 */
export type ItsAuthorizationFilterRequestDto = SelectDataCommonRequestDto<ItsAuthorizationFilterDataRequestDto>;