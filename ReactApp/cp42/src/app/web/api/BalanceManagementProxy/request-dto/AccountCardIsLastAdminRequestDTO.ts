export type AccountCardIsLastAdminRequestDTO = {
    isLastAdmin: boolean;
};