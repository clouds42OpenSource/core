/**
 * Модель запроса к апи для оплаты без ввода данных сохраненным способом оплаты
 */
export type QuickPayRequestDto = {
    paymentMethodId: string;
    totalSum: number;
    isAutoPay: boolean;
};