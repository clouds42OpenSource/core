import { Nullable } from 'app/common/types';

type ProductsForCreate = {
    IsActive: boolean;
    Identifier: string;
    Quantity: number;
    PayPeriod: number;
    Rate: number;
    TotalPrice: number;
    TotalBonus: number;
};

export type CreateInvoiceRequestDto = {
    NotifyAccountUserId: Nullable<string>;
    PayPeriod: number;
    Products: ProductsForCreate[];
    TotalBeforeVAT: number;
    VAT: number;
    TotalAfterVAT: number;
    TotalBonus: number;
    AccountAdditionalCompanyId?: string;
};