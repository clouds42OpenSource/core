import { Nullable } from 'app/common/types';

/**
 * Модель запроса к апи на создание счета на произвольную сумму
 */
export type CreateCustomInvoiceRequestDto = {
    /** Описание */
    Description: string;
    /** Сумма */
    Amount: number;
    /** ID юзера для отправки уведомлений */
    NotifyAccountUserId: Nullable<string>;
};