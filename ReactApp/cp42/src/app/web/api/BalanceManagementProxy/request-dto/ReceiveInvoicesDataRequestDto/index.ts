/**
 * Модель запроса к апи на получения данных счетов
 */
export type ReceiveInvoicesDataRequestDto = {
    /**
     * страница
     */
    pageNumber: number;
};