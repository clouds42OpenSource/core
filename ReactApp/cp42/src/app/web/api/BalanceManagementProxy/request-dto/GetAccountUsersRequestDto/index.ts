/**
 * Модель запроса к апи на получение пользователей аккаунта
 */
export type GetAccountUsersRequestDto = {
    /** Количество получаемых пользователей в 1 странице */
    pageSize?: number;
    /** Страница */
    pageNumber?: number;
    /** тип ортировки */
    orderBy?: string;
    searchLine?: string;
    group?: string;
    filter?: {
        id?: string | null;
        accountId: string | null;
        searchLine?: string;
    }
};