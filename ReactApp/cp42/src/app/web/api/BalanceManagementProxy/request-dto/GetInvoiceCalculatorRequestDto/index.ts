/**
 * Модель запроса к апи на получения данных калькулятора по типа Standard | Fasta
 */
export type GetInvoiceCalculatorRequestDto = {
    /** Тип калькулятора */
    type: string;
};