/**
 * Модель запроса к апи на получения данных о оказанных услугах
 */
export type ReceiveProvidedServicesDataRequestDto = {
    /**
     * страница
     */
    pageNumber: number;
    /**
     * дата с
     */
    'Filter.DateTo'?: string | Date;
    /**
     * дата по
     */
    'Filter.DateFrom'?: string | Date;
    /**
     * фильтр по типу сервисов
     */
    'Filter.Service': number | string | null;
};