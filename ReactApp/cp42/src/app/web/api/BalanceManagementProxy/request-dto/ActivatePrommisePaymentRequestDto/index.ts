/**
 * Модель запроса к апи на для активации обещанного платежа
 */
export type ActivatePromisePaymentRequestDto = {
    /** Id счета на оплату */
    InvoiceId: string;
};