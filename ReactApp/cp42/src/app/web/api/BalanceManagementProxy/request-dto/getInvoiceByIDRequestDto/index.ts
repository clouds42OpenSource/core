/**
 * Модель запроса к апи на получения счета на оплату по ID
 */
export type getInvoiceByIDRequestDto = {
    /** ID запрашиваемого счета */
    id: string;
};