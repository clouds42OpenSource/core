import { ReactText } from 'react';

export type AccountActiveSessionsRequestDTO = {
    accountId?: string;
    userIds: ReactText[];
};