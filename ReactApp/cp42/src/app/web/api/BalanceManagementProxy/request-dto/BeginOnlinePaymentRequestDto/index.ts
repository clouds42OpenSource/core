/**
 * Модель запроса к апи на для оплаты онлайн
 */
export type BeginOnlinePaymentRequestDto = {
    TotalSum: number;
    ReturnUrl: string;
    IsAutoPay: boolean;
};