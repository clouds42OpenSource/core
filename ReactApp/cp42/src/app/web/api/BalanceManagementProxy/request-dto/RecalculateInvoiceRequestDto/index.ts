/**
 * Модель запроса к апи на перерасчет сервисов
 */
export type RecalculateInvoiceRequestDto = {
    /** Период в месяцах */
    PayPeriod: number;
    Products: {
        /** Идентификатор сервиса */
        Identifier: string;
        /** Количество */
        Quantity: number;
        /** Признак активности */
        IsActive: boolean;
    }[]
};