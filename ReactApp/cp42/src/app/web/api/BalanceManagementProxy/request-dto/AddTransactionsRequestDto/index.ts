/**
 * Модель запроса к апи на для добавления транзакций
 */
export type AddTransactionsRequestDto = {
    /**
     * Сумма
     */
    Total: string;
    /**
     * Тип платежа
     */
    Type: number;
    /**
     * Тип транзакции
     */
    TransactionType: number;
    /**
     * Дата
     */
    Date: string;
    /**
     * Описание\комментарий
     */
    Description: string;
};