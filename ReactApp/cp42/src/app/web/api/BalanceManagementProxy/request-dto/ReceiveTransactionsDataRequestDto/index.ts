/**
 * Модель запроса к апи на получения данных транзакций
 */
export type ReceiveTransactionsDataRequestDto = {
    /**
     * страница
     */
    pageNumber: number;
    /**
     * дата с
     */
    'Filter.DateFrom'?: string | Date;
    /**
     * дата по
     */
    'Filter.DateTo'?: string | Date;
    /**
     * Тип транзакции сейчас
     */
    'Filter.Service': number | string | null;
};