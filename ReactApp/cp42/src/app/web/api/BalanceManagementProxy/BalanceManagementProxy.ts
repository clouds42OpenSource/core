import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import {
    AddTransactionsRequestDto,
    CreateInvoiceRequestDto,
    GetInvoiceCalculatorRequestDto,
    RecalculateInvoiceRequestDto,
    ReceiveInvoicesDataRequestDto,
    ReceiveProvidedServicesDataRequestDto,
    ReceiveTransactionsDataRequestDto
} from 'app/web/api/BalanceManagementProxy/request-dto';
import { AccountActiveSessionsRequestDTO } from 'app/web/api/BalanceManagementProxy/request-dto/AccountActiveSessionsRequestDTO';
import { AccountCardIsLastAdminRequestDTO } from 'app/web/api/BalanceManagementProxy/request-dto/AccountCardIsLastAdminRequestDTO';
import { ActivatePromisePaymentRequestDto } from 'app/web/api/BalanceManagementProxy/request-dto/ActivatePrommisePaymentRequestDto';
import { BeginOnlinePaymentRequestDto } from 'app/web/api/BalanceManagementProxy/request-dto/BeginOnlinePaymentRequestDto';
import { CreateCustomInvoiceRequestDto } from 'app/web/api/BalanceManagementProxy/request-dto/CreateCustomInvoiceRequestDto';
import { GetAccountUsersRequestDto } from 'app/web/api/BalanceManagementProxy/request-dto/GetAccountUsersRequestDto';
import { QuickPayRequestDto } from 'app/web/api/BalanceManagementProxy/request-dto/QuickPayRequestDto';
import { getInvoiceByIDRequestDto } from 'app/web/api/BalanceManagementProxy/request-dto/getInvoiceByIDRequestDto';
import {
    BillingAccountResponseDto,
    CreateInvoiceResponseDto,
    InovoiceCalculatorResponseDto,
    InvoicesResponseDto,
    ProvidedLongServicesResponseDto,
    ProvidedServicesResponseDto,
    TransactionsResponseDto
} from 'app/web/api/BalanceManagementProxy/response-dto';
import { AccountActiveSessionsResponseDTO } from 'app/web/api/BalanceManagementProxy/response-dto/AccountActiveSessionsResponseDTO';
import { BeginOnlinePaymentResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto/BeginOnlinePaymentResponseDto';
import { GetAccountUsersResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto/GetAccountUsersResponseDto';
import { IsLastAdminDto } from 'app/web/api/BalanceManagementProxy/response-dto/IsLastAdminDto';
import { QuickPayResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto/QuickPayResponseDto';
import { RecalculateInvoiceResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto/RecalculateInvoiceResponseDto';
import { getInvoiceByIdResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto/getInvoiceByIdResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

const accountId = () => localStorageHelper.getContextAccountId();

/**
 * Прокси для работы с счетами
 */
export class BalanceManagementProxy extends BaseProxy {
    private token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение счетов
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public receiveInvoices(requestKind: RequestKind, args: ReceiveInvoicesDataRequestDto): Promise<InvoicesResponseDto> {
        return this.requestSender.submitRequest<InvoicesResponseDto, ReceiveInvoicesDataRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/invoices`,
            this.getObjectArguments(requestKind, { accountId: accountId(), ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение транзакций
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public recieveTransactions(requestKind: RequestKind, args: ReceiveTransactionsDataRequestDto): Promise<TransactionsResponseDto> {
        return this.requestSender.submitRequest<TransactionsResponseDto, ReceiveTransactionsDataRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/payments`,
            this.getObjectArguments(requestKind, { accountId: accountId(), ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение оказанных услуг
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public recieveProvidedServices(requestKind: RequestKind, args: ReceiveProvidedServicesDataRequestDto): Promise<ProvidedServicesResponseDto> {
        return this.requestSender.submitRequest<ProvidedServicesResponseDto, ReceiveProvidedServicesDataRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/providedservices`,
            this.getObjectArguments(requestKind, { accountId: accountId(), ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение длительных услуг
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public recieveProvidedLongServices(requestKind: RequestKind, args: NonNullable<unknown>): Promise<ProvidedLongServicesResponseDto> {
        return this.requestSender.submitRequest<ProvidedLongServicesResponseDto, NonNullable<unknown>>(
            WebVerb.GET,
            `${ this.host }/api_v2/providedservices/LongServices`,
            this.getObjectArguments(requestKind, { accountId: accountId(), ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение длительных услуг
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public getBillinAccountData(requestKind: RequestKind, args: NonNullable<unknown>): Promise<BillingAccountResponseDto> {
        return this.requestSender.submitRequest<BillingAccountResponseDto, NonNullable<unknown>>(
            WebVerb.GET,
            `${ this.host }/api_v2/BillingAccounts/${ accountId() }`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение длительных услуг
     * @param requestKind Вид запроса
     */
    public ProlongPromisePayment(requestKind: RequestKind): Promise<NonNullable<unknown>> {
        return this.requestSender.submitRequest<NonNullable<unknown>, { accountId: string }>(
            WebVerb.POST,
            `${ this.host }/api_v2/PromisePayments/Prolong`,
            this.getObjectArguments(requestKind, { accountId: accountId() ?? '' }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение длительных услуг
     * @param requestKind Вид запроса
     */
    public PayPromisePayment(requestKind: RequestKind): Promise<NonNullable<unknown>> {
        return this.requestSender.submitRequest<NonNullable<unknown>, { accountId: string }>(
            WebVerb.POST,
            `${ this.host }/api_v2/PromisePayments/Repay`,
            this.getObjectArguments(requestKind, { accountId: accountId() ?? '' }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на добавление транзакции
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public AddTransaction(requestKind: RequestKind, args: AddTransactionsRequestDto): Promise<NonNullable<unknown>> {
        return this.requestSender.submitRequest<NonNullable<unknown>, AddTransactionsRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/payments`,
            this.getObjectArguments(requestKind, { accountId: accountId(), ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Получить счет на оплату по ID
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public getInvoiceById(requestKind: RequestKind, args: getInvoiceByIDRequestDto): Promise<getInvoiceByIdResponseDto> {
        return this.requestSender.submitRequest<getInvoiceByIdResponseDto, NonNullable<unknown>>(
            WebVerb.GET,
            `${ this.host }/api_v2/invoices/${ args.id }`,
            this.getObjectArguments(requestKind, {}, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Получить калькулятор
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public getInvoiceCalculator(requestKind: RequestKind, args: GetInvoiceCalculatorRequestDto): Promise<InovoiceCalculatorResponseDto> {
        return this.requestSender.submitRequest<InovoiceCalculatorResponseDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/accounts/${ accountId() }/invoice-creation/${ args.type }/calculator`,
            this.getObjectArguments(requestKind, undefined, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Перерасчет на бэке услуг фаста
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public recalculateFasta(requestKind: RequestKind, args: RecalculateInvoiceRequestDto): Promise<RecalculateInvoiceResponseDto> {
        return this.requestSender.submitRequest<RecalculateInvoiceResponseDto, RecalculateInvoiceRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/accounts/${ accountId() }/invoice-creation/fasta/calculator`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Перерасчет на бэке основных услуг
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public recalculateStandard(requestKind: RequestKind, args: RecalculateInvoiceRequestDto): Promise<RecalculateInvoiceResponseDto> {
        return this.requestSender.submitRequest<RecalculateInvoiceResponseDto, RecalculateInvoiceRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/accounts/${ accountId() }/invoice-creation/subscription-services/calculator`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Создание счета на оплату
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public createInvoice(requestKind: RequestKind, args: CreateInvoiceRequestDto & { type: string }): Promise<CreateInvoiceResponseDto> {
        return this.requestSender.submitRequest<CreateInvoiceResponseDto, CreateInvoiceRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/accounts/${ accountId() }/invoice-creation/${ args.type }`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Создание счета на произвольную сумму
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public createCustomInvoice(requestKind: RequestKind, args: CreateCustomInvoiceRequestDto): Promise<CreateInvoiceResponseDto> {
        return this.requestSender.submitRequest<CreateInvoiceResponseDto, CreateCustomInvoiceRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/accounts/${ accountId() }/invoice-creation/custom`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Получение списка пользователей аккаунта
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public getAccountUsers(requestKind: RequestKind, args: GetAccountUsersRequestDto): Promise<GetAccountUsersResponseDto> {
        return this.requestSender.submitRequest<GetAccountUsersResponseDto, GetAccountUsersRequestDto>(
            WebVerb.GET,
            `${ this.host }/account-users`,
            this.getObjectArguments(
                requestKind,
                {
                    pageNumber: args.pageNumber,
                    filter: {
                        accountId: accountId(),
                        searchLine: args.searchLine,
                        groups: args.group
                    },
                    orderBy: args.orderBy
                },
                'Application/json',
                { Authorization: `Bearer ${ this.token }` }
            )
        );
    }

    /**
     * Получение последнего админа аккаунта
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public isLastAdmin(requestKind: RequestKind, args: IsLastAdminDto): Promise<boolean> {
        return this.requestSender.submitRequest<boolean, IsLastAdminDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountUsers/IsLastAdminAccount`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Получение результата поиска
     * @param requestKind Вид запроса
     */
    public find(requestKind: RequestKind): Promise<AccountCardIsLastAdminRequestDTO> {
        return this.requestSender.submitRequest<AccountCardIsLastAdminRequestDTO, null>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountUsers/${ accountId() }/AccountUserActiveSessions`,
            this.getObjectArguments(requestKind, null, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Получение списка активных сеансов
     * @param requestKind Вид запроса
     */
    public activeSessions(requestKind: RequestKind): Promise<AccountActiveSessionsResponseDTO> {
        return this.requestSender.submitRequest<AccountActiveSessionsResponseDTO, null>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountUsers/${ accountId() }/AccountUserActiveSessions`,
            this.getObjectArguments(requestKind, null, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Завершение активных сеансов
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public activeSessionsTerminate(requestKind: RequestKind, args: AccountActiveSessionsRequestDTO): Promise<NonNullable<unknown>> {
        return this.requestSender.submitRequest<NonNullable<unknown>, AccountActiveSessionsRequestDTO>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountUsers/AccountUserActiveSessions`,
            this.getObjectArguments(requestKind, { userIds: args.userIds, accountId: `${ accountId() }` }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Активация обещанного платежа
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public activatePromisePayment(requestKind: RequestKind, args: ActivatePromisePaymentRequestDto): Promise<NonNullable<unknown>> {
        return this.requestSender.submitRequest<NonNullable<unknown>, ActivatePromisePaymentRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/accounts/${ accountId() }/promised-payment/activate`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Перерасчет на бэке основных услуг
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public onlinePayment(requestKind: RequestKind, args: BeginOnlinePaymentRequestDto): Promise<BeginOnlinePaymentResponseDto> {
        return this.requestSender.submitRequest<BeginOnlinePaymentResponseDto, BeginOnlinePaymentRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/Payments/BeginOnlinePayment`,
            this.getObjectArguments(requestKind, { ...args, AccountId: accountId() }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Оплата без ввода данных с сохранными способами оплаты
     * @param requestKind Вид запроса
     * @param args данные для оплаты
     */
    public quickPay(requestKind: RequestKind, args: QuickPayRequestDto): Promise<QuickPayResponseDto> {
        return this.requestSender.submitRequest<QuickPayResponseDto, QuickPayRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/Payments/QuickPay`,
            this.getObjectArguments(requestKind, { ...args, accountId: accountId() }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}