/**
 * Модель ответа с элементом транзакций
 */
export type ProvidedServicesItemDto = {
   /** Признак истечения срока действия */
   IsHistoricalRow: boolean;
   /** Описание сервиса */
   ServiceDescription: string;
   /** Сервис */
   Service: number;
   ResourceType: number;
   /** Дата с */
   From: Date;
   /** Дата по */
   To: Date;
   /** Количество */
   Count: number;
   /** Цена */
   Rate: string;
   /** */
   FromCorp: boolean;
   /** Спосируемы аккаунт */
   SposoredAccountName: string;
};