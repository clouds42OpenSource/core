type Setting = {
    Key: string;
    Value: string;
};

/** Модель Сервисов */
export type Product = {
    /** ID сервиса */
    Identifier: string;
    /** Активность */
    IsActive: boolean;
    /** Период покупки сервиса */
    PayPeriod: number;
    /** Цена */
    Rate: number;
    /** Сумма к оплате */
    TotalPrice: number;
    /** Бонусы */
    TotalBonus: number;
    /** Количество */
    Quantity: number;
    /** Название */
    Name: string;
    /** Описание */
    Description: string;
    /** Настройки количества */
    QuantityControl: {
        Type: string,
        Settings: Setting[]
    },
};

/**
 * Модель ответа с элементами калькулятора
 */
export type InovoiceCalculatorResponseDto = {
    /** Покупатель */
    BuyerName: string;
    /** Поставщик */
    SupplierName: string;
    /** ID аккаунта */
    AccountId: string;
    /** Периоды оплаты */
    PayPeriod: number;
    /** Сумма до НДС */
    TotalBeforeVAT: number;
    /** НДС */
    VAT: number;
    /** Данные о валюте */
    Currency: {
        /** Код валюты */
        CurrencyCode: number;
        /** Валюта */
        Currency: string;
    },
    /** Сумма с НДС */
    TotalAfterVAT: number;
    /** Бонусы */
    TotalBonus: number;
    /** Список сервисов */
    Products: Product[];
    /** Настройки периода оплаты */
    PayPeriodControl: {
        Type: string;
        Settings: Setting[]
    }
};