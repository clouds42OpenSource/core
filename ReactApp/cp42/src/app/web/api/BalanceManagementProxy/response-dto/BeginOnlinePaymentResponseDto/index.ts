/**
 *  Модель ответа при оплате онлайн
 */
export type BeginOnlinePaymentResponseDto = {
    PaymentId: string;
    PaymentSystem: 'Yookassa' | 'UkrPay' | 'Paybox' | 'Robokassa';
    Data: {
        ConfirmationToken: string;
        ReturnUrl: string;
        PaymentId: string;
        PaymentSystem: string;
        TotalSum: number;
    } | string;
};