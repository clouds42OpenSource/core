import { Nullable } from 'app/common/types';

export type IsLastAdminDto = {
    accountId: Nullable<string>;
};