/**
 * Модель ответа при оплате без ввода данных с сохраненным способом оплаты
 */
export type QuickPayResponseDto = {
    PaymentId: string;
    PaymentSystem: string;
    TotalSum: number;
    Token: string | null;
    Type: string;
    RedirectUrl: string | null;
}