export enum EStatus {
    New,
    CorpError,
    Reject,
    Accept,
    Processing,
    Processed
}
/**
 * Модель ответа с элементом счета на оплату
 */
export type InvoiceItemResponseDto = {
    /** ID счета на оплату */
    Id: string;
    /** Статус оплаты */
    State: number;
    /** Сумма */
    InvoiceSum: number;
    /** Дата создания */
    InvoiceDate: string;
    /** Описание документа */
    ActDescription: string;
    /** ID документа */
    ActId: string;
    /** */
    Uniq: string;
    /** Номер фискального чека  */
    ReceiptFiscalNumber: number;
    IsNewInvoice: boolean;
    /** Описание */
    Description: string;
    RequiredSignature: boolean;
    Status: EStatus;
};