import { TransactionsInvoiceType } from 'app/views/modules/AccountManagement/BalanceManagementView/types/TransactionsInvoiceType';

export type TransactionsItemResponseDto = {
    Id: string;
    /** Загаловок */
    Title: string;
    /** Тип транзакции */
    TransactionType: number;
    /** Сумма к оплате */
    PaySum: number;
    /** Баланс счета */
    MoneyBalance: number;
    /** Баланс бонусного счета */
    BonusBalance: number;
    /** Дата */
    Date: Date;
    /** Статус транзакцит */
    Status: number;
    /** Тип транзакции */
    Type: TransactionsInvoiceType;
    Remains: number | null;
};