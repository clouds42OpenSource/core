/**
 * Модель ответа с элементом ID счета на оплату
 */
export type CreateInvoiceResponseDto = {
    Id: string;
};