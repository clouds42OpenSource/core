/**
 * Модель ответа с элементом Billing Account
 */
export type BillingAccountResponseDto = {
    /**
     * Признак статуса демо аккаунта
     */
    AccountIsDemo: boolean;
    /**
     * Признак VIP аккаунта
     */
    AccountIsVip: boolean;
    /**
     *  Баланс аккаунта
     */
    CurrentBalance: number;
    /**
     * Бонусы аккаунта
     */
    CurrentBonusBalance: number;
    /**
     *  Ежемесячный платеж
     */
    RegularPayment: number;
    /**
     * Признак ошибки в платежах
     */
    HasErrorInPayment: boolean;
    /**
     * Локаль
     */
    Locale: {
        /**
         * Название
         */
        Name: string;
        /**
         * Валюта
         */
        Currency: string;
    }
    /**
     * Данные об обещанном платеже
     */
    PromisePayment: {
        /**
         * Признак возможности увеличения обещанного платежа
         */
        CanIncreasePromisePayment: boolean;
        /**
         * Признак возможности получения обещанного платежа
         */
        CanTakePromisePayment: boolean;
        /**
         * Причина блокировки обещанного платежа
         */
        BlockReason: string;
    }
    /**
     * Признак возможности погашения обещанный платеж
     */
    CanEarlyPromisePaymentRepay: boolean;
    /**
     * Признак возможности продления обещанного платежа
     */
    CanProlongPromisePayment: boolean;
    /**
     * SuggestedPayment
     */
    SuggestedPayment: string;
};