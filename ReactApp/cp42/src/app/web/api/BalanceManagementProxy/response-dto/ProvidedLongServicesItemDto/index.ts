export type ProvidedLongServicesItemDto = {
    /** Признак истечения срока */
    IsHistoricalRow: boolean;
    /** Сервис */
    Service: number;
    /** */
    ResourceType: number;
    /** Описание сервиса */
    ServiceDescription: string;
    /** Дата с */
    From: Date;
    /** Дата по */
    To: Date;
    /** Количество */
    Count: number;
    /** Цена */
    Rate: string;
    /**  */
    FromCorp: boolean;
    /** Спосируемый аккаунт */
    SposoredAccountName: string;
};