type Product = {
    /** Признак активности сервиса */
    IsActive: boolean;
    /** ID Сервиса */
    Identifier: string;
    /** Количество */
    Quantity: number;
    /** Период */
    PayPeriod: number;
    /** Цена */
    Rate: number;
    /** Сумма */
    TotalPrice: number;
    /** Бонусы */
    TotalBonus: number;
};

/**
 * Модель ответа с элементом перерасчета счета на оплату
 */
export type RecalculateInvoiceResponseDto = {
    /** Период оплаты */
    PayPeriod: number | null;
    /** Сумма без НДС */
    TotalBeforeVAT: number;
    /** НДС */
    VAT: number;
    /** Сумма с НДС */
    TotalAfterVAT: number;
    /** Бонусы */
    TotalBonus: number;
    /** Список сервисов */
    Products: Product[];
};