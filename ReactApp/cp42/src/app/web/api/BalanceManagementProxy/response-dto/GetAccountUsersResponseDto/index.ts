import { Nullable } from 'app/common/types';
import { SelectDataResultMetadataModel } from 'app/web/common';
import { TAccountDepartment } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

/**
 * Модель ответа ползователей аккаунта
 */
export type GetAccountUsersItemResponseDto = {
    /** ID пользователя */
    id: string;
    /** ID аккаунта */
    accountId: string;
    /** Логин */
    login: string;
    /** Имя */
    firstName: string;
    /** Фамилия */
    lastName: string;
    /** Отчество */
    middleName: string;
    /** Полное имя */
    fullName: string;
    /** Почта */
    email: string;
    /** Номер телефона */
    phoneNumber: string;
    /** Активность пользователя */
    activated: boolean;
    /** Дата Создания пользователя */
    createdInAd: boolean;
    /**  */
    unsubscribed: Nullable<boolean>;
    /** */
    creationDate: Date;
    /** Роли пользователя */
    accountRoles: string[];
    isPhoneVerified: boolean;
    isEmailVerified: boolean;
    department: TAccountDepartment | null;
};

export type GetAccountUsersResponseDto = SelectDataResultMetadataModel<GetAccountUsersItemResponseDto>;