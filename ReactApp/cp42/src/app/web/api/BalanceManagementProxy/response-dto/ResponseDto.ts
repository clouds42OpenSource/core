import { InvoiceItemResponseDto, TransactionsItemResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto';
import { ProvidedLongServicesItemDto } from 'app/web/api/BalanceManagementProxy/response-dto/ProvidedLongServicesItemDto';
import { ProvidedServicesItemDto } from 'app/web/api/BalanceManagementProxy/response-dto/ProvidedServicesItemDto';
import { SelectDataResultCommonResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении данных счетов на оплату
 */
export type InvoicesResponseDto = SelectDataResultCommonResponseDto<InvoiceItemResponseDto>;
/**
 * Модель ответа при получении данных транзакций
 */
export type TransactionsResponseDto = SelectDataResultCommonResponseDto<TransactionsItemResponseDto>;
/**
 * Модель ответа при получении данных оказанных услуг
 */
export type ProvidedServicesResponseDto = SelectDataResultCommonResponseDto<ProvidedServicesItemDto>;
/**
 * Модель ответа при получении данных длительных услуг
 */
export type ProvidedLongServicesResponseDto = SelectDataResultCommonResponseDto<ProvidedLongServicesItemDto>;