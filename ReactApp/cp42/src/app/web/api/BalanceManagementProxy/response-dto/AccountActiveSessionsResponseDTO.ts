type UserInformationType = {
    Id: string;
    AccountId: string;
    Login: string;
    FirstName: string;
    LastName: string;
    MiddleName: string;
    FullName: string;
    Email: string;
    PhoneNumber: string;
    Activated: boolean;
    CreatedInAd: boolean;
    Unsubscribed: boolean;
    CreationDate: string;
    AccountRoles: string[];
    Password: string;
    AuthToken: string;
    Roles: number[];
    CorpUserSyncStatus: string;
    AccountCaption: string;
    ResetCode: string;
};

export type AccountActiveSessionsResponseDTO = {
    UserTerminateList: UserInformationType[]
};