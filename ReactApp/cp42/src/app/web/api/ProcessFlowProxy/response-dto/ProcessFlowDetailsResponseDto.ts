import { ActionFlowResponseDto } from 'app/web/api/ProcessFlowProxy/response-dto/ActionFlowResponseDto';
import { ProcessFlowItemResponseDto } from 'app/web/api/ProcessFlowProxy/response-dto/ProcessFlowItemResponseDto';

/**
 * Модель ответа на запрос получения деталей рабочего процесса
 */
export type ProcessFlowDetailsResponseDto = ProcessFlowItemResponseDto & {
    /**
     * Шаги рабочего процесса
     */
    actionFlows: Array<ActionFlowResponseDto>;
};