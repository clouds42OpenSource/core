import { StateMachineComponentStatus } from 'app/common/enums';

/**
 * Модель описывающая свойства рабочего процесса конечного автомата.
 */
export type ProcessFlowItemResponseDto = {
    /**
     * Идентификатор процесса.
     */
    id: string;
    /**
     * Дата создания процесса.
     */
    creationDateTime: string;
    /**
     * Имя процесса.
     */
    name: string;
    /**
     * Имя обрабатываемого процессом объекта.
     */
    processedObjectName: string;
    /**
     * Статус процесса.
     */
    status: StateMachineComponentStatus;
    /**
     * Описание состояния процесса.
     */
    stateDescription: string;
    /**
     * Комментарий среды исполнения.
     */
    comment: string;
};