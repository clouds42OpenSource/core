import { ProcessFlowItemResponseDto } from 'app/web/api/ProcessFlowProxy/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа на запрос получения рабочих процессов
 */
export type ProcessFlowListResponseDto = SelectDataMetadataResponseDto<ProcessFlowItemResponseDto>;