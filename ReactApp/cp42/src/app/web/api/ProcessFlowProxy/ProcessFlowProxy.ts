import { GetProcessFlowDetailsRequestDto, GetProcessFlowRequestDto, RetryProcessFlowRequestDto } from 'app/web/api/ProcessFlowProxy/request-dto';
import { ProcessFlowDetailsResponseDto, ProcessFlowListResponseDto } from 'app/web/api/ProcessFlowProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с CurrentSessionSettings
 */
export class ProcessFlowProxy extends BaseProxy {
    /**
     * Запрос на получение рабочих процессов
     * @param requestKind Вид запроса
     * @param args параметры на получение списка рабочих процессов
     */
    public receiveProcessFlowList(requestKind: RequestKind, args: GetProcessFlowRequestDto) {
        return this.requestSender.submitRequest<ProcessFlowListResponseDto, GetProcessFlowRequestDto>(
            WebVerb.GET,
            `${ this.host }/process-flows`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на получение рабочих процессов
     * @param requestKind Вид запроса
     * @param args параметры на получение списка рабочих процессов
     */
    public retryProcessFlow(requestKind: RequestKind, args: RetryProcessFlowRequestDto) {
        return this.requestSender.submitRequest<void, RetryProcessFlowRequestDto>(
            WebVerb.POST,
            `${ this.host }/process-flows/retry`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на получение деталей рабочего процесса
     * @param requestKind Вид запроса
     * @param args параметры на получение деталей рабочего процесса
     */
    public receiveProcessFlowDetails(requestKind: RequestKind, args: GetProcessFlowDetailsRequestDto) {
        return this.requestSender.submitRequest<ProcessFlowDetailsResponseDto, GetProcessFlowDetailsRequestDto>(
            WebVerb.GET,
            `${ this.host }/process-flows/${ args.processFlowId }`,
            this.getObjectArguments(requestKind)
        );
    }
}