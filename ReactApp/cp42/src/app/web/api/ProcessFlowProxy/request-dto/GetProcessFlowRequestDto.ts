import { StateMachineComponentStatus } from 'app/common/enums';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Фильтр поиска
 */
type Filter = {
    /**
     * Статус компоменты конечного автомата
     */
    Status: StateMachineComponentStatus | null;

    /**
     * Строка поиска
     */
    SearchLine: string;
};

/**
 * Модель параметров на запрос получения рабочих процессов
 */
export type GetProcessFlowRequestDto = SelectDataCommonRequestDto<Filter>;