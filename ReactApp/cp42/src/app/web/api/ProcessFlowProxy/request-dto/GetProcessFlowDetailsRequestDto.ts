/**
 * Модель параметров на запрос получения деталей рабочего процесса
 */
export type GetProcessFlowDetailsRequestDto = {
    /**
     * ID рабочего процеса
     */
    processFlowId: string;
};