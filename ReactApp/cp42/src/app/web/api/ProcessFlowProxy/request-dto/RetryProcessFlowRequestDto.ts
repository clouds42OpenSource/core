/**
 * Модель параметров на запрос перезапуска рабочего процесса
 */
export type RetryProcessFlowRequestDto = {
    /**
     * ID рабочего процеса
     */
    processFlowId: string;
};