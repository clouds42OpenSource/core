/**
 * Модель описывающая свойства служебного аккаунта
 */
export type ServiceAccountItemResponseDto = {
    /**
     * ID аккаунта.
     */
    accountId: string;
    /**
     * Номер аккаунта.
     */
    accountIndexNumber: number;
    /**
     * Название аккаунта.
     */
    accountCaption: string;
    /**
     * Логин пользователя, который создал служебный аккаунт
     */
    accountUserInitiatorName: string;
    /**
     * Дата создания служебного аккаунта.
     */
    creationDateTime: string;
};