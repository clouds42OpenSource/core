import { ServiceAccountItemResponseDto } from 'app/web/api/ServiceAccounts/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа на запрос получения служебных акккаунтов
 */
export type ServiceAccountsResponseDto = SelectDataMetadataResponseDto<ServiceAccountItemResponseDto>;