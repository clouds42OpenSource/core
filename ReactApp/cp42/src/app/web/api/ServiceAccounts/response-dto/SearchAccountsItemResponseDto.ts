/**
 * Ответ на запрос поиска аккаунтов
 */
export type SearchAccountsItemResponseDto = {
    /**
     * Описание аккаунта
     */
    value: string;
    /**
     * Id аккаунта
     */
    key: string;
    /**
     * Номер аккаунта
     */
    indexNumber: number;
};