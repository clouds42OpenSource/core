/**
 * Модель ответа на запрос добавления служебного аккаунта
 */
export type AddServiceAccountResponseDto = {
    /**
     * Логин пользователя, который создал служебный аккаунт
     */
    accountUserInitiatorName: string;
    /**
     * Дата создания
     */
    creationDateTime: string;
};