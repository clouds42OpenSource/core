/**
 * Модель параметров на запрос добавления служебного аккаунта
 */
export type AddServiceAccountRequestDto = {
    /**
     * ID аккаунта для добавления
     */
    AccountId: string;
};