/**
 * Модель параметров на запрос удаление служебного аккаунта
 */
export type DeleteServiceAccountRequestDto = {
    /**
     * ID аккаунта для удаления
     */
    serviceAccountId: string;
};