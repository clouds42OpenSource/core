/**
 * Запрос на поиск аккаунта
 */
export type SearchAccountsRequestDto = {
    /**
     * Строка поиска
     */
    searchLine: string;
};