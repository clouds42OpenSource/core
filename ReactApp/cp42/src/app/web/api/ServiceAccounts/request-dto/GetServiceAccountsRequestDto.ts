import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Фильтр поиска
 */
type Filter = {
    /**
     * Строка поиска
     */
    SearchLine: string;
};

/**
 * Модель параметров на запрос получения служебных аккаунтов
 */
export type GetServiceAccountsRequestDto = SelectDataCommonRequestDto<Filter>;