import { GetServiceAccountsRequestDto, SearchAccountsRequestDto } from 'app/web/api/ServiceAccounts/request-dto';
import { AddServiceAccountRequestDto } from 'app/web/api/ServiceAccounts/request-dto/AddServiceAccountRequestDto';
import { DeleteServiceAccountRequestDto } from 'app/web/api/ServiceAccounts/request-dto/DeleteServiceAccountRequestDto';
import { ServiceAccountsResponseDto } from 'app/web/api/ServiceAccounts/response-dto';
import { AddServiceAccountResponseDto } from 'app/web/api/ServiceAccounts/response-dto/AddServiceAccountResponseDto';
import { SearchAccountsItemResponseDto } from 'app/web/api/ServiceAccounts/response-dto/SearchAccountsItemResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с ServiceAccounts
 */
export class ServiceAccountsProxy extends BaseProxy {
    /**
     * Запрос на получение служебных аккаунтов
     * @param requestKind Вид запроса
     * @param args параметры на получение служебных аккаунтов
     */
    public receiveServiceAccounts(requestKind: RequestKind, args: GetServiceAccountsRequestDto) {
        return this.requestSender.submitRequest<ServiceAccountsResponseDto, GetServiceAccountsRequestDto>(
            WebVerb.GET,
            `${ this.host }/service-accounts`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на удаление служебного аккаунта
     * @param requestKind Вид запроса
     * @param args параметры на удаление служебного аккаунта
     */
    public deleteServiceAccount(requestKind: RequestKind, args: DeleteServiceAccountRequestDto) {
        return this.requestSender.submitRequest<void, DeleteServiceAccountRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/service-accounts/${ args.serviceAccountId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на добавление служебного аккаунта
     * @param requestKind Вид запроса
     * @param args параметры на добавление служебного аккаунта
     */
    public addServiceAccount(requestKind: RequestKind, args: AddServiceAccountRequestDto) {
        return this.requestSender.submitRequest<AddServiceAccountResponseDto, AddServiceAccountRequestDto>(
            WebVerb.POST,
            `${ this.host }/service-accounts`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на удаление служебного аккаунта
     * @param requestKind Вид запроса
     * @param args параметры на удаление служебного аккаунта
     */
    public searchAccounts(requestKind: RequestKind, args: SearchAccountsRequestDto) {
        return this.requestSender.submitRequest<SearchAccountsItemResponseDto[], SearchAccountsRequestDto>(
            WebVerb.GET,
            `${ this.host }/accounts/as-list`,
            this.getObjectArguments(requestKind, args)
        );
    }
}