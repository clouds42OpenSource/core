import { TUserSessionsItem } from 'app/api/endpoints/additionalSessions/response';

/**
 * Параметр на изменение или покупку дополнительных сеансов
 */
export type ChangeCloudServiceResourcesRequestDto = {
    count: number;
    accountUserSessions: TUserSessionsItem[];
};