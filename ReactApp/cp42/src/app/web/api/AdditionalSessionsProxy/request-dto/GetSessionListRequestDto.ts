/**
 * Параметры на получения списка сеансов
 */
export type GetSessionListRequestDto = {
    /**
     * true - выведется полный список сеансов
     * false - выведется только тонкий и веб-клиент
     */
    'full-list': boolean;
    /**
     * При наличии group-by будет группировка сеансов пользователя
     */
    'group-by'?: string;
    'account-id'?: string;
};