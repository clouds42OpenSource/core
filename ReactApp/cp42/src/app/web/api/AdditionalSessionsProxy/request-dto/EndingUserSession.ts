/**
 * Параметры на завершение сеанса пользователя
 */
export type EndingUserSessionRequestDto = {
    SessionID?: string;
    /**
     * Тестовый запуск
     */
    dummy?: boolean;
    /**
     * Сообщение пользователю при завершении сеанса
     */
    message?: string;
    /**
     * Уведомление пользователя перед завершением сеанса
     */
    notify?: boolean;
    'account-id'?: string;
};