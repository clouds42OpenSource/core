import { TUserSessionsItem } from 'app/api/endpoints/additionalSessions/response';

export type BuyCloudServiceResourcesRequestDto = {
    isPromisePayment: boolean;
    costOfTariff: number;
    count: number;
    accountUserSessions: TUserSessionsItem[]
};