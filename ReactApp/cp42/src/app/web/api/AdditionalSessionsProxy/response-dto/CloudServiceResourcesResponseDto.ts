/**
 * Модель купленного ресурса
 */
export type CloudServiceResourcesResponseDto = {
    Id: string;
    Name: string;
    DependServiceId: string;
    ServiceExpireDate: string;
    ServiceDependsOnRent: boolean;
    IsActivatedPromisPayment: boolean;
    Amount: number;
    UsedLicenses: number;
    Currency: string;
};