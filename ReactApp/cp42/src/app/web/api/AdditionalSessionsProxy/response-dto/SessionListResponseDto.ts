type DataItemType = {
    /**
     * Идентификатор сеанса в формате ИдентификаторОбластиДанных-НомерСеанса
     */
    id: string;
    /**
     * Номер сеанса
     */
    number: number;
    /**
     * Область данных
     */
    application: {
        /**
         * Наименование
         */
        name: string;
        /**
         * Идентификатор
         */
        id: string;
        /**
         * Код (42_xxx)
         */
        code: string;
    },
    /**
     * Пользователь
     */
    user: {
        /**
         * Имя пользователя
         */
        name: string;
    },
    /**
     * Момент начала сеанса
     */
    start: string;
    /**
     * Спящий сеанс
     */
    sleeping: boolean;
    /**
     * Заблокированный сеанс
     */
    locked: boolean;
    /**
     * Тип клиентского приложения (Тонкий клиент / Веб клиент)
     */
    client_type: string;
    /**
     * Время получения списка сеансов
     */
    timestamp: string;
};

/**
 * Модель элемента списка сеансов
 */
export type SessionListItem = {
    group: {
        name: string;
        id: string;
    },
    data: DataItemType[];
};

/**
 * Модель списка сеансов
 */
export type SessionListResponseDto = {
    sessions: SessionListItem[],
    'additional-sessions-used': number
};