import { Nullable } from 'app/common/types';

/**
 * Модель ответа при покупке или изменении дополнительных сеансов
 */
export type ChangeCloudServiceResourcesResponseDto = {
    Complete: boolean;
    Error: Nullable<string>;
    EnoughMoney: number;
    CanGetPromisePayment: boolean;
    CostOftariff: number;
    MonthlyCost: number;
    Currency: string;
    Balance: number;
};