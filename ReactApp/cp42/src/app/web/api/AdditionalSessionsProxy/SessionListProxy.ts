import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { EndingUserSessionRequestDto, GetSessionListRequestDto } from 'app/web/api/AdditionalSessionsProxy/request-dto';
import { SessionListResponseDto } from 'app/web/api/AdditionalSessionsProxy/response-dto';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

/**
 * Прокси для работы с списком сеансов
 */
export class SessionListProxy extends BaseProxy {
    private readonly token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение списка сеансов
     * @param requestKind Вид запроса
     * @param args Параметры для получения списка сеансов
     */
    public getSessionList(requestKind: RequestKind, args: GetSessionListRequestDto): Promise<SessionListResponseDto> {
        return this.requestSender.submitRequest<SessionListResponseDto, GetSessionListRequestDto>(
            WebVerb.GET,
            `${ this.host }/Platform/sessions`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на завершение сеанса пользователя
     * @param requestKind Вид запроса
     * @param args Параметры для завершения сеанса пользователя
     */
    public endingUserSession(requestKind: RequestKind, args: EndingUserSessionRequestDto) {
        return this.requestSender.submitRequest<void, EndingUserSessionRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/Platform/sessions/${ args.SessionID }`,
            this.getObjectArguments(
                requestKind,
                {
                    dummy: args.dummy,
                    message: args.message,
                    notify: args.notify,
                    'account-id': args['account-id']
                },
                'Application/json',
                { Authorization: `Bearer ${ this.token }` }
            )
        );
    }
}