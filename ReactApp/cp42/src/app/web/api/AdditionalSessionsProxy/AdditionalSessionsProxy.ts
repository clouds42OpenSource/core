import { BaseProxy } from 'app/web/common';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { BuyCloudServiceResourcesRequestDto, ChangeCloudServiceResourcesRequestDto } from 'app/web/api/AdditionalSessionsProxy/request-dto';
import { ChangeCloudServiceResourcesResponseDto, CloudServiceResourcesResponseDto } from 'app/web/api/AdditionalSessionsProxy/response-dto';
import { IdServices } from 'app/web/api/AdditionalSessionsProxy/enums/idServices';

/**
 * Прокси для работы с дополнительными сеансами
 */
export class AdditionalSessionsProxy extends BaseProxy {
    public token = localStorageHelper.getJWTToken();

    public accountId = localStorageHelper.getContextAccountId();

    /**
     * Запрос на получение количества купленных ресурсов, стоимости, дату окончания ресурсов
     * @param requestKind Вид запроса
     */
    public getCloudServiceResources(requestKind: RequestKind): Promise<CloudServiceResourcesResponseDto> {
        return this.requestSender.submitRequest<CloudServiceResourcesResponseDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/CloudServiceResources/accounts/${ this.accountId }/services/${ IdServices.additionalSessionsServices }`,
            this.getObjectArguments(requestKind, {}, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на покупку или изменение дополнительных сеансов
     * @param requestKind Вид запроса
     * @param args Id сервиса
     */
    public changeCloudServiceResources(requestKind: RequestKind, args: ChangeCloudServiceResourcesRequestDto): Promise<ChangeCloudServiceResourcesResponseDto> {
        return this.requestSender.submitRequest<ChangeCloudServiceResourcesResponseDto, ChangeCloudServiceResourcesRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/CloudServiceResources/accounts`,
            this.getObjectArguments(
                requestKind,
                {
                    accountId: this.accountId,
                    serviceId: IdServices.additionalSessionsServices,
                    count: args.count,
                    accountUserSessions: args.accountUserSessions
                },
                'Application/json',
                { Authorization: `Bearer ${ this.token }` }
            )
        );
    }

    /**
     * Запрос на покупку сервиса за счет собственных средств или за счет обещанного платежа
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public buyCloudServiceResources(requestKind: RequestKind, args: BuyCloudServiceResourcesRequestDto) {
        return this.requestSender.submitRequest<void, BuyCloudServiceResourcesRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/CloudServiceResources/payment-service`,
            this.getObjectArguments(
                requestKind,
                {
                    accountId: this.accountId,
                    serviceId: IdServices.additionalSessionsServices,
                    isPromisePayment: args.isPromisePayment,
                    costOfTariff: args.costOfTariff,
                    count: args.count,
                    accountUserSessions: args.accountUserSessions
                },
                'Application/json',
                { Authorization: `Bearer ${ this.token }` }
            )
        );
    }
}