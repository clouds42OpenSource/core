import { GetUserPermissionByTypeDataRequestDto } from 'app/web/api/GetUserPermissions/request-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { BaseProxy } from 'app/web/common';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

/**
 * Прокси для работы с счетами
 */
export class GetUserPermissionsProxy extends BaseProxy {
    private readonly accountId = localStorageHelper.getContextAccountId();

    private readonly userId = localStorageHelper.getUserId();

    private readonly token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение счетов
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public getUserPermissionByType(requestKind: RequestKind, args: GetUserPermissionByTypeDataRequestDto): Promise<string[]> {
        return this.requestSender.submitRequest<string[], GetUserPermissionByTypeDataRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountUserPermissions/${ this.userId }/GetPermissionsForContext`,
            this.getObjectArguments(requestKind, {
                Context: {
                    AccountId: this.accountId,
                    UserAccountId: this.userId
                },
                ...args
            }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}