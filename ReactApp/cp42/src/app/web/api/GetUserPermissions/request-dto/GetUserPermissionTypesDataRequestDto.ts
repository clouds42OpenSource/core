/**
 * Модель запроса к апи на получения данных о балансе аккаунта
 */
export type GetUserPermissionByTypeDataRequestDto = {
    Permissions: string[];

};