import { TasksPageDataResponseDto } from 'app/web/api/TasksPageProxy/response-dto/TasksPageDataResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с TasksPage
 */
export class TasksPageProxy extends BaseProxy {
    /**
     * Получить данные по странице задач
     * @param requestKind Вид запроса
     */
    public receiveTasksPage(requestKind: RequestKind) {
        return this.requestSender.submitRequest<TasksPageDataResponseDto>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks/visibility`,
            this.getObjectArguments(requestKind)
        );
    }
}