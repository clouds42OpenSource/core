/**
 * Модель ответа сервера с данными по странице задач
 */
export type TasksPageDataResponseDto = {
    /**
     * Признак, что кнопка "Добавить задачу" видна
     */
    isAddTaskButtonVisible: boolean;
    /**
     * Признак, что вкладка "Управление" видна
     */
    isControlTaskTabVisible: boolean;
    /**
     * Признак, что вкладка "Взаимодействие воркера и задач" видна
     */
    isWorkerAvailableTasksBagsTabVisible: boolean;
    /**
     * Признак, что можно отменять запущенную задачу
     */
    canCancelCoreWorkerTasksQueue: boolean;
};