import {
    AccountCardAccountInfoResponseDto,
    AccountCardCommonDataResponseDto,
    AccountCardEditableFieldsResponseDto,
    AccountCardLocaleInfoItemResponseDto,
    AccountCardSegmentInfoItemResponseDto,
    AccountCardTabVisibilityResponseDto,
    AccountCardVisibleFieldsResponseDto
} from 'app/web/api/AccountCardProxy/response-dto';

import { AccountCardButtonsVisibilityResponseDto } from 'app/web/api/AccountCardProxy/response-dto/AccountCardButtonsVisibilityResponseDto';

/**
 * Данные карточки аккаунта
 */
export type AccountCardDataResponseDto = {
    /**
     * Информация о видимости кнопок для карточки аккаунта
     */
    buttonsVisibility: AccountCardButtonsVisibilityResponseDto;

    /**
     * Общая информация о аккаунте
     */
    commonData: AccountCardCommonDataResponseDto;

    /**
     * Информация о аккаунте
     */
    accountInfo: AccountCardAccountInfoResponseDto;

    /**
     * Массив доступных локалей
     */
    locales: AccountCardLocaleInfoItemResponseDto[];

    /**
     * Массив доступных сегментов
     */
    segments: AccountCardSegmentInfoItemResponseDto[] | null;

    /**
     * Список полей которые настраеваемы как видимы или нет
     */
    visibleFields: AccountCardVisibleFieldsResponseDto;

    /**
     * Список полей которые настраеваемы как редактируемые или нет
     */
    editableFields: AccountCardEditableFieldsResponseDto;

    /**
     * Содержит информацию о видимости табов карточки аккаунта
     */
    tabVisibility: AccountCardTabVisibilityResponseDto;
};