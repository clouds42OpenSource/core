/**
 * Информация о акккаунте
 */
export type AccountCardAccountInfoResponseDto = {
    /**
     *  ID аккаунта
     */
    accountId: string;

    /**
     * ID сегмента
     */
    segmentID?: string;

    /**
     * Название аккаунта
     */
    accountCaption: string;

    /**
     * ИНН
     */
    accountInn: string;

    /**
     * Дата регистрации
     */
    registrationDate?: string;

    /**
     * Источник, откуда пришёл пользователь
     */
    userSource?: string;

    /**
     * Наименование денежной еденицы относительно локали
     */
    localeCurrency: string;

    /**
     * ID локали
     */
    localeId: string;

    /**
     * Описание
     */
    accountDescription: string;

    /**
     * Признак что аккаунт ВИП
     */
    isVip: boolean;

    /**
     * Номер аккаунта
     */
    indexNumber: number;

    /**
     * Ссылка на склеп
     */
    cloudStorageWebLink: string;
    supplierId?: string;
    deployment: number | null;
};