/**
 * Информация о сегменте
 */
export type AccountCardSegmentInfoItemResponseDto = {
    /**
     * ID сегмента
     */
    key: string;

    /**
     * Название сегмента
     */
    value: string;
};