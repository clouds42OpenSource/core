export type AccountCardButtonsVisibilityResponseDto = {
    isGotoToAccountButtonVisible: boolean;
};