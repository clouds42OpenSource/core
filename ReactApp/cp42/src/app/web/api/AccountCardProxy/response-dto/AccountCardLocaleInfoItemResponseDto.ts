/**
 * Информация о локали
 */
export type AccountCardLocaleInfoItemResponseDto = {
    /**
     * ID локали
     */
    key: string;

    /**
     * Название локали
     */
    value: string;
};