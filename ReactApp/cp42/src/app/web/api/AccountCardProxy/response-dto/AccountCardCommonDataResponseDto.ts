/**
 * Общая информация для карточки аккаунта
 */
export type AccountCardCommonDataResponseDto = {
    /**
     * Используемое место на диске аккаунта
     */
    usedSizeOnDisk?: number;

    /**
     * Полная сумма стоимости сервисов аккаунта
     */
    serviceTotalSum: number;

    /**
     * Тип аккаунта
     */
    accountType: string;

    /**
     * Название сейл менеджера
     */
    accountSaleManagerCaption: string;

    /**
     * Баланс аккаунта
     */
    balance: number;

    /**
     * Последняя активность
     */
    lastActivity?: string

    /**
     * Имя сегмента
     */
    segmentName: string;

    /**
     * Письма для рассылок
     */
    emails: string[];
};