import { RequestKind, WebVerb } from 'core/requestSender/enums';

import { ChangeAccountSegmentRequestDto } from 'app/web/api/AccountCardProxy/request-dto/ChangeAccountSegmentRequestDto';
import { ReceiveAccountCardRequestDto } from 'app/web/api/AccountCardProxy/request-dto/ReceiveAccountCardRequestDto';
import { UpdateAccountCardRequestDto } from 'app/web/api/AccountCardProxy/request-dto/UpdateAccountCardRequestDto';
import { AccountCardDataResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { BaseProxy } from 'app/web/common';

/**
 * Прокси работающий с Logging
 */
export class AccountCardProxy extends BaseProxy {
    /**
     * Запрос на получение карточки аккаунта
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public receiveAccountCard(requestKind: RequestKind, args: ReceiveAccountCardRequestDto) {
        return this.requestSender.submitRequest<AccountCardDataResponseDto, ReceiveAccountCardRequestDto>(
            WebVerb.GET,
            `${ this.host }/accounts/${ args.accountNumber }/details`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на обновление карточки аккаунта
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public updateAccountCard(requestKind: RequestKind, args: UpdateAccountCardRequestDto) {
        return this.requestSender.submitRequest<void, UpdateAccountCardRequestDto>(
            WebVerb.PUT,
            `${ this.host }/accounts/info`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на обновление сегмента аккаунта
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public changeAccountSegment(requestKind: RequestKind, args: ChangeAccountSegmentRequestDto) {
        return this.requestSender.submitRequest<void, ChangeAccountSegmentRequestDto>(
            WebVerb.PUT,
            `${ this.host }/accounts/segment`,
            this.getObjectArguments(requestKind, args)
        );
    }
}