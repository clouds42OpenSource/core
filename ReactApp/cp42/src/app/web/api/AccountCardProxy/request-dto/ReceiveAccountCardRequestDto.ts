export type ReceiveAccountCardRequestDto = {
    /**
     * Номер аккаунта
     */
    accountNumber: number;
};