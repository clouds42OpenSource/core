/**
 * Информация о акккаунте
 */
export type UpdateAccountCardRequestDto = {

    /**
     *  ID аккаунта
     */
    AccountId: string;

    /**
     * Название аккаунта
     */
    AccountCaption: string;

    /**
     * ИНН
     */
    AccountInn: string;

    /**
     * ID локали
     */
    LocaleId: string;

    /**
     * Описание
     */
    AccountDescription: string;

    /**
     * Признак что аккаунт ВИП
     */
    IsVip: boolean;

    /**
     * Письма для рассылок
     */
    Emails: string[];

    Deployment: number | null;
};