/**
 * Параметры на изменение сегмента аккаунта
 */
export type ChangeAccountSegmentRequestDto = {
    /**
     * Id аккаунта
     */
    accountId: string;

    /**
     * Id сегмента на который нужно сменить
     */
    segmentId: string;
};