import { Configurations1CFilterParamsDto } from 'app/web/api/Configurations1CProxy/request-dto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель параметров на запрос получения конфигураций 1С
 */
export type GetConfigurations1CParamsDto = SelectDataCommonRequestDto<Configurations1CFilterParamsDto>;