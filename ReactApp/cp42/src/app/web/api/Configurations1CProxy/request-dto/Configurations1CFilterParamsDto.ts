/**
 * Модель фильтра для выбора конфигураций 1С
 */
export type Configurations1CFilterParamsDto = {
    /**
     * Название конфигурации
     */
    ConfigurationName: string;

    /**
     * Каталог конфигурации на ИТС
     */
    ConfigurationCatalog: string;
};