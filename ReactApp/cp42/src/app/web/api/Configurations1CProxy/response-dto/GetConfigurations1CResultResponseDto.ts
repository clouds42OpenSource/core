import { Configurations1CItemResponseDto } from 'app/web/api/Configurations1CProxy/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа на получение списка конфигураций 1С
 */
export type GetConfigurations1CResultResponseDto = SelectDataMetadataResponseDto<Configurations1CItemResponseDto>;