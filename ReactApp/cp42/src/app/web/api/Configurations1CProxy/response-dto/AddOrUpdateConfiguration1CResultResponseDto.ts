import { ConfigurationReductionModelResponseDto } from 'app/web/api/Configurations1CProxy/response-dto/ConfigurationReductionModelResponseDto';

export type AddOrUpdateConfiguration1CResultResponseDto = {
    /**
     * Полные пути к архиву с информацией по обновлениям конфигурации.
     */
    Reductions: Array<ConfigurationReductionModelResponseDto>;
};