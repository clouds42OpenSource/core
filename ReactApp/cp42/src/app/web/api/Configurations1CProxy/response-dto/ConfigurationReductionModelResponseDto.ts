/**
 * Модель редакции конйигурации.
 */
export type ConfigurationReductionModelResponseDto = {
    /**
     * Каталоги редакции.
     */
    redactionCatalog: string;

    /**
     * Адрес по скачиванию карты обновлений редакций.
     */
    urlOfMapping: string;
};