import { ReceiveCloudChangesRequestDto, ReceiveLaunchDbAndRdpLogRequestDto } from 'app/web/api/LoggingProxy/request-dto';
import { CloudChangesActionResponseDto, CloudChangesDataResponseDto, LaunchDbAndRdpLogResponseDto } from 'app/web/api/LoggingProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Logging
 */
export class LoggingProxy extends BaseProxy {
    /**
     * Запрос на получение лога о запуске баз и RDP
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public receiveLaunchDbAndRdpLog(requestKind: RequestKind, args: ReceiveLaunchDbAndRdpLogRequestDto) {
        return this.requestSender.submitRequest<LaunchDbAndRdpLogResponseDto, ReceiveLaunchDbAndRdpLogRequestDto>(
            WebVerb.GET,
            `${ this.host }/logs/launch-db-and-start-rdp`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на получение логов действий облака
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public receiveCloudChanges(requestKind: RequestKind, args: ReceiveCloudChangesRequestDto) {
        return this.requestSender.submitRequest<CloudChangesDataResponseDto, ReceiveCloudChangesRequestDto>(
            WebVerb.GET,
            `${ this.host }/logs/cloud-changes`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Выполнить запрос на получение данных по действиям логов
     * @param requestKind Вид запроса
     */
    public receiveCloudChangesActionsData(requestKind: RequestKind) {
        return this.requestSender.submitRequest<Array<CloudChangesActionResponseDto>>(
            WebVerb.GET,
            `${ this.host }/logs/cloud-changes-action`,
            this.getObjectArguments(requestKind)
        );
    }
}