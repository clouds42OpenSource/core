import { LaunchDbAndRdpLogItemResponseDto } from 'app/web/api/LoggingProxy/response-dto';
import { MetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении логирования запуска баз и открытия RDP
 */
export type LaunchDbAndRdpLogResponseDto = {
    /**
     * Массив данных о логировании запуска баз и открытия RDP
     */
    records: LaunchDbAndRdpLogItemResponseDto[];
    /**
     * Информация о страницах логирования запуска баз и открытия RDP
     */
    metadata: MetadataResponseDto;
};