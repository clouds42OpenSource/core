import { CloudChangesItemResponseDto } from 'app/web/api/LoggingProxy/response-dto/CloudChangesItemResponseDto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении данных по логам действий облака
 */
export type CloudChangesDataResponseDto = SelectDataMetadataResponseDto<CloudChangesItemResponseDto>;