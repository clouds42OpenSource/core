/**
 * Модель ответа на запрос получения данных по действиям логов
 */
export type CloudChangesActionResponseDto = {
    /**
     * Id действия
     */
    key: string;
    /**
     * Значение действия/название
     */
    value: string;
    /**
     * Группа действия
     */
    group: string;
};