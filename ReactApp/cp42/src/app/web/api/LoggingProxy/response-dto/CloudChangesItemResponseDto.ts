/**
 * Модель ответа с элементом лога действя облака
 */
export type CloudChangesItemResponseDto = {
    /**
     * Номер аккаунта
     */
    accountNumber: number;
    /**
     * Дата создания лога
     */
    createCloudChangeDate: string;
    /**
     * Id лога
     */
    cloudChangeId: string;
    /**
     * Название действия
     */
    cloudChangesActionName: string;
    /**
     * Описание действия
     */
    cloudChangesActionDescription: string;
    /**
     * Инициатор
     */
    initiatorLogin: string;
};