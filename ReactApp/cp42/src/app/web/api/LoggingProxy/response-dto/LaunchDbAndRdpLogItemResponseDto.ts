import { DbLaunchType, LinkAppType } from 'app/common/enums';
import { DbRdpActionType } from 'app/common/enums/DbRdpActionType';
import { Nullable } from 'app/common/types';

/**
 * Модель ответа данных логирования запуска баз и RDP
 */
export type LaunchDbAndRdpLogItemResponseDto = {
    /**
     * ID записи
     */
    id: number;
    /**
     * Индекс/Номер аккаунта
     */
    accountNumber: number;
    /**
     * Дата создания записи ex: /Date(1238626800000)/
     */
    actionCreated: string;
    /**
     * Id действия логирования (запуск базы, открытия RDP, ...)
     */
    action: DbRdpActionType;
    /**
     * Логин кто выполнял действие
     */
    login: string;
    /**
     * Версия Линка в котором выполняли действие
     */
    linkAppVersion: string;
    /**
     * Тип Линка
     */
    linkAppType: LinkAppType;
    /**
     * Номер базы
     */
    v82Name: string;
    /**
     * Тип запуска базы (Тонкий клиент, толстый клиент, ...)
     */
    launchType: DbLaunchType;
    /**
     * Внешний IP адрес
     */
    externalIpAddress: Nullable<string>;
    /**
     * Внутренний IP адрес
     */
    internalIpAddress: Nullable<string>;
};