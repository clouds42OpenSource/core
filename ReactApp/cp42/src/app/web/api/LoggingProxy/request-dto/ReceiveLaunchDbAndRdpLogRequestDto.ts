import { ReceiveLaunchDbAndRdpLogFilterRequestDto } from 'app/web/api/LoggingProxy/request-dto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель запроса для получения логирования запуска баз и RDP
 */
export type ReceiveLaunchDbAndRdpLogRequestDto = SelectDataCommonRequestDto<ReceiveLaunchDbAndRdpLogFilterRequestDto>;