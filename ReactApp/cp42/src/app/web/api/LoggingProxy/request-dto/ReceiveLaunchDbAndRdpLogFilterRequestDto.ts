import { Nullable } from 'app/common/types';

/**
 * Модель фильтра при получении логирования запуска баз и RDP
 */
export type ReceiveLaunchDbAndRdpLogFilterRequestDto = {
    /**
     * Номер аккаунта
     */
    AccountNumber: number;

    /**
     * Логин
     */
    Login: string;

    /**
     * Номер информационной базы
     */
    V82Name: string;

    /**
     * Версия линка
     */
    LinkAppVersion: string;

    /**
     * Внешний IP адрес
     */
    ExternalIpAddress: Nullable<string>;

    /**
     * Внутренний IP адрес
     */
    InternalIpAddress: Nullable<string>;
};