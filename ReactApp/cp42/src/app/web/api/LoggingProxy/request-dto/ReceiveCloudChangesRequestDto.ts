import { ReceiveCloudChangesFilterRequestDto } from 'app/web/api/LoggingProxy/request-dto/ReceiveCloudChangesFilterRequestDto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель запроса для получения логов действий облака
 */
export type ReceiveCloudChangesRequestDto = SelectDataCommonRequestDto<ReceiveCloudChangesFilterRequestDto>;