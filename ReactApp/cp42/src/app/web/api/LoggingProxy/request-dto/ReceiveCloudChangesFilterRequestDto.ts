import { Nullable } from 'app/common/types';

/**
 * Модель фильтра для получения логов действий облака
 */
export type ReceiveCloudChangesFilterRequestDto = {
    /**
     * ID действия облака
     */
    CloudChangesActionId: string;
    /**
     * Строка поиска
     */
    SearchLine: Nullable<string>;
    /**
     * Дата создания действия с
     */
    CreateCloudChangeDateFrom: Nullable<string>;
    /**
     * Дата создания действия по
     */
    CreateCloudChangeDateTo: Nullable<string>;
    /**
     * Показывать только мои аккаунты
     */
    ShowMyAccountsOnly: boolean;
    /**
     * Показывать только вип аккаунты
     */
    ShowVipAccountsOnly: boolean;
};