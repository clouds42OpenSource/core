import { AgencyAgreementItemResponseDto } from 'app/web/api/AgencyAgreementProxy/response-dto/AgencyAgreementItemResponseDto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа на получение списка агентских соглашений
 */
export type GetAgencyAgreementResultResponseDto = SelectDataMetadataResponseDto<AgencyAgreementItemResponseDto>;