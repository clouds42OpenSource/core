/**
 * Модель ответа на добавление агентского соглашения
 */
export type AddAgencyAgreementResultResponseDto = {
    /**
     * ID агентского соглашения.
     */
    id: string;
};