/**
 * Модель фильтра для выбора агентских соглашений
 */
export type AgencyAgreementFilterParamsDto = {
    /**
     * Название агентского соглашения
     */
    name: string;
};