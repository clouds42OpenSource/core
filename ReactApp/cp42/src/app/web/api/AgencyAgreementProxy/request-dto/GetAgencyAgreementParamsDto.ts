import { AgencyAgreementFilterParamsDto } from 'app/web/api/AgencyAgreementProxy/request-dto/AgencyAgreementFilterParamsDto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель параметров на запрос получения агентских соглашений
 */
export type GetAgencyAgreementParamsDto = SelectDataCommonRequestDto<AgencyAgreementFilterParamsDto>;