import { AddAgencyAgreementRequestDto, GetAgencyAgreementParamsDto } from 'app/web/api/AgencyAgreementProxy/request-dto';
import { GetAgencyAgreementResultResponseDto } from 'app/web/api/AgencyAgreementProxy/response-dto';
import { AddAgencyAgreementResultResponseDto } from 'app/web/api/AgencyAgreementProxy/response-dto/AddAgencyAgreementResultResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с AgencyAgreement
 */
export class AgencyAgreementProxy extends BaseProxy {
    /**
     * Запрос на получение агентских соглашений
     * @param requestKind Вид запроса
     * @param args параметры на получение списка агентских соглашений
     */
    public receiveAgencyAgreements(requestKind: RequestKind, args: GetAgencyAgreementParamsDto) {
        return this.requestSender.submitRequest<GetAgencyAgreementResultResponseDto, GetAgencyAgreementParamsDto>(
            WebVerb.GET,
            `${ this.host }/agency-agreements`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на добавление агентского соглашения
     * @param requestKind Вид запроса
     * @param args параметры на добавление агентского соглашения
     */
    public addAgencyAgreement(requestKind: RequestKind, args: AddAgencyAgreementRequestDto) {
        return this.requestSender.submitRequest<AddAgencyAgreementResultResponseDto, AddAgencyAgreementRequestDto>(
            WebVerb.POST,
            `${ this.host }/agency-agreements`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на открытие агентского соглашения
     * @param args параметры для открытия агентского соглашения
     */
    public openAgencyAgreement(args: { agencyAgreementId: string }) {
        window.open(`${ this.host }/agency-agreements/${ args.agencyAgreementId }/print`, '_blank');
    }
}