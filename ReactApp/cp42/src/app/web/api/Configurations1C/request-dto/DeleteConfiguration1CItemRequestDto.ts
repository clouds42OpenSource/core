/**
 * Модель на удаление конфигурации 1С
 */
export type DeleteConfiguration1CItemRequestDto = {
    /**
     * Название конфигурации
     */
    ConfigurationName: string;
};