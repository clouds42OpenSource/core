/**
 * Модель фильтра для выбора конфигураций 1С
 */
export type Configurations1CFilterParamsDto = {
    /**
     * Название конфигурации
     */
    name: string;

    /**
     * Каталог конфигурации на ИТС
     */
    configurationCatalog: string;
};