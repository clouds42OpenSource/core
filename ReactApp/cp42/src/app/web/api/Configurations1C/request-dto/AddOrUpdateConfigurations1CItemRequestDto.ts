/**
 * Модель на обновление или добавления конфигурации 1С
 */
export type AddOrUpdateConfiguration1CItemRequestDto = {
    /**
     * Название конфигурации
     */
    Name: string;

    /**
     * Каталог конфигурации на ИТС
     */
    ConfigurationCatalog: string;

    /**
     * Редакция
     */
    RedactionCatalogs: string;

    /**
     * Платформа
     */
    PlatformCatalog: string;

    /**
     * Адрес каталога обновлений.
     */
    UpdateCatalogUrl: string;

    /**
     * Краткий код конфигурации, например bp, zup и тд.
     */
    ShortCode: string | null;

    /**
     * Признак что необходимо использовать COM соединения для принятия обновлений в базе
     */
    UseComConnectionForApplyUpdates: boolean;

    /**
     * Признак необходимости проверять наличие обновлений
     */
    NeedCheckUpdates: boolean;

    /**
     * Стоимость конфигурации
     */
    ConfigurationCost: number;

    /**
     * Вариации имени конфигурации 1С
     */
    ConfigurationVariations: Array<string>;
};