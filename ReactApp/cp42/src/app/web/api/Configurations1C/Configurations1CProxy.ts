import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { GetConfigurations1CParamsDto } from 'app/web/api/Configurations1C/request-dto';
import { AddOrUpdateConfiguration1CItemRequestDto } from 'app/web/api/Configurations1C/request-dto/AddOrUpdateConfigurations1CItemRequestDto';
import { DeleteConfiguration1CItemRequestDto } from 'app/web/api/Configurations1C/request-dto/DeleteConfiguration1CItemRequestDto';
import { GetConfigurations1CResultResponseDto } from 'app/web/api/Configurations1C/response-dto';
import { AddOrUpdateConfiguration1CResultResponseDto } from 'app/web/api/Configurations1C/response-dto/AddOrUpdateConfiguration1CResultResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Configurations1C
 */
export class Configurations1CProxy extends BaseProxy {
    private token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение конфигураций 1С
     * @param requestKind Вид запроса
     * @param args параметры на получение списка конфигураций 1С
     */
    public receiveConfigurations1C(requestKind: RequestKind, args: GetConfigurations1CParamsDto) {
        return this.requestSender.submitRequest<GetConfigurations1CResultResponseDto, GetConfigurations1CParamsDto>(
            WebVerb.GET,
            `${ this.host }/configuration-1c`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на редактирование конфигурации 1С
     * @param requestKind Вид запроса
     * @param args параметры на обновление конфигурации 1С
     */
    public updateConfiguration1C(requestKind: RequestKind, args: AddOrUpdateConfiguration1CItemRequestDto) {
        return this.requestSender.submitRequest<AddOrUpdateConfiguration1CResultResponseDto, AddOrUpdateConfiguration1CItemRequestDto>(
            WebVerb.PUT,
            `${ this.host }/configuration-1c`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на добавление конфигурации 1С
     * @param requestKind Вид запроса
     * @param args параметры на добавление конфигурации 1С
     */
    public addConfiguration1C(requestKind: RequestKind, args: AddOrUpdateConfiguration1CItemRequestDto) {
        return this.requestSender.submitRequest<AddOrUpdateConfiguration1CResultResponseDto, AddOrUpdateConfiguration1CItemRequestDto>(
            WebVerb.POST,
            `${ this.host }/configuration-1c`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на добавление конфигурации 1С
     * @param requestKind Вид запроса
     * @param args параметры на добавление конфигурации 1С
     */
    public deleteConfiguration1C(requestKind: RequestKind, args: DeleteConfiguration1CItemRequestDto) {
        return this.requestSender.submitRequest<void, DeleteConfiguration1CItemRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/configuration-1c`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` }),
            true
        );
    }
}