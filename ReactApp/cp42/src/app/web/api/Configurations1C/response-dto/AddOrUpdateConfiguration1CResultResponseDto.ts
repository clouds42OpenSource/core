import { ConfigurationReductionModelResponseDto } from 'app/web/api/Configurations1C/response-dto/ConfigurationReductionModelResponseDto';

/**
 * Модель ответа на обновление или удаление конфигурации 1С
 */
export type AddOrUpdateConfiguration1CResultResponseDto = {
    /**
     * Полные пути к архиву с информацией по обновлениям конфигурации.
     */
    Message?: string;
    message?: string;
    reductions: Array<ConfigurationReductionModelResponseDto>;
};