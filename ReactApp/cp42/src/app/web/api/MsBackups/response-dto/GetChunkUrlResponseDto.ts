type PartsType = {
    url: string;
    headers: {
        Authorization: string;
        'x-amz-content-sha256': string;
        'x-amz-date': string;
    },
    number: number;
};

export type GetChunkUrlResponseDto = {
    parts: PartsType[];
};