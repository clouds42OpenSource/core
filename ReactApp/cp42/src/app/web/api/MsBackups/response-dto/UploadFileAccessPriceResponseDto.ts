import { Nullable } from 'app/common/types';

export type AccountUserLicenceType = {
    AccountUserId: string,
    HasLicense: boolean,
    AccessCost: number
};

export type UploadFileAccessPriceResponseDto = {
    Success: boolean;
    Message: Nullable<string>;
    Data: {
        ServiceTypesId: string;
        AccountUserLicence: AccountUserLicenceType[];
    }
};