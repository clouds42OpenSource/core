import { Nullable } from 'app/common/types';

/**
 * Модель ответа расчета стоимости создаваемой ИБ
 */
export type CostOfCreatingDatabasesResponseDto = {
    Success: boolean;
    Message: Nullable<string>;
    Data: {
        Balance: number;
        Complete: boolean;
        Error: Nullable<string>;
        EnoughMoney: number;
        CanGetPromisePayment: boolean;
        CostOftariff: number;
        MonthlyCost: number;
        Currency: string;
    }
};