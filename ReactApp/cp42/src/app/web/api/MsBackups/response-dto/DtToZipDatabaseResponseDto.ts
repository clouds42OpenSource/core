export type DtToZipDatabaseResponseDto = {
    Success: boolean;
    Data: {
        IsComplete: boolean;
        NotEnoughMoney: string;
        Amount: string;
        CanUsePromisePayment: boolean;
    } | null;
    Message?: string;
};