type UserModelProfiles = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

type UserModel = {
    name: string;
    login: string;
    'db-user-id': string;
    'catalog-user-id': string;
    'account-user-id': string;
    'is-admin': boolean;
    profiles: UserModelProfiles[];
};

type ExtensionsModel = {
    id: string;
    name: string;
    version: string;
    'modifies-data-structure': boolean;
    supported: boolean;
};

type ProfileModel = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

/**
 * Модель ответа информации о файле резервной копии
 */
export type FilesResponseDto = {
    configuration: {
        id: string;
        version: string;
        name: string;
        'version-supported': boolean;
        'configuration-supported': boolean;
    };
    user: UserModel[];
    extensions: ExtensionsModel[];
    errors: string[];
    profiles: ProfileModel[];
    type: string;
    'strong-passwords': boolean;
};