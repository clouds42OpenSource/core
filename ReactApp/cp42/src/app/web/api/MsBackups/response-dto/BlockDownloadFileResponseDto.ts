type PartsType = {
    url: string;
    headers: {
        Authorization: string;
        'x-amz-content-sha256': string;
        'x-amz-date': string;
    },
    number: number;
};

/**
 * Модель данных возвращаемая МСом при инициализации блочной загрузки файла в хранилище
 */
export type BlockDownloadFileResponseDto = {
    'storage-type': string;
    id: string;
    'part-size': number;
    parts: PartsType[];
};