import { Nullable } from 'app/common/types';

/**
 * Модель ответа на создание ИБ из zip
 */
export type ZipDatabaseResponceDto = {
    Success: boolean;
    Message: Nullable<string>;
    Data: {
        ErrorMessage: Nullable<string>;
        IsComplete: boolean;
        NotEnoughMoney: number;
        Amount: number;
        Currency: Nullable<string>;
        CanUsePromisePayment: boolean;
    };
};