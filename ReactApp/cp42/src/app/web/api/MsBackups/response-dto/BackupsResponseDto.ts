export type BackupsModel = {
    id: string;
    'file-id': string;
    'file-size': string;
    date: string;
    'app-version': string;
    'is-original': boolean;
    'is-ondemand': boolean;
    'is-annual': boolean;
    'is-mounthly': boolean;
    'is-daily': boolean;
    'is-eternal': boolean;
    'eternal-backup-url': string;
    type: string;
    comment: string;
};

/**
 * Модель данных списка резервных копий
 */
export type BackupsResponseDto = {
    backups: BackupsModel[];
};