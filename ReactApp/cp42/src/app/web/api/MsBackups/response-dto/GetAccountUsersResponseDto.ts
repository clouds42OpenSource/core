type AccountUserListItem = {
    ID: string;
    AccountID: string;
    Login: string;
    Email: string;
    FullPhoneNumber: string;
    FirstName: string;
    LastName: string;
    MiddleName: string;
    CorpUserID: string;
    CorpUserSyncStatus: string;
    Removed: boolean;
    CreationDate: string;
    IsManager: boolean;
    Activated: boolean;
};

/**
 * Модель данных при получении списка пользователей аккаунта по идентификатору
 */
export type GetAccountUsersResponseDto = {
    AccountUserList: AccountUserListItem[];
};