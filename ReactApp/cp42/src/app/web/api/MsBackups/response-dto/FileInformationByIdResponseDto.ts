type ConfigurationModel = {
    /**
     * Код конфигурации
     */
    id: string;
    /**
     * Версия конфигурации
     */
    version: string;
    /**
     * Наименование конфигурации
     */
    name: string;
    /**
     * Поддержка версии конфигурации в сервисе
     */
    'version-supported': boolean;
    /**
     * Поддержка конфигурации в сервисе
     */
    'configuration-supported': boolean;
};

type ProfilesModel = {
    /**
     * Идентификатор поставляемых данных профиля
     */
    id: string;
    /**
     * Имя профиля
     */
    name: string;
    /**
     * Наличие у профиля админ прав
     */
    admin: boolean;
    /**
     * Поставляемый профиль
     */
    predefined: boolean;
    default: boolean;
};

type UserModel = {
    /**
     * Наименование пользовательского дампа
     */
    name: string;
    /**
     * Имя пользователя ИБ
     */
    login: string;
    /**
     * Идентификатор пользователя ИБ
     */
    'data-dump-user-id': string;
    /**
     * Идентификатор пользователя в справочнике
     */
    'catalog-user-id': string;
    /**
     * Идентификатор пользователя в ЛК
     */
    'account-user-id': string;
    /**
     * Наименование пользователя в ЛК
     */
    'account-user-name': string;
    /**
     * Статус админа ИБ
     */
    'is-admin': boolean;
    /**
     * Список профилей групп доступа
     */
    profiles: ProfilesModel[];
    /**
     * У пользователя возможен вход по паролю
     */
    eauth: boolean;
    /**
     * У пользователя установлен пароль
     */
    'has-password': boolean;
    /**
     * SHA-1 пароля
     */
    hash1: string;
    /**
     * SHA-1 пароля в ВЕРХНЕМ регистре
     */
    hash2: string;
    salt: string;
    alg: 'SHA1' | 'SHA256' | 'SHA512' | 'PBKDF2SHA256' | '';
};

type ExtensionsModel = {
    /**
     * Идентификатор расширения в архиве
     */
    id: string;
    /**
     * Имя расширения
     */
    name: string;
    /**
     * Версия расширения
     */
    version: string;
    /**
     * Расширение изменяет структуру данных
     */
    'modifies-data-structure': boolean;
    /**
     * Расширение поддерживается в сервисе
     */
    supported: boolean;

    'audit-failed': boolean;

    information: string;

    'service-id': string;

    'service-string-id': string;

    'service-is-paid': boolean;
};

type StatusModel = {
    ready?: boolean;
    timeout?: number;
}

/**
 * Модель данных информации о файле
 */
export type FileInformationByIdResponseDto = {
    type: string;
    status?: StatusModel;
    /**
     * Конфигурация
     */
    configuration: ConfigurationModel;
    users: UserModel[];
    /**
     * Список профилей групп доступа
     */
    profiles: ProfilesModel[];
    /**
     * Расширения в архиве
     */
    extensions: ExtensionsModel[];
    /**
     * Список ошибок в архивной резервной копии
     */
    errors: string[];
    messages: string[];
    target: string;
    'strong-passwords': boolean;
};