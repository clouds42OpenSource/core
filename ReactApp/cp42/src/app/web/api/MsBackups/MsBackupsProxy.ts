import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import {
    ApplicationsRequestDto,
    BackupsRequestDto,
    BlockDownloadFileDto,
    DeleteFilesRequestDto,
    DownloadFilesRequestDto,
    EndDownloadFile,
    FileInformationByIdRequestDto,
    FilesRequestDto,
    GetChunksUrlRequestDto
} from 'app/web/api/MsBackups/request-dto';
import {
    BackupsResponseDto,
    BlockDownloadFileResponseDto,
    FileInformationByIdResponseDto,
    FilesResponseDto,
    GetChunkUrlResponseDto
} from 'app/web/api/MsBackups/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с бекапами разделителей через МС
 */
export class MsBackupsProxy extends BaseProxy {
    private token = localStorageHelper.getJWTToken();

    private accountId = localStorageHelper.getContextAccountId();

    /**
     * Запрос на получение информации о файле сервиса
     */
    public getFile(requestKind: RequestKind, args: FilesRequestDto) {
        return this.requestSender.submitRequest<FilesResponseDto>(
            WebVerb.GET,
            `${ this.host }/Platform/files/${ args.fileId }`,
            this.getObjectArguments(requestKind, {}, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение информации о файле резервной копии
     */
    public getFileInfoById(requestKind: RequestKind, args: FileInformationByIdRequestDto) {
        return this.requestSender.submitRequest<FileInformationByIdResponseDto>(
            WebVerb.GET,
            `${ this.host }/Platform/files/${ args.fileId }`,
            this.getObjectArguments(requestKind, { type: args.type }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение ссылки на скачивание файла резервной копии
     */
    public getDownloadFiles(requestKind: RequestKind, args: DownloadFilesRequestDto) {
        return fetch(`${ this.host }/Platform/files/${ args.fileId }/download`, {
            method: WebVerb.GET,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ this.token }`
            }
        });
    }

    /**
     * Запрос на получение списка резервных копий приложения
     */
    public getBackups(requestKind: RequestKind, args: BackupsRequestDto) {
        return this.requestSender.submitRequest<BackupsResponseDto, BackupsRequestDto>(
            WebVerb.GET,
            `${ this.host }/Platform/applications/${ args.applicationID }/backups`,
            this.getObjectArguments(requestKind, {
                to_date: args.to_date,
                from_date: args.from_date
            }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос создает новое приложение из резервной копии
     */
    public postApplications(requestKind: RequestKind, args: ApplicationsRequestDto) {
        return this.requestSender.submitRequest<void, ApplicationsRequestDto>(
            WebVerb.POST,
            `${ this.host }/Platform/applications`,
            this.getObjectArguments(requestKind, { 'account-id': this.accountId, ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос инициализирует блочную загрузку файла в хранилище
     */
    public postFile(requestKind: RequestKind, args: BlockDownloadFileDto) {
        return this.requestSender.submitRequest<BlockDownloadFileResponseDto, BlockDownloadFileDto>(
            WebVerb.POST,
            `${ this.host }/Platform/files`,
            this.getObjectArguments(requestKind, { 'account-id': this.accountId, ...args }, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на прерывание загрузки файла
     */
    public deleteFile(requestKind: RequestKind, args: DeleteFilesRequestDto) {
        return this.requestSender.submitRequest<object, DeleteFilesRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/Platform/files/${ args.fileId }`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на завершение загрузки файла
     */
    public endDownloadFile(requestKind: RequestKind, args: EndDownloadFile) {
        return this.requestSender.submitRequest<object, EndDownloadFile>(
            WebVerb.POST,
            `${ this.host }/Platform/files/${ args.fileId }`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public getChunkUrls(requestKind: RequestKind, args: GetChunksUrlRequestDto) {
        return this.requestSender.submitRequest<GetChunkUrlResponseDto, GetChunksUrlRequestDto>(
            WebVerb.POST,
            `${ this.host }/Platform/files/${ args.fileId }`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}