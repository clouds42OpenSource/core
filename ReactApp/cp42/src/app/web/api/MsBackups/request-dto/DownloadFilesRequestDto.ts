/**
 * Модель данных для получения ссылки на скачивание
 */
export type DownloadFilesRequestDto = {
    fileId: string;
};