/**
 * Модель данных для получения списка резервных копий приложения
 */
export type BackupsRequestDto = {
    applicationID?: string;
    'from_date'?: string;
    'to_date'?: string;
};