/**
 * Модель на расчет стоимости создаваемой ИБ
 */
export type CostOfCreatingDatabasesRequestDto = {
    DatabasesCount: string;
};