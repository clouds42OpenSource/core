export type ProfilesModel = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

export type UsersModel = {
    'data-dump-user-id': string;
    'catalog-user-id': string;
    'account-user-id': string;
    role: string;
    profiles: ProfilesModel[];
};

/**
 * Модель данных на создание нового приложения из резервной копии
 */
export type ApplicationsRequestDto = {
    name: string;
    configuration: string;
    users: UsersModel[];
    'file-id': string;
};