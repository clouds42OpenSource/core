export type ProfilesItem = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

export type UsersItem = {
    'data-dump-user-id': string;
    'catalog-user-id': string;
    'account-user-id': string;
    profiles: ProfilesItem[];
};

export type DtToZipDatabaseRequestDto = {
    name: string;
    configuration : string;
    'databases-service-id': string;
    login: string;
    password: string;
    'file-id': string;
    NeedUsePromisePayment: boolean;
    users: UsersItem[];
};