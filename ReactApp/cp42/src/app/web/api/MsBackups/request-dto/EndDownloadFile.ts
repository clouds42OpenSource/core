/**
 * Модель данных для завершения загрузки файла
 */
export type EndDownloadFile = {
    fileId: string;
    parts: string[];
};