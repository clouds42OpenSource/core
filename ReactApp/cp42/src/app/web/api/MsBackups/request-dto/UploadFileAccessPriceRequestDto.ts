/**
 * Модель данных для получения информации о стоимости добавления пользователя в ИБ при создании с зип
 */
export type UploadFileAccessPriceRequestDto = {
    configurationId: string;
    configurationName: string;
};