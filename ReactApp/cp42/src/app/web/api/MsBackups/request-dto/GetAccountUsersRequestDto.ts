/**
 * Модель данных на получение списка пользователей аккаунта
 */
export type GetAccountUsersRequestDto = {
    AccountId: string;
};