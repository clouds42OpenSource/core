/**
 * Модель данных на получение информации о файле
 */
export type FileInformationByIdRequestDto = {
    fileId: string;
    type: string;
};