export type GetChunksUrlRequestDto = {
    fileId: string;
    'parts-from': number;
    'parts-to': number;
};