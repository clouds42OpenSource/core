/**
 * Модель данных для прерывания загрузки файла
 */
export type DeleteFilesRequestDto = {
    fileId: string;
};