/**
 * Модель данных для получения информации о файле резервной копии
 */
export type FilesRequestDto = {
    fileId: string;
};