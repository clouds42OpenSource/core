/**
 * Модель данных для инициализации блочной загрузки файла в хранилище
 */
export type BlockDownloadFileDto = {
    size: number;
    'part-size'?: number;
    name?: string;
    type: string;
};