type ProfileItemType = {
    'id': string;
    'name': string;
    'admin': boolean;
    'predefined': boolean;
};

type UserItemType = {
    'data-dump-user-id': string;
    'catalog-user-id': string;
    'account-user-id': string;
    'profiles': ProfileItemType[];
};

/**
 * Модель на создание ИБ из зипа
 */
export type ZipDatabaseRequestDto = {
    'NeedUsePromisePayment': boolean;
    'databases-service-id': string;
    'name': string;
    'configuration': string;
    'file-id' : string;
    'users': UserItemType[];
    'extensions': { id: string }[];
};