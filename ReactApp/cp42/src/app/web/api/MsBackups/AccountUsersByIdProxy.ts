import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { CostOfCreatingDatabasesRequestDto, DtToZipDatabaseRequestDto, GetAccountUsersRequestDto, UploadFileAccessPriceRequestDto, ZipDatabaseRequestDto } from 'app/web/api/MsBackups/request-dto';
import { CostOfCreatingDatabasesResponseDto, DtToZipDatabaseResponseDto, GetAccountUsersResponseDto, UploadFileAccessPriceResponseDto, ZipDatabaseResponceDto } from 'app/web/api/MsBackups/response-dto';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

/**
 * Прокси для работы со списком пользователя аккаунта
 */
export class AccountUsersByIdProxy extends BaseProxy {
    private token = localStorageHelper.getJWTToken();

    private accountId = localStorageHelper.getContextAccountId();

    /**
     * Запрос на получение списка пользователей аккаунта по идентификатору
     */
    public getAccountUsers(requestKind: RequestKind) {
        return this.requestSender.submitRequest<GetAccountUsersResponseDto, GetAccountUsersRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountUsers/GetAccountUsers`,
            this.getObjectArguments(requestKind, { AccountId: this.accountId ?? '' }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на расчет стоимости создания ИБ
     */
    public costOfCreatingDatabases(requestKind: RequestKind, count?: string) {
        return this.requestSender.submitRequest<CostOfCreatingDatabasesResponseDto, CostOfCreatingDatabasesRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/${ this.accountId }/CostOfCreatingDatabases`,
            this.getObjectArguments(requestKind, { DatabasesCount: count ?? '1' }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на создание ИБ из zip
     */
    public zipDatabase(requestKind: RequestKind, args: ZipDatabaseRequestDto) {
        return this.requestSender.submitRequest<ZipDatabaseResponceDto, ZipDatabaseRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/ZipDatabase`,
            this.getObjectArguments(requestKind, { 'account-id': this.accountId, ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на создание ИБ из дт в зип
     */
    public dtToZipDatabase(requestKind: RequestKind, args: DtToZipDatabaseRequestDto) {
        return this.requestSender.submitRequest<DtToZipDatabaseResponseDto, DtToZipDatabaseRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/dtTozipDatabase`,
            this.getObjectArguments(requestKind, { 'account-id': this.accountId, ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос стоимость добавления пользователя в новую иб из зип
     */
    public uploadFileAccessPrice(requestKind: RequestKind, args: UploadFileAccessPriceRequestDto) {
        return this.requestSender.submitRequest<UploadFileAccessPriceResponseDto, UploadFileAccessPriceRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/UploadFile/Access-price`,
            this.getObjectArguments(requestKind, { accountId: this.accountId, ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}