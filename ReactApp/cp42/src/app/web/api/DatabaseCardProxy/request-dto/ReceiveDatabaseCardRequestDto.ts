export type ReceiveDatabaseCardRequestDto = {
    /**
     * Номер информационной базы
     */
    accountDatabaseNumber: string;
}