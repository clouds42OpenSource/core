import { getFetchedData } from 'app/api';
import { ReceiveDatabaseCardRequestDto } from 'app/web/api/DatabaseCardProxy/request-dto/ReceiveDatabaseCardRequestDto';
import { DatabaseCardResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с DatabaseCard
 */
export class DatabaseCardProxy extends BaseProxy {
    /**
     * Запрос на получение карточки информационной базы
     * @param requestKind Вид запроса
     * @param args параметры на получение карточки информационной базы
     */
    public async getDatabaseCard(requestKind: RequestKind, args: ReceiveDatabaseCardRequestDto) {
        const { responseBody } = await getFetchedData<DatabaseCardResponseDto, void>({
            url: `${ this.host }/account-databases/${ args.accountDatabaseNumber }`,
            method: WebVerb.GET,
        });

        return responseBody;
    }
}