/**
 * Модель содержащая информацию о видимости табах в карточке информационной базы
 */
export type DatabaseCardTabVisibilityResponseDto = {
    /**
     * Если true, то таб с бекапами виден, иначе false
     */
    isBackupTabVisible: boolean;
    /**
     * Если true, то таб с техподдержкой виден, иначе false
     */
    isTehSupportTabVisible: boolean;
};