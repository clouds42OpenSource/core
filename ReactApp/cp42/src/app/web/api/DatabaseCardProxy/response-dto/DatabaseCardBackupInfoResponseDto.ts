/**
 * Бекап информационной базы
 */
export type DatabaseCardBackupInfoResponseDto = {
    /**
     * Id бекапа
     */
    id: string;

    /**
     * Инициатор
     */
    initiator: string;

    /**
     * Дата создания бекапа
     */
    creationDate: string;

    /**
     * Путь к бекапу
     */
    backupPath: string;

    /**
     * Тригер события
     */
    eventTrigger: string;

    /**
     * Описание тригера события
     */
    eventTriggerDescription: string;

    /**
     * База находится на разделителях
     */
    isDbOnDelimiters: boolean;

    /**
     * Источник хранения бекапа
     */
    sourceType: string;
};