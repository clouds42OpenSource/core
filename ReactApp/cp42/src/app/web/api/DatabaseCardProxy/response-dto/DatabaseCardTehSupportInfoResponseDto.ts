import { SupportState } from 'app/common/enums/SupportState';

/**
 * Информация о подключении информационной базы к АО
 */
export type DatabaseCardTehSupportInfoResponseDto = {
    /**
     * Если true, значит идёт аутентификация к базе или уже пройдена
     */
    isConnects: boolean;

    /**
     * Последняя дата тех-поддержки
     */
    lastHistoryDate?: string;

    /**
     * Состояние базы на тех-подержке
     */
    supportState: SupportState;

    /**
     * Описание состояния
     */
    supportStateDescription: string;
};