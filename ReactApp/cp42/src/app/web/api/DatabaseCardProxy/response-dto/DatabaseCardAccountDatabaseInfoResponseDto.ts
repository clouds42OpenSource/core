import { AccountDatabaseRestoreModelType } from 'app/common/enums/AccountDatabaseRestoreModelType';
import { DatabaseState } from 'app/common/enums/DatabaseState';
import { DbPublishState } from 'app/common/enums/DbPublishState';
import { DistributionType } from 'app/common/enums/DistributionType';
import { PlatformType } from 'app/common/enums/PlatformType';

/**
 * Модель отображения информационной базы
 */
export type DatabaseCardAccountDatabaseInfoResponseDto = {
    /**
     * Id базы
     */
    id: string;

    /**
     * Название базы
     */
    v82Name: string;

    /**
     * Описание
     */
    caption: string;

    /**
     * Размер базы (МБ)
     */
    sizeInMB: number;

    /**
     * Статус базы
     */
    state: DatabaseState;
    accountId: string;

    /**
     * Комментарий по созданию базы
     */
    createAccountDatabaseComment: string;

    /**
     * Название шаблона
     */
    templateName: string;

    /**
     * Ссылка картинки шаблона
     */
    templateImgUrl: string;

    /**
     * Статус публикации
     */
    publishState: DbPublishState;

    /**
     * Ссылка веб публикации
     */
    webPublishPath: string;

    /**
     * Признак необходимости показывать ссылку
     */
    needShowWebLink: boolean;

    /**
     * Дата последней активности
     */
    lastActivityDate: string;

    /**
     * Временная зона
     */
    timezoneName: string;
    /**
     * База удалена
     */
    isDeleted: boolean;

    /**
     * Версия
     */
    version: string;

    /**
     * База на разделителях
     */
    isDbOnDelimiters: boolean;

    /**
     * Признак что база демо на разделителях
     */
    isDemoDelimiters: boolean;

    /**
     * Номер области (если база на разделителях)
     */
    zoneNumber?: number;

    /**
     * Название конфигурации
     */
    configurationName: string;

    /**
     * База является внешной
     */
    isExternalDb: boolean;

    /**
     * Дата создания
     */
    creationDate: string;

    /**
     * Дата бэкапа
     */
    backupDate?: string;

    /**
     * Дата бэкапа(строка)
     */
    backupDateString: string;

    /**
     * Существует путь к бекапу
     */
    existBackUpPath: boolean;

    /**
     * Дата пересчета размера базы
     */
    calculateSizeDateTime?: string;

    /**
     * Платформа
     */
    templatePlatform: PlatformType;

    /**
     * Тип распространения
     */
    distributionType: DistributionType;

    /**
     * Стабильная версия 82
     */
    stable82Version: string;

    /**
     * Альфа версия 83
     */
    alpha83Version: string;

    /**
     * Стабильная версия 83
     */
    stable83Version: string;

    /**
     * Номер базы
     */
    dbNumber: number;

    /**
     * База файловая
     */
    isFile?: boolean;

    /**
     * Путь к файлу
     */
    filePath: string;

    /**
     * База готова
     */
    isReady: boolean;

    /**
     * Шаблон базы
     */
    dbTemplate: string;

    /**
     * Название аккаунта
     */
    accountCaption: string;

    /**
     * Сервер 82
     */
    v82Server: string;

    /**
     * Sql сервер
     */
    sqlServer: string;

    /**
     * Путь к архиву
     */
    archivePath: string;

    /**
     * Название сервиса
     */
    serviceName: string;

    /**
     * Заблокированное состояние
     */
    lockedState: string;

    /**
     * Тип платформы
     */
    platformType: PlatformType;

    /**
     * Возможность вэб публикации
     */
    canWebPublish: boolean;

    /**
     * Статус базы
     */
    databaseState: DatabaseState;

    /**
     * Использование вэб сервиса
     */
    usedWebServices: boolean;

    /**
     * Веб-ссылка на облачное хранилище
     */
    cloudStorageWebLink: string;

    /**
     * Id файлового хранилища
     */
    fileStorageId: string;

    /**
     * Тип модели восстановления
     */
    restoreModelType: AccountDatabaseRestoreModelType;

    /**
     * Признак что есть возможность сменить модель восстановления
     */
    canChangeRestoreModel: boolean;

    /**
     * Наименование файлового хранилища
     */
    fileStorageName: string;
};