import { DatabaseState, DistributionType, PlatformType } from 'app/common/enums';
import { KeyValueResponseDto } from 'app/web/common/response-dto/KeyValueResponseDto';

/**
 * Модель видимости полей и справочники для карточки информационной базы в режиме редактирования
 */
export type DatabaseCardEditModeDictionariesResponseDto = {
    /**
     * Массив платформ
     */
    availablePlatformTypes: Array<PlatformType>;
    /**
     * Массив типов распространения базы
     */
    availableDestributionTypes: Array<KeyValueResponseDto<DistributionType, string>>;
    /**
     * Массив состояний базы
     */
    availableDatabaseStates: Array<DatabaseState>;

    /**
     * Массив файловых хранилищь
     */
    availableFileStorages: Array<KeyValueResponseDto<string, string>>;

    /**
     * Массив шаблонов баз
     */
    availableDatabaseTemplates: Array<KeyValueResponseDto<string, string>>;
};