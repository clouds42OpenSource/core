import { DatabaseCardAccountDatabaseInfoResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto/DatabaseCardAccountDatabaseInfoResponseDto';
import { DatabaseCardBackupInfoResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto/DatabaseCardBackupInfoResponseDto';
import { DatabaseCardCommandVisibilityResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto/DatabaseCardCommandVisibilityResponseDto';
import { DatabaseCardEditModeDictionariesResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto/DatabaseCardEditModeDictionariesResponseDto';
import { DatabaseCardReadModeFieldAccessResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto/DatabaseCardReadModeFieldAccessResponseDto';
import { DatabaseCardTabVisibilityResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto/DatabaseCardTabVisibilityResponseDto';
import { DatabaseCardTehSupportInfoResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto/DatabaseCardTehSupportInfoResponseDto';

/**
 * Модель ответа для карточки информационной базы
 */
export type DatabaseCardResponseDto = {

    /**
     * Информация о базе
     */
    database: DatabaseCardAccountDatabaseInfoResponseDto;

    /**
     * Url для запуска опубликованной базы
     */
    launchPublishedDbUrl: string;

    /**
     * Информация о видемости полей в режиме просмотра
     */
    fieldsAccessReadModeInfo: DatabaseCardReadModeFieldAccessResponseDto;

    /**
     * Информация о бекапах информационной базы
     */
    backups: DatabaseCardBackupInfoResponseDto[],
    /**
     * Информация о техподержке информационной базы
     */

    tehSupportInfo: DatabaseCardTehSupportInfoResponseDto | null;

    tabVisibility: DatabaseCardTabVisibilityResponseDto;

    commandVisibility: DatabaseCardCommandVisibilityResponseDto;

    /**
     * Если true, то можно редактировать данные в карточке базы
     */
    canEditDatabase: boolean;

    /**
     * Видимости полей и справочники для карточки информационной базы в режиме редактирования
     */
    accountCardEditModeDictionaries: DatabaseCardEditModeDictionariesResponseDto;
};