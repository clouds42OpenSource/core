import {
    ArrServersTabProxy,
    BackupStorageTabProxy,
    ContentServersTabProxy,
    EnterpriseServersTabProxy,
    FileStorageTabProxy,
    HostingTabProxy,
    PlatformVersionTabProxy,
    PublishNodesTabProxy,
    SegmentsTabProxy,
    ServerFarmsTabProxy,
    SqlServersTabProxy,
    TerminalFarmsTabProxy,
    TerminalGatewayTabProxy,
    TerminalServerTabProxy
} from 'app/web/api/SegmentsProxy';

import { AccountCardProxy } from 'app/web/api/AccountCardProxy/AccountCardProxy';
import { AccountDatabaseBackupArmProxy } from 'app/web/api/AccountDatabaseBackupArmProxy';
import { AccountProxy } from 'app/web/api/AccountProxy';
import { AdditionalSessionsProxy } from 'app/web/api/AdditionalSessionsProxy/AdditionalSessionsProxy';
import { SessionListProxy } from 'app/web/api/AdditionalSessionsProxy/SessionListProxy';
import { AgencyAgreementProxy } from 'app/web/api/AgencyAgreementProxy/AgencyAgreementProxy';
import { ArmAutoUpdateAccountDatabaseProxy } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy';
import { AutoPaymentProxy } from 'app/web/api/AutoPaymentProxy';
import { BalanceManagementProxy } from 'app/web/api/BalanceManagementProxy';
import { CloudCoreProxy } from 'app/web/api/CloudCoreProxy';
import { CloudServicesProxy } from 'app/web/api/CloudServicesProxy';
import { Configurations1CProxy } from 'app/web/api/Configurations1C/Configurations1CProxy';
import { CoreWorkerTasksProxy } from 'app/web/api/CoreWorkerTasksProxy';
import { CreateCustomDatabaseDtCoreProxy, CreateCustomDatabaseDtProxy } from 'app/web/api/CreateCustomDatabaseDt';
import { DatabaseCardProxy } from 'app/web/api/DatabaseCardProxy/DatabaseCardProxy';
import { DatabaseProxy } from 'app/web/api/DatabaseProxy';
import { DbTemplateDelimetersProxy } from 'app/web/api/DbTemplateDelimetersProxy/DbTemplateProxy';
import { DbTemplateCoreProxy } from 'app/web/api/DbTemplateProxy/DbTemplateCoreProxy';
import { DbTemplateProxy } from 'app/web/api/DbTemplateProxy/DbTemplateProxy';
import { DbTemplateUpdateProxy } from 'app/web/api/DbTemplateUpdateProxy';
import { DelimiterSourceAccountDatabasesProxy } from 'app/web/api/DelimiterSourceAccountDatabasesProxy';
import { GetUserPermissionsProxy } from 'app/web/api/GetUserPermissions';
import { IndustryProxy } from 'app/web/api/IndustryProxy/IndustryProxy';
import { InfoDbCardProxy } from 'app/web/api/InfoDbCardProxy/InfoDbCardProxy';
import { InfoDbProxy } from 'app/web/api/InfoDbProxy';
import { ItsAuthorizationDataProxy } from 'app/web/api/ItsAuthorizationDataProxy';
import { LocalesProxy } from 'app/web/api/LocalesProxy';
import { LoggingProxy } from 'app/web/api/LoggingProxy';
import { AccountUsersByIdProxy, MsBackupsProxy } from 'app/web/api/MsBackups';
import { MyCompanyProxy } from 'app/web/api/MyCompanyProxy/MyCompanyProxy';
import { PrintedHtmlFormProxy } from 'app/web/api/PrintedHtmlForm/PrintedHtmlFormProxy';
import { ProcessFlowProxy } from 'app/web/api/ProcessFlowProxy';
import { ServiceAccountsProxy } from 'app/web/api/ServiceAccounts';
import { SettingsProxy } from 'app/web/api/SettingsProxy/SettingsProxy';
import { SuppliersProxy } from 'app/web/api/SuppliersProxy';
import { TaskControlCardProxy } from 'app/web/api/TaskControlCard';
import { TaskInQueueItemCardProxy } from 'app/web/api/TaskInQueueItemCardProxy/TaskInQueueItemCardProxy';
import { TasksInQueueProxy } from 'app/web/api/TasksInQueueProxy';
import { TasksPageProxy } from 'app/web/api/TasksPageProxy';
import { UserManagementProxy } from 'app/web/api/UserManagementProxy';
import { MSDatabaseManagementProxy } from 'app/web/api/UserManagementProxy/MSDatabaseManagementProxy';
import { WorkerAvailableTasksBagsProxy } from 'app/web/api/WorkerAvailableTasksBagsProxy';
import { WorkersProxy } from 'app/web/api/WorkersProxy/WorkersProxy';
import { WorkerTasksBagsControlCardProxy } from 'app/web/api/WorkerTasksBagsControlCardProxy/WorkerTasksBagsControlCardProxy';
import { IRequestSender } from 'core/requestSender/interfaces';

/**
 * Http прокси
 */
export class ApiProxy {
    /**
     * @Deprecated
     * Хост на Менеджер Сервиса
     */
    private readonly msHost: string;

    /**
     * Хост на ЛК
     */
    private readonly cpHost: string;

    /**
     * Хост на API
     */
    private readonly apiHost: string;

    /**
     * Общий хост на МС
     */
    private readonly msMainHost: string;

    /**
     * Хост на UPLOAD
     */
    private readonly uploadHost: string;

    /**
     * Отправитель запросов
     */
    private readonly requestSender: IRequestSender;

    /**
     * Создаёт инстенс класса HttpProxy
     * @param requestSender Отправитель запросов
     * @param cpHost Хост на ЛК
     * @param apiHost Хост на API
     * @param msHost Хост на MS
     * @param msMainHost Main MS host
     * @param uploadHost Хост на uploadFile
     */
    constructor(requestSender: IRequestSender, cpHost: string, apiHost: string, msHost?: string, msMainHost?: string, uploadHost?: string) {
        this.requestSender = requestSender;
        this.cpHost = cpHost;
        this.apiHost = apiHost;
        this.msMainHost = msMainHost ?? '';
        this.msHost = msHost ?? '';
        this.uploadHost = uploadHost ?? '';
    }

    public get ApiProxyVariables() {
        return {
            apiHost: this.apiHost,
            cpHost: this.cpHost,
            msHost: this.msHost,
            msMainHost: this.msMainHost,
            requestSender: this.requestSender
        };
    }

    /**
     * Возвращат прокси объект работающий с данными по взаимодействию воркера и задач
     */
    public getWorkerAvailableTasksBagsProxy() {
        return new WorkerAvailableTasksBagsProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращат прокси объект работающий с информационной базой
     */
    public getDatabaseApiProxy() {
        return new DatabaseProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращат прокси объект работающий с информационной базой в управлении аккаунтом
     */
    public getInfoDbApiProxy() {
        return new InfoDbProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращат прокси объект работающий с карточкой информационной базы в управлении аккаунтом
     */
    public getInfoDbCardApiProxy() {
        return new InfoDbCardProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращат прокси объект работающий с логированием
     */
    public getLoggingApiProxy() {
        return new LoggingProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси объект для работы с задачами
     */
    public getTasksApiProxy() {
        return new TasksInQueueProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси объект для работы с воркерами
     */
    public getWorkersApiProxy() {
        return new WorkersProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси объект для работы с задачами воркера
     */
    public getCoreWorkerTasksApiProxy() {
        return new CoreWorkerTasksProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращат прокси объект карточки базы
     */
    public getDatabaseCardApiProxy() {
        return new DatabaseCardProxy(this.requestSender, this.apiHost);
    }

    public getAccountCardApiProxy() {
        return new AccountCardProxy(this.requestSender, this.apiHost);
    }

    public getTaskControlCardApiProxy() {
        return new TaskControlCardProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси работающий с карточкой информации о задаче из очереди задач воркера
     */
    public getTaskInQueueItemCardProxy() {
        return new TaskInQueueItemCardProxy(this.requestSender, this.apiHost);
    }

    public getAccountApiProxy() {
        return new AccountProxy(this.requestSender, this.apiHost);
    }

    public getCloudServicesProxy() {
        return new CloudServicesProxy(this.requestSender, this.apiHost);
    }

    public getTasksPageProxy() {
        return new TasksPageProxy(this.requestSender, this.apiHost);
    }

    public getProcessFlowProxy() {
        return new ProcessFlowProxy(this.requestSender, this.apiHost);
    }

    public getDelimiterSourceAccountDatabasesProxy() {
        return new DelimiterSourceAccountDatabasesProxy(this.requestSender, this.apiHost);
    }

    public getWorkerTasksBagsApiProxy() {
        return new WorkerTasksBagsControlCardProxy(this.requestSender, this.apiHost);
    }

    public getServiceAccountsProxy() {
        return new ServiceAccountsProxy(this.requestSender, this.apiHost);
    }

    public getConfigurations1CProxy() {
        return new Configurations1CProxy(this.requestSender, this.apiHost);
    }

    public getIndustryProxy() {
        return new IndustryProxy(this.requestSender, this.apiHost);
    }

    public getDbTemplateProxy() {
        return new DbTemplateProxy(this.requestSender, this.apiHost);
    }

    public getDbTemplateCoreProxy() {
        return new DbTemplateCoreProxy(this.requestSender, this.apiHost);
    }

    public getDbTemplateDelimetersProxy() {
        return new DbTemplateDelimetersProxy(this.requestSender, this.apiHost);
    }

    public getAgencyAgreementProxy() {
        return new AgencyAgreementProxy(this.requestSender, this.apiHost);
    }

    public getPrintedHtmlFormProxy() {
        return new PrintedHtmlFormProxy(this.requestSender, this.apiHost);
    }

    public getCloudCoreProxy() {
        return new CloudCoreProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси работающей с историей бэкапов баз аккаунта
     */
    public getAccountDatabaseBackupArmProxy() {
        return new AccountDatabaseBackupArmProxy(this.requestSender, this.cpHost);
    }

    /**
     * Получить прокси работающей с обновленными шаблонами баз
     */
    public getDbTemplateUpdateProxy() {
        return new DbTemplateUpdateProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси работающей с поставщиками
     */
    public getSupplierProxy() {
        return new SuppliersProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси для работы с локалями
     */
    public getLocalesProxy() {
        return new LocalesProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси для работы с данными о компании
     */
    public getMyCompanyProxy() {
        return new MyCompanyProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси для работы с сегментами
     */
    public getSegmentsProxy() {
        return {
            SegmentsTabProxy: new SegmentsTabProxy(this.requestSender, this.apiHost),
            TerminalFarmsTabProxy: new TerminalFarmsTabProxy(this.requestSender, this.apiHost),
            EnterpriseServersTabProxy: new EnterpriseServersTabProxy(this.requestSender, this.apiHost),
            ContentServersTabProxy: new ContentServersTabProxy(this.requestSender, this.apiHost),
            PublishNodesTabProxy: new PublishNodesTabProxy(this.requestSender, this.apiHost),
            FileStorageTabProxy: new FileStorageTabProxy(this.requestSender, this.apiHost),
            SqlServersTabProxy: new SqlServersTabProxy(this.requestSender, this.apiHost),
            BackupStorageTabProxy: new BackupStorageTabProxy(this.requestSender, this.apiHost),
            TerminalGatewayTabProxy: new TerminalGatewayTabProxy(this.requestSender, this.apiHost),
            TerminalServerTabProxy: new TerminalServerTabProxy(this.requestSender, this.apiHost),
            PlatformVersionTabProxy: new PlatformVersionTabProxy(this.requestSender, this.apiHost),
            HostingTabProxy: new HostingTabProxy(this.requestSender, this.apiHost),
            ServerFarmsTabProxy: new ServerFarmsTabProxy(this.requestSender, this.apiHost),
            ArrServersTabProxy: new ArrServersTabProxy(this.requestSender, this.apiHost)
        };
    }

    /**
     * Получить прокси для работы с доступом ИТС
     */
    public getItsAuthorizationDataProxy() {
        return new ItsAuthorizationDataProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси для работы с АРМ автообнолвением
     */
    public getArmAutoUpdateAccountDatabaseProxy() {
        return new ArmAutoUpdateAccountDatabaseProxy(this.requestSender, this.apiHost);
    }

    /**
     * Получить прокси для работы с настройками
     */
    public getSettingsProxy() {
        return new SettingsProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращат прокси объект работающий с информационной базой
     */
    public getBalanceManagementApiProxy() {
        return new BalanceManagementProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращат прокси объект работающий с информационной базой
     */
    public getUserPermissionsApiProxy() {
        return new GetUserPermissionsProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращат прокси объект работающий с информационной базой
     */
    public getUserManagementApiProxy() {
        return new UserManagementProxy(this.requestSender, this.apiHost);
    }

    /**
     *
     */
    public getMSDatabaseManagementProxy() {
        return new MSDatabaseManagementProxy(this.requestSender, this.msHost);
    }

    /**
     * getAutoPaymentApiProxy
     */
    public getAutoPaymentApiProxy() {
        return new AutoPaymentProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращает прокси объекта, работающий с списком сеансов
     */
    public getSessionListProxy() {
        return new SessionListProxy(this.requestSender, this.msHost);
    }

    /**
     * Возвращает прокси объекта, работающий с дополнительными сеансами
     */
    public getAdditionalSessionsProxy() {
        return new AdditionalSessionsProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращает прокси объекта, работающего с бекапами разделителей через МС
     */
    public getMsBackupsProxy() {
        return new MsBackupsProxy(this.requestSender, this.msHost);
    }

    /**
     * Возвращение прокси объекта, работающего со списком пользователей аккаунта по идентификатору
     */
    public getAccountUsersByIdProxy() {
        return new AccountUsersByIdProxy(this.requestSender, this.apiHost);
    }

    /**
     * Возвращает прокси объекта, работающего с загрузкой файлов чанками
     */
    public getCreateCustomDatabaseDtProxy() {
        return new CreateCustomDatabaseDtProxy(this.requestSender, this.uploadHost);
    }

    /**
     * Возвращает прокси объекта, создающего базу из dt
     */
    public getCreateCustomDatabaseCoreDtProxy() {
        return new CreateCustomDatabaseDtCoreProxy(this.requestSender, this.apiHost);
    }
}