/**
 * Модель запроса к апи для редактирования локали
 */
export type EditLocaleRequestDto = {

    /**
     * ID локали
     */
    id: string;

    /**
     * Адрес сайта личного кабинета
     */
    cpSiteUrl: string;

    /**
     * ID сегмента по умолчанию
     */
    defaultSegmentId: string;
};