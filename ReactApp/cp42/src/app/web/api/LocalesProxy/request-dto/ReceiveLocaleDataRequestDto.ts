/**
 * Модель запроса к апи на получения данных локали для редактирования
 */
export type ReceiveLocaleDataRequestDto = {
    /**
     * ID локали
     */
    localeId: string;
};