import { Nullable } from 'app/common/types';

/**
 * Модель фильтра для получения локалей
 */
export type ReceiveLocalesFilterRequestDto = {
    /**
     * Строка поиска
     */
    searchLine: Nullable<string>;
};