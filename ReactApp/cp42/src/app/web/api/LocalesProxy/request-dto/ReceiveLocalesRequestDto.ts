import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';
import { ReceiveLocalesFilterRequestDto } from 'app/web/api/LocalesProxy/request-dto/ReceiveLocalesFilterRequestDto';

/**
 * Модель запроса для получения локалей
 */
export type ReceiveLocalesRequestDto = SelectDataCommonRequestDto<ReceiveLocalesFilterRequestDto>;