import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { EditLocaleRequestDto, ReceiveLocaleDataRequestDto, ReceiveLocalesRequestDto } from 'app/web/api/LocalesProxy/request-dto';
import { LocaleDataResponseDto, LocalesResponseDto, LocalizationsItemResponseDto } from 'app/web/api/LocalesProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с локалями
 */
export class LocalesProxy extends BaseProxy {
    private token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение локалей
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public receiveLocales(requestKind: RequestKind, args: ReceiveLocalesRequestDto) {
        return this.requestSender.submitRequest<LocalesResponseDto, ReceiveLocalesRequestDto>(
            WebVerb.GET,
            `${ this.host }/locales`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Получить данные локали для редактирования
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    public receiveLocaleDataForEditing(requestKind: RequestKind, args: ReceiveLocaleDataRequestDto) {
        return this.requestSender.submitRequest<LocaleDataResponseDto>(
            WebVerb.GET,
            `${ this.host }/locales/${ args.localeId }`,
            this.getObjectArguments(requestKind, {}, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Изменить локаль
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    public editLocale(requestKind: RequestKind, args: EditLocaleRequestDto) {
        return this.requestSender.submitRequest<void, EditLocaleRequestDto>(
            WebVerb.PUT,
            `${ this.host }/locales`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public getLocalizations(requestKind: RequestKind, args: void) {
        return this.requestSender.submitRequest<LocalizationsItemResponseDto[], void>(
            WebVerb.GET,
            `${ this.host }/locales/localizations`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}