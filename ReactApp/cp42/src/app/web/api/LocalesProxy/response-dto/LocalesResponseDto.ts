import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { LocaleItemResponseDto } from 'app/web/api/LocalesProxy/response-dto/LocaleItemResponseDto';

/**
 * Модель ответа при получении данных локалей
 */
export type LocalesResponseDto = SelectDataMetadataResponseDto<LocaleItemResponseDto>;