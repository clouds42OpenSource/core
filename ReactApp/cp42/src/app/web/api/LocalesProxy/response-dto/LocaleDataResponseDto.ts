import { LocaleItemResponseDto } from 'app/web/api/LocalesProxy/response-dto/LocaleItemResponseDto';

/**
 * Модель ответа с данными локали
 */
export type LocaleDataResponseDto = LocaleItemResponseDto & {
    /**
     * Адрес сайта личного кабинета
     */
    cpSiteUrl: string;
    /**
     * ID сегмента по умолчанию
     */
    defaultSegmentId: string;
};