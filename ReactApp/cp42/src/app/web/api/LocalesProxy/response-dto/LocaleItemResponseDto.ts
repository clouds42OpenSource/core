import { Nullable } from 'app/common/types';

/**
 * Модель ответа с элементом локали
 */
export type LocaleItemResponseDto = {
    /**
     * ID локали
     */
    id: string;
    /**
     * Название
     */
    name: string;
    /**
     * Страна
     */
    country: string;
    /**
     * Код валюты
     */
    currencyCode: Nullable<number>;
    /**
     * Валюта
     */
    currency: string;
};