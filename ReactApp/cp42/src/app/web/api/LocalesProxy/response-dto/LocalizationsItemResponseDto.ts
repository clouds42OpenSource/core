export type LocalizationsItemResponseDto = {
    key: number;
    localeId: string;
    value: string;
}