import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

/**
 * Прокси работающие с DbTemplate core
 */
export class DbTemplateCoreProxy extends BaseProxy {
    public token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение списка имен конфигураций
     * @param requestKind Вид запроса
     */
    public getDbTemplateConfiguration1CName(requestKind: RequestKind) {
        return this.requestSender.submitRequest<string[], NonNullable<unknown>>(
            WebVerb.GET,
            `${ this.host }/api_v2/dbTemplate/Configuration1CName`,
            this.getObjectArguments(requestKind, {}, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}