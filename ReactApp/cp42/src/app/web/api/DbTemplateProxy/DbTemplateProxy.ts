import { GetDbTemplatesParamsDto, GetDbTemplateToEditParamsDto } from 'app/web/api/DbTemplateProxy/request-dto';
import { DbTemplateItemDto } from 'app/web/api/DbTemplateProxy/request-dto/DbTemplateItemDto';
import { DeleteDbTemplateItemRequestDto } from 'app/web/api/DbTemplateProxy/request-dto/DeleteDbTemplateItemRequestDto';
import { GetDbTemplatesResultResponseDto } from 'app/web/api/DbTemplateProxy/response-dto';
import { AddNewDbTemplateResultResponseDto } from 'app/web/api/DbTemplateProxy/response-dto/AddNewDbTemplateResultResponseDto';
import { GetDbTemplateToEditResponseDto } from 'app/web/api/DbTemplateProxy/response-dto/GetDbTemplateToEditResponseDto';
import { BaseProxy } from 'app/web/common';
import { KeyValueResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с DbTemplate
 */
export class DbTemplateProxy extends BaseProxy {
    /**
     * Запрос на получение шаблонов
     * @param requestKind Вид запроса
     * @param args параметры на получение шаблонов
     */
    public receiveDbTemplates(requestKind: RequestKind, args: GetDbTemplatesParamsDto) {
        return this.requestSender.submitRequest<GetDbTemplatesResultResponseDto, GetDbTemplatesParamsDto>(
            WebVerb.GET,
            `${ this.host }/db-templates`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на получение шаблонов ввиде key - ID шаблона, value - описание шаблона
     * @param requestKind Вид запроса
     */
    public receiveAllDbTemplatesAsKeyValue(requestKind: RequestKind) {
        return this.requestSender.submitRequest<KeyValueResponseDto<string, string>[], GetDbTemplatesParamsDto>(
            WebVerb.GET,
            `${ this.host }/db-templates/as-key-value`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на получение шаблона для редактирования
     * @param requestKind Вид запроса
     * @param args параметры на получение шаблонов
     */
    public getDbTemplateDataToEdit(requestKind: RequestKind, args: GetDbTemplateToEditParamsDto) {
        return this.requestSender.submitRequest<GetDbTemplateToEditResponseDto, GetDbTemplateToEditParamsDto>(
            WebVerb.GET,
            `${ this.host }/db-templates/${ args.DbTemplateId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на редактирование шаблона
     * @param requestKind Вид запроса
     * @param args параметры на обновление шаблона
     */
    public updateDbTemplate(requestKind: RequestKind, args: DbTemplateItemDto) {
        return this.requestSender.submitRequest<void, DbTemplateItemDto>(
            WebVerb.PUT,
            `${ this.host }/db-templates`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на добавление шаблона
     * @param requestKind Вид запроса
     * @param args параметры на добавление шаблона
     */
    public addDbTemplate(requestKind: RequestKind, args: DbTemplateItemDto) {
        return this.requestSender.submitRequest<AddNewDbTemplateResultResponseDto, DbTemplateItemDto>(
            WebVerb.POST,
            `${ this.host }/db-templates`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на удаление шаблона
     * @param requestKind Вид запроса
     * @param args параметры на удаление отрасли
     */
    public deleteDbTemplate(requestKind: RequestKind, args: DeleteDbTemplateItemRequestDto) {
        return this.requestSender.submitRequest<void, DeleteDbTemplateItemRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/db-templates/${ args.DbTemplateId }`,
            this.getObjectArguments(requestKind)
        );
    }
}