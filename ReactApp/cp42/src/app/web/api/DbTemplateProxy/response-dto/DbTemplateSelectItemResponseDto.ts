export type DbTemplateSelectItemResponseDto = {
    /**
     * ID шаблона
     */
    id: string;

    /**
     * Название шаблона
     */
    name: string;

    /**
     * Описание шаблона
     */
    defaultCaption: string;

    /**
     * Версия платформы
     */
    platform: string;
};