import { DbTemplateItemDto } from 'app/web/api/DbTemplateProxy/request-dto/DbTemplateItemDto';

/**
 * Модель ответа на получение шаблона для редактирования
 */
export type GetDbTemplateToEditResponseDto = DbTemplateItemDto;