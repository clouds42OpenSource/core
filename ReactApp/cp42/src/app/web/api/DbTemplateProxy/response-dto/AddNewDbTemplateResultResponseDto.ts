/**
 * Модель результата на добавление нового шаблона
 */
export type AddNewDbTemplateResultResponseDto = {
    /**
     * ID шаблона после добавления
     */
    dbTemplateId: string;
};