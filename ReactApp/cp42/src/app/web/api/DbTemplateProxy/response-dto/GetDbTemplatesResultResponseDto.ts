import { DbTemplateSelectItemResponseDto } from 'app/web/api/DbTemplateProxy/response-dto/DbTemplateSelectItemResponseDto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа на получение списка шаблонов
 */
export type GetDbTemplatesResultResponseDto = SelectDataMetadataResponseDto<DbTemplateSelectItemResponseDto>;