import { PlatformType } from 'app/common/enums';

/**
 * Модель шаблона
 */
export type DbTemplateItemDto = {
    /**
     * ID шаблона
     */
    id: string;

    /**
     * Название шаблона
     */
    name: string;

    /**
     * Описание шаблона
     */
    defaultCaption: string;

    /**
     * Порядок
     */
    order: number;

    /**
     * Платформа
     */
    platform: PlatformType;
    /**
     * Типа платформы
     */
    platformType: PlatformType;

    /**
     * Локаль
     */
    localeId: string;

    /**
     * Шаблон с демо данными
     */
    demoTemplateId: string;

    /**
     * Можно ли публиковать на web
     */
    canWebPublish: boolean;

    /**
     * Имя конфигурации 1С
     */
    configuration1CName: string;

    /**
     * Логин администратора
     */
    adminLogin: string;

    /**
     * Пароль администратора
     */
    adminPassword: string;

    /**
     * Необходимо ли автообновление
     */
    needUpdate: boolean;
};