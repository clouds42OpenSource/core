import { DbTemplatesFilterParamsDto } from 'app/web/api/DbTemplateProxy/request-dto/DbTemplatesFilterParamsDto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

export type GetDbTemplatesParamsDto = SelectDataCommonRequestDto<DbTemplatesFilterParamsDto>;