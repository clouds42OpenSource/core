/**
 * Модель на удаление шаблона
 */
export type DeleteDbTemplateItemRequestDto = {
    /**
     * ID шаблона для удаления
     */
    DbTemplateId: string;
};