/**
 * Модель фильтра для выбора шаблонов
 */
export type DbTemplatesFilterParamsDto = {
    /**
     * Название шаблона
     */
    name: string;

    /**
     * Описание шаблона
     */
    defaultCaption: string;
};