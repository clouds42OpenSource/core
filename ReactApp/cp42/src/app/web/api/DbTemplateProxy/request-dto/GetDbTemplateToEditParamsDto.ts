/**
 * Параметры для получения шаблона для редактирования
 */
export type GetDbTemplateToEditParamsDto = {
    /**
     * Id шаблона для редактирования
     */
    DbTemplateId: string;
};