import { WorkerDescriptionResponseDto } from 'app/web/api/WorkersProxy/response-dto/WorkerDescriptionResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Workers
 */
export class WorkersProxy extends BaseProxy {
    /**
     * Выполнить запрос на получение данных по воркерам
     * @param requestKind Вид запроса
     */
    public receiveWorkersData(requestKind: RequestKind) {
        return this.requestSender.submitRequest<Array<WorkerDescriptionResponseDto>>(
            WebVerb.GET,
            `${ this.host }/core-workers`,
            this.getObjectArguments(requestKind)
        );
    }
}