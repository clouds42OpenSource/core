/**
 * Модель ответа на запрос получения данных по воркерам
 */
export type WorkerDescriptionResponseDto = {
    /**
     * ID воркера
     */
    id: number;
    /**
     * Название воркера
     */
    name: string;
}