import { BaseProxy } from 'app/web/common';
import { IRequestSender } from 'core/requestSender/interfaces';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { TLogEvent, TLogEventRequest } from 'app/web/api/LogEventProxy/type';
import { ELogEventNumber } from 'app/web/api/LogEventProxy/enum';

export class LogEventProxy extends BaseProxy {
    private readonly accountId: string | null;

    private readonly token: string | null;

    protected constructor(requestSender: IRequestSender, apiHost: string) {
        super(requestSender, apiHost);
        this.accountId = localStorageHelper.getContextAccountId();
        this.token = localStorageHelper.getJWTToken();
    }

    protected async sendLogEvent(requestKind: RequestKind, log: ELogEventNumber, description: string) {
        try {
            return await this.requestSender.submitRequest<TLogEvent, TLogEventRequest>(
                WebVerb.POST,
                `${ this.host }/logs/log-event/`,
                this.getObjectArguments(requestKind, {
                    accountId: this.accountId ?? '',
                    log,
                    description
                }, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return {
                success: false,
                data: null
            } as TLogEvent;
        }
    }
}