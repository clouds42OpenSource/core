export type TLogEvent = {
    success: boolean;
    data: string | null;
};

export type TLogEventRequest = {
    accountId: string;
    log: number;
    description: string;
};