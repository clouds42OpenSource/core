export enum ELogEventNumber {
    CreatePrivateService = 111,
    DeletePrivateService = 112,
    InstallServiceInDatabase = 90,
}