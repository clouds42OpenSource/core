/**
 * Данные карточки управления задачами
 */
export type TaskControlCardDataResponseDto = {
    /**
     * Id задачи
     */
    id: string;
    /**
     * Название задачи
     */
    name: string;
    /**
     * Время жизни выполнения задачи в минутах
     */
    taskExecutionLifetimeInMinutes: number;
}