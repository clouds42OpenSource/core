export type ReceiveTaskControlCardRequestDto = {
    /**
     * ID задачи
     */
    taskId: string;
};