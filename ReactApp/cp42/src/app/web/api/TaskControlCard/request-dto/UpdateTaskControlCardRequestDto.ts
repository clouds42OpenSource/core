export type UpdateTaskControlCardRequestDto = {
    /**
     * ID задачи
     */
    id: string;
    /**
     * Время жизни выполнения задачи в минутах
     */
    taskExecutionLifetimeInMinutes: number;
};