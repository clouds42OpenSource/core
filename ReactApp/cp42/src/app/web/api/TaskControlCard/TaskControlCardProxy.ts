import { ReceiveTaskControlCardRequestDto } from 'app/web/api/TaskControlCard/request-dto/ReceiveTaskControlCardRequestDto';
import { UpdateTaskControlCardRequestDto } from 'app/web/api/TaskControlCard/request-dto/UpdateTaskControlCardRequestDto';
import { TaskControlCardDataResponseDto } from 'app/web/api/TaskControlCard/response-dto/TaskControlCardDataResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с карточкой управления задачами
 */
export class TaskControlCardProxy extends BaseProxy {
    /**
     * Запрос на получение карточки аккаунта
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public receiveTaskControlCard(requestKind: RequestKind, args: ReceiveTaskControlCardRequestDto) {
        return this.requestSender.submitRequest<TaskControlCardDataResponseDto, ReceiveTaskControlCardRequestDto>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks/for-edit/${ args.taskId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на обновление карточки аккаунта
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public updateTaskControlCard(requestKind: RequestKind, args: UpdateTaskControlCardRequestDto) {
        return this.requestSender.submitRequest<void, UpdateTaskControlCardRequestDto>(
            WebVerb.POST,
            `${ this.host }/core-worker-tasks`,
            this.getObjectArguments(requestKind, args)
        );
    }
}