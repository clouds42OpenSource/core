import { UpdateConfigurationResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy';

/**
 * Детали авто обновления конфигурации 1С
 */
export type AutoUpdateConfiguration1CDetailsResponseDto = {
    /**
     * Текущая версия релиза
     */
    CurrentVersion: string,

    /**
     * Актуальная версия релиза
     */
    ActualVersion: string,

    /**
     * Название конфигурации
     */
    ConfigurationName: string,

    /**
     * Список обновлений конфигурации с порядком обновления
     */
    UpdateConfigurationData: Array<UpdateConfigurationResponseDto>,

    /**
     * Количество попыток для обновления
     */
    CountStepsForUpdate: number
}