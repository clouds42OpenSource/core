/**
 * Модель данных технического результата АО инф. базы
 */
export type DatabaseAutoUpdateTechnicalResultDataResponseDto = {
    /**
     * Время начала АО
     */
    AutoUpdateStartDate: string,

    /**
     * Время завершения АО
     */
    AutoUpdateEndDate: string,

    /**
     * Информация для отладки
     */
    DebugInformation: string
}