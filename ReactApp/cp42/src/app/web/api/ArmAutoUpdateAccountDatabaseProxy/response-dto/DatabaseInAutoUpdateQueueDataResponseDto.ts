import { ConnectedDatabaseOnAutoUpdateDataResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy/response-dto';

/**
 * Модель данных базы которая в очереди на АО
 */
export type DatabaseInAutoUpdateQueueDataResponseDto = ConnectedDatabaseOnAutoUpdateDataResponseDto & {
    /**
     * Дата добавления базы в очередь на АО
     */
    addedDate: string,

    /**
     * В процессе авто обновления
     */
    isProcessingAutoUpdate: boolean
}