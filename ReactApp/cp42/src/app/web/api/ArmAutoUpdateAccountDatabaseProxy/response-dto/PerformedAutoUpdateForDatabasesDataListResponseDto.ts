import { PerformedAutoUpdateForDatabasesDataResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy/response-dto';
import { MetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении логирования запуска баз и открытия RDP
 */
export type PerformedAutoUpdateForDatabasesDataListResponseDto = {
    /**
     * Массив данных о логировании запуска баз и открытия RDP
     */
    records: PerformedAutoUpdateForDatabasesDataResponseDto[],

    /**
     * Информация о страницах логирования запуска баз и открытия RDP
     */
    metadata: MetadataResponseDto;
};