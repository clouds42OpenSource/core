import { DatabaseInAutoUpdateQueueDataResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy/response-dto';
import { MetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении логирования запуска баз и открытия RDP
 */
export type DatabaseInAutoUpdateQueueDataListResponseDto = {
    /**
     * Массив данных о логировании запуска баз и открытия RDP
     */
    records: DatabaseInAutoUpdateQueueDataResponseDto[],

    /**
     * Информация о страницах логирования запуска баз и открытия RDP
     */
    metadata: MetadataResponseDto;
};