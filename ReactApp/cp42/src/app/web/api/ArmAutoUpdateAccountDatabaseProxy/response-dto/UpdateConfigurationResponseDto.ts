/**
 * Данные обновления конфигурации
 */
export type UpdateConfigurationResponseDto = {
    /**
     * Номер обновления
     */
    Number: number,

    /**
     * Текущая версия
     */
    CurrentVersion: string,

    /**
     * Актуальная версия
     */
    ActualVersion: string
}