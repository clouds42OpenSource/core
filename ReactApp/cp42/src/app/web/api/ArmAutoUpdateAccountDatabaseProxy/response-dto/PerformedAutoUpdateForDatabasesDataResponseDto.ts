import { SupportHistoryStateEnumType } from 'app/common/enums';

/**
 * Модель данных выполненного АО базы
 */
export type PerformedAutoUpdateForDatabasesDataResponseDto = {
    /**
     * Id базы
     */
    id: string,

    /**
     * Id задачи которая выполняла обновление
     */
    coreWorkerTasksQueueId: string,

    /**
     * Номер базы
     */
    v82Name: string,

    /**
     * Название базы
     */
    caption: string,

    /**
     * Название конфигурации
     */
    configurationName: string,

    /**
     * Номер воркера который выполнял АО
     */
    capturedWorkerId: number | null,

    /**
     * Статус АО
     */
    status: SupportHistoryStateEnumType,

    /**
     * Описание
     */
    description: string,

    /**
     * Время начала АО
     */
    autoUpdateStartDate: string,
}