import { FilterBaseRequestDto } from 'app/web/common';

/**
 * Модель фильтра технического результата АО инф. базы
 */
export type DatabaseAutoUpdateTechnicalResultFilterRequestDto = FilterBaseRequestDto & {
    /**
     * Id базы
     */
    AccountDatabaseId: string,

    /**
     * Id задачи из очереди
     */
    CoreWorkerTasksQueueId: string
}