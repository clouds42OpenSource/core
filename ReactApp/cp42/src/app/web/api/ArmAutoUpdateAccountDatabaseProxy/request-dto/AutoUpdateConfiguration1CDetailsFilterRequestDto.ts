/**
 * Модель фильтра для получения деталей авто обновления конфигурации 1С
 */
export type AutoUpdateConfiguration1CDetailsFilterRequestDto = {
    /**
     * Текущая версия релиза
     */
    CurrentVersion: string,

    /**
     * Актуальная версия релиза
     */
    ActualVersion: string,

    /**
     * Название конфигурации
     */
    ConfigurationName: string
}