import { SupportHistoryStateEnumType } from 'app/common/enums';
import { Nullable } from 'app/common/types';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель фильтра выполненного АО базы
 */
export type Filter = {
    /**
     * Номер воркера
     */
    coreWorkerId?: number,

    /**
     * Статус АО
     */
    status?: SupportHistoryStateEnumType | string,

    /**
     * Время начала АО
     */
    autoUpdatePeriodFrom?: Nullable<string>,

    /**
     * Время завершения АО
     */
    autoUpdatePeriodTo?: Nullable<string>
}
export type PerformedAutoUpdateForDatabasesFilterRequestDto = SelectDataCommonRequestDto<Filter>