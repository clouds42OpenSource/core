import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

type Filter = {
    /**
     * Только те, которым необходимо авто обновление
     */
    needAutoUpdateOnly?: boolean,

    /**
     * Только те, у которых активна Аренда 1С
     */
    isActiveRent1COnly?: boolean
    isVipAccountsOnly?: boolean
    searchLine?: string
}

export type ConnectedDatabaseOnAutoUpdateFilterRequestDto = SelectDataCommonRequestDto<Filter>