import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель фильтра базы которая в очереди на АО

*/

type Filter = {
    /**
     * Строка поиска
     */
    searchString?: string,

    /**
     * Только вип аккаунты
     */
    isVipAccountsOnly?: boolean,

    /**
     * Конфигурации 1С
     */
    configurations?: Array<string>
    searchLine?: string
}

export type DatabaseInAutoUpdateQueueFilterRequestDto = SelectDataCommonRequestDto<Filter>