import {
    AutoUpdateConfiguration1CDetailsFilterRequestDto,
    AutoUpdateConfiguration1CDetailsResponseDto,
    ConnectedDatabaseOnAutoUpdateFilterRequestDto,
    DatabaseAutoUpdateTechnicalResultDataResponseDto,
    DatabaseAutoUpdateTechnicalResultFilterRequestDto,
    DatabaseInAutoUpdateQueueDataListResponseDto,
    DatabaseInAutoUpdateQueueFilterRequestDto,
    PerformedAutoUpdateForDatabasesDataListResponseDto,
    PerformedAutoUpdateForDatabasesFilterRequestDto
} from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy';
import { ConnectedDatabaseOnAutoUpdateDataListResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy/response-dto';
import { BaseProxy, PaginationDataResultResponseDto } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с АРМ автообновлением
 */
export class ArmAutoUpdateAccountDatabaseProxy extends BaseProxy {
    /**
     * Получить список инф. баз, подключенных к АО
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список инф. баз, подключенных к АО
     */
    public getConnectedDatabasesOnAutoUpdate(requestKind: RequestKind, args: ConnectedDatabaseOnAutoUpdateFilterRequestDto) {
        return this.requestSender.submitRequest<ConnectedDatabaseOnAutoUpdateDataListResponseDto, ConnectedDatabaseOnAutoUpdateFilterRequestDto>(
            WebVerb.POST,
            `${ this.host }/auto-update-account-database-data/connected`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить список инф. баз, в очереди на АО
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список инф. баз, в очереди на АО
     */
    public getDatabaseInAutoUpdateQueue(requestKind: RequestKind, args: DatabaseInAutoUpdateQueueFilterRequestDto) {
        return this.requestSender.submitRequest<DatabaseInAutoUpdateQueueDataListResponseDto, DatabaseInAutoUpdateQueueFilterRequestDto>(
            WebVerb.POST,
            `${ this.host }/auto-update-account-database-data/queued`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить детали авто обновления конфигурации 1С
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Детали авто обновления конфигурации 1С
     */
    public getAutoUpdateConfiguration1CDetails(requestKind: RequestKind, args: AutoUpdateConfiguration1CDetailsFilterRequestDto) {
        return this.requestSender.submitRequest<AutoUpdateConfiguration1CDetailsResponseDto, AutoUpdateConfiguration1CDetailsFilterRequestDto>(
            WebVerb.GET,
            `${ this.host }/auto-update-account-database-data/configuration-1c`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить названия конфигураций 1С
     * @param requestKind Вид запроса
     * @returns Список названий конфигураций 1С
     */
    public getConfigurationNames(requestKind: RequestKind) {
        return this.requestSender.submitRequest<Array<string>>(
            WebVerb.GET,
            `${ this.host }/configuration-1c/names/as-list`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить данные по выполненным АО инф. баз
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Данные по выполненным АО инф. баз
     */
    public getPerformedAutoUpdateForDatabasesData(requestKind: RequestKind, args: PerformedAutoUpdateForDatabasesFilterRequestDto) {
        return this.requestSender.submitRequest<PerformedAutoUpdateForDatabasesDataListResponseDto, PerformedAutoUpdateForDatabasesFilterRequestDto>(
            WebVerb.POST,
            `${ this.host }/auto-update-account-database-data/performed`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить воркеры которые выполняют авто обновление
     * @param requestKind Вид запроса
     * @returns Список воркеров
     */
    public getWorkers(requestKind: RequestKind) {
        return this.requestSender.submitRequest<Array<number>>(
            WebVerb.GET,
            `${ this.host }/auto-update-account-database-data/workers`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить данные технических результатов АО инф. базы
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список технических результатов АО инф. базы
     */
    public getDatabaseAutoUpdateTechnicalResults(requestKind: RequestKind, args: DatabaseAutoUpdateTechnicalResultFilterRequestDto) {
        return this.requestSender.submitRequest<PaginationDataResultResponseDto<DatabaseAutoUpdateTechnicalResultDataResponseDto>, DatabaseAutoUpdateTechnicalResultFilterRequestDto>(
            WebVerb.GET,
            `${ this.host }/auto-update-account-database-data/technical-results`,
            this.getObjectArguments(requestKind, args)
        );
    }
}