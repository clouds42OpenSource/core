import { CoreWorkerTaskInQueueTypeEnum } from 'app/common/enums/CoreWorkerTaskInQueueTypeEnum';

/**
 * Модель ответа на получение данных для карточки информации о задаче из списка задач воркеров
 */
export type TaskInQueueItemCardDataResponseDto = {
    /**
     * Название задачи
     */
    taskName: string;
    /**
     * Параметры задачи
     */
    taskParams: string;
    /**
     * Дата запуска
     */
    startDateTime: string | undefined;
    /**
     * Приоритет
     */
    priority: number | null;
    /**
     * Тип задачи
     */
    taskType: CoreWorkerTaskInQueueTypeEnum;
    /**
     * Комментарий к задаче
     */
    comment: string;
    /**
     * ID задачи из очереди задач воркера
     */
    taskInQueueItemId: string;
    /**
     * Статус
     */
    status: string;
};