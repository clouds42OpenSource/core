/**
 * Модель запроса на получение данных для карточки информации о задаче из списка задач воркеров
 */
export type ReceiveTaskInQueueItemCardRequestDto = {
    /**
     * ID задачи из очереди задач воркера
     */
    taskInQueueItemId: string;
};