import { ReceiveTaskInQueueItemCardRequestDto } from 'app/web/api/TaskInQueueItemCardProxy/request-dto/ReceiveTaskInQueueItemCardRequestDto';
import { TaskInQueueItemCardDataResponseDto } from 'app/web/api/TaskInQueueItemCardProxy/response-dto/TaskInQueueItemCardDataResponseDto';
import { BaseProxy } from 'app/web/common';
import { AddTaskToQueueParams } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/addTaskToQueue/input-params/AddTaskToQueueParams';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с карточкой информации о задаче из очереди задач воркера
 */
export class TaskInQueueItemCardProxy extends BaseProxy {
    /**
     * Запрос на получение информации о задаче из очереди задач воркера
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public receiveTaskInQueueItemCard(requestKind: RequestKind, args: ReceiveTaskInQueueItemCardRequestDto) {
        return this.requestSender.submitRequest<TaskInQueueItemCardDataResponseDto, ReceiveTaskInQueueItemCardRequestDto>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks-queue/${ args.taskInQueueItemId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на добавление новой задачи в очередь
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public addTaskToQueue(requestKind: RequestKind, args: AddTaskToQueueParams) {
        return this.requestSender.submitRequest<void, AddTaskToQueueParams>(
            WebVerb.POST,
            `${ this.host }/core-worker-tasks-queue`,
            this.getObjectArguments(requestKind, args)
        );
    }
}