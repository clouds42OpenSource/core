/**
 * Модель базы на разделителях
 */
export type DbTemplateDelimeterItemDto = {
    /**
     * Код конфигурации
     */
    configurationId: string;

    /**
     * Название конфигурации 1С
     */
    name: string;

    /**
     * Id шаблона
     */
    templateId: string;

    /**
     * название конфигурации в файле DumpInfo
     */
    shortName: string;

    /**
     * Адрес публикации демо базы на разделителях
     */
    demoDatabaseOnDelimitersPublicationAddress: string;

    /**
     * Версия релиза конфигурации
     */
    configurationReleaseVersion: string;

    /**
     * Минимальная версия релиза конфигурации
     */
    minReleaseVersion: string;
};