import { DbTemplateDelimeterItemDto } from 'app/web/api/DbTemplateDelimetersProxy/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа на получение списка шаблонов баз на разделителях
 */
export type GetDbTemplateDelimetersResultResponseDto = SelectDataMetadataResponseDto<DbTemplateDelimeterItemDto>;