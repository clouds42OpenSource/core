/**
 * Параметры для удаления базы на разделителях
 */
export type DeleteDbTemplateDelimeterParamsRequestDto = {
    /**
     * ID конфигурации для удаления базы на разделителях
     */
    ConfigurationId: string;
};