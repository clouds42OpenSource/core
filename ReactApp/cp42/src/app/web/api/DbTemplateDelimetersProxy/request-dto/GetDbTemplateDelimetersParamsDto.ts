import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

export type GetDbTemplateDelimetersParamsDto = SelectDataCommonRequestDto<void>;