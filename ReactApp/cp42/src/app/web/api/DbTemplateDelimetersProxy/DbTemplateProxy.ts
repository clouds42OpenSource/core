import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { DeleteDbTemplateDelimeterParamsRequestDto } from 'app/web/api/DbTemplateDelimetersProxy/request-dto';
import { GetDbTemplateDelimetersParamsDto } from 'app/web/api/DbTemplateDelimetersProxy/request-dto/GetDbTemplateDelimetersParamsDto';
import { DbTemplateDelimeterItemDto } from 'app/web/api/DbTemplateDelimetersProxy/response-dto';
import { GetDbTemplateDelimetersResultResponseDto } from 'app/web/api/DbTemplateDelimetersProxy/response-dto/GetDbTemplateDelimetersResultResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с DbTemplate
 */
export class DbTemplateDelimetersProxy extends BaseProxy {
    private token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение шаблонов баз на разделителях
     * @param requestKind Вид запроса
     * @param args параметры на получение шаблонов баз на разделителях
     */
    public receiveDbTemplateDelimeters(requestKind: RequestKind, args: GetDbTemplateDelimetersParamsDto) {
        return this.requestSender.submitRequest<GetDbTemplateDelimetersResultResponseDto, GetDbTemplateDelimetersParamsDto>(
            WebVerb.GET,
            `${ this.host }/db-template-delimiters`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на редактирование шаблона базы на разделителях
     * @param requestKind Вид запроса
     * @param args параметры на обновление шаблона базы на разделителях
     */
    public updateDbTemplateDelimeter(requestKind: RequestKind, args: DbTemplateDelimeterItemDto) {
        return this.requestSender.submitRequest<void, DbTemplateDelimeterItemDto>(
            WebVerb.PUT,
            `${ this.host }/db-template-delimiters`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на добавление шаблона на разделителях
     * @param requestKind Вид запроса
     * @param args параметры на добавление шаблона на разделителях
     */
    public addDbTemplateDelimeter(requestKind: RequestKind, args: DbTemplateDelimeterItemDto) {
        return this.requestSender.submitRequest<void, DbTemplateDelimeterItemDto>(
            WebVerb.POST,
            `${ this.host }/db-template-delimiters`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на удаление шаблона на разделителях
     * @param requestKind Вид запроса
     * @param args параметры на удаление шаблона на разделителях
     */
    public deleteDbTemplateDelimeter(requestKind: RequestKind, args: DeleteDbTemplateDelimeterParamsRequestDto) {
        return this.requestSender.submitRequest<void, DeleteDbTemplateDelimeterParamsRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/db-template-delimiters/${ args.ConfigurationId }`,
            this.getObjectArguments(requestKind)
        );
    }
}