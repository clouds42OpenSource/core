import { ChangeTasksBagsRequestDto, ReceiveWorkerTasksBagsControlCardDataRequestDto } from 'app/web/api/WorkerTasksBagsControlCardProxy/request-dto';
import { WorkerTasksBagsControlCardDataResponseDto } from 'app/web/api/WorkerTasksBagsControlCardProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с WorkerTasksBagsControlCard
 */
export class WorkerTasksBagsControlCardProxy extends BaseProxy {
    /**
     * Выполнить запрос на получение данных по доступным задачам воркера
     * @param requestKind Вид запроса
     * @param args
     */
    public receiveCardData(requestKind: RequestKind, args: ReceiveWorkerTasksBagsControlCardDataRequestDto) {
        return this.requestSender.submitRequest<WorkerTasksBagsControlCardDataResponseDto, ReceiveWorkerTasksBagsControlCardDataRequestDto>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks/${ args.coreWorkerId }/available-tasks`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на изменение доступных задач воркера
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public changeTasksBags(requestKind: RequestKind, args: ChangeTasksBagsRequestDto) {
        return this.requestSender.submitRequest<void, ChangeTasksBagsRequestDto>(
            WebVerb.PUT,
            `${ this.host }/core-worker-tasks/available-tasks`,
            this.getObjectArguments(requestKind, args)
        );
    }
}