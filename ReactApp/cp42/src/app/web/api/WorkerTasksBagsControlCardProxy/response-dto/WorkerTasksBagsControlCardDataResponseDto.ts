import { KeyValueResponseDto } from 'app/web/common/response-dto/KeyValueResponseDto';

/**
 * Модель ответа с данными карточки управления доступными задачами воркера
 */
export type WorkerTasksBagsControlCardDataResponseDto = {
    /**
     * Доступные задачи для добавления
     */
    availableTasksForAdd: Array<KeyValueResponseDto<string, string>>;
    /**
     * Собственные задачи воркера
     */
    workerTasksBags: Array<KeyValueResponseDto<string, string>>;
    /**
     * Название воркера
     */
    workerName: string;
    /**
     * ID воркера
     */
    workerId: number;
};