/**
 *  Модель запроса на получение данных для карточки управления доступными задачами воркера
 */
export type ReceiveWorkerTasksBagsControlCardDataRequestDto = {
    /**
     * ID воркера
     */
    coreWorkerId: number;
};