/**
 * Модель запроса на изменение данных в карточке взаимодействия воркера и задач
 */
export type ChangeTasksBagsRequestDto = {
    /**
     * ID воркера
     */
    CoreWorkerId: number;
    /**
     * Список ID доступных задач воркера
     */
    WorkerTasksBagsIds: Array<string>;
};