/**
 * Модель для получения дефолтных конфигураций
 */
export type SegmentCloudCoreResponseDto = {
    /**
     * Название дефолтного сегмента
     */
    defaultSegmentName: string;

    /**
     * ID дефолтного сегмента
     */
    segmentId: string;
}