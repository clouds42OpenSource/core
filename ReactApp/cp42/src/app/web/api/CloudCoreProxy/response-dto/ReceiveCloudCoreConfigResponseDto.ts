import { SegmentCloudCoreResponseDto } from './SegmentCloudCoreResponseDto';

/**
 * Модель для получения дефолтных конфигураций для Настройки Облака
 */
export type ReceiveCloudCoreConfigResponseDto = {
    /**
     * Разница часов между Украиной и Москвой
     */
    hoursDifferenceOfUkraineAndMoscow: number;

    /**
     * Обьект дефолтного сегмента
     */
    defaultSegment: SegmentCloudCoreResponseDto;

    /**
     * Коллекция сегментов
     */
    segments: Array<SegmentCloudCoreResponseDto>;

    /**
     * Уведомление главной страницы
     */
    mainPageNotification: string;

    /**
     * Тип платежей Российкого аггрегатора
     */
    russianLocalePaymentAggregator: number;

    bannerUrl: string | null;
};