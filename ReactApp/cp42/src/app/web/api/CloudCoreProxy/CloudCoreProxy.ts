import { EditDefaultSegmentRequestDto, EditDefUkAndMscRequestDto, EditPageNotificationRequestDto, EditRussianLocaleAggregatorRequestDto } from 'app/web/api/CloudCoreProxy/request-dto';
import { ReceiveCloudCoreConfigResponseDto } from 'app/web/api/CloudCoreProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с Настройкой облака
 */
export class CloudCoreProxy extends BaseProxy {
    /**
     * Запрос на получение дефолтных конфигураций
     * @param requestKind Вид запроса
     */
    public getDefultConfiguration(requestKind: RequestKind) {
        return this.requestSender.submitRequest<ReceiveCloudCoreConfigResponseDto>(
            WebVerb.GET,
            `${ this.host }/cloud-core/default-configuration`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на предактирование разницы часов между Украиной и Москвой
     * @param requestKind Вид запроса
     * @param args параметры на редактирование разницы часов между Украиной и Москвой
     */
    public editHoursDifUkAndMsc(requestKind: RequestKind, args: EditDefUkAndMscRequestDto) {
        return this.requestSender.submitRequest<boolean>(
            WebVerb.POST,
            `${ this.host }/cloud-core/hours-difference-ua-and-msc`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на редактирование дефолтного сегмента
     * @param requestKind Вид запроса
     * @param args параметры на редактирование дефолтного сегмента
     */
    public editDefaultSegment(requestKind: RequestKind, args: EditDefaultSegmentRequestDto) {
        return this.requestSender.submitRequest<boolean>(
            WebVerb.POST,
            `${ this.host }/cloud-core/default-segment`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на редактирование контента страницы уведомления
     * @param requestKind Вид запроса
     * @param args параметры на редактирование контента страницы уведомления
     */
    public editMainPageNotification(requestKind: RequestKind, args: EditPageNotificationRequestDto) {
        return this.requestSender.submitRequest<boolean>(
            WebVerb.POST,
            `${ this.host }/cloud-core/page-notification`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на редактирование переключения аггрегатора между Юкассой и Робокассой
     * @param requestKind Вид запроса
     * @param args параметры на редактирование контента страницы уведомления
     */
    public editRussianLocaleAggregator(requestKind: RequestKind, args: EditRussianLocaleAggregatorRequestDto) {
        return this.requestSender.submitRequest<boolean>(
            WebVerb.POST,
            `${ this.host }/cloud-core/russian-locale-payment-aggregator`,
            this.getObjectArguments(requestKind, args)
        );
    }
}