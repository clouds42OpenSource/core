/**
 * Модель для отправки ID дефолтного сегмента
 */
export type EditDefaultSegmentRequestDto = {
    /**
     * ID дефолтного сегмента
     */
    defSegmentId: string;
}