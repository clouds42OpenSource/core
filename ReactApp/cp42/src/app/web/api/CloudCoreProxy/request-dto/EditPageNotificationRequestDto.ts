/**
 * Моделька для оправки отредктированной страницы уведомления
 */
export type EditPageNotificationRequestDto = {
    /**
     * Отредактированное поле для страницы уведомления
     */
    pageNotification: string;
}