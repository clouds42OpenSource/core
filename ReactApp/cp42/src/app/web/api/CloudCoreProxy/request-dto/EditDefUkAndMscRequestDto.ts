/**
 * Моделька для отправки часов разницы между Украиной и Москвой
 */
export type EditDefUkAndMscRequestDto = {
    /**
     * Разница часов
     */
    hours: number;
}