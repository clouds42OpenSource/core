/**
 * Моделька для оправки типа аггрегатора для переключения
 */
export type EditRussianLocaleAggregatorRequestDto = {
    /**
     * тип аггрегатора
     */
    aggregator: number;
}