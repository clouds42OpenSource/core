/**
 * Модель на запуск процесса склейки частей в исходный файл
 */
export type InitMergeChunksToInitialFileProcessRequestDto = {
    uploadFileId: string;
    countOfChunks: number;
};