/**
 * Модель запроса инициализации загрузки файла
 */
export type InitUploadProcessRequestDto = {
    FileName: string;
};