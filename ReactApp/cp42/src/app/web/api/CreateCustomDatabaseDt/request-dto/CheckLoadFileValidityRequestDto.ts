/**
 * Модель на проверки загруженного файла на соответствие требованиям
 */
export type CheckLoadFileValidityRequestDto = {
    uploadFileId: string;
};