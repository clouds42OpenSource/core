/**
 * Модель данных для загрузки части файла
 */
export type UploadChunkOfFileRequestDto = FormData;