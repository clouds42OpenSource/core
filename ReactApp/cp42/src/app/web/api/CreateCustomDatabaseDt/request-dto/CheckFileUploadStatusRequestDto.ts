/**
 * Модель на проверку статуса загрузки бэкапа инф. базы
 */
export type CheckFileUploadStatusRequestDto = {
    uploadFileId: string;
};