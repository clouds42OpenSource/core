export enum ETypeFile {
    DtFile,
    ZipFile
}

export type CreateFromDt = {
    ConfigurationId?: string;
    UploadedFileId: string;
    DbCaption: string;
    UsersIdForAddAccess: string[];
    NeedUserPromisePayment: boolean;
    HasSupport: boolean;
    HasAutoupdate: boolean;
    ConfigurationName?: string;
    Login?: string;
    Password?: string;
    TypeFile?: ETypeFile;
    CurrentVersion?: string | null;
};