/**
 * Модель ответа на инициализацию процесса загрузки файла
 */
export type InitUploadProcessResponseDto = {
    UploadedFileId: string;
};