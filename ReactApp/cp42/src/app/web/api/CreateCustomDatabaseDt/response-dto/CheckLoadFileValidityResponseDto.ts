/**
 * Модель ответа проверки загруженного файла на соответствия требованиям
 */
export type CheckLoadFileValidityResponseDto = {
    Result: boolean;
    ErrorMessage: string;
};