export type CreateFromDtResponseDto = {
    Success: boolean;
    Data: {
        IsComplete: boolean;
        NotEnoughMoney: number;
        Amount: number;
        Currency: string;
        CanUsePromisePayment: boolean;
        ResourceForRedirect: number
    } | null;
    Message: string;
};