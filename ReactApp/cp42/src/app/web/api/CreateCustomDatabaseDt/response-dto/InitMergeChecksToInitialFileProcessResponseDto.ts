/**
 * Модель ответа на запуск процесса склейки частей в исходный файл
 */
export type InitMergeChecksToInitialFileProcessResponseDto = {
    Result: boolean;
    ErrorMessage: boolean;
};