/**
 * Модель ответа на загрузку части файла
 */
export type UploadChunkOfFileResponseDto = {
    Result: boolean;
    ErrorMessage: string;
};