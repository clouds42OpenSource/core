/**
 * Модель ответа на проверку статуса загрузки бэкапа инф. базы
 */
export type CheckFileUploadStatusResponseDto = {
    Result: boolean;
    ErrorMessage: string;
};