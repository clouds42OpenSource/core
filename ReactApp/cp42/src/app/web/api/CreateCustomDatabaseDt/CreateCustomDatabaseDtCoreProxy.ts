import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { CreateFromDt } from 'app/web/api/CreateCustomDatabaseDt/request-dto';
import { CreateFromDtResponseDto } from 'app/web/api/CreateCustomDatabaseDt/response-dto/CreateFromDtResponseDto';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

/**
 * Прокси для работы с dt
 */
export class CreateCustomDatabaseDtCoreProxy extends BaseProxy {
    private readonly token = localStorageHelper.getJWTToken();

    private readonly accountId = localStorageHelper.getContextAccountId();

    /**
     * Создать dt
     */
    public CreateFromDt(requestKind: RequestKind, args: CreateFromDt) {
        return this.requestSender.submitRequest<CreateFromDtResponseDto, CreateFromDt>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabase/CreateFromDT`,
            this.getObjectArguments(requestKind, { AccountId: this.accountId, ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}