import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import {
    CheckLoadFileValidityRequestDto,
    CheckFileUploadStatusRequestDto,
    InitMergeChunksToInitialFileProcessRequestDto,
    InitUploadProcessRequestDto,
    UploadChunkOfFileRequestDto
} from 'app/web/api/CreateCustomDatabaseDt/request-dto';
import {
    CheckFileUploadStatusResponseDto,
    CheckLoadFileValidityResponseDto,
    InitUploadProcessResponseDto,
    InitMergeChecksToInitialFileProcessResponseDto
} from 'app/web/api/CreateCustomDatabaseDt/response-dto';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

/**
 * Прокси для работы с загрузкой файлов dt
 */
export class CreateCustomDatabaseDtProxy extends BaseProxy {
    private readonly token = localStorageHelper.getJWTToken();

    private readonly accountId = localStorageHelper.getContextAccountId();

    /**
     * Выполнить загрузку части файла
     */
    public uploadChunkOfFile(requestKind: RequestKind, args: UploadChunkOfFileRequestDto) {
        return fetch(
            `${ this.host }/api/UploadFile/UploadChunk`,
            {
                method: WebVerb.POST,
                headers: {
                    Authorization: `Bearer ${ this.token }`,
                },
                body: args
            }
        );
    }

    /**
     * Проверить загруженный файл на соответствие требованиям
     */
    public checkLoadFileValidity(requestKind: RequestKind, args: CheckLoadFileValidityRequestDto) {
        return this.requestSender.submitRequest<CheckLoadFileValidityResponseDto, CheckLoadFileValidityRequestDto>(
            WebVerb.POST,
            `${ this.host }/api/UploadFile/CheckLoadFileValidity`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Инициировать процесс загрузки файла
     */
    public initUploadProcess(requestKind: RequestKind, args: InitUploadProcessRequestDto) {
        return this.requestSender.submitRequest<InitUploadProcessResponseDto, InitUploadProcessRequestDto>(
            WebVerb.POST,
            `${ this.host }/api/UploadFile/StartUpload`,
            this.getObjectArguments(requestKind, { AccountId: this.accountId, ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запустить процесс склейки частей в исходный файл
     */
    public initMergeChunkToInitialFileProcess(requestKind: RequestKind, args: InitMergeChunksToInitialFileProcessRequestDto) {
        return this.requestSender.submitRequest<InitMergeChecksToInitialFileProcessResponseDto, InitMergeChunksToInitialFileProcessRequestDto>(
            WebVerb.POST,
            `${ this.host }/api/UploadFile/MergeChunks`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Проверить статус загрузки бэкапа инф. базы
     */
    public checkFileUploadStatus(requestKind: RequestKind, args: CheckFileUploadStatusRequestDto) {
        return this.requestSender.submitRequest<CheckFileUploadStatusResponseDto, CheckFileUploadStatusRequestDto>(
            WebVerb.GET,
            `${ this.host }/api/UploadFile/UploadStatus`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}