import { EDepartmentAction } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

export type TDepartmentRequest = {
    Name: string;
    DepartmentId: string;
    ActionType: EDepartmentAction;
};

export type EditUserRequestDto = {
    Id: string;
    Login: string;
    PhoneNumber: string;
    Email: string;
    LastName: string;
    FirstName: string;
    MiddleName: string;
    Password: string;
    ConfirmPassword: string;
    Roles: Array<string>;
    CurrentPassword?: string;
    DepartmentRequest: TDepartmentRequest | null;
};