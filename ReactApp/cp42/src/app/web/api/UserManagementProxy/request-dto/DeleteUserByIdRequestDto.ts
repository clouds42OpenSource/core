/**
 * Модель запроса на удаление пользователя
 */
export type DeleteUserByIdRequestDto = {
    AccountUserID: string;
}