/**
 * Модель запроса на получения статуса аренды 1С
 */
export type UserWithRent1CRequestDto = {
    userId: string;
};