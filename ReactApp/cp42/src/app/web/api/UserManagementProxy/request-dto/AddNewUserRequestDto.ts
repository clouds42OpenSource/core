import { TDepartmentRequest } from 'app/web/api/UserManagementProxy/request-dto/EditUserRequestDto';

/**
 * Модель запроса на добавление нового пользователя
 */
export type AddNewUserRequestDto = {
    Login: string;
    PhoneNumber: string;
    Email: string;
    LastName: string;
    FirstName: string;
    MidName: string;
    Password: string;
    ConfirmPassword: string;
    Roles: Array<string>;
    DepartmentRequest :TDepartmentRequest;
};