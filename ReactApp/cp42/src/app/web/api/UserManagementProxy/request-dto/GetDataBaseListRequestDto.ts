/**
 * Модель запроса на получение списка баз
 */
export type GetDataBaseListRequestDto = {
    'page-size': number;
    page: number;
    'user-id': string;
}