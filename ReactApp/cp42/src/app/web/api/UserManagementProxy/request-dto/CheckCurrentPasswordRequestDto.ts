/**
 * Модель запроса на проверку текущего пароля
 */
export type CheckCurrentPasswordRequestDto = {
    userId: string;
    currentPassword: string;
};

export type CheckCurrentPasswordQueryRequestDto = {
    userId: string;
    passwordHash: string;
};