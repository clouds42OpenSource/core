/**
 * Модель запроса на смену активности пользователя
 */
export type SetUserActivityRequestDto = {
    isActivate: boolean,
    accountUserID: string;
}