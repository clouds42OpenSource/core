/**
 * Модель запроса на получение списка пользователей с доступом к базам текущего аккаунта
 */
export type AccountRelatedUsersRequestDto = {
    'page-size': number;
    page: number;
    'account-related': boolean;
}