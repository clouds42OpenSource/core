/**
 * Модель запроса на валидацию логина
 */
export type ValidateUserLoginRequestDto = {
    Login: string;
    userId: string;
};

/**
 * Модель запроса на валидацию почты
 */
export type ValidateUserEmailRequestDto = {
    Email: string;
    userId: string;
};

/**
 * Модель запроса на валидацию тел. номера
 */
export type ValidateUserPhoneRequestDto = {
    PhoneNumber: string;
    userId: string;
};