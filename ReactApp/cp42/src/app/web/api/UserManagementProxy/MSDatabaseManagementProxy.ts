import { GetDataBaseListResponseDto } from 'app/web/api/UserManagementProxy/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

import { AccountRelatedUsersRequestDto } from 'app/web/api/UserManagementProxy/request-dto/GetUsersWithAccessToDBRequestDto';
import { AccountRelatedUsersResponseDto } from 'app/web/api/UserManagementProxy/response-dto/GetUsersWithAccessToDBResponseDto';
import { BaseProxy } from 'app/web/common';
import { GetDataBaseListRequestDto } from 'app/web/api/UserManagementProxy/request-dto/GetDataBaseListRequestDto';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

export class MSDatabaseManagementProxy extends BaseProxy {
    accountId = localStorageHelper.getContextAccountId();

    token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение списка баз
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
    */
    public async getDataBaseList(requestKind: RequestKind, args?: GetDataBaseListRequestDto) {
        return this.requestSender.submitRequest<GetDataBaseListResponseDto, GetDataBaseListRequestDto>(
            WebVerb.GET,
            `${ this.host }/Platform/accounts/${ this.accountId }/applications`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение списка пользователей с доступом к ИБ текущего аккаунта
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
    */
    public getAccountRelatedUsers(requestKind: RequestKind, args: AccountRelatedUsersRequestDto) {
        return this.requestSender.submitRequest<AccountRelatedUsersResponseDto, AccountRelatedUsersRequestDto>(
            WebVerb.GET,
            `${ this.host }/Platform/accounts/${ this.accountId }/users`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}