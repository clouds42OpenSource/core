/**
 * Модель ответа получение списка пользователей с доступом к ИБ текущего аккаунта
 */
export type AccountRelatedUsersResponseDto = {
    pages: number;
    'records-count': number;
    users: {
        id: string;
        name: string;
        'account-related': boolean;
    }[];
}