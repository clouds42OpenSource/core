import { AccessLevelEnum, AccessStateEnum } from 'app/common/enums/AccessStateEnum';

export type ApplicationDto = {
    name: string;
    state: AccessStateEnum;
    id: string;
    'access-level': AccessLevelEnum;
}

/**
 * Модель ответа пагинированного списка баз с МС
 */
export type GetDataBaseListResponseDto = {
    pages: number;
    'records-count': number;
    applications: ApplicationDto[]
}