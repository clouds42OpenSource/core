/**
 * Модель ответа о статусе аренды 1С
 */
export type UserWithRent1CResponseDto = {
    Success: boolean,
    Message: string
};