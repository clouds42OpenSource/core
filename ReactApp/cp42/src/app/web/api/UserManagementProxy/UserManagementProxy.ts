import { ValidateUserEmailRequestDto, ValidateUserLoginRequestDto, ValidateUserPhoneRequestDto } from 'app/web/api/UserManagementProxy/request-dto/ValidateUserRequestDto';
import { CreateUserResponseDto } from 'app/web/api/UserManagementProxy/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { AddNewUserRequestDto } from 'app/web/api/UserManagementProxy/request-dto/AddNewUserRequestDto';
import { CheckCurrentPasswordQueryRequestDto, CheckCurrentPasswordRequestDto } from 'app/web/api/UserManagementProxy/request-dto/CheckCurrentPasswordRequestDto';
import { DeleteUserByIdRequestDto } from 'app/web/api/UserManagementProxy/request-dto/DeleteUserByIdRequestDto';
import { EditUserRequestDto } from 'app/web/api/UserManagementProxy/request-dto/EditUserRequestDto';
import { SetUserActivityRequestDto } from 'app/web/api/UserManagementProxy/request-dto/SetUserActivityRequestDto';
import { UserWithRent1CRequestDto } from 'app/web/api/UserManagementProxy/request-dto/UserWithRent1CRequestDto';
import { UserWithRent1CResponseDto } from 'app/web/api/UserManagementProxy/response-dto/UserWithRent1CResponseDto';
import { BaseProxy } from 'app/web/common';
import { TResponseLowerCase } from 'app/api/types';

/**
 * Прокси работающий с Пользователями
 */
export class UserManagementProxy extends BaseProxy {
    private readonly token = localStorageHelper.getJWTToken();

    /**
     * Запрос на удаление пользователя
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public deleteUserById(requestKind: RequestKind, args?: DeleteUserByIdRequestDto) {
        return this.requestSender.submitRequest<NonNullable<unknown>, DeleteUserByIdRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountUsers/Delete`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на изменение активности пользователя
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public setUserActivity(requestKind: RequestKind, args?: SetUserActivityRequestDto) {
        return this.requestSender.submitRequest<NonNullable<unknown>, SetUserActivityRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountUsers/Activate`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на проверку доступности логина пользователя
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public IsUserLoginAviable(requestKind: RequestKind, args?: ValidateUserLoginRequestDto) {
        return this.requestSender.submitRequest<TResponseLowerCase<boolean>, { login: string, userId: string }>(
            WebVerb.GET,
            `${ this.host }/account-users-validation/is-login-available`,
            this.getObjectArguments(requestKind, { login: args?.Login ?? '', userId: args?.userId ?? '' }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на проверку доступности почты
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public IsEmailAvailable(requestKind: RequestKind, args?: ValidateUserEmailRequestDto) {
        return this.requestSender.submitRequest<TResponseLowerCase<boolean>, { email: string, userId: string }>(
            WebVerb.GET,
            `${ this.host }/account-users-validation/is-email-available`,
            this.getObjectArguments(requestKind, { email: args?.Email ?? '', userId: args?.userId ?? '' }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на проверку доступности тел. номера
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public IsPhoneAvailable(requestKind: RequestKind, args?: ValidateUserPhoneRequestDto) {
        return this.requestSender.submitRequest<TResponseLowerCase<boolean>, { phoneNumber: string, userId: string }>(
            WebVerb.GET,
            `${ this.host }/account-users-validation/is-phone-available`,
            this.getObjectArguments(requestKind, { phoneNumber: args?.PhoneNumber ?? '', userId: args?.userId ?? '' }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на добавление нового пользователя
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public addNewUser(requestKind: RequestKind, args?: AddNewUserRequestDto) {
        return this.requestSender.submitRequest<CreateUserResponseDto, AddNewUserRequestDto>(
            WebVerb.POST,
            `${ this.host }/account-users`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на редактирование пользователя
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public editUserFields(requestKind: RequestKind, args?: EditUserRequestDto) {
        return this.requestSender.submitRequest<TResponseLowerCase<boolean>, EditUserRequestDto>(
            WebVerb.PUT,
            `${ this.host }/account-users`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на проверку текущего пользователя
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public checkCurrentPassword(requestKind: RequestKind, args?: CheckCurrentPasswordRequestDto) {
        return this.requestSender.submitRequest<TResponseLowerCase<boolean>, CheckCurrentPasswordQueryRequestDto>(
            WebVerb.GET,
            `${ this.host }/account-users-validation/check-password`,
            this.getObjectArguments(requestKind, { userId: args?.userId ?? '', passwordHash: args?.currentPassword ?? '' }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение статуса активации 1С
     * @param requestKind вид запроса
     * @param args параметры запроса
     */
    public getUserWithRent1C(requestKind: RequestKind, args: UserWithRent1CRequestDto) {
        return this.requestSender.submitRequest<UserWithRent1CResponseDto, UserWithRent1CRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountUsers/UserWithRent1C`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}