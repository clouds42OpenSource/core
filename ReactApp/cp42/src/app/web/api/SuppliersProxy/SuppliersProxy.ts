import { SupplierReferralAccountsRequestDto, SupplierRequestDtoNew } from 'app/web/api/SuppliersProxy/request-dto';
import { InitSelectListItemsResponseDto, SupplierByIdResponseDto, SupplierReferralAccountsResponseDto, SuppliersPaginationResponseDto } from 'app/web/api/SuppliersProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { serialize } from 'object-to-formdata';
import { TNewResponse } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/common/types';
import { TResponseLowerCase } from 'app/api/types';

/**
 * Прокси работающий с SuppliersProxy
 */
export class SuppliersProxy extends BaseProxy {
    /**
     * Получить модель коллекций поставщиков с пагинацией
     * @param requestKind Вид запроса
     * @param page Номер страницы
     * @returns Модель коллекций поставщиков с пагинацией
     */
    public getSuppliersWithPagination(requestKind: RequestKind, page: number, pageSize = 20) {
        return this.requestSender.submitRequest<SuppliersPaginationResponseDto, void>(
            WebVerb.GET,
            `${ this.host }/suppliers-reference?pageNumber=${ page }&pageSize=${ pageSize }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить элементы комбобокса
     * @param requestKind Вид запроса
     * @returns Элементы комбобокса все локали и все печатные формы
     */
    public getInitCreateEditPageItems(requestKind: RequestKind) {
        return this.requestSender.submitRequest<InitSelectListItemsResponseDto, void>(
            WebVerb.GET,
            `${ this.host }/suppliers-reference/combo-boxes`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать поставщика
     * @param requestKind Вид запроса
     * @param args параметры запроса
     */
    public createSupplier(requestKind: RequestKind, args: SupplierRequestDtoNew) {
        const formData = serialize(args);

        return this.requestSender.submitRequest<TNewResponse<null>, FormData>(
            WebVerb.POST,
            `${ this.host }/suppliers-reference`,
            this.getObjectArguments(requestKind, formData, null)
        );
    }

    /**
     * Редактировать поставщика
     * @param requestKind Вид запроса
     * @param args параметры запроса
     */
    public editSupplier(requestKind: RequestKind, args: SupplierRequestDtoNew) {
        const formData = serialize(args);

        return this.requestSender.submitRequest<TNewResponse<null>, FormData>(
            WebVerb.PUT,
            `${ this.host }/suppliers-reference/referral-accounts`,
            this.getObjectArguments(requestKind, formData, null)
        );
    }

    /**
     * Удалить поставщика
     * @param requestKind Вид запроса
     * @param supplierId ID поставщика
     */
    public deleteSupplier(requestKind: RequestKind, supplierId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<null>>(
            WebVerb.DELETE,
            `${ this.host }/suppliers-reference/${ supplierId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить поставщика по ID
     * @param requestKind Вид запроса
     * @param supplierId ID поставщика
     * @returns Модель поставщика
     */
    public getSupplierById(requestKind: RequestKind, supplierId: string) {
        return this.requestSender.submitRequest<SupplierByIdResponseDto, void>(
            WebVerb.GET,
            `${ this.host }/suppliers-reference/${ supplierId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить список рефералов для поставщика в виде словаря
     * @param requestKind Вид запроса
     * @param supplierId ID поставщика
     * @returns Cписок словарей рефералов для поставщика
     */
    public getReferralAccountsBySupplierId(requestKind: RequestKind, supplierId: string) {
        return this.requestSender.submitRequest<SupplierReferralAccountsResponseDto, void>(
            WebVerb.GET,
            `${ this.host }/suppliers-reference/${ supplierId }/referral-accounts`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить список рефералов для поставщика по значения поиска
     * @param requestKind Вид запроса
     * @param searchValue Значение поисковика
     * @returns Cписок словарей рефералов для поставщика
     */
    public getReferralAccountsBySearchValue(requestKind: RequestKind, searchValue: string) {
        return this.requestSender.submitRequest<SupplierReferralAccountsResponseDto, void>(
            WebVerb.GET,
            `${ this.host }/accounts/as-list?searchLine=${ searchValue }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Удалить реферальный аккаунт у поставщика
     * @param requestKind Вид запроса
     * @param args параметры запроса
     */
    public deleteSupplierReferralAccount(requestKind: RequestKind, args: SupplierReferralAccountsRequestDto) {
        return this.requestSender.submitRequest<TNewResponse<null>, void>(
            WebVerb.DELETE,
            `${ this.host }/suppliers-reference/${ args.supplierId }/referral-accounts/${ args.accountId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Добавить реферальный аккаунт для поставщика
     * @param requestKind Вид запроса
     * @param args параметры запроса
     */
    public createSupplierReferralAccount(requestKind: RequestKind, args: SupplierReferralAccountsRequestDto) {
        return this.requestSender.submitRequest<TNewResponse<null>, SupplierReferralAccountsRequestDto>(
            WebVerb.POST,
            `${ this.host }/suppliers-reference/${ args.supplierId }/referral-accounts/${ args.accountId }`,
            this.getObjectArguments(requestKind, args)
        );
    }
}