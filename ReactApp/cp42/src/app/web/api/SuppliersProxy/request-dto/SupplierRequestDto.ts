import { SupplierResponseDto, SupplierResponseDtoOld } from 'app/web/api/SuppliersProxy/response-dto/SupplierResponseDto';

/**
 * Параметры создания и редактирования поставщика
 */
export type SupplierRequestDto = SupplierResponseDtoOld & {
    /**
     * Файл договора офферты
     */
    CloudFile?: File;
    /**
     * ID локали
     */
    LocaleId: string;
    /**
     * ID Договор оферты
     */
    AgreementId?: string;
    /**
     * Печатная форма счета на оплату
     */
    PrintedHtmlFormInvoiceId?: string;
    /**
     * Печатная форма фискального чека
     */
    PrintedHtmlFormInvoiceReceiptId?: string;
};

export type SupplierRequestDtoNew = SupplierResponseDto & {
    cloudFile?: File;
    localeId: string;
    agreementId?: string;
    printedHtmlFormInvoiceId?: string;
    printedHtmlFormInvoiceReceiptId?: string;
};