/**
 * Модель параметров для получения реферальных аккаунтов поставщика
 */
export type SupplierReferralAccountsRequestDto = {
    /**
     * ID поставщика
     */
    supplierId: string;
    /**
     * ID реферального аккаунта
     */
    accountId: string;
};