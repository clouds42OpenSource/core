import { SupplierResponseDto } from 'app/web/api/SuppliersProxy/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель коллекций поставщиков с пагинацией
 */
export type SuppliersPaginationResponseDto = SelectDataMetadataResponseDto<SupplierResponseDto>;