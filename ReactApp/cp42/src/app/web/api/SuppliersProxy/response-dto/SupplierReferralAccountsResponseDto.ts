import { KeyValueResponseDto } from 'app/web/common/response-dto';

/**
 * Модель словаря для реферальных аккаунтов поставщика
 * Key - ID реферального аккаунта
 * Value - название реферального аккаунта
 */
export type SupplierReferralAccountsResponseDto = Array<KeyValueResponseDto<string, string>>;