import { SupplierRequestDto } from 'app/web/api/SuppliersProxy/request-dto';

/**
 * Модель для получения поставщика по ID
 */
export type SupplierByIdResponseDto = SupplierRequestDto & {
    /**
     * Названия файла офферты
     */
    AgreementFileName?: string;
};