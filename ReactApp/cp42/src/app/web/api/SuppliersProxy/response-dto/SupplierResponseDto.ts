/**
 * Модель поставщика
 */
export type SupplierResponseDto = {
    /**
     * ID поставщика
     */
    id: string;
    /**
     * Название поставщика
     */
    name: string;
    /**
     * Код поставщика
     */
    code: string;
    /**
     * Название локали
     */
    localeName?: string;
    /**
     * Стандартный поставщик на локаль
     */
    isDefault: boolean;
};

export type SupplierResponseDtoOld = {
    /**
     * ID поставщика
     */
    Id: string;
    /**
     * Название поставщика
     */
    Name: string;
    /**
     * Код поставщика
     */
    Code: string;
    /**
     * Название локали
     */
    LocaleName?: string;
    /**
     * Стандартный поставщик на локаль
     */
    IsDefault: boolean;
};