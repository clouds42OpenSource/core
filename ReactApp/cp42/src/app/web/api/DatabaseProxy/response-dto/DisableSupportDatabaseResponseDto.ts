import { SupportState } from 'app/common/enums';

/**
 * Информация о подключении информационной базы к АО
 */
export type DisableSupportDatabaseResponseDto = {
    /**
     * Если true, значит идёт аутентификация к базе или уже пройдена
     */
    IsConnects: boolean;

    /**
     * Последняя дата тех-поддержки
     */
    LastHistoryDate?: string;

    /**
     * Состояние базы на тех-подержке
     */
    SupportState: SupportState;

    /**
     * Описание состояния
     */
    SupportStateDescription: string;
};