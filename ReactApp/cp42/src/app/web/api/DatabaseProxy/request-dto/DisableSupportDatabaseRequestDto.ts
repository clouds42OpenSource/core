/**
 * Параметры запроса на удаление техподдержки базы базы
 */
export type DisableSupportDatabaseRequestDto = {
    /**
     * Id информационной базы, которую нужно удалить
     */
    databaseId: string;
};