import { DatabaseState, DistributionType, PlatformType } from 'app/common/enums';

/**
 * Модель для редактирования данных базы данных
 */
export type EditDatabaseDataRequestDto = {
    /**
     * ID базы
     */
    DatabaseId: string;
    /**
     * Номер базы
     */
    V82Name: string;
    /**
     * Наименование базы
     */
    DatabaseCaption: string;
    /**
     * Шаблон базы
     */
    DatabaseTemplateId: string;
    /**
     * Тип платформы
     */
    PlatformType: PlatformType;
    /**
     * Дистрибуция, версия
     */
    DistributionType: DistributionType;
    /**
     * Маркер о том что опубликованы веб сервисы
     */
    UsedWebServices: boolean;
    /**
     * Состояние базы
     */
    DatabaseState: DatabaseState;
    /**
     * Файловое хранилище
     */
    FileStorageId: string;
};