import { DatabaseState, PlatformType } from 'app/common/enums';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель элемента базы данных для списка баз
 */
export type DatabaseListItemDto = {
    /**
     * Id базы
     */
    id: string;

    /**
     * Название базы
     */
    v82Name: string;

    /**
     * Описание
     */
    caption: string;

    /**
     * ID аккаунта
     */
    accountId: string;

    /**
     * Название аккаунта
     */
    accountCaption: string;

    /**
     * Номер аккаунта
     */
    accountNumber: number;

    /**
     * Дата создания
     */
    creationDate: string;

    /**
     * Дата последней активности
     */
    lastActivityDate: string;

    /**
     * Статус базы
     */
    state: DatabaseState;

    /**
     * База файловая
     */
    isFile?: boolean;

    /**
     * Название платформы
     */
    applicationName: string;

    /**
     * Размер базы (МБ)
     */
    sizeInMB: number;

    /**
     * Заблокированное состояние
     */
    lockedState: string;

    /**
     * Тип платформы
     */
    platformType: PlatformType;

    isPublish: boolean;
};

/**
 * Модель ответа на получение списка баз данных
 */
export type GetDatabaseListResultResponseDto = SelectDataMetadataResponseDto<DatabaseListItemDto>;

/**
 * Модель фильтра для выбора списка баз данных
 */
export type DatabaseListFilterParamsDto = {
    /**
     * Строка поиска
     */
    searchLine: string;
};

/**
 * Модель параметров на запрос получения отраслей
 */
export type GetDatabaseListParamsRequestDto = SelectDataCommonRequestDto<DatabaseListFilterParamsDto>;