/**
 * Параметры запроса на удаление техподдержки базы базы
 */
export type EnableSupportDatabaseRequestDto = {
    /**
     * Id информационной базы, которую нужно удалить
     */
    databaseId: string;
    /**
     * Логин администратора в базу
     */
    login: string;
    /**
     * Пароль администратора в базу
     */
    pass: string;
};