/**
 * Параметры запроса на отмену публикации информационной базы
 */
export type CancelPublishDatabaseRequestDto = {
    /**
     * Id информационной базы, у которой нужно отменит публикацию
     */
    databaseId: string;
    /**
     * Опубликовать/снять с публикации
     */
    publish: boolean;
};