/**
 * Параметры запроса на публикацию информационной базы
 */
export type PublishDatabaseRequestDto = {
    /**
     * Id информационной базы, которую нужно опубликовать
     */
    databaseId: string;
    /**
     * Опубликовать/снять с публикации
     */
    publish: boolean;
};