/**
 * Параметры запроса на удаление информационной базы
 */
export type DeleteDatabaseRequestDto = {
    /**
     * Id информационной базы, которую нужно удалить
     */
    databaseId: string;
};