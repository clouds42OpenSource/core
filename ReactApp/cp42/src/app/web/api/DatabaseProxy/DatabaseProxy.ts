import { DeleteDatabaseRequestDto, DisableSupportDatabaseRequestDto, EditDatabaseDataRequestDto, PublishDatabaseRequestDto } from 'app/web/api/DatabaseProxy/request-dto';
import { CancelPublishDatabaseRequestDto } from 'app/web/api/DatabaseProxy/request-dto/CancelPublishDatabaseRequestDto';
import { EnableSupportDatabaseRequestDto } from 'app/web/api/DatabaseProxy/request-dto/EnableSupportDatabaseRequestDto';
import { GetDatabaseListParamsRequestDto, GetDatabaseListResultResponseDto } from 'app/web/api/DatabaseProxy/request-dto/GetDatabaseListDtos';
import { DisableSupportDatabaseResponseDto } from 'app/web/api/DatabaseProxy/response-dto/DisableSupportDatabaseResponseDto';
import { EnableSupportDatabaseResponseDto } from 'app/web/api/DatabaseProxy/response-dto/EnableSupportDatabaseResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { TResponseLowerCase } from 'app/api/types';

/**
 * Прокси работающий с Database
 */
export class DatabaseProxy extends BaseProxy {
    /**
     * Запрос на получение карточки информационной базы
     * @param requestKind Вид запроса
     * @param args Параметры для удаления информационной базы
     * @returns Promise с объектом ответа/результатом на запрос
     */
    public deleteDatabase(requestKind: RequestKind, args: DeleteDatabaseRequestDto) {
        return this.requestSender.submitRequest<void, DeleteDatabaseRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/account-databases/${ args.databaseId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на получение карточки информационной базы
     * @param requestKind Вид запроса
     * @param args Параметры для публиукации информационной базы
     * @returns Promise с объектом ответа/результатом на запрос
     */
    public publishDatabase(requestKind: RequestKind, args: PublishDatabaseRequestDto) {
        return this.requestSender.submitRequest<void, PublishDatabaseRequestDto>(
            WebVerb.POST,
            `${ this.host }/account-databases/publish`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на отмену публикацию базы
     * @param requestKind Вид запроса
     * @param args Параметры для отмены публиукации информационной базы
     * @returns Promise с объектом ответа/результатом на запрос
     */
    public cancelPublishDatabase(requestKind: RequestKind, args: CancelPublishDatabaseRequestDto) {
        return this.requestSender.submitRequest<void, CancelPublishDatabaseRequestDto>(
            WebVerb.PUT,
            `${ this.host }/account-databases/cancel-publish/${ args.databaseId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на удаления техподдержки базы
     * @param requestKind Вид запроса
     * @param args параметры для удаления техподдержки базы
     */
    public disableDatabaseSupport(requestKind: RequestKind, args: DisableSupportDatabaseRequestDto) {
        return this.requestSender.submitRequest<DisableSupportDatabaseResponseDto, DisableSupportDatabaseRequestDto>(
            WebVerb.POST,
            `${ this.host }/account-databases/deauthorization`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на подключение техподдержки базы
     * @param requestKind Вид запроса
     * @param args параметры для подключения техподдержки базы
     */
    public enableDatabaseSupport(requestKind: RequestKind, args: EnableSupportDatabaseRequestDto) {
        return this.requestSender.submitRequest<TResponseLowerCase<EnableSupportDatabaseResponseDto>, EnableSupportDatabaseRequestDto>(
            WebVerb.POST,
            `${ this.host }/account-databases/authorization`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на редактирование данных базы
     * @param requestKind Вид запроса
     * @param args параметры для редактирования данных
     */
    public EditDatabaseData(requestKind: RequestKind, args: EditDatabaseDataRequestDto) {
        return this.requestSender.submitRequest<void, EditDatabaseDataRequestDto>(
            WebVerb.PUT,
            `${ this.host }/account-databases`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на получение списка баз данных
     * @param requestKind Вид запроса
     * @param args параметры для получения списка баз данных
     */
    public receiveDatabaseList(requestKind: RequestKind, args: GetDatabaseListParamsRequestDto): Promise<GetDatabaseListResultResponseDto> {
        return this.requestSender.submitRequest<GetDatabaseListResultResponseDto, GetDatabaseListParamsRequestDto>(
            WebVerb.GET,
            `${ this.host }/account-databases`,
            this.getObjectArguments(requestKind, args)
        );
    }
}