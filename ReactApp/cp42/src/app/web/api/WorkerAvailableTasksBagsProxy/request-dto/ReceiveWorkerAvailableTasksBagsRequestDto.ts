import { ReceiveWorkerAvailableTasksBagsFilterRequestDto } from 'app/web/api/WorkerAvailableTasksBagsProxy/request-dto/ReceiveWorkerAvailableTasksBagsFilterRequestDto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель запроса для получения данных по взаимодействию воркера и задач
 */
export type ReceiveWorkerAvailableTasksBagsRequestDto = SelectDataCommonRequestDto<ReceiveWorkerAvailableTasksBagsFilterRequestDto>;