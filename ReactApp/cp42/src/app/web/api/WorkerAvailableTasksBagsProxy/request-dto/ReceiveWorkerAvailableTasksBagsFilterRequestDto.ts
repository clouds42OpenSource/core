import { Nullable } from 'app/common/types';

export type ReceiveWorkerAvailableTasksBagsFilterRequestDto = {
    /**
     * ID воркера
     */
    WorkerId: Nullable<number>;
    /**
     * ID задачи
     */
    TaskId: Nullable<string>;
    /**
     * Приоритет задачи
     */
    TaskPriority: Nullable<number>;
};