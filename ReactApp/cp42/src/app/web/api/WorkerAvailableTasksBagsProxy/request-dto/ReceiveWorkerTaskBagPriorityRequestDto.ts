/**
 * Модель запроса на получение данных по приоритету доступной задачи воркера
 */
export type ReceiveWorkerTaskBagPriorityRequestDto = {
    /**
     * ID воркера
     */
    workerId: number;
    /**
     * ID задачи
     */
    taskId: string;
};