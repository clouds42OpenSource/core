/**
 * Модель запроса на смену приоритета доступной задачи воркера
 */
export type ChangeWorkerTaskBagPriorityRequestDto = {
    WorkerId: number;
    TaskId: string;
    TaskPriority: number;
};