import { WorkerAvailableTasksBagItemResponseDto } from 'app/web/api/WorkerAvailableTasksBagsProxy/response-dto/WorkerAvailableTasksBagItemResponseDto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении данных по взаимодействию воркера и задач
 */
export type WorkerAvailableTasksBagsDataResponseDto = SelectDataMetadataResponseDto<WorkerAvailableTasksBagItemResponseDto>;