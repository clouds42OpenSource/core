/**
 * Модель ответа с элементом взаимодействия воркера и задачи
 */
export type WorkerAvailableTasksBagItemResponseDto = {
    /**
     * ID задачи
     */
    taskId: string;
    /**
     * Название задачи
     */
    taskName: string;
    /**
     * Приоритет задачи
     */
    taskPriority: number;
    /**
     * ID воркера
     */
    workerId: number;
    /**
     * Название воркера
     */
    workerName: string;
};