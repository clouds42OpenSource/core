/**
 * Модель ответа на получение данных по приоритету доступной задачи воркера
 */
export type WorkerTaskBagPriorityResponseDto = {
    /**
     * ID воркера
     */
    workerId: number;
    /**
     * ID задачи
     */
    taskId: string;
    /**
     * Приоритет задачи
     */
    taskPriority: number;
    /**
     * Название воркера
     */
    workerName: string;
    /**
     * Название задачи
     */
    taskName: string;
};