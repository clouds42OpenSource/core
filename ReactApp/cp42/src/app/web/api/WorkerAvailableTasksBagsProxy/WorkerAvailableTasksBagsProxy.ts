import { ChangeWorkerTaskBagPriorityRequestDto, ReceiveWorkerAvailableTasksBagsRequestDto, ReceiveWorkerTaskBagPriorityRequestDto } from 'app/web/api/WorkerAvailableTasksBagsProxy/request-dto';
import { WorkerAvailableTasksBagsDataResponseDto, WorkerTaskBagPriorityResponseDto } from 'app/web/api/WorkerAvailableTasksBagsProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с данными по взаимодействию воркера и задач
 */
export class WorkerAvailableTasksBagsProxy extends BaseProxy {
    /**
     * Выполнить запрос на получение данных по заимодействию воркера и задач
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public receiveWorkerAvailableTasksBags(requestKind: RequestKind, args: ReceiveWorkerAvailableTasksBagsRequestDto) {
        return this.requestSender.submitRequest<WorkerAvailableTasksBagsDataResponseDto, ReceiveWorkerAvailableTasksBagsRequestDto>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks/available-tasks`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Выполнить запрос на получение данных по приоритету доступной задачи воркера
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public receiveWorkerTaskBagPriority(requestKind: RequestKind, args: ReceiveWorkerTaskBagPriorityRequestDto) {
        return this.requestSender.submitRequest<WorkerTaskBagPriorityResponseDto, ReceiveWorkerTaskBagPriorityRequestDto>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks/${ args.workerId }/available-tasks/${ args.taskId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Выполнить запрос на смену приоритета доступной задачи воркера
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public changeWorkerTaskBagPriority(requestKind: RequestKind, args: ChangeWorkerTaskBagPriorityRequestDto) {
        return this.requestSender.submitRequest<void, ChangeWorkerTaskBagPriorityRequestDto>(
            WebVerb.POST,
            `${ this.host }/core-worker-tasks/available-tasks/priority`,
            this.getObjectArguments(requestKind, args)
        );
    }
}