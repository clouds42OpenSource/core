/**
 * Модель ответа на генерацию нового JWT для переданного CloudService
 */
export type GenerateJWTForCloudServiceResponseDto = {
    /**
     * Сгенерированный JsonWebToken
     */
    jsonWebToken: string;
}