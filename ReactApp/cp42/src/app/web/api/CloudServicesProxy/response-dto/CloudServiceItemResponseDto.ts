/**
 * Модель ответа при получении всех записей CloudServices
 */
export type CloudServiceItemResponseDto = {
    /**
     * ID
     */
    id: string;

    /**
     * ID2
     */
    cloudServiceId: string;

    /**
     * Наименование службы
     */
    serviceCaption: string;

    /**
     * Токен для аутентификации службы
     */
    jsonWebToken: string;
};