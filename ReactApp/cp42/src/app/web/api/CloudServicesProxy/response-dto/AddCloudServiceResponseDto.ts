/**
 * Модель ответа на добавление нового CloudService
 */
export type AddCloudServiceResponseDto = {
    /**
     * Значение для поляId в таблице CloudService
     */
    Id: string;
};