/**
 * Модель параметров на генерацию нового JWT для переданного CloudService
 */
export type GenerateJWTForCloudServiceRequestDto = {
    /**
     * ID CloudServices, для которого нужно сгенерировать JWT
     */
    CloudServiceId: string;
}