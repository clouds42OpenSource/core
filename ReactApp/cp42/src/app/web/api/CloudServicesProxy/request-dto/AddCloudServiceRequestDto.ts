/**
 * Модель параметров для добавления записи CloudService
 */
export type AddCloudServiceRequestDto = {
    /**
     * ID CloudService
     */
    cloudServiceId: string;

    /**
     * Наименование службы
     */
    serviceCaption: string;

    /**
     * Новый JsonWebToken
     */
    jsonWebToken: string;
};