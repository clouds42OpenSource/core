/**
 * Модель параметров для обновления записи CloudService
 */
export type UpdateCloudServiceItemRequestDto = {
    /**
     * ID CloudService
     */
    cloudServiceId: string;

    /**
     * Наименование службы
     */
    serviceCaption: string;

    /**
     * Новый JsonWebToken
     */
    jsonWebToken: string;
};