import { AddCloudServiceRequestDto } from 'app/web/api/CloudServicesProxy/request-dto';
import { GenerateJWTForCloudServiceRequestDto } from 'app/web/api/CloudServicesProxy/request-dto/GenerateJWTForCloudServiceRequestDto';
import { UpdateCloudServiceItemRequestDto } from 'app/web/api/CloudServicesProxy/request-dto/UpdateCloudServiceItemRequestDto';
import { AddCloudServiceResponseDto, GenerateJWTForCloudServiceResponseDto } from 'app/web/api/CloudServicesProxy/response-dto';
import { CloudServiceItemResponseDto } from 'app/web/api/CloudServicesProxy/response-dto/CloudServiceItemResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с CloudServices
 */
export class CloudServicesProxy extends BaseProxy {
    /**
     * Запрос на получение справочника CloudServices
     * @param requestKind Вид запроса
     */
    public receiveCloudServices(requestKind: RequestKind) {
        return this.requestSender.submitRequest<CloudServiceItemResponseDto[], NonNullable<unknown>>(
            WebVerb.GET,
            `${ this.host }/cloud-services`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на обновление записи справочника CloudServices
     * @param requestKind Вид запроса
     * @param args параметры на обновление записи справочника CloudServices
     */
    public updateCloudServiceItem(requestKind: RequestKind, args: UpdateCloudServiceItemRequestDto) {
        return this.requestSender.submitRequest<boolean, UpdateCloudServiceItemRequestDto>(
            WebVerb.PUT,
            `${ this.host }/cloud-services`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на добавление записи справочника CloudServices
     * @param requestKind Вид запроса
     * @param args параметры на добавление записи справочника CloudServices
     */
    public addCloudService(requestKind: RequestKind, args: AddCloudServiceRequestDto) {
        return this.requestSender.submitRequest<AddCloudServiceResponseDto, AddCloudServiceRequestDto>(
            WebVerb.POST,
            `${ this.host }/cloud-services`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на обновление записи справочника CloudServices
     * @param requestKind Вид запроса
     * @param args параметры на обновление записи справочника CloudServices
     */
    public generateJWTForCloudService(requestKind: RequestKind, args: GenerateJWTForCloudServiceRequestDto) {
        return this.requestSender.submitRequest<GenerateJWTForCloudServiceResponseDto, GenerateJWTForCloudServiceRequestDto>(
            WebVerb.GET,
            `${ this.host }/cloud-services/${ args.CloudServiceId }/jwt`,
            this.getObjectArguments(requestKind)
        );
    }
}