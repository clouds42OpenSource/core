/**
 * Enum полей для Печатных форм HTML
 */
export enum PrintedHtmlFormFieldEnum {
    /**
     * Поле Html разметки
     */
    HtmlData = 'HtmlData',
    /**
     * Поле новых прикрепленных файлов
     */
    UploadedFiles = 'UploadedFiles'
}