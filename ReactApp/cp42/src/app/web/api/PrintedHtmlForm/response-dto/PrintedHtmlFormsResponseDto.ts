import { PrintedHtmlFormInfoResponseDto } from 'app/web/api/PrintedHtmlForm/response-dto/PrintedHtmlFormInfoResponseDto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении данных по логам действий облака
 */
export type PrintedHtmlFormsResponseDto = SelectDataMetadataResponseDto<PrintedHtmlFormInfoResponseDto>;