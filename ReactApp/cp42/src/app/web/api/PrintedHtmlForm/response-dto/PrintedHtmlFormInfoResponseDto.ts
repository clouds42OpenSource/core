/**
 * Информация печатной формы HTML
 */
export type PrintedHtmlFormInfoResponseDto = {
    /**
     * Id печатной формы
     */
    id: string;
    /**
     * Название печатной формы Html
     */
    name: string;
}