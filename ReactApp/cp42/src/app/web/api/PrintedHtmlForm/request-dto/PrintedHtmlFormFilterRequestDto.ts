/**
 * Модель фильтра печатных форм HTML
 */
export type PrintedHtmlFormFilterRequestDto = {
    name?: string;
}