import { CloudFileRequestDto } from 'app/web/api/PrintedHtmlForm/request-dto/CloudFileRequestDto';

/**
 * Базовый модель печатной формы
 */
export type BasePrintedHtmlFormRequestDto = {
    /**
     * Название печатной формы Html
     */
    name: string;

    /**
     * Html разметка
     */
    htmlData: string;

    /**
     * Тип модели
     */
    modelType: string;

    /**
     * Прикрепленные файлы
     */
    files?: CloudFileRequestDto[];

    /**
     * Новые прикрепленные файлы
     */
    uploadedFiles?: File[];
}