import { BasePrintedHtmlFormRequestDto } from 'app/web/api/PrintedHtmlForm/request-dto';

/**
 * Модель для создания печатных форм HTML
 */
export type CreatePrintedHtmlFormRequestDto = BasePrintedHtmlFormRequestDto;