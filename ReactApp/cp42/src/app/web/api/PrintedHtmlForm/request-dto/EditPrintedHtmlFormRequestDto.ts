import { BasePrintedHtmlFormRequestDto } from 'app/web/api/PrintedHtmlForm/request-dto';

/**
 * Модель для редактирования печатных форм HTML
 */
export type EditPrintedHtmlFormRequestDto = BasePrintedHtmlFormRequestDto & {
    /**
     * Id печатной формы
     */
    id?: string;
}