import { CreatePrintedHtmlFormRequestDto, EditPrintedHtmlFormRequestDto, PrintedHtmlFormFilterRequestDto } from 'app/web/api/PrintedHtmlForm/request-dto';
import { PrintedHtmlFormsResponseDto } from 'app/web/api/PrintedHtmlForm/response-dto';
import { PrintedHtmlFormFieldEnum } from 'app/web/api/PrintedHtmlForm/types';
import { BaseProxy } from 'app/web/common';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';
import { KeyValueResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { serialize } from 'object-to-formdata';

/**
 * Прокси работающий с PrintedHtmlFormProxy
 */
export class PrintedHtmlFormProxy extends BaseProxy {
    /**
     * Запрос на получение агентских соглашений
     * @param requestKind Вид запроса
     */
    public receivePrintedHtmlFormsForAgencyAgreement(requestKind: RequestKind) {
        return this.requestSender.submitRequest<KeyValueResponseDto<string, string>[], void>(
            WebVerb.GET,
            `${ this.host }/agency-agreements/printed-html-forms/as-key-value`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на получение данных печатных форм HTML с пагинацией
     * @param requestKind Вид запроса
     * @param args параметры на получение данных печатных форм HTML с пагинацией
     */
    public getPrintedHtmlFormsWithPagination(requestKind: RequestKind, args: SelectDataCommonRequestDto<PrintedHtmlFormFilterRequestDto>) {
        return this.requestSender.submitRequest<PrintedHtmlFormsResponseDto>(
            WebVerb.GET,
            `${ this.host }/printed-html-form-references`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить список типов моделей
     * @param requestKind Вид запроса
     * @returns Список типов моделей
     */
    public getModelTypes(requestKind: RequestKind) {
        return this.requestSender.submitRequest<KeyValueResponseDto<string, string>[], void>(
            WebVerb.GET,
            `${ this.host }/printed-html-form-references/model-types`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать печатную форму HTML
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createPrintedHtmlForm(requestKind: RequestKind, args: CreatePrintedHtmlFormRequestDto) {
        const formData = this.serializeToFormDataPrintedHtmlForm(args);

        return this.requestSender.submitRequest<void, FormData>(
            WebVerb.POST,
            `${ this.host }/printed-html-form-references`,
            this.getObjectArguments(requestKind, formData, null)
        );
    }

    /**
     * Получить печатную форму HTML по ID
     * @param requestKind Вид запроса
     * @param printedHtmlId ID печатной формы HTML
     * @returns Модель печатной формы полученный по ID
     */
    public getPrintedHtmlFormById(requestKind: RequestKind, printedHtmlId: string) {
        return this.requestSender.submitRequest<EditPrintedHtmlFormRequestDto>(
            WebVerb.GET,
            `${ this.host }/printed-html-form-references/${ printedHtmlId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Редактировать печатную форму HTML
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editPrintedHtmlForm(requestKind: RequestKind, args: EditPrintedHtmlFormRequestDto) {
        const formData = this.serializeToFormDataPrintedHtmlForm(args);

        return this.requestSender.submitRequest<void, FormData>(
            WebVerb.PUT,
            `${ this.host }/printed-html-form-references/${ formData.get('id') }`,
            this.getObjectArguments(requestKind, formData, null)
        );
    }

    /**
     * Скачать документ PDF с тестовыми данными
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns PDF файл тестового шаблона
     */
    public downloadDocumentWithTestDataForPrintedHtmlForm(requestKind: RequestKind, args: EditPrintedHtmlFormRequestDto) {
        const formData = this.serializeToFormDataPrintedHtmlForm(args);

        return this.requestSender.submitRequestFile(
            WebVerb.POST,
            `${ this.host }/printed-html-form-references/test-data`,
            this.getObjectArguments(requestKind, formData, null)
        );
    }

    /**
     * Сериализация обьектов параметра создания/редактирование в FormData
     * @param args обьект параметра создания/редактирование
     * @returns FormData
     */
    private serializeToFormDataPrintedHtmlForm(args: CreatePrintedHtmlFormRequestDto | EditPrintedHtmlFormRequestDto) {
        const formData = serialize(args, { indices: true });
        formData.delete(PrintedHtmlFormFieldEnum.HtmlData);
        formData.append(PrintedHtmlFormFieldEnum.HtmlData, encodeURI(args.htmlData));

        if (args.uploadedFiles && args.uploadedFiles.length > 0) {
            formData.delete(PrintedHtmlFormFieldEnum.UploadedFiles);
            args.uploadedFiles?.map(file => (formData.append(PrintedHtmlFormFieldEnum.UploadedFiles, file ?? '')));
        }

        return formData;
    }
}