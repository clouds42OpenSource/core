import { domain } from 'app/common/constants';
import { MigrateSelectedDatabasesRequestDto } from 'app/web/api/AccountProxy/requests-dto';
import { AccountDatabasesMigrationResultResponseDto, GetAccountDatabasesForMirgationParamsRequestDto } from 'app/web/api/AccountProxy/requests-dto/AccountDatabasesForMigrationDtos';
import { GetAccountListParamsRequestDto, GetAccountListResultResponseDto } from 'app/web/api/AccountProxy/requests-dto/GetAccountListDtos';
import { GotoToAccountRequestDto } from 'app/web/api/AccountProxy/requests-dto/GotoToAccountRequestDto';
import { AccountSummaryDataResponseDto } from 'app/web/api/AccountProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import Cookies from 'js-cookie';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { Credentials } from 'app/api/endpoints/global/enum';
import { accountId } from 'app/api';

/**
 * Прокси работающий с Account
 */
export class AccountProxy extends BaseProxy {
    /**
     * Установка cookie для входа в чужой аккаунт
     * @param args Параметры для переходя на другой аккаунт
     * @returns Promise с объектом ответа/результатом на запрос
     */
    public gotoToAccount(args: GotoToAccountRequestDto) {
        Cookies.set('SelectedContextAccount', args.accountId, { domain, expires: 1, secure: true });
        localStorageHelper.setItem(Credentials.ContextAccountId, args.accountId);
    }

    /**
     * Установка cookie для выхода из чужого аккаунта
     * @returns Promise с объектом ответа/результатом на запрос
     */
    public exitFromDifferentAccount() {
        Cookies.remove('SelectedContextAccount', { domain, expires: 1, secure: true });
        localStorageHelper.setItem(Credentials.ContextAccountId, accountId());
    }

    /**
     * Запрос на получение списка аккаунтов
     * @param requestKind Вид запроса
     * @param args параметры для получения списка аккаунтов
     */
    public receiveAccountList(requestKind: RequestKind, args: GetAccountListParamsRequestDto): Promise<GetAccountListResultResponseDto> {
        return this.requestSender.submitRequest<GetAccountListResultResponseDto, GetAccountListParamsRequestDto>(
            WebVerb.GET,
            `${ this.host }/accounts`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на получение списка аккаунтов
     * @param requestKind Вид запроса
     */
    public getAccountSummaryData(requestKind: RequestKind): Promise<AccountSummaryDataResponseDto> {
        return this.requestSender.submitRequest<AccountSummaryDataResponseDto, void>(
            WebVerb.GET,
            `${ this.host }/accounts/footer`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на получение списка информационных баз аккаунта для миграций
     * @param requestKind Вид запроса
     * @param args параметры для получения информационных баз аккаунта для миграций
     */
    public getAccountDatabasesForMigration(requestKind: RequestKind, args: GetAccountDatabasesForMirgationParamsRequestDto): Promise<AccountDatabasesMigrationResultResponseDto> {
        return this.requestSender.submitRequest<AccountDatabasesMigrationResultResponseDto, GetAccountDatabasesForMirgationParamsRequestDto>(
            WebVerb.GET,
            `${ this.host }/accounts/database-migration`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на получение списка информационных баз аккаунта для миграций
     * @param requestKind Вид запроса
     * @param args параметры для получения информационных баз аккаунта для миграций
     */
    public migrateSelectedDatabases(requestKind: RequestKind, args: MigrateSelectedDatabasesRequestDto): Promise<void> {
        return this.requestSender.submitRequest<void, MigrateSelectedDatabasesRequestDto>(
            WebVerb.PUT,
            `${ this.host }/accounts/database-migration`,
            this.getObjectArguments(requestKind, args)
        );
    }
}