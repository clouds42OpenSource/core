/**
 * Модель на запрос миграции выбранных баз в новое хранилище
 */
export type MigrateSelectedDatabasesRequestDto = {
    /**
     * Выбранные номера баз для миграции
     */
    databases: Array<string>;
    /**
     * Выбранное хранилище для миграции
     */
    storage: string;
};