/**
 * Параметры для переходя на другой аккаунт
 */
export type GotoToAccountRequestDto = {
    /**
     * На какой аккаунт перейти
     */
    accountId: string;
};