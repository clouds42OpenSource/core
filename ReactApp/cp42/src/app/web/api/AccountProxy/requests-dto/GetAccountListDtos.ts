import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель элемента аккаунта для списка аккаунтов
 */
export type AccountListItemDto = {
    /**
     * Id аккаунта
     */
    accountId: string;

    /**
     * Номер индекса
     */
    indexNumber: number;

    /**
     * Заголовок аккаунта
     */
    accountCaption: string;

    /**
     * Логин Админ аккаунта
     */
    accountAdminLogin: string;

    /**
     * Почта Админ аккаунта
     */
    accountAdminEmail: string;

    /**
     * Количество пользователей
     */
    accountUsersCount: number;

    /**
     * Дата регистрации аккаунта
     */
    accountRegistrationDate: string;

    /**
     * Истечение срока действия
     */
    rent1CExpiredDate: string | null;

    /**
     * Флаг, показывающий является ли аккаунт VIP
     */
    isVip: boolean;

    /**
     * Количество пользователей с активной арендой
     */
    countOfActiveRent1CUsers: number;
};

/**
 * Модель ответа на получение списка аккаунтов
 */
export type GetAccountListResultResponseDto = SelectDataMetadataResponseDto<AccountListItemDto>;

/**
 * Модель фильтра для выбора аккаунтов
 */
export type AccountListFilterParamsDto = {
    /**
     * Строка поиска
     */
    searchLine: string;

    /**
     * Начальная дата регистрации аккаунта
     */
    registeredFrom: string | null;

    /**
     * Конечная дата регистрации аккаунта
     */
    registeredTo: string | null;

    /**
     * Индикатор что бы показать только мои аккаунты
     */
    onlyMine: boolean | null;
};

/**
 * Модель параметров на запрос получения отраслей
 */
export type GetAccountListParamsRequestDto = SelectDataCommonRequestDto<AccountListFilterParamsDto>;