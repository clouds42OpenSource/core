import { DatabaseState } from 'app/common/enums';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';
import { KeyValueResponseDto, SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель фильтра для выбора информационных бах
 */
export type AccountDatabaseMigrationFilterParamsRequestDto = {
    /**
     * Id аккаунта
     */
    accountIndexNumber: number;

    /**
     * Строка поиска
     */
    searchLine?: string;
    accountId: string;

    /**
     * Тип хранилища, <c>true</c> - файловое, <c>false</c> - серверное, <c>null</c> - файловое и серверное
     */
    isTypeStorageFile?: boolean | null;
};

/**
 * Модель информационной базы для миграций
 */
export type AccountDatabaseMigrationItemResponseDto = {
    /**
     * ID базы аккаунта
     */
    accountDatabaseId: string;

    /**
     * Номер информационной базы
     */
    v82Name: string;

    /**
     * Путь по которому располагается ИБ
     */
    path: string;

    /**
     * Занимаемый размер ИБ
     */
    size: number;

    /**
     * Статус ИБ
     */
    status: DatabaseState;

    /**
     *
     */
    isPublishDatabase: boolean;

    /**
     * Web сервис
     */
    isPublishServices: boolean;

    /**
     * Выставляется тип информационной базы Файловая или Серверная
     */
    isFile: boolean;
};

/**
 * Список хранилищ с информацией о размерах и количествах баз в них
 */
export type FileStorageAccountDatabasesSummaryResponseDto = {
    /**
     * Имя хранилища
     */
    fileStorage: string;

    /**
     * Путь хранилища
     */
    pathStorage: string;

    /**
     * Количество ИБ находящихся в этом хранилище
     */
    databaseCount: number;

    /**
     * Место в Mb занимаемое ИБ в хранилище
     */
    totatlSizeOfDatabases: number;
};

/**
 * Модель выбора записей списка информационных баз аккаунта для миграции
 */
export type GetAccountDatabasesForMirgationParamsRequestDto = SelectDataCommonRequestDto<AccountDatabaseMigrationFilterParamsRequestDto>;

/**
 * Модель для миграции информационных баз аккаунта
 */
export type AccountDatabasesMigrationResultResponseDto = {
    /**
     * Список хранилищ с информацией о размерах и количествах баз в них
     */
    fileStorageAccountDatabasesSummary: FileStorageAccountDatabasesSummaryResponseDto[]

    /**
     * Базы аккаунта
     */
    databases: SelectDataMetadataResponseDto<AccountDatabaseMigrationItemResponseDto>

    /**
     * Возможные файловые хранилища для миграции, где Key - ID файлового хранилища, Value - Название файлового хранилища
     */
    avalableFileStorages: KeyValueResponseDto<string, string>[];

    /**
     * Cписка путей доступных хранилищ
     */
    availableStoragePath: string[];

    /**
     * Количество баз разрешенных для миграции
     */
    numberOfDatabasesToMigrate: number;
};