/**
 * Модель ответа при получении суммарных данных по аккаунтам
 */
export type AccountSummaryDataResponseDto = {
    /**
     * Количество зарегестрированных компаний
     */
    registeredCompanyAmount: number;

    /**
     * Количество зарегестрированных компаний в день
     */
    registeredCompanyAmountToday: number;

    /**
     * Последняя регистрация
     */
    lastRegistration: string;

    /**
     * Последняя зарегестрированная компания
     */
    lastRegisteredCompany: string;

    /**
     * Количество пользователей
     */
    usersAmount: number;

    /**
     * Количество баз данных
     */
    databasesAmount: number;

    /**
     * Всего платежей по услугам
     */
    totalPaymentByServices: string;

    /**
     * Всего платежей за месяц
     */
    monthTotalPayment: string;
};