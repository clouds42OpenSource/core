/**
 * Тип ответа на загрузку публичных URL адресов
 */
export type PublicUrlsDto = {
    /**
     *  URL на промо сайт
     */
    PromoSiteUrl: string;
};