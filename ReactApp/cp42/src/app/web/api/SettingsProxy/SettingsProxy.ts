import { PublicUrlsDto } from 'app/web/api/SettingsProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с SuppliersProxy
 */
export class SettingsProxy extends BaseProxy {
    /**
     * Получить публичные URL адреса
     * @param requestKind Вид запроса
     * @returns Публичные URL адреса
     */
    public getPublicUrls(requestKind: RequestKind) {
        return this.requestSender.submitRequest<PublicUrlsDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/settings/GetPublicUrls`,
            this.getObjectArguments(requestKind)
        );
    }
}