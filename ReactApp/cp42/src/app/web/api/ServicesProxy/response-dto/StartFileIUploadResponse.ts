type TFilePartHeader = {
    Authorization: string;
    'x-amz-date': string;
    'x-amz-content-sha256': string;
};

export type TFilePart = {
    url: string;
    headers: TFilePartHeader;
    number: number;

};

export type TStartFileUploadResponse = {
    'storage-type': string;
    id: string;
    'part-size': number;
    parts: TFilePart[];
};