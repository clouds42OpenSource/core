export type ServiceStatusResponseDto = {
    detail?: string;
    error?: string;
    message?: string;
}