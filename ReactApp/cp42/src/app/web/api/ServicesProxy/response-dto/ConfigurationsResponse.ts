export type TConfigurationsResponse = {
    name: string;
    'short-name': string;
    code: string;
    version?: string;
};