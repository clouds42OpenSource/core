import { TConfigurationsResponse } from 'app/web/api/ServicesProxy/response-dto/ConfigurationsResponse';

export type TCommand = {
    name: string;
    presentation: string;
    'repeat-period-in-day': number;
    'days-repeat-period': number;
    'begin-time': string;
    'end-time': string;
    'week-days': Set<number> | Array<number>;
};

export type TFileInformationResponse = {
    type: string;
    name: string;
    synonym: string;
    version: string;
    warning: string;
    'allow-loading': boolean;
    'service-already-exists': boolean;
    'market-service-already-exists': boolean;
    'last-version-configurations': string[];
    'unsafe-mode': boolean;
    isNotSafe: boolean;
    commands: TCommand[];
    fileName?: string | null;
    fileId?: string | null;
    configurations: TConfigurationsResponse[];
    comment: string;
    chosenCommands: TCommand[];
    draft: boolean;
};