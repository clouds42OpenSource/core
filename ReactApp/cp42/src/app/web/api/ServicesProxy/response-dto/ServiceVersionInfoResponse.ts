import { TVersionStatus } from 'app/views/modules/Services/types/globalServicesTypes';
import { TCommand } from 'app/web/api/ServicesProxy/response-dto/FileInformationResponse';

export type TServiceInfoResponse = {
    type: string;
    name: string;
    synonym: string;
    'version-id': string;
    version: string;
    'version-status': TVersionStatus;
    comment: string;
    'configurations-min-versions': { [k: string]: string | null };
    commands: TCommand[];
    'unsafe-mode'?: boolean;
    'audit-result-comment': string;
    'audit-google-sheet': string;
    'publication-date': string;
    'creation-date': string;
    'rework-available': string;
};