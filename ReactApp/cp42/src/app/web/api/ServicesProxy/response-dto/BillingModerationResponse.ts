import { TServiceBillingDataResponse } from 'app/web/api/ServicesProxy/response-dto/ServiceBillingDataResponse';

export type TBillingModerationResponse = {
    Id: string;
    Status: 1 | 3 | 4;
    ChangeRequestType: 1 | 2;
    ModeratorComment: string;
    CurrentServiceData: TServiceBillingDataResponse;
    NewServiceData: TServiceBillingDataResponse;
};