export type TServiceDatabase = {
    AccountDatabaseID: string;
    AccountDatabaseName: string;
    ConfigurationName: string;
    LastActivityDate: string;
    IsCurrent: boolean;
    AccountDatabaseServiceState: 'ВПроцессеУстановки' | 'ОшибкаУстановки' | 'Установлен' | 'НеУстановлен' | 'Удален' | 'ВПроцессеУдаления';
    UrlToRunDatabase: string;
    IsDatabaseReady: boolean;
};

export type TServiceDatabasesResponse = {
    Result: TServiceDatabase[];
};