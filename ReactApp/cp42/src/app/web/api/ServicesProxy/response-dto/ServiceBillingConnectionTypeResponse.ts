export type TServiceBillingConnectionTypeResponse = {
    MyEntUserServiceTypeId: string;
    MyEntUserWebServiceTypeId: string;
};