type ServiceItem = {
    cloud42ServiceKey: string;
    representation: string;
};

export type Cloud42ServiceResponseDto = {
    services: {
        type: string;
        items: ServiceItem[];
    }
};