export type TBillingServiceType = {
    Id: string;
    Key: string;
    DependServiceTypeId: string;
    ServiceTypeId?: string;
    Description: string;
    Name: string;
    BillingType: 0 | 1;
    ServiceTypeCost: number;
    ServiceTypeRelations: string[];
    ConnectionToDatabaseType: 0 | 1 | 2;
    isNew: boolean;
};

export type TServiceBillingDataResponse = {
    Id: string;
    AccountId: string;
    Key: string;
    BillingServiceStatus: 1 | 2 | 3;
    Name: string;
    BillingServiceTypes: TBillingServiceType[];
    InternalCloudService: '1' | '2' | '3' | '4' | '5' | null;
    ReferralLinkForMarket: string;
    LinkToActivateDemoPeriod: string;
    AvailabilityChangeServiceRequestOnModeration: boolean;
    IsExistsChangeServiceRequest: boolean;
    IsActive: boolean;
    MessageStatingThatServiceIsBlocked: string;
    NextPossibleEditServiceCostDate: string;
    CanEditServiceCost: boolean;
    BeforeNextEditServiceCostMonthsCount: number;
    BeforeDeletingServiceTypesMonthsCount: number;
    ServiceOwnerEmail: string;
    IsHybridService: boolean;
};