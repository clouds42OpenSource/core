import { TGetServicesAudience } from 'app/api/endpoints/services/response';

export type TModerationList = {
    'proposal-id': string;
    'service-name': string;
    'service-description': string;
    'service-short-description': string;
    'service-available'?: boolean;
    industries: string[];
    tags: string[];
    'target-audience': TGetServicesAudience[];
    date: string;
    type: 'Редактирование' | 'Создание';
    'service-icon': string;
    'instruction-url': string;
    instruction: string;
    'service-screenshots': string[];
    'proposal-status': string;
    comment: string;
    'service-id'?: string;
    draft: boolean;
};

export type TModerationListPagination = {
    'page-number': number;
    'page-size': number;
    'total-items': number;
    'total-pages': number;
};

export type TModerationListResponse = {
    Result: TModerationList[];
    pagination: TModerationListPagination;
};