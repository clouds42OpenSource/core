export type TAccountServicesResponse = {
    'service-name': string;
    'service-version': string;
    available: boolean;
    'service-id': string;
    draft: boolean;
};