export type TServiceBillingValidationResponse = {
    Success: boolean;
    Message: string;
};