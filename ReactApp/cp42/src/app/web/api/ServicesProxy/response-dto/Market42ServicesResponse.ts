export enum EMarket42ServiceElementState {
    Disabled,
    Enabled,
    Demo,
    DemoDisabled
}

type TMarket42ServiceElement = {
    Key: string;
    Representation: string;
    State: EMarket42ServiceElementState;
};

export type TMarket42ServicesResponse = {
    Table: {
        Type: string;
        Row: TMarket42ServiceElement[];
    };
};