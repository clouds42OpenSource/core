export type TCreateServiceResponse = {
    Result?: {
        'proposal-id': string;
    }
};