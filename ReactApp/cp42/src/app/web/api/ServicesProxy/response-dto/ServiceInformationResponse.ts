import { TGetServicesAudience } from 'app/api/endpoints/services/response';

export type TServiceInformationDraftFilesCommands = {
    Active: boolean;
    Name: string;
    Presentation: string;
    BeginTime?: string;
    DaysRepeatPeriod?: number;
    RepeatPeriodInDay?: number;
    EndTime?: string;
    WeekDays?: number[] | Set<number>;
};

export type TServiceInformationDraftFiles = {
    Comment: string;
    ConfigurationsMinVersions?: Record<string, string>;
    FileId: string;
    Name: string;
    Synonym: string;
    Type: string;
    Version: string;
    UnsafeMode?: boolean;
    Commands?: TServiceInformationDraftFilesCommands[];
};

export type TServiceInformation = {
    ServiceID: string;
    Draft: boolean;
    StringID?: string;
    ServiceName: string;
    ServiceCost?: string;
    ServiceDescription: string;
    ServiceShortDescription: string;
    ServiceIcon: string;
    ServiceIconID: string;
    InstructionURL: string;
    Instruction: string;
    Hybrid: boolean;
    InstructionID: string;
    Industries: string[];
    Tags: string[];
    Video?: string;
    TargetAudience: TGetServicesAudience[] | string[];
    UserBilling?: boolean;
    Private: boolean;
    ServiceScreenshots: string[];
    DraftFiles: TServiceInformationDraftFiles[];
};

export type TServiceInformationResponse = {
    Result: TServiceInformation;
};