export type TChangeModerationStatusRequest = {
    accepted: boolean;
    comment: string;
};