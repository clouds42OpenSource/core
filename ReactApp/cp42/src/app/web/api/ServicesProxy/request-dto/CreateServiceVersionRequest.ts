export type TConfigurationsAndCommands = {
    [k: string]: string;
};

type TCommand = {
    name: string;
    presentation: string;
    'repeat-period-in-day': number;
    'days-repeat-period': number;
    'begin-time': string;
    'end-time': string;
    'week-days': number[];
};

export type TCreateServiceVersionRequest = {
    'account-id': string;
    'user-id': string;
    version: string;
    'full-name': string;
    comment: string;
    'configurations-min-versions': TConfigurationsAndCommands;
    commands: TCommand[];
    'unsafe-mode': boolean | string;
    'file-id': string;
    synonym: string;
    draft: boolean;
};