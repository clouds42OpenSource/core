export type TStartFileUploadRequest = {
    size?: number;
    'part-size': number;
    name: string;
    'account-id': string | null;
    type: string;
};