export type TServiceBillingManagingRequest = {
    Id: string;
    BillingServiceStatus: 1 | 2 | 3;
    InternalCloudService: number | null;
};