import { TBillingServiceType } from 'app/web/api/ServicesProxy/response-dto/ServiceBillingDataResponse';

export type TServiceBillingCreateDataRequest = {
    AccountId: string;
    Key: string;
    Id?: string;
    BillingServiceStatus: 1 | 2 | 3;
    Name: string;
    BillingServiceTypes: TBillingServiceType[];
    CreateServiceRequestId: string;
    IsHybridService: boolean;
};