export type TCreateServiceRequest = {
    name: string;
    'short-description': string;
    opportunities: string;
    icon: string;
    instruction: string;
    'min-cost': number;
    screenshots: string[];
    industries: string[];
    tags: string[];
    'target-audience': string[];
    'account-id': string;
    private: boolean;
    hybrid: boolean;
    'user-billing': boolean;
    draft: boolean;
};