import { ModerationBillingStatusEnum } from 'app/views/modules/Services/enums/ModerationBillingStatusEnum';

export type TBillingModerationRequest = {
    ServiceRequestId: string;
    Status: ModerationBillingStatusEnum;
    ModeratorComment: string;
};