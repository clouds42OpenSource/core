export type TServiceActivityRequest = {
    ServiceId: string;
    IsActive: boolean;
};