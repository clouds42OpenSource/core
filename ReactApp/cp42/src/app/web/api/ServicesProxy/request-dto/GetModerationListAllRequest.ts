export type GetModerationListAllRequest = {
    page: number;
    pageSize?: number;
    fromDate?: string;
    toDate?: string;
    sortFieldName?: string;
    sortType?: number;
    filterProposalType?: string;
    filterProposalStatus?: string;
};