export type TFinishFileUploadRequest = {
    parts: string[];
};