/**
 * Модель запроса на получения списка сервисов Маркет42
 */
export type CheckCurrentPasswordRequestDto = {
    /**
     * ID пользователя
     */
    AccountUserID: string;
};