import { getFetchedData, getRedirected } from 'app/api';
import { tryPayAccessRequestDto } from 'app/api/endpoints/rent/request';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { TErrorType } from 'app/views/modules/Services/types/ErrorTypes';
import { TBillingModerationRequest } from 'app/web/api/ServicesProxy/request-dto/BillingModerationRequest';
import { TChangeModerationStatusRequest } from 'app/web/api/ServicesProxy/request-dto/ChangeModerationStatusRequest';
import { TCreateServiceRequest } from 'app/web/api/ServicesProxy/request-dto/CreateServiceRequest';
import { TCreateServiceVersionRequest } from 'app/web/api/ServicesProxy/request-dto/CreateServiceVersionRequest';
import { TFinishFileUploadRequest } from 'app/web/api/ServicesProxy/request-dto/FinishFileUploadRequest';
import { GetModerationListAllRequest } from 'app/web/api/ServicesProxy/request-dto/GetModerationListAllRequest';
import { TServiceActivityRequest } from 'app/web/api/ServicesProxy/request-dto/ServiceActivityRequest';
import { TServiceBillingCreateDataRequest } from 'app/web/api/ServicesProxy/request-dto/ServiceBillingCreateDataRequest';
import { TServiceBillingManagingRequest } from 'app/web/api/ServicesProxy/request-dto/ServiceBillingManaginRequest';
import { TStartFileUploadRequest } from 'app/web/api/ServicesProxy/request-dto/StartFileUploadRequest';
import {
    ServiceStatusResponseDto,
    TAccountServicesResponse,
    TBillingModerationResponse,
    TConfigurationsResponse,
    TCreateServiceResponse,
    TFileInformationResponse,
    TFilePart,
    TMarket42ServicesResponse,
    TModerationListResponse,
    TServiceBillingConnectionTypeResponse,
    TServiceBillingDataResponse,
    TServiceBillingValidationResponse,
    TServiceDatabasesResponse,
    TServiceIndustriesResponse,
    TServiceInfoResponse,
    TServiceInformationResponse,
    TStartFileUploadResponse
} from 'app/web/api/ServicesProxy/response-dto';
import { TServiceActivityResponse } from 'app/web/api/ServicesProxy/response-dto/ServiceActivityResponse';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { IRequestSender } from 'core/requestSender/interfaces';

export class ServiceProxy extends BaseProxy {
    protected readonly userId: string | null;

    private readonly token: string | null;

    protected readonly accountId: string | null;

    private readonly msHost: string;

    protected constructor(requestSender: IRequestSender, apiHost: string, msHost: string) {
        super(requestSender, apiHost);
        this.msHost = msHost;
        this.userId = localStorageHelper.getUserId();
        this.token = localStorageHelper.getJWTToken();
        this.accountId = localStorageHelper.getContextAccountId();
    }

    /**
     * Запрос на получение списка сервисов из Маркет 42
     * @param requestKind Вид запроса
     */
    protected async getMarket42Services(requestKind: RequestKind) {
        try {
            return await this.requestSender.submitRequest<TMarket42ServicesResponse>(
                WebVerb.GET,
                `${ this.host }/api_v2/Cloud42Service/${ this.accountId }/ServicesList`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение типов подключения для биллинга сервиса
     * @param requestKind Вид запроса
     */
    protected async getBillingConnectionTypes(requestKind: RequestKind) {
        try {
            return await this.requestSender.submitRequest<TServiceBillingConnectionTypeResponse, null>(
                WebVerb.GET,
                `${ this.host }/api_v2/services/service-typesOfconnection`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение информации о биллинге сервиса
     * @param requestKind Вид запроса
     * @param serviceId Идентификатор сервиса в формате UUID
     */
    protected async getBillingServiceData(requestKind: RequestKind, serviceId: string) {
        const { responseBody: { data } } = await getFetchedData<TServiceBillingDataResponse, tryPayAccessRequestDto>({
            url: `${ this.host }/api_v2/services/${ serviceId }/service-data`,
            method: WebVerb.GET,
            hasStandardResponseType: false
        });

        return data;
    }

    /**
     * Запрос на получение информации о биллинге сервиса
     * @param requestKind Вид запроса
     * @param data Тело запроса
     */
    protected async createServiceBilling(requestKind: RequestKind, data: TServiceBillingCreateDataRequest) {
        try {
            return await this.requestSender.submitRequest<{} | null, TServiceBillingCreateDataRequest>(
                WebVerb.POST,
                `${ this.host }/api_v2/services/service-create`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на редактирование информации о биллинге сервиса
     * @param requestKind Вид запроса
     * @param data Тело запроса
     */
    protected async updateServiceBilling(requestKind: RequestKind, data: TServiceBillingCreateDataRequest) {
        try {
            return await this.requestSender.submitRequest<TErrorType | null, TServiceBillingCreateDataRequest>(
                WebVerb.POST,
                `${ this.host }/api_v2/services/service-edit`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на редактирование черновика биллинга сервиса
     * @param requestKind Вид запроса
     * @param data Тело запроса
     */
    protected async updateServiceBillingDraft(requestKind: RequestKind, data: TServiceBillingCreateDataRequest) {
        try {
            return await this.requestSender.submitRequest<{} | null, TServiceBillingCreateDataRequest>(
                WebVerb.POST,
                `${ this.host }/api_v2/services/service-editDraft`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Эндпоинт для вкладки "Управление" в карточке сервиса, доступен только CloudAdmin
     * @param requestKind Вид запроса
     * @param data Необходимые данные
     */
    protected async updateServiceBillingManaging(requestKind: RequestKind, data: TServiceBillingManagingRequest) {
        try {
            return await this.requestSender.submitRequest<{} | null, TServiceBillingManagingRequest>(
                WebVerb.POST,
                `${ this.host }/api_v2/services/manage-edit`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Установить результат заявки на модерацию для биллинга
     * @param requestKind Вид запроса
     * @param data Необходимые данные
     */
    protected async billingModerationResult(requestKind: RequestKind, data: TBillingModerationRequest) {
        try {
            return await this.requestSender.submitRequest<{} | null, TBillingModerationRequest>(
                WebVerb.POST,
                `${ this.host }/api_v2/ServiceRequest/ModerationResult`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Получить заявку на модерацию биллинга
     * @param requestKind Вид запроса
     * @param requestId ID заявки
     */
    protected async getBillingModeration(requestKind: RequestKind, requestId: string) {
        try {
            return await this.requestSender.submitRequest<TBillingModerationResponse | null, null>(
                WebVerb.GET,
                `${ this.host }/api_v2/ServiceRequest/${ requestId }`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Смена активности сервиса
     * @param requestKind Вид запроса
     * @param data Данные для смены активности
     */
    protected async setServiceActivity(requestKind: RequestKind, data: TServiceActivityRequest) {
        try {
            return await this.requestSender.submitRequest<TServiceActivityResponse | null, TServiceActivityRequest>(
                WebVerb.POST,
                `${ this.host }/api_v2/services/service-activity`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Валидация данных о биллинге
     * @param requestKind Вид запроса
     * @param data Данные
     * @param isCreate Флаг создания/редактирования сервиса
     */
    protected async getBillingInfoValidate(requestKind: RequestKind, data: TServiceBillingCreateDataRequest, isCreate: boolean) {
        const urlPart = `${ isCreate ? 'validate-service' : 'validate-edit-service' }`;

        try {
            return await this.requestSender.submitRequest<TServiceBillingValidationResponse | null, TServiceBillingCreateDataRequest>(
                WebVerb.POST,
                `${ this.host }/api_v2/services/${ urlPart }`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение списка личных сервисов
     * @param requestKind Вид запроса
     */
    protected async getAccountServices(requestKind: RequestKind) {
        try {
            return await this.requestSender.submitRequest<TAccountServicesResponse[], null>(
                WebVerb.GET,
                `${ this.msHost }/pl42/hs/Platform/accounts/${ this.accountId }/services`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение списка совместимых конфигураций
     * @param requestKind Вид запроса
     */
    protected async getConfigsFromMs(requestKind: RequestKind) {
        try {
            return await this.requestSender.submitRequest<TConfigurationsResponse[], null>(
                WebVerb.GET,
                `${ this.msHost }/pl42/hs/Platform/configurations`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение списка доступных для сервиса баз
     * @param id Идентификатор сервиса
     */
    protected async getServiceDatabases(id: string) {
        const { responseBody, status } = await getFetchedData<TServiceDatabasesResponse, tryPayAccessRequestDto>({
            url: `${ this.msHost }/ms-market/hs/market/services/${ id }/databases`,
            method: WebVerb.GET,
            hasStandardResponseType: false,
            headers: {
                AccountID: this.accountId ?? ''
            }
        });
        if (status === 403) {
            getRedirected();
        } else {
            return responseBody.data;
        }
    }

    /**
     * Запрос на получение информации о загруженном файле
     * @param requestKind Вид запроса
     * @param fileId Идентификатор загруженного файла
     * @param isService Флаг для обозначения файла разработки
     * @param serviceId id сервиса при новой версии
     */
    protected async getFileInfoFromMs(requestKind: RequestKind, fileId: string, isService: boolean, serviceId?: string) {
        try {
            return await this.requestSender.submitRequest<TFileInformationResponse, null>(
                WebVerb.GET,
                `${ this.msHost }/pl42/hs/Platform/files/${ fileId }?${ isService ? 'type=service&' : null }${ serviceId ? `serviceId=${ serviceId }` : '' }`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос для обозначения начала загрузки файла по чанкам
     * @param requestKind Вид запроса
     * @param request Поля с данными о загружаемом файле
     */
    protected async startFileUploading(requestKind: RequestKind, request: TStartFileUploadRequest) {
        try {
            return await this.requestSender.submitRequest<TStartFileUploadResponse, TStartFileUploadRequest>(
                WebVerb.POST,
                `${ this.msHost }/pl42/hs/Platform/files/`,
                this.getObjectArguments(requestKind, request, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на отправление чанка файла
     * @param requestKind Вид запроса
     * @param dataChunk Чанк загружаемого файла
     * @param data Поля с данными содержащими параметры запроса, его URL и заголовки
     */
    protected async fileChunkUploading(requestKind: RequestKind, dataChunk: FormDataEntryValue | null, data: TFilePart) {
        try {
            const response = await fetch(data.url, { method: WebVerb.PUT, headers: data.headers, body: dataChunk });
            return response.headers.get('Etag');
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос для обозначения окончания загрузки файла по чанкам
     * @param requestKind Вид запроса
     * @param eTagArray Массив, элементы которого добавляются в процессе загрузки чанков из заголовка ответов
     * @see {@link fileChunkUploading}
     * @param fileId Идентификатор загруженого файла
     */
    protected async finishFileUploading(requestKind: RequestKind, eTagArray: TFinishFileUploadRequest, fileId: string) {
        try {
            await this.requestSender.submitRequest<{}, TFinishFileUploadRequest>(
                WebVerb.POST,
                `${ this.msHost }/pl42/hs/Platform/files/${ fileId }`,
                this.getObjectArguments(requestKind, eTagArray, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );

            return fileId;
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос для создания нового сервиса
     * @param requestKind Вид запроса
     * @param data Необходимые данные для создания сервиса
     * @param serviceId Идентификатор создаваемого сервиса формата UUID
     */
    protected async createService(requestKind: RequestKind, data: TCreateServiceRequest, serviceId: string) {
        try {
            return await this.requestSender.submitRequest<TCreateServiceResponse, TCreateServiceRequest>(
                WebVerb.PUT,
                `${ this.msHost }/pl42/hs/Platform/services/${ serviceId }`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос для создания файла разработки сервиса
     * @param requestKind Вид запроса
     * @param data Необходимые данные для создания файла разработки сервиса
     * @param serviceId Идентификатор созданного сервиса формата UUID
     * @param versionId Идентификатор создаваемого файла разработки сервиса формата UUID
     */
    protected async createServiceVersion(requestKind: RequestKind, data: TCreateServiceVersionRequest, serviceId: string, versionId: string) {
        try {
            return await this.requestSender.submitRequest<{}, TCreateServiceVersionRequest>(
                WebVerb.PUT,
                `${ this.msHost }/pl42/hs/Platform/services/${ serviceId }/versions/${ versionId }`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос для смены статуса файла разработки сервиса
     * @param requestKind Вид запроса
     * @param method POST - отправить на доработку, DELETE - отозвать версию
     * @param serviceId Идентификатор созданного сервиса формата UUID
     * @param versionId Идентификатор файла разработки формата UUID
     */
    protected async serviceVersionRequest(requestKind: RequestKind, method: WebVerb.POST | WebVerb.DELETE, serviceId: string, versionId: string) {
        try {
            return await this.requestSender.submitRequest<{}, null>(
                method,
                `${ this.msHost }/pl42/hs/Platform/services/${ serviceId }/versions/${ versionId }`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение списка сервисов
     * @param requestKind Вид запроса
     * @param serviceId Идентификатор созданного сервиса формата UUID
     */
    protected async getService(requestKind: RequestKind, serviceId: string) {
        try {
            return await this.requestSender.submitRequest<TServiceInformationResponse, null>(
                WebVerb.GET,
                `${ this.msHost }/pl42/hs/Platform/services/${ serviceId }`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение списка файлов разработки сервиса
     * @param requestKind Вид запроса
     * @param serviceId Идентификатор созданного сервиса формата UUID
     */
    protected async getServiceVersion(requestKind: RequestKind, serviceId: string) {
        try {
            return await this.requestSender.submitRequest<TServiceInfoResponse[], null>(
                WebVerb.GET,
                `${ this.msHost }/pl42/hs/Platform/services/${ serviceId }/versions/`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение списка отраслей для сервиса
     * @param requestKind Вид запроса
     */
    protected async getIndustries(requestKind: RequestKind) {
        try {
            return await this.requestSender.submitRequest<TServiceIndustriesResponse[], null>(
                WebVerb.GET,
                `${ this.msHost }/pl42/hs/Platform/services/categories`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос для смены статуса базы в сервисе
     * @param requestKind Вид запроса
     * @param method POST - установка базы, DELETE - удаление
     * @param applicationId Идентификатор созданного сервиса формата UUID
     * @param id Идентификатор базы
     */
    protected async serviceStatusRequest(requestKind: RequestKind, method: WebVerb.PUT | WebVerb.DELETE, applicationId: string, id: string) {
        try {
            return await this.requestSender.submitRequest<{}, null>(
                method,
                `${ this.msHost }/pl42/hs/Platform/applications/${ applicationId }/services/${ id }`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, { Authorization: `Bearer ${ this.token }` })
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Запрос на получение информации о сервисе
     * @param requestKind Вид запроса
     * @param method GET - получить информацию, DELETE - удалить сервис
     * @param id Идентификатор созданного сервиса формата UUID
     */
    protected async serviceInfoRequest(requestKind: RequestKind, method: WebVerb.GET | WebVerb.DELETE, id: string) {
        try {
            return await this.requestSender.submitRequest<TServiceInformationResponse, null>(
                method,
                `${ this.msHost }/pl42/hs/Platform/services/${ id }/`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, {
                    Authorization: `Bearer ${ this.token }`
                }),
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Получить список заявок на модерацию по сервису от МС
     * @param requestKind Вид запроса
     * @param serviceId ID сервиса
     */
    protected async getModerationList(requestKind: RequestKind, serviceId: string) {
        try {
            return await this.requestSender.submitRequest<TModerationListResponse, null>(
                WebVerb.GET,
                `${ this.msHost }/pl42/hs/Platform/services/${ serviceId }/proposals`,
                this.getObjectArguments(requestKind, null, this.defaultContentType, {
                    Authorization: `Bearer ${ this.token }`
                }),
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Получить список заявок на модерацию по всем сервисам от МС
     * @param requestKind Вид запроса
     * @param query Query параметры фильтрации
     */
    protected async getModerationListAll(requestKind: RequestKind, query: GetModerationListAllRequest) {
        try {
            return await this.requestSender.submitRequest<TModerationListResponse, GetModerationListAllRequest>(
                WebVerb.GET,
                `${ this.msHost }/pl42/hs/Platform/service-proposals`,
                this.getObjectArguments(requestKind, query, this.defaultContentType, {
                    Authorization: `Bearer ${ this.token }`
                }),
            );
        } catch (err: unknown) {
            return null;
        }
    }

    /**
     * Установить результат заявки на модерацию для МС
     * @param requestKind Вид запроса
     * @param proposalId ID заявки
     * @param data Необходимые данные
     */
    protected async changeModerationStatus(requestKind: RequestKind, proposalId: string, data: TChangeModerationStatusRequest) {
        try {
            return await this.requestSender.submitRequest<ServiceStatusResponseDto | null, TChangeModerationStatusRequest>(
                WebVerb.POST,
                `${ this.msHost }/pl42/hs/Platform/service-proposals/${ proposalId }`,
                this.getObjectArguments(requestKind, data, this.defaultContentType, { Authorization: `Bearer ${ this.token }` }),
            );
        } catch (err: unknown) {
            return null;
        }
    }
}