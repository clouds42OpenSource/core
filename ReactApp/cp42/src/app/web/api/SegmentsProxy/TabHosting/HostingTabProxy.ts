import { TResponseLowerCase } from 'app/api/types';
import { CoreHostingDataModel, CoreHostingFilterParams, CreateCoreHostingParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Хостинг"
 */
export class HostingTabProxy extends BaseProxy {
    /**
     * Получить данные списка хостинг облака
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных хостинга облака
     */
    public getCoreHostings(requestKind: RequestKind, args: CoreHostingFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<CoreHostingDataModel>, CoreHostingFilterParams>(
            WebVerb.GET,
            `${ this.host }/core-hosting`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить хостинг облака
     * @param requestKind Вид запроса
     * @param coreHostingId ID хостинга облака
     */
    public deleteCoreHosting(requestKind: RequestKind, coreHostingId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/core-hosting/${ coreHostingId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить хостинг облака
     * @param requestKind Вид запроса
     * @param coreHostingId ID хостинга облака
     * @returns Модель хостинга облака
     */
    public getCoreHosting(requestKind: RequestKind, coreHostingId: string) {
        return this.requestSender.submitRequest<CoreHostingDataModel>(
            WebVerb.GET,
            `${ this.host }/core-hosting/${ coreHostingId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новый хостинг облака
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createCoreHosting(requestKind: RequestKind, args: CreateCoreHostingParams) {
        return this.requestSender.submitRequest<void, CreateCoreHostingParams>(
            WebVerb.POST,
            `${ this.host }/core-hosting`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать хостинг облака
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editCoreHosting(requestKind: RequestKind, args: CoreHostingDataModel) {
        return this.requestSender.submitRequest<void, CoreHostingDataModel>(
            WebVerb.PUT,
            `${ this.host }/core-hosting`,
            this.getObjectArguments(requestKind, args)
        );
    }
}