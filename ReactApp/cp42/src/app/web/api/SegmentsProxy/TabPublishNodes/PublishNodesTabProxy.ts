import { TResponseLowerCase } from 'app/api/types';
import { PublishNodeCreateResponseDto, PublishNodeResponseDto } from 'app/web/api/SegmentsProxy/common';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { PublishNodeFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPublishNodesApiProxy';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Ноды публикаций"
 */
export class PublishNodesTabProxy extends BaseProxy {
    /**
     * Получить данные по нодам публикаций
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по нодам публикаций
     */
    public getPublishNodes(requestKind: RequestKind, args: PublishNodeFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<PublishNodeResponseDto>, PublishNodeFilterParams>(
            WebVerb.GET,
            `${ this.host }/publish-nodes`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить ноду публикаций
     * @param requestKind Вид запроса
     * @param publishNodeId ID ноды публикаций
     */
    public deletePublishNode(requestKind: RequestKind, publishNodeId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/publish-nodes/${ publishNodeId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить ноду публикаций
     * @param requestKind Вид запроса
     * @param publishNodeId ID ноды публикаций
     * @returns Модель нода публикаций
     */
    public getPublishNode(requestKind: RequestKind, publishNodeId: string) {
        return this.requestSender.submitRequest<PublishNodeResponseDto>(
            WebVerb.GET,
            `${ this.host }/publish-nodes/${ publishNodeId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новую ноду публикаций
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createPublishNode(requestKind: RequestKind, args: PublishNodeCreateResponseDto) {
        return this.requestSender.submitRequest<void, PublishNodeCreateResponseDto>(
            WebVerb.POST,
            `${ this.host }/publish-nodes`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать ноду публикаций
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editPublishNode(requestKind: RequestKind, args: PublishNodeResponseDto) {
        return this.requestSender.submitRequest<void, PublishNodeResponseDto>(
            WebVerb.PUT,
            `${ this.host }/publish-nodes`,
            this.getObjectArguments(requestKind, args)
        );
    }
}