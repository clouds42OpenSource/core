export enum EAutoUpdateNodeType {
  Common,
  Vip,
  Htznr,
}

export type ArrServerDataModel = {
    id: string;
    nodeAddress: string;
    manualUpdateObnovlyatorPath: string;
    autoUpdateObnovlyatorPath: string;
    dontRunInGlobalAU: boolean;
    autoUpdateNodeType: EAutoUpdateNodeType;
    isBusy: boolean;
};