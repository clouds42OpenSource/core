export type ArrServerCreateDataModel = {
  nodeAddress: string;
  manualUpdateObnovlyatorPath: string;
  autoUpdateObnovlyatorPath: string;
}