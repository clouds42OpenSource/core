import { TResponseLowerCase } from 'app/api/types';
import { ArrServerCreateDataModel, ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { GetTerminalFarmsRequestDto } from 'app/web/api/SegmentsProxy/common';
import { BaseProxy, SelectDataMetadataResponseDto } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Сервера ARR"
 */
export class ArrServersTabProxy extends BaseProxy {
    /**
     * Получить данные по серверам ARR
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по серверам ARR
     */
    public getArrServers(requestKind: RequestKind, args: GetTerminalFarmsRequestDto) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<ArrServerDataModel>, GetTerminalFarmsRequestDto>(
            WebVerb.GET,
            `${ this.host }/auto-update-nodes`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить сервер ARR
     * @param requestKind Вид запроса
     * @param arrServerId ID сервера ARR
     */
    public deleteArrServer(requestKind: RequestKind, arrServerId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/auto-update-nodes?id=${ arrServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить сервер ARR
     * @param requestKind Вид запроса
     * @param arrServerId ID сервера ARR
     * @returns Модель сервера ARR
     */
    public getArrServer(requestKind: RequestKind, arrServerId: string) {
        return this.requestSender.submitRequest<ArrServerDataModel>(
            WebVerb.GET,
            `${ this.host }/auto-update-nodes/${ arrServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новый сервер ARR
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createArrServer(requestKind: RequestKind, args: ArrServerCreateDataModel) {
        return this.requestSender.submitRequest<void, ArrServerCreateDataModel>(
            WebVerb.POST,
            `${ this.host }/auto-update-nodes`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать сервер ARR
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editArrServer(requestKind: RequestKind, args: ArrServerDataModel) {
        return this.requestSender.submitRequest<void, ArrServerDataModel>(
            WebVerb.PUT,
            `${ this.host }/auto-update-nodes`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Проверить доступность сервера ARR
     * @param requestKind Вид запроса
     * @param address Адрес
     * @returns Флаг на доступность сервера ARR
     */
    public checkArrServerAvailability(requestKind: RequestKind, address: string) {
        return this.requestSender.submitRequest<boolean>(
            WebVerb.GET,
            `${ this.host }/ArrServer/CheckArrServerAvailability`,
            this.getObjectArguments(requestKind, { address })
        );
    }
}