import { TResponseLowerCase } from 'app/api/types';
import { CommonCreateSegmentParams, CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { CommonSegmentResponseDto, GetTerminalFarmsRequestDto } from 'app/web/api/SegmentsProxy/common';
import { BaseProxy, SelectDataResultMetadataModel } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Фермы ТС"
 */
export class TerminalFarmsTabProxy extends BaseProxy {
    /**
     * Получить данные по фермам ТС
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных с пагинацией по фермам ТС
     */
    public getTerminalFarms(requestKind: RequestKind, args: GetTerminalFarmsRequestDto) {
        return this.requestSender.submitRequest<SelectDataResultMetadataModel<CommonSegmentResponseDto>, GetTerminalFarmsRequestDto>(
            WebVerb.GET,
            `${ this.host }/terminal-farms`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Создать терминальную ферму тс
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createTerminalFarm(requestKind: RequestKind, args: CommonCreateSegmentParams) {
        return this.requestSender.submitRequest<void, CommonCreateSegmentParams>(
            WebVerb.POST,
            `${ this.host }/terminal-farms`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить терминальную ферму тс по ID
     * @param requestKind Вид запроса
     * @param terminalFarmId ID терминальной фермы
     */
    public deleteTerminalFarm(requestKind: RequestKind, terminalFarmId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/terminal-farms/${ terminalFarmId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить терминальную ферму тс по ID
     * @param requestKind Вид запроса
     * @param terminalFarmId ID терминальной фермы
     */
    public getTerminalFarm(requestKind: RequestKind, terminalFarmId: string) {
        return this.requestSender.submitRequest<CommonSegmentResponseDto>(
            WebVerb.GET,
            `${ this.host }/terminal-farms/${ terminalFarmId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Редактировать терминальную ферму тс
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editTerminalFarm(requestKind: RequestKind, args: CommonSegmentDataModel) {
        return this.requestSender.submitRequest<void, CommonSegmentDataModel>(
            WebVerb.PUT,
            `${ this.host }/terminal-farms`,
            this.getObjectArguments(requestKind, args)
        );
    }
}