import { CreateCloudFileStorageServerResponseDto } from 'app/web/api/SegmentsProxy/TabFileStorage/response-dto';

/**
 * Модель файлового хранилища
 */
export type CloudFileStorageServerResponseDto = CreateCloudFileStorageServerResponseDto & {
    /**
     * Id записи
     */
    Id: string;
}