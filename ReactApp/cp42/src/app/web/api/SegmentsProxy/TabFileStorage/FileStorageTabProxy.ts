import { TResponseLowerCase } from 'app/api/types';
import { CloudFileStorageServerDataModel, CreateCloudFileStorageServerDataModel, FileStorageFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Файл хранилища"
 */
export class FileStorageTabProxy extends BaseProxy {
    /**
     * Получить данные файловых хранилищ
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по файловым хранилищам
     */
    public getCloudFileStorageServers(requestKind: RequestKind, args: FileStorageFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<CloudFileStorageServerDataModel>, FileStorageFilterParams>(
            WebVerb.GET,
            `${ this.host }/file-storage-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить файл хранилища
     * @param requestKind Вид запроса
     * @param fileStorageId ID файла хранилища
     */
    public deleteCloudFileStorageServer(requestKind: RequestKind, fileStorageId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/file-storage-servers/${ fileStorageId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить файл хранилища
     * @param requestKind Вид запроса
     * @param fileStorageId ID файла хранилища
     * @returns Модель файла хранилища
     */
    public getCloudFileStorageServer(requestKind: RequestKind, fileStorageId: string) {
        return this.requestSender.submitRequest<CloudFileStorageServerDataModel>(
            WebVerb.GET,
            `${ this.host }/file-storage-servers/${ fileStorageId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новый файл хранилища
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createCloudFileStorageServer(requestKind: RequestKind, args: CreateCloudFileStorageServerDataModel) {
        return this.requestSender.submitRequest<void, CreateCloudFileStorageServerDataModel>(
            WebVerb.POST,
            `${ this.host }/file-storage-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать файл хранилища
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editCloudFileStorageServer(requestKind: RequestKind, args: CloudFileStorageServerDataModel) {
        return this.requestSender.submitRequest<void, CloudFileStorageServerDataModel>(
            WebVerb.PUT,
            `${ this.host }/file-storage-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }
}