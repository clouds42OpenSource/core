import { SearchBaseRequestDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Модель создания файлового хранилища
 */
export type CreateCloudFileStorageServerResponseDto = SearchBaseRequestDto & {
    /**
     * DNS имя хранилища
     */
    DnsName: string;
}