import { FilterBaseRequestDto, SearchBaseRequestDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Модель фильтра файлового хранилища
 */
export type FileStorageFilterRequestDto = SearchBaseRequestDto & FilterBaseRequestDto;