import { PublishNodeResponseDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Нода публикации фермы
 */
export type ServerFarmPublishNodeResponseDto = PublishNodeResponseDto & {
    /**
     * Флаг что нода доступна для выбора
     */
    IsAvailable: boolean;
}