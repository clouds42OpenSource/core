import { FilterBaseRequestDto, SearchBaseRequestDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Модель фильтра фермы серверов
 */
export type ServerFarmFilterRequestDto = FilterBaseRequestDto & SearchBaseRequestDto;