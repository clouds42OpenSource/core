import { ServerFarmPublishNodeResponseDto, ServerFarmResponseDto } from 'app/web/api/SegmentsProxy/TabServerFarms/response-dto';

/**
 * Модель параметров для создания/редактирования фермы серверов
 */
export type ServerFarmRequestDto = ServerFarmResponseDto & {
    /**
     * Выбранные ноды публикации
     */
    Servers: Array<ServerFarmPublishNodeResponseDto>;
}