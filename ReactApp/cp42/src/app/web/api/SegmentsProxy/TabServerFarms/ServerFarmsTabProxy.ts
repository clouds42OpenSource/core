import { TResponseLowerCase } from 'app/api/types';
import { PublishNodeResponseDto } from 'app/web/api/SegmentsProxy/common';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { ServerFarmDataModel, ServerFarmFilterParams, ServerFarmModel, ServerFarmParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Ферма серверов"
 */
export class ServerFarmsTabProxy extends BaseProxy {
    /**
     * Получить данные ферм сервера
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных ферм сервера
     */
    public getServerFarms(requestKind: RequestKind, args: ServerFarmFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<ServerFarmDataModel>, ServerFarmFilterParams>(
            WebVerb.GET,
            `${ this.host }/server-farms`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить ферму сервера
     * @param requestKind Вид запроса
     * @param serverFarmId ID фермы сервера
     */
    public deleteServerFarm(requestKind: RequestKind, serverFarmId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/server-farms/${ serverFarmId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить ферму сервера
     * @param requestKind Вид запроса
     * @param serverFarmId ID фермы сервера
     * @returns Модель фермы сервера
     */
    public getServerFarm(requestKind: RequestKind, serverFarmId: string) {
        return this.requestSender.submitRequest<ServerFarmModel>(
            WebVerb.GET,
            `${ this.host }/server-farms/${ serverFarmId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новую ферму сервера
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createServerFarm(requestKind: RequestKind, args: ServerFarmParams) {
        return this.requestSender.submitRequest<void, ServerFarmParams>(
            WebVerb.POST,
            `${ this.host }/server-farms`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать ферму сервера
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editServerFarm(requestKind: RequestKind, args: ServerFarmParams) {
        return this.requestSender.submitRequest<void, ServerFarmParams>(
            WebVerb.PUT,
            `${ this.host }/server-farms`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить все ноды публикации
     * @param requestKind Вид запроса
     * @returns Список нод публикации
     */
    public getAllPublishNodes(requestKind: RequestKind) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<PublishNodeResponseDto>>(
            WebVerb.GET,
            `${ this.host }/publish-nodes`,
            this.getObjectArguments(requestKind, { pageCount: 1000 })
        );
    }
}