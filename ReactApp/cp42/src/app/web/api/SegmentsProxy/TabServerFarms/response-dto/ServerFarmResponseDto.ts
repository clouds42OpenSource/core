import { ServerFarmPublishNodeResponseDto } from 'app/web/api/SegmentsProxy/TabServerFarms/response-dto';

/**
 * Модель фермы серверов
 */
export type ServerFarmResponseDto = {
    /**
     * Id фермы серверов
     */
    Id: string;
    /**
     * Название фермы
     */
    Name: string;
    /**
     * Описание
     */
    Description: string;
    /**
     * Доступные сервера для выбора
     */
    AvailableServers?: Array<ServerFarmPublishNodeResponseDto>;
}