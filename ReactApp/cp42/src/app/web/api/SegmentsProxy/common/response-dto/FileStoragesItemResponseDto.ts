export type FileStoragesItemResponseDto = {
    canDeleteFromAvailable: boolean;
    databasesCount: number;
    isDefault: boolean;
    key: string;
    value: string;
}