import { SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { FilterBaseParams } from 'app/web/common/params';

export type GetTerminalFarmsRequestDto = {
    filter: SearchBaseParams;
} & FilterBaseParams;