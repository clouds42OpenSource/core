/**
 * Модель элемента сегмента
 */
export type SegmentElementResponseDto = {
    /**
     * Id элемента
     */
    key: string;
    /**
     * Название элемента
     */
    value: string;
}