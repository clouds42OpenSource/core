/**
 * Нода публикаций
 */
export type PublishNodeResponseDto = {
    /**
     * Идентификатор ноды
     */
    id: string;
    /**
     * Описание
     */
    description: string;
    /**
     * Адрес подключения
     */
    address: string;
}