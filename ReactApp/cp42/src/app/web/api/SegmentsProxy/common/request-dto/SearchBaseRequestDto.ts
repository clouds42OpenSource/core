/**
 * Базовый модель поисковика
 */
export type SearchBaseRequestDto = {
    /**
     * Адрес подключения
     */
    ConnectionAddress?: string;
    /**
     * Название
     */
    Name?: string;
    /**
     * Описание
     */
    Description?: string;
}