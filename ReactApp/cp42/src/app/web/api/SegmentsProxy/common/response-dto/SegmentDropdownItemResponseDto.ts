/**
 * Модель элемента списка
 */
export type SegmentDropdownItemResponseDto = {
    key: string;
    value: string;
}