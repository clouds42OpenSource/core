export type PublishNodeCreateResponseDto = {
    address: string;
    description: string;
}