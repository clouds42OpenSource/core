/**
 * Общий модель сегмента
 */
export type CommonSegmentResponseDto = {
    /**
     * ID сегмента
     */
    name: string;
    description: string;
    connectionAddress: string;
    id: string;
    usingOutdatedWindows: boolean;
}