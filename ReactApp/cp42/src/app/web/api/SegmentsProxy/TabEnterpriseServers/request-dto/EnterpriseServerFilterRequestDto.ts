import { SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { FilterBaseParams } from 'app/web/common/params';

/**
 * Фильтр поиска серверов 1С:Предприятие
 */
export type EnterpriseServerFilterRequestDto = FilterBaseParams & SearchBaseParams & {
    /**
     * Версия платформы
     */
    version: string;
}