import { TResponseLowerCase } from 'app/api/types';
import uuid from 'app/common/helpers/GenerateUuidHelper/uuid';
import { EnterpriseServerFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { EnterpriseServerResponseDto } from 'app/web/api/SegmentsProxy/TabEnterpriseServers';
import { EnterpriseServerCreateResponseDto } from 'app/web/api/SegmentsProxy/TabEnterpriseServers/response-dto/EnterpriseServerCreateResponseDto';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Сервера 1С"
 */
export class EnterpriseServersTabProxy extends BaseProxy {
    /**
     * Получить данные по серверам 1С:Предприятие
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по серверам 1С:Предприятие
     */
    public getEnterpriseServers(requestKind: RequestKind, args: EnterpriseServerFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<EnterpriseServerResponseDto>, EnterpriseServerFilterParams>(
            WebVerb.GET,
            `${ this.host }/enterprise-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Создать новый сервер 1С:Предприятие
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createEnterpriseServer(requestKind: RequestKind, args: EnterpriseServerCreateResponseDto) {
        return this.requestSender.submitRequest<void, EnterpriseServerCreateResponseDto>(
            WebVerb.POST,
            `${ this.host }/enterprise-servers`,
            this.getObjectArguments(requestKind, { ...args, id: uuid() })
        );
    }

    /**
     * Получить сервер 1С:Предприятие по ID
     * @param requestKind Вид запроса
     * @param enterpriseServerId ID сервера 1С:Предприятие
     * @returns Модель сервера 1С:Предприятие
     */
    public getEnterpriseServer(requestKind: RequestKind, enterpriseServerId: string) {
        return this.requestSender.submitRequest<EnterpriseServerResponseDto>(
            WebVerb.GET,
            `${ this.host }/enterprise-servers/${ enterpriseServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Удалить сервер 1С:Предприятие по ID
     * @param requestKind Вид запроса
     * @param enterpriseServerId ID сервера 1С:Предприятие
     * @returns Модель сервера 1С:Предприятие
     */
    public deleteEnterpriseServer(requestKind: RequestKind, enterpriseServerId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/enterprise-servers/${ enterpriseServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Редактировать новый сервер 1С:Предприятие
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editEnterpriseServer(requestKind: RequestKind, args: EnterpriseServerResponseDto) {
        return this.requestSender.submitRequest<void, EnterpriseServerResponseDto>(
            WebVerb.PUT,
            `${ this.host }/enterprise-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }
}