import { PlatformEnumType } from 'app/common/enums';

/**
 * Сервер 1С:Предприятие
 */
export type EnterpriseServerResponseDto = {
    /**
     * Идентификатор сервера
     */
    id: string;
    /**
     * Адрес подключения
     */
    connectionAddress: string;
    /**
     * Название
     */
    name: string;
    /**
     * Описание
     */
    description: string;
    clusterSettingsPath: string;
    /**
     * Версия платформы
     */
    versionEnum: PlatformEnumType;
    /**
     * Версия платформы
     */
    version: string;
    /**
     * Логин администратора
     */
    adminName: string;
    /**
     * Пароль администратора
     */
    adminPassword: string;
}