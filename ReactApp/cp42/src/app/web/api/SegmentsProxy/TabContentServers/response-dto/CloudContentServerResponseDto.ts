import { NodesDataResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto/NodesDataResponseDto';

/**
 * Модель сервера публикации
 */
export type CloudContentServerResponseDto = {
    id: string;
    description: string;
    groupByAccount: boolean;
    name: string;
    publishSiteName: string;
    nodesData: NodesDataResponseDto;
}