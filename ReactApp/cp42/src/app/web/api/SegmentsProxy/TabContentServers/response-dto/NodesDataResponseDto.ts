import { PublishNodeResponseDto } from 'app/web/api/SegmentsProxy/common';

export type NodesDataResponseDto = {
    available: PublishNodeResponseDto[];
    selected: PublishNodeResponseDto[];
}