import { TResponseLowerCase } from 'app/api/types';
import { CloudContentServerResponseDto, NodesDataResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { CloudContentServerDataModel, CloudContentServerFilterParams, CreateCloudContentServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabContentServersApiProxy';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Сайты публикаций"
 */
export class ContentServersTabProxy extends BaseProxy {
    /**
     * Получить данные серверов публикации
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных серверов публикации
     */
    public getCloudContentServers(requestKind: RequestKind, args: CloudContentServerFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<CloudContentServerResponseDto>, CloudContentServerFilterParams>(
            WebVerb.GET,
            `${ this.host }/content-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить сервер публикации
     * @param requestKind Вид запроса
     * @param contentServerId ID сервера публикации
     */
    public deleteCloudContentServer(requestKind: RequestKind, contentServerId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/content-servers/${ contentServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить модели доступных нод публикации
     * @param requestKind Вид запроса
     * @returns Массив нода публикаций
     */
    public getAvailablePublishNodes(requestKind: RequestKind) {
        return this.requestSender.submitRequest<NodesDataResponseDto>(
            WebVerb.GET,
            `${ this.host }/publish-nodes/available`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить модель сервера публикации
     * @param requestKind Вид запроса
     * @param contentServerId ID сервера публикации
     * @returns Модель сервера публикации
     */
    public getCloudContentServer(requestKind: RequestKind, contentServerId: string) {
        return this.requestSender.submitRequest<CloudContentServerResponseDto>(
            WebVerb.GET,
            `${ this.host }/content-servers/${ contentServerId }`,
            this.getObjectArguments(requestKind, { contentServerId })
        );
    }

    /**
     * Создать сервер публикации
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createCloudContentServer(requestKind: RequestKind, args: CreateCloudContentServerParams) {
        return this.requestSender.submitRequest<void, CreateCloudContentServerParams>(
            WebVerb.POST,
            `${ this.host }/content-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать сервер публикации
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editCloudContentServer(requestKind: RequestKind, args: CloudContentServerDataModel) {
        return this.requestSender.submitRequest<void, CloudContentServerDataModel>(
            WebVerb.PUT,
            `${ this.host }/content-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Проверить доступность ноды
     * @param requestKind Вид запроса
     * @param publishNodeId ID нода публикации
     * @returns Флаг доступности ноды
     */
    public checkNodeAvailability(requestKind: RequestKind, publishNodeId: string) {
        return this.requestSender.submitRequest<boolean>(
            WebVerb.GET,
            `${ this.host }/PublishNodeReferences/CheckNodeAvailability`,
            this.getObjectArguments(requestKind, { id: publishNodeId })
        );
    }
}