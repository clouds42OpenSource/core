import { TResponseLowerCase } from 'app/api/types';
import { CommonCreateSegmentParams, CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { CommonSegmentResponseDto, GetTerminalFarmsRequestDto } from 'app/web/api/SegmentsProxy/common';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Терм. сервера"
 */
export class TerminalServerTabProxy extends BaseProxy {
    /**
     * Получить данные терминальных серверов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по терминальным серверам
     */
    public getCloudTerminalServers(requestKind: RequestKind, args: GetTerminalFarmsRequestDto) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<CommonSegmentDataModel>, GetTerminalFarmsRequestDto>(
            WebVerb.GET,
            `${ this.host }/terminal-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить терминальный сервер
     * @param requestKind Вид запроса
     * @param terminalServerId ID терминального сервера
     */
    public deleteCloudTerminalServer(requestKind: RequestKind, terminalServerId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/terminal-servers/${ terminalServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить терминальный сервер
     * @param requestKind Вид запроса
     * @param terminalServerId ID терминального сервера
     * @returns Модель терминального сервера
     */
    public getCloudTerminalServer(requestKind: RequestKind, terminalServerId: string) {
        return this.requestSender.submitRequest<CommonSegmentResponseDto>(
            WebVerb.GET,
            `${ this.host }/terminal-servers/${ terminalServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новый терминальный сервер
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createCloudTerminalServer(requestKind: RequestKind, args: CommonCreateSegmentParams) {
        return this.requestSender.submitRequest<void, CommonCreateSegmentParams>(
            WebVerb.POST,
            `${ this.host }/terminal-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать терминальный сервер
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editCloudTerminalServer(requestKind: RequestKind, args: CommonSegmentDataModel) {
        return this.requestSender.submitRequest<void, CommonSegmentDataModel>(
            WebVerb.PUT,
            `${ this.host }/terminal-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }
}