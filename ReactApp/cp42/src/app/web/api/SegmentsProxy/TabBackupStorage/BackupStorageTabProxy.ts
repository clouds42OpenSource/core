import { TResponseLowerCase } from 'app/api/types';
import { CloudBackupStorageDataModel, CloudBackupStorageFilterParams, CreateCloudBackupStorageParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Хранилище бекапов"
 */
export class BackupStorageTabProxy extends BaseProxy {
    /**
     * Получить данные хранилищ бэкапов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по хранилищам бэкапов
     */
    public getCloudBackupStorages(requestKind: RequestKind, args: CloudBackupStorageFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<CloudBackupStorageDataModel>, CloudBackupStorageFilterParams>(
            WebVerb.GET,
            `${ this.host }/backup-storages`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить хранилище бэкапов
     * @param requestKind Вид запроса
     * @param backupStorageId ID хранилища бэкапа
     */
    public deleteCloudBackupStorage(requestKind: RequestKind, backupStorageId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/backup-storages/${ backupStorageId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить хранилище бэкапов
     * @param requestKind Вид запроса
     * @param backupStorageId ID хранилища бэкапа
     * @returns Модель хранилища бэкапов
     */
    public getCloudBackupStorage(requestKind: RequestKind, backupStorageId: string) {
        return this.requestSender.submitRequest<CloudBackupStorageDataModel>(
            WebVerb.GET,
            `${ this.host }/backup-storages/${ backupStorageId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новое хранилище бэкапов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createCloudBackupStorage(requestKind: RequestKind, args: CreateCloudBackupStorageParams) {
        return this.requestSender.submitRequest<void, CreateCloudBackupStorageParams>(
            WebVerb.POST,
            `${ this.host }/backup-storages`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать хранилище бэкапов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editCloudBackupStorage(requestKind: RequestKind, args: CloudBackupStorageDataModel) {
        return this.requestSender.submitRequest<void, CloudBackupStorageDataModel>(
            WebVerb.PUT,
            `${ this.host }/backup-storages`,
            this.getObjectArguments(requestKind, args)
        );
    }
}