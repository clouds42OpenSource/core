import { CreateCloudBackupStorageRequestDto } from 'app/web/api/SegmentsProxy/TabBackupStorage/request-dto';

/**
 * Модель хранилища бэкапов
 */
export type CloudBackupStorageResponseDto = CreateCloudBackupStorageRequestDto & {
    /**
     * ID записи
     */
    Id: string;
}