import { SearchBaseRequestDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Модель создания хранилища бэкапов
 */
export type CreateCloudBackupStorageRequestDto = SearchBaseRequestDto & {
    /**
     * Физический путь
     */
    PhysicalPath: string;
}