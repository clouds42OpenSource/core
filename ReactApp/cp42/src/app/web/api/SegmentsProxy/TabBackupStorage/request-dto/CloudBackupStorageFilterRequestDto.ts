import { FilterBaseRequestDto, SearchBaseRequestDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Модель фильтра хранилища бэкапов
 */
export type CloudBackupStorageFilterRequestDto = FilterBaseRequestDto & SearchBaseRequestDto;