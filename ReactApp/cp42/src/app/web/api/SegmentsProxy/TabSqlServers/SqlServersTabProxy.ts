import { TResponseLowerCase } from 'app/api/types';
import { CloudSqlServerDataModel, CloudSqlServerFilterParams, CreateCloudSqlServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "SQL"
 */
export class SqlServersTabProxy extends BaseProxy {
    /**
     * Получить данные sql сервера
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по sql серверам
     */
    public getCloudSqlServers(requestKind: RequestKind, args: CloudSqlServerFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<CloudSqlServerDataModel>, CloudSqlServerFilterParams>(
            WebVerb.GET,
            `${ this.host }/sql-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить sql сервер
     * @param requestKind Вид запроса
     * @param sqlServerId ID sql сервера
     */
    public deleteCloudSqlServer(requestKind: RequestKind, sqlServerId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/sql-servers/${ sqlServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить sql сервер
     * @param requestKind Вид запроса
     * @param sqlServerId ID sql сервера
     * @returns Модель sql сервера
     */
    public getCloudSqlServer(requestKind: RequestKind, sqlServerId: string) {
        return this.requestSender.submitRequest<CloudSqlServerDataModel>(
            WebVerb.GET,
            `${ this.host }/sql-servers/${ sqlServerId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новый sql сервер
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createCloudSqlServer(requestKind: RequestKind, args: CreateCloudSqlServerParams) {
        return this.requestSender.submitRequest<void, CreateCloudSqlServerParams>(
            WebVerb.POST,
            `${ this.host }/sql-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать sql сервер
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editCloudSqlServer(requestKind: RequestKind, args: CloudSqlServerDataModel) {
        return this.requestSender.submitRequest<void, CloudSqlServerDataModel>(
            WebVerb.PUT,
            `${ this.host }/sql-servers`,
            this.getObjectArguments(requestKind, args)
        );
    }
}