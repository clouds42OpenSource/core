import { AccountDatabaseRestoreModelType } from 'app/common/enums';
import { SearchBaseRequestDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Модель создания Sql сервера
 */
export type CreateCloudSqlServerRequestDto = SearchBaseRequestDto & {
    /**
     * Тип модели восстановления
     */
    RestoreModelType?: AccountDatabaseRestoreModelType;
}