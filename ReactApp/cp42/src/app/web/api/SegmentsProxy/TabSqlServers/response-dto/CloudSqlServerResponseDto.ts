import { CreateCloudSqlServerRequestDto } from 'app/web/api/SegmentsProxy/TabSqlServers/request-dto';

/**
 * Модель Sql сервера
 */
export type CloudSqlServerResponseDto = CreateCloudSqlServerRequestDto & {
    /**
     * Id записи
     */
    Id: string;
}