import { SearchBaseRequestDto } from 'app/web/api/SegmentsProxy/common';
import { CreateCloudSqlServerRequestDto } from 'app/web/api/SegmentsProxy/TabSqlServers/request-dto';

/**
 * Фильтр поиска SQL серверов
 */
export type CloudSqlServerFilterRequestDto = SearchBaseRequestDto & CreateCloudSqlServerRequestDto;