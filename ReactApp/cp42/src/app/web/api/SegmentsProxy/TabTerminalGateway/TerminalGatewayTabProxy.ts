import { TResponseLowerCase } from 'app/api/types';
import { CommonCreateSegmentParams, CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { GetTerminalFarmsRequestDto } from 'app/web/api/SegmentsProxy/common';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Терм. шлюзы"
 */
export class TerminalGatewayTabProxy extends BaseProxy {
    /**
     * Получить данные терминальных шлюзов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по терминальным шлюзам
     */
    public getCloudGatewayTerminals(requestKind: RequestKind, args: GetTerminalFarmsRequestDto) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<CommonSegmentDataModel>, GetTerminalFarmsRequestDto>(
            WebVerb.GET,
            `${ this.host }/gateway-terminals`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить терминальный шлюз
     * @param requestKind Вид запроса
     * @param cloudGatewayTerminalId ID терминального шлюза
     */
    public deleteCloudGatewayTerminal(requestKind: RequestKind, cloudGatewayTerminalId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/gateway-terminals/${ cloudGatewayTerminalId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить терминальный шлюз
     * @param requestKind Вид запроса
     * @param cloudGatewayTerminalId ID терминального шлюза
     * @returns Модель терминального шлюза
     */
    public getCloudGatewayTerminal(requestKind: RequestKind, cloudGatewayTerminalId: string) {
        return this.requestSender.submitRequest<CommonSegmentDataModel>(
            WebVerb.GET,
            `${ this.host }/gateway-terminals/${ cloudGatewayTerminalId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новый терминальный шлюз
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createCloudGatewayTerminal(requestKind: RequestKind, args: CommonCreateSegmentParams) {
        return this.requestSender.submitRequest<void, CommonCreateSegmentParams>(
            WebVerb.POST,
            `${ this.host }/gateway-terminals`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать терминальный шлюз
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editCloudGatewayTerminal(requestKind: RequestKind, args: CommonSegmentDataModel) {
        return this.requestSender.submitRequest<void, CommonSegmentDataModel>(
            WebVerb.PUT,
            `${ this.host }/gateway-terminals`,
            this.getObjectArguments(requestKind, args)
        );
    }
}