import { CloudServicesSegmentBaseResponseDto } from 'app/web/api/SegmentsProxy';

/**
 * Модель редактирования сегмента
 */
export type EditCloudServicesSegmentRequestDto = Omit<CloudServicesSegmentBaseResponseDto, 'autoUpdateNodeId'> & {
    /**
     * ID элемента сегмента
     */
    id: string;

    /**
     * Доступные сегменты для миграции
     */
    availableMigrationSegment: {
        id: string;
        name: string;
    }[];

    /**
     * Файловые хранилища сегмента
     */
    segmentFileStorageServers: {
        id: string;
        name: string;
    }[];

    /**
     * Терминальные сервера сегмента
     */
    segmentTerminalServers: {
        id: string;
        name: string;
        connectionAddress: string;
        available: boolean;
    }[];
    autoUpdateNodeId: string | null;
}