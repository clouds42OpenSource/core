import { SegmentDropdownItemResponseDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Элементы сегмента
 */
export type SegmentDropdownListResponseDto = {
    /**
     * Список бэкап серверов
     */
    backupStorages: SegmentDropdownItemResponseDto[];
    /**
     * Список файловых хранилищ
     */
    fileStorageServers: SegmentDropdownItemResponseDto[];
    /**
     * Список серверов Предприятия 8.3
     */
    enterprise82Servers: SegmentDropdownItemResponseDto[];
    /**
     * Список серверов Предприятия 8.3
     */
    enterprise83Servers: SegmentDropdownItemResponseDto[];
    /**
     * Список серверов публикаций
     */
    contentServers: SegmentDropdownItemResponseDto[];
    /**
     * Список SQL серверов
     */
    sqlServers: SegmentDropdownItemResponseDto[];
    /**
     * Список ферм ТС
     */
    terminalFarms: SegmentDropdownItemResponseDto[];
    /**
     * Список шлюзов
     */
    gatewayTerminals: SegmentDropdownItemResponseDto[];
    /**
     * Список стабильных версий 8.2
     */
    stable82Versions: string[];
    /**
     * Список стабильных версий 8.3
     */
    stable83Versions: string[];
    /**
     * Список альфа версий 8.3
     */
    alpha83Versions: string[];
    /**
     * Список хостингов облака
     */
    coreHostingList: SegmentDropdownItemResponseDto[];
    autoUpdateNodeList: SegmentDropdownItemResponseDto[];
}