export type KeyValueStringLowerType = {
    key: string;
    value: string;
}