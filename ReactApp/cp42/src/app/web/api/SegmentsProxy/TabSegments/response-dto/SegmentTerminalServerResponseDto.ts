/**
 * Терминальный сервер сегмента
 */
export type SegmentTerminalServerResponseDto = {
    connectionAddress: string;
    key: string;
    value: string;
}