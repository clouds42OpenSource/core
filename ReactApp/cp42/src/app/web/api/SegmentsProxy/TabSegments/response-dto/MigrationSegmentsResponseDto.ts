import { MigrationSegmentResponseDto } from 'app/web/api/SegmentsProxy';

/**
 * Модель данных сегментов для миграции
 */
export type MigrationSegmentsResponseDto = {
    /**
     * Доступные сегменты для миграции
     */
    available: MigrationSegmentResponseDto[];
    /**
     * Сегменты для миграции
     */
    selected: MigrationSegmentResponseDto[];
}