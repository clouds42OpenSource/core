import { FilterBaseParams } from 'app/web/common/params';

/**
 * Фильтр поиска аккаунтов сегмента
 */
export type SegmentAccountFilterArgs = FilterBaseParams & {
    /**
     * ID сегмента
     */
    segmentId: string;
    /**
     * Номер аккаунта
     */
    accountIndexNumber?: number;
    /**
     * Название аккаунта
     */
    accountCaption?: string;
}

export type SegmentAccountFilterRequestDto = FilterBaseParams & {
    /**
     * Номер аккаунта
     */
    accountIndexNumber?: number;
    /**
     * Название аккаунта
     */
    accountCaption?: string;
}