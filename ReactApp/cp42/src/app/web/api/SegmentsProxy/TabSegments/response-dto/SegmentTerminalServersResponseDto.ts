import { SegmentTerminalServerResponseDto } from 'app/web/api/SegmentsProxy';

/**
 * Модель данных терминальных серверов для сегмента
 */
export type SegmentTerminalServersResponseDto = {
    /**
     * Терминальные сервера сегмента
     */
    selected: SegmentTerminalServerResponseDto[];
    /**
     * Доступные терминальные сервера для сегмента
     */
    available: SegmentTerminalServerResponseDto[];
}