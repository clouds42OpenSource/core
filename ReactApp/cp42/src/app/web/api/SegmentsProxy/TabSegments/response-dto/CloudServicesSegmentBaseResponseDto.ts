/**
 * Базовые данные сегмента
 */
export type CloudServicesSegmentBaseResponseDto = {
    /**
     * Имя элемента сегмента
     */
    name: string;
    /**
     * ID сервера бэкапов
     */
    backupStorageId: string;
    /**
     * ID файлового хранилища
     */
    fileStorageServersId: string;
    /**
     * ID сервера Предприятие 8.2
     */
    enterpriseServer82Id: string;
    /**
     * ID сервера Предприятие 8.3
     */
    enterpriseServer83Id: string;
    /**
     * ID сервера публикаций
     */
    contentServerId: string;
    /**
     * ID SQL сервера
     */
    sqlServerId: string;
    /**
     * ID фермы ТС
     */
    servicesTerminalFarmId: string;
    /**
     * ID шлюза
     */
    gatewayTerminalsId: string;
    /**
     * ID стабильной версии 8.2
     */
    stable82VersionId: string;
    /**
     * ID стабильной версии 8.3
     */
    stable83VersionId: string;
    /**
     * ID альфа версии 8.3
     */
    alpha83VersionId: string | null;
    /**
     * Путь к файлам пользователей
     */
    customFileStoragePath: string;
    /**
     * Название хостинга
     */
    hostingName: string;
    /**
     * Описание элемента сегмента
     */
    description: string;
    /**
     * Открывать базы в тонком клиенте по http ссылке
     */
    delimiterDatabaseMustUseWebService: boolean;
    /**
     * Id дефолтного файлового хранилища
     */
    defaultSegmentStorageId: string;
    autoUpdateNodeId: string | null;
    notMountDiskR?: boolean;
}