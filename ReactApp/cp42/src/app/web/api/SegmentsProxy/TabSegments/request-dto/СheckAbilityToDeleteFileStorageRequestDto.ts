/**
 * Модель параметра для проверки возможности удаления файлового хранилища из доступных
 */
export type CheckAbilityToDeleteFileStorageRequestDto = {
    /**
     * ID сегмента
     */
    SegmentId: string,

    /**
     * ID хранилища
     */
    StorageId: string;
}