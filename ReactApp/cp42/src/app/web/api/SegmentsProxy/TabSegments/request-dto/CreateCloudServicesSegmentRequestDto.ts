import { CloudServicesSegmentBaseResponseDto } from 'app/web/api/SegmentsProxy';

/**
 * Модель создания сегмента
 */
export type CreateCloudServicesSegmentRequestDto = CloudServicesSegmentBaseResponseDto & {
    /**
     * ID хостинга
     */
    coreHostingId: string;
    /**
     * Сегмент по умолчанию
     */
    isDefault: boolean;
}