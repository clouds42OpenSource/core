import { FilterBaseRequestDto, SearchBaseRequestDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Модель фильтра сегмента
 */
export type CloudSegmentFilterRequestDto = FilterBaseRequestDto & SearchBaseRequestDto & {
    /**
     * Массив Id терминальных ферм
     */
    TerminalFarmIds?: string[];
    /**
     * Массив Id серверов предприятий
     */
    EnterpriseServerIds?: string[];
    /**
     * Массив Id серверов публикаций
     */
    ContentServerIds?: string[];
    /**
     * Массив Id файловых хранилищ
     */
    FileStorageServerIds?: string[];
    /**
     * Массив Id SQL серверов
     */
    SqlServerIds?: string[];
    /**
     * Массив Id терминальных шлюзов
     */
    GatewayTerminalIds?: string[];
    /**
     * Путь к файлам пользователей
     */
    CustomFileStoragePath?: string;
}