/**
 * Модель данных сегмента для миграции
 */
export type MigrationSegmentResponseDto = {
    isAvailable: boolean;
    isSimilar: boolean;
    key: string;
    value: string;
}