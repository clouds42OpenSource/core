import { CloudServicesSegmentBaseResponseDto, MigrationSegmentsResponseDto, SegmentElementsResponseDto, SegmentFileStorageServersResponseDto, SegmentTerminalServersResponseDto } from 'app/web/api/SegmentsProxy';

/**
 * Модель сегмента
 */
export type CloudServicesSegmentResponseDto = {
    /**
     * ID элемента сегмента
     */
    id: string;
    /**
     * Базовые данные сегмента
     */
    baseData: CloudServicesSegmentBaseResponseDto;
    /**
     * Данные файловых хранилищ для сегмента
     */
    fileStoragesData: SegmentFileStorageServersResponseDto;
    /**
     * Данные сегментов для миграции сегмента
     */
    migrationData: MigrationSegmentsResponseDto;
    /**
     * Данные по терминальным серверам для сегмента
     */
    terminalServersData: SegmentTerminalServersResponseDto;
    /**
     * Элементы сегмента
     */
    additionalData: SegmentElementsResponseDto;
}