import { TResponseLowerCase } from 'app/api/types';
import { SegmentElementResponseDto } from 'app/web/api/SegmentsProxy/common';
import {
    CheckAbilityToDeleteFileStorageRequestDto,
    CreateCloudServicesSegmentRequestDto,
    EditCloudServicesSegmentRequestDto,
    SegmentAccountFilterArgs,
    SegmentAccountFilterRequestDto
} from 'app/web/api/SegmentsProxy/TabSegments/request-dto';
import {
    CloudSegmentResponseDto,
    CloudServicesSegmentResponseDto,
    GetSegmentShortDataResponseDto,
    SegmentAccountResponseDto,
    SegmentDropdownListResponseDto,
    SegmentElementsResponseDto
} from 'app/web/api/SegmentsProxy/TabSegments/response-dto';
import { BaseProxy, SelectDataResultMetadataModel } from 'app/web/common';
import { CloudSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Сегменты"
 */
export class SegmentsTabProxy extends BaseProxy {
    /**
     * Получить терминальные фермы по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных терминальных ферм
     */
    public getCloudTerminalFarms(requestKind: RequestKind, searchValue: string) {
        return this.requestSender.submitRequest<Array<SegmentElementResponseDto>>(
            WebVerb.GET,
            `${ this.host }/CloudTerminalFarm/SearchCloudTerminalFarms`,
            this.getObjectArguments(requestKind, { searchValue })
        );
    }

    /**
     * Получить сервера предприятий по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных серверов предприятий
     */
    public getCloudEnterpriseServers(requestKind: RequestKind, searchValue: string) {
        return this.requestSender.submitRequest<Array<SegmentElementResponseDto>>(
            WebVerb.GET,
            `${ this.host }/CloudEnterpriseServer/SearchCloudEnterpriseServers`,
            this.getObjectArguments(requestKind, { searchValue })
        );
    }

    /**
     * Получить сервера публикации по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных серверов публикации
     */
    public getCloudContentServers(requestKind: RequestKind, searchValue: string) {
        return this.requestSender.submitRequest<Array<SegmentElementResponseDto>>(
            WebVerb.GET,
            `${ this.host }/CloudContentServer/SearchCloudContentServers`,
            this.getObjectArguments(requestKind, { searchValue })
        );
    }

    /**
     * Получить файловые хранилища по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных файловых хранилищ
     */
    public getCloudFileStorageServers(requestKind: RequestKind, searchValue: string) {
        return this.requestSender.submitRequest<Array<SegmentElementResponseDto>>(
            WebVerb.GET,
            `${ this.host }/CloudFileStorageServer/SearchCloudFileStorageServers`,
            this.getObjectArguments(requestKind, { searchValue })
        );
    }

    /**
     * Получить SQL сервера по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных SQL серверов
     */
    public getCloudSqlServers(requestKind: RequestKind, searchValue: string) {
        return this.requestSender.submitRequest<Array<SegmentElementResponseDto>>(
            WebVerb.GET,
            `${ this.host }/CloudSqlServer/SearchCloudSqlServers`,
            this.getObjectArguments(requestKind, { searchValue })
        );
    }

    /**
     * Получить терминальных шлюзов по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных терминальных шлюзов
     */
    public getCloudGatewayTerminals(requestKind: RequestKind, searchValue: string) {
        return this.requestSender.submitRequest<Array<SegmentElementResponseDto>>(
            WebVerb.GET,
            `${ this.host }/CloudGatewayTerminal/SearchCloudGatewayTerminals`,
            this.getObjectArguments(requestKind, { searchValue })
        );
    }

    /**
     * Получить данные сегментов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список сегментов c пагинацией
     */
    public getCloudSegments(requestKind: RequestKind, args: CloudSegmentFilterParams) {
        return this.requestSender.submitRequest<SelectDataResultMetadataModel<CloudSegmentResponseDto>, CloudSegmentFilterParams>(
            WebVerb.GET,
            `${ this.host }/segments`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить элементы сегмента
     * @param requestKind Вид запроса
     * @returns Элементы сегмента
     */
    public getSegmentElements(requestKind: RequestKind) {
        return this.requestSender.submitRequest<SegmentElementsResponseDto>(
            WebVerb.GET,
            `${ this.host }/CloudSegmentReference/GetSegmentElements`,
            this.getObjectArguments(requestKind)
        );
    }

    public getSegmentDropdownList(requestKind: RequestKind) {
        return this.requestSender.submitRequest<SegmentDropdownListResponseDto>(
            WebVerb.GET,
            `${ this.host }/segments/drop-downs`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новый сегмент
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createSegment(requestKind: RequestKind, args: CreateCloudServicesSegmentRequestDto) {
        return this.requestSender.submitRequest<void, CreateCloudServicesSegmentRequestDto>(
            WebVerb.POST,
            `${ this.host }/segments`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить сегмент по ID
     * @param requestKind Вид запроса
     * @param segmentId ID сегмента
     * @returns Модель сегмента
     */
    public getSegment(requestKind: RequestKind, segmentId: string) {
        return this.requestSender.submitRequest<CloudServicesSegmentResponseDto>(
            WebVerb.GET,
            `${ this.host }/segments/${ segmentId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить данные по аккаунтам сегмента
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по аккаунтам сегмента
     */
    public getSegmentAccounts(requestKind: RequestKind, args: SegmentAccountFilterArgs) {
        const { segmentId, ...rest } = args;
        return this.requestSender.submitRequest<SelectDataResultMetadataModel<SegmentAccountResponseDto>, SegmentAccountFilterRequestDto>(
            WebVerb.GET,
            `${ this.host }/segments/${ segmentId }/accounts`,
            this.getObjectArguments(requestKind, rest)
        );
    }

    /**
     * Удалить сегмент
     * @param requestKind Вид запроса
     * @param segmentId ID сегмента
     */
    public deleteSegment(requestKind: RequestKind, segmentId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/segments/${ segmentId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Редактировать сегмент
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editSegment(requestKind: RequestKind, args: EditCloudServicesSegmentRequestDto) {
        return this.requestSender.submitRequest<TResponseLowerCase<null> | null, EditCloudServicesSegmentRequestDto>(
            WebVerb.PUT,
            `${ this.host }/segments`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Выполнить запрос на получение кратких данных по сегментам
     * @param requestKind Вид запроса
     */
    public receiveSegmentsShortData(requestKind: RequestKind) {
        return this.requestSender.submitRequest<GetSegmentShortDataResponseDto>(
            WebVerb.GET,
            `${ this.host }/segments/segment-short-data`,
            {
                requestKind
            }
        );
    }

    /**
     * Проверить возможность удаления файлового хранилища из доступных
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Флаг обозначающий возможность удаления файлового хранилища из доступных
     */
    public checkAbilityToDeleteFileStorage(requestKind: RequestKind, args: CheckAbilityToDeleteFileStorageRequestDto) {
        return this.requestSender.submitRequest<boolean, CheckAbilityToDeleteFileStorageRequestDto>(
            WebVerb.GET,
            `${ this.host }/CloudSegmentReference/CheckAbilityToDeleteFileStorage`,
            this.getObjectArguments(requestKind, args)
        );
    }
}