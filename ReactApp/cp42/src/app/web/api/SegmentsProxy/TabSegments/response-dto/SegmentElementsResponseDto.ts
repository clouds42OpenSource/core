import { SegmentElementResponseDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Элементы сегмента
 */
export type SegmentElementsResponseDto = {
    /**
     * Список бэкап серверов
     */
    backupStorages: SegmentElementResponseDto[];
    /**
     * Список файловых хранилищ
     */
    fileStorageServers: SegmentElementResponseDto[];
    /**
     * Список серверов Предприятия 8.3
     */
    enterprise82Servers: SegmentElementResponseDto[];
    /**
     * Список серверов Предприятия 8.3
     */
    enterprise83Servers: SegmentElementResponseDto[];
    /**
     * Список серверов публикаций
     */
    contentServers: SegmentElementResponseDto[];
    /**
     * Список SQL серверов
     */
    sqlServers: SegmentElementResponseDto[];
    /**
     * Список ферм ТС
     */
    terminalFarms: SegmentElementResponseDto[];
    /**
     * Список шлюзов
     */
    gatewayTerminals: SegmentElementResponseDto[];
    /**
     * Список стабильных версий 8.2
     */
    stable82Versions: string[];
    /**
     * Список стабильных версий 8.3
     */
    stable83Versions: string[];
    /**
     * Список альфа версий 8.3
     */
    alpha83Versions: string[];
    /**
     * Список хостингов облака
     */
    coreHostingList: SegmentElementResponseDto[];
    autoUpdateNodeList: SegmentElementResponseDto[];
}