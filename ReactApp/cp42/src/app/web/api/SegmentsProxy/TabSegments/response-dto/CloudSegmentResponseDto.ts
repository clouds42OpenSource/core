/**
 * Модель данных сегмента
 */
export type CloudSegmentResponseDto = {
    /**
     * ID сегмента
     */
    id: string;
    /**
     * Имя сегмента
     */
    name: string;
    /**
     * Описание сегмента
     */
    description: string;
    /**
     * Имя шлюза
     */
    gatewayTerminalName: string;
    /**
     * Имя фермы ТС
     */
    terminalFarmName: string;
    /**
     * Имя сервера Предприятия 8.2
     */
    enterpriseServer82Name: string;
    /**
     * Имя сервера Предприятия 8.3
     */
    enterpriseServer83Name: string;
    /**
     * Имя сервера публикаций
     */
    contentServerName: string;
    /**
     * Имя файлового хранилища
     */
    fileStorageServerName: string;
    /**
     * Имя SQL сервера
     */
    sqlServerName: string;
    /**
     * Имя сервера бэкапов
     */
    backupStorageName: string;
    /**
     * Путь к файлам пользователей
     */
    customFileStoragePath: string;
    /**
     * Файловое хранилище по умолчанию
     */
    defaultFileStorageName: string;
    /**
     * версия платформы
     */
    stable83Version: string;
}