import { TResponseLowerCase } from 'app/api/types';
import { PlatformVersion1CDataModel, PlatformVersion1CFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { BaseProxy } from 'app/web/common';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с Табом "Платформа 1С"
 */
export class PlatformVersionTabProxy extends BaseProxy {
    /**
     * Получить данные версий платформы 1С
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных версий платформы 1С
     */
    public getPlatformVersions(requestKind: RequestKind, args: PlatformVersion1CFilterParams) {
        return this.requestSender.submitRequest<SelectDataMetadataResponseDto<PlatformVersion1CDataModel>, PlatformVersion1CFilterParams>(
            WebVerb.GET,
            `${ this.host }/platform-versions`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Удалить версию платформы 1С
     * @param requestKind Вид запроса
     * @param platformVersion Версия платформы 1С
     */
    public deletePlatformVersion(requestKind: RequestKind, platformVersion: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>>(
            WebVerb.DELETE,
            `${ this.host }/platform-versions?version=${ platformVersion }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Получить версию платформы 1С
     * @param requestKind Вид запроса
     * @param platformVersion Версия платформы 1С
     * @returns Модель версии платформы 1С
     */
    public getPlatformVersion(requestKind: RequestKind, platformVersion: string) {
        return this.requestSender.submitRequest<PlatformVersion1CDataModel>(
            WebVerb.GET,
            `${ this.host }/platform-versions/one?version=${ platformVersion }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Создать новый версию платформы 1С
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public createPlatformVersion(requestKind: RequestKind, args: PlatformVersion1CDataModel) {
        return this.requestSender.submitRequest<TResponseLowerCase<void>, PlatformVersion1CDataModel>(
            WebVerb.POST,
            `${ this.host }/platform-versions`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Редактировать версию платформы 1С
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    public editPlatformVersion(requestKind: RequestKind, args: PlatformVersion1CDataModel) {
        return this.requestSender.submitRequest<void, PlatformVersion1CDataModel>(
            WebVerb.PUT,
            `${ this.host }/platform-versions`,
            this.getObjectArguments(requestKind, args)
        );
    }
}