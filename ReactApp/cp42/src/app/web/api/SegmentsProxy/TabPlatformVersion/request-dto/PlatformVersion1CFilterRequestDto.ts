import { FilterBaseRequestDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Фильтер версии платформы 1С
 */
export type PlatformVersion1CFilterRequestDto = FilterBaseRequestDto & {
    /**
     * Версия платформы
     */
    Version: string;
    /**
     * Путь к платформе
     */
    PathToPlatform: string;
    /**
     * Путь к платформе x64
     */
    PathToPlatformX64: string;
}