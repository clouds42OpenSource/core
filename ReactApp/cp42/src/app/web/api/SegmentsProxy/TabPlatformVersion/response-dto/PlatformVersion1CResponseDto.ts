/**
 * Модель версии платформы 1С
 */
export type PlatformVersion1CResponseDto = {
    /**
     * Версия платформы
     */
    Version: string;
    /**
     * Путь к платформе
     */
    PathToPlatform: string;
    /**
     * Путь к платформе x64
     */
    PathToPlatformX64?: string;
    /**
     * Ссылка на скачивание тонкого клиента для MacOs
     */
    MacOsThinClientDownloadLink?: string;
    /**
     * Ссылка на скачивание тонкого клиента для Windows
     */
    WindowsThinClientDownloadLink?: string;
}