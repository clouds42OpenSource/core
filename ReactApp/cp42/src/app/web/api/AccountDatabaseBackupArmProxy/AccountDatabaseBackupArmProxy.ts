import { GetAccountDatabaseBackupHistoriesRequestDto } from 'app/web/api/AccountDatabaseBackupArmProxy/request-dto';
import { AccountDatabaseBackupArmPaginationResponseDto } from 'app/web/api/AccountDatabaseBackupArmProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с ARM бэкапированием
 */
export class AccountDatabaseBackupArmProxy extends BaseProxy {
    /**
     * Получить список историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
     * @param requestKind Вид запроса
     * @param args параметры при получении результата
     */
    public getAccountDatabaseBackupHistoriesWithPagination(requestKind: RequestKind, args: GetAccountDatabaseBackupHistoriesRequestDto) {
        return this.requestSender.submitRequest<AccountDatabaseBackupArmPaginationResponseDto>(
            WebVerb.GET,
            `${ this.host }/AccountDatabaseBackupArm/GetAccountDatabaseBackupHistoriesWithPagination`,
            this.getObjectArguments(requestKind, args)
        );
    }
}