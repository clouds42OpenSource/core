/**
 * Параметры на получения историй бэкапов баз аккаунтов
 */
export type GetAccountDatabaseBackupHistoriesRequestDto = {
    /**
     * номер страницы DangerTable
     */
    dangerTablePage: number,

    /**
     * номер страницы WarningTable
     */
    warningTablePage: number,

    /**
     * фильтр поиска
     */
    searchQuery: string
}