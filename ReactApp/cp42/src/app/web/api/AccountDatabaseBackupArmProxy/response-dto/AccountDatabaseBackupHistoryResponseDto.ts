/**
 * Dto обьект истории баз аккаунта
 */
export type AccountDatabaseBackupHistoryResponseDto = {
    /**
     * ID истории бэкапа базы аккаунта
     */
    AccountDatabaseBackupHistoryId: string;

    /**
     * Дата создания записи
     */
    AccountDatabaseBackupHistoryCreateDateTime: string;

    /**
     * Коментарий архивации
     */
    AccountDatabaseBackupHistoryMessage: string;

    /**
     * Номер базы
     */
    V82Name: string;

    /**
     * ID инф. базы
     */
    AccountDatabaseId: string;

    /**
     * Номер аккаунта
     */
    AccountIndexNumber: number;

    /**
     * ID аккаунта
     */
    AccountId: string;
}