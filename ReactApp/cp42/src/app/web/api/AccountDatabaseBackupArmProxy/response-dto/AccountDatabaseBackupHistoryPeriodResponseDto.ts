/**
 * Модель периода от и до для историй бэкапов баз аккаунта
 */
export type AccountDatabaseBackupHistoryPeriodResponseDto = {
    /**
     * Период от
     */
    FromDays: number;

    /**
     * Период до
     */
    ToDays?: number;
}