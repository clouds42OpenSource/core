import { AccountDatabaseBackupHistoryPeriodResponseDto } from 'app/web/api/AccountDatabaseBackupArmProxy/response-dto';
import { SelectDataResultCommonResponseDto } from 'app/web/common/response-dto';
import { AccountDatabaseBackupHistoryResponseDto } from './AccountDatabaseBackupHistoryResponseDto';

/**
 * Dto обьект историй бэкапов баз аккаунтов с пагинацией
 */
export type AccountDatabaseBackupArmPaginationResponseDto = {
    /**
     * Список старых историй бэкапов баз аккаунтов с пагинацией
     */
    DangerTable: SelectDataResultCommonResponseDto<AccountDatabaseBackupHistoryResponseDto>;

    /**
     * Список новых историй бэкапов баз аккаунтов с пагинацией
     */
    WarningTable: SelectDataResultCommonResponseDto<AccountDatabaseBackupHistoryResponseDto>;

    /**
     * Период старых историй бэкапов баз аккаунтов
     */
    DangerTablePeriod: AccountDatabaseBackupHistoryPeriodResponseDto;

    /**
     * Период новых историй бэкапов баз аккаунтов
     */
    WarningTablePeriod: AccountDatabaseBackupHistoryPeriodResponseDto;
}