import { DbTemplateUpdatesRequestDto } from 'app/web/api/DbTemplateUpdateProxy/request-dto';
import { DbTemplatesResponseDto, DbTemplateUpdatesResponseDto } from 'app/web/api/DbTemplateUpdateProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { TResponseLowerCase } from 'app/api/types';

/**
 * Прокси работающий с DbTemplateUpdate
 */
export class DbTemplateUpdateProxy extends BaseProxy {
    /**
     * Запрос на получение обновлений шаблона
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Массив моделей для обновления шаблона баз
     */
    public getTemplateUpdates(requestKind: RequestKind, args: DbTemplateUpdatesRequestDto) {
        return this.requestSender.submitRequest<Array<DbTemplateUpdatesResponseDto>, DbTemplateUpdatesRequestDto>(
            WebVerb.GET,
            `${ this.host }/db-template-update-arm`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Получить все шаблоны ввиде key - ID шаблона, value - описание шаблона
     * @param requestKind Вид запроса
     * @returns Все шаблоны ввиде словаря
     */
    public getDbTemplates(requestKind: RequestKind) {
        return this.requestSender.submitRequest<Array<DbTemplatesResponseDto>>(
            WebVerb.GET,
            `${ this.host }/db-templates/as-key-value`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Принять обновления шаблона.
     * @param requestKind Вид запроса
     * @param dbTemplateUpdateId ID обновленного шаблона
     */
    public applyUpdates(requestKind: RequestKind, dbTemplateUpdateId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<null> | null>(
            WebVerb.PUT,
            `${ this.host }/db-template-update-arm/${ dbTemplateUpdateId }/apply-updates`,
            this.getObjectArguments(requestKind, { templateId: dbTemplateUpdateId })
        );
    }

    /**
     * Отклонить обновления шаблона.
     * @param requestKind Вид запроса
     * @param dbTemplateUpdateId ID обновленного шаблона
     */
    public rejectUpdates(requestKind: RequestKind, dbTemplateUpdateId: string) {
        return this.requestSender.submitRequest<TResponseLowerCase<null> | null>(
            WebVerb.PUT,
            `${ this.host }/db-template-update-arm/${ dbTemplateUpdateId }/reject-updates`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Удалить обновления шаблона.
     * @param requestKind Вид запроса
     */
    public deleteUpdates(requestKind: RequestKind) {
        return this.requestSender.submitRequest<TResponseLowerCase<null> | null>(
            WebVerb.DELETE,
            `${ this.host }/db-template-update-arm`,
            this.getObjectArguments(requestKind)
        );
    }
}