import { DbTemplateUpdateStateEnumType } from 'app/common/enums';

/**
 * Модель для обновления шаблона баз
 */
export type DbTemplateUpdatesResponseDto = {
    /**
     * ID Обновления шаблона баз
     */
    id: string;

    /**
     * Заголовок по умолчанию у шаблона базы
     */
    defaultCaption: string;

    /**
     * Версия обновления
     */
    updateVersion: string;

    /**
     * Дата обновления
     */
    updateVersionDate: string;

    /**
     * Полный путь до обновленного шаблона
     */
    updateTemplatePath: string;

    /**
     * Статус проверки обновления на ошибки человеком
     */
    validateState: DbTemplateUpdateStateEnumType;
}