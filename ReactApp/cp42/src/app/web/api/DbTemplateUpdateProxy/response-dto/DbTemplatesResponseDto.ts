import { KeyValueResponseDto } from 'app/web/common/response-dto';

/**
 * Все шаблоны ввиде словаря key - ID шаблона, value - описание шаблона
 */
export type DbTemplatesResponseDto = KeyValueResponseDto<string, string>;