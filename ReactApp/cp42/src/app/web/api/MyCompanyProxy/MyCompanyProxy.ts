import { TResponseUpperCase } from 'app/api/types';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { ChangeDatabaseMigrationRequestDto, ChangeMyCompanySegmentRequestDto, DatabaseMigrationRequestDto, EditMyCompanyRequestDto, MyCompanyRequestDto, MyCompanySegmentRequestDto } from 'app/web/api/MyCompanyProxy/request-dto';
import { DatabaseMigrationResponseDto, MyCompanyResponseDto, MyCompanySegmentResponseDto } from 'app/web/api/MyCompanyProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с Данными о компании
 */
export class MyCompanyProxy extends BaseProxy {
    accountId = localStorageHelper.getContextAccountId();

    token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение данных о компании
     * @param requestKind тип запроса
     */
    public receiveMyCompany(requestKind: RequestKind) {
        return this.requestSender.submitRequest<MyCompanyResponseDto, MyCompanyRequestDto>(
            WebVerb.GET,
            `${ this.host }/accounts/${ this.accountId }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на изменение данных о компании
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    public editMyCompany(requestKind: RequestKind, args: EditMyCompanyRequestDto) {
        return this.requestSender.submitRequest<TResponseUpperCase<boolean>, EditMyCompanyRequestDto>(
            WebVerb.PUT,
            `${ this.host }/accounts/info`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получения сегментов компании
     * @param requestKind тип запроса
     */
    public receiveMyCompanySegment(requestKind: RequestKind) {
        return this.requestSender.submitRequest<MyCompanySegmentResponseDto, MyCompanySegmentRequestDto>(
            WebVerb.GET,
            `${ this.host }/accounts/${ this.accountId }/segment`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на изменение сегмента компании
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    public changeMyCompanySegment(requestKind: RequestKind, args: ChangeMyCompanySegmentRequestDto) {
        return this.requestSender.submitRequest<void, ChangeMyCompanySegmentRequestDto>(
            WebVerb.PUT,
            `${ this.host }/accounts/segment`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение БД для миграции
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    public receiveDatabaseMigration(requestKind: RequestKind, args: DatabaseMigrationRequestDto) {
        return this.requestSender.submitRequest<DatabaseMigrationResponseDto, DatabaseMigrationRequestDto>(
            WebVerb.GET,
            `${ this.host }/accounts/database-migration`,
            this.getObjectArguments(requestKind, { 'Filter.AccountId': this.accountId, ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на миграцию БД
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    public changeDatabasesMigration(requestKind: RequestKind, args: ChangeDatabaseMigrationRequestDto) {
        return this.requestSender.submitRequest<void, ChangeDatabaseMigrationRequestDto>(
            WebVerb.PUT,
            `${ this.host }/accounts/database-migration`,
            this.getObjectArguments(
                requestKind,
                {
                    databases: args.databases,
                    storage: args.storage
                },
                'Application/json',
                { Authorization: `Bearer ${ this.token }` }
            )
        );
    }
}