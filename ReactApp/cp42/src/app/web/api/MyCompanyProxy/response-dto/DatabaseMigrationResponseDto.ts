import { Nullable } from 'app/common/types';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

type FileStorageAccountDatabasesSummaryItem = {
    fileStorage: string;
    pathStorage: Nullable<string>;
    databaseCount: number;
    totatlSizeOfDatabases: number;
};

type RecordsItem = {
    accountDatabaseId: string;
    v82Name: string;
    path: string;
    size: number;
    status: number;
    isPublishDatabase: boolean;
    isPublishServices: boolean;
    isFile: boolean;
};

export type DatabasesItem = SelectDataMetadataResponseDto<RecordsItem>;

type AvalableFileStoragesItem = {
    key: string;
    value: string;
};

/**
 * Модель ответа с данными о БД для миграции
 */
export type DatabaseMigrationResponseDto = {
    fileStorageAccountDatabasesSummary: FileStorageAccountDatabasesSummaryItem[];
    databases: DatabasesItem;
    avalableFileStorages: AvalableFileStoragesItem[];
    availableStoragePath: Nullable<string>[];
    numberOfDatabasesToMigrate: number;
};