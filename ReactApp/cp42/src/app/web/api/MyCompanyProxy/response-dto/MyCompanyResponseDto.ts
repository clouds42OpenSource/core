import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';
import { Nullable } from 'app/common/types';
import { TAccountDepartment, TAdditionalCompany } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

type LocalesType = {
    key: string;
    value: string;
};

/**
 * Модель ответа с данными о компании
 */
export type MyCompanyResponseDto = {
    accountCaption: string;
    accountDescription: string;
    accountId: string;
    accountInn: Nullable<string>;
    accountSaleManagerCaption: string;
    accountType: string;
    balance: number;
    canChangeLocale: boolean;
    cloudStorageWebLink: string;
    emails: string[];
    indexNumber: number;
    isVip: boolean;
    lastActivity: string;
    localeCurrency: string;
    localeId: string;
    locales: Nullable<LocalesType[]>;
    registrationDate: string;
    segmentId: string;
    segmentName: string;
    serviceTotalSum: number;
    usedSizeOnDisk: number;
    userSource: string;
    additionalCompanies: TAdditionalCompany[];
    verificationStatus: EVerificationStatus | null;
    createClusterDatabase: boolean;
    departments: TAccountDepartment[];
};