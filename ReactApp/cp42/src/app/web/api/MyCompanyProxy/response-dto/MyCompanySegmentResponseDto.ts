import { MyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompanySegment';

/**
 * Модель ответа сегментов компании
 */
export type MyCompanySegmentResponseDto = MyCompanySegmentDataModel[];