/**
 * Модель запроса к апи для миграции БД
 */
export type ChangeDatabaseMigrationRequestDto = {
    storage: string;
    databases: string[];
};