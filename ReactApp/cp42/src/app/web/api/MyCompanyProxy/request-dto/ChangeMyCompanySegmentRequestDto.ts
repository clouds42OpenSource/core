/**
 * Модель запроса к апи для изменения сегмента компании
 */
export type ChangeMyCompanySegmentRequestDto = {
    AccountId: string;
    SegmentId: string;
};