import { EDepartmentAction } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

type TEditDepartment = {
    DepartmentId?: string;
    Name: string;
    ActionType?: EDepartmentAction;
};

export type EditMyCompanyRequestDto = {
    AccountId: string;
    AccountDescription: string;
    AccountCaption: string;
    AccountInn: string;
    IsVip: boolean;
    LocaleId: string;
    Emails: string[];
    CreateClusterDatabase: boolean;
    DepartmentRequests: TEditDepartment[];
};