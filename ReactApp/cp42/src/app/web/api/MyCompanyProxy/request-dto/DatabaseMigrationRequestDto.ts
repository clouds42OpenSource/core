import { Nullable } from 'app/common/types';

/**
 * Модель запроса к апи для получения БД для миграции
 */
export type DatabaseMigrationRequestDto = {
    'Filter.AccountIndexNumber': Nullable<number>;
    'Filter.SearchLine'?: string;
    'Filter.IsTypeStorageFile'?: Nullable<boolean>;
    'PageNumber': number;
    'OrderBy'?: string;
};