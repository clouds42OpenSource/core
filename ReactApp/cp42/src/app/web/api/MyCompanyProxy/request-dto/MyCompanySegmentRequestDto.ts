/**
 * Модель запроса к апи для получения сегментов компании
 */
export type MyCompanySegmentRequestDto = {
    accountId: string;
};