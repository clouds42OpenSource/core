/**
 * Модель запроса к апи для получения данных о компании
 */
export type MyCompanyRequestDto = {
    accountId: string;
};