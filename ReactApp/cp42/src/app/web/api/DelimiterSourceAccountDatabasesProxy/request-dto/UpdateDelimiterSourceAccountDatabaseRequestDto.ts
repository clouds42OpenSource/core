/**
 * Параметры запроса для обновления материнской базы разделителей
 */
export type UpdateDelimiterSourceAccountDatabaseRequestDto = {

    /**
     * Id информационной базы
     */
    AccountDatabaseId: string;

    /**
     * Код конфигурации базы на разделителях
     */
    DbTemplateDelimiterCode: string;

    /**
     * Адрес публикации базы на разделителях
     */
    DatabaseOnDelimitersPublicationAddress: string;
};