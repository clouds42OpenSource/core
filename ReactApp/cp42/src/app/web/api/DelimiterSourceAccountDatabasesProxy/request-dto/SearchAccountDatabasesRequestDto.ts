/**
 * Запрос на поиск базы
 */
export type SearchAccountDatabasesRequestDto = {
    /**
     * Строка поиска
     */
    searchLine: string;
};