/**
 * Параметры запроса для добавления новой записи материнской базы разделителей
 */
export type InsertDelimiterSourceAccountDatabaseRequestDto = {

    /**
     * Id информационной базы
     */
    AccountDatabaseId: string;

    /**
     * Код конфигурации базы на разделителях
     */
    DbTemplateDelimiterCode: string;

    /**
     * Адрес публикации базы на разделителях
     */
    DatabaseOnDelimitersPublicationAddress: string;
};