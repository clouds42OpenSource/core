/**
 * Параметры запроса для удаления записи материнской базы разделителей
 */
export type DeleteDelimiterSourceAccountDatabaseRequestDto = {

    /**
     * Id информационной базы
     */
    accountDatabaseId: string;

    /**
     * Код конфигурации базы на разделителях
     */
    dbTemplateDelimiterCode: string;
};