/**
 * Модель фильтра записей материнских баз разделителей
 */
export type DelimiterSourceAccountDatabaseFilterDto = {
    /**
     * Строка поиска
     */
    SearchLine: string;
};