import { DelimiterSourceAccountDatabaseFilterDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/request-dto/DelimiterSourceAccountDatabaseFilterDto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель параметров на запрос получения материнских баз разделителей
 */
export type GetDelimiterSourceAccountDatabaseFilterParamsRequestDto = SelectDataCommonRequestDto<DelimiterSourceAccountDatabaseFilterDto>;