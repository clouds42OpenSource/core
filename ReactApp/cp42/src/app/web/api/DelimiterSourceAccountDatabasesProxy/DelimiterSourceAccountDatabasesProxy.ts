import { GetDelimiterSourceAccountDatabaseFilterParamsRequestDto, InsertDelimiterSourceAccountDatabaseRequestDto, UpdateDelimiterSourceAccountDatabaseRequestDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/request-dto';
import { DeleteDelimiterSourceAccountDatabaseRequestDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/request-dto/DeleteDelimiterSourceAccountDatabaseRequestDto';
import { SearchAccountDatabasesRequestDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/request-dto/SearchAccountDatabasesRequestDto';
import { DelimiterSourceAccountDatabaseResultResponseDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/response-dto';
import { SearchAccountDatabaseResponseDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/response-dto/SearchAccountDatabaseResponseDto';
import { BaseProxy } from 'app/web/common';
import { KeyValueResponseDto } from 'app/web/common/response-dto/KeyValueResponseDto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с DelimiterSourceAccountDatabases
 */
export class DelimiterSourceAccountDatabasesProxy extends BaseProxy {
    /**
     * Запрос на получение материнских баз разделителей
     * @param requestKind Вид запроса
     * @param args параметры на получение списка материнских баз разделителей
     */
    public receiveDelimiterSourceAccountDatabases(requestKind: RequestKind, args: GetDelimiterSourceAccountDatabaseFilterParamsRequestDto) {
        return this.requestSender.submitRequest<DelimiterSourceAccountDatabaseResultResponseDto, GetDelimiterSourceAccountDatabaseFilterParamsRequestDto>(
            WebVerb.GET,
            `${ this.host }/delimiter-source-account-databases`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на обновление записи материнской базы разделителей
     * @param requestKind Вид запроса
     * @param args параметры на обновление записи материнской базы разделителей
     */
    public updateDelimiterSourceAccountDatabase(requestKind: RequestKind, args: UpdateDelimiterSourceAccountDatabaseRequestDto) {
        return this.requestSender.submitRequest<void, UpdateDelimiterSourceAccountDatabaseRequestDto>(
            WebVerb.PUT,
            `${ this.host }/delimiter-source-account-databases`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на добавление новой записи материнской базы разделителей
     * @param requestKind Вид запроса
     * @param args параметры на добавление новой записи материнской базы разделителей
     */
    public insertDelimiterSourceAccountDatabase(requestKind: RequestKind, args: InsertDelimiterSourceAccountDatabaseRequestDto) {
        return this.requestSender.submitRequest<void, InsertDelimiterSourceAccountDatabaseRequestDto>(
            WebVerb.POST,
            `${ this.host }/delimiter-source-account-databases`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на удаление записи материнской базы разделителей
     * @param requestKind Вид запроса
     * @param args параметры на удаление записи материнской базы разделителей
     */
    public deleteDelimiterSourceAccountDatabase(requestKind: RequestKind, args: DeleteDelimiterSourceAccountDatabaseRequestDto) {
        return this.requestSender.submitRequest<void, DeleteDelimiterSourceAccountDatabaseRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/delimiter-source-account-databases/${ args.accountDatabaseId }/${ args.dbTemplateDelimiterCode }`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Запрос на поиск информационной базы
     * @param requestKind Вид запроса
     * @param args параметры на поиск информационной базы
     */
    public searchAccountDatabases(requestKind: RequestKind, args: SearchAccountDatabasesRequestDto) {
        return this.requestSender.submitRequest<Array<SearchAccountDatabaseResponseDto>, SearchAccountDatabasesRequestDto>(
            WebVerb.GET,
            `${ this.host }/account-databases/as-list`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на получение конфигураций базы на разделителях
     * @param requestKind Вид запроса
     */
    public getDbTemplateDelimiters(requestKind: RequestKind) {
        return this.requestSender.submitRequest<Array<KeyValueResponseDto<string, string>>, void>(
            WebVerb.GET,
            `${ this.host }/db-template-delimiters/as-key-value`,
            this.getObjectArguments(requestKind)
        );
    }
}