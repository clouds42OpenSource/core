import { DelimiterSourceAccountDatabaseItemResponseDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Записи материнских баз разделителей
 */
export type DelimiterSourceAccountDatabaseResultResponseDto = SelectDataMetadataResponseDto<DelimiterSourceAccountDatabaseItemResponseDto>;