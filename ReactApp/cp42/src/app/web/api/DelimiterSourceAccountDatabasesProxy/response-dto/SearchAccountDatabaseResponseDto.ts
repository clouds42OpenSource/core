/**
 * Модель с найденой информационной базой.
 */
export type SearchAccountDatabaseResponseDto = {
    /**
     * Описание информационной базы
     */
    accountDatabaseCaption: string;

    /**
     * Id информационной базы
     */
    accountDatabaseId: string;

    /**
     * Номер информационной базы
     */
    accountDatabaseV82Name: string;
};