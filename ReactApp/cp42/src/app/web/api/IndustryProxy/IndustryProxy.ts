import { DeleteIndustryItemRequestDtoRequestDto, GetIndustryParamsDto, UpdateIndustryItemRequestDto } from 'app/web/api/IndustryProxy/request-dto';
import { AddIndustryItemRequestDto } from 'app/web/api/IndustryProxy/request-dto/AddIndustryItemRequestDto';
import { GetIndustryResultResponseDto } from 'app/web/api/IndustryProxy/response-dto';
import { AddNewIndustryResultResponseDto } from 'app/web/api/IndustryProxy/response-dto/AddNewIndustryResultResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

/**
 * Прокси работающий с Industry
 */
export class IndustryProxy extends BaseProxy {
    private token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение отраслей
     * @param requestKind Вид запроса
     * @param args параметры на получение отраслей
     */
    public receiveIndustry(requestKind: RequestKind, args: GetIndustryParamsDto) {
        return this.requestSender.submitRequest<GetIndustryResultResponseDto, GetIndustryParamsDto>(
            WebVerb.GET,
            `${ this.host }/industries`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на редактирование отрасли
     * @param requestKind Вид запроса
     * @param args параметры на обновление отрасли
     */
    public updateIndustry(requestKind: RequestKind, args: UpdateIndustryItemRequestDto) {
        return this.requestSender.submitRequest<void, UpdateIndustryItemRequestDto>(
            WebVerb.PUT,
            `${ this.host }/industries`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на добавление отрасли
     * @param requestKind Вид запроса
     * @param args параметры на добавление отрасли
     */
    public addIndustry(requestKind: RequestKind, args: AddIndustryItemRequestDto) {
        return this.requestSender.submitRequest<AddNewIndustryResultResponseDto, AddIndustryItemRequestDto>(
            WebVerb.POST,
            `${ this.host }/industries`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на добавление отрасли
     * @param requestKind Вид запроса
     * @param args параметры на удаление отрасли
     */
    public deleteIndustry(requestKind: RequestKind, args: DeleteIndustryItemRequestDtoRequestDto) {
        return this.requestSender.submitRequest<void, DeleteIndustryItemRequestDtoRequestDto>(
            WebVerb.DELETE,
            `${ this.host }/industries`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}