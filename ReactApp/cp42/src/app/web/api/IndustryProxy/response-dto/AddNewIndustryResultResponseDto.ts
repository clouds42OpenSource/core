/**
 * Модель результата на добавление новой отрасли
 */
export type AddNewIndustryResultResponseDto = {
    /**
     * ID отрасли после добавления
     */
    industryId: string;
};