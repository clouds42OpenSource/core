/**
 *  Модель свойств элемента отрасли
 */
export type IndustryItemResponseDto = {
    /**
     * ID отрасли
     */
    id: string;

    /**
     * Название отрасли
     */
    name: string;

    /**
     * Описание отрасли
     */
    description: string;
};