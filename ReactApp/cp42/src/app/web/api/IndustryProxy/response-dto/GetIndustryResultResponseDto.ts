import { IndustryItemResponseDto } from 'app/web/api/IndustryProxy/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа на получение списка отраслей
 */
export type GetIndustryResultResponseDto = SelectDataMetadataResponseDto<IndustryItemResponseDto>;