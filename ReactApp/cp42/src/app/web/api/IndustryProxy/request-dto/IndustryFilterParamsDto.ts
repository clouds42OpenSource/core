/**
 * Модель фильтра для выбора отраслей
 */
export type IndustryFilterParamsDto = {
    /**
     * Название отрасли
     */
    name: string;
};