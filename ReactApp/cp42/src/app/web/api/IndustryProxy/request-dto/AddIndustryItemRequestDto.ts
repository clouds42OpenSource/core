/**
 * Модель на добавление отрасли
 */
export type AddIndustryItemRequestDto = {
    /**
     * Название отрасли
     */
    Name: string;

    /**
     * Описание отрасли
     */
    Description: string;
};