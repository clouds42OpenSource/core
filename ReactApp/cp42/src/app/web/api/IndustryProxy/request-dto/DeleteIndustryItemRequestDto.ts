/**
 * Модель на удаление отрасли
 */
export type DeleteIndustryItemRequestDtoRequestDto = {
    /**
     * ID отрасли для удаления
     */
    IndustryId: string;
};