/**
 * Модель на обновление отрасли
 */
export type UpdateIndustryItemRequestDto = {
    /**
     * ID отрасли
     */
    Id: string;

    /**
     * Название отрасли
     */
    Name: string;

    /**
     * Описание отрасли
     */
    Description: string;
};