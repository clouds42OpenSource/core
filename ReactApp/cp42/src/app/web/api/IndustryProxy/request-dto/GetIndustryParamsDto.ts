import { IndustryFilterParamsDto } from 'app/web/api/IndustryProxy/request-dto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель параметров на запрос получения отраслей
 */
export type GetIndustryParamsDto = SelectDataCommonRequestDto<IndustryFilterParamsDto>;