import { Nullable } from 'app/common/types';

export type BankCardDetails = {
    firstSixDigits: string;
    lastFourDigits: string;
    expiryMonth: string;
    expiryYear: string;
    cardType: string;
};

export type SavedMethodsResponseDto = {
    id: string;
    accountId: string;
    createdOn: string;
    title: string;
    type: string;
    bankCardDetails: Nullable<BankCardDetails>;
    token: string;
    metadata: string;
    defaultPaymentMethod: boolean;
    paymentName: string | null;
}[];