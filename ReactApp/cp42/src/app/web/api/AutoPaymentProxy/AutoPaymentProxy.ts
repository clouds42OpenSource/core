import { DeletePaymentMethodRequestDto } from 'app/web/api/AutoPaymentProxy/request-dto/DeletePaymentMethodRequestDto';
import { SavedMethodsResponseDto } from 'app/web/api/AutoPaymentProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';

/**
 * Прокси для работы с счетами
 */
export class AutoPaymentProxy extends BaseProxy {
    private accountId = localStorageHelper.getContextAccountId();

    protected token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение счетов
     * @param requestKind Вид запроса
     */
    public getSavedPaymentMethods(requestKind: RequestKind) {
        return this.requestSender.submitRequest<SavedMethodsResponseDto, NonNullable<unknown>>(
            WebVerb.GET,
            `${ this.host }/saved-payments-methods`,
            this.getObjectArguments(requestKind, { accountId: this.accountId }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение счетов
     * @param requestKind Вид запроса
     * @param args данные фильтра
     */
    public deletePaymentMethod(requestKind: RequestKind, args: DeletePaymentMethodRequestDto): Promise<NonNullable<unknown>> {
        return this.requestSender.submitRequest(
            WebVerb.DELETE,
            `${ this.host }/saved-payments-methods/${ args.id }`,
            this.getObjectArguments(requestKind, undefined, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}