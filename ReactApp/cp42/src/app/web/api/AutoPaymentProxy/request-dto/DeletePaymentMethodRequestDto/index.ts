export type DeletePaymentMethodRequestDto = {
   id: string;
};