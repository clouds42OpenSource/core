import { CancelCoreWorkerTasksQueueRequestDto, ReceiveTasksInQueueRequestDto } from 'app/web/api/TasksInQueueProxy/request-dto';
import { TasksInQueueDataResponseDto } from 'app/web/api/TasksInQueueProxy/response-dto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с задачами в очереди
 */
export class TasksInQueueProxy extends BaseProxy {
    /**
     * Выполнить запрос на данных по очереди задач воркеров
     * @param requestKind Вид запроса
     * @param args фильтр при получении результата
     */
    public receiveTasksInQueueData(requestKind: RequestKind, args: ReceiveTasksInQueueRequestDto) {
        return this.requestSender.submitRequest<TasksInQueueDataResponseDto, ReceiveTasksInQueueRequestDto>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks-queue`,
            this.getObjectArguments(requestKind, args)
        );
    }

    /**
     * Запрос на отмену запущенной задачи
     * @param requestKind Вид запроса
     * @param args Модель запроса на отмену запущенной задачи
     */
    public cancelCoreWorkerTasksQueue(requestKind: RequestKind, args: CancelCoreWorkerTasksQueueRequestDto) {
        return this.requestSender.submitRequest<void, CancelCoreWorkerTasksQueueRequestDto>(
            WebVerb.POST,
            `${ this.host }/core-worker-tasks-queue/cancel-queued`,
            this.getObjectArguments(requestKind, args)
        );
    }
}