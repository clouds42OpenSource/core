/**
 * Модель ответа с элементом очереди задач воркеров
 */
export type TaskInQueueItemResponseDto = {
    /**
     * ID задачи
     */
    id: string;
    /**
     * Название задачи
     */
    name: string;
    /**
     * Статус
     */
    status: string;
    /**
     * Дата и время запуска
     */
    createDateTime: string;
    /**
     * Дата и время выполнения
     */
    finishDateTime: string;
    /**
     * ID воркера, который захватил задачу
     */
    workerId: number | null;
    /**
     * Комментарий к задаче
     */
    comment: string;
};