import { TaskInQueueItemResponseDto } from 'app/web/api/TasksInQueueProxy/response-dto/TaskInQueueItemResponseDto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении данных по очереди задач воркеров
 */
export type TasksInQueueDataResponseDto = SelectDataMetadataResponseDto<TaskInQueueItemResponseDto>;