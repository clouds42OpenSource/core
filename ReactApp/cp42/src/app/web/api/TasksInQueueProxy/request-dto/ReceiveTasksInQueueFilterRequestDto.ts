/**
 * Модель фильтра для получения данных по очереди задач воркеров
 */
export type ReceiveTasksInQueueFilterRequestDto = {
    /**
     * ID воркера, который захватил задачу
     */
    WorkerId: number | null;
    /**
     * ID задачи
     */
    TaskId: string | null;
    /**
     * Статус задачи
     */
    Status: string | null;
    /**
     * Начальный период поиска
     */
    PeriodFrom: string | null;
    /**
     * Конечный период поиска
     */
    PeriodTo: string | null;
    /**
     * Строка поиска
     */
    SearchLine: string | null;
};