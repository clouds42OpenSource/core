/**
 * Модель запроса на отмену запущенной задачи
 */
export type CancelCoreWorkerTasksQueueRequestDto = {

    /**
     * Id запущенной задачи
     */
    coreWorkerTasksQueueId: string;

    /**
     * Причина отмены задачи
     */
    reasonCancellation: string;
};