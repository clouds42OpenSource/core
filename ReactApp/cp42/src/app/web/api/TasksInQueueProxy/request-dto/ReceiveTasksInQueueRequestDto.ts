import { ReceiveTasksInQueueFilterRequestDto } from 'app/web/api/TasksInQueueProxy/request-dto/ReceiveTasksInQueueFilterRequestDto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Модель запроса для получения данных по очереди задач воркеров
 */
export type ReceiveTasksInQueueRequestDto = SelectDataCommonRequestDto<ReceiveTasksInQueueFilterRequestDto>;