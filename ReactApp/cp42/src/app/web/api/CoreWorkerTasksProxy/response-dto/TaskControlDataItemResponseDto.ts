/**
 * Модель ответа с элементом управления задачами воркера
 */
export type TaskControlDataItemResponseDto = {
    /**
     * Id задачи
     */
    id: string;
    /**
     * Название задачи
     */
    name: string;
    /**
     * Время жизни выполнения задачи в минутах
     */
    taskExecutionLifetimeInMinutes: number;
};