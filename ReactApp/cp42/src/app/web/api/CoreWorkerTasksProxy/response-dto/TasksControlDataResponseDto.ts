import { TaskControlDataItemResponseDto } from 'app/web/api/CoreWorkerTasksProxy/response-dto/TaskControlDataItemResponseDto';
import { SelectDataMetadataResponseDto } from 'app/web/common/response-dto';

/**
 * Модель ответа при получении данных по управлению задачами
 */
export type TasksControlDataResponseDto = SelectDataMetadataResponseDto<TaskControlDataItemResponseDto>;