import { TaskControlFilterRequestDto } from 'app/web/api/CoreWorkerTasksProxy/request-dto/TaskControlFilterRequestDto';
import { SelectDataCommonRequestDto } from 'app/web/common/request-dto';

/**
 * Запрос на получение данных по управлению задачами
 */
export type ReceiveTasksControlDataRequestDto = SelectDataCommonRequestDto<TaskControlFilterRequestDto>;