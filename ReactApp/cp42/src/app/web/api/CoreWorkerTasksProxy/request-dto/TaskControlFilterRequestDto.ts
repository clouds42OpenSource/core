export type TaskControlFilterRequestDto = {
    taskId: string | null;
};