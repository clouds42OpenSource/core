import { ReceiveTasksControlDataRequestDto } from 'app/web/api/CoreWorkerTasksProxy/request-dto/ReceiveTasksControlDataRequestDto';
import { TasksControlDataResponseDto } from 'app/web/api/CoreWorkerTasksProxy/response-dto/TasksControlDataResponseDto';
import { BaseProxy } from 'app/web/common';
import { KeyValueResponseDto } from 'app/web/common/response-dto/KeyValueResponseDto';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси для работы с задачами воркера
 */
export class CoreWorkerTasksProxy extends BaseProxy {
    /**
     * Выполнить запрос на данных по задачам воркера
     * @param requestKind Вид запроса
     */
    public receiveAllCoreWorkerTasksData(requestKind: RequestKind) {
        return this.requestSender.submitRequest<Array<KeyValueResponseDto<string, string>>>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks/as-key-value`,
            this.getObjectArguments(requestKind)
        );
    }

    /**
     * Выполнить запрос на получение данных по управлению задачами
     * @param requestKind Вид запроса
     * @param args параметры запроса
     */
    public receiveTasksControlData(requestKind: RequestKind, args: ReceiveTasksControlDataRequestDto) {
        return this.requestSender.submitRequest<TasksControlDataResponseDto, ReceiveTasksControlDataRequestDto>(
            WebVerb.GET,
            `${ this.host }/core-worker-tasks`,
            this.getObjectArguments(requestKind, args)
        );
    }
}