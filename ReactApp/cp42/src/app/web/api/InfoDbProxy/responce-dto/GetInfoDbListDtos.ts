import { SelectDataCommonInfoDb } from 'app/web/common/request-dto/SelectDataCommonInfoDb';
import { SelectDataResultCommonResponseDto } from 'app/web/common/response-dto';

export type InfoDbListItemDto = {
    Id: string;
    Name: string;
    TemplateCaption: string;
    TemplateImageName: string;
    SizeInMb: number;
    LastActivityDate: string;
    DatabaseLaunchLink: string;
    IsFile: boolean;
    IsDbOnDelimiters: boolean;
    IsDemo: boolean;
    HasSupport: boolean;
    HasAutoUpdate: boolean;
    HasAcDbSupport: boolean;
    State: number;
    PublishState: number;
    NeedShowWebLink: boolean;
    CreateAccountDatabaseComment: string | null;
    IsExistSessionTerminationProcess: boolean;
    WebPublishPath?: string;
    ConfigurationName: string;
};

export type GetInfoDbResultResponseDto = SelectDataResultCommonResponseDto<InfoDbListItemDto>;

/**
 * Модель параметров на запрос получения таблицы
 */
export type GetInfoDbListParamsRequestDto = SelectDataCommonInfoDb;