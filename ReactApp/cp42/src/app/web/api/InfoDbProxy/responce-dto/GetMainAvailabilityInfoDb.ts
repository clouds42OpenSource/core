import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';

export type GetMainAvailabilityInfoDb = {
    Data: GetAvailabilityInfoDb;
}