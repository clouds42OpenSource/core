export type GetAvailabilityInfoDb = {
    AccessToServerDatabaseServiceTypeId: string;
    AccountAdminInfo: string;
    AccountId: string;
    AccountLocaleName: string;
    AccountUserId: string;
    IsMainServiceAllowed: boolean;
    MyDatabaseServiceId: string;
    IsVipAccount: boolean;
    AccountAdminPhoneNumber: string;
    MainServiceStatusInfo: {
        PromisedPaymentExpireDate: string | null;
        PromisedPaymentIsActive: boolean;
        PromisedPaymentSum: number | null;
        ServiceIsLocked: boolean;
        ServiceLockReason: string | null;
        ServiceLockReasonType: number | null;
    },
    AllowAddingDatabaseInfo: {
        ForbiddeningReasonMessage: string;
        IsAllowedCreateDb: boolean;
        SurchargeForCreation: number;
    },
    PermissionsForWorkingWithDatabase: {
        HasPermissionForCreateAccountDatabase: boolean;
        HasPermissionForDisplayAutoUpdateButton: boolean;
        HasPermissionForDisplayPayments: boolean;
        HasPermissionForDisplaySupportData: boolean;
        HasPermissionForMultipleActionsWithDb: boolean;
        HasPermissionForRdp: boolean;
        HasPermissionForWeb: boolean;
    }
}