import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { GetGeneralData, GetGeneralDataLower } from 'app/web/api/InfoDbCardProxy/responce-dto/GetGeneralData';
import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import { InputIdsArr } from 'app/web/api/InfoDbProxy/request-dto/InputIdsArr';
import { GetInfoDbListParamsRequestDto, GetInfoDbResultResponseDto, InfoDbListItemDto } from 'app/web/api/InfoDbProxy/responce-dto/GetInfoDbListDtos';
import { GetMainAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetMainAvailabilityInfoDb';
import { BaseProxy } from 'app/web/common';
import { deleteDbType } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/deleteInfoDbList/input-params';
import { editDbListType } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/editInfoDbList/input-params';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с InfoDb
 */
export class InfoDbProxy extends BaseProxy {
    private readonly token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение списка баз данных
     * @param requestKind Вид запроса
     * @param args параметры для получения списка баз данных
     */
    public receiveInfoDbList(requestKind: RequestKind, args: GetInfoDbListParamsRequestDto): Promise<GetGeneralData<GetInfoDbResultResponseDto>> {
        const { accountID } = args;

        return this.requestSender.submitRequest<GetGeneralData<GetInfoDbResultResponseDto>, GetInfoDbListParamsRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/${ accountID }`,
            this.getObjectArguments(requestKind, { isNewPage: true, ...args }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение списка баз данных из других аккаунтов
     * @param requestKind Вид запроса
     * @param args параметры для получения списка баз данных
     */
    public receiveInfoDbListOther(requestKind: RequestKind, args: GetInfoDbListParamsRequestDto): Promise<GetGeneralData<GetInfoDbResultResponseDto>> {
        const { accountID } = args;
        return this.requestSender.submitRequest<GetGeneralData<GetInfoDbResultResponseDto>, GetInfoDbListParamsRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/${ accountID }`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос для удаления базы(баз)
     * @param requestKind Вид запроса
     * @param args параметры для удаления базы(баз)
     */
    public deleteInfoDbList(requestKind: RequestKind, args: deleteDbType): Promise<GetGeneralDataLower<null>> {
        return this.requestSender.submitRequest<GetGeneralDataLower<null>, deleteDbType>(
            WebVerb.DELETE,
            `${ this.host }/api_v2/AccountDatabases`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` }),
            true
        );
    }

    /**
     * Запрос на редактирование карточки базы, вкладка Общие и ползунки ТиИ и Автообновления на общей странице
     * @param requestKind Вид запроса
     * @param args параметры для редактирования карточки
     */
    public editInfoDbList(requestKind: RequestKind, args: editDbListType): Promise<GetGeneralData<null>> {
        return this.requestSender.submitRequest<GetGeneralData<null>, editDbListType>(
            WebVerb.PUT,
            `${ this.host }/api_v2/AccountDatabases/Items`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` }),
            true
        );
    }

    /**
     * Запрос на получение статуса баз
     * @param requestKind Вид запроса
     * @param args параметры для получения статуса баз
     */
    public statusInfoDbList(requestKind: RequestKind, args: InputIdsArr) {
        return this.requestSender.submitRequest<{ Data: InfoDbListItemDto[] }, NonNullable<unknown>>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/States`,
            this.getObjectArguments(requestKind, { AccountDatabasesIDs: args.ids }, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение общих данных для работы с базами
     * @param requestKind Вид запроса
     * @param args параметры для получения данных
     */
    public availabilityInfoDbList(requestKind: RequestKind, args: InputAccountId) {
        return this.requestSender.submitRequest<GetMainAvailabilityInfoDb, InputAccountId>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/Availability`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}