export type InputAccountId = {
   accountId: string;
   accountUserId: string;
}