import { KeyValueType } from 'app/web/api/InfoDbCardProxy/responce-dto/KeyValueType';

export type GetInfoDbCardAbilityResponseDto = {
   AvailableAccountDatabaseRestoreModelTypes: KeyValueType[];
   AvailableDatabaseStatuses: KeyValueType[];
   AvailableDbTemplates: KeyValueType[];
   AvailablePlatformTypes: KeyValueType[];
   ServiceExtensionsForDatabaseTypes: KeyValueType[];
}