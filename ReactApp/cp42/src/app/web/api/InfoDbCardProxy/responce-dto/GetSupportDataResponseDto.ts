import { SupportHistories } from 'app/web/api/InfoDbCardProxy/responce-dto/SupportHistories';

export type GetSupportDataResponseDto = {
  AcDbSupportHistories : SupportHistories[];
  DatabaseId: string;
  IsConnects: boolean;
  IsAuthorized: boolean;
  LastTehSupportDate: string | undefined;
  Login: string | undefined;
  Password: string | undefined;
  SupportState: number;
  SupportStateDescription: string;
  CompleteSessioin: boolean;
  TimeOfUpdate: number;
  HasAcDbSupport: boolean;
  HasAutoUpdate: boolean;
  HasSupport: boolean;
  HasModifications: boolean;
}