export type GetStatusServiceInfoDbResponseDto = {
   ExtensionDatabaseStatus: number;
   SetStatusDateTime: string;
   serviceId: string;
}