export type GetSuccessResponseDto = {
   Success: boolean;
   Message: string;
}