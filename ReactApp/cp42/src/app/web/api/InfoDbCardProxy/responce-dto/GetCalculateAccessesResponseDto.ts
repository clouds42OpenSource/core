export type GetCalculateAccessesItem = {
    AccessCost: number;
    AccountUserId: string;
    HasLicense: boolean;
}

export type GetCalculateAccessesResponseDto = GetCalculateAccessesItem[];