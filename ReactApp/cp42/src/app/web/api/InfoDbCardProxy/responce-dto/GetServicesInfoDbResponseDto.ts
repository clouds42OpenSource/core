export type GetServicesInfoDbResponseDto = {
   ExtensionLastActivityDate: string | null;
   ExtensionState: number | null;
   Id: string;
   IsInstalled: boolean;
   IsActiveService: boolean;
   IsFrozenService: boolean;
   Name: string;
   ShortDescription: string;
}