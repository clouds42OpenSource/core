export type GetBuckupsItemResponseDto = {
   BackupPath: string;
   CreationDate: string;
   EventTrigger: number;
   EventTriggerDescription: string;
   Id: string;
   Initiator: string;
   IsDbOnDelimiters: boolean;
   SourceType: number;
}

export type GetBuckupsInfoDbResponseDto = {
   DatabaseBackups: GetBuckupsItemResponseDto[];
   DbBackupsCount: number;
}