export type KeyValueType = {
   Key: number | string,
   Value: string
}