export type SupportHistories = {
   Date: string;
   Operation: string;
   Description: string;
}