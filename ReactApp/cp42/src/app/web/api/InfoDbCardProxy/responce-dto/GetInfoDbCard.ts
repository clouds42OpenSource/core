import {
    GetInfoDbCardAbilityResponseDto
} from 'app/web/api/InfoDbCardProxy/responce-dto/GetInfoDbCardAbilityResponseDto';

type DatabaseOperationsItem = {
    Cost: number;
    Currency: string;
    Id: string;
    Name: string;
};

export type GetInfoDbCard = {
    AccountCaption: string;
    ActualDatabaseBackupId: string;
    Alpha83Version: string;
    ArchivePath: string;
    AvailableFileStorages: { key: string, value: string }[];
    BackupDate: string;
    CalculateSizeDateTime: string;
    CanChangeRestoreModel: boolean;
    CanEditDatabase: boolean;
    CanWebPublish: boolean;
    Caption: string;
    CloudStorageWebLink: string;
    ConfigurationName: string;
    CreationDate: string;
    DatabaseConnectionPath: string;
    DatabaseState: number;
    DbNumber: number;
    DbTemplate: string;
    DistributionType: number;
    FilePath: string;
    FileStorageId: string;
    FileStorageName: string;
    Id: string;
    ConfigurationCode: string | null;
    IsDbOnDelimiters: boolean;
    IsDemoDelimiters: boolean;
    IsExistTerminatingSessionsProcessInDatabase: boolean;
    IsFile: boolean;
    IsStatusErrorCreated: boolean;
    CreateDatabaseComment?: string;
    LastEditedDateTime: string;
    MyDatabasesServiceTypeId: string;
    PlatformType: number;
    PublishState: number;
    RestoreModelType: number;
    SizeInMb: number;
    SqlServer: string;
    Stable82Version: string;
    Stable83Version: string;
    TemplateName: string;
    TemplatePlatform: number;
    UsedWebServices: boolean;
    V82Name: string;
    V82Server: string;
    Version: number;
    WebPublishPath: string;
    ZoneNumber: number;
    CommonDataForWorkingWithDB: GetInfoDbCardAbilityResponseDto;
    DatabaseOperations: DatabaseOperationsItem[];
};