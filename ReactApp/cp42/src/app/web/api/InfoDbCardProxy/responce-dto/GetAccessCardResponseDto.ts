export type DatabaseAccesses = {
   AccountCaption: string;
   AccountIndexNumber: number;
   AccountInfo: string;
   DelayReason: string | null;
   HasAccess: boolean;
   IsExternalAccess: boolean;
   State: number;
   UserEmail: string;
   UserFirstName: string | null;
   UserFullName: string;
   UserId: string;
   UserLastName: string | null;
   UserLogin: string;
   UserMiddleName: string | null;
}

export type RateData = {
   ServiceId: string;
   ServiceTypeId: string;
   ServiceTypeName: string;
   ServiceTypeDescription: string;
   ServiceName: string;
   BillingType: number;
   SystemServiceType: number;
   Clouds42ServiceType: number;
   IsActiveService: boolean;
   CostPerOneLicense: number;
   LimitOnFreeLicenses: number;
   VolumeInQuantity: number;
   TotalAmount: number;
}

export type GetAccessCardResponseDto = {
   Currency: string;
   ClientServerAccessCost: number;
   DatabaseAccesses: DatabaseAccesses[];
   RateData: RateData;
}