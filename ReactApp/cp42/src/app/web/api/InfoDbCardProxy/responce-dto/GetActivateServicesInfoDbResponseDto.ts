export type GetActivateServicesInfoDbResponseDto = Partial<{
   DemoPeriodEndDate?: string;
   IsAlreadyActivated?: boolean;
   ServiceName?: string;
   ErrorMessage: string;
}>