import { Nullable } from 'app/common/types';

/**
 * Модель ответа при переносе ИБ в архив или бекап
 */
export type ArchiveOrBackupsResponseDto = {
    Success: boolean;
    Message: string;
    Data: {
        ErrorMessage: string;
        IsComplete: boolean;
        NotEnoughMoney: number;
        Amount: number;
        Currency: Nullable<string>;
        CanUsePromisePayment: boolean;
    }
};