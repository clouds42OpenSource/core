export type EternalUserSuccess = {
   AccessCost?: number;
   AccountCaption?: string;
   AccountIndexNumber?: number;
   AccountInfo?: string;
   HasAccess?: boolean;
   HasLicense?: boolean;
   UserEmail?: string;
   UserFirstName?: string | null;
   UserFullName?: string;
   UserId?: string;
   UserLastName?: string | null;
   UserLogin?: string;
   UserMiddleName?: string | null;
   Message?: string;
};

export type GetEternalUserResponseDto = EternalUserSuccess;