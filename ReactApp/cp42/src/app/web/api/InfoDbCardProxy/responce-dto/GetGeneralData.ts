export type GetGeneralData<IType> = {
    Data: IType;
    Success: boolean;
    Message: string | null;
};

export type GetGeneralDataLower<IType> = {
    data: IType;
    success: boolean;
    message: string | null;
};