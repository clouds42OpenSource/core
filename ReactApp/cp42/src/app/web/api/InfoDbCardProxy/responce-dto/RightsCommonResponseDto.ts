import { RightsResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/RightsResponseDto';

export type RightsCommonResponseDto = {
    Data: RightsResponseDto;
}