export type RightsResponseDto = [
   {
      ObjectAction: string;
      HasAccess: boolean;
   }
]