export type GetTryPayAccessesResponseDto = {
    amount: number;
    canUsePromisePayment: boolean;
    currency: string;
    isComplete: boolean;
    notEnoughMoney: number;
    paymentIds: string[];
};