/**
 * Модель ответа валидации на возможность списать указанную сумму с счета при переносе в архив или отправке БД в бекап
 */
export type CheckPaymentOptionResponseDto = {
    Complete: boolean;
    ErrorMessage: string;
    NotEnoughMoney: number;
    Amount: number;
    Currency: string;
    CanUsePromisePayment: boolean;
    CanIncreasePromisePayment: boolean;
};