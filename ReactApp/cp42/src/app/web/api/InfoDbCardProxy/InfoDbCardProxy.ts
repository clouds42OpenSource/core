import { getFetchedData } from 'app/api';
import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { ArchiveOrBackupRequestDto } from 'app/web/api/InfoDbCardProxy/request-dto/ArchiveOrBackupRequestDto';
import { CheckPaymentOptionRequestDto } from 'app/web/api/InfoDbCardProxy/request-dto/CheckPaymentOptionRequestDto';
import { ClearCacheDto } from 'app/web/api/InfoDbCardProxy/request-dto/ClearCacheDto';
import { InputActivateServicesInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputActivateServicesInfoDbCard';
import { InputAuthInfoDb } from 'app/web/api/InfoDbCardProxy/request-dto/InputAuthInfoDb';
import { InputBuckupsInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputBuckupsInfoDbCard';
import { InputCalculateAccesses } from 'app/web/api/InfoDbCardProxy/request-dto/InputCalculateAccesses';
import { InputChangeServicesInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputChangeServicesInfoDbCard';
import { InputDatabaseId } from 'app/web/api/InfoDbCardProxy/request-dto/InputDatabaseId';
import { InputEditAccessUserDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputEditAccessUserDbCard';
import { InputEditPlatformInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputEditPlatformInfoDbCard';
import { InputExternalUserDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputExternalUserDbCard';
import { InputGetAccessCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputGetAccessCard';
import { InputGetServicesInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputGetServicesInfoDbCard';
import { InputInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputInfoDbCard';
import { InputPublishInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputPublishInfoDbCard';
import { InputRightsInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputRightsInfoDbCard';
import { InputStatusServiceInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputStatusServiceInfoDbCard';
import { InputTryPayAccesses } from 'app/web/api/InfoDbCardProxy/request-dto/InputTryBuyAccesses';
import { ArchiveOrBackupsResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/ArchiveOrBackupsResponseDto';
import { CheckPaymentOptionResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/CheckPaymentOptionResponseDto';
import { GetAccessCardResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetAccessCardResponseDto';
import { GetActivateServicesInfoDbResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetActivateServicesInfoDbResponseDto';
import { GetBuckupsInfoDbResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetBuckupsInfoDbResponseDto';
import { GetCalculateAccessesResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetCalculateAccessesResponseDto';
import { GetEternalUserResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetEternalUserResponseDto';
import { GetGeneralData } from 'app/web/api/InfoDbCardProxy/responce-dto/GetGeneralData';
import { GetInfoDbCard } from 'app/web/api/InfoDbCardProxy/responce-dto/GetInfoDbCard';
import { GetServicesInfoDbResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetServicesInfoDbResponseDto';
import { GetStatusServiceInfoDbResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetStatusServiceInfoDbResponseDto';
import { GetSuccessResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetSuccessResponseDto';
import { GetSupportDataResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetSupportDataResponseDto';
import { GetTryPayAccessesResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetTryPayAccessesResponseDto';
import { RightsCommonResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/RightsCommonResponseDto';
import { BaseProxy } from 'app/web/common';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

/**
 * Прокси работающий с карточкой базы в Управление аккаунтом
 */
export class InfoDbCardProxy extends BaseProxy {
    private accountId = localStorageHelper.getContextAccountId();

    private token = localStorageHelper.getJWTToken();

    /**
     * Запрос на получение карточки базы
     * @param requestKind Вид запроса
     * @param args параметры для получения карточки базы
     */
    public getInfoDbCardList(requestKind: RequestKind, args: InputInfoDbCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetInfoDbCard>, InputInfoDbCard>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/Items`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на получение пермишенов базы
     * @param requestKind Вид запроса
     * @param args параметры для получения пермишенов базы
     */
    public getRightsInfoDbCard(requestKind: RequestKind, args: InputRightsInfoDbCard): Promise<RightsCommonResponseDto> {
        return this.requestSender.submitRequest<RightsCommonResponseDto, NonNullable<unknown>>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/Rights?databaseId=${ args.databaseId }&objectActions=${ args.objectActions }`,
            this.getObjectArguments(requestKind, {}, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на редактирование платформы базы и релиза платформы
     * @param requestKind Вид запроса
     * @param args параметры для редактирования платформы базы и релиза платформы
     */
    public editPlatformInfoDbCard(requestKind: RequestKind, args: InputEditPlatformInfoDbCard) {
        return this.requestSender.submitRequest<void, InputEditPlatformInfoDbCard>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/Release`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос публикацию базы
     * @param requestKind Вид запроса
     * @param args параметры публикации базы
     */
    public publishInfoDbCard(requestKind: RequestKind, args: InputPublishInfoDbCard) {
        return this.requestSender.submitRequest<GetSuccessResponseDto, InputPublishInfoDbCard>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/Publish`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос для завершения активных сеансов базы
     * @param requestKind Вид запроса
     * @param args параметры для завершения активных сеансов базы
     */
    public sessionInfoDbCard(requestKind: RequestKind, args: InputDatabaseId) {
        return this.requestSender.submitRequest<GetSuccessResponseDto, InputDatabaseId>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/Session`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос для изменения типа базы(файловая, серверная)
     * @param requestKind Вид запроса
     * @param args параметры для  изменения типа базы
     */
    public typeInfoDbCard(requestKind: RequestKind, args: InputDatabaseId) {
        return this.requestSender.submitRequest<GetSuccessResponseDto, InputDatabaseId>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/Type`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос для очистки кеша
     * @param requestKind Вид запроса
     * @param args параметры для очистки кеша
     */
    public clearCache(requestKind: RequestKind, args: ClearCacheDto) {
        return this.requestSender.submitRequest<GetSuccessResponseDto, ClearCacheDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/ClearCache`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос для перезапуска пула IIS базы
     * @param requestKind Вид запроса
     * @param args параметры для перезапуска пула IIS базы
     */
    public restartIISPollInfoDbCard(requestKind: RequestKind, args: InputDatabaseId) {
        return this.requestSender.submitRequest<GetSuccessResponseDto, InputDatabaseId>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/IIS-pool`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
    /**
     * Запрос для получения информации о поддержке(Авторизация)
     * @param requestKind Вид запроса
     * @param args параметры для получения информации о поддержке
     */

    public getSupportDataCard(requestKind: RequestKind, args: InputDatabaseId) {
        return this.requestSender.submitRequest<GetGeneralData<GetSupportDataResponseDto>, InputDatabaseId>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/SupportData`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public AuthInfoDbCard(requestKind: RequestKind, args: InputAuthInfoDb) {
        return this.requestSender.submitRequest<GetGeneralData<GetSupportDataResponseDto>, InputAuthInfoDb>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/SupportData`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public DeauthInfoDbCard(requestKind: RequestKind, args: { databaseId: string; }) {
        return this.requestSender.submitRequest<GetGeneralData<GetSupportDataResponseDto>, { databaseId: string; }>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/Deauthorization`,
            this.getObjectArguments(requestKind, args, 'Application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public GetAccessInfoDbCard(requestKind: RequestKind, args: InputGetAccessCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetAccessCardResponseDto>, InputGetAccessCard>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/Access`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public GetExternalUserInfoDbCard(requestKind: RequestKind, args: InputExternalUserDbCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetEternalUserResponseDto>, InputExternalUserDbCard>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/Access/External-user`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public EditAccessUserInfoDbCard(requestKind: RequestKind, args: InputEditAccessUserDbCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetSuccessResponseDto[]>, InputEditAccessUserDbCard>(
            WebVerb.PUT,

            `${ this.host }/api_v2/AccountDatabases/Access`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public GetServicesInfoDbCard(requestKind: RequestKind, args: InputGetServicesInfoDbCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetServicesInfoDbResponseDto[]>, InputGetServicesInfoDbCard>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/Services`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public ActivateServicesInfoDbCard(requestKind: RequestKind, args: InputActivateServicesInfoDbCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetActivateServicesInfoDbResponseDto>, InputActivateServicesInfoDbCard>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/Services/Demo`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public ChangeConnectServicesInfoDbCard(requestKind: RequestKind, args: InputChangeServicesInfoDbCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetActivateServicesInfoDbResponseDto>, InputChangeServicesInfoDbCard>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/Services`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public GetStatusServicesInfoDbCard(requestKind: RequestKind, args: InputStatusServiceInfoDbCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetStatusServiceInfoDbResponseDto>, InputStatusServiceInfoDbCard>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/Services/Status`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public GetBackupsInfoDbCard(requestKind: RequestKind, args: InputBuckupsInfoDbCard) {
        return this.requestSender.submitRequest<GetGeneralData<GetBuckupsInfoDbResponseDto>, InputBuckupsInfoDbCard>(
            WebVerb.GET,
            `${ this.host }/api_v2/AccountDatabases/Backups`,
            this.getObjectArguments(requestKind, args, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public CalculateAccesses(requestKind: RequestKind, args: InputCalculateAccesses) {
        return this.requestSender.submitRequest<GetGeneralData<GetCalculateAccessesResponseDto>, InputCalculateAccesses>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/Access/Access-price`,
            this.getObjectArguments(requestKind, { ...args, AccountId: this.accountId }, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    public async TryPayAccesses(requestKind: RequestKind, args: InputTryPayAccesses) {
        const { responseBody } = await getFetchedData<GetTryPayAccessesResponseDto, InputTryPayAccesses>({
            url: `${ this.host }/billing-services/apply`,
            method: WebVerb.PUT,
            body: args
        });

        return responseBody;
    }

    /**
     * Запрос на валидацию возможности списать средства при переносе ИБ в бекап или архив
     * @param requestKind тип запроса
     * @param args параметры для получения инофрмации о валидации
     */
    public CheckPaymentOption(requestKind: RequestKind, args: CheckPaymentOptionRequestDto) {
        return this.requestSender.submitRequest<CheckPaymentOptionResponseDto, CheckPaymentOptionRequestDto>(
            WebVerb.GET,
            `${ this.host }/api_v2/Payment/CheckPaymentOption`,
            this.getObjectArguments(requestKind, { AccountId: this.accountId, ...args }, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }

    /**
     * Запрос на отправку ИБ в архив или бекап
     * @param requestKind тип запроса
     * @param args параметры для получения информации о переноси в архив или бекап
     */
    public ArchiveOrBackups(requestKind: RequestKind, args: ArchiveOrBackupRequestDto) {
        return this.requestSender.submitRequest<ArchiveOrBackupsResponseDto, ArchiveOrBackupRequestDto>(
            WebVerb.POST,
            `${ this.host }/api_v2/AccountDatabases/ArchiveOrBackup`,
            this.getObjectArguments(requestKind, { AccountId: this.accountId, ...args }, 'application/json', { Authorization: `Bearer ${ this.token }` })
        );
    }
}