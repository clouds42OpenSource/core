/**
 * Модель данных для получения информации о валидации на возможность списать указанную сумму при архивации или отправки бд в бекап
 */
export type CheckPaymentOptionRequestDto = {
    cost: number;
    currency: string;
};