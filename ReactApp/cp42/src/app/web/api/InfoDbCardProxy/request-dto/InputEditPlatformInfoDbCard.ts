export type InputEditPlatformInfoDbCard = {
   databaseId: string;
   platformType?: number;
   distributionType?: number;
}