/**
 * Модель данных для отправки ИБ в архив или бекап
 */
export type ArchiveOrBackupRequestDto = {
    databaseId: string;
    serviceId?: string;
    operation: number;
    usePromisePayment?: boolean;
    cost?: number;
    currency?: string;
};