export type InputRightsInfoDbCard = {
   databaseId: string;
   objectActions: string;
}