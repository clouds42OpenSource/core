export type InputChangeServicesInfoDbCard = {
   DatabaseId: string;
   ServiceId: string;
   Install: boolean;
}