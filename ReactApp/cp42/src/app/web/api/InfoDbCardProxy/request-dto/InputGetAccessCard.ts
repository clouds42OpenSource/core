export type InputGetAccessCard = {
   DatabaseId: string;
   SearchString?: string;
   UsersByAccessFilterType?: number;
}