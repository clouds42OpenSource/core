export type InputExternalUserDbCard = {
   databaseId: string;
   accountUserEmail: string;
}