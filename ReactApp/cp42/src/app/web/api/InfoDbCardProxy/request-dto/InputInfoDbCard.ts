export type InputInfoDbCard = {
   accountId: string;
   databaseId: string;
};