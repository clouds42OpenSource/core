export type InputDatabaseId = {
   databaseId: string;
   adminPhoneNumber?: string;
   databaseAdminLogin?: string;
   databaseAdminPassword?: string;
}