import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

export type InputTryPayAccessesItem = {
   billingServiceTypeId: string;
   sponsorship: {
      i: boolean;
      label: string;
      me: boolean;
   }
   status: boolean;
   subject: string;
};

export type InputTryPayAccesses = IForceThunkParam & {
   accountId: string;
   billingServiceId: string;
   changedUsers: InputTryPayAccessesItem[];
   usePromisePayment: boolean;
};