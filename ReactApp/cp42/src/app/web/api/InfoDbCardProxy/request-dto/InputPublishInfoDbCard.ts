export type InputPublishInfoDbCard = {
   databaseId: string;
   publish: boolean;
}