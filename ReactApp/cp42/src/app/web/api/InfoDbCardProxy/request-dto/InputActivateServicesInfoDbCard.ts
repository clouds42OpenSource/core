export type InputActivateServicesInfoDbCard = {
   serviceId: string;
   accountDatabaseId: string;
   accountId: string;
}