export type InputEditAccessUserDbCard = {
   DatabaseId: string;
   UsersId: string[];
   giveAccess: boolean;
}