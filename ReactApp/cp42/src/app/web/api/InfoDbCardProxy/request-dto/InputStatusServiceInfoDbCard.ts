export type InputStatusServiceInfoDbCard = {
   serviceId: string;
   accountDatabaseId: string;
}