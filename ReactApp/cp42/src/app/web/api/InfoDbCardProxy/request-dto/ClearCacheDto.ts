export type ClearCacheDto = {
    AccountDatabaseId: string;
    ClearDatabaseLogs?: boolean;
}