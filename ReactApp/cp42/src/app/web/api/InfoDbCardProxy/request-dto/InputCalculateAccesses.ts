export type serviceTypesIdsList = {
    serviceTypesIdsList: (string | null)[];
};

export type AccountUserDataModelsList = {
    UserId: string;
    HasAccess: boolean;
    State?: number;
    HasLicense?: boolean;
}

export type InputBuckupsInfoDbCardItem = {
    accountUserDataModelsList: AccountUserDataModelsList[];
};

export type InputCalculateAccesses = InputBuckupsInfoDbCardItem & serviceTypesIdsList;