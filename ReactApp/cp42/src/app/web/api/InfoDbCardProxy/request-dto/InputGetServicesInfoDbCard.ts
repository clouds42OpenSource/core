export type InputGetServicesInfoDbCard = {
   AccountId: string;
   ServiceName: string;
   ServiceExtensionsForDatabaseType: number;
   AccountDatabaseId: string;
}