export type InputBuckupsInfoDbCard = {
   DatabaseId: string;
   CreationDateFrom: string | Date;
   CreationDateTo: string | Date;
}