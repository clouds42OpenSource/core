export type InputAuthInfoDb = {
   databaseId: string;
   login: string;
   password: string;
   timeOfUpdate: number;
   completeSessioin: boolean;
   hasSupport: boolean;
   hasAutoUpdate: boolean;
}