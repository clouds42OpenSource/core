import { localStorageHelper } from 'app/common/helpers/LocalStorageHelper';
import { AjaxSenderBase } from 'core/requestSender/AjaxSenderBase';
import { getLogout } from 'app/api';

/**
 * Класс для отправки AJAX запросов
 */
export class AjaxSender extends AjaxSenderBase {
    /**
     * Возвращает тип токена для аутентификации
     */
    public getAuthenticationTokenType() {
        return (
            window.sessionStorage.getItem(AjaxSenderBase.AuthenticationTypeKey) ||
            window.localStorage.getItem(AjaxSenderBase.AuthenticationTypeKey)
        );
    }

    /**
     * Возвращает токен аутентификации
     */
    public getAuthenticationToken() {
        return (
            window.sessionStorage.getItem(AjaxSenderBase.AuthenticationTokenKey) ||
            window.localStorage.getItem(AjaxSenderBase.AuthenticationTokenKey) ||
            localStorageHelper.getJWTToken()
        );
    }

    /**
     * Пытается найти ошибку в ответе на запрос и вернуть Promise, который всегда rejected
     * @param response Ответ на отправленный запрос
     * @template TResult Тип данных, полученных при вызове метода API
     * @returns Если ошибка в ответе на запрос найдена, то возвращается Promise.reject(Error), иначе null
     */
    protected GetRejectedPromiseFromResponse<TResult>(response: Response): Promise<TResult> | null {
        if (response.redirected) {
            const redirectUrl = new URL(response.url);
            const { searchParams } = redirectUrl;

            if (searchParams.has('returnUrl')) {
                searchParams.set('returnUrl', window.location.href.replace(window.location.origin, ''));
            }

            redirectUrl.search = searchParams.toString();
            window.location.href = redirectUrl.toString();
        }

        if (response.status === 401) {
            getLogout();

            return null;
        }

        if (response.status === 500) {
            return Promise.reject(
                new Error('Произошла ошибка, обратитесь пожалуйста в тех-поддержку.')
            );
        }

        return null;
    }
}