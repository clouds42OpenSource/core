export class ValidationError extends Error {
    /**
     * Ошибки валидации
     */

    public errors: {
        /**
         * key - название поля
         * value - массив ошибок
         */
        [property: string]: string[]
    };

    /**
     * Error for common usage
     * @param message - Текст ошибки
     * @param errors - Ошибки валидации
     * key - название поля
     * value - массив ошибок
     */
    constructor(
        message: string,
        errors: { [property: string]: string[] }
    ) {
        super(message);
        this.errors = errors;
    }
}