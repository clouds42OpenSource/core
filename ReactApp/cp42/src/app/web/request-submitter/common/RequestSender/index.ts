import { WebBaseResponseDto, WebErrorResponseDto, WebSuccessResponseDto } from 'app/web/request-submitter/response-dto';

import { ValidationError } from 'app/web/request-submitter/common/RequestSender/ValidationError';
import { WebVerb } from 'core/requestSender/enums';
import { RequestSenderBase } from 'core/requestSender/RequestSenderBase';

/**
 * Класс для отправки API запросов
 */
export class RequestSender extends RequestSenderBase<WebBaseResponseDto> {
    /**
     * Метод обработки ответа
     * @param result Полученный ответ при вызове метода API
     * @param method
     * @param requestUrl
     * @template TResult Тип данных, полученных при вызове метода API
     * - При успешном выполнении вызванного метода API возвращается Promise с полученным результатом типа TResult
     * - При неуспешном выполнении вызванного метода API возвращается Promise с объектом Error
     */
    protected handleResponse<TResult>(result: WebBaseResponseDto, method: WebVerb, requestUrl: string): Promise<TResult> {
        if (!result?.success && !(result as WebSuccessResponseDto<TResult>).data) {
            return Promise.resolve<TResult>((result as unknown as TResult));
        }
        if (result?.success ?? true) {
            return Promise.resolve<TResult>((result as WebSuccessResponseDto<TResult>)?.data);
        }

        const responseError = result as WebErrorResponseDto;
        if (responseError?.validationState && responseError.validationState.length) {
            const validationErrors: { [property: string]: string[] } = {};

            responseError.validationState.forEach(item => {
                validationErrors[item.property] = item.errors;
            });

            const error = new ValidationError(
                `Запрос "[${ method }]:${ requestUrl }" завершился с ошибкой валидации модели запроса.`,
                validationErrors
            );
            (console.error ?? console.log)([error, validationErrors]);
            return Promise.reject<TResult>(error);
        }

        const errorMessage = responseError?.errorMessage ?? responseError?.ErrorMessage ?? responseError?.message ?? responseError?.data ?? 'An unknown error has occurred.';

        const fullErrorText = responseError.errors
            ? `${ errorMessage }\n\n${ responseError?.errors?.join('\n') ?? '' }`
            : errorMessage;

        (console.error ?? console.log)(`[${ method }]:${ requestUrl }\n${ fullErrorText }`);
        return Promise.reject<TResult>(new Error(fullErrorText));
    }
}