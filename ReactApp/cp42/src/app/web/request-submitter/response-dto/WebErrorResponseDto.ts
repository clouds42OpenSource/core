import { WebBaseResponseDto } from 'app/web/request-submitter/response-dto';

/**
 * Модель неуспешного ответа при вызове метода API
 */
export type WebErrorResponseDto = WebBaseResponseDto & {
    /**
     * Содержит текст ошибки
     */
    readonly errorMessage: string;
    /**
     * Содержит текст ошибки
     */
    readonly ErrorMessage: string;
    /**
     * Содержит текст ошибки
     */
    readonly message: string;
    /**
     * Содержит текст ошибки
     */
    readonly data: string;
    /**
     * содержит код ошибки
     */
    readonly errorCode: string;
    readonly validationState: {
        property: string;
        errors: string[]
    }[];
    /**
     * Содержит массив дополнительных  ошибок
     */
    readonly errors: string[];
};