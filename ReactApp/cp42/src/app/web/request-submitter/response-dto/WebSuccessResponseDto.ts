import { WebBaseResponseDto } from 'app/web/request-submitter/response-dto';

/**
 * Модель успешного ответа при вызове метода API
 * @template TResult Тип данных, полученных при вызове метода API
 */
export type WebSuccessResponseDto<TResult> = WebBaseResponseDto & {
    /**
     * Содержит данные, полученные при вызове метода API
     */
    readonly data: TResult;
};