/**
 * Базовая модель ответа на запрос
 */
export type WebBaseResponseDto = {
  /**
   * Если содержит true, то вызываемый метод отработал успешно, иначе false
   */
  readonly success: boolean;
};