import { IoCContainer } from 'app/IoCСontainer';
import { CloudCoreConfigurationDataModel } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/data-models';
import { EditDefaultSegmentParams, EditDefUkAndMscParams, EditPageNotificationParams, EditRussianLocaleAggregatorParams } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/input-params';
import { cloudCoreConfigurationMapFrom } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/mappings';
import { RequestKind } from 'core/requestSender/enums';

export const CloudCoreApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getCloudCoreApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getCloudCoreProxy();
    }

    /**
     * Получить дефолтные конфигурации
     * @param requestKind тип запроса
     */
    async function receiveCloudCoreConfigurationData(requestKind: RequestKind): Promise<CloudCoreConfigurationDataModel> {
        return getCloudCoreApiProxy().getDefultConfiguration(requestKind).then(responseDto => {
            return cloudCoreConfigurationMapFrom(responseDto);
        });
    }

    /**
     * Редактировать разницу между Украиной и Москвой
     * @param requestKind тип запроса
     * @param args разница часов
     */
    async function editHoursDifUkAndMsc(requestKind: RequestKind, args: EditDefUkAndMscParams): Promise<boolean> {
        return getCloudCoreApiProxy().editHoursDifUkAndMsc(requestKind, {
            hours: args.hours
        }).then(response => {
            return response;
        });
    }

    /**
     * Редактировать сегмент по умолчания
     * @param requestKind тип запроса
     * @param args айдишка дефолтного сегмента
     */
    async function editDefaultSegment(requestKind: RequestKind, args: EditDefaultSegmentParams): Promise<boolean> {
        return getCloudCoreApiProxy().editDefaultSegment(requestKind, {
            defSegmentId: args.defSegmentId
        }).then(response => {
            return response;
        });
    }

    /**
     * Редактировать страницу уведомления
     * @param requestKind тип запроса
     * @param args отредактированное поле для страницы уведомления
     */
    async function editMainPageNotification(requestKind: RequestKind, args: EditPageNotificationParams): Promise<boolean> {
        return getCloudCoreApiProxy().editMainPageNotification(requestKind, {
            pageNotification: args.pageNotification
        }).then(response => {
            return response;
        });
    }

    /**
     * Редактировать переключатель аггрегатора между Юкассой и Робокассой
     * @param requestKind тип запроса
     * @param args тип аггрегатора
     */
    async function editRussianLocaleAggregator(requestKind: RequestKind, args: EditRussianLocaleAggregatorParams): Promise<boolean> {
        return getCloudCoreApiProxy().editRussianLocaleAggregator(requestKind, {
            aggregator: args.aggregator
        }).then(response => {
            return response;
        });
    }

    return {
        receiveCloudCoreConfigurationData,
        editHoursDifUkAndMsc,
        editDefaultSegment,
        editMainPageNotification,
        editRussianLocaleAggregator
    };
})();