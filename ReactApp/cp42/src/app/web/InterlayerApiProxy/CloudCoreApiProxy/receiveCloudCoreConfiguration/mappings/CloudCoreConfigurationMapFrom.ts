import { ReceiveCloudCoreConfigResponseDto } from 'app/web/api/CloudCoreProxy/response-dto';
import { CloudCoreConfigurationDataModel } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy/receiveCloudCoreConfiguration/data-models';

/**
 * Выполнить маппинг модели данных по дефолтной конфигурации, полученной при запросе, в модель приложения
 * @param value Модель ответа с дефолтными конфигами
 */
export function cloudCoreConfigurationMapFrom(value: ReceiveCloudCoreConfigResponseDto): CloudCoreConfigurationDataModel {
    return {
        hourseDifferenceOfUkraineAndMoscow: value.hoursDifferenceOfUkraineAndMoscow,
        defaultSegment: value.defaultSegment,
        segments: value.segments,
        mainPageNotification: value.mainPageNotification,
        russianLocalePaymentAggregator: value.russianLocalePaymentAggregator,
        bannerUrl: value.bannerUrl
    };
}