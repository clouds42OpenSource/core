/**
 * Параметр для редактривания дефолтного сегмента
 */
export type EditDefaultSegmentParams = {
    /**
     * ID дефолтного сегмента
     */
    defSegmentId: string;
}