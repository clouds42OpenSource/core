/**
 * Моделька для оправки типа аггрегатора для переключения
 */
export type EditRussianLocaleAggregatorParams = {
    /**
     * тип аггрегатора
     */
    aggregator: number;
}