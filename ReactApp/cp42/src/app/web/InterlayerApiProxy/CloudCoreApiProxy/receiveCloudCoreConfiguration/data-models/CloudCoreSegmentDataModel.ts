/**
 * Модель дефолтного сегмента
 */
export type CloudCoreSegmentDataModel = {
    /**
     * Имя дефолтного сегмента
     */
    defaultSegmentName: string;
    /**
     * ID дефолтного сегмента
     */
    segmentId: string;
}