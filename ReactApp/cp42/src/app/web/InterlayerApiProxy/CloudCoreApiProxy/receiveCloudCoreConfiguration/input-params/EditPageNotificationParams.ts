/**
 * Параметр для редактировани контента уведомления главной страницы
 */
export type EditPageNotificationParams = {
    /**
     * Контент главной страницы
     */
    pageNotification: string;
}