import { CloudCoreSegmentDataModel } from './CloudCoreSegmentDataModel';

/**
 * Модель для получения дефолтных конфигураций для Настройки Облака
 */
export type CloudCoreConfigurationDataModel = {
    /**
     * Разница часов между Украиной и Москвой
     */
    hourseDifferenceOfUkraineAndMoscow: number;
    /**
     * Обьект дефолтного сегмента
     */
    defaultSegment: CloudCoreSegmentDataModel;
    /**
     * Коллекция сегментов
     */
    segments: Array<CloudCoreSegmentDataModel>;
    /**
     * Уведомление главной страницы
     */
    mainPageNotification: string;
    /**
     * Тип платежей Российкого аггрегатора
     */
    russianLocalePaymentAggregator: number;
    bannerUrl: string | null;
};