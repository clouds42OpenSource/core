import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { CommonCreateSegmentParams, CommonSegmentDataModel, CommonSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Фермы ТС"
 * @returns Обьект функций для работы с апи прокси таба "Фермы ТС"
 */
export function getTabTerminalFarmsApiProxyMethods() {
    function getTabTerminalFarmsApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().TerminalFarmsTabProxy;
    }

    /**
     * Получить данные по фермам ТС
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных с пагинацией по фермам ТС
     */
    async function getTerminalFarms(requestKind: RequestKind, args: CommonSegmentFilterParams): Promise<SelectDataResultMetadataModel<CommonSegmentDataModel>> {
        return getTabTerminalFarmsApiProxy().getTerminalFarms(requestKind, args);
    }

    /**
     * Создать терминальную ферму тс
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createTerminalFarm(requestKind: RequestKind, args: CommonCreateSegmentParams): Promise<void> {
        return getTabTerminalFarmsApiProxy().createTerminalFarm(requestKind, {
            name: args.name,
            description: args.description,
            connectionAddress: args.connectionAddress,
            usingOutdatedWindows: args.usingOutdatedWindows,
        });
    }

    /**
     * Удалить терминальную ферму тс по ID
     * @param requestKind Вид запроса
     * @param terminalFarmId ID терминальной фермы
     */
    async function deleteTerminalFarm(requestKind: RequestKind, terminalFarmId: string): Promise<TResponseLowerCase<void>> {
        return getTabTerminalFarmsApiProxy().deleteTerminalFarm(requestKind, terminalFarmId);
    }

    /**
     * Получить терминальную ферму тс по ID
     * @param requestKind Вид запроса
     * @param terminalFarmId ID терминальной фермы
     */
    async function getTerminalFarm(requestKind: RequestKind, terminalFarmId: string): Promise<CommonSegmentDataModel> {
        return getTabTerminalFarmsApiProxy().getTerminalFarm(requestKind, terminalFarmId);
    }

    /**
     * Редактировать терминальную ферму тс
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editTerminalFarm(requestKind: RequestKind, args: CommonSegmentDataModel): Promise<void> {
        return getTabTerminalFarmsApiProxy().editTerminalFarm(requestKind, args);
    }

    return {
        getTerminalFarms,
        createTerminalFarm,
        deleteTerminalFarm,
        getTerminalFarm,
        editTerminalFarm
    };
}