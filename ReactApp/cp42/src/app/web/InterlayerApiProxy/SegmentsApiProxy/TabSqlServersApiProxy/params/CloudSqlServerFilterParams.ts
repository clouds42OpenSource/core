import { FilterBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { CreateCloudSqlServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy/params';

/**
 * Фильтр поиска SQL серверов
 */
export type CloudSqlServerFilterParams = FilterBaseParams & {
    filter: CreateCloudSqlServerParams;
};