import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy/data-models';
import { CloudSqlServerFilterParams, CreateCloudSqlServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy/params';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "SQL"
 * @returns Обьект функций для работы с апи прокси таба "SQL"
 */
export function getSqlServersTabApiProxyMethods() {
    function getSqlServersTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().SqlServersTabProxy;
    }

    /**
     * Получить данные sql сервера
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по sql серверам
     */
    async function getCloudSqlServers(requestKind: RequestKind, args: CloudSqlServerFilterParams): Promise<SelectDataMetadataResponseDto<CloudSqlServerDataModel>> {
        return getSqlServersTabApiProxy().getCloudSqlServers(requestKind, args);
    }

    /**
     * Удалить sql сервер
     * @param requestKind Вид запроса
     * @param sqlServerId  ID sql сервера
     */
    async function deleteCloudSqlServer(requestKind: RequestKind, sqlServerId: string): Promise<TResponseLowerCase<void>> {
        return getSqlServersTabApiProxy().deleteCloudSqlServer(requestKind, sqlServerId);
    }

    /**
     * Получить sql сервер
     * @param requestKind Вид запроса
     * @param sqlServerId  ID sql сервера
     * @returns Модель sql сервера
     */
    async function getCloudSqlServer(requestKind: RequestKind, sqlServerId: string): Promise<CloudSqlServerDataModel> {
        return getSqlServersTabApiProxy().getCloudSqlServer(requestKind, sqlServerId);
    }

    /**
     * Создать новый sql сервер
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createCloudSqlServer(requestKind: RequestKind, args: CreateCloudSqlServerParams): Promise<void> {
        return getSqlServersTabApiProxy().createCloudSqlServer(requestKind, args);
    }

    /**
     * Редактировать sql сервер
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editCloudSqlServer(requestKind: RequestKind, args: CloudSqlServerDataModel): Promise<void> {
        return getSqlServersTabApiProxy().editCloudSqlServer(requestKind, args);
    }

    return {
        getCloudSqlServers,
        deleteCloudSqlServer,
        getCloudSqlServer,
        createCloudSqlServer,
        editCloudSqlServer
    };
}