import { AccountDatabaseRestoreModelType } from 'app/common/enums';
import { SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель создания Sql сервера
 */
export type CreateCloudSqlServerParams = SearchBaseParams & {
    /**
     * Тип модели восстановления
     */
    restoreModelType?: AccountDatabaseRestoreModelType;
}