import { CreateCloudSqlServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy/params';

/**
 * Модель Sql сервера
 */
export type CloudSqlServerDataModel = CreateCloudSqlServerParams & {
    /**
     * Id записи
     */
    id: string;
}