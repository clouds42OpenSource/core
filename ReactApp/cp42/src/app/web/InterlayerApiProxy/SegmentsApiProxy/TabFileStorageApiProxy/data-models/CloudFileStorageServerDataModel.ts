import { CreateCloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy/data-models/CreateCloudFileStorageServerDataModel';

/**
 * Модель файлового хранилища
 */
export type CloudFileStorageServerDataModel = CreateCloudFileStorageServerDataModel & {
    /**
     * Id записи
     */
    id: string;
}