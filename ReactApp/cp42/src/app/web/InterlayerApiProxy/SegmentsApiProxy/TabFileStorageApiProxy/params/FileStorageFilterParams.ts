import { FilterBaseParams, SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель фильтра файлового хранилища
 */
export type FileStorageFilterParams = FilterBaseParams & {
    filter: SearchBaseParams;
};