import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudFileStorageServerDataModel, CreateCloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy/data-models';
import { FileStorageFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy/params';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Файл хранилища"
 * @returns Обьект функций для работы с апи прокси таба "Файл хранилища"
 */
export function getFileStorageTabApiProxyMethods() {
    function getFileStorageTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().FileStorageTabProxy;
    }

    /**
     * Получить данные файловых хранилищ
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по файловым хранилищам
     */
    async function getCloudFileStorageServers(requestKind: RequestKind, args: FileStorageFilterParams): Promise<SelectDataMetadataResponseDto<CloudFileStorageServerDataModel>> {
        return getFileStorageTabApiProxy().getCloudFileStorageServers(requestKind, args);
    }

    /**
     * Удалить файл хранилища
     * @param requestKind Вид запроса
     * @param fileStorageId ID файла хранилища
     */
    async function deleteCloudFileStorageServer(requestKind: RequestKind, fileStorageId: string): Promise<TResponseLowerCase<void>> {
        return getFileStorageTabApiProxy().deleteCloudFileStorageServer(requestKind, fileStorageId);
    }

    /**
     * Получить файл хранилища
     * @param requestKind Вид запроса
     * @param fileStorageId ID файла хранилища
     * @returns Модель файла хранилища
     */
    async function getCloudFileStorageServer(requestKind: RequestKind, fileStorageId: string): Promise<CloudFileStorageServerDataModel> {
        return getFileStorageTabApiProxy().getCloudFileStorageServer(requestKind, fileStorageId);
    }

    /**
     * Создать новый файл хранилища
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createCloudFileStorageServer(requestKind: RequestKind, args: CreateCloudFileStorageServerDataModel): Promise<void> {
        return getFileStorageTabApiProxy().createCloudFileStorageServer(requestKind, args);
    }

    /**
     * Редактировать файл хранилища
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editCloudFileStorageServer(requestKind: RequestKind, args: CloudFileStorageServerDataModel): Promise<void> {
        return getFileStorageTabApiProxy().editCloudFileStorageServer(requestKind, args);
    }

    return {
        getCloudFileStorageServers,
        deleteCloudFileStorageServer,
        getCloudFileStorageServer,
        createCloudFileStorageServer,
        editCloudFileStorageServer
    };
}