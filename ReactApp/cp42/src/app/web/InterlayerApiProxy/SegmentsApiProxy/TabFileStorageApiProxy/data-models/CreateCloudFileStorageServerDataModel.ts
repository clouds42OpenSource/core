import { SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель создания файлового хранилища
 */
export type CreateCloudFileStorageServerDataModel = SearchBaseParams & {
    /**
     * DNS имя хранилища
     */
    dnsName: string;
}