import { FilterBaseParams, SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Фильтр поиска серверов 1С:Предприятие
 */
export type EnterpriseServerFilterParams = FilterBaseParams & {
    /**
     * Версия платформы
     */
    filter: SearchBaseParams & {
        version: string;
    }
}