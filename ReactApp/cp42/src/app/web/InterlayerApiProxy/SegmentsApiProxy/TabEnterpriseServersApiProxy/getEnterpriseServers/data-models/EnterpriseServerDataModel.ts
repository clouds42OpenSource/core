import { PlatformEnumType } from 'app/common/enums';

/**
 * Сервер 1С:Предприятие
 */
export type EnterpriseServerDataModel = {
    /**
     * Идентификатор сервера
     */
    id: string;
    /**
     * Адрес подключения
     */
    connectionAddress: string;
    clusterSettingsPath: string;
    /**
     * Название
     */
    name: string;
    /**
     * Описание
     */
    description: string;
    /**
     * Версия платформы
     */
    versionEnum: PlatformEnumType;
    /**
     * Версия платформы
     */
    version: string;
    /**
     * Логин администратора
     */
    adminName: string;
    /**
     * Пароль администратора
     */
    adminPassword: string;
}