import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { EnterpriseServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { EnterpriseServerDataModel, EnterpriseServerFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy/getEnterpriseServers';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Сервера 1С"
 * @returns Обьект функций для работы с апи прокси таба "Сервера 1С"
 */
export function getTabEnterpriseServersApiProxyMethods() {
    function getTabEnterpriseServersApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().EnterpriseServersTabProxy;
    }

    /**
     * Получить данные по серверам 1С:Предприятие
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по серверам 1С:Предприятие
     */
    async function getEnterpriseServers(requestKind: RequestKind, args: EnterpriseServerFilterParams): Promise<SelectDataMetadataResponseDto<EnterpriseServerDataModel>> {
        return getTabEnterpriseServersApiProxy().getEnterpriseServers(requestKind, args);
    }

    /**
     * Создать новый сервер 1С:Предприятие
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createEnterpriseServer(requestKind: RequestKind, args: EnterpriseServerParams): Promise<void> {
        const { ...rest } = args;
        return getTabEnterpriseServersApiProxy().createEnterpriseServer(requestKind, rest);
    }

    /**
     * Получить сервер 1С:Предприятие по ID
     * @param requestKind Вид запроса
     * @param enterpriseServerId  ID сервера 1С:Предприятие
     * @returns Модель сервера 1С:Предприятие
     */
    async function getEnterpriseServer(requestKind: RequestKind, enterpriseServerId: string): Promise<EnterpriseServerDataModel> {
        return getTabEnterpriseServersApiProxy().getEnterpriseServer(requestKind, enterpriseServerId);
    }

    /**
     * Редактировать сервер 1С:Предприятие
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editEnterpriseServer(requestKind: RequestKind, args: EnterpriseServerParams): Promise<void> {
        return getTabEnterpriseServersApiProxy().editEnterpriseServer(requestKind, args);
    }

    /**
     * Удалить сервер 1С:Предприятие по ID
     * @param requestKind Вид запроса
     * @param enterpriseServerId  ID сервера 1С:Предприятие
     */
    async function deleteEnterpriseServer(requestKind: RequestKind, enterpriseServerId: string): Promise<TResponseLowerCase<void>> {
        return getTabEnterpriseServersApiProxy().deleteEnterpriseServer(requestKind, enterpriseServerId);
    }

    return {
        getEnterpriseServers,
        createEnterpriseServer,
        getEnterpriseServer,
        editEnterpriseServer,
        deleteEnterpriseServer
    };
}