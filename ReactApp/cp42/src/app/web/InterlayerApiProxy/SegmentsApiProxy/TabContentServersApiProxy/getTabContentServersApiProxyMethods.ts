import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { CloudContentServerResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import {
    CloudContentServerDataModel,
    CloudContentServerFilterParams,
    CreateCloudContentServerParams,
} from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabContentServersApiProxy';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Сайты публикаций"
 * @returns Обьект функций для работы с апи прокси таба "Сайты публикаций"
 */
export function getTabContentServersApiProxyMethods() {
    function getTabContentServersApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().ContentServersTabProxy;
    }

    /**
     * Получить данные серверов публикации
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных серверов публикации
     */
    async function getCloudContentServers(requestKind: RequestKind, args: CloudContentServerFilterParams): Promise<SelectDataMetadataResponseDto<CloudContentServerResponseDto>> {
        return getTabContentServersApiProxy().getCloudContentServers(requestKind, args);
    }

    /**
     * Удалить сервер публикации
     * @param requestKind Вид запроса
     * @param contentServerId ID сервера публикации
     */
    async function deleteCloudContentServer(requestKind: RequestKind, contentServerId: string): Promise<TResponseLowerCase<void>> {
        return getTabContentServersApiProxy().deleteCloudContentServer(requestKind, contentServerId);
    }

    /**
     * Получить модели доступных нод публикации
     * @param requestKind Вид запроса
     * @returns Массив нода публикаций
     */
    async function getAvailablePublishNodes(requestKind: RequestKind): Promise<PublishNodeDataModel[]> {
        return getTabContentServersApiProxy().getAvailablePublishNodes(requestKind)
            .then(response => response.available);
    }

    /**
     * Получить модель сервера публикации
     * @param requestKind Вид запроса
     * @param contentServerId ID сервера публикации
     * @returns Модель сервера публикации
     */
    async function getCloudContentServer(requestKind: RequestKind, contentServerId: string): Promise<CloudContentServerResponseDto> {
        return getTabContentServersApiProxy().getCloudContentServer(requestKind, contentServerId);
    }

    /**
     * Создать сервер публикации
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createCloudContentServer(requestKind: RequestKind, args: CreateCloudContentServerParams): Promise<void> {
        return getTabContentServersApiProxy().createCloudContentServer(requestKind, args);
    }

    /**
     * Редактировать сервер публикации
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editCloudContentServer(requestKind: RequestKind, args: CloudContentServerDataModel): Promise<void> {
        return getTabContentServersApiProxy().editCloudContentServer(requestKind, args);
    }

    /**
     * Проверить доступность ноды
     * @param requestKind Вид запроса
     * @param publishNodeId ID нода публикации
     * @returns Флаг доступности ноды
     */
    async function checkNodeAvailability(requestKind: RequestKind, publishNodeId: string): Promise<boolean> {
        return getTabContentServersApiProxy().checkNodeAvailability(requestKind, publishNodeId);
    }

    return {
        getCloudContentServers,
        deleteCloudContentServer,
        getAvailablePublishNodes,
        getCloudContentServer,
        createCloudContentServer,
        editCloudContentServer,
        checkNodeAvailability
    };
}