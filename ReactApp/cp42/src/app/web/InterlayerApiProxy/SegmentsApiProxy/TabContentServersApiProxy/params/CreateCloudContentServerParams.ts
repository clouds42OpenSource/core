import { PublishNodeDataModel, SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель создания сервера публикации
 */
export type CreateCloudContentServerParams = SearchBaseParams & {
    /**
     * Сайт публикации
     */
    publishSiteName: string;
    /**
     * Сгрупирован по аккаунту
     */
    groupByAccount: boolean;
    /**
     * Выбранные ноды публикации
     */
    publishNodes: PublishNodeDataModel[];
}