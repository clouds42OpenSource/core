import { FilterBaseParams, SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель фильтра сервера публикации
 */
export type CloudContentServerFilterParams = FilterBaseParams & {
    filter: {
        /**
         * Сайт публикации
         */
        publishSiteName?: string;
    } & SearchBaseParams
}