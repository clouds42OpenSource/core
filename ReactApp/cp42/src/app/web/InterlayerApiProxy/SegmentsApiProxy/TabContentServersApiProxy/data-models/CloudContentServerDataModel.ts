import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { CreateCloudContentServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabContentServersApiProxy';

/**
 * Модель сервера публикации
 */
export type CloudContentServerDataModel = CreateCloudContentServerParams & {
    /**
     * Id сервера публикации
     */
    id: string;
    /**
     * Доступные ноды публикации
     */
    availablePublishNodes: PublishNodeDataModel[];
}