import { FilterBaseParams, SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель фильтра фермы серверов
 */
export type ServerFarmFilterParams = FilterBaseParams & {
    filter: SearchBaseParams;
};