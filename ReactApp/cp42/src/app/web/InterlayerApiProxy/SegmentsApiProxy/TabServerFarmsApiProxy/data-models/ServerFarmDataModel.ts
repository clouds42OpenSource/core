import { ServerFarmPublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy/data-models/ServerFarmPublishNodeDataModel';

/**
 * Модель фермы серверов
 */
export type ServerFarmDataModel = {
    /**
     * Id фермы серверов
     */
    id: string;
    /**
     * Название фермы
     */
    name: string;
    /**
     * Описание
     */
    description: string;
    /**
     * Доступные сервера для выбора
     */
    availableServers?: Array<ServerFarmPublishNodeDataModel>;
}