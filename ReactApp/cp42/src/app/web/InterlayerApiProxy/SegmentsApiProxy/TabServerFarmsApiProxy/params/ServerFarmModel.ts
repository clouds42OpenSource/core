import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

export type ServerFarmModel = {
    /**
     * Id фермы серверов
     */
    id: string;
    /**
     * Название фермы
     */
    name: string;
    /**
     * Описание
     */
    description: string;
    /**
     * Доступные сервера для выбора
     */
    nodesData: {
        available: PublishNodeDataModel[];
        selected: PublishNodeDataModel[];
    };
}