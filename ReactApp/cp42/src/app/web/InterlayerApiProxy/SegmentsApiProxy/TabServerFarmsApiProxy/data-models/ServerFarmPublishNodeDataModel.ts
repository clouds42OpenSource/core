import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Нода публикации фермы
 */
export type ServerFarmPublishNodeDataModel = PublishNodeDataModel;