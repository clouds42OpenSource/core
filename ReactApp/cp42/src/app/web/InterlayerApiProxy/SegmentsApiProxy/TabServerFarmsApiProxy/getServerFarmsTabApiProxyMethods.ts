import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ServerFarmDataModel, ServerFarmFilterParams, ServerFarmModel, ServerFarmParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Форма серверов"
 * @returns Обьект функций для работы с апи прокси таба "Форма серверов"
 */
export function getServerFarmsTabApiProxyMethods() {
    function getServerFarmsTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().ServerFarmsTabProxy;
    }

    /**
     * Получить данные ферм сервера
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных ферм сервера
     */
    async function getServerFarms(requestKind: RequestKind, args: ServerFarmFilterParams): Promise<SelectDataMetadataResponseDto<ServerFarmDataModel>> {
        return getServerFarmsTabApiProxy().getServerFarms(requestKind, args);
    }

    /**
     * Удалить ферму сервера
     * @param requestKind Вид запроса
     * @param serverFarmId ID фермы сервера
     */
    async function deleteServerFarm(requestKind: RequestKind, serverFarmId: string): Promise<TResponseLowerCase<void>> {
        return getServerFarmsTabApiProxy().deleteServerFarm(requestKind, serverFarmId);
    }

    /**
     * Получить ферму сервера
     * @param requestKind Вид запроса
     * @param serverFarmId ID фермы сервера
     * @returns Модель фермы сервера
     */
    async function getServerFarm(requestKind: RequestKind, serverFarmId: string): Promise<ServerFarmModel> {
        return getServerFarmsTabApiProxy().getServerFarm(requestKind, serverFarmId);
    }

    /**
     * Создать новый ферму сервера
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createServerFarm(requestKind: RequestKind, args: ServerFarmParams): Promise<void> {
        return getServerFarmsTabApiProxy().createServerFarm(requestKind, args);
    }

    /**
     * Редактировать ферму сервера
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editServerFarm(requestKind: RequestKind, args: ServerFarmParams): Promise<void> {
        return getServerFarmsTabApiProxy().editServerFarm(requestKind, args);
    }

    /**
     * Получить все ноды публикации
     * @param requestKind Вид запроса
     * @returns Список нод публикации
     */
    async function getAllPublishNodes(requestKind: RequestKind): Promise<PublishNodeDataModel[]> {
        return getServerFarmsTabApiProxy().getAllPublishNodes(requestKind)
            .then(responseDto => responseDto.records);
    }

    return {
        getServerFarms,
        deleteServerFarm,
        getServerFarm,
        createServerFarm,
        editServerFarm,
        getAllPublishNodes
    };
}