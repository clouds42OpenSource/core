import { ServerFarmDataModel, ServerFarmPublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy/data-models';

/**
 * Модель параметров для создания/редактирования фермы серверов
 */
export type ServerFarmParams = ServerFarmDataModel & {
    /**
     * Выбранные ноды публикации
     */
    servers: Array<ServerFarmPublishNodeDataModel>;
}