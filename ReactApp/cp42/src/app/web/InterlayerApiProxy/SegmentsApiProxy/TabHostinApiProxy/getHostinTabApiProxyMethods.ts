import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CoreHostingDataModel, CoreHostingFilterParams, CreateCoreHostingParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Хостинг"
 * @returns Обьект функций для работы с апи прокси таба "Хостинг"
 */
export function getHostinTabApiProxyMethods() {
    function getHostinTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().HostingTabProxy;
    }

    /**
     * Получить данные списка хостинг облака
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных хостинга облака
     */
    async function getCoreHostings(requestKind: RequestKind, args: CoreHostingFilterParams): Promise<SelectDataMetadataResponseDto<CoreHostingDataModel>> {
        return getHostinTabApiProxy().getCoreHostings(requestKind, args);
    }

    /**
     * Удалить хостинг облака
     * @param requestKind Вид запроса
     * @param coreHostingId ID хостинга облака
     */
    async function deleteCoreHosting(requestKind: RequestKind, coreHostingId: string): Promise<TResponseLowerCase<void>> {
        return getHostinTabApiProxy().deleteCoreHosting(requestKind, coreHostingId);
    }

    /**
     * Получить хостинг облака
     * @param requestKind Вид запроса
     * @param coreHostingId ID хостинга облака
     * @returns Модель нода публикаций
     */
    async function getCoreHosting(requestKind: RequestKind, coreHostingId: string): Promise<CoreHostingDataModel> {
        return getHostinTabApiProxy().getCoreHosting(requestKind, coreHostingId);
    }

    /**
     * Создать новый хостинг облака
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createCoreHosting(requestKind: RequestKind, args: CreateCoreHostingParams): Promise<void> {
        return getHostinTabApiProxy().createCoreHosting(requestKind, args);
    }

    /**
     * Редактировать хостинг облака
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editCoreHosting(requestKind: RequestKind, args: CoreHostingDataModel): Promise<void> {
        return getHostinTabApiProxy().editCoreHosting(requestKind, args);
    }

    return {
        getCoreHostings,
        deleteCoreHosting,
        getCoreHosting,
        createCoreHosting,
        editCoreHosting
    };
}