/**
 * Модель для создания хостинга
 */
export type CreateCoreHostingParams = {
    /**
     * Название хостинга
     */
    name: string;
    /**
     * Путь для загружаемых файлов
     */
    uploadFilesPath: string;
    /**
     * Адрес API для загрузки файлов
     */
    uploadApiUrl: string;
}