import { CreateCoreHostingParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy/params';

/**
 * Модель хостинга
 */
export type CoreHostingDataModel = CreateCoreHostingParams & {
    /**
     * ID хостинга
     */
    id: string;
}