import { FilterBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { CreateCoreHostingParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy/params';

/**
 * Модель фильтра хостинга облака
 */
export type CoreHostingFilterParams = FilterBaseParams & {
    filter: CreateCoreHostingParams;
};