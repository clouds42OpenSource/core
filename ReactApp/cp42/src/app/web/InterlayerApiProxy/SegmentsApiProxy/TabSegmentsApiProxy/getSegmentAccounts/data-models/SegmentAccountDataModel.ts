/**
 * Модель аккаунта сегмента
 */
export type SegmentAccountDataModel = {
    /**
     * Номер аккаунта
     */
    indexNumber: number;
    /**
     * Название аккаунта
     */
    accountCaption: string;
    /**
     * Размер файловых баз в мб
     */
    sizeOfFileAccountDatabases: number;
    /**
     * Размер серверных баз в мб
     */
    sizeOfServerAccountDatabases: number;
    /**
     * Размер клиентских файлов в мб
     */
    sizeOfClientFiles: number;
}