import { SegmentElementDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Элементы сегмента
 */
export type SegmentElementsDataModel = {
    /**
     * Список бэкап серверов
     */
    backupStorages: SegmentElementDataModel[];
    /**
     * Список файловых хранилищ
     */
    fileStorageServers: SegmentElementDataModel[];
    /**
     * Список серверов Предприятия 8.3
     */
    enterprise82Servers: SegmentElementDataModel[];
    /**
     * Список серверов Предприятия 8.3
     */
    enterprise83Servers: SegmentElementDataModel[];
    /**
     * Список серверов публикаций
     */
    contentServers: SegmentElementDataModel[];
    /**
     * Список SQL серверов
     */
    sqlServers: SegmentElementDataModel[];
    /**
     * Список ферм ТС
     */
    terminalFarms: SegmentElementDataModel[];
    /**
     * Список шлюзов
     */
    gatewayTerminals: SegmentElementDataModel[];
    /**
     * Список стабильных версий 8.2
     */
    stable82Versions: string[];
    /**
     * Список стабильных версий 8.3
     */
    stable83Versions: string[];
    /**
     * Список альфа версий 8.3
     */
    alpha83Versions: string[];
    /**
     * Список хостингов облака
     */
    coreHostingList: SegmentElementDataModel[];
}