import { FilterBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Фильтр поиска аккаунтов сегмента
 */
export type SegmentAccountFilterParams = FilterBaseParams & {
    /**
     * ID сегмента
     */
    segmentId: string;
    /**
     * Номер аккаунта
     */
    accountIndexNumber?: number;
    /**
     * Название аккаунта
     */
    accountCaption?: string;
}