import { SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { SelectDataCommonDataModel } from 'app/web/common';

/**
 * Модель фильтра сегмента
 */
export type CloudSegmentFilterParams = SelectDataCommonDataModel<{
    /**
     * Массив Id терминальных ферм
     */
    terminalFarmId?: string;
    /**
     * Массив Id серверов предприятий
     */
    enterpriseServerId?: string;
    /**
     * Массив Id серверов публикаций
     */
    contentServerId?: string;
    /**
     * Массив Id файловых хранилищ
     */
    fileStorageServerId?: string;
    /**
     * Массив Id SQL серверов
     */
    sqlServerId?: string;
    /**
     * Массив Id терминальных шлюзов
     */
    gatewayTerminalId?: string;
    /**
     * Путь к файлам пользователей
     */
    customFileStoragePath?: string;
} & SearchBaseParams>