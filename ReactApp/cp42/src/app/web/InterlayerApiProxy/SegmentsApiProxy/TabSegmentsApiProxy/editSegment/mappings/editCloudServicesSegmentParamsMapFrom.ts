import { EditCloudServicesSegmentRequestDto } from 'app/web/api/SegmentsProxy';
import { EditCloudServicesSegmentParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';

/**
 * Функция для маппинга EditCloudServicesSegmentParams к EditCloudServicesSegmentRequestDto
 * @param value Data model параметры модели редактирования сегмента
 * @returns Request dto параметры модели редактирования сегмента
 */
export function editCloudServicesSegmentParamsMapFrom(value: EditCloudServicesSegmentParams): EditCloudServicesSegmentRequestDto {
    return {
        alpha83VersionId: value.alpha83VersionId,
        backupStorageId: value.backupStorageId,
        contentServerId: value.contentServerId,
        customFileStoragePath: value.customFileStoragePath,
        defaultSegmentStorageId: value.defaultSegmentStorageId,
        delimiterDatabaseMustUseWebService: value.delimiterDatabaseMustUseWebService,
        description: value.description,
        enterpriseServer82Id: value.enterpriseServer82Id,
        enterpriseServer83Id: value.enterpriseServer83Id,
        fileStorageServersId: value.fileStorageServersId,
        gatewayTerminalsId: value.gatewayTerminalsId,
        hostingName: value.hostingName,
        name: value.name,
        sqlServerId: value.sqlServerId,
        servicesTerminalFarmId: value.servicesTerminalFarmId,
        stable82VersionId: value.stable82VersionId,
        stable83VersionId: value.stable83VersionId,
        id: value.id,
        availableMigrationSegment: value.availableMigrationSegment,
        segmentFileStorageServers: value.segmentFileStorageServers,
        segmentTerminalServers: value.segmentTerminalServers.map((item) => ({ connectionAddress: item.connectionAddress, id: item.key, name: item.value, available: false })),
        autoUpdateNodeId: value.autoUpdateNodeId,
        notMountDiskR: value.notMountDiskR,
    };
}