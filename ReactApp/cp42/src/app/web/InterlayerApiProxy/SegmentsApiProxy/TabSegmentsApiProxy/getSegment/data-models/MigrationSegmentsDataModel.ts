import { MigrationSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';

/**
 * Модель данных сегментов для миграции
 */
export type MigrationSegmentsDataModel = {
    /**
     * Доступные сегменты для миграции
     */
    availableMigrationSegment: MigrationSegmentDataModel[];
    /**
     * Сегменты для миграции
     */
    segmentsForMigration: MigrationSegmentDataModel[];
}