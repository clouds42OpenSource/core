import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SegmentDropdownListResponseDto } from 'app/web/api/SegmentsProxy';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { SegmentElementDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import {
    CloudSegmentDataModel,
    CloudSegmentFilterParams,
    CreateCloudServicesSegmentParams,
    EditCloudServicesSegmentParams,
    editCloudServicesSegmentParamsMapFrom,
    SegmentAccountDataModel,
    SegmentAccountFilterParams,
    SegmentElementsDataModel,
    SegmentsShortDataModel
} from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { CloudServicesSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/getSegment/data-models/CloudServicesSegmentDataModel';
import { segmentShortDataModelMapFrom } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/getSegmentAccounts/mappings/segmentShortDataModelMapFrom';
import { CheckAbilityToDeleteFileStorageParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/сheckAbilityToDeleteFileStorage';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Сегменты"
 * @returns Обьект функций для работы с апи прокси таба "Сегменты"
 */
export function getTabSegmentsApiProxyMethods() {
    function getTabSegmentsApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().SegmentsTabProxy;
    }

    /**
     * Поиск терминальных ферм по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных терминальных ферм
     */
    async function getCloudTerminalFarms(requestKind: RequestKind, searchValue: string): Promise<Array<SegmentElementDataModel>> {
        return getTabSegmentsApiProxy().getCloudTerminalFarms(requestKind, searchValue);
    }

    /**
     * Получить сервера предприятий по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных серверов предприятий
     */
    async function getCloudEnterpriseServers(requestKind: RequestKind, searchValue: string): Promise<Array<SegmentElementDataModel>> {
        return getTabSegmentsApiProxy().getCloudEnterpriseServers(requestKind, searchValue);
    }

    /**
     * Получить сервера публикации по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных серверов публикации
     */
    async function getCloudContentServers(requestKind: RequestKind, searchValue: string): Promise<Array<SegmentElementDataModel>> {
        return getTabSegmentsApiProxy().getCloudContentServers(requestKind, searchValue);
    }

    /**
     * Получить файловые хранилища по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных файловых хранилищ
     */
    async function getCloudFileStorageServers(requestKind: RequestKind, searchValue: string): Promise<Array<SegmentElementDataModel>> {
        return getTabSegmentsApiProxy().getCloudFileStorageServers(requestKind, searchValue);
    }

    /**
     * Получить SQL сервера по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных SQL серверов
     */
    async function getCloudSqlServers(requestKind: RequestKind, searchValue: string): Promise<Array<SegmentElementDataModel>> {
        return getTabSegmentsApiProxy().getCloudSqlServers(requestKind, searchValue);
    }

    /**
     * Получить терминальные шлюзы по строке поиска
     * @param requestKind Вид запроса
     * @param searchValue Строка поиска
     * @returns Список найденных терминальных шлюзов
     */
    async function getCloudGatewayTerminals(requestKind: RequestKind, searchValue: string): Promise<Array<SegmentElementDataModel>> {
        return getTabSegmentsApiProxy().getCloudGatewayTerminals(requestKind, searchValue);
    }

    /**
     * Получить данные сегментов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список сегментов c пагинацией
     */
    async function getCloudSegments(requestKind: RequestKind, args: CloudSegmentFilterParams): Promise<SelectDataResultMetadataModel<CloudSegmentDataModel>> {
        return getTabSegmentsApiProxy().getCloudSegments(requestKind, args);
    }

    async function getCloudSegmentsDropdownList(requestKind: RequestKind): Promise<SegmentDropdownListResponseDto> {
        return getTabSegmentsApiProxy().getSegmentDropdownList(requestKind);
    }

    /**
     * Получить элементы сегмента
     * @param requestKind Вид запроса
     * @returns Элементы сегмента
     */
    async function getSegmentElements(requestKind: RequestKind): Promise<SegmentElementsDataModel> {
        return getTabSegmentsApiProxy().getSegmentElements(requestKind);
    }

    /**
     * Создать новый сегмент
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createSegment(requestKind: RequestKind, args: CreateCloudServicesSegmentParams): Promise<void> {
        return getTabSegmentsApiProxy().createSegment(requestKind, args);
    }

    /**
     * Получить сегмент по ID
     * @param requestKind Вид запроса
     * @param segmentId ID сегмента
     * @returns Модель сегмента
     */
    async function getSegment(requestKind: RequestKind, segmentId: string): Promise<CloudServicesSegmentDataModel> {
        return getTabSegmentsApiProxy().getSegment(requestKind, segmentId);
    }

    /**
     * Получить данные по аккаунтам сегмента
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Получить данные по аккаунтам сегмента
     */
    async function getSegmentAccounts(requestKind: RequestKind, args: SegmentAccountFilterParams): Promise<SelectDataMetadataResponseDto<SegmentAccountDataModel>> {
        return getTabSegmentsApiProxy().getSegmentAccounts(requestKind, args);
    }

    /**
     * Удалить сегмент
     * @param requestKind Вид запроса
     * @param segmentId ID сегмента
     */
    async function deleteSegment(requestKind: RequestKind, segmentId: string): Promise<TResponseLowerCase<void>> {
        return getTabSegmentsApiProxy().deleteSegment(requestKind, segmentId);
    }

    /**
     * Редактировать сегмент
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editSegment(requestKind: RequestKind, args: EditCloudServicesSegmentParams): Promise<TResponseLowerCase<null> | null> {
        return getTabSegmentsApiProxy().editSegment(requestKind, editCloudServicesSegmentParamsMapFrom(args));
    }

    /**
     * Получить данные по задачам воркера
     * @param requestKind тип запроса
     */
    async function receiveSegmentsShortData(requestKind: RequestKind): Promise<SegmentsShortDataModel> {
        return getTabSegmentsApiProxy().receiveSegmentsShortData(requestKind)
            .then(responseDto => {
                const itemsResponse: SegmentsShortDataModel = {
                    items: []
                };

                for (const item of Object.entries(responseDto)) {
                    itemsResponse.items.push(segmentShortDataModelMapFrom(item));
                }

                return itemsResponse;
            });
    }

    /**
     * Проверить возможность удаления файлового хранилища из доступных
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Флаг обозначающий возможность удаления файлового хранилища из доступных
     */
    async function checkAbilityToDeleteFileStorage(requestKind: RequestKind, args: CheckAbilityToDeleteFileStorageParams): Promise<boolean> {
        return getTabSegmentsApiProxy().checkAbilityToDeleteFileStorage(requestKind, {
            SegmentId: args.segmentId,
            StorageId: args.storageId
        });
    }

    return {
        getCloudTerminalFarms,
        getCloudEnterpriseServers,
        getCloudContentServers,
        getCloudFileStorageServers,
        getCloudSqlServers,
        getCloudGatewayTerminals,
        getCloudSegments,
        getSegmentElements,
        createSegment,
        getSegment,
        getSegmentAccounts,
        deleteSegment,
        editSegment,
        receiveSegmentsShortData,
        checkAbilityToDeleteFileStorage,
        getCloudSegmentsDropdownList
    };
}