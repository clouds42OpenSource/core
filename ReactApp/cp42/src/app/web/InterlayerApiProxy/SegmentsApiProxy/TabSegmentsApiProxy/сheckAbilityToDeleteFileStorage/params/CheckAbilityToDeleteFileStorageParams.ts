/**
 * Модель параметра для проверки возможности удаления файлового хранилища из доступных
 */
export type CheckAbilityToDeleteFileStorageParams = {
    /**
     * ID сегмента
     */
    segmentId: string;
    /**
     * ID хранилища
     */
    storageId: string;
}