import { KeyValueDataModel } from 'app/web/common/data-models';

/**
 * Модель кратких данных по сегментам
 */
export type SegmentsShortDataModel = {

    /**
     * Массив записей
     */
    items: Array<KeyValueDataModel<string, string>>;
};