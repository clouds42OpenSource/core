import { SegmentTerminalServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';

/**
 * Модель данных терминальных серверов для сегмента
 */
export type SegmentTerminalServersDataModel = {
    /**
     * Терминальные сервера сегмента
     */
    selected: SegmentTerminalServerDataModel[];
    /**
     * Доступные терминальные сервера для сегмента
     */
    available: SegmentTerminalServerDataModel & {
        available: boolean;
    }[];
}