import { SegmentElementDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель данных сегмента для миграции
 */
export type MigrationSegmentDataModel = SegmentElementDataModel & {
    /**
     * Признак что сегмент доступен
     */
    isAvailable: boolean;
    /**
     * Признак что сегмент по параметрам похож на текущий
     */
    isSimilar: boolean;
}