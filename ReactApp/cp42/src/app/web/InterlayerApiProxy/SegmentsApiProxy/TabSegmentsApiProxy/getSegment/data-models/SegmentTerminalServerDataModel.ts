import { SegmentTerminalServerResponseDto } from 'app/web/api/SegmentsProxy';

/**
 * Терминальный сервер сегмента
 */
export type SegmentTerminalServerDataModel = SegmentTerminalServerResponseDto;