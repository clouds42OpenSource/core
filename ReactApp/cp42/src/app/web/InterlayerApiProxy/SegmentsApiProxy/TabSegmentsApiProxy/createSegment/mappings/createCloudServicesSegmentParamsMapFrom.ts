import { CreateCloudServicesSegmentRequestDto } from 'app/web/api/SegmentsProxy/TabSegments/request-dto';
import { CreateCloudServicesSegmentParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy/createSegment/params';

/**
 * Маппинг Data model params модели создания сегмента к request dto
 * @param value Data model params модель создания сегмента
 * @returns Request dto модель создания сегмента
 */
export function createCloudServicesSegmentParamsMapFrom(value: CreateCloudServicesSegmentParams): CreateCloudServicesSegmentRequestDto {
    return value;
}