import { CloudServicesSegmentBaseDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель создания сегмента
 */
export type CreateCloudServicesSegmentParams = CloudServicesSegmentBaseDataModel & {
    /**
     * ID хостинга
     */
    coreHostingId: string;
    /**
     * Сегмент по умолчанию
     */
    isDefault: boolean;
}