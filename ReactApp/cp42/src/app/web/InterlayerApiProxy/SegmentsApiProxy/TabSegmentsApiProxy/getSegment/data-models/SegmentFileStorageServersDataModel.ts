import { FileStoragesItemResponseDto } from 'app/web/api/SegmentsProxy/common';

/**
 * Модель данных файловых хранилищ для сегмента
 */
export type SegmentFileStorageServersDataModel = {
    /**
     * Доступные файловые хранилища для сегмента
     */
    available: FileStoragesItemResponseDto[];
    /**
     * Файловые хранилища сегмента
     */
    selected: FileStoragesItemResponseDto[];
}