import { CloudServicesSegmentBaseDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { SegmentTerminalServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';

/**
 * Модель редактирования сегмента
 */
export type EditCloudServicesSegmentParams = CloudServicesSegmentBaseDataModel & {
    /**
     * ID элемента сегмента
     */
    id: string;
    /**
     * Доступные сегменты для миграции
     */
    availableMigrationSegment: {
        id: string;
        name: string
    }[];
    /**
     * Файловые хранилища сегмента
     */
    segmentFileStorageServers: {
        id: string;
        name: string
    }[];
    /**
     * Терминальные сервера сегмента
     */
    segmentTerminalServers: SegmentTerminalServerDataModel[];
    autoUpdateNodeId: string | null;
}