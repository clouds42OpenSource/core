import { KeyValueDataModel } from 'app/web/common';

export function segmentShortDataModelMapFrom(value: [string, string]): KeyValueDataModel<string, string> {
    return {
        key: value[0],
        value: value[1]
    };
}