import { SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

export type CommonCreateSegmentParams = SearchBaseParams;