import { CommonCreateSegmentParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Общий модель сегмента
 */
export type CommonSegmentDataModel = CommonCreateSegmentParams & {
    /**
     * ID записи
     */
    id: string;
}