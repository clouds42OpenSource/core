import { FilterBaseParams } from 'app/web/common/params/FilterBaseParams';
import { SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common/params/SearchBaseParams';

/**
 * Общий модель фильтра, поиска сегментов
 */
export type CommonSegmentFilterParams = {
    filter: SearchBaseParams;
} & FilterBaseParams;