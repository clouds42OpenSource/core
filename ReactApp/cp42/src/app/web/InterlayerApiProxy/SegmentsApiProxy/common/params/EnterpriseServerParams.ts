import { EnterpriseServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';

/**
 * Тип параметра для сервера 1С:Предприятия
 */
export type EnterpriseServerParams = EnterpriseServerDataModel;