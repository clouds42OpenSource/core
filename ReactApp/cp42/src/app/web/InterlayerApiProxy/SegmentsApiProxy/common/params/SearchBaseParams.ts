export type SearchBaseParams = {
    connectionAddress?: string;
    name?: string;
    description?: string;
    usingOutdatedWindows?: boolean;
}