/**
 * Модель элемента сегмента
 */
export type SegmentElementDataModel = {
    /**
     * Id элемента
     */
    key: string;
    /**
     * Название элемента
     */
    value: string;
}