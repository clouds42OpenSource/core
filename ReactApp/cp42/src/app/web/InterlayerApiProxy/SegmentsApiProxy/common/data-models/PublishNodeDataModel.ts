/**
 * Нода публикаций
 */
export type PublishNodeDataModel = {
    /**
     * Идентификатор ноды
     */
    id: string;
    /**
     * Описание
     */
    description: string;
    /**
     * Адрес подключения
     */
    address: string;
}