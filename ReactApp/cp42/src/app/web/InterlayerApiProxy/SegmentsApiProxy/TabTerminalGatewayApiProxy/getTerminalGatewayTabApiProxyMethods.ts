import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonCreateSegmentParams, CommonSegmentDataModel, CommonSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Терм. шлюзы"
 * @returns Обьект функций для работы с апи прокси таба "Терм. шлюзы"
 */
export function getTerminalGatewayTabApiProxyMethods() {
    function getTerminalGatewayTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().TerminalGatewayTabProxy;
    }

    /**
     * Получить данные терминальных шлюзов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по терминальным шлюзам
     */
    async function getCloudGatewayTerminals(requestKind: RequestKind, args: CommonSegmentFilterParams): Promise<SelectDataMetadataResponseDto<CommonSegmentDataModel>> {
        return getTerminalGatewayTabApiProxy().getCloudGatewayTerminals(requestKind, args);
    }

    /**
     * Удалить терминальный шлюз
     * @param requestKind Вид запроса
     * @param cloudGatewayTerminalId  ID терминального шлюза
     */
    async function deleteCloudGatewayTerminal(requestKind: RequestKind, cloudGatewayTerminalId: string): Promise<TResponseLowerCase<void>> {
        return getTerminalGatewayTabApiProxy().deleteCloudGatewayTerminal(requestKind, cloudGatewayTerminalId);
    }

    /**
     * Получить терминальный шлюз
     * @param requestKind Вид запроса
     * @param cloudGatewayTerminalId ID терминального шлюза
     * @returns Модель терминального шлюза
     */
    async function getCloudGatewayTerminal(requestKind: RequestKind, cloudGatewayTerminalId: string): Promise<CommonSegmentDataModel> {
        return getTerminalGatewayTabApiProxy().getCloudGatewayTerminal(requestKind, cloudGatewayTerminalId);
    }

    /**
     * Создать новый терминальный шлюз
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createCloudGatewayTerminal(requestKind: RequestKind, args: CommonCreateSegmentParams): Promise<void> {
        return getTerminalGatewayTabApiProxy().createCloudGatewayTerminal(requestKind, args);
    }

    /**
     * Редактировать терминальный шлюз
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editCloudGatewayTerminal(requestKind: RequestKind, args: CommonSegmentDataModel): Promise<void> {
        return getTerminalGatewayTabApiProxy().editCloudGatewayTerminal(requestKind, args);
    }

    return {
        getCloudGatewayTerminals,
        deleteCloudGatewayTerminal,
        getCloudGatewayTerminal,
        createCloudGatewayTerminal,
        editCloudGatewayTerminal
    };
}