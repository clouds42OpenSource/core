import { getArrServersTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabArrServersApiProxy';
import { getBackupStorageTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { getTabContentServersApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabContentServersApiProxy';
import { getTabEnterpriseServersApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabEnterpriseServersApiProxy';
import { getFileStorageTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { getHostinTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabHostinApiProxy';
import { getPlatformVersionTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { getPublishNodesTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPublishNodesApiProxy';
import { getTabSegmentsApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSegmentsApiProxy';
import { getServerFarmsTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabServerFarmsApiProxy';
import { getSqlServersTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { getTabTerminalFarmsApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabTerminalFarmsApiProxy';
import { getTerminalGatewayTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabTerminalGatewayApiProxy';
import { getTerminalServerTabApiProxyMethods } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabTerminalServerApiProxy';

/**
 * Апи прокси сегментов
 */
export const SegmentsApiProxy = (() => {
    return {
        getTabSegmentsApiProxyMethods,
        getTabTerminalFarmsApiProxyMethods,
        getTabEnterpriseServersApiProxyMethods,
        getTabContentServersApiProxyMethods,
        getPublishNodesTabApiProxyMethods,
        getFileStorageTabApiProxyMethods,
        getSqlServersTabApiProxyMethods,
        getBackupStorageTabApiProxyMethods,
        getTerminalGatewayTabApiProxyMethods,
        getTerminalServerTabApiProxyMethods,
        getPlatformVersionTabApiProxyMethods,
        getHostinTabApiProxyMethods,
        getServerFarmsTabApiProxyMethods,
        getArrServersTabApiProxyMethods
    };
})();