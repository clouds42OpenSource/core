import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { PlatformVersion1CDataModel, PlatformVersion1CFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Платформа 1С"
 * @returns Обьект функций для работы с апи прокси таба "Платформа 1С"
 */
export function getPlatformVersionTabApiProxyMethods() {
    function getPlatformVersionTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().PlatformVersionTabProxy;
    }

    /**
     * Получить данные версий платформы 1С
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных версий платформы 1С
     */
    async function getPlatformVersions(requestKind: RequestKind, args: PlatformVersion1CFilterParams): Promise<SelectDataMetadataResponseDto<PlatformVersion1CDataModel>> {
        return getPlatformVersionTabApiProxy().getPlatformVersions(requestKind, args);
    }

    /**
     * Удалить версию платформы 1С
     * @param requestKind Вид запроса
     * @param platformVersion  Версия платформы 1С
     */
    async function deletePlatformVersion(requestKind: RequestKind, platformVersion: string): Promise<TResponseLowerCase<void>> {
        return getPlatformVersionTabApiProxy().deletePlatformVersion(requestKind, platformVersion);
    }

    /**
     * Получить версию платформы 1С
     * @param requestKind Вид запроса
     * @param platformVersion  Версия платформы 1С
     * @returns Модель нода публикаций
     */
    async function getPlatformVersion(requestKind: RequestKind, platformVersion: string): Promise<PlatformVersion1CDataModel> {
        return getPlatformVersionTabApiProxy().getPlatformVersion(requestKind, platformVersion);
    }

    /**
     * Создать новую версию платформы 1С
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createPlatformVersion(requestKind: RequestKind, args: PlatformVersion1CDataModel): Promise<TResponseLowerCase<void>> {
        return getPlatformVersionTabApiProxy().createPlatformVersion(requestKind, args);
    }

    /**
     * Редактировать версию платформы 1С
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editPlatformVersion(requestKind: RequestKind, args: PlatformVersion1CDataModel): Promise<void> {
        return getPlatformVersionTabApiProxy().editPlatformVersion(requestKind, args);
    }

    return {
        getPlatformVersions,
        deletePlatformVersion,
        getPlatformVersion,
        createPlatformVersion,
        editPlatformVersion
    };
}