import { FilterBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Фильтер версии платформы 1С
 */
export type PlatformVersion1CFilterParams = FilterBaseParams & {
    filter: {
        /**
         * Версия платформы
         */
        version: string;
        /**
         * Путь к платформе
         */
        pathToPlatform: string;
        /**
         * Путь к платформе x64
         */
        pathToPlatformX64: string;
    }
}