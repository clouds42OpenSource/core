import { CreateCloudBackupStorageParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy/params';

/**
 * Модель хранилища бэкапов
 */
export type CloudBackupStorageDataModel = CreateCloudBackupStorageParams & {
    /**
     * ID записи
     */
    id: string;
}