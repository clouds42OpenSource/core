import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudBackupStorageDataModel, CloudBackupStorageFilterParams, CreateCloudBackupStorageParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Хранилище бекапов"
 * @returns Обьект функций для работы с апи прокси таба "Хранилище бекапов"
 */
export function getBackupStorageTabApiProxyMethods() {
    function getBackupStorageTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().BackupStorageTabProxy;
    }

    /**
     * Получить данные хранилищ бэкапов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по хранилищам бэкапов
     */
    async function getCloudBackupStorages(requestKind: RequestKind, args: CloudBackupStorageFilterParams): Promise<SelectDataMetadataResponseDto<CloudBackupStorageDataModel>> {
        return getBackupStorageTabApiProxy().getCloudBackupStorages(requestKind, args);
    }

    /**
     * Удалить хранилище бэкапов
     * @param requestKind Вид запроса
     * @param backupStorageId ID хранилища бэкапа
     */
    async function deleteCloudBackupStorage(requestKind: RequestKind, backupStorageId: string): Promise<TResponseLowerCase<void>> {
        return getBackupStorageTabApiProxy().deleteCloudBackupStorage(requestKind, backupStorageId);
    }

    /**
     * Получить хранилище бэкапов
     * @param requestKind Вид запроса
     * @param backupStorageId ID хранилища бэкапа
     * @returns Модель хранилища бэкапов
     */
    async function getCloudBackupStorage(requestKind: RequestKind, backupStorageId: string): Promise<CloudBackupStorageDataModel> {
        return getBackupStorageTabApiProxy().getCloudBackupStorage(requestKind, backupStorageId);
    }

    /**
     * Создать новое хранилище бэкапов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createCloudBackupStorage(requestKind: RequestKind, args: CreateCloudBackupStorageParams): Promise<void> {
        return getBackupStorageTabApiProxy().createCloudBackupStorage(requestKind, args);
    }

    /**
     * Редактировать хранилище бэкапов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editCloudBackupStorage(requestKind: RequestKind, args: CloudBackupStorageDataModel): Promise<void> {
        return getBackupStorageTabApiProxy().editCloudBackupStorage(requestKind, args);
    }

    return {
        getCloudBackupStorages,
        deleteCloudBackupStorage,
        getCloudBackupStorage,
        createCloudBackupStorage,
        editCloudBackupStorage
    };
}