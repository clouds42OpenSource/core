import { SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель создания хранилища бэкапов
 */
export type CreateCloudBackupStorageParams = SearchBaseParams & {
    /**
     * Физический путь
     */
    physicalPath: string;
}