import { FilterBaseParams, SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Модель фильтра хранилища бэкапов
 */
export type CloudBackupStorageFilterParams = FilterBaseParams & {
    filter: SearchBaseParams;
};