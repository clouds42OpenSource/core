import { CloudBackupStorageResponseDto } from 'app/web/api/SegmentsProxy/TabBackupStorage';
import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';

/**
 * Маппинг Response Dto хранилища бэкапов к Data Model
 * @param value Response Dto хранилища бэкапов
 * @returns Data Model хранилища бэкапов
 */
export function cloudBackupStorageDataModelMapFrom(value: CloudBackupStorageResponseDto): CloudBackupStorageDataModel {
    return {
        id: value.Id,
        physicalPath: value.PhysicalPath,
        connectionAddress: value.ConnectionAddress,
        description: value.Description,
        name: value.Name
    };
}