import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { ArrServerCreateDataModel, ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Сервера ARR"
 * @returns Обьект функций для работы с апи прокси таба "Сервера ARR"
 */
export function getArrServersTabApiProxyMethods() {
    function getArrServersTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().ArrServersTabProxy;
    }

    /**
     * Получить данные по серверам ARR
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по серверам ARR
     */
    async function getArrServers(requestKind: RequestKind, args: CommonSegmentFilterParams): Promise<SelectDataMetadataResponseDto<ArrServerDataModel>> {
        return getArrServersTabApiProxy().getArrServers(requestKind, args);
    }

    /**
     * Удалить сервер ARR
     * @param requestKind Вид запроса
     * @param arrServerId ID сервера ARR
     */
    async function deleteArrServer(requestKind: RequestKind, arrServerId: string): Promise<TResponseLowerCase<void>> {
        return getArrServersTabApiProxy().deleteArrServer(requestKind, arrServerId);
    }

    /**
     * Получить сервер ARR
     * @param requestKind Вид запроса
     * @param arrServerId ID сервера ARR
     * @returns Модель сервера ARR
     */
    async function getArrServer(requestKind: RequestKind, arrServerId: string): Promise<ArrServerDataModel> {
        return getArrServersTabApiProxy().getArrServer(requestKind, arrServerId);
    }

    /**
     * Создать новый сервер ARR
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createArrServer(requestKind: RequestKind, args: ArrServerCreateDataModel): Promise<void> {
        return getArrServersTabApiProxy().createArrServer(requestKind, args);
    }

    /**
     * Редактировать сервер ARR
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editArrServer(requestKind: RequestKind, args: ArrServerDataModel): Promise<void> {
        return getArrServersTabApiProxy().editArrServer(requestKind, args);
    }

    /**
     * Проверить доступность сервера ARR
     * @param requestKind Вид запроса
     * @param address Адрес
     * @returns Флаг на доступность сервера ARR
     */
    async function checkArrServerAvailability(requestKind: RequestKind, address: string): Promise<boolean> {
        return getArrServersTabApiProxy().checkArrServerAvailability(requestKind, address);
    }

    return {
        getArrServers,
        deleteArrServer,
        getArrServer,
        createArrServer,
        editArrServer,
        checkArrServerAvailability
    };
}