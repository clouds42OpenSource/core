import { FilterBaseParams, SearchBaseParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';

/**
 * Фильтр поиска нод публикаций
 */
export type PublishNodeFilterParams = FilterBaseParams & {
    filter: SearchBaseParams & {
        address?: string
    };
};