import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { PublishNodeDataModel, } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { PublishNodeFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPublishNodesApiProxy/params';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Ноды публикаций"
 * @returns Обьект функций для работы с апи прокси таба "Ноды публикаций"
 */
export function getPublishNodesTabApiProxyMethods() {
    function getPublishNodesTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().PublishNodesTabProxy;
    }

    /**
     * Получить данные по нодам публикаций
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по нодам публикаций
     */
    async function getPublishNodes(requestKind: RequestKind, args: PublishNodeFilterParams): Promise<SelectDataMetadataResponseDto<PublishNodeDataModel>> {
        return getPublishNodesTabApiProxy().getPublishNodes(requestKind, args);
    }

    /**
     * Удалить ноду публикаций
     * @param requestKind Вид запроса
     * @param publishNodeId ID ноды публикаций
     */
    async function deletePublishNode(requestKind: RequestKind, publishNodeId: string): Promise<TResponseLowerCase<void>> {
        return getPublishNodesTabApiProxy().deletePublishNode(requestKind, publishNodeId);
    }

    /**
     * Получить ноду публикаций
     * @param requestKind Вид запроса
     * @param publishNodeId ID ноды публикаций
     * @returns Модель нода публикаций
     */
    async function getPublishNode(requestKind: RequestKind, publishNodeId: string): Promise<PublishNodeDataModel> {
        return getPublishNodesTabApiProxy().getPublishNode(requestKind, publishNodeId);
    }

    /**
     * Создать новую ноду публикаций
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createPublishNode(requestKind: RequestKind, args: PublishNodeDataModel): Promise<void> {
        const { address, description } = args;
        return getPublishNodesTabApiProxy().createPublishNode(requestKind, { address, description });
    }

    /**
     * Редактировать ноду публикаций
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editPublishNode(requestKind: RequestKind, args: PublishNodeDataModel): Promise<void> {
        return getPublishNodesTabApiProxy().editPublishNode(requestKind, args);
    }

    return {
        getPublishNodes,
        deletePublishNode,
        getPublishNode,
        createPublishNode,
        editPublishNode
    };
}