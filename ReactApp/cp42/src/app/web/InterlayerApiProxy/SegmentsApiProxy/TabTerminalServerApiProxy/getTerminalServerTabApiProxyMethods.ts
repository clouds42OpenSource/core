import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonCreateSegmentParams, CommonSegmentDataModel, CommonSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Получить обьект функций для работы с апи прокси таба "Терм. сервера"
 * @returns Обьект функций для работы с апи прокси таба "Терм. сервера"
 */
export function getTerminalServerTabApiProxyMethods() {
    function getTerminalServerTabApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSegmentsProxy().TerminalServerTabProxy;
    }

    /**
     * Получить данные терминальных серверов
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список данных по терминальным серверам
     */
    async function getCloudTerminalServers(requestKind: RequestKind, args: CommonSegmentFilterParams): Promise<SelectDataMetadataResponseDto<CommonSegmentDataModel>> {
        return getTerminalServerTabApiProxy().getCloudTerminalServers(requestKind, args);
    }

    /**
     * Удалить терминальный сервер
     * @param requestKind Вид запроса
     * @param terminalServerId  ID терминального сервера
     */
    async function deleteCloudTerminalServer(requestKind: RequestKind, terminalServerId: string): Promise<TResponseLowerCase<void>> {
        return getTerminalServerTabApiProxy().deleteCloudTerminalServer(requestKind, terminalServerId);
    }

    /**
     * Получить терминальный сервер
     * @param requestKind Вид запроса
     * @param terminalServerId ID терминального сервера
     * @returns Модель терминального сервера
     */
    async function getCloudTerminalServer(requestKind: RequestKind, terminalServerId: string): Promise<CommonSegmentDataModel> {
        return getTerminalServerTabApiProxy().getCloudTerminalServer(requestKind, terminalServerId);
    }

    /**
     * Создать новый терминальный сервер
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createCloudTerminalServer(requestKind: RequestKind, args: CommonCreateSegmentParams): Promise<void> {
        return getTerminalServerTabApiProxy().createCloudTerminalServer(requestKind, args);
    }

    /**
     * Редактировать терминальный сервер
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editCloudTerminalServer(requestKind: RequestKind, args: CommonSegmentDataModel): Promise<void> {
        return getTerminalServerTabApiProxy().editCloudTerminalServer(requestKind, args);
    }

    return {
        getCloudTerminalServers,
        deleteCloudTerminalServer,
        getCloudTerminalServer,
        createCloudTerminalServer,
        editCloudTerminalServer
    };
}