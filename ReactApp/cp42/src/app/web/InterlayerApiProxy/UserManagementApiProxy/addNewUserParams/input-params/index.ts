import { TAccountDepartment } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

/**
 * Параметры для создания нового пользователя
 */
export type AddNewUserRequestDataModel = {
    /** Логин */
    login: string;
    /** Тел. номер */
    phoneNumber: string;
    /** Почта */
    email: string;
    /** Фамилия */
    lastName: string;
    /** Имя */
    firstName: string;
    /** Отчество */
    midName: string;
    /** Пароль */
    password: string;
    /** Подтверждение пароля */
    confirmPassword: string;
    /** Роли */
    roles: Array<string>;
    departmentRequest: TAccountDepartment | null;
};