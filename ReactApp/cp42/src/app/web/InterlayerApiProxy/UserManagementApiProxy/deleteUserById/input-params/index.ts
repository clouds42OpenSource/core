/**
 * Параметры для удаления пользователя
 */
export type DeleteUserByIdInputParams = {
    AccountUserID: string;
};