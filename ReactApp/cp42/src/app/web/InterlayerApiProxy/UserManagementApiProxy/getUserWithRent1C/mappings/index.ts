import { GetUserWithRent1CDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUserWithRent1C/data-models';
import { UserWithRent1CResponseDto } from 'app/web/api/UserManagementProxy/response-dto/UserWithRent1CResponseDto';

/**
 * Маппинг статуса аренды 1С
 */
export function getUserWithRent1CMapFrom(value: UserWithRent1CResponseDto): GetUserWithRent1CDataModel {
    return {
        success: value.Success,
        message: value.Message
    };
}