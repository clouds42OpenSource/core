/**
 * Параметры для получения статуса аренды 1С
 */
export type GetUserWithRent1CParams = {
    userId: string;
};