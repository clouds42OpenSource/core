/**
 * Модель статуса аренды 1С
 */
export type GetUserWithRent1CDataModel = {
    success: boolean;
    message: string;
};