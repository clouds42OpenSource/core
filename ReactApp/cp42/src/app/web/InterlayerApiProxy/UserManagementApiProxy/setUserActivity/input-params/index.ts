/**
 * Параметры для изменения акивтивности пользователя
 */
export type SetUserActivityParams = {
    /** Признак активности */
    isActivate: boolean;
    /** ы пользователя */
    accountUserID: string;
};