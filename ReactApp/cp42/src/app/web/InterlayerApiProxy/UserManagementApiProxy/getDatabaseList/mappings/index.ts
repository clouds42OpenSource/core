import { ApplicationDto, GetDataBaseListResponseDto } from 'app/web/api/UserManagementProxy/response-dto';
import { ApplicationItemDataModel, GetDatabaseListDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getDatabaseList/data-models';

function applicationMapFrom(value: ApplicationDto): ApplicationItemDataModel {
    return {
        'access-level': value['access-level'],
        id: value.id,
        name: value.name,
        state: value.state,
    };
}

/**
 * Маппинг для получения поставщика
 * @param value Response dto поставщика
 * @returns Data model поставщика
 */
export function getDatabaseListMapFrom(value: GetDataBaseListResponseDto): GetDatabaseListDataModel {
    return {
        'records-count': value['records-count'],
        pages: value.pages,
        applications: value.applications.map(item => applicationMapFrom(item))
    };
}