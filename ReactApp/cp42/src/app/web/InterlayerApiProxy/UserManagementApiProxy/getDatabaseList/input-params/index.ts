/**
 * Параметры для получения списка баз
 */
export type GetDatabaseListInputParams = {
    /** Количество записей */
    size: number;
    /** Страница */
    page: number;
    /** ID пользователя */
    userid: string;
};