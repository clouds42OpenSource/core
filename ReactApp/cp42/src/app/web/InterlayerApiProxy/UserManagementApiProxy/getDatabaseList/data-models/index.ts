import { AccessLevelEnum, AccessStateEnum } from 'app/common/enums/AccessStateEnum';

/**
 * Модель базы
 */
export type ApplicationItemDataModel = {
    /** Название базы */
    name: string;
    /** Статус выдачи доступа */
    state: AccessStateEnum;
    /** ID базы */
    id: string;
    /** Уровень доступа к базе */
    'access-level': AccessLevelEnum;
}

/**
 * Модель пагинированного списка баз
 */
export type GetDatabaseListDataModel = {
    /** Количество страниц */
    pages: number;
    /** Количество записей */
    'records-count': number;
    /** Массив модели базы */
    applications: ApplicationItemDataModel[]
}