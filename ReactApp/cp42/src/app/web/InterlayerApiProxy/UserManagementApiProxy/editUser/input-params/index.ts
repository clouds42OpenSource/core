import { TAccountDepartment } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

/**
 * Параметры для редактирования полей пользователя
 */
export type EditUserParamsDataModel = {
    /** ID пользователя */
    id: string;
    /** Логин */
    login: string;
    /** Телефон  */
    phoneNumber: string;
    /** Почта */
    email: string;
    /** Фамилия */
    lastName: string;
    /* *Имя */
    firstName: string;
    /** Отчество */
    midName: string;
    /** Пароль */
    password: string;
    /** Подтверждение пароля */
    confirmPassword: string;
    /** Роли */
    roles: Array<string>;
    /** Текущий пароль */
    currentPassword?: string;
    departmentRequest: TAccountDepartment | null;
};