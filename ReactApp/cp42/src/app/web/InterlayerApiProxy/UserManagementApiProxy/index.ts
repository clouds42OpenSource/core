import { TResponseLowerCase } from 'app/api/types';
import { changeToCamelCase } from 'app/web/common';
import { ValidateUserEmailParams, ValidateUserLoginParams, ValidateUserPhoneParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/validateUserFields/input-params';

import { IoCContainer } from 'app/IoCСontainer';
import { AddNewUserRequestDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/addNewUserParams/input-params';
import { CheckCurrentPasswordInputParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/checkCurrentPassword/input-params';
import { DeleteUserByIdInputParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/deleteUserById/input-params';
import { EditUserParamsDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/editUser/input-params';
import { GetDatabaseListDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getDatabaseList/data-models';
import { GetDatabaseListInputParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getDatabaseList/input-params';
import { getDatabaseListMapFrom } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getDatabaseList/mappings';
import { GetUserWithRent1CDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUserWithRent1C/data-models';
import { GetUserWithRent1CParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUserWithRent1C/input-params';
import { getUserWithRent1CMapFrom } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUserWithRent1C/mappings';
import { AccountRelatedUsersDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUsersWithAccess/data-models';
import { AccountRelatedUsersParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUsersWithAccess/input-params';
import { getAccountRelatedUsersMapFrom } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUsersWithAccess/mappings';
import { SetUserActivityParams } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/setUserActivity/input-params';
import { RequestKind } from 'core/requestSender/enums';

export const UserManagementApiProxy = (() => {
    function getUserManagementApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getUserManagementApiProxy();
    }

    function getMSDatabaseManagementProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getMSDatabaseManagementProxy();
    }

    async function deleteUserById(requestKind: RequestKind, args?: DeleteUserByIdInputParams): Promise<{}> {
        return getUserManagementApiProxy().deleteUserById(requestKind, args);
    }

    async function setUserActivity(requestKind: RequestKind, args?: SetUserActivityParams): Promise<{}> {
        return getUserManagementApiProxy().setUserActivity(requestKind, args);
    }

    async function IsEmailAvailable(requestKind: RequestKind, args?: ValidateUserEmailParams) {
        return getUserManagementApiProxy().IsEmailAvailable(requestKind, args);
    }

    async function IsPhoneAvailable(requestKind: RequestKind, args?: ValidateUserPhoneParams) {
        return getUserManagementApiProxy().IsPhoneAvailable(requestKind, args);
    }

    async function IsUserLoginAviable(requestKind: RequestKind, args?: ValidateUserLoginParams) {
        return getUserManagementApiProxy().IsUserLoginAviable(requestKind, args);
    }

    async function checkCurrentPassword(requestKind: RequestKind, args?: CheckCurrentPasswordInputParams) {
        return getUserManagementApiProxy().checkCurrentPassword(requestKind, args);
    }

    async function addNewUser(requestKind: RequestKind, args: AddNewUserRequestDataModel) {
        return getUserManagementApiProxy().addNewUser(requestKind, {
            ConfirmPassword: args.confirmPassword,
            Email: args.email,
            FirstName: args.firstName,
            LastName: args.lastName,
            Login: args.login,
            MidName: args.midName,
            Password: args.password,
            PhoneNumber: args.phoneNumber,
            Roles: args.roles,
            DepartmentRequest: changeToCamelCase(args?.departmentRequest, true)
        });
    }

    async function editUser(requestKind: RequestKind, args?: EditUserParamsDataModel): Promise<TResponseLowerCase<boolean>> {
        return getUserManagementApiProxy().editUserFields(requestKind, {
            ConfirmPassword: args?.confirmPassword ?? '',
            Email: args?.email ?? '',
            FirstName: args?.firstName ?? '',
            LastName: args?.lastName ?? '',
            Login: args?.login ?? '',
            MiddleName: args?.midName ?? '',
            Password: args?.password ?? '',
            PhoneNumber: args?.phoneNumber ?? '',
            Roles: args?.roles ?? [],
            Id: args?.id ?? '',
            CurrentPassword: args?.currentPassword ?? '',
            DepartmentRequest: changeToCamelCase(args?.departmentRequest, true)
        });
    }

    async function getDataBaseList(requestKind: RequestKind, args?: GetDatabaseListInputParams): Promise<GetDatabaseListDataModel> {
        return getMSDatabaseManagementProxy().getDataBaseList(requestKind, {
            'page-size': args!.size,
            page: args!.page,
            'user-id': args!.userid
        }).then(response => getDatabaseListMapFrom(response));
    }

    async function getAccountRelatedUsers(requestKind: RequestKind, args?: AccountRelatedUsersParams): Promise<AccountRelatedUsersDataModel> {
        return getMSDatabaseManagementProxy().getAccountRelatedUsers(requestKind, args!).then(response => getAccountRelatedUsersMapFrom(response));
    }

    async function getUserWithRent1C(requsetKind: RequestKind, args?: GetUserWithRent1CParams): Promise<GetUserWithRent1CDataModel> {
        return getUserManagementApiProxy().getUserWithRent1C(requsetKind, args!).then(response => getUserWithRent1CMapFrom(response));
    }

    return {
        getDataBaseList,
        deleteUserById,
        setUserActivity,
        IsEmailAvailable,
        IsPhoneAvailable,
        IsUserLoginAviable,
        addNewUser,
        editUser,
        checkCurrentPassword,
        getAccountRelatedUsers,
        getUserWithRent1C
    };
})();