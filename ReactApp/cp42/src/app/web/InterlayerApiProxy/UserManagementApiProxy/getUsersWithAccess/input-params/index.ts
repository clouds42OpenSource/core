/**
 * Параметры для получения списка баз
 */
export type AccountRelatedUsersParams = {
    /** Количество на странице */
    'page-size': number;
    /** Страница */
    page: number;
    /** Признак связи с тек. аккаунтом */
    'account-related': boolean;
}