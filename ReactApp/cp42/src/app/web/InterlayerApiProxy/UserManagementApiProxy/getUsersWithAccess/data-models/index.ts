export type AccountRelatedUsersItemDataModel = {
    /* Id пользователя */
    id: string;
    /* Имя или логин */
    name: string;
    /* Признак связи с аккаунтом */
    accountRelated: boolean
};

/**
 * Модель пагинированного списка пользователей с доступом к базам текущего пользователя
 */
export type AccountRelatedUsersDataModel = {
    pages: number;
    recordsCount: number;
    users: AccountRelatedUsersItemDataModel[]
};