import { AccountRelatedUsersDataModel } from 'app/web/InterlayerApiProxy/UserManagementApiProxy/getUsersWithAccess/data-models';
import { AccountRelatedUsersResponseDto } from 'app/web/api/UserManagementProxy/response-dto/GetUsersWithAccessToDBResponseDto';

/**
 * Маппинг пользователей связанных с аккаунтом
 * @param value Response dto поставщика
 * @returns Data model поставщика
 */
export function getAccountRelatedUsersMapFrom(value: AccountRelatedUsersResponseDto): AccountRelatedUsersDataModel {
    return {
        pages: value.pages,
        recordsCount: value['records-count'],
        users: value.users.map(item => {
            return {
                accountRelated: item['account-related'],
                id: item.id,
                name: item.name
            };
        })
    };
}