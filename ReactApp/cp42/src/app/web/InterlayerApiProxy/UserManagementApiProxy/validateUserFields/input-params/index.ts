/**
 * Параметры для валидации логина
 */
export type ValidateUserLoginParams = {
    Login: string;
    userId: string;
};

/**
 * Параметры для валидации почты
 */
export type ValidateUserEmailParams = {
    Email: string;
    userId: string;
};

/**
 * Параметры для валидации тел. номера
 */
export type ValidateUserPhoneParams = {
    PhoneNumber: string;
    userId: string;
};