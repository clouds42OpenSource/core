/**
 * Параметры для проверки текущкего пароля
 */
export type CheckCurrentPasswordInputParams = {
    /** ID пользователя */
    userId: string;
    /** Текущий пароль */
    currentPassword: string;
};