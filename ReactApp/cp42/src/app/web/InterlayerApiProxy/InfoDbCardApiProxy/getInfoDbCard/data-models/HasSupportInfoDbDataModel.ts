import { SupportHistories } from 'app/web/api/InfoDbCardProxy/responce-dto/SupportHistories';

/**
 * Модель вкладки Авторизации информационной базы
 */
export type HasSupportInfoDbDataModel = {
  acDbSupportHistories: SupportHistories[];
  databaseId: string;
  isConnects: boolean;
  isAuthorized: boolean;
  lastTehSupportDate: Date | undefined;
  login: string | undefined;
  password: string | undefined;
  supportState: number;
  supportStateDescription: string;
  completeSession: boolean;
  timeOfUpdate: number;
  hasAcDbSupport: boolean;
  hasAutoUpdate: boolean;
  hasSupport: boolean;
  hasModifications: boolean;
}