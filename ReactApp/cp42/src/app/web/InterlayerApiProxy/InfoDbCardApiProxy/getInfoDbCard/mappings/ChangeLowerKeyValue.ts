export function changeLowerKeyValue(obj: any) {
    const result = {} as any;
    for (const prop in obj) {
        result[prop] = obj[prop].map((item: any) => {
            return { key: item.Key.toString(), value: item.Value };
        });
    }

    return result;
}