export type BuckupItemInfoDbDataModel = {
  backupPath: string;
  creationDate: Date | undefined;
  eventTrigger: number;
  eventTriggerDescription: string;
  id: string;
  initiator: string;
  isDbOnDelimiters: boolean;
  sourceType: number;
}

/**
 * Модель вкладки Архивные копии
 */
export type BuckupInfoDbDataModel = {
  databaseBackups: BuckupItemInfoDbDataModel[];
  dbBackupsCount: number;
}