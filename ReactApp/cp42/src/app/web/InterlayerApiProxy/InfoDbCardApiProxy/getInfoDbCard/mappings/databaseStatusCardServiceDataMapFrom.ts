import { DateUtility } from 'app/utils';
import { GetStatusServiceInfoDbResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetStatusServiceInfoDbResponseDto';
import { StatusServiceInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/StatusServiceInfoDbDataModel';

/**
 * Mapping модели статуса данных сервисов карточки
 * @param value Модель получения статуса сервиса
 */
export function databaseStatusCardServiceDataMapFrom(value: GetStatusServiceInfoDbResponseDto): StatusServiceInfoDbDataModel {
    return {
        extensionDatabaseStatus: value.ExtensionDatabaseStatus,
        setStatusDateTime: value.SetStatusDateTime ? DateUtility.convertToDate(value.SetStatusDateTime) : null,
        serviceId: value.serviceId
    };
}