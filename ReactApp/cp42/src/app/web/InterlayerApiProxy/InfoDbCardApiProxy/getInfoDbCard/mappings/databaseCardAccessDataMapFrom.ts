import { GetAccessCardResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetAccessCardResponseDto';
import { GetAccessCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';
import { databaseCardAccessItemDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardAccessItemDataMapFrom';

/**
 * Mapping модели данных настроек доступа
 * @param value Модель по настройке по показу общих данных во вкладке Настройки доступа
 */
export function databaseCardAccessDataMapFrom(value: GetAccessCardResponseDto): GetAccessCardDataModel {
    return {
        currency: value.Currency,
        clientServerAccessCost: value.ClientServerAccessCost,
        databaseAccesses: !value?.DatabaseAccesses ? [] : value.DatabaseAccesses.map(item => databaseCardAccessItemDataMapFrom(item)),
        rateData: {
            totalAmount: value.RateData.TotalAmount,
            systemServiceType: value.RateData.SystemServiceType,
            serviceId: value.RateData.ServiceId,
            billingType: value.RateData.BillingType,
            clouds42ServiceType: value.RateData.Clouds42ServiceType,
            costPerOneLicense: value.RateData.CostPerOneLicense,
            isActiveService: value.RateData.IsActiveService,
            limitOnFreeLicenses: value.RateData.LimitOnFreeLicenses,
            serviceName: value.RateData.ServiceName,
            serviceTypeDescription: value.RateData.ServiceTypeDescription,
            serviceTypeId: value.RateData.ServiceTypeId,
            serviceTypeName: value.RateData.ServiceTypeName,
            volumeInQuantity: value.RateData.VolumeInQuantity
        }
    };
}