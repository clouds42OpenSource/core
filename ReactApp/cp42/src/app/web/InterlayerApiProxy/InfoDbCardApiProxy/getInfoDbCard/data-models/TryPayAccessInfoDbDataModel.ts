/**
 * Модель попытки оплаты доступа к информационной базе
 */
export type TryPayAccessInfoDbDataModel = {
  amount: number;
  canUsePromisePayment: boolean;
  currency: string;
  isComplete: boolean;
  notEnoughMoney: number;
  paymentIds: string[];
}