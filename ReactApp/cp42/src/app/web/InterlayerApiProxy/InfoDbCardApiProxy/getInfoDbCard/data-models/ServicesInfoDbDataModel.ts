/**
 * Модель отображения сервиса информационной базы
 */
export type ServicesInfoDbDataModel = {
    extensionLastActivityDate: Date | null;
    extensionState: number | null;
    id: string;
    isInstalled: boolean;
    isActiveService: boolean;
    isFrozenService: boolean;
    name: string;
    shortDescription: string;
}