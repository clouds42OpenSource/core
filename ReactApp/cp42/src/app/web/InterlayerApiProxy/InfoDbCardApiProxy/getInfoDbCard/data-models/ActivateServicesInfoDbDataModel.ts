/**
 * Активация сервиса
 */
export type ActivateServicesInfoDbDataModel = Partial<{
  demoPeriodEndDate: string,
  isAlreadyActivated: boolean,
  serviceName: string,
  errorMessage: string
}>