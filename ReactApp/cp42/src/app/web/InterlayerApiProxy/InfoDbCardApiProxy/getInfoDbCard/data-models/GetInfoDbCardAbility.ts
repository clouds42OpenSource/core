import { keyValueLowerForStringType, keyValueLowerType } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/keyValueLowerType';
/**
 * Модель данных с возможными значениями полей информационной базы
 */
export type GetInfoDbCardAbility = {
   AvailableAccountDatabaseRestoreModelTypes: keyValueLowerType[];
   AvailableDatabaseStatuses: keyValueLowerType[];
   AvailableDbTemplates: keyValueLowerForStringType[];
   AvailablePlatformTypes: keyValueLowerType[];
   ServiceExtensionsForDatabaseTypes: keyValueLowerType[];
}