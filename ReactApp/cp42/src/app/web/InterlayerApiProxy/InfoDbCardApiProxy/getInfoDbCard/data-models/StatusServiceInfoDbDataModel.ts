/**
 * Модель статуса сервиса информационной базы
 */
export type StatusServiceInfoDbDataModel = {
  extensionDatabaseStatus: number;
  setStatusDateTime: Date | null;
  serviceId: string;
}