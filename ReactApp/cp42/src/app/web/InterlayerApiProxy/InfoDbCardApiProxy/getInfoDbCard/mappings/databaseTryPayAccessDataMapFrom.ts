import { GetTryPayAccessesResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetTryPayAccessesResponseDto';
import { TryPayAccessInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/TryPayAccessInfoDbDataModel';

/**
 * Mapping модели попытки оплаты доступов
 * @param value Модель ответа попытки оплаты доступов
 */
export function databaseTryPayAccessDataMapFrom(value: GetTryPayAccessesResponseDto): TryPayAccessInfoDbDataModel {
    return {
        amount: value.amount,
        canUsePromisePayment: value.canUsePromisePayment,
        currency: value.currency,
        isComplete: value.isComplete,
        notEnoughMoney: value.notEnoughMoney,
        paymentIds: value.paymentIds,
    };
}