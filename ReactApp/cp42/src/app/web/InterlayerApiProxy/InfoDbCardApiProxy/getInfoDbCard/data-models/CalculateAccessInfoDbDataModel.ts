export type CalculateAccessInfoDbDataModelItem = {
  accessCost: number;
  accountUserId: string;
  hasLicense: boolean;
}

/**
 * Модель подсчета стоимости доступа к информационной базе
 */
export type CalculateAccessInfoDbDataModel = CalculateAccessInfoDbDataModelItem[];