import { DateUtility } from 'app/utils';
import { GetBuckupsInfoDbResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetBuckupsInfoDbResponseDto';
import { BuckupInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/BuckupInfoDbDataModel';

/**
 * Mapping модели с информацией о бекапах
 * @param value Модель с информацией о бекапах с сервера
 */
export function databaseBuckupsInfoDbDataMapFrom(value: GetBuckupsInfoDbResponseDto): BuckupInfoDbDataModel {
    return {
        databaseBackups: value.DatabaseBackups.length ? [...value.DatabaseBackups.map(i => {
            return {
                backupPath: i.BackupPath,
                creationDate: DateUtility.convertToDate(i.CreationDate),
                eventTrigger: i.EventTrigger,
                eventTriggerDescription: i.EventTriggerDescription,
                id: i.Id,
                initiator: i.Initiator,
                isDbOnDelimiters: i.IsDbOnDelimiters,
                sourceType: i.SourceType
            };
        })] : [],
        dbBackupsCount: value.DbBackupsCount
    };
}