import { DateUtility } from 'app/utils';
import { GetInfoDbCard } from 'app/web/api/InfoDbCardProxy/responce-dto/GetInfoDbCard';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { changeLowerKeyValue } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/ChangeLowerKeyValue';

/**
 * Mapping модели общей информации о базе
 * @param value Модель общей информации о базе полученной при запросе
 */
export function infoDbCardModelMapFrom(value: GetInfoDbCard | null): InfoDbCardAccountDatabaseInfoDataModel | null {
    return value ? {
        accountCaption: value.AccountCaption,
        actualDatabaseBackupId: value.ActualDatabaseBackupId,
        alpha83Version: value.Alpha83Version,
        archivePath: value.ArchivePath,
        availableFileStorages: value.AvailableFileStorages,
        backupDate: value.BackupDate ? DateUtility.convertToDate(value.BackupDate) : '',
        calculateSizeDateTime: value.CalculateSizeDateTime,
        canChangeRestoreModel: value.CanChangeRestoreModel,
        canEditDatabase: value.CanEditDatabase,
        canWebPublish: value.CanWebPublish,
        caption: value.Caption,
        configurationCode: value.ConfigurationCode,
        cloudStorageWebLink: value.CloudStorageWebLink,
        configurationName: value.ConfigurationName,
        creationDate: DateUtility.convertToDate(value.CreationDate),
        databaseConnectionPath: value.DatabaseConnectionPath,
        databaseState: value.DatabaseState,
        dbNumber: value.DbNumber,
        dbTemplate: value.DbTemplate,
        distributionType: value.DistributionType,
        filePath: value.FilePath,
        fileStorageId: value.FileStorageId,
        fileStorageName: value.FileStorageName,
        id: value.Id,
        isDbOnDelimiters: value.IsDbOnDelimiters,
        isDemoDelimiters: value.IsDemoDelimiters,
        isExistTerminatingSessionsProcessInDatabase: value.IsExistTerminatingSessionsProcessInDatabase,
        isFile: value.IsFile,
        isStatusErrorCreated: value.IsStatusErrorCreated,
        createDatabaseComment: value.CreateDatabaseComment,
        lastEditedDateTime: value.LastEditedDateTime,
        myDatabasesServiceTypeId: value.MyDatabasesServiceTypeId,
        platformType: value.PlatformType,
        publishState: value.PublishState,
        restoreModelType: value.RestoreModelType,
        sizeInMb: value.SizeInMb,
        sqlServer: value.SqlServer,
        stable82Version: value.Stable82Version,
        stable83Version: value.Stable83Version,
        templateName: value.TemplateName,
        templatePlatform: value.TemplatePlatform,
        usedWebServices: value.UsedWebServices,
        v82Name: value.V82Name,
        v82Server: value.V82Server,
        version: value.Version,
        webPublishPath: value.WebPublishPath,
        zoneNumber: value.ZoneNumber,
        commonDataForWorkingWithDB: changeLowerKeyValue(value.CommonDataForWorkingWithDB),
        databaseOperations: value.DatabaseOperations.map(item => {
            return {
                cost: item.Cost,
                currency: item.Currency,
                id: item.Id,
                name: item.Name
            };
        })
    } : null;
}