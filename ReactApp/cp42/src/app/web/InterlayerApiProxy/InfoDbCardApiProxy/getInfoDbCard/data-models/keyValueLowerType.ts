/**
 * Модель ключей к полям информационной базы
 */
export type keyValueLowerType = {
   key: number;
   value: string;
}

export type keyValueLowerForStringType = {
   key: string;
   value: string;
}