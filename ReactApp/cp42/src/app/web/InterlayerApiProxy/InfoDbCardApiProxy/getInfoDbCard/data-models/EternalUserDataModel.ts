export type EternalUserDataModel = {
   accessCost?: number;
   accountCaption?: string;
   accountIndexNumber?: number;
   accountInfo?: string;
   hasLicense?: boolean;
   hasAccess?: boolean;
   userEmail?: string;
   userFirstName?: string | null;
   userFullName?: string;
   userId?: string;
   userLastName?: string | null;
   userLogin?: string;
   userMiddleName?: string | null;
   message?: string;
}

/**
 * Модель внешнего пользователя информационной базы
 */
export type GeneralEternalUserDataModel = EternalUserDataModel;