import {
    GetInfoDbCardAbility
} from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/GetInfoDbCardAbility';

export type DatabaseOperationsItem = {
    cost: number;
    currency: string;
    id: string;
    name: string;
};

/**
 * Модель отображения информационной базы
 */
export type InfoDbCardAccountDatabaseInfoDataModel = {
    accountCaption: string;
    actualDatabaseBackupId: string;
    alpha83Version: string;
    archivePath: string;
    availableFileStorages: { key: string, value: string }[];
    backupDate: Date | string;
    calculateSizeDateTime: string;
    canChangeRestoreModel: boolean;
    canEditDatabase: boolean;
    canWebPublish: boolean;
    caption: string;
    cloudStorageWebLink: string;
    configurationName: string;
    creationDate: Date;
    databaseConnectionPath: string;
    databaseState: number;
    dbNumber: number;
    dbTemplate: string;
    distributionType: number;
    filePath: string;
    fileStorageId: string;
    fileStorageName: string;
    id: string;
    isDbOnDelimiters: boolean;
    configurationCode: string | null;
    isDemoDelimiters: boolean;
    isExistTerminatingSessionsProcessInDatabase: boolean;
    isFile: boolean;
    isStatusErrorCreated: boolean;
    createDatabaseComment?: string;
    lastEditedDateTime: string;
    myDatabasesServiceTypeId: string;
    platformType: number;
    publishState: number;
    restoreModelType: number;
    sizeInMb: number;
    sqlServer: string;
    stable82Version: string;
    stable83Version: string;
    templateName: string;
    templatePlatform: number;
    usedWebServices: boolean;
    v82Name: string;
    v82Server: string;
    version: number;
    webPublishPath: string;
    zoneNumber: number;
    commonDataForWorkingWithDB: GetInfoDbCardAbility;
    databaseOperations: DatabaseOperationsItem[];
};