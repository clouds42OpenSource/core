import { GetCalculateAccessesResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetCalculateAccessesResponseDto';
import { CalculateAccessInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/CalculateAccessInfoDbDataModel';

/**
 * Mapping модели данных подсчета стоимости доступа
 * @param value Модель данныхсо стоимостью
 */
export function databaseCalculatePriceAccessDataMapFrom(value: GetCalculateAccessesResponseDto): CalculateAccessInfoDbDataModel {
    return value.map(i => {
        return {
            accessCost: i.AccessCost,
            accountUserId: i.AccountUserId,
            hasLicense: i.HasLicense
        };
    });
}