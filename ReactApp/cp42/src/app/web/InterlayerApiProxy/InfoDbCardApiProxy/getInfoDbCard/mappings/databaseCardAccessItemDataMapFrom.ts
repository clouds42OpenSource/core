import { DatabaseAccesses } from 'app/web/api/InfoDbCardProxy/responce-dto/GetAccessCardResponseDto';
import { DatabaseAccessesDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/AccessInfoDbDataModel';

/**
 * Mapping модели данных вкладки Настройки доступа
 * @param value Модель по настройке по показу общих данных во вкладке Настройки доступа
 */
export function databaseCardAccessItemDataMapFrom(value: DatabaseAccesses): DatabaseAccessesDataModel {
    return {
        accountCaption: value.AccountCaption,
        accountIndexNumber: value.AccountIndexNumber,
        accountInfo: value.AccountInfo,
        delayReason: value.DelayReason,
        hasAccess: value.HasAccess,
        isExternalAccess: value.IsExternalAccess,
        state: value.State,
        userEmail: value.UserEmail,
        userFirstName: value.UserFirstName,
        userFullName: value.UserFullName,
        userId: value.UserId,
        userLastName: value.UserLastName,
        userLogin: value.UserLogin,
        userMiddleName: value.UserMiddleName
    };
}