import { GetEternalUserResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetEternalUserResponseDto';
import { GeneralEternalUserDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/EternalUserDataModel';

/**
 * Mapping модели данных авторизации карточки
 * @param value Модель по получению внешнего юзера во вкладке Настройки доступа
 */
export function databaseCardEternalUserDataMapFrom(value: { Data: GetEternalUserResponseDto } & { Message: string | null }): GeneralEternalUserDataModel {
    if (!value.Message) {
        return {
            accessCost: value.Data.AccessCost,
            accountCaption: value.Data.AccountCaption,
            accountIndexNumber: value.Data.AccountIndexNumber,
            accountInfo: value.Data.AccountInfo,
            hasLicense: value.Data.HasLicense,
            hasAccess: value.Data.HasAccess,
            userEmail: value.Data.UserEmail,
            userFirstName: value.Data.UserFirstName,
            userFullName: value.Data.UserFullName,
            userId: value.Data.UserId,
            userLastName: value.Data.UserLastName,
            userLogin: value.Data.UserLogin,
            userMiddleName: value.Data.UserMiddleName,
            message: value.Message || ''
        };
    }

    return { message: value.Message };
}