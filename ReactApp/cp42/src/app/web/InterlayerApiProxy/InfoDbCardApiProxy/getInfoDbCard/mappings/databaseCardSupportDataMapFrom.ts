import { DateUtility } from 'app/utils';
import { GetSupportDataResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetSupportDataResponseDto';
import { HasSupportInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/HasSupportInfoDbDataModel';

/**
 * Mapping модели данных авторизации карточки
 * @param value Модель по настройке по показу общих данных во вкладке Авторизация
 */
export function databaseCardSupportDataMapFrom(value: GetSupportDataResponseDto): HasSupportInfoDbDataModel {
    return {
        acDbSupportHistories: value.AcDbSupportHistories,
        databaseId: value.DatabaseId,
        isConnects: value.IsConnects,
        isAuthorized: value.IsAuthorized,
        lastTehSupportDate: value.LastTehSupportDate ? DateUtility.convertToDate(value.LastTehSupportDate) : undefined,
        login: value.Login,
        password: value.Password,
        supportState: value.SupportState,
        supportStateDescription: value.SupportStateDescription,
        completeSession: value.CompleteSessioin,
        timeOfUpdate: value.TimeOfUpdate,
        hasAcDbSupport: value.HasAcDbSupport,
        hasAutoUpdate: value.HasAutoUpdate,
        hasSupport: value.HasSupport,
        hasModifications: value.HasModifications,
    };
}