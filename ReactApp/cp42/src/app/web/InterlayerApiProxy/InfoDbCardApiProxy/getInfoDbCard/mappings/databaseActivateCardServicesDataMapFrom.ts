import { GetActivateServicesInfoDbResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetActivateServicesInfoDbResponseDto';
import { ActivateServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ActivateServicesInfoDbDataModel';

/**
 * Mapping модели данных сервисов карточки
 * @param value Модель по подключения демо периода во вкладке Сервисы
 */
export function databaseActivateCardServicesDataMapFrom(value: GetActivateServicesInfoDbResponseDto): ActivateServicesInfoDbDataModel {
    return {
        demoPeriodEndDate: value.DemoPeriodEndDate,
        isAlreadyActivated: value.IsAlreadyActivated,
        serviceName: value.ServiceName,
        errorMessage: value.ErrorMessage
    };
}