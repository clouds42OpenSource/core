import { DateUtility } from 'app/utils';
import { GetServicesInfoDbResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/GetServicesInfoDbResponseDto';
import { ServicesInfoDbDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/ServicesInfoDbDataModel';

/**
 * Mapping модели данных сервисов карточки
 * @param value Модель по настройке по показу общих данных во вкладке Сервисы
 */
export function databaseCardServicesDataMapFrom(value: GetServicesInfoDbResponseDto[]): ServicesInfoDbDataModel[] {
    return value.length
        ? value.map(item => {
            return {
                extensionLastActivityDate: item.ExtensionLastActivityDate ? DateUtility.convertToDate(item.ExtensionLastActivityDate) : null,
                extensionState: item.ExtensionState,
                id: item.Id,
                isInstalled: item.IsInstalled,
                isActiveService: item.IsActiveService,
                isFrozenService: item.IsFrozenService,
                name: item.Name,
                shortDescription: item.ShortDescription
            };
        })
        : [];
}