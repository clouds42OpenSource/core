import { Nullable } from 'app/common/types';

export type ArchiveOrBackupDataModel = {
    success: boolean;
    message: string;
    data: {
        errorMessage: string;
        isComplete: boolean;
        notEnoughMoney: number;
        amount: number;
        currency: Nullable<string>;
        canUsePromisePayment: boolean;
    }
};