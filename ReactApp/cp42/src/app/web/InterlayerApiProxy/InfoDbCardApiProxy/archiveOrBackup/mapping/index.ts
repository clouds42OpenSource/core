import { ArchiveOrBackupsResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/ArchiveOrBackupsResponseDto';
import { ArchiveOrBackupDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/archiveOrBackup';

export function archiveOrBackupModelMapFrom(value: ArchiveOrBackupsResponseDto): ArchiveOrBackupDataModel {
    return {
        success: value.Success,
        message: value.Message,
        data: {
            errorMessage: value.Data.ErrorMessage,
            isComplete: value.Data.IsComplete,
            notEnoughMoney: value.Data.NotEnoughMoney,
            amount: value.Data.Amount,
            currency: value.Data.Currency,
            canUsePromisePayment: value.Data.CanUsePromisePayment
        }
    };
}