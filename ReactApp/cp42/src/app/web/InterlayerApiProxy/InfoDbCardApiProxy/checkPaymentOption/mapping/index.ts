import { CheckPaymentOptionResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/CheckPaymentOptionResponseDto';
import { CheckPaymentOptionDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/checkPaymentOption/data-model';

/**
 * Маппинг модели валидации возможности оплатить перенос ИБ в бекап или архив в модель приложения
 * @param value модель ответа
 */
export function checkPaymentOptionModelMapFrom(value: CheckPaymentOptionResponseDto): CheckPaymentOptionDataModel {
    return {
        complete: value.Complete,
        errorMessage: value.ErrorMessage,
        notEnoughMoney: value.NotEnoughMoney,
        amount: value.Amount,
        currency: value.Currency,
        canUsePromisePayment: value.CanUsePromisePayment,
        canIncreasePromisePayment: value.CanIncreasePromisePayment
    };
}