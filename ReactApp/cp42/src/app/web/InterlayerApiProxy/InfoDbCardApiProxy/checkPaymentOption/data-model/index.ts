export type CheckPaymentOptionDataModel = {
    complete: boolean;
    errorMessage: string;
    notEnoughMoney: number;
    amount: number;
    currency: string;
    canUsePromisePayment: boolean;
    canIncreasePromisePayment: boolean;
};