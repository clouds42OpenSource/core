import { IoCContainer } from 'app/IoCСontainer';
import { ArchiveOrBackupRequestDto } from 'app/web/api/InfoDbCardProxy/request-dto/ArchiveOrBackupRequestDto';
import { CheckPaymentOptionRequestDto } from 'app/web/api/InfoDbCardProxy/request-dto/CheckPaymentOptionRequestDto';
import { ClearCacheDto } from 'app/web/api/InfoDbCardProxy/request-dto/ClearCacheDto';
import { InputActivateServicesInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputActivateServicesInfoDbCard';
import { InputAuthInfoDb } from 'app/web/api/InfoDbCardProxy/request-dto/InputAuthInfoDb';
import { InputBuckupsInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputBuckupsInfoDbCard';
import { InputCalculateAccesses } from 'app/web/api/InfoDbCardProxy/request-dto/InputCalculateAccesses';
import { InputChangeServicesInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputChangeServicesInfoDbCard';
import { InputDatabaseId } from 'app/web/api/InfoDbCardProxy/request-dto/InputDatabaseId';
import { InputEditAccessUserDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputEditAccessUserDbCard';
import { InputEditPlatformInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputEditPlatformInfoDbCard';
import { InputExternalUserDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputExternalUserDbCard';
import { InputGetAccessCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputGetAccessCard';
import { InputGetServicesInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputGetServicesInfoDbCard';
import { InputInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputInfoDbCard';
import { InputPublishInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputPublishInfoDbCard';
import { InputRightsInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputRightsInfoDbCard';
import { InputStatusServiceInfoDbCard } from 'app/web/api/InfoDbCardProxy/request-dto/InputStatusServiceInfoDbCard';
import { InputTryPayAccesses } from 'app/web/api/InfoDbCardProxy/request-dto/InputTryBuyAccesses';
import { ArchiveOrBackupDataModel, archiveOrBackupModelMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/archiveOrBackup';
import { CheckPaymentOptionDataModel, checkPaymentOptionModelMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/checkPaymentOption';
import { InfoDbCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { databaseActivateCardServicesDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseActivateCardServicesDataMapFrom';
import { databaseBuckupsInfoDbDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseBuckupsInfoDbDataMapFrom';
import { databaseCalculatePriceAccessDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCalculatePriceAccessDataMapFrom';
import { databaseCardAccessDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardAccessDataMapFrom';
import { infoDbCardModelMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardAccountDatabaseInfoDataModelMapFrom';
import { databaseCardEternalUserDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardEternalUserDataMapFrom';
import { databaseCardServicesDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardServicesDataMapFrom';
import { databaseCardSupportDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseCardSupportDataMapFrom';
import { databaseStatusCardServiceDataMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getInfoDbCard/mappings/databaseStatusCardServiceDataMapFrom';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';
import { rightsInfoDbCardModelMapFrom } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/mappings';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

export const InfoDbCardApiProxy = (() => {
    function getInfoDbApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getInfoDbCardApiProxy();
    }

    async function getInfoDbCard(requestKind: RequestKind, args: InputInfoDbCard): Promise<InfoDbCardAccountDatabaseInfoDataModel | null | string> {
        return getInfoDbApiProxy().getInfoDbCardList(
            requestKind,
            {
                accountId: args.accountId,
                databaseId: args.databaseId
            }
        ).then(responseDto => {
            return responseDto.Success ? infoDbCardModelMapFrom(responseDto.Data) : responseDto.Message;
        });
    }

    async function getRightsInfoDbCard(requestKind: RequestKind, args: InputRightsInfoDbCard): Promise<RightsInfoDbCardDataModel> {
        return getInfoDbApiProxy().getRightsInfoDbCard(
            requestKind,
            {
                databaseId: args.databaseId,
                objectActions: args.objectActions

            }
        ).then(responseDto => {
            return rightsInfoDbCardModelMapFrom(responseDto.Data);
        });
    }

    async function editPlatformInfoDbCard(requestKind: RequestKind, args: InputEditPlatformInfoDbCard) {
        return getInfoDbApiProxy().editPlatformInfoDbCard(
            requestKind,
            {
                databaseId: args.databaseId,
                platformType: args.platformType,
                distributionType: args.distributionType

            }
        );
    }

    async function publishInfoDbCard(requestKind: RequestKind, args: InputPublishInfoDbCard) {
        return getInfoDbApiProxy().publishInfoDbCard(
            requestKind,
            {
                databaseId: args.databaseId,
                publish: args.publish

            }
        ).then(responseDto => responseDto.Success);
    }

    async function sessionInfoDbCard(requestKind: RequestKind, args: InputDatabaseId) {
        return getInfoDbApiProxy().sessionInfoDbCard(
            requestKind,
            {
                databaseId: args.databaseId,
            }
        ).then(responseDto => responseDto.Success);
    }

    async function typeInfoDbCard(requestKind: RequestKind, args: InputDatabaseId) {
        return getInfoDbApiProxy().typeInfoDbCard(requestKind, args)
            .then(responseDto => responseDto.Success);
    }

    async function clearCache(requestKind: RequestKind, args: ClearCacheDto) {
        return getInfoDbApiProxy().clearCache(
            requestKind,
            {
                AccountDatabaseId: args.AccountDatabaseId,
                ClearDatabaseLogs: args.ClearDatabaseLogs
            }
        ).then(responseDto => responseDto);
    }

    async function restartIISPollInfoDbCard(requestKind: RequestKind, args: InputDatabaseId) {
        return getInfoDbApiProxy().restartIISPollInfoDbCard(
            requestKind,
            {
                databaseId: args.databaseId,
            }
        ).then(responseDto => responseDto.Success);
    }

    async function getSupportDataCard(requestKind: RequestKind, args: IForceThunkParam & InputDatabaseId) {
        return getInfoDbApiProxy().getSupportDataCard(
            requestKind,
            {
                databaseId: args.databaseId,
            }
        ).then(responseDto => databaseCardSupportDataMapFrom(responseDto.Data || {}));
    }

    async function AuthInfoDbCard(requestKind: RequestKind, args: InputAuthInfoDb) {
        return getInfoDbApiProxy().AuthInfoDbCard(requestKind, args).then(responseDto => responseDto);
    }

    async function DeauthInfoDbCard(requestKind: RequestKind, args: { databaseId: string; }) {
        return getInfoDbApiProxy().DeauthInfoDbCard(
            requestKind,
            {
                databaseId: args.databaseId
            }
        ).then(responseDto => responseDto.Success);
    }

    async function GetAccessInfoDbCard(requestKind: RequestKind, args: IForceThunkParam & InputGetAccessCard) {
        return getInfoDbApiProxy().GetAccessInfoDbCard(
            requestKind,
            {
                DatabaseId: args.DatabaseId,
                SearchString: args.SearchString,
                UsersByAccessFilterType: args.UsersByAccessFilterType
            }
        ).then(responseDto => databaseCardAccessDataMapFrom(responseDto.Data));
    }

    async function GetExternalUserInfoDbCard(requestKind: RequestKind, args: InputExternalUserDbCard) {
        return getInfoDbApiProxy().GetExternalUserInfoDbCard(
            requestKind,
            {
                databaseId: args.databaseId,
                accountUserEmail: args.accountUserEmail,
            }
        ).then(responseDto => databaseCardEternalUserDataMapFrom(responseDto));
    }

    async function EditAccessUserInfoDbCard(requestKind: RequestKind, args: InputEditAccessUserDbCard) {
        return getInfoDbApiProxy().EditAccessUserInfoDbCard(
            requestKind,
            {
                DatabaseId: args.DatabaseId,
                UsersId: args.UsersId,
                giveAccess: args.giveAccess
            }
        ).then(responseDto => responseDto);
    }

    async function GetServicesInfoDbCard(requestKind: RequestKind, args: InputGetServicesInfoDbCard) {
        return getInfoDbApiProxy().GetServicesInfoDbCard(
            requestKind,
            {
                AccountDatabaseId: args.AccountDatabaseId,
                AccountId: args.AccountId,
                ServiceExtensionsForDatabaseType: args.ServiceExtensionsForDatabaseType,
                ServiceName: args.ServiceName

            }
        ).then(responseDto => databaseCardServicesDataMapFrom(responseDto.Data ?? []));
    }

    async function ActivateServicesInfoDbCard(requestKind: RequestKind, args: InputActivateServicesInfoDbCard) {
        return getInfoDbApiProxy().ActivateServicesInfoDbCard(
            requestKind,
            {
                accountDatabaseId: args.accountDatabaseId,
                serviceId: args.serviceId,
                accountId: args.accountId
            }
        ).then(responseDto => {
            if (responseDto.Data) {
                return databaseActivateCardServicesDataMapFrom(responseDto.Data);
            }
            throw Error(responseDto.Message ?? '');
        });
    }

    async function ChangeConnectServicesInfoDbCard(requestKind: RequestKind, args: InputChangeServicesInfoDbCard) {
        return getInfoDbApiProxy().ChangeConnectServicesInfoDbCard(
            requestKind,
            {
                DatabaseId: args.DatabaseId,
                ServiceId: args.ServiceId,
                Install: args.Install
            }
        ).then(responseDto => responseDto);
    }

    async function GetStatusServicesInfoDbCard(requestKind: RequestKind, args: InputStatusServiceInfoDbCard) {
        return getInfoDbApiProxy().GetStatusServicesInfoDbCard(
            requestKind,
            {
                accountDatabaseId: args.accountDatabaseId,
                serviceId: args.serviceId

            }
        ).then(responseDto => databaseStatusCardServiceDataMapFrom({ ...responseDto.Data, serviceId: args.serviceId }));
    }

    async function GetBackupsInfoDbCard(requestKind: RequestKind, args: InputBuckupsInfoDbCard) {
        return getInfoDbApiProxy().GetBackupsInfoDbCard(
            requestKind,
            {
                DatabaseId: args.DatabaseId,
                CreationDateTo: args.CreationDateTo,
                CreationDateFrom: args.CreationDateFrom
            }
        ).then(responseDto => databaseBuckupsInfoDbDataMapFrom(responseDto.Data));
    }

    async function CalculateAccesses(requestKind: RequestKind, args: InputCalculateAccesses) {
        return getInfoDbApiProxy().CalculateAccesses(
            requestKind,
            {
                serviceTypesIdsList: args.serviceTypesIdsList,
                accountUserDataModelsList: args.accountUserDataModelsList,
            }
        ).then(responseDto => databaseCalculatePriceAccessDataMapFrom(responseDto.Data));
    }

    async function TryPayAccesses(requestKind: RequestKind, args: InputTryPayAccesses) {
        return getInfoDbApiProxy().TryPayAccesses(
            requestKind,
            {
                accountId: args.accountId,
                billingServiceId: args.billingServiceId,
                changedUsers: args.changedUsers,
                usePromisePayment: args.usePromisePayment
            }
        );
    }

    async function CheckPaymentOption(requestKind: RequestKind, args: CheckPaymentOptionRequestDto): Promise<CheckPaymentOptionDataModel> {
        return getInfoDbApiProxy().CheckPaymentOption(requestKind, args)
            .then(response => checkPaymentOptionModelMapFrom(response));
    }

    async function ArchiveOrBackup(requestKind: RequestKind, args: ArchiveOrBackupRequestDto): Promise<ArchiveOrBackupDataModel> {
        return getInfoDbApiProxy().ArchiveOrBackups(requestKind, args)
            .then(response => archiveOrBackupModelMapFrom(response));
    }

    return {
        getInfoDbCard,
        getRightsInfoDbCard,
        editPlatformInfoDbCard,
        publishInfoDbCard,
        sessionInfoDbCard,
        typeInfoDbCard,
        restartIISPollInfoDbCard,
        getSupportDataCard,
        AuthInfoDbCard,
        DeauthInfoDbCard,
        GetAccessInfoDbCard,
        GetExternalUserInfoDbCard,
        EditAccessUserInfoDbCard,
        GetServicesInfoDbCard,
        ActivateServicesInfoDbCard,
        ChangeConnectServicesInfoDbCard,
        GetStatusServicesInfoDbCard,
        GetBackupsInfoDbCard,
        CalculateAccesses,
        TryPayAccesses,
        CheckPaymentOption,
        ArchiveOrBackup,
        clearCache
    };
})();