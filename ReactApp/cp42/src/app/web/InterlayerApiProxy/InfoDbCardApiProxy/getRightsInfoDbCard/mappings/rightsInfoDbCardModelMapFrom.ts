import { RightsResponseDto } from 'app/web/api/InfoDbCardProxy/responce-dto/RightsResponseDto';
import { RightsInfoDbCardDataModel } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy/getRightsInfoDbCard/data-models/InfoDbCardAccountDatabaseInfoDataModel';

/**
 * Mapping модели прав для базы
 * @param value Модель прав для базе полученной при запросе
 */
export function rightsInfoDbCardModelMapFrom(value: RightsResponseDto): RightsInfoDbCardDataModel {
    return value
        .filter(item => item.HasAccess)
        .map(item => item.ObjectAction) ||
        [];
}