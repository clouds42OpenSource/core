import { IoCContainer } from 'app/IoCСontainer';
import { TaskControlCardInfoDataModel } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy/receiveTaskControlCard';
import { ReceiveTaskControlCardParams } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy/receiveTaskControlCard/input-params';
import { taskControlCardDataModelMapFrom } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy/receiveTaskControlCard/mappings/taskControlCardDataModelMapFrom';
import { RequestKind } from 'core/requestSender/enums';

export const TaskControlCardApiProxy = (() => {
    function getTaskControlCardApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getTaskControlCardApiProxy();
    }

    async function receiveTaskControlCard(requestKind: RequestKind, args: ReceiveTaskControlCardParams): Promise<TaskControlCardInfoDataModel> {
        return getTaskControlCardApiProxy().receiveTaskControlCard(requestKind, {
            taskId: args.taskId
        }).then(responseDto => taskControlCardDataModelMapFrom(responseDto));
    }

    function updateTaskControlCard(requestKind: RequestKind, args: any): Promise<void> {
        return getTaskControlCardApiProxy().updateTaskControlCard(requestKind, {
            id: args.taskId,
            taskExecutionLifetimeInMinutes: args.taskExecutionLifetimeInMinutes
        });
    }

    return {
        receiveTaskControlCard,
        updateTaskControlCard
    };
})();