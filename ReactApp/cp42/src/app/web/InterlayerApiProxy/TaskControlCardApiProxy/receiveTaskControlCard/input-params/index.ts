/**
 * Параметры для получения карточки управления задачей
 */
export type ReceiveTaskControlCardParams = {
    /**
     * ID задачи, для которой нужно получить карточку
     */
    taskId: string;
};