import { TaskControlCardDataResponseDto } from 'app/web/api/TaskControlCard/response-dto/TaskControlCardDataResponseDto';
import { TaskControlCardInfoDataModel } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy/receiveTaskControlCard';

/**
 * Mapping модели информации для карточки управления задачами
 * @param value Модель общей информации для для карточки управления задачами
 */
export function taskControlCardDataModelMapFrom(value: TaskControlCardDataResponseDto): TaskControlCardInfoDataModel {
    return {
        taskId: value.id,
        taskLifeTimeInMinutes: value.taskExecutionLifetimeInMinutes,
        taskName: value.name
    };
}