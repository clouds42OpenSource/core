/**
 * Модель информации в карчтоке управления задачей
 */
export type TaskControlCardInfoDataModel = {
    /**
     * ID задачи
     */
    taskId:string;
    /**
     * Название задачи
     */
    taskName:string;
    /**
     * Время жизни задачи в минутах
     */
    taskLifeTimeInMinutes: number;
};