import { LogEventProxy } from 'app/web/api/LogEventProxy';
import { IoCContainer } from 'app/IoCСontainer';
import { RequestKind } from 'core/requestSender/enums';
import { ELogEventNumber } from 'app/web/api/LogEventProxy/enum';

export class LogEventApiProxy extends LogEventProxy {
    constructor() {
        const { apiHost, requestSender } = IoCContainer.getApiProxy().ApiProxyVariables;
        super(requestSender, apiHost);
    }

    public createService(serviceName: string) {
        return this.sendLogEvent(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, ELogEventNumber.CreatePrivateService, `Создан сервис "${ serviceName }".`);
    }

    public deleteService(serviceName: string) {
        return this.sendLogEvent(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, ELogEventNumber.DeletePrivateService, `Удален сервис "${ serviceName }".`);
    }

    public changeServiceStatus(serviceName: string, databaseName: string) {
        return this.sendLogEvent(RequestKind.SEND_BY_USER_ASYNCHRONOUSLY, ELogEventNumber.InstallServiceInDatabase, `Изменен статус сервиса "${ serviceName }" в базе "${ databaseName }".`);
    }
}