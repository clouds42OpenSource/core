/**
 * Параметры для перехода к аккаунту
 */
export type GotoToAccountParams = {
    /**
     * ID аккаунта, к которому перейти
     */
    accountId: string;
}