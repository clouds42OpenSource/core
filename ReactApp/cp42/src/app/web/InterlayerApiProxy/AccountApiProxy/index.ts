import { IoCContainer } from 'app/IoCСontainer';
import { DateUtility } from 'app/utils';
import { AccountSummaryDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/getAccountSummaryData/data-models/AccountSummaryDataModel';
import { GotoToAccountParams } from 'app/web/InterlayerApiProxy/AccountApiProxy/gotoToAccount';
import { MigrateSelectedDatabasesParams } from 'app/web/InterlayerApiProxy/AccountApiProxy/migrateSelectedDatabases/input-params';
import { AccountDatabasesMigrationResultDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models';
import { ReceiveAccountDatabasesForMigrationParams } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/input-params';
import { accountDatabasesMigrationResultDataModelMapFrom } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/mappings';
import { GetAccountListResultDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/data-models';
import { ReceiveAccountListParams } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/input-params';
import { accountListItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/mappings';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { RequestKind } from 'core/requestSender/enums';

export const AccountApiProxy = (() => {
    function getAccountApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getAccountApiProxy();
    }

    function gotoToAccount(args: GotoToAccountParams) {
        getAccountApiProxy().gotoToAccount({ accountId: args.accountId });
    }

    function exitFromDifferentAccount() {
        getAccountApiProxy().exitFromDifferentAccount();
    }

    async function receiveAccountList(requestKind: RequestKind, args: ReceiveAccountListParams): Promise<GetAccountListResultDataModel> {
        return getAccountApiProxy().receiveAccountList(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    searchLine: args.filter.searchLine,
                    onlyMine: args.filter.onlyMine,
                    registeredFrom: args.filter.registeredFrom ? DateUtility.dateToDayMonthYearHourMinuteISO(new Date(args.filter.registeredFrom)) : null,
                    registeredTo: args.filter.registeredTo ? DateUtility.dateToDayMonthYearHourMinuteISO(new Date(args.filter.registeredTo)) : null
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => {
            return selectDataResultMetadataModelMapFrom(responseDto, accountListItemDataModelMapFrom);
        });
    }

    async function getAccountSummaryData(requestKind: RequestKind): Promise<AccountSummaryDataModel> {
        return getAccountApiProxy().getAccountSummaryData(requestKind);
    }

    async function receiveAccountDatabasesForMigration(requestKind: RequestKind, args: ReceiveAccountDatabasesForMigrationParams): Promise<AccountDatabasesMigrationResultDataModel> {
        return getAccountApiProxy().getAccountDatabasesForMigration(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    accountIndexNumber: args.filter.accountIndexNumber,
                    searchLine: args.filter.searchLine,
                    isTypeStorageFile: args.filter.isTypeStorageFile,
                    accountId: args.filter.accountId
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => {
            return accountDatabasesMigrationResultDataModelMapFrom(responseDto);
        });
    }

    async function migrateSelectedDatabases(requestKind: RequestKind, args: MigrateSelectedDatabasesParams): Promise<void> {
        return getAccountApiProxy().migrateSelectedDatabases(requestKind, {
            databases: args.selectedDatabaseNumbers,
            storage: args.selectedStorageId
        });
    }

    return {
        gotoToAccount,
        exitFromDifferentAccount,
        receiveAccountList,
        getAccountSummaryData,
        receiveAccountDatabasesForMigration,
        migrateSelectedDatabases
    };
})();