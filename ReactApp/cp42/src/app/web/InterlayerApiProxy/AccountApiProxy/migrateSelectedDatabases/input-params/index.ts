/**
 * Модель на запрос миграции выбранных баз в новое хранилище
 */
export type MigrateSelectedDatabasesParams = {
    /**
     * Выбранные номера баз для миграции
     */
    selectedDatabaseNumbers: Array<string>;
    /**
     * выбранное хранилище для миграции
     */
    selectedStorageId: string;
};