/**
 * Модель элемента аккаунта для списка аккаунтов
 */
export type AccountListItemDataModel = {
    /**
     * Id аккаунта
     */
    accountId: string;
    /**
     * Номер индекса
     */
    indexNumber: number;
    /**
     * Заголовок аккаунта
     */
    accountCaption: string;
    /**
     * Логин Админ аккаунта
     */
    accountAdminLogin: string;
    /**
     * Почта Админ аккаунта
     */
    accountAdminEmail: string;
    /**
     * Количество пользователей
     */
    accountUsersCount: number;
    /**
     * Дата регистрации аккаунта
     */
    accountRegistrationDate: Date;
    /**
     * Истечение срока действия
     */
    rent1CExpiredDate: Date | null;
    /**
     * Флаг, показывающий является ли аккаунт VIP
     */
    isVip: boolean;
    /**
     * Количество пользователей с активной арендой 1с
     */
    countOfActiveRent1CUsers: number;
};