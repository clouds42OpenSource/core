import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * строка поиска
     */
    searchLine: string;
    /**
     * Начальная дата регистрации аккаунта
     */
    registeredFrom: Date | null;
    /**
     * Конечная дата регистрации аккаунта
     */
    registeredTo: Date | null;
    /**
     * Индикатор что бы показать только мои аккаунты
     */
    onlyMine: boolean | null;
};

/**
 * Параметры для загрузки аккаунтов
 */
export type ReceiveAccountListParams = SelectDataCommonDataModel<Filter>;