import { DateUtility } from 'app/utils';
import { AccountListItemDto } from 'app/web/api/AccountProxy/requests-dto/GetAccountListDtos';
import { AccountListItemDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/data-models';

/**
 * Mapping модели аккаунта
 * @param value ответ модели аккаунта
 */
export function accountListItemDataModelMapFrom(value: AccountListItemDto): AccountListItemDataModel {
    return {
        accountId: value.accountId,
        indexNumber: value.indexNumber,
        accountUsersCount: value.accountUsersCount,
        accountAdminEmail: value.accountAdminEmail,
        accountCaption: value.accountCaption,
        accountAdminLogin: value.accountAdminLogin,
        isVip: value.isVip,
        accountRegistrationDate: DateUtility.convertToDate(value.accountRegistrationDate),
        rent1CExpiredDate: value.rent1CExpiredDate ? DateUtility.convertToDate(value.rent1CExpiredDate) : null,
        countOfActiveRent1CUsers: value.countOfActiveRent1CUsers
    };
}