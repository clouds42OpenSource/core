import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { AccountListItemDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountList/data-models/AccountListItemDataModel';

/**
 * Модель ответа на получение списка аккаунтов
 */
export type GetAccountListResultDataModel = SelectDataResultMetadataModel<AccountListItemDataModel>;