import { FileStorageAccountDatabasesSummaryResponseDto } from 'app/web/api/AccountProxy/requests-dto/AccountDatabasesForMigrationDtos';
import { FileStorageAccountDatabasesSummaryDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models';

/**
 * Mapping модели хранилищ с информацией о размерах и количествах баз в них
 * @param value ответ модели хранилищ с информацией о размерах и количествах баз в них
 */
export function fileStorageAccountDatabasesSummaryDataModelMapFrom(value: FileStorageAccountDatabasesSummaryResponseDto): FileStorageAccountDatabasesSummaryDataModel {
    return {
        fileStorage: value.fileStorage,
        databaseCount: value.databaseCount,
        pathStorage: value.pathStorage,
        totatlSizeOfDatabases: value.totatlSizeOfDatabases,
        isTotalInfo: false
    };
}