/**
 * Список хранилищ с информацией о размерах и количествах баз в них
 */
export type FileStorageAccountDatabasesSummaryDataModel = {
    /**
     * Имя хранилища
     */
    fileStorage: string;
    /**
     * Путь хранилища
     */
    pathStorage: string;
    /**
     * Количество ИБ находящихся в этом хранилище
     */
    databaseCount: number;
    /**
     * Место в Mb занимаемое ИБ в хранилище
     */
    totatlSizeOfDatabases: number;
    /**
     * Показывает, что строка является обще-подсчитывемой
     */
    isTotalInfo: boolean;
};