import { AccountDatabasesMigrationResultResponseDto } from 'app/web/api/AccountProxy/requests-dto/AccountDatabasesForMigrationDtos';
import { keyValueDataModelMapFrom, selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { AccountDatabasesMigrationResultDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models';
import { accountDatabaseMigrationItemDataModelMapFrom, fileStorageAccountDatabasesSummaryDataModelMapFrom } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/mappings';

/**
 * Mapping модели для миграции информационных баз аккаунта
 * @param value ответ модели для миграции информационных баз аккаунта
 */
export function accountDatabasesMigrationResultDataModelMapFrom(value: AccountDatabasesMigrationResultResponseDto): AccountDatabasesMigrationResultDataModel {
    return {
        availableStoragePath: value.availableStoragePath,
        avalableFileStorages: value.avalableFileStorages.map(item => keyValueDataModelMapFrom(item)),
        numberOfDatabasesToMigrate: value.numberOfDatabasesToMigrate,
        fileStorageAccountDatabasesSummary:
            [...value.fileStorageAccountDatabasesSummary.map(item => fileStorageAccountDatabasesSummaryDataModelMapFrom(item)),
                {
                    fileStorage: 'Всего',
                    databaseCount: value.fileStorageAccountDatabasesSummary.map(item => item.databaseCount).reduce((prev, next) => prev + next),
                    totatlSizeOfDatabases: value.fileStorageAccountDatabasesSummary.map(item => item.totatlSizeOfDatabases).reduce((prev, next) => prev + next),
                    pathStorage: '',
                    isTotalInfo: true
                }
            ],
        databases: selectDataResultMetadataModelMapFrom(value.databases, accountDatabaseMigrationItemDataModelMapFrom)
    };
}