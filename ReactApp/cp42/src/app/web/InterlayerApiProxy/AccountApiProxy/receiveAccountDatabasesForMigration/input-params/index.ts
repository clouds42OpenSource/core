import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Номер аккаунта
     */
    accountIndexNumber: number;
    accountId: string;
    /**
     * Строка поиска
     */
    searchLine?: string;
    /**
     * Тип хранилища, <c>true</c> - файловое, <c>false</c> - серверное, <c>null</c> - файловое и серверное
     */
    isTypeStorageFile?: boolean | null;
};

/**
 * Параметры для загрузки аккаунтов
 */
export type ReceiveAccountDatabasesForMigrationParams = SelectDataCommonDataModel<Filter>;