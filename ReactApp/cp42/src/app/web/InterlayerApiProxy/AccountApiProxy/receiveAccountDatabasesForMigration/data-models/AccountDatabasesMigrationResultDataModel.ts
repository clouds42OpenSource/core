import { KeyValueDataModel, SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { AccountDatabaseMigrationItemDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models/AccountDatabaseMigrationItemDataModel';
import { FileStorageAccountDatabasesSummaryDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models/FileStorageAccountDatabasesSummaryDataModel';

/**
 * Модель для миграции информационных баз аккаунта
 */
export type AccountDatabasesMigrationResultDataModel = {
    /**
     * Список хранилищ с информацией о размерах и количествах баз в них
     */
    fileStorageAccountDatabasesSummary: FileStorageAccountDatabasesSummaryDataModel[];
    /**
     * Базы аккаунта
     */
    databases: SelectDataResultMetadataModel<AccountDatabaseMigrationItemDataModel>;
    /**
     * Возможные файловые хранилища для миграции, где Key - ID файлового хранилища, Value - Название файлового хранилища
     */
    avalableFileStorages: KeyValueDataModel<string, string>[];
    /**
     * Cписка путей доступных хранилищ
     */
    availableStoragePath: string[];
    /**
     * Количество баз разрешенных для миграции
     */
    numberOfDatabasesToMigrate: number;
};