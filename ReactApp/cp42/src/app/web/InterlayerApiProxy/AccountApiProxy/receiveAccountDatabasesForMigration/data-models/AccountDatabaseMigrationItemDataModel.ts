import { DatabaseState } from 'app/common/enums';

/**
 * Модель информационной базы для миграций
 */
export type AccountDatabaseMigrationItemDataModel = {
    /**
     * ID базы аккаунта
     */
    accountDatabaseId: string;
    /**
     * Номер информационной базы
     */
    v82Name: string;
    /**
     * Путь по которому располагается ИБ
     */
    path: string;
    /**
     * Занимаемый размер ИБ
     */
    size: number;
    /**
     * Статус ИБ
     */
    status: DatabaseState;
    isPublishDatabase: boolean;
    /**
     * Web сервис
     */
    isPublishServices: boolean;
    /**
     * Выставляется тип информационной базы Файловая или Серверная
     */
    isFile: boolean;
};