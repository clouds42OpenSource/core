import { AccountDatabaseMigrationItemResponseDto } from 'app/web/api/AccountProxy/requests-dto/AccountDatabasesForMigrationDtos';
import { AccountDatabaseMigrationItemDataModel } from 'app/web/InterlayerApiProxy/AccountApiProxy/receiveAccountDatabasesForMigration/data-models';

/**
 * Mapping модели информационной базы для миграций
 * @param value ответ модели информационной базы для миграций
 */
export function accountDatabaseMigrationItemDataModelMapFrom(value: AccountDatabaseMigrationItemResponseDto): AccountDatabaseMigrationItemDataModel {
    return {
        accountDatabaseId: value.accountDatabaseId,
        path: value.path,
        size: value.size,
        isFile: value.isFile,
        status: value.status,
        v82Name: value.v82Name,
        isPublishDatabase: value.isPublishDatabase,
        isPublishServices: value.isPublishServices
    };
}