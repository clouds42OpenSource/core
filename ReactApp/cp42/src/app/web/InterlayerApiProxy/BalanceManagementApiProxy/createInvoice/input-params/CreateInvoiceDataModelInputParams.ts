import { CreateInvoiceProduct } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/createInvoice/input-params/CreateInvoiceProduct';
import { Nullable } from 'app/common/types';

/**
 * Параметры для создания счета на оплату
 */
export type CreateInvoiceDataModelInputParams = {
    /** ID пользователя для отправки уведомлений */
    NotifyAccountUserId: Nullable<string>;
    /** Период оплаты */
    PayPeriod: number;
    /** Список сервисов */
    Products: CreateInvoiceProduct[];
    /** Сумма без НДС */
    TotalBeforeVAT: number;
    /** НДС */
    VAT: number;
    /** Сумма с НДС */
    TotalAfterVAT: number;
    /** Бонусы */
    TotalBonus: number;
};