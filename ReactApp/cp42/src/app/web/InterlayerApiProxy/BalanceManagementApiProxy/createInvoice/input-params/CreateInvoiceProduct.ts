export type CreateInvoiceProduct = {
    IsActive: boolean;
    Identifier: string;
    Quantity: number;
    PayPeriod: number;
    Rate: number;
    TotalPrice: number;
    TotalBonus: number;
};