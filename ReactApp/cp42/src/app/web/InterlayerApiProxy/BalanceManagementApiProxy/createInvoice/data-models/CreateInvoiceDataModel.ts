/**
 * Модель ответа создания счета на оплату
 */
export type CreateInvoiceDataModel = {
    Id: string;
};