import { Nullable } from 'app/common/types';

/**
 * Модель создания счета на произвольную сумму
 */
export type CreateCustomInvoiceDataModel = {
    /** Описание */
    Description: string;
    /** Сумма */
    Amount: number;
    /** ID пользователя для отправки уведомлений */
    NotifyAccountUserId: Nullable<string>;
};