/**
 * Модель ответа с элементом данных о счете по ID
 */
export type InvoiceByIdItemDataModel = {
    /** ID счетан на оплату */
    Id: string;
    /** Дата создания */
    InvoiceDate: string;
    /** Сумма */
    InvoiceSum: number;
    /** Описание */
    Description: string;
    /** Статус оплаты */
    State: string;
    /** Описание документа */
    ActDescription: string
    /** ID документа */
    ActId: string;
    /**  */
    IsNewInvoice: boolean;
    /** Номер фискального чека */
    ReceiptFiscalNumber: string;
};