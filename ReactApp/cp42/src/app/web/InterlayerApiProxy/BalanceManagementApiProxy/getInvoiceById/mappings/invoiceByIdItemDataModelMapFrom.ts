import { InvoiceByIdItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceById/data-models';
import { getInvoiceByIdResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto/getInvoiceByIdResponseDto';

/**
 * Выполнить маппинг модели данных счета на оплату, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по счету на оплату
 */
export function invoiceByIdItemDataModelMapFrom(value: getInvoiceByIdResponseDto): InvoiceByIdItemDataModel {
    return {
        ...value
    };
}