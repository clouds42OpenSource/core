/**
 * Параметры для получения счета на оплату по ID
 */
export type GetInvoiceByIdInputParams = {
    id: string;
};