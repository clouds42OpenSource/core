import { InovoiceCalculatorDataModel, Services } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import { InovoiceCalculatorResponseDto, Product } from 'app/web/api/BalanceManagementProxy/response-dto';

function invoiceProductsMapFrom(value: Product): Services {
    return {
        QuantityControl: value.QuantityControl,
        description: value.Description,
        identifier: value.Identifier,
        isActive: value.IsActive,
        name: value.Name,
        payPeriod: value.PayPeriod,
        quantity: value.Quantity,
        rate: value.Rate,
        totalBonus: value.TotalBonus,
        totalPrice: value.TotalPrice,
        isInitialActive: value.IsActive ? 1 : 2
    };
}

/**
 * Выполнить маппинг модели данных аккаунта, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по аккаунта
 */
export function invoiceCalculatorDataModelMapFrom(value: InovoiceCalculatorResponseDto): InovoiceCalculatorDataModel {
    return {
        accountId: value.AccountId,
        buyerName: value.BuyerName,
        payPeriod: value.PayPeriod,
        PayPeriodControl: value.PayPeriodControl,
        products: value.Products.map(item => invoiceProductsMapFrom(item)),
        currency: value.Currency,
        supplierName: value.SupplierName,
        totalAfterVAT: value.TotalAfterVAT,
        totalBeforeVAT: value.TotalBeforeVAT,
        totalBonus: value.TotalBonus,
        VAT: value.VAT
    };
}