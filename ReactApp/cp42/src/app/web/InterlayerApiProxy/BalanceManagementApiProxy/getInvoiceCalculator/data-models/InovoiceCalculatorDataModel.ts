export type Setting = {
    Key: string;
    Value: string;
};

export type Services = {
    /** ID сервиса */
    identifier: string;
    /** Активность */
    isActive: boolean;
    /** Период покупки сервиса */
    payPeriod: number;
    /** Цена */
    rate: number;
    /** Сумма к оплате */
    totalPrice: number;
    /** Бонусы */
    totalBonus: number;
    /** Количество */
    quantity: number;
    /** Название */
    name: string;
    /** Описание */
    description: string;
    /** Настройки количества */
    QuantityControl: {
        Type: string,
        Settings: Setting[]
    },
    isInitialActive: number
};

export type PayPeriodControl = {
    Type: string;
    Settings: Setting[]
};

/**
 * Модель ответа с элементом данных о аккаунте
 */
export type InovoiceCalculatorDataModel = {
    /** Покупатель */
    buyerName: string;
    /** Поставщик */
    supplierName: string;
    /** ID аккаунта */
    accountId: string;
    /** Периоды оплаты */
    payPeriod: number;
    /** Сумма до НДС */
    totalBeforeVAT: number;
    /** НДС */
    VAT: number;
    /** Данные о валюте */
    currency: {
        /** Код валюты */
        CurrencyCode: number;
        /** Валюта */
        Currency: string;
    },
    /** Сумма с НДС */
    totalAfterVAT: number;
    /** Бонусы */
    totalBonus: number;
    /** Список сервисов */
    products: Services[];
    /** Настройки периода оплаты */
    PayPeriodControl: PayPeriodControl;
};