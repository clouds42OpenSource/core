/**
 * Параметры для загрузки данных о аккаунте
 */
export type GetInvoiceCalulatorInputParams = {
    /** Тип Калькулятора */
    type: string;
};