/**
 * Модель ответа оплаты с сохраненным способом оплаты
 */
export type QuickPayResponseDataModel = {
    paymentId: string;
    paymentSystem: string;
    totalSum: number;
    token: string | null;
    type: string;
    redirectUrl: string | null;
};