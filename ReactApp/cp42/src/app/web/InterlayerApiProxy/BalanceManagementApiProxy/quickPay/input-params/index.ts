/**
 * Параметры для оплаты без ввода данных с сохраненным способом оплаты
 */
export type QuickPayDataModel = {
    paymentMethodId: string;
    totalSum: number;
    isAutoPay: boolean;
};