import { QuickPayResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto';
import { QuickPayResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/quickPay/data-models';

export function quickPayDataModelMapFrom(value: QuickPayResponseDto): QuickPayResponseDataModel {
    return {
        paymentId: value.PaymentId,
        paymentSystem: value.PaymentSystem,
        totalSum: value.TotalSum,
        type: value.Type,
        token: value.Token,
        redirectUrl: value.RedirectUrl
    };
}