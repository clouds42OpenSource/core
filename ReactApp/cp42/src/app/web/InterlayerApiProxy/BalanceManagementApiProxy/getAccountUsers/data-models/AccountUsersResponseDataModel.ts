import { Nullable } from 'app/common/types';
import { SelectDataResultMetadataModel } from 'app/web/common';
import { TAccountDepartment } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

export type AccountUsersItemDataModel = {
    id: string;
    accountId: string;
    login: string;
    firstName: string;
    lastName: string;
    middleName: string;
    fullName: string;
    email: string;
    phoneNumber: string;
    activated: boolean;
    createdInAd: boolean;
    unsubscribed: Nullable<boolean>;
    creationDate: Date;
    accountRoles: string[];
    isPhoneVerified: boolean;
    isEmailVerified: boolean;
    department: TAccountDepartment | null;
};

export type AccountUsersResponseDataModel = SelectDataResultMetadataModel<AccountUsersItemDataModel>;