/**
 * Параметры для сздания счета на оплату
 */
export type AccountUsersDataModelInputParams = {
    /** Количество получаемых пользователей в 1 странице */
    pageSize?: number;
    /** Страница */
    pageNumber?: number;
    /** тип сортировки */
    orderBy?: string;
    search?: string;
    group?: string;
};