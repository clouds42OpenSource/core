import { AccountUsersItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers/data-models';
import { GetAccountUsersItemResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto/GetAccountUsersResponseDto';

/**
 * Выполнить маппинг модели данных создания счета, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по созданию счета
 */
export function accountUsersDataModelMapFrom(value: GetAccountUsersItemResponseDto): AccountUsersItemDataModel {
    return {
        ...value
    };
}