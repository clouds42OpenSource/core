export type RecalculateProductType = {
    Identifier: string;
    Quantity: number;
    IsActive: boolean;
};

export type RecalculateInvoiceParams = {
    PayPeriod: number,
    Products: RecalculateProductType[];
};

export type RecalculatedProduct = {
    IsActive: boolean;
    Identifier: string;
    Quantity: number;
    PayPeriod: number;
    Rate: number;
    TotalPrice: number;
    TotalBonus: number;
};

export type RecalculateItemDataModel = {
    PayPeriod: number | null;
    TotalBeforeVAT: number;
    VAT: number;
    TotalAfterVAT: number;
    TotalBonus: number;
    Products: RecalculatedProduct[];
};