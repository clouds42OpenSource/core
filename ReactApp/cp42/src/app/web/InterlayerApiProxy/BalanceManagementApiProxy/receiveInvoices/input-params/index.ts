import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Параметры для загрузки счетов
 */
export type ReceiveInvoiceListParams = IForceThunkParam & SelectDataCommonDataModel<NonNullable<unknown>>;