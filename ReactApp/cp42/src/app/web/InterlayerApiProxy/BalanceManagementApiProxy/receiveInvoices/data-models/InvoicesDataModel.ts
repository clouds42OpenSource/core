import { InvoiceItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices';
import { SelectDataResultCommonDataModel } from 'app/web/common/data-models';

/**
 * Модель данных счетов
 */
export type InvoicesDataModel = SelectDataResultCommonDataModel<InvoiceItemDataModel>;