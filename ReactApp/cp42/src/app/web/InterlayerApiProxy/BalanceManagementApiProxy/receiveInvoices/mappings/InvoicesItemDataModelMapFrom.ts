import { InvoiceItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices';
import { InvoiceItemResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto';

/**
 * Выполнить маппинг модели данных счетов, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по счетам
 */
export function invoicesItemDataModelMapFrom(value: InvoiceItemResponseDto): InvoiceItemDataModel {
    return {
        Id: value.Id,
        State: value.State,
        ActId: value.ActId,
        ActDescription: value.ActDescription,
        Uniq: value.Uniq,
        ReceiptFiscalNumber: value.ReceiptFiscalNumber,
        InvoiceDate: value.InvoiceDate,
        InvoiceSum: value.InvoiceSum,
        Description: value.Description,
        IsNewInvoice: value.IsNewInvoice,
        Status: value.Status,
        RequiredSignature: value.RequiredSignature
    };
}