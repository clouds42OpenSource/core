import { BillingAccountDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData';
import { BillingAccountResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto';

/**
 * Выполнить маппинг модели данных аккаунта, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по аккаунта
 */
export function billingAccountDataModelMapFrom(value: BillingAccountResponseDto): BillingAccountDataModel {
    return {
        accountIsDemo: value.AccountIsDemo,
        accountIsVip: value.AccountIsVip,
        currentBalance: value.CurrentBalance,
        currentBonusBalance: value.CurrentBonusBalance,
        hasErrorInPayment: value.HasErrorInPayment,
        locale: {
            currency: value.Locale.Currency,
            name: value.Locale.Name
        },
        promisePayment: {
            blockReason: value.PromisePayment.BlockReason,
            canTakePromisePayment: value.PromisePayment.CanTakePromisePayment,
            canIncreasePromisePayment: value.PromisePayment.CanIncreasePromisePayment
        },
        regularPayment: value.RegularPayment,
        canEarlyPromisePaymentRepay: value.CanEarlyPromisePaymentRepay,
        canProlongPromisePayment: value.CanProlongPromisePayment,
        suggestedPayment: value.SuggestedPayment
    };
}