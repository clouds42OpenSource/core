import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры для загрузки данных о аккаунте
 */
export type getBillingAccountParams = IForceThunkParam;