/**
 * Модель ответа с элементом данных о аккаунте
 */
export type BillingAccountDataModel = {
    /**
     * Признак демо аккаунта
     */
    accountIsDemo: boolean;
    /**
     * Признак VIP аккаунта
     */
    accountIsVip: boolean;
    /**
     *  Баланс аккаунта
     */
    currentBalance: number;
    /**
     * Бонусы аккаунта
     */
    currentBonusBalance: number;
    /**
     *  Ежемесячный платеж
     */
    regularPayment: number;
    /**
     * Признак ошибки в платежах
     */
    hasErrorInPayment: boolean;
    /**
     * Локаль
     */
    locale: {
        /**
         * Название
         */
        name: string;
        /**
         * Валюта
         */
        currency: string;
    }
    /**
     * Данные об обещанном платеже
     */
    promisePayment: {
        /**
         * Признак возможности увеличения обещанного платежа
         */
        canIncreasePromisePayment: boolean;
        /**
         * Признак возможности получения обещанного платежа
         */
        canTakePromisePayment: boolean;
        /**
         * Причина блокировки обещанного платежа
         */
        blockReason: string;
    }
    /**
     * Признак возможности погашения обещанный платеж
     */
    canEarlyPromisePaymentRepay: boolean;
    /**
     * Признак возможности продления обещанного платежа
     */
    canProlongPromisePayment: boolean;

    /**
     * SuggestedPayment
     */
    suggestedPayment: string;
};