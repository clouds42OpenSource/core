import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    start: Date
    end: Date
    type: number | null
};

/**
 * Параметры для загрузки транзакций
 */
export type ReceiveTransactionsListParams = IForceThunkParam & SelectDataCommonDataModel<Filter>;