import { SelectDataResultCommonDataModel } from 'app/web/common/data-models';
import { TransactionsItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions';

/**
 * Модель данных транзакций
 */
export type TransactionsDataModel = SelectDataResultCommonDataModel<TransactionsItemDataModel>;