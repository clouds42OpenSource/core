import { TransactionsInvoiceType } from 'app/views/modules/AccountManagement/BalanceManagementView/types/TransactionsInvoiceType';

/**
 * Модель ответа с элементом транзакций
 */
export type TransactionsItemDataModel = {
    Id: string;
    /** Загаловок */
    Title: string;
    /** Тип транзакции */
    TransactionType: number;
    /** Сумма к оплате */
    PaySum: number;
    /** Баланс счета */
    Balance: number;
    /** Баланс бонусного счета */
    BonusBalance: number;
    /** Дата */
    Date: Date;
    /** Статус транзакцит */
    Status: number;
    /** Тип транзакции */
    Type: TransactionsInvoiceType;
    Remains: number | null;
};