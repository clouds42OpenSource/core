import { TransactionsItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions';
import { TransactionsItemResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto';

/**
 * Выполнить маппинг модели данных транзакций, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по транзакций
 */
export function transactionsItemDataModelMapFrom(value: TransactionsItemResponseDto): TransactionsItemDataModel {
    return {
        Id: value.Id,
        Balance: value.MoneyBalance,
        Date: value.Date,
        PaySum: value.PaySum,
        Status: value.Status,
        Title: value.Title,
        TransactionType: value.TransactionType,
        BonusBalance: value.BonusBalance,
        Type: value.Type,
        Remains: value.Remains
    };
}