import { IoCContainer } from 'app/IoCСontainer';
import { IsLastAdminAccountCardThunkParams } from 'app/modules/AccountManagement/UserManagement/store/reducers/isLastAccountReducer/params';
import { DateUtility } from 'app/utils';
import { AddTransactionsRequestDto } from 'app/web/api/BalanceManagementProxy/request-dto';
import { AccountActiveSessionsRequestDTO } from 'app/web/api/BalanceManagementProxy/request-dto/AccountActiveSessionsRequestDTO';
import { AccountActiveSessionsResponseDTO } from 'app/web/api/BalanceManagementProxy/response-dto/AccountActiveSessionsResponseDTO';
import { selectDataResultCommonDataModelMapFrom, selectDataResultMetadataModelMapFrom } from 'app/web/common';
import { ActivatePromisePaymentParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/activatePromisePayment';
import { CreateInvoiceDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/createInvoice';
import { CreateCustomInvoiceDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/createInvoice/input-params/CreateCustomInvoiceDataModel';
import { CreateInvoiceDataModelInputParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/createInvoice/input-params/CreateInvoiceDataModelInputParams';
import { AccountUsersDataModelInputParams, accountUsersDataModelMapFrom, AccountUsersResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getAccountUsers';
import { BillingAccountDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData';
import { billingAccountDataModelMapFrom } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getBilingAccountData/mappings';
import { GetInvoiceByIdInputParams, InvoiceByIdItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceById';
import { invoiceByIdItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceById/mappings';
import { GetInvoiceCalulatorInputParams, InovoiceCalculatorDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator';
import { invoiceCalculatorDataModelMapFrom } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/getInvoiceCalculator/mappings';
import { OnlinePaymentDataModelInputParams, onlinePaymentDataModelMapFrom, OnlinePaymentResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/onlinePayment';
import { QuickPayDataModel, quickPayDataModelMapFrom, QuickPayResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/quickPay';
import { RecalculateInvoiceParams, RecalculateItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recalculateInvoice';
import { ReceiveInvoiceListParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices';
import { InvoicesDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices/data-models/InvoicesDataModel';
import { invoicesItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/receiveInvoices/mappings';
import { ProvidedLongServicesDataModel, ReceiveProvidedLongServicesParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices';
import { providedLongServicesItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices/mappings';
import { ProvidedServicesDataModel, ReceiveProvidedServicesParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices';
import { providedServicesItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices/mappings';
import { ReceiveTransactionsListParams } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions';
import { TransactionsDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions/data-models/TransactionsDataModel';
import { transactionsItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveTransactions/mappings';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Прокси для работы с данными Биллинга
 */
export const BalanceManagementApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getInvoicesProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getBalanceManagementApiProxy();
    }

    /**
     * Получить данные по счетам
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveInvoices(requestKind: RequestKind, args: ReceiveInvoiceListParams): Promise<InvoicesDataModel> {
        return getInvoicesProxy().receiveInvoices(requestKind, { pageNumber: args.pageNumber })
            .then(responseDto => {
                return selectDataResultCommonDataModelMapFrom(responseDto, invoicesItemDataModelMapFrom);
            });
    }

    /**
     * Получить данные по транзакций
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveTransactions(requestKind: RequestKind, args: ReceiveTransactionsListParams): Promise<TransactionsDataModel> {
        return getInvoicesProxy().recieveTransactions(requestKind, {
            pageNumber: args.pageNumber,
            'Filter.DateFrom': args.filter?.start ? DateUtility.dateToYearMonthDay(args.filter.start) : DateUtility.dateToYearMonthDay(DateUtility.getSomeYearAgoDate(1)),
            'Filter.DateTo': args.filter?.end ? DateUtility.dateToYearMonthDay(args.filter.end) : DateUtility.dateToYearMonthDay(new Date()),
            'Filter.Service': !args.filter || args.filter.type === -1 ? '' : args.filter.type,
        })
            .then(responseDto => selectDataResultCommonDataModelMapFrom(responseDto, transactionsItemDataModelMapFrom));
    }

    /**
     * Получить данные предоставляемых услуг
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function recieveProvidedServices(requestKind: RequestKind, args: ReceiveProvidedServicesParams): Promise<ProvidedServicesDataModel> {
        return getInvoicesProxy().recieveProvidedServices(requestKind, {
            pageNumber: args.pageNumber,
            'Filter.DateTo': args.filter?.end ? DateUtility.dateToYearMonthDay(args.filter.end) : DateUtility.dateToYearMonthDay(new Date()),
            'Filter.DateFrom': args.filter?.start ? DateUtility.dateToYearMonthDay(args.filter.start) : DateUtility.dateToYearMonthDay(DateUtility.getSomeYearAgoDate(1)),
            'Filter.Service': !args.filter || args.filter.type === -1 ? '' : args.filter.type
        })
            .then(responseDto => selectDataResultCommonDataModelMapFrom(responseDto, providedServicesItemDataModelMapFrom));
    }

    /**
     * Получить данные длительных услуг
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function recieveProvidedLongServices(requestKind: RequestKind, args: ReceiveProvidedLongServicesParams): Promise<ProvidedLongServicesDataModel> {
        return getInvoicesProxy().recieveProvidedLongServices(requestKind, args)
            .then(responseDto => selectDataResultCommonDataModelMapFrom(responseDto, providedLongServicesItemDataModelMapFrom));
    }

    /**
     * Получить данные длительных услуг
     * @param requestKind тип запроса
     */
    async function recieveBillingAccountData(requestKind: RequestKind): Promise<BillingAccountDataModel> {
        return getInvoicesProxy().getBillinAccountData(requestKind, {})
            .then(responseDto => billingAccountDataModelMapFrom(responseDto));
    }

    /**
     * Получить данные длительных услуг
     * @param requestKind тип запроса
     */
    async function ProlongPromisePayment(requestKind: RequestKind): Promise<NonNullable<unknown>> {
        return getInvoicesProxy().ProlongPromisePayment(requestKind)
            .then(responseDto => responseDto);
    }

    /**
     * Получить данные длительных услуг
     * @param requestKind тип запроса
     */
    async function RepayPromisePayment(requestKind: RequestKind): Promise<NonNullable<unknown>> {
        return getInvoicesProxy().PayPromisePayment(requestKind)
            .then(responseDto => responseDto);
    }

    /**
     * Добавление платежа
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function AddTransaction(requestKind: RequestKind, args: AddTransactionsRequestDto): Promise<NonNullable<unknown>> {
        return getInvoicesProxy().AddTransaction(requestKind, args)
            .then(responseDto => responseDto);
    }

    /**
     * Получить счет на оплату по ID
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getInvoiceById(requestKind: RequestKind, args: GetInvoiceByIdInputParams): Promise<InvoiceByIdItemDataModel> {
        return getInvoicesProxy().getInvoiceById(requestKind, args)
            .then(responseDto => invoiceByIdItemDataModelMapFrom(responseDto));
    }

    /**
     * Получить калькулятор
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getInvoiceCalculator(requestKind: RequestKind, args: GetInvoiceCalulatorInputParams): Promise<InovoiceCalculatorDataModel> {
        return getInvoicesProxy().getInvoiceCalculator(requestKind, args)
            .then(responseDto => invoiceCalculatorDataModelMapFrom(responseDto));
    }

    /**
     * Перерасчет на бэке услуг фаста
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function recalculateFasta(requestKind: RequestKind, args: RecalculateInvoiceParams): Promise<RecalculateItemDataModel> {
        return getInvoicesProxy().recalculateFasta(requestKind, args);
    }

    /**
     * Перерасчет на бэке основных услуг
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function recalculateStandard(requestKind: RequestKind, args: RecalculateInvoiceParams): Promise<RecalculateItemDataModel> {
        return getInvoicesProxy().recalculateStandard(requestKind, args);
    }

    /**
     * Создание счета на оплату
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function createInvoice(requestKind: RequestKind, args: CreateInvoiceDataModelInputParams & { type: string }): Promise<CreateInvoiceDataModel> {
        return getInvoicesProxy().createInvoice(requestKind, args);
    }

    /**
     * Создание счета на произвольную сумму
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function createCustomInvoice(requestKind: RequestKind, args: CreateCustomInvoiceDataModel): Promise<CreateInvoiceDataModel> {
        return getInvoicesProxy().createCustomInvoice(requestKind, args);
    }

    /**
     * Получение списка пользователей аккаунта
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getAccountUsers(requestKind: RequestKind, args: AccountUsersDataModelInputParams): Promise<AccountUsersResponseDataModel> {
        return getInvoicesProxy().getAccountUsers(requestKind, {
            ...args,
            group: args.group ? args.group : '',
            searchLine: args.search ? args.search : '',
        })
            .then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, accountUsersDataModelMapFrom));
    }

    /**
     * Получение проверки последнего админа
     * @param requestKind тип запроса
     * @param args параметры запроса
     */

    async function isAccountLastAdmin(requestKind: RequestKind, args: IsLastAdminAccountCardThunkParams): Promise<boolean> {
        return getInvoicesProxy().isLastAdmin(requestKind, args);
    }

    /**
     * Получение списка активных сеансов
     * @param requestKind тип запроса
     */

    async function getActiveSessions(requestKind: RequestKind): Promise<AccountActiveSessionsResponseDTO> {
        return getInvoicesProxy().activeSessions(requestKind);
    }

    /**
     * Отправка списка пользователей для завершения сеансов
     * @param requestKind тип запроса
     * @param args параметры запроса
     */

    async function terminateActiveSessions(requestKind: RequestKind, args: AccountActiveSessionsRequestDTO): Promise<NonNullable<unknown>> {
        return getInvoicesProxy().activeSessionsTerminate(requestKind, args);
    }

    /**
     * Создание обещанного платежа
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function activatePromisePayment(requestKind: RequestKind, args: ActivatePromisePaymentParams): Promise<NonNullable<unknown>> {
        return getInvoicesProxy().activatePromisePayment(requestKind, args)
            .then(responseDto => responseDto);
    }

    /**
     * Создание обещанного платежа
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function onlinePayment(requestKind: RequestKind, args: OnlinePaymentDataModelInputParams): Promise<OnlinePaymentResponseDataModel> {
        return getInvoicesProxy().onlinePayment(requestKind, args)
            .then(responseDto => onlinePaymentDataModelMapFrom(responseDto));
    }

    /**
     * Оплата без ввода данных с сохраненным способом оплаты
     * @param requestKind тип запроса
     * @param args аргументы на оплату
     */
    async function quickPay(requestKind: RequestKind, args: QuickPayDataModel): Promise<QuickPayResponseDataModel> {
        return getInvoicesProxy().quickPay(requestKind, args).then(response => quickPayDataModelMapFrom(response));
    }

    return {
        activatePromisePayment,
        createCustomInvoice,
        getAccountUsers,
        isAccountLastAdmin,
        getActiveSessions,
        terminateActiveSessions,
        getInvoiceById,
        createInvoice,
        recalculateFasta,
        recalculateStandard,
        getInvoiceCalculator,
        receiveInvoices,
        receiveTransactions,
        recieveProvidedServices,
        recieveProvidedLongServices,
        recieveBillingAccountData,
        ProlongPromisePayment,
        RepayPromisePayment,
        AddTransaction,
        onlinePayment,
        quickPay
    };
})();