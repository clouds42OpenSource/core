import { BeginOnlinePaymentResponseDto } from 'app/web/api/BalanceManagementProxy/response-dto';
import { OnlinePaymentResponseDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/onlinePayment/data-models';

/**
 * Выполнить маппинг модели данных создания счета, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по созданию счета
 */
export function onlinePaymentDataModelMapFrom(value: BeginOnlinePaymentResponseDto): OnlinePaymentResponseDataModel {
    return {
        PaymentSystem: value.PaymentSystem,
        ConfirmationTokenOrUrl: typeof value.Data === 'string' ? value.Data : value.Data.ConfirmationToken
    };
}