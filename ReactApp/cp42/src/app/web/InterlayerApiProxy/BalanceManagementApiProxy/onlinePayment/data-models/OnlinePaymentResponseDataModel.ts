/**
 * Модель ответа создания счета на оплату
 */
export type OnlinePaymentResponseDataModel = {
    PaymentSystem: 'Yookassa' | 'UkrPay' | 'Paybox' | 'Robokassa';
    ConfirmationTokenOrUrl: string;
};