/**
 * Параметры для сздания счета на оплату
 */
export type OnlinePaymentDataModelInputParams = {
    TotalSum: number;
    ReturnUrl: string;
    IsAutoPay: boolean;
};