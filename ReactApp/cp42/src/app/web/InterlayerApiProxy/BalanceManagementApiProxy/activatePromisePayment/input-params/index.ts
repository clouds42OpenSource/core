/**
 * Параметры для создания обещанного платежа
 */
export type ActivatePromisePaymentParams = {
  /** ID счета на оплату */
  InvoiceId: string;
};