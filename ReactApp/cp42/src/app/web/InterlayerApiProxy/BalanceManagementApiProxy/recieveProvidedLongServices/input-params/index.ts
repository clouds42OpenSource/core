import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Параметры для загрузки аккаунтов
 */
export type ReceiveProvidedLongServicesParams = IForceThunkParam & SelectDataCommonDataModel<NonNullable<unknown>>;