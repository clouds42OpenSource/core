import { ProvidedLongServicesItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices';
import { ProvidedLongServicesItemDto } from 'app/web/api/BalanceManagementProxy/response-dto/ProvidedLongServicesItemDto';

/**
 * Выполнить маппинг модели данных транзакций, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по транзакций
 */
export function providedLongServicesItemDataModelMapFrom(value: ProvidedLongServicesItemDto): ProvidedLongServicesItemDataModel {
    return {
        ActiveFrom: value.From,
        ActiveTo: value.To,
        Rate: value.Rate,
        ServiceDescription: value.ServiceDescription,
        Count: value.Count,
        FromCorp: value.FromCorp,
        IsHistoricalRow: value.IsHistoricalRow,
        ResourceType: value.ResourceType,
        Service: value.Service,
        SposoredAccountName: value.SposoredAccountName
    };
}