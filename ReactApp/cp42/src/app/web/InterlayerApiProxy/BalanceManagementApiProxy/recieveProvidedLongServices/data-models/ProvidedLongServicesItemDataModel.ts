/**
 * Модель ответа с элементом оказанных услуг
 */
export type ProvidedLongServicesItemDataModel = {
   /** Признак истечения срока */
    IsHistoricalRow: boolean
    /** Сервис */
    Service: number
    /** */
    ResourceType: number
    /** Описание сервиса */
    ServiceDescription: string
    /** Дата с */
    ActiveFrom: Date
    /** Дата по */
    ActiveTo: Date
    /** Количество */
    Count: number
    /** Цена */
    Rate: string
    /**  */
    FromCorp: boolean
    /** Спосируемый аккаунт */
    SposoredAccountName: string
};