import { ProvidedLongServicesItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedLongServices';
import { SelectDataResultCommonDataModel } from 'app/web/common/data-models';

/**
 * Модель данных длительных услуг
 */
export type ProvidedLongServicesDataModel = SelectDataResultCommonDataModel<ProvidedLongServicesItemDataModel>;