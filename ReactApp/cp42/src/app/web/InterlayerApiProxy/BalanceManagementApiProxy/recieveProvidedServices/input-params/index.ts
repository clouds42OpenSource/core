import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    start: Date
    end: Date
    type: number | null
};

/**
 * Параметры для загрузки оказанных услуг
 */
export type ReceiveProvidedServicesParams = IForceThunkParam & SelectDataCommonDataModel<Filter>;