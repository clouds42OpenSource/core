import { ProvidedServicesItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices';
import { SelectDataResultCommonDataModel } from 'app/web/common/data-models';

/**
 * Модель данных оказанных услуг
 */
export type ProvidedServicesDataModel = SelectDataResultCommonDataModel<ProvidedServicesItemDataModel>;