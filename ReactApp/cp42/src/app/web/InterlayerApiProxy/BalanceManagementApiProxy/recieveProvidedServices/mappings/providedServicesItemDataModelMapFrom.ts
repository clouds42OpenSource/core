import { ProvidedServicesItemDataModel } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy/recieveProvidedServices';
import { ProvidedServicesItemDto } from 'app/web/api/BalanceManagementProxy/response-dto/ProvidedServicesItemDto';

/**
 * Выполнить маппинг модели оказанных услуг, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по услугам
 */
export function providedServicesItemDataModelMapFrom(value: ProvidedServicesItemDto): ProvidedServicesItemDataModel {
    return {
        ActiveFrom: value.From,
        ActiveTo: value.To,
        Count: value.Count,
        Rate: value.Rate,
        ServiceDescription: value.ServiceDescription,
        Source: value.FromCorp,
        SposoredAccountName: value.SposoredAccountName,
        Service: value.Service,
        IsHistoricalRow: value.IsHistoricalRow
    };
}