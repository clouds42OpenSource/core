/**
 * Модель ответа с элементом оказанных услуг
 */
export type ProvidedServicesItemDataModel = {
    /** Признак истечения срока действия */
    IsHistoricalRow: boolean;
    /** Сервис */
    Service: number;
    /** Описание сервиса */
    ServiceDescription: string;
    /** Дата с */
    ActiveFrom: Date;
    /** Дата по */
    ActiveTo: Date;
    /** Количество */
    Count: number;
    Rate: string;
    /** Цена */
    Source: boolean;
    /** Спосируемы аккаунт */
    SposoredAccountName: string;
};