/**
 * Параметры для добавления служебного аккаунта
 */
export type AddServiceAccountParams = {
    /**
     * ID аккаунта для добавления.
     */
    accountId: string;
};