/**
 * Модель ответа на добавление служебного аккаунта
 */
export type AddServiceAccountResultDataModel = {
    /**
     * Логин пользователя, который создал служебный аккаунт
     */
    accountUserInitiatorName: string;

    /**
     * Дата создания
     */
    creationDateTime: Date;
};