import { DateUtility } from 'app/utils';
import { AddServiceAccountResponseDto } from 'app/web/api/ServiceAccounts/response-dto';
import { AddServiceAccountResultDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/addServiceAccount/data-models';

/**
 * Mapping модели ответа на добавление служебного аккаунта
 * @param value Модель ответа на добавление служебного аккаунта
 */
export function addServiceAccountResultDataModelMapFrom(value: AddServiceAccountResponseDto): AddServiceAccountResultDataModel {
    return {
        ...value,
        creationDateTime: DateUtility.convertToDate(value.creationDateTime),
    };
}