/**
 * Параметры для удаления записи служебного аккаунта
 */
export type DeleteServiceAccountParams = {
    /**
     * Id аккаунта для удаления
     */
    accountId: string;
};