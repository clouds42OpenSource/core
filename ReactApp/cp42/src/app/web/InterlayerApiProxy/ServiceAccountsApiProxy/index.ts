import { IoCContainer } from 'app/IoCСontainer';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { AddServiceAccountResultDataModel, addServiceAccountResultDataModelMapFrom } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/addServiceAccount';
import { AddServiceAccountParams } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/addServiceAccount/input-params';
import { DeleteServiceAccountParams } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/deleteServiceAccount/input-params';
import { serviceAccountItemDataModelMapFrom, ServiceAccountsDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts';
import { ReceiveServiceAccountsParams } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts/input-params';
import { SearchAccountsItemDataModel, searchAccountsItemDataModelMapFrom, SearchAccountsParams } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/searchAccounts';
import { RequestKind } from 'core/requestSender/enums';

export const ServiceAccountsApiProxy = (() => {
    function getServiceAccountsApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getServiceAccountsProxy();
    }

    async function receiveServiceAccounts(requestKind: RequestKind, args: ReceiveServiceAccountsParams): Promise<ServiceAccountsDataModel> {
        return getServiceAccountsApiProxy().receiveServiceAccounts(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    SearchLine: args.filter.searchLine
                } : null,
            orderBy: args.orderBy,
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, serviceAccountItemDataModelMapFrom));
    }

    async function deleteServiceAccount(requestKind: RequestKind, args: DeleteServiceAccountParams): Promise<void> {
        return getServiceAccountsApiProxy().deleteServiceAccount(requestKind, {
            serviceAccountId: args.accountId
        });
    }

    async function addServiceAccount(requestKind: RequestKind, args: AddServiceAccountParams): Promise<AddServiceAccountResultDataModel> {
        return getServiceAccountsApiProxy().addServiceAccount(requestKind, {
            AccountId: args.accountId
        }).then(responseDto => {
            return addServiceAccountResultDataModelMapFrom(responseDto);
        });
    }

    async function searchAccounts(requestKind: RequestKind, args: SearchAccountsParams): Promise<SearchAccountsItemDataModel[]> {
        return getServiceAccountsApiProxy().searchAccounts(requestKind, {
            searchLine: args.searchLine
        }).then(responseDto => {
            return responseDto ? responseDto.map(item => searchAccountsItemDataModelMapFrom(item)) : [];
        });
    }

    return {
        receiveServiceAccounts,
        deleteServiceAccount,
        addServiceAccount,
        searchAccounts
    };
})();