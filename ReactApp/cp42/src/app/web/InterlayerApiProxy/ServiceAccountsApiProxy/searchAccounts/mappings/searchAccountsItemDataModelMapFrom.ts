import { SearchAccountsItemResponseDto } from 'app/web/api/ServiceAccounts/response-dto';
import { SearchAccountsItemDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/searchAccounts/data-models';

/**
 * Mapping модели информацию о найденном аккаунте
 * @param value Модель информации о найденном аккаунте
 */
export function searchAccountsItemDataModelMapFrom(value: SearchAccountsItemResponseDto): SearchAccountsItemDataModel {
    return {
        accountId: value.key,
        accountCaption: value.value,
        accountIndexNumber: value.indexNumber
    };
}