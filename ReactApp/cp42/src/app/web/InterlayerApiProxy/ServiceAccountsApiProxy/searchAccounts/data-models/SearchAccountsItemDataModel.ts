/**
 * Ответ на запрос поиска аккаунтов
 */
export type SearchAccountsItemDataModel = {
    /**
     * Описание аккаунта
     */
    accountCaption: string;

    /**
     * Id аккаунта
     */
    accountId: string;

    /**
     * Номер аккаунта
     */
    accountIndexNumber: number;
};