/**
 * Запрос на поиск аккаунта
 */
export type SearchAccountsParams = {
    /**
     * Строка поиска
     */
    searchLine: string;
};