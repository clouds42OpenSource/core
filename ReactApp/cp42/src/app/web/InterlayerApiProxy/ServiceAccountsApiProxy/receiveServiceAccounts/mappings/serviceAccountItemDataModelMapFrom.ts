import { DateUtility } from 'app/utils';
import { ServiceAccountItemResponseDto } from 'app/web/api/ServiceAccounts/response-dto';
import { ServiceAccountItemDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts/data-models';

/**
 * Mapping модели информацию о служебном аккаунте
 * @param value Модель информации о служебном аккаунте
 */
export function serviceAccountItemDataModelMapFrom(value: ServiceAccountItemResponseDto): ServiceAccountItemDataModel {
    return {
        ...value,
        creationDateTime: DateUtility.convertToDate(value.creationDateTime),
    };
}