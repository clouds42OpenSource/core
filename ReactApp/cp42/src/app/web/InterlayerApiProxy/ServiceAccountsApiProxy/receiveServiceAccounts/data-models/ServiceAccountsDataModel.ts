import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { ServiceAccountItemDataModel } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy/receiveServiceAccounts/data-models/ServiceAccountItemDataModel';

/**
 * Модель ответа на запрос получения служебных аккаунтов
 */
export type ServiceAccountsDataModel = SelectDataResultMetadataModel<ServiceAccountItemDataModel>;