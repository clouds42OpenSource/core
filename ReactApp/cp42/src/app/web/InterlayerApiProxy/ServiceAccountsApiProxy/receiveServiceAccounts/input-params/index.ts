import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Строка фильтра
     */
    searchLine: string;
};

/**
 * Параметры для загрузки служебных аккаунтов
 */
export type ReceiveServiceAccountsParams = SelectDataCommonDataModel<Filter>;