/**
 * Тип ответа на загрузку публичных URL адресов
 */
export type PublicUrlsDataModel = {
    /**
     *  URL на промо сайт
     */
    promoSiteUrl: string;
};