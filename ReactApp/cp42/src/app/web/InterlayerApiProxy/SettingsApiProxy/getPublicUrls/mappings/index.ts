import { PublicUrlsDto } from 'app/web/api/SettingsProxy/response-dto';
import { PublicUrlsDataModel } from 'app/web/InterlayerApiProxy/SettingsApiProxy/getPublicUrls/data-models';

/**
 * Маппинг для получения публичных адресов
 * @param value Response dto публичных адресов
 * @returns Data model публичных адресов
 */
export function getPublicUrlsDataModelFrom(value: PublicUrlsDto): PublicUrlsDataModel {
    return {
        promoSiteUrl: value.PromoSiteUrl
    };
}