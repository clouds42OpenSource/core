import { IoCContainer } from 'app/IoCСontainer';
import { PublicUrlsDataModel } from 'app/web/InterlayerApiProxy/SettingsApiProxy/getPublicUrls/data-models';
import { getPublicUrlsDataModelFrom } from 'app/web/InterlayerApiProxy/SettingsApiProxy/getPublicUrls/mappings';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Апи прокси поставщиков
 */
export const SettingsApiProxy = (() => {
    function getSettingsApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSettingsProxy();
    }

    /**
     * Запрос на получение публичных URL адресов
     * @param requestKind тип запроса
     */
    async function getPublicUrls(requestKind: RequestKind): Promise<PublicUrlsDataModel> {
        return getSettingsApiProxy().getPublicUrls(requestKind)
            .then(responseDto => getPublicUrlsDataModelFrom(responseDto));
    }

    return {
        getPublicUrls
    };
})();