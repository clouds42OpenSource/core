import { IoCContainer } from 'app/IoCСontainer';
import { GetGeneralData, GetGeneralDataLower } from 'app/web/api/InfoDbCardProxy/responce-dto/GetGeneralData';
import { InputAccountId } from 'app/web/api/InfoDbProxy/request-dto/InputAccountId';
import { InputIdsArr } from 'app/web/api/InfoDbProxy/request-dto/InputIdsArr';
import { GetAvailabilityInfoDb } from 'app/web/api/InfoDbProxy/responce-dto/GetAvailabilityInfoDb';
import { selectDataResultCommonDataModelMapFrom } from 'app/web/common/mappings';
import { selectStatusDataResultCommonDataModelMapFrom } from 'app/web/common/mappings/commonStatusDataModelMapFrom';
import { deleteDbType } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/deleteInfoDbList/input-params';
import { editDbListType } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/editInfoDbList/input-params';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models';
import { GetInfoDbListResultDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models/GetInfoDbListResultDataModel';
import { ReceiveInfoDbListParams } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/input-params';
import { infoDbListItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/mappings/infoDbListItemDataModelMapFrom';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { RequestKind } from 'core/requestSender/enums';

export const InfoDbApiProxy = (() => {
    function getInfoDbApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getInfoDbApiProxy();
    }

    async function receiveInfoDbList(requestKind: RequestKind, args: ReceiveInfoDbListParams): Promise<GetInfoDbListResultDataModel> {
        return getInfoDbApiProxy().receiveInfoDbList(
            requestKind,
            {
                accountID: args.accountID,
                search: args.search,
                type: args.type,
                page: args.pageNumber,
                size: args.size,
                affiliation: args.affiliation,
                field: args.field,
                sort: args.sort,
                accountUserId: args.accountUserId,
            }
        ).then(responseDto => {
            return selectDataResultCommonDataModelMapFrom(responseDto.Data, infoDbListItemDataModelMapFrom);
        });
    }

    async function receiveInfoDbListOther(requestKind: RequestKind, args: ReceiveInfoDbListParams): Promise<GetInfoDbListResultDataModel> {
        return getInfoDbApiProxy().receiveInfoDbListOther(
            requestKind,
            {
                accountID: args.accountID,
                search: args.search,
                type: args.type,
                page: args.pageNumber,
                size: args.size,
                affiliation: args.affiliation,
                field: args.field,
                sort: args.sort,
                accountUserId: args.accountUserId,
            }
        ).then(responseDto => {
            return selectDataResultCommonDataModelMapFrom(responseDto.Data, infoDbListItemDataModelMapFrom);
        });
    }

    async function deleteInfoDbList(requestKind: RequestKind, args: deleteDbType): Promise<GetGeneralDataLower<null>> {
        return getInfoDbApiProxy().deleteInfoDbList(
            requestKind,
            { databasesId: args.databasesId }
        ).then(responseDto => {
            return responseDto;
        });
    }

    async function editInfoDbList(requestKind: RequestKind, args: editDbListType): Promise<GetGeneralData<null>> {
        return getInfoDbApiProxy().editInfoDbList(
            requestKind,
            {
                databaseId: args.databaseId,
                caption: args.caption,
                templateId: args.templateId,
                v82Name: args.v82Name,
                usedWebServices: args.usedWebServices,
                databaseState: args.databaseState,
                hasModifications: args.hasModifications,
                restoreModel: args.restoreModel
            }
        ).then(responseDto => {
            return responseDto;
        });
    }

    async function statusInfoDbList(requestKind: RequestKind, args: IForceThunkParam & InputIdsArr): Promise<InfoDbListItemDataModel[]> {
        return getInfoDbApiProxy().statusInfoDbList(requestKind, { ids: args.ids }).then(responseDto => {
            return selectStatusDataResultCommonDataModelMapFrom(responseDto.Data, infoDbListItemDataModelMapFrom);
        });
    }

    async function availabilityInfoDbList(requestKind: RequestKind, args: IForceThunkParam & InputAccountId): Promise<GetAvailabilityInfoDb> {
        return getInfoDbApiProxy().availabilityInfoDbList(
            requestKind,
            {
                accountId: args.accountId,
                accountUserId: args.accountUserId
            }
        ).then(responseDto => {
            return responseDto.Data;
        });
    }

    return {
        receiveInfoDbList,
        receiveInfoDbListOther,
        deleteInfoDbList,
        editInfoDbList,
        availabilityInfoDbList,
        statusInfoDbList
    };
})();