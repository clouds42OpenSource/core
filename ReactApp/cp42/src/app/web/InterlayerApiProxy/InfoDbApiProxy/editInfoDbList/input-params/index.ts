/**
 * Редактирование ИБ
 */
export type editDbListType = {
    databaseId: string;
    caption?: string;
    templateId?: string;
    usedWebServices?: boolean;
    v82Name?: string;
    databaseState?: number;
    hasModifications?: boolean;
    restoreModel?: number;
};