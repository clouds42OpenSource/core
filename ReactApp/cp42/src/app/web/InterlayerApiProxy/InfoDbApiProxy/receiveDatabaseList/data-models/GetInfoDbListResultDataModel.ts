import { SelectDataResultCommonDataModel } from 'app/web/common/data-models';
import { InfoDbListItemDataModel } from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models/InfoDbListItemDataModel';

/**
 * Модель ответа на получение списка баз данных
 */
export type GetInfoDbListResultDataModel = SelectDataResultCommonDataModel<InfoDbListItemDataModel>;