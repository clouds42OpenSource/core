import { TStatusItem } from 'app/api/endpoints/createDb/response';

/**
 * Модель элемента базы данных для списка баз
 */
export type InfoDbListItemDataModel = {
    createAccountDatabaseComment: string | null;
    name: string;
    databaseId: string;
    isFile: boolean;
    template: string;
    lastActivityDate: Date;
    webPublishPath: string;
    sizeInMb: number;
    createStatus?: TStatusItem;
    state: number;
    isDbOnDelimiters: boolean;
    isExistSessionTerminationProcess: boolean;
    isDemo: boolean;
    needShowWebLink: boolean;
    publishState: number;
    templateCaption: string;
    icon: string;
    templateImageName: string;
    configurationName: string;
    hasSupport: boolean;
    hasAutoUpdate: boolean;
    hasAcDbSupport: boolean;
};