import { DateUtility } from 'app/utils';
import { InfoDbListItemDto } from 'app/web/api/InfoDbProxy/responce-dto/GetInfoDbListDtos';
import {
    InfoDbListItemDataModel
} from 'app/web/InterlayerApiProxy/InfoDbApiProxy/receiveDatabaseList/data-models/InfoDbListItemDataModel';

/**
 * Mapping модели базы данных на общей странице
 * @param value ответ модели базы данных
 */
export function infoDbListItemDataModelMapFrom(value: InfoDbListItemDto): InfoDbListItemDataModel {
    return {
        createAccountDatabaseComment: value.CreateAccountDatabaseComment,
        name: value.Name,
        icon: value.TemplateImageName,
        databaseId: value.Id,
        isFile: value.IsFile,
        template: value.TemplateCaption,
        lastActivityDate: DateUtility.convertToDate(value.LastActivityDate),
        webPublishPath: value.DatabaseLaunchLink ?? value.WebPublishPath,
        sizeInMb: value.SizeInMb,
        state: value.State,
        isDbOnDelimiters: value.IsDbOnDelimiters,
        isExistSessionTerminationProcess: value.IsExistSessionTerminationProcess,
        isDemo: value.IsDemo,
        needShowWebLink: value.NeedShowWebLink,
        publishState: value.PublishState,
        templateCaption: value.TemplateCaption,
        templateImageName: value.TemplateImageName,
        configurationName: value.ConfigurationName,
        hasAcDbSupport: value.HasAcDbSupport,
        hasAutoUpdate: value.HasAutoUpdate,
        hasSupport: value.HasSupport
    };
}