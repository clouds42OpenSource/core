import { SelectInfoDbGetQueryParams } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * строка поиска
     */
    searchLine: string;

};

/**
 * Параметры для фильтра баз
 */
export type ReceiveInfoDbListParams = SelectInfoDbGetQueryParams<Filter>;