import { GetUserPermissionByTypeParams } from 'app/web/InterlayerApiProxy/GetUserPermissionsApiProxy/getUserPermissionsData';
import { IoCContainer } from 'app/IoCСontainer';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Прокси для работы с данными Permissions
 */
export const GetUserPermissionsApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getInvoicesProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getUserPermissionsApiProxy();
    }

    /**
     * Получить данные по типу permissions
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getUserPermissionByType(requestKind: RequestKind, args: GetUserPermissionByTypeParams): Promise<string[]> {
        return getInvoicesProxy().getUserPermissionByType(requestKind, args)
            .then(responseDto => {
                return responseDto;
            });
    }

    return {
        getUserPermissionByType
    };
})();