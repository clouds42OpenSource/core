/**
 * Параметры для добавления отрасли
 */
export type GetUserPermissionByTypeParams = {
    Permissions: string[];
};