import { EDepartmentAction } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany';

export type TEditDepartment = {
    departmentId?: string;
    name: string;
    actionType?: EDepartmentAction;
};

export type EditMyCompanyDataModel = {
    accountId: string;
    accountDescription: string;
    accountCaption: string;
    accountInn: string
    isVip: boolean;
    localeId: string;
    emails: string[];
    createClusterDatabase: boolean;
    departments: TEditDepartment[];
};