/**
 * Модель миграции БД
 */
export type ChangeDatabaseMigrationDataModel = {
    storage: string;
    databases: string[];
};