import { DatabaseMigrationItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/data-model/DatabaseMigrationItemDataModel';

/**
 * Модель БД для миграции
 */
export type DatabaseMigrationDataModel = DatabaseMigrationItemDataModel;