import { DatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/data-model';
import { DatabaseMigrationResponseDto, DatabasesItem } from 'app/web/api/MyCompanyProxy/response-dto';

function databasesModelMapFrom(value: DatabasesItem) {
    return {
        records: value.records.map(item => {
            return {
                accountDatabaseId: item.accountDatabaseId,
                v82Name: item.v82Name,
                path: item.path,
                size: item.size,
                status: item.status,
                isPublishDatabase: item.isPublishDatabase,
                isPublishServices: item.isPublishServices,
                isFile: item.isFile
            };
        }),
        metadata: value.metadata
    };
}

/**
 * Выполнить маппинг модели ответа о компании, полученных при запросе, в модель приложения
 * @param value модель ответа при получении данных о компании
 */
export function databaseMigrationDataModelMapFrom(value: DatabaseMigrationResponseDto): DatabaseMigrationDataModel {
    return {
        fileStorageAccountDatabasesSummary: value.fileStorageAccountDatabasesSummary.map(item => {
            return {
                fileStorage: item.fileStorage,
                pathStorage: item.pathStorage,
                databaseCount: item.databaseCount,
                totatlSizeOfDatabases: item.totatlSizeOfDatabases
            };
        }),
        databases: databasesModelMapFrom(value.databases),
        avalableFileStorages: value.avalableFileStorages.map(item => {
            return {
                key: item.key,
                value: item.value
            };
        }),
        availableStoragePath: value.availableStoragePath,
        numberOfDatabasesToMigrate: value.numberOfDatabasesToMigrate
    };
}