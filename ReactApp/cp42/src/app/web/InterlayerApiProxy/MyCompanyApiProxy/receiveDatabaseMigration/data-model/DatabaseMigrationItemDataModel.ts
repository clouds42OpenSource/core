import { Nullable } from 'app/common/types';
import { SelectDataResultMetadataModel } from 'app/web/common/data-models';

type FileStorageAccountDatabasesSummaryItem = {
    fileStorage: string;
    pathStorage: Nullable<string>;
    databaseCount: number;
    totatlSizeOfDatabases: number;
};

export type RecordsItem = {
    accountDatabaseId: string;
    v82Name: string;
    path: string;
    size: number;
    status: number;
    isPublishDatabase: boolean;
    isPublishServices: boolean;
    isFile: boolean;
};

export type DatabasesType = SelectDataResultMetadataModel<RecordsItem>;

export type AvalableFileStoragesItem = {
    key: string;
    value: string;
};

/**
 * Модель БД для миграции
 */
export type DatabaseMigrationItemDataModel = {
    fileStorageAccountDatabasesSummary: FileStorageAccountDatabasesSummaryItem[];
    databases: DatabasesType;
    avalableFileStorages: AvalableFileStoragesItem[];
    availableStoragePath: Nullable<string>[];
    numberOfDatabasesToMigrate: number;
};