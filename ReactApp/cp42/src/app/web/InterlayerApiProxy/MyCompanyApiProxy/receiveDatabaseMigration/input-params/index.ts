import { Nullable } from 'app/common/types';

/**
 * Параметр для получения БД для миграции
 */
export type ReceiveDatabaseMigrationInputParam = {
    accountIndexNumber: Nullable<number>;
    searchLine?: string;
    isTypeStorageFile?: boolean;
    pageNumber: number;
    orderBy?: string;
};