/**
 * Модель данных сегмента компании
 */
export type MyCompanySegmentItemDataModel = {
    id: string;
    name: string;
};