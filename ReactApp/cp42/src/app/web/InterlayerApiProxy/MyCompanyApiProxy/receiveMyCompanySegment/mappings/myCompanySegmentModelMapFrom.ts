import { MyCompanySegmentItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompanySegment/data-models';
import { MyCompanySegmentResponseDto } from 'app/web/api/MyCompanyProxy/response-dto';

/**
 * Выполнить маппинг модели сегментов компании, полученных при запросе, в модель приложения
 * @param value Модель ответа при получении данных о компании
 */
export function myCompanySegmentModelMapFrom(value: MyCompanySegmentResponseDto): MyCompanySegmentItemDataModel[] {
    return value.map(item => item);
}