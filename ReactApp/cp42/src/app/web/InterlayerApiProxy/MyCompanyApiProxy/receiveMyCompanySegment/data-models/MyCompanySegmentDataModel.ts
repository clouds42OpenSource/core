import { MyCompanySegmentItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompanySegment/data-models/MyCompanySegmentItemDataModel';

/**
 * Модель данных сегмента компании
 */
export type MyCompanySegmentDataModel = MyCompanySegmentItemDataModel;