import { IoCContainer } from 'app/IoCСontainer';
import { ChangeDatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeDatabaseMigration/data-models';
import { ChangeMyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/changeMyCompanySegment/data-models';
import { EditMyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/editMyCompany/data-models';
import { DatabaseMigrationDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/data-model';
import { ReceiveDatabaseMigrationInputParam } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveDatabaseMigration/input-params';
import { MyCompanyDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany/data-models';
import { MyCompanySegmentDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompanySegment/data-models';
import { RequestKind } from 'core/requestSender/enums';
import { databaseMigrationDataModelMapFrom } from './receiveDatabaseMigration/mapping';
import { myCompanyDataModelMapFrom } from './receiveMyCompany/mappings';

/**
 * Прокси для работы с сегментами компании
 */
export const MyCompanyApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getMyCompanyProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getMyCompanyProxy();
    }

    /**
     * Получение данных о компании
     * @param requestKind тип запроса
     */
    async function receiveMyCompany(requestKind: RequestKind): Promise<MyCompanyDataModel> {
        return getMyCompanyProxy().receiveMyCompany(requestKind)
            .then(responseDto => myCompanyDataModelMapFrom(responseDto));
    }

    /**
     * Изменение данных о компании
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function editMyCompany(requestKind: RequestKind, args: EditMyCompanyDataModel) {
        return getMyCompanyProxy().editMyCompany(requestKind, {
            AccountId: args.accountId,
            AccountDescription: args.accountDescription,
            AccountCaption: args.accountCaption,
            AccountInn: args.accountInn,
            IsVip: args.isVip,
            LocaleId: args.localeId,
            Emails: args.emails,
            CreateClusterDatabase: args.createClusterDatabase,
            DepartmentRequests: args.departments.flatMap(department => {
                if (department.actionType) {
                    return {
                        Name: department.name,
                        DepartmentId: department.departmentId || undefined,
                        ActionType: department.actionType,
                    };
                }

                return [];
            }),
        });
    }

    return {
        receiveMyCompany,
        editMyCompany
    };
})();

export const MyCompanySegmentApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getMyCompanyProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getMyCompanyProxy();
    }

    /**
     * Получение сегментов компании
     * @param requestKind тип запроса
     */
    async function receiveMyCompanySegment(requestKind: RequestKind): Promise<MyCompanySegmentDataModel[]> {
        return getMyCompanyProxy().receiveMyCompanySegment(requestKind)
            .then(responseDto => responseDto);
    }

    /**
     * Изменение сегментов компании
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function changeMyCompanySegment(requestKind: RequestKind, args: ChangeMyCompanySegmentDataModel): Promise<void> {
        return getMyCompanyProxy().changeMyCompanySegment(requestKind, {
            AccountId: args.accountId,
            SegmentId: args.segmentId
        });
    }

    return {
        receiveMyCompanySegment,
        changeMyCompanySegment
    };
})();

export const DatabaseMigrationApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getMyCompanyProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getMyCompanyProxy();
    }

    /**
     * Получение БД для миграции
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveDatabaseMigration(requestKind: RequestKind, args: ReceiveDatabaseMigrationInputParam): Promise<DatabaseMigrationDataModel> {
        return getMyCompanyProxy().receiveDatabaseMigration(requestKind, {
            'Filter.AccountIndexNumber': args.accountIndexNumber,
            'Filter.SearchLine': args.searchLine ?? '',
            'Filter.IsTypeStorageFile': args.isTypeStorageFile ?? null,
            PageNumber: args.pageNumber,
            OrderBy: args.orderBy
        })
            .then(responseDto => databaseMigrationDataModelMapFrom(responseDto));
    }

    /**
     * Миграции БД
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function changeDatabaseMigration(requestKind: RequestKind, args: ChangeDatabaseMigrationDataModel): Promise<void> {
        return getMyCompanyProxy().changeDatabasesMigration(requestKind, {
            storage: args.storage,
            databases: args.databases
        });
    }

    return {
        receiveDatabaseMigration,
        changeDatabaseMigration
    };
})();