/**
 * Модель изменения сегмента компании
 */
export type ChangeMyCompanySegmentDataModel = {
    /**
     * ID аккаунта
     */
    accountId: string;
    /**
     * ID сегмента
     */
    segmentId: string;
};