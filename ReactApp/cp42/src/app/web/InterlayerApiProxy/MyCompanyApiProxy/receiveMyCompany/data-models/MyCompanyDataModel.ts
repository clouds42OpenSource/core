import { MyCompanyItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany/data-models/MyCompanyItemDataModel';

/**
 * Модель данных о компании
 */
export type MyCompanyDataModel = MyCompanyItemDataModel;