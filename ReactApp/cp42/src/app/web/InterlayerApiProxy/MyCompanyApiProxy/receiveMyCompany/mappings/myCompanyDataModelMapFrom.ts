import { MyCompanyItemDataModel } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy/receiveMyCompany/data-models';
import { MyCompanyResponseDto } from 'app/web/api/MyCompanyProxy/response-dto';

export const myCompanyDataModelMapFrom = ({ locales, accountInn, ...other }: MyCompanyResponseDto): MyCompanyItemDataModel => ({
    ...other,
    locales: locales?.map(item => ({ id: item.key, value: item.value })) ?? null,
    accountInn: accountInn ?? '',
});