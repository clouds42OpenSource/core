import { EVerificationStatus } from 'app/api/endpoints/accounts/enum';
import { TCreateAdditionalCompanyRequest } from 'app/api/endpoints/accounts/request';
import { Nullable } from 'app/common/types';

export type TAdditionalCompany = Omit<TCreateAdditionalCompanyRequest, 'accountId'> & {
    id: string;
};

type LocalesType = {
    id: string;
    value: string;
};

export enum EDepartmentAction {
    create = 1,
    update = 2,
    remove = 3,
}

export type TAccountDepartment = {
    name: string;
    departmentId: string;
    actionType?: EDepartmentAction;
};

export type MyCompanyItemDataModel = {
    accountCaption: string;
    accountDescription: string;
    accountId: string;
    accountInn: string;
    accountSaleManagerCaption: string;
    accountType: string;
    balance: number;
    canChangeLocale: boolean;
    cloudStorageWebLink: string;
    emails: string[];
    indexNumber: number;
    isVip: boolean;
    lastActivity: string;
    localeCurrency: string;
    localeId: string;
    locales: Nullable<LocalesType[]>;
    registrationDate: string;
    segmentId: string;
    segmentName: string;
    serviceTotalSum: number;
    usedSizeOnDisk: number;
    userSource: string;
    additionalCompanies: TAdditionalCompany[];
    verificationStatus: EVerificationStatus | null;
    createClusterDatabase: boolean;
    departments: TAccountDepartment[];
};