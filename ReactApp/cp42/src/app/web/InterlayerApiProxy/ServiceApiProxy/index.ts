import { ELogEvent } from 'app/api/endpoints/global/enum';
import { FETCH_API } from 'app/api/useFetchApi';
import { getEmptyUuid } from 'app/common/helpers';
import uuid from 'app/common/helpers/GenerateUuidHelper/uuid';
import { IoCContainer } from 'app/IoCСontainer';
import { TCreateServiceState } from 'app/views/modules/Services/views/AddServiceCard';
import { TBillingModerationRequest } from 'app/web/api/ServicesProxy/request-dto/BillingModerationRequest';
import { TChangeModerationStatusRequest } from 'app/web/api/ServicesProxy/request-dto/ChangeModerationStatusRequest';
import { TConfigurationsAndCommands, TCreateServiceVersionRequest } from 'app/web/api/ServicesProxy/request-dto/CreateServiceVersionRequest';
import { TFinishFileUploadRequest } from 'app/web/api/ServicesProxy/request-dto/FinishFileUploadRequest';
import { GetModerationListAllRequest } from 'app/web/api/ServicesProxy/request-dto/GetModerationListAllRequest';
import { TServiceActivityRequest } from 'app/web/api/ServicesProxy/request-dto/ServiceActivityRequest';
import { TServiceBillingManagingRequest } from 'app/web/api/ServicesProxy/request-dto/ServiceBillingManaginRequest';
import { TStartFileUploadRequest } from 'app/web/api/ServicesProxy/request-dto/StartFileUploadRequest';
import { TFileInformationResponse, TServiceBillingDataResponse } from 'app/web/api/ServicesProxy/response-dto';
import { TFilePart } from 'app/web/api/ServicesProxy/response-dto/StartFileIUploadResponse';
import { ServiceProxy } from 'app/web/api/ServicesProxy/ServicesProxy';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy/index';
import { billingResponseToRequest, serviceStateToRequest } from 'app/web/InterlayerApiProxy/ServiceApiProxy/mapping';
import { RequestKind, WebVerb } from 'core/requestSender/enums';

type ErrorResponse = {
    Code: number;
    Description: string;
};

const { sendLogEvent } = FETCH_API.GLOBAL;

export class ServiceApiProxy extends ServiceProxy {
    constructor() {
        const { apiHost, requestSender, msMainHost } = IoCContainer.getApiProxy().ApiProxyVariables;
        super(requestSender, apiHost, msMainHost);
        this.receiveMarket42Services = this.receiveMarket42Services.bind(this);
        this.receiveBillingServiceData = this.receiveBillingServiceData.bind(this);
        this.receiveAccountServices = this.receiveAccountServices.bind(this);
        this.receiveConfigsFromMs = this.receiveConfigsFromMs.bind(this);
        this.receiveServiceInfo = this.receiveServiceInfo.bind(this);
        this.deleteServiceInfo = this.deleteServiceInfo.bind(this);
        this.receiveServiceDatabases = this.receiveServiceDatabases.bind(this);
        this.installServiceIntoApplication = this.installServiceIntoApplication.bind(this);
        this.deleteServiceFromApplication = this.deleteServiceFromApplication.bind(this);
        this.receiveConfigInfoFromMs = this.receiveConfigInfoFromMs.bind(this);
        this.receiveServiceIndustries = this.receiveServiceIndustries.bind(this);
        this.receiveServiceInformation = this.receiveServiceInformation.bind(this);
        this.receiveServiceVersionInformation = this.receiveServiceVersionInformation.bind(this);
        this.createNewServiceVersion = this.createNewServiceVersion.bind(this);
        this.reworkServiceVersion = this.reworkServiceVersion.bind(this);
        this.startUploadFileToMs = this.startUploadFileToMs.bind(this);
        this.putFileToMs = this.putFileToMs.bind(this);
        this.finishUploadFileToMs = this.finishUploadFileToMs.bind(this);
        this.deleteServiceVersion = this.deleteServiceVersion.bind(this);
        this.createServiceCard = this.createServiceCard.bind(this);
        this.setServiceData = this.setServiceData.bind(this);
        this.reworkServiceBilling = this.reworkServiceBilling.bind(this);
        this.receiveBillingConnectionType = this.receiveBillingConnectionType.bind(this);
        this.modifyServiceCard = this.modifyServiceCard.bind(this);
        this.receiveModerationList = this.receiveModerationList.bind(this);
        this.setModerationStatus = this.setModerationStatus.bind(this);
        this.receiveModerationListAll = this.receiveModerationListAll.bind(this);
        this.receiveBillingModeration = this.receiveBillingModeration.bind(this);
        this.setBillingModerationStatus = this.setBillingModerationStatus.bind(this);
        this.receiveBillingValidation = this.receiveBillingValidation.bind(this);
        this.reworkServiceBillingManaging = this.reworkServiceBillingManaging.bind(this);
        this.changeServiceActivity = this.changeServiceActivity.bind(this);
    }

    public receiveMarket42Services(requestKind: RequestKind) {
        return this.getMarket42Services(requestKind);
    }

    public async receiveBillingServiceData(requestKind: RequestKind, serviceId: string) {
        const response = await this.getBillingServiceData(requestKind, serviceId);

        if (!response) {
            return (response as unknown as ErrorResponse).Description;
        }

        const responseObject = response as object;

        if (responseObject.hasOwnProperty('Code')) {
            return (response as unknown as ErrorResponse).Description;
        }

        return response;
    }

    public receiveBillingConnectionType(requestKind: RequestKind) {
        return this.getBillingConnectionTypes(requestKind);
    }

    public receiveAccountServices(requestKind: RequestKind) {
        return this.getAccountServices(requestKind);
    }

    public receiveConfigInfoFromMs(requestKind: RequestKind, fileId: string, idService?: string, isService = true) {
        return this.getFileInfoFromMs(requestKind, fileId, isService, idService);
    }

    private startUploadFileToMs(requestKind: RequestKind, requestBody: TStartFileUploadRequest) {
        return this.startFileUploading(requestKind, requestBody);
    }

    private async putFileToMs(requestKind: RequestKind, fileChunkArrayInfo: TFilePart[], file: File, chunkSize: number) {
        try {
            const chunkFormDataName = 'data';
            const eTagArray = new Array<string>();

            for (let start = 0, index = 0; start < file.size; start += chunkSize, ++index) {
                const fileChunk = file.slice(start, start + chunkSize);
                const chunkFormData = new FormData();
                chunkFormData.set(chunkFormDataName, fileChunk);
                const eTag = await this.fileChunkUploading(requestKind, chunkFormData.get(chunkFormDataName), fileChunkArrayInfo[index]);

                if (eTag) {
                    eTagArray.push(eTag);
                }
            }
            return eTagArray;
        } catch (err: unknown) {
            return null;
        }
    }

    private finishUploadFileToMs(requestKind: RequestKind, requestBody: TFinishFileUploadRequest, fileId: string) {
        return this.finishFileUploading(requestKind, requestBody, fileId);
    }

    public async uploadFileToMs(requestKind: RequestKind, file: File, type: 'service' | 'service_attachment', chunkSize = 30 * 1024 ** 2) {
        const startResponse = await this.startUploadFileToMs(requestKind, {
            size: file.size,
            'part-size': chunkSize,
            name: file.name,
            'account-id': this.accountId,
            type
        });

        if (startResponse) {
            const uploadResponse = await this.putFileToMs(requestKind, startResponse.parts, file, chunkSize);

            if (uploadResponse) {
                const uploadRequestBody: TFinishFileUploadRequest = { parts: uploadResponse };

                return await this.finishUploadFileToMs(requestKind, uploadRequestBody, startResponse.id);
            }
        }

        return null;
    }

    public async receiveServiceInformation(requestKind: RequestKind, serviceId: string) {
        const serviceInformationResponse = await this.getService(requestKind, serviceId);

        if (serviceInformationResponse) {
            const serviceInformation = serviceInformationResponse.Result;

            if (!serviceInformation) {
                return null;
            }

            const {
                ServiceName,
                ServiceShortDescription,
                Private,
                ServiceIconID,
                ServiceScreenshots,
                ServiceDescription,
                InstructionID,
                Instruction,
                Hybrid,
                Industries,
                Draft,
                Tags,
                TargetAudience
            } = serviceInformation;

            const mappedServiceInformation: TCreateServiceState = {
                draft: Draft,
                fields: {
                    name: ServiceName,
                    description: ServiceShortDescription,
                },
                isForMarket: !Private,
                hybrid: Hybrid,
                logoId: ServiceIconID,
                screenshotsId: ServiceScreenshots.map(value => value.match(/attachments\/([0-9a-z-]{1,36})/)?.[1] ?? ''),
                richTextDescription: ServiceDescription,
                instructionUrl: InstructionID,
                instruction: Instruction,
                isMultipleFiles: false,
                configFiles: [],
                isLoading: false,
                industry: Industries,
                tags: Tags,
                audience: TargetAudience,
                onSubmitPreview: false
            };

            return { mappedServiceInformation, serviceInformation };
        }

        return null;
    }

    public receiveServiceVersionInformation(requestKind: RequestKind, serviceId: string) {
        return this.getServiceVersion(requestKind, serviceId);
    }

    public receiveConfigsFromMs(requestKind: RequestKind) {
        return this.getConfigsFromMs(requestKind);
    }

    public receiveServiceInfo(requestKind: RequestKind, id: string) {
        return this.serviceInfoRequest(requestKind, WebVerb.GET, id);
    }

    public deleteServiceInfo(requestKind: RequestKind, id: string) {
        return this.serviceInfoRequest(requestKind, WebVerb.DELETE, id);
    }

    public receiveServiceDatabases(id: string) {
        return this.getServiceDatabases(id);
    }

    public installServiceIntoApplication(requestKind: RequestKind, applicationId: string, id: string) {
        return this.serviceStatusRequest(requestKind, WebVerb.PUT, applicationId, id);
    }

    public deleteServiceFromApplication(requestKind: RequestKind, applicationId: string, id: string) {
        return this.serviceStatusRequest(requestKind, WebVerb.DELETE, applicationId, id);
    }

    public receiveServiceIndustries(requestKind: RequestKind) {
        return this.getIndustries(requestKind);
    }

    public deleteServiceVersion(requestKind: RequestKind, versionId: string, serviceId: string) {
        return this.serviceVersionRequest(requestKind, WebVerb.DELETE, serviceId, versionId);
    }

    public reworkServiceVersion(requestKind: RequestKind, versionId: string, serviceId: string) {
        return this.serviceVersionRequest(requestKind, WebVerb.POST, serviceId, versionId);
    }

    public async reworkServiceBilling(requestKind: RequestKind, data: TServiceBillingDataResponse, serviceId: string, isCreate: boolean, requestId: string, hybrid: boolean, name?: string) {
        const requestData = billingResponseToRequest(data, this.accountId!, requestId, serviceId, hybrid, name);

        if (isCreate) {
            return await this.createServiceBilling(requestKind, requestData);
        }

        if (data.BillingServiceStatus === 1) {
            requestData.CreateServiceRequestId = getEmptyUuid();
            return await this.updateServiceBillingDraft(requestKind, requestData);
        }

        const response = await this.updateServiceBilling(requestKind, requestData);

        if (response?.hasOwnProperty('Description')) {
            void this.setModerationStatus(
                RequestKind.SEND_BY_USER_ASYNCHRONOUSLY,
                requestData.CreateServiceRequestId,
                {
                    accepted: false,
                    comment: response?.Description ?? ''
                }
            );

            return response?.Description;
        }

        return response;
    }

    public reworkServiceBillingManaging(requestKind: RequestKind, data: TServiceBillingManagingRequest) {
        return this.updateServiceBillingManaging(requestKind, data);
    }

    public async createNewServiceVersion(requestKind: RequestKind, data: TFileInformationResponse, serviceId: string) {
        const configMinVersions: TConfigurationsAndCommands = {};

        for (const config of data.configurations) {
            configMinVersions[config.code] = config.version ?? '00.00.00.00';
        }

        const createServiceVersionData: TCreateServiceVersionRequest = {
            draft: data.draft,
            'account-id': this.accountId!,
            'user-id': this.userId!,
            version: data.version,
            'full-name': data.name,
            synonym: data.synonym,
            comment: data.comment,
            'configurations-min-versions': configMinVersions,
            'unsafe-mode': data.isNotSafe ?? false,
            'file-id': data.fileId!,
            commands: data.chosenCommands.map(chosenCommand => {
                const weekDays: number[] = [];

                if (chosenCommand['week-days'] !== undefined) {
                    chosenCommand['week-days'].forEach((day: number) => {
                        if (!isNaN(day)) {
                            weekDays.push(day);
                        }
                    });
                }

                return {
                    ...chosenCommand,
                    'week-days': weekDays
                };
            })
        };

        return await this.createServiceVersion(requestKind, createServiceVersionData, serviceId, uuid());
    }

    public async setServiceData(requestKind: RequestKind, data: TCreateServiceState, serviceId: string, billingData?: TServiceBillingDataResponse) {
        const isUserBilling = !!billingData?.BillingServiceTypes.find(serviceType => serviceType.BillingType === 0);
        let price = 0;
        if (billingData) {
            price = Math.min(...billingData.BillingServiceTypes.map(serviceType => serviceType.ServiceTypeCost));
        }

        return this.createService(requestKind, serviceStateToRequest(data, this.accountId!, isUserBilling, price), serviceId);
    }

    public async modifyServiceCard(requestKind: RequestKind, allData: TCreateServiceState, serviceId: string, billingData?: TServiceBillingDataResponse, isServiceHasBilling?: boolean) {
        const { isForMarket, draft } = allData;
        const createServiceResult = await this.setServiceData(requestKind, allData, serviceId, billingData);

        if (isForMarket && !createServiceResult?.Result && !draft) {
            return createServiceResult;
        }

        if (allData.configFiles.length > 0) {
            allData.configFiles.forEach(configFile => {
                configFile.draft = allData.draft;
                this.createNewServiceVersion(requestKind, configFile, serviceId);
            });
        }

        if (isForMarket && billingData && createServiceResult && (billingData.IsExistsChangeServiceRequest || allData.draft || billingData.BillingServiceStatus === 1)) {
            billingData.BillingServiceStatus = allData.draft ? 1 : 2;

            return this.reworkServiceBilling(requestKind, billingData, serviceId, !(isServiceHasBilling), createServiceResult.Result?.['proposal-id'] ?? '', allData.hybrid, allData.fields.name);
        }

        return createServiceResult;
    }

    public async createServiceCard(requestKind: RequestKind, allData: TCreateServiceState, billingData?: TServiceBillingDataResponse, isForMarket = false, serviceId = uuid()) {
        const createServiceResult = await this.setServiceData(requestKind, allData, serviceId, billingData);

        if (createServiceResult && !('error' in createServiceResult)) {
            const configsResult = await Promise.all(allData.configFiles.map(configFile => {
                configFile.draft = allData.draft;
                return this.createNewServiceVersion(requestKind, configFile, serviceId);
            }));
            let badRequest = 0;

            for (const result of configsResult) {
                if (result === null || 'error' in configsResult) {
                    badRequest++;
                }
            }

            if (badRequest >= configsResult.length && !allData.draft) {
                await this.deleteServiceInfo(requestKind, serviceId);

                return null;
            }

            if (allData.isForMarket && billingData) {
                if (allData.draft) {
                    billingData.BillingServiceStatus = 1;
                }

                const billingSuccess = await this.reworkServiceBilling(requestKind, billingData, serviceId, true, createServiceResult.Result?.['proposal-id'] ?? '', allData.hybrid, allData.fields.name);

                if ((billingSuccess && billingSuccess.hasOwnProperty('Code')) || !billingSuccess) {
                    await this.deleteServiceInfo(requestKind, serviceId);

                    return billingSuccess;
                }
            }

            if (!allData.isForMarket) {
                void sendLogEvent(ELogEvent.CreatePrivateService, `Создан сервис "${ allData.fields.name }".`);
            }

            return {};
        }

        return createServiceResult;
    }

    public receiveModerationList(requestKind: RequestKind, serviceId: string) {
        return this.getModerationList(requestKind, serviceId);
    }

    public async receiveModerationListAll(requestKind: RequestKind, query: GetModerationListAllRequest) {
        if (!query.pageSize) {
            query.pageSize = 50;
        }

        return this.getModerationListAll(requestKind, query);
    }

    public setModerationStatus(requestKind: RequestKind, requestId: string, data: TChangeModerationStatusRequest) {
        return this.changeModerationStatus(requestKind, requestId, data);
    }

    public async receiveBillingModeration(requestKind: RequestKind, requestId: string) {
        const result = await this.getBillingModeration(requestKind, requestId);

        if (!result || result.hasOwnProperty('Code')) {
            return (result as unknown as ErrorResponse).Description;
        }

        if (result.NewServiceData && result.NewServiceData.BillingServiceTypes) {
            result.NewServiceData.BillingServiceTypes = result.NewServiceData.BillingServiceTypes.map(serviceType => ({
                ...serviceType,
                Id: serviceType.ServiceTypeId ?? ''
            }));
        }

        return result;
    }

    public setBillingModerationStatus(requestKind: RequestKind, data: TBillingModerationRequest) {
        return this.billingModerationResult(requestKind, data);
    }

    public receiveBillingValidation(requestKind: RequestKind, data: TServiceBillingDataResponse, serviceId: string, isCreate: boolean, hybrid: boolean, name?: string) {
        const requestData = billingResponseToRequest(data, this.accountId!, uuid(), serviceId, hybrid, name);
        return this.getBillingInfoValidate(requestKind, requestData, isCreate);
    }

    public changeServiceActivity(requestKind: RequestKind, data: TServiceActivityRequest) {
        return this.setServiceActivity(requestKind, data);
    }
}