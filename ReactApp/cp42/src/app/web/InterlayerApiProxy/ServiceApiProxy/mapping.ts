import { TCreateServiceState } from 'app/views/modules/Services/views/AddServiceCard';
import { TCreateServiceRequest } from 'app/web/api/ServicesProxy/request-dto/CreateServiceRequest';
import { TServiceBillingCreateDataRequest } from 'app/web/api/ServicesProxy/request-dto/ServiceBillingCreateDataRequest';
import { TServiceBillingDataResponse } from 'app/web/api/ServicesProxy/response-dto';

export const serviceStateToRequest = (
    {
        fields: { name, description },
        richTextDescription,
        instruction,
        screenshotsId,
        isForMarket,
        logoId,
        industry,
        hybrid,
        draft,
        tags,
        audience
    }: TCreateServiceState,
    accountId: string,
    userBilling: boolean,
    price: number
): TCreateServiceRequest => {
    return {
        draft,
        name,
        'short-description': description,
        opportunities: richTextDescription,
        icon: logoId,
        instruction: instruction ?? '',
        'min-cost': price,
        screenshots: screenshotsId,
        industries: industry,
        private: !isForMarket,
        hybrid,
        'account-id': accountId,
        'user-billing': userBilling,
        tags,
        'target-audience': audience as string[]
    };
};

export const billingResponseToRequest = (
    { AccountId, BillingServiceStatus, BillingServiceTypes, Name, Key }: TServiceBillingDataResponse,
    accountId: string,
    requestId: string,
    serviceId: string,
    hybrid: boolean,
    name?: string
): TServiceBillingCreateDataRequest => {
    const id = Key.length === 0 ? serviceId : Key;

    return {
        AccountId: accountId ?? AccountId,
        BillingServiceStatus: BillingServiceStatus ?? 2,
        BillingServiceTypes,
        CreateServiceRequestId: requestId,
        Key: id,
        Id: id,
        Name: name ?? Name,
        IsHybridService: hybrid
    };
};