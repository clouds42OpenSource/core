import { LocalizationsItemResponseDto } from 'app/web/api/LocalesProxy/response-dto';

export type LocalizationsDataModel = LocalizationsItemResponseDto[];