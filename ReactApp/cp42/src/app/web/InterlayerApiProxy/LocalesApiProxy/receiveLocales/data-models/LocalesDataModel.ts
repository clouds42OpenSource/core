import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { LocaleItemDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models/LocaleItemDataModel';

/**
 * Модель данных локалей
 */
export type LocalesDataModel = SelectDataResultMetadataModel<LocaleItemDataModel>;