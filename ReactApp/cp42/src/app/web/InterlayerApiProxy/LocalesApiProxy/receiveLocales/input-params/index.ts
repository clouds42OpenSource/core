import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { LocalesFilterDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';

/**
 * Параметры для получения локалей
 */
export type ReceiveLocalesParams = SelectDataCommonDataModel<LocalesFilterDataModel>;