import { Nullable } from 'app/common/types';

/**
 * Модель локали
 */
export type LocaleItemDataModel = {
    /**
     * ID локали
     */
    localeId: string;
    /**
     * Название
     */
    name: string;
    /**
     * Страна
     */
    country: string;
    /**
     * Код валюты
     */
    currencyCode: Nullable<number>;
    /**
     * Валюта
     */
    currency: string;
};