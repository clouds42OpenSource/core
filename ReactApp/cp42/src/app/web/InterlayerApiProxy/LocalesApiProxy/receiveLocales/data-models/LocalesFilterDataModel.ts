import { Nullable } from 'app/common/types';

/**
 * Модель фильтра поиска локалей
 */
export type LocalesFilterDataModel = {

    /**
     * Строка поиска
     */
    searchString: Nullable<string>;
};