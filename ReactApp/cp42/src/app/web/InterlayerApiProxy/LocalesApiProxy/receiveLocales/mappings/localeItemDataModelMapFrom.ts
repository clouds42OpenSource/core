import { LocaleItemResponseDto } from 'app/web/api/LocalesProxy/response-dto';
import { LocaleItemDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';

/**
 * Выполнить маппинг модели данных локали, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по локали
 */
export function localeItemDataModelMapFrom(value: LocaleItemResponseDto): LocaleItemDataModel {
    return {
        localeId: value.id,
        country: value.country,
        currency: value.currency,
        currencyCode: value.currencyCode,
        name: value.name
    };
}