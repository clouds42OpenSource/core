import { LocalesFilterDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';
import { ReceiveLocalesFilterRequestDto } from 'app/web/api/LocalesProxy/request-dto';

/**
 * Маппинг модели фильтра получения локалей в модель запроса на API метод
 * @param value Модель фильтра локалей
 */
export function receiveLocalesFilterRequestDtoMapFrom(value: LocalesFilterDataModel): ReceiveLocalesFilterRequestDto {
    return {
        searchLine: value.searchString
    };
}