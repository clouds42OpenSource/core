import { LocaleItemDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models/LocaleItemDataModel';

/**
 * Модель данных локали
 */
export type LocaleDataModel = LocaleItemDataModel & {
    /**
     * Адрес сайта личного кабинета
     */
    cpSiteUrl: string;

    /**
     * ID сегмента по умолчанию
     */
    defaultSegmentId: string;
};