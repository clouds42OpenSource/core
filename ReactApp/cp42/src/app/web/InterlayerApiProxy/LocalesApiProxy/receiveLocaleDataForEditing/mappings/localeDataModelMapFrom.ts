import { LocaleDataResponseDto } from 'app/web/api/LocalesProxy/response-dto';
import { LocaleDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';

/**
 * Выполнить маппинг модели данных локали, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по локали
 */
export function localeDataModelMapFrom(value: LocaleDataResponseDto): LocaleDataModel {
    return {
        localeId: value.id,
        country: value.country,
        currency: value.currency,
        currencyCode: value.currencyCode,
        name: value.name,
        cpSiteUrl: value.cpSiteUrl,
        defaultSegmentId: value.defaultSegmentId
    };
}