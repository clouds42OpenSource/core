/**
 * Параметры получения данных для редактирования локали
 */
export type ReceiveLocaleDataForEditingParams = {
    /**
     * ID локали
     */
    localeId: string;
};