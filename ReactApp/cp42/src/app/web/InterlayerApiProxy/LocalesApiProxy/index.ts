import { IoCContainer } from 'app/IoCСontainer';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { EditLocaleDataParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/editLocale/data-models';
import { ReceiveLocaleDataForEditingParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocaleDataForEditing/input-params';
import { localeDataModelMapFrom } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocaleDataForEditing/mappings/localeDataModelMapFrom';
import { LocaleDataModel, LocalesDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/data-models';
import { LocalizationsDataModel } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocalizations/data-models';
import { ReceiveLocalesParams } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/input-params';
import { receiveLocalesFilterRequestDtoMapFrom } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/mappings';
import { localeItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/LocalesApiProxy/receiveLocales/mappings/localeItemDataModelMapFrom';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Прокси для работы с данными локалей
 */
export const LocalesApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getLocalesProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getLocalesProxy();
    }

    /**
     * Получить данные по локалям
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveLocales(requestKind: RequestKind, args: ReceiveLocalesParams): Promise<LocalesDataModel> {
        return getLocalesProxy().receiveLocales(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter ? receiveLocalesFilterRequestDtoMapFrom(args.filter) : null,
            orderBy: args.orderBy
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, localeItemDataModelMapFrom));
    }

    /**
     * Получить данные локали для редактирования
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveLocaleDataForEditing(requestKind: RequestKind, args: ReceiveLocaleDataForEditingParams): Promise<LocaleDataModel> {
        return getLocalesProxy().receiveLocaleDataForEditing(requestKind, {
            localeId: args.localeId
        }).then(responseDto => localeDataModelMapFrom(responseDto));
    }

    /**
     * Изменить локаль
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function editLocale(requestKind: RequestKind, args: EditLocaleDataParams): Promise<void> {
        return getLocalesProxy().editLocale(requestKind, {
            id: args.localeId,
            cpSiteUrl: args.cpSiteUrl,
            defaultSegmentId: args.defaultSegmentId
        });
    }

    async function receiveLocalizations(requestKind: RequestKind): Promise<LocalizationsDataModel> {
        return getLocalesProxy().getLocalizations(requestKind).then(responseDto => responseDto);
    }

    return {
        receiveLocales,
        receiveLocaleDataForEditing,
        editLocale,
        receiveLocalizations
    };
})();