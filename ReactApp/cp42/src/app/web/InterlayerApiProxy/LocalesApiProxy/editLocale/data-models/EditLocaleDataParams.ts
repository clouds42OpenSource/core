/**
 * Модель редактирования локали
 */
export type EditLocaleDataParams = {
    /**
     * ID локали
     */
    localeId: string;
    /**
     * Адрес сайта личного кабинета
     */
    cpSiteUrl: string;
    /**
     * ID сегмента по умолчанию
     */
    defaultSegmentId: string;
};