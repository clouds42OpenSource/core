/**
 * Модель параметров для добавления задачи в очередь
 */
export type AddTaskToQueueParams = {
    /**
     * ID задачи
     */
    CloudTaskId: string;
    /**
     * Параметры задачи
     */
    TaskParams: string | null;
    /**
     * Коментарий к задаче
     */
    Comment: string | null;
};