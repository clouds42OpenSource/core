import { DateUtility } from 'app/utils';
import { TaskInQueueItemCardDataResponseDto } from 'app/web/api/TaskInQueueItemCardProxy/response-dto/TaskInQueueItemCardDataResponseDto';
import { TaskInQueueItemCardDataModel } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/data-models';

/**
 * Mapping модели данных для карточки информации о задаче из очереди задач воркера
 * @param value Модель данных для карточки информации о задаче из очереди задач воркера
 */
export function taskInQueueItemCardDataModelMapFrom(value: TaskInQueueItemCardDataResponseDto): TaskInQueueItemCardDataModel {
    const startDate = value.startDateTime
        ? DateUtility.convertToDate(value.startDateTime)
        : undefined;
    return {
        ...value,
        startDate,
        priority: value.priority ?? -1,
    };
}