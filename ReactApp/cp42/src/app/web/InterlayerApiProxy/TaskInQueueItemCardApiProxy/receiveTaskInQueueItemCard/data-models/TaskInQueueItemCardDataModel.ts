import { CoreWorkerTaskInQueueTypeEnum } from 'app/common/enums/CoreWorkerTaskInQueueTypeEnum';

/**
 * Модель данных для карточки информации о задаче из очереди задач воркера
 */
export type TaskInQueueItemCardDataModel = {
    /**
     * Название задачи
     */
    taskName: string;
    /**
     * Параметры задачи
     */
    taskParams: string;
    /**
     * Дата запуска
     */
    startDate: Date | undefined;
    /**
     * Приоритет
     */
    priority: number;
    /**
     * Тип задачи
     */
    taskType: CoreWorkerTaskInQueueTypeEnum;
    /**
     * Комментарий к задаче
     */
    comment: string;
    /**
     * ID задачи из очереди задач воркера
     */
    taskInQueueItemId: string;
    /**
     * Статус
     */
    status: string;
};