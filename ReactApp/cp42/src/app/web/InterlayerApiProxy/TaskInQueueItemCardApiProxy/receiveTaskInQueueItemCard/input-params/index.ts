/**
 * Параметры для получения данных для карточки информации о задаче из очереди задач воркера
 */
export type ReceiveTaskInQueueItemCardParams = {
    /**
     * ID задачи из очереди задач воркера
     */
    taskInQueueItemId: string;
};