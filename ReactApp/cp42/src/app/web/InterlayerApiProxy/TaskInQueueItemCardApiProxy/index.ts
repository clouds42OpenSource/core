import { IoCContainer } from 'app/IoCСontainer';
import { AddTaskToQueueParams } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/addTaskToQueue/input-params/AddTaskToQueueParams';
import { TaskInQueueItemCardDataModel } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/data-models';
import { ReceiveTaskInQueueItemCardParams } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/input-params';
import { taskInQueueItemCardDataModelMapFrom } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy/receiveTaskInQueueItemCard/mappings/taskInQueueItemCardDataModelMapFrom';
import { RequestKind } from 'core/requestSender/enums';

export const TaskInQueueItemCardApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getTaskInQueueItemCardApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getTaskInQueueItemCardProxy();
    }

    /**
     * Получить информацию о задаче, из очереди задач воркера
     * @param requestKind Вид запроса
     * @param args Параметры для получения данных для карточки информации о задаче из очереди задач воркера
     */
    async function receiveTaskInQueueItemCard(requestKind: RequestKind, args: ReceiveTaskInQueueItemCardParams): Promise<TaskInQueueItemCardDataModel> {
        return getTaskInQueueItemCardApiProxy().receiveTaskInQueueItemCard(requestKind, {
            taskInQueueItemId: args.taskInQueueItemId
        }).then(responseDto => {
            return taskInQueueItemCardDataModelMapFrom(responseDto);
        });
    }

    /**
     * Добавить новую задачу в очередь
     * @param requestKind Вид запроса
     * @param args Модель параметров для добавления задачи в очередь
     */
    async function addTaskToQueue(requestKind: RequestKind, args: AddTaskToQueueParams): Promise<void> {
        return getTaskInQueueItemCardApiProxy().addTaskToQueue(requestKind, {
            Comment: args.Comment,
            CloudTaskId: args.CloudTaskId,
            TaskParams: args.TaskParams
        });
    }

    return {
        receiveTaskInQueueItemCard,
        addTaskToQueue
    };
})();