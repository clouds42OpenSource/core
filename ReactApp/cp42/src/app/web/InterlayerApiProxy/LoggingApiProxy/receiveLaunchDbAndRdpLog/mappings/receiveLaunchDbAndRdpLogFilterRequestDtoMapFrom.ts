import { ReceiveLaunchDbAndRdpLogFilterRequestDto } from 'app/web/api/LoggingProxy/request-dto';
import { LaunchDbAndRdpLogFilterDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog';

/**
 * Mapping модели фильтра о получении логирования запуска баз и RDP в модель запроса на API метод
 * @param value Модель фильтра о получении логирования запуска баз и RDP
 */
export function receiveLaunchDbAndRdpLogFilterRequestDtoMapFrom(value: LaunchDbAndRdpLogFilterDataModel): ReceiveLaunchDbAndRdpLogFilterRequestDto {
    return {
        AccountNumber: parseInt(value.accountNumber, 10),
        LinkAppVersion: value.linkAppVersion,
        Login: value.login,
        V82Name: value.v82Name,
        ExternalIpAddress: value.externalIpAddress,
        InternalIpAddress: value.internalIpAddress,
    };
}