import { MetadataModel } from 'app/web/common/data-models';
import { LaunchDbAndRdpLogItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog/data-models/LaunchDbAndRdpLogItemDataModel';

/**
 * Модель ответа при получении логирования запуска баз и RDP
 */
export type LaunchDbAndRdpLogDataModel = {
    /**
     * Массив данных о логировании запуска баз и RDP
     */
    records: LaunchDbAndRdpLogItemDataModel[];
    /**
     * Информация о страницах логирования запуска баз и RDP
     */
    metadata: MetadataModel;
};