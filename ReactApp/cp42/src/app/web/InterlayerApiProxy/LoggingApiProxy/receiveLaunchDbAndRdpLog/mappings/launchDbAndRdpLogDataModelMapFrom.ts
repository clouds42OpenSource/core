import { DateUtility } from 'app/utils';
import { LaunchDbAndRdpLogItemResponseDto } from 'app/web/api/LoggingProxy/response-dto';
import { LaunchDbAndRdpLogItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog/data-models/LaunchDbAndRdpLogItemDataModel';

/**
 * Mapping модели записи о логировании запуска баз и  RDP полученное при запросе в модель приложения
 * @param value Модель записи о логировании запуска баз и RDP полученное при запросе
 */
export function launchDbAndRdpLogDataModelMapFrom(value: LaunchDbAndRdpLogItemResponseDto): LaunchDbAndRdpLogItemDataModel {
    return {
        id: value.id,
        accountNumber: value.accountNumber,
        actionCreated: DateUtility.convertToDate(value.actionCreated),
        action: value.action,
        login: value.login,
        linkAppVersion: value.linkAppVersion,
        linkAppType: value.linkAppType,
        v82Name: value.v82Name,
        launchType: value.launchType,
        externalIpAddress: value.externalIpAddress,
        internalIpAddress: value.internalIpAddress
    };
}