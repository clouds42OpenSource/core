import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { LaunchDbAndRdpLogFilterDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog/data-models/LaunchDbAndRdpLogFilterDataModel';

/**
 * Параметры для загрузки логирования запуска информационных баз и RDP
 */
export type ReceiveLaunchDbAndRdpLogParams = SelectDataCommonDataModel<LaunchDbAndRdpLogFilterDataModel>;