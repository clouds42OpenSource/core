import { Nullable } from 'app/common/types';

/**
 * Модель фильтра для получения лога о запуске баз и открытии RDP
 */
export type LaunchDbAndRdpLogFilterDataModel = {
    /**
     * Номер аккаунта
     */
    accountNumber: string;
    /**
     * Логин
     */
    login: string;
    /**
     * Номер информационной базы
     */
    v82Name: string;
    /**
     * Версия Линка
     */
    linkAppVersion: string;
    /**
     * Внешний IP адрес
     */
    externalIpAddress: Nullable<string>;
    /**
     * Внутренний IP адрес
     */
    internalIpAddress: Nullable<string>;
};