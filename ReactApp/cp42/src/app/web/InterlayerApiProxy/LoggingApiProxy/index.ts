import { IoCContainer } from 'app/IoCСontainer';
import { DateUtility } from 'app/utils';
import { receiveLaunchDbAndRdpLogFilterRequestDtoMapFrom } from 'app/web/InterlayerApiProxy/LoggingApiProxy/mappings';
import { CloudChangesDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/data-models';
import { ReceiveCloudChangesParams } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/input-params';
import { receiveCloudChangesFilterRequestDtoMapFrom } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/mappings';
import { CloudChangesActionDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChangesActionsData/data-models';
import { cloudChangesActionDataModelMapFrom } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChangesActionsData/mappings';
import { ReceiveLaunchDbAndRdpLogParams } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog';
import { LaunchDbAndRdpLogDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveLaunchDbAndRdpLog/data-models/LaunchDbAndRdpLogDataModel';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Прокси для работы с логированием
 */
export const LoggingApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getLoggingApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getLoggingApiProxy();
    }

    /**
     * Получить данные логов о запуске баз и RDP
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveLaunchDbAndRdpLog(requestKind: RequestKind, args: ReceiveLaunchDbAndRdpLogParams): Promise<LaunchDbAndRdpLogDataModel> {
        return getLoggingApiProxy().receiveLaunchDbAndRdpLog(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter ? receiveLaunchDbAndRdpLogFilterRequestDtoMapFrom(args.filter) : null,
            orderBy: args.orderBy,
        }).then(responseDto => {
            return {
                metadata: responseDto.metadata,
                records: responseDto.records.map(record => ({ ...record, actionCreated: DateUtility.convertToDate(record.actionCreated) }))
            };
        });
    }

    /**
     * Получить данные логов действий облака
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveCloudChanges(requestKind: RequestKind, args: ReceiveCloudChangesParams): Promise<CloudChangesDataModel> {
        return getLoggingApiProxy().receiveCloudChanges(requestKind, {
            PageNumber: args.pageNumber,
            orderBy: args.orderBy,
            Filter: args.filter ? receiveCloudChangesFilterRequestDtoMapFrom(args.filter) : null
        }).then(responseDto => {
            return {
                metadata: responseDto.metadata,
                records: responseDto.records.map(record => ({ ...record, createCloudChangeDate: DateUtility.convertToDate(record.createCloudChangeDate) }))
            };
        });
    }

    /**
     * Получить данные по действиям логов
     * @param requestKind тип запроса
     */
    async function receiveCloudChangesActionsData(requestKind: RequestKind): Promise<Array<CloudChangesActionDataModel>> {
        return getLoggingApiProxy().receiveCloudChangesActionsData(requestKind).then(responseDto => {
            return responseDto.map(item => cloudChangesActionDataModelMapFrom(item));
        });
    }

    return {
        receiveLaunchDbAndRdpLog,
        receiveCloudChanges,
        receiveCloudChangesActionsData
    };
})();