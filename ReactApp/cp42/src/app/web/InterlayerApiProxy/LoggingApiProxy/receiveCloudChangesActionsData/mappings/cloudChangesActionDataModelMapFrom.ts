import { CloudChangesActionResponseDto } from 'app/web/api/LoggingProxy/response-dto';
import { CloudChangesActionDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChangesActionsData/data-models';

/**
 * Выполнить маппинг модели действия логирования, полученному при запросе, в модель приложения
 * @param value Модель ответа на запрос получения данных по действиям логов
 */
export function cloudChangesActionDataModelMapFrom(value: CloudChangesActionResponseDto): CloudChangesActionDataModel {
    return {
        id: value.key,
        name: value.value,
        group: value.group
    };
}