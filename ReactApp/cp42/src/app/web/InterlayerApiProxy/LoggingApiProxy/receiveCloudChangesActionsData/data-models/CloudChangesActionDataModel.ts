/**
 * Модель действия логирования
 */
export type CloudChangesActionDataModel = {
    /**
     * Id действия
     */
    id: string;
    /**
     * Значение действия/название
     */
    name: string;
    /**
     * Группа действия
     */
    group: string;
};