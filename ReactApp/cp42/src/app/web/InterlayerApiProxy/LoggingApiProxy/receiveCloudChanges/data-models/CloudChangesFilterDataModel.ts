import { Nullable } from 'app/common/types';

/**
 * Модель фильтра для получения логов действий облака
 */
export type CloudChangesFilterDataModel = {
    /**
     * ID действия облака
     */
    cloudChangesActionId: string;
    /**
     * Строка поиска
     */
    searchLine: Nullable<string>;
    /**
     * Дата создания действия с
     */
    createCloudChangeDateFrom: Nullable<Date>;
    /**
     * Дата создания действия по
     */
    createCloudChangeDateTo: Nullable<Date>;
    /**
     * Показывать только мои аккаунты
     */
    showMyAccountsOnly: boolean;
    /**
     * Показывать только вип аккаунты
     */
    showVipAccountsOnly: boolean;
};