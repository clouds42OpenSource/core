import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { CloudChangesItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/data-models/CloudChangesItemDataModel';

/**
 * Модель данных по логам действий облака
 */
export type CloudChangesDataModel = SelectDataResultMetadataModel<CloudChangesItemDataModel>;