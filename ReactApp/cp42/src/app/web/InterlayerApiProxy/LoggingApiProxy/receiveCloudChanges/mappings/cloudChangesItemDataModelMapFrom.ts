import { DateUtility } from 'app/utils';
import { CloudChangesItemResponseDto } from 'app/web/api/LoggingProxy/response-dto';
import { CloudChangesItemDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/data-models';

/**
 * Выполнить маппинг модели данных по логам действий облака, полученной при запросе, в модель приложения
 * @param value Модель ответа с элементом лога действя облака
 */
export function cloudChangesItemDataModelMapFrom(value: CloudChangesItemResponseDto): CloudChangesItemDataModel {
    return {
        accountNumber: value.accountNumber,
        cloudChangesActionName: value.cloudChangesActionName,
        cloudChangesActionDescription: value.cloudChangesActionDescription,
        initiatorLogin: value.initiatorLogin,
        createCloudChangeDate: DateUtility.convertToDate(value.createCloudChangeDate),
        cloudChangeId: value.cloudChangeId
    };
}