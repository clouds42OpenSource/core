import { adjustFilterDatePeriod } from 'app/common/functions';
import { getFilterPeriodString } from 'app/common/functions/getFilterPeriodString';
import { ReceiveCloudChangesFilterRequestDto } from 'app/web/api/LoggingProxy/request-dto';
import { CloudChangesFilterDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/data-models';

/**
 * Выполнить маппинг модели фильтра для получения логов действий облака в модель запроса на API метод
 * @param value Модель фильтра для получения логов действий облака
 */
export function receiveCloudChangesFilterRequestDtoMapFrom(value: CloudChangesFilterDataModel): ReceiveCloudChangesFilterRequestDto {
    return {
        CloudChangesActionId: value.cloudChangesActionId,
        SearchLine: value.searchLine,
        ShowMyAccountsOnly: value.showMyAccountsOnly,
        ShowVipAccountsOnly: value.showVipAccountsOnly,
        CreateCloudChangeDateFrom: getFilterPeriodString(adjustFilterDatePeriod(value.createCloudChangeDateFrom, false)),
        CreateCloudChangeDateTo: getFilterPeriodString(adjustFilterDatePeriod(value.createCloudChangeDateTo, true))
    };
}