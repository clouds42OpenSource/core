import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { CloudChangesFilterDataModel } from 'app/web/InterlayerApiProxy/LoggingApiProxy/receiveCloudChanges/data-models';

export type ReceiveCloudChangesParams = SelectDataCommonDataModel<CloudChangesFilterDataModel>;