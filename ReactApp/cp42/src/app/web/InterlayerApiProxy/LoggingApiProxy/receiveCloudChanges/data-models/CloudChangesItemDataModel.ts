/**
 * Модель данных с элементом лога действя облака
 */
export type CloudChangesItemDataModel = {
    /**
     * Номер аккаунта
     */
    accountNumber: number;
    /**
     * Дата создания лога
     */
    createCloudChangeDate: Date;
    /**
     * Id лога
     */
    cloudChangeId: string;
    /**
     * Название действия
     */
    cloudChangesActionName: string;
    /**
     * Описание действия
     */
    cloudChangesActionDescription: string;
    /**
     * Инициатор
     */
    initiatorLogin: string;
};