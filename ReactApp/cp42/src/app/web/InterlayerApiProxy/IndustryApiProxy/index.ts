import { IoCContainer } from 'app/IoCСontainer';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { AddNewIndustryDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/addIndustry/data-models/AddNewIndustryDataModel';
import { AddIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/addIndustry/input-params';
import { addIndustryItemRequestDtoMapFrom } from 'app/web/InterlayerApiProxy/IndustryApiProxy/addIndustry/mappings';
import { DeleteIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/deleteIndustry/input-params';
import { GetIndustryResultDataModel, industryItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry';
import { ReceiveIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry/input-params';
import { UpdateIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/updateIndustry/input-params';
import { updateIndustryItemRequestDtoMapFrom } from 'app/web/InterlayerApiProxy/IndustryApiProxy/updateIndustry/mappings';
import { RequestKind } from 'core/requestSender/enums';

export const IndustryApiProxy = (() => {
    function getIndustryApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getIndustryProxy();
    }

    async function receiveIndustry(requestKind: RequestKind, args: ReceiveIndustryParams): Promise<GetIndustryResultDataModel> {
        return getIndustryApiProxy().receiveIndustry(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    name: args.filter.name
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, industryItemDataModelMapFrom));
    }

    async function updateIndustry(requestKind: RequestKind, args: UpdateIndustryParams): Promise<void> {
        return getIndustryApiProxy().updateIndustry(requestKind, {
            ...updateIndustryItemRequestDtoMapFrom(args)
        });
    }

    async function addIndustry(requestKind: RequestKind, args: AddIndustryParams): Promise<AddNewIndustryDataModel> {
        return getIndustryApiProxy().addIndustry(requestKind, {
            ...addIndustryItemRequestDtoMapFrom(args)
        }).then(responseDto => {
            return {
                industryId: responseDto.industryId
            };
        });
    }

    async function deleteIndustry(requestKind: RequestKind, args: DeleteIndustryParams): Promise<void> {
        return getIndustryApiProxy().deleteIndustry(requestKind, {
            IndustryId: args.industryId
        });
    }

    return {
        receiveIndustry,
        updateIndustry,
        addIndustry,
        deleteIndustry
    };
})();