/**
 * Параметры для удаления отрасли
 */
export type DeleteIndustryParams = {
    /**
     * ID отрасли для удаления
     */
    industryId: string
};