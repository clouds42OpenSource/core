import { UpdateIndustryItemRequestDto } from 'app/web/api/IndustryProxy/request-dto';
import { UpdateIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/updateIndustry/input-params';

/**
 * Mapping модели отрасли
 * @param value ответ модели отрасли
 */
export function updateIndustryItemRequestDtoMapFrom(value: UpdateIndustryParams): UpdateIndustryItemRequestDto {
    return {
        Id: value.industryId,
        Name: value.industryName,
        Description: value.industryDescription
    };
}