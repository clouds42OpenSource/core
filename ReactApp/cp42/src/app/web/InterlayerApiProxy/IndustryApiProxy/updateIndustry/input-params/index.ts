/**
 * Параметры для обновления отрасли
 */
export type UpdateIndustryParams = {
    /**
     * ID отрасли
     */
    industryId: string;
    /**
     * Название отрасли
     */
    industryName: string;
    /**
     * Описание отрасли
     */
    industryDescription: string;
};