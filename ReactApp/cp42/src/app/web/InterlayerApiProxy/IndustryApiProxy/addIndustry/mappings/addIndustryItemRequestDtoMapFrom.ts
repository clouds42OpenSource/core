import { AddIndustryItemRequestDto } from 'app/web/api/IndustryProxy/request-dto/AddIndustryItemRequestDto';
import { AddIndustryParams } from 'app/web/InterlayerApiProxy/IndustryApiProxy/addIndustry/input-params';

/**
 * Mapping модели отрасли
 * @param value ответ модели отрасли
 */
export function addIndustryItemRequestDtoMapFrom(value: AddIndustryParams): AddIndustryItemRequestDto {
    return {
        Name: value.industryName,
        Description: value.industryDescription
    };
}