/**
 * Параметры для добавления отрасли
 */
export type AddIndustryParams = {
    /**
     * Название отрасли
     */
    industryName: string;
    /**
     * Описание отрасли
     */
    industryDescription: string;
};