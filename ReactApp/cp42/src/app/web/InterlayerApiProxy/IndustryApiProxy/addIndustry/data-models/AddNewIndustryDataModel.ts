/**
 * Модель результата на добавление новой отрасли
 */
export type AddNewIndustryDataModel = {
    /**
     * ID отрасли после добавления
     */
    industryId: string;
};