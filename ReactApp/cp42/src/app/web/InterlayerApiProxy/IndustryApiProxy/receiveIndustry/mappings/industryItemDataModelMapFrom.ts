import { IndustryItemResponseDto } from 'app/web/api/IndustryProxy/response-dto';
import { IndustryItemDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry/data-models';

/**
 * Mapping модели отрасли
 * @param value ответ модели отрасли
 */
export function industryItemDataModelMapFrom(value: IndustryItemResponseDto): IndustryItemDataModel {
    return {
        industryId: value.id,
        industryName: value.name,
        industryDescription: value.description
    };
}