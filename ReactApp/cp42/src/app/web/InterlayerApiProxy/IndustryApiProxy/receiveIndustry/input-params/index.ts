import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Название отрасли
     */
    name: string;
};

/**
 * Параметры для загрузки отраслей
 */
export type ReceiveIndustryParams = SelectDataCommonDataModel<Filter>;