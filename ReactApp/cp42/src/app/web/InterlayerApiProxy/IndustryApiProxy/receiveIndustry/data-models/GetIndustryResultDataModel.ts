import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { IndustryItemDataModel } from 'app/web/InterlayerApiProxy/IndustryApiProxy/receiveIndustry/data-models/IndustryItemDataModel';

/**
 * Модель ответа на получение списка отраслей
 */
export type GetIndustryResultDataModel = SelectDataResultMetadataModel<IndustryItemDataModel>;