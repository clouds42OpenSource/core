/**
 *  Модель свойств элемента отрасли
 */
export type IndustryItemDataModel = {
    /**
     * ID отрасли
     */
    industryId: string;

    /**
     * Название отрасли
     */
    industryName: string;

    /**
     * Описание отрасли
     */
    industryDescription: string;
};