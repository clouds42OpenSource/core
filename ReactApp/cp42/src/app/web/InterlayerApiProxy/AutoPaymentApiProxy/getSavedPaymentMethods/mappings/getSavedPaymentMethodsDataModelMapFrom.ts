import { BankCardDetails, GetSavedPaymentMethods, GetSavedPaymentMethodsDataModel } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods/data-models';
import { SavedMethodsResponseDto } from 'app/web/api/AutoPaymentProxy/response-dto';

/**
 * Выполнить маппинг модели данных транзакций, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по транзакций
 */
export function getSavedPaymentMethodsDataModelMapFrom(value: SavedMethodsResponseDto): GetSavedPaymentMethodsDataModel {
    const mappedData: GetSavedPaymentMethods[] = value.map(method => {
        const bankCardDetails: BankCardDetails | null = method.bankCardDetails ? {
            FirstSixDigits: parseInt(method.bankCardDetails.firstSixDigits, 10),
            LastFourDigits: parseInt(method.bankCardDetails.lastFourDigits, 10),
            ExpiryMonth: parseInt(method.bankCardDetails.expiryMonth, 10),
            ExpiryYear: parseInt(method.bankCardDetails.expiryYear, 10),
            CardType: method.bankCardDetails.cardType,
        } : null;

        return {
            Id: method.id,
            AccountId: method.accountId,
            CreatedOn: method.createdOn,
            Title: method.title,
            Type: method.type,
            BankCardDetails: bankCardDetails,
            Token: method.token,
            Metadata: method.metadata,
            DefaultPaymentMethod: method.defaultPaymentMethod,
            paymentName: method.paymentName ?? ''
        };
    });

    return {
        data: mappedData
    };
}