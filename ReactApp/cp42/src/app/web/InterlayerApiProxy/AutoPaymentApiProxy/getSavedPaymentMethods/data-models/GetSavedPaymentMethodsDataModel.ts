export type BankCardDetails = {
    FirstSixDigits: number;
    LastFourDigits: number;
    ExpiryMonth: number;
    ExpiryYear: number;
    CardType: string;
};

export type GetSavedPaymentMethods = {
    Id: string;
    AccountId: string;
    CreatedOn: string;
    Title: string;
    Type: string;
    BankCardDetails: BankCardDetails | null;
    Token: string;
    Metadata: string;
    DefaultPaymentMethod: boolean;
    paymentName: string;
};

/**
 * Модель данных транзакций
 */
export type GetSavedPaymentMethodsDataModel = {
    data: GetSavedPaymentMethods[]
};