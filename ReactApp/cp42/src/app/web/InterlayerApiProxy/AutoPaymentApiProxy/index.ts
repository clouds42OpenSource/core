import { GetSavedPaymentMethodsDataModel } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods';
import { DeletePaymentMethodParams } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/deletePaymentMethod';
import { IoCContainer } from 'app/IoCСontainer';
import { RequestKind } from 'core/requestSender/enums';
import { getSavedPaymentMethodsDataModelMapFrom } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy/getSavedPaymentMethods/mappings';

/**
 * Прокси для работы с данными Биллинга
 */
export const AutoPaymentApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getInvoicesProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getAutoPaymentApiProxy();
    }

    /**
     * Получить данные по счетам
     * @param requestKind тип запроса
     */
    async function getSavedPaymentMethods(requestKind: RequestKind): Promise<GetSavedPaymentMethodsDataModel> {
        return getInvoicesProxy().getSavedPaymentMethods(requestKind)
            .then(responseDto => {
                return getSavedPaymentMethodsDataModelMapFrom(responseDto);
            });
    }

    /**
     * Получить данные по счетам
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function deletePaymentMethods(requestKind: RequestKind, args: DeletePaymentMethodParams): Promise<NonNullable<unknown>> {
        return getInvoicesProxy().deletePaymentMethod(requestKind, args)
            .then(responseDto => {
                return responseDto;
            });
    }

    return {
        getSavedPaymentMethods,
        deletePaymentMethods,
    };
})();