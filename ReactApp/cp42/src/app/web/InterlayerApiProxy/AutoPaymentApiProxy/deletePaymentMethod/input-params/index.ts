/**
 * Параметры для загрузки транзакций
 */
export type DeletePaymentMethodParams = {
    id: string;
};