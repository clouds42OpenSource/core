/**
 * Модель купленного ресурса
 */
export type CloudServiceResourcesDataModel = {
    id: string;
    name: string;
    dependServiceId: string;
    serviceExpireDate: string;
    serviceDependsOnRent: boolean;
    isActivatedPromisPayment: boolean;
    amount: number;
    usedLicenses: number;
    currency: string;
};