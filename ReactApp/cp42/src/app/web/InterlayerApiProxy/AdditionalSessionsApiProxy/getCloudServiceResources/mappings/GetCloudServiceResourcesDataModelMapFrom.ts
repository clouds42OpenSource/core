import { TErrorType } from 'app/views/modules/Services/types/ErrorTypes';
import { CloudServiceResourcesResponseDto } from 'app/web/api/AdditionalSessionsProxy/response-dto';
import { CloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getCloudServiceResources';

/**
 * Выполнить маппинг модели сервиса, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении купленных ресурсов
 */
export function GetCloudServiceResourcesDataModelMapFrom(value: CloudServiceResourcesResponseDto): CloudServiceResourcesDataModel | string {
    if (Object.hasOwn(value, 'Description')) {
        return (value as unknown as TErrorType).Description;
    }

    return {
        id: value.Id,
        name: value.Name,
        dependServiceId: value.DependServiceId,
        serviceExpireDate: value.ServiceExpireDate,
        serviceDependsOnRent: value.ServiceDependsOnRent,
        isActivatedPromisPayment: value.IsActivatedPromisPayment,
        amount: value.Amount,
        usedLicenses: value.UsedLicenses,
        currency: value.Currency
    };
}