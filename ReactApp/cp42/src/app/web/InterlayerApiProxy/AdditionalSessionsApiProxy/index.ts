import { IoCContainer } from 'app/IoCСontainer';
import { RequestKind } from 'core/requestSender/enums';
import { GetSessionListParams, SessionListDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList';
import { CloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getCloudServiceResources';
import { ChangeCloudServiceResourcesDataModel, ChangeCloudServiceResourcesParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/changeCloudServiceResources';
import { EndingUserSessionParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/endingUserSession';
import { GetSessionListDataModelMapFrom } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList/mappings/GetSessionListDataModelMapFrom';
import { GetCloudServiceResourcesDataModelMapFrom } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getCloudServiceResources/mappings/GetCloudServiceResourcesDataModelMapFrom';
import { ChangeCloudServiceResourcesDataModelMapFrom } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/changeCloudServiceResources/mappings/ChangeCloudServiceResourcesDataModelMapFrom';
import { BuyCloudServiceResourcesParams } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/buyCloudServiceResources';

export const AdditionalSessionsApiProxy = (() => {
    function getSessionListApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getSessionListProxy();
    }

    function getAdditionalSessionsApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getAdditionalSessionsProxy();
    }

    function getSessionList(requestKind: RequestKind, args: GetSessionListParams): Promise<SessionListDataModel> {
        return getSessionListApiProxy().getSessionList(requestKind, args).then(responseDto => {
            return GetSessionListDataModelMapFrom(responseDto);
        });
    }

    function endingUserSession(requestKind: RequestKind, args: EndingUserSessionParams): Promise<void> {
        return getSessionListApiProxy().endingUserSession(requestKind, {
            SessionID: args.sessionId,
            dummy: args.dummy,
            message: args.message,
            notify: args.notify,
            'account-id': args.accountId,
        });
    }

    function getCloudServiceResources(requestKind: RequestKind): Promise<CloudServiceResourcesDataModel | string> {
        return getAdditionalSessionsApiProxy().getCloudServiceResources(requestKind).then(responseDto => {
            return GetCloudServiceResourcesDataModelMapFrom(responseDto);
        });
    }

    function changeCloudServiceResources(requestKind: RequestKind, args: ChangeCloudServiceResourcesParams): Promise<ChangeCloudServiceResourcesDataModel> {
        return getAdditionalSessionsApiProxy().changeCloudServiceResources(requestKind, {
            count: args.count,
            accountUserSessions: args.accountUserSessions
        }).then(responseDto => {
            return ChangeCloudServiceResourcesDataModelMapFrom(responseDto);
        });
    }

    function buyCloudServiceResources(requestKind: RequestKind, args: BuyCloudServiceResourcesParams): Promise<void> {
        return getAdditionalSessionsApiProxy().buyCloudServiceResources(requestKind, {
            isPromisePayment: args.isPromisePayment,
            costOfTariff: args.costOfTariff,
            count: args.count,
            accountUserSessions: args.accountUserSessions
        });
    }

    return {
        getSessionList,
        endingUserSession,
        getCloudServiceResources,
        changeCloudServiceResources,
        buyCloudServiceResources
    };
})();