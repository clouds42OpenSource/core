import { SessionListResponseDto } from 'app/web/api/AdditionalSessionsProxy/response-dto';
import { SessionListDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/getSessionList';

/**
 * Выполнить маппинг модели сервиса, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении списка сеансов
 */
export function GetSessionListDataModelMapFrom(value: SessionListResponseDto): SessionListDataModel {
    return {
        sessions: value.sessions.map(item => {
            return {
                group: {
                    id: item.group.id,
                    name: item.group.name
                },
                data: item.data.map(item => {
                    return {
                        id: item.id,
                        number: item.number,
                        start: item.start,
                        sleeping: item.sleeping,
                        locked: item.locked,
                        client_type: item.client_type,
                        timestamp: item.timestamp,
                        user: {
                            name: item.user.name
                        },
                        application: {
                            name: item.application.name,
                            id: item.application.id,
                            code: item.application.code
                        }
                    };
                }),
            };
        }),
        additionalSessionsUsed: value['additional-sessions-used']
    };
}