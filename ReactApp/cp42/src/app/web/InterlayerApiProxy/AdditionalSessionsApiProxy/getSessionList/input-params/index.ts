/**
 * Параметры для получения списка сеансов
 */
export type GetSessionListParams = {
    /**
     * true - вывод полного списка сеансов
     * false - вывести тонкий и веб-клиент
     */
    'full-list': boolean;
    'group-by'?: string;
    'account-id'?: string;
};