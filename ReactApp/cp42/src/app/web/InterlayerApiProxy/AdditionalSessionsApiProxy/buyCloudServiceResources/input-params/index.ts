import { TUserSessionsItem } from 'app/api/endpoints/additionalSessions/response';

/**
 * Параметры на покупку сервиза за счет собственных средств или за счет обещанного платежа
 */
export type BuyCloudServiceResourcesParams = {
    isPromisePayment: boolean;
    costOfTariff: number;
    count: number;
    accountUserSessions: TUserSessionsItem[];
};