import { Nullable } from 'app/common/types';

/**
 * Модель ответа на изменение или покупку дополнительных сеансов
 */
export type ChangeCloudServiceResourcesDataModel = {
    complete: boolean;
    error: Nullable<string>;
    enoughMoney: number;
    canGetPromisePayment: boolean;
    costOftariff: number;
    monthlyCost: number;
    currency: string;
    balance: number;
};