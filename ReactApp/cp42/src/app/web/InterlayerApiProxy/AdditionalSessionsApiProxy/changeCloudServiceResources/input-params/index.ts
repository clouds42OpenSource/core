import { TUserSessionsItem } from 'app/api/endpoints/additionalSessions/response';

/**
 * Параметр на изменение или покупку дополнительных сеансов
 */
export type ChangeCloudServiceResourcesParams = {
    count: number;
    accountUserSessions: TUserSessionsItem[];
};