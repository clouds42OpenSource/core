import { ChangeCloudServiceResourcesResponseDto } from 'app/web/api/AdditionalSessionsProxy/response-dto';
import { ChangeCloudServiceResourcesDataModel } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy/changeCloudServiceResources';

/**
 * Выполнить маппинг модели сервиса, полученной при запросе, в модель приложения
 * @param value Модель ответа при изменении или покупки дополнительных сеансов
 */
export function ChangeCloudServiceResourcesDataModelMapFrom(value: ChangeCloudServiceResourcesResponseDto): ChangeCloudServiceResourcesDataModel {
    return {
        complete: value.Complete,
        error: value.Error,
        enoughMoney: value.EnoughMoney,
        canGetPromisePayment: value.CanGetPromisePayment,
        costOftariff: value.CostOftariff,
        monthlyCost: value.MonthlyCost,
        currency: value.Currency,
        balance: value.Balance
    };
}