/**
 * Параметры для завершения сеанса пользователя
 */
export type EndingUserSessionParams = {
    sessionId?: string;
    dummy?: boolean;
    message?: string;
    notify?: boolean;
    accountId?: string;
};