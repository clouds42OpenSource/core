import { IoCContainer } from 'app/IoCСontainer';
import { ReceiveWorkerTaskBagPriorityThunkParams } from 'app/modules/workerAvailableTasksBags/store/reducers/receiveWorkerTaskBagPriority/params';
import { ChangeWorkerTaskBagPriorityDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/changeWorkerTaskBagPriority/data-models';
import { WorkerAvailableTasksBagsDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/data-models';
import { ReceiveWorkerAvailableTasksBagsParams } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/input-params';
import { workerAvailableTasksBagItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/mappings';
import { receiveWorkerAvailableTasksBagsFilterRequestDtoMapFrom } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/mappings/receiveWorkerAvailableTasksBagsFilterRequestDtoMapFrom';
import { WorkerTaskBagPriorityDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerTaskBagPriority/data-models';
import { workerTaskBagPriorityDataModelMapFrom } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerTaskBagPriority/mappings/workerTaskBagPriorityDataModelMapFrom';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Прокси для работы с данными по взаимодействию воркера и задач
 */
export const WorkerAvailableTasksBagsApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getWorkerAvailableTasksBagsProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getWorkerAvailableTasksBagsProxy();
    }

    /**
     * Получить данные по взаимодействию воркера и задач
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveWorkerAvailableTasksBags(requestKind: RequestKind, args: ReceiveWorkerAvailableTasksBagsParams): Promise<WorkerAvailableTasksBagsDataModel> {
        return getWorkerAvailableTasksBagsProxy().receiveWorkerAvailableTasksBags(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter ? receiveWorkerAvailableTasksBagsFilterRequestDtoMapFrom(args.filter) : null,
            orderBy: args.orderBy
        }).then(responseDto => {
            return {
                records: responseDto.records.map(record => (workerAvailableTasksBagItemDataModelMapFrom(record))),
                metadata: responseDto.metadata
            };
        });
    }

    /**
     * Получить данные по приоритету доступной задачи воркера
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveWorkerTaskBagPriority(requestKind: RequestKind, args: ReceiveWorkerTaskBagPriorityThunkParams): Promise<WorkerTaskBagPriorityDataModel> {
        return getWorkerAvailableTasksBagsProxy().receiveWorkerTaskBagPriority(requestKind, {
            taskId: args.taskId,
            workerId: args.workerId
        }).then(responseDto => {
            return workerTaskBagPriorityDataModelMapFrom(responseDto);
        });
    }

    /**
     * Сменить приоритет доступной задачи воркера
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    function changeWorkerTaskBagPriority(requestKind: RequestKind, args: ChangeWorkerTaskBagPriorityDataModel): Promise<void> {
        return getWorkerAvailableTasksBagsProxy().changeWorkerTaskBagPriority(requestKind, {
            WorkerId: args.coreWorkerId,
            TaskId: args.taskId,
            TaskPriority: args.priority
        });
    }

    return {
        receiveWorkerAvailableTasksBags,
        receiveWorkerTaskBagPriority,
        changeWorkerTaskBagPriority
    };
})();