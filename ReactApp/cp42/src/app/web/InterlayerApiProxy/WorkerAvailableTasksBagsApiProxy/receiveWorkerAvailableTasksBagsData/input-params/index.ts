import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { WorkerAvailableTasksBagsFilterDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData';

/**
 * Параметры для получения карточки управления задачей
 */
export type ReceiveWorkerAvailableTasksBagsParams = SelectDataCommonDataModel<WorkerAvailableTasksBagsFilterDataModel>;