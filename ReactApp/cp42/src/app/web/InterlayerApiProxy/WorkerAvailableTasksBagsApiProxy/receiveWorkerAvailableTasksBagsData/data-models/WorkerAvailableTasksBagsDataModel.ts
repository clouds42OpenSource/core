import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { WorkerAvailableTasksBagItemDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/data-models/WorkerAvailableTasksBagItemDataModel';

/**
 * Модель данных по взаимодействию воркера и задач
 */
export type WorkerAvailableTasksBagsDataModel = SelectDataResultMetadataModel<WorkerAvailableTasksBagItemDataModel>;