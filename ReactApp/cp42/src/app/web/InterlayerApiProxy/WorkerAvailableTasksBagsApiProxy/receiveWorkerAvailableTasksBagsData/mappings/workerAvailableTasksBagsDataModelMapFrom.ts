import { WorkerAvailableTasksBagItemResponseDto } from 'app/web/api/WorkerAvailableTasksBagsProxy/response-dto';
import { WorkerAvailableTasksBagItemDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData/data-models';

/**
 * Выполнить маппинг модели данных по взаимодействию воркера и задач, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных по взаимодействию воркера и задач
 */
export function workerAvailableTasksBagItemDataModelMapFrom(value: WorkerAvailableTasksBagItemResponseDto): WorkerAvailableTasksBagItemDataModel {
    return {
        ...value,
        workerName: `[${ value.workerId }] ${ value.workerName ?? 'core-worker' }`,
        id: `[${ value.workerId }][${ value.taskId }]`
    };
}