import { Nullable } from 'app/common/types';

/**
 * Модель фильтра поиска данных по взаимодействию воркера и задач
 */
export type WorkerAvailableTasksBagsFilterDataModel = {

    /**
     * ID воркера
     */
    workerId: Nullable<number>;

    /**
     * ID задачи
     */
    taskId: Nullable<string>;

    /**
     * Приоритет задачи
     */
    taskPriority: Nullable<number>;
};