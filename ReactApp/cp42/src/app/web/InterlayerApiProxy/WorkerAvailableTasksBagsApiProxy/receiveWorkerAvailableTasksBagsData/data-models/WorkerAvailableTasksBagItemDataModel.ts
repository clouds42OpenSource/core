/**
 * Модель взаимодействия воркера и задачи
 */
export type WorkerAvailableTasksBagItemDataModel = {
    /**
     * ID задачи
     */
    taskId: string;
    /**
     * Название задачи
     */
    taskName: string;
    /**
     * Приоритет задачи
     */
    taskPriority: number;
    /**
     * ID воркера
     */
    workerId: number;
    /**
     * Название воркера
     */
    workerName: string;
    /**
     * Id записи
     */
    id: string;
};