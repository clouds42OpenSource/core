import { ReceiveWorkerAvailableTasksBagsFilterRequestDto } from 'app/web/api/WorkerAvailableTasksBagsProxy/request-dto/ReceiveWorkerAvailableTasksBagsFilterRequestDto';
import { WorkerAvailableTasksBagsFilterDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerAvailableTasksBagsData';

/**
 * Mapping модели фильтра о получении связи задач и воркера в модель запроса на API метод
 * @param value Модель фильтра связи задач и воркера
 */
export function receiveWorkerAvailableTasksBagsFilterRequestDtoMapFrom(value: WorkerAvailableTasksBagsFilterDataModel): ReceiveWorkerAvailableTasksBagsFilterRequestDto {
    return {
        TaskId: value.taskId,
        TaskPriority: value.taskPriority,
        WorkerId: value.workerId,
    };
}