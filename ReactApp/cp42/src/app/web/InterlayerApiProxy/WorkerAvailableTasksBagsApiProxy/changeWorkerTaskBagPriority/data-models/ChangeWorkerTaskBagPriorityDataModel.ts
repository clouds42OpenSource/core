/**
 * Модель смены приоритета доступной задачи воркера
 */
export type ChangeWorkerTaskBagPriorityDataModel = {
    /**
     * ID воркера
     */
    coreWorkerId: number;
    /**
     * ID задачи
     */
    taskId: string;
    /**
     * Приоритет
     */
    priority: number;
};