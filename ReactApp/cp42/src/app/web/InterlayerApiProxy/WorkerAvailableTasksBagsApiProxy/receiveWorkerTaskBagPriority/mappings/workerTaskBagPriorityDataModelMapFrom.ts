import { WorkerTaskBagPriorityResponseDto } from 'app/web/api/WorkerAvailableTasksBagsProxy/response-dto';
import { WorkerTaskBagPriorityDataModel } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy/receiveWorkerTaskBagPriority/data-models';

/**
 * Выполнить маппинг модели данных приоритета доступной задачи воркера, полученной при запросе, в модель приложения
 * @param value Модель ответа при получении данных приоритета доступной задачи воркера
 */
export function workerTaskBagPriorityDataModelMapFrom(value: WorkerTaskBagPriorityResponseDto): WorkerTaskBagPriorityDataModel {
    return {
        taskId: value.taskId,
        taskName: value.taskName,
        priority: value.taskPriority,
        coreWorkerId: value.workerId,
        workerName: `[${ value.workerId }] ${ value.workerName ?? 'core-worker' }`
    };
}