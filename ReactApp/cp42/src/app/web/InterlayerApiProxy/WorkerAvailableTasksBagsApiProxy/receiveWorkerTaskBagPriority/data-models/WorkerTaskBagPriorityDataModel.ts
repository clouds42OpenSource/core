/**
 * Модель приоритета доступной задачи воркера
 */
export type WorkerTaskBagPriorityDataModel = {
    /**
     * ID воркера
     */
    coreWorkerId: number;
    /**
     * ID задачи
     */
    taskId: string;
    /**
     * Название задачи
     */
    taskName: string;
    /**
     * Название воркера
     */
    workerName: string;
    /**
     * Приоритет
     */
    priority: number;
};