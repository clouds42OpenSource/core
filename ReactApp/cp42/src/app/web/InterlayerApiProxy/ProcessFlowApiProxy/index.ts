import { StateMachineComponentStatus } from 'app/common/enums';
import { IoCContainer } from 'app/IoCСontainer';
import { ProcessFlowDetailsDataModel, ReceiveProcessFlowDetailsParams } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails';
import { processFlowDetailsDataModelMapFrom } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails/mappings';
import { processFlowItemDataModelMapFrom, ProcessFlowListDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList';
import { ReceiveProcessFlowParams } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList/input-params';
import { RetryProcessFlowParams } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/retryProcessFlow/input-params';
import { RequestKind } from 'core/requestSender/enums';

export const ProcessFlowApiProxy = (() => {
    function getProcessFlowApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getProcessFlowProxy();
    }

    async function receiveProcessFlowList(requestKind: RequestKind, args: ReceiveProcessFlowParams): Promise<ProcessFlowListDataModel> {
        return getProcessFlowApiProxy().receiveProcessFlowList(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    Status: args.filter.status === StateMachineComponentStatus.Any ? null : args.filter.status,
                    SearchLine: args.filter.searchString
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => ({
            records: responseDto.records.map(record => processFlowItemDataModelMapFrom(record)),
            metadata: responseDto.metadata
        }));
    }

    async function retryProcessFlow(requestKind: RequestKind, args: RetryProcessFlowParams): Promise<void> {
        return getProcessFlowApiProxy().retryProcessFlow(requestKind, {
            processFlowId: args.processFlowId
        });
    }

    /**
     * Получить детали рабочего процесса
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveProcessFlowDetails(requestKind: RequestKind, args: ReceiveProcessFlowDetailsParams): Promise<ProcessFlowDetailsDataModel> {
        return getProcessFlowApiProxy().receiveProcessFlowDetails(requestKind, {
            processFlowId: args.processFlowId
        }).then(responseDto => processFlowDetailsDataModelMapFrom(responseDto));
    }

    return {
        receiveProcessFlowList,
        retryProcessFlow,
        receiveProcessFlowDetails
    };
})();