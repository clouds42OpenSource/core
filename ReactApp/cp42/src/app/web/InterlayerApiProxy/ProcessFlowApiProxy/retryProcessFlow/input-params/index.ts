/**
 * Параметры для перезапуска рабочего процеса
 */
export type RetryProcessFlowParams = {
    /**
     * ID рабочего процеса
     */
    processFlowId: string;
};