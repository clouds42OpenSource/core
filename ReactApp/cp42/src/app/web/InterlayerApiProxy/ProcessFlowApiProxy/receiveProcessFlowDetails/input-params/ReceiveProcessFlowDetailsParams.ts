/**
 * Параметры для получения деталей рабочего процесса
 */
export type ReceiveProcessFlowDetailsParams = {
    /**
     * ID рабочего процесса
     */
    processFlowId: string;
};