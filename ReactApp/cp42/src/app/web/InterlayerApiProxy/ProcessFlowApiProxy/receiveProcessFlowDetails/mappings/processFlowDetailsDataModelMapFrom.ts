import { DateUtility } from 'app/utils';
import { ProcessFlowDetailsResponseDto } from 'app/web/api/ProcessFlowProxy/response-dto';
import { ProcessFlowDetailsDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails/data-models';

/**
 * Mapping модели информацию о рабочем процессе
 * @param value Модель информации о рабочем процессе
 */
export function processFlowDetailsDataModelMapFrom(value: ProcessFlowDetailsResponseDto): ProcessFlowDetailsDataModel {
    return {
        ...value,
        processFlowId: value.id,
        creationDateTime: DateUtility.convertToDate(value.creationDateTime),
        processFlowName: value.name,
    };
}