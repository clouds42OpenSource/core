import { ProcessFlowItemDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList/data-models';
import { ActionFlowDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails/data-models/ActionFlowDataModel';

/**
 * Модель деталей рабочего процесса
 */
export type ProcessFlowDetailsDataModel = ProcessFlowItemDataModel & {
    /**
     * Шаги рабочего процесса
     */
    actionFlows: Array<ActionFlowDataModel>;
};