import { StateMachineComponentStatus } from 'app/common/enums';

/**
 * Модель шага рабочего процесса
 */
export type ActionFlowDataModel = {
    /**
     * Сообщение об ошибке
     */
    errorMessage: string;
    /**
     * Количество попыток выполнения
     */
    countOfAttemps: number;
    /**
     * Название шага
     */
    actionName: string;
    /**
     * Количество попыток выполнения
     */
    actionDescription: string;
    /**
     * Описание шага
     */
    status: StateMachineComponentStatus;
};