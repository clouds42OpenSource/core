import { DateUtility } from 'app/utils';
import { ProcessFlowItemResponseDto } from 'app/web/api/ProcessFlowProxy/response-dto';
import { ProcessFlowItemDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList/data-models';

/**
 * Mapping модели информацию о рабочем процессе
 * @param value Модель информации о рабочем процессе
 */
export function processFlowItemDataModelMapFrom(value: ProcessFlowItemResponseDto): ProcessFlowItemDataModel {
    return {
        ...value,
        processFlowId: value.id,
        creationDateTime: DateUtility.convertToDate(value.creationDateTime),
        processFlowName: value.name,
    };
}