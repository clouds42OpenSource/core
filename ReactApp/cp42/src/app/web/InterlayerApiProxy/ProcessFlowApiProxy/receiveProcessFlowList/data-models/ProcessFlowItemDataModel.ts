import { StateMachineComponentStatus } from 'app/common/enums';

/**
 * Модель описывающая свойства рабочего процесса конечного автомата.
 */
export type ProcessFlowItemDataModel = {
    /**
     * Идентификатор процесса.
     */
    processFlowId: string;

    /**
     * Дата создания процесса.
     */
    creationDateTime: Date;

    /**
     * Имя процесса.
     */
    processFlowName: string;

    /**
     * Имя обрабатываемого процессом объекта.
     */
    processedObjectName: string;

    /**
     * Статус процесса.
     */
    status: StateMachineComponentStatus;

    /**
     * Описание состояния процесса.
     */
    stateDescription: string;

    /**
     * Комментарий среды исполнения.
     */
    comment: string;
};