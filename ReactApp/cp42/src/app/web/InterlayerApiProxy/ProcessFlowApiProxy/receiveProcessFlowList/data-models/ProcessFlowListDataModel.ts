import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { ProcessFlowItemDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList/data-models/ProcessFlowItemDataModel';

/**
 * Модель ответа на запрос получения рабочих процессов
 */
export type ProcessFlowListDataModel = SelectDataResultMetadataModel<ProcessFlowItemDataModel>;