import { StateMachineComponentStatus } from 'app/common/enums';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Строка фильтра
     */
    searchString: string;
    /**
     * Статус рабочего процеса
     */
    status: StateMachineComponentStatus;
};

/**
 * Параметры для загрузки рабочих процесов
 */
export type ReceiveProcessFlowParams = SelectDataCommonDataModel<Filter>;