import { DatabaseCardReadModeFieldAccessResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto';
import { DatabaseCardReadModeFieldAccessDataModel } from '../data-models';

/**
 * Mapping модели настройки по показу полей в карточке информационной базы
 * @param value Модель по настройке по показу полей в карточке информационной базы
 */
export function databaseCardReadModeFieldAccessDataModelMapFrom(value: DatabaseCardReadModeFieldAccessResponseDto): DatabaseCardReadModeFieldAccessDataModel {
    return {
        canShowWebPublishPath: value.canShowWebPublishPath,
        canShowUsedWebServices: value.canShowUsedWebServices,
        canShowDatabaseDetails: value.canShowDatabaseDetails,
        canShowSqlServer: value.canShowSqlServer,
        canShowDatabaseState: value.canShowDatabaseState,
        canShowDatabaseFileStorage: value.canShowDatabaseFileStorage,
    };
}