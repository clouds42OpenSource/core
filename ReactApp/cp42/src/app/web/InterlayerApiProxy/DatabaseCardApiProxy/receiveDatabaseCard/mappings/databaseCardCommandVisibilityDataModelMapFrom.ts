import { DatabaseCardCommandVisibilityResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto';
import { DatabaseCardCommandVisibilityDataModel } from '../data-models';

/**
 * Mapping модели по установки видимости кнопок по работе с базой
 * @param value Модель установки кнопок по работе с базой
 */
export function databaseCardCommandVisibilityDataModelMapFrom(value: DatabaseCardCommandVisibilityResponseDto): DatabaseCardCommandVisibilityDataModel {
    return {
        isCanacelingDatabasePublishingInfoVisible: value.isCanacelingDatabasePublishingInfoVisible,
        isCancelPublishDatabaseCommandVisible: value.isCancelPublishDatabaseCommandVisible,
        isDatabasePublishingInfoVisible: value.isDatabasePublishingInfoVisible,
        isDeleteDatabaseCommandVisible: value.isDeleteDatabaseCommandVisible,
        isPublishDatabaseCommandVisible: value.isPublishDatabaseCommandVisible

    };
}