import { DateUtility } from 'app/utils';
import { DatabaseCardTehSupportInfoResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto';
import { DatabaseCardTehSupportInfoDataModel } from '../data-models';

/**
 * Mapping модели с информацией о техподержке базы
 * @param value Модель с информацией о техподдержке базы
 */
export function databaseCardTehSupportInfoDataModelMapFrom(value: DatabaseCardTehSupportInfoResponseDto): DatabaseCardTehSupportInfoDataModel {
    return {
        isInConnectingState: value.isConnects,
        supportState: value.supportState,
        supportStateDescription: value.supportStateDescription,
        lastHistoryDate: value.lastHistoryDate ? DateUtility.convertToDate(value.lastHistoryDate) : undefined
    };
}