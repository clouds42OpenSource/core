import { DatabaseState, DistributionType, PlatformType } from 'app/common/enums';
import { KeyValueDataModel } from 'app/web/common/data-models';

/**
 * Модель видимости полей и справочники для карточки информационной базы в режиме редактирования
 */
export type DatabaseCardEditModeDictionariesDataModel = {
    /**
     * Массив платформ
     */
    availablePlatformTypes: Array<PlatformType>;
    /**
     * Массив типов распространения базы
     */
    availableDestributionTypes: Array<KeyValueDataModel<DistributionType, string>>;
    /**
     * Массив состояний базы
     */
    availableDatabaseStates: Array<DatabaseState>;

    /**
     * Массив файловых хранилищь
     */
    availableFileStorages: Array<KeyValueDataModel<string, string>>;

    /**
     * Массив шаблонов баз
     */
    availableDatabaseTemplates: Array<KeyValueDataModel<string, string>>;
};