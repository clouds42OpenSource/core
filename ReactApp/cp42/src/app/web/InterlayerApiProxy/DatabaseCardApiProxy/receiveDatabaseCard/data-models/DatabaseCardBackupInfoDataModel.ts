/**
 * Бекап информационной базы
 */
export type DatabaseCardBackupInfoDataModel = {
    /**
     * Id бекапа
     */
    id: string;

    /**
     * Инициатор
     */
    initiator: string;

    /**
     * Дата создания бекапа
     */
    createDateTime: Date;

    /**
     * Путь к бекапу
     */
    backupPath: string;

    /**
     * Тригер события
     */
    eventTrigger: string;

    /**
     * База находится на разделителях
     */
    isDbOnDelimiters: boolean;
}