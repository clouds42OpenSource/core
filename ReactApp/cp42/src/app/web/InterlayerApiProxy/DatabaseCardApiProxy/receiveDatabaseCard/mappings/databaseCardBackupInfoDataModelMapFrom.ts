import { DateUtility } from 'app/utils';
import { DatabaseCardBackupInfoResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto';
import { DatabaseCardBackupInfoDataModel } from '../data-models';

/**
 * Mapping модели с информацией о бекапах
 * @param value Модель с информацией о бекапах с сервера
 */
export function databaseCardBackupInfoDataModelMapFrom(value: DatabaseCardBackupInfoResponseDto): DatabaseCardBackupInfoDataModel {
    return {
        id: value.id,
        initiator: value.initiator,
        backupPath: value.backupPath,
        eventTrigger: value.eventTriggerDescription,
        createDateTime: DateUtility.convertToDate(value.creationDate),
        isDbOnDelimiters: value.isDbOnDelimiters
    };
}