import {
    DatabaseCardBackupInfoDataModel,
    DatabaseCardCommandVisibilityDataModel,
    DatabaseCardReadModeFieldAccessDataModel,
    DatabaseCardTabVisibilityDataModel,
    DatabaseCardTehSupportInfoDataModel
} from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models';
import { DatabaseCardAccountDatabaseInfoDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models/DatabaseCardAccountDatabaseInfoDataModel';
import { DatabaseCardEditModeDictionariesDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models/DatabaseCardEditModeDictionariesDataModel';

/**
 * Модель ответа для карточки информационной базы
 */
export type DatabaseCardDataModel = {
    /**
     * Информация о базе
     */
    database: DatabaseCardAccountDatabaseInfoDataModel;

    /**
     * Url для запуска опубликованной базы
     */
    launchPublishedDbUrl: string;

    /**
     * Информация о видемости полей в режиме просмотра
     */
    fieldsAccessReadModeInfo: DatabaseCardReadModeFieldAccessDataModel;

    /**
     * Информация о бекапах информационной базы
     */
    backups: DatabaseCardBackupInfoDataModel[],

    /**
     * Информация о техподержке информационной базы
     */
    tehSupportInfo: DatabaseCardTehSupportInfoDataModel | null;

    /**
     * Видимость табов
     */
    tabVisibility: DatabaseCardTabVisibilityDataModel;

    /**
     * Команды с базой
     */
    commandVisibility: DatabaseCardCommandVisibilityDataModel;

    /**
     * Если true, то можно редактировать данные в карточке базы
     */
    canEditDatabase: boolean;

    /**
     * Видимости полей и справочники для карточки информационной базы в режиме редактирования
     */
    accountCardEditModeDictionaries: DatabaseCardEditModeDictionariesDataModel;
};