import { DatabaseCardEditModeDictionariesResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto/DatabaseCardEditModeDictionariesResponseDto';
import { keyValueDataModelMapFrom } from 'app/web/common/mappings/keyValueDataModelMapFrom';
import { DatabaseCardEditModeDictionariesDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models/DatabaseCardEditModeDictionariesDataModel';

/**
 * Mapping модели DatabaseCardEditModeDictionariesDataModel
 * @param value response объекта DatabaseCardEditModeDictionariesResponseDto
 */
export function databaseCardEditModeDictionariesDataModelMapFrom(value: DatabaseCardEditModeDictionariesResponseDto): DatabaseCardEditModeDictionariesDataModel {
    return {
        availablePlatformTypes: value.availablePlatformTypes,
        availableDatabaseStates: value.availableDatabaseStates,
        availableDatabaseTemplates: value.availableDatabaseTemplates.map(item => keyValueDataModelMapFrom(item)),
        availableFileStorages: value.availableFileStorages.map(item => keyValueDataModelMapFrom(item)),
        availableDestributionTypes: value.availableDestributionTypes.map(item => keyValueDataModelMapFrom(item))
    };
}