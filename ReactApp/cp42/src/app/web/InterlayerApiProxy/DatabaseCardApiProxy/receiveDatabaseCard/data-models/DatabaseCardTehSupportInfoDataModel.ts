import { SupportState } from 'app/common/enums/SupportState';

/**
 * Информация о подключении информационной базы к АО
 */
export type DatabaseCardTehSupportInfoDataModel = {
    /**
     * Если true, значит идёт аутентификация к базе или уже пройдена
     */
    isInConnectingState: boolean;

    /**
     * Последняя дата тех-поддержки
     */
    lastHistoryDate?: Date;

    /**
     * Состояние базы на тех-подержке
     */
    supportState: SupportState;

    /**
     * Описание состояния
     */
    supportStateDescription: string;
}