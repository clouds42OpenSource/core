/**
 * Модель содержащая информацию о видимости комманд с базой
 */
export type DatabaseCardCommandVisibilityDataModel = {
    /**
     * Если true, то комманда на публикацию видна, иначе false
     */
    isPublishDatabaseCommandVisible: boolean;
    /**
     * Если true, то комманда на отмену публикации видна, иначе false
     */
    isCancelPublishDatabaseCommandVisible: boolean;
    /**
     * Если true, то комманда на удаление базы видна, иначе false
     */
    isDeleteDatabaseCommandVisible: boolean;
    /**
     * Если true, то информация о том что база публикуется видна, иначе false
     */
    isDatabasePublishingInfoVisible: boolean;
    /**
     * Если true, то информация о том что отменяется публикация базы видна, иначе false
     */
    isCanacelingDatabasePublishingInfoVisible: boolean;
}