import { DatabaseCardDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models/DatabaseCardDataModel';
import {
    databaseCardAccountDatabaseInfoDataModelMapFrom,
    databaseCardBackupInfoDataModelMapFrom,
    databaseCardCommandVisibilityDataModelMapFrom,
    databaseCardReadModeFieldAccessDataModelMapFrom,
    databaseCardTabVisibilityDataModelMapFrom,
    databaseCardTehSupportInfoDataModelMapFrom
} from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/mappings';
import { DatabaseCardResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto';
import { databaseCardEditModeDictionariesDataModelMapFrom } from './databaseCardEditModeDictionariesDataModelMapFrom';

/**
 * Mapping модели карточки информационной базы
 * @param value Модель карточки информационной базы
 */
export function databaseCardDataModelMapFrom(value: DatabaseCardResponseDto): DatabaseCardDataModel {
    return {
        database: databaseCardAccountDatabaseInfoDataModelMapFrom(value.database),
        launchPublishedDbUrl: value.launchPublishedDbUrl,
        backups: value.backups.map(item => databaseCardBackupInfoDataModelMapFrom(item)),
        commandVisibility: databaseCardCommandVisibilityDataModelMapFrom(value.commandVisibility),
        fieldsAccessReadModeInfo: databaseCardReadModeFieldAccessDataModelMapFrom(value.fieldsAccessReadModeInfo),
        tabVisibility: databaseCardTabVisibilityDataModelMapFrom(value.tabVisibility),
        tehSupportInfo: value.tehSupportInfo ? databaseCardTehSupportInfoDataModelMapFrom(value.tehSupportInfo) : null,
        canEditDatabase: value.canEditDatabase,
        accountCardEditModeDictionaries: databaseCardEditModeDictionariesDataModelMapFrom(value.accountCardEditModeDictionaries)
    };
}