import { DatabaseCardTabVisibilityResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto';
import { DatabaseCardTabVisibilityDataModel } from '../data-models';

/**
 * Mapping модели с информацией о видимости табав в карточке информационной базы
 * @param value Модель с информацией о видимости табов
 */
export function databaseCardTabVisibilityDataModelMapFrom(value: DatabaseCardTabVisibilityResponseDto): DatabaseCardTabVisibilityDataModel {
    return {
        isBackupTabVisible: value.isBackupTabVisible,
        isTehSupportTabVisible: value.isTehSupportTabVisible
    };
}