/**
 * Настройки по показу полей в карточке информационной базы
 */
export type DatabaseCardReadModeFieldAccessDataModel = {

    /**
     * показывать ли поле "База доступна по адресу"
     */
    canShowWebPublishPath: boolean;

    /**
     * показывать ли поле "Опубликованы веб сервисы"
     */
    canShowUsedWebServices: boolean;

    /**
     * показывать ли поля "Детали базы"
     */
    canShowDatabaseDetails: boolean;

    /**
     * показывать ли поле "Сервер SQL"
     */
    canShowSqlServer: boolean;

    /**
     * показывать ли поле "Статус базы"
     */
    canShowDatabaseState: boolean;

    /**
     * показывать ли поле "Хранилище"
     */
    canShowDatabaseFileStorage: boolean;
}