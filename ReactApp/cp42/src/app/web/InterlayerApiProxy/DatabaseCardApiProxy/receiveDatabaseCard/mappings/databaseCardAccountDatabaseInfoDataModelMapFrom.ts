import { DateUtility } from 'app/utils';
import { DatabaseCardAccountDatabaseInfoResponseDto } from 'app/web/api/DatabaseCardProxy/response-dto';
import { DatabaseCardAccountDatabaseInfoDataModel } from '../data-models';

/**
 * Mapping модели общей информации о базе
 * @param value Модель общей информации о базе полученной при запросе
 */
export function databaseCardAccountDatabaseInfoDataModelMapFrom(value: DatabaseCardAccountDatabaseInfoResponseDto): DatabaseCardAccountDatabaseInfoDataModel {
    return {
        id: value.id,
        v82Name: value.v82Name,
        caption: value.caption,
        sizeInMB: value.sizeInMB,
        state: value.state,
        createAccountDatabaseComment: value.createAccountDatabaseComment,
        templateName: value.templateName,
        templateImgUrl: value.templateImgUrl,
        publishState: value.publishState,
        webPublishPath: value.webPublishPath,
        needShowWebLink: value.needShowWebLink,
        lastActivityDate: DateUtility.convertToDate(value.lastActivityDate),
        timezoneName: value.timezoneName,
        isDeleted: value.isDeleted,
        version: value.version,
        isDbOnDelimiters: value.isDbOnDelimiters,
        isDemoDelimiters: value.isDemoDelimiters,
        zoneNumber: value.zoneNumber,
        configurationName: value.configurationName,
        isExternalDb: value.isExternalDb,
        creationDate: DateUtility.convertToDate(value.creationDate),
        backupDate: value.backupDate ? DateUtility.convertToDate(value.backupDate) : undefined,
        backupDateString: value.backupDateString,
        existBackUpPath: value.existBackUpPath,
        calculateSizeDateTime: value.calculateSizeDateTime ? DateUtility.convertToDate(value.calculateSizeDateTime) : undefined,
        templatePlatform: value.templatePlatform,
        distributionType: value.distributionType,
        stable82Version: value.stable82Version,
        alpha83Version: value.alpha83Version,
        stable83Version: value.stable83Version,
        dbNumber: value.dbNumber,
        isFile: value.isFile,
        filePath: value.filePath,
        isReady: value.isReady,
        dbTemplate: value.dbTemplate,
        accountCaption: value.accountCaption,
        accountId: value.accountId,
        v82Server: value.v82Server,
        sqlServer: value.sqlServer,
        archivePath: value.archivePath,
        serviceName: value.serviceName,
        lockedState: value.lockedState,
        platformType: value.platformType,
        canWebPublish: value.canWebPublish,
        databaseState: value.state,
        usedWebServices: value.usedWebServices,
        cloudStorageWebLink: value.cloudStorageWebLink,
        fileStorage: value.fileStorageId,
        restoreModelType: value.restoreModelType,
        canChangeRestoreModel: value.canChangeRestoreModel,
        fileStorageName: value.fileStorageName
    };
}