import { IoCContainer } from 'app/IoCСontainer';
import { TResponseLowerCase } from 'app/api/types';
import { ReceiveDatabaseCardThunkParams } from 'app/modules/databaseCard/store/reducers/receiveDatabaseCardReducer/params';
import { DatabaseCardDataModel } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/data-models/DatabaseCardDataModel';
import { databaseCardDataModelMapFrom } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy/receiveDatabaseCard/mappings';
import { RequestKind } from 'core/requestSender/enums';

export const DatabaseCardApiProxy = (() => {
    function getDatabaseCardApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getDatabaseCardApiProxy();
    }

    async function receiveDatabaseCard(requestKind: RequestKind, args: ReceiveDatabaseCardThunkParams): Promise<TResponseLowerCase<DatabaseCardDataModel>> {
        return getDatabaseCardApiProxy().getDatabaseCard(requestKind, {
            accountDatabaseNumber: args.accountDatabaseNumber
        }).then(responseDto => {
            return {
                success: responseDto.success,
                data: responseDto.data ? databaseCardDataModelMapFrom(responseDto.data) : null,
                message: responseDto.message
            };
        });
    }

    return {
        receiveDatabaseCard
    };
})();