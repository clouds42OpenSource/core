/**
 * Параметры для удаления базы
 */
export type DeleteDatabaseParams = {
    /**
     * ID базы, для удаления
     */
    databaseId: string;
}