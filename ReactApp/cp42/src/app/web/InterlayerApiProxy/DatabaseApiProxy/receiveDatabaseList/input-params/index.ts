import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * строка поиска
     */
    searchLine: string;
};

/**
 * Параметры для загрузки конфигураций 1С
 */
export type ReceiveDatabaseListParams = SelectDataCommonDataModel<Filter>;