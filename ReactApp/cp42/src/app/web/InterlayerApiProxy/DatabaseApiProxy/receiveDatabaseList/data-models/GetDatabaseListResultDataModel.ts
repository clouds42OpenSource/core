import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { DatabaseListItemDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/data-models';

/**
 * Модель ответа на получение списка баз данных
 */
export type GetDatabaseListResultDataModel = SelectDataResultMetadataModel<DatabaseListItemDataModel>;