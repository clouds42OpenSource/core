import { DateUtility } from 'app/utils';
import { DatabaseListItemDto } from 'app/web/api/DatabaseProxy/request-dto/GetDatabaseListDtos';
import { DatabaseListItemDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/data-models';

/**
 * Mapping модели базы данных
 * @param value ответ модели базы данных
 */
export function databaseListItemDataModelMapFrom(value: DatabaseListItemDto): DatabaseListItemDataModel {
    return {
        accountCaption: value.accountCaption,
        accountDatabaseId: value.id,
        accountId: value.accountId,
        accountNumber: value.accountNumber,
        caption: value.caption,
        lockedState: value.lockedState,
        platform: value.applicationName,
        state: value.state,
        v82Name: value.v82Name,
        isFile: value.isFile,
        sizeInMB: value.sizeInMB,
        platformType: value.platformType,
        creationDate: DateUtility.convertToDate(value.creationDate),
        lastActivityDate: DateUtility.convertToDate(value.lastActivityDate),
        isPublish: value.isPublish
    };
}