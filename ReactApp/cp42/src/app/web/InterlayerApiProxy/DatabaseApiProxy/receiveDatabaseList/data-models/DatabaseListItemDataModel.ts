import { DatabaseState, PlatformType } from 'app/common/enums';

export type DatabaseListItemDataModel = {
    accountDatabaseId: string;
    v82Name: string;
    accountId: string;
    accountCaption: string;
    accountNumber: number;
    caption: string;
    sizeInMB: number;
    lastActivityDate: Date;
    creationDate: Date;
    state: DatabaseState;
    isFile?: boolean;
    lockedState: string;
    platformType: PlatformType;
    platform: string;
    isPublish: boolean;
};