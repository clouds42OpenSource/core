import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Параметры для публикации базы
 */
export type PublishDatabaseParams = IForceThunkParam & {
    /**
     * ID базы, для публикации
     */
    databaseId: string
};