import { DateUtility } from 'app/utils';
import { EnableSupportDatabaseResponseDto } from 'app/web/api/DatabaseProxy/response-dto/EnableSupportDatabaseResponseDto';
import { EnableSupportDatabaseDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/enableDatabaseSupport';

/**
 * Mapping модели с информацией о техподержке базы
 * @param value Модель с информацией о техподдержке базы
 */
export function enableSupportDatabaseDataModelMapFrom(value: EnableSupportDatabaseResponseDto): EnableSupportDatabaseDataModel {
    return {
        isInConnectingState: value.isConnects,
        supportState: value.supportState,
        supportStateDescription: value.supportStateDescription,
        lastHistoryDate: value.lastHistoryDate ? DateUtility.convertToDate(value.lastHistoryDate) : undefined
    };
}