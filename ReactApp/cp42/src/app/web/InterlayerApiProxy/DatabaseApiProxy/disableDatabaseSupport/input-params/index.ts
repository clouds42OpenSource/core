/**
 * Параметры для отключения информационной базы от тех поддержки
 */
export type DisableDatabaseSupportParams = {
    /**
     * ID базы, для отключения от тех поддержки
     */
    databaseId: string;
}