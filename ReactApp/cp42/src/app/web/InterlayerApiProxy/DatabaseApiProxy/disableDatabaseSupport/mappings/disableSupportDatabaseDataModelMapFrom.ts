import { DisableSupportDatabaseDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/disableDatabaseSupport/data-models/DisableSupportDatabaseDataModel';
import { DateUtility } from 'app/utils';
import { DisableSupportDatabaseResponseDto } from 'app/web/api/DatabaseProxy/response-dto/DisableSupportDatabaseResponseDto';

/**
 * Mapping модели с информацией о техподержке базы
 * @param value Модель с информацией о техподдержке базы
 */
export function disableSupportDatabaseDataModelMapFrom(value: DisableSupportDatabaseResponseDto): DisableSupportDatabaseDataModel {
    return {
        isInConnectingState: value.IsConnects,
        supportState: value.SupportState,
        supportStateDescription: value.SupportStateDescription,
        lastHistoryDate: value.LastHistoryDate ? DateUtility.convertToDate(value.LastHistoryDate) : undefined
    };
}