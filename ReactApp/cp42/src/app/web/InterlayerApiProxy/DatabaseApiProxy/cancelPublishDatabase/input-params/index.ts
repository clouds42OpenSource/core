/**
 * Параметры для отмены публикации базы
 */
export type CancelPublishDatabaseParams = {
    /**
     * ID базы, для отмены публикации
     */
    databaseId: string;
};