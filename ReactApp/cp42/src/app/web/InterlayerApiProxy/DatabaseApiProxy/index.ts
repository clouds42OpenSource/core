import { IoCContainer } from 'app/IoCСontainer';
import { DisableDatabaseSupportThunkParams } from 'app/modules/databaseCard/store/reducers/disableDatabaseSupportReducer/params';
import { EnableDatabaseSupportThunkParams } from 'app/modules/databaseCard/store/reducers/enableDatabaseSupportReducer/params';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { CancelPublishDatabaseParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/cancelPublishDatabase';
import { DeleteDatabaseParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/deleteDatabase/input-params';
import { DisableSupportDatabaseDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/disableDatabaseSupport/data-models/DisableSupportDatabaseDataModel';
import { disableSupportDatabaseDataModelMapFrom } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/disableDatabaseSupport/mappings/disableSupportDatabaseDataModelMapFrom';
import { EditDatabaseDataParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/editDatabaseData';
import { EnableSupportDatabaseDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/enableDatabaseSupport';
import { enableSupportDatabaseDataModelMapFrom } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/enableDatabaseSupport/mappings/enableSupportDatabaseDataModelMapFrom';
import { PublishDatabaseParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/publishDatabase';
import { GetDatabaseListResultDataModel } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/data-models';
import { ReceiveDatabaseListParams } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/input-params';
import { databaseListItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/DatabaseApiProxy/receiveDatabaseList/mappings';
import { RequestKind } from 'core/requestSender/enums';
import { TResponseLowerCase } from 'app/api/types';
import { EnableSupportDatabaseResponseDto } from 'app/web/api/DatabaseProxy/response-dto/EnableSupportDatabaseResponseDto';

export const DatabaseApiProxy = (() => {
    function getDatabaseApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getDatabaseApiProxy();
    }

    function deleteDatabase(requestKind: RequestKind, args: DeleteDatabaseParams): Promise<void> {
        return getDatabaseApiProxy().deleteDatabase(requestKind, {
            databaseId: args.databaseId
        });
    }

    function publishDatabase(requestKind: RequestKind, args: PublishDatabaseParams): Promise<void> {
        return getDatabaseApiProxy().publishDatabase(requestKind, {
            databaseId: args.databaseId,
            publish: true
        });
    }

    function cancelPublishDatabase(requestKind: RequestKind, args: CancelPublishDatabaseParams): Promise<void> {
        return getDatabaseApiProxy().cancelPublishDatabase(requestKind, {
            databaseId: args.databaseId,
            publish: false
        });
    }

    async function disableDatabaseSupport(requestKind: RequestKind, args: DisableDatabaseSupportThunkParams): Promise<DisableSupportDatabaseDataModel> {
        return getDatabaseApiProxy().disableDatabaseSupport(requestKind, {
            databaseId: args.databaseId
        }).then(responseDto => disableSupportDatabaseDataModelMapFrom(responseDto));
    }

    async function enableDatabaseSupport(requestKind: RequestKind, args: EnableDatabaseSupportThunkParams): Promise<TResponseLowerCase<EnableSupportDatabaseDataModel>> {
        const response = await getDatabaseApiProxy().enableDatabaseSupport(requestKind, {
            databaseId: args.databaseId,
            login: args.login,
            pass: args.password
        });

        return {
            success: response.success ?? true,
            data: enableSupportDatabaseDataModelMapFrom(response.data ?? response as unknown as EnableSupportDatabaseResponseDto),
            message: response.message ?? null
        };
    }

    async function editDatabaseData(requestKind: RequestKind, args: EditDatabaseDataParams): Promise<void> {
        return getDatabaseApiProxy().EditDatabaseData(requestKind, {
            DatabaseId: args.databaseId,
            V82Name: args.v82Name,
            FileStorageId: args.fileStorageId,
            DatabaseCaption: args.databaseCaption,
            DatabaseState: args.databaseState,
            DatabaseTemplateId: args.databaseTemplateId,
            DistributionType: args.distributionType,
            PlatformType: args.platformType,
            UsedWebServices: args.usedWebServices,
        });
    }

    async function receiveDatabaseList(requestKind: RequestKind, args: ReceiveDatabaseListParams): Promise<GetDatabaseListResultDataModel> {
        return getDatabaseApiProxy().receiveDatabaseList(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    searchLine: args.filter.searchLine.trim()
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, databaseListItemDataModelMapFrom));
    }

    return {
        deleteDatabase,
        publishDatabase,
        cancelPublishDatabase,
        disableDatabaseSupport,
        enableDatabaseSupport,
        editDatabaseData,
        receiveDatabaseList
    };
})();