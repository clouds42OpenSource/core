export type AccountUserListItem = {
    id: string;
    accountId: string;
    login: string;
    email: string;
    fullPhoneNumber: string;
    firstName: string;
    lastName: string;
    middleName: string;
    corpUserID: string;
    corpUserSyncStatus: string;
    removed: boolean;
    creationDate: string;
    isManager: boolean;
    activated: boolean;
};

/**
 * Модель данных списка пользователей полученных по идентификатору
 */
export type AccountUsersByIdModel = {
    accountUserList: AccountUserListItem[];
};