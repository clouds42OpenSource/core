import { GetAccountUsersResponseDto } from 'app/web/api/MsBackups/response-dto/GetAccountUsersResponseDto';
import { AccountUsersByIdModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getAccountUsersById';

export function accountUsersByIdMapFrom(value: GetAccountUsersResponseDto): AccountUsersByIdModel {
    return {
        accountUserList: value.AccountUserList.map(item => {
            return {
                id: item.ID,
                accountId: item.AccountID,
                login: item.Login,
                email: item.Email,
                fullPhoneNumber: item.FullPhoneNumber,
                firstName: item.FirstName,
                lastName: item.LastName,
                middleName: item.MiddleName,
                corpUserID: item.CorpUserID,
                corpUserSyncStatus: item.CorpUserSyncStatus,
                removed: item.Removed,
                creationDate: item.CreationDate,
                isManager: item.IsManager,
                activated: item.Activated
            };
        })
    };
}