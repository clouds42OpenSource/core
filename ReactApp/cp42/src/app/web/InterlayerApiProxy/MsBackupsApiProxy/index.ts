import { IoCContainer } from 'app/IoCСontainer';
import { AppConsts } from 'app/common/constants';
import { DtToZipDatabaseDataModel, DtToZipDatabaseInputParams, dtToZipDatabaseMapFrom } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/DtToZipDatabase';
import {
    BlockDownloadFileDataModel,
    BlockDownloadFileInputParam,
    blockDownloadFileMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/blockDownloadFile';
import {
    CostOfCreatingDatabasesModel,
    costOrCreateDatabasesMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/costOfCreatingDatabases';
import { DeleteFilesInputParams } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/deleteFiles';
import { EndDownloadFileInputParams } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/endDownloadFile';
import {
    AccountUsersByIdModel,
    accountUsersByIdMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getAccountUsersById';
import {
    BackupsInputParams,
    BackupsModel,
    backupsModelMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getBackups';
import {
    DownloadFilesInputParams,
    DownloadFilesModel,
    downloadFilesModelMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getDownloadFiles';
import {
    FileInfoByIdInputParams,
    FileInfoByIdModel,
    fileInfoByIdMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFileInfoById';
import {
    FilesInputParams,
    FilesModel,
    filesModelMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFiles';
import { ApplicationsInputParams } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/putApplications';
import {
    UploadFileAccessPriceModel,
    uploadFileAccessPriceMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/uploadFileAccessPrice';
import {
    ZipDatabaseModel,
    ZipDatabasesInputParams,
    zipDatabaseMapFrom
} from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/zipDatabase';
import { GetChunksUrlRequestDto, UploadFileAccessPriceRequestDto } from 'app/web/api/MsBackups/request-dto';
import { GetChunkUrlResponseDto } from 'app/web/api/MsBackups/response-dto';
import { RequestKind } from 'core/requestSender/enums';

export const MsBackupsApiProxy = (() => {
    const apiProxy = IoCContainer.getApiProxy();

    /**
     * Получение экземпляр прокси для работы с бекапами через МС
     */
    function getMsBackupsApiProxy() {
        return apiProxy.getMsBackupsProxy();
    }

    /**
     * Получение экземпляра прокси для работы со списком пользователей
     */
    function getAccountUsersApiProxy() {
        return apiProxy.getAccountUsersByIdProxy();
    }

    /**
     * Получение информации о файле сервиса
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getFiles(requestKind: RequestKind, args: FilesInputParams): Promise<FilesModel> {
        return getMsBackupsApiProxy().getFile(requestKind, { fileId: args.fileId })
            .then(response => filesModelMapFrom(response));
    }

    /**
     * Получение информации о файле резервной копии
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getFileInfoById(requestKind: RequestKind, args: FileInfoByIdInputParams): Promise<FileInfoByIdModel> {
        return getMsBackupsApiProxy().getFileInfoById(requestKind, { fileId: args.fileId, type: args.type })
            .then(response => fileInfoByIdMapFrom(response));
    }

    /**
     * Получение информации о готовности файла резервной копии
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getDownloadFiles(requestKind: RequestKind, args: DownloadFilesInputParams): Promise<DownloadFilesModel> {
        return getMsBackupsApiProxy().getDownloadFiles(requestKind, { fileId: args.fileId })
            .then(async response => downloadFilesModelMapFrom(await response));
    }

    /**
     * Получение списка резервных копий приложения
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getBackups(requestKind: RequestKind, args: BackupsInputParams): Promise<BackupsModel> {
        return getMsBackupsApiProxy().getBackups(requestKind, { applicationID: args.applicationId, to_date: args.toDate ?? '', from_date: args.fromDate ?? '' })
            .then(response => backupsModelMapFrom(response));
    }

    /**
     * Запрос на создание приложения из резервной копии
     * @param requestKind тип запроса
     * @param args параметры запроса
     * @deprecated 13.02.2023
     */
    async function postApplications(requestKind: RequestKind, args: ApplicationsInputParams): Promise<void> {
        return getMsBackupsApiProxy().postApplications(
            requestKind,
            {
                'file-id': args.fileId,
                name: args.name,
                configuration: args.configuration,
                users: args.users.map(item => {
                    return {
                        'data-dump-user-id': item.dataDumpUserId,
                        'catalog-user-id': item.catalogUserId,
                        'account-user-id': item.accountUserId,
                        role: item.role,
                        profiles: item.profiles.map(profilesItem => {
                            return {
                                id: profilesItem.id,
                                name: profilesItem.name,
                                admin: profilesItem.admin,
                                predefined: profilesItem.predefined
                            };
                        })
                    };
                })
            }
        );
    }

    /**
     * Запрос на инициализацию блочной загрузки файла в хранилище
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function postFiles(requestKind: RequestKind, args: BlockDownloadFileInputParam): Promise<BlockDownloadFileDataModel> {
        return getMsBackupsApiProxy().postFile(
            requestKind,
            {
                size: args.size,
                'part-size': AppConsts.maxSizeChunkForBase * 1000000,
                name: args.name,
                type: args.type,
            }
        )
            .then(response => blockDownloadFileMapFrom(response));
    }

    /**
     * Запрос на прерывание загрузки файла
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function deleteFiles(requestKind: RequestKind, args: DeleteFilesInputParams) {
        return getMsBackupsApiProxy().deleteFile(requestKind, { fileId: args.fileId });
    }

    /**
     * Запрос на прерывание загрузки файла
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function endDownloadFile(requestKind: RequestKind, args: EndDownloadFileInputParams) {
        return getMsBackupsApiProxy().endDownloadFile(requestKind, { fileId: args.fileId, parts: args.parts });
    }

    /**
     * Запрос на получение списка пользователей аккаунта по идентификатору
     * @param requestKind тип запроса
     */
    async function getAccountUsersById(requestKind: RequestKind): Promise<AccountUsersByIdModel> {
        return getAccountUsersApiProxy().getAccountUsers(requestKind)
            .then(response => accountUsersByIdMapFrom(response));
    }

    /**
     * Запрос на расчет стоимости создания ИБ
     */
    async function costOrCreatingDatabases(requestKind: RequestKind, count?: string): Promise<CostOfCreatingDatabasesModel> {
        return getAccountUsersApiProxy().costOfCreatingDatabases(requestKind, count)
            .then(response => costOrCreateDatabasesMapFrom(response));
    }

    /**
     * Запрос на создание ИБ из зипа
     */
    async function zipDatabase(requestKind: RequestKind, args: ZipDatabasesInputParams): Promise<ZipDatabaseModel> {
        return getAccountUsersApiProxy().zipDatabase(requestKind, {
            NeedUsePromisePayment: args.needUsePromisePayment,
            'databases-service-id': args.databasesServiceId,
            name: args.name,
            configuration: args.configuration,
            'file-id': args.fileId,
            users: args.users.map(item => {
                return {
                    'account-user-id': item.accountUserId,
                    'catalog-user-id': item.catalogUserId,
                    'data-dump-user-id': item.dataDumpUserId,
                    profiles: item.profiles
                };
            }),
            extensions: args.extentions.map(extention => ({ id: extention }))
        }).then(response => zipDatabaseMapFrom(response));
    }

    /**
     * Запрос на создание ИБ из дт в зип
     */
    async function dtToZipDatabase(requestKind: RequestKind, args: DtToZipDatabaseInputParams): Promise<DtToZipDatabaseDataModel> {
        return getAccountUsersApiProxy().dtToZipDatabase(requestKind, {
            'databases-service-id': args.databasesServiceId,
            'file-id': args.fileId,
            NeedUsePromisePayment: args.needUsePromisePayment,
            configuration: args.configuration,
            login: args.login,
            name: args.name,
            password: args.password,
            users: args.users.map(userItem => {
                return {
                    'account-user-id': userItem.accountUserId,
                    'catalog-user-id': userItem.catalogUserId,
                    'data-dump-user-id': userItem.dataDumpUserId,
                    profiles: userItem.profiles.map(profileItem => {
                        return {
                            name: profileItem.name,
                            id: profileItem.id,
                            admin: profileItem.admin,
                            predefined: profileItem.predefined
                        };
                    })
                };
            })
        }).then(response => dtToZipDatabaseMapFrom(response));
    }

    /**
     * Запрос на получение стоимости добавления пользователя в ИБ при создании с зип
     */
    async function uploadFileAccessPrice(requestKind: RequestKind, args: UploadFileAccessPriceRequestDto): Promise<UploadFileAccessPriceModel> {
        return getAccountUsersApiProxy().uploadFileAccessPrice(requestKind, args)
            .then(response => uploadFileAccessPriceMapFrom(response));
    }

    async function getChunkUrls(requestKind: RequestKind, args: GetChunksUrlRequestDto): Promise<GetChunkUrlResponseDto> {
        return getMsBackupsApiProxy().getChunkUrls(requestKind, args)
            .then(response => response);
    }

    return {
        getFiles,
        getFileInfoById,
        getDownloadFiles,
        getBackups,
        postApplications,
        postFiles,
        deleteFiles,
        endDownloadFile,
        getAccountUsersById,
        costOrCreatingDatabases,
        zipDatabase,
        uploadFileAccessPrice,
        dtToZipDatabase,
        getChunkUrls
    };
})();