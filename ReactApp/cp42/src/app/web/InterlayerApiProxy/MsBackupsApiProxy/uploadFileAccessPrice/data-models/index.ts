import { Nullable } from 'app/common/types';

export type AccountUserLicenceType = {
    accountUserId: string;
    hasLicense: boolean;
    accessCost: number;
};

export type UploadFileAccess = {
    serviceTypesId: string;
    accountUserLicence: AccountUserLicenceType[];
}

export type UploadFileAccessPriceModel = {
    success: boolean;
    message: Nullable<string>;
    data: UploadFileAccess;
};