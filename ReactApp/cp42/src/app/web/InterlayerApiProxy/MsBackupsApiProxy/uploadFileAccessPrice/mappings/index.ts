import { UploadFileAccessPriceResponseDto } from 'app/web/api/MsBackups/response-dto';
import { UploadFileAccessPriceModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/uploadFileAccessPrice/data-models';

export function uploadFileAccessPriceMapFrom(value: UploadFileAccessPriceResponseDto): UploadFileAccessPriceModel {
    return {
        success: value.Success,
        message: value.Message,
        data: {
            serviceTypesId: value.Data.ServiceTypesId,
            accountUserLicence: value.Data.AccountUserLicence.map(item => {
                return {
                    accountUserId: item.AccountUserId,
                    hasLicense: item.HasLicense,
                    accessCost: item.AccessCost
                };
            })
        }
    };
}