type ConfigurationModel = {
    /**
     * Код конфигурации
     */
    id: string;
    /**
     * Версия конфигурации
     */
    version: string;
    /**
     * Наименование конфигурации
     */
    name: string;
    /**
     * Поддержка версии конфигурации в сервисе
     */
    versionSupported: boolean;
    /**
     * Поддержка конфигурации в сервисе
     */
    configurationSupported: boolean;
};

export type ProfilesModel = {
    /**
     * Идентификатор поставляемых данных профиля
     */
    id: string;
    /**
     * Имя профиля
     */
    name: string;
    /**
     * Наличие у профиля админ прав
     */
    admin: boolean;
    /**
     * Поставляемый профиль
     */
    predefined: boolean;
    default: boolean;
};

export type UserModel = {
    /**
     * Наименование пользовательского дампа
     */
    name: string;
    /**
     * Имя пользователя ИБ
     */
    login: string;
    /**
     * Идентификатор пользователя ИБ
     */
    dataDumpUserId: string;
    /**
     * Идентификатор пользователя в справочнике
     */
    catalogUserId: string;
    /**
     * Идентификатор пользователя в ЛК
     */
    accountUserId: string;
    /**
     * Наименование пользователя в ЛК
     */
    accountUserName: string;
    /**
     * Статус админа ИБ
     */
    isAdmin: boolean;
    /**
     * Список профилей групп доступа
     */
    profiles: ProfilesModel[],
    eauth: boolean;
    hasPassword: boolean;
    hash1: string;
    hash2: string;
    salt: string;
    alg: 'SHA1' | 'SHA256' | 'SHA512' | 'PBKDF2SHA256' | '';
};

export type ExtensionsModel = {
    /**
     * Идентификатор расширения в архиве
     */
    id: string;
    /**
     * Имя расширения
     */
    name: string;
    /**
     * Версия расширения
     */
    version: string;
    /**
     * Расширение изменяет структуру данных
     */
    modifiesDataStructure: boolean;
    /**
     * Расширение поддерживается в сервисе
     */
    supported: boolean;
    serviceId: string;
    serviceStringId: string;
    information: string;
    auditFailed: boolean;
    serviceIsPaid: boolean;
};

type StatusModel = {
    ready?: boolean;
    timeout?: number;
};

/**
 * Модель данных информации о файле
 */
export type FileInfoByIdModel = {
    type: string;
    status?: StatusModel;
    /**
     * Конфигурация
     */
    configuration: ConfigurationModel;
    users: UserModel[];
    /**
     * Список профилей групп доступа
     */
    profiles: ProfilesModel[];
    /**
     * Расширения в архиве
     */
    extensions: ExtensionsModel[];
    /**
     * Список ошибок в архивной резервной копии
     */
    errors: string[];
    messages: string[];
    target: string;
    strongPassword: boolean;
};