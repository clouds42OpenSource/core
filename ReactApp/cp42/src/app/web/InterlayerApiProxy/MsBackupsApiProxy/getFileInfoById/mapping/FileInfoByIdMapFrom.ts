import { FileInformationByIdResponseDto } from 'app/web/api/MsBackups/response-dto';
import { FileInfoByIdModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFileInfoById';

/**
 * Маппинг модели ответа в модель приложения
 * @param value модель ответа при получении информации о файле резервной копии
 */
export function fileInfoByIdMapFrom(value: FileInformationByIdResponseDto): FileInfoByIdModel {
    return {
        type: value.type,
        status: value.status
            ? {
                ready: value.status.ready,
                timeout: value.status.timeout
            }
            : undefined,
        configuration: {
            id: value.configuration.id,
            version: value.configuration.version,
            name: value.configuration.name,
            versionSupported: value.configuration['version-supported'],
            configurationSupported: value.configuration['configuration-supported']
        },
        users: value.users.map(item => {
            return {
                name: item.name,
                login: item.login,
                dataDumpUserId: item['data-dump-user-id'],
                catalogUserId: item['catalog-user-id'],
                accountUserId: item['account-user-id'],
                accountUserName: item['account-user-name'],
                isAdmin: item['is-admin'],
                profiles: item.profiles,
                hasPassword: item['has-password'],
                eauth: item.eauth,
                hash1: item.hash1,
                hash2: item.hash2,
                alg: item.alg,
                salt: item.salt
            };
        }),
        profiles: value.profiles,
        extensions: value.extensions.map(item => {
            return {
                id: item.id,
                name: item.name,
                version: item.version,
                modifiesDataStructure: item['modifies-data-structure'],
                supported: item.supported,
                serviceId: item['service-id'],
                serviceStringId: item['service-string-id'],
                information: item.information,
                auditFailed: item['audit-failed'],
                serviceIsPaid: item['service-is-paid']
            };
        }),
        errors: value.errors,
        strongPassword: value['strong-passwords'],
        messages: value.messages,
        target: value.target
    };
}