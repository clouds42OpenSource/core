/**
 * Параметры для получении информации о файле резервной копии
 */
export type FileInfoByIdInputParams = {
    fileId: string;
    type: 'data_dump' | 'dt';
};