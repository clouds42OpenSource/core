/**
 * Параметры для получения информации о файле сервиса
 */
export type FilesInputParams = {
    fileId: string;
};