type UserModelProfiles = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

type UserModel = {
    name: string;
    login: string;
    dbUserId: string;
    catalogUserId: string;
    accountUserId: string;
    isAdmin: boolean;
    profiles: UserModelProfiles[];
};

type ExtensionsModel = {
    id: string;
    name: string;
    version: string;
    modifiesDataStructure: boolean;
    supported: boolean;
};

type ProfileModel = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

/**
 * Модель информации о файле сервиса
 */
export type FilesModel = {
    configuration: {
        id: string;
        version: string;
        name: string;
        versionSupported: boolean;
        configurationSupported: boolean;
    };
    user: UserModel[];
    extensions: ExtensionsModel[];
    errors: string[];
    profiles: ProfileModel[];
    type: string;
    strongPasswords: boolean;
};