import { FilesResponseDto } from 'app/web/api/MsBackups/response-dto';
import { FilesModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getFiles';

/**
 * Маппинг модели ответа в модель приложения
 * @param value модель ответа при получении информации о файле сервиса
 */
export function filesModelMapFrom(value: FilesResponseDto): FilesModel {
    return {
        configuration: {
            id: value.configuration.id,
            version: value.configuration.version,
            name: value.configuration.name,
            versionSupported: value.configuration['version-supported'],
            configurationSupported: value.configuration['configuration-supported']
        },
        user: value.user.map(item => {
            return {
                name: item.name,
                login: item.login,
                dbUserId: item['db-user-id'],
                catalogUserId: item['catalog-user-id'],
                accountUserId: item['account-user-id'],
                isAdmin: item['is-admin'],
                profiles: item.profiles.map(profilesItem => {
                    return {
                        id: profilesItem.id,
                        name: profilesItem.name,
                        admin: profilesItem.admin,
                        predefined: profilesItem.predefined
                    };
                })
            };
        }),
        extensions: value.extensions.map(item => {
            return {
                id: item.id,
                name: item.name,
                version: item.version,
                modifiesDataStructure: item['modifies-data-structure'],
                supported: item.supported
            };
        }),
        errors: value.errors,
        profiles: value.profiles,
        type: value.type,
        strongPasswords: value['strong-passwords']
    };
}