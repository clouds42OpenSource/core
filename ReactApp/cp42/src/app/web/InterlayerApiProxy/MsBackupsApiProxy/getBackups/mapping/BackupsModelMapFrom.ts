import { BackupsResponseDto } from 'app/web/api/MsBackups/response-dto';
import { BackupsModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getBackups';

export function backupsModelMapFrom(value: BackupsResponseDto): BackupsModel {
    return {
        backups: value.backups
            ? value.backups.map(item => {
                return {
                    id: item.id,
                    fileId: item['file-id'],
                    fileSize: item['file-size'],
                    date: item.date,
                    appVersion: item['app-version'],
                    isOriginal: item['is-original'],
                    isOndemand: item['is-ondemand'],
                    isAnnual: item['is-annual'],
                    isMounthly: item['is-mounthly'],
                    isDaily: item['is-daily'],
                    isEternal: item['is-eternal'],
                    eternalBackupUrl: item['eternal-backup-url'],
                    comment: item.comment,
                    type: item.type
                };
            })
            : []
    };
}