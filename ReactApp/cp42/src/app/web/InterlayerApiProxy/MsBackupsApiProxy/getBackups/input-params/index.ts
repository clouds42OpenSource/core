/**
 * Параметры для получения списка резервных копий приложения
 */
export type BackupsInputParams = {
    applicationId: string,
    fromDate?: string,
    toDate?: string
};