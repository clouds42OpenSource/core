export type BackupsItem = {
    id: string;
    fileId: string;
    fileSize: string;
    date: string;
    appVersion: string;
    isOriginal: boolean;
    isOndemand: boolean;
    isAnnual: boolean;
    isMounthly: boolean;
    isDaily: boolean;
    isEternal: boolean;
    type: string;
    eternalBackupUrl: string;
    comment: string;
};

/**
 * Модель данных списка резервных копий
 */
export type BackupsModel = {
    backups: BackupsItem[];
};