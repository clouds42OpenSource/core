type ProfilesItemInputParams = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

export type editTableUser = {
    accountUserId: string;
    name: string;
    dataDumpUserId: string;
    profilesIds: string[];
};

export type UserItemInputParams = {
    dataDumpUserId: string;
    catalogUserId: string;
    accountUserId: string;
    profiles: ProfilesItemInputParams[];
};

export type ZipDatabasesInputParams = {
    needUsePromisePayment: boolean;
    databasesServiceId: string;
    name: string;
    configuration: string;
    fileId : string;
    users: UserItemInputParams[];
    extentions: string[]
};