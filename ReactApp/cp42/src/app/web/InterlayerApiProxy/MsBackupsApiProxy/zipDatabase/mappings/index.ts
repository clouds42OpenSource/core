import { ZipDatabaseResponceDto } from 'app/web/api/MsBackups/response-dto/ZipDatabaseResponceDto';
import { ZipDatabaseModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/zipDatabase';

export function zipDatabaseMapFrom(value: ZipDatabaseResponceDto): ZipDatabaseModel {
    return {
        success: value.Success,
        message: value.Message,
        data: {
            errorMessage: value.Data.ErrorMessage,
            isComplete: value.Data.IsComplete,
            notEnoughMoney: value.Data.NotEnoughMoney,
            amount: value.Data.Amount,
            currency: value.Data.Currency,
            canUsePromisePayment: value.Data.CanUsePromisePayment
        }
    };
}