import { Nullable } from 'app/common/types';

export type ZipDatabaseModel = {
    success: boolean;
    message: Nullable<string>;
    data: {
        errorMessage: Nullable<string>;
        isComplete: boolean;
        notEnoughMoney: number;
        amount: number;
        currency: Nullable<string>;
        canUsePromisePayment: boolean;
    }
};