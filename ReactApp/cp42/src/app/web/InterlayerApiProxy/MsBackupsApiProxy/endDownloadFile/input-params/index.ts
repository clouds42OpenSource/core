/**
 * Модель данных для завершения загрузки файлов
 */
export type EndDownloadFileInputParams = {
    fileId: string;
    parts: string[];
};