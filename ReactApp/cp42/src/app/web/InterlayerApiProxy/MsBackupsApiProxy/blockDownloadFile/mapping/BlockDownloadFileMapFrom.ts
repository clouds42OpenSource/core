import { BlockDownloadFileResponseDto } from 'app/web/api/MsBackups/response-dto';
import { BlockDownloadFileDataModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/blockDownloadFile';

/**
 * Маппинг модели ответа в модель приложения
 * @param value модель ответа при получении информации об инициализации блочной загрузки файла в хранилище
 */
export function blockDownloadFileMapFrom(value: BlockDownloadFileResponseDto): BlockDownloadFileDataModel {
    return {
        storageType: value['storage-type'],
        id: value.id,
        partSize: value['part-size'],
        parts: value.parts.map(item => {
            return {
                url: item.url,
                headers: item.headers,
                number: item.number
            };
        })
    };
}