/**
 * Параметры для инициализации блочной загрузки файла в хранилище
 */
export type BlockDownloadFileInputParam = {
    size: number;
    name?: string;
    type: string;
};