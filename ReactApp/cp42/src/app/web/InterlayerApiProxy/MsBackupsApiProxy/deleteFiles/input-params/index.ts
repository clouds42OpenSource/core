/**
 * Модель данных для прерывания загрузки файла
 */
export type DeleteFilesInputParams = {
    fileId: string
};