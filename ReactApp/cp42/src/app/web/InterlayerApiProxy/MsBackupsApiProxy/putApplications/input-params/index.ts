type ProfilesModel = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

type UsersModel = {
    dataDumpUserId: string;
    catalogUserId: string;
    accountUserId: string;
    role: string;
    profiles: ProfilesModel[];
};

/**
 * Параметры для создания нового приложения из резервной копии
 */
export type ApplicationsInputParams = {
    fileId: string;
    accountId: string;
    name: string;
    configuration: string;
    users: UsersModel[];
};