export type DtToZipDatabaseDataModel = {
    success: boolean;
    data: {
        isComplete: boolean;
        notEnoughMoney: string;
        amount: string;
        canUsePromisePayment: boolean;
    };
    message: string | null;
};