export type ProfilesItemInputParams = {
    id: string;
    name: string;
    admin: boolean;
    predefined: boolean;
};

export type UsersItemInputParams = {
    dataDumpUserId: string;
    catalogUserId: string;
    accountUserId: string;
    profiles: ProfilesItemInputParams[];
};

export type DtToZipDatabaseInputParams = {
    name: string;
    configuration : string;
    databasesServiceId: string;
    login: string;
    password: string;
    fileId: string;
    needUsePromisePayment: boolean;
    users: UsersItemInputParams[];
};