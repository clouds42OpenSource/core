import { DtToZipDatabaseResponseDto } from 'app/web/api/MsBackups/response-dto';
import { DtToZipDatabaseDataModel } from '../data-model';

export function dtToZipDatabaseMapFrom(value: DtToZipDatabaseResponseDto): DtToZipDatabaseDataModel {
    return {
        success: value.Success,
        data: {
            amount: value.Data?.Amount ?? '0',
            canUsePromisePayment: value.Data?.CanUsePromisePayment ?? false,
            isComplete: value.Data?.IsComplete ?? false,
            notEnoughMoney: value.Data?.NotEnoughMoney ?? ''
        },
        message: value.Message ?? '',
    };
}