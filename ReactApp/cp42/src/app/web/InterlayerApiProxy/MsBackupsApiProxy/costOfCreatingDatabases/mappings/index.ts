import { CostOfCreatingDatabasesResponseDto } from 'app/web/api/MsBackups/response-dto/CostOfCreatingDatabasesResponseDto';
import { CostOfCreatingDatabasesModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/costOfCreatingDatabases';

export function costOrCreateDatabasesMapFrom(value: CostOfCreatingDatabasesResponseDto): CostOfCreatingDatabasesModel {
    return {
        data: {
            balance: value.Data.Balance,
            costOftariff: value.Data.CostOftariff,
            error: value.Data.Error,
            canGetPromisePayment: value.Data.CanGetPromisePayment,
            complete: value.Data.Complete,
            monthlyCost: value.Data.MonthlyCost,
            currency: value.Data.Currency,
            enoughMoney: value.Data.EnoughMoney
        },
        message: value.Message,
        success: value.Success
    };
}