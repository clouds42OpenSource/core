import { Nullable } from 'app/common/types';

export type CostOfCreating = {
    balance: number,
    complete: boolean,
    error: Nullable<string>,
    enoughMoney: number,
    canGetPromisePayment: boolean,
    costOftariff: number,
    monthlyCost: number,
    currency: string
}

export type CostOfCreatingDatabasesModel = {
    success: boolean,
    message: Nullable<string>,
    data: CostOfCreating
};