/**
 * Параметры для получения информации о файле резервной копии
 */
export type DownloadFilesInputParams = {
    fileId: string;
};