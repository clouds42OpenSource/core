import { DownloadFilesModel } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy/getDownloadFiles';

/**
 * Маппинг модели ответа в модель приложения
 * @param value модель ответа при получении информации о файле резервной копии
 */
export async function downloadFilesModelMapFrom(value: Response): Promise<DownloadFilesModel> {
    const response: Response = value.status === 200 ? await value.json() : {};
    return {
        url: response.url ?? '',
        header: value.headers,
        status: value.status
    };
}