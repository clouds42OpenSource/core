/**
 * Модель о файле резервной копии
 */
export type DownloadFilesModel = {
    url: string;
    header: Headers;
    status: number;
};