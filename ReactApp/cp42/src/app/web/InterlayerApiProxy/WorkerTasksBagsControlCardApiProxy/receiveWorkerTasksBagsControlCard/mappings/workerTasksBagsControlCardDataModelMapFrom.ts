import { WorkerTasksBagsControlCardDataModel } from 'app/web/InterlayerApiProxy/WorkerTasksBagsControlCardApiProxy/receiveWorkerTasksBagsControlCard/data-models';
import { WorkerTasksBagsControlCardDataResponseDto } from 'app/web/api/WorkerTasksBagsControlCardProxy/response-dto/WorkerTasksBagsControlCardDataResponseDto';
import { keyValueDataModelMapFrom } from 'app/web/common/mappings/keyValueDataModelMapFrom';

/**
 * Выполнить маппинг модели данных карточки управления доступными задачами воркера, полученной при запросе, в модель приложения
 * @param value Модель данных карточки управления доступными задачами воркера
 */
export function workerTasksBagsControlCardDataModelMapFrom(value: WorkerTasksBagsControlCardDataResponseDto): WorkerTasksBagsControlCardDataModel {
    return {
        availableTasksForAdd: value.availableTasksForAdd.map(item => keyValueDataModelMapFrom(item)),
        workerTasksBags: value.workerTasksBags.map(item => keyValueDataModelMapFrom(item)),
        workerName: value.workerName ?? 'core-worker',
        coreWorkerId: value.workerId
    };
}