import { KeyValueDataModel } from 'app/web/common/data-models';

/**
 * Модель карточки управления доступными задачами воркера
 */
export type WorkerTasksBagsControlCardDataModel = {
    /**
     * ID воркера
     */
    coreWorkerId: number;
    /**
     * Название воркера
     */
    workerName: string;
    /**
     * Доступные задачи для добавления
     */
    availableTasksForAdd: Array<KeyValueDataModel<string, string>>;
    /**
     * Собственные задачи воркера
     */
    workerTasksBags: Array<KeyValueDataModel<string, string>>;
};