import { IoCContainer } from 'app/IoCСontainer';
import { RequestKind } from 'core/requestSender/enums';
import { WorkerTasksBagsControlCardDataModel } from 'app/web/InterlayerApiProxy/WorkerTasksBagsControlCardApiProxy/receiveWorkerTasksBagsControlCard/data-models';
import { workerTasksBagsControlCardDataModelMapFrom } from 'app/web/InterlayerApiProxy/WorkerTasksBagsControlCardApiProxy/receiveWorkerTasksBagsControlCard/mappings/workerTasksBagsControlCardDataModelMapFrom';
import { ReceiveWorkerTaskBagsControlCardThunkParams } from 'app/modules/workerTasksBagsControlCard/store/reducers/receiveWorkerTasksBagsControlCard/params/ReceiveWorkerTaskBagsControlCardThunkParams';
import { ChangeTasksBagsParams } from 'app/modules/workerTasksBagsControlCard/store/reducers/changeTasksBags/params';

/**
 * Прокси для работы с карточкой управения доступными задачами воркера
 */
export const WorkerTasksBagsControlCardApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getWorkerTasksBagsApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getWorkerTasksBagsApiProxy();
    }

    /**
     * Получить данные для карточки
     * @param requestKind тип запроса
     * @param args
     */
    async function receiveCardData(requestKind: RequestKind, args: ReceiveWorkerTaskBagsControlCardThunkParams): Promise<WorkerTasksBagsControlCardDataModel> {
        return getWorkerTasksBagsApiProxy().receiveCardData(requestKind, {
            coreWorkerId: args.coreWorkerId
        }).then(responseDto => {
            return workerTasksBagsControlCardDataModelMapFrom(responseDto);
        });
    }

    async function changeTasksBags(requestKind: RequestKind, args: ChangeTasksBagsParams): Promise<void> {
        return getWorkerTasksBagsApiProxy().changeTasksBags(requestKind, {
            CoreWorkerId: args.coreWorkerId,
            WorkerTasksBagsIds: args.workerTasksBagsIds
        });
    }

    return {
        receiveCardData,
        changeTasksBags
    };
})();