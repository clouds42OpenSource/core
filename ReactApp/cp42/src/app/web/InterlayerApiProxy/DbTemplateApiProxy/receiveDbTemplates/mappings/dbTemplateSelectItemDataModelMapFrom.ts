import { DbTemplateSelectItemResponseDto } from 'app/web/api/DbTemplateProxy/response-dto';
import { DbTemplateSelectItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateSelectItemDataModel';

/**
 * Mapping модели шаблона
 * @param value ответ модели шаблона
 */
export function dbTemplateSelectItemDataModelMapFrom(value: DbTemplateSelectItemResponseDto): DbTemplateSelectItemDataModel {
    return {
        dbTemplateId: value.id,
        dbTemplateName: value.name,
        dbTemplateDescription: value.defaultCaption,
        platform: value.platform
    };
}