import { PlatformType } from 'app/common/enums';

/**
 * Модель шаблона
 */
export type DbTemplateItemDataModel = {
    /**
     * ID шаблона
     */
    templateId: string;

    /**
     * Название шаблона
     */
    templateName: string;

    /**
     * Описание шаблона
     */
    templateDescription: string;

    /**
     * Порядок
     */
    order: number;

    /**
     * Платформа
     */
    platform: PlatformType;
    platformType: PlatformType;

    /**
     * Локаль
     */
    localeId: string;

    /**
     * Шаблон с демо данными
     */
    demoTemplateId: string;

    /**
     * Можно ли публиковать на web
     */
    canWebPublish: boolean;

    /**
     * Имя конфигурации 1с
     */
    configuration1CName: string;

    /**
     * Логин администратора
     */
    adminLogin: string;

    /**
     * Пароль администратора
     */
    adminPassword: string;

    /**
     * Необходимо ли автообновление
     */
    needUpdate: boolean
};