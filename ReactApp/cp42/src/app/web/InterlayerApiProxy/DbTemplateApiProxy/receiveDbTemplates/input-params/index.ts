import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Название шаблона
     */
    dbTemplateName: string;

    /**
     * Описание шаблона
     */
    dbTemplateDescription: string;
};

/**
 * Параметры для загрузки шаблонов
 */
export type ReceiveDbTemplatesParams = SelectDataCommonDataModel<Filter>;