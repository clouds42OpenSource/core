import { DbTemplateItemDto } from 'app/web/api/DbTemplateProxy/request-dto/DbTemplateItemDto';
import { DbTemplateItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateItemDataModel';

/**
 * Mapping модели шаблона
 * @param value ответ модели шаблона
 */
export function dbTemplateItemDataModelMapFrom(value: DbTemplateItemDto): DbTemplateItemDataModel {
    return {
        templateId: value.id,
        localeId: value.localeId,
        templateName: value.name,
        order: value.order,
        templateDescription: value.defaultCaption,
        canWebPublish: value.canWebPublish,
        configuration1CName: value.configuration1CName,
        demoTemplateId: value.demoTemplateId,
        needUpdate: value.needUpdate,
        platform: value.platform,
        platformType: value.platformType,
        adminLogin: value.adminLogin,
        adminPassword: value.adminPassword
    };
}