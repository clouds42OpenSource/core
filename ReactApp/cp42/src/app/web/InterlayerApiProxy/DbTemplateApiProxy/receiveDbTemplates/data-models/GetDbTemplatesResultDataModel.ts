import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { DbTemplateSelectItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateSelectItemDataModel';

/**
 * Модель ответа на получение списка шаблонов
 */
export type GetDbTemplatesResultDataModel = SelectDataResultMetadataModel<DbTemplateSelectItemDataModel>;