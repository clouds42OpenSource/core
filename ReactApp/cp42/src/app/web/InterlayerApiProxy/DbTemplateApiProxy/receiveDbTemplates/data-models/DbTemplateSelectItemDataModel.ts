export type DbTemplateSelectItemDataModel = {
    /**
     * ID шаблона
     */
    dbTemplateId: string;

    /**
     * Название шаблона
     */
    dbTemplateName: string;

    /**
     * Описание шаблона
     */
    dbTemplateDescription: string;

    /**
     * Версия платформы
     */
    platform: string;
};