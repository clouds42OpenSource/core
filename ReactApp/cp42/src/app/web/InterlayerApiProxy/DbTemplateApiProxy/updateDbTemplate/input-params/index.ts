import { DbTemplateItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateItemDataModel';

/**
 * Параметры для обновления шаблона
 */
export type UpdateDbTemplateParams = DbTemplateItemDataModel;