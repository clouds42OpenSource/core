import { DbTemplateItemDto } from 'app/web/api/DbTemplateProxy/request-dto/DbTemplateItemDto';
import { DbTemplateItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateItemDataModel';

/**
 * Mapping модели шаблона
 * @param value ответ модели шаблона
 */
export function dbTemplateItemDtoMapFrom(value: DbTemplateItemDataModel): DbTemplateItemDto {
    return {
        id: value.templateId,
        localeId: value.localeId,
        name: value.templateName,
        order: value.order,
        defaultCaption: value.templateDescription,
        canWebPublish: value.canWebPublish,
        demoTemplateId: value.demoTemplateId,
        needUpdate: value.needUpdate,
        configuration1CName: value.configuration1CName,
        platformType: value.platformType,
        platform: value.platform,
        adminLogin: value.adminLogin,
        adminPassword: value.adminPassword
    };
}