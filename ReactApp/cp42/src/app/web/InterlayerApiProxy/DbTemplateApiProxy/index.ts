import { IoCContainer } from 'app/IoCСontainer';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { keyValueDataModelMapFrom, selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { AddNewDbTemplateDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/addDbTemplate/data-models/AddNewIndustryDataModel';
import { AddDbTemplateParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/addDbTemplate/input-params';
import { DeleteDbTemplateParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/deleteDbTemplate/input-params';
import { DbTemplateDataToEditDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/getTemplateDataToEdit/data-models/DbTemplateDataToEditDataModel';
import { GetDbTemplateDataToEditParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/getTemplateDataToEdit/input-params';
import { templateDataToEditDataModelMapFrom } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/getTemplateDataToEdit/mappings/templateDataToEditDataModelMapFrom';
import { dbTemplateSelectItemDataModelMapFrom, GetDbTemplatesResultDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates';
import { ReceiveDbTemplatesParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/input-params';
import { UpdateDbTemplateParams } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/updateDbTemplate/input-params';
import { dbTemplateItemDtoMapFrom } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/updateDbTemplate/mappings/dbTemplateItemDtoMapFrom';
import { RequestKind } from 'core/requestSender/enums';

export const DbTemplateApiProxy = (() => {
    function getDbTemplateApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getDbTemplateProxy();
    }

    function getDbTemplateApiProxyCore() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getDbTemplateCoreProxy();
    }

    async function receiveDbTemplates(requestKind: RequestKind, args: ReceiveDbTemplatesParams): Promise<GetDbTemplatesResultDataModel> {
        return getDbTemplateApiProxy().receiveDbTemplates(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    name: args.filter.dbTemplateName,
                    defaultCaption: args.filter.dbTemplateDescription
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, dbTemplateSelectItemDataModelMapFrom));
    }

    async function receiveAllDbTemplatesAsKeyValue(requestKind: RequestKind): Promise<KeyValueDataModel<string, string>[]> {
        return getDbTemplateApiProxy().receiveAllDbTemplatesAsKeyValue(requestKind)
            .then(responseDto => responseDto.map(item => keyValueDataModelMapFrom(item)));
    }

    async function getDbTemplateDataToEdit(requestKind: RequestKind, args: GetDbTemplateDataToEditParams): Promise<DbTemplateDataToEditDataModel> {
        return getDbTemplateApiProxy().getDbTemplateDataToEdit(requestKind, {
            DbTemplateId: args.dbTemplateId
        }).then(responseDto => templateDataToEditDataModelMapFrom(responseDto));
    }

    async function updateDbtemplate(requestKind: RequestKind, args: UpdateDbTemplateParams): Promise<void> {
        return getDbTemplateApiProxy().updateDbTemplate(requestKind, {
            ...dbTemplateItemDtoMapFrom(args)
        });
    }

    async function addDbtemplate(requestKind: RequestKind, args: AddDbTemplateParams): Promise<AddNewDbTemplateDataModel> {
        return getDbTemplateApiProxy().addDbTemplate(requestKind, {
            ...dbTemplateItemDtoMapFrom(args)
        }).then(responseDto => {
            return {
                dbTemplateId: responseDto.dbTemplateId
            };
        });
    }

    async function deleteDbTemplate(requestKind: RequestKind, args: DeleteDbTemplateParams): Promise<void> {
        return getDbTemplateApiProxy().deleteDbTemplate(requestKind, {
            DbTemplateId: args.dbTemplateId
        });
    }

    async function getDbTemplateConfiguration1CName(requestKind: RequestKind): Promise<string[]> {
        return getDbTemplateApiProxyCore().getDbTemplateConfiguration1CName(requestKind);
    }

    return {
        receiveDbTemplates,
        getDbTemplateDataToEdit,
        updateDbtemplate,
        addDbtemplate,
        deleteDbTemplate,
        receiveAllDbTemplatesAsKeyValue,
        getDbTemplateConfiguration1CName
    };
})();