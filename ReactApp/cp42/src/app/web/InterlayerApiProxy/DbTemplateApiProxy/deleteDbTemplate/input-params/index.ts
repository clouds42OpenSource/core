/**
 * Параметры для удаления шаблона
 */
export type DeleteDbTemplateParams = {
    /**
     * ID шаблона для удаления
     */
    dbTemplateId: string;
};