import { DbTemplateItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateItemDataModel';

/**
 * Модель ответа на получение шаблона для редактирования
 */
export type DbTemplateDataToEditDataModel = {
    /**
     * Шаблон для редактирования
     */
    dbTemplateToEdit?: DbTemplateItemDataModel;
};