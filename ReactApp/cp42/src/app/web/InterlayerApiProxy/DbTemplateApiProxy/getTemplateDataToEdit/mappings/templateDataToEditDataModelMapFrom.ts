import { GetDbTemplateToEditResponseDto } from 'app/web/api/DbTemplateProxy/response-dto/GetDbTemplateToEditResponseDto';
import { DbTemplateDataToEditDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/getTemplateDataToEdit/data-models/DbTemplateDataToEditDataModel';
import { dbTemplateItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/mappings/dbTemplateItemDataModelMapFrom';

/**
 * Mapping модели ответа на получение шаблона для редактирования
 * @param value ответ на получение шаблона для редактирования
 */
export function templateDataToEditDataModelMapFrom(value: GetDbTemplateToEditResponseDto): DbTemplateDataToEditDataModel {
    return {
        dbTemplateToEdit: dbTemplateItemDataModelMapFrom(value)
    };
}