/**
 * Параметры для загрузки шаблона для редактирования
 */
export type GetDbTemplateDataToEditParams = {
    /**
     * ID шаблона для загрузки
     */
    dbTemplateId: string
};