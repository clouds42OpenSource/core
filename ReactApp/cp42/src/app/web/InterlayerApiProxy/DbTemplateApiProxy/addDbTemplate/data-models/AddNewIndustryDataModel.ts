/**
 * Модель результата на добавление нового шаблона
 */
export type AddNewDbTemplateDataModel = {
    /**
     * ID шаблона после добавления
     */
    dbTemplateId: string;
};