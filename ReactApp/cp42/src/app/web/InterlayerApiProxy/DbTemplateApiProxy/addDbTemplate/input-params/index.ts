import { DbTemplateItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy/receiveDbTemplates/data-models/DbTemplateItemDataModel';

/**
 * Параметры для добавления шаблона
 */
export type AddDbTemplateParams = DbTemplateItemDataModel;