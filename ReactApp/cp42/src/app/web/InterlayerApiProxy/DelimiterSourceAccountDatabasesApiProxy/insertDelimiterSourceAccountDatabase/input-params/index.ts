/**
 * Параметры для добавления новой записи материнской базы разделителей
 */
export type InsertDelimiterSourceAccountDatabaseParams = {
    /**
     * Id информационной базы
     */
    accountDatabaseId: string;

    /**
     * Код конфигурации базы на разделителях
     */
    dbTemplateDelimiterCode: string;

    /**
     * Адрес публикации базы на разделителях
     */
    databaseOnDelimitersPublicationAddress: string;
};