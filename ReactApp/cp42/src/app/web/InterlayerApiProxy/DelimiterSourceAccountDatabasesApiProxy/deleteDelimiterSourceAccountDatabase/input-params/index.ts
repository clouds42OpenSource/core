/**
 * Параметры для удаления записи материнской базы разделителей
 */
export type DeleteDelimiterSourceAccountDatabaseParams = {
    /**
     * Id информационной базы
     */
    accountDatabaseId: string;

    /**
     * Код конфигурации базы на разделителях
     */
    dbTemplateDelimiterCode: string;
};