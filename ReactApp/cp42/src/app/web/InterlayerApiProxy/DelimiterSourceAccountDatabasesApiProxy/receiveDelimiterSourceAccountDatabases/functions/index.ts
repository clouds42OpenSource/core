import { DelimiterSourceAccountDatabaseItemDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases/data-models';

/**
 * Генерирует значение `DelimiterSourceAccountDatabaseItemDataModel.id` для переданного элемента `item`
 * @param item Модель материнской базы разделителей
 *
 * ```ts
 * ts: item.id = `${item.accountDatabaseId}${item.dbTemplateDelimiterCode}`;
 * ```
 */
export function delimiterSourceAccountDatabaseItemGenerateIdFor(item: DelimiterSourceAccountDatabaseItemDataModel) {
    item.id = `${ item.accountDatabaseId }${ item.dbTemplateDelimiterCode }`;
}