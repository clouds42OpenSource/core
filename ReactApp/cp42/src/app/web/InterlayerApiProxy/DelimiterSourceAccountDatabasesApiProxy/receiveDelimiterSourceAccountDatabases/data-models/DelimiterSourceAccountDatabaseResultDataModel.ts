import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { DelimiterSourceAccountDatabaseItemDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases/data-models/DelimiterSourceAccountDatabaseItemDataModel';

/**
 * Записи материнских баз разделителей
 */
export type DelimiterSourceAccountDatabaseResultDataModel = SelectDataResultMetadataModel<DelimiterSourceAccountDatabaseItemDataModel>;