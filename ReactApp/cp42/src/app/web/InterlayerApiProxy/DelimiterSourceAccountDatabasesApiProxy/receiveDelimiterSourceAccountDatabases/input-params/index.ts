import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Строка фильтра
     */
    searchLine: string,
};

/**
 * Параметры для загрузки материнских баз разделителей
 */
export type ReceiveDelimiterSourceAccountDatabasesParams = SelectDataCommonDataModel<Filter>;