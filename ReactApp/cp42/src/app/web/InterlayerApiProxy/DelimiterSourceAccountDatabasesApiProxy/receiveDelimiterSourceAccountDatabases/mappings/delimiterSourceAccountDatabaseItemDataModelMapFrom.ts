import { DelimiterSourceAccountDatabaseItemResponseDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/response-dto';
import { DelimiterSourceAccountDatabaseItemDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import { delimiterSourceAccountDatabaseItemGenerateIdFor } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases/functions';

/**
 * Mapping модели информацию о материнских базах разделителей
 * @param value Модель информации о материнских базах разделителей
 */
export function delimiterSourceAccountDatabaseItemDataModelMapFrom(value: DelimiterSourceAccountDatabaseItemResponseDto): DelimiterSourceAccountDatabaseItemDataModel {
    const item = {
        id: '',
        accountDatabaseId: value.accountDatabaseId,
        accountDatabaseV82Name: value.accountDatabaseV82Name,
        dbTemplateDelimiterCode: value.dbTemplateDelimiterCode,
        databaseOnDelimitersPublicationAddress: value.databaseOnDelimitersPublicationAddress
    };

    delimiterSourceAccountDatabaseItemGenerateIdFor(item);
    return item;
}