/**
 * Модель материнской базы разделителей
 */
export type DelimiterSourceAccountDatabaseItemDataModel = {
    /**
     * Уникальное значение записи
     */
    id: string;

    /**
     * Id информационной базы
     */
    accountDatabaseId: string;

    /**
     * Код конфигурации базы на разделителях
     */
    dbTemplateDelimiterCode: string;

    /**
     * Адрес публикации базы на разделителях
     */
    databaseOnDelimitersPublicationAddress: string;

    /**
     * Номер информационной базы
     */
    accountDatabaseV82Name: string;
};