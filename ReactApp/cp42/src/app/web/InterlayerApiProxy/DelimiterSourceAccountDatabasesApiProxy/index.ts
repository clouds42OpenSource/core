import { IoCContainer } from 'app/IoCСontainer';
import { KeyValueDataModel } from 'app/web/common/data-models';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { keyValueDataModelMapFrom } from 'app/web/common/mappings/keyValueDataModelMapFrom';
import { DeleteDelimiterSourceAccountDatabaseParams } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/deleteDelimiterSourceAccountDatabase/input-params';
import { InsertDelimiterSourceAccountDatabaseParams } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/insertDelimiterSourceAccountDatabase/input-params';
import { delimiterSourceAccountDatabaseItemDataModelMapFrom, DelimiterSourceAccountDatabaseResultDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases';
import { ReceiveDelimiterSourceAccountDatabasesParams } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/receiveDelimiterSourceAccountDatabases/input-params';
import { SearchAccountDatabaseDataModel, searchAccountDatabaseDataModelMapFrom } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/searchAccountDatabases';
import { SearchAccountDatabasesParams } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/searchAccountDatabases/input-params';
import { UpdateDelimiterSourceAccountDatabaseParams } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/updateDelimiterSourceAccountDatabase/input-params';
import { RequestKind } from 'core/requestSender/enums';

export const DelimiterSourceAccountDatabasesApiProxy = (() => {
    function getDelimiterSourceAccountDatabasesApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getDelimiterSourceAccountDatabasesProxy();
    }

    async function receiveDelimiterSourceAccountDatabases(requestKind: RequestKind, args: ReceiveDelimiterSourceAccountDatabasesParams): Promise<DelimiterSourceAccountDatabaseResultDataModel> {
        return getDelimiterSourceAccountDatabasesApiProxy().receiveDelimiterSourceAccountDatabases(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    SearchLine: args.filter.searchLine
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, delimiterSourceAccountDatabaseItemDataModelMapFrom));
    }

    async function updateDelimiterSourceAccountDatabase(requestKind: RequestKind, args: UpdateDelimiterSourceAccountDatabaseParams): Promise<boolean> {
        return getDelimiterSourceAccountDatabasesApiProxy().updateDelimiterSourceAccountDatabase(requestKind, {
            AccountDatabaseId: args.accountDatabaseId,
            DbTemplateDelimiterCode: args.dbTemplateDelimiterCode,
            DatabaseOnDelimitersPublicationAddress: args.databaseOnDelimitersPublicationAddress
        }).then(_responseDto => true);
    }

    async function insertDelimiterSourceAccountDatabase(requestKind: RequestKind, args: InsertDelimiterSourceAccountDatabaseParams): Promise<boolean> {
        return getDelimiterSourceAccountDatabasesApiProxy().insertDelimiterSourceAccountDatabase(requestKind, {
            AccountDatabaseId: args.accountDatabaseId,
            DbTemplateDelimiterCode: args.dbTemplateDelimiterCode,
            DatabaseOnDelimitersPublicationAddress: args.databaseOnDelimitersPublicationAddress
        }).then(_responseDto => true);
    }

    async function deleteDelimiterSourceAccountDatabase(requestKind: RequestKind, args: DeleteDelimiterSourceAccountDatabaseParams): Promise<boolean> {
        return getDelimiterSourceAccountDatabasesApiProxy().deleteDelimiterSourceAccountDatabase(requestKind, {
            accountDatabaseId: args.accountDatabaseId,
            dbTemplateDelimiterCode: args.dbTemplateDelimiterCode
        }).then(_responseDto => true);
    }

    async function searchAccountDatabases(requestKind: RequestKind, args: SearchAccountDatabasesParams): Promise<Array<SearchAccountDatabaseDataModel>> {
        return getDelimiterSourceAccountDatabasesApiProxy().searchAccountDatabases(requestKind, {
            searchLine: args.searchLine
        }).then(responseDto => responseDto.map(item => searchAccountDatabaseDataModelMapFrom(item)));
    }

    async function getDbTemplateDelimiters(requestKind: RequestKind): Promise<Array<KeyValueDataModel<string, string>>> {
        return getDelimiterSourceAccountDatabasesApiProxy().getDbTemplateDelimiters(requestKind).then(responseDto => responseDto.map(item => keyValueDataModelMapFrom(item)));
    }

    return {
        receiveDelimiterSourceAccountDatabases,
        updateDelimiterSourceAccountDatabase,
        insertDelimiterSourceAccountDatabase,
        deleteDelimiterSourceAccountDatabase,
        searchAccountDatabases,
        getDbTemplateDelimiters
    };
})();