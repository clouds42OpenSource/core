/**
 * Параметры для обновления записи материнской базы разделителей
 */
export type UpdateDelimiterSourceAccountDatabaseParams = {
    /**
     * Id информационной базы
     */
    accountDatabaseId: string;
    /**
     * Код конфигурации базы на разделителях
     */
    dbTemplateDelimiterCode: string;
    /**
     * Адрес публикации базы на разделителях
     */
    databaseOnDelimitersPublicationAddress: string;
};