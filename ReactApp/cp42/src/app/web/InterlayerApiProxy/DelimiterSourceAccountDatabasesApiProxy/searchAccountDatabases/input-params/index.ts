/**
 * Запрос на поиск базы
 */
export type SearchAccountDatabasesParams = {
    /**
     * Строка поиска
     */
    searchLine: string;
};