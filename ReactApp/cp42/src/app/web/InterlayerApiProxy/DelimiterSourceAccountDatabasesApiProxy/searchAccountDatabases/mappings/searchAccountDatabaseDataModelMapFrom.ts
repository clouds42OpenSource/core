import { SearchAccountDatabaseResponseDto } from 'app/web/api/DelimiterSourceAccountDatabasesProxy/response-dto/SearchAccountDatabaseResponseDto';
import { SearchAccountDatabaseDataModel } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy/searchAccountDatabases/data-models';

/**
 * Mapping модели информации о найденной информационной базы
 * @param value Модель информации о найденной информационной базы
 */
export function searchAccountDatabaseDataModelMapFrom(value: SearchAccountDatabaseResponseDto): SearchAccountDatabaseDataModel {
    return {
        accountDatabaseId: value.accountDatabaseId,
        accountDatabaseCaption: value.accountDatabaseCaption,
        accountDatabaseV82Name: value.accountDatabaseV82Name,
    };
}