import { IoCContainer } from 'app/IoCСontainer';
import { TasksPageTabVisibilityDataModel } from 'app/web/InterlayerApiProxy/TasksPageApiProxy/receiveTasksPage/data-models';
import { RequestKind } from 'core/requestSender/enums';

export const TasksPageApiProxy = (() => {
    function getTasksPageApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getTasksPageProxy();
    }

    async function receiveTasksPage(requestKind: RequestKind): Promise<TasksPageTabVisibilityDataModel> {
        return getTasksPageApiProxy().receiveTasksPage(requestKind)
            .then(responseDto => responseDto);
    }

    return {
        receiveTasksPage
    };
})();