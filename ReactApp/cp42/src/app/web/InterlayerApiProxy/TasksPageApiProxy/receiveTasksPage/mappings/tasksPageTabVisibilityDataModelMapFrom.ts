import { TasksPageTabVisibilityDataModel } from 'app/web/InterlayerApiProxy/TasksPageApiProxy/receiveTasksPage/data-models';
import { TasksPageDataResponseDto } from 'app/web/api/TasksPageProxy/response-dto/TasksPageDataResponseDto';

/**
 * Mapping модели данных по странице задач
 * @param value Модель данных по странице задач
 */
export function tasksPageTabVisibilityDataModelMapFrom(value: TasksPageDataResponseDto): TasksPageTabVisibilityDataModel {
    return {
        isAddTaskButtonVisible: value.isAddTaskButtonVisible,
        isControlTaskTabVisible: value.isControlTaskTabVisible,
        isWorkerAvailableTasksBagsTabVisible: value.isWorkerAvailableTasksBagsTabVisible,
        canCancelCoreWorkerTasksQueue: value.canCancelCoreWorkerTasksQueue
    };
}