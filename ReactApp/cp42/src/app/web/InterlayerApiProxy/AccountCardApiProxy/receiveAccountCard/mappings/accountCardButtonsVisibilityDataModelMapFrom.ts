import { AccountCardButtonsVisibilityResponseDto } from 'app/web/api/AccountCardProxy/response-dto/AccountCardButtonsVisibilityResponseDto';
import { AccountCardButtonsVisibilityDataModel } from '../data-models';

/**
 * Mapping модели о видемости кнопок для карточки аккаунта
 * @param value Модель о видемости кнопок карточки аккаунта
 */
export function accountCardButtonsVisibilityDataModelMapFrom(value: AccountCardButtonsVisibilityResponseDto): AccountCardButtonsVisibilityDataModel {
    return {
        isGotoToAccountButtonVisible: value.isGotoToAccountButtonVisible
    };
}