import { AccountCardTabVisibilityResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { AccountCardTabVisibilityDataModel } from '../data-models';

/**
 * Mapping модели о видемости табов карточки аккаунта
 * @param value Модель о видемости табов карточки аккаунта
 */
export function accountCardTabVisibilityDataModelMapFrom(value: AccountCardTabVisibilityResponseDto): AccountCardTabVisibilityDataModel {
    return {
        isAccountDetailsTabVisible: value.isAccountDetailsTabVisible,
        isMigrateDatabaseTabVisible: value.isMigrateDatabaseTabVisible,
        isChangeSegmentTabVisible: value.isChangeSegmentTabVisible

    };
}