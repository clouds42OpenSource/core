/**
 * Информация о акккаунте
 */
export type AccountCardAccountInfoDataModel = {
    /**
     *  ID аккаунта
     */
    id: string;
    /**
     * ID сегмента
     */
    segmentID?: string;
    /**
     * Название аккаунта
     */
    accountCaption: string;
    /**
     * ИНН
     */
    inn: string;
    /**
     * Дата регистрации
     */
    registrationDate?: Date;
    /**
     * Источник, откуда пришёл пользователь
     */
    userSource?: string;
    /**
     * Наименование денежной еденицы относительно локали
     */
    localeCurrency: string;
    /**
     * ID локали
     */
    localeId: string;
    /**
     * Описание
     */
    description: string;
    /**
     * Признак что аккаунт ВИП
     */
    isVip: boolean;
    /**
     * Номер аккаунта
     */
    indexNumber: number;
    /**
     * Ссылка на склеп
     */
    cloudStorageWebLink: string;
    /**
     * ID поставщика
     */
    supplerId?: string;
    /** Волна деплоя */
    deployment: number | null;
}