import { DateUtility } from 'app/utils';
import { AccountCardCommonDataResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { AccountCardCommonDataDataModel } from '../data-models';

/**
 * Mapping модели общей информации для карточки
 * @param value Модель общей информации для карточки
 */
export function accountCardCommonDataDataModelMapFrom(value: AccountCardCommonDataResponseDto): AccountCardCommonDataDataModel {
    return {
        accountSaleManagerCaption: value.accountSaleManagerCaption,
        accountType: value.accountType,
        balance: value.balance,
        emails: value.emails,
        segmentName: value.segmentName,
        serviceTotalSum: value.serviceTotalSum,
        usedSizeOnDisk: value.usedSizeOnDisk,
        lastActivity: value.lastActivity ? DateUtility.convertToDate(value.lastActivity) : undefined
    };
}