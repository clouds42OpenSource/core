/**
 * Общая информация для карточки аккаунта
 */
export type AccountCardCommonDataDataModel = {
    /**
     * Используемое место на диске аккаунта
     */
    usedSizeOnDisk?: number;
    /**
     * Полная сумма стоимости сервисов аккаунта
     */
    serviceTotalSum: number;
    /**
     * Тип аккаунта
     */
    accountType: string;
    /**
     * Название сейл менеджера
     */
    accountSaleManagerCaption: string;
    /**
     * Баланс аккаунта
     */
    balance: number;
    /**
     * Последняя активность
     */
    lastActivity?: Date
    /**
     * Имя сегмента
     */
    segmentName: string;
    /**
     * Письма для рассылок
     */
    emails: string[];
}