import { AccountCardSegmentInfoItemResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { AccountCardSegmentInfoItemDataModel } from '../data-models';

/**
 * Mapping модели о сегменте
 * @param value Модель о сегменте
 */
export function accountCardSegmentInfoItemDataModelMapFrom(value: AccountCardSegmentInfoItemResponseDto): AccountCardSegmentInfoItemDataModel {
    return {
        segmentId: value.key,
        segmentText: (value.value ?? '').trim()
    };
}