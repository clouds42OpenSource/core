/**
 * Информация о сегменте
 */
export type AccountCardSegmentInfoItemDataModel = {
    /**
     * ID сегмента
     */
    segmentId: string;
    /**
     * Название сегмента
     */
    segmentText: string;
}