import { AccountCardLocaleInfoItemResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { AccountCardLocaleInfoItemDataModel } from '../data-models';

/**
 * Mapping модели о локали
 * @param value Модель о локали
 */
export function accountCardLocaleInfoItemDataModelMapFrom(value: AccountCardLocaleInfoItemResponseDto): AccountCardLocaleInfoItemDataModel {
    return {
        localeId: value.key,
        localeText: (value.value ?? '').trim()
    };
}