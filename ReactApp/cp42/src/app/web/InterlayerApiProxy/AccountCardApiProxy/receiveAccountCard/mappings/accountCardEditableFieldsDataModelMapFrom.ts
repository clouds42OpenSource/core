import { AccountCardEditableFieldsResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { AccountCardEditableFieldsDataModel } from '../data-models';

/**
 * Mapping модели список полей которые можно редактировать
 * @param value Модель списка полей которые можно редактировать
 */
export function accountCardEditableFieldsDataModelMapFrom(value: AccountCardEditableFieldsResponseDto): AccountCardEditableFieldsDataModel {
    return {
        isAccountCaptionEditable: value.isAccountCaptionEditable,
        isInnEditable: value.isInnEditable,
        isLocaleEditable: value.isLocaleEditable
    };
}