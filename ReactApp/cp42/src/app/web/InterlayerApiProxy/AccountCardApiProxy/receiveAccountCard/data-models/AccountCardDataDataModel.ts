import {
    AccountCardAccountInfoDataModel,
    AccountCardButtonsVisibilityDataModel,
    AccountCardCommonDataDataModel,
    AccountCardEditableFieldsDataModel,
    AccountCardLocaleInfoItemDataModel,
    AccountCardSegmentInfoItemDataModel,
    AccountCardTabVisibilityDataModel,
    AccountCardVisibleFieldsDataModel
} from './index';

/**
 * Данные карточки аккаунта
 */
export type AccountCardDataDataModel = {
    /**
     * Информация о видимости кнопок для карточки аккаунта
     */
    buttonsVisibility: AccountCardButtonsVisibilityDataModel;
    /**
     * Общая информация о аккаунте
     */
    commonData: AccountCardCommonDataDataModel;
    /**
     * Информация о аккаунте
     */
    accountInfo: AccountCardAccountInfoDataModel;
    /**
     * Массив доступных локалей
     */
    locales: AccountCardLocaleInfoItemDataModel[];
    /**
     * Массив доступных сегментов
     */
    segments: AccountCardSegmentInfoItemDataModel[];
    /**
     * Список полей которые настраеваемы как видимы или нет
     */
    visibleFields: AccountCardVisibleFieldsDataModel;
    /**
     * Список полей которые настраеваемы как редактируемые или нет
     */
    editableFields: AccountCardEditableFieldsDataModel;
    /**
     * Содержит информацию о видимости табов карточки аккаунта
     */
    tabVisibility: AccountCardTabVisibilityDataModel;
};