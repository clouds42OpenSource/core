/**
 * Содержит список полей которые видимы или нет
 */
export type AccountCardVisibleFieldsDataModel = {
    /**
     * Можно ли показывать полную информацио о аккаунте или нет
     */
    canShowFullInfo: boolean;
    /**
     * Можно ли показывать информацию о сейл менеджере или нет
     */
    canShowSaleManagerInfo: boolean;
}