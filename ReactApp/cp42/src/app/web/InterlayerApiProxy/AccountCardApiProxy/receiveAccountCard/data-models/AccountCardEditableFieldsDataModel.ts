/**
 * Содержит список полей которые можно редактировать или нет
 */
export type AccountCardEditableFieldsDataModel = {
    /**
     * Можно ли редактировать название аккаунта
     */
    isAccountCaptionEditable: boolean;
    /**
     * Можно ли редактировать ИНН
     */
    isInnEditable: boolean;
    /**
     * Можно ли редактировать локаль
     */
    isLocaleEditable: boolean;
}