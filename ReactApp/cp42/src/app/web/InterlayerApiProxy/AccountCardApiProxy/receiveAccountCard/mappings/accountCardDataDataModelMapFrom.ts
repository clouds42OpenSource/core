import {
    accountCardAccountInfoDataModelMapFrom,
    accountCardCommonDataDataModelMapFrom,
    accountCardEditableFieldsDataModelMapFrom,
    accountCardLocaleInfoItemDataModelMapFrom,
    accountCardSegmentInfoItemDataModelMapFrom,
    accountCardTabVisibilityDataModelMapFrom,
    accountCardVisibleFieldsDataModelMapFrom
} from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard/mappings';

import { accountCardButtonsVisibilityDataModelMapFrom } from 'app/web/InterlayerApiProxy/AccountCardApiProxy/receiveAccountCard/mappings/accountCardButtonsVisibilityDataModelMapFrom';
import { AccountCardDataResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { AccountCardDataDataModel } from '../data-models';

/**
 * Mapping модели общей информации для карточки
 * @param value Модель общей информации для карточки
 */
export function accountCardDataDataModelMapFrom(value: AccountCardDataResponseDto): AccountCardDataDataModel {
    return {
        buttonsVisibility: accountCardButtonsVisibilityDataModelMapFrom(value.buttonsVisibility),
        accountInfo: accountCardAccountInfoDataModelMapFrom(value.accountInfo),
        commonData: accountCardCommonDataDataModelMapFrom(value.commonData),
        editableFields: accountCardEditableFieldsDataModelMapFrom(value.editableFields),
        locales: value.locales.map(item => accountCardLocaleInfoItemDataModelMapFrom(item)),
        segments: value.segments ? value.segments.map(item => accountCardSegmentInfoItemDataModelMapFrom(item)) : [],
        tabVisibility: accountCardTabVisibilityDataModelMapFrom(value.tabVisibility),
        visibleFields: accountCardVisibleFieldsDataModelMapFrom(value.visibleFields),
    };
}