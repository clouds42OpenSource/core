/**
 * Содержит информацию о видемости табов карточки аккаунта
 */
export type AccountCardTabVisibilityDataModel = {
    /**
     *  Можно ли показывать таб по общей информации аккаунта
     */
    isAccountDetailsTabVisible: boolean;
    /**
     * Можно ли показывать таб по миграции баз по хранилищам
     */
    isMigrateDatabaseTabVisible: boolean;
    /**
     * Можно ли показывать таб по смене сегмента или нет
     */
    isChangeSegmentTabVisible: boolean;
};