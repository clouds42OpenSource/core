/**
 * Информация о локали
 */
export type AccountCardLocaleInfoItemDataModel = {
    /**
     * ID локали
     */
    localeId: string;
    /**
     * Название локали
     */
    localeText: string;
}