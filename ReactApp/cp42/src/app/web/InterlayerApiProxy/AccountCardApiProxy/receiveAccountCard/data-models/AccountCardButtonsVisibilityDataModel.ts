export type AccountCardButtonsVisibilityDataModel = {
    isGotoToAccountButtonVisible: boolean;
}