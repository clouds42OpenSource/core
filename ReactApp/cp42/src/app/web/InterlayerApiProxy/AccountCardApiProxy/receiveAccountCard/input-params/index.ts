/**
 * Параметры для получения карточки аккаунта
 */
export type ReceiveAccountCardParams = {
    /**
     * Номер аккаунта, для которого получить карточку
     */
    accountNumber: number;
}