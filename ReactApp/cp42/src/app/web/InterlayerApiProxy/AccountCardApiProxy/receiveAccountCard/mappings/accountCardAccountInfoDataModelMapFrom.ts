import { DateUtility } from 'app/utils';
import { AccountCardAccountInfoResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { AccountCardAccountInfoDataModel } from '../data-models';

/**
 * Mapping модели информации о акккаунте
 * @param value Модель информации о акккаунте
 */
export function accountCardAccountInfoDataModelMapFrom(value: AccountCardAccountInfoResponseDto): AccountCardAccountInfoDataModel {
    return {
        accountCaption: value.accountCaption,
        description: value.accountDescription,
        id: value.accountId,
        indexNumber: value.indexNumber,
        inn: value.accountInn,
        isVip: value.isVip,
        localeCurrency: value.localeCurrency,
        localeId: value.localeId,
        segmentID: value.segmentID,
        registrationDate: value.registrationDate ? DateUtility.convertToDate(value.registrationDate) : undefined,
        userSource: value.userSource ?? undefined,
        cloudStorageWebLink: value.cloudStorageWebLink,
        supplerId: value.supplierId,
        deployment: value.deployment
    };
}