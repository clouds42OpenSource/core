import { AccountCardVisibleFieldsResponseDto } from 'app/web/api/AccountCardProxy/response-dto';
import { AccountCardVisibleFieldsDataModel } from '../data-models';

/**
 * Mapping модели списка полей которые видимы
 * @param value Модель списка полей которые видимы
 */
export function accountCardVisibleFieldsDataModelMapFrom(value: AccountCardVisibleFieldsResponseDto): AccountCardVisibleFieldsDataModel {
    return {
        canShowFullInfo: value.canShowFullInfo,
        canShowSaleManagerInfo: value.canShowSaleManagerInfo
    };
}