import { IoCContainer } from 'app/IoCСontainer';
import { ChangeAccountSegmentThunkParams } from 'app/modules/accountCard/store/reducers/changeAccountSegmentReducer/params';
import { UpdateAccountCardParams } from 'app/modules/accountCard/store/reducers/updateAccountCardReducer/params';
import { RequestKind } from 'core/requestSender/enums';
import { AccountCardDataDataModel, accountCardDataDataModelMapFrom, ReceiveAccountCardParams } from './receiveAccountCard';

export const AccountCardApiProxy = (() => {
    function getAccountCardApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getAccountCardApiProxy();
    }

    async function receiveAccountCard(requestKind: RequestKind, args: ReceiveAccountCardParams): Promise<AccountCardDataDataModel> {
        return getAccountCardApiProxy().receiveAccountCard(requestKind, {
            accountNumber: args.accountNumber
        }).then(responseDto => {
            return accountCardDataDataModelMapFrom(responseDto);
        });
    }

    function updateAccountCard(requestKind: RequestKind, args: UpdateAccountCardParams): Promise<void> {
        return getAccountCardApiProxy().updateAccountCard(requestKind, {
            AccountId: args.accountId,
            AccountCaption: args.accountCaption,
            AccountInn: args.inn,
            LocaleId: args.localeId,
            AccountDescription: args.description,
            IsVip: args.isVip,
            Emails: args.emails,
            Deployment: args.deployment || null
        });
    }

    function changeAccountSegment(requestKind: RequestKind, args: ChangeAccountSegmentThunkParams): Promise<void> {
        return getAccountCardApiProxy().changeAccountSegment(requestKind, {
            accountId: args.accountId,
            segmentId: args.segmentId
        });
    }

    return {
        receiveAccountCard,
        updateAccountCard,
        changeAccountSegment
    };
})();