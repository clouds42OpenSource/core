import { IoCContainer } from 'app/IoCСontainer';
import { selectDataResultCommonDataModelMapFrom } from 'app/web/common/mappings';
import { RequestKind } from 'core/requestSender/enums';
import {
    AccountDatabaseBackupArmPaginationDataModel,
    accountDatabaseBackupArmPaginationDataModelMapFrom,
    accountDatabaseBackupHistoryPeriodDataModelMapFrom,
    GetAccountDatabaseBackupHistoriesParams
} from './getAccountDatabaseBackupHistoriesPagination';

/**
 * Апи прокси истории бэкапов баз аккаунтов
 */
export const AccountDatabaseBackupArmApiProxy = (() => {
    function getAccountDatabaseBackupArmApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getAccountDatabaseBackupArmProxy();
    }

    /**
     * Получить список историй бэкапов баз аккаунтов с пагинацией для таблиц Арм бэкапирования
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getAccountDatabaseBackupHistoriesWithPagination(requestKind: RequestKind, args: GetAccountDatabaseBackupHistoriesParams): Promise<AccountDatabaseBackupArmPaginationDataModel> {
        return getAccountDatabaseBackupArmApiProxy().getAccountDatabaseBackupHistoriesWithPagination(
            requestKind,
            {
                dangerTablePage: args.dangerTablePage,
                warningTablePage: args.warningTablePage,
                searchQuery: args.searchQuery
            }
        ).then(responseDto => ({
            dangerTable: selectDataResultCommonDataModelMapFrom(responseDto.DangerTable, accountDatabaseBackupArmPaginationDataModelMapFrom),
            warningTable: selectDataResultCommonDataModelMapFrom(responseDto.WarningTable, accountDatabaseBackupArmPaginationDataModelMapFrom),
            dangerTablePeriod: accountDatabaseBackupHistoryPeriodDataModelMapFrom(responseDto.DangerTablePeriod),
            warningTablePeriod: accountDatabaseBackupHistoryPeriodDataModelMapFrom(responseDto.WarningTablePeriod)
        }));
    }

    return {
        getAccountDatabaseBackupHistoriesWithPagination
    };
})();