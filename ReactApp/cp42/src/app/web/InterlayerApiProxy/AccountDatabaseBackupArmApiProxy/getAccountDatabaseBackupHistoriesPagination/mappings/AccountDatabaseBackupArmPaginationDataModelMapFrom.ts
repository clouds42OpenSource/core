import { DateUtility } from 'app/utils';
import { AccountDatabaseBackupHistoryResponseDto } from 'app/web/api/AccountDatabaseBackupArmProxy/response-dto';
import { AccountDatabaseBackupHistoryDataModel } from '../data-models';

/**
 * Mapping модели истории бэкапа базы аккаунта
 * @param value Модель истории базы аккаунта
 */
export function accountDatabaseBackupArmPaginationDataModelMapFrom(value: AccountDatabaseBackupHistoryResponseDto): AccountDatabaseBackupHistoryDataModel {
    return {
        accountId: value.AccountId,
        accountIndexNumber: value.AccountIndexNumber,
        accountDatabaseBackupHistoryCreateDateTime: value.AccountDatabaseBackupHistoryCreateDateTime !== null ? DateUtility.convertToDate(value.AccountDatabaseBackupHistoryCreateDateTime) : null,
        accountDatabaseBackupHistoryId: value.AccountDatabaseBackupHistoryId,
        accountDatabaseId: value.AccountDatabaseId,
        v82Name: value.V82Name,
        accountDatabaseBackupHistoryMessage: value.AccountDatabaseBackupHistoryMessage
    };
}