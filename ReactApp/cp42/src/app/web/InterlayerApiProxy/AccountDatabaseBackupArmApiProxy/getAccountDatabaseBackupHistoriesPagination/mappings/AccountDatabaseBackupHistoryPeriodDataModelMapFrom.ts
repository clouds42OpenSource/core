import { AccountDatabaseBackupHistoryPeriodResponseDto } from 'app/web/api/AccountDatabaseBackupArmProxy/response-dto';
import { AccountDatabaseBackupHistoryPeriodDataModel } from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy/getAccountDatabaseBackupHistoriesPagination';

/**
 * Функция для маппинга response dto с data model
 * @param value Response dto периода от и до для историй бэкапов баз аккаунта
 * @returns Data model периода от и до для историй бэкапов баз аккаунта
 */
export function accountDatabaseBackupHistoryPeriodDataModelMapFrom(value: AccountDatabaseBackupHistoryPeriodResponseDto): AccountDatabaseBackupHistoryPeriodDataModel {
    return {
        fromDays: value.FromDays,
        toDays: value.ToDays ?? undefined
    };
}