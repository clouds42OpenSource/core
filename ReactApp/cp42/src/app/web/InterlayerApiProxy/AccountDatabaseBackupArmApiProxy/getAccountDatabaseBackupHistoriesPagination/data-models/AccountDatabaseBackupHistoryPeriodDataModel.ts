/**
 * Модель периода от и до для историй бэкапов баз аккаунта
 */
export type AccountDatabaseBackupHistoryPeriodDataModel = {
    /**
     * Период от
     */
    fromDays: number;
    /**
     * Период до
     */
    toDays?: number;
}