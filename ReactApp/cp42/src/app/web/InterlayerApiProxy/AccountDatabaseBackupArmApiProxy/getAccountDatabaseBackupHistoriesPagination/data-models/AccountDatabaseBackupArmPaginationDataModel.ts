import { SelectDataResultCommonDataModel } from 'app/web/common/data-models';
import { AccountDatabaseBackupHistoryPeriodDataModel } from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy/getAccountDatabaseBackupHistoriesPagination';
import { AccountDatabaseBackupHistoryDataModel } from './AccountDatabaseBackupHistoryDataModel';

/**
 * обьект историй бэкапов баз аккаунтов с пагинацией
 */
export type AccountDatabaseBackupArmPaginationDataModel = {
    /**
     * Список историй старых бэкапов баз аккаунтов с пагинацией
     */
    dangerTable: SelectDataResultCommonDataModel<AccountDatabaseBackupHistoryDataModel>;
    /**
     * Список историй новых бэкапов баз аккаунтов с пагинацией
     */
    warningTable: SelectDataResultCommonDataModel<AccountDatabaseBackupHistoryDataModel>;
    /**
     * Период старых историй бэкапов баз аккаунтов
     */
    dangerTablePeriod: AccountDatabaseBackupHistoryPeriodDataModel;
    /**
     * Период новых историй бэкапов баз аккаунтов
     */
    warningTablePeriod: AccountDatabaseBackupHistoryPeriodDataModel;
};