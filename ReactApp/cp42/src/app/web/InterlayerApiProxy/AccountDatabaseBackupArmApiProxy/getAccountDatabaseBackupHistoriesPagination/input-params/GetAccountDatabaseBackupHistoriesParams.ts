/**
 * Параметры на получения историй баз аккаунтов
 */
export type GetAccountDatabaseBackupHistoriesParams = {
    /**
     * номер страницы DangerTable
     */
    dangerTablePage: number;
    /**
     * номер страницы WarningTable
     */
    warningTablePage: number;
    /**
     * фильтр поиска
     */
    searchQuery: string;
};