import { Nullable } from 'app/common/types';

/**
 * обьект истории баз аккаунта
 */
export type AccountDatabaseBackupHistoryDataModel = {
    /**
     * ID истории бэкапа базы аккаунта
     */
    accountDatabaseBackupHistoryId: string;
    /**
     * Дата создания записи
     */
    accountDatabaseBackupHistoryCreateDateTime: Nullable<Date>;
    /**
     * Коментарий архивации
     */
    accountDatabaseBackupHistoryMessage: string;
    /**
     * Номер базы
     */
    v82Name: string;
    /**
     * ID инф. базы
     */
    accountDatabaseId: string;
    /**
     * Номер аккаунта
     */
    accountIndexNumber: number;
    /**
     * ID аккаунта
     */
    accountId: string;
}