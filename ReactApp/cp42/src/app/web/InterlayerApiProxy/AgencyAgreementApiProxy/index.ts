import { IoCContainer } from 'app/IoCСontainer';
import { AddAgencyAgreementResultDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/addAgencyAgreement/data-models/AddAgencyAgreementResultDataModel';
import { AddAgencyAgreementParams } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/addAgencyAgreement/input-params';
import { addAgencyAgreementRequestDtoMapFrom, addAgencyAgreementResultDataModelMapFrom } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/addAgencyAgreement/mappings';
import { OpenAgencyAgreementParams } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/openAgencyAgreement/input-params';
import { agencyAgreementItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements';
import { GetAgencyAgreementResultDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/data-models/GetAgencyAgreementResultDataModel';
import { ReceiveAgencyAgreementsParams } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/input-params';
import { RequestKind } from 'core/requestSender/enums';

export const AgencyAgreementApiProxy = (() => {
    function getAgencyAgreementApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getAgencyAgreementProxy();
    }

    async function receiveAgencyAgreements(requestKind: RequestKind, args: ReceiveAgencyAgreementsParams): Promise<GetAgencyAgreementResultDataModel> {
        return getAgencyAgreementApiProxy().receiveAgencyAgreements(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    name: args.filter.agencyAgreementName
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => ({
            records: responseDto.records.map(record => agencyAgreementItemDataModelMapFrom(record)),
            metadata: responseDto.metadata
        }));
    }

    async function addAgencyAgreement(requestKind: RequestKind, args: AddAgencyAgreementParams): Promise<AddAgencyAgreementResultDataModel> {
        return getAgencyAgreementApiProxy().addAgencyAgreement(requestKind, {
            ...addAgencyAgreementRequestDtoMapFrom(args)
        }).then(responseDto => addAgencyAgreementResultDataModelMapFrom(responseDto));
    }

    function openAgencyAgreement(args: OpenAgencyAgreementParams): void {
        getAgencyAgreementApiProxy().openAgencyAgreement(args);
    }

    return {
        receiveAgencyAgreements,
        addAgencyAgreement,
        openAgencyAgreement
    };
})();