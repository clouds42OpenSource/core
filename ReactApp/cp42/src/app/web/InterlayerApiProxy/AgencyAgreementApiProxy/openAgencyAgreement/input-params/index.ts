/**
 * Параметры на открытие агентского соглашения
 */
export type OpenAgencyAgreementParams = {
    /**
     * ID агентского соглашения для открытия
     */
    agencyAgreementId: string;
};