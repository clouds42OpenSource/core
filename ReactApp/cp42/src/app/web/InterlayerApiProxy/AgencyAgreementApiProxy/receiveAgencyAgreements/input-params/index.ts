import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Название агентского соглашения
     */
    agencyAgreementName: string;
};

/**
 * Параметры для загрузки конфигураций 1С
 */
export type ReceiveAgencyAgreementsParams = SelectDataCommonDataModel<Filter>;