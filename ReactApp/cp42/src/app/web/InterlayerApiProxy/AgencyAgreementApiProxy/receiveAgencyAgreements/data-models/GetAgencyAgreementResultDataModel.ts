import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { AgencyAgreementItemDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/data-models/AgencyAgreementItemDataModel';

/**
 * Модель ответа на получение списка агентских соглашений
 */
export type GetAgencyAgreementResultDataModel = SelectDataResultMetadataModel<AgencyAgreementItemDataModel>;