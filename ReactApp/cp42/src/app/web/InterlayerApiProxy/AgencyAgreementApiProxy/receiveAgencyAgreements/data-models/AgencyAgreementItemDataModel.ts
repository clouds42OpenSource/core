/**
 *  Модель свойств элемента агентского соглашения
 */
export type AgencyAgreementItemDataModel = {
    /**
     * ID агентского соглашения
     */
    agencyAgreementId: string;
    /**
     * Название агентского соглашения
     */
    agencyAgreementName: string;
    /**
     * Печатная форма
     */
    printedHtmlFormId: string;
    /**
     * Дата вступления в силу
     */
    effectiveDate: Date;
    /**
     * Дата создания
     */
    creationDate: Date;
    /**
     * Вознаграждение за сервис "Аренда 1С" (проценты)
     */
    rent1CRewardPercent: number;
    /**
     * Вознаграждение за сервис "Мой диск" (проценты)
     */
    myDiskRewardPercent: number;
    /**
     * Вознаграждение за сервис разработчика (проценты)
     */
    serviceOwnerRewardPercent: number;
    /**
     * Вознаграждение за сервис "Аренда 1С" для вип аккаунтов (проценты)
     */
    rent1CRewardPercentForVipAccounts: number;
    /**
     * Вознаграждение за сервис "Мой диск" для вип аккаунтов (проценты)
     */
    myDiskRewardPercentForVipAccounts: number;
    /**
     * Вознаграждение за сервис разработчика для вип аккаунтов (проценты)
     */
    serviceOwnerRewardPercentForVipAccounts: number;
};