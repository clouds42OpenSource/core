import { DateUtility } from 'app/utils';
import { AgencyAgreementItemResponseDto } from 'app/web/api/AgencyAgreementProxy/response-dto';
import { AgencyAgreementItemDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/receiveAgencyAgreements/data-models/AgencyAgreementItemDataModel';

/**
 * Mapping модели агентского соглашения
 * @param value ответ модели агентского соглашения
 */
export function agencyAgreementItemDataModelMapFrom(value: AgencyAgreementItemResponseDto): AgencyAgreementItemDataModel {
    return {
        ...value,
        agencyAgreementId: value.id,
        agencyAgreementName: value.name,
        creationDate: DateUtility.convertToDate(value.creationDate),
        effectiveDate: DateUtility.convertToDate(value.effectiveDate),
    };
}