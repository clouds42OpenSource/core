import { AddAgencyAgreementRequestDto } from 'app/web/api/AgencyAgreementProxy/request-dto';
import { AddAgencyAgreementParams } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/addAgencyAgreement/input-params';

/**
 * Mapping модели агентского соглашения
 * @param value ответ модели агентского соглашения
 */
export function addAgencyAgreementRequestDtoMapFrom(value: AddAgencyAgreementParams): AddAgencyAgreementRequestDto {
    return {
        ...value,
        name: value.agencyAgreementName,
        effectiveDate: value.effectiveDate,
    };
}