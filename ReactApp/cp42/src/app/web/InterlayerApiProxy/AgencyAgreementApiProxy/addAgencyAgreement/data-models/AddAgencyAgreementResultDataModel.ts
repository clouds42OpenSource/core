/**
 * Модель ответа на добавление агентского соглашения
 */
export type AddAgencyAgreementResultDataModel = {
    /**
     * ID агентского соглашения.
     */
    agencyAgreementId: string;
};