import { AddAgencyAgreementResultResponseDto } from 'app/web/api/AgencyAgreementProxy/response-dto/AddAgencyAgreementResultResponseDto';
import { AddAgencyAgreementResultDataModel } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy/addAgencyAgreement/data-models/AddAgencyAgreementResultDataModel';

/**
 * Mapping модели результата добавления агентского соглашения
 * @param value ответ результата добавления агентского соглашения
 */
export function addAgencyAgreementResultDataModelMapFrom(value: AddAgencyAgreementResultResponseDto): AddAgencyAgreementResultDataModel {
    return {
        agencyAgreementId: value.id
    };
}