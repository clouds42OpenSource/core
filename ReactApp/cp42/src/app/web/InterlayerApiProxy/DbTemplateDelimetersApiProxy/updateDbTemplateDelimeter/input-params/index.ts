import { DbTemplateDelimeterItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters';

/**
 * Параметры для обновления шаблона материнской базы
 */
export type UpdateDbTemplateDelimeterParams = DbTemplateDelimeterItemDataModel;