import { IoCContainer } from 'app/IoCСontainer';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { AddDbTemplateDelimeterParams } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/addDbTemplateDelimeter/input-params';
import { DeleteDbTemplateDelimeterParams } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/deleteDbTemplateDelimeter/input-params';
import { dbTemplateDelimetersItemDataModelMapFrom, dbTemplateDelimetersItemDtoMapFrom } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters';
import { GetDbTemplateDelimetersResultDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters/data-models/GetDbTemplateDelimetersResultDataModel';
import { GetDbTemplateDelimetersParams } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters/input-params';
import { UpdateDbTemplateDelimeterParams } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/updateDbTemplateDelimeter/input-params';
import { RequestKind } from 'core/requestSender/enums';

export const DbTemplateDelimetersApiProxy = (() => {
    function getDbTemplateDelimetersApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getDbTemplateDelimetersProxy();
    }

    async function receiveDbTemplateDelimeters(requestKind: RequestKind, args: GetDbTemplateDelimetersParams): Promise<GetDbTemplateDelimetersResultDataModel> {
        return getDbTemplateDelimetersApiProxy().receiveDbTemplateDelimeters(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter,
            orderBy: args.orderBy
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, dbTemplateDelimetersItemDataModelMapFrom));
    }

    async function updateDbTemplateDelimeter(requestKind: RequestKind, args: UpdateDbTemplateDelimeterParams): Promise<void> {
        return getDbTemplateDelimetersApiProxy().updateDbTemplateDelimeter(requestKind, {
            ...dbTemplateDelimetersItemDtoMapFrom(args)
        });
    }

    async function addDbTemplateDelimeter(requestKind: RequestKind, args: AddDbTemplateDelimeterParams): Promise<void> {
        return getDbTemplateDelimetersApiProxy().addDbTemplateDelimeter(requestKind, {
            ...dbTemplateDelimetersItemDtoMapFrom(args)
        });
    }

    async function deleteDbTemplateDelimeter(requestKind: RequestKind, args: DeleteDbTemplateDelimeterParams): Promise<void> {
        try {
            const res = await getDbTemplateDelimetersApiProxy().deleteDbTemplateDelimeter(requestKind, {
                ConfigurationId: args.configurationId
            });

            // @ts-ignore
            if(res.success === false) {
                throw res;
            }

            return res;
        } catch (er){
            throw(er);
        }
    }
    return {
        receiveDbTemplateDelimeters,
        updateDbTemplateDelimeter,
        addDbTemplateDelimeter,
        deleteDbTemplateDelimeter
    };
})();