import { DbTemplateDelimeterItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters';

/**
 * Параметры для добавления шаблона материнской базы
 */
export type AddDbTemplateDelimeterParams = DbTemplateDelimeterItemDataModel;