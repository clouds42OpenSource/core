/**
 * Параметры для удаления шаблона материнской базы
 */
export type DeleteDbTemplateDelimeterParams = {
    /**
     * ID конфигурации для удаления базы на разделителях
     */
    configurationId: string;
};