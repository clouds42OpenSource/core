import { SelectDataCommonDataModel } from 'app/web/common/data-models';

export type GetDbTemplateDelimetersParams = SelectDataCommonDataModel<void>;