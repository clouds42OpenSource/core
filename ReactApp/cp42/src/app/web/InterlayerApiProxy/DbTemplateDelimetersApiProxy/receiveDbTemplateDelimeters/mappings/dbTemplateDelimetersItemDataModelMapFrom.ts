import { DbTemplateDelimeterItemDto } from 'app/web/api/DbTemplateDelimetersProxy/response-dto';
import { DbTemplateDelimeterItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters/data-models/DbTemplateDelimeterItemDataModel';

/**
 * Mapping модели шаблона базы на разделителях
 * @param value ответ модели шаблона базы на разделителях
 */
export function dbTemplateDelimetersItemDataModelMapFrom(value: DbTemplateDelimeterItemDto): DbTemplateDelimeterItemDataModel {
    return {
        configurationId: value.configurationId,
        name: value.name,
        configurationReleaseVersion: value.configurationReleaseVersion,
        shortName: value.shortName,
        minReleaseVersion: value.minReleaseVersion,
        demoDatabaseOnDelimitersPublicationAddress: value.demoDatabaseOnDelimitersPublicationAddress,
        templateId: value.templateId
    };
}