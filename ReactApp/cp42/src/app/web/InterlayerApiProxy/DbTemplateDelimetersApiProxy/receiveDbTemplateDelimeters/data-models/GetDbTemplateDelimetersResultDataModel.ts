import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { DbTemplateDelimeterItemDataModel } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy/receiveDbTemplateDelimeters/data-models/DbTemplateDelimeterItemDataModel';

/**
 * Модель ответа на получение списка шаблонов баз на разделителях
 */
export type GetDbTemplateDelimetersResultDataModel = SelectDataResultMetadataModel<DbTemplateDelimeterItemDataModel>;