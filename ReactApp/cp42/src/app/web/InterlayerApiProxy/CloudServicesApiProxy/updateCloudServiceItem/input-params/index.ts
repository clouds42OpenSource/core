/**
 * Параметры для обновления записи справочника CloudServices
 */
export type UpdateCloudServiceItemParams = {
    /**
     * ID CloudService
     */
    cloudServiceId: string;
    /**
     * Наименование службы
     */
    serviceCaption: string;
    /**
     * Новый JsonWebToken
     */
    jsonWebToken: string;
};