/**
 * Параметры для обновления записи справочника CloudServices
 */
export type AddCloudServiceParams = {
    /**
     * ID CloudService
     */
    cloudServiceId: string;
    /**
     * Наименование службы
     */
    serviceCaption: string;
    /**
     * Новый JsonWebToken
     */
    jsonWebToken: string;
};