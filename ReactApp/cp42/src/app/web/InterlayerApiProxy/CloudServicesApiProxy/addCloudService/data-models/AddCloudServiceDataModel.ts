/**
 * Модель ответа на добавление нового CloudService
 */
export type AddCloudServiceDataModel = {
    /**
     * Значение для поля Id в таблице CloudService
     */
    id: string;
};