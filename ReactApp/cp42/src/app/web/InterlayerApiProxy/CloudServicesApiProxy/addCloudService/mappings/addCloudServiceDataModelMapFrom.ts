import { AddCloudServiceResponseDto } from 'app/web/api/CloudServicesProxy/response-dto';
import { AddCloudServiceDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/addCloudService/data-models/AddCloudServiceDataModel';

/**
 * Выполнить маппинг модели данных по добавлению записи справочника CloudServices
 * @param value Модель данных по добавлению записи справочника CloudServices
 */
export function addCloudServiceDataModelMapFrom(value: AddCloudServiceResponseDto): AddCloudServiceDataModel | string {
    if ('message' in value) {
        return value.message as string;
    }

    return {
        id: value.Id
    };
}