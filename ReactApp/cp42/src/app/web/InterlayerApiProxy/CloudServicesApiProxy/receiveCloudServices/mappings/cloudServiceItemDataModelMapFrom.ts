import { CloudServiceItemResponseDto } from 'app/web/api/CloudServicesProxy/response-dto';
import { CloudServiceItemDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices/data-models';

/**
 * Выполнить маппинг модели данных по записи справочника CloudServices
 * @param value Модель данных по записи справочника CloudServices
 */
export function cloudServiceItemDataModelMapFrom(value: CloudServiceItemResponseDto): CloudServiceItemDataModel {
    return {
        id: value.id,
        cloudServiceId: value.cloudServiceId,
        serviceCaption: value.serviceCaption,
        jsonWebToken: value.jsonWebToken
    };
}