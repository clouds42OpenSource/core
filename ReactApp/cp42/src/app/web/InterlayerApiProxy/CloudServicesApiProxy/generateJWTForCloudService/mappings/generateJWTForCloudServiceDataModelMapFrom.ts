import { GenerateJWTForCloudServiceResponseDto } from 'app/web/api/CloudServicesProxy/response-dto';
import { GenerateJWTForCloudServiceDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/generateJWTForCloudService/data-models';

/**
 * Выполнить маппинг модели данных по записи справочника CloudServices
 * @param value Модель данных по записи справочника CloudServices
 */
export function generateJWTForCloudServiceDataModelMapFrom(value: GenerateJWTForCloudServiceResponseDto): GenerateJWTForCloudServiceDataModel {
    return {
        jsonWebToken: value.jsonWebToken
    };
}