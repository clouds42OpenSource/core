/**
 * Модель ответа при генерации нового JWT для записи CloudServices
 */
export type GenerateJWTForCloudServiceDataModel = {
    /**
     * Сгенерированный JsonWebToken
     */
    jsonWebToken: string;
}