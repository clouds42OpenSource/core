/**
 * Параметры для гекнерации JWT для записи справочника CloudServices
 */
export type GenerateJWTForCloudServiceParams = {
    /**
     * ID CloudServices, для которого нужно сгенерировать JWT
     */
    cloudServiceId: string;
}