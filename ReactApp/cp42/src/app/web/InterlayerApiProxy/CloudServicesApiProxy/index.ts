import { IoCContainer } from 'app/IoCСontainer';
import { AddCloudServiceParams } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/addCloudService';
import { AddCloudServiceDataModel } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/addCloudService/data-models/AddCloudServiceDataModel';
import { addCloudServiceDataModelMapFrom } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/addCloudService/mappings';
import { GenerateJWTForCloudServiceDataModel, generateJWTForCloudServiceDataModelMapFrom, GenerateJWTForCloudServiceParams } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/generateJWTForCloudService';
import { CloudServiceItemDataModel, cloudServiceItemDataModelMapFrom } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/receiveCloudServices';
import { UpdateCloudServiceItemParams } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy/updateCloudServiceItem';
import { RequestKind } from 'core/requestSender/enums';

export const CloudServicesApiProxy = (() => {
    function getCloudServicesApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getCloudServicesProxy();
    }

    async function receiveCloudServices(requestKind: RequestKind): Promise<CloudServiceItemDataModel[]> {
        return getCloudServicesApiProxy().receiveCloudServices(requestKind)
            .then(responseDto => {
                return responseDto.map(item => cloudServiceItemDataModelMapFrom(item));
            });
    }

    async function updateCloudServiceItem(requestKind: RequestKind, args: UpdateCloudServiceItemParams): Promise<boolean> {
        return getCloudServicesApiProxy().updateCloudServiceItem(requestKind, {
            cloudServiceId: args.cloudServiceId,
            serviceCaption: args.serviceCaption,
            jsonWebToken: args.jsonWebToken
        });
    }

    async function addCloudService(requestKind: RequestKind, args: AddCloudServiceParams): Promise<AddCloudServiceDataModel | string> {
        return getCloudServicesApiProxy().addCloudService(requestKind, {
            cloudServiceId: args.cloudServiceId,
            serviceCaption: args.serviceCaption,
            jsonWebToken: args.jsonWebToken
        }).then(responseDto => {
            return addCloudServiceDataModelMapFrom(responseDto);
        });
    }

    async function generateJWTForCloudService(requestKind: RequestKind, args: GenerateJWTForCloudServiceParams): Promise<GenerateJWTForCloudServiceDataModel> {
        return getCloudServicesApiProxy().generateJWTForCloudService(requestKind, {
            CloudServiceId: args.cloudServiceId
        }).then(responseDto => {
            return generateJWTForCloudServiceDataModelMapFrom(responseDto);
        });
    }

    return {
        receiveCloudServices,
        updateCloudServiceItem,
        addCloudService,
        generateJWTForCloudService
    };
})();