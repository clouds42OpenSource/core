import { DateUtility } from 'app/utils';
import { DbTemplateUpdatesResponseDto } from 'app/web/api/DbTemplateUpdateProxy/response-dto';
import { DbTemplateUpdatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/data-models';

/**
 * Функция для маппинга response dto с data model
 * @param value response dto обновлений шаблона
 * @returns Массив data model обновлений шаблона баз
 */
export function dbTemplateUpdatesMapFrom(value: Array<DbTemplateUpdatesResponseDto>): Array<DbTemplateUpdatesDataModel> {
    return value.map<DbTemplateUpdatesDataModel>(item => ({
        ...item,
        updateVersionDate: item.updateVersionDate != null ? DateUtility.convertToDate(item.updateVersionDate) : null,
    }));
}