import { DbTemplateUpdatesRequestDto } from 'app/web/api/DbTemplateUpdateProxy/request-dto';
import { DbTemplateUpdatesParams } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/input-params';

/**
 * Маппинг Params апи прокси с Request параметра апи
 * @param value Params апи прокси
 * @returns Request параметры апи
 */
export function dbTemplateUpdatesParamsMapFrom(value: DbTemplateUpdatesParams): DbTemplateUpdatesRequestDto {
    return {
        validateState: value.validateState,
        templateId: value.templateId
    };
}