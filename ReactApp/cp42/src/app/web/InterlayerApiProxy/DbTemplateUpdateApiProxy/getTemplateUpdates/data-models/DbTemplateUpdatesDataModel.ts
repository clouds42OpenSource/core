import { DbTemplateUpdateStateEnumType } from 'app/common/enums';
import { Nullable } from 'app/common/types';

/**
 * Модель для обновления шаблона баз
 */
export type DbTemplateUpdatesDataModel = {
    /**
     * ID Обновления шаблона баз
     */
    id: string;
    /**
     * Заголовок по умолчанию у шаблона базы
     */
    defaultCaption: string;
    /**
     * Версия обновления
     */
    updateVersion: string;
    /**
     * Дата обновления
     */
    updateVersionDate: Nullable<Date>;
    /**
     * Полный путь до обновленного шаблона
     */
    updateTemplatePath: string;
    /**
     * Статус проверки обновления на ошибки человеком
     */
    validateState: DbTemplateUpdateStateEnumType;
}