import { DbTemplateUpdateStateEnumType } from 'app/common/enums';

/**
 * Модель параметров для получения обновлений шаблона баз
 */
export type DbTemplateUpdatesParams = {
    /**
     * ID шаблона
     */
    templateId?: string;
    /**
     * Статус обновленного шаблона
     */
    validateState: DbTemplateUpdateStateEnumType;
}