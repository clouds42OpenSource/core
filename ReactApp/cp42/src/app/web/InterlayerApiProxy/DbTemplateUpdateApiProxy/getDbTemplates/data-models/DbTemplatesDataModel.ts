import { KeyValueDataModel } from 'app/web/common/data-models';

/**
 * Все шаблоны ввиде словаря key - ID шаблона, value - описание шаблона
 */
export type DbTemplatesDataModel = KeyValueDataModel<string, string>;