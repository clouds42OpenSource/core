import { DbTemplatesResponseDto } from 'app/web/api/DbTemplateUpdateProxy/response-dto';
import { keyValueDataModelMapFrom } from 'app/web/common/mappings';
import { DbTemplatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getDbTemplates/data-models';

/**
 * Маппинг Response Dto c Data model
 * @param value Response Dto всех шаблонов ввиде словаря
 * @returns Data model всех шаблонов ввиде словаря
 */
export function dbTemplatesDataModelMapFrom(value: Array<DbTemplatesResponseDto>): Array<DbTemplatesDataModel> {
    return value.map<DbTemplatesDataModel>(item => (keyValueDataModelMapFrom<string, string>(item)));
}