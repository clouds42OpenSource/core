import { IoCContainer } from 'app/IoCСontainer';
import { DateUtility } from 'app/utils';
import { DbTemplatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getDbTemplates/data-models';
import { dbTemplatesDataModelMapFrom } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getDbTemplates/mappings';
import { DbTemplateUpdatesDataModel } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/data-models';
import { DbTemplateUpdatesParams } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/input-params';
import { dbTemplateUpdatesParamsMapFrom } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy/getTemplateUpdates/mappings';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Апи прокси обновлений шаблона баз
 */
export const DbTemplateUpdateApiProxy = (() => {
    function getDbTemplateUpdateApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getDbTemplateUpdateProxy();
    }

    /**
     * Запрос на получение обновлений шаблона
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Массив моделей для обновления шаблона баз
     */
    async function getTemplateUpdates(requestKind: RequestKind, args: DbTemplateUpdatesParams): Promise<Array<DbTemplateUpdatesDataModel>> {
        return getDbTemplateUpdateApiProxy().getTemplateUpdates(requestKind, dbTemplateUpdatesParamsMapFrom(args))
            .then(responseDto => responseDto.map(item => ({ ...item, updateVersionDate: item.updateVersionDate != null ? DateUtility.convertToDate(item.updateVersionDate) : null })));
    }

    /**
     * Получить все шаблоны ввиде key - ID шаблона, value - описание шаблона
     * @param requestKind Вид запроса
     * @returns Все шаблоны ввиде словаря
     */
    async function getDbTemplates(requestKind: RequestKind): Promise<Array<DbTemplatesDataModel>> {
        return getDbTemplateUpdateApiProxy().getDbTemplates(requestKind)
            .then(responseDto => dbTemplatesDataModelMapFrom(responseDto));
    }

    /**
     * Принять обновления шаблона баз
     * @param requestKind Вид запроса
     * @param dbTemplateUpdateId ID обновления шаблона баз
     */
    async function applyUpdates(requestKind: RequestKind, dbTemplateUpdateId: string) {
        return getDbTemplateUpdateApiProxy().applyUpdates(requestKind, dbTemplateUpdateId);
    }

    /**
     * Отклонить обновления шаблона баз
     * @param requestKind Вид запроса
     * @param dbTemplateUpdateId ID обновления шаблона баз
     */
    async function rejectUpdates(requestKind: RequestKind, dbTemplateUpdateId: string) {
        return getDbTemplateUpdateApiProxy().rejectUpdates(requestKind, dbTemplateUpdateId);
    }

    /**
     * Удалить обновления шаблона баз
     * @param requestKind Вид запроса
     */
    async function deleteUpdates(requestKind: RequestKind,) {
        return getDbTemplateUpdateApiProxy().deleteUpdates(requestKind);
    }

    return {
        getTemplateUpdates,
        getDbTemplates,
        applyUpdates,
        rejectUpdates,
        deleteUpdates
    };
})();