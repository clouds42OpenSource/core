/**
 * Модель файла облака
 */
export type CloudFileDataModel = {
    /**
     * ID файла облака
     */
    id: string;
    /**
     * Хэш данные
     */
    base64: string;
    /**
     * Тип файла
     */
    contentType: string;
    /**
     * Имя файла
     */
    fileName: string;
    /**
     * Файл удален
     */
    isDeleted: boolean;
}