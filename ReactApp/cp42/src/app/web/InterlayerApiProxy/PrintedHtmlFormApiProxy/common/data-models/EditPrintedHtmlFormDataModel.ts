import { BasePrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';

/**
 * Модель для редактирования печатных форм HTML
 */
export type EditPrintedHtmlFormDataModel = BasePrintedHtmlFormDataModel & {
    /**
     * Id печатной формы
     */
    id?: string;
}