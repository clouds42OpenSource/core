import { CloudFileDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models/CloudFileDataModel';

/**
 * Базовый модель печатной формы
 */
export type BasePrintedHtmlFormDataModel = {
    /**
     * Название печатной формы Html
     */
    name: string;
    /**
     * Html разметка
     */
    htmlData: string;
    /**
     * Тип модели
     */
    modelType: string;
    /**
     *  Прикрепленные файлы
     */
    files?: CloudFileDataModel[];
    /**
     * Прикрепленные файлы
     */
    uploadedFiles?: File[];
}