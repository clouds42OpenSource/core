import { CreatePrintedHtmlFormRequestDto } from 'app/web/api/PrintedHtmlForm/request-dto';
import { CreatePrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/createPrintedHtmlForm/data-models';

/**
 * Маппинг Data model для создания печатных форм HTML на Response dto
 * @param value Data model для создания печатных форм HTML
 * @returns Response dto для создания печатных форм HTML
 */
export function createPrintedHtmlFormDataModelMapFrom(value: CreatePrintedHtmlFormDataModel): CreatePrintedHtmlFormRequestDto {
    return {
        htmlData: value.htmlData,
        modelType: value.modelType,
        name: value.name,
        uploadedFiles: value.uploadedFiles,
        files: value.files?.map(element => ({
            base64: element.base64,
            contentType: element.contentType,
            fileName: element.fileName,
            id: element.id,
            isDeleted: element.isDeleted
        }))
    };
}