import { BasePrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';

/**
 * Модель для создания печатных форм HTML
 */
export type CreatePrintedHtmlFormDataModel = BasePrintedHtmlFormDataModel;