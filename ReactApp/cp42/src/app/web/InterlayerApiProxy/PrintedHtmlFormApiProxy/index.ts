import { IoCContainer } from 'app/IoCСontainer';
import { KeyValueDataModel, SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';
import { CreatePrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/createPrintedHtmlForm/data-models';
import { createPrintedHtmlFormDataModelMapFrom } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/createPrintedHtmlForm/mappings';
import { editPrintedHtmlFormDataModelMapFrom } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/editPrintedHtmlForm/mappings';
import { getPrintedHtmlFormByIdMapFrom } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormById/mappings';
import { PrintedHtmlFormInfoDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/data-models';
import { ReceivePrintedHtmlFormFilterParams } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/input-params';
import { printedHtmlFormInfoMapFrom } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/mappings';
import { RequestKind } from 'core/requestSender/enums';
import { saveAs } from 'file-saver';

export const PrintedHtmlFormApiProxy = (() => {
    function getPrintedHtmlFormProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getPrintedHtmlFormProxy();
    }

    /**
     * Получить список печатных форм для агентских соглашений
     * @param requestKind Вид запроса
     * @returns Список печатных форм для агентских соглашений
     */
    async function receivePrintedHtmlFormsForAgencyAgreement(requestKind: RequestKind): Promise<KeyValueDataModel<string, string>[]> {
        return getPrintedHtmlFormProxy().receivePrintedHtmlFormsForAgencyAgreement(requestKind)
            .then(responseDto => {
                return responseDto.map(item => {
                    return {
                        key: item.key,
                        value: item.value
                    };
                });
            });
    }

    /**
     * Получить коллекцию информации печатной формы HTML с пагинацией
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Коллекция информации печатной формы HTML с пагинацией
     */
    async function getPrintedHtmlFormsWithPagination(requestKind: RequestKind, args: ReceivePrintedHtmlFormFilterParams): Promise<SelectDataResultMetadataModel<PrintedHtmlFormInfoDataModel>> {
        return getPrintedHtmlFormProxy().getPrintedHtmlFormsWithPagination(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter ? args.filter : null,
            orderBy: args.orderBy,
        })
            .then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, printedHtmlFormInfoMapFrom));
    }

    /**
     * Получить список типов моделей
     * @param requestKind Вид запроса
     * @returns Список типов моделей
     */
    async function getModelTypes(requestKind: RequestKind): Promise<KeyValueDataModel<string, string>[]> {
        return getPrintedHtmlFormProxy().getModelTypes(requestKind)
            .then(responseDto => {
                return responseDto.map(item => {
                    return {
                        key: item.key,
                        value: item.value
                    };
                });
            });
    }

    /**
     * Создать печатную форму HTML
     * @param requestKind Вид запроса
     * @param args
     */
    async function createPrintedHtmlForm(requestKind: RequestKind, args: CreatePrintedHtmlFormDataModel): Promise<void> {
        return getPrintedHtmlFormProxy().createPrintedHtmlForm(requestKind, createPrintedHtmlFormDataModelMapFrom(args));
    }

    /**
     * Получить печатную форму HTML по ID
     * @param requestKind Вид запроса
     * @param printedHtmlId ID печатной формы HTML
     * @returns Печатная форма HTML
     */
    async function getPrintedHtmlFormById(requestKind: RequestKind, printedHtmlId: string): Promise<EditPrintedHtmlFormDataModel> {
        return getPrintedHtmlFormProxy().getPrintedHtmlFormById(requestKind, printedHtmlId)
            .then(responseDto => getPrintedHtmlFormByIdMapFrom(responseDto));
    }

    /**
     * Редактировать печатную форму HTML
     * @param requestKind Вид запроса
     * @param args
     */
    async function editPrintedHtmlForm(requestKind: RequestKind, args: EditPrintedHtmlFormDataModel): Promise<void> {
        return getPrintedHtmlFormProxy().editPrintedHtmlForm(requestKind, editPrintedHtmlFormDataModelMapFrom(args));
    }

    /**
     * Скачать документ PDF с тестовыми данными
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function downloadDocumentWithTestDataForPrintedHtmlForm(requestKind: RequestKind, args: EditPrintedHtmlFormDataModel): Promise<void> {
        return getPrintedHtmlFormProxy().downloadDocumentWithTestDataForPrintedHtmlForm(requestKind, editPrintedHtmlFormDataModelMapFrom(args))
            .then(pdf => {
                const url = window.URL.createObjectURL(pdf);
                const fileName = `${ args.name }.${ pdf.type.replace('application/', '') }`;
                saveAs(url, fileName);
            });
    }

    return {
        receivePrintedHtmlFormsForAgencyAgreement,
        getPrintedHtmlFormsWithPagination,
        getModelTypes,
        createPrintedHtmlForm,
        getPrintedHtmlFormById,
        editPrintedHtmlForm,
        downloadDocumentWithTestDataForPrintedHtmlForm
    };
})();