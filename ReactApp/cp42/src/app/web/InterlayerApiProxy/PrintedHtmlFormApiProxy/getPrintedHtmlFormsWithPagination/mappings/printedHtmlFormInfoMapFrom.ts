import { PrintedHtmlFormInfoResponseDto } from 'app/web/api/PrintedHtmlForm/response-dto';
import { PrintedHtmlFormInfoDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/data-models';

/**
 * Маппинг Response dto информации печатной формы HTML в Data model
 * @param value Response dto информации печатной формы HTML
 * @returns Data model информации печатной формы HTML
 */
export function printedHtmlFormInfoMapFrom(value: PrintedHtmlFormInfoResponseDto): PrintedHtmlFormInfoDataModel {
    return {
        id: value.id,
        name: value.name
    };
}