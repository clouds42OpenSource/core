/**
 * Модель фильтра печатных форм HTML
 */
export type PrintedHtmlFormFilter = {
    /**
     * Название печатной формы
     */
    name: string;
}