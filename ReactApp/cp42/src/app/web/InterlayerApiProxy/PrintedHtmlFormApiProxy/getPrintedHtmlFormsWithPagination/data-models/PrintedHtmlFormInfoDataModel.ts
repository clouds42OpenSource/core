/**
 * Информация печатной формы HTML
 */
export type PrintedHtmlFormInfoDataModel = {
    /**
     * Id печатной формы
     */
    id: string;
    /**
     * Название печатной формы Html
     */
    name: string;
}