import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { PrintedHtmlFormFilter } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/getPrintedHtmlFormsWithPagination/data-models';

export type ReceivePrintedHtmlFormFilterParams = SelectDataCommonDataModel<PrintedHtmlFormFilter>;