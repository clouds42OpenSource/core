import { PrintedHtmlFormFilterRequestDto } from 'app/web/api/PrintedHtmlForm/request-dto';

/**
 * Маппинг input param фильтра печатных форм HTML в Request dto
 * @param value input param фильтра печатных форм HTML
 * @returns Request dto фильтра печатных форм HTML
 */
export function printedHtmlFormFilterRequestDtoMapFrom(value: any): PrintedHtmlFormFilterRequestDto {
    return {
        name: value.name,
    };
}