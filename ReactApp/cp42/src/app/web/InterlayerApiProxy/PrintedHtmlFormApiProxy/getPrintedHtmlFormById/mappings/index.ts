import { EditPrintedHtmlFormRequestDto } from 'app/web/api/PrintedHtmlForm/request-dto';
import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';

/**
 * Маппинг Response dto для получения печатных форм HTML на Data model
 * @param value Response dto для получения печатных форм HTML
 * @returns Data model для получения печатных форм HTML
 */
export function getPrintedHtmlFormByIdMapFrom(value: EditPrintedHtmlFormRequestDto): EditPrintedHtmlFormDataModel {
    return {
        id: value.id,
        htmlData: value.htmlData,
        modelType: value.modelType,
        name: value.name,
        files: value.files?.map(element => ({
            base64: element.base64,
            contentType: element.contentType,
            fileName: element.fileName,
            id: element.id,
            isDeleted: element.isDeleted ?? false
        }))
    };
}