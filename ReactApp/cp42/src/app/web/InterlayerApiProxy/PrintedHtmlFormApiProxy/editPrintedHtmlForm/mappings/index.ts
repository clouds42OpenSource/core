import { EditPrintedHtmlFormRequestDto } from 'app/web/api/PrintedHtmlForm/request-dto';
import { EditPrintedHtmlFormDataModel } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy/common/data-models';

/**
 * Маппинг Data model для редактирование печатных форм HTML на Response dto
 * @param value Data model для редактирование печатных форм HTML
 * @returns Response dto для редактирование печатных форм HTML
 */
export function editPrintedHtmlFormDataModelMapFrom(value: EditPrintedHtmlFormDataModel): EditPrintedHtmlFormRequestDto {
    return {
        id: value.id,
        htmlData: value.htmlData,
        modelType: value.modelType,
        name: value.name,
        uploadedFiles: value.uploadedFiles,
        files: value.files?.map(element => ({
            base64: element.base64,
            contentType: element.contentType,
            fileName: element.fileName,
            id: element.id,
            isDeleted: element.isDeleted
        }))
    };
}