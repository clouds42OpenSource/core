import { SupplierParams } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/common';

type TAgreement = {
    base64: null | string;
    bytes: string;
    content: null | string;
    contentType: string;
    fileName: string;
    id: string;
};

/**
 * Модель для получения поставщика по ID
 */
export type SupplierByIdDataModel = SupplierParams & {
    /**
     * Названия файла офферты
     */
    agreementFileName?: string;
    agreement?: TAgreement;
};