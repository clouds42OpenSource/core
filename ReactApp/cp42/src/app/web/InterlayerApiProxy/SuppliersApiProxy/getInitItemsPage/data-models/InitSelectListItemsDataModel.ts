import { ComboboxItemModel } from 'app/views/components/controls/forms/ComboBox/models';

/**
 * Модель для получения элементов комбобокса
 * при инциализации страниц создания/редактирования поставщика
 */
export type InitSelectListItemsDataModel = {
    /**
     * Все локали
     */
    locales: Array<ComboboxItemModel<string>>;
    /**
     * Все печатные формы
     */
    printedHtmlForms: Array<ComboboxItemModel<string>>;
};