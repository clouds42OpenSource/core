import { SupplierReferralAccountsResponseDto } from 'app/web/api/SuppliersProxy/response-dto';
import { keyValueDataModelMapFrom } from 'app/web/common/mappings';
import { SupplierReferralAccountsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getReferralAccountsBySupplierId';

/**
 * Функция для маппинга SupplierReferralAccountsResponseDto c SupplierReferralAccountsDataModel
 * @param value Модель словаря для реферальных ResponseDto
 * @returns Модель словаря для реферальных DataModel
 */
export function supplierReferralAccountsDataModelMapFrom(value: SupplierReferralAccountsResponseDto): SupplierReferralAccountsDataModel {
    return value.map(refAccount => (keyValueDataModelMapFrom(refAccount)));
}