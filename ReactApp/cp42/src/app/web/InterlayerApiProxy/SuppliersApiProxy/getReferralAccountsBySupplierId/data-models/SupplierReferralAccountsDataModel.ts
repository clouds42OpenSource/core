import { KeyValueDataModel } from 'app/web/common/data-models';

/**
 * Модель словаря для реферальных аккаунтов поставщика
 * Key - ID реферального аккаунта
 * Value - название реферального аккаунта
 */
export type SupplierReferralAccountsDataModel = Array<KeyValueDataModel<string, string>>;