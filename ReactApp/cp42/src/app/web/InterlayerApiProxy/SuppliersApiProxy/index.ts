import { IoCContainer } from 'app/IoCСontainer';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { createEditSupplierParamsMapFrom, SupplierParams, SupplierReferralAccountsParams } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/common';
import { InitSelectListItemsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getInitItemsPage';
import { supplierReferralAccountsDataModelMapFrom } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getReferralAccountsBySupplierId';
import { SupplierReferralAccountsDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getReferralAccountsBySupplierId/data-models';
import { GetSuppliersWithPaginationParams, SuppliersDataModelMapFrom, SuppliersPaginationDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';
import { RequestKind, WebVerb } from 'core/requestSender/enums';
import { TNewResponse } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/common/types';
import { SupplierByIdResponseDto } from 'app/web/api/SuppliersProxy/response-dto';
import { apiHost, getFetchedBlobData } from 'app/api';

/**
 * Апи прокси поставщиков
 */
export const SuppliersApiProxy = (() => {
    function getSuppliersApiProxy() {
        return IoCContainer.getApiProxy().getSupplierProxy();
    }

    /**
     * Запрос на получение списка поставщиков с пагинацией
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function getSuppliersWithPagination(requestKind: RequestKind, args: GetSuppliersWithPaginationParams): Promise<SuppliersPaginationDataModel> {
        return getSuppliersApiProxy().getSuppliersWithPagination(requestKind, args.page, args.pageSize)
            .then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, SuppliersDataModelMapFrom));
    }

    /**
     * Запрос на получение элементов комбобокса
     * @param requestKind тип запроса
     */
    async function getInitCreateEditPageItems(requestKind: RequestKind): Promise<InitSelectListItemsDataModel> {
        return getSuppliersApiProxy().getInitCreateEditPageItems(requestKind);
    }

    /**
     * Запрос на создание поставщика
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function createSupplier(requestKind: RequestKind, args: SupplierParams): Promise<TNewResponse<null>> {
        return getSuppliersApiProxy().createSupplier(requestKind, createEditSupplierParamsMapFrom(args));
    }

    /**
     * Запрос на редактирование поставщика
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function editSupplier(requestKind: RequestKind, args: SupplierParams): Promise<TNewResponse<null>> {
        return getSuppliersApiProxy().editSupplier(requestKind, createEditSupplierParamsMapFrom(args));
    }

    /**
     * Запрос на получение поставщика по ID
     * @param requestKind тип запроса
     * @param supplierId ID поставщика
     */
    async function getSuppliersById(requestKind: RequestKind, supplierId: string): Promise<SupplierByIdResponseDto> {
        return getSuppliersApiProxy().getSupplierById(requestKind, supplierId);
    }

    /**
     * Запрос на удаление поставщика
     * @param requestKind тип запроса
     * @param supplierId ID поставщика
     */
    async function deleteSupplier(requestKind: RequestKind, supplierId: string) {
        return getSuppliersApiProxy().deleteSupplier(requestKind, supplierId);
    }

    /**
     * Получить список рефералов для поставщика в виде словаря
     * @param requestKind Вид запроса
     * @param supplierId ID поставщика
     * @returns Cписок словарей рефералов для поставщика
     */
    async function getReferralAccountsBySupplierId(requestKind: RequestKind, supplierId: string): Promise<SupplierReferralAccountsDataModel> {
        return getSuppliersApiProxy().getReferralAccountsBySupplierId(requestKind, supplierId)
            .then(responseDto => supplierReferralAccountsDataModelMapFrom(responseDto));
    }

    /**
     * Получить список рефералов для поставщика в виде словаря по значению поисковика
     * @param requestKind Вид запроса
     * @param searchValue Значение поиска
     * @returns Cписок словарей рефералов для поставщика
     */
    async function getReferralAccountsBySearchValue(requestKind: RequestKind, searchValue: string): Promise<SupplierReferralAccountsDataModel> {
        return getSuppliersApiProxy().getReferralAccountsBySearchValue(requestKind, searchValue)
            .then(responseDto => supplierReferralAccountsDataModelMapFrom(responseDto));
    }

    /**
     * Удалить реферальный аккаунт у поставщика
     * @param requestKind Вид запроса
     * @param args параметры запроса
     */
    async function deleteSupplierReferralAccount(requestKind: RequestKind, args: SupplierReferralAccountsParams): Promise<TNewResponse<null>> {
        return getSuppliersApiProxy().deleteSupplierReferralAccount(requestKind, {
            accountId: args.accountId,
            supplierId: args.supplierId
        });
    }

    /**
     * Добавить реферальный аккаунт для поставщика
     * @param requestKind Вид запроса
     * @param args параметры запроса
     */
    async function createSupplierReferralAccount(requestKind: RequestKind, args: SupplierReferralAccountsParams): Promise<TNewResponse<null>> {
        return getSuppliersApiProxy().createSupplierReferralAccount(requestKind, {
            accountId: args.accountId,
            supplierId: args.supplierId
        });
    }

    async function getOfferDocumentFile(id: string) {
        return getFetchedBlobData<void>({
            url: `${ apiHost('suppliers-reference/offer-document-file?supplierId=') }${ id }`,
            method: WebVerb.GET
        });
    }

    return {
        getSuppliersWithPagination,
        getInitCreateEditPageItems,
        createSupplier,
        editSupplier,
        getSuppliersById,
        deleteSupplier,
        getReferralAccountsBySupplierId,
        getReferralAccountsBySearchValue,
        deleteSupplierReferralAccount,
        createSupplierReferralAccount,
        getOfferDocumentFile
    };
})();