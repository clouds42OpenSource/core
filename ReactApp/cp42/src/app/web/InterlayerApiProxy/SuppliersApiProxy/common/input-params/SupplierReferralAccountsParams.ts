/**
 * Модель параметров для получения реферальных аккаунтов поставщика
 */
export type SupplierReferralAccountsParams = {
    /**
     * ID поставщика
     */
    supplierId: string;
    /**
     * ID реферального аккаунта
     */
    accountId: string;
};