import uuid from 'app/common/helpers/GenerateUuidHelper/uuid';
import { SupplierParams } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/common/input-params';
import { SupplierRequestDtoNew } from 'app/web/api/SuppliersProxy/request-dto';

/**
 * Маппинг Input param модель параметров с Request dto модель параметров
 * @param value Input param модель параметров при создании/редактировании поставщика
 * @returns Request dto модель параметров при создании/редактировании поставщика
 */
export function createEditSupplierParamsMapFrom(value: SupplierParams): SupplierRequestDtoNew {
    return {
        code: value.code,
        id: value.id === '' || !value.id ? uuid() : value.id,
        isDefault: value.isDefault,
        localeId: value.localeId,
        name: value.name,
        agreementId: value.agreementId,
        cloudFile: value.cloudFile,
        printedHtmlFormInvoiceId: value.printedHtmlFormInvoiceId,
        printedHtmlFormInvoiceReceiptId: value.printedHtmlFormInvoiceReceiptId
    };
}