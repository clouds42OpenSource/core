import { SupplierDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';

/**
 * Модель параметров при создании поставщика
 */
export type SupplierParams = SupplierDataModel & {
    /**
     * Файл договора офферты
     */
    cloudFile?: File;
    /**
     * ID локали
     */
    localeId: string;
    /**
     * ID Договор оферты
     */
    agreementId?: string;
    /**
     * Печатная форма счета на оплату
     */
    printedHtmlFormInvoiceId?: string;
    /**
     * Печатная форма фискального чека
     */
    printedHtmlFormInvoiceReceiptId?: string;
};