export type TNewResponse<T> = {
    Data: T | null;
    Success: boolean;
    Message: string | null;
};