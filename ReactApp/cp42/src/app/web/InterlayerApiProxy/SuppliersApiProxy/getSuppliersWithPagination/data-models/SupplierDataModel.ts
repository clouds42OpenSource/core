/**
 * Модель поставщика
 */
export type SupplierDataModel = {
    /**
     * ID поставщика
     */
    id: string;

    /**
     * Название поставщика
     */
    name: string;

    /**
     * Код поставщика
     */
    code: string;

    /**
     * Название локали
     */
    localeName: string;

    /**
     * Стандартный поставщик на локаль
     */
    isDefault: boolean;
};