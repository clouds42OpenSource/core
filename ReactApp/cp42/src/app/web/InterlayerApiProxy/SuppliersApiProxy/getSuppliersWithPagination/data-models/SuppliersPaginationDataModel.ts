import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { SupplierDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';

/**
 * Модель коллекций поставщиков с пагинацией
 */
export type SuppliersPaginationDataModel = SelectDataResultMetadataModel<SupplierDataModel>;