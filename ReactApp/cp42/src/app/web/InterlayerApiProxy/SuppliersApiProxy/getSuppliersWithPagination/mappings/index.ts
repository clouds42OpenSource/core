import { SupplierResponseDto } from 'app/web/api/SuppliersProxy/response-dto';
import { SupplierDataModel } from 'app/web/InterlayerApiProxy/SuppliersApiProxy/getSuppliersWithPagination';

/**
 * Для маппинга response dto обьекта поставщика с data model
 * @param value response dto обьект поставщика
 * @returns data model обьект поставщика
 */
export const SuppliersDataModelMapFrom = (value: SupplierResponseDto): SupplierDataModel => ({ ...value, localeName: value.localeName ?? '' });