export type GetSuppliersWithPaginationParams = {
    page: number;
    pageSize?: number;
}