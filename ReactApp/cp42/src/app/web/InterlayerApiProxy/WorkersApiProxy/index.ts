import { IoCContainer } from 'app/IoCСontainer';
import { RequestKind } from 'core/requestSender/enums';
import { WorkerDescriptionDataModel } from 'app/web/InterlayerApiProxy/WorkersApiProxy/receiveWorkersData/data-models';
import { workerDescriptionDataModelMapForm } from 'app/web/InterlayerApiProxy/WorkersApiProxy/receiveWorkersData/mappings';

/**
 * Прокси для работы с воркерами
 */
export const WorkersApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getWorkersApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getWorkersApiProxy();
    }

    /**
     * Получить данные по очереди задач воркеров
     * @param requestKind тип запроса
     */
    async function receiveWorkers(requestKind: RequestKind): Promise<Array<WorkerDescriptionDataModel>> {
        return getWorkersApiProxy().receiveWorkersData(requestKind)
            .then(responseDto => {
                return responseDto.map(item => workerDescriptionDataModelMapForm(item));
            });
    }

    return {
        receiveWorkers
    };
})();