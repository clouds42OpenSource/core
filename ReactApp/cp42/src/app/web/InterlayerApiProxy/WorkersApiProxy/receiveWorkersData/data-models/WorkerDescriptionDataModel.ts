/**
 * Модель описания воркера
 */
export type WorkerDescriptionDataModel = {
    /**
     * ID воркера
     */
    id:number;
    /**
     * Название воркера
     */
    name:string;
};