import { WorkerDescriptionResponseDto } from 'app/web/api/WorkersProxy/response-dto/WorkerDescriptionResponseDto';
import { WorkerDescriptionDataModel } from 'app/web/InterlayerApiProxy/WorkersApiProxy/receiveWorkersData/data-models';

/**
 * Выполнить маппинг модели данных по воркеру, полученному при запросе, в модель приложения
 * @param value Модель данных воркера, полученная при запросе
 */
export function workerDescriptionDataModelMapForm(value: WorkerDescriptionResponseDto): WorkerDescriptionDataModel {
    return {
        id: value.id,
        name: value.name
    };
}