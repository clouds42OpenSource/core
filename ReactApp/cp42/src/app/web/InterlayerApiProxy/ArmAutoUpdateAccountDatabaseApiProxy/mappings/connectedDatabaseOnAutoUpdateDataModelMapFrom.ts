import { DateUtility } from 'app/utils';
import { ConnectedDatabaseOnAutoUpdateDataResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy';
import { ConnectedDatabaseOnAutoUpdateDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';

/**
 * Маппинг Response dto данных базы подключенной на АО к Data model
 * @param value Response dto данных базы подключенной на АО
 * @returns Data model данных базы подключенной на АО
 */
export function connectedDatabaseOnAutoUpdateDataModelMapFrom(value: ConnectedDatabaseOnAutoUpdateDataResponseDto): ConnectedDatabaseOnAutoUpdateDataModel {
    return {
        id: value.id,
        actualVersion: value.actualVersion,
        caption: value.caption,
        configurationName: value.configurationName,
        currentVersion: value.currentVersion,
        platformVersion: value.platformVersion,
        v82Name: value.v82Name,
        connectDate: value.connectDate !== null ? DateUtility.convertToDate(value.connectDate) : null,
        lastSuccessAuDate: value.lastSuccessAuDate !== null ? DateUtility.convertToDate(value.lastSuccessAuDate) : null
    };
}