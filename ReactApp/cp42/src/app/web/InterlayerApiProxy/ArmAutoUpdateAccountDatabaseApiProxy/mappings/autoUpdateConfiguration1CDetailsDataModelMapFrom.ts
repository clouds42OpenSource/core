import { AutoUpdateConfiguration1CDetailsResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy';
import { AutoUpdateConfiguration1CDetailsDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';

/**
 * Маппинг Response dto детали авто обновления конфигурации 1С к Data model
 * @param value Response dto детали авто обновления конфигурации 1С
 * @returns Data model детали авто обновления конфигурации 1С
 */
export function autoUpdateConfiguration1CDetailsDataModelMapFrom(value: AutoUpdateConfiguration1CDetailsResponseDto): AutoUpdateConfiguration1CDetailsDataModel {
    return {
        actualVersion: value.ActualVersion,
        configurationName: value.ConfigurationName,
        countStepsForUpdate: value.CountStepsForUpdate,
        currentVersion: value.CurrentVersion,
        updateConfigurationData: value.UpdateConfigurationData.map(item => ({
            actualVersion: item.ActualVersion,
            currentVersion: item.CurrentVersion,
            number: item.Number
        }))
    };
}