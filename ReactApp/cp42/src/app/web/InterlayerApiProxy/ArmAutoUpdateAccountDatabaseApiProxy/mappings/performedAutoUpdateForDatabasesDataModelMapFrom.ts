import { DateUtility } from 'app/utils';
import { PerformedAutoUpdateForDatabasesDataResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy';
import { PerformedAutoUpdateForDatabasesDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';

/**
 * Маппинг Response dto данных выполненного АО базы к Data model
 * @param value Response dto данных выполненного АО базы
 * @returns Data model данных выполненного АО базы
 */
export function performedAutoUpdateForDatabasesDataModelMapFrom(value: PerformedAutoUpdateForDatabasesDataResponseDto): PerformedAutoUpdateForDatabasesDataModel {
    return {
        autoUpdateStartDate: value.autoUpdateStartDate !== null ? DateUtility.convertToDate(value.autoUpdateStartDate) : null,
        caption: value.caption,
        capturedWorkerId: value.capturedWorkerId ?? 1,
        configurationName: value.configurationName,
        coreWorkerTasksQueueId: value.coreWorkerTasksQueueId,
        description: value.description,
        id: value.id,
        status: value.status,
        v82Name: value.v82Name
    };
}