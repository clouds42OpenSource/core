import { DateUtility } from 'app/utils';
import { DatabaseAutoUpdateTechnicalResultDataResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy';
import { DatabaseAutoUpdateTechnicalResultDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';

/**
 * Маппинг Response dto данных технического результата АО инф. базы к Data model
 * @param value Response dto данных технического результата АО инф. базы
 * @returns Data model данных технического результата АО инф. базы
 */
export function databaseAutoUpdateTechnicalResultDataModelMapFrom(value: DatabaseAutoUpdateTechnicalResultDataResponseDto): DatabaseAutoUpdateTechnicalResultDataModel {
    return {
        autoUpdateEndDate: value.AutoUpdateEndDate ? DateUtility.convertToDate(value.AutoUpdateEndDate) : null,
        autoUpdateStartDate: value.AutoUpdateStartDate ? DateUtility.convertToDate(value.AutoUpdateStartDate) : null,
        debugInformation: value.DebugInformation
    };
}