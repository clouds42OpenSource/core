import { DateUtility } from 'app/utils';
import { DatabaseInAutoUpdateQueueDataResponseDto } from 'app/web/api/ArmAutoUpdateAccountDatabaseProxy';
import { DatabaseInAutoUpdateQueueDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import { connectedDatabaseOnAutoUpdateDataModelMapFrom } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/mappings';

/**
 * Маппинг Response dto модели данных базы которая в очереди на АО к Data model
 * @param value Response dto модели данных базы которая в очереди на АО
 * @returns Data model данных базы которая в очереди на АО
 */
export function databaseInAutoUpdateQueueDataModelMapFrom(value: DatabaseInAutoUpdateQueueDataResponseDto): DatabaseInAutoUpdateQueueDataModel {
    return {
        ...connectedDatabaseOnAutoUpdateDataModelMapFrom(value),
        addedDate: value.addedDate !== null ? DateUtility.convertToDate(value.addedDate) : null,
        isProcessingAutoUpdate: value.isProcessingAutoUpdate
    };
}