import { SelectDataCommonDataModel } from 'app/web/common/data-models';

type Filter = {
    /**
     * Только те, которым необходимо авто обновление
     */
    needAutoUpdateOnly?: boolean,

    /**
     * Только те, у которых активна Аренда 1С
     */
    isActiveRent1COnly?: boolean
    isVipAccountsOnly?: boolean
    searchLine?: string
    configurations?: string[]
}

export type ConnectedDatabaseOnAutoUpdateFilterParams = SelectDataCommonDataModel<Filter>