import { SelectDataCommonDataModel } from 'app/web/common/data-models';

type Filter = {
    /**
     * Только те, которым необходимо авто обновление
     */
    /**
  * Строка поиска
  */
    searchLine?: string,

    /**
     * Только вип аккаунты
     */
    isVipAccountsOnly?: boolean,

    /**
     * Конфигурации 1С
     */
    configurations?: string[]
}

export type DatabaseInAutoUpdateQueueFilterParams = SelectDataCommonDataModel<Filter>