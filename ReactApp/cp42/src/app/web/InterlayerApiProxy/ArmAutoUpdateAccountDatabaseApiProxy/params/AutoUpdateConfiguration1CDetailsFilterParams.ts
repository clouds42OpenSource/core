/**
 * Модель фильтра для получения деталей авто обновления конфигурации 1С
 */
export type AutoUpdateConfiguration1CDetailsFilterParams = {
    /**
     * Текущая версия релиза
     */
    currentVersion: string,

    /**
     * Актуальная версия релиза
     */
    actualVersion: string,

    /**
     * Название конфигурации
     */
    configurationName: string
}