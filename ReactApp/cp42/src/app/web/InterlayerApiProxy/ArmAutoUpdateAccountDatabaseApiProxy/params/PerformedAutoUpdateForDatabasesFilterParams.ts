import { SupportHistoryStateEnumType } from 'app/common/enums';
import { Nullable } from 'app/common/types';
import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Модель фильтра выполненного АО базы
 */
export type Filter = {
    /**
     * Номер воркера
     */
    coreWorkerId?: number,

    /**
     * Статус АО
     */
    status?: SupportHistoryStateEnumType | string,

    /**
     * Время начала АО
     */
    autoUpdatePeriodFrom?: Nullable<Date>,

    /**
     * Время завершения АО
     */
    isVipAccountsOnly?: boolean,

    autoUpdatePeriodTo?: Nullable<Date>,

    configurations?: string[]

    searchLine?: string,
}

export type PerformedAutoUpdateForDatabasesFilterParams = SelectDataCommonDataModel<Filter>;