import { FilterBaseParams } from 'app/web/common/params';

/**
 * Модель фильтра технического результата АО инф. базы
 */
export type DatabaseAutoUpdateTechnicalResultFilterParams = FilterBaseParams & {
    /**
     * Id базы
     */
    accountDatabaseId: string,

    /**
     * Id задачи из очереди
     */
    coreWorkerTasksQueueId: string
}