import { IoCContainer } from 'app/IoCСontainer';
import { commonFilterParamsMapFrom, paginationDataResultToSelectDataResultMapFrom, SelectDataResultCommonDataModel, SelectDataResultMetadataModel } from 'app/web/common';
import {
    AutoUpdateConfiguration1CDetailsDataModel,
    ConnectedDatabaseOnAutoUpdateDataModel,
    DatabaseAutoUpdateTechnicalResultDataModel,
    DatabaseInAutoUpdateQueueDataModel,
    PerformedAutoUpdateForDatabasesDataModel
} from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';
import {
    autoUpdateConfiguration1CDetailsDataModelMapFrom,
    connectedDatabaseOnAutoUpdateDataModelMapFrom,
    databaseAutoUpdateTechnicalResultDataModelMapFrom,
    databaseInAutoUpdateQueueDataModelMapFrom,
    performedAutoUpdateForDatabasesDataModelMapFrom
} from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/mappings';
import {
    AutoUpdateConfiguration1CDetailsFilterParams,
    ConnectedDatabaseOnAutoUpdateFilterParams,
    DatabaseAutoUpdateTechnicalResultFilterParams,
    DatabaseInAutoUpdateQueueFilterParams,
    PerformedAutoUpdateForDatabasesFilterParams
} from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/params';
import { RequestKind } from 'core/requestSender/enums';

import { DateUtility } from 'app/utils';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
/**
 * Прокси для работы с АРМ автообновлением
 */
export const ArmAutoUpdateAccountDatabaseApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getArmAutoUpdateAccountDatabaseApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getArmAutoUpdateAccountDatabaseProxy();
    }

    /**
     * Получить список инф. баз, подключенных к АО
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список инф. баз, подключенных к АО
     */
    async function getConnectedDatabasesOnAutoUpdate(requestKind: RequestKind, args: ConnectedDatabaseOnAutoUpdateFilterParams): Promise<SelectDataResultMetadataModel<ConnectedDatabaseOnAutoUpdateDataModel>> {
        return getArmAutoUpdateAccountDatabaseApiProxy().getConnectedDatabasesOnAutoUpdate(requestKind, {
            PageNumber: args.pageNumber,
            orderBy: args.orderBy,
            Filter: args.filter ? args.filter : null,
            PageSize: args.size
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, connectedDatabaseOnAutoUpdateDataModelMapFrom));
    }

    /**
     * Получить список инф. баз, в очереди на АО
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список инф. баз, в очереди на АО
     */
    async function getDatabaseInAutoUpdateQueue(requestKind: RequestKind, args: DatabaseInAutoUpdateQueueFilterParams): Promise<SelectDataResultMetadataModel<DatabaseInAutoUpdateQueueDataModel>> {
        return getArmAutoUpdateAccountDatabaseApiProxy().getDatabaseInAutoUpdateQueue(requestKind, {
            PageNumber: args.pageNumber,
            orderBy: args.orderBy,
            Filter: args.filter ? args.filter : null,
            PageSize: args.size
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, databaseInAutoUpdateQueueDataModelMapFrom));
    }

    /**
     * Получить детали авто обновления конфигурации 1С
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Детали авто обновления конфигурации 1С
     */
    async function getAutoUpdateConfiguration1CDetails(requestKind: RequestKind, args: AutoUpdateConfiguration1CDetailsFilterParams): Promise<AutoUpdateConfiguration1CDetailsDataModel> {
        return getArmAutoUpdateAccountDatabaseApiProxy().getAutoUpdateConfiguration1CDetails(requestKind, {
            ActualVersion: args.actualVersion,
            ConfigurationName: args.configurationName,
            CurrentVersion: args.currentVersion
        }).then(responseDto => autoUpdateConfiguration1CDetailsDataModelMapFrom(responseDto));
    }

    /**
     * Получить названия конфигураций 1С
     * @param requestKind Вид запроса
     * @returns Список названий конфигураций 1С
     */
    async function getConfigurationNames(requestKind: RequestKind): Promise<Array<string>> {
        return getArmAutoUpdateAccountDatabaseApiProxy().getConfigurationNames(requestKind);
    }

    /**
     * Получить данные по выполненным АО инф. баз
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Данные по выполненным АО инф. баз
     */
    async function getPerformedAutoUpdateForDatabasesData(requestKind: RequestKind, args: PerformedAutoUpdateForDatabasesFilterParams): Promise<SelectDataResultMetadataModel<PerformedAutoUpdateForDatabasesDataModel>> {
        return getArmAutoUpdateAccountDatabaseApiProxy().getPerformedAutoUpdateForDatabasesData(requestKind, {
            PageNumber: args.pageNumber,
            orderBy: args.orderBy,
            Filter: args.filter ? {
                ...args.filter,
                autoUpdatePeriodFrom: args.filter.autoUpdatePeriodFrom ? DateUtility.dateToIsoDateString(args.filter.autoUpdatePeriodFrom, { includeTime: true, includeMilliseconds: true }) : null,
                autoUpdatePeriodTo: args.filter.autoUpdatePeriodTo ? DateUtility.dateToIsoDateString(args.filter.autoUpdatePeriodTo, { includeTime: true, includeMilliseconds: true }) : null
            } : null,
            PageSize: args.size
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, performedAutoUpdateForDatabasesDataModelMapFrom));
    }

    /**
     * Получить воркеры которые выполняют авто обновление
     * @param requestKind Вид запроса
     * @returns Список воркеров
     */
    async function getWorkers(requestKind: RequestKind): Promise<Array<number>> {
        return await getArmAutoUpdateAccountDatabaseApiProxy().getWorkers(requestKind);
    }

    /**
     * Получить данные технических результатов АО инф. базы
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     * @returns Список технических результатов АО инф. базы
     */
    async function getDatabaseAutoUpdateTechnicalResults(requestKind: RequestKind, args: DatabaseAutoUpdateTechnicalResultFilterParams): Promise<SelectDataResultCommonDataModel<DatabaseAutoUpdateTechnicalResultDataModel>> {
        return await getArmAutoUpdateAccountDatabaseApiProxy().getDatabaseAutoUpdateTechnicalResults(requestKind, {
            ...commonFilterParamsMapFrom(args),
            AccountDatabaseId: args.accountDatabaseId,
            CoreWorkerTasksQueueId: args.coreWorkerTasksQueueId
        }).then(responseDto => paginationDataResultToSelectDataResultMapFrom(responseDto, databaseAutoUpdateTechnicalResultDataModelMapFrom));
    }

    return {
        getConnectedDatabasesOnAutoUpdate,
        getDatabaseInAutoUpdateQueue,
        getAutoUpdateConfiguration1CDetails,
        getConfigurationNames,
        getPerformedAutoUpdateForDatabasesData,
        getWorkers,
        getDatabaseAutoUpdateTechnicalResults
    };
})();