import { UpdateConfigurationDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';

/**
 * Детали авто обновления конфигурации 1С
 */
export type AutoUpdateConfiguration1CDetailsDataModel = {
    /**
     * Текущая версия релиза
     */
    currentVersion: string,

    /**
     * Актуальная версия релиза
     */
    actualVersion: string,

    /**
     * Название конфигурации
     */
    configurationName: string,

    /**
     * Список обновлений конфигурации с порядком обновления
     */
    updateConfigurationData: Array<UpdateConfigurationDataModel>,

    /**
     * Количество попыток для обновления
     */
    countStepsForUpdate: number
}