import { Nullable } from 'app/common/types';

/**
 * Модель данных технического результата АО инф. базы
 */
export type DatabaseAutoUpdateTechnicalResultDataModel = {
    /**
     * Время начала АО
     */
    autoUpdateStartDate: Nullable<Date>,

    /**
     * Время завершения АО
     */
    autoUpdateEndDate: Nullable<Date>,

    /**
     * Информация для отладки
     */
    debugInformation: string
}