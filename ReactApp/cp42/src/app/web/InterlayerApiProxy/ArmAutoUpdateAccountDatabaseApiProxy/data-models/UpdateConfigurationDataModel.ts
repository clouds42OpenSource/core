/**
 * Данные обновления конфигурации
 */
export type UpdateConfigurationDataModel = {
    /**
     * Номер обновления
     */
    number: number,

    /**
     * Текущая версия
     */
    currentVersion: string,

    /**
     * Актуальная версия
     */
    actualVersion: string
}