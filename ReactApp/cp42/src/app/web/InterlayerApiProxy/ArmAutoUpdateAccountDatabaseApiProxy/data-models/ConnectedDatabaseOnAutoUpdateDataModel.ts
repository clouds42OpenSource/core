import { Nullable } from 'app/common/types';

/**
 * Модель данных базы подключенной на АО
 */
export type ConnectedDatabaseOnAutoUpdateDataModel = {
    /**
     * Id базы
     */
    id: string,

    /**
     * Номер базы
     */
    v82Name: string,

    /**
     * Название базы
     */
    caption: string,

    /**
     * Название конфигурации
     */
    configurationName: string,

    /**
     * Текущая версия релиза
     */
    currentVersion: string,

    /**
     * Актуальная версия релиза
     */
    actualVersion: string,

    /**
     * Версия платформы
     */
    platformVersion: string,

    /**
     * Дата подключения
     */
    connectDate: Nullable<Date>,

    /**
     * Дата последнего успешного АО
     */
    lastSuccessAuDate?: Nullable<Date>
}