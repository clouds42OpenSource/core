import { Nullable } from 'app/common/types';
import { ConnectedDatabaseOnAutoUpdateDataModel } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy/data-models';

/**
 * Модель данных базы которая в очереди на АО
 */
export type DatabaseInAutoUpdateQueueDataModel = ConnectedDatabaseOnAutoUpdateDataModel & {
    /**
     * Дата добавления базы в очередь на АО
     */
    addedDate: Nullable<Date>,

    /**
     * В процессе авто обновления
     */
    isProcessingAutoUpdate: boolean
}