import { SupportHistoryStateEnumType } from 'app/common/enums';
import { Nullable } from 'app/common/types';

/**
 * Модель данных выполненного АО базы
 */
export type PerformedAutoUpdateForDatabasesDataModel = {
    /**
     * Id базы
     */
    id: string,

    /**
     * Id задачи которая выполняла обновление
     */
    coreWorkerTasksQueueId: string,

    /**
     * Номер базы
     */
    v82Name: string,

    /**
     * Название базы
     */
    caption: string,

    /**
     * Название конфигурации
     */
    configurationName: string,

    /**
     * Номер воркера который выполнял АО
     */
    capturedWorkerId: number,

    /**
     * Статус АО
     */
    status: SupportHistoryStateEnumType,

    /**
     * Описание
     */
    description: string,

    /**
     * Время начала АО
     */
    autoUpdateStartDate: Nullable<Date>,
}