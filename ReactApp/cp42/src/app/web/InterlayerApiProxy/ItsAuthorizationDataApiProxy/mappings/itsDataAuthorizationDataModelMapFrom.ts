import { ItsAuthorizationDataResponseDto } from 'app/web/api/ItsAuthorizationDataProxy';
import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';

/**
 * Маппинг Response Dto данных авторизации в ИТС к Data Model
 * @param value Response Dto данных авторизации в ИТС
 * @returns Data Model данных авторизации в ИТС
 */
export function itsDataAuthorizationDataModelMapFrom(value: ItsAuthorizationDataResponseDto): ItsDataAuthorizationDataModel {
    return {
        id: value.id,
        login: value.login,
        passwordHash: value.passwordHash
    };
}