import { ItsAuthorizationFilterDataRequestDto } from 'app/web/api/ItsAuthorizationDataProxy';
import { ItsAuthorizationFilterDataParams } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/params';

/**
 * Маппинг Data Model фильтра для получения данных авторизации в ИТС к Request Dto
 * @param value Модель фильтра для получения данных авторизации в ИТС
 * @returns Request Dto фильтра для получения данных авторизации в ИТС
 */
export function itsAuthorizationFilterParamsMapFrom(value: ItsAuthorizationFilterDataParams): ItsAuthorizationFilterDataRequestDto {
    return {
        searchLine: value.searchString
    };
}