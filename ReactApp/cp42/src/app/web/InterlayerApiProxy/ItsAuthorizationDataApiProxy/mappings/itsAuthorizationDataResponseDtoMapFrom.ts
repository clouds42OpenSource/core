import { ItsAuthorizationDataResponseDto } from 'app/web/api/ItsAuthorizationDataProxy';
import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';

/**
 * Маппинг Data Model данных авторизации в ИТС к Response Dto
 * @param value Data Model данных авторизации в ИТС
 * @returns Response Dto данных авторизации в ИТС
 */
export function itsAuthorizationDataResponseDtoMapFrom(value: ItsDataAuthorizationDataModel): ItsAuthorizationDataResponseDto {
    return {
        id: value.id,
        login: value.login,
        passwordHash: value.passwordHash
    };
}