import { SelectDataCommonDataModel } from 'app/web/common';
import { ItsAuthorizationFilterDataParams } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/params';

/**
 * Модель фильтра для получения списка данных авторизации в ИТС
 */
export type ItsAuthorizationFilterParams = SelectDataCommonDataModel<ItsAuthorizationFilterDataParams>;