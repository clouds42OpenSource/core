import { Nullable } from 'app/common/types';

/**
 * Данные фильтра для получения данных авторизации в ИТС
 */
export type ItsAuthorizationFilterDataParams = {
    /**
     * Строка поиска
     */
    searchString: Nullable<string>
}