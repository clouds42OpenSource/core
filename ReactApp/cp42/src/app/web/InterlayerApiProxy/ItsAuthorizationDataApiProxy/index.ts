import { TResponseLowerCase } from 'app/api/types';
import { IoCContainer } from 'app/IoCСontainer';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import { ItsAuthorizationDataPaginationDataModel, ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';
import { itsAuthorizationDataResponseDtoMapFrom, itsAuthorizationFilterParamsMapFrom, itsDataAuthorizationDataModelMapFrom } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/mappings';
import { ItsAuthorizationFilterParams } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/params/ItsAuthorizationFilterParams';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Прокси для работы с данными доступа к ИТС
 */
export const ItsAuthorizationDataApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getItsAuthorizationDataProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getItsAuthorizationDataProxy();
    }

    /**
     * Получить список авторизации данных ИТС с пагинацией
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function getItsDataAuthorizations(requestKind: RequestKind, args: ItsAuthorizationFilterParams): Promise<ItsAuthorizationDataPaginationDataModel> {
        return getItsAuthorizationDataProxy().getItsDataAuthorizations(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter ? itsAuthorizationFilterParamsMapFrom(args.filter) : null,
            orderBy: args.orderBy
        }).then(responseDto => selectDataResultMetadataModelMapFrom(responseDto, itsDataAuthorizationDataModelMapFrom));
    }

    /**
     * Получить модель авторизации данных ИТС по ID
     * @param requestKind Вид запроса
     * @param itsAuthorizationId ID модель авторизации данных
     */
    async function getItsDataAuthorization(requestKind: RequestKind, itsAuthorizationId: string): Promise<ItsDataAuthorizationDataModel> {
        return getItsAuthorizationDataProxy().getItsDataAuthorization(requestKind, itsAuthorizationId)
            .then(responseDto => itsDataAuthorizationDataModelMapFrom(responseDto));
    }

    /**
     * Создать новые данные авторизации ИТС
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function createItsDataAuthorization(requestKind: RequestKind, args: ItsDataAuthorizationDataModel): Promise<TResponseLowerCase<null> | null> {
        return getItsAuthorizationDataProxy().createItsDataAuthorization(requestKind, {
            login: args.login,
            passwordHash: args.passwordHash
        });
    }

    /**
     * Редактировать данные авторизации ИТС
     * @param requestKind Вид запроса
     * @param args Параметры запроса
     */
    async function editItsDataAuthorization(requestKind: RequestKind, args: ItsDataAuthorizationDataModel): Promise<void> {
        return getItsAuthorizationDataProxy().editItsDataAuthorization(requestKind, itsAuthorizationDataResponseDtoMapFrom(args));
    }

    /**
     * Удалить авторизации данных ИТС
     * @param requestKind Вид запроса
     * @param itsAuthorizationId ID модель авторизации данных
     */
    async function deleteItsDataAuthorization(requestKind: RequestKind, itsAuthorizationId: string): Promise<void> {
        return getItsAuthorizationDataProxy().deleteItsDataAuthorization(requestKind, itsAuthorizationId);
    }

    /**
     * Получить коллекцию авторизации данных в ИТС
     * @param requestKind Вид запроса
     * @returns Коллекцию авторизации данных в ИТС
     */
    async function getItsAuthorizationDataBySearchValue(requestKind: RequestKind): Promise<Array<ItsDataAuthorizationDataModel>> {
        return getItsAuthorizationDataProxy().getItsAuthorizationDataBySearchValue(requestKind)
            .then(responseDto => responseDto.records.map<ItsDataAuthorizationDataModel>(item => itsDataAuthorizationDataModelMapFrom(item)));
    }

    return {
        getItsDataAuthorizations,
        getItsDataAuthorization,
        createItsDataAuthorization,
        editItsDataAuthorization,
        deleteItsDataAuthorization,
        getItsAuthorizationDataBySearchValue
    };
})();