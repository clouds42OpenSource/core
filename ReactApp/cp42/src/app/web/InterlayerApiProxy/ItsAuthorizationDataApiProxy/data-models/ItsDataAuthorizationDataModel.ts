/**
 * Модель данных авторизации в ИТС
 */
export type ItsDataAuthorizationDataModel = {
    /**
     * ID записи
     */
    id: string;
    /**
     * Логин
     */
    login: string;
    /**
     * Хэш пароля
     */
    passwordHash: string;
}