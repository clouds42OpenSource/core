import { SelectDataResultMetadataModel } from 'app/web/common';
import { ItsDataAuthorizationDataModel } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy/data-models';

/**
 * Модель списка данных авторизации в ИТС с пагинацией
 */
export type ItsAuthorizationDataPaginationDataModel = SelectDataResultMetadataModel<ItsDataAuthorizationDataModel>;