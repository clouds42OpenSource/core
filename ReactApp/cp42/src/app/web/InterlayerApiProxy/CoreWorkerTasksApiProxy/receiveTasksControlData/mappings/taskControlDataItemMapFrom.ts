import { TaskControlDataItemResponseDto } from 'app/web/api/CoreWorkerTasksProxy/response-dto/TaskControlDataItemResponseDto';
import { TasksControlItemDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';

/**
 * Выполнить маппинг модели данных по управлению задачей, полученной при запросе, в модель приложения
 * @param value Модель ответа с элементом управления задачей
 */
export function taskControlDataItemMapFrom(value: TaskControlDataItemResponseDto): TasksControlItemDataModel {
    return {
        taskId: value.id,
        taskName: value.name,
        taskExecutionLifetimeInMinutes: value.taskExecutionLifetimeInMinutes
    };
}