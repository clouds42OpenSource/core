/**
 * Модель элемента управления задачами воркера
 */
export type TasksControlItemDataModel = {
    /**
     * Id задачи
     */
    taskId: string;
    /**
     * Название задачи
     */
    taskName:string;
    /**
     * Время жизни выполнения задачи в минутах
     */
    taskExecutionLifetimeInMinutes: number;
};