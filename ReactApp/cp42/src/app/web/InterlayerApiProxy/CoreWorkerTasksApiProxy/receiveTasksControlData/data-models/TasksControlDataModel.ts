import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { TasksControlItemDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData/data-models/TasksControlItemDataModel';

/**
 * Модель данных по управлению задачами
 */
export type TasksControlDataModel = SelectDataResultMetadataModel<TasksControlItemDataModel>;