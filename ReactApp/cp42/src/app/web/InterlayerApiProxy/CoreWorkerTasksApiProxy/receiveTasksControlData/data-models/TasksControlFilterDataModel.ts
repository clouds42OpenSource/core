import { Nullable } from 'app/common/types';

/**
 * Фильтр поиска элементов управления задачами воркера
 */
export type TasksControlFilterDataModel = {
    /**
     * ID задачи
     */
    taskId: Nullable<string>;
};