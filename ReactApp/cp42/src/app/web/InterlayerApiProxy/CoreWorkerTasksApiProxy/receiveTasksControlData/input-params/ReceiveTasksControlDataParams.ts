import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { TasksControlFilterDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';

export type ReceiveTasksControlDataParams = SelectDataCommonDataModel<TasksControlFilterDataModel>;