import { KeyValueDataModel } from 'app/web/common/data-models';

/**
 * Модель задач воркера
 */
export type CoreWorkerTasksDataModel = {

    /**
     * Список задач воркера
     */
    items: Array<KeyValueDataModel<string, string>>;
};