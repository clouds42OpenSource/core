import { IoCContainer } from 'app/IoCСontainer';
import { CoreWorkerTasksDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveCoreWorkerTasksData/data-models';
import { ReceiveTasksControlDataParams, TasksControlDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';
import { taskControlDataItemMapFrom } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData/mappings/taskControlDataItemMapFrom';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Прокси для работы с задачами воркера
 */
export const CoreWorkerTasksApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getCoreWorkerTasksApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getCoreWorkerTasksApiProxy();
    }

    /**
     * Получить данные по задачам воркера
     * @param requestKind тип запроса
     */
    async function receiveAllCoreWorkerTasks(requestKind: RequestKind): Promise<CoreWorkerTasksDataModel> {
        return getCoreWorkerTasksApiProxy().receiveAllCoreWorkerTasksData(requestKind)
            .then(responseDto => {
                return {
                    items: responseDto
                };
            });
    }

    /**
     * Получить данные по управлению задачами воркера
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveTasksControlData(requestKind: RequestKind, args: ReceiveTasksControlDataParams): Promise<TasksControlDataModel> {
        return getCoreWorkerTasksApiProxy().receiveTasksControlData(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter ? args.filter : null,
        }).then(responseDto => {
            return {
                records: responseDto.records.map(record => taskControlDataItemMapFrom(record)),
                metadata: responseDto.metadata
            };
        });
    }

    return {
        receiveAllCoreWorkerTasks,
        receiveTasksControlData
    };
})();