import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Название конфигурации
     */
    configurationName: string;

    /**
     * Каталог конфигурации на ИТС
     */
    configurationCatalog: string;
};

/**
 * Параметры для загрузки материнских баз разделителей
 */
export type ReceiveConfigurations1CParams = SelectDataCommonDataModel<Filter>;