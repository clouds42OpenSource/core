import { ConfigurationReductionDataModel } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C/data-models/ConfigurationReductionDataModel';

/**
 *  Модель свойств элемента конфигурации 1С
 */
export type Configurations1CItemDataModel = {
    /**
     * Название конфигурации
     */
    configurationName: string;
    /**
     * Каталог конфигурации на ИТС
     */
    configurationCatalog: string;
    /**
     * Редакция
     */
    redactionCatalogs: string;
    /**
     * Платформа
     */
    platformCatalog: string;
    /**
     * Адрес каталога обновлений.
     */
    updateCatalogUrl: string;
    /**
     * Краткий код конфигурации, например bp, zup и тд.
     */
    shortCode: string;
    /**
     * Признак что необходимо использовать COM соединения для принятия обновлений в базе
     */
    useComConnectionForApplyUpdates: boolean;
    /**
     * Признак необходимости проверять наличие обновлений
     */
    needCheckUpdates: boolean;
    /**
     * Стоимость конфигурации
     */
    configurationCost: number;
    /**
     * Вариации имени конфигурации 1С
     */
    configurationVariations: Array<string>;
    /**
     * ID данных авторизации в ИТС
     */
    itsAuthorizationDataId: string;
    /**
     * Полные пути к архиву с информацией по обновлениям конфигурации.
     */
    reductions: Array<ConfigurationReductionDataModel>;
};