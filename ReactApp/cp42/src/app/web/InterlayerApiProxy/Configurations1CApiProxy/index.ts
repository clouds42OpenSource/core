import { IoCContainer } from 'app/IoCСontainer';
import { selectDataResultMetadataModelMapFrom } from 'app/web/common/mappings';
import {
    AddConfiguration1CResultDataModel
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/addConfiguration1C/data-models';
import {
    AddConfiguration1CParams
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/addConfiguration1C/input-params';
import {
    addConfigurations1CItemRequestDtoMapFrom
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/addConfiguration1C/mappings';
import {
    DeleteConfiguration1CParams
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/deleteConfiguration1C/input-params';
import {
    configurations1CItemDataModelMapFrom,
    GetConfigurations1CResultDataModel,
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C';
import {
    ReceiveConfigurations1CParams
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C/input-params';
import {
    UpdateConfiguration1CResultDataModel
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/updateConfiguration1C/data-models';
import {
    UpdateConfiguration1CParams
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/updateConfiguration1C/input-params';
import {
    updateConfiguration1CItemRequestDtoMapFrom
} from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/updateConfiguration1C/mappings';
import { RequestKind } from 'core/requestSender/enums';

export const Configurations1CApiProxy = (() => {
    function getConfigurations1CApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getConfigurations1CProxy();
    }

    async function receiveConfigurations1C(
        requestKind: RequestKind,
        args: ReceiveConfigurations1CParams
    ): Promise<GetConfigurations1CResultDataModel> {
        return getConfigurations1CApiProxy()
            .receiveConfigurations1C(requestKind, {
                PageNumber: args.pageNumber,
                Filter: args.filter
                    ? {
                        name: args.filter.configurationName,
                        configurationCatalog: args.filter.configurationCatalog,
                    }
                    : null,
                orderBy: args.orderBy,
            }).then(responseDto => selectDataResultMetadataModelMapFrom(
                responseDto,
                configurations1CItemDataModelMapFrom
            ));
    }

    async function updateConfiguration1C(
        requestKind: RequestKind,
        args: UpdateConfiguration1CParams
    ): Promise<UpdateConfiguration1CResultDataModel> {
        return getConfigurations1CApiProxy().updateConfiguration1C(requestKind, { ...updateConfiguration1CItemRequestDtoMapFrom(args) });
    }

    async function addConfiguration1C(
        requestKind: RequestKind,
        args: AddConfiguration1CParams
    ): Promise<AddConfiguration1CResultDataModel> {
        try {
            const res = await getConfigurations1CApiProxy().addConfiguration1C(
                requestKind,
                {
                    ...addConfigurations1CItemRequestDtoMapFrom(args),
                }
            );

            // @ts-ignore
            if (res.success === false || res.Success === false) {
                throw { success: false, message: res?.Message || res.message };
            }
            return res;
        } catch (err) {
            throw err;
        }
    }

    async function deleteConfiguration1C(
        requestKind: RequestKind,
        args: DeleteConfiguration1CParams
    ): Promise<void> {
        try {
            const res = await getConfigurations1CApiProxy().deleteConfiguration1C(
                requestKind,
                {
                    ConfigurationName: args.configurationName,
                }
            );

            // @ts-ignore
            if (res.success === false) {
                throw res;
            }
            return res;
        } catch (err) {
            throw err;
        }
    }

    return {
        receiveConfigurations1C,
        updateConfiguration1C,
        addConfiguration1C,
        deleteConfiguration1C,
    };
})();