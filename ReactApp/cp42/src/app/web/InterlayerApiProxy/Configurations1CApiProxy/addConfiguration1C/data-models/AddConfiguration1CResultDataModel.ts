import { ConfigurationReductionDataModel } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/receiveConfigurations1C/data-models/ConfigurationReductionDataModel';

export type AddConfiguration1CResultDataModel = {
    /**
     * Полные пути к архиву с информацией по обновлениям конфигурации.
     */
    reductions: Array<ConfigurationReductionDataModel>;
};