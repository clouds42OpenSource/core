import { AddOrUpdateConfiguration1CResultResponseDto } from 'app/web/api/Configurations1CProxy/response-dto';
import { AddConfiguration1CResultDataModel } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/addConfiguration1C/data-models';

/**
 * Mapping модели путей к архиву с информацией по обновлениям конфигурации
 * @param value ответ модель путей к архиву с информацией по обновлениям конфигурации
 */
export function addConfiguration1CResultDataModelMapFrom(value: AddOrUpdateConfiguration1CResultResponseDto): AddConfiguration1CResultDataModel {
    return {
        reductions: value.Reductions?.map(item => {
            return {
                redactionCatalog: item.redactionCatalog,
                urlOfMapping: item.urlOfMapping
            };
        })
    };
}