import { AddOrUpdateConfiguration1CItemRequestDto } from 'app/web/api/Configurations1CProxy/request-dto/AddOrUpdateConfigurations1CItemRequestDto';
import { AddConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy/addConfiguration1C/input-params';

/**
 * Mapping модели конфигурации 1С
 * @param value ответ модели конфигурации 1С
 */
export function addConfigurations1CItemRequestDtoMapFrom(value: AddConfiguration1CParams): AddOrUpdateConfiguration1CItemRequestDto {
    return {
        Name: value.configurationName,
        ConfigurationCatalog: value.configurationCatalog,
        RedactionCatalogs: value.redactionCatalogs,
        PlatformCatalog: value.platformCatalog,
        UseComConnectionForApplyUpdates: value.useComConnectionForApplyUpdates,
        ShortCode: value.shortCode || null,
        UpdateCatalogUrl: value.updateCatalogUrl,
        NeedCheckUpdates: value.needCheckUpdates,
        ConfigurationCost: value.configurationCost,
        ConfigurationVariations: value.configurationVariations,
        ItsAuthorizationDataId: value.itsAuthorizationDataId
    };
}