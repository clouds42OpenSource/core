import { IoCContainer } from 'app/IoCСontainer';
import { CancelCoreWorkerTasksQueueParams } from 'app/web/InterlayerApiProxy/TasksApiProxy/cancelCoreWorkerTasksQueue/input-params';
import { ReceiveTasksInQueueParams } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData';
import { TasksInQueueDataModel } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData/data-models';
import { receiveTasksInQueueFilterRequestDtoMapFrom, tasksInQueueDataModelMapFrom } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData/mappings';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Прокси для работы с задачами
 */
export const TasksApiProxy = (() => {
    /**
     * Получить экземпляр прокси
     */
    function getTasksApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getTasksApiProxy();
    }

    /**
     * Получить данные по очереди задач воркеров
     * @param requestKind тип запроса
     * @param args параметры запроса
     */
    async function receiveTasksInQueue(requestKind: RequestKind, args: ReceiveTasksInQueueParams): Promise<TasksInQueueDataModel> {
        return getTasksApiProxy().receiveTasksInQueueData(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? receiveTasksInQueueFilterRequestDtoMapFrom(args.filter)
                : null,
            orderBy: args.orderBy
        }).then(responseDto => {
            return {
                records: responseDto.records.map(record => tasksInQueueDataModelMapFrom(record)),
                metadata: responseDto.metadata
            };
        });
    }

    /**
     * Отменить запущенную задачу
     * @param requestKind Вид запроса
     * @param args Параметры для отмены запущенной задачи
     */
    async function cancelCoreWorkerTasksQueue(requestKind: RequestKind, args: CancelCoreWorkerTasksQueueParams): Promise<void> {
        return getTasksApiProxy().cancelCoreWorkerTasksQueue(requestKind, {
            coreWorkerTasksQueueId: args.coreWorkerTasksQueueId,
            reasonCancellation: args.reasonCancellation
        });
    }

    return {
        receiveTasksInQueue,
        cancelCoreWorkerTasksQueue
    };
})();