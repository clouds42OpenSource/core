import { SelectDataCommonDataModel } from 'app/web/common/data-models';
import { TasksInQueueFilterDataModel } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData/data-models';

/**
 * Параметры для получения данных для карточки информации о задаче из очереди задач воркера
 */
export type ReceiveTasksInQueueParams = SelectDataCommonDataModel<TasksInQueueFilterDataModel>;