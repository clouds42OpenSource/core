import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { TaskInQueueItemDataModel } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData/data-models/TaskInQueueItemDataModel';

/**
 * Модель ответа при получении данных по очереди задач воркеров
 */
export type TasksInQueueDataModel = SelectDataResultMetadataModel<TaskInQueueItemDataModel>;