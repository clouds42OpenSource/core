/**
 * Модель задачи в очереди
 */
export type TaskInQueueItemDataModel = {
    /**
     * ID задачи
     */
    id: string;
    /**
     * Название задачи
     */
    name: string;
    /**
     * Статус
     */
    status: string;
    /**
     * Дата и время запуска
     */
    createDateTime: Date | null;
    /**
     * Дата и время выполнения
     */
    finishDateTime: Date | null;
    /**
     * ID воркера, который выполнил задачу
     */
    workerId: number | null;
    /**
     * Комментарий к задаче
     */
    comment: string;
};