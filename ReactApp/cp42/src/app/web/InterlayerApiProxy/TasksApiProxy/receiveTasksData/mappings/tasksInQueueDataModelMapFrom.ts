import { DateUtility } from 'app/utils';
import { TaskInQueueItemResponseDto } from 'app/web/api/TasksInQueueProxy/response-dto';
import { TaskInQueueItemDataModel } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData/data-models';

/**
 * Выполнить маппинг модели данных по очереди задач воркеров, полученной при запросе, в модель приложения
 * @param value Модели данных по очереди задач воркеров, полученная при запросе
 */
export function tasksInQueueDataModelMapFrom(value: TaskInQueueItemResponseDto): TaskInQueueItemDataModel {
    return {
        ...value,
        createDateTime: value.createDateTime ? DateUtility.convertToDate(value.createDateTime) : null,
        finishDateTime: value.finishDateTime ? DateUtility.convertToDate(value.finishDateTime) : null
    };
}