/**
 * Модель фильтра поиска задач в очереди
 */
export type TasksInQueueFilterDataModel = {
    /**
     * Номер воркера
     */
    workerId: number | null;
    /**
     * Название задачи
     */
    taskId: string | null;
    /**
     * Статус
     */
    status: string | null;
    /**
     * Период с
     */
    periodFrom: Date | null;
    /**
     * Период по
     */
    periodTo: Date | null;

    /**
     * Строка поиска
     */
    searchString: string | null;
};