import { adjustFilterDatePeriod } from 'app/common/functions';
import { getFilterPeriodString } from 'app/common/functions/getFilterPeriodString';
import { TasksInQueueFilterDataModel } from 'app/web/InterlayerApiProxy/TasksApiProxy/receiveTasksData/data-models';
import { ReceiveTasksInQueueFilterRequestDto } from 'app/web/api/TasksInQueueProxy/request-dto';

/**
 * Выполнить маппинг модели фильтра очереди задач воркеров в модель запроса на API метод
 * @param value Модель фильтра очереди задач воркеров
 */
export function receiveTasksInQueueFilterRequestDtoMapFrom(value: TasksInQueueFilterDataModel): ReceiveTasksInQueueFilterRequestDto {
    return {
        /**
         * Номер воркера
         */
        WorkerId: value.workerId,
        /**
         * Название задачи
         */
        TaskId: value.taskId,
        /**
         * Статус
         */
        Status: value.status,
        /**
         * Период с
         */
        PeriodFrom: getFilterPeriodString(adjustFilterDatePeriod(value.periodFrom, false)),
        /**
         * Период по
         */
        PeriodTo: getFilterPeriodString(adjustFilterDatePeriod(value.periodTo, true)),
        /**
         * Строка поиска
         */
        SearchLine: value.searchString
    };
}