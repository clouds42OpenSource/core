/**
 * Параметры для отмены запущенной задачи
 */
export type CancelCoreWorkerTasksQueueParams = {
    /**
     * Id запущенной задачи
     */
    coreWorkerTasksQueueId: string;

    /**
     * Причина отмены задачи
     */
    reasonCancellation: string;
};