export type CheckFileUploadStatusParams = {
    uploadFileId: string;
};