export type CheckFileUploadStatusModel = {
    result: boolean;
    errorMessage: string;
};