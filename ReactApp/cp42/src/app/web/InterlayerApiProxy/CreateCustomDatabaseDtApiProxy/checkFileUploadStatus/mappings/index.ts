import { CheckFileUploadStatusResponseDto } from 'app/web/api/CreateCustomDatabaseDt/response-dto/CheckFileUploadStatusResponseDto';
import { CheckFileUploadStatusModel } from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/checkFileUploadStatus/data-models';

export function checkFileUploadStatusMapFrom(value: CheckFileUploadStatusResponseDto): CheckFileUploadStatusModel {
    return {
        result: value.Result,
        errorMessage: value.ErrorMessage
    };
}