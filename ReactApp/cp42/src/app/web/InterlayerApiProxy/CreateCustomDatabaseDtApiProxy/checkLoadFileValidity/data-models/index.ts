export type CheckLoadFileValidityModel = {
    result: boolean;
    errorMessage: string;
};