import { CheckLoadFileValidityResponseDto } from 'app/web/api/CreateCustomDatabaseDt/response-dto/CheckLoadFileValidityResponseDto';
import { CheckLoadFileValidityModel } from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/checkLoadFileValidity/data-models';

export function checkLoadFileValidityMapFrom(value: CheckLoadFileValidityResponseDto): CheckLoadFileValidityModel {
    return {
        result: value.Result,
        errorMessage: value.ErrorMessage
    };
}