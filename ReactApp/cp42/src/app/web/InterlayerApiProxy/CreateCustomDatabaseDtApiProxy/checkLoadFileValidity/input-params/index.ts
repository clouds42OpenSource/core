export type CheckLoadFileValidityParams = {
    uploadFileId: string;
};