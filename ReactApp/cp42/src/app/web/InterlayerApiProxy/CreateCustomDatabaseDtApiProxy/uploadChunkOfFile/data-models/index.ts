export type UploadChunkOfFileModel = {
    result: boolean;
    errorMessage: string;
};