import { IoCContainer } from 'app/IoCСontainer';
import {
    CheckFileUploadStatusModel,
    CheckFileUploadStatusParams,
    checkFileUploadStatusMapFrom
} from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/checkFileUploadStatus';
import {
    CheckLoadFileValidityModel,
    CheckLoadFileValidityParams,
    checkLoadFileValidityMapFrom
} from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/checkLoadFileValidity';
import {
    CreateFromDtParams,
    CreateFromDtDataModel,
    createFromDtMapFrom
} from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/createFromDt';
import {
    InitMergeChecksToInitialFileProcessModel,
    InitMergeChunksToInitialFileProcessParams,
    initMergeChecksToInitialFileMapFrom
} from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/initMergeChunksToInitialFileProcess';
import {
    InitUploadProcessModel,
    InitUploadProcessParams,
    initUploadProcessMapFrom
} from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/initUploadProcess';
import {
    UploadChunkOfFileParams,
} from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/uploadChunkOfFile';
import { RequestKind } from 'core/requestSender/enums';

export const CreateCustomDatabaseDtApiProxy = (() => {
    const api = IoCContainer.getApiProxy();

    /**
     * Получить экземпляр прокси для работы с загрузкой файла частями
     */
    function getUploadFileApiProxy() {
        return api.getCreateCustomDatabaseDtProxy();
    }

    /**
     * Получить экземпляр прокси для создания базы из dt
     */
    function getCreateFromDtApiProxy() {
        return api.getCreateCustomDatabaseCoreDtProxy();
    }

    /**
     * Загрузить часть файла
     */
    async function uploadChunkOfFile(requestKind: RequestKind, args: UploadChunkOfFileParams) {
        return getUploadFileApiProxy().uploadChunkOfFile(requestKind, args);
    }

    /**
     * Проверить загруженность файла на соответствие требованиям
     */
    async function checkLoadFileValidity(requestKind: RequestKind, args: CheckLoadFileValidityParams): Promise<CheckLoadFileValidityModel> {
        return getUploadFileApiProxy().checkLoadFileValidity(requestKind, args)
            .then(response => checkLoadFileValidityMapFrom(response));
    }

    /**
     * Инициализация процесса загрузки файла
     */
    async function initUploadProcess(requestKind: RequestKind, args: InitUploadProcessParams): Promise<InitUploadProcessModel> {
        return getUploadFileApiProxy().initUploadProcess(requestKind, { FileName: args.fileName })
            .then(response => initUploadProcessMapFrom(response));
    }

    /**
     * Запуск процесса склейки частей в исходный файл
     */
    async function initMergeChunksToInitialFileProcess(requestKind: RequestKind, args: InitMergeChunksToInitialFileProcessParams): Promise<InitMergeChecksToInitialFileProcessModel> {
        return getUploadFileApiProxy().initMergeChunkToInitialFileProcess(requestKind, args)
            .then(response => initMergeChecksToInitialFileMapFrom(response));
    }

    /**
     * Проверка статуса загрузки бэекапа инф. базы
     */
    async function checkFileUploadStatus(requestKind: RequestKind, args: CheckFileUploadStatusParams): Promise<CheckFileUploadStatusModel> {
        return getUploadFileApiProxy().checkFileUploadStatus(requestKind, args)
            .then(response => checkFileUploadStatusMapFrom(response));
    }

    /**
     * Создания базы из dt
     */
    async function createFromDt(requestKind: RequestKind, args: CreateFromDtParams): Promise<CreateFromDtDataModel> {
        return getCreateFromDtApiProxy().CreateFromDt(
            requestKind,
            {
                ConfigurationId: args.configurationId,
                UsersIdForAddAccess: args.usersIdForAddAccess,
                DbCaption: args.dbCaption,
                NeedUserPromisePayment: args.needUsePromisePayment,
                UploadedFileId: args.uploadedFileId,
                ConfigurationName: args.configurationName,
                Password: args.password,
                CurrentVersion: args.currentVersion,
                Login: args.login,
                TypeFile: args.typeFile,
                HasAutoupdate: args.hasAutoupdate,
                HasSupport: args.hasSupport,
            }
        ).then(response => createFromDtMapFrom(response));
    }

    return {
        uploadChunkOfFile,
        checkLoadFileValidity,
        initUploadProcess,
        initMergeChunksToInitialFileProcess,
        checkFileUploadStatus,
        createFromDt
    };
})();