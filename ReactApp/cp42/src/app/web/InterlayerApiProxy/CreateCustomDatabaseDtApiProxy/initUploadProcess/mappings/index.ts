import { InitUploadProcessResponseDto } from 'app/web/api/CreateCustomDatabaseDt/response-dto/InitUploadProcessResponseDto';
import { InitUploadProcessModel } from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/initUploadProcess/data-models';

export function initUploadProcessMapFrom(value: InitUploadProcessResponseDto): InitUploadProcessModel {
    return {
        uploadedFileId: value.UploadedFileId
    };
}