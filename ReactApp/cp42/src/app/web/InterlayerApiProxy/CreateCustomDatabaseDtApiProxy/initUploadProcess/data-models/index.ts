export type InitUploadProcessModel = {
    uploadedFileId: string;
};