export type InitUploadProcessParams = {
    fileName: string;
};