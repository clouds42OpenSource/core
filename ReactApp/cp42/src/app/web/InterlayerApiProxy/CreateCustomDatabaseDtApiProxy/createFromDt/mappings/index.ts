import { CreateFromDtResponseDto } from 'app/web/api/CreateCustomDatabaseDt/response-dto/CreateFromDtResponseDto';
import { CreateFromDtDataModel } from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/createFromDt';

export function createFromDtMapFrom(value: CreateFromDtResponseDto): CreateFromDtDataModel {
    return {
        success: value.Success,
        data: value.Data
            ? {
                amount: value.Data.Amount,
                canUsePromisePayment: value.Data.CanUsePromisePayment,
                currency: value.Data.Currency,
                isComplete: value.Data.IsComplete,
                notEnoughMoney: value.Data.NotEnoughMoney,
                resourceForRedirect: value.Data.ResourceForRedirect
            }
            : null,
        message: value.Message
    };
}