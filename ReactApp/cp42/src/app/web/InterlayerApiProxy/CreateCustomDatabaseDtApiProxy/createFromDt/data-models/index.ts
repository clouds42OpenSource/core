export type CreateFromDtDataModel = {
    success: boolean;
    data: {
        isComplete: boolean;
        notEnoughMoney: number;
        amount: number;
        currency: string;
        canUsePromisePayment: boolean;
        resourceForRedirect: number
    } | null;
    message: string;
};