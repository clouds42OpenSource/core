import { ETypeFile } from 'app/web/api/CreateCustomDatabaseDt/request-dto';

export type CreateFromDtParams = {
    configurationId?: string;
    uploadedFileId: string;
    dbCaption: string;
    usersIdForAddAccess: string[];
    needUsePromisePayment: boolean;
    hasSupport: boolean;
    hasAutoupdate: boolean;
    configurationName?: string;
    login?: string;
    password?: string;
    typeFile?: ETypeFile;
    currentVersion?: string | null;
};