import { InitMergeChecksToInitialFileProcessResponseDto } from 'app/web/api/CreateCustomDatabaseDt/response-dto/InitMergeChecksToInitialFileProcessResponseDto';
import { InitMergeChecksToInitialFileProcessModel } from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy/initMergeChunksToInitialFileProcess/data-models';

export function initMergeChecksToInitialFileMapFrom(value: InitMergeChecksToInitialFileProcessResponseDto): InitMergeChecksToInitialFileProcessModel {
    return {
        result: value.Result,
        errorMessage: value.ErrorMessage
    };
}