export type InitMergeChecksToInitialFileProcessModel = {
    result: boolean;
    errorMessage: boolean;
};