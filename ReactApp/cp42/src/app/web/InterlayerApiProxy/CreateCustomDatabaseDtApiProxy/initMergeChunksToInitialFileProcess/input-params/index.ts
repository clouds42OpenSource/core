export type InitMergeChunksToInitialFileProcessParams = {
    uploadFileId: string;
    countOfChunks: number;
};