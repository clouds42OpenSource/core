/**
 * Параметры для удаления конфигурации 1С
 */
export type DeleteConfiguration1CParams = {
    /**
     * Название конфигурации
     */
    configurationName: string;
};