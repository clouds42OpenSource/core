import { ConfigurationReductionDataModel } from 'app/web/InterlayerApiProxy/Configurations1C/receiveConfigurations1C/data-models/ConfigurationReductionDataModel';

/**
 * Модель ответа на обновление конфигурации 1С
 */
export type UpdateConfiguration1CResultDataModel = {
    /**
     * Полные пути к архиву с информацией по обновлениям конфигурации.
     */
    reductions: Array<ConfigurationReductionDataModel>;
};