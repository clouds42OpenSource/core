import { AddOrUpdateConfiguration1CItemRequestDto } from 'app/web/api/Configurations1C/request-dto/AddOrUpdateConfigurations1CItemRequestDto';
import { UpdateConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1C/updateConfiguration1C/input-params';

/**
 * Mapping модели конфигурации 1С для обновления
 * @param value модель конфигурации 1С для обновления
 */
export function addOrUpdateConfiguration1CItemRequestDtoMapFrom(value: UpdateConfiguration1CParams): AddOrUpdateConfiguration1CItemRequestDto {
    return {
        Name: value.configurationName,
        ConfigurationCatalog: value.configurationCatalog,
        RedactionCatalogs: value.redactionCatalogs,
        PlatformCatalog: value.platformCatalog,
        UseComConnectionForApplyUpdates: value.useComConnectionForApplyUpdates,
        ShortCode: value.shortCode,
        UpdateCatalogUrl: value.updateCatalogUrl,
        NeedCheckUpdates: value.needCheckUpdates,
        ConfigurationCost: value.configurationCost,
        ConfigurationVariations: value.configurationVariations
    };
}