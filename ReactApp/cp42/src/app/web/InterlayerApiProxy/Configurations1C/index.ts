import { IoCContainer } from 'app/IoCСontainer';
import { AddConfiguration1CResultDataModel } from 'app/web/InterlayerApiProxy/Configurations1C/addConfiguration1C/data-models';
import { AddConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1C/addConfiguration1C/input-params';
import { addOrUpdateConfigurations1CItemRequestDtoMapFrom } from 'app/web/InterlayerApiProxy/Configurations1C/addConfiguration1C/mappings';
import { DeleteConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1C/deleteConfiguration1C/input-params';
import { configurations1CItemDataModelMapFrom, GetConfigurations1CResultDataModel } from 'app/web/InterlayerApiProxy/Configurations1C/receiveConfigurations1C';
import { ReceiveConfigurations1CParams } from 'app/web/InterlayerApiProxy/Configurations1C/receiveConfigurations1C/input-params';
import { UpdateConfiguration1CResultDataModel } from 'app/web/InterlayerApiProxy/Configurations1C/updateConfiguration1C/data-models';
import { UpdateConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1C/updateConfiguration1C/input-params';
import { addOrUpdateConfiguration1CItemRequestDtoMapFrom } from 'app/web/InterlayerApiProxy/Configurations1C/updateConfiguration1C/mappings';
import { RequestKind } from 'core/requestSender/enums';

export const Configurations1CApiProxy = (() => {
    function getConfigurations1CApiProxy() {
        const apiProxy = IoCContainer.getApiProxy();
        return apiProxy.getConfigurations1CProxy();
    }

    async function receiveConfigurations1C(requestKind: RequestKind, args: ReceiveConfigurations1CParams): Promise<GetConfigurations1CResultDataModel> {
        return getConfigurations1CApiProxy().receiveConfigurations1C(requestKind, {
            PageNumber: args.pageNumber,
            Filter: args.filter
                ? {
                    name: args.filter.configurationName,
                    configurationCatalog: args.filter.configurationCatalog
                } : null,
            orderBy: args.orderBy
        }).then(responseDto => ({
            records: responseDto.records.map(record => configurations1CItemDataModelMapFrom(record)),
            metadata: responseDto.metadata
        }));
    }

    async function updateConfiguration1C(requestKind: RequestKind, args: UpdateConfiguration1CParams): Promise<UpdateConfiguration1CResultDataModel> {
        return getConfigurations1CApiProxy().updateConfiguration1C(requestKind, { ...addOrUpdateConfiguration1CItemRequestDtoMapFrom(args) });
    }

    async function addConfiguration1C(requestKind: RequestKind, args: AddConfiguration1CParams): Promise<AddConfiguration1CResultDataModel> {
        return getConfigurations1CApiProxy().addConfiguration1C(requestKind, { ...addOrUpdateConfigurations1CItemRequestDtoMapFrom(args) });
    }

    async function deleteConfiguration1C(requestKind: RequestKind, args: DeleteConfiguration1CParams): Promise<void> {
        return getConfigurations1CApiProxy().deleteConfiguration1C(requestKind, {
            ConfigurationName: args.configurationName
        });
    }

    return {
        receiveConfigurations1C,
        updateConfiguration1C,
        addConfiguration1C,
        deleteConfiguration1C
    };
})();