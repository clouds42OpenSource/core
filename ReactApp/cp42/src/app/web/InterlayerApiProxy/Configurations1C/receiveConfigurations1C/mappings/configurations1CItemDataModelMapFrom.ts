import { Configurations1CItemResponseDto } from 'app/web/api/Configurations1C/response-dto';
import { Configurations1CItemDataModel } from 'app/web/InterlayerApiProxy/Configurations1C/receiveConfigurations1C/data-models';

/**
 * Mapping модели конфигурации 1С
 * @param value ответ модели конфигурации 1С
 */
export function configurations1CItemDataModelMapFrom(value: Configurations1CItemResponseDto): Configurations1CItemDataModel {
    return {
        configurationName: value.name ?? '',
        configurationCatalog: value.configurationCatalog ?? '',
        redactionCatalogs: value.redactionCatalogs ?? '',
        platformCatalog: value.platformCatalog ?? '',
        useComConnectionForApplyUpdates: value.useComConnectionForApplyUpdates ?? false,
        shortCode: value.shortCode ?? '',
        updateCatalogUrl: value.updateCatalogUrl ?? '',
        needCheckUpdates: value.needCheckUpdates ?? false,
        configurationCost: value.configurationCost,
        configurationVariations: value.configurationVariations ?? [],
        reductions: value.reductions.map(item => {
            return {
                redactionCatalog: item.redactionCatalog,
                urlOfMapping: item.urlOfMapping
            };
        })
    };
}