import { SelectDataCommonDataModel } from 'app/web/common/data-models';

/**
 * Фильтр
 */
type Filter = {
    /**
     * Название конфигурации
     */
    configurationName: string;

    /**
     * Каталог конфигурации на ИТС
     */
    configurationCatalog: string;
};

/**
 * Параметры для загрузки конфигураций 1С
 */
export type ReceiveConfigurations1CParams = SelectDataCommonDataModel<Filter>;