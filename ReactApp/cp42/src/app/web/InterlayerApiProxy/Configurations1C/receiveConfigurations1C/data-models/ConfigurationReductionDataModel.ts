/**
 * Модель редакции конфигурации.
 */
export type ConfigurationReductionDataModel = {
    /**
     * Каталоги редакции.
     */
    redactionCatalog: string;

    /**
     * Адрес по скачиванию карты обновлений редакций.
     */
    urlOfMapping: string;
};