import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { Configurations1CItemDataModel } from 'app/web/InterlayerApiProxy/Configurations1C/receiveConfigurations1C/data-models/Configurations1CItemDataModel';

/**
 * Модель ответа на получение списка конфигураций 1С
 */
export type GetConfigurations1CResultDataModel = SelectDataResultMetadataModel<Configurations1CItemDataModel>;