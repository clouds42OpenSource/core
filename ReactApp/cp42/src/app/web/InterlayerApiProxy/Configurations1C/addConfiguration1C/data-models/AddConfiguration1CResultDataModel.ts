import { ConfigurationReductionDataModel } from 'app/web/InterlayerApiProxy/Configurations1C/receiveConfigurations1C/data-models/ConfigurationReductionDataModel';

/**
 * Модель ответа на добавление конфигурации 1С
 */
export type AddConfiguration1CResultDataModel = {
    /**
     * Полные пути к архиву с информацией по обновлениям конфигурации.
     */
    reductions: Array<ConfigurationReductionDataModel>;
};