import { AddOrUpdateConfiguration1CItemRequestDto } from 'app/web/api/Configurations1C/request-dto/AddOrUpdateConfigurations1CItemRequestDto';
import { AddConfiguration1CParams } from 'app/web/InterlayerApiProxy/Configurations1C/addConfiguration1C/input-params';

/**
 * Mapping модели конфигурации 1С
 * @param value ответ модели конфигурации 1С
 */
export function addOrUpdateConfigurations1CItemRequestDtoMapFrom(value: AddConfiguration1CParams): AddOrUpdateConfiguration1CItemRequestDto {
    return {
        Name: value.configurationName,
        ConfigurationCatalog: value.configurationCatalog,
        RedactionCatalogs: value.redactionCatalogs,
        PlatformCatalog: value.platformCatalog,
        UseComConnectionForApplyUpdates: value.useComConnectionForApplyUpdates,
        ShortCode: value.shortCode,
        UpdateCatalogUrl: value.updateCatalogUrl,
        NeedCheckUpdates: value.needCheckUpdates,
        ConfigurationVariations: value.configurationVariations,
        ConfigurationCost: value.configurationCost
    };
}