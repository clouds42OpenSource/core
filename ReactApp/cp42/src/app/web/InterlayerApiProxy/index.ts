import { AccountApiProxy } from 'app/web/InterlayerApiProxy/AccountApiProxy';
import { AccountCardApiProxy } from 'app/web/InterlayerApiProxy/AccountCardApiProxy';
import { AccountDatabaseBackupArmApiProxy } from 'app/web/InterlayerApiProxy/AccountDatabaseBackupArmApiProxy';
import { AgencyAgreementApiProxy } from 'app/web/InterlayerApiProxy/AgencyAgreementApiProxy';
import { ArmAutoUpdateAccountDatabaseApiProxy } from 'app/web/InterlayerApiProxy/ArmAutoUpdateAccountDatabaseApiProxy';
import { BalanceManagementApiProxy } from 'app/web/InterlayerApiProxy/BalanceManagementApiProxy';
import { CloudCoreApiProxy } from 'app/web/InterlayerApiProxy/CloudCoreApiProxy';
import { CloudServicesApiProxy } from 'app/web/InterlayerApiProxy/CloudServicesApiProxy';
import { Configurations1CApiProxy } from 'app/web/InterlayerApiProxy/Configurations1CApiProxy';
import { CoreWorkerTasksApiProxy } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy';
import { DatabaseApiProxy } from 'app/web/InterlayerApiProxy/DatabaseApiProxy';
import { DatabaseCardApiProxy } from 'app/web/InterlayerApiProxy/DatabaseCardApiProxy';
import { DbTemplateApiProxy } from 'app/web/InterlayerApiProxy/DbTemplateApiProxy';
import { DbTemplateDelimetersApiProxy } from 'app/web/InterlayerApiProxy/DbTemplateDelimetersApiProxy';
import { DbTemplateUpdateApiProxy } from 'app/web/InterlayerApiProxy/DbTemplateUpdateApiProxy';
import { DelimiterSourceAccountDatabasesApiProxy } from 'app/web/InterlayerApiProxy/DelimiterSourceAccountDatabasesApiProxy';
import { GetUserPermissionsApiProxy } from 'app/web/InterlayerApiProxy/GetUserPermissionsApiProxy';
import { IndustryApiProxy } from 'app/web/InterlayerApiProxy/IndustryApiProxy';
import { InfoDbApiProxy } from 'app/web/InterlayerApiProxy/InfoDbApiProxy';
import { InfoDbCardApiProxy } from 'app/web/InterlayerApiProxy/InfoDbCardApiProxy';
import { ItsAuthorizationDataApiProxy } from 'app/web/InterlayerApiProxy/ItsAuthorizationDataApiProxy';
import { LocalesApiProxy } from 'app/web/InterlayerApiProxy/LocalesApiProxy';
import { LoggingApiProxy } from 'app/web/InterlayerApiProxy/LoggingApiProxy';
import { PrintedHtmlFormApiProxy } from 'app/web/InterlayerApiProxy/PrintedHtmlFormApiProxy';
import { ProcessFlowApiProxy } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy';
import { SegmentsApiProxy } from 'app/web/InterlayerApiProxy/SegmentsApiProxy';
import { ServiceAccountsApiProxy } from 'app/web/InterlayerApiProxy/ServiceAccountsApiProxy';
import { SuppliersApiProxy } from 'app/web/InterlayerApiProxy/SuppliersApiProxy';
import { TaskControlCardApiProxy } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy';
import { TaskInQueueItemCardApiProxy } from 'app/web/InterlayerApiProxy/TaskInQueueItemCardApiProxy';
import { TasksApiProxy } from 'app/web/InterlayerApiProxy/TasksApiProxy';
import { TasksPageApiProxy } from 'app/web/InterlayerApiProxy/TasksPageApiProxy';
import { UserManagementApiProxy } from 'app/web/InterlayerApiProxy/UserManagementApiProxy';
import { WorkerAvailableTasksBagsApiProxy } from 'app/web/InterlayerApiProxy/WorkerAvailableTasksBagsApiProxy';
import { WorkersApiProxy } from 'app/web/InterlayerApiProxy/WorkersApiProxy';
import { WorkerTasksBagsControlCardApiProxy } from 'app/web/InterlayerApiProxy/WorkerTasksBagsControlCardApiProxy';
import { AdditionalSessionsApiProxy } from 'app/web/InterlayerApiProxy/AdditionalSessionsApiProxy';
import { ServiceApiProxy } from 'app/web/InterlayerApiProxy/ServiceApiProxy';
import { MyCompanyApiProxy, MyCompanySegmentApiProxy, DatabaseMigrationApiProxy } from 'app/web/InterlayerApiProxy/MyCompanyApiProxy';
import { AutoPaymentApiProxy } from 'app/web/InterlayerApiProxy/AutoPaymentApiProxy';
import { MsBackupsApiProxy } from 'app/web/InterlayerApiProxy/MsBackupsApiProxy';
import { CreateCustomDatabaseDtApiProxy } from 'app/web/InterlayerApiProxy/CreateCustomDatabaseDtApiProxy';
import { LogEventApiProxy } from 'app/web/InterlayerApiProxy/LogEventApiProxy';

export const InterlayerApiProxy = {
    getLoggingApi: () => LoggingApiProxy,
    getDatabaseCardApi: () => DatabaseCardApiProxy,
    getDatabaseApi: () => DatabaseApiProxy,
    getAccountCardApi: () => AccountCardApiProxy,
    getAccountApi: () => AccountApiProxy,
    getTasksApi: () => TasksApiProxy,
    getWorkersApi: () => WorkersApiProxy,
    getCoreWorkerTasksApi: () => CoreWorkerTasksApiProxy,
    getInfoDbApi: () => InfoDbApiProxy,
    getInfoDbCardApi: () => InfoDbCardApiProxy,
    getTaskInQueueItemCardApiProxy: () => TaskInQueueItemCardApiProxy,
    getTaskControlCardApi: () => TaskControlCardApiProxy,
    getCloudServicesApi: () => CloudServicesApiProxy,
    getTasksPageApiProxy: () => TasksPageApiProxy,
    getWorkerAvailableTasksBagsApiProxy: () => WorkerAvailableTasksBagsApiProxy,
    getProcessFlowApi: () => ProcessFlowApiProxy,
    getDelimiterSourceAccountDatabasesApi: () => DelimiterSourceAccountDatabasesApiProxy,
    getWorkerTasksBagsControlCardApiProxy: () => WorkerTasksBagsControlCardApiProxy,
    getServiceAccountsApi: () => ServiceAccountsApiProxy,
    getConfigurations1CApiProxy: () => Configurations1CApiProxy,
    getIndustryApiProxy: () => IndustryApiProxy,
    getDbTemplateApiProxy: () => DbTemplateApiProxy,
    getDbTemplateDelimetersApiProxy: () => DbTemplateDelimetersApiProxy,
    getCloudCoreApi: () => CloudCoreApiProxy,
    getAgencyAgreementApiProxy: () => AgencyAgreementApiProxy,
    getPrintedHtmlFormApiProxy: () => PrintedHtmlFormApiProxy,
    getAccountDatabaseBackupArmApiProxy: () => AccountDatabaseBackupArmApiProxy,
    getDbTemplateUpdateApiProxy: () => DbTemplateUpdateApiProxy,
    getSuppliersApiProxy: () => SuppliersApiProxy,
    getLocalesApiProxy: () => LocalesApiProxy,
    getSegmentsApiProxy: () => SegmentsApiProxy,
    getItsAuthorizationDataApiProxy: () => ItsAuthorizationDataApiProxy,
    getArmAutoUpdateAccountDatabaseApiProxy: () => ArmAutoUpdateAccountDatabaseApiProxy,
    getBalanceManagementApiProxy: () => BalanceManagementApiProxy,
    getUserPermissionsApiProxy: () => GetUserPermissionsApiProxy,
    getUserManagementApiProxy: () => UserManagementApiProxy,
    getMyCompanyApiProxy: () => MyCompanyApiProxy,
    getMyCompanySegmentApiProxy: () => MyCompanySegmentApiProxy,
    getDatabaseMigrationApiProxy: () => DatabaseMigrationApiProxy,
    getAdditionalSessionsApiProxy: () => AdditionalSessionsApiProxy,
    getServicesApiProxy: () => new ServiceApiProxy(),
    getLogEventApiProxy: () => new LogEventApiProxy(),
    getAutoPaymentApiProxy: () => AutoPaymentApiProxy,
    getMsBackupsApiProxy: () => MsBackupsApiProxy,
    getCreateCustomDatabaseDtApiProxy: () => CreateCustomDatabaseDtApiProxy
};