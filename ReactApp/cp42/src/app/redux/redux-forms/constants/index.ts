/**
 * Константы для модуля ReduxForms
 */
export const ReduxFormsConsts = {
    /**
     * Название редюсера в модуле ReduxForms
     */
    reducerName: 'REDUX_FORMS'
};