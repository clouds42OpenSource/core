/**
 * Процесы ReduxForms
 */
export enum ReduxFormsProcessId {
    /**
     * Процесс инициализации Redux формы
     */
    InitializeReduxForm = 'INITIALIZE_REDUX_FORM',

    /**
     * Процесс обновления полей Redux формы
     */
    UpdateReduxFormFields = 'UPDATE_REDUX_FORM_FIELDS',

    /**
     * Процесс удаления Redux формы
     */
    ResetReduxForm = 'RESET_FORM'
}