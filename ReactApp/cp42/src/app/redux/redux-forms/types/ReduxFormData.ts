export type ReduxFormData<TFormData> = {
    isInitialized: boolean;
    fields: TFormData;
};