import { ReduxFormsActions } from 'app/redux/redux-forms/store/actions';
import { ResetReduxFormParams } from 'app/redux/redux-forms/store/reducers/resetReduxFormReducer/params';
import { ResetReduxFormActionFailedPayload, ResetReduxFormActionStartPayload, ResetReduxFormActionSuccessPayload } from 'app/redux/redux-forms/store/reducers/resetReduxFormReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

type TActionStartPayload = ResetReduxFormActionStartPayload;
type TActionSuccessPayload = ResetReduxFormActionSuccessPayload;
type TActionFailedPayload = ResetReduxFormActionFailedPayload;

type TInputParams = ResetReduxFormParams;

const { ResetReduxForm: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ReduxFormsActions;

type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

export class ResetReduxFormThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    public static invoke(args?: TInputParams) {
        return new ResetReduxFormThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().ReduxForms,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const inputArgs = args.inputParams!;
        args.success({
            form: inputArgs.form
        });
    }
}