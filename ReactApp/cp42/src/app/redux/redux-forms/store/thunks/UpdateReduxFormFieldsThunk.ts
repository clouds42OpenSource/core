import { ReduxFormsActions } from 'app/redux/redux-forms/store/actions';
import { UpdateReduxFormFieldsParams } from 'app/redux/redux-forms/store/reducers/updateReduxFormFieldsReducer/params';
import { UpdateReduxFormFieldsActionFailedPayload, UpdateReduxFormFieldsActionStartPayload, UpdateReduxFormFieldsActionSuccessPayload } from 'app/redux/redux-forms/store/reducers/updateReduxFormFieldsReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';

type TActionStartPayload = UpdateReduxFormFieldsActionStartPayload;
type TActionSuccessPayload = UpdateReduxFormFieldsActionSuccessPayload;
type TActionFailedPayload = UpdateReduxFormFieldsActionFailedPayload;

type TInputParams<TFormData> = UpdateReduxFormFieldsParams<TFormData>;

const { UpdateReduxFormFields: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ReduxFormsActions;

type TThunkStartActionParams<TFormData> = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams<TFormData>>;
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;
type TThunkStartExecutionParams<TFormData> = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams<TFormData>>;

export class UpdateReduxFormFieldsThunk<TFormData> extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams<TFormData>> {
    public static invoke<TFormData>(args?: TInputParams<TFormData>) {
        return new UpdateReduxFormFieldsThunk().execute(args);
    }

    protected get startActionValue(): string { return START_ACTION; }

    protected get successActionValue(): string { return SUCCESS_ACTION; }

    protected get failedActionValue(): string { return FAILED_ACTION; }

    protected getStartActionArguments(args: TThunkStartActionParams<TFormData>): TThunkStartActionResult {
        return {
            condition: true,
            getReducerStateFunc: () => args.getStore().ReduxForms,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams<TFormData>): Promise<void> {
        const inputArgs = args.inputParams!;
        args.success({
            form: inputArgs.form,
            data: { ...inputArgs.data }
        });
    }
}