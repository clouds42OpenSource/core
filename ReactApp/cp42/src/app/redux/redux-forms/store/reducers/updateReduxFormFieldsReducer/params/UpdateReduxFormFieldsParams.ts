export type UpdateReduxFormFieldsParams<TFormData> = {
    form: string;
    data: Partial<{ [key in keyof TFormData]: any }>;
};