import { ReduxFormsProcessId } from 'app/redux/redux-forms/ReduxFormsProcessId';
import { ReduxFormsActions } from 'app/redux/redux-forms/store/actions';
import { InitializeReduxFormActionAnyPayload, InitializeReduxFormActionFailedPayload, InitializeReduxFormActionSuccessPayload } from 'app/redux/redux-forms/store/reducers/initializeReduxFormReducer/payloads';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { reducerStateProcessFail, reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { reducerStateProcessReset } from 'core/redux/functions/reducerStateProcessReset';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для инициализации Redux формы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const initializeReduxFormReducer: TPartialReducerObject<ReduxFormsReducerState<any>, InitializeReduxFormActionAnyPayload> =
    (state, action) => {
        const processId = ReduxFormsProcessId.InitializeReduxForm;
        const { InitializeReduxForm: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION, RESET_ACTION } } = ReduxFormsActions;

        switch (action.type) {
            case START_ACTION: {
                return reducerStateProcessStart(state, processId);
            }

            case SUCCESS_ACTION: {
                const payload = action.payload as InitializeReduxFormActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    [payload.form]: {
                        isInitialized: true,
                        fields: { ...payload.data }
                    }
                });
            }

            case FAILED_ACTION: {
                const payload = action.payload as InitializeReduxFormActionFailedPayload;
                return reducerStateProcessFail(state, processId, payload.error);
            }

            case RESET_ACTION: {
                return reducerStateProcessReset(state, processId);
            }

            default:
                return null;
        }
    };