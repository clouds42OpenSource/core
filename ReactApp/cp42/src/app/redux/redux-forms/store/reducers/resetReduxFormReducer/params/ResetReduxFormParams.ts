/**
 * Модель на запрос обновления полей Redux формы
 */
export type ResetReduxFormParams = {
    /**
     * какую Redux форму удалить
     */
    form: string | string[];
};