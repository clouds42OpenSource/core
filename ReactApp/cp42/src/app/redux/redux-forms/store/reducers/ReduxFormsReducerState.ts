import { ReduxFormsProcessId } from 'app/redux/redux-forms/ReduxFormsProcessId';
import { ReduxFormData } from 'app/redux/redux-forms/types/ReduxFormData';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с Redux формой
 */
export type ReduxFormsReducerState<TFormData> = IReducerState<ReduxFormsProcessId> & Record<string, ReduxFormData<TFormData>>;