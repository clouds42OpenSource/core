import { ReduxFormsConsts } from 'app/redux/redux-forms/constants';
import { initializeReduxFormReducer } from 'app/redux/redux-forms/store/reducers/initializeReduxFormReducer';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { resetReduxFormReducer } from 'app/redux/redux-forms/store/reducers/resetReduxFormReducer';
import { updateReduxFormFieldsReducer } from 'app/redux/redux-forms/store/reducers/updateReduxFormFieldsReducer';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера AccountCard
 */
const initialState = initReducerState<ReduxFormsReducerState<any>>(
    ReduxFormsConsts.reducerName,
    {}
);

/**
 * Объединённые части редюсера для всего состояния ReduxForms
 */
const partialReducers = [
    initializeReduxFormReducer,
    updateReduxFormFieldsReducer,
    resetReduxFormReducer
];

/**
 * Редюсер ReduxForms
 */
export const reduxFormsReducer = createReducer(initialState, partialReducers);