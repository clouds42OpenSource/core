import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте инициализации Redux формы
 */
export type InitializeReduxFormActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки инициализации Redux формы
 */
export type InitializeReduxFormActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешной инициализации Redux формы
 */
export type InitializeReduxFormActionSuccessPayload = ReducerActionSuccessPayload & {
    /**
     * Название Redux формы
     */
    form: string;
    /**
     * Поля формы
     */
    data: { [key: string]: any }
};

/**
 * Все возможные Action при инициализации Redux формы
 */
export type InitializeReduxFormActionAnyPayload =
    | InitializeReduxFormActionStartPayload
    | InitializeReduxFormActionSuccessPayload
    | InitializeReduxFormActionFailedPayload;