/**
 * Модель на запрос инициализации Redux формы
 */
export type InitializeReduxFormParams<TFormData> = {
    /**
     * Для какой формы провести инициализацию полей
     */
    form: string;

    /**
     * Поля для инициализации
     */
    data: { [key in keyof TFormData]: any };
};