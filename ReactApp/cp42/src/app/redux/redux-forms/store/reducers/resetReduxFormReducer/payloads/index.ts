import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте удаления Redux формы
 */
export type ResetReduxFormActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки удаления Redux формы
 */
export type ResetReduxFormActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешной удаления Redux формы
 */
export type ResetReduxFormActionSuccessPayload = ReducerActionSuccessPayload & {
    form: string | string[];
};

/**
 * Все возможные Action при удаления Redux формы
 */
export type ResetReduxFormActionAnyPayload =
    | ResetReduxFormActionStartPayload
    | ResetReduxFormActionSuccessPayload
    | ResetReduxFormActionFailedPayload;