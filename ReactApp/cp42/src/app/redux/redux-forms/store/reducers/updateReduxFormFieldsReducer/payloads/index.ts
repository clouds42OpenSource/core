import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте обновления полей Redux формы
 */
export type UpdateReduxFormFieldsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки обновления полей Redux формы
 */
export type UpdateReduxFormFieldsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешной обновления полей Redux формы
 */
export type UpdateReduxFormFieldsActionSuccessPayload = ReducerActionSuccessPayload & {
    form: string;
    data: Partial<{ [key: string]: any }>
};

/**
 * Все возможные Action при обновления полей Redux формы
 */
export type UpdateReduxFormFieldsActionAnyPayload =
    | UpdateReduxFormFieldsActionStartPayload
    | UpdateReduxFormFieldsActionSuccessPayload
    | UpdateReduxFormFieldsActionFailedPayload;