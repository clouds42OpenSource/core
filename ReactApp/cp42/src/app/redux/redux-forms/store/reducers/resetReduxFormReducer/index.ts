import { ReduxFormsProcessId } from 'app/redux/redux-forms/ReduxFormsProcessId';
import { ReduxFormsActions } from 'app/redux/redux-forms/store/actions';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { ResetReduxFormActionAnyPayload, ResetReduxFormActionFailedPayload, ResetReduxFormActionSuccessPayload } from 'app/redux/redux-forms/store/reducers/resetReduxFormReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { reducerStateProcessReset } from 'core/redux/functions/reducerStateProcessReset';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления Redux формы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const resetReduxFormReducer: TPartialReducerObject<ReduxFormsReducerState<any>, ResetReduxFormActionAnyPayload> =
    (state, action) => {
        const processId = ReduxFormsProcessId.ResetReduxForm;
        const { ResetReduxForm: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION, RESET_ACTION } } = ReduxFormsActions;

        switch (action.type) {
            case START_ACTION: {
                return reducerStateProcessStart(state, processId);
            }

            case SUCCESS_ACTION: {
                const payload = action.payload as ResetReduxFormActionSuccessPayload;

                const resetForms: { [key: string]: any } = {};

                if (payload.form instanceof Array) {
                    for (const form of payload.form) {
                        resetForms[form] = undefined;
                    }
                } else {
                    resetForms[payload.form] = undefined;
                }

                return reducerStateProcessSuccess(state, processId, {
                    ...resetForms
                });
            }

            case FAILED_ACTION: {
                const payload = action.payload as ResetReduxFormActionFailedPayload;
                return reducerStateProcessFail(state, processId, payload.error);
            }

            case RESET_ACTION: {
                return reducerStateProcessReset(state, processId);
            }

            default:
                return null;
        }
    };