import { ReduxFormsProcessId } from 'app/redux/redux-forms/ReduxFormsProcessId';
import { ReduxFormsActions } from 'app/redux/redux-forms/store/actions';
import { ReduxFormsReducerState } from 'app/redux/redux-forms/store/reducers/ReduxFormsReducerState';
import { UpdateReduxFormFieldsActionAnyPayload, UpdateReduxFormFieldsActionFailedPayload, UpdateReduxFormFieldsActionSuccessPayload } from 'app/redux/redux-forms/store/reducers/updateReduxFormFieldsReducer/payloads';
import { reducerStateProcessFail, reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { reducerStateProcessReset } from 'core/redux/functions/reducerStateProcessReset';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для обновления полей Redux формы
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateReduxFormFieldsReducer: TPartialReducerObject<ReduxFormsReducerState<any>, UpdateReduxFormFieldsActionAnyPayload> =
    (state, action) => {
        const processId = ReduxFormsProcessId.UpdateReduxFormFields;
        const { UpdateReduxFormFields: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION, RESET_ACTION } } = ReduxFormsActions;

        switch (action.type) {
            case START_ACTION: {
                return reducerStateProcessStart(state, processId);
            }

            case SUCCESS_ACTION: {
                const payload = action.payload as UpdateReduxFormFieldsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    [payload.form]: {
                        ...state[payload.form],
                        fields: {
                            ...state[payload.form]?.fields,
                            ...payload.data
                        }
                    }
                });
            }

            case FAILED_ACTION: {
                const payload = action.payload as UpdateReduxFormFieldsActionFailedPayload;
                return reducerStateProcessFail(state, processId, payload.error);
            }

            case RESET_ACTION: {
                return reducerStateProcessReset(state, processId);
            }

            default:
                return null;
        }
    };