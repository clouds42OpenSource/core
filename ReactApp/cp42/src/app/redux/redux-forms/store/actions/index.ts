import { ReduxFormsConsts } from 'app/redux/redux-forms/constants';
import { ReduxFormsProcessId } from 'app/redux/redux-forms/ReduxFormsProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера ReduxForms
 */
export const ReduxFormsActions = {
    /**
     * Набор action на инициализацию Redux формы
     */
    InitializeReduxForm: createReducerActions(ReduxFormsConsts.reducerName, ReduxFormsProcessId.InitializeReduxForm),

    /**
     * Набор action для обновления полей Redux формы
     */
    UpdateReduxFormFields: createReducerActions(ReduxFormsConsts.reducerName, ReduxFormsProcessId.UpdateReduxFormFields),

    /**
     * Набор action для удаления Redux формы
     */
    ResetReduxForm: createReducerActions(ReduxFormsConsts.reducerName, ReduxFormsProcessId.ResetReduxForm)
};