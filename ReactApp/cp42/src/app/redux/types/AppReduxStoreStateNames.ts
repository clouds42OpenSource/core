import { AppReduxStoreReducers } from 'app/redux';

export type AppReduxStoreStateNames = keyof typeof AppReduxStoreReducers;