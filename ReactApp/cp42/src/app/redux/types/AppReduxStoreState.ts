import { AppReduxStoreReducers } from 'app/redux';

export type AppReduxStoreState<T = typeof AppReduxStoreReducers> = {
    [P in keyof T]: T[P] extends (...args: any) => infer R ? R : any;
};