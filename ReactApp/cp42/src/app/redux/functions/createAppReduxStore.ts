import { AppReduxStoreReducers } from 'app/redux';
import { AppReduxStoreStateNames } from 'app/redux/types';
import { createStore } from 'core/redux/functions';
import { StoreEnhancer } from 'redux';

/**
 * Создаёт Redux хранилище для приложения
 * @param enhancer middleware при создании хранилища
 */
export const createAppReduxStore = (enhancer: StoreEnhancer) => createStore<AppReduxStoreStateNames>(AppReduxStoreReducers, enhancer);