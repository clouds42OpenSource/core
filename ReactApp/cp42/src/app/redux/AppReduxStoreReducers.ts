import { autoPaymentReducer } from 'app/modules/AccountManagement/AutoPayment/store/reducers';
import { balanceManagementReducer } from 'app/modules/AccountManagement/BalanceManagement/store/reducers';
import { userManagementReducer } from 'app/modules/AccountManagement/UserManagement/store/reducers';
import { agencyAgreementReducer } from 'app/modules/AgencyAgreement/store/reducers';
import { dbTemplatesReducer } from 'app/modules/DbTemplates/reducers';
import { processFlowReducer } from 'app/modules/ProcessFlow/store/reducers';
import { accountCardReducer } from 'app/modules/accountCard/store/reducers';
import { AccountCredentialsReducer } from 'app/modules/accountCredentials/store/reducers/AccountCredentialsState';
import { AccountDatabaseBackupArmReducer } from 'app/modules/accountDatabaseBackupArm/store/reducers';
import { accountDatabasesForMigrationReducer } from 'app/modules/accountDatabasesForMigrations/store/reducers';
import { accountListReducer } from 'app/modules/accountList/store/reducers';
import { additionalSessionReducer } from 'app/modules/additionalSessions/store/reducers/AdditionalSessionsReducer';
import { ArmAutoUpdateAccountDatabaseReducer } from 'app/modules/armAutoUpdateAccountDatabase/store/reducers';
import { CloudCoreReducer } from 'app/modules/cloudCore/store/reducers';
import { cloudServicesReducer } from 'app/modules/cloudServices/store/reducers/cloudServicesReducer';
import { configurations1CReducer } from 'app/modules/configurations1C/store/reducers';
import { coreWorkerTasksReducer } from 'app/modules/coreWorkerTasks/store/reducers/coreWorkerTasksReducer';
import { databaseCardReducer } from 'app/modules/databaseCard/store/reducers';
import { databaseListReducer } from 'app/modules/databaseList/store/reducers/databaseListReducer';
import { dbTemplateDelimetersReducer } from 'app/modules/dbTemplateDelimeters/reducers/dbTemplateDelimetersReducer';
import { DbTemplateUpdateReducer } from 'app/modules/dbTemplateUpdate/store/reducers';
import { delimiterSourceAccountDatabasesReducer } from 'app/modules/delimiterSourceAccountDatabases/store/reducers';
import { industryReducer } from 'app/modules/industry/store/reducers';
import { infoDbCardReducer } from 'app/modules/infoDbCard/store/reducers';
import { infoDbListReducer } from 'app/modules/infoDbList/store/reducers';
import { infoDbListOtherReducer } from 'app/modules/infoDbList/store/reducers/infoDbListOtherReducer';
import { ItsAuthorizationDataReducer } from 'app/modules/itsAuthorizationData/store/reducers';
import { localesReducer } from 'app/modules/locales/store/reducers';
import { loggingReducer } from 'app/modules/logging/store/reducers';
import { myCompanyReducer } from 'app/modules/myCompany/store/reducers/MyCompanyReducer';
import { PrintedHtmlFormsReducer } from 'app/modules/printedHtmlFormsReference/store/reducers';
import { TabArrServersReducer } from 'app/modules/segments/TabArrServers/store/reducer';
import { TabBackupStorageReducer } from 'app/modules/segments/TabBackupStorage/store/reducer';
import { TabContentServersReducer } from 'app/modules/segments/TabContentServers/store/reducer';
import { TabEnterpriseServersReducer } from 'app/modules/segments/TabEnterpriseServers/store/reducer/TabEnterpriseServersReducer';
import { TabFileStoragesReducer } from 'app/modules/segments/TabFileStorages/store/reducer';
import { TabHostingReducer } from 'app/modules/segments/TabHosting/store/reducer';
import { TabPlatformVersionReducer } from 'app/modules/segments/TabPlatformVersion/store/reducer';
import { TabPublishNodesReducer } from 'app/modules/segments/TabPublishNodes/store/reducer';
import { SegmentsReducer } from 'app/modules/segments/TabSegments/store/reducers/SegmentsReducer';
import { TabServerFarmReducer } from 'app/modules/segments/TabServerFarms/store/reducer';
import { TabSqlServersReducer } from 'app/modules/segments/TabSqlServers/store/reducer';
import { TabTerminalFarmsReducer } from 'app/modules/segments/TabTerminalFarms/store/reducers/TabTerminalFarmsReducer';
import { TabTerminalGatewayReducer } from 'app/modules/segments/TabTerminalGateway/store/reducer';
import { TabTerminalServerReducer } from 'app/modules/segments/TabTerminalServer/store/reducer';
import { serviceAccountsReducer } from 'app/modules/serviceAccounts/store/reducers';
import { SuppliersReducer } from 'app/modules/suppliers/store/reducers';
import { taskControlCardReducer } from 'app/modules/taskControlCard/store/reducers/taskControlCardReducer';
import { taskInQueueItemCardReducer } from 'app/modules/taskInQueueItemCard/store/reducers';
import { tasksPageReducer } from 'app/modules/tasksPage/store/reducers';
import { tasksReducer } from 'app/modules/tasksTable/store/reducers';
import { workerAvailableTasksBagsReducer } from 'app/modules/workerAvailableTasksBags/store/reducers';
import { workerTasksBagsControlCardReducer } from 'app/modules/workerTasksBagsControlCard/store/reducers';
import { workersReducer } from 'app/modules/workers/store/reducers/workersReducer';
import { reduxFormsReducer } from 'app/redux/redux-forms/store/reducers/reduxFormsReducer';

import { PartnersReducer } from 'app/modules/partners';
import { MarketingReducer } from 'app/modules/marketing';
import { ArmManagerReducer } from 'app/modules/armManager';
import { MainPageReducer } from 'app/modules/main';
import { AccountUsersReducer } from 'app/modules/accountUsers';
import { AccountDatabasesReducer } from 'app/modules/accountDatabases';
import { ArticlesReducer } from 'app/modules/articles';
import { UsersPageReducer } from 'app/modules/users';
import { GlobalReducer } from 'app/modules/global';
import { Link42Reducer } from 'app/modules/link42';
import { NotificationsReducer } from 'app/modules/notifications';
import { AccountsReducer } from 'app/modules/accounts';
import { CombineReducerPersonalSessions } from 'app/modules/additionalSessions';
import { ApdexReducer } from 'app/modules/Apdex';
import { InformationBasesReducer } from 'app/modules/informationBases';

/**
 * Редюсеры Redux хранилища приложения
 * !Не использовать напрямую!
 */
export const AppReduxStoreReducers = {
    ReduxForms: reduxFormsReducer,
    CloudServicesState: cloudServicesReducer,
    LoggingState: loggingReducer,
    DatabaseCardState: databaseCardReducer,
    AccountCardState: accountCardReducer,
    TasksInQueueState: tasksReducer,
    WorkersState: workersReducer,
    CoreWorkerTasksState: coreWorkerTasksReducer,
    TaskInQueueItemCardState: taskInQueueItemCardReducer,
    TaskControlCardState: taskControlCardReducer,
    TasksPageState: tasksPageReducer,
    WorkerAvailableTasksBagsReducerState: workerAvailableTasksBagsReducer,
    ProcessFlowState: processFlowReducer,
    WorkerTasksBagsControlCardState: workerTasksBagsControlCardReducer,
    DelimiterSourceAccountDatabasesState: delimiterSourceAccountDatabasesReducer,
    ServiceAccountsState: serviceAccountsReducer,
    Configurations1CState: configurations1CReducer,
    IndustryState: industryReducer,
    DbTemplatesState: dbTemplatesReducer,
    DbTemplateDelimetersState: dbTemplateDelimetersReducer,
    AgencyAgreementState: agencyAgreementReducer,
    DatabaseListState: databaseListReducer,
    InfoDbListState: infoDbListReducer,
    InfoDbListOtherState: infoDbListOtherReducer,
    InfoDbCardState: infoDbCardReducer,
    AccountListState: accountListReducer,
    AccountDatabasesForMigrationState: accountDatabasesForMigrationReducer,
    CloudCoreState: CloudCoreReducer,
    AccountDatabaseBackupArmState: AccountDatabaseBackupArmReducer,
    DbTemplateUpdateState: DbTemplateUpdateReducer,
    SuppliersState: SuppliersReducer,
    LocalesState: localesReducer,
    AutoPaymentState: autoPaymentReducer,
    BalanceManagementState: balanceManagementReducer,
    UserManagementState: userManagementReducer,
    PrintedHtmlFormsState: PrintedHtmlFormsReducer,
    MyCompanyState: myCompanyReducer,
    AdditionalSessionState: additionalSessionReducer,
    SegmentsState: SegmentsReducer,
    TabTerminalFarmsState: TabTerminalFarmsReducer,
    TabEnterpriseServersState: TabEnterpriseServersReducer,
    TabContentServersState: TabContentServersReducer,
    TabPublishNodesState: TabPublishNodesReducer,
    TabFileStoragesState: TabFileStoragesReducer,
    TabSqlServersState: TabSqlServersReducer,
    TabBackupStorageState: TabBackupStorageReducer,
    TabTerminalGatewayState: TabTerminalGatewayReducer,
    TabTerminalServerState: TabTerminalServerReducer,
    TabPlatformVersionState: TabPlatformVersionReducer,
    TabHostingState: TabHostingReducer,
    TabServerFarmState: TabServerFarmReducer,
    TabArrServersState: TabArrServersReducer,
    ItsAuthorizationDataState: ItsAuthorizationDataReducer,
    ArmAutoUpdateAccountDatabaseState: ArmAutoUpdateAccountDatabaseReducer,
    AccountCredentialsState: AccountCredentialsReducer,
    PartnersState: PartnersReducer,
    MarketingState: MarketingReducer,
    ArmManagerState: ArmManagerReducer,
    MainPageState: MainPageReducer,
    AccountUsersState: AccountUsersReducer,
    AccountDatabasesState: AccountDatabasesReducer,
    Global: GlobalReducer,
    Notifications: NotificationsReducer,
    PersonalAdditionalSession: CombineReducerPersonalSessions,
    ArticlesState: ArticlesReducer,
    Link42State: Link42Reducer,
    Users: UsersPageReducer,
    Accounts: AccountsReducer,
    Apdex: ApdexReducer,
    InformationBases: InformationBasesReducer
};