import { composeWithDevTools } from 'redux-devtools-extension';
import { applyMiddleware, combineReducers, legacy_createStore as createStore } from 'redux';
import thunk from 'redux-thunk';

import { AppReduxStoreReducers } from './AppReduxStoreReducers';

const rootReducer = combineReducers(AppReduxStoreReducers);

export const setupStore = () => createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export type TRootState = ReturnType<typeof rootReducer>;
export type TAppStore = ReturnType<typeof setupStore>;
export type TAppDispatch = TAppStore['dispatch'];