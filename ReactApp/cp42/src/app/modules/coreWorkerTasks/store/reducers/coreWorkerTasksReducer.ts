import { getInitialMetadata } from 'app/common/functions';
import { CoreWorkerTasksConstants } from 'app/modules/coreWorkerTasks/consts';
import { CoreWorkerTasksReducerState } from 'app/modules/coreWorkerTasks/store/reducers/CoreWorkerTasksReducerState';
import { receiveCoreWorkerTasksDataReducer } from 'app/modules/coreWorkerTasks/store/reducers/receiveCoreWorkerTasksData';
import { receiveTasksControlDataReducer } from 'app/modules/coreWorkerTasks/store/reducers/receiveTasksControlData';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редьюсера
 */
const initialState = initReducerState<CoreWorkerTasksReducerState>(
    CoreWorkerTasksConstants.reducerName,
    {
        tasks: {
            items: []
        },
        tasksControlData: {
            items: [],
            metadata: getInitialMetadata(),
            filter: {
                taskId: ''
            }
        },
        hasSuccessFor: {
            hasTasksDataReceived: false,
            hasTasksControlDataReceived: false
        }
    }
);

const partialReducers = [
    receiveCoreWorkerTasksDataReducer,
    receiveTasksControlDataReducer
];

export const coreWorkerTasksReducer = createReducer(initialState, partialReducers);