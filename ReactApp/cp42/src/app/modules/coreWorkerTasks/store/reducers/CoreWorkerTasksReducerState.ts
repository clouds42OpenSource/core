import { CoreWorkerTasksProcessId } from 'app/modules/coreWorkerTasks/CoreWorkerTasksProcessId';
import { KeyValueDataModel, MetadataModel } from 'app/web/common/data-models';
import { TasksControlFilterDataModel, TasksControlItemDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для воркеров
 */
export type CoreWorkerTasksReducerState = IReducerState<CoreWorkerTasksProcessId> & {
    /**
     * Задачи воркера
     */
    tasks: {
        /**
         * Массив записей
         */
        items: Array<KeyValueDataModel<string, string>>
    }

    /**
     * Данные управления задачами
     */
    tasksControlData: {
        /**
         * Массив записей по управлению задачами
         */
        items: TasksControlItemDataModel[]

        /**
         * Данные о странице для текущих записей по управлению задачами
         */
        metadata: MetadataModel;

        /**
         * Фильтр поиска записей
         */
        filter: TasksControlFilterDataModel;
    }

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то данные по задачам уже получены
         */
        hasTasksDataReceived: boolean;

        /**
         * Если true, то данные управления задачами уже получены
         */
        hasTasksControlDataReceived: boolean;
    };
};