import { MetadataModel } from 'app/web/common/data-models';
import { TasksControlFilterDataModel, TasksControlItemDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка элементов для управления задачами
 */
export type ReceiveTasksControlDataActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка элементов для управления задачами
 */
export type ReceiveTasksControlDataActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка элементов для управления задачами
 */
export type ReceiveTasksControlDataActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Массив записей
     */
    tasksControlData: TasksControlItemDataModel[];

    /**
     * Информация о пагинации
     */
    metadata: MetadataModel;

    /**
     * Фильтр поиска записей
     */
    filter: TasksControlFilterDataModel;
};

/**
 * Все возможные Action при получении списка элементов для управления задачами
 */
export type ReceiveTasksControlDataActionAnyPayload =
    | ReceiveTasksControlDataActionStartPayload
    | ReceiveTasksControlDataActionSuccessPayload
    | ReceiveTasksControlDataActionFailedPayload;