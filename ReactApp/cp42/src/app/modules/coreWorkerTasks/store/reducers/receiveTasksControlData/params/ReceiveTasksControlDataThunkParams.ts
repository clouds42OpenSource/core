import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveTasksControlDataParams } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';

/**
 * Модель параметров thunk на получение списка элементов для управления задачами
 */
export type ReceiveTasksControlDataThunkParams = ReceiveTasksControlDataParams & IForceThunkParam;