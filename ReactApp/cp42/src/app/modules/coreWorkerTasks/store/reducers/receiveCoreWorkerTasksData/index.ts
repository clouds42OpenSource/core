import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CoreWorkerTasksProcessId } from 'app/modules/coreWorkerTasks/CoreWorkerTasksProcessId';
import { CoreWorkerTasksActions } from 'app/modules/coreWorkerTasks/store/actions';
import { CoreWorkerTasksReducerState } from 'app/modules/coreWorkerTasks/store/reducers/CoreWorkerTasksReducerState';
import { ReceiveCoreWorkerTasksDataActionAnyPayload, ReceiveCoreWorkerTasksDataActionSuccessPayload } from 'app/modules/coreWorkerTasks/store/reducers/receiveCoreWorkerTasksData/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер получения задач воркера
 * @param state состояние
 * @param action действие
 */
export const receiveCoreWorkerTasksDataReducer: TPartialReducerObject<CoreWorkerTasksReducerState, ReceiveCoreWorkerTasksDataActionAnyPayload> =
    (state, action) => {
        const processId = CoreWorkerTasksProcessId.ReceiveCoreWorkerTasksData;
        const actions = CoreWorkerTasksActions.ReceiveCoreWorkerTasksData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveCoreWorkerTasksDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    tasks: {
                        items: [...payload.tasksData]
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasTasksDataReceived: true
                    }
                });
            }
        });
    };