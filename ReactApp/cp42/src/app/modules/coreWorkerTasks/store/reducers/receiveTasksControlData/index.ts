import { reducerActionState } from 'app/common/functions/reducerActionState';
import { CoreWorkerTasksProcessId } from 'app/modules/coreWorkerTasks/CoreWorkerTasksProcessId';
import { CoreWorkerTasksActions } from 'app/modules/coreWorkerTasks/store/actions';
import { CoreWorkerTasksReducerState } from 'app/modules/coreWorkerTasks/store/reducers/CoreWorkerTasksReducerState';
import { ReceiveTasksControlDataActionAnyPayload, ReceiveTasksControlDataActionSuccessPayload } from 'app/modules/coreWorkerTasks/store/reducers/receiveTasksControlData/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер получения списка элементов для управления задачами
 * @param state состояние
 * @param action действие
 */
export const receiveTasksControlDataReducer: TPartialReducerObject<CoreWorkerTasksReducerState, ReceiveTasksControlDataActionAnyPayload> =
    (state, action) => {
        const processId = CoreWorkerTasksProcessId.ReceiveTasksControlData;
        const actions = CoreWorkerTasksActions.ReceiveTasksControlData;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveTasksControlDataActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    tasksControlData: {
                        metadata: { ...payload.metadata },
                        items: [...payload.tasksControlData],
                        filter: { ...payload.filter }
                    },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasTasksControlDataReceived: true
                    }
                });
            }
        });
    };