import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { KeyValueDataModel } from 'app/web/common/data-models';

/**
 * Данные в редюсер при старте выполнения получения записей по очереди задач воркеров
 */
export type ReceiveCoreWorkerTasksDataActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения записей по очереди задач воркеров
 */
export type ReceiveCoreWorkerTasksDataActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения записей по очереди задач воркеров
 */
export type ReceiveCoreWorkerTasksDataActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Массив записей
     */
    tasksData: Array<KeyValueDataModel<string, string>>;
};

/**
 * Все возможные Action при получении списка воркеров в системе
 */
export type ReceiveCoreWorkerTasksDataActionAnyPayload =
    | ReceiveCoreWorkerTasksDataActionStartPayload
    | ReceiveCoreWorkerTasksDataActionSuccessPayload
    | ReceiveCoreWorkerTasksDataActionFailedPayload;