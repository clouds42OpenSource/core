import { createReducerActions } from 'core/redux/functions';
import { CoreWorkerTasksProcessId } from 'app/modules/coreWorkerTasks/CoreWorkerTasksProcessId';
import { CoreWorkerTasksConstants } from 'app/modules/coreWorkerTasks/consts';

/**
 * Все действия (Actions) для редюсера CoreWorkerTasks
 */
export const CoreWorkerTasksActions = {
    /**
     * Действие для получения данных по задачам воркера
     */
    ReceiveCoreWorkerTasksData: createReducerActions(CoreWorkerTasksConstants.reducerName, CoreWorkerTasksProcessId.ReceiveCoreWorkerTasksData),

    /**
     * Действие для получения списка элементов для управления задачами
     */
    ReceiveTasksControlData: createReducerActions(CoreWorkerTasksConstants.reducerName, CoreWorkerTasksProcessId.ReceiveTasksControlData)
};