import { CoreWorkerTasksActions } from 'app/modules/coreWorkerTasks/store/actions';
import { ReceiveTasksControlDataThunkParams } from 'app/modules/coreWorkerTasks/store/reducers/receiveTasksControlData/params/ReceiveTasksControlDataThunkParams';
import { ReceiveTasksControlDataActionFailedPayload, ReceiveTasksControlDataActionStartPayload, ReceiveTasksControlDataActionSuccessPayload } from 'app/modules/coreWorkerTasks/store/reducers/receiveTasksControlData/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveTasksControlData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CoreWorkerTasksActions;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveTasksControlDataThunkParams;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReceiveTasksControlDataActionStartPayload, ReceiveTasksControlDataActionSuccessPayload, ReceiveTasksControlDataActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReceiveTasksControlDataActionStartPayload, ReceiveTasksControlDataActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReceiveTasksControlDataActionStartPayload, ReceiveTasksControlDataActionSuccessPayload, ReceiveTasksControlDataActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка элементов для управления задачами
 */
export class ReceiveTasksControlDataThunk extends BaseReduxThunkObject<AppReduxStoreState, ReceiveTasksControlDataActionStartPayload, ReceiveTasksControlDataActionSuccessPayload, ReceiveTasksControlDataActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveTasksControlDataThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const tasksState = args.getStore().CoreWorkerTasksState;
        return {
            condition: !tasksState.hasSuccessFor.hasTasksControlDataReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CoreWorkerTasksState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const requestArgs = args.inputParams!;
            const workerTasksApi = InterlayerApiProxy.getCoreWorkerTasksApi();
            const tasksControlDataModel = await workerTasksApi.receiveTasksControlData(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                pageNumber: requestArgs.pageNumber,
                filter: requestArgs.filter,
                orderBy: requestArgs.orderBy
            });
            args.success({
                tasksControlData: tasksControlDataModel.records,
                metadata: tasksControlDataModel.metadata,
                filter: requestArgs.filter!
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}