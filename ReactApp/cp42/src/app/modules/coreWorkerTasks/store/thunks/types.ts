import { ReceiveCoreWorkerTasksDataActionFailedPayload, ReceiveCoreWorkerTasksDataActionStartPayload, ReceiveCoreWorkerTasksDataActionSuccessPayload } from 'app/modules/coreWorkerTasks/store/reducers/receiveCoreWorkerTasksData/payloads';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
export type TActionStartPayload = ReceiveCoreWorkerTasksDataActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
export type TActionSuccessPayload = ReceiveCoreWorkerTasksDataActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
export type TActionFailedPayload = ReceiveCoreWorkerTasksDataActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
export type TInputParams = IForceThunkParam;