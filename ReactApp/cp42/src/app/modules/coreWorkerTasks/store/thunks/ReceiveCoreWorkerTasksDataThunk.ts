import { CoreWorkerTasksActions } from 'app/modules/coreWorkerTasks/store/actions';
import { TActionFailedPayload, TActionStartPayload, TActionSuccessPayload, TInputParams } from 'app/modules/coreWorkerTasks/store/thunks/types';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

const { ReceiveCoreWorkerTasksData: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = CoreWorkerTasksActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка записей по очереди задач воркеров
 */
export class ReceiveCoreWorkerTasksDataThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveCoreWorkerTasksDataThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const tasksState = args.getStore().CoreWorkerTasksState;
        return {
            condition: !tasksState.hasSuccessFor.hasTasksDataReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().CoreWorkerTasksState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const workerTasksApi = InterlayerApiProxy.getCoreWorkerTasksApi();
            const workerTasksDataModel = await workerTasksApi.receiveAllCoreWorkerTasks(RequestKind.SEND_BY_ROBOT);
            args.success({
                tasksData: workerTasksDataModel.items
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}