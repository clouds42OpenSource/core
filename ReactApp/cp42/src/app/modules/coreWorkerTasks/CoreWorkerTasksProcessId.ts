/**
 * Процесы для задач воркеров
 */
export enum CoreWorkerTasksProcessId {
    /**
     * Процесс для получения списка задач воркеров
     */
    ReceiveCoreWorkerTasksData = 'RECEIVE_CORE_WORKERS_TASKS_DATA',

    /**
     * Процесс для получения списка элементов для управления задачами
     */
    ReceiveTasksControlData = 'RECEIVE_TASKS_CONTROL_DATA'
}