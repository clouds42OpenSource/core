/**
 * Константы для модуля ProcessFlow
 */
export const ProcessFlowConsts = {
    /**
     * Название редюсера для модуля ProcessFlow
     */
    reducerName: 'PROCESS_FLOW'
};