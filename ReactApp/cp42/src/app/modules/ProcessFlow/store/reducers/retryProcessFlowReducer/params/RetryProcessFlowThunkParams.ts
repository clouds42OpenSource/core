import { RetryProcessFlowParams } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/retryProcessFlow/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для перезапуска рабочего процеса
 */
export type RetryProcessFlowThunkParams = IForceThunkParam & RetryProcessFlowParams;