import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ProcessFlowProcessId } from 'app/modules/ProcessFlow';
import { ProcessFlowActions } from 'app/modules/ProcessFlow/store/actions';
import { ProcessFlowReducerState } from 'app/modules/ProcessFlow/store/reducers';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';
import { ReceiveProcessFlowDetailsActionAnyPayload, ReceiveProcessFlowDetailsActionSuccessPayload } from 'app/modules/ProcessFlow/store/reducers/receiveProcessFlowDetailsReducer/payloads';

/**
 * Редюсер для получения деталей рабочего процесса
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveProcessFlowDetailsReducer: TPartialReducerObject<ProcessFlowReducerState, ReceiveProcessFlowDetailsActionAnyPayload> =
    (state, action) => {
        const processId = ProcessFlowProcessId.ReceiveProcessFlowDetails;

        return reducerActionState({
            action,
            actions: ProcessFlowActions.ReceiveProcessFlowDetails,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveProcessFlowDetailsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    processFlowDetails: { ...payload.processFlowDetails },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasProcessFlowDetailsReceived: true
                    }
                });
            }
        });
    };