import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения перезапуска рабочего процеса
 */
export type RetryProcessFlowActionStartPayload = ReducerActionStartPayload & {
    /**
     * ID рабочего процесса для перезапуска
     */
    processFlowId: string;
};

/**
 * Данные в редюсер при неуспешном выполнении перезапуска рабочего процеса
 */
export type RetryProcessFlowActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении перезапуска рабочего процеса
 */
export type RetryProcessFlowActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при получении рабочих процесов
 */
export type RetryProcessFlowActionAnyPayload =
    | RetryProcessFlowActionStartPayload
    | RetryProcessFlowActionFailedPayload
    | RetryProcessFlowActionSuccessPayload;