import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { ProcessFlowDetailsDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails/data-models';

/**
 * Данные в редюсер при старте получении деталей рабочего процесса
 */
export type ReceiveProcessFlowDetailsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном получении деталей рабочего процесса
 */
export type ReceiveProcessFlowDetailsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении деталей рабочего процесса
 */
export type ReceiveProcessFlowDetailsActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Детали рабочего процесса
     */
    processFlowDetails: ProcessFlowDetailsDataModel
};

/**
 * Все возможные Action при получении деталей рабочего процесса
 */
export type ReceiveProcessFlowDetailsActionAnyPayload =
    | ReceiveProcessFlowDetailsActionStartPayload
    | ReceiveProcessFlowDetailsActionFailedPayload
    | ReceiveProcessFlowDetailsActionSuccessPayload;