import { ReceiveProcessFlowParams } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList/input-params';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель параметров для получения рабочих процесов
 */
export type ReceiveProcessFlowThunkParams = IForceThunkParam & ReceiveProcessFlowParams;