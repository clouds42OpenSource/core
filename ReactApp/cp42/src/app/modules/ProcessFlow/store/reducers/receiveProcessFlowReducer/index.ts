import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ProcessFlowProcessId } from 'app/modules/ProcessFlow';
import { ProcessFlowActions } from 'app/modules/ProcessFlow/store/actions';
import { ProcessFlowReducerState } from 'app/modules/ProcessFlow/store/reducers';
import { ReceiveProcessFlowActionAnyPayload, ReceiveProcessFlowActionSuccessPayload } from 'app/modules/ProcessFlow/store/reducers/receiveProcessFlowReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения записи в ProcessFlow
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveProcessFlowReducer: TPartialReducerObject<ProcessFlowReducerState, ReceiveProcessFlowActionAnyPayload> =
    (state, action) => {
        const processId = ProcessFlowProcessId.ReceiveProcessFlow;
        const actions = ProcessFlowActions.ReceiveProcessFlow;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveProcessFlowActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    processFlow: { ...payload.processFlow },
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasProcessFlowReceived: true
                    }
                });
            }
        });
    };