import { reducerActionState } from 'app/common/functions/reducerActionState';
import { ProcessFlowProcessId } from 'app/modules/ProcessFlow';
import { ProcessFlowActions } from 'app/modules/ProcessFlow/store/actions';
import { ProcessFlowReducerState } from 'app/modules/ProcessFlow/store/reducers';
import { RetryProcessFlowActionAnyPayload, RetryProcessFlowActionStartPayload } from 'app/modules/ProcessFlow/store/reducers/retryProcessFlowReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для перезапуска рабочего процеса в ProcessFlow
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const retryProcessFlowReducer: TPartialReducerObject<ProcessFlowReducerState, RetryProcessFlowActionAnyPayload> =
    (state, action) => {
        const processId = ProcessFlowProcessId.RetryProcessFlow;
        const actions = ProcessFlowActions.RetryProcessFlow;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                const payload = action.payload as RetryProcessFlowActionStartPayload;
                return reducerStateProcessStart(state, processId, {
                    propcessFlowIdInRetry: payload.processFlowId
                });
            },
            onSuccessProcessState: () => {
                const newRecords = state.processFlow.records.filter(item => item.processFlowId !== state.propcessFlowIdInRetry);
                return reducerStateProcessSuccess(state, processId, {
                    processFlow: {
                        ...state.processFlow,
                        records: newRecords,
                        metadata: {
                            ...state.processFlow.metadata,
                            pageSize: state.processFlow.metadata.pageSize - 1,
                            totalItemCount: state.processFlow.metadata.totalItemCount - 1
                        }
                    }
                });
            }
        });
    };