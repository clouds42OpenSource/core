import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';
import { ReceiveProcessFlowDetailsParams } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails';

/**
 * Параметры для получения деталей рабочего процесса
 */
export type ReceiveProcessFlowDetailsThunkParams = IForceThunkParam & ReceiveProcessFlowDetailsParams;