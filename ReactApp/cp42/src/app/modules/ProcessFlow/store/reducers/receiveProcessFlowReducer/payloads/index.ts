import { ProcessFlowListDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения рабочих процесов
 */
export type ReceiveProcessFlowActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения рабочих процесов
 */
export type ReceiveProcessFlowActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения рабочих процесов
 */
export type ReceiveProcessFlowActionSuccessPayload = ReducerActionSuccessPayload & {

    /**
     * Рабочие процесы
     */
    processFlow: ProcessFlowListDataModel
};

/**
 * Все возможные Action при получении рабочих процесов
 */
export type ReceiveProcessFlowActionAnyPayload =
    | ReceiveProcessFlowActionStartPayload
    | ReceiveProcessFlowActionFailedPayload
    | ReceiveProcessFlowActionSuccessPayload;