import { ProcessFlowProcessId } from 'app/modules/ProcessFlow/ProcessFlowProcessId';
import { ProcessFlowListDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowList';
import { IReducerState } from 'core/redux/interfaces';
import { ProcessFlowDetailsDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails/data-models';

/**
 * Состояние редюсера для работы с рабочими процесами
 */
export type ProcessFlowReducerState = IReducerState<ProcessFlowProcessId> & {

    /**
     * ID рабочего процесса, который пытается перезапуститься
     */
    propcessFlowIdInRetry: string | null;
    processFlow: ProcessFlowListDataModel;
    processFlowDetails: ProcessFlowDetailsDataModel,
    hasSuccessFor: {
        /**
         * Если true, то рабочие процесы получены
         */
        hasProcessFlowReceived: boolean,

        /**
         * Если true, то детали рабочего процеса получены
         */
        hasProcessFlowDetailsReceived: boolean
    };
};