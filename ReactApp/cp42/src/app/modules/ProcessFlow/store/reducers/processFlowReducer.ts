import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { ProcessFlowConsts } from 'app/modules/ProcessFlow/constants';
import { ProcessFlowReducerState } from 'app/modules/ProcessFlow/store/reducers/ProcessFlowReducerState';
import { receiveProcessFlowDetailsReducer } from 'app/modules/ProcessFlow/store/reducers/receiveProcessFlowDetailsReducer';
import { receiveProcessFlowReducer } from 'app/modules/ProcessFlow/store/reducers/receiveProcessFlowReducer';
import { retryProcessFlowReducer } from 'app/modules/ProcessFlow/store/reducers/retryProcessFlowReducer';
import { ProcessFlowDetailsDataModel } from 'app/web/InterlayerApiProxy/ProcessFlowApiProxy/receiveProcessFlowDetails/data-models';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера ProcessFlow
 */
const initialState = initReducerState<ProcessFlowReducerState>(
    ProcessFlowConsts.reducerName,
    {
        propcessFlowIdInRetry: null,
        processFlow: {
            records: [],
            metadata: getInitialMetadata()
        },
        processFlowDetails: partialFillOf<ProcessFlowDetailsDataModel>(),
        hasSuccessFor: {
            hasProcessFlowReceived: false,
            hasProcessFlowDetailsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния ProcessFlow
 */
const partialReducers = [
    receiveProcessFlowReducer,
    retryProcessFlowReducer,
    receiveProcessFlowDetailsReducer
];

/**
 * Редюсер ProcessFlow
 */
export const processFlowReducer = createReducer(initialState, partialReducers);