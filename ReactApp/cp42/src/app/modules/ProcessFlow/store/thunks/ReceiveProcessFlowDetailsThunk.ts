import { ProcessFlowActions } from 'app/modules/ProcessFlow/store/actions';
import { ReceiveProcessFlowDetailsThunkParams } from 'app/modules/ProcessFlow/store/reducers/receiveProcessFlowDetailsReducer/params';
import { ReceiveProcessFlowDetailsActionFailedPayload, ReceiveProcessFlowDetailsActionStartPayload, ReceiveProcessFlowDetailsActionSuccessPayload } from 'app/modules/ProcessFlow/store/reducers/receiveProcessFlowDetailsReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = ReceiveProcessFlowDetailsActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = ReceiveProcessFlowDetailsActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = ReceiveProcessFlowDetailsActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveProcessFlowDetailsThunkParams;

const { ReceiveProcessFlowDetails: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ProcessFlowActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class ReceiveProcessFlowDetailsThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveProcessFlowDetailsThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const processFlowState = args.getStore().ProcessFlowState;
        return {
            condition: !processFlowState.hasSuccessFor.hasProcessFlowDetailsReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().ProcessFlowState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const ProcessFlowApi = InterlayerApiProxy.getProcessFlowApi();
            const response = await ProcessFlowApi.receiveProcessFlowDetails(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success({
                processFlowDetails: response
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}