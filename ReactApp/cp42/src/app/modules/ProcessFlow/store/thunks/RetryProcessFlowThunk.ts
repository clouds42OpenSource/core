import { ProcessFlowActions } from 'app/modules/ProcessFlow/store/actions';
import { RetryProcessFlowThunkParams } from 'app/modules/ProcessFlow/store/reducers/retryProcessFlowReducer/params';
import { RetryProcessFlowActionFailedPayload, RetryProcessFlowActionStartPayload, RetryProcessFlowActionSuccessPayload } from 'app/modules/ProcessFlow/store/reducers/retryProcessFlowReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = RetryProcessFlowActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = RetryProcessFlowActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = RetryProcessFlowActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = RetryProcessFlowThunkParams;

const { RetryProcessFlow: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = ProcessFlowActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса списка действий облака для логирования
 */
export class RetryProcessFlowThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new RetryProcessFlowThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().ProcessFlowState,
            startActionPayload: {
                processFlowId: args.inputParams!.processFlowId
            }
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        try {
            const ProcessFlowApi = InterlayerApiProxy.getProcessFlowApi();
            await ProcessFlowApi.retryProcessFlow(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);

            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}