import { ProcessFlowConsts } from 'app/modules/ProcessFlow/constants';
import { ProcessFlowProcessId } from 'app/modules/ProcessFlow/ProcessFlowProcessId';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions дле редюсера ProcessFlow
 */
export const ProcessFlowActions = {
    /**
     * Набор action на получение рабочих процессов
     */
    ReceiveProcessFlow: createReducerActions(ProcessFlowConsts.reducerName, ProcessFlowProcessId.ReceiveProcessFlow),

    /**
     * Набор action на перезапуск рабочего процесса
     */
    RetryProcessFlow: createReducerActions(ProcessFlowConsts.reducerName, ProcessFlowProcessId.RetryProcessFlow),

    /**
     * Набор action на получение деталей рабочего процесса
     */
    ReceiveProcessFlowDetails: createReducerActions(ProcessFlowConsts.reducerName, ProcessFlowProcessId.ReceiveProcessFlowDetails),
};