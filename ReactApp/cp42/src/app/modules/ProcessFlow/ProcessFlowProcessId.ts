/**
 * Процесы ProcessFlow
 */
export enum ProcessFlowProcessId {
    /**
     * Процесс получить рабочие процессы
     */
    ReceiveProcessFlow = 'RECEIVE_PROCESS_FLOW',

    /**
     * Процесс перезапуска рабочего процесса
     */
    RetryProcessFlow = 'RETRY_PROCESS_FLOW',

    /**
     * Процесс для получения деталей рабочего процесса
     */
    ReceiveProcessFlowDetails = 'RECEIVE_PROCESS_FLOW_DETAILS',
}