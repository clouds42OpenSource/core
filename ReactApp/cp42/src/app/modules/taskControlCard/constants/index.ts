/**
 * Константы для модуля TaskControlCard
 */
export const TaskControlCardConstants = {
    /**
     * Название редюсера для модуля TaskControlCard
     */
    reducerName: 'TASK_CONTROL_CARD'
};