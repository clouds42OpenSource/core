/**
 * Процесы TaskControlCard
 */
export enum TaskControlCardProcessId {
    /**
     * Процесс для получения карточки управления задачей
     */
    ReceiveTaskControlCard = 'RECEIVE_TASK_CONTROL_CARD',

    /**
     * Процесс для обновления карточки управления задачей
     */
    UpdateTaskControlCard = 'UPDATE_TASK_CONTROL_CARD'
}