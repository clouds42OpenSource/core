import { createReducerActions } from 'core/redux/functions';
import { TaskControlCardProcessId } from 'app/modules/taskControlCard/TaskControlCardProcessId';
import { TaskControlCardConstants } from 'app/modules/taskControlCard/constants';

/**
 * Все доступные actions дле редюсера TaskControlCardActions
 */
export const TaskControlCardActions = {
    /**
     * Набор action для получения карточки управления задачей
     */
    ReceiveTaskControlCard: createReducerActions(TaskControlCardConstants.reducerName, TaskControlCardProcessId.ReceiveTaskControlCard),

    /**
     * Набор action для обновления карточки управления задачей
     */
    UpdateTaskControlCard: createReducerActions(TaskControlCardConstants.reducerName, TaskControlCardProcessId.UpdateTaskControlCard)
};