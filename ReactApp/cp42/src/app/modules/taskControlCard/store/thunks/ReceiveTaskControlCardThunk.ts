import { TaskControlCardActions } from 'app/modules/taskControlCard/store/actions';
import { ReceiveTaskControlCardThunkParams } from 'app/modules/taskControlCard/store/reducers/receiveTaskControlCardReducer/params';
import { ReceiveTaskControlCardActionFailedPayload, ReceiveTaskControlCardActionStartPayload, ReceiveTaskControlCardActionSuccessPayload } from 'app/modules/taskControlCard/store/reducers/receiveTaskControlCardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = ReceiveTaskControlCardThunkParams;

const { ReceiveTaskControlCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TaskControlCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, ReceiveTaskControlCardActionStartPayload, ReceiveTaskControlCardActionSuccessPayload, ReceiveTaskControlCardActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<ReceiveTaskControlCardActionStartPayload, ReceiveTaskControlCardActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, ReceiveTaskControlCardActionStartPayload, ReceiveTaskControlCardActionSuccessPayload, ReceiveTaskControlCardActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса на получение карточки управления задачей
 */
export class ReceiveTaskControlCardThunk extends BaseReduxThunkObject<AppReduxStoreState, ReceiveTaskControlCardActionStartPayload, ReceiveTaskControlCardActionSuccessPayload, ReceiveTaskControlCardActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new ReceiveTaskControlCardThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const accountCardState = args.getStore().TaskControlCardState;
        return {
            condition: !accountCardState.hasSuccessFor.hasTaskControlCardReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TaskControlCardState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const taskControlCardApi = InterlayerApiProxy.getTaskControlCardApi();

        try {
            const taskControlCardInfo = await taskControlCardApi.receiveTaskControlCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                taskId: requestArgs.taskId
            });
            args.success({
                taskControlCard: taskControlCardInfo
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}