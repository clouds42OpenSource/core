import { ReceiveTasksControlDataThunk } from 'app/modules/coreWorkerTasks/store/thunks';
import { TaskControlCardActions } from 'app/modules/taskControlCard/store/actions';
import { UpdateTaskControlCardThunkParams } from 'app/modules/taskControlCard/store/reducers/updateTaskCotrolCardReducer/params/UpdateTaskControlCardParams';
import { UpdateTaskControlCardActionFailedPayload, UpdateTaskControlCardActionStartPayload, UpdateTaskControlCardActionSuccessPayload } from 'app/modules/taskControlCard/store/reducers/updateTaskCotrolCardReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = UpdateTaskControlCardThunkParams;

const { UpdateTaskControlCard: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TaskControlCardActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, UpdateTaskControlCardActionStartPayload, UpdateTaskControlCardActionSuccessPayload, UpdateTaskControlCardActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<UpdateTaskControlCardActionStartPayload, UpdateTaskControlCardActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, UpdateTaskControlCardActionStartPayload, UpdateTaskControlCardActionSuccessPayload, UpdateTaskControlCardActionFailedPayload, TInputParams>;

/**
 * Thunk для запроса на обновление карточки управления задачей
 */
export class UpdateTaskControlCardThunk extends BaseReduxThunkObject<AppReduxStoreState, UpdateTaskControlCardActionStartPayload, UpdateTaskControlCardActionSuccessPayload, UpdateTaskControlCardActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new UpdateTaskControlCardThunk().execute(args);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const accountCardState = args.getStore().TaskControlCardState;
        return {
            condition: !accountCardState.hasSuccessFor.hasTaskControlCardReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TaskControlCardState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const requestArgs = args.inputParams!;
        const taskControlCardApi = InterlayerApiProxy.getTaskControlCardApi();
        const coreWorkerTasksState = args.getStore().CoreWorkerTasksState;

        try {
            await taskControlCardApi.updateTaskControlCard(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, {
                taskId: requestArgs.taskId,
                taskExecutionLifetimeInMinutes: requestArgs.taskExecutionLifetimeInMinutes
            });
            args.success({
                UpdatedItem: requestArgs
            });
            args.thunkDispatch(new ReceiveTasksControlDataThunk(), {
                pageNumber: coreWorkerTasksState.tasksControlData.metadata.pageNumber,
                filter: {
                    taskId: ''
                },
                force: true
            });
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}