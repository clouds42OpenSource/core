import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';
import { TaskControlCardInfoDataModel } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy/receiveTaskControlCard';

/**
 * Данные в редюсер при старте получения карточки управления задачей
 */
export type ReceiveTaskControlCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки получения карточки управления задачей
 */
export type ReceiveTaskControlCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном получении карточки управления задачей
 */
export type ReceiveTaskControlCardActionSuccessPayload = ReducerActionSuccessPayload & {
    taskControlCard: TaskControlCardInfoDataModel;
};

/**
 * Все возможные Action при получении карточки управления задачей
 */
export type ReceiveTaskControlCardActionAnyPayload =
    | ReceiveTaskControlCardActionStartPayload
    | ReceiveTaskControlCardActionSuccessPayload
    | ReceiveTaskControlCardActionFailedPayload;