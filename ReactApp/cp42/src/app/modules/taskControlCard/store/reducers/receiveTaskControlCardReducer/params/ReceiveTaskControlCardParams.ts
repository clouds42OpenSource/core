import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос получения карточки управления задачей
 */
export type ReceiveTaskControlCardThunkParams = IForceThunkParam & {
    /**
     * ID задачи
     */
    taskId: string;
};