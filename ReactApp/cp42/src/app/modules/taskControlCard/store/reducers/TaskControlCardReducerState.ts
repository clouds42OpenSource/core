import { IReducerState } from 'core/redux/interfaces';
import { TaskControlCardProcessId } from 'app/modules/taskControlCard/TaskControlCardProcessId';
import { TaskControlCardInfoDataModel } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy/receiveTaskControlCard';

/**
 * Состояние редюсера для работы с карточкой управления задачей
 */
export type TaskControlCardReducerState = IReducerState<TaskControlCardProcessId> & {
    /**
     * Информация об управлении задачей
     */
    taskControlInfo: TaskControlCardInfoDataModel;

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то карточка управления задачей уже получена
         */
        hasTaskControlCardReceived: boolean,
    };
};