import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TaskControlCardActions } from 'app/modules/taskControlCard/store/actions';
import { ReceiveTaskControlCardActionAnyPayload, ReceiveTaskControlCardActionSuccessPayload } from 'app/modules/taskControlCard/store/reducers/receiveTaskControlCardReducer/payloads';
import { TaskControlCardReducerState } from 'app/modules/taskControlCard/store/reducers/TaskControlCardReducerState';
import { TaskControlCardProcessId } from 'app/modules/taskControlCard/TaskControlCardProcessId';
import { TaskControlCardInfoDataModel } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy/receiveTaskControlCard';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения карточки управления задачей
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const receiveTaskControlCardReducer: TPartialReducerObject<TaskControlCardReducerState, ReceiveTaskControlCardActionAnyPayload> =
    (state, action) => {
        const processId = TaskControlCardProcessId.ReceiveTaskControlCard;
        const actions = TaskControlCardActions.ReceiveTaskControlCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    taskControlInfo: {} as TaskControlCardInfoDataModel,
                    hasSuccessFor: {
                        ...state.hasSuccessFor,
                        hasTaskControlCardReceived: false
                    }
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as ReceiveTaskControlCardActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    taskControlInfo: payload.taskControlCard,
                    hasSuccessFor: {
                        hasTaskControlCardReceived: true
                    }
                });
            }
        });
    };