import { TasksControlItemDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте обновления карточки управления задачей
 */
export type UpdateTaskControlCardActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при ошибки обновления карточки управления задачей
 */
export type UpdateTaskControlCardActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном обновления карточки управления задачей
 */
export type UpdateTaskControlCardActionSuccessPayload = ReducerActionSuccessPayload & {
    UpdatedItem: TasksControlItemDataModel;
};

/**
 * Все возможные Action при обновления карточки управления задачей
 */
export type UpdateTaskControlCardActionAnyPayload =
    | UpdateTaskControlCardActionStartPayload
    | UpdateTaskControlCardActionSuccessPayload
    | UpdateTaskControlCardActionFailedPayload;