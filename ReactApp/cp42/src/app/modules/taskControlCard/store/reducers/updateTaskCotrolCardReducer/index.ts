import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TaskControlCardActions } from 'app/modules/taskControlCard/store/actions';
import { TaskControlCardReducerState } from 'app/modules/taskControlCard/store/reducers/TaskControlCardReducerState';
import { UpdateTaskControlCardActionAnyPayload, UpdateTaskControlCardActionSuccessPayload } from 'app/modules/taskControlCard/store/reducers/updateTaskCotrolCardReducer/payloads';
import { TaskControlCardProcessId } from 'app/modules/taskControlCard/TaskControlCardProcessId';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для обновления карточки управления задачей
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const updateTaskControlCardReducer: TPartialReducerObject<TaskControlCardReducerState, UpdateTaskControlCardActionAnyPayload> =
    (state, action) => {
        const processId = TaskControlCardProcessId.UpdateTaskControlCard;
        const actions = TaskControlCardActions.UpdateTaskControlCard;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as UpdateTaskControlCardActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    taskControlInfo: {
                        ...state.taskControlInfo,
                        taskLifeTimeInMinutes: payload.UpdatedItem.taskExecutionLifetimeInMinutes,
                        taskId: payload.UpdatedItem.taskId
                    }
                });
            }
        });
    };