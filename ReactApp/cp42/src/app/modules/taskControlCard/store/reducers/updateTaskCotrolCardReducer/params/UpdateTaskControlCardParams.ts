import { TasksControlItemDataModel } from 'app/web/InterlayerApiProxy/CoreWorkerTasksApiProxy/receiveTasksControlData';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Модель на запрос обновления карточки управления задачей
 */
export type UpdateTaskControlCardThunkParams = TasksControlItemDataModel & IForceThunkParam;