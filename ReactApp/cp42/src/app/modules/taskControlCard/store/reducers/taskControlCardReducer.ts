import { partialFillOf } from 'app/common/functions/fillObjectWithUndefined';
import { createReducer, initReducerState } from 'core/redux/functions';
import { TaskControlCardReducerState } from 'app/modules/taskControlCard/store/reducers/TaskControlCardReducerState';
import { TaskControlCardConstants } from 'app/modules/taskControlCard/constants';
import { receiveTaskControlCardReducer } from 'app/modules/taskControlCard/store/reducers/receiveTaskControlCardReducer';
import { TaskControlCardInfoDataModel } from 'app/web/InterlayerApiProxy/TaskControlCardApiProxy/receiveTaskControlCard';
import { updateTaskControlCardReducer } from 'app/modules/taskControlCard/store/reducers/updateTaskCotrolCardReducer';

/**
 * Начальное состояние редюсера TaskControlCard
 */
const initialState = initReducerState<TaskControlCardReducerState>(
    TaskControlCardConstants.reducerName,
    {
        /**
         * Информация об управлении задачей
         */
        taskControlInfo: partialFillOf<TaskControlCardInfoDataModel>(),

        hasSuccessFor: {
            /**
             * Если true, то карточка управления задачей уже получена
             */
            hasTaskControlCardReceived: false,
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния TaskControlCard
 */
const partialReducers = [
    receiveTaskControlCardReducer,
    updateTaskControlCardReducer
];

/**
 * Редюсер TaskControlCard
 */
export const taskControlCardReducer = createReducer(initialState, partialReducers);