export const GLOBAL = {
    getCurrentSessionSettings: 'getCurrentSessionSettings',
    getAlertMessage: 'getAlertMessage',
    getUserPermissions: 'getUserPermissions',
    getNavMenu: 'getNavMenu'
} as const;