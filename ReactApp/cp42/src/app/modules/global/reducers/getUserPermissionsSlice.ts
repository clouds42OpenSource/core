import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { GetPermissions } from 'app/api/endpoints/global/response/getPermissions';
import { GLOBAL } from 'app/modules/global/constants';

const initialState: GetPermissions = {
    permissions: [],
    hasSuccessFor: {
        permissionsReceived: false
    }
};

export const getUserPermissionsSlice = createSlice({
    name: GLOBAL.getUserPermissions,
    initialState,
    reducers: {
        success(state, action: PayloadAction<string[]>) {
            state.permissions = action.payload;
            state.hasSuccessFor.permissionsReceived = true;
        }
    }
});

export default getUserPermissionsSlice.reducer;