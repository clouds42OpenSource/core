import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { GetCurrentSessionSettings } from 'app/api/endpoints/global/response/getCurrentSessionSettings';
import { GLOBAL } from 'app/modules/global/constants';

const initialState: GetCurrentSessionSettings = {
    settings: {
        currentContextInfo: {
            isDifferentContextAccount: false,
            contextAccountIndex: 0,
            contextAccountName: '',
            currentUserGroups: [],
            isPhoneVerified: null,
            isEmailVerified: null,
            isPhoneExists: false,
            isEmailExists: false,
            showSupportMessage: false,
            userId: '',
            contextAccountId: '',
            accountId: '',
            isNew: false,
            locale: 'ru-ru',
            userSource: ''
        }
    },
    hasSuccessFor: {
        hasSettingsReceived: false
    }
};

export const getCurrentSessionSettingsSlice = createSlice({
    name: GLOBAL.getCurrentSessionSettings,
    initialState,
    reducers: {
        success(state, action: PayloadAction<GetCurrentSessionSettings>) {
            state.settings = action.payload.settings;
            state.hasSuccessFor.hasSettingsReceived = action.payload.hasSuccessFor.hasSettingsReceived;
        },
    }
});

export default getCurrentSessionSettingsSlice.reducer;