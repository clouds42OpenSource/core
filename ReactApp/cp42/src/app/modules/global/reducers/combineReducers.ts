import getCurrentSessionSettingsReducer from 'app/modules/global/reducers/getCurrentSessionSettingsSlice';
import getAlertMessagesReducer from 'app/modules/global/reducers/getAlertMessagesSlice';
import getUserPermissionsReducer from 'app/modules/global/reducers/getUserPermissionsSlice';
import getNavMenuReducer from 'app/modules/global/reducers/getNavMenuSlice';
import { combineReducers } from 'redux';

export const GlobalReducer = combineReducers({
    getCurrentSessionSettingsReducer,
    getAlertMessagesReducer,
    getUserPermissionsReducer,
    getNavMenuReducer
});