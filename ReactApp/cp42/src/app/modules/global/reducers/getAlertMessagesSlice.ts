import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AlertMessage, GetAlertMessages } from 'app/api/endpoints/global/response';
import { GLOBAL } from 'app/modules/global/constants';

const initialState: AlertMessage = {
    alert: [],
    hasSuccessFor: {
        hasAlertsReceived: false
    }
};

export const getAlertMessagesSlice = createSlice({
    name: GLOBAL.getAlertMessage,
    initialState,
    reducers: {
        success(state, action: PayloadAction<GetAlertMessages[]>) {
            state.alert = action.payload.map(item => ({ type: item.Type, text: item.Text }));
            state.hasSuccessFor.hasAlertsReceived = true;
        }
    }
});

export default getAlertMessagesSlice.reducer;