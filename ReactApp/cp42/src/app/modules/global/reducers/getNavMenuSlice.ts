import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { GetNavMenu, NavMenuItemInfo } from 'app/api/endpoints/global/response/getNavMenu';
import { TDataReturn } from 'app/api/types';
import { GLOBAL } from 'app/modules/global/constants';
import Cookies from 'js-cookie';

const checkIsExpanded = (menuCategory: string): boolean => {
    return !!menuCategory && Cookies.get(menuCategory) === 'true';
};

const initialState: TDataReturn<GetNavMenu> = {
    isLoading: false,
    data: {
        rawData: {
            leftSideMenu: [],
            topSideMenu: null,
            isOpenNavMobile: false
        }
    },
    error: null
};

export const getNavMenuSlice = createSlice({
    name: GLOBAL.getNavMenu,
    initialState,
    reducers: {
        loading(state) {
            state.isLoading = true;
            state.data = null;
            state.error = null;
        },
        success(state, action: PayloadAction<GetNavMenu>) {
            state.isLoading = false;
            state.data = {
                rawData: {
                    topSideMenu: action.payload.topSideMenu,
                    leftSideMenu: action.payload.leftSideMenu.map(item => {
                        return {
                            ...item,
                            startMenu: {
                                ...item.startMenu,
                                isExpanded: checkIsExpanded(item.categoryKey)
                            }
                        };
                    }),
                    isOpenNavMobile: false
                }
            };
            state.error = null;
        },
        changeLeftMenu(state, action: PayloadAction<NavMenuItemInfo[]>) {
            if (state.data) {
                state.isLoading = false;
                state.data = {
                    rawData: {
                        topSideMenu: state.data.rawData.topSideMenu,
                        leftSideMenu: action.payload,
                        isOpenNavMobile: state.data.rawData.isOpenNavMobile
                    }
                };
                state.error = null;
            }
        },
        openNavMobile(state) {
            if (state.data) {
                state.isLoading = false;
                state.data = {
                    rawData: {
                        topSideMenu: state.data.rawData.topSideMenu,
                        leftSideMenu: state.data.rawData.leftSideMenu,
                        isOpenNavMobile: !state.data.rawData.isOpenNavMobile
                    }
                };
                state.error = null;
            }
        },
        error(state, action: PayloadAction<string>) {
            state.isLoading = false;
            state.data = null;
            state.error = action.payload;
        }
    }
});

export default getNavMenuSlice.reducer;