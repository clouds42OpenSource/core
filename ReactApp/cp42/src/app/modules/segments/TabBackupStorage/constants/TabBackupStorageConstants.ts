/**
 * Константы для модуля Segments -> Таб Хранилище бекапов
 */
export const TabBackupStorageConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб Хранилище бекапов
     */
    reducerName: 'TAB_BACKUP_STORAGE'
};