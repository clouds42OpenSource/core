/**
 * Процесы Segments -> "Хранилище бекапов"
 */
export enum TabBackupStorageProcessId {
    /**
     * Процесс на получение списка данных по хранилищам бекапов
     */
    GetCloudBackupStorages = 'GET_CLOUD_BACKUP_STORAGES',

    /**
     * Процесс на создание хранилища бэкапов
     */
    CreateCloudBackupStorage = 'CREATE_CLOUD_BACKUP_STORAGE',

    /**
     * Процесс на редактирование хранилища бэкапов
     */
    EditCloudBackupStorage = 'EDIT_CLOUD_BACKUP_STORAGE',

    /**
     * Процесс на получение хранилища бэкапов по ID
     */
    GetCloudBackupStorage = 'GET_CLOUD_BACKUP_STORAGE',

    /**
     * Процесс на удаление хранилища бэкапов по ID
     */
    DeleteCloudBackupStorage = 'DELETE_CLOUD_BACKUP_STORAGE'
}