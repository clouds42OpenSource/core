import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk для получения хранилища бекапов
 */
export type GetCloudBackupStorageThunkParams = IForceThunkParam & {
    /**
     * ID хранилища бекапов
     */
    backupStorageId: string
};