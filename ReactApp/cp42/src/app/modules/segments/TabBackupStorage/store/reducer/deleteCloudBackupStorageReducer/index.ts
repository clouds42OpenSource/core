import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { TabBackupStorageActions } from 'app/modules/segments/TabBackupStorage/store/actions';
import { TabBackupStorageState } from 'app/modules/segments/TabBackupStorage/store/reducer';
import { DeleteCloudBackupStorageActionAnyPayload, DeleteCloudBackupStorageActionSuccessPayload } from 'app/modules/segments/TabBackupStorage/store/reducer/deleteCloudBackupStorageReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления хранилища бекапов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteCloudBackupStorageReducer: TPartialReducerObject<TabBackupStorageState, DeleteCloudBackupStorageActionAnyPayload> =
    (state, action) => {
        const processId = TabBackupStorageProcessId.DeleteCloudBackupStorage;
        const actions = TabBackupStorageActions.DeleteCloudBackupStorage;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteCloudBackupStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };