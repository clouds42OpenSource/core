import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления хранилища бекапов
 */
export type DeleteCloudBackupStorageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления хранилища бекапов
 */
export type DeleteCloudBackupStorageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления хранилища бекапов
 */
export type DeleteCloudBackupStorageActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении хранилища бекапов
 */
export type DeleteCloudBackupStorageActionAnyPayload =
    | DeleteCloudBackupStorageActionStartPayload
    | DeleteCloudBackupStorageActionFailedPayload
    | DeleteCloudBackupStorageActionSuccessPayload;