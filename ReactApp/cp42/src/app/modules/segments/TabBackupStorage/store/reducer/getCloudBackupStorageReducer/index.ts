import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { TabBackupStorageActions } from 'app/modules/segments/TabBackupStorage/store/actions';
import { TabBackupStorageState } from 'app/modules/segments/TabBackupStorage/store/reducer';
import { GetCloudBackupStorageActionAnyPayload, GetCloudBackupStorageActionSuccessPayload } from 'app/modules/segments/TabBackupStorage/store/reducer/getCloudBackupStorageReducer/payloads';
import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения хранилища бекапов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudBackupStorageReducer: TPartialReducerObject<TabBackupStorageState, GetCloudBackupStorageActionAnyPayload> =
    (state, action) => {
        const processId = TabBackupStorageProcessId.GetCloudBackupStorage;
        const actions = TabBackupStorageActions.GetCloudBackupStorage;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    cloudBackupStorage: partialFillOf<CloudBackupStorageDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetCloudBackupStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudBackupStorage: payload
                });
            }
        });
    };