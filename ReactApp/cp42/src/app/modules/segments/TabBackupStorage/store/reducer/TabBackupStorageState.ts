import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Хранилище бекапов
 */
export type TabBackupStorageState = IReducerState<TabBackupStorageProcessId> & {
    /**
     * Массив хранилища бекапов
     */
    cloudBackupStorages: SelectDataMetadataResponseDto<CloudBackupStorageDataModel>,

    /**
     * Модель хранилища бекапов
     */
    cloudBackupStorage: CloudBackupStorageDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то хранилища бекапов получены
         */
        hasCloudBackupStoragesReceived: boolean
    };
};