import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { TabBackupStorageActions } from 'app/modules/segments/TabBackupStorage/store/actions';
import { TabBackupStorageState } from 'app/modules/segments/TabBackupStorage/store/reducer';
import { EditCloudBackupStorageActionAnyPayload, EditCloudBackupStorageActionSuccessPayload } from 'app/modules/segments/TabBackupStorage/store/reducer/editCloudBackupStorageReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редкатирования хранилища бекапов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editCloudBackupStorageReducer: TPartialReducerObject<TabBackupStorageState, EditCloudBackupStorageActionAnyPayload> =
    (state, action) => {
        const processId = TabBackupStorageProcessId.EditCloudBackupStorage;
        const actions = TabBackupStorageActions.EditCloudBackupStorage;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditCloudBackupStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };