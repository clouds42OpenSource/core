import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { TabBackupStorageActions } from 'app/modules/segments/TabBackupStorage/store/actions';
import { TabBackupStorageState } from 'app/modules/segments/TabBackupStorage/store/reducer';
import { CreateCloudBackupStorageActionAnyPayload, CreateCloudBackupStorageActionSuccessPayload } from 'app/modules/segments/TabBackupStorage/store/reducer/createCloudBackupStorageReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания хранилища бекапов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createCloudBackupStorageReducer: TPartialReducerObject<TabBackupStorageState, CreateCloudBackupStorageActionAnyPayload> =
    (state, action) => {
        const processId = TabBackupStorageProcessId.CreateCloudBackupStorage;
        const actions = TabBackupStorageActions.CreateCloudBackupStorage;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateCloudBackupStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };