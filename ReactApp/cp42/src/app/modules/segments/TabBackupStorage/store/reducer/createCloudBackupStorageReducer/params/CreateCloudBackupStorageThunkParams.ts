import { CreateCloudBackupStorageParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметр для создания хранилища бекапов
 */
export type CreateCloudBackupStorageThunkParams = IForceThunkParam & CreateCloudBackupStorageParams;