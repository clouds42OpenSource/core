import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания хранилища бекапов
 */
export type CreateCloudBackupStorageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания хранилища бекапов
 */
export type CreateCloudBackupStorageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания хранилища бекапов
 */
export type CreateCloudBackupStorageActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании хранилища бекапов
 */
export type CreateCloudBackupStorageActionAnyPayload =
    | CreateCloudBackupStorageActionStartPayload
    | CreateCloudBackupStorageActionFailedPayload
    | CreateCloudBackupStorageActionSuccessPayload;