import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования хранилища бекапов
 */
export type EditCloudBackupStorageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования хранилища бекапов
 */
export type EditCloudBackupStorageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования хранилища бекапов
 */
export type EditCloudBackupStorageActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании хранилища бекапов
 */
export type EditCloudBackupStorageActionAnyPayload =
    | EditCloudBackupStorageActionStartPayload
    | EditCloudBackupStorageActionFailedPayload
    | EditCloudBackupStorageActionSuccessPayload;