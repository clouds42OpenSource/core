import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { TabBackupStorageActions } from 'app/modules/segments/TabBackupStorage/store/actions';
import { TabBackupStorageState } from 'app/modules/segments/TabBackupStorage/store/reducer';
import { GetCloudBackupStoragesActionAnyPayload, GetCloudBackupStoragesActionSuccessPayload } from 'app/modules/segments/TabBackupStorage/store/reducer/getCloudBackupStoragesReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по хранилищам бекапов
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudBackupStoragesReducer: TPartialReducerObject<TabBackupStorageState, GetCloudBackupStoragesActionAnyPayload> =
    (state, action) => {
        const processId = TabBackupStorageProcessId.GetCloudBackupStorages;
        const actions = TabBackupStorageActions.GetCloudBackupStorages;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetCloudBackupStoragesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudBackupStorages: payload,
                    hasSuccessFor: {
                        ...state,
                        hasCloudBackupStoragesReceived: true
                    }
                });
            }
        });
    };