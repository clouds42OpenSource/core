import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по хранилищам бекапов
 */
export type GetCloudBackupStoragesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по хранилищам бекапов
 */
export type GetCloudBackupStoragesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по хранилищам бекапов
 */
export type GetCloudBackupStoragesActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<CloudBackupStorageDataModel>;

/**
 * Все возможные Action при получении списка данных по хранилищам бекапов
 */
export type GetCloudBackupStoragesActionAnyPayload =
    | GetCloudBackupStoragesActionStartPayload
    | GetCloudBackupStoragesActionFailedPayload
    | GetCloudBackupStoragesActionSuccessPayload;