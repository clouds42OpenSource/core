import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления хранилища бекапов
 */
export type DeleteCloudBackupStorageThunkParams = IForceThunkParam & {
    /**
     * ID хранилища бекапов
     */
    backupStorageId: string
};