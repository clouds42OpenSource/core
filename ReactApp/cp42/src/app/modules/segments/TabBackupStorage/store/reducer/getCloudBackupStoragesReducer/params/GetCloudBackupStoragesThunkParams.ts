import { CloudBackupStorageFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка хранилища бекапов
 */
export type GetCloudBackupStoragesThunkParams = IForceThunkParam & CloudBackupStorageFilterParams;