import { TabBackupStorageProcessId } from 'app/modules/segments/TabBackupStorage';
import { TabBackupStorageConstants } from 'app/modules/segments/TabBackupStorage/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Хранилище бекапов"
 */
export const TabBackupStorageActions = {
    /**
     * Набор action на получение списка данных по хранилищам бэкапов
     */
    GetCloudBackupStorages: createReducerActions(TabBackupStorageConstants.reducerName, TabBackupStorageProcessId.GetCloudBackupStorages),

    /**
     * Набор action на создание хранилища бэкапов
     */
    CreateCloudBackupStorage: createReducerActions(TabBackupStorageConstants.reducerName, TabBackupStorageProcessId.CreateCloudBackupStorage),

    /**
     * Набор action на редактирование хранилища бэкапов
     */
    EditCloudBackupStorage: createReducerActions(TabBackupStorageConstants.reducerName, TabBackupStorageProcessId.EditCloudBackupStorage),

    /**
     * Набор action на получение хранилища бэкапов по ID
     */
    GetCloudBackupStorage: createReducerActions(TabBackupStorageConstants.reducerName, TabBackupStorageProcessId.GetCloudBackupStorage),

    /**
     * Набор action на удаление хранилища бэкапов по ID
     */
    DeleteCloudBackupStorage: createReducerActions(TabBackupStorageConstants.reducerName, TabBackupStorageProcessId.DeleteCloudBackupStorage)
};