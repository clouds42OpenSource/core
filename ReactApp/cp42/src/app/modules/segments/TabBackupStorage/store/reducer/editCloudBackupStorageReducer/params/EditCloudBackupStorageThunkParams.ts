import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования хранилища бекапов
 */
export type EditCloudBackupStorageThunkParams = IForceThunkParam & CloudBackupStorageDataModel;