import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabBackupStorageState } from 'app/modules/segments/TabBackupStorage/store/reducer';
import { createCloudBackupStorageReducer } from 'app/modules/segments/TabBackupStorage/store/reducer/createCloudBackupStorageReducer';
import { deleteCloudBackupStorageReducer } from 'app/modules/segments/TabBackupStorage/store/reducer/deleteCloudBackupStorageReducer';
import { editCloudBackupStorageReducer } from 'app/modules/segments/TabBackupStorage/store/reducer/editCloudBackupStorageReducer';
import { getCloudBackupStorageReducer } from 'app/modules/segments/TabBackupStorage/store/reducer/getCloudBackupStorageReducer';
import { getCloudBackupStoragesReducer } from 'app/modules/segments/TabBackupStorage/store/reducer/getCloudBackupStoragesReducer';
import { TabContentServersConstants } from 'app/modules/segments/TabContentServers/constants';
import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Хранилище бекапов
 */
const initialState = initReducerState<TabBackupStorageState>(
    TabContentServersConstants.reducerName,
    {
        cloudBackupStorages: {
            metadata: getInitialMetadata(),
            records: []
        },
        cloudBackupStorage: partialFillOf<CloudBackupStorageDataModel>(),
        hasSuccessFor: {
            hasCloudBackupStoragesReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Хранилище бекапов
 */
const partialReducers = [
    getCloudBackupStoragesReducer,
    deleteCloudBackupStorageReducer,
    getCloudBackupStorageReducer,
    createCloudBackupStorageReducer,
    editCloudBackupStorageReducer
];

/**
 * Редюсер Segments -> Хранилище бекапов
 */
export const TabBackupStorageReducer = createReducer(initialState, partialReducers);