import { CloudBackupStorageDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabBackupStorageApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения хранилища бекапов
 */
export type GetCloudBackupStorageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения хранилища бекапов
 */
export type GetCloudBackupStorageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения хранилища бекапов
 */
export type GetCloudBackupStorageActionSuccessPayload = ReducerActionSuccessPayload & CloudBackupStorageDataModel;

/**
 * Все возможные Action при получении хранилища бекапов
 */
export type GetCloudBackupStorageActionAnyPayload =
    | GetCloudBackupStorageActionStartPayload
    | GetCloudBackupStorageActionFailedPayload
    | GetCloudBackupStorageActionSuccessPayload;