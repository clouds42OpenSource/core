/**
 * Процесы Segments -> Таб фермы тс
 */
export enum TabTerminalFarmsProcessId {
    /**
     * Процесс на получение списка терминальных ферм с пагинацией
     */
    GetTerminalFarms = 'GET_TERMINAL_FARMS',

    /**
     * Процесс на получение терминальной фермы по ID
     */
    GetTerminalFarm = 'GET_TERMINAL_FARM',

    /**
     * Процесс на создание терминальной фермы
     */
    CreateTerminalFarm = 'CREATE_TERMINAL_FARM',

    /**
     * Процесс на удаление терминальной фермы
     */
    DeleteTerminalFarm = 'DELETE_TERMINAL_FARM',

    /**
     * Процесс на редактирование терминальной фермы
     */
    EditTerminalFarm = 'EDIT_TERMINAL_FARM'
}