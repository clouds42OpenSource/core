import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabTerminalFarmsConsts } from 'app/modules/segments/TabTerminalFarms/constants';
import { createTerminalFarmReducer } from 'app/modules/segments/TabTerminalFarms/store/reducers/createTerminalFarmReducer';
import { deleteTerminalFarmReducer } from 'app/modules/segments/TabTerminalFarms/store/reducers/deleteTerminalFarmReducer';
import { editTerminalFarmReducer } from 'app/modules/segments/TabTerminalFarms/store/reducers/editTerminalFarmReducer';
import { getTerminalFarmReducer } from 'app/modules/segments/TabTerminalFarms/store/reducers/getTerminalFarmReducer';
import { getTerminalFarmsReducer } from 'app/modules/segments/TabTerminalFarms/store/reducers/getTerminalFarmsReducer';
import { TabTerminalFarmsState } from 'app/modules/segments/TabTerminalFarms/store/reducers/TabTerminalFarmsState';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Таб фермы тс
 */
const initialState = initReducerState<TabTerminalFarmsState>(
    TabTerminalFarmsConsts.reducerName,
    {
        terminalFarms: {
            metadata: getInitialMetadata(),
            records: []
        },
        terminalFarm: partialFillOf<CommonSegmentDataModel>(),
        hasSuccessFor: {
            hasTerminalFarmsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Таб фермы тс
 */
const partialReducers = [
    getTerminalFarmsReducer,
    createTerminalFarmReducer,
    deleteTerminalFarmReducer,
    getTerminalFarmReducer,
    editTerminalFarmReducer
];

/**
 * Редюсер Segments -> Таб фермы тс
 */
export const TabTerminalFarmsReducer = createReducer(initialState, partialReducers);