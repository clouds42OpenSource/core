import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования терминальной фермы ТС
 */
export type EditTerminalFarmThunkParams = IForceThunkParam & CommonSegmentDataModel;