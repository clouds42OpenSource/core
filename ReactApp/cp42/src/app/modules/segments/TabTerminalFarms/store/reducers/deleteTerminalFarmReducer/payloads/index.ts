import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления терминальной фермы тс
 */
export type DeleteTerminalFarmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления терминальной фермы тс
 */
export type DeleteTerminalFarmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления терминальной фермы тс
 */
export type DeleteTerminalFarmActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удаления терминальной фермы тс
 */
export type DeleteTerminalFarmActionAnyPayload =
    | DeleteTerminalFarmActionStartPayload
    | DeleteTerminalFarmActionFailedPayload
    | DeleteTerminalFarmActionSuccessPayload;