import { CommonCreateSegmentParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания терминальной фермы ТС
 */
export type CreateTerminalFarmThunkParams = IForceThunkParam & CommonCreateSegmentParams;