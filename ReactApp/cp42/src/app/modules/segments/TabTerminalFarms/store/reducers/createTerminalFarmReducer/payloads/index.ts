import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания терминальной фермы тс
 */
export type CreateTerminalFarmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания терминальной фермы тс
 */
export type CreateTerminalFarmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания терминальной фермы тс
 */
export type CreateTerminalFarmActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создания терминальной фермы тс
 */
export type CreateTerminalFarmActionAnyPayload =
    | CreateTerminalFarmActionStartPayload
    | CreateTerminalFarmActionFailedPayload
    | CreateTerminalFarmActionSuccessPayload;