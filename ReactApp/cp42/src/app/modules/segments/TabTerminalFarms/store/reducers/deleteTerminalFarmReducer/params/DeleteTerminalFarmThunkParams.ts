import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления терминальной фермы
 */
export type DeleteTerminalFarmThunkParams = IForceThunkParam & {
    /**
     * ID терминальной фермы
     */
    terminalFarmId: string;
};