import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования терминальной фермы тс
 */
export type EditTerminalFarmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования терминальной фермы тс
 */
export type EditTerminalFarmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования терминальной фермы тс
 */
export type EditTerminalFarmActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактирования терминальной фермы тс
 */
export type EditTerminalFarmActionAnyPayload =
    | EditTerminalFarmActionStartPayload
    | EditTerminalFarmActionFailedPayload
    | EditTerminalFarmActionSuccessPayload;