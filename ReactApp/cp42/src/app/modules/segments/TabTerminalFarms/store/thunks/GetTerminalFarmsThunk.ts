import { TabTerminalFarmsActions } from 'app/modules/segments/TabTerminalFarms/store/actions';
import { GetTerminalFarmsThunkParams } from 'app/modules/segments/TabTerminalFarms/store/reducers/getTerminalFarmsReducer/params';
import { GetTerminalFarmsActionFailedPayload, GetTerminalFarmsActionStartPayload, GetTerminalFarmsActionSuccessPayload } from 'app/modules/segments/TabTerminalFarms/store/reducers/getTerminalFarmsReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = GetTerminalFarmsActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = GetTerminalFarmsActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = GetTerminalFarmsActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = GetTerminalFarmsThunkParams;

const { GetTerminalFarms: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabTerminalFarmsActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для получения списка терминальных ферм тс с пагинацией
 */
export class GetTerminalFarmsThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new GetTerminalFarmsThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        const tabTerminalFarmsState = args.getStore().TabTerminalFarmsState;
        return {
            condition: !tabTerminalFarmsState.hasSuccessFor.hasTerminalFarmsReceived || args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabTerminalFarmsState,
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getTabTerminalFarmsApiProxyMethods();

        try {
            const response = await api.getTerminalFarms(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}