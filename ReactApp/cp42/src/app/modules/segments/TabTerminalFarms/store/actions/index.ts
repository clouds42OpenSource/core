import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { TabTerminalFarmsConsts } from 'app/modules/segments/TabTerminalFarms/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> Таб фермы тс
 */
export const TabTerminalFarmsActions = {
    /**
     * Набор action на получение списка терминальных ферм с пагинацией
     */
    GetTerminalFarms: createReducerActions(TabTerminalFarmsConsts.reducerName, TabTerminalFarmsProcessId.GetTerminalFarms),

    /**
     * Набор action на получение терминальной фермы по ID
     */
    GetTerminalFarm: createReducerActions(TabTerminalFarmsConsts.reducerName, TabTerminalFarmsProcessId.GetTerminalFarm),

    /**
     * Набор action на создание терминальной фермы
     */
    CreateTerminalFarm: createReducerActions(TabTerminalFarmsConsts.reducerName, TabTerminalFarmsProcessId.CreateTerminalFarm),

    /**
     * Набор action на редактирование терминальной фермы
     */
    EditTerminalFarm: createReducerActions(TabTerminalFarmsConsts.reducerName, TabTerminalFarmsProcessId.EditTerminalFarm),

    /**
     * Набор action на удаление терминальной фермы
     */
    DeleteTerminalFarm: createReducerActions(TabTerminalFarmsConsts.reducerName, TabTerminalFarmsProcessId.DeleteTerminalFarm)
};