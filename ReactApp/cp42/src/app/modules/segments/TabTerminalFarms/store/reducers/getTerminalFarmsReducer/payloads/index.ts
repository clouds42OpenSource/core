import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения терминальных ферм с пагинацией
 */
export type GetTerminalFarmsActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения терминальных ферм с пагинацией
 */
export type GetTerminalFarmsActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения терминальных ферм с пагинацией
 */
export type GetTerminalFarmsActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<CommonSegmentDataModel>;

/**
 * Все возможные Action при получении терминальных ферм с пагинацией
 */
export type GetTerminalFarmsActionAnyPayload =
    | GetTerminalFarmsActionStartPayload
    | GetTerminalFarmsActionFailedPayload
    | GetTerminalFarmsActionSuccessPayload;