import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { TabTerminalFarmsActions } from 'app/modules/segments/TabTerminalFarms/store/actions';
import { TabTerminalFarmsState } from 'app/modules/segments/TabTerminalFarms/store/reducers';
import { GetTerminalFarmActionAnyPayload, GetTerminalFarmActionSuccessPayload } from 'app/modules/segments/TabTerminalFarms/store/reducers/getTerminalFarmReducer/payloads';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения терминальных ферм тс по ID
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getTerminalFarmReducer: TPartialReducerObject<TabTerminalFarmsState, GetTerminalFarmActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalFarmsProcessId.GetTerminalFarm;
        const actions = TabTerminalFarmsActions.GetTerminalFarm;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    terminalFarm: partialFillOf<CommonSegmentDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetTerminalFarmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    terminalFarm: payload
                });
            }
        });
    };