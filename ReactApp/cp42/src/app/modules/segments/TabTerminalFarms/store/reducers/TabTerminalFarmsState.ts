import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Таб фермы тс
 */
export type TabTerminalFarmsState = IReducerState<TabTerminalFarmsProcessId> & {
    /**
     * Массив терминальных ферм с пагинацией
     */
    terminalFarms: SelectDataMetadataResponseDto<CommonSegmentDataModel>,

    /**
     * Модель фермы ТС
     */
    terminalFarm: CommonSegmentDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то терминальные фермы получены
         */
        hasTerminalFarmsReceived: boolean
    };
};