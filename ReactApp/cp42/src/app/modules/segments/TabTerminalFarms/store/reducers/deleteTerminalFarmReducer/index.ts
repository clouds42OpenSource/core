import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { TabTerminalFarmsActions } from 'app/modules/segments/TabTerminalFarms/store/actions';
import { TabTerminalFarmsState } from 'app/modules/segments/TabTerminalFarms/store/reducers';
import { DeleteTerminalFarmActionAnyPayload, DeleteTerminalFarmActionSuccessPayload } from 'app/modules/segments/TabTerminalFarms/store/reducers/deleteTerminalFarmReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления терминальной фермы ТС
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteTerminalFarmReducer: TPartialReducerObject<TabTerminalFarmsState, DeleteTerminalFarmActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalFarmsProcessId.DeleteTerminalFarm;
        const actions = TabTerminalFarmsActions.DeleteTerminalFarm;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteTerminalFarmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };