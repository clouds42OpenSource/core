import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { TabTerminalFarmsActions } from 'app/modules/segments/TabTerminalFarms/store/actions';
import { TabTerminalFarmsState } from 'app/modules/segments/TabTerminalFarms/store/reducers';
import { CreateTerminalFarmActionAnyPayload, CreateTerminalFarmActionSuccessPayload } from 'app/modules/segments/TabTerminalFarms/store/reducers/createTerminalFarmReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания терминальной фермы ТС
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createTerminalFarmReducer: TPartialReducerObject<TabTerminalFarmsState, CreateTerminalFarmActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalFarmsProcessId.CreateTerminalFarm;
        const actions = TabTerminalFarmsActions.CreateTerminalFarm;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateTerminalFarmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };