import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения терминальных ферм
 */
export type GetTerminalFarmActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения терминальных ферм
 */
export type GetTerminalFarmActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения терминальных ферм
 */
export type GetTerminalFarmActionSuccessPayload = ReducerActionSuccessPayload & CommonSegmentDataModel;

/**
 * Все возможные Action при получении терминальных ферм
 */
export type GetTerminalFarmActionAnyPayload =
    | GetTerminalFarmActionStartPayload
    | GetTerminalFarmActionFailedPayload
    | GetTerminalFarmActionSuccessPayload;