import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения терминальной фермы по ID
 */
export type GetTerminalFarmThunkParams = IForceThunkParam & {
    /**
     * ID терминальной фемры ТС
     */
    terminalFarmId: string;
};