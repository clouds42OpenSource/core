import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { TabTerminalFarmsActions } from 'app/modules/segments/TabTerminalFarms/store/actions';
import { TabTerminalFarmsState } from 'app/modules/segments/TabTerminalFarms/store/reducers';
import { EditTerminalFarmActionAnyPayload, EditTerminalFarmActionSuccessPayload } from 'app/modules/segments/TabTerminalFarms/store/reducers/editTerminalFarmReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования терминальной фермы ТС
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editTerminalFarmReducer: TPartialReducerObject<TabTerminalFarmsState, EditTerminalFarmActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalFarmsProcessId.EditTerminalFarm;
        const actions = TabTerminalFarmsActions.EditTerminalFarm;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditTerminalFarmActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };