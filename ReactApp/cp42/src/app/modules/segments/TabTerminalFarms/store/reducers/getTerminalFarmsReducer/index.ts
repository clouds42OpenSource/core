import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalFarmsProcessId } from 'app/modules/segments/TabTerminalFarms';
import { TabTerminalFarmsActions } from 'app/modules/segments/TabTerminalFarms/store/actions';
import { TabTerminalFarmsState } from 'app/modules/segments/TabTerminalFarms/store/reducers';
import { GetTerminalFarmsActionAnyPayload, GetTerminalFarmsActionSuccessPayload } from 'app/modules/segments/TabTerminalFarms/store/reducers/getTerminalFarmsReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения терминальных ферм тс с пагинацией
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getTerminalFarmsReducer: TPartialReducerObject<TabTerminalFarmsState, GetTerminalFarmsActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalFarmsProcessId.GetTerminalFarms;
        const actions = TabTerminalFarmsActions.GetTerminalFarms;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetTerminalFarmsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    terminalFarms: payload,
                    hasSuccessFor: {
                        ...state,
                        hasTerminalFarmsReceived: true
                    }
                });
            }
        });
    };