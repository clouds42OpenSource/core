import { CommonSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры на получение терминальных ферм тс
 */
export type GetTerminalFarmsThunkParams = IForceThunkParam & CommonSegmentFilterParams;