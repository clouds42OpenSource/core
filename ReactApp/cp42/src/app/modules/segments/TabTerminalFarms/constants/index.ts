/**
 * Константы для модуля Segments -> Таб Фермы ТС
 */
export const TabTerminalFarmsConsts = {
    /**
     * Название редюсера для модуля Segments -> Таб Фермы ТС
     */
    reducerName: 'TAB_TERMINAL_FARMS'
};