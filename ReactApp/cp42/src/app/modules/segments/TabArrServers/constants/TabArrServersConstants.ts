/**
 * Константы для модуля Segments -> Таб Сервера ARR
 */
export const TabArrServersConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб Сервера ARR
     */
    reducerName: 'TAB_ARR_SERVERS'
};