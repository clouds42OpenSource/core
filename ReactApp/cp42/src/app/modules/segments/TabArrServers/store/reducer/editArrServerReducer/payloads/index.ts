import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования сервера ARR
 */
export type EditArrServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования сервера ARR
 */
export type EditArrServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования сервера ARR
 */
export type EditArrServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при выполнении редактирования сервера ARR
 */
export type EditArrServerActionAnyPayload =
    | EditArrServerActionStartPayload
    | EditArrServerActionFailedPayload
    | EditArrServerActionSuccessPayload;