import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { TabArrServersActions } from 'app/modules/segments/TabArrServers/store/actions';
import { TabArrServersState } from 'app/modules/segments/TabArrServers/store/reducer';
import { GetArrServerActionAnyPayload, GetArrServerActionSuccessPayload } from 'app/modules/segments/TabArrServers/store/reducer/getArrServerReducer/payloads';
import { ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения сервера ARR
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getArrServerReducer: TPartialReducerObject<TabArrServersState, GetArrServerActionAnyPayload> =
    (state, action) => {
        const processId = TabArrServersProcessId.GetArrServer;
        const actions = TabArrServersActions.GetArrServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    arrServer: partialFillOf<ArrServerDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetArrServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    arrServer: payload
                });
            }
        });
    };