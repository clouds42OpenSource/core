import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания сервера ARR
 */
export type CreateArrServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания сервера ARR
 */
export type CreateArrServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания сервера ARR
 */
export type CreateArrServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при выполнении создания сервера ARR
 */
export type CreateArrServerActionAnyPayload =
    | CreateArrServerActionStartPayload
    | CreateArrServerActionFailedPayload
    | CreateArrServerActionSuccessPayload;