import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения проверки на доступность сервера ARR
 */
export type CheckArrServerAvailabilityActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении проверки на доступность сервера ARR
 */
export type CheckArrServerAvailabilityActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении проверки на доступность сервера ARR
 */
export type CheckArrServerAvailabilityActionSuccessPayload = ReducerActionSuccessPayload & boolean;

/**
 * Все возможные Action при выполнении проверки на доступность сервера ARR
 */
export type CheckArrServerAvailabilityActionAnyPayload =
    | CheckArrServerAvailabilityActionStartPayload
    | CheckArrServerAvailabilityActionFailedPayload
    | CheckArrServerAvailabilityActionSuccessPayload;