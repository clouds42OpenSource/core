import { templateErrorText } from 'app/api';
import { TabArrServersActions } from 'app/modules/segments/TabArrServers/store/actions';
import { DeleteArrServerThunkParams } from 'app/modules/segments/TabArrServers/store/reducer/deleteArrServerReducer/params';
import { DeleteArrServerActionFailedPayload, DeleteArrServerActionStartPayload, DeleteArrServerActionSuccessPayload } from 'app/modules/segments/TabArrServers/store/reducer/deleteArrServerReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = DeleteArrServerActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = DeleteArrServerActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = DeleteArrServerActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = DeleteArrServerThunkParams;

const { DeleteArrServer: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabArrServersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для удаления сервера ARR
 */
export class DeleteArrServerThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new DeleteArrServerThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabArrServersState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getArrServersTabApiProxyMethods();

        try {
            const response = await api.deleteArrServer(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!.arrServerId);
            if (response?.success === false) {
                throw new Error(response.message ?? templateErrorText);
            }
            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}