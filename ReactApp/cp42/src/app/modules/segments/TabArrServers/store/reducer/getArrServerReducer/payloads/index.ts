import { ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения сервера ARR
 */
export type GetArrServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения сервера ARR
 */
export type GetArrServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения сервера ARR
 */
export type GetArrServerActionSuccessPayload = ReducerActionSuccessPayload & ArrServerDataModel;

/**
 * Все возможные Action при выполнении получения сервера ARR
 */
export type GetArrServerActionAnyPayload =
    | GetArrServerActionStartPayload
    | GetArrServerActionFailedPayload
    | GetArrServerActionSuccessPayload;