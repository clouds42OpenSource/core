import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для проверки на доступность сервер ARR
 */
export type CheckArrServerAvailabilityThunkParams = IForceThunkParam & {
    /**
     * Адрес сервера ARR
     */
    address: string
};