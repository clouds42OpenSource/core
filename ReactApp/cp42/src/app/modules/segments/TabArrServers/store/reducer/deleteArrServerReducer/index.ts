import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { TabArrServersActions } from 'app/modules/segments/TabArrServers/store/actions';
import { TabArrServersState } from 'app/modules/segments/TabArrServers/store/reducer';
import { DeleteArrServerActionAnyPayload, DeleteArrServerActionSuccessPayload } from 'app/modules/segments/TabArrServers/store/reducer/deleteArrServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления сервера ARR
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteArrServerReducer: TPartialReducerObject<TabArrServersState, DeleteArrServerActionAnyPayload> =
    (state, action) => {
        const processId = TabArrServersProcessId.DeleteArrServer;
        const actions = TabArrServersActions.DeleteArrServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteArrServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };