import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { TabArrServersActions } from 'app/modules/segments/TabArrServers/store/actions';
import { TabArrServersState } from 'app/modules/segments/TabArrServers/store/reducer';
import { CheckArrServerAvailabilityActionAnyPayload, CheckArrServerAvailabilityActionSuccessPayload } from 'app/modules/segments/TabArrServers/store/reducer/checkArrServerAvailabilityReducer/payloads';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для проверки на доступность сервера ARR
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const checkArrServerAvailabilityReducer: TPartialReducerObject<TabArrServersState, CheckArrServerAvailabilityActionAnyPayload> =
    (state, action) => {
        const processId = TabArrServersProcessId.CheckArrServerAvailability;
        const actions = TabArrServersActions.CheckArrServerAvailability;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    isArrServerAvailable: false
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as CheckArrServerAvailabilityActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    isArrServerAvailable: payload
                });
            }
        });
    };