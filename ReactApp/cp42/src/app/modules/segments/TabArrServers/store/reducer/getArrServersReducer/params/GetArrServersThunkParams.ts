import { CommonSegmentFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка серверов ARR
 */
export type GetArrServersThunkParams = IForceThunkParam & CommonSegmentFilterParams;