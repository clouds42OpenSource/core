import { ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по серверам ARR
 */
export type GetArrServersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по серверам ARR
 */
export type GetArrServersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по серверам ARR
 */
export type GetArrServersActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<ArrServerDataModel>;

/**
 * Все возможные Action при получении списка данных по серверам ARR
 */
export type GetArrServersActionAnyPayload =
    | GetArrServersActionStartPayload
    | GetArrServersActionFailedPayload
    | GetArrServersActionSuccessPayload;