import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения модели сервера ARR
 */
export type GetArrServerThunkParams = IForceThunkParam & {
    /**
     * Id сервера ARR
     */
    arrServerId: string
};