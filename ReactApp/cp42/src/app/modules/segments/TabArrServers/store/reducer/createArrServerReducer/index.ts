import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { TabArrServersActions } from 'app/modules/segments/TabArrServers/store/actions';
import { TabArrServersState } from 'app/modules/segments/TabArrServers/store/reducer';
import { CreateArrServerActionAnyPayload, CreateArrServerActionSuccessPayload } from 'app/modules/segments/TabArrServers/store/reducer/createArrServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания сервера ARR
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createArrServerReducer: TPartialReducerObject<TabArrServersState, CreateArrServerActionAnyPayload> =
    (state, action) => {
        const processId = TabArrServersProcessId.CreateArrServer;
        const actions = TabArrServersActions.CreateArrServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateArrServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };