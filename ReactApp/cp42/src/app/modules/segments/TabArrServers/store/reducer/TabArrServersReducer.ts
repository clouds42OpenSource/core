import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabArrServersConstants } from 'app/modules/segments/TabArrServers/constants';
import { TabArrServersState } from 'app/modules/segments/TabArrServers/store/reducer';
import { checkArrServerAvailabilityReducer } from 'app/modules/segments/TabArrServers/store/reducer/checkArrServerAvailabilityReducer';
import { createArrServerReducer } from 'app/modules/segments/TabArrServers/store/reducer/createArrServerReducer';
import { deleteArrServerReducer } from 'app/modules/segments/TabArrServers/store/reducer/deleteArrServerReducer';
import { editArrServerReducer } from 'app/modules/segments/TabArrServers/store/reducer/editArrServerReducer';
import { getArrServerReducer } from 'app/modules/segments/TabArrServers/store/reducer/getArrServerReducer';
import { getArrServersReducer } from 'app/modules/segments/TabArrServers/store/reducer/getArrServersReducer';
import { ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Сервера ARR
 */
const initialState = initReducerState<TabArrServersState>(
    TabArrServersConstants.reducerName,
    {
        arrServers: {
            metadata: getInitialMetadata(),
            records: []
        },
        arrServer: partialFillOf<ArrServerDataModel>(),
        isArrServerAvailable: false,
        hasSuccessFor: {
            hasArrServersReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Сервера ARR
 */
const partialReducers = [
    getArrServersReducer,
    deleteArrServerReducer,
    getArrServerReducer,
    createArrServerReducer,
    editArrServerReducer,
    checkArrServerAvailabilityReducer
];

/**
 * Редюсер Segments -> Сервера ARR
 */
export const TabArrServersReducer = createReducer(initialState, partialReducers);