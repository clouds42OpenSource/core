import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { TabArrServersActions } from 'app/modules/segments/TabArrServers/store/actions';
import { TabArrServersState } from 'app/modules/segments/TabArrServers/store/reducer';
import { GetArrServersActionAnyPayload, GetArrServersActionSuccessPayload } from 'app/modules/segments/TabArrServers/store/reducer/getArrServersReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по серверам ARR
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getArrServersReducer: TPartialReducerObject<TabArrServersState, GetArrServersActionAnyPayload> =
    (state, action) => {
        const processId = TabArrServersProcessId.GetArrServers;
        const actions = TabArrServersActions.GetArrServers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetArrServersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    arrServers: payload,
                    hasSuccessFor: {
                        ...state,
                        hasArrServersReceived: true
                    }
                });
            }
        });
    };