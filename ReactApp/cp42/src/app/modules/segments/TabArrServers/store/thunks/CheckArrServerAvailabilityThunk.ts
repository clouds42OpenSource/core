import { TabArrServersActions } from 'app/modules/segments/TabArrServers/store/actions';
import { CheckArrServerAvailabilityThunkParams } from 'app/modules/segments/TabArrServers/store/reducer/checkArrServerAvailabilityReducer/params';
import {
    CheckArrServerAvailabilityActionFailedPayload,
    CheckArrServerAvailabilityActionStartPayload,
    CheckArrServerAvailabilityActionSuccessPayload
} from 'app/modules/segments/TabArrServers/store/reducer/checkArrServerAvailabilityReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = CheckArrServerAvailabilityActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = CheckArrServerAvailabilityActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = CheckArrServerAvailabilityActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = CheckArrServerAvailabilityThunkParams;

const { CheckArrServerAvailability: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabArrServersActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для проверки на доступность сервера ARR
 */
export class CheckArrServerAvailabilityThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new CheckArrServerAvailabilityThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabArrServersState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getArrServersTabApiProxyMethods();

        try {
            const response = await api.checkArrServerAvailability(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!.address);
            args.success(response);
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}