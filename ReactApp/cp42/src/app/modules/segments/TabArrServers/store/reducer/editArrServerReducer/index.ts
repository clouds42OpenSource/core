import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { TabArrServersActions } from 'app/modules/segments/TabArrServers/store/actions';
import { TabArrServersState } from 'app/modules/segments/TabArrServers/store/reducer';
import { EditArrServerActionAnyPayload, EditArrServerActionSuccessPayload } from 'app/modules/segments/TabArrServers/store/reducer/editArrServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования сервера ARR
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editArrServerReducer: TPartialReducerObject<TabArrServersState, EditArrServerActionAnyPayload> =
    (state, action) => {
        const processId = TabArrServersProcessId.EditArrServer;
        const actions = TabArrServersActions.EditArrServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditArrServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };