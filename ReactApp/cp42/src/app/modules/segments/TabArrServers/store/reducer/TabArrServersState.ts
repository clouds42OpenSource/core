import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Сервера ARR
 */
export type TabArrServersState = IReducerState<TabArrServersProcessId> & {
    /**
     * Массив сервера ARR
     */
    arrServers: SelectDataMetadataResponseDto<ArrServerDataModel>,

    /**
     * Модель сервера ARR
     */
    arrServer: ArrServerDataModel,

    /**
     * Флаг на доступность сервера ARR
     */
    isArrServerAvailable: boolean,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то сервера ARR получены
         */
        hasArrServersReceived: boolean
    };
};