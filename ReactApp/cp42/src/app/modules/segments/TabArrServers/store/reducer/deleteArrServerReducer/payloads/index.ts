import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления сервера ARR
 */
export type DeleteArrServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления сервера ARR
 */
export type DeleteArrServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления сервера ARR
 */
export type DeleteArrServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении сервера ARR
 */
export type DeleteArrServerActionAnyPayload =
    | DeleteArrServerActionStartPayload
    | DeleteArrServerActionFailedPayload
    | DeleteArrServerActionSuccessPayload;