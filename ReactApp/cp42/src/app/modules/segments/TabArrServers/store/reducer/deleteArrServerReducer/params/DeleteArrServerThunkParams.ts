import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления сервера ARR
 */
export type DeleteArrServerThunkParams = IForceThunkParam & {
    /**
     * Id сервера ARR
     */
    arrServerId: string
};