import { ArrServerCreateDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания сервера ARR
 */
export type CreateArrServerThunkParams = IForceThunkParam & ArrServerCreateDataModel;