import { ArrServerDataModel } from 'app/web/api/SegmentsProxy/TabArrServers/response-dto';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования сервера ARR
 */
export type EditArrServerThunkParams = IForceThunkParam & ArrServerDataModel;