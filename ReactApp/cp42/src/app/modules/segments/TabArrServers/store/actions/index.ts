import { TabArrServersProcessId } from 'app/modules/segments/TabArrServers';
import { TabArrServersConstants } from 'app/modules/segments/TabArrServers/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Сервера ARR"
 */
export const TabArrServersActions = {
    /**
     * Набор action на получение списка данных по серверам ARR
     */
    GetArrServers: createReducerActions(TabArrServersConstants.reducerName, TabArrServersProcessId.GetArrServers),

    /**
     * Набор action на создание сервера ARR
     */
    CreateArrServer: createReducerActions(TabArrServersConstants.reducerName, TabArrServersProcessId.CreateArrServer),

    /**
     * Набор action на редактирование сервера ARR
     */
    EditArrServer: createReducerActions(TabArrServersConstants.reducerName, TabArrServersProcessId.EditArrServer),

    /**
     * Набор action на получение сервера ARR по ID
     */
    GetArrServer: createReducerActions(TabArrServersConstants.reducerName, TabArrServersProcessId.GetArrServer),

    /**
     * Набор action на удаление сервера ARR по ID
     */
    DeleteArrServer: createReducerActions(TabArrServersConstants.reducerName, TabArrServersProcessId.DeleteArrServer),

    /**
     * Набор action на проверку доступности сервера ARR
     */
    CheckArrServerAvailability: createReducerActions(TabArrServersConstants.reducerName, TabArrServersProcessId.CheckArrServerAvailability)
};