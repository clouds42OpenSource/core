/**
 * Процесы Segments -> "Сервера ARR"
 */
export enum TabArrServersProcessId {
    /**
     * Процесс на получение списка данных по серверам ARR
     */
    GetArrServers = 'GET_ARR_SERVERS',

    /**
     * Процесс на создание сервера ARR
     */
    CreateArrServer = 'CREATE_ARR_SERVER',

    /**
     * Процесс на редактирование сервера ARR
     */
    EditArrServer = 'EDIT_ARR_SERVER',

    /**
     * Процесс на получение сервера ARR по ID
     */
    GetArrServer = 'GET_ARR_SERVER',

    /**
     * Процесс на удаление сервера ARR по ID
     */
    DeleteArrServer = 'DELETE_ARR_SERVER',

    /**
     * Процесс на проверку доступности сервера ARR
     */
    CheckArrServerAvailability = 'CHECK_ARR_SERVER_AVAILABILITY'
}