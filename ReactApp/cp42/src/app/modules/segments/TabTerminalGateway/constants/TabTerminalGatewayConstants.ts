/**
 * Константы для модуля Segments -> Таб Терм. шлюзы
 */
export const TabTerminalGatewayConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб Терм. шлюзы
     */
    reducerName: 'TAB_TERMINAL_GATEWAY'
};