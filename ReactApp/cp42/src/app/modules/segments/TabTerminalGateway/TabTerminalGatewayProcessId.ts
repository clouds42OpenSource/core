/**
 * Процесы Segments -> "Терм. шлюзы"
 */
export enum TabTerminalGatewayProcessId {
    /**
     * Процесс на получение списка данных по терминальным шлюзам
     */
    GetTerminalGateways = 'GET_TERMINAL_GATEWAYS',

    /**
     * Процесс на создание терминального шлюза
     */
    CreateTerminalGateway = 'CREATE_TERMINAL_GATEWAY',

    /**
     * Процесс на редактирование терминального шлюза
     */
    EditTerminalGateway = 'EDIT_TERMINAL_GATEWAY',

    /**
     * Процесс на получение терминального шлюза по ID
     */
    GetTerminalGateway = 'GET_TERMINAL_GATEWAY',

    /**
     * Процесс на удаление терминального шлюза по ID
     */
    DeleteTerminalGateway = 'DELETE_TERMINAL_GATEWAY'
}