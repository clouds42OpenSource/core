import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления терминального шлюза
 */
export type DeleteCloudGatewayTerminalThunkParams = IForceThunkParam & {
    /**
     * ID терминального шлюза
     */
    cloudGatewayTerminalId: string
};