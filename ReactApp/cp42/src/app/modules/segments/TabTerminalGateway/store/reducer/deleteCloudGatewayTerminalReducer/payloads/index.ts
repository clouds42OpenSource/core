import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления терминального шлюза
 */
export type DeleteTerminalGatewayActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления терминального шлюза
 */
export type DeleteTerminalGatewayActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления терминального шлюза
 */
export type DeleteTerminalGatewayActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении терминального шлюза
 */
export type DeleteTerminalGatewayActionAnyPayload =
    | DeleteTerminalGatewayActionStartPayload
    | DeleteTerminalGatewayActionFailedPayload
    | DeleteTerminalGatewayActionSuccessPayload;