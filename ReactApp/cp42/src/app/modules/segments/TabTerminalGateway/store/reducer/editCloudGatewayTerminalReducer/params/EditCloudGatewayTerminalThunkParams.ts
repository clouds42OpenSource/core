import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования терминального шлюза
 */
export type EditCloudGatewayTerminalThunkParams = IForceThunkParam & CommonSegmentDataModel;