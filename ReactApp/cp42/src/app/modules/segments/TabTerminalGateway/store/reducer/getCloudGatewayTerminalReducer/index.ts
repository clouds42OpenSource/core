import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { TabTerminalGatewayActions } from 'app/modules/segments/TabTerminalGateway/store/actions';
import { TabTerminalGatewayState } from 'app/modules/segments/TabTerminalGateway/store/reducer';
import { GetTerminalGatewayActionAnyPayload, GetTerminalGatewayActionSuccessPayload } from 'app/modules/segments/TabTerminalGateway/store/reducer/getCloudGatewayTerminalReducer/payloads';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения терминального шлюза
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudGatewayTerminalReducer: TPartialReducerObject<TabTerminalGatewayState, GetTerminalGatewayActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalGatewayProcessId.GetTerminalGateway;
        const actions = TabTerminalGatewayActions.GetTerminalGateway;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    cloudGatewayTerminal: partialFillOf<CommonSegmentDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetTerminalGatewayActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudGatewayTerminal: payload
                });
            }
        });
    };