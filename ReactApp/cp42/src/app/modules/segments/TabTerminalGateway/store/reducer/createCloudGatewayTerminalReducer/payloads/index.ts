import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания терминального шлюза
 */
export type CreateTerminalGatewayActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания терминального шлюза
 */
export type CreateTerminalGatewayActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания терминального шлюза
 */
export type CreateTerminalGatewayActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании терминального шлюза
 */
export type CreateTerminalGatewayActionAnyPayload =
    | CreateTerminalGatewayActionStartPayload
    | CreateTerminalGatewayActionFailedPayload
    | CreateTerminalGatewayActionSuccessPayload;