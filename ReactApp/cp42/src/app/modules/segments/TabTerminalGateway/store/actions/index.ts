import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { TabTerminalGatewayConstants } from 'app/modules/segments/TabTerminalGateway/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Терм. шлюзы"
 */
export const TabTerminalGatewayActions = {
    /**
     * Набор action на получение списка данных по терминальным шлюзам
     */
    GetTerminalGateways: createReducerActions(TabTerminalGatewayConstants.reducerName, TabTerminalGatewayProcessId.GetTerminalGateways),

    /**
     * Набор action на создание терминального шлюза
     */
    CreateTerminalGateway: createReducerActions(TabTerminalGatewayConstants.reducerName, TabTerminalGatewayProcessId.CreateTerminalGateway),

    /**
     * Набор action на редактирование терминального шлюза
     */
    EditTerminalGateway: createReducerActions(TabTerminalGatewayConstants.reducerName, TabTerminalGatewayProcessId.EditTerminalGateway),

    /**
     * Набор action на получение терминального шлюза по ID
     */
    GetTerminalGateway: createReducerActions(TabTerminalGatewayConstants.reducerName, TabTerminalGatewayProcessId.GetTerminalGateway),

    /**
     * Набор action на удаление терминального шлюза по ID
     */
    DeleteTerminalGateway: createReducerActions(TabTerminalGatewayConstants.reducerName, TabTerminalGatewayProcessId.DeleteTerminalGateway)
};