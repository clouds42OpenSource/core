import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования терминального шлюза
 */
export type EditTerminalGatewayActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования терминального шлюза
 */
export type EditTerminalGatewayActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования терминального шлюза
 */
export type EditTerminalGatewayActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании терминального шлюза
 */
export type EditTerminalGatewayActionAnyPayload =
    | EditTerminalGatewayActionStartPayload
    | EditTerminalGatewayActionFailedPayload
    | EditTerminalGatewayActionSuccessPayload;