import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { TabTerminalGatewayActions } from 'app/modules/segments/TabTerminalGateway/store/actions';
import { TabTerminalGatewayState } from 'app/modules/segments/TabTerminalGateway/store/reducer';
import { EditTerminalGatewayActionAnyPayload, EditTerminalGatewayActionSuccessPayload } from 'app/modules/segments/TabTerminalGateway/store/reducer/editCloudGatewayTerminalReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования терминального шлюза
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editCloudGatewayTerminalReducer: TPartialReducerObject<TabTerminalGatewayState, EditTerminalGatewayActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalGatewayProcessId.EditTerminalGateway;
        const actions = TabTerminalGatewayActions.EditTerminalGateway;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditTerminalGatewayActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };