import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по терминальным шлюзам
 */
export type GetTerminalGatewaysActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по терминальным шлюзам
 */
export type GetTerminalGatewaysActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по терминальным шлюзам
 */
export type GetTerminalGatewaysActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<CommonSegmentDataModel>;

/**
 * Все возможные Action при получении списка данных по терминальным шлюзам
 */
export type GetTerminalGatewaysActionAnyPayload =
    | GetTerminalGatewaysActionStartPayload
    | GetTerminalGatewaysActionFailedPayload
    | GetTerminalGatewaysActionSuccessPayload;