import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { TabTerminalGatewayActions } from 'app/modules/segments/TabTerminalGateway/store/actions';
import { TabTerminalGatewayState } from 'app/modules/segments/TabTerminalGateway/store/reducer';
import { DeleteTerminalGatewayActionAnyPayload, DeleteTerminalGatewayActionSuccessPayload } from 'app/modules/segments/TabTerminalGateway/store/reducer/deleteCloudGatewayTerminalReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления терминального шлюза
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteCloudGatewayTerminalReducer: TPartialReducerObject<TabTerminalGatewayState, DeleteTerminalGatewayActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalGatewayProcessId.DeleteTerminalGateway;
        const actions = TabTerminalGatewayActions.DeleteTerminalGateway;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteTerminalGatewayActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };