import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { TabTerminalGatewayActions } from 'app/modules/segments/TabTerminalGateway/store/actions';
import { TabTerminalGatewayState } from 'app/modules/segments/TabTerminalGateway/store/reducer';
import { CreateTerminalGatewayActionAnyPayload, CreateTerminalGatewayActionSuccessPayload } from 'app/modules/segments/TabTerminalGateway/store/reducer/createCloudGatewayTerminalReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания терминального шлюза
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createCloudGatewayTerminalReducer: TPartialReducerObject<TabTerminalGatewayState, CreateTerminalGatewayActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalGatewayProcessId.CreateTerminalGateway;
        const actions = TabTerminalGatewayActions.CreateTerminalGateway;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateTerminalGatewayActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };