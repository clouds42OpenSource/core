import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { TabTerminalGatewayActions } from 'app/modules/segments/TabTerminalGateway/store/actions';
import { TabTerminalGatewayState } from 'app/modules/segments/TabTerminalGateway/store/reducer';
import { GetTerminalGatewaysActionAnyPayload, GetTerminalGatewaysActionSuccessPayload } from 'app/modules/segments/TabTerminalGateway/store/reducer/getCloudGatewayTerminalsReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по терминальным шлюзам
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudGatewayTerminalsReducer: TPartialReducerObject<TabTerminalGatewayState, GetTerminalGatewaysActionAnyPayload> =
    (state, action) => {
        const processId = TabTerminalGatewayProcessId.GetTerminalGateways;
        const actions = TabTerminalGatewayActions.GetTerminalGateways;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetTerminalGatewaysActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudGatewayTerminals: payload,
                    hasSuccessFor: {
                        ...state,
                        hasCloudGatewayTerminalsReceived: true
                    }
                });
            }
        });
    };