import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения терминального шлюза
 */
export type GetCloudGatewayTerminalThunkParams = IForceThunkParam & {
    /**
     * ID терминального шлюза
     */
    cloudGatewayTerminalId: string
};