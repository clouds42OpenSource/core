import { CommonCreateSegmentParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания терминального шлюза
 */
export type CreateCloudGatewayTerminalThunkParams = IForceThunkParam & CommonCreateSegmentParams;