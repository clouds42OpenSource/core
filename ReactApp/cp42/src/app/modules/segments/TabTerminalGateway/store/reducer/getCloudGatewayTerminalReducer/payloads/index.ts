import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения терминального шлюза
 */
export type GetTerminalGatewayActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения терминального шлюза
 */
export type GetTerminalGatewayActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения терминального шлюза
 */
export type GetTerminalGatewayActionSuccessPayload = ReducerActionSuccessPayload & CommonSegmentDataModel;

/**
 * Все возможные Action при получении терминального шлюза
 */
export type GetTerminalGatewayActionAnyPayload =
    | GetTerminalGatewayActionStartPayload
    | GetTerminalGatewayActionFailedPayload
    | GetTerminalGatewayActionSuccessPayload;