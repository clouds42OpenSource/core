import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabTerminalGatewayConstants } from 'app/modules/segments/TabTerminalGateway/constants';
import { TabTerminalGatewayState } from 'app/modules/segments/TabTerminalGateway/store/reducer';
import { createCloudGatewayTerminalReducer } from 'app/modules/segments/TabTerminalGateway/store/reducer/createCloudGatewayTerminalReducer';
import { deleteCloudGatewayTerminalReducer } from 'app/modules/segments/TabTerminalGateway/store/reducer/deleteCloudGatewayTerminalReducer';
import { editCloudGatewayTerminalReducer } from 'app/modules/segments/TabTerminalGateway/store/reducer/editCloudGatewayTerminalReducer';
import { getCloudGatewayTerminalReducer } from 'app/modules/segments/TabTerminalGateway/store/reducer/getCloudGatewayTerminalReducer';
import { getCloudGatewayTerminalsReducer } from 'app/modules/segments/TabTerminalGateway/store/reducer/getCloudGatewayTerminalsReducer';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Терм. шлюзы
 */
const initialState = initReducerState<TabTerminalGatewayState>(
    TabTerminalGatewayConstants.reducerName,
    {
        cloudGatewayTerminals: {
            metadata: getInitialMetadata(),
            records: []
        },
        cloudGatewayTerminal: partialFillOf<CommonSegmentDataModel>(),
        hasSuccessFor: {
            hasCloudGatewayTerminalsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Терм. шлюзы
 */
const partialReducers = [
    getCloudGatewayTerminalsReducer,
    deleteCloudGatewayTerminalReducer,
    getCloudGatewayTerminalReducer,
    createCloudGatewayTerminalReducer,
    editCloudGatewayTerminalReducer
];

/**
 * Редюсер Segments -> Терм. шлюзы
 */
export const TabTerminalGatewayReducer = createReducer(initialState, partialReducers);