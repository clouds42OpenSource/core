import { templateErrorText } from 'app/api';
import { TabTerminalGatewayActions } from 'app/modules/segments/TabTerminalGateway/store/actions';
import { DeleteCloudGatewayTerminalThunkParams } from 'app/modules/segments/TabTerminalGateway/store/reducer/deleteCloudGatewayTerminalReducer/params';
import { DeleteTerminalGatewayActionFailedPayload, DeleteTerminalGatewayActionStartPayload, DeleteTerminalGatewayActionSuccessPayload } from 'app/modules/segments/TabTerminalGateway/store/reducer/deleteCloudGatewayTerminalReducer/payloads';
import { AppReduxStoreState } from 'app/redux/types';
import { InterlayerApiProxy } from 'app/web/InterlayerApiProxy';
import { ErrorObject } from 'core/redux';
import { BaseReduxThunkObject } from 'core/redux/BaseReduxThunkObject';
import { IThunkStartActionParams, IThunkStartActionResult, IThunkStartExecutionParams } from 'core/redux/BaseReduxThunkObject/interfaces';
import { RequestKind } from 'core/requestSender/enums';

/**
 * Тип данных в редюсер при вызове действия START_ACTION
 */
type TActionStartPayload = DeleteTerminalGatewayActionStartPayload;

/**
 * Тип данных в редюсер при успешном вызове действия SUCCESS_ACTION
 */
type TActionSuccessPayload = DeleteTerminalGatewayActionSuccessPayload;

/**
 * Тип данных в редюсер при неуспешном вызове действия FAILED_ACTION
 */
type TActionFailedPayload = DeleteTerminalGatewayActionFailedPayload;

/**
 * Тип параметров выполнения Thunk-и
 */
type TInputParams = DeleteCloudGatewayTerminalThunkParams;

const { DeleteTerminalGateway: { START_ACTION, SUCCESS_ACTION, FAILED_ACTION } } = TabTerminalGatewayActions;

/**
 * Тип параметров Thunk-и при старте выполнении
 */
type TThunkStartActionParams = IThunkStartActionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Тип результата после подготовки выполнения Thunk-и
 */
type TThunkStartActionResult = IThunkStartActionResult<TActionStartPayload, TActionFailedPayload>;

/**
 * Тип параметров выполнения Thunk-и
 */
type TThunkStartExecutionParams = IThunkStartExecutionParams<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams>;

/**
 * Thunk для удаления терминального шлюза
 */
export class DeleteCloudGatewayTerminalThunk extends BaseReduxThunkObject<AppReduxStoreState, TActionStartPayload, TActionSuccessPayload, TActionFailedPayload, TInputParams> {
    /**
     * Запуск Thunk-и
     * @param args Параметры запуска Thunk-и
     */
    public static invoke(args?: TInputParams) {
        return new DeleteCloudGatewayTerminalThunk().execute(args, args?.showLoadingProgress);
    }

    protected get startActionValue(): string {
        return START_ACTION;
    }

    protected get successActionValue(): string {
        return SUCCESS_ACTION;
    }

    protected get failedActionValue(): string {
        return FAILED_ACTION;
    }

    protected getStartActionArguments(args: TThunkStartActionParams): TThunkStartActionResult {
        return {
            condition: args.inputParams?.force === true,
            getReducerStateFunc: () => args.getStore().TabTerminalGatewayState
        };
    }

    protected async startExecution(args: TThunkStartExecutionParams): Promise<void> {
        const api = InterlayerApiProxy.getSegmentsApiProxy().getTerminalGatewayTabApiProxyMethods();

        try {
            const response = await api.deleteCloudGatewayTerminal(RequestKind.SEND_BY_USER_SYNCHRONOUSLY, args.inputParams!.cloudGatewayTerminalId);
            if (response?.success === false) {
                throw new Error(response.message ?? templateErrorText);
            }
            args.success();
        } catch (er) {
            args.failed({
                error: er as ErrorObject
            });
        }
    }
}