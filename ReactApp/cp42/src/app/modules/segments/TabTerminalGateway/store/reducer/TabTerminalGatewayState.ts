import { TabTerminalGatewayProcessId } from 'app/modules/segments/TabTerminalGateway';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CommonSegmentDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Терм. шлюзы
 */
export type TabTerminalGatewayState = IReducerState<TabTerminalGatewayProcessId> & {
    /**
     * Массив терминального шлюза
     */
    cloudGatewayTerminals: SelectDataMetadataResponseDto<CommonSegmentDataModel>,

    /**
     * Модель терминального шлюза
     */
    cloudGatewayTerminal: CommonSegmentDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то терминальные шлюзы получены
         */
        hasCloudGatewayTerminalsReceived: boolean
    };
};