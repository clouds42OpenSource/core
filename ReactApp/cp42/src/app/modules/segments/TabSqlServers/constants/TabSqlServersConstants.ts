/**
 * Константы для модуля Segments -> Таб SQL
 */
export const TabSqlServersConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб SQL
     */
    reducerName: 'TAB_SQL_SERVERS'
};