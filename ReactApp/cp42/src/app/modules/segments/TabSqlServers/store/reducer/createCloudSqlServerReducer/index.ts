import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { TabSqlServersActions } from 'app/modules/segments/TabSqlServers/store/actions';
import { TabSqlServersState } from 'app/modules/segments/TabSqlServers/store/reducer';
import { CreateCloudSqlServerActionAnyPayload, CreateCloudSqlServerActionSuccessPayload } from 'app/modules/segments/TabSqlServers/store/reducer/createCloudSqlServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания sql сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createCloudSqlServerReducer: TPartialReducerObject<TabSqlServersState, CreateCloudSqlServerActionAnyPayload> =
    (state, action) => {
        const processId = TabSqlServersProcessId.CreateCloudSqlServer;
        const actions = TabSqlServersActions.CreateCloudSqlServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateCloudSqlServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };