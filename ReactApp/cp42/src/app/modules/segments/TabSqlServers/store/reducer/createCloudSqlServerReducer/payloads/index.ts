import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания sql сервера
 */
export type CreateCloudSqlServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания sql сервера
 */
export type CreateCloudSqlServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания sql сервера
 */
export type CreateCloudSqlServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании sql сервера
 */
export type CreateCloudSqlServerActionAnyPayload =
    | CreateCloudSqlServerActionStartPayload
    | CreateCloudSqlServerActionFailedPayload
    | CreateCloudSqlServerActionSuccessPayload;