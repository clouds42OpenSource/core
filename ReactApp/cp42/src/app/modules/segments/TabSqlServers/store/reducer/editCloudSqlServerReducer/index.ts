import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { TabSqlServersActions } from 'app/modules/segments/TabSqlServers/store/actions';
import { TabSqlServersState } from 'app/modules/segments/TabSqlServers/store/reducer';
import { EditCloudSqlServerActionAnyPayload, EditCloudSqlServerActionSuccessPayload } from 'app/modules/segments/TabSqlServers/store/reducer/editCloudSqlServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования sql сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editCloudSqlServerReducer: TPartialReducerObject<TabSqlServersState, EditCloudSqlServerActionAnyPayload> =
    (state, action) => {
        const processId = TabSqlServersProcessId.EditCloudSqlServer;
        const actions = TabSqlServersActions.EditCloudSqlServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditCloudSqlServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };