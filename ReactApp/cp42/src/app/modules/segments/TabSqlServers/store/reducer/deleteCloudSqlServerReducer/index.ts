import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { TabSqlServersActions } from 'app/modules/segments/TabSqlServers/store/actions';
import { TabSqlServersState } from 'app/modules/segments/TabSqlServers/store/reducer';
import { DeleteCloudSqlServerActionAnyPayload, DeleteCloudSqlServerActionSuccessPayload } from 'app/modules/segments/TabSqlServers/store/reducer/deleteCloudSqlServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления sql сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteCloudSqlServerReducer: TPartialReducerObject<TabSqlServersState, DeleteCloudSqlServerActionAnyPayload> =
    (state, action) => {
        const processId = TabSqlServersProcessId.DeleteCloudSqlServer;
        const actions = TabSqlServersActions.DeleteCloudSqlServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteCloudSqlServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };