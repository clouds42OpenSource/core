import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { TabSqlServersConstants } from 'app/modules/segments/TabSqlServers/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "SQL"
 */
export const TabSqlServersActions = {
    /**
     * Набор action на получение списка данных по sql серверам
     */
    GetCloudSqlServers: createReducerActions(TabSqlServersConstants.reducerName, TabSqlServersProcessId.GetCloudSqlServers),

    /**
     * Набор action на создание sql сервера
     */
    CreateCloudSqlServer: createReducerActions(TabSqlServersConstants.reducerName, TabSqlServersProcessId.CreateCloudSqlServer),

    /**
     * Набор action на редактирование sql сервера
     */
    EditCloudSqlServer: createReducerActions(TabSqlServersConstants.reducerName, TabSqlServersProcessId.EditCloudSqlServer),

    /**
     * Набор action на получение sql сервера по ID
     */
    GetCloudSqlServer: createReducerActions(TabSqlServersConstants.reducerName, TabSqlServersProcessId.GetCloudSqlServer),

    /**
     * Набор action на удаление sql сервера по ID
     */
    DeleteCloudSqlServer: createReducerActions(TabSqlServersConstants.reducerName, TabSqlServersProcessId.DeleteCloudSqlServer)
};