import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabSqlServersConstants } from 'app/modules/segments/TabSqlServers/constants';
import { TabSqlServersState } from 'app/modules/segments/TabSqlServers/store/reducer';
import { createCloudSqlServerReducer } from 'app/modules/segments/TabSqlServers/store/reducer/createCloudSqlServerReducer';
import { deleteCloudSqlServerReducer } from 'app/modules/segments/TabSqlServers/store/reducer/deleteCloudSqlServerReducer';
import { editCloudSqlServerReducer } from 'app/modules/segments/TabSqlServers/store/reducer/editCloudSqlServerReducer';
import { getCloudSqlServerReducer } from 'app/modules/segments/TabSqlServers/store/reducer/getCloudSqlServerReducer';
import { getCloudSqlServersReducer } from 'app/modules/segments/TabSqlServers/store/reducer/getCloudSqlServersReducer';
import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> SQL
 */
const initialState = initReducerState<TabSqlServersState>(
    TabSqlServersConstants.reducerName,
    {
        cloudSqlServers: {
            metadata: getInitialMetadata(),
            records: []
        },
        cloudSqlServer: partialFillOf<CloudSqlServerDataModel>(),
        hasSuccessFor: {
            hasCloudSqlServerReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> SQL
 */
const partialReducers = [
    getCloudSqlServersReducer,
    deleteCloudSqlServerReducer,
    getCloudSqlServerReducer,
    createCloudSqlServerReducer,
    editCloudSqlServerReducer
];

/**
 * Редюсер Segments -> SQL
 */
export const TabSqlServersReducer = createReducer(initialState, partialReducers);