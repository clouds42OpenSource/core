import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления sql сервера
 */
export type DeleteCloudSqlServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления sql сервера
 */
export type DeleteCloudSqlServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления sql сервера
 */
export type DeleteCloudSqlServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении sql сервера
 */
export type DeleteCloudSqlServerActionAnyPayload =
    | DeleteCloudSqlServerActionStartPayload
    | DeleteCloudSqlServerActionFailedPayload
    | DeleteCloudSqlServerActionSuccessPayload;