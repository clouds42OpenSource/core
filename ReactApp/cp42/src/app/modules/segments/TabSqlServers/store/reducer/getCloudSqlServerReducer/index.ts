import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { TabSqlServersActions } from 'app/modules/segments/TabSqlServers/store/actions';
import { TabSqlServersState } from 'app/modules/segments/TabSqlServers/store/reducer';
import { GetCloudSqlServerActionAnyPayload, GetCloudSqlServerActionSuccessPayload } from 'app/modules/segments/TabSqlServers/store/reducer/getCloudSqlServerReducer/payloads';
import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения sql сервера
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudSqlServerReducer: TPartialReducerObject<TabSqlServersState, GetCloudSqlServerActionAnyPayload> =
    (state, action) => {
        const processId = TabSqlServersProcessId.GetCloudSqlServer;
        const actions = TabSqlServersActions.GetCloudSqlServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    cloudSqlServer: partialFillOf<CloudSqlServerDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetCloudSqlServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudSqlServer: payload
                });
            }
        });
    };