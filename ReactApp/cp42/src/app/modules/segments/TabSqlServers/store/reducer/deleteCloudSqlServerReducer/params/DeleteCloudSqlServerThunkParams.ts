import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления sql сервера
 */
export type DeleteCloudSqlServerThunkParams = IForceThunkParam & {
    /**
     * ID sql сервера
     */
    sqlServerId: string
};