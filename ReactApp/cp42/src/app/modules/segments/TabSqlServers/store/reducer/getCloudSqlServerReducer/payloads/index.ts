import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения sql сервера
 */
export type GetCloudSqlServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения sql сервера
 */
export type GetCloudSqlServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения sql сервера
 */
export type GetCloudSqlServerActionSuccessPayload = ReducerActionSuccessPayload & CloudSqlServerDataModel;

/**
 * Все возможные Action при получении sql сервера
 */
export type GetCloudSqlServerActionAnyPayload =
    | GetCloudSqlServerActionStartPayload
    | GetCloudSqlServerActionFailedPayload
    | GetCloudSqlServerActionSuccessPayload;