import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по sql серверам
 */
export type GetCloudSqlServersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по sql серверам
 */
export type GetCloudSqlServersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по sql серверам
 */
export type GetCloudSqlServersActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<CloudSqlServerDataModel>;

/**
 * Все возможные Action при получении списка данных по sql серверам
 */
export type GetCloudSqlServersActionAnyPayload =
    | GetCloudSqlServersActionStartPayload
    | GetCloudSqlServersActionFailedPayload
    | GetCloudSqlServersActionSuccessPayload;