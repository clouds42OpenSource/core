import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования sql сервера
 */
export type EditCloudSqlServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования sql сервера
 */
export type EditCloudSqlServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования sql сервера
 */
export type EditCloudSqlServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании sql сервера
 */
export type EditCloudSqlServerActionAnyPayload =
    | EditCloudSqlServerActionStartPayload
    | EditCloudSqlServerActionFailedPayload
    | EditCloudSqlServerActionSuccessPayload;