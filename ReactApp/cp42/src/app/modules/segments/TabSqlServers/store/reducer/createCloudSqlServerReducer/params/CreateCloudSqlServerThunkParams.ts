import { CreateCloudSqlServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания sql сервера
 */
export type CreateCloudSqlServerThunkParams = IForceThunkParam & CreateCloudSqlServerParams;