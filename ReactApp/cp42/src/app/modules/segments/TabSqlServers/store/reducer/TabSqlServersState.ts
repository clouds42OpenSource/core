import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Файл хранилища
 */
export type TabSqlServersState = IReducerState<TabSqlServersProcessId> & {
    /**
     * Массив Sql сервера
     */
    cloudSqlServers: SelectDataMetadataResponseDto<CloudSqlServerDataModel>,

    /**
     * Модель Sql сервера
     */
    cloudSqlServer: CloudSqlServerDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то массив Sql сервера получены
         */
        hasCloudSqlServerReceived: boolean
    };
};