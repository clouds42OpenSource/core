import { CloudSqlServerFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения массива sql сервера
 */
export type GetCloudSqlServersThunkParams = IForceThunkParam & CloudSqlServerFilterParams;