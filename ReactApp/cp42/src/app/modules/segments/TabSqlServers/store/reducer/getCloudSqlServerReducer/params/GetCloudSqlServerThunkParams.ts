import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения sql сервера
 */
export type GetCloudSqlServerThunkParams = IForceThunkParam & {
    /**
     * ID sql сервера
     */
    sqlServerId: string
};