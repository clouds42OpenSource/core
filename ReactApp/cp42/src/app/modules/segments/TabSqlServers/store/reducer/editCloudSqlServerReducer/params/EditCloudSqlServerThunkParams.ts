import { CloudSqlServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabSqlServersApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования sql сервера
 */
export type EditCloudSqlServerThunkParams = IForceThunkParam & CloudSqlServerDataModel;