import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabSqlServersProcessId } from 'app/modules/segments/TabSqlServers';
import { TabSqlServersActions } from 'app/modules/segments/TabSqlServers/store/actions';
import { TabSqlServersState } from 'app/modules/segments/TabSqlServers/store/reducer';
import { GetCloudSqlServersActionAnyPayload, GetCloudSqlServersActionSuccessPayload } from 'app/modules/segments/TabSqlServers/store/reducer/getCloudSqlServersReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по sql серверам
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudSqlServersReducer: TPartialReducerObject<TabSqlServersState, GetCloudSqlServersActionAnyPayload> =
    (state, action) => {
        const processId = TabSqlServersProcessId.GetCloudSqlServers;
        const actions = TabSqlServersActions.GetCloudSqlServers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetCloudSqlServersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudSqlServers: payload,
                    hasSuccessFor: {
                        ...state,
                        hasCloudSqlServerReceived: true
                    }
                });
            }
        });
    };