/**
 * Процесы Segments -> "SQL"
 */
export enum TabSqlServersProcessId {
    /**
     * Процесс на получение списка данных по sql серверам
     */
    GetCloudSqlServers = 'GET_CLOUD_SQL_SERVERS',

    /**
     * Процесс на создание sql сервера
     */
    CreateCloudSqlServer = 'CREATE_CLOUD_SQL_SERVER',

    /**
     * Процесс на редактирование sql сервера
     */
    EditCloudSqlServer = 'EDIT_CLOUD_SQL_SERVER',

    /**
     * Процесс на получение sql сервера по ID
     */
    GetCloudSqlServer = 'GET_CLOUD_SQL_SERVER',

    /**
     * Процесс на удаление sql сервера по ID
     */
    DeleteCloudSqlServer = 'DELETE_CLOUD_SQL_SERVER'
}