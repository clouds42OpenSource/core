/**
 * Константы для модуля Segments -> Таб Файл хранилища
 */
export const TabFileStoragesConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб Файл хранилища
     */
    reducerName: 'TAB_FILE_STORAGES'
};