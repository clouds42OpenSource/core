/**
 * Процесы Segments -> "Файл хранилища"
 */
export enum TabFileStoragesProcessId {
    /**
     * Процесс на получение списка данных по файлам хранилища
     */
    GetCloudFileStorageServers = 'GET_CLOUD_FILE_STORAGE_SERVERS',

    /**
     * Процесс на создание файла хранилища
     */
    CreateCloudFileStorageServer = 'CREATE_CLOUD_FILE_STORAGE_SERVER',

    /**
     * Процесс на редактирование файла хранилища
     */
    EditCloudFileStorageServer = 'EDIT_CLOUD_FILE_STORAGE_SERVER',

    /**
     * Процесс на получение файла хранилища по ID
     */
    GetCloudFileStorageServer = 'GET_CLOUD_FILE_STORAGE_SERVER',

    /**
     * Процесс на удаление файла хранилища по ID
     */
    DeleteCloudFileStorageServer = 'DELETE_CLOUD_FILE_STORAGE_SERVER'
}