import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по файлам хранилища
 */
export type GetCloudFileStoragesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по файлам хранилища
 */
export type GetCloudFileStoragesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по файлам хранилища
 */
export type GetCloudFileStoragesActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<CloudFileStorageServerDataModel>;

/**
 * Все возможные Action при получении списка данных по файлам хранилища
 */
export type GetCloudFileStoragesActionAnyPayload =
    | GetCloudFileStoragesActionStartPayload
    | GetCloudFileStoragesActionFailedPayload
    | GetCloudFileStoragesActionSuccessPayload;