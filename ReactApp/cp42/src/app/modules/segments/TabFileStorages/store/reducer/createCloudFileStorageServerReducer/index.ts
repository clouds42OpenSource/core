import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { TabFileStoragesActions } from 'app/modules/segments/TabFileStorages/store/actions';
import { TabFileStoragesState } from 'app/modules/segments/TabFileStorages/store/reducer';
import { CreateCloudFileStorageActionAnyPayload, CreateCloudFileStorageActionSuccessPayload } from 'app/modules/segments/TabFileStorages/store/reducer/createCloudFileStorageServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания нового файлового хранилища
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createCloudFileStorageServerReducer: TPartialReducerObject<TabFileStoragesState, CreateCloudFileStorageActionAnyPayload> =
    (state, action) => {
        const processId = TabFileStoragesProcessId.CreateCloudFileStorageServer;
        const actions = TabFileStoragesActions.CreateCloudFileStorageServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateCloudFileStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };