import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания файлового хранилища
 */
export type CreateCloudFileStorageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания файлового хранилища
 */
export type CreateCloudFileStorageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания файлового хранилища
 */
export type CreateCloudFileStorageActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании файлового хранилища
 */
export type CreateCloudFileStorageActionAnyPayload =
    | CreateCloudFileStorageActionStartPayload
    | CreateCloudFileStorageActionFailedPayload
    | CreateCloudFileStorageActionSuccessPayload;