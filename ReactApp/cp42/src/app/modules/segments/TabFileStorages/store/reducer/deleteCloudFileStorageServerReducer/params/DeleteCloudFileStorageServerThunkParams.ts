import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления файлового хранилища
 */
export type DeleteCloudFileStorageServerThunkParams = IForceThunkParam & {
    /**
     * ID файлового хранилища
     */
    fileStorageId: string
};