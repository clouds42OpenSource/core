import { CloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования файлового хранилища
 */
export type EditCloudFileStorageServerThunkParams = IForceThunkParam & CloudFileStorageServerDataModel;