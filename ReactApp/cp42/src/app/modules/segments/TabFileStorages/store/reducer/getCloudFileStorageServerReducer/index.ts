import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { TabFileStoragesActions } from 'app/modules/segments/TabFileStorages/store/actions';
import { TabFileStoragesState } from 'app/modules/segments/TabFileStorages/store/reducer';
import { GetCloudFileStorageActionAnyPayload, GetCloudFileStorageActionSuccessPayload } from 'app/modules/segments/TabFileStorages/store/reducer/getCloudFileStorageServerReducer/payloads';
import { CloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения файла хранилища
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudFileStorageServerReducer: TPartialReducerObject<TabFileStoragesState, GetCloudFileStorageActionAnyPayload> =
    (state, action) => {
        const processId = TabFileStoragesProcessId.GetCloudFileStorageServer;
        const actions = TabFileStoragesActions.GetCloudFileStorageServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    cloudFileStorageServer: partialFillOf<CloudFileStorageServerDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetCloudFileStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudFileStorageServer: payload
                });
            }
        });
    };