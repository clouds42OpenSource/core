import { FileStorageFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка файлового хранилища
 */
export type GetCloudFileStorageServersThunkParams = IForceThunkParam & FileStorageFilterParams;