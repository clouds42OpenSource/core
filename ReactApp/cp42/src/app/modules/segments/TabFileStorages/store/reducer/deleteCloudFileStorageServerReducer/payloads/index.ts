import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления файлового хранилища
 */
export type DeleteCloudFileStorageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления файлового хранилища
 */
export type DeleteCloudFileStorageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления файлового хранилища
 */
export type DeleteCloudFileStorageActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении файлового хранилища
 */
export type DeleteCloudFileStorageActionAnyPayload =
    | DeleteCloudFileStorageActionStartPayload
    | DeleteCloudFileStorageActionFailedPayload
    | DeleteCloudFileStorageActionSuccessPayload;