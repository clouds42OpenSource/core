import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { TabFileStoragesActions } from 'app/modules/segments/TabFileStorages/store/actions';
import { TabFileStoragesState } from 'app/modules/segments/TabFileStorages/store/reducer';
import { GetCloudFileStoragesActionAnyPayload, GetCloudFileStoragesActionSuccessPayload } from 'app/modules/segments/TabFileStorages/store/reducer/getCloudFileStorageServersReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по файлам хранилища
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudFileStorageServersReducer: TPartialReducerObject<TabFileStoragesState, GetCloudFileStoragesActionAnyPayload> =
    (state, action) => {
        const processId = TabFileStoragesProcessId.GetCloudFileStorageServers;
        const actions = TabFileStoragesActions.GetCloudFileStorageServers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetCloudFileStoragesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    cloudFileStorageServers: payload,
                    hasSuccessFor: {
                        ...state,
                        hasCloudFileStorageServerReceived: true
                    }
                });
            }
        });
    };