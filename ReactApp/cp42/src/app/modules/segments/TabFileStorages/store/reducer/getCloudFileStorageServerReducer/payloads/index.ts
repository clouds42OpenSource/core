import { CloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения файла хранилища
 */
export type GetCloudFileStorageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения файла хранилища
 */
export type GetCloudFileStorageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения файла хранилища
 */
export type GetCloudFileStorageActionSuccessPayload = ReducerActionSuccessPayload & CloudFileStorageServerDataModel;

/**
 * Все возможные Action при получении файла хранилища
 */
export type GetCloudFileStorageActionAnyPayload =
    | GetCloudFileStorageActionStartPayload
    | GetCloudFileStorageActionFailedPayload
    | GetCloudFileStorageActionSuccessPayload;