import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabContentServersConstants } from 'app/modules/segments/TabContentServers/constants';
import { TabFileStoragesState } from 'app/modules/segments/TabFileStorages/store/reducer';
import { createCloudFileStorageServerReducer } from 'app/modules/segments/TabFileStorages/store/reducer/createCloudFileStorageServerReducer';
import { deleteCloudFileStorageServerReducer } from 'app/modules/segments/TabFileStorages/store/reducer/deleteCloudFileStorageServerReducer';
import { editCloudFileStorageServerReducer } from 'app/modules/segments/TabFileStorages/store/reducer/editCloudFileStorageServerReducer';
import { getCloudFileStorageServerReducer } from 'app/modules/segments/TabFileStorages/store/reducer/getCloudFileStorageServerReducer';
import { getCloudFileStorageServersReducer } from 'app/modules/segments/TabFileStorages/store/reducer/getCloudFileStorageServersReducer';
import { CloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Файл хранилища
 */
const initialState = initReducerState<TabFileStoragesState>(
    TabContentServersConstants.reducerName,
    {
        cloudFileStorageServers: {
            metadata: getInitialMetadata(),
            records: []
        },
        cloudFileStorageServer: partialFillOf<CloudFileStorageServerDataModel>(),
        hasSuccessFor: {
            hasCloudFileStorageServerReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Файл хранилища
 */
const partialReducers = [
    getCloudFileStorageServersReducer,
    deleteCloudFileStorageServerReducer,
    getCloudFileStorageServerReducer,
    createCloudFileStorageServerReducer,
    editCloudFileStorageServerReducer
];

/**
 * Редюсер Segments -> Файл хранилища
 */
export const TabFileStoragesReducer = createReducer(initialState, partialReducers);