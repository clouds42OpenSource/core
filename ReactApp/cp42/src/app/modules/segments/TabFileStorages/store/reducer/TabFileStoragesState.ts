import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { CloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Файл хранилища
 */
export type TabFileStoragesState = IReducerState<TabFileStoragesProcessId> & {
    /**
     * Массив файлового хранилища
     */
    cloudFileStorageServers: SelectDataMetadataResponseDto<CloudFileStorageServerDataModel>,

    /**
     * Модель файлового хранилища
     */
    cloudFileStorageServer: CloudFileStorageServerDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то файлы хранилища получены
         */
        hasCloudFileStorageServerReceived: boolean
    };
};