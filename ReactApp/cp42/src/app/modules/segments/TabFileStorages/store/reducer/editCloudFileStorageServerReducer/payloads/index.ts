import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования файлового хранилища
 */
export type EditCloudFileStorageActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования файлового хранилища
 */
export type EditCloudFileStorageActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования файлового хранилища
 */
export type EditCloudFileStorageActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании файлового хранилища
 */
export type EditCloudFileStorageActionAnyPayload =
    | EditCloudFileStorageActionStartPayload
    | EditCloudFileStorageActionFailedPayload
    | EditCloudFileStorageActionSuccessPayload;