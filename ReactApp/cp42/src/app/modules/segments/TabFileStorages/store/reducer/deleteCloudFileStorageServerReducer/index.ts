import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { TabFileStoragesActions } from 'app/modules/segments/TabFileStorages/store/actions';
import { TabFileStoragesState } from 'app/modules/segments/TabFileStorages/store/reducer';
import { DeleteCloudFileStorageActionAnyPayload, DeleteCloudFileStorageActionSuccessPayload } from 'app/modules/segments/TabFileStorages/store/reducer/deleteCloudFileStorageServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления файлового хранилища
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteCloudFileStorageServerReducer: TPartialReducerObject<TabFileStoragesState, DeleteCloudFileStorageActionAnyPayload> =
    (state, action) => {
        const processId = TabFileStoragesProcessId.DeleteCloudFileStorageServer;
        const actions = TabFileStoragesActions.DeleteCloudFileStorageServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteCloudFileStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };