import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения файлового хранилища
 */
export type GetCloudFileStorageServerThunkParams = IForceThunkParam & {
    /**
     * ID файлового хранилища
     */
    fileStorageId: string
};