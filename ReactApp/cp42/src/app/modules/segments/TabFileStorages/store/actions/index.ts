import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { TabFileStoragesConstants } from 'app/modules/segments/TabFileStorages/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Файл хранилища"
 */
export const TabFileStoragesActions = {
    /**
     * Набор action на получение списка данных по файлам хранилища
     */
    GetCloudFileStorageServers: createReducerActions(TabFileStoragesConstants.reducerName, TabFileStoragesProcessId.GetCloudFileStorageServers),

    /**
     * Набор action на создание файла хранилища
     */
    CreateCloudFileStorageServer: createReducerActions(TabFileStoragesConstants.reducerName, TabFileStoragesProcessId.CreateCloudFileStorageServer),

    /**
     * Набор action на редактирование файла хранилища
     */
    EditCloudFileStorageServer: createReducerActions(TabFileStoragesConstants.reducerName, TabFileStoragesProcessId.EditCloudFileStorageServer),

    /**
     * Набор action на получение файла хранилища по ID
     */
    GetCloudFileStorageServer: createReducerActions(TabFileStoragesConstants.reducerName, TabFileStoragesProcessId.GetCloudFileStorageServer),

    /**
     * Набор action на удаление файла хранилища по ID
     */
    DeleteCloudFileStorageServer: createReducerActions(TabFileStoragesConstants.reducerName, TabFileStoragesProcessId.DeleteCloudFileStorageServer)
};