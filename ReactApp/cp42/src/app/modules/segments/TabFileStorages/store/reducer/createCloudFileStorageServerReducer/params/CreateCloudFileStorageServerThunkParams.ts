import { CreateCloudFileStorageServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabFileStorageApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания нового файлового хранилища
 */
export type CreateCloudFileStorageServerThunkParams = IForceThunkParam & CreateCloudFileStorageServerDataModel;