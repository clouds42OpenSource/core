import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabFileStoragesProcessId } from 'app/modules/segments/TabFileStorages';
import { TabFileStoragesActions } from 'app/modules/segments/TabFileStorages/store/actions';
import { TabFileStoragesState } from 'app/modules/segments/TabFileStorages/store/reducer';
import { EditCloudFileStorageActionAnyPayload, EditCloudFileStorageActionSuccessPayload } from 'app/modules/segments/TabFileStorages/store/reducer/editCloudFileStorageServerReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования файлового хранилища
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editCloudFileStorageServerReducer: TPartialReducerObject<TabFileStoragesState, EditCloudFileStorageActionAnyPayload> =
    (state, action) => {
        const processId = TabFileStoragesProcessId.EditCloudFileStorageServer;
        const actions = TabFileStoragesActions.EditCloudFileStorageServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditCloudFileStorageActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };