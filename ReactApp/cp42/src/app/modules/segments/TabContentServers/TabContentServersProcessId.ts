/**
 * Процесы Segments -> "Сайты публикаций"
 */
export enum TabContentServersProcessId {
    /**
     * Процесс на получение списка данных по сайтам публикации
     */
    GetContentServers = 'GET_CONTENT_SERVERS',

    /**
     * Процесс на создание сайта публикации
     */
    CreateContentServer = 'CREATE_CONTENT_SERVER',

    /**
     * Процесс на редактирование сайта публикации
     */
    EditContentServer = 'EDIT_CONTENT_SERVER',

    /**
     * Процесс на получение сайта публикации по ID
     */
    GetContentServer = 'GET_CONTENT_SERVER',

    /**
     * Процесс на удаление сайта публикации по ID
     */
    DeleteContentServer = 'DELETE_CONTENT_SERVER',

    /**
     * Процесс на получение массива доступных нод публикации
     */
    GetAvailablePublishNodes = 'GET_AVAILABLE_PUBLISH_NODES',

    /**
     * Процесс на проверку доступности нода
     */
    CheckNodeAvailability = 'CHECK_NODE_AVAILABILITY'
}