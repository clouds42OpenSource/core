/**
 * Константы для модуля Segments -> Таб Сайты публикаций
 */
export const TabContentServersConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб Сайты публикаций
     */
    reducerName: 'TAB_CONTENT_SERVERS'
};