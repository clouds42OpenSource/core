import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { TabContentServersActions } from 'app/modules/segments/TabContentServers/store/actions';
import { GetContentServerActionAnyPayload, GetContentServerActionSuccessPayload } from 'app/modules/segments/TabContentServers/store/reducer/getCloudContentServerReducer/payloads';
import { TabContentServersState } from 'app/modules/segments/TabContentServers/store/reducer/TabContentServersState';
import { CloudContentServerResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения данных по сайту публикации по ID
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudContentServerReducer: TPartialReducerObject<TabContentServersState, GetContentServerActionAnyPayload> =
    (state, action) => {
        const processId = TabContentServersProcessId.GetContentServer;
        const actions = TabContentServersActions.GetContentServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    contentServer: partialFillOf<CloudContentServerResponseDto>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetContentServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    contentServer: payload
                });
            }
        });
    };