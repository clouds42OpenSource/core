import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { TabContentServersActions } from 'app/modules/segments/TabContentServers/store/actions';
import { GetContentServersActionAnyPayload, GetContentServersActionSuccessPayload } from 'app/modules/segments/TabContentServers/store/reducer/getCloudContentServersReducer/payloads';
import { TabContentServersState } from 'app/modules/segments/TabContentServers/store/reducer/TabContentServersState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по сайтам публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getCloudContentServersReducer: TPartialReducerObject<TabContentServersState, GetContentServersActionAnyPayload> =
    (state, action) => {
        const processId = TabContentServersProcessId.GetContentServers;
        const actions = TabContentServersActions.GetContentServers;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetContentServersActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    contentServers: payload,
                    hasSuccessFor: {
                        ...state,
                        hasContentServersReceived: true
                    }
                });
            }
        });
    };