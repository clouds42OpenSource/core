import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabContentServersConstants } from 'app/modules/segments/TabContentServers/constants';
import { checkNodeAvailabilityReducer } from 'app/modules/segments/TabContentServers/store/reducer/checkNodeAvailabilityReducer';
import { createCloudContentServerReducer } from 'app/modules/segments/TabContentServers/store/reducer/createCloudContentServerReducer';
import { deleteCloudContentServerReducer } from 'app/modules/segments/TabContentServers/store/reducer/deleteCloudContentServerReducer';
import { editCloudContentServerReducer } from 'app/modules/segments/TabContentServers/store/reducer/editCloudContentServerReducer';
import { getAvailablePublishNodesReducer } from 'app/modules/segments/TabContentServers/store/reducer/getAvailablePublishNodesReducer';
import { getCloudContentServerReducer } from 'app/modules/segments/TabContentServers/store/reducer/getCloudContentServerReducer';
import { getCloudContentServersReducer } from 'app/modules/segments/TabContentServers/store/reducer/getCloudContentServersReducer';
import { TabContentServersState } from 'app/modules/segments/TabContentServers/store/reducer/TabContentServersState';
import { CloudContentServerResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Сайты публикации
 */
const initialState = initReducerState<TabContentServersState>(
    TabContentServersConstants.reducerName,
    {
        contentServers: {
            metadata: getInitialMetadata(),
            records: []
        },
        contentServer: partialFillOf<CloudContentServerResponseDto>(),
        availablePublishNodes: [],
        isNodeAvailable: false,
        hasSuccessFor: {
            hasContentServersReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Сайты публикации
 */
const partialReducers = [
    getCloudContentServersReducer,
    deleteCloudContentServerReducer,
    getAvailablePublishNodesReducer,
    getCloudContentServerReducer,
    createCloudContentServerReducer,
    editCloudContentServerReducer,
    checkNodeAvailabilityReducer
];

/**
 * Редюсер Segments -> Сайты публикации
 */
export const TabContentServersReducer = createReducer(initialState, partialReducers);