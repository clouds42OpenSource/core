import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления сервера публикации
 */
export type DeleteContentServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления сервера публикации
 */
export type DeleteContentServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления сервера публикации
 */
export type DeleteContentServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении сервера публикации
 */
export type DeleteContentServerActionAnyPayload =
    | DeleteContentServerActionStartPayload
    | DeleteContentServerActionFailedPayload
    | DeleteContentServerActionSuccessPayload;