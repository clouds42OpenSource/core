import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования сервера публикации
 */
export type EditContentServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования сервера публикации
 */
export type EditContentServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования сервера публикации
 */
export type EditContentServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании сервера публикации
 */
export type EditContentServerActionAnyPayload =
    | EditContentServerActionStartPayload
    | EditContentServerActionFailedPayload
    | EditContentServerActionSuccessPayload;