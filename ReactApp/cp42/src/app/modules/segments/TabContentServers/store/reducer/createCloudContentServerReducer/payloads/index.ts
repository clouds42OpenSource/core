import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания сервера публикации
 */
export type CreateContentServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания сервера публикации
 */
export type CreateContentServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания сервера публикации
 */
export type CreateContentServerActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании сервера публикации
 */
export type CreateContentServerActionAnyPayload =
    | CreateContentServerActionStartPayload
    | CreateContentServerActionFailedPayload
    | CreateContentServerActionSuccessPayload;