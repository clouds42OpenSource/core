import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения массива доступных нод публикации
 */
export type GetAvailablePublishNodesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения массива доступных нод публикации
 */
export type GetAvailablePublishNodesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения массива доступных нод публикации
 */
export type GetAvailablePublishNodesActionSuccessPayload = ReducerActionSuccessPayload & PublishNodeDataModel[];

/**
 * Все возможные Action при получении массива доступных нод публикации
 */
export type GetAvailablePublishNodesActionAnyPayload =
    | GetAvailablePublishNodesActionStartPayload
    | GetAvailablePublishNodesActionFailedPayload
    | GetAvailablePublishNodesActionSuccessPayload;