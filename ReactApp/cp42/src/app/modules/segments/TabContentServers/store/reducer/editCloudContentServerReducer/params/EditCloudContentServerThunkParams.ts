import { CloudContentServerDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabContentServersApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования сервера публикации
 */
export type EditCloudContentServerThunkParams = IForceThunkParam & CloudContentServerDataModel;