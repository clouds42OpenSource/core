import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { TabContentServersActions } from 'app/modules/segments/TabContentServers/store/actions';
import { GetAvailablePublishNodesActionAnyPayload, GetAvailablePublishNodesActionSuccessPayload } from 'app/modules/segments/TabContentServers/store/reducer/getAvailablePublishNodesReducer/payloads';
import { TabContentServersState } from 'app/modules/segments/TabContentServers/store/reducer/TabContentServersState';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения массива доступных нод публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getAvailablePublishNodesReducer: TPartialReducerObject<TabContentServersState, GetAvailablePublishNodesActionAnyPayload> =
    (state, action) => {
        const processId = TabContentServersProcessId.GetAvailablePublishNodes;
        const actions = TabContentServersActions.GetAvailablePublishNodes;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    availablePublishNodes: []
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetAvailablePublishNodesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    availablePublishNodes: payload
                });
            }
        });
    };