import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения проверки доступности ноды
 */
export type CheckNodeAvailabilityActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении проверки доступности ноды
 */
export type CheckNodeAvailabilityActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении проверки доступности ноды
 */
export type CheckNodeAvailabilityActionSuccessPayload = ReducerActionSuccessPayload & boolean;

/**
 * Все возможные Action при проверки доступности ноды
 */
export type CheckNodeAvailabilityActionAnyPayload =
    | CheckNodeAvailabilityActionStartPayload
    | CheckNodeAvailabilityActionFailedPayload
    | CheckNodeAvailabilityActionSuccessPayload;