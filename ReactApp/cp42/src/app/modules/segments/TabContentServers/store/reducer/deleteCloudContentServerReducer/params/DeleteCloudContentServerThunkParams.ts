import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления сервера публикации
 */
export type DeleteCloudContentServerThunkParams = IForceThunkParam & {
    /**
     * ID сервера публикации
     */
    contentServerId: string;
};