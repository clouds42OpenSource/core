import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения сервера публикации по ID
 */
export type GetCloudContentServerThunkParams = IForceThunkParam & {
    /**
     * ID сервера публикации
     */
    contentServerId: string;
};