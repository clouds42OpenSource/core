import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { CloudContentServerResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Сайты публикации
 */
export type TabContentServersState = IReducerState<TabContentServersProcessId> & {
    /**
     * Массив сервера публикации
     */
    contentServers: SelectDataMetadataResponseDto<CloudContentServerResponseDto>,

    /**
     * Модель сервера публикации
     */
    contentServer: CloudContentServerResponseDto,

    /**
     * Массив нода публикаций
     */
    availablePublishNodes: PublishNodeDataModel[],

    /**
     * Флаг на доступность ноды
     */
    isNodeAvailable: boolean;

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то сервера публикации получены
         */
        hasContentServersReceived: boolean
    };
};