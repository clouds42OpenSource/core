import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { TabContentServersActions } from 'app/modules/segments/TabContentServers/store/actions';
import { CheckNodeAvailabilityActionAnyPayload, CheckNodeAvailabilityActionSuccessPayload } from 'app/modules/segments/TabContentServers/store/reducer/checkNodeAvailabilityReducer/payloads';
import { TabContentServersState } from 'app/modules/segments/TabContentServers/store/reducer/TabContentServersState';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для проверки доступности ноды
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const checkNodeAvailabilityReducer: TPartialReducerObject<TabContentServersState, CheckNodeAvailabilityActionAnyPayload> =
    (state, action) => {
        const processId = TabContentServersProcessId.CheckNodeAvailability;
        const actions = TabContentServersActions.CheckNodeAvailability;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    isNodeAvailable: false
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as CheckNodeAvailabilityActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    isNodeAvailable: payload
                });
            }
        });
    };