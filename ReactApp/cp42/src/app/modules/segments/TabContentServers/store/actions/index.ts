import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { TabContentServersConstants } from 'app/modules/segments/TabContentServers/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Сайты публикаций"
 */
export const TabContentServersActions = {
    /**
     * Набор action на получение списка данных по сайтам публикации
     */
    GetContentServers: createReducerActions(TabContentServersConstants.reducerName, TabContentServersProcessId.GetContentServers),

    /**
     * Набор action на создание сайта публикации
     */
    CreateContentServer: createReducerActions(TabContentServersConstants.reducerName, TabContentServersProcessId.CreateContentServer),

    /**
     * Набор action на редактирование сайта публикации
     */
    EditContentServer: createReducerActions(TabContentServersConstants.reducerName, TabContentServersProcessId.EditContentServer),

    /**
     * Набор action на получение сайта публикации по ID
     */
    GetContentServer: createReducerActions(TabContentServersConstants.reducerName, TabContentServersProcessId.GetContentServer),

    /**
     * Набор action на удаление сайта публикации по ID
     */
    DeleteContentServer: createReducerActions(TabContentServersConstants.reducerName, TabContentServersProcessId.DeleteContentServer),

    /**
     * Набор action на получение массива доступных нод публикации
     */
    GetAvailablePublishNodes: createReducerActions(TabContentServersConstants.reducerName, TabContentServersProcessId.GetAvailablePublishNodes),

    /**
     * Набор action на проверку доступности нода
     */
    CheckNodeAvailability: createReducerActions(TabContentServersConstants.reducerName, TabContentServersProcessId.CheckNodeAvailability)
};