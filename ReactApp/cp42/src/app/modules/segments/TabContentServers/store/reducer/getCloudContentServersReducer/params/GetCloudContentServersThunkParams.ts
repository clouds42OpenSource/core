import { CloudContentServerFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabContentServersApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения списка данных сайтов публикаций
 */
export type GetCloudContentServersThunkParams = IForceThunkParam & CloudContentServerFilterParams;