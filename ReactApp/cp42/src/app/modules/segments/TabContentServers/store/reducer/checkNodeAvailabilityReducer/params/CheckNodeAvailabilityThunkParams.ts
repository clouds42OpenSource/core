import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для проверки доступности нода публикации
 */
export type CheckNodeAvailabilityThunkParams = IForceThunkParam & {
    /**
     * ID нода публикации
     */
    publishNodeId: string;
};