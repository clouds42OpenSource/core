import { CloudContentServerResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { SelectDataMetadataResponseDto } from 'app/web/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по серверам публикации
 */
export type GetContentServersActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по серверам публикации
 */
export type GetContentServersActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по серверам публикации
 */
export type GetContentServersActionSuccessPayload = ReducerActionSuccessPayload & SelectDataMetadataResponseDto<CloudContentServerResponseDto>;

/**
 * Все возможные Action при получении списка данных по серверам публикации
 */
export type GetContentServersActionAnyPayload =
    | GetContentServersActionStartPayload
    | GetContentServersActionFailedPayload
    | GetContentServersActionSuccessPayload;