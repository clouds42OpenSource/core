import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { TabContentServersActions } from 'app/modules/segments/TabContentServers/store/actions';
import { DeleteContentServerActionAnyPayload, DeleteContentServerActionSuccessPayload } from 'app/modules/segments/TabContentServers/store/reducer/deleteCloudContentServerReducer/payloads';
import { TabContentServersState } from 'app/modules/segments/TabContentServers/store/reducer/TabContentServersState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления сервера публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deleteCloudContentServerReducer: TPartialReducerObject<TabContentServersState, DeleteContentServerActionAnyPayload> =
    (state, action) => {
        const processId = TabContentServersProcessId.DeleteContentServer;
        const actions = TabContentServersActions.DeleteContentServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeleteContentServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };