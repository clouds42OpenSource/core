import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { TabContentServersActions } from 'app/modules/segments/TabContentServers/store/actions';
import { EditContentServerActionAnyPayload, EditContentServerActionSuccessPayload } from 'app/modules/segments/TabContentServers/store/reducer/editCloudContentServerReducer/payloads';
import { TabContentServersState } from 'app/modules/segments/TabContentServers/store/reducer/TabContentServersState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования сервера публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editCloudContentServerReducer: TPartialReducerObject<TabContentServersState, EditContentServerActionAnyPayload> =
    (state, action) => {
        const processId = TabContentServersProcessId.EditContentServer;
        const actions = TabContentServersActions.EditContentServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditContentServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };