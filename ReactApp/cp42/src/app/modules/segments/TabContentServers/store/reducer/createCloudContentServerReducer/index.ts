import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabContentServersProcessId } from 'app/modules/segments/TabContentServers';
import { TabContentServersActions } from 'app/modules/segments/TabContentServers/store/actions';
import { CreateContentServerActionAnyPayload, CreateContentServerActionSuccessPayload } from 'app/modules/segments/TabContentServers/store/reducer/createCloudContentServerReducer/payloads';
import { TabContentServersState } from 'app/modules/segments/TabContentServers/store/reducer/TabContentServersState';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания сервера публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createCloudContentServerReducer: TPartialReducerObject<TabContentServersState, CreateContentServerActionAnyPayload> =
    (state, action) => {
        const processId = TabContentServersProcessId.CreateContentServer;
        const actions = TabContentServersActions.CreateContentServer;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreateContentServerActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };