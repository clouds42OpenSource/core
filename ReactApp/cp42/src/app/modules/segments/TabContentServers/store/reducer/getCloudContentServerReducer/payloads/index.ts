import { CloudContentServerResponseDto } from 'app/web/api/SegmentsProxy/TabContentServers/response-dto';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения данных по серверу публикации по ID
 */
export type GetContentServerActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения данных по серверу публикации по ID
 */
export type GetContentServerActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения данных по серверу публикации по ID
 */
export type GetContentServerActionSuccessPayload = ReducerActionSuccessPayload & CloudContentServerResponseDto;

/**
 * Все возможные Action при получении данных по серверу публикации по ID
 */
export type GetContentServerActionAnyPayload =
    | GetContentServerActionStartPayload
    | GetContentServerActionFailedPayload
    | GetContentServerActionSuccessPayload;