import { CreateCloudContentServerParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabContentServersApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk парамтры для создания сервера публикации
 */
export type CreateCloudContentServerThunkParams = IForceThunkParam & CreateCloudContentServerParams;