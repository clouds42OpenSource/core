/**
 * Процесы Segments -> "Ноды публикаций"
 */
export enum TabPublishNodesProcessId {
    /**
     * Процесс на получение списка данных по нодам публикации
     */
    GetPublishNodes = 'GET_PUBLISH_NODES',

    /**
     * Процесс на создание нода публикации
     */
    CreatePublishNode = 'CREATE_PUBLISH_NODE',

    /**
     * Процесс на редактирование нода публикации
     */
    EditPublishNode = 'EDIT_PUBLISH_NODE',

    /**
     * Процесс на получение нода публикации по ID
     */
    GetPublishNode = 'GET_PUBLISH_NODE',

    /**
     * Процесс на удаление нода публикации по ID
     */
    DeletePublishNode = 'DELETE_PUBLISH_NODE'
}