import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления нода публикации
 */
export type DeletePublishNodeActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления нода публикации
 */
export type DeletePublishNodeActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления нода публикации
 */
export type DeletePublishNodeActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении нода публикации
 */
export type DeletePublishNodeActionAnyPayload =
    | DeletePublishNodeActionStartPayload
    | DeletePublishNodeActionFailedPayload
    | DeletePublishNodeActionSuccessPayload;