import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabContentServersConstants } from 'app/modules/segments/TabContentServers/constants';
import { TabPublishNodesState } from 'app/modules/segments/TabPublishNodes/store/reducer';
import { createPublishNodeReducer } from 'app/modules/segments/TabPublishNodes/store/reducer/createPublishNodeReducer';
import { deletePublishNodeReducer } from 'app/modules/segments/TabPublishNodes/store/reducer/deletePublishNodeReducer';
import { editPublishNodeReducer } from 'app/modules/segments/TabPublishNodes/store/reducer/editPublishNodeReducer';
import { getPublishNodeReducer } from 'app/modules/segments/TabPublishNodes/store/reducer/getPublishNodeReducer';
import { getPublishNodesReducer } from 'app/modules/segments/TabPublishNodes/store/reducer/getPublishNodesReducer';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Ноды публикации
 */
const initialState = initReducerState<TabPublishNodesState>(
    TabContentServersConstants.reducerName,
    {
        publishNodes: {
            metadata: getInitialMetadata(),
            records: []
        },
        publishNode: partialFillOf<PublishNodeDataModel>(),
        hasSuccessFor: {
            hasPublishNodesReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Ноды публикации
 */
const partialReducers = [
    getPublishNodesReducer,
    deletePublishNodeReducer,
    getPublishNodeReducer,
    createPublishNodeReducer,
    editPublishNodeReducer
];

/**
 * Редюсер Segments -> Ноды публикации
 */
export const TabPublishNodesReducer = createReducer(initialState, partialReducers);