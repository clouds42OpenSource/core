import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { TabPublishNodesActions } from 'app/modules/segments/TabPublishNodes/store/actions';
import { TabPublishNodesState } from 'app/modules/segments/TabPublishNodes/store/reducer';
import { CreatePublishNodeActionAnyPayload, CreatePublishNodeActionSuccessPayload } from 'app/modules/segments/TabPublishNodes/store/reducer/createPublishNodeReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для создания нода публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const createPublishNodeReducer: TPartialReducerObject<TabPublishNodesState, CreatePublishNodeActionAnyPayload> =
    (state, action) => {
        const processId = TabPublishNodesProcessId.CreatePublishNode;
        const actions = TabPublishNodesActions.CreatePublishNode;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as CreatePublishNodeActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };