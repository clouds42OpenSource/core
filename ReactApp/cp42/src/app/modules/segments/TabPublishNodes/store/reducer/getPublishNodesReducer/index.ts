import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { TabPublishNodesActions } from 'app/modules/segments/TabPublishNodes/store/actions';
import { TabPublishNodesState } from 'app/modules/segments/TabPublishNodes/store/reducer';
import { GetPublishNodesActionAnyPayload, GetPublishNodesActionSuccessPayload } from 'app/modules/segments/TabPublishNodes/store/reducer/getPublishNodesReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по нодам публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getPublishNodesReducer: TPartialReducerObject<TabPublishNodesState, GetPublishNodesActionAnyPayload> =
    (state, action) => {
        const processId = TabPublishNodesProcessId.GetPublishNodes;
        const actions = TabPublishNodesActions.GetPublishNodes;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetPublishNodesActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    publishNodes: payload,
                    hasSuccessFor: {
                        ...state,
                        hasPublishNodesReceived: true
                    }
                });
            }
        });
    };