import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения списка данных по нодам публикации
 */
export type GetPublishNodesActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения списка данных по нодам публикации
 */
export type GetPublishNodesActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения списка данных по нодам публикации
 */
export type GetPublishNodesActionSuccessPayload = ReducerActionSuccessPayload & SelectDataResultMetadataModel<PublishNodeDataModel>;

/**
 * Все возможные Action при получении списка данных по нодам публикации
 */
export type GetPublishNodesActionAnyPayload =
    | GetPublishNodesActionStartPayload
    | GetPublishNodesActionFailedPayload
    | GetPublishNodesActionSuccessPayload;