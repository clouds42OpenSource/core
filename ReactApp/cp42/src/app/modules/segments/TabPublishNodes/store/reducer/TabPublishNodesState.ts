import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { SelectDataResultMetadataModel } from 'app/web/common/data-models';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { IReducerState } from 'core/redux/interfaces';

/**
 * Состояние редюсера для работы с процесами Segments -> Ноды публикации
 */
export type TabPublishNodesState = IReducerState<TabPublishNodesProcessId> & {
    /**
     * Массив нода публикации
     */
    publishNodes: SelectDataResultMetadataModel<PublishNodeDataModel>,

    /**
     * Модель нода публикации
     */
    publishNode: PublishNodeDataModel,

    /**
     * содержит ярлыки для фиксации загрузок данных, была ли загрузка или ещё нет
     */
    hasSuccessFor: {
        /**
         * Если true, то ноды публикации получены
         */
        hasPublishNodesReceived: boolean
    };
};