import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { TabPublishNodesConstants } from 'app/modules/segments/TabPublishNodes/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Ноды публикаций"
 */
export const TabPublishNodesActions = {
    /**
     * Набор action на получение списка данных по нодам публикации
     */
    GetPublishNodes: createReducerActions(TabPublishNodesConstants.reducerName, TabPublishNodesProcessId.GetPublishNodes),

    /**
     * Набор action на создание нода публикации
     */
    CreatePublishNode: createReducerActions(TabPublishNodesConstants.reducerName, TabPublishNodesProcessId.CreatePublishNode),

    /**
     * Набор action на редактирование нода публикации
     */
    EditPublishNode: createReducerActions(TabPublishNodesConstants.reducerName, TabPublishNodesProcessId.EditPublishNode),

    /**
     * Набор action на получение нода публикации по ID
     */
    GetPublishNode: createReducerActions(TabPublishNodesConstants.reducerName, TabPublishNodesProcessId.GetPublishNode),

    /**
     * Набор action на удаление нода публикации по ID
     */
    DeletePublishNode: createReducerActions(TabPublishNodesConstants.reducerName, TabPublishNodesProcessId.DeletePublishNode)
};