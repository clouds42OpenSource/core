import { PublishNodeFilterParams } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPublishNodesApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры на получения списка нодов публикаций
 */
export type GetPublishNodesThunkParams = IForceThunkParam & PublishNodeFilterParams;