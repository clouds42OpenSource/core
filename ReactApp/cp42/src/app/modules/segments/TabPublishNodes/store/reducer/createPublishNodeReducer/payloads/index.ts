import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания нода публикации
 */
export type CreatePublishNodeActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания нода публикации
 */
export type CreatePublishNodeActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания нода публикации
 */
export type CreatePublishNodeActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании нода публикации
 */
export type CreatePublishNodeActionAnyPayload =
    | CreatePublishNodeActionStartPayload
    | CreatePublishNodeActionFailedPayload
    | CreatePublishNodeActionSuccessPayload;