import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для получения нода публикации по ID
 */
export type GetPublishNodeThunkParams = IForceThunkParam & {
    /**
     * ID нода публикации
     */
    publishNodeId: string;
};