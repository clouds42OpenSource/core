import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для удаления нода публикации
 */
export type DeletePublishNodeThunkParams = IForceThunkParam & {
    /**
     * ID нода публикации
     */
    publishNodeId: string;
};