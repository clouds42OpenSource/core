import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { TabPublishNodesActions } from 'app/modules/segments/TabPublishNodes/store/actions';
import { TabPublishNodesState } from 'app/modules/segments/TabPublishNodes/store/reducer';
import { EditPublishNodeActionAnyPayload, EditPublishNodeActionSuccessPayload } from 'app/modules/segments/TabPublishNodes/store/reducer/editPublishNodeReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования нода публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editPublishNodeReducer: TPartialReducerObject<TabPublishNodesState, EditPublishNodeActionAnyPayload> =
    (state, action) => {
        const processId = TabPublishNodesProcessId.EditPublishNode;
        const actions = TabPublishNodesActions.EditPublishNode;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditPublishNodeActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };