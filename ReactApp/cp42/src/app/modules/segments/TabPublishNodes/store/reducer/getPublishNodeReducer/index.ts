import { partialFillOf } from 'app/common/functions';
import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { TabPublishNodesActions } from 'app/modules/segments/TabPublishNodes/store/actions';
import { TabPublishNodesState } from 'app/modules/segments/TabPublishNodes/store/reducer';
import { GetPublishNodeActionAnyPayload, GetPublishNodeActionSuccessPayload } from 'app/modules/segments/TabPublishNodes/store/reducer/getPublishNodeReducer/payloads';
import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { reducerStateProcessStart, reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения нода публикации по ID
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getPublishNodeReducer: TPartialReducerObject<TabPublishNodesState, GetPublishNodeActionAnyPayload> =
    (state, action) => {
        const processId = TabPublishNodesProcessId.GetPublishNode;
        const actions = TabPublishNodesActions.GetPublishNode;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onStartProcessState: () => {
                return reducerStateProcessStart(state, processId, {
                    publishNode: partialFillOf<PublishNodeDataModel>()
                });
            },
            onSuccessProcessState: () => {
                const payload = action.payload as GetPublishNodeActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    publishNode: payload
                });
            }
        });
    };