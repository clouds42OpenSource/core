import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPublishNodesProcessId } from 'app/modules/segments/TabPublishNodes';
import { TabPublishNodesActions } from 'app/modules/segments/TabPublishNodes/store/actions';
import { TabPublishNodesState } from 'app/modules/segments/TabPublishNodes/store/reducer';
import { DeletePublishNodeActionAnyPayload, DeletePublishNodeActionSuccessPayload } from 'app/modules/segments/TabPublishNodes/store/reducer/deletePublishNodeReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для удаления нода публикации
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const deletePublishNodeReducer: TPartialReducerObject<TabPublishNodesState, DeletePublishNodeActionAnyPayload> =
    (state, action) => {
        const processId = TabPublishNodesProcessId.DeletePublishNode;
        const actions = TabPublishNodesActions.DeletePublishNode;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as DeletePublishNodeActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };