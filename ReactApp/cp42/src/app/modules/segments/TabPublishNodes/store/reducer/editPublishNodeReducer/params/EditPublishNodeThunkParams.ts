import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для редактирования нода публикации
 */
export type EditPublishNodeThunkParams = IForceThunkParam & PublishNodeDataModel;