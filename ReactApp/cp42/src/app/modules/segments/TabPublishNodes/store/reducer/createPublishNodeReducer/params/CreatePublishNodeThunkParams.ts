import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания нода публикации
 */
export type CreatePublishNodeThunkParams = IForceThunkParam & PublishNodeDataModel;