import { PublishNodeDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/common';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения нода публикации
 */
export type GetPublishNodeActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения нода публикации
 */
export type GetPublishNodeActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения нода публикации
 */
export type GetPublishNodeActionSuccessPayload = ReducerActionSuccessPayload & PublishNodeDataModel;

/**
 * Все возможные Action при получении нода публикации
 */
export type GetPublishNodeActionAnyPayload =
    | GetPublishNodeActionStartPayload
    | GetPublishNodeActionFailedPayload
    | GetPublishNodeActionSuccessPayload;