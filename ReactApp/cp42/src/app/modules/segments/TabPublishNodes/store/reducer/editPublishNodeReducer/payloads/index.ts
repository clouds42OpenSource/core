import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования нода публикации
 */
export type EditPublishNodeActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования нода публикации
 */
export type EditPublishNodeActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования нода публикации
 */
export type EditPublishNodeActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании нода публикации
 */
export type EditPublishNodeActionAnyPayload =
    | EditPublishNodeActionStartPayload
    | EditPublishNodeActionFailedPayload
    | EditPublishNodeActionSuccessPayload;