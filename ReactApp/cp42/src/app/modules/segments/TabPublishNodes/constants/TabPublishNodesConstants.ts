/**
 * Константы для модуля Segments -> Таб Ноды публикаций
 */
export const TabPublishNodesConstants = {
    /**
     * Название редюсера для модуля Segments -> Таб Ноды публикаций
     */
    reducerName: 'TAB_PUBLISH_NODES'
};