import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения удаления версии платформы 1С
 */
export type DeletePlatformVersionActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении удаления версии платформы 1С
 */
export type DeletePlatformVersionActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении удаления версии платформы 1С
 */
export type DeletePlatformVersionActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при удалении версии платформы 1С
 */
export type DeletePlatformVersionActionAnyPayload =
    | DeletePlatformVersionActionStartPayload
    | DeletePlatformVersionActionFailedPayload
    | DeletePlatformVersionActionSuccessPayload;