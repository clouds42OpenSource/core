import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { TabPlatformVersionConstants } from 'app/modules/segments/TabPlatformVersion/constants';
import { createReducerActions } from 'core/redux/functions';

/**
 * Все доступные actions для редюсера Segments -> "Платформа 1С"
 */
export const TabPlatformVersionActions = {
    /**
     * Набор action на получение списка данных по версиям платформы 1С
     */
    GetPlatformVersions: createReducerActions(TabPlatformVersionConstants.reducerName, TabPlatformVersionProcessId.GetPlatformVersions),

    /**
     * Набор action на создание версии платформы 1С
     */
    CreatePlatformVersion: createReducerActions(TabPlatformVersionConstants.reducerName, TabPlatformVersionProcessId.CreatePlatformVersion),

    /**
     * Набор action на редактирование версии платформы 1С
     */
    EditPlatformVersion: createReducerActions(TabPlatformVersionConstants.reducerName, TabPlatformVersionProcessId.EditPlatformVersion),

    /**
     * Набор action на получение версии платформы 1С по ID
     */
    GetPlatformVersion: createReducerActions(TabPlatformVersionConstants.reducerName, TabPlatformVersionProcessId.GetPlatformVersion),

    /**
     * Набор action на удаление версии платформы 1С по ID
     */
    DeletePlatformVersion: createReducerActions(TabPlatformVersionConstants.reducerName, TabPlatformVersionProcessId.DeletePlatformVersion)
};