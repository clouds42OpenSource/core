import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения редактирования версии платформы 1С
 */
export type EditPlatformVersionActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении редактирования версии платформы 1С
 */
export type EditPlatformVersionActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении редактирования версии платформы 1С
 */
export type EditPlatformVersionActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при редактировании версии платформы 1С
 */
export type EditPlatformVersionActionAnyPayload =
    | EditPlatformVersionActionStartPayload
    | EditPlatformVersionActionFailedPayload
    | EditPlatformVersionActionSuccessPayload;