import { getInitialMetadata, partialFillOf } from 'app/common/functions';
import { TabPlatformVersionConstants } from 'app/modules/segments/TabPlatformVersion/constants';
import { TabPlatformVersionState } from 'app/modules/segments/TabPlatformVersion/store/reducer';
import { createPlatformVersionReducer } from 'app/modules/segments/TabPlatformVersion/store/reducer/createPlatformVersionReducer';
import { deletePlatformVersionReducer } from 'app/modules/segments/TabPlatformVersion/store/reducer/deletePlatformVersionReducer';
import { editPlatformVersionReducer } from 'app/modules/segments/TabPlatformVersion/store/reducer/editPlatformVersionReducer';
import { getPlatformVersionReducer } from 'app/modules/segments/TabPlatformVersion/store/reducer/getPlatformVersionReducer';
import { getPlatformVersionsReducer } from 'app/modules/segments/TabPlatformVersion/store/reducer/getPlatformVersionsReducer';
import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { createReducer, initReducerState } from 'core/redux/functions';

/**
 * Начальное состояние редюсера Segments -> Платформа 1С
 */
const initialState = initReducerState<TabPlatformVersionState>(
    TabPlatformVersionConstants.reducerName,
    {
        platformVersions: {
            metadata: getInitialMetadata(),
            records: []
        },
        platformVersion: partialFillOf<PlatformVersion1CDataModel>(),
        hasSuccessFor: {
            hasPlatformVersionsReceived: false
        }
    }
);

/**
 * Объединённые части редюсера для всего состояния Segments -> Платформа 1С
 */
const partialReducers = [
    getPlatformVersionsReducer,
    deletePlatformVersionReducer,
    getPlatformVersionReducer,
    createPlatformVersionReducer,
    editPlatformVersionReducer
];

/**
 * Редюсер Segments -> Платформа 1С
 */
export const TabPlatformVersionReducer = createReducer(initialState, partialReducers);