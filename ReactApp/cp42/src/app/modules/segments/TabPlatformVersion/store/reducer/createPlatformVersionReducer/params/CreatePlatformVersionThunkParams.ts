import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import IForceThunkParam from 'core/redux/interfaces/IForceThunkParam';

/**
 * Thunk параметры для создания версии платформы 1С
 */
export type CreatePlatformVersionThunkParams = IForceThunkParam & PlatformVersion1CDataModel;