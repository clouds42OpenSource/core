import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения создания версии платформы 1С
 */
export type CreatePlatformVersionActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении создания версии платформы 1С
 */
export type CreatePlatformVersionActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении создания версии платформы 1С
 */
export type CreatePlatformVersionActionSuccessPayload = ReducerActionSuccessPayload;

/**
 * Все возможные Action при создании версии платформы 1С
 */
export type CreatePlatformVersionActionAnyPayload =
    | CreatePlatformVersionActionStartPayload
    | CreatePlatformVersionActionFailedPayload
    | CreatePlatformVersionActionSuccessPayload;