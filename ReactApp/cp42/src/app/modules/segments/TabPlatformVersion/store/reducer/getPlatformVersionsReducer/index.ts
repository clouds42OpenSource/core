import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { TabPlatformVersionActions } from 'app/modules/segments/TabPlatformVersion/store/actions';
import { TabPlatformVersionState } from 'app/modules/segments/TabPlatformVersion/store/reducer';
import { GetPlatformVersionsActionAnyPayload, GetPlatformVersionsActionSuccessPayload } from 'app/modules/segments/TabPlatformVersion/store/reducer/getPlatformVersionsReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для получения списка данных по версиям платформы 1С
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const getPlatformVersionsReducer: TPartialReducerObject<TabPlatformVersionState, GetPlatformVersionsActionAnyPayload> =
    (state, action) => {
        const processId = TabPlatformVersionProcessId.GetPlatformVersions;
        const actions = TabPlatformVersionActions.GetPlatformVersions;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as GetPlatformVersionsActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, {
                    platformVersions: payload,
                    hasSuccessFor: {
                        ...state,
                        hasPlatformVersionsReceived: true
                    }
                });
            }
        });
    };