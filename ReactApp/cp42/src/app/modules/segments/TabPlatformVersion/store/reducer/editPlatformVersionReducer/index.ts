import { reducerActionState } from 'app/common/functions/reducerActionState';
import { TabPlatformVersionProcessId } from 'app/modules/segments/TabPlatformVersion';
import { TabPlatformVersionActions } from 'app/modules/segments/TabPlatformVersion/store/actions';
import { TabPlatformVersionState } from 'app/modules/segments/TabPlatformVersion/store/reducer';
import { EditPlatformVersionActionAnyPayload, EditPlatformVersionActionSuccessPayload } from 'app/modules/segments/TabPlatformVersion/store/reducer/editPlatformVersionReducer/payloads';
import { reducerStateProcessSuccess } from 'core/redux/functions';
import { TPartialReducerObject } from 'core/redux/types';

/**
 * Редюсер для редактирования версии платформы 1С
 * @param state состояние редюсера
 * @param action действие над редюсером
 */
export const editPlatformVersionReducer: TPartialReducerObject<TabPlatformVersionState, EditPlatformVersionActionAnyPayload> =
    (state, action) => {
        const processId = TabPlatformVersionProcessId.EditPlatformVersion;
        const actions = TabPlatformVersionActions.EditPlatformVersion;

        return reducerActionState({
            action,
            actions,
            processId,
            state,
            onSuccessProcessState: () => {
                const payload = action.payload as EditPlatformVersionActionSuccessPayload;
                return reducerStateProcessSuccess(state, processId, payload);
            }
        });
    };