import { PlatformVersion1CDataModel } from 'app/web/InterlayerApiProxy/SegmentsApiProxy/TabPlatformVersionApiProxy';
import { ReducerActionFailedPayload, ReducerActionStartPayload, ReducerActionSuccessPayload } from 'core/redux/interfaces';

/**
 * Данные в редюсер при старте выполнения получения версии платформы 1С
 */
export type GetPlatformVersionActionStartPayload = ReducerActionStartPayload;

/**
 * Данные в редюсер при неуспешном выполнении получения версии платформы 1С
 */
export type GetPlatformVersionActionFailedPayload = ReducerActionFailedPayload;

/**
 * Данные в редюсер при успешном выполнении получения версии платформы 1С
 */
export type GetPlatformVersionActionSuccessPayload = ReducerActionSuccessPayload & PlatformVersion1CDataModel;

/**
 * Все возможные Action при получении версии платформы 1С
 */
export type GetPlatformVersionActionAnyPayload =
    | GetPlatformVersionActionStartPayload
    | GetPlatformVersionActionFailedPayload
    | GetPlatformVersionActionSuccessPayload;